object FmDeposimp: TFmDeposimp
  Left = 399
  Top = 174
  Caption = 'CHQ-DEPOS-001 :: Impress'#227'o de Dep'#243'sitos de Cheques'
  ClientHeight = 557
  ClientWidth = 792
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Label12: TLabel
    Left = 368
    Top = 240
    Width = 38
    Height = 13
    Caption = 'Label12'
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 48
    Width = 792
    Height = 395
    ActivePage = TabSheet2
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Configura'#231#227'o de impress'#227'o'
      object RGAgrupa: TRadioGroup
        Left = 0
        Top = 326
        Width = 784
        Height = 41
        Align = alBottom
        Caption = ' Agrupamentos: '
        Columns = 4
        ItemIndex = 0
        Items.Strings = (
          '0'
          '1'
          '2'
          '3')
        TabOrder = 0
      end
      object Panel2: TPanel
        Left = 0
        Top = 206
        Width = 784
        Height = 120
        Align = alBottom
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 1
        object RGOrdem1: TRadioGroup
          Left = 0
          Top = 0
          Width = 195
          Height = 120
          Align = alLeft
          Caption = ' Ordem 1: '
          ItemIndex = 0
          Items.Strings = (
            'Cliente'
            'Dia Dep'#243'sito'
            'M'#234's Dep'#243'sito'
            'Emitente'
            'CPF/CNPJ')
          TabOrder = 0
          OnClick = RGOrdem1Click
        end
        object RGOrdem2: TRadioGroup
          Left = 195
          Top = 0
          Width = 195
          Height = 120
          Align = alLeft
          Caption = ' Ordem 2: '
          ItemIndex = 1
          Items.Strings = (
            'Cliente'
            'Dia Dep'#243'sito'
            'M'#234's Dep'#243'sito'
            'Emitente'
            'CPF/CNPJ')
          TabOrder = 1
          OnClick = RGOrdem1Click
        end
        object RGOrdem3: TRadioGroup
          Left = 390
          Top = 0
          Width = 195
          Height = 120
          Align = alLeft
          Caption = ' Ordem 3: '
          ItemIndex = 2
          Items.Strings = (
            'Cliente'
            'Dia Dep'#243'sito'
            'M'#234's Dep'#243'sito'
            'Emitente'
            'CPF/CNPJ')
          TabOrder = 2
          OnClick = RGOrdem1Click
        end
        object RGOrdem4: TRadioGroup
          Left = 585
          Top = 0
          Width = 195
          Height = 120
          Align = alLeft
          Caption = ' Ordem 4: '
          ItemIndex = 3
          Items.Strings = (
            'Cliente'
            'Dia Dep'#243'sito'
            'M'#234's Dep'#243'sito'
            'Emitente'
            'CPF/CNPJ')
          TabOrder = 3
          OnClick = RGOrdem1Click
        end
      end
      object PainelDados: TPanel
        Left = 0
        Top = 0
        Width = 784
        Height = 185
        Align = alTop
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 2
        object Label3: TLabel
          Left = 8
          Top = 8
          Width = 35
          Height = 13
          Caption = 'Cliente:'
        end
        object Label34: TLabel
          Left = 384
          Top = 8
          Width = 98
          Height = 13
          Caption = 'Data dep'#243'sito inicial:'
        end
        object Label1: TLabel
          Left = 488
          Top = 8
          Width = 91
          Height = 13
          Caption = 'Data dep'#243'sito final:'
        end
        object Label2: TLabel
          Left = 8
          Top = 48
          Width = 44
          Height = 13
          Caption = 'Emitente:'
        end
        object Label4: TLabel
          Left = 436
          Top = 48
          Width = 66
          Height = 13
          Caption = 'CPF emitente:'
        end
        object LaColigado: TLabel
          Left = 4
          Top = 139
          Width = 44
          Height = 13
          Caption = 'Coligado:'
          Enabled = False
        end
        object EdCliente: TdmkEditCB
          Left = 8
          Top = 24
          Width = 57
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdClienteChange
          DBLookupComboBox = CBCliente
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBCliente: TdmkDBLookupComboBox
          Left = 68
          Top = 24
          Width = 313
          Height = 21
          KeyField = 'Codigo'
          ListField = 'NOMECLIENTE'
          ListSource = DsClientes
          TabOrder = 1
          dmkEditCB = EdCliente
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object TPIni: TDateTimePicker
          Left = 384
          Top = 24
          Width = 101
          Height = 21
          Date = 38675.000000000000000000
          Time = 0.714976851901155900
          TabOrder = 2
          OnChange = TPIniChange
        end
        object TPFim: TDateTimePicker
          Left = 488
          Top = 24
          Width = 101
          Height = 21
          Date = 38675.000000000000000000
          Time = 0.714976851901155900
          TabOrder = 3
          OnChange = TPFimChange
        end
        object EdEmitente: TdmkEdit
          Left = 8
          Top = 64
          Width = 161
          Height = 21
          TabOrder = 5
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnChange = EdEmitenteChange
        end
        object EdCPF: TdmkEdit
          Left = 436
          Top = 64
          Width = 113
          Height = 21
          TabOrder = 7
          FormatType = dmktfString
          MskType = fmtCPFJ
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnChange = EdCPFChange
        end
        object RGMascara: TRadioGroup
          Left = 172
          Top = 48
          Width = 257
          Height = 41
          Caption = ' M'#225'scara: '
          Columns = 3
          ItemIndex = 0
          Items.Strings = (
            'Prefixo e sufixo'
            'Prefixo'
            'Sufixo')
          TabOrder = 6
          OnClick = RGMascaraClick
        end
        object GroupBox1: TGroupBox
          Left = 4
          Top = 92
          Width = 337
          Height = 45
          Caption = ' Status pagamento: '
          TabOrder = 9
          object CkP0: TCheckBox
            Left = 8
            Top = 16
            Width = 57
            Height = 17
            Caption = 'Abertos'
            TabOrder = 0
            OnClick = CkP0Click
          end
          object CkP1: TCheckBox
            Left = 68
            Top = 16
            Width = 82
            Height = 17
            Caption = 'Pago parcial'
            TabOrder = 1
            OnClick = CkP1Click
          end
          object CkP2: TCheckBox
            Left = 152
            Top = 16
            Width = 53
            Height = 17
            Caption = 'Pagos'
            TabOrder = 2
            OnClick = CkP2Click
          end
          object CkP3: TCheckBox
            Left = 208
            Top = 16
            Width = 82
            Height = 17
            Caption = 'Antecipados'
            TabOrder = 3
            OnClick = CkP2Click
          end
        end
        object EdColigado: TdmkEditCB
          Left = 4
          Top = 155
          Width = 57
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 13
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdColigadoChange
          DBLookupComboBox = CBColigado
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBColigado: TdmkDBLookupComboBox
          Left = 64
          Top = 155
          Width = 717
          Height = 21
          Enabled = False
          KeyField = 'Codigo'
          ListField = 'NOMECLIENTE'
          ListSource = DsColigados
          TabOrder = 14
          dmkEditCB = EdColigado
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object RGRepasse: TRadioGroup
          Left = 339
          Top = 92
          Width = 150
          Height = 45
          Caption = ' Status repasse: '
          Columns = 3
          ItemIndex = 0
          Items.Strings = (
            'N'#227'o'
            'Sim'
            'N+S')
          TabOrder = 10
          OnClick = RGRepasseClick
        end
        object RGReforco: TRadioGroup
          Left = 484
          Top = 92
          Width = 149
          Height = 45
          Caption = ' Status refor'#231'o caixa: '
          Columns = 3
          ItemIndex = 0
          Items.Strings = (
            'N'#227'o'
            'Sim'
            'N+S')
          TabOrder = 11
          OnClick = RGReforcoClick
        end
        object BtConfigura: TBitBtn
          Tag = 263
          Left = 594
          Top = 4
          Width = 90
          Height = 40
          Caption = 'Con&figura'
          NumGlyphs = 2
          TabOrder = 4
          OnClick = BtConfiguraClick
        end
        object RGTipo: TRadioGroup
          Left = 552
          Top = 48
          Width = 229
          Height = 45
          Caption = ' Tipo: '
          Columns = 2
          ItemIndex = 0
          Items.Strings = (
            'Anal'#237'tico'
            'Sint'#233'tico')
          TabOrder = 8
        end
        object RGAlinIts: TRadioGroup
          Left = 631
          Top = 92
          Width = 150
          Height = 45
          Caption = ' Cheques devolvidos: '
          Columns = 3
          ItemIndex = 0
          Items.Strings = (
            'N'#227'o'
            'Sim'
            'N+S')
          TabOrder = 12
          OnClick = RGRepasseClick
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Confer'#234'ncia de cheques'
      ImageIndex = 1
      object Panel3: TPanel
        Left = 0
        Top = 53
        Width = 784
        Height = 48
        Align = alTop
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 1
        object Label36: TLabel
          Left = 8
          Top = 4
          Width = 143
          Height = 13
          Caption = 'Leitura pela banda magn'#233'tica:'
        end
        object Label5: TLabel
          Left = 296
          Top = 4
          Width = 34
          Height = 13
          Caption = 'Banco:'
        end
        object Label6: TLabel
          Left = 340
          Top = 4
          Width = 42
          Height = 13
          Caption = 'Ag'#234'ncia:'
        end
        object Label8: TLabel
          Left = 392
          Top = 4
          Width = 31
          Height = 13
          Caption = 'Conta:'
        end
        object Label32: TLabel
          Left = 508
          Top = 4
          Width = 40
          Height = 13
          Caption = 'Cheque:'
        end
        object Label10: TLabel
          Left = 644
          Top = 4
          Width = 124
          Height = 13
          Caption = 'Itens e valores conferidos:'
        end
        object Label11: TLabel
          Left = 564
          Top = 4
          Width = 27
          Height = 13
          Caption = 'Valor:'
        end
        object EdBanda: TdmkEdit
          Left = 8
          Top = 20
          Width = 285
          Height = 21
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnChange = EdBandaChange
        end
        object EdBanco: TdmkEdit
          Left = 296
          Top = 20
          Width = 41
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdAgencia: TdmkEdit
          Left = 340
          Top = 20
          Width = 49
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdConta: TdmkEdit
          Left = 392
          Top = 20
          Width = 113
          Height = 21
          Enabled = False
          TabOrder = 3
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdCheque: TdmkEdit
          Left = 508
          Top = 20
          Width = 53
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 4
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object DBEdItens: TDBEdit
          Left = 644
          Top = 20
          Width = 41
          Height = 21
          TabStop = False
          DataField = 'QTDE'
          DataSource = DsSumD
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 5
          OnChange = DBEdItensChange
        end
        object DBEdValor: TDBEdit
          Left = 688
          Top = 20
          Width = 89
          Height = 21
          TabStop = False
          DataField = 'Valor'
          DataSource = DsSumD
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 6
          OnChange = DBEdValorChange
        end
        object EdValor: TdmkEdit
          Left = 564
          Top = 20
          Width = 77
          Height = 21
          Alignment = taRightJustify
          TabOrder = 7
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnExit = EdValorExit
        end
      end
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 784
        Height = 53
        Align = alTop
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Label9: TLabel
          Left = 644
          Top = 8
          Width = 119
          Height = 13
          Caption = 'Itens e valores a conferir:'
          FocusControl = DBEdit2
        end
        object Label7: TLabel
          Left = 108
          Top = 6
          Width = 95
          Height = 13
          Caption = 'Soma selecionados:'
        end
        object BtLimpar: TBitBtn
          Tag = 170
          Left = 2
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Limpar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtLimparClick
        end
        object RGOQue: TRadioGroup
          Left = 356
          Top = 1
          Width = 283
          Height = 47
          Caption = ' Conferir: '
          Columns = 3
          ItemIndex = 0
          Items.Strings = (
            'CMC-7 e Valor'
            'S'#243' o CMC-7')
          TabOrder = 1
        end
        object DBEdit2: TDBEdit
          Left = 644
          Top = 24
          Width = 41
          Height = 21
          DataField = 'QTDE'
          DataSource = DsSumT
          TabOrder = 2
        end
        object DBEdit1: TDBEdit
          Left = 688
          Top = 24
          Width = 89
          Height = 21
          DataField = 'Valor'
          DataSource = DsSumT
          TabOrder = 3
        end
        object BtDeposito: TBitBtn
          Tag = 264
          Left = 212
          Top = 4
          Width = 90
          Height = 40
          Caption = '&Destino'
          NumGlyphs = 2
          TabOrder = 4
          OnClick = BtDepositoClick
        end
        object EdSoma: TdmkEdit
          Left = 108
          Top = 22
          Width = 101
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 5
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnExit = EdValorExit
        end
      end
      object DBG1: TDBGrid
        Left = 0
        Top = 101
        Width = 784
        Height = 266
        Align = alClient
        DataSource = DsLotIts
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit, dgMultiSelect]
        TabOrder = 2
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnCellClick = DBG1CellClick
        OnDrawColumnCell = DBG1DrawColumnCell
        OnKeyDown = DBG1KeyDown
        Columns = <
          item
            Expanded = False
            FieldName = 'Depositado'
            Title.Caption = 'CH'
            Width = 18
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CONF_V'
            Title.Caption = '$'
            Width = 18
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ReforcoCxa'
            Title.Caption = 'Cx'
            Width = 18
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Controle'
            Width = 49
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Banco'
            Title.Caption = 'Bco'
            Width = 28
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Agencia'
            Title.Caption = 'Ag.'
            Width = 32
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Conta'
            Width = 113
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Cheque'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CPF'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Emitente'
            Width = 184
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Valor'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValDeposito'
            Title.Caption = '$ Dep'#243'sito'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMECART'
            Title.Caption = 'Carteira de destino'
            Width = 485
            Visible = True
          end>
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 744
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 696
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 450
        Height = 32
        Caption = 'Impress'#227'o de Dep'#243'sitos de Cheques'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 450
        Height = 32
        Caption = 'Impress'#227'o de Dep'#243'sitos de Cheques'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 450
        Height = 32
        Caption = 'Impress'#227'o de Dep'#243'sitos de Cheques'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 443
    Width = 792
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 788
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 487
    Width = 792
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 646
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BitBtn1: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 644
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtPesquisa: TBitBtn
        Tag = 22
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Pesquisa'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtPesquisaClick
      end
      object BtImprime: TBitBtn
        Tag = 5
        Left = 148
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Imprime'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtImprimeClick
      end
    end
  end
  object QrClientes: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMECLIENTE, Codigo'
      'FROM entidades'
      'WHERE Cliente1='#39'V'#39
      'ORDER BY NOMECLIENTE')
    Left = 292
    Top = 64
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 320
    Top = 64
  end
  object QrLotIts: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrLotItsAfterOpen
    AfterClose = QrLotItsAfterClose
    OnCalcFields = QrLotItsCalcFields
    SQL.Strings = (
      '/*'
      
        'SELECT ca.Nome NOMECART, MONTH(li.DDeposito)+YEAR(li.DDeposito)*' +
        '100+0.00 PERIODO,'
      
        'CASE WHEN cl.Tipo=0 THEN cl.RazaoSocial ELSE cl.Nome END NOMECLI' +
        'ENTE,'
      
        'CASE WHEN co.Tipo=0 THEN co.RazaoSocial ELSE co.Nome END NOMECOL' +
        'IGADO,'
      'li.Banco, li.Agencia, li.Conta, li.Cheque, li.Codigo,'
      'li.Controle, li.Depositado, li.Emitente, li.CPF,'
      'li.DDeposito, li.Vencto, li.Valor, li.ValDeposito,'
      'li.ReforcoCxa, lo.NF, lo.Cliente'
      'FROM lot esits li'
      'LEFT JOIN lot es     lo ON lo.Codigo=li.Codigo'
      'LEFT JOIN repasits  ri ON ri.Origem=li.Controle'
      'LEFT JOIN repas     re ON re.Codigo=ri.Codigo'
      'LEFT JOIN entidades cl ON cl.Codigo=lo.Cliente'
      'LEFT JOIN entidades co ON co.Codigo=re.Coligado'
      'LEFT JOIN carteiras ca ON ca.Codigo=li.CartDep'
      'WHERE lo.Tipo in (0, 2)'
      'AND lo.TxCompra+lo.ValValorem+ 1 >= 0.01'
      'AND li.DDeposito BETWEEN "2006-11-08" AND "2006-12-08"'
      'AND li.Quitado in (0, 1, 2)'
      'AND li.Repassado in (0, 1)'
      'AND li.ReforcoCxa in (0, 1)'
      'AND li.NaoDeposita=0'
      'ORDER BY NOMECLIENTE, PERIODO, Emitente, CPF'
      '*/'
      ''
      'SELECT ca.Nome NOMECART, '
      'MONTH(li.DDeposito)+YEAR(li.DDeposito)*100+0.00 PERIODO,'
      'IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMECLIENTE,'
      'IF(co.Tipo=0, co.RazaoSocial, co.Nome) NOMECOLIGADO,'
      'li.Banco, li.Agencia, li.ContaCorrente, li.Documento, '
      'li.FatNum, li.FatParcela, li.Depositado, li.Emitente, '
      'li.CNPJCPF, li.DDeposito, li.Vencimento, li.Credito, '
      'li.ValDeposito, li.ReforcoCxa, lo.NF, lo.Cliente'
      'FROM lct0001a li'
      'LEFT JOIN lot0001a lo ON lo.Codigo=li.FatNum'
      'LEFT JOIN repasits  ri ON ri.Origem=li.FatParcela'
      'LEFT JOIN repas     re ON re.Codigo=ri.Codigo'
      'LEFT JOIN entidades cl ON cl.Codigo=lo.Cliente'
      'LEFT JOIN entidades co ON co.Codigo=re.Coligado'
      'LEFT JOIN carteiras ca ON ca.Codigo=li.CartDep'
      'WHERE li.FatID=301 '
      'AND lo.Tipo in (0, 2)'
      'AND lo.TxCompra + lo.ValValorem + 1 >= 0.01'
      'AND li.DDeposito BETWEEN "2006-11-08" AND "2006-12-08"'
      'AND li.Quitado in (0, 1, 2)'
      'AND li.Repassado in (0, 1)'
      'AND li.ReforcoCxa in (0, 1)'
      'AND li.NaoDeposita=0'
      'ORDER BY NOMECLIENTE, PERIODO, Emitente, CNPJCPF'
      '')
    Left = 152
    Top = 220
    object QrLotItsPERIODO: TFloatField
      FieldName = 'PERIODO'
      Origin = 'PERIODO'
    end
    object QrLotItsBanco: TIntegerField
      FieldName = 'Banco'
      Required = True
      DisplayFormat = '000'
    end
    object QrLotItsEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrLotItsDDeposito: TDateField
      FieldName = 'DDeposito'
      Required = True
    end
    object QrLotItsDDeposito_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DDeposito_TXT'
      Calculated = True
    end
    object QrLotItsMES_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MES_TXT'
      Size = 40
      Calculated = True
    end
    object QrLotItsCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Size = 30
      Calculated = True
    end
    object QrLotItsValor_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Valor_TXT'
      Size = 40
      Calculated = True
    end
    object QrLotItsCONTAGEM: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'CONTAGEM'
      Calculated = True
    end
    object QrLotItsNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Origin = 'NOMECLIENTE'
      Size = 100
    end
    object QrLotItsNOMESTATUS: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESTATUS'
      Size = 30
      Calculated = True
    end
    object QrLotItsNF: TIntegerField
      FieldName = 'NF'
    end
    object QrLotItsNOMECOLIGADO: TWideStringField
      FieldName = 'NOMECOLIGADO'
      Origin = 'NOMECOLIGADO'
      Size = 100
    end
    object QrLotItsDepositado: TSmallintField
      FieldName = 'Depositado'
      Required = True
    end
    object QrLotItsValDeposito: TFloatField
      FieldName = 'ValDeposito'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrLotItsCONF_V: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'CONF_V'
      Calculated = True
    end
    object QrLotItsCONF_I: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'CONF_I'
      Calculated = True
    end
    object QrLotItsNF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NF_TXT'
      Calculated = True
    end
    object QrLotItsReforcoCxa: TSmallintField
      FieldName = 'ReforcoCxa'
      Required = True
    end
    object QrLotItsNOMECART: TWideStringField
      FieldName = 'NOMECART'
      Origin = 'carteiras.Nome'
      Size = 100
    end
    object QrLotItsCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrLotItsAgencia: TIntegerField
      FieldName = 'Agencia'
      DisplayFormat = '0000'
    end
    object QrLotItsContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrLotItsDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrLotItsFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrLotItsFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrLotItsCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrLotItsVencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrLotItsCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrLotItsCREDITO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CREDITO_TXT'
      Size = 30
      Calculated = True
    end
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 100
    OnTimer = Timer1Timer
    Left = 284
    Top = 124
  end
  object QrColigados: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMECLIENTE, Codigo'
      'FROM entidades'
      'WHERE Fornece2='#39'V'#39
      'ORDER BY NOMECLIENTE')
    Left = 496
    Top = 336
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
  end
  object DsColigados: TDataSource
    DataSet = QrColigados
    Left = 524
    Top = 336
  end
  object DsSumD: TDataSource
    DataSet = QrSumD
    Left = 204
    Top = 256
  end
  object QrSumT: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrLotItsCalcFields
    SQL.Strings = (
      'SELECT SUM(li.Valor) Valor, COUNT(li.Controle) QTDE'
      'FROM lot esits li'
      'LEFT JOIN lot es     lo ON lo.Codigo=li.Codigo'
      'LEFT JOIN repasits  ri ON ri.Origem=li.Controle'
      'LEFT JOIN repas     re ON re.Codigo=ri.Codigo'
      'LEFT JOIN entidades cl ON cl.Codigo=lo.Cliente'
      'LEFT JOIN entidades co ON co.Codigo=re.Coligado'
      'WHERE lo.Tipo=0'
      'AND li.DDeposito BETWEEN '#39'2004-01-01'#39' AND '#39'2007-01-01'#39)
    Left = 176
    Top = 284
    object QrSumTValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrSumTQTDE: TLargeintField
      FieldName = 'QTDE'
      Required = True
      DisplayFormat = '0;-0; '
    end
  end
  object QrSumD: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrLotItsCalcFields
    SQL.Strings = (
      'SELECT SUM(li.Valor) Valor, COUNT(li.Controle) QTDE'
      'FROM lot esits li'
      'LEFT JOIN lot es     lo ON lo.Codigo=li.Codigo'
      'LEFT JOIN repasits  ri ON ri.Origem=li.Controle'
      'LEFT JOIN repas     re ON re.Codigo=ri.Codigo'
      'LEFT JOIN entidades cl ON cl.Codigo=lo.Cliente'
      'LEFT JOIN entidades co ON co.Codigo=re.Coligado'
      'WHERE lo.Tipo=0 AND Depositado=1'
      'AND li.DDeposito BETWEEN '#39'2004-01-01'#39' AND '#39'2007-01-01'#39)
    Left = 176
    Top = 256
    object QrSumDValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrSumDQTDE: TLargeintField
      FieldName = 'QTDE'
      Required = True
      DisplayFormat = '0;-0; '
    end
  end
  object DsLotIts: TDataSource
    DataSet = QrLotIts
    Left = 180
    Top = 220
  end
  object DsSumT: TDataSource
    DataSet = QrSumT
    Left = 204
    Top = 284
  end
  object Timer2: TTimer
    Enabled = False
    Interval = 300
    OnTimer = Timer2Timer
    Left = 321
    Top = 327
  end
  object PMDeposito: TPopupMenu
    Left = 260
    Top = 360
    object Selecionados1: TMenuItem
      Caption = '&Selecionado(s)'
      OnClick = Selecionados1Click
    end
    object Tudo1: TMenuItem
      Caption = '&Tudo'
      OnClick = Tudo1Click
    end
  end
  object Timer3: TTimer
    Enabled = False
    Interval = 300
    OnTimer = Timer3Timer
    Left = 424
    Top = 248
  end
  object Query1: TMySQLQuery
    Database = Dmod.MyDB
    Left = 468
    Top = 428
    object Query1Controle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object frxDsLotIts: TfrxDBDataset
    UserName = 'frxDsLotIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'PERIODO=PERIODO'
      'Banco=Banco'
      'Emitente=Emitente'
      'DDeposito=DDeposito'
      'DDeposito_TXT=DDeposito_TXT'
      'MES_TXT=MES_TXT'
      'CPF_TXT=CPF_TXT'
      'Valor_TXT=Valor_TXT'
      'CONTAGEM=CONTAGEM'
      'NOMECLIENTE=NOMECLIENTE'
      'NOMESTATUS=NOMESTATUS'
      'NF=NF'
      'NOMECOLIGADO=NOMECOLIGADO'
      'Depositado=Depositado'
      'ValDeposito=ValDeposito'
      'CONF_V=CONF_V'
      'CONF_I=CONF_I'
      'NF_TXT=NF_TXT'
      'ReforcoCxa=ReforcoCxa'
      'NOMECART=NOMECART'
      'Cliente=Cliente'
      'Agencia=Agencia'
      'ContaCorrente=ContaCorrente'
      'Documento=Documento'
      'FatNum=FatNum'
      'FatParcela=FatParcela'
      'CNPJCPF=CNPJCPF'
      'Vencimento=Vencimento'
      'Credito=Credito'
      'CREDITO_TXT=CREDITO_TXT')
    DataSet = QrLotIts
    BCDToCurrency = False
    
    Left = 176
    Top = 84
  end
  object frxDeposIts: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39687.940250463000000000
    ReportOptions.LastChange = 39687.940250463000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure ReportTitle1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  {    '
      '  PageHeader1.Height := <VARF_ANALITICO> + 72;'
      '  MasterData1.Height := <VARF_ANALITICO>;'
      '  Memo1.Height := <VARF_ANALITICO>;'
      '  Memo2.Height := <VARF_ANALITICO>;'
      '  Memo3.Height := <VARF_ANALITICO>;'
      '  Memo8.Height := <VARF_ANALITICO>;'
      '  Memo9.Height := <VARF_ANALITICO>;'
      '  Memo10.Height := <VARF_ANALITICO>;'
      '  Memo11.Height := <VARF_ANALITICO>;'
      '  Memo12.Height := <VARF_ANALITICO>;'
      '  Memo13.Height := <VARF_ANALITICO>;'
      '  Memo14.Height := <VARF_ANALITICO>;'
      '  Memo15.Height := <VARF_ANALITICO>;'
      '  Memo16.Height := <VARF_ANALITICO>;'
      '  Memo17.Height := <VARF_ANALITICO>;'
      '  Memo18.Height := <VARF_ANALITICO>;'
      '  Memo20.Height := <VARF_ANALITICO>;'
      '  Memo21.Height := <VARF_ANALITICO>;'
      '  Memo22.Height := <VARF_ANALITICO>;'
      '  Memo23.Height := <VARF_ANALITICO>;'
      '  Memo25.Height := <VARF_ANALITICO>;'
      '  Memo26.Height := <VARF_ANALITICO>;'
      '  Memo29.Height := <VARF_ANALITICO>;'
      '  Memo30.Height := <VARF_ANALITICO>;'
      '  Memo33.Height := <VARF_ANALITICO>;'
      '  GH1.Height := <VARF_ANALITICO>;'
      '  GH2.Height := <VARF_ANALITICO>;'
      '  GH3.Height := <VARF_ANALITICO>;'
      '  GF1.Height := 17;'
      '  GF2.Height := 17;'
      '  GF3.Height := 17;'
      '  Memo34.Font.Style := 0;'
      '  Memo36.Font.Style := 0;'
      '  Memo37.Font.Style := 0;'
      '  Memo38.Font.Style := 0;'
      '  Memo44.Font.Style := 0;'
      '  Memo45.Font.Style := 0;'
      '  }      '
      'end;'
      ''
      'procedure GF3OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if <VFR_ORD3> > 0 then'
      '  GF3.Visible := True else GF3.Visible := False;  '
      'end;'
      ''
      'procedure GF2OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if <VFR_ORD2> > 0 then'
      '  GF2.Visible := True else GF2.Visible := False;  '
      'end;'
      ''
      'procedure GF1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if <VFR_ORD1> > 0 then'
      '  GF1.Visible := True else GF1.Visible := False;  '
      'end;'
      ''
      'procedure PageHeader1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if <VFR_ORD1> > 0 then'
      '  GH1.Visible := True else GH1.Visible := False;'
      ''
      '  if <VFR_ORD2> > 0 then'
      '  GH2.Visible := True else GH2.Visible := False;'
      ''
      '  if <VFR_ORD3> > 0 then'
      '  GH3.Visible := True else GH3.Visible := False;'
      ''
      
        '  if <VFR_ORD1> = 0 then GH1.Condition := '#39'frxDsLotIts."NOMECLIE' +
        'NTE"'#39';'
      
        '  if <VFR_ORD1> = 1 then GH1.Condition := '#39'frxDsLotIts."NOMECLIE' +
        'NTE"'#39';'
      
        '  if <VFR_ORD1> = 2 then GH1.Condition := '#39'frxDsLotIts."DDeposit' +
        'o_TXT"'#39';'
      
        '  if <VFR_ORD1> = 3 then GH1.Condition := '#39'frxDsLotIts."PERIODO"' +
        #39';'
      
        '  if <VFR_ORD1> = 4 then GH1.Condition := '#39'frxDsLotIts."Emitente' +
        '"'#39';'
      
        '  if <VFR_ORD1> = 5 then GH1.Condition := '#39'frxDsLotIts."CNPJCPF"' +
        #39';'
      '    '
      '  if <VFR_ORD2> = 0 then GH2.Condition := GH1.Condition;'
      
        '  if <VFR_ORD2> = 1 then GH2.Condition := '#39'frxDsLotIts."NOMECLIE' +
        'NTE"'#39';'
      
        '  if <VFR_ORD2> = 2 then GH2.Condition := '#39'frxDsLotIts."DDeposit' +
        'o_TXT"'#39';'
      
        '  if <VFR_ORD2> = 3 then GH2.Condition := '#39'frxDsLotIts."PERIODO"' +
        #39';'
      
        '  if <VFR_ORD2> = 4 then GH2.Condition := '#39'frxDsLotIts."Emitente' +
        '"'#39';'
      
        '  if <VFR_ORD2> = 5 then GH2.Condition := '#39'frxDsLotIts."CNPJCPF"' +
        #39';'
      '    '
      '  if <VFR_ORD3> = 0 then GH3.Condition := GH2.Condition;'
      
        '  if <VFR_ORD3> = 1 then GH3.Condition := '#39'frxDsLotIts."NOMECLIE' +
        'NTE"'#39';'
      
        '  if <VFR_ORD3> = 2 then GH3.Condition := '#39'frxDsLotIts."DDeposit' +
        'o_TXT"'#39';'
      
        '  if <VFR_ORD3> = 3 then GH3.Condition := '#39'frxDsLotIts."PERIODO"' +
        #39';'
      
        '  if <VFR_ORD3> = 4 then GH3.Condition := '#39'frxDsLotIts."Emitente' +
        '"'#39';'
      
        '  if <VFR_ORD3> = 5 then GH3.Condition := '#39'frxDsLotIts."CNPJCPF"' +
        #39';'
      'end;'
      ''
      'begin                                                  '
      'end.')
    OnGetValue = frxDeposIts_GetValue
    Left = 204
    Top = 84
    Datasets = <
      item
        DataSet = frxDsLotIts
        DataSetName = 'frxDsLotIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 86.929190000000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        OnBeforePrint = 'ReportTitle1OnBeforePrint'
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 589.795300000000000000
          Top = 37.795300000000000000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 53.795300000000000000
          Width = 716.000000000000000000
          Height = 26.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 2.000000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CLIENTE]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 100.149660000000000000
        Top = 128.504020000000000000
        Width = 793.701300000000000000
        OnBeforePrint = 'PageHeader1OnBeforePrint'
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 7.559060000000017000
          Width = 700.000000000000000000
          Height = 22.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'RELAT'#211'RIO DE DEP'#211'SITO CHEQUES [VARF_SITUACOES]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 35.338590000000010000
          Width = 340.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_TODOS]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 557.795300000000000000
          Top = 35.338590000000010000
          Width = 180.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Per'#237'odo: [VARF_PERIODO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 79.370095830000000000
          Top = 79.370078740000000000
          Width = 22.677165350000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Bco.')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 102.047268500000000000
          Top = 79.370078740000000000
          Width = 30.236220470000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ag.')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283488980000000000
          Top = 79.370078740000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Conta')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 192.755929920000000000
          Top = 79.370078740000000000
          Width = 37.795275590000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '000000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cheq.')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 272.126008660000000000
          Top = 79.370078740157500000
          Width = 207.873991340000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Emitente')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 480.000000000000000000
          Top = 79.370078740157500000
          Width = 102.047244090000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'CPF / CNPJ')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 676.535433070000000000
          Top = 79.370078740157500000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 582.047244090000000000
          Top = 79.370078740157500000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Vencto')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 631.181102360000000000
          Top = 79.370078740157500000
          Width = 45.354330710000000000
          Height = 17.000000000000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Dep'#243'sito')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 57.338590000000010000
          Width = 700.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_FILTROS]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 230.551205510000000000
          Top = 79.370078740000000000
          Width = 41.574803150000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '000000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'N.F.')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 469.795300000000000000
          Top = 35.338590000000010000
          Width = 84.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Dep'#243'sito: [VARF_REFORCO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 381.795300000000000000
          Top = 35.338590000000010000
          Width = 84.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Repasse: [VARF_REPASSE]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 41.574830000000000000
          Top = 79.370129999999990000
          Width = 37.795273150000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '000000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cliente')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 17.000000000000000000
        Top = 427.086890000000000000
        Width = 793.701300000000000000
        DataSet = frxDsLotIts
        DataSetName = 'frxDsLotIts'
        RowCount = 0
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 79.370130000000000000
          Width = 22.677165350000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39', <frxDsLotIts."Banco">)]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 102.047268500000000000
          Width = 30.236220470000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39', <frxDsLotIts."Agencia">)]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283488980000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLotIts."ContaCorrente"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 192.755929920000000000
          Width = 37.795275590000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39', <frxDsLotIts."Documento">)]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 272.126008660000000000
          Width = 207.873991340000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsLotIts."Emitente"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 480.000000000000000000
          Width = 102.047244094488000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsLotIts."CPF_TXT"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 676.535433070000000000
          Width = 60.472440944881890000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLotIts."Credito"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 582.047244090000000000
          Width = 49.133858267716500000
          Height = 17.000000000000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLotIts."Vencimento"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 631.181102360000000000
          Width = 45.354330708661400000
          Height = 17.000000000000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLotIts."DDeposito"]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 230.551205510000000000
          Width = 41.574803150000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLotIts."NF_TXT"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 41.574830000000000000
          Width = 37.795273150000000000
          Height = 17.000000000000000000
          DataField = 'Cliente'
          DataSet = frxDsLotIts
          DataSetName = 'frxDsLotIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLotIts."Cliente"]')
          ParentFont = False
        end
      end
      object GH1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 21.779530000000000000
        Top = 291.023810000000000000
        Width = 793.701300000000000000
        Condition = 'frxDsLotIts."NOMECLIENTE"'
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 3.779530000000022000
          Width = 699.212598430000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_LA1NOME]')
          ParentFont = False
        end
      end
      object GH2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 21.779530000000000000
        Top = 336.378170000000000000
        Width = 793.701300000000000000
        Condition = 'frxDsLotIts."MES_TXT"'
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 64.252010000000000000
          Top = 3.779530000000022000
          Width = 672.755907950000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_LA2NOME]')
          ParentFont = False
        end
      end
      object GH3: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 21.779530000000000000
        Top = 381.732530000000000000
        Width = 793.701300000000000000
        Condition = 'frxDsLotIts."DDeposito_TXT"'
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149660000000000000
          Top = 3.779530000000022000
          Width = 653.858270160000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_LA3NOME]')
          ParentFont = False
        end
      end
      object GF1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 20.779530000000000000
        Top = 468.661720000000000000
        Width = 793.701300000000000000
        OnBeforePrint = 'GF1OnBeforePrint'
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149628260000000000
          Width = 548.031483860000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VFR_LA3NOME]: [SUM(<frxDsLotIts."CONTAGEM">)] cheque(s):')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 631.181112120000000000
          Width = 105.826788740000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLotIts."Credito">)]')
          ParentFont = False
        end
      end
      object GF2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 20.779530000000000000
        Top = 514.016080000000000000
        Width = 793.701300000000000000
        OnBeforePrint = 'GF2OnBeforePrint'
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 64.251992910000000000
          Width = 566.929119210000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VFR_LA2NOME]: [SUM(<frxDsLotIts."CONTAGEM">)] cheque(s):')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 631.181112120000000000
          Width = 105.826788740000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLotIts."Credito">)]')
          ParentFont = False
        end
      end
      object GF3: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 20.779530000000000000
        Top = 559.370440000000000000
        Width = 793.701300000000000000
        OnBeforePrint = 'GF3OnBeforePrint'
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Width = 593.322820000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VFR_LA1NOME]: [SUM(<frxDsLotIts."CONTAGEM">)] cheque(s):')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 631.181112120000000000
          Width = 105.826788740000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLotIts."Credito">)]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 36.000000000000000000
        Top = 699.213050000000000000
        Width = 793.701300000000000000
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 619.795300000000000000
          Top = 15.212120000000030000
          Width = 88.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[PAGE#] de ')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 707.795300000000000000
          Top = 15.212120000000030000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[TOTALPAGES]')
          ParentFont = False
        end
      end
      object SumarioDoRelatorio1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 32.000000000000000000
        Top = 642.520100000000000000
        Width = 793.701300000000000000
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 41.574820240000000000
          Top = 10.204389999999990000
          Width = 589.543290000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Total geral de [SUM(<frxDsLotIts."CONTAGEM">)] cheque(s):')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 631.181102360000000000
          Top = 10.204389999999990000
          Width = 105.826788740000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLotIts."Credito">)]')
          ParentFont = False
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Left = 40.456710000000000000
          Top = 6.204389999999990000
          Width = 696.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 2.000000000000000000
        end
      end
    end
  end
end
