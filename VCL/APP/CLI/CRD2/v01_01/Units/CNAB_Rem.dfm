object FmCNAB_Rem: TFmCNAB_Rem
  Left = 377
  Top = 168
  Caption = 'FBB-CNABC-006 :: Remessa CNAB A'
  ClientHeight = 479
  ClientWidth = 800
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 800
    Height = 317
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 792
    ExplicitHeight = 313
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 800
      Height = 48
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitLeft = 1
      ExplicitTop = 1
      ExplicitWidth = 790
      object Label23: TLabel
        Left = 4
        Top = 4
        Width = 110
        Height = 13
        Caption = 'Configura'#231#227'o de envio:'
        FocusControl = DBEdit1
      end
      object DBEdit1: TDBEdit
        Left = 4
        Top = 20
        Width = 41
        Height = 21
        DataField = 'Codigo'
        DataSource = DmBco.DsCNAB_Cfg
        TabOrder = 0
      end
      object DBEdit2: TDBEdit
        Left = 48
        Top = 20
        Width = 304
        Height = 21
        DataField = 'Nome'
        DataSource = DmBco.DsCNAB_Cfg
        TabOrder = 1
      end
      object DBRadioGroup1: TDBRadioGroup
        Left = 356
        Top = 1
        Width = 117
        Height = 46
        Caption = ' CNAB: '
        Columns = 2
        DataSource = DmBco.DsCNAB_Cfg
        Items.Strings = (
          '240'
          '400')
        ParentBackground = True
        TabOrder = 2
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 48
      Width = 800
      Height = 269
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 1
      ExplicitLeft = 1
      ExplicitTop = 205
      ExplicitWidth = 790
      ExplicitHeight = 260
      object TabSheet1: TTabSheet
        Caption = 'Arquivos'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 782
        ExplicitHeight = 383
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 792
          Height = 241
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          ExplicitWidth = 782
          ExplicitHeight = 383
          object Panel10: TPanel
            Left = 0
            Top = 192
            Width = 792
            Height = 49
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 0
            ExplicitWidth = 784
            object Label11: TLabel
              Left = 4
              Top = 4
              Width = 29
              Height = 13
              Caption = 'Linha:'
            end
            object Label12: TLabel
              Left = 44
              Top = 4
              Width = 36
              Height = 13
              Caption = 'Coluna:'
            end
            object Label13: TLabel
              Left = 84
              Top = 4
              Width = 27
              Height = 13
              Caption = 'Tam.:'
            end
            object Label14: TLabel
              Left = 124
              Top = 4
              Width = 87
              Height = 13
              Caption = 'Texto pesquisado:'
            end
            object Edit2: TEdit
              Left = 4
              Top = 20
              Width = 37
              Height = 21
              TabOrder = 0
              Text = '0'
              OnChange = Edit2Change
            end
            object Edit3: TEdit
              Left = 44
              Top = 20
              Width = 37
              Height = 21
              TabOrder = 1
              Text = '0'
              OnChange = Edit3Change
            end
            object Edit4: TEdit
              Left = 84
              Top = 20
              Width = 37
              Height = 21
              TabOrder = 2
              Text = '0'
              OnChange = Edit4Change
            end
            object Edit6: TEdit
              Left = 124
              Top = 20
              Width = 653
              Height = 21
              ReadOnly = True
              TabOrder = 3
            end
          end
          object PageControl2: TPageControl
            Left = 0
            Top = 0
            Width = 792
            Height = 192
            ActivePage = TabSheet7
            Align = alClient
            TabOrder = 1
            OnChange = PageControl2Change
            ExplicitWidth = 782
            ExplicitHeight = 334
            object TabSheet10: TTabSheet
              Caption = 'Gerado'
              object Panel11: TPanel
                Left = 0
                Top = 0
                Width = 784
                Height = 164
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Panel8: TPanel
                  Left = 0
                  Top = 0
                  Width = 784
                  Height = 29
                  Align = alTop
                  BevelOuter = bvNone
                  Caption = 'Panel8'
                  TabOrder = 0
                  ExplicitWidth = 776
                  object Label15: TLabel
                    Left = 8
                    Top = 8
                    Width = 39
                    Height = 13
                    Caption = 'Arquivo:'
                  end
                  object EdArqGerado: TEdit
                    Left = 52
                    Top = 4
                    Width = 717
                    Height = 21
                    ReadOnly = True
                    TabOrder = 0
                  end
                end
                object MeGerado: TMemo
                  Left = 0
                  Top = 29
                  Width = 784
                  Height = 114
                  Align = alClient
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Courier New'
                  Font.Style = []
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 1
                  WordWrap = False
                  OnEnter = MeGeradoEnter
                  OnExit = MeGeradoExit
                end
                object dmkMBGravado: TdmkMemoBar
                  Left = 0
                  Top = 143
                  Width = 784
                  Height = 21
                  Align = alBottom
                  BevelOuter = bvNone
                  TabOrder = 2
                  Memo = MeGerado
                  OverwriteCaretWidth = 12
                  OverwriteCaretHeight = 16
                end
              end
            end
            object TabSheet11: TTabSheet
              Caption = 'Arquivo a ser comparado'
              ImageIndex = 1
              object Panel7: TPanel
                Left = 0
                Top = 0
                Width = 784
                Height = 164
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Panel12: TPanel
                  Left = 0
                  Top = 0
                  Width = 784
                  Height = 29
                  Align = alTop
                  BevelOuter = bvNone
                  TabOrder = 0
                  ExplicitWidth = 776
                  object Label28: TLabel
                    Left = 8
                    Top = 8
                    Width = 39
                    Height = 13
                    Caption = 'Arquivo:'
                  end
                  object SpeedButton1: TSpeedButton
                    Left = 748
                    Top = 4
                    Width = 21
                    Height = 21
                    Caption = '...'
                    OnClick = SpeedButton1Click
                  end
                  object EdArqCompar: TEdit
                    Left = 52
                    Top = 4
                    Width = 693
                    Height = 21
                    ReadOnly = True
                    TabOrder = 0
                  end
                end
                object MeCompar: TMemo
                  Left = 0
                  Top = 29
                  Width = 784
                  Height = 114
                  Align = alClient
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Courier New'
                  Font.Style = []
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 1
                  WordWrap = False
                  OnEnter = MeComparEnter
                  OnExit = MeComparExit
                end
                object dmkMBCompar: TdmkMemoBar
                  Left = 0
                  Top = 143
                  Width = 784
                  Height = 21
                  Align = alBottom
                  BevelOuter = bvNone
                  TabOrder = 2
                  Memo = MeCompar
                  OverwriteCaretWidth = 12
                  OverwriteCaretHeight = 16
                end
              end
            end
            object TabSheet7: TTabSheet
              Caption = 'Compara'#231#227'o'
              ImageIndex = 2
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 776
              ExplicitHeight = 0
              object Panel13: TPanel
                Left = 0
                Top = 0
                Width = 784
                Height = 164
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                ExplicitWidth = 776
                object GradeG: TStringGrid
                  Left = 0
                  Top = 86
                  Width = 784
                  Height = 18
                  Align = alClient
                  ColCount = 240
                  DefaultColWidth = 7
                  DefaultRowHeight = 13
                  FixedCols = 0
                  RowCount = 2
                  FixedRows = 0
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Courier New'
                  Font.Style = []
                  Options = [goFixedVertLine, goFixedHorzLine, goEditing]
                  ParentFont = False
                  TabOrder = 0
                  OnDrawCell = GradeGDrawCell
                  OnSelectCell = GradeGSelectCell
                  ExplicitWidth = 776
                end
                object Panel17: TPanel
                  Left = 0
                  Top = 0
                  Width = 784
                  Height = 69
                  Align = alTop
                  BevelOuter = bvNone
                  TabOrder = 1
                  ExplicitWidth = 776
                  object Panel18: TPanel
                    Left = 0
                    Top = 0
                    Width = 101
                    Height = 69
                    Align = alLeft
                    BevelOuter = bvNone
                    TabOrder = 0
                    object Label22: TLabel
                      Left = 0
                      Top = 48
                      Width = 96
                      Height = 13
                      Caption = 'Endere'#231'o, tamanho:'
                    end
                    object BitBtn1: TBitBtn
                      Left = 0
                      Top = 1
                      Width = 90
                      Height = 40
                      Caption = '&Compara'
                      NumGlyphs = 2
                      TabOrder = 0
                      OnClick = BitBtn1Click
                    end
                  end
                  object Panel19: TPanel
                    Left = 101
                    Top = 0
                    Width = 683
                    Height = 69
                    Align = alClient
                    BevelOuter = bvNone
                    TabOrder = 1
                    ExplicitWidth = 675
                    object Label16: TLabel
                      Left = 0
                      Top = 4
                      Width = 29
                      Height = 13
                      Caption = 'Linha:'
                    end
                    object Label17: TLabel
                      Left = 0
                      Top = 26
                      Width = 36
                      Height = 13
                      Caption = 'Coluna:'
                    end
                    object Label18: TLabel
                      Left = 88
                      Top = 48
                      Width = 36
                      Height = 13
                      Caption = 'Campo:'
                    end
                    object Label19: TLabel
                      Left = 88
                      Top = 26
                      Width = 83
                      Height = 13
                      Caption = 'Valor comparado:'
                    end
                    object Label20: TLabel
                      Left = 88
                      Top = 4
                      Width = 63
                      Height = 13
                      Caption = 'Valor gerado:'
                    end
                    object dmkEdVerifLin: TdmkEdit
                      Left = 40
                      Top = 0
                      Width = 44
                      Height = 21
                      Alignment = taRightJustify
                      ReadOnly = True
                      TabOrder = 0
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 3
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '000'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                    end
                    object dmkEdVerifCol: TdmkEdit
                      Left = 40
                      Top = 22
                      Width = 44
                      Height = 21
                      Alignment = taRightJustify
                      ReadOnly = True
                      TabOrder = 1
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 3
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '000'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                    end
                    object dmkEdVerifValCompar: TdmkEdit
                      Left = 172
                      Top = 22
                      Width = 1920
                      Height = 21
                      ReadOnly = True
                      TabOrder = 2
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 3
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object dmkEdVerifCampoCod: TdmkEdit
                      Left = 128
                      Top = 44
                      Width = 44
                      Height = 21
                      Alignment = taRightJustify
                      ReadOnly = True
                      TabOrder = 3
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 3
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '-1000'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '000'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                    end
                    object dmkEdVerifValGerado: TdmkEdit
                      Left = 172
                      Top = 0
                      Width = 1920
                      Height = 21
                      ReadOnly = True
                      TabOrder = 4
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 3
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object dmkEdVerifCampoTxt: TdmkEdit
                      Left = 172
                      Top = 44
                      Width = 1920
                      Height = 21
                      ReadOnly = True
                      TabOrder = 5
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 3
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object dmkEdVerifPos: TdmkEdit
                      Left = 0
                      Top = 44
                      Width = 84
                      Height = 21
                      ReadOnly = True
                      TabOrder = 6
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 3
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '000 a 000 = 000'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = '000 a 000 = 000'
                      ValWarn = False
                    end
                  end
                end
                object GradeO: TStringGrid
                  Left = 5
                  Top = 136
                  Width = 108
                  Height = 65
                  ColCount = 240
                  DefaultColWidth = 7
                  DefaultRowHeight = 13
                  FixedCols = 0
                  RowCount = 2
                  FixedRows = 0
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Courier New'
                  Font.Style = []
                  Options = [goFixedVertLine, goFixedHorzLine, goEditing]
                  ParentFont = False
                  TabOrder = 2
                  Visible = False
                end
                object GradeC: TStringGrid
                  Left = 0
                  Top = 144
                  Width = 784
                  Height = 20
                  Align = alBottom
                  ColCount = 240
                  DefaultColWidth = 24
                  DefaultRowHeight = 13
                  FixedCols = 0
                  RowCount = 2
                  FixedRows = 0
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Courier New'
                  Font.Style = []
                  Options = [goFixedVertLine, goFixedHorzLine, goEditing]
                  ParentFont = False
                  ScrollBars = ssNone
                  TabOrder = 3
                  Visible = False
                  ExplicitWidth = 776
                end
                object GradeI: TStringGrid
                  Left = 0
                  Top = 104
                  Width = 784
                  Height = 20
                  Align = alBottom
                  ColCount = 240
                  DefaultColWidth = 24
                  DefaultRowHeight = 13
                  FixedCols = 0
                  RowCount = 2
                  FixedRows = 0
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Courier New'
                  Font.Style = []
                  Options = [goFixedVertLine, goFixedHorzLine, goEditing]
                  ParentFont = False
                  ScrollBars = ssNone
                  TabOrder = 4
                  Visible = False
                  ExplicitWidth = 776
                end
                object GradeF: TStringGrid
                  Left = 0
                  Top = 124
                  Width = 784
                  Height = 20
                  Align = alBottom
                  ColCount = 240
                  DefaultColWidth = 24
                  DefaultRowHeight = 13
                  FixedCols = 0
                  RowCount = 2
                  FixedRows = 0
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Courier New'
                  Font.Style = []
                  Options = [goFixedVertLine, goFixedHorzLine, goEditing]
                  ParentFont = False
                  ScrollBars = ssNone
                  TabOrder = 5
                  Visible = False
                  ExplicitWidth = 776
                end
                object PB1: TProgressBar
                  Left = 0
                  Top = 69
                  Width = 784
                  Height = 17
                  Align = alTop
                  TabOrder = 6
                  ExplicitWidth = 776
                end
              end
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' [0] Header '
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 784
        ExplicitHeight = 0
        object Panel4: TPanel
          Left = 0
          Top = 0
          Width = 792
          Height = 241
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          ExplicitWidth = 784
          object GroupBox1: TGroupBox
            Left = 0
            Top = 0
            Width = 792
            Height = 61
            Align = alTop
            Caption = ' Dados do registro'
            TabOrder = 0
            ExplicitWidth = 784
            object Label1: TLabel
              Left = 8
              Top = 16
              Width = 29
              Height = 13
              Caption = 'Linha:'
            end
            object Label2: TLabel
              Left = 200
              Top = 16
              Width = 42
              Height = 13
              Caption = 'Registro:'
            end
            object Label3: TLabel
              Left = 48
              Top = 16
              Width = 39
              Height = 13
              Caption = 'Servi'#231'o:'
            end
            object Label7: TLabel
              Left = 300
              Top = 16
              Width = 44
              Height = 13
              Caption = 'Empresa:'
            end
            object Label9: TLabel
              Left = 560
              Top = 16
              Width = 135
              Height = 13
              Caption = 'Ident. do cedente no banco:'
            end
            object Label24: TLabel
              Left = 704
              Top = 16
              Width = 58
              Height = 13
              Caption = 'Dt Gera'#231#227'o:'
            end
            object dmkEd_0_000: TdmkEdit
              Left = 8
              Top = 32
              Width = 37
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 4
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0001'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 1
              ValWarn = False
            end
            object dmkEd_0_005: TdmkEdit
              Left = 200
              Top = 32
              Width = 21
              Height = 21
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '1'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 1
              ValWarn = False
            end
            object dmkEd_0_006: TdmkEdit
              Left = 224
              Top = 32
              Width = 73
              Height = 21
              TabOrder = 4
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = 'REMESSA'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 'REMESSA'
              ValWarn = False
            end
            object dmkEd_0_003: TdmkEdit
              Left = 48
              Top = 32
              Width = 21
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 2
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '01'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 1
              ValWarn = False
            end
            object dmkEd_0_004: TdmkEdit
              Left = 72
              Top = 32
              Width = 125
              Height = 21
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = 'COBRANCA'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 'COBRANCA'
              ValWarn = False
            end
            object dmkEd_0_402: TdmkEdit
              Left = 300
              Top = 32
              Width = 257
              Height = 21
              TabOrder = 5
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object dmkEd_0_990: TdmkEdit
              Left = 704
              Top = 32
              Width = 68
              Height = 21
              TabOrder = 7
              FormatType = dmktfDate
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0d
              ValWarn = False
            end
            object dmkEd_0_410: TdmkEdit
              Left = 560
              Top = 32
              Width = 141
              Height = 21
              TabOrder = 6
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
          end
          object GroupBox2: TGroupBox
            Left = 0
            Top = 61
            Width = 792
            Height = 60
            Align = alTop
            Caption = ' Dados banc'#225'rios: '
            TabOrder = 1
            ExplicitWidth = 784
            object Label4: TLabel
              Left = 8
              Top = 16
              Width = 42
              Height = 13
              Caption = 'Ag'#234'ncia:'
            end
            object Label5: TLabel
              Left = 80
              Top = 16
              Width = 73
              Height = 13
              Caption = 'Conta corrente:'
            end
            object Label6: TLabel
              Left = 188
              Top = 16
              Width = 25
              Height = 13
              Caption = 'DAC:'
            end
            object Label8: TLabel
              Left = 228
              Top = 16
              Width = 95
              Height = 13
              Caption = 'Institui'#231#227'o banc'#225'ria:'
            end
            object Label21: TLabel
              Left = 636
              Top = 16
              Width = 116
              Height = 13
              Caption = 'C'#243'digo oculto do banco:'
            end
            object dmkEd_0_020: TdmkEdit
              Left = 8
              Top = 32
              Width = 37
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 4
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object dmkEd_0_021: TdmkEdit
              Left = 80
              Top = 32
              Width = 73
              Height = 21
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object dmkEd_0_022: TdmkEdit
              Left = 48
              Top = 32
              Width = 17
              Height = 21
              CharCase = ecUpperCase
              MaxLength = 1
              TabOrder = 1
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = True
              ValueVariant = ''
              ValWarn = False
            end
            object dmkEd_0_023: TdmkEdit
              Left = 156
              Top = 32
              Width = 17
              Height = 21
              CharCase = ecUpperCase
              MaxLength = 1
              TabOrder = 3
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = True
              ValueVariant = ''
              ValWarn = False
            end
            object dmkEd_0_024: TdmkEdit
              Left = 188
              Top = 32
              Width = 25
              Height = 21
              CharCase = ecUpperCase
              MaxLength = 1
              TabOrder = 4
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = True
              ValueVariant = ''
              ValWarn = False
            end
            object dmkEdNomeBanco: TdmkEdit
              Left = 260
              Top = 32
              Width = 373
              Height = 21
              TabOrder = 6
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object dmkEd_0_001: TdmkEdit
              Left = 228
              Top = 32
              Width = 29
              Height = 21
              Alignment = taRightJustify
              TabOrder = 5
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 3
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = dmkEd_0_001Change
            end
            object dmkEd_0_699: TdmkEdit
              Left = 636
              Top = 32
              Width = 137
              Height = 21
              TabOrder = 7
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
          end
        end
      end
      object TabSheet3: TTabSheet
        Caption = ' [1] Detalhe '
        ImageIndex = 2
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 782
        ExplicitHeight = 383
        object Panel9: TPanel
          Left = 0
          Top = 0
          Width = 792
          Height = 241
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          ExplicitWidth = 782
          ExplicitHeight = 383
          object Grade_1: TStringGrid
            Left = 0
            Top = 0
            Width = 792
            Height = 211
            Align = alClient
            ColCount = 2
            DefaultColWidth = 32
            DefaultRowHeight = 18
            RowCount = 2
            Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
            TabOrder = 0
            OnSelectCell = Grade_1SelectCell
            ExplicitWidth = 782
            ExplicitHeight = 353
          end
          object Panel5: TPanel
            Left = 0
            Top = 211
            Width = 792
            Height = 30
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 1
            ExplicitTop = 353
            ExplicitWidth = 782
            object Label10: TLabel
              Left = 8
              Top = 8
              Width = 183
              Height = 13
              Caption = 'C'#243'digo de item da coluna selecionada:'
            end
            object LaCodDmk_1: TLabel
              Left = 196
              Top = 8
              Width = 22
              Height = 13
              Caption = '000'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
          end
        end
      end
      object TabSheet4: TTabSheet
        Caption = '  [2] Mensagem '
        ImageIndex = 3
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 784
        ExplicitHeight = 0
        object Panel14: TPanel
          Left = 0
          Top = 0
          Width = 792
          Height = 241
          Align = alClient
          BevelOuter = bvNone
          Caption = 'Sem implementa'#231#227'o'
          TabOrder = 0
          ExplicitWidth = 784
        end
      end
      object TabSheet5: TTabSheet
        Caption = ' [3] Rateio de cr'#233'dito '
        ImageIndex = 4
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 784
        ExplicitHeight = 0
        object Panel15: TPanel
          Left = 0
          Top = 0
          Width = 792
          Height = 241
          Align = alClient
          BevelOuter = bvNone
          Caption = 'Sem implementa'#231#227'o'
          TabOrder = 0
          ExplicitWidth = 784
        end
      end
      object TabSheet6: TTabSheet
        Caption = ' [5] Detalhe '
        ImageIndex = 5
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 784
        ExplicitHeight = 0
        object Grade_5: TStringGrid
          Left = 0
          Top = 0
          Width = 792
          Height = 241
          Align = alClient
          ColCount = 2
          DefaultColWidth = 32
          DefaultRowHeight = 18
          RowCount = 2
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
          TabOrder = 0
          ExplicitWidth = 784
        end
      end
      object TabSheet9: TTabSheet
        Caption = ' [9] Trailer '
        ImageIndex = 8
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 784
        ExplicitHeight = 0
        object Panel16: TPanel
          Left = 0
          Top = 0
          Width = 792
          Height = 241
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          ExplicitWidth = 784
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 800
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 630
    object GB_R: TGroupBox
      Left = 752
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 582
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 704
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 534
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 223
        Height = 32
        Caption = 'Remessa CNAB A'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 223
        Height = 32
        Caption = 'Remessa CNAB A'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 223
        Height = 32
        Caption = 'Remessa CNAB A'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 365
    Width = 800
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitLeft = 68
    ExplicitTop = 573
    ExplicitWidth = 792
    object Panel20: TPanel
      Left = 2
      Top = 15
      Width = 796
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 626
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 409
    Width = 800
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 333
    ExplicitWidth = 630
    object PnSaiDesis: TPanel
      Left = 654
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 484
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel21: TPanel
      Left = 2
      Top = 15
      Width = 652
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 482
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Gera'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object OpenDialog1: TOpenDialog
    Left = 757
    Top = 115
  end
end
