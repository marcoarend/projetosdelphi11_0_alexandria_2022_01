object FmNFs: TFmNFs
  Left = 334
  Top = 209
  Caption = 'NFs-CADAS-001 :: NFs'
  ClientHeight = 426
  ClientWidth = 823
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 96
    Width = 823
    Height = 330
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBConfirma: TGroupBox
      Left = 0
      Top = 267
      Width = 823
      Height = 63
      Align = alBottom
      TabOrder = 0
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel3: TPanel
        Left = 713
        Top = 15
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 823
      Height = 217
      Align = alTop
      TabOrder = 1
      object Label9: TLabel
        Left = 16
        Top = 8
        Width = 63
        Height = 13
        Caption = 'N'#250'mero N.F.:'
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 24
        Width = 100
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 96
    Width = 823
    Height = 330
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object DBGrid1: TDBGrid
      Left = 0
      Top = 146
      Width = 823
      Height = 120
      Align = alBottom
      DataSource = DsLot
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'Lote'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Lote'
          Title.Caption = 'Border'#244
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECLIENTE'
          Title.Caption = 'Cliente'
          Width = 240
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Total'
          Title.Caption = 'Negociado'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Data'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MINAV'
          Title.Caption = '$ Ad Valorem'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ISS_Val'
          Title.Caption = '$ ISS'
          Width = 72
          Visible = True
        end>
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 266
      Width = 823
      Height = 64
      Align = alBottom
      TabOrder = 1
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 126
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel1: TPanel
        Left = 300
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 412
          Top = 0
          Width = 109
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BTNF: TBitBtn
          Left = 8
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&NF'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BTNFClick
        end
        object BtEuro: TBitBtn
          Tag = 120
          Left = 100
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Border'#244
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtEuroClick
        end
      end
    end
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 823
      Height = 57
      Align = alTop
      TabOrder = 2
      object Label1: TLabel
        Left = 16
        Top = 8
        Width = 63
        Height = 13
        Caption = 'N'#250'mero N.F.:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 124
        Top = 8
        Width = 66
        Height = 13
        Caption = '$ Ad Valorem:'
        FocusControl = DBEdit1
      end
      object Label3: TLabel
        Left = 208
        Top = 8
        Width = 29
        Height = 13
        Caption = '$ ISS:'
        FocusControl = DBEdit2
      end
      object DBEdCodigo: TDBEdit
        Left = 16
        Top = 24
        Width = 100
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsNFs
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdit1: TDBEdit
        Left = 124
        Top = 24
        Width = 80
        Height = 21
        DataField = 'AdValor'
        DataSource = DsNFs
        TabOrder = 1
      end
      object DBEdit2: TDBEdit
        Left = 208
        Top = 24
        Width = 80
        Height = 21
        DataField = 'ISS_Val'
        DataSource = DsNFs
        TabOrder = 2
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 823
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 819
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 823
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 775
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 559
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 50
        Height = 32
        Caption = 'NFs'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 50
        Height = 32
        Caption = 'NFs'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 50
        Height = 32
        Caption = 'NFs'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object DsNFs: TDataSource
    DataSet = QrNFs
    Left = 440
    Top = 8
  end
  object QrNFs: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrNFsBeforeOpen
    AfterOpen = QrNFsAfterOpen
    AfterScroll = QrNFsAfterScroll
    SQL.Strings = (
      'SELECT * FROM nfs'
      'WHERE Codigo > 0')
    Left = 412
    Top = 8
    object QrNFsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNFsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrNFsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrNFsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrNFsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrNFsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrNFsAdValor: TFloatField
      FieldName = 'AdValor'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFsISS_Val: TFloatField
      FieldName = 'ISS_Val'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = '*.txt'
    Filter = 'Arquivos Texto|*.txt'
    Left = 580
    Top = 8
  end
  object QrLot: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrLotCalcFields
    SQL.Strings = (
      'SELECT IF(en.Tipo=0, en.RazaoSocial,'
      'en.Nome) NOMECLIENTE,'
      'IF(en.Tipo=0, en.CNPJ,'
      'en.CPF) CNPJCPF, lo.*'
      'FROM lot es lo'
      'LEFT JOIN entidades en ON en.Codigo=lo.Cliente'
      'WHERE lo.NF=:P0'
      '')
    Left = 468
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLotA_PG_LIQ: TFloatField
      FieldKind = fkCalculated
      FieldName = 'A_PG_LIQ'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrLotVAL_LIQUIDO_MEU: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VAL_LIQUIDO_MEU'
      Calculated = True
    end
    object QrLotSUB_TOTAL_MEU: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SUB_TOTAL_MEU'
      Calculated = True
    end
    object QrLotTAXA_AM_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TAXA_AM_TXT'
      Size = 30
      Calculated = True
    end
    object QrLotADVAL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ADVAL_TXT'
      Size = 30
      Calculated = True
    end
    object QrLotCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLotTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrLotCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrLotLote: TSmallintField
      FieldName = 'Lote'
      DisplayFormat = '000'
    end
    object QrLotData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLotTotal: TFloatField
      FieldName = 'Total'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLotDias: TFloatField
      FieldName = 'Dias'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLotPeCompra: TFloatField
      FieldName = 'PeCompra'
      Required = True
    end
    object QrLotTxCompra: TFloatField
      FieldName = 'TxCompra'
    end
    object QrLotAdValorem: TFloatField
      FieldName = 'AdValorem'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrLotIOC: TFloatField
      FieldName = 'IOC'
    end
    object QrLotCPMF: TFloatField
      FieldName = 'CPMF'
    end
    object QrLotLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLotDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLotDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLotUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrLotUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrLotNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrLotTipoAdV: TIntegerField
      FieldName = 'TipoAdV'
      Required = True
    end
    object QrLotValValorem: TFloatField
      FieldName = 'ValValorem'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrLotSpread: TSmallintField
      FieldName = 'Spread'
      Required = True
    end
    object QrLotIOC_VAL: TFloatField
      FieldName = 'IOC_VAL'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLotCPMF_VAL: TFloatField
      FieldName = 'CPMF_VAL'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLotISS: TFloatField
      FieldName = 'ISS'
      Required = True
    end
    object QrLotISS_Val: TFloatField
      FieldName = 'ISS_Val'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrLotPIS_R: TFloatField
      FieldName = 'PIS_R'
      Required = True
    end
    object QrLotPIS_R_Val: TFloatField
      FieldName = 'PIS_R_Val'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrLotIRRF: TFloatField
      FieldName = 'IRRF'
      Required = True
    end
    object QrLotIRRF_Val: TFloatField
      FieldName = 'IRRF_Val'
      Required = True
    end
    object QrLotCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 18
    end
    object QrLotTarifas: TFloatField
      FieldName = 'Tarifas'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLotOcorP: TFloatField
      FieldName = 'OcorP'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLotCOFINS_R: TFloatField
      FieldName = 'COFINS_R'
      Required = True
      DisplayFormat = '#,###,##0.0000'
    end
    object QrLotCOFINS_R_Val: TFloatField
      FieldName = 'COFINS_R_Val'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrLotPIS: TFloatField
      FieldName = 'PIS'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLotCOFINS: TFloatField
      FieldName = 'COFINS'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLotPIS_Val: TFloatField
      FieldName = 'PIS_Val'
      Required = True
    end
    object QrLotCOFINS_Val: TFloatField
      FieldName = 'COFINS_Val'
      Required = True
    end
    object QrLotMaxVencto: TDateField
      FieldName = 'MaxVencto'
      Required = True
    end
    object QrLotCHDevPg: TFloatField
      FieldName = 'CHDevPg'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrLotMINTC: TFloatField
      FieldName = 'MINTC'
      Required = True
    end
    object QrLotMINAV: TFloatField
      FieldName = 'MINAV'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrLotMINTC_AM: TFloatField
      FieldName = 'MINTC_AM'
      Required = True
    end
    object QrLotPIS_T_Val: TFloatField
      FieldName = 'PIS_T_Val'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrLotCOFINS_T_Val: TFloatField
      FieldName = 'COFINS_T_Val'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrLotPgLiq: TFloatField
      FieldName = 'PgLiq'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrLotDUDevPg: TFloatField
      FieldName = 'DUDevPg'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrLotNF: TIntegerField
      FieldName = 'NF'
      Required = True
    end
    object QrLotItens: TIntegerField
      FieldName = 'Itens'
      Required = True
    end
    object QrLotConferido: TIntegerField
      FieldName = 'Conferido'
      Required = True
    end
    object QrLotECartaSac: TSmallintField
      FieldName = 'ECartaSac'
      Required = True
    end
    object QrLotCBE: TIntegerField
      FieldName = 'CBE'
      Required = True
    end
    object QrLotSCB: TIntegerField
      FieldName = 'SCB'
      Required = True
    end
    object QrLotAllQuit: TSmallintField
      FieldName = 'AllQuit'
      Required = True
    end
    object QrLotSobraIni: TFloatField
      FieldName = 'SobraIni'
      Required = True
    end
    object QrLotSobraNow: TFloatField
      FieldName = 'SobraNow'
      Required = True
    end
    object QrLotWebCod: TIntegerField
      FieldName = 'WebCod'
      Required = True
    end
    object QrLotAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrLotIOFd: TFloatField
      FieldName = 'IOFd'
      Required = True
    end
    object QrLotIOFd_VAL: TFloatField
      FieldName = 'IOFd_VAL'
      Required = True
    end
    object QrLotIOFv: TFloatField
      FieldName = 'IOFv'
      Required = True
    end
    object QrLotIOFv_VAL: TFloatField
      FieldName = 'IOFv_VAL'
      Required = True
    end
    object QrLotTipoIOF: TSmallintField
      FieldName = 'TipoIOF'
      Required = True
    end
    object QrLotAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object DsLot: TDataSource
    DataSet = QrLot
    Left = 496
    Top = 8
  end
  object QrSUM: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(MINAV) MINAV, SUM(ISS_Val) ISS_Val'
      'FROM lot es '
      'WHERE NF=:P0')
    Left = 552
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSUMMINAV: TFloatField
      FieldName = 'MINAV'
    end
    object QrSUMISS_Val: TFloatField
      FieldName = 'ISS_Val'
    end
  end
  object PMBordero: TPopupMenu
    Left = 424
    Top = 284
    object Adiona1: TMenuItem
      Caption = '&Adiciona'
      OnClick = Adiona1Click
    end
    object Retira1: TMenuItem
      Caption = '&Retira'
      OnClick = Retira1Click
    end
  end
  object QrMax: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Max(Codigo) Codigo'
      'FROM nfs')
    Left = 524
    Top = 8
    object QrMaxCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object frxNF_Dados: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39038.633847465300000000
    ReportOptions.LastChange = 39104.842247175900000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure Page1OnManualBuild(Sender: TfrxComponent);'
      'begin'
      'end;'
      ''
      'begin'
      '  ReportTitle1.Height := <AlturaInicial>;'
      '  //PageHeader1.Top := <AlturaInicial>;'
      'end.')
    OnGetValue = frxNF_TudoGetValue
    Left = 636
    Top = 8
    Datasets = <
      item
        DataSet = Dmod.frxDsControle
        DataSetName = 'frxDsControle'
      end
      item
      end
      item
        DataSet = Dmod.frxDsEndereco
        DataSetName = 'frxDsEndereco'
      end
      item
        DataSet = frxDsNF
        DataSetName = 'frxDsNF'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
      OnManualBuild = 'Page1OnManualBuild'
      object ReportTitle1: TfrxReportTitle
        Height = 7.559055120000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
      end
      object PageHeader1: TfrxPageHeader
        Height = 1058.268400000000000000
        Top = 49.133890000000000000
        Width = 793.701300000000000000
        object Memo14: TfrxMemoView
          Left = 593.385826770000000000
          Top = 83.149659999999900000
          Width = 128.504020000000000000
          Height = 22.677180000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNF."Data"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Left = 105.826840000000000000
          Top = 117.165430000000000000
          Width = 638.740570000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEndereco."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Left = 105.826840000000000000
          Top = 132.283550000000000000
          Width = 396.850650000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEndereco."E_MIN"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Left = 105.826840000000000000
          Top = 147.401670000000000000
          Width = 396.850650000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEndereco."CIDADE"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 105.826840000000000000
          Top = 162.519790000000000000
          Width = 396.850650000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEndereco."TE1_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 540.472790000000000000
          Top = 132.283550000000000000
          Width = 211.653680000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEndereco."BAIRRO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 540.472790000000000000
          Top = 147.401670000000000000
          Width = 211.653680000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEndereco."NOMEUF"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Left = 540.472790000000000000
          Top = 162.519790000000000000
          Width = 211.653680000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEndereco."ECEP_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Left = 105.826840000000000000
          Top = 177.637910000000000000
          Width = 287.244280000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEndereco."CNPJ_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          Left = 495.118430000000000000
          Top = 177.637910000000000000
          Width = 253.228510000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEndereco."IE_RG"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Left = 18.897637795275600000
          Top = 222.992270000000000000
          Width = 37.795263390000000000
          Height = 15.118107800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '01')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 94.488250000000000000
          Top = 222.992270000000000000
          Width = 491.338582677165000000
          Height = 15.118107800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VARF_NFLINHA1]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Left = 589.606299212598000000
          Top = 222.992270000000000000
          Width = 102.047273390000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNF."MINAV"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo127: TfrxMemoView
          Left = 94.488250000000000000
          Top = 238.110390000000000000
          Width = 491.338863390000000000
          Height = 15.118107800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VARF_NFLINHA2]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo128: TfrxMemoView
          Left = 94.488250000000000000
          Top = 253.228510000000000000
          Width = 491.338582677165000000
          Height = 15.118107800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VARF_NFLINHA3]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo130: TfrxMemoView
          Left = 7.559055118110240000
          Top = 294.803340000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNF."MINAV"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 94.488188976378000000
          Top = 294.803340000000000000
          Width = 68.031540000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNF."ISS_Val"]')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Left = 593.385826771654000000
          Top = 423.307360000000000000
          Width = 128.504020000000000000
          Height = 22.677180000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNF."Data"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          Left = 105.826840000000000000
          Top = 457.323130000000000000
          Width = 638.740570000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEndereco."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo63: TfrxMemoView
          Left = 105.826840000000000000
          Top = 472.441250000000000000
          Width = 396.850650000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEndereco."E_MIN"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo64: TfrxMemoView
          Left = 105.826840000000000000
          Top = 487.559370000000000000
          Width = 396.850650000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEndereco."CIDADE"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo65: TfrxMemoView
          Left = 105.826840000000000000
          Top = 502.677490000000000000
          Width = 396.850650000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEndereco."TE1_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo66: TfrxMemoView
          Left = 540.472790000000000000
          Top = 472.441250000000000000
          Width = 211.653680000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEndereco."BAIRRO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo67: TfrxMemoView
          Left = 540.472790000000000000
          Top = 487.559370000000000000
          Width = 211.653680000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEndereco."NOMEUF"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo68: TfrxMemoView
          Left = 540.472790000000000000
          Top = 502.677490000000000000
          Width = 211.653680000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEndereco."ECEP_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo69: TfrxMemoView
          Left = 105.826840000000000000
          Top = 517.795610000000000000
          Width = 287.244280000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEndereco."CNPJ_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo70: TfrxMemoView
          Left = 495.118430000000000000
          Top = 517.795610000000000000
          Width = 253.228510000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEndereco."IE_RG"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo76: TfrxMemoView
          Left = 18.897637795275600000
          Top = 563.149970000000000000
          Width = 37.795263390000000000
          Height = 15.118107800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '01')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo77: TfrxMemoView
          Left = 94.488250000000000000
          Top = 563.149970000000000000
          Width = 491.338582677165000000
          Height = 15.118107800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VARF_NFLINHA1]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo78: TfrxMemoView
          Left = 589.606299212598000000
          Top = 563.149970000000000000
          Width = 102.047273390000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNF."MINAV"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo79: TfrxMemoView
          Left = 94.488250000000000000
          Top = 578.268090000000000000
          Width = 491.338582677165000000
          Height = 15.118107800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VARF_NFLINHA2]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo80: TfrxMemoView
          Left = 94.488250000000000000
          Top = 593.386210000000000000
          Width = 491.338582677165000000
          Height = 15.118107800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VARF_NFLINHA3]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo82: TfrxMemoView
          Left = 7.559055118110240000
          Top = 634.961040000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNF."MINAV"]')
          ParentFont = False
        end
        object Memo84: TfrxMemoView
          Left = 94.488188976378000000
          Top = 634.961040000000000000
          Width = 68.031540000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNF."ISS_Val"]')
          ParentFont = False
        end
        object Memo97: TfrxMemoView
          Left = 593.385826771654000000
          Top = 763.465060000000000000
          Width = 128.504020000000000000
          Height = 22.677180000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNF."Data"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo107: TfrxMemoView
          Left = 105.826840000000000000
          Top = 797.480830000000000000
          Width = 638.740570000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEndereco."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo108: TfrxMemoView
          Left = 105.826840000000000000
          Top = 812.598950000000000000
          Width = 396.850650000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEndereco."E_MIN"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo109: TfrxMemoView
          Left = 105.826840000000000000
          Top = 827.717070000000000000
          Width = 396.850650000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEndereco."CIDADE"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo110: TfrxMemoView
          Left = 105.826840000000000000
          Top = 842.835190000000000000
          Width = 396.850650000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEndereco."TE1_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo111: TfrxMemoView
          Left = 540.472790000000000000
          Top = 812.598950000000000000
          Width = 211.653680000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEndereco."BAIRRO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo112: TfrxMemoView
          Left = 540.472790000000000000
          Top = 827.717070000000000000
          Width = 211.653680000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEndereco."NOMEUF"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo113: TfrxMemoView
          Left = 540.472790000000000000
          Top = 842.835190000000000000
          Width = 211.653680000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEndereco."ECEP_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo114: TfrxMemoView
          Left = 105.826840000000000000
          Top = 857.953310000000000000
          Width = 287.244280000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEndereco."CNPJ_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo115: TfrxMemoView
          Left = 495.118430000000000000
          Top = 857.953310000000000000
          Width = 253.228510000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEndereco."IE_RG"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo121: TfrxMemoView
          Left = 18.897637795275600000
          Top = 903.307670000000000000
          Width = 37.795263390000000000
          Height = 15.118107800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '01')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo122: TfrxMemoView
          Left = 94.488250000000000000
          Top = 903.307670000000000000
          Width = 491.338582677165000000
          Height = 15.118107800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VARF_NFLINHA1]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo123: TfrxMemoView
          Left = 589.606299212598000000
          Top = 903.307670000000000000
          Width = 102.047273390000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNF."MINAV"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo124: TfrxMemoView
          Left = 94.488250000000000000
          Top = 918.425790000000000000
          Width = 491.338582677165000000
          Height = 15.118107800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VARF_NFLINHA2]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo125: TfrxMemoView
          Left = 94.488250000000000000
          Top = 933.543910000000000000
          Width = 491.338582677165000000
          Height = 15.118107800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VARF_NFLINHA3]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo131: TfrxMemoView
          Left = 7.559055118110240000
          Top = 975.118740000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNF."MINAV"]')
          ParentFont = False
        end
        object Memo133: TfrxMemoView
          Left = 94.488188976378000000
          Top = 975.118740000000000000
          Width = 68.031540000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNF."ISS_Val"]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 211.653543310000000000
          Top = 45.354330710000000000
          Width = 377.953000000000000000
          Height = 41.574830000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_MEUENDERECO]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 211.653543310000000000
          Top = 385.511811020000000000
          Width = 377.953000000000000000
          Height = 41.574830000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_MEUENDERECO]')
          ParentFont = False
        end
      end
    end
  end
  object frxDsNF: TfrxDBDataset
    UserName = 'frxDsNF'
    CloseDataSource = False
    DataSet = QrLot
    BCDToCurrency = False
    Left = 664
    Top = 8
  end
  object PMNF: TPopupMenu
    OnPopup = PMNFPopup
    Left = 332
    Top = 280
    object IncluinovaNF1: TMenuItem
      Caption = '&Inclui nova NF'
      OnClick = IncluinovaNF1Click
    end
    object AlteraNFatual1: TMenuItem
      Caption = '&Altera NF atual'
      OnClick = AlteraNFatual1Click
    end
    object ExcluiNFatual1: TMenuItem
      Caption = '&Exclui NF atual'
      OnClick = ExcluiNFatual1Click
    end
  end
  object frxNF_Tudo: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39038.633847465300000000
    ReportOptions.LastChange = 39038.633847465300000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  if <LogoNFExiste> = True then'
      '  begin'
      '    Picture1.LoadFromFile(<LogoNFCaminho>);'
      '    Picture2.LoadFromFile(<LogoNFCaminho>);'
      '    Picture3.LoadFromFile(<LogoNFCaminho>);'
      '  end;'
      'end.')
    OnGetValue = frxNF_TudoGetValue
    Left = 584
    Top = 60
    Datasets = <
      item
        DataSet = Dmod.frxDsControle
        DataSetName = 'frxDsControle'
      end
      item
      end
      item
        DataSet = Dmod.frxDsEndereco
        DataSetName = 'frxDsEndereco'
      end
      item
        DataSet = frxDsNF
        DataSetName = 'frxDsNF'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      object Shape1: TfrxShapeView
        Left = 597.165740000000000000
        Top = 18.897650000000000000
        Width = 158.740260000000000000
        Height = 90.708720000000000000
        ShowHint = False
        Frame.Width = 0.500000000000000000
        Shape = skRoundRectangle
      end
      object Shape2: TfrxShapeView
        Left = 37.795300000000000000
        Top = 18.897650000000000000
        Width = 555.590910000000000000
        Height = 90.708720000000000000
        ShowHint = False
        Shape = skRoundRectangle
      end
      object Picture1: TfrxPictureView
        Left = 41.574830000000000000
        Top = 22.677180000000000000
        Width = 166.299212600000000000
        Height = 64.251968500000000000
        ShowHint = False
        DataField = 'Logo'
        HightQuality = False
        Transparent = False
        TransparentColor = clWhite
      end
      object Memo1: TfrxMemoView
        Left = 211.653543310000000000
        Top = 45.354330710000000000
        Width = 377.953000000000000000
        Height = 41.574830000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsDono."E_CUC"]')
        ParentFont = False
      end
      object Memo4: TfrxMemoView
        Left = 211.653680000000000000
        Top = 26.456710000000000000
        Width = 377.953000000000000000
        Height = 18.897650000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsDono."NOMEDONO"]')
        ParentFont = False
      end
      object Memo5: TfrxMemoView
        Left = 602.834645670000000000
        Top = 22.677180000000000000
        Width = 147.401670000000000000
        Height = 9.448818897637789000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = [ftBottom]
        HAlign = haCenter
        Memo.UTF8W = (
          'NOTA FISCAL DE PRESTA'#199#195'O DE SERVI'#199'O')
        ParentFont = False
      end
      object Memo6: TfrxMemoView
        Left = 602.834645670000000000
        Top = 34.015770000000000000
        Width = 147.401670000000000000
        Height = 11.338590000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Arial'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          'S'#201'RIE "F"')
        ParentFont = False
      end
      object Memo7: TfrxMemoView
        Left = 41.574830000000000000
        Top = 90.708720000000000000
        Width = 275.905690000000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = [ftTop]
        Memo.UTF8W = (
          'C.M.C [frxDsControle."CMC"]')
        ParentFont = False
      end
      object Memo8: TfrxMemoView
        Left = 317.480520000000000000
        Top = 90.708720000000000000
        Width = 272.126160000000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = [ftTop]
        HAlign = haRight
        Memo.UTF8W = (
          'CNPJ [frxDsDono."CNPJ_TXT"]')
        ParentFont = False
      end
      object Memo2: TfrxMemoView
        Left = 602.834645670000000000
        Top = 45.354360000000000000
        Width = 26.456710000000000000
        Height = 18.897650000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          'N'#186)
        ParentFont = False
      end
      object Memo9: TfrxMemoView
        Left = 629.291355670000000000
        Top = 45.354360000000000000
        Width = 120.944960000000000000
        Height = 18.897650000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsNF."NF"]')
        ParentFont = False
      end
      object Memo10: TfrxMemoView
        Left = 602.834645670000000000
        Top = 64.252010000000000000
        Width = 147.401670000000000000
        Height = 12.094490630000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '1'#170' Via - Destinat'#225'rio')
        ParentFont = False
      end
      object Memo13: TfrxMemoView
        Left = 597.165740000000000000
        Top = 79.370130000000000000
        Width = 158.740260000000000000
        Height = 10.204724410000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = [ftTop]
        HAlign = haCenter
        Memo.UTF8W = (
          'DATA EMISS'#195'O')
        ParentFont = False
      end
      object Memo14: TfrxMemoView
        Left = 612.283860000000000000
        Top = 83.149660000000000000
        Width = 128.504020000000000000
        Height = 22.677180000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsNF."Data"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Shape3: TfrxShapeView
        Left = 37.795300000000000000
        Top = 117.165430000000000000
        Width = 718.110700000000000000
        Height = 79.370130000000000000
        ShowHint = False
        Shape = skRoundRectangle
      end
      object Memo15: TfrxMemoView
        Left = 49.133890000000000000
        Top = 117.165430000000000000
        Width = 56.692913390000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          'Nome:')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo16: TfrxMemoView
        Left = 49.133890000000000000
        Top = 132.283550000000000000
        Width = 56.692913390000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          'Endere'#231'o:')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo17: TfrxMemoView
        Left = 49.133890000000000000
        Top = 147.401670000000000000
        Width = 56.692913390000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          'Munic'#237'pio:')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo18: TfrxMemoView
        Left = 49.133890000000000000
        Top = 162.519790000000000000
        Width = 56.692913390000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          'Fone/Fax:')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo19: TfrxMemoView
        Left = 502.677490000000000000
        Top = 132.283550000000000000
        Width = 37.795275590000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          'Bairro:')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo20: TfrxMemoView
        Left = 502.677490000000000000
        Top = 147.401670000000000000
        Width = 37.795275590000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          'UF:')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo21: TfrxMemoView
        Left = 502.677490000000000000
        Top = 162.519790000000000000
        Width = 37.795275590000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          'CEP:')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo22: TfrxMemoView
        Left = 49.133890000000000000
        Top = 177.637910000000000000
        Width = 56.692913390000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          'CNPJ:')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo23: TfrxMemoView
        Left = 400.630180000000000000
        Top = 177.637910000000000000
        Width = 94.488250000000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          'Inscr. Est.:')
        ParentFont = False
        VAlign = vaCenter
      end
      object Line1: TfrxLineView
        Left = 37.795300000000000000
        Top = 177.637910000000000000
        Width = 718.110700000000000000
        ShowHint = False
        Frame.Typ = [ftTop]
      end
      object Memo24: TfrxMemoView
        Left = 105.826840000000000000
        Top = 117.165430000000000000
        Width = 638.740570000000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[frxDsEndereco."NOMEDONO"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo25: TfrxMemoView
        Left = 105.826840000000000000
        Top = 132.283550000000000000
        Width = 396.850650000000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[frxDsEndereco."E_MIN"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo26: TfrxMemoView
        Left = 105.826840000000000000
        Top = 147.401670000000000000
        Width = 396.850650000000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[frxDsEndereco."CIDADE"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo27: TfrxMemoView
        Left = 105.826840000000000000
        Top = 162.519790000000000000
        Width = 396.850650000000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[frxDsEndereco."TE1_TXT"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo28: TfrxMemoView
        Left = 540.472790000000000000
        Top = 132.283550000000000000
        Width = 211.653680000000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[frxDsEndereco."BAIRRO"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo29: TfrxMemoView
        Left = 540.472790000000000000
        Top = 147.401670000000000000
        Width = 211.653680000000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[frxDsEndereco."NOMEUF"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo30: TfrxMemoView
        Left = 540.472790000000000000
        Top = 162.519790000000000000
        Width = 211.653680000000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[frxDsEndereco."ECEP_TXT"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Line2: TfrxLineView
        Left = 396.850650000000000000
        Top = 177.637910000000000000
        Height = 18.897650000000000000
        ShowHint = False
        Frame.Typ = [ftLeft]
      end
      object Memo31: TfrxMemoView
        Left = 105.826840000000000000
        Top = 177.637910000000000000
        Width = 287.244280000000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[frxDsEndereco."CNPJ_TXT"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo32: TfrxMemoView
        Left = 495.118430000000000000
        Top = 177.637910000000000000
        Width = 253.228510000000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[frxDsEndereco."IE_RG"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Shape4: TfrxShapeView
        Left = 37.795300000000000000
        Top = 207.874150000000000000
        Width = 718.110700000000000000
        Height = 105.826840000000000000
        ShowHint = False
        Shape = skRoundRectangle
      end
      object Memo33: TfrxMemoView
        Left = 37.795300000000000000
        Top = 196.535560000000000000
        Width = 147.401670000000000000
        Height = 11.338590000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          'PRESTA'#199#195'O DE SERVI'#199'O')
        ParentFont = False
        VAlign = vaBottom
      end
      object Memo34: TfrxMemoView
        Left = 37.795300000000000000
        Top = 207.874150000000000000
        Width = 52.913383390000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          'QUANT.')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo35: TfrxMemoView
        Left = 90.708720000000000000
        Top = 207.874150000000000000
        Width = 551.811343390000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          'DISCRIMINA'#199#195'O')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo36: TfrxMemoView
        Left = 642.520100000000000000
        Top = 207.874150000000000000
        Width = 113.385863390000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          'VALOR TOTAL')
        ParentFont = False
        VAlign = vaCenter
      end
      object Line3: TfrxLineView
        Left = 37.795300000000000000
        Top = 222.992270000000000000
        Width = 718.110700000000000000
        ShowHint = False
        Frame.Typ = [ftTop]
      end
      object Line4: TfrxLineView
        Left = 90.708720000000000000
        Top = 207.874150000000000000
        Height = 60.472480000000000000
        ShowHint = False
        Frame.Typ = [ftLeft]
      end
      object Line5: TfrxLineView
        Left = 642.520100000000000000
        Top = 207.874150000000000000
        Height = 60.472480000000000000
        ShowHint = False
        Frame.Typ = [ftLeft]
      end
      object Line6: TfrxLineView
        Left = 37.795300000000000000
        Top = 268.346630000000000000
        Width = 718.110700000000000000
        ShowHint = False
        Frame.Typ = [ftTop]
      end
      object Memo37: TfrxMemoView
        Left = 41.574830000000000000
        Top = 317.480520000000000000
        Width = 117.165393390000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          'N'#227'o vale como recibo*')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo39: TfrxMemoView
        Left = 45.354360000000000000
        Top = 222.992270000000000000
        Width = 37.795263390000000000
        Height = 15.118107800000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '01')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo40: TfrxMemoView
        Left = 94.488250000000000000
        Top = 222.992270000000000000
        Width = 544.252283390000000000
        Height = 15.118107800000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[VARF_NFLINHA1]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo41: TfrxMemoView
        Left = 646.299630000000000000
        Top = 222.992270000000000000
        Width = 102.047273390000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = '%2.2n'
        DisplayFormat.Kind = fkNumeric
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsNF."MINAV"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Line7: TfrxLineView
        Top = 340.157700000000000000
        Width = 797.480830000000000000
        ShowHint = False
        Frame.Style = fsDashDotDot
        Frame.Typ = [ftTop]
      end
      object Line14: TfrxLineView
        Top = 680.315400000000000000
        Width = 797.480830000000000000
        ShowHint = False
        Frame.Style = fsDashDotDot
        Frame.Typ = [ftTop]
      end
      object Line21: TfrxLineView
        Top = 1130.079470000000000000
        Width = 797.480830000000000000
        ShowHint = False
        Frame.Style = fsDashDotDot
        Frame.Typ = [ftTop]
      end
      object Memo127: TfrxMemoView
        Left = 94.488250000000000000
        Top = 238.110390000000000000
        Width = 544.252283390000000000
        Height = 15.118107800000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[VARF_NFLINHA2]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo128: TfrxMemoView
        Left = 94.488250000000000000
        Top = 253.228510000000000000
        Width = 544.252283390000000000
        Height = 15.118107800000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[VARF_NFLINHA3]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo129: TfrxMemoView
        Left = 41.574830000000000000
        Top = 272.125984251969000000
        Width = 83.149660000000000000
        Height = 18.897650000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          'Base de c'#225'lculo:')
        ParentFont = False
      end
      object Memo130: TfrxMemoView
        Left = 45.354360000000000000
        Top = 294.803340000000000000
        Width = 75.590600000000000000
        Height = 18.897650000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = '%2.2n'
        DisplayFormat.Kind = fkNumeric
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsNF."MINAV"]')
        ParentFont = False
      end
      object Line23: TfrxLineView
        Left = 128.504020000000000000
        Top = 268.346630000000000000
        Height = 45.354360000000000000
        ShowHint = False
        Frame.Typ = [ftLeft]
      end
      object Line22: TfrxLineView
        Left = 37.795300000000000000
        Top = 291.023810000000000000
        Width = 718.110700000000000000
        ShowHint = False
        Frame.Typ = [ftTop]
      end
      object Memo11: TfrxMemoView
        Left = 132.283550000000000000
        Top = 272.125984251969000000
        Width = 71.811070000000000000
        Height = 18.897650000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          'Valor do ISS:')
        ParentFont = False
      end
      object Memo12: TfrxMemoView
        Left = 132.283550000000000000
        Top = 294.803340000000000000
        Width = 68.031540000000000000
        Height = 18.897650000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = '%2.2n'
        DisplayFormat.Kind = fkNumeric
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsNF."ISS_Val"]')
        ParentFont = False
      end
      object Memo38: TfrxMemoView
        Left = 207.874150000000000000
        Top = 272.126160000000000000
        Width = 544.252320000000000000
        Height = 18.897650000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          '[IMPOSTO]')
        ParentFont = False
      end
      object Memo42: TfrxMemoView
        Left = 207.874150000000000000
        Top = 294.803340000000000000
        Width = 544.252320000000000000
        Height = 18.897650000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsControle."NFLei"]')
        ParentFont = False
      end
      object Line24: TfrxLineView
        Left = 204.094620000000000000
        Top = 268.346630000000000000
        Height = 45.354360000000000000
        ShowHint = False
        Frame.Typ = [ftLeft]
      end
      object Shape5: TfrxShapeView
        Left = 597.165740000000000000
        Top = 359.055350000000000000
        Width = 158.740260000000000000
        Height = 90.708720000000000000
        ShowHint = False
        Frame.Width = 0.500000000000000000
        Shape = skRoundRectangle
      end
      object Shape6: TfrxShapeView
        Left = 37.795300000000000000
        Top = 359.055350000000000000
        Width = 555.590910000000000000
        Height = 90.708720000000000000
        ShowHint = False
        Shape = skRoundRectangle
      end
      object Picture2: TfrxPictureView
        Left = 41.574830000000000000
        Top = 362.834880000000000000
        Width = 166.299212600000000000
        Height = 64.251968500000000000
        ShowHint = False
        DataField = 'Logo'
        HightQuality = False
        Transparent = False
        TransparentColor = clWhite
      end
      object Memo3: TfrxMemoView
        Left = 211.653680000000000000
        Top = 385.512060000000000000
        Width = 377.953000000000000000
        Height = 41.574830000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsDono."E_CUC"]')
        ParentFont = False
      end
      object Memo43: TfrxMemoView
        Left = 211.653680000000000000
        Top = 366.614410000000000000
        Width = 377.953000000000000000
        Height = 18.897650000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsDono."NOMEDONO"]')
        ParentFont = False
      end
      object Memo44: TfrxMemoView
        Left = 602.834645670000000000
        Top = 362.834880000000000000
        Width = 147.401670000000000000
        Height = 9.448818900000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = [ftBottom]
        HAlign = haCenter
        Memo.UTF8W = (
          'NOTA FISCAL DE PRESTA'#199#195'O DE SERVI'#199'O')
        ParentFont = False
      end
      object Memo45: TfrxMemoView
        Left = 602.834645670000000000
        Top = 374.173470000000000000
        Width = 147.401670000000000000
        Height = 11.338590000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Arial'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          'S'#201'RIE "F"')
        ParentFont = False
      end
      object Memo46: TfrxMemoView
        Left = 41.574830000000000000
        Top = 430.866420000000000000
        Width = 275.905690000000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = [ftTop]
        Memo.UTF8W = (
          'C.M.C [frxDsControle."CMC"]')
        ParentFont = False
      end
      object Memo47: TfrxMemoView
        Left = 317.480520000000000000
        Top = 430.866420000000000000
        Width = 272.126160000000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = [ftTop]
        HAlign = haRight
        Memo.UTF8W = (
          'CNPJ [frxDsDono."CNPJ_TXT"]')
        ParentFont = False
      end
      object Memo48: TfrxMemoView
        Left = 602.834645670000000000
        Top = 385.512060000000000000
        Width = 26.456710000000000000
        Height = 18.897650000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          'N'#186)
        ParentFont = False
      end
      object Memo49: TfrxMemoView
        Left = 629.291355670000000000
        Top = 385.512060000000000000
        Width = 120.944960000000000000
        Height = 18.897650000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsNF."NF"]')
        ParentFont = False
      end
      object Memo50: TfrxMemoView
        Left = 602.834645670000000000
        Top = 404.409710000000000000
        Width = 147.401670000000000000
        Height = 12.094490630000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '2'#170' Via - Contabilidade')
        ParentFont = False
      end
      object Memo51: TfrxMemoView
        Left = 597.165740000000000000
        Top = 419.527830000000000000
        Width = 158.740260000000000000
        Height = 10.204724410000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = [ftTop]
        HAlign = haCenter
        Memo.UTF8W = (
          'DATA EMISS'#195'O')
        ParentFont = False
      end
      object Memo52: TfrxMemoView
        Left = 612.283860000000000000
        Top = 423.307360000000000000
        Width = 128.504020000000000000
        Height = 22.677180000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsNF."Data"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Shape7: TfrxShapeView
        Left = 37.795300000000000000
        Top = 457.323130000000000000
        Width = 718.110700000000000000
        Height = 79.370130000000000000
        ShowHint = False
        Shape = skRoundRectangle
      end
      object Memo53: TfrxMemoView
        Left = 49.133890000000000000
        Top = 457.323130000000000000
        Width = 56.692913390000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          'Nome:')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo54: TfrxMemoView
        Left = 49.133890000000000000
        Top = 472.441250000000000000
        Width = 56.692913390000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          'Endere'#231'o:')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo55: TfrxMemoView
        Left = 49.133890000000000000
        Top = 487.559370000000000000
        Width = 56.692913390000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          'Municipio:')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo56: TfrxMemoView
        Left = 49.133890000000000000
        Top = 502.677490000000000000
        Width = 56.692913390000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          'Fone/Fax:')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo57: TfrxMemoView
        Left = 502.677490000000000000
        Top = 472.441250000000000000
        Width = 37.795275590000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          'Bairro:')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo58: TfrxMemoView
        Left = 502.677490000000000000
        Top = 487.559370000000000000
        Width = 37.795275590000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          'UF:')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo59: TfrxMemoView
        Left = 502.677490000000000000
        Top = 502.677490000000000000
        Width = 37.795275590000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          'CEP:')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo60: TfrxMemoView
        Left = 49.133890000000000000
        Top = 517.795610000000000000
        Width = 56.692913390000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          'CNPJ:')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo61: TfrxMemoView
        Left = 400.630180000000000000
        Top = 517.795610000000000000
        Width = 94.488250000000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          'Inscr. Est.:')
        ParentFont = False
        VAlign = vaCenter
      end
      object Line8: TfrxLineView
        Left = 37.795300000000000000
        Top = 517.795610000000000000
        Width = 718.110700000000000000
        ShowHint = False
        Frame.Typ = [ftTop]
      end
      object Memo62: TfrxMemoView
        Left = 105.826840000000000000
        Top = 457.323130000000000000
        Width = 638.740570000000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[frxDsEndereco."NOMEDONO"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo63: TfrxMemoView
        Left = 105.826840000000000000
        Top = 472.441250000000000000
        Width = 396.850650000000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[frxDsEndereco."E_MIN"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo64: TfrxMemoView
        Left = 105.826840000000000000
        Top = 487.559370000000000000
        Width = 396.850650000000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[frxDsEndereco."CIDADE"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo65: TfrxMemoView
        Left = 105.826840000000000000
        Top = 502.677490000000000000
        Width = 396.850650000000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[frxDsEndereco."TE1_TXT"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo66: TfrxMemoView
        Left = 540.472790000000000000
        Top = 472.441250000000000000
        Width = 211.653680000000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[frxDsEndereco."BAIRRO"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo67: TfrxMemoView
        Left = 540.472790000000000000
        Top = 487.559370000000000000
        Width = 211.653680000000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[frxDsEndereco."NOMEUF"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo68: TfrxMemoView
        Left = 540.472790000000000000
        Top = 502.677490000000000000
        Width = 211.653680000000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[frxDsEndereco."ECEP_TXT"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Line9: TfrxLineView
        Left = 396.850650000000000000
        Top = 517.795610000000000000
        Height = 18.897650000000000000
        ShowHint = False
        Frame.Typ = [ftLeft]
      end
      object Memo69: TfrxMemoView
        Left = 105.826840000000000000
        Top = 517.795610000000000000
        Width = 287.244280000000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[frxDsEndereco."CNPJ_TXT"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo70: TfrxMemoView
        Left = 495.118430000000000000
        Top = 517.795610000000000000
        Width = 253.228510000000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[frxDsEndereco."IE_RG"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Shape8: TfrxShapeView
        Left = 37.795300000000000000
        Top = 548.031850000000000000
        Width = 718.110700000000000000
        Height = 105.826840000000000000
        ShowHint = False
        Shape = skRoundRectangle
      end
      object Memo71: TfrxMemoView
        Left = 37.795300000000000000
        Top = 536.693260000000000000
        Width = 147.401670000000000000
        Height = 11.338590000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          'PRESTA'#199#195'O DE SERVI'#199'O')
        ParentFont = False
        VAlign = vaBottom
      end
      object Memo72: TfrxMemoView
        Left = 37.795300000000000000
        Top = 548.031850000000000000
        Width = 52.913383390000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          'QUANT.')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo73: TfrxMemoView
        Left = 90.708720000000000000
        Top = 548.031850000000000000
        Width = 551.811343390000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          'DISCRIMINA'#199#195'O')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo74: TfrxMemoView
        Left = 642.520100000000000000
        Top = 548.031850000000000000
        Width = 113.385863390000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          'VALOR TOTAL')
        ParentFont = False
        VAlign = vaCenter
      end
      object Line10: TfrxLineView
        Left = 37.795300000000000000
        Top = 563.149970000000000000
        Width = 718.110700000000000000
        ShowHint = False
        Frame.Typ = [ftTop]
      end
      object Line11: TfrxLineView
        Left = 90.708720000000000000
        Top = 548.031850000000000000
        Height = 60.472480000000000000
        ShowHint = False
        Frame.Typ = [ftLeft]
      end
      object Line12: TfrxLineView
        Left = 642.520100000000000000
        Top = 548.031850000000000000
        Height = 60.472480000000000000
        ShowHint = False
        Frame.Typ = [ftLeft]
      end
      object Line13: TfrxLineView
        Left = 37.795300000000000000
        Top = 608.504330000000000000
        Width = 718.110700000000000000
        ShowHint = False
        Frame.Typ = [ftTop]
      end
      object Memo75: TfrxMemoView
        Left = 41.574830000000000000
        Top = 657.638220000000000000
        Width = 117.165393390000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          'N'#227'o vale como recibo*')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo76: TfrxMemoView
        Left = 45.354360000000000000
        Top = 563.149970000000000000
        Width = 37.795263390000000000
        Height = 15.118107800000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '01')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo77: TfrxMemoView
        Left = 94.488250000000000000
        Top = 563.149970000000000000
        Width = 544.252283390000000000
        Height = 15.118107800000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[VARF_NFLINHA1]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo78: TfrxMemoView
        Left = 646.299630000000000000
        Top = 563.149970000000000000
        Width = 102.047273390000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = '%2.2n'
        DisplayFormat.Kind = fkNumeric
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsNF."MINAV"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo79: TfrxMemoView
        Left = 94.488250000000000000
        Top = 578.268090000000000000
        Width = 544.252283390000000000
        Height = 15.118107800000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[VARF_NFLINHA2]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo80: TfrxMemoView
        Left = 94.488250000000000000
        Top = 593.386210000000000000
        Width = 544.252283390000000000
        Height = 15.118107800000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[VARF_NFLINHA3]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo81: TfrxMemoView
        Left = 41.574830000000000000
        Top = 612.283464566929000000
        Width = 83.149660000000000000
        Height = 18.897650000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          'Base de c'#225'lculo:')
        ParentFont = False
      end
      object Memo82: TfrxMemoView
        Left = 45.354360000000000000
        Top = 634.961040000000000000
        Width = 75.590600000000000000
        Height = 18.897650000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = '%2.2n'
        DisplayFormat.Kind = fkNumeric
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsNF."MINAV"]')
        ParentFont = False
      end
      object Line15: TfrxLineView
        Left = 128.504020000000000000
        Top = 608.504330000000000000
        Height = 45.354360000000000000
        ShowHint = False
        Frame.Typ = [ftLeft]
      end
      object Line16: TfrxLineView
        Left = 37.795300000000000000
        Top = 631.181510000000000000
        Width = 718.110700000000000000
        ShowHint = False
        Frame.Typ = [ftTop]
      end
      object Memo83: TfrxMemoView
        Left = 132.283550000000000000
        Top = 612.283464566929000000
        Width = 71.811070000000000000
        Height = 18.897650000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          'Valor do ISS:')
        ParentFont = False
      end
      object Memo84: TfrxMemoView
        Left = 132.283550000000000000
        Top = 634.961040000000000000
        Width = 68.031540000000000000
        Height = 18.897650000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = '%2.2n'
        DisplayFormat.Kind = fkNumeric
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsNF."ISS_Val"]')
        ParentFont = False
      end
      object Memo85: TfrxMemoView
        Left = 207.874150000000000000
        Top = 612.283860000000000000
        Width = 544.252320000000000000
        Height = 18.897650000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          '[IMPOSTO]')
        ParentFont = False
      end
      object Memo86: TfrxMemoView
        Left = 207.874150000000000000
        Top = 634.961040000000000000
        Width = 544.252320000000000000
        Height = 18.897650000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsControle."NFLei"]')
        ParentFont = False
      end
      object Line17: TfrxLineView
        Left = 204.094620000000000000
        Top = 608.504330000000000000
        Height = 45.354360000000000000
        ShowHint = False
        Frame.Typ = [ftLeft]
      end
      object Shape9: TfrxShapeView
        Left = 597.165740000000000000
        Top = 699.213050000000000000
        Width = 158.740260000000000000
        Height = 90.708720000000000000
        ShowHint = False
        Frame.Width = 0.500000000000000000
        Shape = skRoundRectangle
      end
      object Shape10: TfrxShapeView
        Left = 37.795300000000000000
        Top = 699.213050000000000000
        Width = 555.590910000000000000
        Height = 90.708720000000000000
        ShowHint = False
        Shape = skRoundRectangle
      end
      object Picture3: TfrxPictureView
        Left = 41.574830000000000000
        Top = 702.992580000000000000
        Width = 166.299212600000000000
        Height = 64.251968500000000000
        ShowHint = False
        DataField = 'Logo'
        HightQuality = False
        Transparent = False
        TransparentColor = clWhite
      end
      object Memo87: TfrxMemoView
        Left = 211.653680000000000000
        Top = 725.669760000000000000
        Width = 377.953000000000000000
        Height = 41.574830000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsDono."E_CUC"]')
        ParentFont = False
      end
      object Memo88: TfrxMemoView
        Left = 211.653680000000000000
        Top = 706.772110000000000000
        Width = 377.953000000000000000
        Height = 18.897650000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsDono."NOMEDONO"]')
        ParentFont = False
      end
      object Memo89: TfrxMemoView
        Left = 602.834645670000000000
        Top = 702.992580000000000000
        Width = 147.401670000000000000
        Height = 9.448818900000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = [ftBottom]
        HAlign = haCenter
        Memo.UTF8W = (
          'NOTA FISCAL DE PRESTA'#199#195'O DE SERVI'#199'O')
        ParentFont = False
      end
      object Memo90: TfrxMemoView
        Left = 602.834645670000000000
        Top = 714.331170000000000000
        Width = 147.401670000000000000
        Height = 11.338590000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Arial'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          'S'#201'RIE "F"')
        ParentFont = False
      end
      object Memo91: TfrxMemoView
        Left = 41.574830000000000000
        Top = 771.024120000000000000
        Width = 275.905690000000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = [ftTop]
        Memo.UTF8W = (
          'C.M.C [frxDsControle."CMC"]')
        ParentFont = False
      end
      object Memo92: TfrxMemoView
        Left = 317.480520000000000000
        Top = 771.024120000000000000
        Width = 272.126160000000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = [ftTop]
        HAlign = haRight
        Memo.UTF8W = (
          'CNPJ [frxDsDono."CNPJ_TXT"]')
        ParentFont = False
      end
      object Memo93: TfrxMemoView
        Left = 602.834645670000000000
        Top = 725.669760000000000000
        Width = 26.456710000000000000
        Height = 18.897650000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          'N'#186)
        ParentFont = False
      end
      object Memo94: TfrxMemoView
        Left = 629.291355670000000000
        Top = 725.669760000000000000
        Width = 120.944960000000000000
        Height = 18.897650000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsNF."NF"]')
        ParentFont = False
      end
      object Memo95: TfrxMemoView
        Left = 602.834645670000000000
        Top = 744.567410000000000000
        Width = 147.401670000000000000
        Height = 12.094490630000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '3'#170' Via - Tal'#227'o')
        ParentFont = False
      end
      object Memo96: TfrxMemoView
        Left = 597.165740000000000000
        Top = 759.685530000000000000
        Width = 158.740260000000000000
        Height = 10.204724410000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = [ftTop]
        HAlign = haCenter
        Memo.UTF8W = (
          'DATA EMISS'#195'O')
        ParentFont = False
      end
      object Memo97: TfrxMemoView
        Left = 612.283860000000000000
        Top = 763.465060000000000000
        Width = 128.504020000000000000
        Height = 22.677180000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsNF."Data"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Shape11: TfrxShapeView
        Left = 37.795300000000000000
        Top = 797.480830000000000000
        Width = 718.110700000000000000
        Height = 79.370130000000000000
        ShowHint = False
        Shape = skRoundRectangle
      end
      object Memo98: TfrxMemoView
        Left = 49.133890000000000000
        Top = 797.480830000000000000
        Width = 56.692913390000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          'Nome:')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo99: TfrxMemoView
        Left = 49.133890000000000000
        Top = 812.598950000000000000
        Width = 56.692913390000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          'Endere'#231'o:')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo100: TfrxMemoView
        Left = 49.133890000000000000
        Top = 827.717070000000000000
        Width = 56.692913390000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          'Munic'#237'pio:')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo101: TfrxMemoView
        Left = 49.133890000000000000
        Top = 842.835190000000000000
        Width = 56.692913390000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          'Fone/Fax:')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo102: TfrxMemoView
        Left = 502.677490000000000000
        Top = 812.598950000000000000
        Width = 37.795275590000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          'Bairro:')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo103: TfrxMemoView
        Left = 502.677490000000000000
        Top = 827.717070000000000000
        Width = 37.795275590000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          'UF:')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo104: TfrxMemoView
        Left = 502.677490000000000000
        Top = 842.835190000000000000
        Width = 37.795275590000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          'CEP:')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo105: TfrxMemoView
        Left = 49.133890000000000000
        Top = 857.953310000000000000
        Width = 56.692913390000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          'CNPJ:')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo106: TfrxMemoView
        Left = 400.630180000000000000
        Top = 857.953310000000000000
        Width = 94.488250000000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          'Inscr. Est.:')
        ParentFont = False
        VAlign = vaCenter
      end
      object Line18: TfrxLineView
        Left = 37.795300000000000000
        Top = 857.953310000000000000
        Width = 718.110700000000000000
        ShowHint = False
        Frame.Typ = [ftTop]
      end
      object Memo107: TfrxMemoView
        Left = 105.826840000000000000
        Top = 797.480830000000000000
        Width = 638.740570000000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[frxDsEndereco."NOMEDONO"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo108: TfrxMemoView
        Left = 105.826840000000000000
        Top = 812.598950000000000000
        Width = 396.850650000000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[frxDsEndereco."E_MIN"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo109: TfrxMemoView
        Left = 105.826840000000000000
        Top = 827.717070000000000000
        Width = 396.850650000000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[frxDsEndereco."CIDADE"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo110: TfrxMemoView
        Left = 105.826840000000000000
        Top = 842.835190000000000000
        Width = 396.850650000000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[frxDsEndereco."TE1_TXT"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo111: TfrxMemoView
        Left = 540.472790000000000000
        Top = 812.598950000000000000
        Width = 211.653680000000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[frxDsEndereco."BAIRRO"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo112: TfrxMemoView
        Left = 540.472790000000000000
        Top = 827.717070000000000000
        Width = 211.653680000000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[frxDsEndereco."NOMEUF"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo113: TfrxMemoView
        Left = 540.472790000000000000
        Top = 842.835190000000000000
        Width = 211.653680000000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[frxDsEndereco."ECEP_TXT"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Line19: TfrxLineView
        Left = 396.850650000000000000
        Top = 857.953310000000000000
        Height = 18.897650000000000000
        ShowHint = False
        Frame.Typ = [ftLeft]
      end
      object Memo114: TfrxMemoView
        Left = 105.826840000000000000
        Top = 857.953310000000000000
        Width = 287.244280000000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[frxDsEndereco."CNPJ_TXT"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo115: TfrxMemoView
        Left = 495.118430000000000000
        Top = 857.953310000000000000
        Width = 253.228510000000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[frxDsEndereco."IE_RG"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Shape12: TfrxShapeView
        Left = 37.795300000000000000
        Top = 888.189550000000000000
        Width = 718.110700000000000000
        Height = 105.826840000000000000
        ShowHint = False
        Shape = skRoundRectangle
      end
      object Memo116: TfrxMemoView
        Left = 37.795300000000000000
        Top = 876.850960000000000000
        Width = 147.401670000000000000
        Height = 11.338590000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          'PRESTA'#199#195'O DE SERVI'#199'O')
        ParentFont = False
        VAlign = vaBottom
      end
      object Memo117: TfrxMemoView
        Left = 37.795300000000000000
        Top = 888.189550000000000000
        Width = 52.913383390000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          'QUANT.')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo118: TfrxMemoView
        Left = 90.708720000000000000
        Top = 888.189550000000000000
        Width = 551.811343390000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          'DISCRIMINA'#199#195'O')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo119: TfrxMemoView
        Left = 642.520100000000000000
        Top = 888.189550000000000000
        Width = 113.385863390000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          'VALOR TOTAL')
        ParentFont = False
        VAlign = vaCenter
      end
      object Line20: TfrxLineView
        Left = 37.795300000000000000
        Top = 903.307670000000000000
        Width = 718.110700000000000000
        ShowHint = False
        Frame.Typ = [ftTop]
      end
      object Line25: TfrxLineView
        Left = 90.708720000000000000
        Top = 888.189550000000000000
        Height = 60.472480000000000000
        ShowHint = False
        Frame.Typ = [ftLeft]
      end
      object Line26: TfrxLineView
        Left = 642.520100000000000000
        Top = 888.189550000000000000
        Height = 60.472480000000000000
        ShowHint = False
        Frame.Typ = [ftLeft]
      end
      object Line27: TfrxLineView
        Left = 37.795300000000000000
        Top = 948.662030000000000000
        Width = 718.110700000000000000
        ShowHint = False
        Frame.Typ = [ftTop]
      end
      object Memo120: TfrxMemoView
        Left = 41.574830000000000000
        Top = 997.795920000000000000
        Width = 117.165393390000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          'N'#227'o vale como recibo*')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo121: TfrxMemoView
        Left = 45.354360000000000000
        Top = 903.307670000000000000
        Width = 37.795263390000000000
        Height = 15.118107800000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '01')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo122: TfrxMemoView
        Left = 94.488250000000000000
        Top = 903.307670000000000000
        Width = 544.252283390000000000
        Height = 15.118107800000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[VARF_NFLINHA1]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo123: TfrxMemoView
        Left = 646.299630000000000000
        Top = 903.307670000000000000
        Width = 102.047273390000000000
        Height = 15.118110240000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = '%2.2n'
        DisplayFormat.Kind = fkNumeric
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsNF."MINAV"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo124: TfrxMemoView
        Left = 94.488250000000000000
        Top = 918.425790000000000000
        Width = 544.252283390000000000
        Height = 15.118107800000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[VARF_NFLINHA2]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo125: TfrxMemoView
        Left = 94.488250000000000000
        Top = 933.543910000000000000
        Width = 544.252283390000000000
        Height = 15.118107800000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[VARF_NFLINHA3]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo126: TfrxMemoView
        Left = 41.574830000000000000
        Top = 952.440944881890000000
        Width = 83.149660000000000000
        Height = 18.897650000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          'Base de c'#225'lculo:')
        ParentFont = False
      end
      object Memo131: TfrxMemoView
        Left = 45.354360000000000000
        Top = 975.118740000000000000
        Width = 75.590600000000000000
        Height = 18.897650000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = '%2.2n'
        DisplayFormat.Kind = fkNumeric
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsNF."MINAV"]')
        ParentFont = False
      end
      object Line28: TfrxLineView
        Left = 128.504020000000000000
        Top = 948.662030000000000000
        Height = 45.354360000000000000
        ShowHint = False
        Frame.Typ = [ftLeft]
      end
      object Line29: TfrxLineView
        Left = 37.795300000000000000
        Top = 971.339210000000000000
        Width = 718.110700000000000000
        ShowHint = False
        Frame.Typ = [ftTop]
      end
      object Memo132: TfrxMemoView
        Left = 132.283550000000000000
        Top = 952.440944881890000000
        Width = 71.811070000000000000
        Height = 18.897650000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          'Valor do ISS:')
        ParentFont = False
      end
      object Memo133: TfrxMemoView
        Left = 132.283550000000000000
        Top = 975.118740000000000000
        Width = 68.031540000000000000
        Height = 18.897650000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = '%2.2n'
        DisplayFormat.Kind = fkNumeric
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsNF."ISS_Val"]')
        ParentFont = False
      end
      object Memo134: TfrxMemoView
        Left = 207.874150000000000000
        Top = 952.441560000000000000
        Width = 544.252320000000000000
        Height = 18.897650000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          '[IMPOSTO]')
        ParentFont = False
      end
      object Memo135: TfrxMemoView
        Left = 207.874150000000000000
        Top = 975.118740000000000000
        Width = 544.252320000000000000
        Height = 18.897650000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsControle."NFLei"]')
        ParentFont = False
      end
      object Line30: TfrxLineView
        Left = 204.094620000000000000
        Top = 948.662030000000000000
        Height = 45.354360000000000000
        ShowHint = False
        Frame.Typ = [ftLeft]
      end
    end
  end
  object PMImprime: TPopupMenu
    Left = 8
    Top = 56
    object DadosApenaspreencherimpresso1: TMenuItem
      Caption = '&Dados (Apenas preencher impresso)'
      object Semomeuendereo1: TMenuItem
        Caption = '&Sem o meu endere'#231'o'
        OnClick = Semomeuendereo1Click
      end
      object Comomeuendereo1: TMenuItem
        Caption = '&Com o meu endere'#231'o'
        OnClick = Comomeuendereo1Click
      end
    end
    object udoFolhaembranco1: TMenuItem
      Caption = '&Tudo (Folha em branco)'
      OnClick = udoFolhaembranco1Click
    end
  end
end
