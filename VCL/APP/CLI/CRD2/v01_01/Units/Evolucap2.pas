unit Evolucap2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Db, mySQLDbTables, ComCtrls, Grids,
  DBGrids, DBCtrls, dmkGeral, dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkImage,
  DmkDAC_PF, UnDmkEnums;

type
  TFmEvolucap2 = class(TForm)
    QrEvolucap2: TmySQLQuery;
    DsEvolucap2: TDataSource;
    QrColigado: TmySQLQuery;
    QrColigadoCodigo: TIntegerField;
    QrColigadoNOMECOLIGADO: TWideStringField;
    DsColigado: TDataSource;
    QrCHDevolAbe: TmySQLQuery;
    QrCHDevolAbeValor: TFloatField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel1: TPanel;
    PnAtualiza: TPanel;
    PnEditSdoCC: TPanel;
    Label3: TLabel;
    BitBtn1: TBitBtn;
    EdSaldoConta: TdmkEdit;
    BitBtn2: TBitBtn;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    TPIniB: TDateTimePicker;
    TPFimB: TDateTimePicker;
    QrEvolper: TmySQLQuery;
    QrEvolperCARTEIREAL: TFloatField;
    QrEvolperVALOR_SUB_T: TFloatField;
    QrEvolperVALOR_TOTAL: TFloatField;
    QrMax: TmySQLQuery;
    QrMaxTOTAL: TFloatField;
    BtGrafico: TBitBtn;
    RGIntervalo: TRadioGroup;
    PnDados: TPanel;
    GroupBox1: TGroupBox;
    Label34: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    TPIniA: TDateTimePicker;
    TPFimA: TDateTimePicker;
    EdColigado: TdmkEditCB;
    CBColigado: TdmkDBLookupComboBox;
    BtOK: TBitBtn;
    ProgressBar1: TProgressBar;
    EdSaldoCaixa: TdmkEdit;
    Label6: TLabel;
    TabSheet3: TTabSheet;
    DBGrid2: TDBGrid;
    QrCHNull: TmySQLQuery;
    DsCHNull: TDataSource;
    QrCHNullCliente: TIntegerField;
    QrCHNullEmitente: TWideStringField;
    QrCHNullCPF: TWideStringField;
    QrCHNullBanco: TIntegerField;
    QrCHNullAgencia: TIntegerField;
    QrCHNullConta: TWideStringField;
    QrCHNullCheque: TIntegerField;
    QrCHNullValor: TFloatField;
    QrCHNullTaxas: TFloatField;
    QrCHNullMulta: TFloatField;
    QrCHNullJuros: TFloatField;
    QrCHNullDesconto: TFloatField;
    QrCHNullValPago: TFloatField;
    QrCHNullChequeOrigem: TIntegerField;
    QrCHNullLoteOrigem: TIntegerField;
    Panel2: TPanel;
    BtExclui1: TBitBtn;
    QrCHNullCodigo: TIntegerField;
    TabSheet4: TTabSheet;
    DBGrid3: TDBGrid;
    QrCHPrecoce: TmySQLQuery;
    DsCHPrecoce: TDataSource;
    Panel3: TPanel;
    QrDUVencido: TmySQLQuery;
    DBGrid1: TDBGrid;
    CkMoroso: TCheckBox;
    QrDUMoroso: TmySQLQuery;
    QrDUVencPag: TmySQLQuery;
    QrDUVencidoValor: TFloatField;
    QrDUMoroPag: TmySQLQuery;
    QrEmcart: TmySQLQuery;
    QrEmcartColigado: TLargeintField;
    QrEmcartTipo: TSmallintField;
    QrEmcartValor: TFloatField;
    QrDUVencidoColigado: TLargeintField;
    QrDUVencidoTipo: TSmallintField;
    QrDUVencPagColigado: TLargeintField;
    QrDUVencPagTipo: TSmallintField;
    QrDUMoroPagColigado: TLargeintField;
    QrDUMoroPagTipo: TSmallintField;
    QrDUMorosoColigado: TLargeintField;
    QrDUMorosoTipo: TSmallintField;
    QrDUMorosoValor: TFloatField;
    QrEvolucap2DataE: TDateField;
    QrEvolucap2CHAbeA: TFloatField;
    QrEvolucap2CHAbeB: TFloatField;
    QrEvolucap2CHAbeC: TFloatField;
    QrEvolucap2DUAbeA: TFloatField;
    QrEvolucap2DUAbeB: TFloatField;
    QrEvolucap2DUAbeC: TFloatField;
    QrEvolucap2CHDevA: TFloatField;
    QrEvolucap2CHDevB: TFloatField;
    QrEvolucap2CHDevC: TFloatField;
    QrEvolucap2DUDevA: TFloatField;
    QrEvolucap2DUDevB: TFloatField;
    QrEvolucap2DUDevC: TFloatField;
    QrEvolucap2CHPgdA: TFloatField;
    QrEvolucap2CHPgdB: TFloatField;
    QrEvolucap2CHPgdC: TFloatField;
    QrEvolucap2DUPgdA: TFloatField;
    QrEvolucap2DUPgdB: TFloatField;
    QrEvolucap2DUPgdC: TFloatField;
    QrEvolucap2CHMorA: TFloatField;
    QrEvolucap2CHMorB: TFloatField;
    QrEvolucap2CHMorC: TFloatField;
    QrEvolucap2DUMorA: TFloatField;
    QrEvolucap2DUMorB: TFloatField;
    QrEvolucap2DUMorC: TFloatField;
    QrEvolucap2CHPgmA: TFloatField;
    QrEvolucap2CHPgmB: TFloatField;
    QrEvolucap2CHPgmC: TFloatField;
    QrEvolucap2DUPgmA: TFloatField;
    QrEvolucap2DUPgmB: TFloatField;
    QrEvolucap2DUPgmC: TFloatField;
    QrCHVencido: TmySQLQuery;
    QrCHVencidoColigado: TLargeintField;
    QrCHVencidoTipo: TSmallintField;
    QrCHVencidoValor: TFloatField;
    QrCHVencPag: TmySQLQuery;
    QrCHVencPagColigado: TLargeintField;
    QrCHVencPagTipo: TSmallintField;
    QrCHVencPagValor: TFloatField;
    QrCHMoroso: TmySQLQuery;
    LargeintField1: TLargeintField;
    SmallintField1: TSmallintField;
    FloatField1: TFloatField;
    QrCHMoroPag: TmySQLQuery;
    LargeintField2: TLargeintField;
    SmallintField2: TSmallintField;
    FloatField2: TFloatField;
    QrDUVencPagValor: TFloatField;
    QrDUMoroPagValor: TFloatField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrEvolperDataE: TDateField;
    QrEvolperCHAbeA: TFloatField;
    QrEvolperCHAbeB: TFloatField;
    QrEvolperCHAbeC: TFloatField;
    QrEvolperDUAbeA: TFloatField;
    QrEvolperDUAbeB: TFloatField;
    QrEvolperDUAbeC: TFloatField;
    QrEvolperCHDevA: TFloatField;
    QrEvolperCHDevB: TFloatField;
    QrEvolperCHDevC: TFloatField;
    QrEvolperDUDevA: TFloatField;
    QrEvolperDUDevB: TFloatField;
    QrEvolperDUDevC: TFloatField;
    QrEvolperCHPgdA: TFloatField;
    QrEvolperCHPgdB: TFloatField;
    QrEvolperCHPgdC: TFloatField;
    QrEvolperDUPgdA: TFloatField;
    QrEvolperDUPgdB: TFloatField;
    QrEvolperDUPgdC: TFloatField;
    QrEvolperCHMorA: TFloatField;
    QrEvolperCHMorB: TFloatField;
    QrEvolperCHMorC: TFloatField;
    QrEvolperDUMorA: TFloatField;
    QrEvolperDUMorB: TFloatField;
    QrEvolperDUMorC: TFloatField;
    QrEvolperCHPgmA: TFloatField;
    QrEvolperCHPgmB: TFloatField;
    QrEvolperCHPgmC: TFloatField;
    QrEvolperDUPgmA: TFloatField;
    QrEvolperDUPgmB: TFloatField;
    QrEvolperDUPgmC: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtOKClick(Sender: TObject);
    procedure QrEvolucap2CalcFields(DataSet: TDataSet);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure QrEvolperCalcFields(DataSet: TDataSet);
    procedure BtGraficoClick(Sender: TObject);
    procedure QrCHNullAfterOpen(DataSet: TDataSet);
    procedure QrCHNullBeforeClose(DataSet: TDataSet);
    procedure BtExclui1Click(Sender: TObject);
  private
    { Private declarations }
    FRegistryPath: String;
    procedure EditaSaldoConta;
    procedure InsereItensAbe(
              var ValChA, ValChB, ValChC, ValDuA, ValDuB, ValDuC: Double;
              Data: String; Status: Integer; Texto: String);
    procedure InsereItensVen(Query: TmySQLQuery; var ValA, ValB, ValC: Double;
              Data: String; Status: Integer; Texto: String);
    procedure ReopenEvolucap2(Data: TDateTime);
    procedure ReopenCHNull();
  public
    { Public declarations }
  end;

  var
  FmEvolucap2: TFmEvolucap2;

implementation

{$R *.DFM}

uses UnMyObjects, Module, Principal, UMySQLModule, MyListas, UnInternalConsts;

procedure TFmEvolucap2.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEvolucap2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  {
  QrCHNull.Close;
  QrCHNull. Open;
  }
  ReopenCHNull();
  //
  if QrCHNull.RecordCount > 0 then
    PageControl1.ActivePageIndex := 2;

{
  QrCHPrecoce.Close;
  QrCHPrecoce. Open;
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrCHPrecoce, Dmod.MyDB, [
  'SELECT * ',
  'FROM alinits ai',
  'LEFT JOIN ' + CO_TabLctA + ' li ON li.FatParcela=ai.ChequeOrigem',
  'WHERE li.FatID=' + Geral.FF0(VAR_FATID_0301),
  'AND li.DDeposito >= SYSDATE()',
  'AND ai.Data3=0', 
  '']);
  //
  if QrCHPrecoce.RecordCount > 0 then
    PageControl1.ActivePageIndex := 3;
end;

procedure TFmEvolucap2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEvolucap2.FormCreate(Sender: TObject);
var
  Especial: Integer;
begin
  ImgTipo.SQLType := stLok;
  //
  TabSheet2.TabVisible := False;
  //
  // Atualizar LastEditLote
  PageControl1.ActivePageIndex := 0;
  UMyMod.AbreQuery(Dmod.QrControle, Dmod.MyDB);
  //
  FRegistryPath := Application.Title+'\Evolucap2';
  TPIniA.Date := Geral.ReadAppKey('DataIniA', FRegistryPath, ktDate, Int(Date), HKEY_LOCAL_MACHINE);
  TPFimA.Date := Geral.ReadAppKey('DataFimA', FRegistryPath, ktDate, Int(Date), HKEY_LOCAL_MACHINE);
  TPIniB.Date := Geral.ReadAppKey('DataIniB', FRegistryPath, ktDate, Int(Date-30), HKEY_LOCAL_MACHINE);
  TPFimB.Date := Geral.ReadAppKey('DataFimB', FRegistryPath, ktDate, Int(Date), HKEY_LOCAL_MACHINE);
  if Dmod.QrControleLastEditLote.Value > 0 then
    if TPIniA.Date > Dmod.QrControleLastEditLote.Value then
      TPIniA.Date := Dmod.QrControleLastEditLote.Value;
  Especial := Dmod.QrControleColigEsp.Value;
  if Especial <> 0 then
  begin
    EdColigado.Text := IntToStr(Especial);
    CBColigado.KeyValue := Especial;
  end;
  UMyMod.AbreQuery(QrColigado, Dmod.MyDB);
  ReopenEvolucap2(0);
  //
end;

procedure TFmEvolucap2.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Geral.WriteAppKey('DataIniA', FRegistryPath, Int(TPIniA.Date), ktDate, HKEY_LOCAL_MACHINE);
  Geral.WriteAppKey('DataFimA', FRegistryPath, Int(TPFimA.Date), ktDate, HKEY_LOCAL_MACHINE);
  Geral.WriteAppKey('DataIniB', FRegistryPath, Int(TPIniB.Date), ktDate, HKEY_LOCAL_MACHINE);
  Geral.WriteAppKey('DataFimB', FRegistryPath, Int(TPFimB.Date), ktDate, HKEY_LOCAL_MACHINE);
end;

procedure TFmEvolucap2.BtOKClick(Sender: TObject);
var
  i, a, b: Integer;
  d: String;
  CHAbeA, CHAbeB, CHAbeC, DUAbeA, DUAbeB, DUAbeC: Double;
  CHVenA, CHVenB, CHVenC, DUVenA, DUVenB, DUVenC: Double;
  CHPgdA, CHPgdB, CHPgdC, DUPgdA, DUPgdB, DUPgdC: Double;
  CHMorA, CHMorB, CHMorC, DUMorA, DUMorB, DUMorC: Double;
  CHPgmA, CHPgmB, CHPgmC, DUPgmA, DUPgmB, DUPgmC: Double;
begin
  CHAbeA := 0;
  CHAbeB := 0;
  CHAbeC := 0;
  DUAbeA := 0;
  DUAbeB := 0;
  DUAbeC := 0;
  CHVenA := 0;
  CHVenB := 0;
  CHVenC := 0;
  DUVenA := 0;
  DUVenB := 0;
  DUVenC := 0;
  CHPgdA := 0;
  CHPgdB := 0;
  CHPgdC := 0;
  DUPgdA := 0;
  DUPgdB := 0;
  DUPgdC := 0;
  CHMorA := 0;
  CHMorB := 0;
  CHMorC := 0;
  DUMorA := 0;
  DUMorB := 0;
  DUMorC := 0;
  CHPgmA := 0;
  CHPgmB := 0;
  CHPgmC := 0;
  DUPgmA := 0;
  DUPgmB := 0;
  DUPgmC := 0;
  //
  a := Trunc(TPIniA.Date);
  b := Trunc(TPFimA.Date);
  if b - a > 92 then
  begin
    if Geral.MensagemBox('Confirma a atualiza��o dos dados? '+
    'Per�odos grandes podem demorar alguns minutos!', 'Pergunta', MB_YESNOCANCEL+
    MB_ICONQUESTION) <> ID_YES then Exit;
  end;
  Screen.Cursor := crDefault;
  if b - a < 0 then
  begin
    Geral.MensagemBox('Per�odo inv�lido', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM evolucap2 WHERE DataE BETWEEN :P0 AND :P1');
  Dmod.QrUpd.Params[0].AsString := Geral.FDT(a, 1);
  Dmod.QrUpd.Params[1].AsString := Geral.FDT(b, 1);
  Dmod.QrUpd.ExecSQL;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM evolucap2its WHERE DataE BETWEEN :P0 AND :P1');
  Dmod.QrUpd.Params[0].AsString := Geral.FDT(a, 1);
  Dmod.QrUpd.Params[1].AsString := Geral.FDT(b, 1);
  Dmod.QrUpd.ExecSQL;
  //
  ProgressBar1.Position := 0;
  ProgressBar1.Max := b - a + 1;
  ProgressBar1.Visible := True;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO evolucap2 SET DataE=:P0, ');

  Dmod.QrUpd.SQL.Add('CHAbeA=:P01, CHAbeB=:P02, CHAbeC=:P03, DUAbeA=:P04, DUAbeB=:P05, DUAbeC=:P06, ');
  Dmod.QrUpd.SQL.Add('CHDevA=:P07, CHDevB=:P08, CHDevC=:P09, DUDevA=:P10, DUDevB=:P11, DUDevC=:P12, ');
  Dmod.QrUpd.SQL.Add('CHPgdA=:P13, CHPgdB=:P14, CHPgdC=:P15, DUPgdA=:P16, DUPgdB=:P17, DUPgdC=:P18, ');
  Dmod.QrUpd.SQL.Add('CHMorA=:P19, CHMorB=:P20, CHMorC=:P21, DUMorA=:P22, DUMorB=:P23, DUMorC=:P24, ');
  Dmod.QrUpd.SQL.Add('CHPgmA=:P25, CHPgmB=:P26, CHPgmC=:P27, DUPgmA=:P28, DUPgmB=:P29, DUPgmC=:P30  ');


  for i := a to b do
  begin
    ProgressBar1.Position := ProgressBar1.Position + 1;
    //
    d := FormatDateTime('yyyy/mm/dd', i);
    //
{
    QrEmcart.Close;
    QrEmcart.Params[0].AsString  := d;
    QrEmcart.Params[1].AsString  := d;
    QrEmcart. Open;
}
/////////////////////////

    UnDmkDAC_PF.AbreMySQLQuery0(QrEmCart, Dmod.MyDB, [
    'SELECT IF(li.Repassado = 0, 0, re.Coligado) Coligado,',
    'lo.Tipo, SUM(li.Credito) Valor',
    'FROM ' + CO_TabLctA + ' li',
    'LEFT JOIN ' + CO_TabLotA + ' lo ON lo.Codigo=li.FatNum',
    'LEFT JOIN alinits ai ON ai.ChequeOrigem=li.FatParcela',
    'LEFT JOIN repasits ri ON ri.Origem=li.FatParcela ',
    'LEFT JOIN repas    re ON re.Codigo=ri.Codigo',
    'WHERE li.FatID=' + Geral.FF0(VAR_FATID_0301),
    'AND li.DCompra <= "' + d + '"',
    'AND li.DDeposito >= "' + d + '"',
    'AND ai.ChequeOrigem IS NULL',
    'GROUP BY lo.Tipo, Coligado',
    '']);
    InsereItensAbe(ChAbeA, ChAbeB, ChAbeC, DUAbeA, DUAbeB, DuAbeC, d, 0, 'em carteira');
    //
{
    QrDUVencido.Close;
    QrDUVencido.Params[0].AsString  := d;
    QrDUVencido.Params[1].AsString  := d;
    QrDUVencido. Open;
}
    UnDmkDAC_PF.AbreMySQLQuery0(QrDUVencido, Dmod.MyDB, [
    'SELECT IF(li.Repassado = 0, 0, re.Coligado) Coligado,',
    'lo.Tipo, SUM(li.Credito) Valor',
    'FROM ' + CO_TabLctA + ' li',
    'LEFT JOIN ' + CO_TabLotA + ' lo ON lo.Codigo=li.FatNum',
    'LEFT JOIN repasits ri ON ri.Origem=li.FatParcela ',
    'LEFT JOIN repas    re ON re.Codigo=ri.Codigo',
    'WHERE li.FatID=' + Geral.FF0(VAR_FATID_0301),
    'AND lo.Tipo=1',
    'AND li.DDeposito < "' + d + '"',
    'AND ((li.Data3 > "' + d + '") OR (li.Data3 = 0))',
    'AND li.Quitado <> -3',
    'GROUP BY lo.Tipo, Coligado',
    '']);
    InsereItensVen(QrDUVencido, DUVenA, DUVenB, DUVenC, d, 1, 'duplicatas vencidas');
    //
{
    QrDUVencPag.Close;
    QrDUVencPag.Params[0].AsString  := d;
    QrDUVencPag.Params[1].AsString  := d;
    QrDUVencPag.Params[2].AsString  := d;
    QrDUVencPag. Open;
    UnDmkDAC_PF.AbreMySQLQuery0(QrDUVencPag, Dmod.MyDB, [
    'SELECT IF(li.Repassado = 0, 0, re.Coligado) Coligado,',
    'lo.Tipo, SUM(ad.Pago) Valor',
    'FROM adup pgs ad',
    'LEFT JOIN ' + CO_TabLctA + ' li ON li.FatParcela=ad.' + FLD_LOIS,
    'LEFT JOIN ' + CO_TabLotA + ' lo ON lo.Codigo=li.FatNum',
    'LEFT JOIN repasits ri ON ri.Origem=li.FatParcela ',
    'LEFT JOIN repas    re ON re.Codigo=ri.Codigo',
    'WHERE li.FatID=' + Geral.FF0(VAR_FATID_0301),
    'AND lo.Tipo=1',
    'AND li.DDeposito < "' + d + '"',
    'AND ((li.Data3 > "' + d + '") OR (li.Data3 = 0))',
    'AND li.Quitado <> -3',
    'AND ad.Data <= "' + d + '"',
    'GROUP BY lo.Tipo, Coligado',
    '']);
}
    UnDmkDAC_PF.AbreMySQLQuery0(QrDUVencPag, Dmod.MyDB, [
    'SELECT IF(li.Repassado = 0, 0, re.Coligado) Coligado, ',
    'lo.Tipo, SUM(ad.Credito) Valor ',
    'FROM ' + CO_TabLctA + ' ad ',
    'LEFT JOIN ' + CO_TabLctA + ' li ON li.FatParcela=ad.FatParcRef ',
    'LEFT JOIN ' + CO_TabLotA + ' lo ON lo.Codigo=li.FatNum ',
    'LEFT JOIN repasits ri ON ri.Origem=li.FatParcela ',
    'LEFT JOIN repas    re ON re.Codigo=ri.Codigo ',
    'WHERE li.FatID=' + TXT_VAR_FATID_0301,
    //'AND ad.FatID=' + TXT_VAR_FATID_0305,
    'AND ad.FatID=' + TXT_VAR_FATID_0311,
    'AND lo.Tipo=1 ',
    'AND li.DDeposito < "' + d + '"',
    'AND ((li.Data3 > "' + d + '") OR (li.Data3 = 0))',
    'AND li.Quitado <> -3',
    'AND ad.Data <= "' + d + '"',
    'GROUP BY lo.Tipo, Coligado',
    '']);
    //
    InsereItensVen(QrDUVencPag, DUPgdA, DUPgdB, DUPgdC, d, 2, 'pagamento de duplicatas vencidas');
    //
{
    QrDUMoroso.Close;
    QrDUMoroso.Params[0].AsString  := d;
    QrDUMoroso.Params[1].AsString  := d;
    QrDUMoroso. Open;
}

    UnDmkDAC_PF.AbreMySQLQuery0(QrDUMoroso, Dmod.MyDB, [
    'SELECT IF(li.Repassado = 0, 0, re.Coligado) Coligado,',
    'lo.Tipo, SUM(li.Credito) Valor',
    'FROM ' + CO_TabLctA + ' li',
    'LEFT JOIN ' + CO_TabLotA + ' lo ON lo.Codigo=li.FatNum',
    'LEFT JOIN repasits ri ON ri.Origem=li.FatParcela ',
    'LEFT JOIN repas    re ON re.Codigo=ri.Codigo',
    'WHERE li.FatID=' + Geral.FF0(VAR_FATID_0301),
    'AND lo.Tipo=1',
    'AND li.DDeposito < "' + d + '"',
    'AND ((li.Data3 > "' + d + '") OR (li.Data3 = 0))',
    'AND li.Quitado = -3',
    'GROUP BY lo.Tipo, Coligado',
    '']);
    InsereItensVen(QrDUMoroso, DUMorA, DUMorB, DUMorC, d, 3, 'duplicatas morosas');
    //
{
    QrDUMoroPag.Close;
    QrDUMoroPag.Params[0].AsString  := d;
    QrDUMoroPag.Params[1].AsString  := d;
    QrDUMoroPag.Params[2].AsString  := d;
    QrDUMoroPag. Open;
    UnDmkDAC_PF.AbreMySQLQuery0(QrDUMoroPag, Dmod.MyDB, [
    'SELECT IF(li.Repassado = 0, 0, re.Coligado) Coligado,',
    'lo.Tipo, SUM(ad.Pago) Valor',
    'FROM adup pgs ad',
    'LEFT JOIN ' + CO_TabLctA + ' li ON li.FatParcela=ad.' + FLD_LOIS,
    'LEFT JOIN ' + CO_TabLotA + ' lo ON lo.Codigo=li.FatNum',
    'LEFT JOIN repasits ri ON ri.Origem=li.FatParcela ',
    'LEFT JOIN repas    re ON re.Codigo=ri.Codigo',
    'WHERE li.FatID=' + Geral.FF0(VAR_FATID_0301),
    'AND lo.Tipo=1',
    'AND li.DDeposito < "' + d + '"',
    'AND ((li.Data3 > "' + d + '") OR (li.Data3 = 0))',
    'AND li.Quitado = -3',
    'AND ad.Data <= "' + d + '"',
    'GROUP BY lo.Tipo, Coligado',
    '']);
}
    UnDmkDAC_PF.AbreMySQLQuery0(QrDUMoroPag, Dmod.MyDB, [
    'SELECT IF(li.Repassado = 0, 0, re.Coligado) Coligado,',
    'lo.Tipo, SUM(ad.Credito) Valor',
    'FROM ' + CO_TabLctA + ' ad',
    'LEFT JOIN ' + CO_TabLctA + ' li ON li.FatParcela=ad.FatParcRef',
    'LEFT JOIN ' + CO_TabLotA + ' lo ON lo.Codigo=li.FatNum',
    'LEFT JOIN repasits ri ON ri.Origem=li.FatParcela ',
    'LEFT JOIN repas    re ON re.Codigo=ri.Codigo',
    'WHERE li.FatID=' + Geral.FF0(VAR_FATID_0301),
    'AND lo.Tipo=1',
    'AND li.DDeposito < "' + d + '"',
    'AND ((li.Data3 > "' + d + '") OR (li.Data3 = 0))',
    'AND li.Quitado = -3',
    'AND ad.Data <= "' + d + '"',
    'GROUP BY lo.Tipo, Coligado',
    '']);
    InsereItensVen(QrDUMoroPag, DUPgmA, DUPgmB, DUPgmC, d, 4, 'pagamento de duplicatas morosas');
    //
{
    QrCHVencido.Close;
    QrCHVencido.Params[0].AsString  := d;
    QrCHVencido.Params[1].AsString  := d;
    QrCHVencido.Params[2].AsString  := d;
    QrCHVencido. Open;
}
    UnDmkDAC_PF.AbreMySQLQuery0(QrCHVencido, Dmod.MyDB, [
      'SELECT IF(li.Repassado = 0, 0, re.Coligado) Coligado,',
      'lo.Tipo, SUM(li.Credito) Valor',
      'FROM ' + CO_TabLctA + ' li',
      'LEFT JOIN ' + CO_TabLotA + ' lo ON lo.Codigo=li.FatNum',
      'LEFT JOIN repasits  ri ON ri.Origem=li.FatParcela',
      'LEFT JOIN repas     re ON re.Codigo=ri.Codigo',
      'LEFT JOIN alinits   ai ON ai.ChequeOrigem=li.FatParcela',
      'WHERE li.FatID=' + Geral.FF0(VAR_FATID_0301),
      'AND lo.Tipo=0',
      'AND li.DCompra < "' + d + '"',
      'AND li.DDeposito < "' + d + '"',
      'AND ai.ChequeOrigem <> 0',
      'AND ( (ai.Data3 = 0) OR (ai.Data3 > "' + d + '") )',
      'AND li.Quitado<>-3',
      'GROUP BY lo.Tipo, Coligado',
      '']);
    InsereItensVen(QrCHVencido, CHVenA, CHVenB, CHVenC, d, 5, 'cheques vencidos');
    //
{
    QrCHVencPag.Close;
    QrCHVencPag.Params[0].AsString  := d;
    QrCHVencPag.Params[1].AsString  := d;
    QrCHVencPag.Params[2].AsString  := d;
    QrCHVencPag. Open;
}
    UnDmkDAC_PF.AbreMySQLQuery0(QrCHVencPag, Dmod.MyDB, [
      'SELECT IF(li.Repassado = 0, 0, re.Coligado) Coligado,',
      'lo.Tipo, SUM(ap.Credito) Valor',
      'FROM ' + CO_TabLctA + ' li',
      'LEFT JOIN ' + CO_TabLotA + ' lo ON lo.Codigo=li.FatNum',
      'LEFT JOIN repasits  ri ON ri.Origem=li.FatParcela',
      'LEFT JOIN repas     re ON re.Codigo=ri.Codigo',
      'LEFT JOIN alinits   ai ON ai.ChequeOrigem=li.FatParcela',
      'LEFT JOIN ' + CO_TabLctA + ' ap ON ap.FatParcRef=ai.Codigo',
      'WHERE li.FatID=' + Geral.FF0(VAR_FATID_0301),
      //'AND ap.FatID=' + TXT_VAR_FATID_0305,
      'AND ap.FatID=' + TXT_VAR_FATID_0311,
      'AND lo.Tipo=0',
      'AND li.DCompra < "' + d + '"',
      'AND li.DDeposito < "' + d + '"',
      'AND ai.ChequeOrigem <> 0',
      'AND ( (ai.Data3 = 0) OR (ai.Data3 > "' + d + '") )',
      'AND ap.Credito <> 0',
      'AND li.Quitado<>-3',
      'GROUP BY lo.Tipo, Coligado',
      '']);
    InsereItensVen(QrCHVencPag, CHPgdA, CHPgdB, CHPgdC, d, 6, 'pagamento de cheques vencidos');
    //
{
    QrCHMoroso.Close;
    QrCHMoroso.Params[0].AsString  := d;
    QrCHMoroso.Params[1].AsString  := d;
    QrCHMoroso.Params[2].AsString  := d;
    QrCHMoroso. Open;
}
    UnDmkDAC_PF.AbreMySQLQuery0(QrCHMoroso, Dmod.MyDB, [
    'SELECT IF(li.Repassado = 0, 0, re.Coligado) Coligado,',
    'lo.Tipo, SUM(li.Credito) Valor',
    'FROM ' + CO_TabLctA + ' li',
    'LEFT JOIN ' + CO_TabLotA + ' lo ON lo.Codigo=li.FatNum',
    'LEFT JOIN repasits  ri ON ri.Origem=li.FatParcela',
    'LEFT JOIN repas     re ON re.Codigo=ri.Codigo',
    'LEFT JOIN alinits   ai ON ai.ChequeOrigem=li.FatParcela',
    'WHERE li.FatID=' + Geral.FF0(VAR_FATID_0301),
    'AND lo.Tipo=0',
    'AND li.DCompra < "' + d + '"',
    'AND li.DDeposito < "' + d + '"',
    'AND ai.ChequeOrigem <> 0',
    'AND ( (ai.Data3 = 0) OR (ai.Data3 > "' + d + '") )',
    'AND li.Quitado=-3',
    'GROUP BY lo.Tipo, Coligado',
    '']);
    InsereItensVen(QrCHMoroso, CHMorA, CHMorB, CHMorC, d, 7, 'cheques morosos');
    //
{
    QrCHMoroPag.Close;
    QrCHMoroPag.Params[0].AsString  := d;
    QrCHMoroPag.Params[1].AsString  := d;
    QrCHMoroPag.Params[2].AsString  := d;
    QrCHMoroPag. Open;
}
    UnDmkDAC_PF.AbreMySQLQuery0(QrCHMoroPag, Dmod.MyDB, [
      'SELECT IF(li.Repassado = 0, 0, re.Coligado) Coligado,',
      'lo.Tipo, SUM(ap.Credito) Valor',
      'FROM ' + CO_TabLctA + ' li',
      'LEFT JOIN ' + CO_TabLotA + ' lo ON lo.Codigo=li.FatNum',
      'LEFT JOIN repasits  ri ON ri.Origem=li.FatParcela',
      'LEFT JOIN repas     re ON re.Codigo=ri.Codigo',
      'LEFT JOIN alinits   ai ON ai.ChequeOrigem=li.FatParcela',
      'LEFT JOIN ' + CO_TabLctA + ' ap ON ap.FatParcRef=ai.Codigo',
      'WHERE li.FatID=' + Geral.FF0(VAR_FATID_0301),
      //'AND ap.FatID=' + TXT_VAR_FATID_0305,
      'AND ap.FatID=' + TXT_VAR_FATID_0311,
      'AND lo.Tipo=0',
      'AND li.DCompra < "' + d + '"',
      'AND li.DDeposito < "' + d + '"',
      'AND ai.ChequeOrigem <> 0',
      'AND ( (ai.Data3 = 0) OR (ai.Data3 > "' + d + '") )',
      'AND ap.Credito <> 0',
      'AND li.Quitado=-3',
      'GROUP BY lo.Tipo, Coligado']);
    InsereItensVen(QrCHMoroPag, CHPgmA, CHPgmB, CHPgmC, d, 8, 'pagamento de cheques morosos');
    //
    //
    // Inclus�o do registro
    //
    Dmod.QrUpd.Params[00].AsString := d;
    Dmod.QrUpd.Params[01].AsFloat  := CHAbeA;
    Dmod.QrUpd.Params[02].AsFloat  := CHAbeB;
    Dmod.QrUpd.Params[03].AsFloat  := CHAbeC;
    Dmod.QrUpd.Params[04].AsFloat  := DUAbeA;
    Dmod.QrUpd.Params[05].AsFloat  := DUAbeB;
    Dmod.QrUpd.Params[06].AsFloat  := DUAbeC;
    Dmod.QrUpd.Params[07].AsFloat  := CHVenA;
    Dmod.QrUpd.Params[08].AsFloat  := CHVenB;
    Dmod.QrUpd.Params[09].AsFloat  := CHVenC;
    Dmod.QrUpd.Params[10].AsFloat  := DUVenA;
    Dmod.QrUpd.Params[11].AsFloat  := DUVenB;
    Dmod.QrUpd.Params[12].AsFloat  := DUVenC;
    Dmod.QrUpd.Params[13].AsFloat  := CHPgdA;
    Dmod.QrUpd.Params[14].AsFloat  := CHPgdB;
    Dmod.QrUpd.Params[15].AsFloat  := CHPgdC;
    Dmod.QrUpd.Params[16].AsFloat  := DUPgdA;
    Dmod.QrUpd.Params[17].AsFloat  := DUPgdB;
    Dmod.QrUpd.Params[18].AsFloat  := DUPgdC;
    Dmod.QrUpd.Params[19].AsFloat  := CHMorA;
    Dmod.QrUpd.Params[20].AsFloat  := CHMorB;
    Dmod.QrUpd.Params[21].AsFloat  := CHMorC;
    Dmod.QrUpd.Params[22].AsFloat  := DUMorA;
    Dmod.QrUpd.Params[23].AsFloat  := DUMorB;
    Dmod.QrUpd.Params[24].AsFloat  := DUMorC;
    Dmod.QrUpd.Params[25].AsFloat  := CHPgmA;
    Dmod.QrUpd.Params[26].AsFloat  := CHPgmB;
    Dmod.QrUpd.Params[27].AsFloat  := CHPgmC;
    Dmod.QrUpd.Params[28].AsFloat  := DUPgmA;
    Dmod.QrUpd.Params[29].AsFloat  := DUPgmB;
    Dmod.QrUpd.Params[30].AsFloat  := DUPgmC;
    // Parei Aqui continuar

    Dmod.QrUpd.ExecSQL;
  end;
  ReopenEvolucap2(0);
  FmPrincipal.AtualizaLastEditLote(Geral.FDT(b, 1));
  ProgressBar1.Visible := False;
  ProgressBar1.Position := 0;
  Screen.Cursor := crDefault;
end;

procedure TFmEvolucap2.QrEvolucap2CalcFields(DataSet: TDataSet);
begin
  (*QrEvolucap2CARTEIREAL.Value :=
  // Parei aqui
    QrEvolucap2CarteiTudo.Value -
    QrEvolucap2RepassEspe.Value -
    QrEvolucap2RepassOutr.Value;
  QrEvolucap2VALOR_TOTAL.Value :=
    QrEvolucap2CarteiTudo.Value +
    QrEvolucap2CHDevolAbe.Value +
    QrEvolucap2DUDevolAbe.Value +
    QrEvolucap2SaldoCaixa.Value +
    QrEvolucap2SaldoConta.Value;
  QrEvolucap2VALOR_SUB_T.Value :=
    QrEvolucap2VALOR_TOTAL.Value -
    QrEvolucap2RepassOutr.Value; *)
end;

procedure TFmEvolucap2.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_RETURN then EditaSaldoConta;
end;

procedure TFmEvolucap2.DBGrid1DblClick(Sender: TObject);
begin
  EditaSaldoConta;
end;

procedure TFmEvolucap2.BitBtn1Click(Sender: TObject);
begin
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE evolucap2 SET SaldoConta=:P0, SaldoCaixa=:P1 ');
  Dmod.QrUpd.SQL.Add('WHERE DataE=:Pa');
  Dmod.QrUpd.Params[0].AsFloat   := Geral.DMV(EdSaldoConta.Text);
  Dmod.QrUpd.Params[1].AsFloat   := Geral.DMV(EdSaldoCaixa.Text);
  //
  Dmod.QrUpd.Params[2].AsString  := Geral.FDT(QrEvolucap2DataE.Value, 1);
  Dmod.QrUpd.ExecSQL;
  //
  ReopenEvolucap2(QrEvolucap2DataE.Value);
  PnAtualiza.Visible  := True;
  PnDados.Enabled     := True;
  PnEditSdoCC.Visible := False;
  DBGrid1.SetFocus;
end;

procedure TFmEvolucap2.ReopenCHNull();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCHNull, Dmod.MyDB, [
  'SELECT ai.Cliente, ai.Emitente, ai.CPF, ',
  'ai.Banco, ai.Agencia, ai.Conta, ai.Cheque, ',
  'ai.Valor, ai.Taxas, ai.Multa, ai.JurosV Juros, ',
  'ai.Desconto, ai.ValPago, ai.ChequeOrigem, ',
  'ai.LoteOrigem, ai.Codigo',
  'FROM alinits ai',
  'LEFT JOIN ' + CO_TabLctA + ' li ON li.FatParcela=ai.ChequeOrigem',
  'WHERE li.FatID=' + Geral.FF0(VAR_FATID_0301),
  'AND ai.Data3=0',
  'AND li.Quitado IS NULL',
  '']);
end;

procedure TFmEvolucap2.ReopenEvolucap2(Data: TDateTime);
begin
  UMyMod.AbreQuery(QrEvolucap2, Dmod.MyDB);
  //
  if Data > 0 then QrEvolucap2.Locate('DataE', Int(Data), [])
  else QrEvolucap2.Last;
  //
end;

procedure TFmEvolucap2.EditaSaldoConta;
begin
  ///////
  Exit;//
  ///////
  PnEditSdoCC.Visible := True;
  PnAtualiza.Visible  := False;
  PnDados.Enabled     := False;
  //
  (*EdSaldoConta.Text := Geral.FFT(QrEvolucap2SaldoConta.Value, 2, siNegativo);
  EdSaldoCaixa.Text := Geral.FFT(QrEvolucap2SaldoCaixa.Value, 2, siNegativo);
  EdSaldoConta.SetFocus;*)
end;

procedure TFmEvolucap2.BitBtn2Click(Sender: TObject);
begin
  PnAtualiza.Visible  := True;
  PnDados.Enabled     := True;
  PnEditSdoCC.Visible := False;
  //
  DBGrid1.SetFocus;
end;

procedure TFmEvolucap2.QrEvolperCalcFields(DataSet: TDataSet);
begin
{ ? 2011-12-10
  QrEvolperCARTEIREAL.Value :=
    QrEvolperCarteiTudo.Value -
    QrEvolperRepassEspe.Value -
    QrEvolperRepassOutr.Value;
  QrEvolperVALOR_TOTAL.Value :=
    QrEvolperCarteiTudo.Value +
    QrEvolperCHDevolAbe.Value +
    QrEvolperDUDevolAbe.Value +
    QrEvolperSaldoCaixa.Value +
    QrEvolperSaldoConta.Value;
  QrEvolperVALOR_SUB_T.Value :=
    QrEvolperVALOR_TOTAL.Value -
    QrEvolperRepassOutr.Value;
}
end;

procedure TFmEvolucap2.BtGraficoClick(Sender: TObject);
(*
var
  Fator: Integer;
  Maximo: Double;
*)
begin
(*
  O ProEssentials n�o � mais instalado migrar para os gr�ficos nativos do Delphi

  QrEvolper.Close;
  QrEvolper.SQL.Clear;
  case RGIntervalo.ItemIndex of
    0:
    begin
      QrEvolper.SQL.Add('SELECT * FROM evolucap2');
      QrEvolper.SQL.Add('WHERE DataE BETWEEN :P0 AND :P1');
      QrEvolper.SQL.Add('ORDER BY DataE');
    end;
    1:
    begin
      QrEvolper.SQL.Add('SELECT * FROM evolucap2');
      QrEvolper.SQL.Add('WHERE DataE BETWEEN :P0 AND :P1');
      QrEvolper.SQL.Add('AND MONTH(DataE) <> MONTH(DATE_ADD(DataE, INTERVAL 1 DAY))');
    end;
  end;
  QrEvolper.Params[0].AsString := Geral.FDT(TPIniB.Date, 1);
  QrEvolper.Params[1].AsString := Geral.FDT(TPFimB.Date, 1);
  UMyMod.AbreQuery(QrEvolper, Dmod.MyDB);
  //
  // N�o � assim!!!
  UnDmkDAC_PF.AbreMySQLQuery0(QrMax, Dmod.MyDB, [
  'SELECT MAX(CarteiTudo+CHDevolAbe+DUDevolAbe+SaldoConta) TOTAL ',
  'FROM evolucap2 ',
  'WHERE DataE BETWEEN "' + Geral.FDT(TPIniB.Date, 1) +
  '" AND "' + Geral.FDT(TPFimB.Date, 1) + '"' +
  '']);
  //
  Fator := 1;
  Maximo := QrMaxTOTAL.Value;
  while Maximo > 1000 do
  begin
    Fator := Fator * 1000;
    Maximo := Maximo / 1000;
  end;
  //
  Pego1.Subsets := 9; // Carteira, Especial, Outros, CH Dev, DU venc., saldo conta, Total - outros, Total
  Pego1.Points := QrEvolper.RecordCount;
  QrEvolper.First;
  while not QrEvolper.Eof do
  begin
    Pego1.YData[0, QrEvolper.RecNo-1] := QrEvolperCARTEIREAL.Value / Fator;
    Pego1.YData[7, QrEvolper.RecNo-1] := QrEvolperVALOR_SUB_T.Value / Fator;
    Pego1.YData[8, QrEvolper.RecNo-1] := QrEvolperVALOR_TOTAL.Value / Fator;
    //
    Pego1.PointLabels[QrEvolper.RecNo-1] := Geral.FDT(QrEvolperDataE.Value, 2);
    //
    QrEvolper.Next;
  end;
  // Set Various Properties //
  Pego1.DeskColor := RGB(192, 192, 192);
  Pego1.GraphBackColor := 0;
  Pego1.GraphForeColor := RGB(255, 255, 255);

  // Set DataShadows to show shadows//
  Pego1.DataShadows := gWithShadows;
  Pego1.BorderTypes := gInset;
  Pego1.MainTitle := '';
  Pego1.SubTitle := 'Evolu��o do Capital de '+
    Geral.FDT(TPIniB.Date, 2)+' at� ' + Geral.FDT(TPFimB.Date, 2);
  if Fator = 1 then
    Pego1.YAxisLabel := '$ Valor'
  else
    Pego1.YAxisLabel := '$ Valor x '+Geral.FFT(Fator, 0, siNegativo);
  Pego1.XAxisLabel := 'Data';
  Pego1.FocalRect := False;
  Pego1.PlottingMethod := gSpline;
  Pego1.GridLineControl := gNoGrid;
  Pego1.AllowRibbon := True;
  Pego1.AllowZooming := gHorzPlusVertZooming;
  Pego1.ZoomStyle := gRO2NOT;

  // Set SubsetLabels property array for 4 subsets //
  Pego1.SubsetLabels[00] := 'Carteira';
  Pego1.SubsetLabels[01] := 'Especial';
  Pego1.SubsetLabels[02] := 'Outros';
  Pego1.SubsetLabels[03] := 'CH Dev.';
  Pego1.SubsetLabels[04] := 'DU Dev.';
  Pego1.SubsetLabels[05] := 'Sdo caixa';
  Pego1.SubsetLabels[06] := 'Sdo conta';
  Pego1.SubsetLabels[07] := '(-)outros';
  Pego1.SubsetLabels[08] := 'Total';

  // this is how to change subset colors //
  Pego1.SubsetColors[0] := RGB(198, 0, 0);
  Pego1.SubsetColors[1] := RGB(0, 198, 198);
  Pego1.SubsetColors[2] := RGB(198, 198, 0);
  Pego1.SubsetColors[3] := RGB(0, 198, 0);

  // this is how to change line types //
  Pego1.SubsetLineTypes[0] := 0;//PELT_MEDIUMSOLID;
  Pego1.SubsetLineTypes[1] := 1;//PELT_MEDIUMSOLID;
  Pego1.SubsetLineTypes[2] := 2;//PELT_MEDIUMSOLID;
  Pego1.SubsetLineTypes[3] := 3;//PELT_MEDIUMSOLID;
  Pego1.SubsetLineTypes[4] := 0;//PELT_MEDIUMSOLID;
  Pego1.SubsetLineTypes[5] := 1;//PELT_MEDIUMSOLID;
  Pego1.SubsetLineTypes[6] := 2;//PELT_MEDIUMSOLID;
  Pego1.SubsetLineTypes[7] := 3;//PELT_MEDIUMSOLID;
  Pego1.SubsetLineTypes[8] := 6;//PELT_MEDIUMSOLID;

  // this is how to change point types //
  Pego1.SubsetPointTypes[0] := 0;//PEPT_DOTSOLID;
  Pego1.SubsetPointTypes[1] := 1;//PEPT_UPTRIANGLESOLID;
  Pego1.SubsetPointTypes[2] := 2;//PEPT_SQUARESOLID;
  Pego1.SubsetPointTypes[3] := 3;//PEPT_DOWNTRIANGLESOLID;
  Pego1.SubsetPointTypes[4] := 0;//PEPT_DOTSOLID;
  Pego1.SubsetPointTypes[5] := 1;//PEPT_UPTRIANGLESOLID;
  Pego1.SubsetPointTypes[6] := 2;//PEPT_SQUARESOLID;
  Pego1.SubsetPointTypes[7] := 0;//PEPT_SQUARESOLID;
  Pego1.SubsetPointTypes[8] := 3;//PEPT_DOWNTRIANGLESOLID;

  // Various other features //
  Pego1.FixedFonts := True;
  Pego1.BitmapGradientMode := True;
  Pego1.QuickStyle := gLightLine;
  Pego1.SimpleLineLegend:=True;
  Pego1.SimplePointLegend:=True;
  Pego1.LegendStyle:=gOneLine;

  Pego1.GradientBars := 8;
  Pego1.MainTitleBold := True;
  Pego1.SubTitleBold := True;
  Pego1.LabelBold := True;
  Pego1.LineShadows := True;
  Pego1.TextShadows := gShadowBoldText;
  Pego1.FontSize := gSmall;

  // Always call PEactions := 0 at end //
  Pego1.Visible := True;
  Pego1.PEactions := epeactions(0);
  PageControl1.ActivePageIndex := 1;
*)
end;

procedure TFmEvolucap2.QrCHNullAfterOpen(DataSet: TDataSet);
begin
  if QrCHNull.RecordCount > 0 then
    BtExclui1.Enabled := True
  else
    BtExclui1.Enabled := False;
end;

procedure TFmEvolucap2.QrCHNullBeforeClose(DataSet: TDataSet);
begin
  BtExclui1.Enabled := False;
end;

procedure TFmEvolucap2.BtExclui1Click(Sender: TObject);
begin
  if Geral.MensagemBox('Confirma a exclus�o do cheque devolvido orf�o?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM alinits WHERE Codigo=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrCHNullCodigo.Value;
    Dmod.QrUpd.ExecSQL;
    //
{
    QrCHNull.Close;
    QrCHNull. Open;
}
    ReopenCHNull();
  //
  end;
end;

procedure TFmEvolucap2.InsereItensAbe(
 var ValChA, ValChB, ValChC, ValDuA, ValDuB, ValDuC: Double;
 Data: String; Status: Integer; Texto: String);
var
  x: String;
begin
  ValChA := 0;
  ValChB := 0;
  ValChC := 0;
  ValDuA := 0;
  ValDuB := 0;
  ValDuC := 0;
  x := Geral.FF0(QrEmcart.RecordCount);
  QrEmcart.First;
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('INSERT INTO evolucap2its SET ');
  Dmod.QrUpdM.SQL.Add('DataE=:P0, Coligado=:P1, Tipo=:P2, Status=:P3, ');
  Dmod.QrUpdM.SQL.Add('Valor=:P4 ');
  while not QrEmcart.Eof do
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'incluindo item ' + Geral.FF0(
    QrEmcart.RecNo) + ' de ' + x + ' de ' + Texto);
    if QrEmcartColigado.Value = 0 then
    begin
      case QrEmcartTipo.Value of
        0: ValChA := ValChA + QrEmcartValor.Value;
        1: ValDuA := ValDuA + QrEmcartValor.Value;
      end;
    end else
    if QrEmcartColigado.Value = Dmod.QrControleColigEsp.Value then
    begin
      case QrEmcartTipo.Value of
        0: ValChB := ValChB + QrEmcartValor.Value;
        1: ValDuB := ValDuB + QrEmcartValor.Value;
      end;
    end else
    begin
      case QrEmcartTipo.Value of
        0: ValChC := ValChC + QrEmcartValor.Value;
        1: ValDuC := ValDuC + QrEmcartValor.Value;
      end;
    end;
    // SQL Itens
    Dmod.QrUpdM.Params[00].AsString  := Data;
    Dmod.QrUpdM.Params[01].AsInteger := QrEmcartColigado.Value;
    Dmod.QrUpdM.Params[02].AsInteger := QrEmcartTipo.Value;
    Dmod.QrUpdM.Params[03].AsInteger := Status;
    Dmod.QrUpdM.Params[04].AsFloat   := QrEmcartValor.Value;
    Dmod.QrUpdM.ExecSQL;
    //
    QrEmcart.Next;
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmEvolucap2.InsereItensVen(Query: TmySQLQuery; var ValA, ValB, ValC: Double;
              Data: String; Status: Integer; Texto: String);
var
  Valor: Double;
  Coligado, Tipo: Integer;
  x: String;
begin
  x := Geral.FF0(Query.RecordCount);
  ValA := 0;
  ValB := 0;
  ValC := 0;
  Query.First;
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('INSERT INTO evolucap2its SET ');
  Dmod.QrUpdM.SQL.Add('DataE=:P0, Coligado=:P1, Tipo=:P2, Status=:P3, ');
  Dmod.QrUpdM.SQL.Add('Valor=:P4 ');
  while not Query.Eof do
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'incluindo item ' + Geral.FF0(
    Query.RecNo) + ' de ' + x + ' de ' + Texto);
    try
      Valor    := Query.FieldByName('Valor').AsFloat;
      Coligado := Query.FieldByName('Coligado').AsInteger;
      Tipo     := Query.FieldByName('Tipo').AsInteger
    except
      Valor := 0;
      Coligado := 0;
      Tipo := 0;
    end;
    if Coligado = 0 then ValA := ValA + Valor
    else
    if Coligado = Dmod.QrControleColigEsp.Value then
      ValB := ValB + Valor
    else
      ValC := ValC + Valor;
    // SQL Itens
    Dmod.QrUpdM.Params[00].AsString  := Data;
    Dmod.QrUpdM.Params[01].AsInteger := Coligado;
    Dmod.QrUpdM.Params[02].AsInteger := Tipo;
    Dmod.QrUpdM.Params[03].AsInteger := Status;
    Dmod.QrUpdM.Params[04].AsFloat   := Valor;
    Dmod.QrUpdM.ExecSQL;
    //
    Query.Next;
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

end.

