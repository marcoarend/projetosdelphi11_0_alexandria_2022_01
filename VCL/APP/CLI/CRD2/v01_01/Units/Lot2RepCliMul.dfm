object FmLot2RepCliMul: TFmLot2RepCliMul
  Left = 360
  Top = 177
  Caption = 'BDR-GEREN-012 :: Pagamento com Cheques de Terceiros'
  ClientHeight = 600
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 48
    Width = 1008
    Height = 438
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Leitora CMC7'
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 1000
        Height = 410
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Panel4: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 101
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object Panel5: TPanel
            Left = 788
            Top = 0
            Width = 212
            Height = 101
            Align = alRight
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Panel7: TPanel
              Left = 0
              Top = 0
              Width = 101
              Height = 101
              Align = alLeft
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object CkDescende1: TCheckBox
                Left = 4
                Top = 12
                Width = 89
                Height = 17
                Caption = 'Descendente.'
                TabOrder = 0
              end
              object CkDescende2: TCheckBox
                Left = 4
                Top = 44
                Width = 89
                Height = 17
                Caption = 'Descendente.'
                Checked = True
                State = cbChecked
                TabOrder = 1
              end
              object CkDescende3: TCheckBox
                Left = 4
                Top = 76
                Width = 89
                Height = 17
                Caption = 'Descendente.'
                TabOrder = 2
              end
            end
            object GroupBox1: TGroupBox
              Left = 96
              Top = 0
              Width = 116
              Height = 101
              Align = alRight
              Caption = ' Soma selecionados: '
              TabOrder = 1
              object Label1: TLabel
                Left = 6
                Top = 50
                Width = 9
                Height = 13
                Caption = '(-)'
              end
              object Label2: TLabel
                Left = 6
                Top = 76
                Width = 6
                Height = 13
                Caption = '='
              end
              object EdSoma: TdmkEdit
                Left = 6
                Top = 20
                Width = 97
                Height = 21
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnChange = EdSomaChange
              end
              object EdFalta0: TDBEdit
                Left = 20
                Top = 46
                Width = 83
                Height = 21
                DataField = 'SALDOAPAGAR'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 1
                OnChange = EdFalta0Change
              end
              object EdSaldo: TdmkEdit
                Left = 20
                Top = 72
                Width = 83
                Height = 21
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                ReadOnly = True
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
            end
          end
          object Panel6: TPanel
            Left = 0
            Top = 0
            Width = 788
            Height = 101
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 1
            object RGOrdem3: TRadioGroup
              Left = 0
              Top = 32
              Width = 788
              Height = 32
              Align = alTop
              Caption = ' Ordem 3: '
              Columns = 6
              ItemIndex = 1
              Items.Strings = (
                'Vencimento'
                'Valor'
                'Cheque'
                'BAC'
                'Emitente'
                'Cliente')
              TabOrder = 0
              OnClick = RGOrdem1Click
            end
            object RGOrdem2: TRadioGroup
              Left = 0
              Top = 64
              Width = 788
              Height = 32
              Align = alTop
              Caption = ' Ordem 2: '
              Columns = 6
              ItemIndex = 2
              Items.Strings = (
                'Vencimento'
                'Valor'
                'Cheque'
                'BAC'
                'Emitente'
                'Cliente')
              TabOrder = 1
              OnClick = RGOrdem1Click
            end
            object RGOrdem1: TRadioGroup
              Left = 0
              Top = 0
              Width = 788
              Height = 32
              Align = alTop
              Caption = ' Ordem 1: '
              Columns = 6
              ItemIndex = 0
              Items.Strings = (
                'Vencimento'
                'Valor'
                'Cheque'
                'BAC'
                'Emitente'
                'Cliente')
              TabOrder = 2
              OnClick = RGOrdem1Click
            end
          end
        end
        object Panel3: TPanel
          Left = 0
          Top = 101
          Width = 1000
          Height = 309
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object DBG1: TDBGrid
            Left = 0
            Top = 0
            Width = 1000
            Height = 220
            Align = alClient
            DataSource = DsCheque7
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnCellClick = DBG1CellClick
            OnKeyDown = DBG1KeyDown
            Columns = <
              item
                Expanded = False
                FieldName = 'DDeposito'
                Title.Caption = 'Vencto'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Credito'
                Title.Caption = 'Valor'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Documento'
                Title.Caption = 'Cheque'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Banco'
                Title.Caption = 'Bco'
                Width = 24
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Agencia'
                Title.Caption = 'Agen.'
                Width = 32
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ContaCorrente'
                Title.Caption = 'Conta corrente'
                Width = 100
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Emitente'
                Width = 208
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMECLI'
                Title.Caption = 'Cliente'
                Width = 208
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Lote'
                Title.Caption = 'Border'#244
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'FatNum'
                Title.Caption = 'Lote'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'FatParcela'
                Visible = True
              end>
          end
          object Panel9: TPanel
            Left = 0
            Top = 220
            Width = 1000
            Height = 89
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 0
            TabStop = True
            object Panel10: TPanel
              Left = 0
              Top = 0
              Width = 1000
              Height = 89
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label43: TLabel
                Left = 4
                Top = 4
                Width = 143
                Height = 13
                Caption = 'Leitura pela banda magn'#233'tica:'
              end
              object Label49: TLabel
                Left = 4
                Top = 44
                Width = 56
                Height = 13
                Caption = 'CGC / CPF:'
              end
              object Label50: TLabel
                Left = 164
                Top = 44
                Width = 44
                Height = 13
                Caption = 'Emitente:'
              end
              object Label44: TLabel
                Left = 256
                Top = 4
                Width = 27
                Height = 13
                Caption = 'Com.:'
              end
              object Label45: TLabel
                Left = 288
                Top = 4
                Width = 22
                Height = 13
                Caption = 'Bco:'
              end
              object Label46: TLabel
                Left = 320
                Top = 4
                Width = 34
                Height = 13
                Caption = 'Ag'#234'nc:'
              end
              object Label47: TLabel
                Left = 360
                Top = 4
                Width = 55
                Height = 13
                Caption = 'Conta corr.:'
              end
              object Label48: TLabel
                Left = 436
                Top = 4
                Width = 45
                Height = 13
                Caption = 'N'#186' cheq.:'
              end
              object Label42: TLabel
                Left = 492
                Top = 4
                Width = 27
                Height = 13
                Caption = 'Valor:'
              end
              object Label3: TLabel
                Left = 572
                Top = 4
                Width = 59
                Height = 13
                Caption = 'Vencimento:'
              end
              object EdCMC_7_1: TEdit
                Left = 132
                Top = 16
                Width = 121
                Height = 21
                MaxLength = 30
                TabOrder = 0
                Visible = False
                OnChange = EdCMC_7_1Change
              end
              object EdCPF_2: TdmkEdit
                Left = 4
                Top = 60
                Width = 157
                Height = 21
                Alignment = taCenter
                Enabled = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdEmitente2: TdmkEdit
                Left = 164
                Top = 60
                Width = 481
                Height = 21
                CharCase = ecUpperCase
                Enabled = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdRegiaoCompe: TdmkEdit
                Left = 256
                Top = 20
                Width = 29
                Height = 21
                Alignment = taRightJustify
                Enabled = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 3
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 3
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '000'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object EdBanco: TdmkEdit
                Left = 288
                Top = 20
                Width = 29
                Height = 21
                Alignment = taRightJustify
                Enabled = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 4
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 3
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '000'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object EdAgencia: TdmkEdit
                Left = 320
                Top = 20
                Width = 37
                Height = 21
                Alignment = taRightJustify
                Enabled = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 5
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 4
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0000'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object EdConta: TdmkEdit
                Left = 360
                Top = 20
                Width = 73
                Height = 21
                Alignment = taRightJustify
                Enabled = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 6
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 10
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0000000000'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object EdRealCC: TdmkEdit
                Left = 360
                Top = 40
                Width = 73
                Height = 21
                Alignment = taRightJustify
                Enabled = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 7
                Visible = False
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 10
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0000000000'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object EdCheque: TdmkEdit
                Left = 436
                Top = 20
                Width = 53
                Height = 21
                Alignment = taRightJustify
                Enabled = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 8
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 6
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '000000'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object EdCredito: TdmkEdit
                Left = 492
                Top = 20
                Width = 77
                Height = 21
                Alignment = taRightJustify
                Enabled = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 9
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object EdVencto: TdmkEdit
                Left = 572
                Top = 20
                Width = 73
                Height = 21
                Alignment = taRightJustify
                Enabled = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 10
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 6
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '000000'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object EdBanda: TdmkEdit
                Left = 4
                Top = 20
                Width = 249
                Height = 21
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                MaxLength = 34
                ParentFont = False
                TabOrder = 11
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnChange = EdBandaChange
              end
            end
          end
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Pesquisa manual'
      ImageIndex = 1
      object Panel11: TPanel
        Left = 0
        Top = 0
        Width = 1000
        Height = 65
        Align = alTop
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Label5: TLabel
          Left = 364
          Top = 4
          Width = 56
          Height = 13
          Caption = 'CGC / CPF:'
        end
        object Label6: TLabel
          Left = 524
          Top = 4
          Width = 44
          Height = 13
          Caption = 'Emitente:'
        end
        object Label8: TLabel
          Left = 4
          Top = 4
          Width = 22
          Height = 13
          Caption = 'Bco:'
        end
        object Label9: TLabel
          Left = 36
          Top = 4
          Width = 34
          Height = 13
          Caption = 'Ag'#234'nc:'
        end
        object Label10: TLabel
          Left = 76
          Top = 4
          Width = 55
          Height = 13
          Caption = 'Conta corr.:'
        end
        object Label11: TLabel
          Left = 152
          Top = 4
          Width = 45
          Height = 13
          Caption = 'N'#186' cheq.:'
        end
        object Label12: TLabel
          Left = 208
          Top = 4
          Width = 27
          Height = 13
          Caption = 'Valor:'
        end
        object Label13: TLabel
          Left = 288
          Top = 4
          Width = 59
          Height = 13
          Caption = 'Vencimento:'
        end
        object EdCPFM: TdmkEdit
          Left = 364
          Top = 20
          Width = 157
          Height = 21
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 6
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnExit = EdCPFMExit
        end
        object EdEmitM: TdmkEdit
          Left = 524
          Top = 20
          Width = 253
          Height = 21
          CharCase = ecUpperCase
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 7
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnExit = EdEmitMExit
        end
        object EdBancoM: TdmkEdit
          Left = 4
          Top = 20
          Width = 29
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 3
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnExit = EdBancoMExit
        end
        object EdAgenciaM: TdmkEdit
          Left = 36
          Top = 20
          Width = 37
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 4
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnExit = EdAgenciaMExit
        end
        object EdContaM: TdmkEdit
          Left = 76
          Top = 20
          Width = 73
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 10
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0000000000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnExit = EdContaMExit
        end
        object EdChequeM: TdmkEdit
          Left = 152
          Top = 20
          Width = 53
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 6
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '000000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnExit = EdChequeMExit
        end
        object EdCreditoM: TdmkEdit
          Left = 208
          Top = 20
          Width = 77
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 4
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnExit = EdCreditoMExit
        end
        object EdVctoM: TdmkEdit
          Left = 288
          Top = 20
          Width = 73
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 5
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 6
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '000000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnExit = EdVctoMExit
        end
        object CkDepositados: TCheckBox
          Left = 4
          Top = 44
          Width = 773
          Height = 17
          Caption = 'Incluir cheques depositados.'
          TabOrder = 8
          OnClick = CkDepositadosClick
        end
      end
      object DBG2: TDBGrid
        Left = 0
        Top = 65
        Width = 1000
        Height = 345
        Align = alClient
        DataSource = DsChequeM
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnKeyDown = DBG1KeyDown
        Columns = <
          item
            Expanded = False
            FieldName = 'DDeposito'
            Title.Caption = 'Vencto'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Credito'
            Title.Caption = 'Valor'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Documento'
            Title.Caption = 'Cheque'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Banco'
            Title.Caption = 'Bco'
            Width = 24
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Agencia'
            Title.Caption = 'Agen.'
            Width = 32
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ContaCorrente'
            Title.Caption = 'Conta corrente'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Emitente'
            Width = 208
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMECLI'
            Title.Caption = 'Cliente'
            Width = 208
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Lote'
            Title.Caption = 'Border'#244
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FatNum'
            Title.Caption = 'Lote'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FatParcela'
            Visible = True
          end>
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Pesquisa Valor'
      ImageIndex = 2
      object Panel12: TPanel
        Left = 0
        Top = 0
        Width = 1000
        Height = 101
        Align = alTop
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Label4: TLabel
          Left = 4
          Top = 4
          Width = 27
          Height = 13
          Caption = 'Valor:'
        end
        object EdCreditoV: TdmkEdit
          Left = 4
          Top = 20
          Width = 77
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnExit = EdCreditoVExit
        end
        object CkAuto: TCheckBox
          Left = 88
          Top = 24
          Width = 273
          Height = 17
          Caption = 'Autom'#225'tico (Quando apenas um cheque encontrado).'
          Checked = True
          State = cbChecked
          TabOrder = 1
        end
        object GroupBox2: TGroupBox
          Left = 889
          Top = 0
          Width = 111
          Height = 101
          Align = alRight
          Caption = ' Saldo pendente: '
          TabOrder = 2
          object Label14: TLabel
            Left = 6
            Top = 50
            Width = 9
            Height = 13
            Caption = '(-)'
          end
          object Label15: TLabel
            Left = 6
            Top = 76
            Width = 6
            Height = 13
            Caption = '='
            Visible = False
          end
          object EdSoma2: TdmkEdit
            Left = 6
            Top = 20
            Width = 97
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
            Visible = False
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnChange = EdSomaChange
          end
          object EdFalta2: TDBEdit
            Left = 20
            Top = 46
            Width = 83
            Height = 21
            DataField = 'SALDOAPAGAR'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
            OnChange = EdFalta0Change
          end
          object EdSaldo2: TdmkEdit
            Left = 20
            Top = 72
            Width = 83
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            ReadOnly = True
            TabOrder = 2
            Visible = False
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
      end
      object DBG3: TDBGrid
        Left = 0
        Top = 101
        Width = 1000
        Height = 309
        Align = alClient
        DataSource = DsChequeV
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnKeyDown = DBG1KeyDown
        Columns = <
          item
            Expanded = False
            FieldName = 'DDeposito'
            Title.Caption = 'Vencto'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Credito'
            Title.Caption = 'Valor'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Documento'
            Title.Caption = 'Cheque'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Banco'
            Title.Caption = 'Bco'
            Width = 24
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Agencia'
            Title.Caption = 'Agen.'
            Width = 32
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ContaCorrente'
            Title.Caption = 'Conta corrente'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Emitente'
            Width = 208
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMECLI'
            Title.Caption = 'Cliente'
            Width = 208
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Lote'
            Title.Caption = 'Border'#244
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FatNum'
            Title.Caption = 'Lote'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FatParcela'
            Title.Caption = 'Controle'
            Visible = True
          end>
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 469
        Height = 32
        Caption = 'Pagamento com Cheques de Terceiros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 469
        Height = 32
        Caption = 'Pagamento com Cheques de Terceiros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 469
        Height = 32
        Caption = 'Pagamento com Cheques de Terceiros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 486
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel8: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 530
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BitBtn3: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel13: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label16: TLabel
        Left = 508
        Top = 4
        Width = 46
        Height = 13
        Caption = 'Em caixa:'
        FocusControl = DBEdit2
      end
      object BtOK: TBitBtn
        Tag = 20
        Left = 20
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Pagar'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BitBtn1: TBitBtn
        Tag = 301
        Left = 204
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Depositar'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BitBtn1Click
      end
      object BitBtn2: TBitBtn
        Tag = 293
        Left = 388
        Top = 4
        Width = 90
        Height = 40
        Caption = 'So&bra'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BitBtn2Click
      end
      object DBEdit2: TDBEdit
        Left = 508
        Top = 20
        Width = 76
        Height = 21
        DataField = 'TOTAL'
        DataSource = DsCaixa
        TabOrder = 3
      end
    end
  end
  object QrCheque7: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial,'
      'ent.Nome) NOMECLI, lot.Lote, loi.*'
      'FROM lct0001a loi'
      'LEFT JOIN lot0001a lot ON lot.Codigo=loi.FatNum'
      'LEFT JOIN entidades ent ON ent.Codigo=lot.Cliente'
      'WHERE loi.FatID=0301'
      'AND lot.Tipo = 0'
      'AND loi.RepCli = 0'
      'AND loi.Repassado = 0'
      'AND loi.Devolucao = 0'
      'AND loi.Depositado = 0'
      '')
    Left = 20
    Top = 220
    object QrCheque7NOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrCheque7Lote: TIntegerField
      FieldName = 'Lote'
    end
    object QrCheque7Data: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCheque7Credito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00'
    end
    object QrCheque7Documento: TFloatField
      FieldName = 'Documento'
      DisplayFormat = '000000'
    end
    object QrCheque7Vencimento: TDateField
      FieldName = 'Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCheque7FatNum: TFloatField
      FieldName = 'FatNum'
      DisplayFormat = '000000'
    end
    object QrCheque7FatParcela: TIntegerField
      FieldName = 'FatParcela'
      DisplayFormat = '000000'
    end
    object QrCheque7Emitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
    object QrCheque7Banco: TIntegerField
      FieldName = 'Banco'
      DisplayFormat = '000'
    end
    object QrCheque7Agencia: TIntegerField
      FieldName = 'Agencia'
      DisplayFormat = '0000'
    end
    object QrCheque7ContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrCheque7CNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrCheque7DCompra: TDateField
      FieldName = 'DCompra'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCheque7DDeposito: TDateField
      FieldName = 'DDeposito'
      DisplayFormat = 'dd/mm/yy'
    end
  end
  object DsCheque7: TDataSource
    DataSet = QrCheque7
    Left = 48
    Top = 220
  end
  object Timer1: TTimer
    Interval = 300
    OnTimer = Timer1Timer
    Left = 332
    Top = 205
  end
  object QrChequeM: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial'
      'ELSE ent.Nome END NOMECLI, lot.Lote, loi.*'
      'FROM lot esits loi'
      'LEFT JOIN lot es lot ON lot.Codigo=loi.Codigo'
      'LEFT JOIN entidades ent ON ent.Codigo=lot.Cliente'
      'WHERE lot.Tipo = 0'
      'AND loi.RepCli = 0'
      'AND loi.Repassado = 0'
      'AND loi.Depositado = 0'
      'AND loi.Devolucao = 0'
      'AND loi.DDeposito <= :P0')
    Left = 20
    Top = 248
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrChequeMNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrChequeMLote: TIntegerField
      FieldName = 'Lote'
    end
    object QrChequeMData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrChequeMCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00'
    end
    object QrChequeMDocumento: TFloatField
      FieldName = 'Documento'
      DisplayFormat = '000000'
    end
    object QrChequeMVencimento: TDateField
      FieldName = 'Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrChequeMFatNum: TFloatField
      FieldName = 'FatNum'
      DisplayFormat = '000000'
    end
    object QrChequeMFatParcela: TIntegerField
      FieldName = 'FatParcela'
      DisplayFormat = '000000'
    end
    object QrChequeMEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
    object QrChequeMBanco: TIntegerField
      FieldName = 'Banco'
      DisplayFormat = '000'
    end
    object QrChequeMAgencia: TIntegerField
      FieldName = 'Agencia'
      DisplayFormat = '0000'
    end
    object QrChequeMContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrChequeMCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrChequeMDCompra: TDateField
      FieldName = 'DCompra'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrChequeMDDeposito: TDateField
      FieldName = 'DDeposito'
      DisplayFormat = 'dd/mm/yy'
    end
  end
  object DsChequeM: TDataSource
    DataSet = QrChequeM
    Left = 48
    Top = 248
  end
  object QrChequeV: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial'
      'ELSE ent.Nome END NOMECLI, lot.Lote, loi.*'
      'FROM lot esits loi'
      'LEFT JOIN lot es lot ON lot.Codigo=loi.Codigo'
      'LEFT JOIN entidades ent ON ent.Codigo=lot.Cliente'
      'WHERE lot.Tipo = 0'
      'AND loi.RepCli = 0'
      'AND loi.Repassado = 0'
      'AND loi.Depositado = 0'
      'AND loi.Devolucao = 0'
      'AND loi.DDeposito <= :P0')
    Left = 20
    Top = 276
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrChequeVNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrChequeVLote: TIntegerField
      FieldName = 'Lote'
    end
    object QrChequeVData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrChequeVCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00'
    end
    object QrChequeVDocumento: TFloatField
      FieldName = 'Documento'
      DisplayFormat = '000000'
    end
    object QrChequeVVencimento: TDateField
      FieldName = 'Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrChequeVFatNum: TFloatField
      FieldName = 'FatNum'
      DisplayFormat = '000000'
    end
    object QrChequeVFatParcela: TIntegerField
      FieldName = 'FatParcela'
      DisplayFormat = '000000'
    end
    object QrChequeVEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
    object QrChequeVBanco: TIntegerField
      FieldName = 'Banco'
      DisplayFormat = '000'
    end
    object QrChequeVAgencia: TIntegerField
      FieldName = 'Agencia'
      DisplayFormat = '0000'
    end
    object QrChequeVContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrChequeVCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrChequeVDCompra: TDateField
      FieldName = 'DCompra'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrChequeVDDeposito: TDateField
      FieldName = 'DDeposito'
      DisplayFormat = 'dd/mm/yy'
    end
  end
  object DsChequeV: TDataSource
    DataSet = QrChequeV
    Left = 48
    Top = 276
  end
  object QrCaixa: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Valor) TOTAL'
      'FROM lot esits loi'
      'LEFT JOIN lot es lot ON lot.Codigo=loi.Codigo'
      'WHERE lot.Tipo = 0'
      'AND loi.RepCli = 0'
      'AND loi.Repassado = 0'
      'AND loi.Depositado = 0'
      'AND loi.Devolucao = 0'
      'AND loi.DDeposito <= :P0')
    Left = 20
    Top = 304
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCaixaTOTAL: TFloatField
      FieldName = 'TOTAL'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsCaixa: TDataSource
    DataSet = QrCaixa
    Left = 48
    Top = 304
  end
end
