unit Lot2Enc;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, UnDmkEnums;

type
  TFmLot2Enc = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    PB1: TProgressBar;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure EncerraAbertos();
  end;

  var
  FmLot2Enc: TFmLot2Enc;

implementation

uses UnMyObjects, Module, ModuleLot2, ModuleFin, ModuleLot, UMySQLModule;

{$R *.DFM}

procedure TFmLot2Enc.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLot2Enc.EncerraAbertos();
begin
  Screen.Cursor := crHourGlass;
  try
    UMyMod.AbreQuery(DmLot.QrLot, Dmod.MyDB);
    PB1.Position := 0;
    PB1.Max := DmLot.QrLot.RecordCount;
    if DmLot.QrLot.RecordCount > 0 then
    begin
      if not DModFin.HahContasSemFat(True) then
      begin
        DmLot.QrLot.First;
        while not DmLot.QrLot.Eof do
        begin
          PB1.Position := PB1.Position + 1;
          MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Encerrando o lote ' +
          Geral.FF0(DmLot.QrLotCodigo.Value));
          //
          DmLot2.LancamentosAutomaticosBordero(DmLot.QrLotCodigo.Value);
          //
          DmLot.QrLot.Next;
        end;
      end;
    end else Geral.MensagemBox('N�o h� lotes a serem encerrados', 'Informa��o',
    MB_OK+MB_ICONINFORMATION);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmLot2Enc.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  ImgTipo.SQLType := stPsq;
end;

procedure TFmLot2Enc.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
