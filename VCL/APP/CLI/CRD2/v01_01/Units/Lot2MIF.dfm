object FmLot2MIF: TFmLot2MIF
  Left = 339
  Top = 185
  Caption = '???-?????-999 :: Migra'#231#227'o de Itens de Border'#244's'
  ClientHeight = 569
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 542
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 494
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 446
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 374
        Height = 32
        Caption = 'Migra'#231#227'o de Itens de Border'#244's'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 374
        Height = 32
        Caption = 'Migra'#231#227'o de Itens de Border'#244's'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 374
        Height = 32
        Caption = 'Migra'#231#227'o de Itens de Border'#244's'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 499
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 1
    ExplicitTop = 482
    ExplicitWidth = 542
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 396
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 394
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Memo1: TMemo
    Left = 561
    Top = 121
    Width = 447
    Height = 378
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 2
    ExplicitLeft = 0
    ExplicitTop = 356
    ExplicitWidth = 1008
    ExplicitHeight = 213
  end
  object Panel2: TPanel
    Left = 0
    Top = 121
    Width = 561
    Height = 378
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 3
    ExplicitTop = 53
    ExplicitHeight = 446
    object CGMigra: TdmkCheckGroup
      Left = 0
      Top = 0
      Width = 561
      Height = 64
      Align = alTop
      Caption = ' Tabelas a serem migradas: '
      Columns = 4
      Items.Strings = (
        'Itens de border'#244' mortos'
        'Itens de border'#244' ativos'
        'Taxas'
        'Ocorr'#234'ncias'
        'Pagto de ch devolvidos'
        'Pagto de dupl. vencidas'
        'Cabe'#231'. border'#244's ativos'
        'Cabe'#231'. border'#244's mortos')
      TabOrder = 0
      UpdType = utYes
      Value = 0
      OldValor = 0
      ExplicitTop = 121
      ExplicitWidth = 542
    end
    object DBGrid1: TDBGrid
      Left = 0
      Top = 64
      Width = 561
      Height = 314
      Align = alClient
      DataSource = DModFin.DsSemConta
      TabOrder = 1
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 48
    Width = 1008
    Height = 73
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 4
    ExplicitLeft = 4
    ExplicitTop = 4
    ExplicitWidth = 561
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 56
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 557
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 39
        Width = 1004
        Height = 17
        Align = alBottom
        TabOrder = 0
        ExplicitWidth = 557
      end
      object PB0: TProgressBar
        Left = 0
        Top = 22
        Width = 1004
        Height = 17
        Align = alBottom
        TabOrder = 1
        ExplicitWidth = 557
      end
    end
  end
  object QrLotIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT loi.AlinPgs ChqPgs, loi.* '
      'FROM lotesits loi'
      'LEFT JOIN lotes lot ON lot .Codigo=loi.Codigo'
      'ORDER BY Codigo, Controle ')
    Left = 256
    Top = 60
    object QrLotItsChqPgs: TIntegerField
      FieldName = 'ChqPgs'
    end
    object QrLotItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLotItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLotItsComp: TIntegerField
      FieldName = 'Comp'
    end
    object QrLotItsPraca: TIntegerField
      FieldName = 'Praca'
    end
    object QrLotItsBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrLotItsAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrLotItsConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrLotItsCheque: TIntegerField
      FieldName = 'Cheque'
    end
    object QrLotItsCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrLotItsEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrLotItsBruto: TFloatField
      FieldName = 'Bruto'
    end
    object QrLotItsDesco: TFloatField
      FieldName = 'Desco'
    end
    object QrLotItsValor: TFloatField
      FieldName = 'Valor'
    end
    object QrLotItsEmissao: TDateField
      FieldName = 'Emissao'
    end
    object QrLotItsDCompra: TDateField
      FieldName = 'DCompra'
    end
    object QrLotItsDDeposito: TDateField
      FieldName = 'DDeposito'
    end
    object QrLotItsVencto: TDateField
      FieldName = 'Vencto'
    end
    object QrLotItsTxaCompra: TFloatField
      FieldName = 'TxaCompra'
    end
    object QrLotItsTxaJuros: TFloatField
      FieldName = 'TxaJuros'
    end
    object QrLotItsTxaAdValorem: TFloatField
      FieldName = 'TxaAdValorem'
    end
    object QrLotItsVlrCompra: TFloatField
      FieldName = 'VlrCompra'
    end
    object QrLotItsVlrAdValorem: TFloatField
      FieldName = 'VlrAdValorem'
    end
    object QrLotItsDMais: TIntegerField
      FieldName = 'DMais'
    end
    object QrLotItsDias: TIntegerField
      FieldName = 'Dias'
    end
    object QrLotItsDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 12
    end
    object QrLotItsDevolucao: TIntegerField
      FieldName = 'Devolucao'
    end
    object QrLotItsProrrVz: TIntegerField
      FieldName = 'ProrrVz'
    end
    object QrLotItsProrrDd: TIntegerField
      FieldName = 'ProrrDd'
    end
    object QrLotItsQuitado: TIntegerField
      FieldName = 'Quitado'
    end
    object QrLotItsBcoCobra: TIntegerField
      FieldName = 'BcoCobra'
    end
    object QrLotItsAgeCobra: TIntegerField
      FieldName = 'AgeCobra'
    end
    object QrLotItsTotalJr: TFloatField
      FieldName = 'TotalJr'
    end
    object QrLotItsTotalDs: TFloatField
      FieldName = 'TotalDs'
    end
    object QrLotItsTotalPg: TFloatField
      FieldName = 'TotalPg'
    end
    object QrLotItsData3: TDateField
      FieldName = 'Data3'
    end
    object QrLotItsRepassado: TSmallintField
      FieldName = 'Repassado'
    end
    object QrLotItsDepositado: TSmallintField
      FieldName = 'Depositado'
    end
    object QrLotItsValQuit: TFloatField
      FieldName = 'ValQuit'
    end
    object QrLotItsValDeposito: TFloatField
      FieldName = 'ValDeposito'
    end
    object QrLotItsTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrLotItsAliIts: TIntegerField
      FieldName = 'AliIts'
    end
    object QrLotItsAlinPgs: TIntegerField
      FieldName = 'AlinPgs'
    end
    object QrLotItsNaoDeposita: TSmallintField
      FieldName = 'NaoDeposita'
    end
    object QrLotItsReforcoCxa: TSmallintField
      FieldName = 'ReforcoCxa'
    end
    object QrLotItsCartDep: TIntegerField
      FieldName = 'CartDep'
    end
    object QrLotItsCobranca: TIntegerField
      FieldName = 'Cobranca'
    end
    object QrLotItsRepCli: TIntegerField
      FieldName = 'RepCli'
    end
    object QrLotItsMotivo: TWideStringField
      FieldName = 'Motivo'
      Size = 70
    end
    object QrLotItsObservacao: TWideStringField
      FieldName = 'Observacao'
      Size = 170
    end
    object QrLotItsObsGerais: TWideStringField
      FieldName = 'ObsGerais'
      Size = 60
    end
    object QrLotItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLotItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLotItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLotItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrLotItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrLotItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrLotItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrLotItsCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrLotItsData4: TDateField
      FieldName = 'Data4'
    end
    object QrLotItsDescAte: TDateField
      FieldName = 'DescAte'
    end
    object QrLotItsTipific: TSmallintField
      FieldName = 'Tipific'
    end
    object QrLotItsStatusSPC: TSmallintField
      FieldName = 'StatusSPC'
    end
    object QrLotItsCNAB_Lot: TIntegerField
      FieldName = 'CNAB_Lot'
    end
  end
  object DsLotIts: TDataSource
    Left = 284
    Top = 60
  end
  object QrLotTxs: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lot.Data, lot.Cliente, txa.PlaGen, txs.*'
      'FROM lot estxs txs'
      'LEFT JOIN lot0001a lot ON lot.Codigo=txs.Codigo'
      'LEFT JOIN taxas txa ON txa.Codigo=txs.TaxaCod'
      'ORDER BY txs.Codigo, txs.Controle')
    Left = 320
    Top = 60
    object QrLotTxsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLotTxsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLotTxsForma: TSmallintField
      FieldName = 'Forma'
    end
    object QrLotTxsTaxaCod: TIntegerField
      FieldName = 'TaxaCod'
    end
    object QrLotTxsTaxaTxa: TFloatField
      FieldName = 'TaxaTxa'
    end
    object QrLotTxsTaxaQtd: TFloatField
      FieldName = 'TaxaQtd'
    end
    object QrLotTxsSysAQtd: TFloatField
      FieldName = 'SysAQtd'
    end
    object QrLotTxsTaxaVal: TFloatField
      FieldName = 'TaxaVal'
    end
    object QrLotTxsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLotTxsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLotTxsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLotTxsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrLotTxsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrLotTxsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrLotTxsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrLotTxsData: TDateField
      FieldName = 'Data'
    end
    object QrLotTxsCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrLotTxsPlaGen: TIntegerField
      FieldName = 'PlaGen'
    end
  end
  object QrOcorP: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ocp.*, ocb.PlaGen,'
      'oco.Lot esIts LOIS, oco.Cliente, oco.Ocorrencia'
      'FROM ocor rpg ocp'
      'LEFT JOIN ocorreu oco ON oco.Codigo=ocp.Ocorreu'
      'LEFT JOIN ocorbank ocb ON ocb.Codigo=oco.Ocorrencia'
      'ORDER BY ocp.Data, ocp.Codigo')
    Left = 352
    Top = 60
    object QrOcorPCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOcorPOcorreu: TIntegerField
      FieldName = 'Ocorreu'
    end
    object QrOcorPData: TDateField
      FieldName = 'Data'
    end
    object QrOcorPJuros: TFloatField
      FieldName = 'Juros'
    end
    object QrOcorPPago: TFloatField
      FieldName = 'Pago'
    end
    object QrOcorPLotePg: TIntegerField
      FieldName = 'LotePg'
    end
    object QrOcorPLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOcorPDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOcorPDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOcorPUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOcorPUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOcorPAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrOcorPAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrOcorPPlaGen: TIntegerField
      FieldName = 'PlaGen'
    end
    object QrOcorPLOIS: TIntegerField
      FieldName = 'LOIS'
    end
    object QrOcorPCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrOcorPOcorrencia: TIntegerField
      FieldName = 'Ocorrencia'
    end
  end
  object QrPCH: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT apc.PlaGen, ali.Cliente, '
      'ali.Alinea1, ali.Alinea2, apg.* '
      'FROM alin pgs apg'
      'LEFT JOIN alinits ali ON ali.Codigo=apg.AlinIts'
      'LEFT JOIN apcd apc ON apc.Codigo=apg.APCD'
      'ORDER BY apg.Codigo, apg.Data')
    Left = 264
    Top = 264
    object QrPCHCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPCHAlinIts: TIntegerField
      FieldName = 'AlinIts'
    end
    object QrPCHData: TDateField
      FieldName = 'Data'
    end
    object QrPCHJuros: TFloatField
      FieldName = 'Juros'
    end
    object QrPCHPago: TFloatField
      FieldName = 'Pago'
    end
    object QrPCHDesco: TFloatField
      FieldName = 'Desco'
    end
    object QrPCHLotePg: TIntegerField
      FieldName = 'LotePg'
    end
    object QrPCHAPCD: TIntegerField
      FieldName = 'APCD'
    end
    object QrPCHDataCh: TDateField
      FieldName = 'DataCh'
    end
    object QrPCHLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPCHDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPCHDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPCHUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPCHUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPCHAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrPCHAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPCHCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrPCHAlinea1: TIntegerField
      FieldName = 'Alinea1'
    end
    object QrPCHAlinea2: TIntegerField
      FieldName = 'Alinea2'
    end
  end
  object QrPDU: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT apg.Lot esIts LOIS, apg.* '
      'FROM adup pgs apg'
      'ORDER BY Controle, Data')
    Left = 264
    Top = 296
    object QrPDUControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPDUData: TDateField
      FieldName = 'Data'
    end
    object QrPDUJuros: TFloatField
      FieldName = 'Juros'
    end
    object QrPDUDesco: TFloatField
      FieldName = 'Desco'
    end
    object QrPDUPago: TFloatField
      FieldName = 'Pago'
    end
    object QrPDULotePg: TIntegerField
      FieldName = 'LotePg'
    end
    object QrPDULk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPDUDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPDUDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPDUUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPDUUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPDUAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrPDUAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPDULOIS: TIntegerField
      FieldName = 'LOIS'
    end
  end
  object QrAIs: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Valor, Taxas, '
      'Multa, JurosV, PgDesc, ValPago,'
      'Alinea1, Alinea2, Cliente '
      'FROM AlinIts'
      'WHERE Codigo IN '
      '     ('
      '     SELECT DISTINCT AlinIts'
      '     FROM alinpgs'
      '     )  '
      'ORDER BY Codigo')
    Left = 208
    Top = 264
    object QrAIsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAIsValor: TFloatField
      FieldName = 'Valor'
    end
    object QrAIsTaxas: TFloatField
      FieldName = 'Taxas'
    end
    object QrAIsMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrAIsJurosV: TFloatField
      FieldName = 'JurosV'
    end
    object QrAIsPgDesc: TFloatField
      FieldName = 'PgDesc'
    end
    object QrAIsValPago: TFloatField
      FieldName = 'ValPago'
    end
    object QrAIsAlinea1: TIntegerField
      FieldName = 'Alinea1'
    end
    object QrAIsAlinea2: TIntegerField
      FieldName = 'Alinea2'
    end
    object QrAIsCliente: TIntegerField
      FieldName = 'Cliente'
    end
  end
  object QrAPs: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, LotePg, Pago, Data, DataCH, '
      'Juros, Desco, AlinIts, APCD,'
      'Lk, DataCad, DataAlt, UserCad, UserAlt, AlterWeb, Ativo'
      'FROM alinpgs'
      'WHERE AlinIts=:P0'
      '')
    Left = 236
    Top = 264
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAPsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAPsLotePg: TIntegerField
      FieldName = 'LotePg'
    end
    object QrAPsPago: TFloatField
      FieldName = 'Pago'
    end
    object QrAPsData: TDateField
      FieldName = 'Data'
    end
    object QrAPsDataCH: TDateField
      FieldName = 'DataCH'
    end
    object QrAPsJuros: TFloatField
      FieldName = 'Juros'
    end
    object QrAPsDesco: TFloatField
      FieldName = 'Desco'
    end
    object QrAPsAlinIts: TIntegerField
      FieldName = 'AlinIts'
    end
    object QrAPsAPCD: TIntegerField
      FieldName = 'APCD'
    end
    object QrAPsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrAPsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrAPsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrAPsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrAPsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrAPsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrAPsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrDIs: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle LctCtrl, Sub LctSub,'
      'FatParcela, Cliente, Credito, Debito,'
      'Vencimento, Duplicata, CartDep'
      'FROM lct0001a'
      'WHERE FatID=301'
      'AND FatParcela IN '
      '     ('
      '     SELECT DISTINCT LotesIts'
      '     FROM aduppgs'
      '     )')
    Left = 208
    Top = 296
    object QrDIsLctCtrl: TIntegerField
      FieldName = 'LctCtrl'
    end
    object QrDIsLctSub: TSmallintField
      FieldName = 'LctSub'
    end
    object QrDIsFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrDIsCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrDIsCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrDIsDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrDIsVencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrDIsDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 13
    end
    object QrDIsCartDep: TIntegerField
      FieldName = 'CartDep'
    end
  end
  object QrDPs: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM aduppgs'
      'WHERE LotesIts=:P0'
      'ORDER BY Data, LotesIts')
    Left = 236
    Top = 296
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrDPsLotesIts: TIntegerField
      FieldName = 'LotesIts'
    end
    object QrDPsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrDPsData: TDateField
      FieldName = 'Data'
    end
    object QrDPsJuros: TFloatField
      FieldName = 'Juros'
    end
    object QrDPsDesco: TFloatField
      FieldName = 'Desco'
    end
    object QrDPsPago: TFloatField
      FieldName = 'Pago'
    end
    object QrDPsLotePg: TIntegerField
      FieldName = 'LotePg'
    end
    object QrDPsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrDPsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrDPsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrDPsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrDPsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrDPsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrDPsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
end
