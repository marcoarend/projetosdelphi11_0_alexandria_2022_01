object FmLot2Web: TFmLot2Web
  Left = 339
  Top = 185
  Caption = 'BDR-GEREN-013 :: Lote(s) da Web'
  ClientHeight = 496
  ClientWidth = 792
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 744
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 696
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 189
        Height = 32
        Caption = 'Lote(s) da Web'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 189
        Height = 32
        Caption = 'Lote(s) da Web'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 189
        Height = 32
        Caption = 'Lote(s) da Web'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 792
    Height = 334
    Align = alClient
    TabOrder = 1
    object DBGrid1: TDBGrid
      Left = 2
      Top = 79
      Width = 788
      Height = 253
      Align = alClient
      DataSource = DmLot2.DsLLC
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDblClick = DBGrid1DblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'Cliente'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECLI'
          Title.Caption = 'Nome'
          Width = 380
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Data'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Lote'
          Title.Caption = 'Lote net'
          Width = 47
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Itens'
          Width = 36
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Total'
          Width = 73
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMETIPO'
          Title.Caption = 'Tipo'
          Width = 100
          Visible = True
        end>
    end
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 788
      Height = 64
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object CgStatus: TdmkCheckGroup
        Left = 0
        Top = 0
        Width = 788
        Height = 64
        Align = alClient
        Caption = ' Mostrar lote(s) web com status: '
        Columns = 3
        Items.Strings = (
          'Baixado'
          'Aceito'
          'Rejeitado')
        TabOrder = 0
        UpdType = utYes
        Value = 0
        OldValor = 0
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 382
    Width = 792
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 788
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 426
    Width = 792
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 646
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 14
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 644
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BitBtn1: TBitBtn
        Tag = 1002
        Left = 136
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Rejeita'
        TabOrder = 1
        OnClick = BitBtn1Click
        NumGlyphs = 2
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        TabOrder = 0
        OnClick = BtOKClick
        NumGlyphs = 2
      end
    end
  end
  object QrLLC: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE ent.Nome'
      
        'END NOMECLI, IF(llc.Tipo=0, "Cheque", "Duplicata") NOMETIPO, llc' +
        '.* '
      'FROM llotecab llc'
      'LEFT JOIN entidades ent ON ent.Codigo=llc.Cliente'
      'WHERE llc.Baixado=2')
    Left = 404
    Top = 156
    object QrLLCCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLLCCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrLLCLote: TIntegerField
      FieldName = 'Lote'
      DisplayFormat = '000000'
    end
    object QrLLCTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrLLCData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLLCTotal: TFloatField
      FieldName = 'Total'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLLCDias: TFloatField
      FieldName = 'Dias'
    end
    object QrLLCItens: TIntegerField
      FieldName = 'Itens'
      DisplayFormat = '00'
    end
    object QrLLCBaixado: TIntegerField
      FieldName = 'Baixado'
    end
    object QrLLCCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrLLCNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrLLCNOMETIPO: TWideStringField
      FieldName = 'NOMETIPO'
      Required = True
      Size = 9
    end
  end
  object QrLLI: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT * FROM lloteits'
      'WHERE Codigo=:P0')
    Left = 404
    Top = 188
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLLICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLLIControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLLIComp: TIntegerField
      FieldName = 'Comp'
    end
    object QrLLIPraca: TIntegerField
      FieldName = 'Praca'
    end
    object QrLLIBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrLLIAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrLLIConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrLLICheque: TIntegerField
      FieldName = 'Cheque'
    end
    object QrLLICPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrLLIEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrLLIBruto: TFloatField
      FieldName = 'Bruto'
    end
    object QrLLIDesco: TFloatField
      FieldName = 'Desco'
    end
    object QrLLIValor: TFloatField
      FieldName = 'Valor'
    end
    object QrLLIEmissao: TDateField
      FieldName = 'Emissao'
    end
    object QrLLIDCompra: TDateField
      FieldName = 'DCompra'
    end
    object QrLLIDDeposito: TDateField
      FieldName = 'DDeposito'
    end
    object QrLLIVencto: TDateField
      FieldName = 'Vencto'
    end
    object QrLLIDias: TIntegerField
      FieldName = 'Dias'
    end
    object QrLLIDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 12
    end
    object QrLLIIE: TWideStringField
      FieldName = 'IE'
      Size = 25
    end
    object QrLLIRua: TWideStringField
      FieldName = 'Rua'
      Size = 30
    end
    object QrLLINumero: TLargeintField
      FieldName = 'Numero'
    end
    object QrLLICompl: TWideStringField
      FieldName = 'Compl'
      Size = 30
    end
    object QrLLIBairro: TWideStringField
      FieldName = 'Bairro'
      Size = 30
    end
    object QrLLICidade: TWideStringField
      FieldName = 'Cidade'
      Size = 25
    end
    object QrLLIUF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrLLICEP: TIntegerField
      FieldName = 'CEP'
    end
    object QrLLITel1: TWideStringField
      FieldName = 'Tel1'
    end
    object QrLLIUser_ID: TIntegerField
      FieldName = 'User_ID'
    end
    object QrLLIPasso: TIntegerField
      FieldName = 'Passo'
    end
  end
end
