object FmGerChqPgDv: TFmGerChqPgDv
  Left = 339
  Top = 185
  Caption = 'CHQ-CNTRL-004 :: Pagamento de Cheque Devolvido'
  ClientHeight = 403
  ClientWidth = 798
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 798
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 750
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 702
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 404
        Height = 32
        Caption = 'Pagamento de Cheque Devolvido'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 404
        Height = 32
        Caption = 'Pagamento de Cheque Devolvido'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 404
        Height = 32
        Caption = 'Pagamento de Cheque Devolvido'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 798
    Height = 241
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 798
      Height = 241
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 798
        Height = 241
        Align = alClient
        TabOrder = 0
        object PainelPgDv: TPanel
          Left = 2
          Top = 15
          Width = 794
          Height = 224
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Panel9: TPanel
            Left = 0
            Top = 0
            Width = 794
            Height = 85
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object Label33: TLabel
              Left = 4
              Top = 4
              Width = 52
              Height = 13
              Caption = 'Data base:'
            end
            object Label34: TLabel
              Left = 120
              Top = 4
              Width = 53
              Height = 13
              Caption = 'Valor base:'
            end
            object Label35: TLabel
              Left = 196
              Top = 4
              Width = 66
              Height = 13
              Caption = '% Tx.jur.base:'
            end
            object Label37: TLabel
              Left = 272
              Top = 4
              Width = 65
              Height = 13
              Caption = '% Jur. per'#237'od:'
            end
            object Label12: TLabel
              Left = 348
              Top = 4
              Width = 26
              Height = 13
              Caption = 'Data:'
            end
            object Label28: TLabel
              Left = 464
              Top = 4
              Width = 59
              Height = 13
              Caption = 'Vencimento:'
            end
            object Label30: TLabel
              Left = 4
              Top = 44
              Width = 37
              Height = 13
              Caption = '$ Juros:'
            end
            object Label32: TLabel
              Left = 196
              Top = 44
              Width = 66
              Height = 13
              Caption = 'Total a pagar:'
            end
            object Label31: TLabel
              Left = 388
              Top = 44
              Width = 69
              Height = 13
              Caption = '$ Val. a pagar:'
            end
            object Label1: TLabel
              Left = 292
              Top = 44
              Width = 58
              Height = 13
              Caption = '$ Desconto:'
            end
            object Label2: TLabel
              Left = 100
              Top = 44
              Width = 62
              Height = 13
              Caption = 'Total devido:'
            end
            object Label9: TLabel
              Left = 484
              Top = 44
              Width = 84
              Height = 13
              Caption = '$ Valor pendente:'
            end
            object TPDataBase2: TDateTimePicker
              Left = 4
              Top = 20
              Width = 112
              Height = 21
              Date = 38698.785142685200000000
              Time = 38698.785142685200000000
              Color = clBtnFace
              Enabled = False
              TabOrder = 0
              TabStop = False
            end
            object EdValorBase2: TdmkEdit
              Left = 120
              Top = 20
              Width = 72
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              Color = clBtnFace
              ReadOnly = True
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              OnChange = EdValorBase2Change
            end
            object EdJurosBase2: TdmkEdit
              Left = 196
              Top = 20
              Width = 72
              Height = 21
              Alignment = taRightJustify
              Color = clWhite
              TabOrder = 2
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 6
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,000000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              OnChange = EdJurosBase2Change
            end
            object EdJurosPeriodo2: TdmkEdit
              Left = 272
              Top = 20
              Width = 72
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              Color = clBtnFace
              ReadOnly = True
              TabOrder = 3
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 6
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,000000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
            end
            object TPData2: TDateTimePicker
              Left = 348
              Top = 20
              Width = 112
              Height = 21
              Date = 38698.785142685200000000
              Time = 38698.785142685200000000
              TabOrder = 4
              OnChange = TPData2Change
            end
            object TPPagto2: TDateTimePicker
              Left = 464
              Top = 20
              Width = 112
              Height = 21
              Date = 38698.785142685200000000
              Time = 38698.785142685200000000
              TabOrder = 5
              OnChange = TPPagto2Change
            end
            object EdJuros2: TdmkEdit
              Left = 4
              Top = 60
              Width = 92
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              Color = clBtnFace
              ReadOnly = True
              TabOrder = 6
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              OnChange = EdJuros2Change
            end
            object EdAPagar2: TdmkEdit
              Left = 196
              Top = 60
              Width = 92
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              Color = clBtnFace
              ReadOnly = True
              TabOrder = 8
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              OnChange = EdAPagar2Change
            end
            object EdValPagar2: TdmkEdit
              Left = 388
              Top = 60
              Width = 92
              Height = 21
              Alignment = taRightJustify
              TabOrder = 10
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              OnChange = EdValPagar2Change
              OnExit = EdValPagar2Exit
            end
            object EdDesco2: TdmkEdit
              Left = 292
              Top = 60
              Width = 92
              Height = 21
              Alignment = taRightJustify
              TabOrder = 9
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              OnChange = EdDesco2Change
            end
            object EdCorrigido2: TdmkEdit
              Left = 100
              Top = 60
              Width = 92
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              Color = clBtnFace
              ReadOnly = True
              TabOrder = 7
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
            end
            object EdPendente2: TdmkEdit
              Left = 484
              Top = 60
              Width = 92
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              Color = clBtnFace
              ReadOnly = True
              TabOrder = 11
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
            end
          end
          object Panel8: TPanel
            Left = 0
            Top = 85
            Width = 794
            Height = 139
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 1
            TabStop = True
            object Label42: TLabel
              Left = 4
              Top = 4
              Width = 34
              Height = 13
              Caption = 'Al'#237'nea:'
            end
            object Label43: TLabel
              Left = 4
              Top = 44
              Width = 143
              Height = 13
              Caption = 'Leitura pela banda magn'#233'tica:'
            end
            object Label44: TLabel
              Left = 256
              Top = 44
              Width = 27
              Height = 13
              Caption = 'Com.:'
            end
            object Label45: TLabel
              Left = 288
              Top = 44
              Width = 22
              Height = 13
              Caption = 'Bco:'
            end
            object Label46: TLabel
              Left = 320
              Top = 44
              Width = 34
              Height = 13
              Caption = 'Ag'#234'nc:'
            end
            object Label47: TLabel
              Left = 360
              Top = 44
              Width = 55
              Height = 13
              Caption = 'Conta corr.:'
            end
            object Label48: TLabel
              Left = 436
              Top = 44
              Width = 45
              Height = 13
              Caption = 'N'#186' cheq.:'
            end
            object Label49: TLabel
              Left = 4
              Top = 84
              Width = 129
              Height = 13
              Caption = 'CGC / CPF [F4 - Pesquisa]:'
            end
            object Label50: TLabel
              Left = 164
              Top = 84
              Width = 44
              Height = 13
              Caption = 'Emitente:'
            end
            object SpeedButton1: TSpeedButton
              Left = 468
              Top = 20
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = SpeedButton1Click
            end
            object EdAPCD: TdmkEditCB
              Left = 4
              Top = 20
              Width = 41
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              DBLookupComboBox = CBAPCD
              IgnoraDBLookupComboBox = False
            end
            object CBAPCD: TdmkDBLookupComboBox
              Left = 48
              Top = 20
              Width = 417
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsAPCD
              TabOrder = 1
              dmkEditCB = EdAPCD
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdBanda2: TdmkEdit
              Left = 4
              Top = 60
              Width = 249
              Height = 21
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              MaxLength = 34
              ParentFont = False
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              OnChange = EdBanda2Change
            end
            object EdRegiaoCompe2: TdmkEdit
              Left = 256
              Top = 60
              Width = 29
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 3
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 3
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              OnChange = EdRegiaoCompe2Change
              OnExit = EdRegiaoCompe2Exit
            end
            object EdBanco2: TdmkEdit
              Left = 288
              Top = 60
              Width = 29
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 4
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 3
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              OnChange = EdBanco2Change
            end
            object EdAgencia2: TdmkEdit
              Left = 320
              Top = 60
              Width = 37
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 5
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 4
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              OnChange = EdAgencia2Change
            end
            object EdConta2: TdmkEdit
              Left = 360
              Top = 60
              Width = 73
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 6
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 10
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0000000000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              OnChange = EdConta2Change
            end
            object EdCheque2: TdmkEdit
              Left = 436
              Top = 60
              Width = 53
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 7
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 6
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '000000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
            end
            object EdCPF_2: TdmkEdit
              Left = 4
              Top = 100
              Width = 157
              Height = 21
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 8
              FormatType = dmktfString
              MskType = fmtCPFJ
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              OnExit = EdCPF_2Exit
              OnKeyDown = EdCPF_2KeyDown
            end
            object EdEmitente2: TdmkEdit
              Left = 164
              Top = 100
              Width = 325
              Height = 21
              CharCase = ecUpperCase
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 9
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
            end
            object EdCMC_7_2: TEdit
              Left = 80
              Top = 56
              Width = 121
              Height = 21
              MaxLength = 30
              TabOrder = 11
              Visible = False
              OnChange = EdCMC_7_2Change
            end
            object EdRealCC: TdmkEdit
              Left = 360
              Top = 80
              Width = 73
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 12
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 10
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0000000000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
            end
            object GroupBox2: TGroupBox
              Left = 497
              Top = 0
              Width = 297
              Height = 139
              Align = alRight
              TabOrder = 10
              object Label51: TLabel
                Left = 4
                Top = 26
                Width = 45
                Height = 13
                Caption = 'Cheques:'
              end
              object Label52: TLabel
                Left = 4
                Top = 48
                Width = 53
                Height = 13
                Caption = 'Duplicatas:'
              end
              object Label53: TLabel
                Left = 60
                Top = 8
                Width = 30
                Height = 13
                Caption = 'Risco:'
              end
              object Label96: TLabel
                Left = 137
                Top = 8
                Width = 42
                Height = 13
                Caption = 'Vencido:'
              end
              object Label98: TLabel
                Left = 214
                Top = 8
                Width = 56
                Height = 13
                Caption = 'Sub-total:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object Label99: TLabel
                Left = 4
                Top = 74
                Width = 56
                Height = 13
                Caption = 'Sub-total:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object Label91: TLabel
                Left = 4
                Top = 116
                Width = 50
                Height = 13
                Caption = 'Ocorr'#234'nc.:'
              end
              object Label54: TLabel
                Left = 214
                Top = 96
                Width = 45
                Height = 13
                Caption = 'TOTAL:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object EdDR: TdmkEdit
                Left = 60
                Top = 44
                Width = 76
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 3
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
              end
              object EdDT: TdmkEdit
                Left = 214
                Top = 44
                Width = 76
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 5
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
              end
              object EdRT: TdmkEdit
                Left = 60
                Top = 70
                Width = 76
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 6
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
              end
              object EdVT: TdmkEdit
                Left = 137
                Top = 70
                Width = 80
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 7
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
              end
              object EdST: TdmkEdit
                Left = 214
                Top = 70
                Width = 76
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 8
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
              end
              object EdCT: TdmkEdit
                Left = 214
                Top = 22
                Width = 76
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 2
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
              end
              object EdDV: TdmkEdit
                Left = 137
                Top = 44
                Width = 76
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 4
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
              end
              object EdCR: TdmkEdit
                Left = 60
                Top = 22
                Width = 76
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 0
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
              end
              object EdCV: TdmkEdit
                Left = 137
                Top = 22
                Width = 76
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 1
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
              end
              object EdOA: TdmkEdit
                Left = 60
                Top = 112
                Width = 76
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 9
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
              end
              object EdTT: TdmkEdit
                Left = 214
                Top = 112
                Width = 76
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 10
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
              end
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 289
    Width = 798
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 794
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 333
    Width = 798
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 652
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 650
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrAPCD: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Deposita, ExigeCMC7 '
      'FROM apcd'
      'ORDER BY Nome')
    Left = 612
    Top = 12
    object QrAPCDCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAPCDNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrAPCDDeposita: TSmallintField
      FieldName = 'Deposita'
    end
    object QrAPCDExigeCMC7: TSmallintField
      FieldName = 'ExigeCMC7'
    end
  end
  object DsAPCD: TDataSource
    DataSet = QrAPCD
    Left = 640
    Top = 12
  end
  object QrBanco2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nome, DVCC '
      'FROM bancos'
      'WHERE Codigo=:P0')
    Left = 552
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrBanco2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrBanco2DVCC: TSmallintField
      FieldName = 'DVCC'
    end
  end
  object DsBanco2: TDataSource
    DataSet = QrBanco2
    Left = 580
    Top = 12
  end
  object QrSCB: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SCB'
      'FROM entidades'
      'WHERE Codigo=:P0')
    Left = 672
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSCBSCB: TIntegerField
      FieldName = 'SCB'
    end
  end
  object QrCli: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM lot es'
      'WHERE Codigo=:P0')
    Left = 704
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrLocPg: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      '/*'
      'SELECT * FROM alin pgs'
      'WHERE Data=('
      'SELECT Max(Data) FROM alin pgs'
      'WHERE AlinIts=:P0)'
      'ORDER BY Codigo DESC'
      '*/'
      'SELECT Data, FatParcela FROM lct0001a'
      'WHERE FatID=0305'
      'AND Data=( '
      'SELECT Max(Data) FROM lct0001a'
      'WHERE FatID=0305'
      'AND FatParcRef=:P0) '
      'ORDER BY FatParcela DESC '
      '')
    Left = 736
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocPgData: TDateField
      FieldName = 'Data'
    end
    object QrLocPgFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
  end
end
