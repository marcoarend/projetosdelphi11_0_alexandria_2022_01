object FmGruSacEmiIts: TFmGruSacEmiIts
  Left = 339
  Top = 185
  Caption = 'EMI-TENTE-006 ::  Adi'#231#227'o de Sacado / Emitente ao Grupo'
  ClientHeight = 249
  ClientWidth = 647
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 647
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 599
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 551
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 473
        Height = 32
        Caption = 'Adi'#231#227'o de Sacado / Emitente ao Grupo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 473
        Height = 32
        Caption = 'Adi'#231#227'o de Sacado / Emitente ao Grupo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 473
        Height = 32
        Caption = 'Adi'#231#227'o de Sacado / Emitente ao Grupo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 647
    Height = 87
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 647
      Height = 87
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 647
        Height = 87
        Align = alClient
        TabOrder = 0
        object Label3: TLabel
          Left = 12
          Top = 16
          Width = 164
          Height = 13
          Caption = 'CPF / CNPJ Emitente [F8 localiza]:'
        end
        object EdCPF1: TdmkEdit
          Left = 12
          Top = 32
          Width = 116
          Height = 21
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtCPFJ
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          OnChange = EdCPF1Change
          OnExit = EdCPF1Exit
        end
        object DBEdEmitente: TDBEdit
          Left = 145
          Top = 8
          Width = 489
          Height = 21
          TabStop = False
          DataField = 'Nome'
          DataSource = DsEmiSac
          TabOrder = 1
          Visible = False
          OnChange = DBEdEmitenteChange
        end
        object EdNomeEmiSac: TdmkEdit
          Left = 128
          Top = 32
          Width = 506
          Height = 21
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
        object CkContinuar: TCheckBox
          Left = 16
          Top = 60
          Width = 213
          Height = 17
          Caption = 'Continuar inserindo ap'#243's a confirma'#231#227'o.'
          Checked = True
          State = cbChecked
          TabOrder = 3
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 135
    Width = 647
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 643
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 179
    Width = 647
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 501
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 499
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrEmiSac: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT Nome, CPF, "Emitente" Tipo FROM emitcpf'
      'WHERE CPF = :P0'
      'UNION'
      'SELECT DISTINCT Nome, CNPJ CPF, "Sacado" Tipo FROM sacados'
      'WHERE CNPJ= :P1'
      ''
      'ORDER BY Nome')
    Left = 236
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrEmiSacNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrEmiSacCPF: TWideStringField
      FieldName = 'CPF'
      Required = True
      Size = 15
    end
    object QrEmiSacTipo: TWideStringField
      FieldName = 'Tipo'
      Required = True
      Size = 8
    end
  end
  object DsEmiSac: TDataSource
    DataSet = QrEmiSac
    Left = 264
    Top = 64
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CNPJ_CPF'
      'FROM grusacemiits'
      'WHERE CNPJ_CPF=:P0'
      'AND Codigo=:P1')
    Left = 297
    Top = 65
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLocCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 15
    end
  end
end
