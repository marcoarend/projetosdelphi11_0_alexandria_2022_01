unit Deposito;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, Db, mySQLDbTables,
  ComCtrls, Mask, DBCtrls, Menus, dmkEdit, dmkImage, dmkGeral, UnDmkProcFunc,
  DmkDAC_PF, UnDmkEnums;

type
  TFmDeposito = class(TForm)
    Panel1: TPanel;
    QrCheques: TmySQLQuery;
    DsCheques: TDataSource;
    DBG1: TDBGrid;
    Panel4: TPanel;
    Panel5: TPanel;
    Panel7: TPanel;
    CkDescende1: TCheckBox;
    CkDescende2: TCheckBox;
    CkDescende3: TCheckBox;
    Panel8: TPanel;
    Label24: TLabel;
    EdSoma: TdmkEdit;
    Panel6: TPanel;
    RGOrdem3: TRadioGroup;
    RGOrdem2: TRadioGroup;
    RGOrdem1: TRadioGroup;
    Panel3: TPanel;
    TPVencto: TDateTimePicker;
    Label1: TLabel;
    Timer1: TTimer;
    QrSoma: TmySQLQuery;
    DsSoma: TDataSource;
    QrSomaVALOR: TFloatField;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    PMDeposito: TPopupMenu;
    Itemselecionado1: TMenuItem;
    Determinarperodo1: TMenuItem;
    QrMin: TmySQLQuery;
    QrMinVencto: TDateField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel9: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel10: TPanel;
    BtRefresh: TBitBtn;
    BtDeposito: TBitBtn;
    QrChequesNOMECLI: TWideStringField;
    QrChequesLote: TIntegerField;
    QrChequesFatNum: TFloatField;
    QrChequesFatParcela: TIntegerField;
    QrChequesData: TDateField;
    QrChequesVencimento: TDateField;
    QrChequesDCompra: TDateField;
    QrChequesDDeposito: TDateField;
    QrChequesEmitente: TWideStringField;
    QrChequesCNPJCPF: TWideStringField;
    QrChequesCredito: TFloatField;
    QrChequesDocumento: TFloatField;
    QrChequesBanco: TIntegerField;
    QrChequesContaCorrente: TWideStringField;
    QrChequesAgencia: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrChequesAfterOpen(DataSet: TDataSet);
    procedure QrChequesAfterClose(DataSet: TDataSet);
    procedure BtRefreshClick(Sender: TObject);
    procedure TPVenctoChange(Sender: TObject);
    procedure RGOrdem1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure DBG1CellClick(Column: TColumn);
    procedure DBG1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtDepositoClick(Sender: TObject);
    procedure Itemselecionado1Click(Sender: TObject);
    procedure Determinarperodo1Click(Sender: TObject);
  private
    { Private declarations }
    FAscendente: Boolean;
    procedure ReopenCheques(Controle: Integer);
    procedure FechaQrCheques;
    procedure SomaLinhas(Ascendente: Boolean);
    procedure PreparaSoma(Ascendente: Boolean);
    procedure DepositaSelecionados;
    procedure DepositaAtual;
    //procedure Deposita;
  public
    { Public declarations }
  end;

  var
  FmDeposito: TFmDeposito;

implementation

uses UnMyObjects, Module, DataDef, UnInternalConsts, UMySQLModule, MyListas,
  TedC_Aux;

{$R *.DFM}

procedure TFmDeposito.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmDeposito.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmDeposito.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmDeposito.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  //
  TPVencto.Date := Date;
  ReopenCheques(0);
end;

procedure TFmDeposito.QrChequesAfterOpen(DataSet: TDataSet);
begin
  BtRefresh.Enabled := False;
end;

procedure TFmDeposito.QrChequesAfterClose(DataSet: TDataSet);
begin
  BtRefresh.Enabled := True;
end;

procedure TFmDeposito.BtRefreshClick(Sender: TObject);
begin
  if QrCheques.State = dsBrowse then
    ReopenCheques(QrChequesFatParcela.Value)
  else
    ReopenCheques(0);
end;

procedure TFmDeposito.TPVenctoChange(Sender: TObject);
begin
  FechaQrCheques;
end;

procedure TFmDeposito.RGOrdem1Click(Sender: TObject);
begin
  FechaQrCheques;
end;

procedure TFmDeposito.FechaQrCheques;
begin
  QrCheques.Close;
  QrSoma.Close;
end;

procedure TFmDeposito.SomaLinhas(Ascendente: Boolean);
var
  Valor: Double;
  i, k: Integer;
begin
  k := QrChequesFatParcela.Value;
  if DBG1.SelectedRows.Count = 0 then Valor := QrChequesCredito.Value else
  begin
    Valor := 0;
    with DBG1.DataSource.DataSet do
    begin
      if Ascendente then
      begin
        for i:= 0 to DBG1.SelectedRows.Count-1 do
        begin
          //GotoBookmark(pointer(DBG1.SelectedRows.Items[i]));
          GotoBookmark(DBG1.SelectedRows.Items[i]);
          Valor := Valor + QrChequesCredito.Value;
        end;
      end else begin
        for i:= DBG1.SelectedRows.Count-1 downto 0 do
        begin
          //GotoBookmark(pointer(DBG1.SelectedRows.Items[i]));
          GotoBookmark(DBG1.SelectedRows.Items[i]);
          Valor := Valor + QrChequesCredito.Value;
        end;
      end;
    end;
    QrCheques.Locate('Controle', k, []);
  end;
  EdSoma.Text := Geral.FFT(Valor, 2, siNegativo);
end;

procedure TFmDeposito.Timer1Timer(Sender: TObject);
begin
  SomaLinhas(FAscendente);
end;

procedure TFmDeposito.DBG1CellClick(Column: TColumn);
begin
  SomaLinhas(True);
end;

procedure TFmDeposito.DBG1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_DOWN) and (Shift=[ssShift]) then PreparaSoma(True)  else
  if (Key=VK_UP)   and (Shift=[ssShift]) then PreparaSoma(False) else
  if (Key=VK_DOWN)                       then PreparaSoma(True)  else
  if (Key=VK_UP)                         then PreparaSoma(False);
end;

procedure TFmDeposito.PreparaSoma(Ascendente: Boolean);
begin
  FAscendente := Ascendente;
  if Timer1.Enabled then Timer1.Enabled := False;
  Timer1.Enabled := True;
end;

procedure TFmDeposito.ReopenCheques(Controle: Integer);
const
  NomeA: array[0..5] of string = ('DDeposito','Credito','Documento',
    'Banco , Agencia , ContaCorrente','Emitente','NOMECLI');
  NomeD: array[0..5] of string = ('DDeposito DESC','Documento DESC',
    'Credito DESC','Banco DESC, Agencia DESC, ContaCorrente DESC',
    'Emitente','NOMECLI');
var
  Ordem: String;
begin
  Ordem := 'ORDER BY ';
  if CkDescende1.Checked then
    Ordem := Ordem + NomeD[RGOrdem1.ItemIndex]+', '
  else
    Ordem := Ordem + NomeA[RGOrdem1.ItemIndex]+', ';
  if CkDescende2.Checked then
    Ordem := Ordem + NomeD[RGOrdem2.ItemIndex]+', '
  else
    Ordem := Ordem + NomeA[RGOrdem2.ItemIndex]+', ';
  if CkDescende3.Checked then
    Ordem := Ordem + NomeD[RGOrdem3.ItemIndex]
  else
    Ordem := Ordem + NomeA[RGOrdem3.ItemIndex];
  //
{
  QrSoma.Close;
  QrCheques.Close;
  QrCheques.SQL.Clear;
  QrCheques.SQL.Add('SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
  QrCheques.SQL.Add('ELSE ent.Nome END NOMECLI, lot.Lote, loi.*');
  QrCheques.SQL.Add('FROM lot esits loi');
  QrCheques.SQL.Add('LEFT JOIN lot es lot ON lot.Codigo=loi.Codigo');
  QrCheques.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=lot.Cliente');
  QrCheques.SQL.Add('WHERE lot.Tipo = 0');
  QrCheques.SQL.Add('AND loi.RepCli = 0');
  QrCheques.SQL.Add('AND loi.Repassado = 0');
  QrCheques.SQL.Add('AND loi.Devolucao = 0');
  QrCheques.SQL.Add('AND loi.Depositado = 0');
  QrCheques.SQL.Add('');
  QrCheques.SQL.Add(dmkPF.SQL_Periodo('AND loi.Vencto ', 0, Date, False, True));
  //
  QrCheques.SQL.Add(Ordem);
  QrCheques. Open;
}

  //  Aqui ajuda a definir o movimento do cheque?

  UnDmkDAC_PF.AbreMySQLQuery0(QrCheques, Dmod.MyDB, [
  'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ',
  'ent.Nome) NOMECLI, lot.Lote, loi.*',
  'FROM ' + CO_TabLctA + ' loi',
  'LEFT JOIN ' + CO_TabLotA + ' lot ON lot.Codigo=loi.FatNum',
  'LEFT JOIN entidades ent ON ent.Codigo=lot.Cliente',
  'WHERE loi.FatID=' + FormatFloat('0', VAR_FATID_0301),
  'AND lot.Tipo = 0',
  'AND loi.RepCli = 0',
  'AND loi.Repassado = 0',
  'AND loi.Devolucao = 0',
  'AND loi.Depositado = ' + Geral.FF0(CO_CH_DEPOSITADO_NAO),
  dmkPF.SQL_Periodo('AND loi.Vencimento ', 0, Date, False, True),
  Ordem,
  '']);
  //
{
  QrSoma.SQL.Clear;
  QrSoma.SQL.Add('SELECT SUM(Valor) Valor');
  QrSoma.SQL.Add('FROM lot esits loi');
  QrSoma.SQL.Add('LEFT JOIN lot es lot ON lot.Codigo=loi.Codigo');
  QrSoma.SQL.Add('WHERE lot.Tipo = 0');
  QrSoma.SQL.Add('AND loi.RepCli = 0');
  QrSoma.SQL.Add('AND loi.Repassado = 0');
  QrSoma.SQL.Add('AND loi.Devolucao = 0');
  QrSoma.SQL.Add('AND loi.Depositado = 0');
  QrSoma.SQL.Add('');
  QrSoma.SQL.Add(dmkPF.SQL_Periodo('AND loi.Vencto ', 0, Date, False, True));
  QrSoma. Open;
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrSoma, Dmod.MyDB, [
  'SELECT SUM(Credito) Valor',
  'FROM ' + CO_TabLctA + ' loi',
  'LEFT JOIN ' + CO_TabLotA + ' lot ON lot.Codigo=loi.FatNum',
  'WHERE loi.FatID=' + FormatFloat('0', VAR_FATID_0301),
  'AND lot.Tipo = 0',
  'AND loi.RepCli = 0',
  'AND loi.Repassado = 0',
  'AND loi.Devolucao = 0',
  'AND loi.Depositado = ' + Geral.FF0(CO_CH_DEPOSITADO_NAO),
  dmkPF.SQL_Periodo('AND loi.Vencimento ', 0, Date, False, True),
  '']);
end;

procedure TFmDeposito.BtDepositoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMDeposito, BtDeposito);
end;

procedure TFmDeposito.Itemselecionado1Click(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  if DBG1.SelectedRows.Count = 0 then
    DepositaAtual
  else
    DepositaSelecionados;
  ReopenCheques(0);
  Screen.Cursor := crDefault;
end;

procedure TFmDeposito.DepositaSelecionados;
var
  i : Integer;
begin
  with DBG1.DataSource.DataSet do
  for i:= 0 to DBG1.SelectedRows.Count-1 do
  begin
    //GotoBookmark(pointer(DBG1.SelectedRows.Items[i]));
    GotoBookmark(DBG1.SelectedRows.Items[i]);
    DepositaAtual;
  end;
end;

procedure TFmDeposito.DepositaAtual;
const
  Depositado = CO_CH_DEPOSITADO_MANUAL;
begin
{
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('UPDATE lot esits SET AlterWeb=1,   Depositado=1 WHERE Controle=:P0 ');
  Dmod.QrUpdM.Params[0].AsInteger := QrChequesFatParcela.Value;
  //
  Dmod.QrUpdM.ExecSQL;
}
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_TabLctA, False, [
  'Depositado'], ['FatID', 'FatParcela'], [
  Depositado], [VAR_FATID_0301, QrChequesFatParcela.Value], True);
end;

procedure TFmDeposito.Determinarperodo1Click(Sender: TObject);
const
  Depositado1 = CO_CH_DEPOSITADO_MANUAL;
  TpAlin = 0;
  RepCli = 0;
  Repassado = 0;
  Devolucao = 0;
  Depositado0 = CO_CH_DEPOSITADO_NAO;
begin
{
  QrMin.Close;
  QrMin. Open;
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrMin, Dmod.MyDB, [
  'SELECT MIN(loi.Vencimento) Vencto ',
  'FROM ' + CO_TabLctA + ' loi ',
  'LEFT JOIN ' + CO_TabLotA + ' lot ON lot.Codigo=loi.FatNum ',
  'WHERE loi.FatID=' + FormatFloat('0', VAR_FATID_0301),
  'AND lot.Tipo=0 ',
  'AND loi.RepCli = 0 ',
  'AND loi.Repassado = 0 ',
  'AND loi.Devolucao = 0 ',
  'AND loi.Depositado = ' + Geral.FF0(CO_CH_DEPOSITADO_NAO),
  '']);
  //
  MLAGeral.EscolhePeriodo(TFmDataDef, FmDataDef, QrMinVencto.Value, Date-2);
  if VAR_DATA_FIN_ESCOLHIDA > 0 then
  begin
{
    Dmod.QrUpdM.SQL.Clear;
    Dmod.QrUpdM.SQL.Add('UPDATE lot esits SET AlterWeb=1,  loi, Lot es lot');
    Dmod.QrUpdM.SQL.Add('SET loi.Depositado = 1');
    Dmod.QrUpdM.SQL.Add('WHERE lot.Codigo=loi.Codigo');
    Dmod.QrUpdM.SQL.Add('AND lot.Tipo=0');
    Dmod.QrUpdM.SQL.Add('AND loi.RepCli = 0');
    Dmod.QrUpdM.SQL.Add('AND loi.Repassado = 0');
    Dmod.QrUpdM.SQL.Add('AND loi.Devolucao = 0');
    Dmod.QrUpdM.SQL.Add('AND loi.Depositado = 0');
    Dmod.QrUpdM.SQL.Add(dmkPF.SQL_Periodo('AND loi.Vencto ',
      VAR_DATA_INI_ESCOLHIDA, VAR_DATA_FIN_ESCOLHIDA, True, True));
    Dmod.QrUpdM.ExecSQL;
}
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_TabLctA, False, [
    'Depositado'], ['FatID', 'FatNum',
    'TpAlin', 'RepCli', 'Repassado',
    'Devolucao', 'Depositado', CO_JOKE_SQL], [
    Depositado1], [VAR_FATID_0301, QrChequesFatNum.Value,
    TpAlin, RepCli, Repassado,
    Devolucao, Depositado0, dmkPF.SQL_Periodo('Vencimento ',
      VAR_DATA_INI_ESCOLHIDA, VAR_DATA_FIN_ESCOLHIDA, True, True)
    ], True);
    //
    VAR_DATA_INI_ESCOLHIDA := 0;
    VAR_DATA_FIN_ESCOLHIDA := 0;
    //
    ReopenCheques(0);
  end;
end;

end.

