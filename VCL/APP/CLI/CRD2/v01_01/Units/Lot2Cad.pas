unit Lot2Cad;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Mask, MyListas, DmkDAC_PF, UnDmkEnums, UnDmkProcFunc;

type
  TFmLot2Cad = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtConfirma: TBitBtn;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    Panel24: TPanel;
    Label9: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    LaAdValorem: TLabel;
    Label34: TLabel;
    Label133: TLabel;
    Label154: TLabel;
    Label203: TLabel;
    EdCodigo: TdmkEdit;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    EdLote: TdmkEdit;
    RGTipo: TRadioGroup;
    EdAdValorem: TdmkEdit;
    TPData: TdmkEditDateTimePicker;
    EdNF: TdmkEdit;
    EdCBE: TdmkEdit;
    CkSCB: TCheckBox;
    EdCPMF: TdmkEdit;
    Panel25: TPanel;
    PainelBco0: TPanel;
    Label126: TLabel;
    Label127: TLabel;
    Label128: TLabel;
    EdBanco0: TdmkEdit;
    EdAgencia0: TdmkEdit;
    DBEdit15: TDBEdit;
    Panel38: TPanel;
    Label101: TLabel;
    Label97: TLabel;
    Label100: TLabel;
    Label28: TLabel;
    Label27: TLabel;
    Label26: TLabel;
    LaCartDep1: TLabel;
    EdCOFINS_R: TdmkEdit;
    EdCOFINS: TdmkEdit;
    EdPIS_R: TdmkEdit;
    EdPIS: TdmkEdit;
    EdIRRF: TdmkEdit;
    EdISS: TdmkEdit;
    EdCartDep1: TdmkEditCB;
    CBCartDep1: TdmkDBLookupComboBox;
    PainelPdxAdVal: TPanel;
    GradeAV: TStringGrid;
    PainelItens: TPanel;
    GradeX1: TStringGrid;
    Panel18: TPanel;
    Label80: TLabel;
    Label81: TLabel;
    EdDMaisX: TdmkEdit;
    EdTxaCompraX: TdmkEdit;
    ProgressBar4: TProgressBar;
    SpeedButton5: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure EdLoteKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure RGTipoClick(Sender: TObject);
    procedure TPDataChange(Sender: TObject);
    procedure EdNFKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure EdBanco0Change(Sender: TObject);
    procedure EdBanco0Exit(Sender: TObject);
    procedure GradeAVDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeAVKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure GradeX1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure EdNFDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
  private
    { Private declarations }
    FCasasAdValorem: Integer;
    //
    procedure CalculaValoresChequeImportando(Dias: Integer);
    procedure CalculaValoresDuplicataImportando(Dias: Integer);
    procedure ConfiguraTaxas();
    procedure DefineNovaNF();
    procedure DefineNovoLote();
    procedure DefineAdValoremEmpresas();
    procedure ImportaCheque(FatNum, Cliente, CBE: Integer);
    procedure ImportaDuplicata(Codigo, Cliente: Integer; DataTransacao:
              TDateTime);
    procedure LancaTaxasNovoLote(Cliente, TipoLote, NumeroDoLote: Integer);
    function  LoteExiste(Cliente, Codigo, Lote: Integer): Boolean;
    procedure ReopenBanco0();
    procedure VerificaNF(NF: Integer);

  public
    { Public declarations }
    FTipoBordero: TLoteBordero;
    FAlteraTodos: Boolean;
  end;

  var
  FmLot2Cad: TFmLot2Cad;

implementation

uses UnMyObjects, Module, ModuleLot, Principal, Lot2Cab, UMySQLModule,
  ModuleLot2, MyDBCheck, ModuleGeral;

{$R *.DFM}

procedure TFmLot2Cad.BtConfirmaClick(Sender: TObject);
const
  LctsAuto = 0;
var
  Cliente, Tipo, Lote: Integer;
  AdValorem: Double;
  DCompra, Data: String;
  TipoAdV, Spread: Integer;
  IRRF, PIS_R, ISS, Cofins_R, PIS, COFINS: Double;
  NF, Conferido, CBE, SCB: Integer;
  CPMF, IOFd, IOFv: Double;
  TipoIOF, WebCod, Codigo, FatNum: Integer;
  //
  DMais, Idx: Integer;
  TxaCompra: Double;
  OldFile, NewFile: String;
begin
  Screen.Cursor := crHourGlass;
  try
    NF     := EdNF.ValueVariant;
    Codigo := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, CO_TabLotA, 'Codigo', [], [],
      ImgTipo.SQLType, EdCodigo.ValueVariant, siPositivo, EdCodigo);
    //
    //Verifica n�mero NF
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT Codigo ',
    'FROM lot0001a ',
    'WHERE NF =' + Geral.FF0(NF),
    'AND NF <> 0    ',
    'AND Codigo <>' + Geral.FF0(Codigo),
    '']);
    if Dmod.QrAux.RecordCount > 0 then
    begin
      Geral.MensagemBox('Est� nota j� foi lan�ada no border� ID n�mero ' +
        Geral.FF0(Dmod.QrAux.FieldByName('Codigo').AsInteger), 'Aviso',
        MB_OK+MB_ICONWARNING);
      Screen.Cursor := crDefault;  
      Exit;
    end;   
    //
    Lote := EdLote.ValueVariant;
    Cliente := EdCliente.ValueVariant;
    if Cliente = 0 then
    begin
      Geral.MensagemBox('Defina um cliente.', 'Aviso', MB_OK+MB_ICONWARNING);
      Screen.Cursor := crDefault;
      Exit;
    end else begin
      if ImgTipo.SQLType = stUpd then
      begin
        {
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('UPDATE lot esits SET AlterWeb=1, Cliente=:P0 WHERE Codigo=:Pa');
        Dmod.QrUpd.Params[0].AsInteger := Cliente;
        Dmod.QrUpd.Params[1].AsInteger := Codigo;
        Dmod.QrUpd.ExecSQL;
        }
        //
        // Mudar o cliente ( e a carteira de dep�sito? -> CartDep )
        // A data n�o pode pois pode ser diferente! (Duplicata por exemplo!)
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_TabLctA, False, [
        'Cliente'], [CO_JOKE_SQL, 'FatNum'], [
        Cliente], [
        'FatID BETWEEN ' + TXT_VAR_FATID_MIN + ' AND ' + TXT_VAR_FATID_MAX,
        Codigo], True);
      end;
    end;
    FmPrincipal.ObtemTipoEPercDeIOF2008(DmLot.QrClientesTipo.Value, DmLot.QrClientesSimples.Value,
      0, IOFd, TipoIOF);
    IOFd := IOFd / 365; // o IOF � por dia
    AdValorem := Geral.DMV(EdAdValorem.Text);
    Data := FormatDateTime(VAR_FORMATDATE, TPData.Date);
    if LoteExiste(Cliente, Codigo, Lote) then Exit;
    //**Dmod.QrUpdU.SQL.Clear;
    if ImgTipo.SQLType = stIns then
    begin
      //**Dmod.QrUpdU.SQL.Add('INSERT INTO lot es SET ');
      case FTipoBordero of
        loteCheque: Tipo := 0;
        loteDuplicata: Tipo := 1;
        else Tipo := -1;
      end;
    end else
    begin
      //**Dmod.QrUpdU.SQL.Add('UPDATE lot es SET AlterWeb=1,   ');
      Tipo := FmLot2Cab.QrLotTipo.Value;
      FmLot2Cab.FConferido := FmLot2Cab.QrLotConferido.Value;
    end;
    //**Dmod.QrUpdU.SQL.Add('Cliente=:P0, Tipo=:P1, Lote=:P2, AdValorem=:P3,');
    //**Dmod.QrUpdU.SQL.Add('Data=:P4, TipoAdV=:P5, Spread=:P6, IRRF=:P7, ');
    //**Dmod.QrUpdU.SQL.Add('PIS_R=:P8, ISS=:P9, Cofins_R=:P10, PIS=:P11, ');
    //**Dmod.QrUpdU.SQL.Add('COFINS=:P12, NF=:P13, Conferido=:P14, CBE=:P15, ');
    //**Dmod.QrUpdU.SQL.Add('SCB=:P16, CPMF=:P17, IOFd=:P18, IOFv=:P19, ');
    //**Dmod.QrUpdU.SQL.Add('TipoIOF=:P20, ');
    //
    CBE := EdCBE.ValueVariant;
    SCB := MLAGeral.BoolToInt(CkSCB.Checked);
    if (NF=0) and (FmPrincipal.FConnections=0) and
    (FmPrincipal.FMyDBs.MaxDBs > 0) then
    begin
      Geral.MensagemBox('Erro em A��o!', 'Erro', MB_OK+MB_ICONERROR);
      Exit;
    end;


    (***
    if ImgTipo.SQLType = stIns then
    begin
      Dmod.QrUpdU.SQL.Add('DataCad=:Pa, UserCad=:Pb, Codigo=:Pc');
      Dmod.QrUpdU.SQL.Add(', WebCod=:Pd');
    end else Dmod.QrUpdU.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
    Dmod.QrUpdU.Params[00].AsInteger := Cliente;
    Dmod.QrUpdU.Params[01].AsInteger := Tipo;
    Dmod.QrUpdU.Params[02].AsInteger := Lote;
    Dmod.QrUpdU.Params[03].AsFloat   := AdValorem;
    Dmod.QrUpdU.Params[04].AsString  := Data;
    Dmod.QrUpdU.Params[05].AsInteger := Dmod.QrControleTipoAdValorem.Value;
    Dmod.QrUpdU.Params[06].AsInteger := RGTipo.ItemIndex;
    Dmod.QrUpdU.Params[07].AsFloat   := Geral.DMV(EdIRRF.Text);
    Dmod.QrUpdU.Params[08].AsFloat   := Geral.DMV(EdPIS_R.Text);
    Dmod.QrUpdU.Params[09].AsFloat   := Geral.DMV(EdISS.Text);
    Dmod.QrUpdU.Params[10].AsFloat   := Geral.DMV(EdCOFINS_R.Text);
    Dmod.QrUpdU.Params[11].AsFloat   := Geral.DMV(EdPIS.Text);
    Dmod.QrUpdU.Params[12].AsFloat   := Geral.DMV(EdCOFINS.Text);
    Dmod.QrUpdU.Params[13].AsInteger := NF;
    Dmod.QrUpdU.Params[14].AsInteger := FmLot2Cab.FConferido;
    Dmod.QrUpdU.Params[15].AsInteger := CBE;
    Dmod.QrUpdU.Params[16].AsInteger := SCB;
    Dmod.QrUpdU.Params[17].AsFloat   := Geral.DMV(EdCPMF.Text);
    Dmod.QrUpdU.Params[18].AsFloat   := IOFd;
    Dmod.QrUpdU.Params[19].AsFloat   := Dmod.QrControleIOF_Ex.Value;
    Dmod.QrUpdU.Params[20].AsInteger := TipoIOF;
    //
    Dmod.QrUpdU.Params[21].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
    Dmod.QrUpdU.Params[22].AsInteger := VAR_USUARIO;
    Dmod.QrUpdU.Params[23].AsInteger := Codigo;
    if ImgTipo.SQLType = stIns then
    begin
      Dmod.QrUpdU.Params[24].AsInteger := FmPrincipal.FloteWebDone;
      if FmPrincipal.FloteWebDone > 0 then
      begin
        Dmod.QrUpdM.SQL.Clear;
        Dmod.QrUpdM.SQL.Add('UPDATE llotecab SET Baixado=3 WHERE Codigo=:P0');
        Dmod.QrUpdM.Params[0].AsInteger := FmPrincipal.FloteWebDone;
        Dmod.QrUpdM.ExecSQL;
      end;
    end;
    Dmod.QrUpdU.ExecSQL;
    *)

    TipoAdV    := Dmod.QrControleTipoAdValorem.Value;
    Spread     := RGTipo.ItemIndex;
    IRRF       := Geral.DMV(EdIRRF.Text);
    PIS_R      := Geral.DMV(EdPIS_R.Text);
    ISS        := Geral.DMV(EdISS.Text);
    Cofins_R   := Geral.DMV(EdCOFINS_R.Text);
    PIS        := Geral.DMV(EdPIS.Text);
    COFINS     := Geral.DMV(EdCOFINS.Text);
    Conferido  := FmLot2Cab.FConferido;
    CPMF       := Geral.DMV(EdCPMF.Text);
    IOFv       := Dmod.QrControleIOF_Ex.Value;
    if ImgTipo.SQLType = stIns then
    begin
      WebCod := FmPrincipal.FloteWebDone;
      //
      Dmod.QrUpdM.SQL.Clear;
      Dmod.QrUpdM.SQL.Add('UPDATE llotecab SET Baixado=3 WHERE Codigo=:P0');
      Dmod.QrUpdM.Params[0].AsInteger := FmPrincipal.FloteWebDone;
      Dmod.QrUpdM.ExecSQL;
    end else
      WebCod := FmLot2Cab.QrLotWebCod.Value;


    if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, CO_TabLotA, False, [
    'Tipo', 'Spread', 'Cliente',
    'CBE', 'SCB', 'Lote',
    'Data', (*'Total', 'Dias',
    'PeCompra', 'TxCompra', 'ValValorem',
    'MINTC', 'MINAV', 'MINTC_AM',*)
    'AdValorem', (*'IOC', 'IOC_VAL',
    'Tarifas',*) 'CPMF', (*'CPMF_VAL',*)
    'TipoAdV', 'IRRF', (*'IRRF_Val',*)
    'ISS', (*'ISS_Val',*) 'PIS',
    (*'PIS_Val',*) 'PIS_R', (*'PIS_R_Val',
    'PIS_T_Val',*) 'COFINS', (*'COFINS_Val',*)
    'COFINS_R', (*'COFINS_R_Val', 'COFINS_T_Val',
    'OcorP', 'MaxVencto', 'CHDevPg',
    'DUDevPg', 'PgLiq',*) 'NF',
    (*'Itens',*) 'Conferido', (*'ECartaSac',
    'AllQuit', 'SobraIni', 'SobraNow',*)
    'WebCod', 'IOFd', (*'IOFd_VAL',*)
    'IOFv', (*'IOFv_VAL',*) 'TipoIOF',
    'LctsAuto'], [
    'Codigo'], [
    Tipo, Spread, Cliente,
    CBE, SCB, Lote,
    Data, (*Total, Dias,
    PeCompra, TxCompra, ValValorem,
    MINTC, MINAV, MINTC_AM,*)
    AdValorem, (*IOC, IOC_VAL,
    Tarifas,*) CPMF, (*CPMF_VAL,*)
    TipoAdV, IRRF, (*IRRF_Val,*)
    ISS, (*ISS_Val,*) PIS,
    (*PIS_Val,*) PIS_R, (*PIS_R_Val,
    PIS_T_Val,*) COFINS, (*COFINS_Val,*)
    COFINS_R, (*COFINS_R_Val, COFINS_T_Val,
    OcorP, MaxVencto, CHDevPg,
    DUDevPg, PgLiq,*) NF,
    (*Itens,*) Conferido, (*ECartaSac,
    AllQuit, SobraIni, SobraNow,*)
    WebCod, IOFd, (*IOFd_VAL,*)
    IOFv, (*IOFv_VAL,*) TipoIOF,
    LctsAuto], [
    Codigo], True) then
    begin
      if ImgTipo.SQLType = stIns then
      begin
        Dmod.QrUpdM.SQL.Clear;
        Dmod.QrUpdM.SQL.Add('UPDATE llotecab SET Baixado=3 WHERE Codigo=:P0');
        Dmod.QrUpdM.Params[0].AsInteger := WebCod;
        Dmod.QrUpdM.ExecSQL;
      end;
      UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, CO_TabLotA, 'Codigo');
      DefineAdValoremEmpresas();
      if ImgTipo.SQLType = stIns then
      begin
        LancaTaxasNovoLote(Cliente, Tipo, Codigo);
        FmPrincipal.AtualizaSP2(1);
        FmPrincipal.AtualizaSP2(2);
      end;
    end;
    ////////////////////////////////////////////////////////////////////////////
    if FmLot2Cab.FIncluindoCH then
    begin
      DmLot2.QrLCHs.First;
      ProgressBar4.Position := 0;
      ProgressBar4.Max := DmLot2.QrLCHs.RecordCount;
      ProgressBar4.Visible := True;
      ProgressBar4.Refresh;
      Application.ProcessMessages;
      while not DmLot2.QrLCHs.Eof do
      begin
        ProgressBar4.Position := ProgressBar4.Position + 1;
        ProgressBar4.Refresh;
        Application.ProcessMessages;
        ImportaCheque(Codigo, Cliente, CBE);
        DmLot2.QrLCHs.Next;
      end;
      FmLot2Cab.FIncluindoCh := False;
      FmPrincipal.FGradeFocused := FmPrincipal.FGradeFocused -1;
      if FmPrincipal.FGradeFocused = 2 then
      begin
        Dmod.QrUpdL.SQL.Clear;
        Dmod.QrUpdL.SQL.Add('DELETE FROM importlote WHERE Tipo=0');
        Dmod.QrUpdL.ExecSQL;
        DmLot2.ReopenImportLote(0,0);
        //
        //FmPrincipal.Importar(il0_Nil, 0);
      end else
{???
        MostraEdicao(0, stLok, 0)};
    end else if FmLot2Cab.FIncluindoDU then
    begin
      ProgressBar4.Position := 0;
      ProgressBar4.Max := DmLot2.QrLDUs.RecordCount;
      ProgressBar4.Visible := True;
      ProgressBar4.Refresh;
      Application.ProcessMessages;
      DmLot2.QrLDUs.First;
      while not DmLot2.QrLDUs.Eof do
      begin
        ProgressBar4.Position := ProgressBar4.Position +1;
        ProgressBar4.Refresh;
        Application.ProcessMessages;
        ImportaDuplicata(Codigo, Cliente, DmLot2.FDataTransacao);
        DmLot2.QrLDUs.Next;
      end;
      FmLot2Cab.FIncluindoDU := False;
      FmPrincipal.FGradeFocused := FmPrincipal.FGradeFocused -2;
      if FmPrincipal.FGradeFocused = 1 then
      begin
        Dmod.QrUpdL.SQL.Clear;
        Dmod.QrUpdL.SQL.Add('DELETE FROM importlote WHERE Tipo=1');
        Dmod.QrUpdL.ExecSQL;
        DmLot2.ReopenImportLote(0,0);
        //
        //FmPrincipal.Importar(il0_Nil, 0);
      end else
{???
        MostraEdicao(0, stLok, 0)};
    end else begin
      if FAlteraTodos then
      begin
        TxaCompra := EdTxaCompraX.ValueVariant;
        DMais     := EdDMaisX.ValueVariant;

{
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('UPDATE lot esits SET AlterWeb=1, TxaCompra=:P0, ');
        Dmod.QrUpd.SQL.Add('DMais=:P1, DCompra=:P2 WHERE Codigo=:Pa');
        Dmod.QrUpd.Params[0].AsFloat   := TxaCompra;
        Dmod.QrUpd.Params[1].AsInteger := DMais;
        Dmod.QrUpd.Params[2].AsString  := Data;
        //
        Dmod.QrUpd.Params[3].AsInteger := Codigo;
        Dmod.QrUpd.ExecSQL;
}
        DCompra := Data;
        FatNum  := Codigo;
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_TabLctA, False, [
        'TxaCompra', 'DMais', 'DCompra'], ['FatID', 'FatNum'], [
        TxaCompra, DMais, DCompra], [VAR_FATID_0301, FatNum], True);
        //
        for Idx := 0 to FmPrincipal.FMyDBs.MaxDBs -1 do
        begin
          if FmPrincipal.FMyDBs.Connected[Idx] = '1' then
          begin
           Dmod.QrUpd_.Database := Dmod.FArrayMySQLBD[Idx];
           //
           Dmod.QrUpd_.SQL.Clear;
           Dmod.QrUpd_.SQL.Add('UPDATE emlotits SET TaxaPer=:P0, TaxaVal=:P1, ');
           Dmod.QrUpd_.SQL.Add('JuroPer=:P2 WHERE Codigo=:Pa');
           //
           Dmod.QrUpd_.Params[0].AsFloat   := Geral.DMV(GradeX1.Cells[01, Idx + 1]);
           Dmod.QrUpd_.Params[1].AsFloat   := 0;
           Dmod.QrUpd_.Params[2].AsFloat   := 0;
           //
           Dmod.QrUpd_.Params[3].AsInteger := Codigo;
           Dmod.QrUpd_.ExecSQL;
           //
          end;
        end;
      end;
      {??? 
      FmLot2Cab.MostraEdicao(0, stLok, 0);
      }
      FmLot2Cab.LocCod(Codigo,Codigo);
      Update;
      Refresh;
      Application.ProcessMessages;
    end;
    FmLot2Cab.RecalculaBordero(Codigo);
    FmLot2Cab.LocCod(Codigo, Codigo);
    Screen.Cursor := crDefault;
  except
    Screen.Cursor := crDefault;
    raise;
  end;
  VerificaNF(NF);
  FmPrincipal.AtualizaLastEditLote(Data);
  ProgressBar4.Visible := False;
  if FmPrincipal.FTipoImport = il3_FTP then
  begin
    OldFile := FmPrincipal.FDirR;
    if OldFile[Length(OldFile)] <> '\' then OldFile := OldFile + '\';
    OldFile := OldFile + ExtractFileName(FmPrincipal.OpenDialog1.FileName);
    NewFile := Dmod.QrControleFTP_Lix.Value;
    If Trim(NewFile) = '' then
    begin
      Geral.MensagemBox('Defina o diret�rio de arquivos scx baixados e processados em '+
      'Op��es -> Espec�ficos, na orelha Exporta\Importa',
      'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
    if NewFile[Length(NewFile)] <> '\' then NewFile := NewFile + '\';
    NewFile := NewFile + ExtractFileName(FmPrincipal.OpenDialog1.FileName);
    if FileExists(OldFile) and (DirectoryExists(Dmod.QrControleFTP_Lix.Value)) then
    begin
      if Geral.MensagemBox('Deseja mover o arquivo "' + OldFile +
      '" para o diret�rio "' + Dmod.QrControleFTP_Lix.Value + '"?', 'Pergunta',
      MB_YESNO+MB_ICONQUESTION) = ID_YES then
      begin
        MoveFile(PChar(OldFile), PChar(NewFile));
        FmPrincipal.SP2.Panels[7].Text :=
          Geral.FF0(dmkPF.GetAllFiles(False, FmPrincipal.FDirR + '*.scx',
          FmPrincipal.ListBox1, True));
      end;
    end;
  end;
  FmPrincipal.FTipoImport  := il0_Nil;
  FmPrincipal.FloteWebDone := 0;
  //
  Close;
end;

procedure TFmLot2Cad.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLot2Cad.CalculaValoresChequeImportando(Dias: Integer);
var
  TaxaT, Valor, JuroT: Double;
  i: Integer;
begin
  FmLot2Cab.FTaxa[0] := Geral.DMV(EdTxaCompraX.Text);
  for i := 1 to GradeX1.RowCount -1 do FmLot2Cab.FTaxa[i] :=
    Geral.DMV(GradeX1.Cells[1, i]);
  // Setar, n�o setados
  for i := GradeX1.RowCount to 6 do FmLot2Cab.FTaxa[i] := 0;
  TaxaT := 0;
  for i := 0 to 6 do TaxaT := TaxaT +FmLot2Cab.FTaxa[i];
  Valor := DmLot2.QrLCHsValor.Value;
  //juros compostos ?
  JuroT   := MLAGeral.CalculaJuroComposto(TaxaT, Dias);
  //
  //////////////////////////////////////////////////////////////////////////////
  if TaxaT > FmLot2Cab.FTaxa[0] then
  begin
    if Dmod.QrControleCalcMyJuro.Value = 1 then
    begin
      for i := 0 to 6 do
      begin
        if TaxaT = 0 then FmLot2Cab.FJuro[i] := 0 else
          FmLot2Cab.FJuro[i] := FmLot2Cab.FTaxa[i] / TaxaT * JuroT;
      end;
      for i := 0 to 6 do
          FmLot2Cab.FValr[i] := FmLot2Cab.FJuro[i] * Valor / 100;
    end else begin
      FmLot2Cab.FJuro[0] := MLAGeral.CalculaJuroComposto(FmLot2Cab.FTaxa[0], Dias);
      FmLot2Cab.FValr[0] := FmLot2Cab.FJuro[0] * Valor / 100;
      JuroT := JuroT - FmLot2Cab.FJuro[0];
      TaxaT := TaxaT - FmLot2Cab.FTaxa[0];
      for i := 1 to 6 do
      begin
        if TaxaT = 0 then FmLot2Cab.FJuro[i] := 0 else
          FmLot2Cab.FJuro[i] := FmLot2Cab.FTaxa[i] / TaxaT * JuroT;
      end;
      for i := 1 to 6 do
          FmLot2Cab.FValr[i] := FmLot2Cab.FJuro[i] * Valor / 100;
    end;
  end else begin
    FmLot2Cab.FJuro[0] := JuroT;
    FmLot2Cab.FValr[0] := JuroT * Valor / 100;
  end;
end;

procedure TFmLot2Cad.CalculaValoresDuplicataImportando(Dias: Integer);
var
  TaxaT, Valor, JuroT: Double;
  i: Integer;
begin
  FmLot2Cab.FTaxa[0] := Geral.DMV(EdTxaCompraX.Text);
  for i := 1 to GradeX1.RowCount -1 do FmLot2Cab.FTaxa[i] :=
    Geral.DMV(GradeX1.Cells[1, i]);
  // setar, n�o setados
  for i := GradeX1.RowCount to 6 do FmLot2Cab.FTaxa[i] := 0;
  TaxaT := 0;
  for i := 0 to 6 do TaxaT := TaxaT + FmLot2Cab.FTaxa[i];
  Valor := DmLot2.QrLDUsValor.Value;
  //juros compostos ?
  JuroT   := MLAGeral.CalculaJuroComposto(TaxaT, Dias);
  //
  //////////////////////////////////////////////////////////////////////////////
  if TaxaT > FmLot2Cab.FTaxa[0] then
  begin
    if Dmod.QrControleCalcMyJuro.Value = 1 then
    begin
      for i := 0 to 6 do
      begin
        if TaxaT = 0 then FmLot2Cab.FJuro[i] := 0 else
          FmLot2Cab.FJuro[i] := FmLot2Cab.FTaxa[i] / TaxaT * JuroT;
      end;
      for i := 0 to 6 do
          FmLot2Cab.FValr[i] := FmLot2Cab.FJuro[i] * Valor / 100;
    end else begin
      FmLot2Cab.FJuro[0] := MLAGeral.CalculaJuroComposto(FmLot2Cab.FTaxa[0], Dias);
      FmLot2Cab.FValr[0] := FmLot2Cab.FJuro[0] * Valor / 100;
      JuroT := JuroT - FmLot2Cab.FJuro[0];
      TaxaT := TaxaT - FmLot2Cab.FTaxa[0];
      for i := 1 to 6 do
      begin
        if TaxaT = 0 then FmLot2Cab.FJuro[i] := 0 else
          FmLot2Cab.FJuro[i] := FmLot2Cab.FTaxa[i] / TaxaT * JuroT;
      end;
      for i := 1 to 6 do
          FmLot2Cab.FValr[i] := FmLot2Cab.FJuro[i] * Valor / 100;
    end;
  end else begin
    FmLot2Cab.FJuro[0] := JuroT;
    FmLot2Cab.FValr[0] := JuroT * Valor / 100;
  end;
end;

procedure TFmLot2Cad.ConfiguraTaxas();
var
  i: Integer;
begin
  if ImgTipo.SQLType <> stIns then Exit;
  //EdAdValorem.LeftZeros := FCasasAdValorem;
  EdAdValorem.ValueVariant := DmLot.QrClientesAdValorem.Value;
  for i := 0 to FmPrincipal.FMyDBs.MaxDBs -1 do
  begin
    if FmPrincipal.FMyDBs.Connected[i] = '1' then
    begin
      GradeAV.Cells[00, i+1] := FmPrincipal.FMyDBs.IDName[i];
      FmLot2Cab.FArrayMySQLEE[i].Close;
      FmLot2Cab.FArrayMySQLEE[i].Params[0].AsInteger := EdCliente.ValueVariant;
      UMyMod.AbreQuery(FmLot2Cab.FArrayMySQLEE[i], FmLot2Cab.FArrayMySQLEE[i].Database);
      GradeAV.Cells[01, i+1] := Geral.FFT(
        FmLot2Cab.FArrayMySQLEE[i].FieldByName('AdVal').AsFloat, 6, siPositivo);
    end;
  end;
{
  EdDMaisC.Text := Geral.FF0(DmLot.QrClientesDMaisC.Value);
  EdDMaisD.Text := Geral.FF0(DmLot.QrClientesDMaisD.Value);
}
  if Dmod.QrControleCBEMinimo.Value > 0 then
    EdCBE.Text := Geral.FF0(Dmod.QrControleCBEMinimo.Value)
  else
    EdCBE.Text := Geral.FF0(DmLot.QrClientesCBE.Value);
  if Dmod.QrControleSBCPadrao.Value = 2 then
    CkSCB.Checked := Geral.IntToBool_0(DmLot.QrClientesSCB.Value)
  else
    CkSCB.Checked := Geral.IntToBool_0(Dmod.QrControleSBCPadrao.Value);
  if Dmod.QrControleCPMF_Padrao.Value = 0 then
    EdCPMF.Text := Geral.FFT(DmLot.QrClientesCPMF.Value, 4, siPositivo);
end;

procedure TFmLot2Cad.DefineAdValoremEmpresas();
var
  i: Integer;
begin
  for i := 0 to FmPrincipal.FMyDBs.MaxDBs -1 do
  begin
    if FmPrincipal.FMyDBs.Connected[i] = '1' then
    begin
     Dmod.QrUpd_.Database := Dmod.FArrayMySQLBD[i];
     Dmod.QrUpd_.SQL.Clear;
     Dmod.QrUpd_.SQL.Add('DELETE FROM emlot WHERE Codigo=:P0');
     Dmod.QrUpd_.Params[0].AsInteger := EdCodigo.ValueVariant;
     Dmod.QrUpd_.ExecSQL;
     //
     Dmod.QrUpd_.SQL.Clear;
     Dmod.QrUpd_.SQL.Add('INSERT INTO emlot SET AlterWeb=1, AdValorem=:P0, ');
     Dmod.QrUpd_.SQL.Add('Codigo=:Pa');
     //
     Dmod.QrUpd_.Params[0].AsFloat   := Geral.DMV(GradeAV.Cells[1, i+1]);
     //
     Dmod.QrUpd_.Params[1].AsInteger := EdCodigo.ValueVariant;
     Dmod.QrUpd_.ExecSQL;
     //
    end;
  end;
end;

procedure TFmLot2Cad.DefineNovaNF();
begin
  DmLot.QrLocNF.Close;
  UMyMod.AbreQuery(DmLot.QrLocNF, Dmod.MyDB);
  EdNF.Text := MLAGeral.FFD(DmLot.QrLocNFCodigo.Value + 1, 6, siNegativo);
end;

procedure TFmLot2Cad.DefineNovoLote();
begin
{
  DmLot.QrLocLote.Close;
  DmLot.QrLocLote.Params[0].AsInteger := EdCliente.ValueVariant;
  DmLot.QrLocLote.Params[1].AsInteger := EdCodigo.ValueVariant;
  DmLot.QrLocLote. Open;
}
  DmLot.ReopenLocLote(EdCliente.ValueVariant, EdCodigo.ValueVariant);
  EdLote.ValueVariant := DmLot.QrLocLoteLote.Value + 1;
end;

procedure TFmLot2Cad.EdBanco0Change(Sender: TObject);
begin
  if not EdBanco0.Focused then
    ReopenBanco0();
end;

procedure TFmLot2Cad.EdBanco0Exit(Sender: TObject);
begin
  ReopenBanco0();
end;

procedure TFmLot2Cad.EdClienteChange(Sender: TObject);
begin
  ConfiguraTaxas();
  if ImgTipo.SQLType = stIns then
    DefineNovoLote();
end;

procedure TFmLot2Cad.EdLoteKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_F4 then
    DefineNovoLote();
end;

procedure TFmLot2Cad.EdNFDblClick(Sender: TObject);
begin
  if Geral.MensagemBox('Deseja realmente alterar o n�mero da NF?', 'Pergunta',
    MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES
  then
    Exit;
  if not DBCheck.LiberaPelaSenhaBoss then Exit;
  //
  EdNF.ReadOnly := False;
end;

procedure TFmLot2Cad.EdNFKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (key = VK_F4) and (EdNF.ReadOnly = True) then
    DefineNovaNF();
end;

procedure TFmLot2Cad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLot2Cad.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  DmLot.FShowLot2Cad := False;
end;

procedure TFmLot2Cad.FormCreate(Sender: TObject);
begin
  DmLot.FShowLot2Cad := True;
  //
  TPData.Date := Date;
  GradeAV.ColCount := 2;
  GradeAV.RowCount := 2;
  //
  GradeAV.ColWidths[00] := 240;
  GradeAV.ColWidths[01] := 80;
  //
  GradeAV.Cells[00,00] := 'Coligada';
  if Dmod.QrControleTipoAdValorem.Value = 0 then
  begin
    LaAdValorem.Caption := '% Ad Valorem';
    GradeAV.Cells[01,00] := '% Ad Valorem';
    FCasasAdValorem := 4;
  end else begin
    LaAdValorem.Caption := '$ Ad Valorem';
    GradeAV.Cells[01,00] := '$ Ad Valorem';
    FCasasAdValorem := 2;
  end;
  EdAdValorem.LeftZeros := FCasasAdValorem;
  GradeX1.ColCount := 2;
  GradeX1.RowCount := 2;
  GradeX1.ColWidths[00] := 240;
  GradeX1.ColWidths[01] := 80;
  GradeX1.Cells[00,00] := 'Coligada';
  GradeX1.Cells[01,00] := '% Compra';
end;

procedure TFmLot2Cad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLot2Cad.FormShow(Sender: TObject);
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, False,
    'Para editar o n�mero da NF d� um duplo clique no campo NF para permitir a altera��o!');
end;

procedure TFmLot2Cad.GradeAVDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  OldAlign, Color: Integer;
begin
  if GradeAV = nil then Exit;
  Color := clBlack;
  SetTextColor(GradeAV.Canvas.Handle, Color);
  if ARow = 0 then
  begin
    //
  end else if ACol = 0 then begin
    if ARow <> 0 then begin
      OldAlign := SetTextAlign(GradeAV.Canvas.Handle, TA_LEFT);
      GradeAV.Canvas.TextRect(Rect, Rect.Left+2, Rect.Top + 2,
        GradeAV.Cells[Acol, ARow]);
      SetTextAlign(GradeAV.Canvas.Handle, OldAlign);
    end;
  end else if ACol = 01 then begin
      OldAlign := SetTextAlign(GradeAV.Canvas.Handle, TA_RIGHT);
      GradeAV.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Geral.TFT(GradeAV.Cells[Acol, ARow], 6, siPositivo));
      SetTextAlign(GradeAV.Canvas.Handle, OldAlign);
  end //else if ACol = 02 then begin
end;

procedure TFmLot2Cad.GradeAVKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_RETURN then
  begin
    if GradeAV.EditorMode = True then
    begin
      if (GradeAV.Row < GradeAV.RowCount-1) then
        GradeAV.Row := GradeAV.Row +1
      else if PainelItens.Visible then EdDMaisX.SetFocus
      else BtConfirma.SetFocus;
    end;
  end
end;

procedure TFmLot2Cad.GradeX1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = VK_RETURN then
  begin
    if GradeX1.EditorMode = True then
    begin
      if (GradeX1.Row < GradeX1.RowCount-1) then
        GradeX1.Row := GradeX1.Row +1
      else BtConfirma.SetFocus;
    end;
  end
end;

procedure TFmLot2Cad.ImportaCheque(FatNum, Cliente, CBE: Integer);
const
{  FatNum = 0;
  DMais = 0;
  StatusSPC = -1;
  TxaCompra = 0;
  TxaAdValorem = 0;
  VlrCompra = 0;
  VlrAdValorem = 0;
  TxaJuros = 0;
  AliIts = 0;
  Alin#Pgs = 0;
  Descricao = 'CH Avulso';
  Data3 = '0000-00-00';
}
  Tipific = 5;
var
  DMais, Dias, Praca: Integer;
  Vence, Data_: TDateTime;
  Comp, Banco, StatusSPC: Integer;
  DDeposito, DCompra, Emitente: String;
  //
  Agencia, Documento, FatParcela, Controle, Carteira, TipoCart, Sub: Integer;
  Vencimento, ContaCorrente, CNPJCPF, DataDoc: String;
  Credito: Double;
begin
  Screen.Cursor := crHourGlass;
  try
    StatusSPC     := DmLot2.QrLCHsStatusSPC.Value;
    Credito       := DmLot2.QrLCHsValor.Value;
    Vence         := DmLot2.QrLCHsDVence.Value;
    Vencimento    := Geral.FDT(Vence, 1);
    Data_         := DmLot2.QrLCHsDCompra.Value;
    DCompra       := Geral.FDT(Data_, 1);
    DataDoc       := DCompra;
    Comp          := DmLot2.QrLCHsComp.Value;
    Banco         := DmLot2.QrLCHsBanco.Value;
    Agencia       := DmLot2.QrLCHsAgencia.Value;
    ContaCorrente := DmLot2.QrLCHsConta.Value;
    Documento     := DmLot2.QrLCHsCheque.Value;
    CNPJCPF       := Geral.SoNumero_TT(DmLot2.QrLCHsCPF.Value);
    Emitente      := DmLot2.QrLCHsEmitente.Value;
    Praca         := DmLot2.QrLCHsPraca.Value;
    //
    DMais         := EdDMaisX.ValueVariant;
    //
    ////////////////////////////////////////////////////////////////////////////
    if Comp = Dmod.QrControleRegiaoCompe.Value then
      Comp := Dmod.QrControleMinhaCompe.Value
    else
      Comp := Dmod.QrControleOutraCompe.Value;
    if Credito < DMod.QrControleChequeMaior.Value then Comp := Comp + 1;
    Dias := UMyMod.CalculaDias(int(TPData.Date), int(DmLot2.QrLCHsDVence.Value), DMais,
      Comp, Dmod.QrControleTipoPrazoDesc.Value, CBE);
    ////////////////////////////////////////////////////////////////////////////
    DDeposito := FormatDateTime(VAR_FORMATDATE, UMyMod.CalculaDataDeposito(Vence));
    CalculaValoresChequeImportando(Dias);
    Dmod.QrUpd.SQL.Clear;
{
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
        'Lot esIts', 'Lot esIts', 'Controle');
    Dmod.QrUpd.SQL.Add('INSERT INTO lot esits SET Quitado=0, ');
    Dmod.QrUpd.SQL.Add('Comp=:P0, Banco=:P1, Agencia=:P2, Conta=:P3, ');
    Dmod.QrUpd.SQL.Add('Cheque=:P4, CPF=:P5, Emitente=:P6, Valor=:P7, ');
    Dmod.QrUpd.SQL.Add('Vencto=:P8, TxaCompra=:P9, TxaAdValorem=:P10, ');
    Dmod.QrUpd.SQL.Add('VlrCompra=:P11, VlrAdValorem=:P12, DMais=:P13, ');
    Dmod.QrUpd.SQL.Add('Dias=:P14, TxaJuros=:P15, DCompra=:P16, ');
    Dmod.QrUpd.SQL.Add('DDeposito=:P17, Praca=:P18, Cliente=:P19, ');
    Dmod.QrUpd.SQL.Add('Data3=:P20, StatusSPC=:P21 ');
    Dmod.QrUpd.SQL.Add(', Codigo=:Pa, Controle=:Pb');
    //
    Dmod.QrUpd.Params[00].AsInteger := Comp;
    Dmod.QrUpd.Params[01].AsInteger := Banco;
    Dmod.QrUpd.Params[02].AsInteger := Agencia;
    Dmod.QrUpd.Params[03].AsString  := Conta;
    Dmod.QrUpd.Params[04].AsInteger := Cheque;
    Dmod.QrUpd.Params[05].AsString  := CPF;
    Dmod.QrUpd.Params[06].AsString  := Emitente;
    Dmod.QrUpd.Params[07].AsFloat   := Valor;
    Dmod.QrUpd.Params[08].AsString  := Vencto;
    //
    Dmod.QrUpd.Params[09].AsFloat   := FmLot2Cab.FTaxa[0];
    Dmod.QrUpd.Params[10].AsFloat   := 0;
    Dmod.QrUpd.Params[11].AsFloat   := FmLot2Cab.FValr[0];
    Dmod.QrUpd.Params[12].AsFloat   := 0;
    Dmod.QrUpd.Params[13].AsInteger := DMais;
    Dmod.QrUpd.Params[14].AsInteger := Dias;
    Dmod.QrUpd.Params[15].AsFloat   := FmLot2Cab.FJuro[0];
    Dmod.QrUpd.Params[16].AsString  := DCompra;
    Dmod.QrUpd.Params[17].AsString  := DDeposito;
    Dmod.QrUpd.Params[18].AsInteger := Praca;
    Dmod.QrUpd.Params[19].AsInteger := Cliente;
    Dmod.QrUpd.Params[20].AsString  := DDeposito; // Quita autom�tico at� voltar
    Dmod.QrUpd.Params[21].AsInteger := StatusSPC;
    //
    Dmod.QrUpd.Params[22].AsInteger := Codigo;
    Dmod.QrUpd.Params[23].AsInteger := Controle;
    Dmod.QrUpd.ExecSQL;
}
    Sub := 0;
    Carteira := CO_CARTEIRA_CHQ_BORDERO;
    TipoCart := 2;
{ TODO : Comparar c�culo com creditor! }
    FmLot2Cab.SQL_CH(FatParcela, Comp, Banco, Agencia, Documento,
                DMais, Dias, FatNum, Praca, Tipific, StatusSPC, ContaCorrente,
                CNPJCPF, Emitente, DataDoc, Vencimento, DCompra, DDeposito, Credito,
                ImgTipo.SQLType, FmLot2Cab.QrLotCliente.Value,
                TipoCart, Carteira, Controle, Sub);
    //
    //
    FmLot2Cab.DefineCompraEmpresas(FatNum, FatParcela);
    //
    Screen.Cursor := crDefault;
  except
    Screen.Cursor := crDefault;
    raise;
  end;
end;

procedure TFmLot2Cad.ImportaDuplicata(Codigo, Cliente: Integer; DataTransacao:
  TDateTime);
const
  DescAte = '0000-00-00';
var
  //StatusSPC,
  Comp, DMais, Dias, CartDep, Controle, Banco, Agencia, FatParcela,
  TipoCart, Carteira, Sub, FatNum: Integer;
  Credito, Desco, Bruto: Double;
  Duplicata, DDeposito, DCompra, Emissao, Vencimento, CNPJCPF, Emitente, Rua,
  IE: String;
begin
  Screen.Cursor := crHourGlass;
  try
    //StatusSPC  := DmLot2.QrLDUsStatusSPC.Value;
    Emissao    := Geral.FDT(DmLot2.QrLDUsDEmiss.Value, 1);
    //CartDep    := EdCartDep1.ValueVariant;
    Duplicata  := DmLot2.QrLDUsDuplicata.Value;
    Credito    := DmLot2.QrLDUsValor.Value;
    Desco      := DmLot2.QrLDUsDesco.Value;
    Bruto      := DmLot2.QrLDUsBruto.Value;
    Vencimento := Geral.FDT(DmLot2.QrLDUsDVence.Value, 1);
    CNPJCPF    := Geral.SoNumero_TT(DmLot2.QrLDUsCPF.Value);
    Emitente   := DmLot2.QrLDUsEmitente.Value;
    //
    if DataTransacao < 2 then
      DCompra   := FormatDateTime(VAR_FORMATDATE, Date)
    else
      DCompra   := FormatDateTime(VAR_FORMATDATE, DataTransacao);
    //
    Rua       := DmLot2.QrLDUsRua.Value;
    IE        := DmLot2.QrLDUsIE.Value;
    DMais     := EdDMaisX.ValueVariant;
    ////////////////////////////////////////////////////////////////////////////
    Comp      := 0; // S� para Cheques
    Dias := UMyMod.CalculaDias(int(TPData.Date), int(DmLot2.QrLDUsDVence.Value),
      DMais, Comp, Dmod.QrControleTipoPrazoDesc.Value, 0(*CBE*));
    ////////////////////////////////////////////////////////////////////////////
    CalculaValoresDuplicataImportando(Dias);
    DDeposito := FormatDateTime(VAR_FORMATDATE,
      UMyMod.CalculaDataDeposito(DmLot2.QrLDUsDVence.Value));
{
    Dmod.QrUpd.SQL.Clear;
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
      'Lot esIts', 'Lot esIts', 'Controle');
    Dmod.QrUpd.SQL.Add('INSERT INTO lot esits SET ');
    Dmod.QrUpd.SQL.Add('Duplicata=:P0, CPF=:P1, Emitente=:P2, Valor=:P3, ');
    Dmod.QrUpd.SQL.Add('Vencto=:P4, TxaCompra=:P5, VlrCompra=:P6, DMais=:P7, ');
    Dmod.QrUpd.SQL.Add('Dias=:P8, TxaJuros=:P9, DCompra=:P10, DDeposito=:P11, ');
    Dmod.QrUpd.SQL.Add('Emissao=:P12, Desco=:P13, Bruto=:P14, Banco=:P15, ');
    Dmod.QrUpd.SQL.Add('Agencia=:P16, Cliente=:P17, CartDep=:P18, ');
    Dmod.QrUpd.SQL.Add('StatusSPC=:P19 ');
    Dmod.QrUpd.SQL.Add(', Codigo=:Pa, Controle=:Pb');
    Dmod.QrUpd.Params[00].AsString  := Duplic;
    Dmod.QrUpd.Params[01].AsString  := CPF;
    Dmod.QrUpd.Params[02].AsString  := Sacado;
    Dmod.QrUpd.Params[03].AsFloat   := Valor;
    Dmod.QrUpd.Params[04].AsString  := Vencto;
    //
    Dmod.QrUpd.Params[05].AsFloat   := FmLot2Cab.FTaxa[0];
    Dmod.QrUpd.Params[06].AsFloat   := FmLot2Cab.FValr[0];
    Dmod.QrUpd.Params[07].AsInteger := DMais;
    Dmod.QrUpd.Params[08].AsInteger := Dias;
    Dmod.QrUpd.Params[09].AsFloat   := FmLot2Cab.FJuro[0];
    Dmod.QrUpd.Params[10].AsString  := DCompra;
    Dmod.QrUpd.Params[11].AsString  := DDeposito;
    Dmod.QrUpd.Params[12].AsString  := Emissao;
    Dmod.QrUpd.Params[13].AsFloat   := Desco;
    Dmod.QrUpd.Params[14].AsFloat   := Bruto;
    Dmod.QrUpd.Params[15].AsInteger := EdBanco0.ValueVariant;
    Dmod.QrUpd.Params[16].AsInteger := EdAgencia0.ValueVariant;
    Dmod.QrUpd.Params[17].AsInteger := Cliente;
    Dmod.QrUpd.Params[18].AsInteger := CartDep;
    Dmod.QrUpd.Params[19].AsInteger := StatusSPC;
    //
    Dmod.QrUpd.Params[20].AsInteger := Codigo;
    Dmod.QrUpd.Params[21].AsInteger := Controle;
    Dmod.QrUpd.ExecSQL;
}
    FatNum := Codigo;
    Banco := EdBanco0.ValueVariant;
    Agencia := EdAgencia0.ValueVariant;
    CartDep := 0;
    TipoCart := 2;
    Carteira := CO_CARTEIRA_DUP_BORDERO;
    Controle := 0;
    Sub := 0;
    //
    FmLot2Cab.SQL_DU(FatNum, FatParcela, DMais, Dias, Banco, Agencia, Credito,
      Desco, Bruto, Duplicata, CNPJCPF, Emitente, Vencimento, DCompra,
      DDeposito, Emissao, DescAte, stIns, Cliente, CartDep, TipoCart, Carteira,
      Controle, Sub);
    //
    FmLot2Cab.DefineCompraEmpresas(Codigo, Controle);
    //
    Screen.Cursor := crDefault;
  except
    Screen.Cursor := crDefault;
    raise;
  end;
end;

procedure TFmLot2Cad.LancaTaxasNovoLote(Cliente, TipoLote,
  NumeroDoLote: Integer);
const
  Qtde = 0;
  MoraDia = 0;
var
{
  TaxaCod: Integer;
  TaxaTxa, TaxaVal, TaxaQtd: Double;
  //
}
  FatID_Sub, FatParcela, Controle, Genero, Tipific: Integer;
  FatNum, MoraTxa, MoraVal, Credito: Double;
  Dta: TDateTime;
begin
  TipoLote := TipoLote+1;
{
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO ' + CO TabLotTxs + ' SET AlterWeb=1, ');
  Dmod.QrUpd.SQL.Add('TaxaCod=:P0, TaxaTxa=:P1, TaxaVal=:P2, Forma=:P3, ');
  Dmod.QrUpd.SQL.Add('UserCad=:Pa, DataCad=:Pb, Codigo=:Pc, Controle=:Pd');
}
  //
  if Dmod.QrControleTaxasAutom.Value = 0 then
  begin
{
    DmLot.QrNovoTxs.Close;
    UMyMod.AbreQuery(DmLot.QrNovoTxs);
}
    DmLot.ReopenNovoTxs();
   //
    DmLot.QrNovoTxs.First;
    while not DmLot.QrNovoTxs.Eof do
    begin
      Genero := DmLot.QrNovoTxsPlaGen.Value;
      //if
      MyObjects.FIC(Genero=0, nil, 'A Taxa cadastro n� ' + Geral.FF0(
      DmLot.QrNovoTxsCodigo.Value) +
      ' n�o tem definida sua conta do plano de conta! (1)');
      //
      DmLot.QrNovoTxs.Next;
    end;
    //
    DmLot.QrNovoTxs.First;
    while not DmLot.QrNovoTxs.Eof do
    begin
      if MLAGeral.IntInConjunto(TipoLote, DmLot.QrNovoTxsGenero.Value) then
      if MLAGeral.IntInConjunto(TipoLote, DmLot.QrNovoTxsAutomatico.Value) then
      begin
{
        Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
          CO TabLotTxs, CO TabLotTxs, 'Controle');
        Dmod.QrUpd.Params[00].AsInteger := DmLot.QrNovoTxsCodigo.Value;
        Dmod.QrUpd.Params[01].AsFloat   := DmLot.QrNovoTxsValor.Value;
        Dmod.QrUpd.Params[02].AsFloat   := DmLot.QrNovoTxsValor.Value;
        Dmod.QrUpd.Params[03].AsInteger := DmLot.QrNovoTxsForma.Value;
        //
        Dmod.QrUpd.Params[04].AsInteger := VAR_USUARIO;
        Dmod.QrUpd.Params[05].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
        Dmod.QrUpd.Params[06].AsInteger := NumeroDoLote;
        Dmod.QrUpd.Params[07].AsInteger := Controle;
        Dmod.QrUpd.ExecSQL;
}
        FatID_Sub := DmLot.QrNovoTxsCodigo.Value;
        FatParcela := 0;
        Controle := 0;
        Genero := DmLot.QrNovoTxsPlaGen.Value;
        Tipific := DmLot.QrNovoTxsForma.Value;
        FatNum := NumeroDoLote;
        MoraTxa := DmLot.QrNovoTxsValor.Value;
        MoraVal := DmLot.QrNovoTxsValor.Value;
        Credito := DmLot.QrNovoTxsValor.Value;
        Dta := Date;
        //
        if DmLot.SQL_Txa(Dmod.QrUpd, ImgTipo.SQLType,
        FatParcela, Controle, FatID_Sub, Genero, Cliente, Tipific,
        FatNum, Qtde, Credito, MoraTxa, MoraDia, MoraVal, Dta, 0, 0) then ;
      end;
      DmLot.QrNovoTxs.Next;
    end;
  end else begin
    DmLot.QrTaxasCli.Close;
    DmLot.QrTaxasCli.Params[0].AsInteger := Cliente;
    UMyMod.AbreQuery(DmLot.QrTaxasCli, Dmod.MyDB);
    DmLot.QrTaxasCli.First;
    while not DmLot.QrTaxasCli.Eof do
    begin
      Genero := DmLot.QrTaxasCliPlaGen.Value;
      //if
      MyObjects.FIC(Genero=0, nil, 'A Taxa cadastro n� ' + Geral.FF0(
      DmLot.QrTaxasCliTaxa.Value) +
      ' n�o tem definida sua conta do plano de contas! (2)');
      //
      DmLot.QrTaxasCli.Next;
    end;
    DmLot.QrTaxasCli.First;
    while not DmLot.QrTaxasCli.Eof do
    begin
      if MLAGeral.IntInConjunto(TipoLote, DmLot.QrTaxasCliGenero.Value) then
      begin
{
        Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
          CO TabLotTxs, CO TabLotTxs, 'Controle');
        Dmod.QrUpd.Params[00].AsInteger := DmLot.QrTaxasCliTaxa.Value;
        Dmod.QrUpd.Params[01].AsFloat   := DmLot.QrTaxasCliValor.Value;
        Dmod.QrUpd.Params[02].AsFloat   := DmLot.QrTaxasCliValor.Value;
        Dmod.QrUpd.Params[03].AsInteger := DmLot.QrTaxasCliForma.Value;
        //
        Dmod.QrUpd.Params[04].AsInteger := VAR_USUARIO;
        Dmod.QrUpd.Params[05].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
        Dmod.QrUpd.Params[06].AsInteger := NumeroDoLote;
        Dmod.QrUpd.Params[07].AsInteger := Controle;
        Dmod.QrUpd.ExecSQL;
}
        FatID_Sub := DmLot.QrTaxasCliTaxa.Value;
        FatParcela := 0;
        Controle := 0;
        Genero := DmLot.QrTaxasCliPlaGen.Value;
        Tipific := DmLot.QrTaxasCliForma.Value;
        FatNum := NumeroDoLote;
        MoraTxa := DmLot.QrTaxasCliValor.Value;
        MoraVal := DmLot.QrTaxasCliValor.Value;
        Credito := DmLot.QrTaxasCliValor.Value;
        Dta := Date;
        //
        if DmLot.SQL_Txa(Dmod.QrUpd, ImgTipo.SQLType,
        FatParcela, Controle, FatID_Sub, Genero, Cliente, Tipific,
        FatNum, Qtde, Credito, MoraTxa, MoraDia, MoraVal, Dta, 0, 0) then ;
      end;
      DmLot.QrTaxasCli.Next;
    end;
  end;
end;

function TFmLot2Cad.LoteExiste(Cliente, Codigo, Lote: Integer): Boolean;
begin
{
  DmLot.QrLotExist.Close;
  DmLot.QrLotExist.Params[00].AsInteger := Cliente;
  DmLot.QrLotExist.Params[01].AsInteger := Codigo;
  DmLot.QrLotExist.Params[02].AsInteger := Lote;
  DmLot.QrLotExist. Open;
}
  UnDmkDAC_PF.AbreMySQLQuery0(DmLot.QrLotExist, Dmod.MyDB, [
  'SELECT Lote ',
  'FROM ' + CO_TabLotA,
  'WHERE Cliente=' + FormatFloat('0', Cliente),
  'AND Codigo<>' + FormatFloat('0', Codigo),
  'AND Lote=' + FormatFloat('0', Lote),
  '']);
  //
  if DmLot.QrLotExist.RecordCount > 0 then
  begin
    Result := True;
    Geral.MensagemBox('Este lote j� existe!', 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
  end else Result := False;
end;

procedure TFmLot2Cad.ReopenBanco0();
begin
  DmLot.QrBanco0.Close;
  DmLot.QrBanco0.Params[0].AsString := EdBanco0.Text;
  UMyMod.AbreQuery(DmLot.QrBanco0, Dmod.MyDB);
end;

procedure TFmLot2Cad.RGTipoClick(Sender: TObject);
begin
  ConfiguraTaxas();
end;

procedure TFmLot2Cad.SpeedButton5Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  FmPrincipal.MostraEntiJur1(EdCliente.ValueVariant);
  //
  UMyMod.AbreQuery(DmLot.QrClientes, Dmod.MyDB);
  //
  if VAR_CADASTRO > 0 then
  begin
    EdCliente.ValueVariant := VAR_CADASTRO;
    CBCliente.KeyValue     := VAR_CADASTRO;
    EdCliente.SetFocus;
  end;
end;

procedure TFmLot2Cad.TPDataChange(Sender: TObject);
begin
{??? Deve ter algum engano aqui!
  CalculaDiasCH();
}
end;

procedure TFmLot2Cad.VerificaNF(NF: Integer);
begin
  if NF = 0 then Exit;
  DmLot.QrVerNF.Close;
  DmLot.QrVerNF.Params[0].AsInteger := NF;
  UMyMod.AbreQuery(DmLot.QrVerNF, Dmod.MyDB);
  //
  if DmLot.QrVerNFCodigo.Value = 0 then
  begin
    Dmod.QrUpdU.SQL.Clear;
    Dmod.QrUpdU.SQL.Add('INSERT INTO nfs SET ');
    Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz');
    Dmod.QrUpdU.Params[00].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
    Dmod.QrUpdU.Params[01].AsInteger := VAR_USUARIO;
    Dmod.QrUpdU.Params[02].AsInteger := NF;
    Dmod.QrUpdU.ExecSQL;
  end;
end;

end.
