unit ChequesExtras;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, DBCtrls, Db,
  mySQLDbTables, ComCtrls, dmkGeral, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  dmkImage, dmkEditDateTimePicker, DmkDAC_PF, UnDmkEnums;

type
  TFmChequesExtras = class(TForm)
    Panel8: TPanel;
    DBGrid1: TDBGrid;
    QrEntidades: TmySQLQuery;
    DsEntidades: TDataSource;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNOMEENTI: TWideStringField;
    QrSCB: TmySQLQuery;
    QrSCBSCB: TIntegerField;
    QrBanco2: TmySQLQuery;
    QrBanco2Nome: TWideStringField;
    QrBanco2DVCC: TSmallintField;
    GroupBox1: TGroupBox;
    Label51: TLabel;
    Label52: TLabel;
    Label53: TLabel;
    Label96: TLabel;
    Label98: TLabel;
    Label99: TLabel;
    Label91: TLabel;
    Label54: TLabel;
    EdDR: TdmkEdit;
    EdDT: TdmkEdit;
    EdRT: TdmkEdit;
    EdVT: TdmkEdit;
    EdST: TdmkEdit;
    EdCT: TdmkEdit;
    EdDV: TdmkEdit;
    EdCR: TdmkEdit;
    EdCV: TdmkEdit;
    EdOA: TdmkEdit;
    EdTT: TdmkEdit;
    QrAvulsos: TmySQLQuery;
    DsAvulsos: TDataSource;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BitBtn1: TBitBtn;
    Panel3: TPanel;
    BitBtn2: TBitBtn;
    BtExclui: TBitBtn;
    GroupBox2: TGroupBox;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    Label47: TLabel;
    Label48: TLabel;
    Label49: TLabel;
    Label50: TLabel;
    Label35: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    EdBanda2: TdmkEdit;
    EdCMC_7_2: TEdit;
    EdRegiaoCompe2: TdmkEdit;
    EdBanco2: TdmkEdit;
    EdAgencia2: TdmkEdit;
    EdConta2: TdmkEdit;
    EdRealCC: TdmkEdit;
    EdCheque2: TdmkEdit;
    EdCPF_2: TdmkEdit;
    EdEmitente2: TdmkEdit;
    EdValor: TdmkEdit;
    QrAvulsosBanco: TIntegerField;
    QrAvulsosContaCorrente: TWideStringField;
    QrAvulsosDocumento: TFloatField;
    QrAvulsosCredito: TFloatField;
    QrAvulsosDCompra: TDateField;
    QrAvulsosVencimento: TDateField;
    QrAvulsosCliente: TIntegerField;
    QrAvulsosEmitente: TWideStringField;
    QrAvulsosCNPJCPF: TWideStringField;
    QrAvulsosFatParcela: TIntegerField;
    QrAvulsosNOMECLI: TWideStringField;
    QrAvulsosCPF_TXT: TWideStringField;
    QrAvulsosFatNum: TFloatField;
    QrAvulsosCarteira: TIntegerField;
    QrAvulsosAgencia: TIntegerField;
    TPData4: TdmkEditDateTimePicker;
    TPVence4: TdmkEditDateTimePicker;
    QrAvulsosData: TDateField;
    QrAvulsosTipo: TSmallintField;
    QrAvulsosControle: TIntegerField;
    QrAvulsosSub: TSmallintField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdBanda2Change(Sender: TObject);
    procedure EdCMC_7_2Change(Sender: TObject);
    procedure EdBanco2Change(Sender: TObject);
    procedure EdBanco2Exit(Sender: TObject);
    procedure EdAgencia2Change(Sender: TObject);
    procedure EdConta2Change(Sender: TObject);
    procedure EdCPF_2Exit(Sender: TObject);
    procedure EdCPF_2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure QrAvulsosCalcFields(DataSet: TDataSet);
    procedure EdVence4Exit(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure EdCPF_2Change(Sender: TObject);
  private
    { Private declarations }
    function CadastraCheque: Boolean;
    function LocalizaEmitente2(MudaNome: Boolean): Boolean;
    procedure ReopenBanco2;
    function CriaBAC_Pesq(Banco, Agencia, Conta: String; DVCC: Integer): String;
    procedure ReopenAvulsos(FatParcela: Integer);
  public
    { Public declarations }
  end;

  var
  FmChequesExtras: TFmChequesExtras;

implementation

uses UnInternalConsts, Module, UMySQLModule, Principal, LotEmi, ModuleLot2,
  UnMyObjects, MyListas, UnFinanceiro, ModuleLot, UnDmkProcFunc;

{$R *.DFM}

procedure TFmChequesExtras.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmChequesExtras.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  EdCliente.SetFocus;
end;

procedure TFmChequesExtras.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmChequesExtras.CadastraCheque: Boolean;
const
  FatNum = 0;
  DMais = 0;
  Tipific = 5;
  StatusSPC = -1;
  TxaCompra = 0;
  TxaAdValorem = 0;
  VlrCompra = 0;
  VlrAdValorem = 0;
  TxaJuros = 0;
  AliIts = 0;
  CHQPgs = 0;
  Descricao = 'CH Avulso';
  //Data3 = '0000-00-00';
var
  TpAlin, Cliente, FatParcela, Documento,
  TipoCart, Carteira, Controle, Sub,
  Dias, Comp, Banco, Praca: Integer;
  Credito: Double;
  Agencia, Data3, DDeposito, DCompra, Vencimento, ContaCorrente, CNPJCPF, DataDoc,
  Emitente: String;
  Vence, Data_: TDateTime;
begin
  Vence := TPVence4.Date;
  Data_  := TPData4.Date;
  Cliente := EdCliente.ValueVariant;
  Result := True;
  TpAlin := -9; // Cheques avulsos
  Dias    := Trunc(Vence - Data_);
  Credito := EdValor.ValueVariant;
  //Desco   := 0;//Geral.DMV(EdDesco2.Text);
  Vencimento  := FormatDateTime(VAR_FORMATDATE, Vence);
  DCompra := FormatDateTime(VAR_FORMATDATE, Data_);
  DataDoc := DCompra;
  Banco   := EdBanco2.ValueVariant;
  Agencia := FormatFloat('0', EdAgencia2.ValueVariant);
  ContaCorrente   := EdConta2.Text;
  Documento  := EdCheque2.ValueVariant;
  CNPJCPF     := Geral.SoNumero_TT(EdCPF_2.Text);
  Emitente:= EdEmitente2.Text;
  Praca   := EdRegiaoCompe2.ValueVariant;
  QrSCB.Close;
  QrSCB.Params[0].AsInteger := Cliente;
  UMyMod.AbreQuery(QrSCB, Dmod.MyDB);
  //
  Comp := FmPrincipal.VerificaDiasCompensacao(
    Praca, Cliente, Credito, Data_, Vence, QrSCBSCB.Value);
  DDeposito := Geral.FDT(UMyMod.CalculaDataDeposito(Vence), 1);
  Data3 := DDeposito;
{
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO lot esits SET Quitado=0, ');
  Dmod.QrUpd.SQL.Add('Comp=:P0, Banco=:P1, Agencia=:P2, Conta=:P3, ');
  Dmod.QrUpd.SQL.Add('Cheque=:P4, CPF=:P5, Emitente=:P6, Valor=:P7, ');
  Dmod.QrUpd.SQL.Add('Vencto=:P8, TxaCompra=:P9, TxaAdValorem=:P10, ');
  Dmod.QrUpd.SQL.Add('VlrCompra=:P11, VlrAdValorem=:P12, DMais=:P13, ');
  Dmod.QrUpd.SQL.Add('Dias=:P14, TxaJuros=:P15, DCompra=:P16, ');
  Dmod.QrUpd.SQL.Add('DDeposito=:P17, Praca=:P18, Tipo=:P19, ');
  Dmod.QrUpd.SQL.Add('AliIts=:P20, Alin#Pgs=:P21, Cliente=:P22, Data3=:P23 ');
  Dmod.QrUpd.SQL.Add(', Codigo=:Pa, Controle=:Pb');
  //
  Dmod.QrUpd.Params[00].AsInteger := Comp;
  Dmod.QrUpd.Params[01].AsInteger := Banco;
  Dmod.QrUpd.Params[02].AsInteger := Agencia;
  Dmod.QrUpd.Params[03].AsString  := Conta;
  Dmod.QrUpd.Params[04].AsInteger := Cheque;
  Dmod.QrUpd.Params[05].AsString  := CPF;
  Dmod.QrUpd.Params[06].AsString  := Emitente;
  Dmod.QrUpd.Params[07].AsFloat   := Valor;
  Dmod.QrUpd.Params[08].AsString  := Vencto;
  //
  Dmod.QrUpd.Params[09].AsFloat   := 0;//FTaxa[0];
  Dmod.QrUpd.Params[10].AsFloat   := 0;
  Dmod.QrUpd.Params[11].AsFloat   := 0;//FValr[0];
  Dmod.QrUpd.Params[12].AsFloat   := 0;
  Dmod.QrUpd.Params[13].AsInteger := 0;//DMais;
  Dmod.QrUpd.Params[14].AsInteger := Dias;
  Dmod.QrUpd.Params[15].AsFloat   := 0;//FJuro[0];
  Dmod.QrUpd.Params[16].AsString  := DCompra;
  Dmod.QrUpd.Params[17].AsString  := DDeposito;
  Dmod.QrUpd.Params[18].AsInteger := Praca;
  Dmod.QrUpd.Params[19].AsInteger := APCD;
  Dmod.QrUpd.Params[20].AsInteger := 0;//QrAlinItsCodigo.Value;
  Dmod.QrUpd.Params[21].AsInteger := 0;//Codigo;
  Dmod.QrUpd.Params[22].AsInteger := Cliente;
  Dmod.QrUpd.Params[23].AsString  := DDeposito; // Quita automatico at� voltar
  //
  Dmod.QrUpd.Params[24].AsInteger := 0;//-Cliente;
  Dmod.QrUpd.Params[25].AsInteger := Controle;
  Dmod.QrUpd.ExecSQL;
}
  //
  TipoCart := 2;
  Carteira := CO_CARTEIRA_CHQ_BORDERO;
  Controle := 0;
  Sub := 0;
  DmLot.SQL_CH(FatParcela, Comp, Banco, Agencia, Documento, DMais, Dias, FatNum,
    Praca, Tipific, StatusSPC, ContaCorrente, CNPJCPF, Emitente, Vencimento,
    DataDoc, DCompra, DDeposito, Credito, stIns, Cliente,
    TxaCompra, TxaAdValorem, VlrCompra, VlrAdValorem, TxaJuros,
    Data3, TpAlin, AliIts, CHQPgs, Descricao,
    TipoCart, Carteira, Controle, Sub);
  //
  DmLot2.AtualizaEmitente(FormatFloat('000', Banco),
    FormatFloat('0000', Geral.IMV(Agencia)), ContaCorrente, CNPJCPF, Emitente, -1);
  QrSCB.Close;
  ReopenAvulsos(FatParcela);
  EdCliente.SetFocus;
  EdBanda2.Text := '';
  EdCPF_2.Text := '';
  EdEmitente2.Text := '';
  EdValor.ValueVariant := 0;
end;

procedure TFmChequesExtras.EdBanda2Change(Sender: TObject);
begin
  if MLAGeral.LeuTodoCMC7(EdBanda2.Text) then
    EdCMC_7_2.Text := Geral.SoNumero_TT(EdBanda2.Text);
end;

procedure TFmChequesExtras.EdCMC_7_2Change(Sender: TObject);
var
  Banda: TBandaMagnetica;
begin
  if MLAGeral.CalculaCMC7(EdCMC_7_2.Text) = 0 then
  begin
    Banda := TBandaMagnetica.Create;
    Banda.BandaMagnetica := EdCMC_7_2.Text;
    EdRegiaoCompe2.Text  := Banda.Compe;
    EdBanco2.Text        := Banda.Banco;
    EdAgencia2.Text      := Banda.Agencia;
    EdConta2.Text        := Banda.Conta;
    EdCheque2.Text       := Banda.Numero;
    //
    EdCPF_2.SetFocus;
  end else begin
    EdBanco2.Text   := '000';
    EdAgencia2.Text := '0000';
    EdConta2.Text   := '0000000000';
    EdRealCC.Text   := '0000000000';
    EdCheque2.Text  := '000000';
  end;
end;

procedure TFmChequesExtras.EdBanco2Change(Sender: TObject);
begin
  LocalizaEmitente2(True);
  if not EdBanco2.Focused then ReopenBanco2;
end;

procedure TFmChequesExtras.EdBanco2Exit(Sender: TObject);
begin
  LocalizaEmitente2(True);
end;

procedure TFmChequesExtras.EdAgencia2Change(Sender: TObject);
begin
  LocalizaEmitente2(True);
end;

procedure TFmChequesExtras.EdConta2Change(Sender: TObject);
begin
  LocalizaEmitente2(True);
end;

procedure TFmChequesExtras.EdCPF_2Change(Sender: TObject);
begin
  EdCR.ValueVariant := 0;
  EdCV.ValueVariant := 0;
  EdCT.ValueVariant := 0;
  EdDR.ValueVariant := 0;
  EdDV.ValueVariant := 0;
  EdDT.ValueVariant := 0;
  EdRT.ValueVariant := 0;
  EdVT.ValueVariant := 0;
  EdST.ValueVariant := 0;
  EdOA.ValueVariant := 0;
  EdTT.ValueVariant := 0;
end;

procedure TFmChequesExtras.EdCPF_2Exit(Sender: TObject);
begin
  DmLot.PesquisaRiscoSacado(EdCPF_2.Text,
      EdCR, EdCV, EdCT,
      EdDR, EdDV, EdDT,
      EdRT, EdVT, EdST,
      EdOA, EdTT);
end;

procedure TFmChequesExtras.EdCPF_2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
  begin
    Dmod.QrLocCPF.Close;
    Dmod.QrLocCPF.Params[0].AsString := Geral.SoNumero_TT(EdCPF_2.Text);
    UMyMod.AbreQuery(Dmod.QrLocCPF, Dmod.MyDB);
    case Dmod.QrLocCPF.RecordCount of
      0: Application.MessageBox('N�o foi localizado emitente para este CPF!',
        'Aviso', MB_OK+MB_ICONWARNING);
      1: EdEmitente2.Text := Dmod.QrLocCPFNome.Value;
      else begin
        Application.CreateForm(TFmLotEmi, FmLotEmi);
        FmLotEmi.ShowModal;
        if FmLotEmi.FEmitenteNome <> '' then
          EdEmitente2.Text := FmLotEmi.FEmitenteNome;
        FmLotEmi.Destroy;
      end;
    end;
  end;
end;

procedure TFmChequesExtras.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stNil;
  //
  TPData4.Date := 0;
  TPVence4.Date := 0;
  //
  UMyMod.AbreQuery(QrEntidades, Dmod.MyDB);
  ReopenAvulsos(0);
end;

function TFmChequesExtras.LocalizaEmitente2(MudaNome: Boolean): Boolean;
var
  B,A,C: String;
begin
  Result := False;
  if  (EdBanco2.ValueVariant > 0)
  and (EdAgencia2.ValueVariant > 0)
  and (Geral.DMV(EdConta2.Text) > 0.1) then
  begin
    Screen.Cursor := crHourGlass;
    try
      if MudaNome then
      begin
        B := EdBanco2.Text;
        A := EdAgencia2.Text;
        C := EdConta2.Text;
        //
        DmLot2.QrLocEmiBAC.Close;
        DmLot2.QrLocEmiBAC.Params[0].AsInteger := QrBanco2DVCC.Value;
        DmLot2.QrLocEmiBAC.Params[1].AsString  := CriaBAC_Pesq(B,A,C,QrBanco2DVCC.Value);
        UMyMod.AbreQuery(DmLot2.QrLocEmiBAC, Dmod.MyDB);
        //
        if DmLot2.QrLocEmiBAC.RecordCount = 0 then
        begin
          EdEmitente2.Text := '';
          EdCPF_2.Text := '';
        end else begin
          DmLot2.QrLocEmiCPF.Close;
          DmLot2.QrLocEmiCPF.Params[0].AsString := DmLot2.QrLocEmiBACCPF.Value;
          UMyMod.AbreQuery(DmLot2.QrLocEmiCPF, Dmod.MyDB);
          //
          if DmLot2.QrLocEmiCPF.RecordCount = 0 then
          begin
            EdEmitente2.Text := '';
            EdCPF_2.Text := '';
          end else begin
            EdEmitente2.Text := DmLot2.QrLocEmiCPFNome.Value;
            EdCPF_2.Text      := Geral.FormataCNPJ_TT(DmLot2.QrLocEmiCPFCPF.Value);
            DmLot.PesquisaRiscoSacado(EdCPF_2.Text,
              EdCR, EdCV, EdCT,
              EdDR, EdDV, EdDT,
              EdRT, EdVT, EdST,
              EdOA, EdTT);
          end;
        end;
      end;
      Screen.Cursor := crDefault;
      Result := True;
    except
      Screen.Cursor := crDefault;
      raise;
    end;
  end;
end;

procedure TFmChequesExtras.ReopenBanco2;
begin
  QrBanco2.Close;
  QrBanco2.Params[0].AsString := EdBanco2.Text;
  UMyMod.AbreQuery(QrBanco2, Dmod.MyDB);
end;

function TFmChequesExtras.CriaBAC_Pesq(Banco, Agencia, Conta: String;
  DVCC: Integer): String;
begin
  EdRealCC.Text := Copy(Conta, 10-DVCC+1, DVCC);
  Result := Banco+Agencia+EdRealCC.Text;
end;

procedure TFmChequesExtras.ReopenAvulsos(FatParcela: Integer);
begin
{
  QrAvulsos.Close;
  QrAvulsos. Open;
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrAvulsos, Dmod.MyDB, [
  'SELECT loi.Banco, loi.Agencia, loi.ContaCorrente, ',
  'loi.Documento, loi.Credito, loi.DCompra, loi.Vencimento, ',
  'loi.Cliente, loi.Emitente, loi.CNPJCPF, loi.FatParcela, ',
  'loi.FatNum, loi.Carteira, ',
  'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NOMECLI, ',
  'loi.Data, loi.Tipo, loi.Controle, loi.Sub ', 
  'FROM ' + CO_TabLctA + ' loi ',
  'LEFT JOIN entidades cli ON cli.Codigo=loi.Cliente ',
  'WHERE loi.FatID=' + FormatFloat('0', VAR_FATID_0301),
  'AND loi.FatNum=0 ',
  'AND loi.TpAlin=-9 ',
  'ORDER BY loi.Vencimento, loi.DCompra ',
  '']);
  //
  if FatParcela > 0 then QrAvulsos.Locate('FatParcela', FatParcela, []);
end;

procedure TFmChequesExtras.QrAvulsosCalcFields(DataSet: TDataSet);
begin
  QrAvulsosCPF_TXT.Value := Geral.FormataCNPJ_TT(QrAvulsosCNPJCPF.Value);
end;

procedure TFmChequesExtras.EdVence4Exit(Sender: TObject);
var
  Data, Vence: TDateTime;
begin
  Data  := TPData4.Date;
  Vence := TPVence4.Date;
  if Vence > 2 then
  begin
    if Vence < Data then
    begin
      if Geral.MensagemBox(
      'Data do vencimento anterior a emiss�o! Deseja incrementar um ano?',
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
      begin
        Vence := IncMonth(Vence, 12);
        TPVence4.SetFocus;
      end else begin
        TPVence4.SetFocus;
        Exit;
      end;
    end;
    TPVence4.Date := Vence;
  end else TPVence4.SetFocus;
end;

procedure TFmChequesExtras.BitBtn2Click(Sender: TObject);
begin
  CadastraCheque();
end;

procedure TFmChequesExtras.BtExcluiClick(Sender: TObject);
var
  //FatNum: Double;
  FatParcela(*, Carteira*): Integer;
begin
  if Geral.MensagemBox('Confirma a exclus�o deste cheque?',
  'Pergunta de exclus�o', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    //FatNum := QrAvulsosFatNum.Value;
    FatParcela := QrAvulsosFatParcela.Value;
    //Carteira := QrAvulsosCarteira.Value;
    //
{
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM lot esits WHERE Controle=:P0');
    Dmod.QrUpd.Params[0].AsInteger := FatParcela;
    Dmod.QrUpd.ExecSQL;
}
    {N�o usar no Credito2
    UFinanceiro.ExcluiLct_FatParcela(QrAvulsos,  VAR_FATID_0301,
    FatNum, FatParcela, Carteira, CO_TabLctA, True, False);
    }
    UFinanceiro.ExcluiLct_Unico(CO_TabLctA, QrAvulsos.Database,
      QrAvulsosData.Value, QrAvulsosTipo.Value, QrAvulsosCarteira.Value,
      QrAvulsosControle.Value, QrAvulsosSub.Value,
      dmkPF.MotivDel_ValidaCodigo(300), True);
    //
    ReopenAvulsos(FatParcela);
    //
    Screen.Cursor := crDefault;
  end;
end;

end.

