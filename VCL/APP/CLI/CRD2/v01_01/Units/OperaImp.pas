unit OperaImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DBCtrls, Db, mySQLDbTables, ComCtrls,
  frxClass, frxDBSet, Variants, dmkGeral, dmkDBLookupComboBox, dmkEdit,
  dmkEditCB, DmkDAC_PF, dmkImage, UnDmkEnums;

type
  TFmOperaImp = class(TForm)
    PainelDados: TPanel;
    QrClientes: TmySQLQuery;
    DsClientes: TDataSource;
    QrClientesNOMECLIENTE: TWideStringField;
    QrClientesCodigo: TIntegerField;
    QrOpera: TmySQLQuery;
    QrOperaNOMECLIENTE: TWideStringField;
    QrOperaCodigo: TIntegerField;
    QrOperaTipo: TSmallintField;
    QrOperaSpread: TSmallintField;
    QrOperaCliente: TIntegerField;
    QrOperaLote: TSmallintField;
    QrOperaData: TDateField;
    QrOperaTotal: TFloatField;
    QrOperaDias: TFloatField;
    QrOperaPeCompra: TFloatField;
    QrOperaTxCompra: TFloatField;
    QrOperaValValorem: TFloatField;
    QrOperaAdValorem: TFloatField;
    QrOperaIOC: TFloatField;
    QrOperaCPMF: TFloatField;
    QrOperaTipoAdV: TIntegerField;
    QrOperaIRRF: TFloatField;
    QrOperaIRRF_Val: TFloatField;
    QrOperaISS: TFloatField;
    QrOperaISS_Val: TFloatField;
    QrOperaPIS_R: TFloatField;
    QrOperaPIS_R_Val: TFloatField;
    QrOperaLk: TIntegerField;
    QrOperaDataCad: TDateField;
    QrOperaDataAlt: TDateField;
    QrOperaUserCad: TIntegerField;
    QrOperaUserAlt: TIntegerField;
    QrOperaTarifas: TFloatField;
    QrOperaOcorP: TFloatField;
    QrOperaCOFINS_R: TFloatField;
    QrOperaCOFINS_R_Val: TFloatField;
    QrOperaPIS: TFloatField;
    QrOperaPIS_Val: TFloatField;
    QrOperaCOFINS: TFloatField;
    QrOperaCOFINS_Val: TFloatField;
    QrOperaPIS_TOTAL: TFloatField;
    QrOperaCOFINS_TOTAL: TFloatField;
    QrOperaIOC_VAL: TFloatField;
    QrOperaCPMF_VAL: TFloatField;
    QrOperaMaxVencto: TDateField;
    QrOperaCHDevPg: TFloatField;
    QrOperaMINTC: TFloatField;
    QrOperaMINAV: TFloatField;
    QrOperaMINTC_AM: TFloatField;
    QrOperaPIS_T_Val: TFloatField;
    QrOperaCOFINS_T_Val: TFloatField;
    QrOperaPgLiq: TFloatField;
    QrOperaDUDevPg: TFloatField;
    QrOperaNF: TIntegerField;
    QrOperaItens: TIntegerField;
    frxOperacoes: TfrxReport;
    Panel1: TPanel;
    Panel2: TPanel;
    RGPeriodo: TRadioGroup;
    CBDiaSemanaI: TComboBox;
    StaticText1: TStaticText;
    CBDiaSemanaF: TComboBox;
    StaticText2: TStaticText;
    Panel3: TPanel;
    Panel4: TPanel;
    Label3: TLabel;
    EdCliente: TdmkEditCB;
    CbCliente: TdmkDBLookupComboBox;
    TPIni: TDateTimePicker;
    Label34: TLabel;
    Label1: TLabel;
    TPFim: TDateTimePicker;
    Panel5: TPanel;
    QrOperaCNPJCPF: TWideStringField;
    QrOperaIOFd_VAL: TFloatField;
    QrOperaIOFv_VAL: TFloatField;
    QrOperaTipoIOF: TSmallintField;
    QrOperaCNPJCPF_TXT: TWideStringField;
    frxDsOperacoes: TfrxDBDataset;
    QrOperaIOF_TOTAL: TFloatField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel6: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BitBtn1: TBitBtn;
    Panel7: TPanel;
    BtImprime: TBitBtn;
    PB1: TProgressBar;
    GBCorrige: TGroupBox;
    BtCorrige: TBitBtn;
    CkPIS: TCheckBox;
    CkCOFINS: TCheckBox;
    CkISS: TCheckBox;
    CkCorrige: TCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure frxOperacoesGetValue(const VarName: String;
      var Value: Variant);
    procedure QrOperaCalcFields(DataSet: TDataSet);
    procedure BtCorrigeClick(Sender: TObject);
    procedure CkCorrigeClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FArrayMySQLSI: array[0..5] of TmySQLQuery;
    FArrayMySQLLO: array[0..5] of TmySQLQuery;
  end;

  var
  FmOperaImp: TFmOperaImp;

implementation

{$R *.DFM}

uses UnMyObjects, Module, UnInternalConsts, UMySQLModule, MyListas, ModuleLot,
  Principal, ModuleGeral, MyDBCheck;

procedure TFmOperaImp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOperaImp.CkCorrigeClick(Sender: TObject);
begin
  GBCorrige.Visible := CkCorrige.Checked;
end;

procedure TFmOperaImp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOperaImp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOperaImp.FormCreate(Sender: TObject);
var
  Cam: String;
  i, f, k, t: Integer;
  Data: TDateTime;
  Ano, Mes, Dia, DiaI, DiaF: Word;
begin
  UMyMod.AbreQuery(QrClientes, Dmod.MyDB);
  //
  with CBDiaSemanaI.Items do
  begin
    Add(CO_DOMINGO      );
    Add(CO_SEGUNDAFEIRA );
    Add(CO_TERCAFEIRA   );
    Add(CO_QUARTAFEIRA  );
    Add(CO_QUINTAFEIRA  );
    Add(CO_SEXTAFEIRA   );
    Add(CO_SABADO       );
  end;
  with CBDiaSemanaF.Items do
  begin
    Add(CO_DOMINGO      );
    Add(CO_SEGUNDAFEIRA );
    Add(CO_TERCAFEIRA   );
    Add(CO_QUARTAFEIRA  );
    Add(CO_QUINTAFEIRA  );
    Add(CO_SEXTAFEIRA   );
    Add(CO_SABADO       );
  end;
  //
  Cam := Application.Title+'\InitialConfig\'+'OperaImp';
  t := Geral.ReadAppKey('TipPeriod', Cam, ktInteger, 0, HKey_LOCAL_MACHINE);
  i := Geral.ReadAppKey('DiaSemIni', Cam, ktInteger, 0, HKEY_LOCAL_MACHINE);
  f := Geral.ReadAppKey('DiaSemFim', Cam, ktInteger, 6, HKEY_LOCAL_MACHINE);
  RGPeriodo.ItemIndex := t;
  //
  CBDiaSemanaI.ItemIndex := i-1;
  CBDiaSemanaF.ItemIndex := f-1;
  //if t = 0 then
  //begin
    TPIni.Date := Date-7;
    TPFim.Date := Date;
  //end else
  case t of
    1:
    begin
      k := -1;
      Data := Date+1;
      while k <> f-1 do
      begin
        Data := Data -1;
        k := DayOfWeek(Data);
      end;
      TPFim.Date := Data+1;
      TPIni.Date := Data-f+i+1;
    end;
    2:
    begin
      DecodeDate(Date, Ano, Mes, Dia);
      if Dia > 21 then DiaI := 11 else
      if Dia > 11 then DiaI := 01 else
                       DiaI := 21;
      if DiaI = 01 then DiaF := 10 else
      if DiaI = 11 then DiaF := 20 else
         DecodeDate(Geral.UltimoDiaDoMes(IncMonth(Date, -1)), Ano, Mes, DiaF);
      //
      TPIni.Date := EncodeDate(Ano, Mes, DiaI);
      TPFim.Date := EncodeDate(Ano, Mes, DiaF);
    end;
  end;
end;

procedure TFmOperaImp.BtCorrigeClick(Sender: TObject);
  procedure AtualizaImpostos(Imposto, Lote: Integer; TabLctA: String);
  var
    i: Integer;
    PIS, PIS_R, COFINS, COFINS_R, ISS, AdValor, TaxaValX, ISS_Val, PIS_VAL,
    PIS_R_Val, COFINS_Val, COFINS_R_Val, Total, MINTC, MINAV, COFINS_T_VAL,
    PIS_T_VAL: Double;
  begin
    Dmod.ReopenControle;
    //
    Total    := QrOperaTotal.Value;
    PIS      := 0;
    PIS_R    := 0;
    COFINS   := 0;
    COFINS_R := 0;
    ISS      := 0;
    //
    case Imposto of
      0: //PIS
      begin
        PIS   := Dmod.QrControlePIS.Value;
        PIS_R := Dmod.QrControlePIS_R.Value;
        //
        for i := 0 to FmPrincipal.FMyDBs.MaxDBs -1 do
        begin
          if FmPrincipal.FMyDBs.Connected[i] = '1'  then
          begin
            FArrayMySQLSI[i].Close;
            FArrayMySQLSI[i].Params[0].AsFloat := Lote;
            FArrayMySQLSI[i]. Open;
            //
            AdValor   := Total * FArrayMySQLLO[i].FieldByName('AdValorem').AsFloat / 100;
            TaxaValX  := FArrayMySQLSI[i].FieldByName('TaxaVal').AsFloat;
            PIS_Val   := Trunc(PIS * (AdValor+TaxaValX) + 0.5) / 100;
            PIS_R_Val := Trunc(PIS_R * (AdValor+TaxaValX) + 0.5) / 100;
            //
            Dmod.QrUpd_.Database := Dmod.FArrayMySQLBD[i];
            //
            UMyMod.SQLInsUpd(Dmod.QrUpd_, stUpd, 'emlot', False,
            ['AlterWeb', 'PIS_R', 'PIS_R_Val', 'PIS', 'PIS_VAL'], ['Codigo'],
            [1, PIS_R, PIS_R_Val, PIS, PIS_Val], [Lote], False);
          end;
        end;
      end;
      1: //COFINS
      begin
        COFINS   := Dmod.QrControleCOFINS.Value;
        COFINS_R := Dmod.QrControleCOFINS_R.Value;
        //
        for i := 0 to FmPrincipal.FMyDBs.MaxDBs -1 do
        begin
          if FmPrincipal.FMyDBs.Connected[i] = '1'  then
          begin
            FArrayMySQLSI[i].Close;
            FArrayMySQLSI[i].Params[0].AsFloat := Lote;
            FArrayMySQLSI[i]. Open;
            //
            AdValor      := Total * FArrayMySQLLO[i].FieldByName('AdValorem').AsFloat / 100;
            TaxaValX     := FArrayMySQLSI[i].FieldByName('TaxaVal').AsFloat;
            COFINS_Val   := Trunc(COFINS * (AdValor+TaxaValX)+0.5)/100;
            COFINS_R_Val := Trunc(COFINS_R * (AdValor+TaxaValX)+0.5)/100;
            //
            Dmod.QrUpd_.Database := Dmod.FArrayMySQLBD[i];
            //
            UMyMod.SQLInsUpd(Dmod.QrUpd_, stUpd, 'emlot', False,
            ['AlterWeb', 'COFINS_R', 'COFINS_R_VAL', 'COFINS', 'COFINS_VAL'], ['Codigo'],
            [1, COFINS_R, COFINS_R_Val, COFINS, COFINS_Val], [Lote], False);
          end;
        end;
      end;
      2: //ISS
      begin
        ISS := Dmod.QrControleISS.Value;
        //
        for i := 0 to FmPrincipal.FMyDBs.MaxDBs -1 do
        begin
          if FmPrincipal.FMyDBs.Connected[i] = '1'  then
          begin
            FArrayMySQLSI[i].Close;
            FArrayMySQLSI[i].Params[0].AsFloat := Lote;
            FArrayMySQLSI[i]. Open;
            //
            AdValor := Total * FArrayMySQLLO[i].FieldByName('AdValorem').AsFloat / 100;
            ISS_Val := Trunc(ISS * AdValor+0.5)/100;
            //
            Dmod.QrUpd_.Database := Dmod.FArrayMySQLBD[i];
            //
            UMyMod.SQLInsUpd(Dmod.QrUpd_, stUpd, 'emlot', False, ['AlterWeb',
            'ISS', 'ISS_Val'], ['Codigo'], [1, ISS, ISS_Val], [Lote], False);
          end;
        end;
      end;
    end;
    ///////// M�nimos ///////////////////
    MINAV := QrOperaMINAV.Value;
    MINTC := QrOperaMINTC.Value;
    //
    case Imposto of
      0: //PIS
      begin
        PIS_Val   := Trunc(PIS * (MINAV+MINTC)+0.5)/100;
        PIS_R_Val := Trunc(PIS_R * (MINAV+MINTC)+0.5)/100;
        PIS_T_Val := Trunc((PIS+PIS_R) * (MINAV+MINTC)+0.5)/100;
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_TabLotA, False,
        ['PIS_R', 'PIS_R_Val', 'PIS', 'PIS_Val', 'PIS_T_Val'], ['Codigo'],
        [PIS_R, PIS_R_Val, PIS, PIS_VAL, PIS_T_Val], [Lote], True);
      end;
      1: //COFINS
      begin
        COFINS_Val   := Trunc(COFINS * (MINAV+MINTC)+0.5)/100;
        COFINS_R_Val := Trunc(COFINS_R * (MINAV+MINTC)+0.5)/100;
        COFINS_T_Val := Trunc((COFINS + COFINS_R) * (MINAV+MINTC)+0.5)/100;
       //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_TabLotA, False,
        ['COFINS', 'COFINS_VAL', 'COFINS_R', 'COFINS_R_VAL', 'COFINS_T_Val'],
        ['Codigo'], [COFINS, COFINS_VAL, COFINS_R, COFINS_R_VAL, COFINS_T_Val],
        [Lote], True);
      end;
      2: //ISS
      begin
        ISS_Val := Trunc(ISS * MINAV+0.5)/100;
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_TabLotA, False,
        ['ISS', 'ISS_Val'], ['Codigo'], [ISS, ISS_Val], [Lote], True);
      end;
    end;
  end;
var
  TabLctA: String;
  Codigo, CliInt: Integer;
begin
  if (QrOpera.State <> dsInactive) and (QrOpera.RecordCount > 0) then
  begin
    if not DBCheck.LiberaPelaSenhaBoss then Exit;
    //
    if Geral.MensagemBox('Voc� realmente deseja recalcular os impostos dos lan�amentos do per�odo selecionado?',
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES
    then
      Exit;
    //
    try
      Screen.Cursor := crHourGlass;
      //
      DmLot.QrEmLot1.Database := Dmod.MyDB1;
      DmLot.QrEmLot2.Database := Dmod.MyDB2;
      DmLot.QrEmLot3.Database := Dmod.MyDB3;
      DmLot.QrEmLot4.Database := Dmod.MyDB4;
      DmLot.QrEmLot5.Database := Dmod.MyDB5;
      DmLot.QrEmLot6.Database := Dmod.MyDB6;
      //
      DmLot.QrSum1.Database := Dmod.MyDB1;
      DmLot.QrSum2.Database := Dmod.MyDB2;
      DmLot.QrSum3.Database := Dmod.MyDB3;
      DmLot.QrSum4.Database := Dmod.MyDB4;
      DmLot.QrSum5.Database := Dmod.MyDB5;
      DmLot.QrSum6.Database := Dmod.MyDB6;
      //
      FArrayMySQLLO[00] := DmLot.QrEmLot1;
      FArrayMySQLLO[01] := DmLot.QrEmLot2;
      FArrayMySQLLO[02] := DmLot.QrEmLot3;
      FArrayMySQLLO[03] := DmLot.QrEmLot4;
      FArrayMySQLLO[04] := DmLot.QrEmLot5;
      FArrayMySQLLO[05] := DmLot.QrEmLot6;
      //
      FArrayMySQLSI[00] := DmLot.QrSum1;
      FArrayMySQLSI[01] := DmLot.QrSum2;
      FArrayMySQLSI[02] := DmLot.QrSum3;
      FArrayMySQLSI[03] := DmLot.QrSum4;
      FArrayMySQLSI[04] := DmLot.QrSum5;
      FArrayMySQLSI[05] := DmLot.QrSum6;
      //
      PB1.Visible  := True;
      PB1.Max      := QrOpera.RecordCount;
      PB1.Position := 0;
      //
      CliInt   := DModG.EmpresaAtual_ObtemCodigo(tecCliInt);
      TabLctA  := DModG.NomeTab(TMeuDB, ntLct, False, ttA, CliInt);
      //
      QrOpera.First;
      while not QrOpera.Eof do
      begin
        Codigo := QrOperaCodigo.Value;
        //
        if CkPIS.Checked then
          AtualizaImpostos(0, Codigo, TabLctA);
        if CkCOFINS.Checked then
          AtualizaImpostos(1, Codigo, TabLctA);
        if CkISS.Checked then
          AtualizaImpostos(2, Codigo, TabLctA);
        //
        Application.ProcessMessages;
        PB1.Position := PB1.Position + 1;
        //
        QrOpera.Next;
      end;
      PB1.Visible := False;
      Geral.MensagemBox('Corre��o conclu�da!', 'Aviso', MB_OK+MB_ICONWARNING);
    finally
      Screen.Cursor := crDefault;
    end;
  end else
    Geral.MensagemBox('Voc� primeiro deve gerar o relat�rio para conferir os lan�amentos / per�odo a ser corrigido!',
      'Aviso', MB_OK+MB_ICONWARNING);
end;

procedure TFmOperaImp.BtImprimeClick(Sender: TObject);
var
  Cliente: Integer;
begin
  Cliente := EdCliente.ValueVariant;
  QrOpera.Close;
  QrOpera.SQL.Clear;
  QrOpera.SQL.Add('SELECT lo.*, IF(en.Tipo=0, en.RazaoSocial, en.Nome)NOMECLIENTE,');
  QrOpera.SQL.Add('IF(en.Tipo=0, en.CNPJ, en.CPF) CNPJCPF');
  QrOpera.SQL.Add('FROM ' + CO_TabLotA + ' lo');
  QrOpera.SQL.Add('LEFT JOIN entidades en ON en.Codigo=lo.Cliente');
  QrOpera.SQL.Add('');
  QrOpera.SQL.Add('');
  QrOpera.SQL.Add('WHERE lo.data BETWEEN :P0 AND :P1');
  QrOpera.SQL.Add('AND (TxCompra + ValValorem) >= 0.01');
  if Cliente <> 0 then
    QrOpera.SQL.Add('AND lo.Cliente='+IntToStr(Cliente));
  QrOpera.SQL.Add('ORDER BY lo.NF, lo.Data, NOMECLIENTE, lo.Lote');
  //
  QrOpera.Params[00].AsString := FormatDateTime(VAR_FORMATDATE, TPIni.Date);
  QrOpera.Params[01].AsString := FormatDateTime(VAR_FORMATDATE, TPFim.Date);
  UMyMod.AbreQuery(QrOpera, Dmod.MyDB);
  //
  MyObjects.frxMostra(frxOperacoes, 'Relat�rio de opera��es');
end;

procedure TFmOperaImp.FormClose(Sender: TObject; var Action: TCloseAction);
var
  Cam: String;
begin
  Cam := Application.Title+'\InitialConfig\'+'OperaImp';
  Geral.WriteAppKey('DiaSemIni', Cam, CBDiaSemanaI.ItemIndex+1, ktInteger, HKey_LOCAL_MACHINE);
  Geral.WriteAppKey('DiaSemFim', Cam, CBDiaSemanaF.ItemIndex+1, ktInteger, HKey_LOCAL_MACHINE);
  Geral.WriteAppKey('TipPeriod', Cam, RGPeriodo.ItemIndex, ktInteger, HKey_LOCAL_MACHINE);
end;

procedure TFmOperaImp.frxOperacoesGetValue(const VarName: String;
  var Value: Variant);
begin
  if AnsiCompareText(VarName, 'VARF_CLIENTE') = 0 then
  begin
    if CbCliente.KeyValue = NULL then Value := 'TODOS' else
    Value := CBCliente.Text;
  end else if AnsiCompareText(VarName, 'VARF_PERIODO') = 0 then Value :=
    FormatDateTime(VAR_FORMATDATE3, TPIni.Date) + CO_ATE +
    FormatDateTime(VAR_FORMATDATE3, TPFim.Date);
end;

procedure TFmOperaImp.QrOperaCalcFields(DataSet: TDataSet);
begin
  QrOperaCNPJCPF_TXT.Value := Geral.FormataCNPJ_TT(QrOperaCNPJCPF.Value);
  QrOperaIOF_TOTAL.Value := QrOperaIOC_VAL.Value +
    QrOperaIOFd_VAL.Value + QrOperaIOFv_VAL.Value;
end;

end.

