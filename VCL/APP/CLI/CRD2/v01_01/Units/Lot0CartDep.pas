unit Lot0CartDep;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Db, mySQLDbTables, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkGeral, dmkImage, UnDmkEnums;

type
  TFmLot0CartDep = class(TForm)
    Panel1: TPanel;
    QrCarteiras: TmySQLQuery;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    QrCarteirasBanco1: TIntegerField;
    QrCarteirasAgencia1: TIntegerField;
    QrCarteirasConta1: TWideStringField;
    DsCarteiras: TDataSource;
    EdCartDep: TdmkEditCB;
    LaCartDep1: TLabel;
    CBCartDep: TdmkDBLookupComboBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmLot0CartDep: TFmLot0CartDep;

implementation

uses UnMyObjects, Module, Principal, Lot2Cab, UMySQLModule, MyListas,
  UnInternalConsts;

{$R *.DFM}

procedure TFmLot0CartDep.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLot0CartDep.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLot0CartDep.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLot0CartDep.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stIns;
  //
  UMyMod.AbreQuery(QrCarteiras, Dmod.MyDB);
end;

procedure TFmLot0CartDep.BtOKClick(Sender: TObject);
var
  Controle: Integer;
begin
  case FmPrincipal.FFormLotShow of
{
    0:
    begin
      Dmod.QrUpd.Params[01].AsInteger := FmLot0.QrLot esCodigo.Value;
      Dmod.QrUpd.ExecSQL;
      if FmLot0.QrLotIts.State = dsBrowse then
      begin
        Controle := FmLot0.QrLotItsFatParcela.Value;
        FmLot0.QrLotIts.Close;
        UMyMod.AbreQuery(FmLot0.QrLotIts);
        FmLot0.QrLotIts.Locate('Controle', Controle, []);
      end;
    end;
    1:
    begin
      Dmod.QrUpd.Params[01].AsInteger := FmLot1.QrLotCodigo.Value;
      Dmod.QrUpd.ExecSQL;
      if FmLot1.QrLotIts.State = dsBrowse then
      begin
        Controle := FmLot1.QrLotItsFatParcela.Value;
        FmLot1.QrLotIts.Close;
        UMyMod.AbreQuery(FmLot1.QrLotIts);
        FmLot1.QrLotIts.Locate('Controle', Controle, []);
      end;
    end;
}
    2:
    begin
{
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE Lot esIts SET CartDep=:P0 WHERE Codigo=:P1');
      Dmod.QrUpd.Params[00].AsInteger := EdCartDep.ValueVariant;
      Dmod.QrUpd.Params[01].AsInteger := FmLot2Cab.QrLotCodigo.Value;
      Dmod.QrUpd.ExecSQL;
}
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_TabLctA, False, [
      'CartDep'], [
      'FatID', 'FatNum'],
      [EdCartDep.ValueVariant], [
      VAR_FATID_0301, FmLot2Cab.QrLotCodigo.Value], True);
      //
      if FmLot2Cab.QrLotIts.State = dsBrowse then
      begin
        Controle := FmLot2Cab.QrLotItsFatParcela.Value;
        FmLot2Cab.QrLotIts.Close;
        UMyMod.AbreQuery(FmLot2Cab.QrLotIts, Dmod.MyDB);
        FmLot2Cab.QrLotIts.Locate('Controle', Controle, []);
      end;
    end;
    else Geral.MensagemBox('Janela de gerenciamento de border�s ' +
    'n�o definida! (13)', 'ERRO', MB_OK+MB_ICONERROR);
  end;
  Close;
end;

end.
