unit PesqCPFCNPJ;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, Db, mySQLDbTables,
  dmkGeral, dmkEdit, dmkImage, UnDmkEnums;

type
  TFmPesqCPFCNPJ = class(TForm)
    Panel1: TPanel;
    RGMascara: TRadioGroup;
    Label1: TLabel;
    EdEmitente: TdmkEdit;
    DBGrid1: TDBGrid;
    QrEmitCPF: TmySQLQuery;
    DsEmitCPF: TDataSource;
    QrEmitCPFNome: TWideStringField;
    QrEmitCPFCPF: TWideStringField;
    QrEmitCPFTipo: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel2: TPanel;
    BtPesquisa: TBitBtn;
    BtSaida: TBitBtn;
    BtConfirma: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure RGMascaraClick(Sender: TObject);
    procedure EdEmitenteChange(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmPesqCPFCNPJ: TFmPesqCPFCNPJ;

implementation

uses UnMyObjects, Module, UnInternalConsts, UMySQLModule;

{$R *.DFM}

procedure TFmPesqCPFCNPJ.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPesqCPFCNPJ.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPesqCPFCNPJ.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
end;

procedure TFmPesqCPFCNPJ.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPesqCPFCNPJ.BtPesquisaClick(Sender: TObject);
var
  Emitente: String;
begin
  //if Trim(EdEmitente.Text) <> '' then
  //begin
    Emitente := EdEmitente.Text;
    if RGMascara.ItemIndex in ([0,1]) then Emitente := '%'+Emitente;
    if RGMascara.ItemIndex in ([0,2]) then Emitente := Emitente+'%';
  //end else Emitente := '';
  //////////////////////////////////////////////////////////////////////////////
  QrEmitCPF.Close;
  QrEmitCPF.Params[0].AsString := Emitente;
  QrEmitCPF.Params[1].AsString := Emitente;
  UMyMod.AbreQuery(QrEmitCPF, Dmod.MyDB);
  //
  BtPesquisa.Enabled := False;
  BtConfirma.Enabled := True;
  //configurar grade
end;

procedure TFmPesqCPFCNPJ.RGMascaraClick(Sender: TObject);
begin
  BtPesquisa.Enabled := True;
  BtConfirma.Enabled := False;
end;

procedure TFmPesqCPFCNPJ.EdEmitenteChange(Sender: TObject);
begin
  BtPesquisa.Enabled := True;
  BtConfirma.Enabled := False;
end;

procedure TFmPesqCPFCNPJ.DBGrid1DblClick(Sender: TObject);
begin
  BtConfirmaClick(Self);
end;

procedure TFmPesqCPFCNPJ.BtConfirmaClick(Sender: TObject);
begin
  VAR_CPF_PESQ := Geral.FormataCNPJ_TT(QrEmitCPFCPF.Value);
  Close;
end;

end.
