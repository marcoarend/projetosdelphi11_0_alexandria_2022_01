object FmLSincro: TFmLSincro
  Left = 339
  Top = 185
  Caption = 'CDR-GERAL-004 :: Adi'#231#227'o de Dados Externos'
  ClientHeight = 364
  ClientWidth = 577
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 577
    Height = 202
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Label1: TLabel
      Left = 248
      Top = 8
      Width = 115
      Height = 13
      Caption = 'Tabela fonte dos dados:'
    end
    object Label2: TLabel
      Left = 284
      Top = 164
      Width = 34
      Height = 13
      Caption = 'Incluiu:'
    end
    object Label3: TLabel
      Left = 344
      Top = 164
      Width = 45
      Height = 13
      Caption = 'N'#227'o incl.:'
    end
    object GroupBox1: TGroupBox
      Left = 1
      Top = 1
      Width = 224
      Height = 188
      Caption = ' Banco de dados externo (Fonte dos dados): '
      TabOrder = 0
      object Label32: TLabel
        Left = 8
        Top = 16
        Width = 42
        Height = 13
        Caption = 'Servidor:'
      end
      object Label33: TLabel
        Left = 8
        Top = 56
        Width = 39
        Height = 13
        Caption = 'Usu'#225'rio:'
      end
      object Label34: TLabel
        Left = 8
        Top = 96
        Width = 34
        Height = 13
        Caption = 'Senha:'
      end
      object Label35: TLabel
        Left = 8
        Top = 136
        Width = 81
        Height = 13
        Caption = 'Banco de dados:'
      end
      object EdWeb_Host: TdmkEdit
        Left = 8
        Top = 32
        Width = 201
        Height = 21
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdWeb_User: TdmkEdit
        Left = 8
        Top = 72
        Width = 201
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdWeb_Pwd: TdmkEdit
        Left = 8
        Top = 112
        Width = 201
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdWeb_DB: TdmkEdit
        Left = 8
        Top = 152
        Width = 201
        Height = 21
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
    end
    object RGTabela: TRadioGroup
      Left = 248
      Top = 52
      Width = 313
      Height = 105
      Caption = ' Tabela a atualizar: '
      Items.Strings = (
        'CPFs (Emitentes)'
        'BACs (Emitentes)')
      TabOrder = 1
    end
    object EdTabSource: TEdit
      Left = 248
      Top = 24
      Width = 121
      Height = 21
      TabOrder = 2
    end
    object EdA: TdmkEdit
      Left = 284
      Top = 180
      Width = 57
      Height = 21
      Alignment = taRightJustify
      ReadOnly = True
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
    object EdB: TdmkEdit
      Left = 344
      Top = 180
      Width = 57
      Height = 21
      Alignment = taRightJustify
      ReadOnly = True
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 250
    Width = 577
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 573
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 294
    Width = 577
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 431
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BitBtn1: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 429
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Importa'
        TabOrder = 0
        OnClick = BtOKClick
        NumGlyphs = 2
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 577
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 529
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 481
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 322
        Height = 32
        Caption = 'Adi'#231#227'o de Dados Externos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 322
        Height = 32
        Caption = 'Adi'#231#227'o de Dados Externos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 322
        Height = 32
        Caption = 'Adi'#231#227'o de Dados Externos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object QrF: TmySQLQuery
    Database = Dmod.MyDB
    Left = 304
    Top = 257
  end
  object QrS: TmySQLQuery
    Database = DBx
    Left = 276
    Top = 257
  end
  object DBx: TmySQLDatabase
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    DatasetOptions = []
    Left = 248
    Top = 256
  end
end
