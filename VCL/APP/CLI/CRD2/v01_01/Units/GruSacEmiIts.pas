unit GruSacEmiIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Mask, UnDmkEnums;

type
  TFmGruSacEmiIts = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    Label3: TLabel;
    EdCPF1: TdmkEdit;
    DBEdEmitente: TDBEdit;
    EdNomeEmiSac: TdmkEdit;
    QrEmiSac: TmySQLQuery;
    QrEmiSacNome: TWideStringField;
    QrEmiSacCPF: TWideStringField;
    QrEmiSacTipo: TWideStringField;
    DsEmiSac: TDataSource;
    QrLoc: TmySQLQuery;
    QrLocCNPJ_CPF: TWideStringField;
    CkContinuar: TCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure DBEdEmitenteChange(Sender: TObject);
    procedure EdCPF1Change(Sender: TObject);
    procedure EdCPF1Exit(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenEmiSac(CPF_CNPJ: String; Reabre: Boolean);
  public
    { Public declarations }
  end;

  var
  FmGruSacEmiIts: TFmGruSacEmiIts;

implementation

uses UnMyObjects, Module, GruSacEmi, PesqCPFCNPJ, UMySQLModule;

{$R *.DFM}

procedure TFmGruSacEmiIts.BtOKClick(Sender: TObject);
var
  Nome, CNPJ_CPF: String;
  Codigo: Integer;
begin
  Nome     := EdNomeEmiSac.Text;
  CNPJ_CPF := Geral.SoNumero_TT(EdCPF1.Text);
  Codigo   := FmGruSacEmi.QrGruSacEmiCodigo.Value;
  //
  if ImgTipo.SQLType = stIns then
  begin
    QrLoc.Close;
    QrLoc.Params[00].AsString  := CNPJ_CPF;
    QrLoc.Params[01].AsInteger := Codigo;
    UMyMod.AbreQuery(QrLoc, Dmod.MyDB);
    //
    if MyObjects.FIC(QrLoc.RecordCount > 0, EdCPF1,
      'Este CNPJ / CPF j� est� cadastrado no grupo selecionado') then
      Exit;
  end;
  if MyObjects.FIC(EdNomeEmiSac.Text = '', EdNomeEmiSac, 'Informe um nome!') then
    Exit;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'grusacemiits', False, [
  'Nome'], ['Codigo', 'CNPJ_CPF'], [
  Nome], [Codigo, CNPJ_CPF], False) then
  begin
    FmGruSacEmi.ReopenGruSacEmiIts();
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType := stIns;
      EdCPF1.Text := '';
      EdNomeEmiSac.Text := '';
      //
      EdCPF1.ReadOnly := False;
      EdCPF1.SetFocus;
    end else
      Close;
  end;
end;

procedure TFmGruSacEmiIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGruSacEmiIts.DBEdEmitenteChange(Sender: TObject);
begin
  EdNomeEmiSac.Text := DBEdEmitente.Text;
end;

procedure TFmGruSacEmiIts.EdCPF1Change(Sender: TObject);
var
  //Num,
  CPF: String;
begin
  CPF := Geral.SoNumero_TT(EdCPF1.Text);
  if (CPF <> '') and (CPF = Geral.CalculaCNPJCPF(CPF)) then
    ReopenEmiSac(CPF, True)
  else
    ReopenEmiSac(CPF, False);
end;

procedure TFmGruSacEmiIts.EdCPF1Exit(Sender: TObject);
begin
  ReopenEmiSac(EdCPF1.Text, True);
end;

procedure TFmGruSacEmiIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  if EdCPF1.ReadOnly then
    EdNomeEmiSac.SetFocus;
end;

procedure TFmGruSacEmiIts.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F8 then
  begin
    Application.CreateForm(TFmPesqCPFCNPJ, FmPesqCPFCNPJ);
    FmPesqCPFCNPJ.ShowModal;
    FmPesqCPFCNPJ.Destroy;
    if VAR_CPF_PESQ <> '' then
    begin
      EdCPF1.Text := VAR_CPF_PESQ;
    end;
    ReopenEmiSac(VAR_CPF_PESQ, True);
  end;
end;

procedure TFmGruSacEmiIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGruSacEmiIts.ReopenEmiSac(CPF_CNPJ: String; Reabre: Boolean);
begin
  QrEmiSac.Close;
  QrEmiSac.Params[0].AsString := Geral.SoNumero_TT(CPF_CNPJ);
  QrEmiSac.Params[1].AsString := Geral.SoNumero_TT(CPF_CNPJ);
  if Reabre then
    UMyMod.AbreQuery(QrEmiSac, Dmod.MyDB);
end;

end.
