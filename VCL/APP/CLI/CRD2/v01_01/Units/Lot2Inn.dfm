object FmLot2Inn: TFmLot2Inn
  Left = 339
  Top = 185
  Caption = 'BDR-GEREN-005 :: Lote - Importa'#231#227'o'
  ClientHeight = 666
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 214
        Height = 32
        Caption = 'Lote - Importa'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 214
        Height = 32
        Caption = 'Lote - Importa'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 214
        Height = 32
        Caption = 'Lote - Importa'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 596
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 1
    object Panel9: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 53
      Align = alClient
      TabOrder = 0
      object Label33: TLabel
        Left = 8
        Top = 1
        Width = 123
        Height = 13
        Caption = 'C'#243'digo e nome do cliente:'
      end
      object EdCodCli: TdmkEdit
        Left = 8
        Top = 17
        Width = 65
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnChange = EdCodCliChange
      end
      object EdNomCli: TdmkEdit
        Left = 76
        Top = 17
        Width = 501
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        OnChange = EdNomCliChange
      end
      object Panel10: TPanel
        Left = 576
        Top = 1
        Width = 427
        Height = 51
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 2
        object Label48: TLabel
          Left = 8
          Top = 0
          Width = 91
          Height = 13
          Caption = 'Data da transa'#231#227'o:'
        end
        object Label49: TLabel
          Left = 124
          Top = 0
          Width = 107
          Height = 13
          Caption = 'Cheques + Duplicatas:'
        end
        object Label50: TLabel
          Left = 216
          Top = 0
          Width = 50
          Height = 13
          Caption = 'Valor total:'
        end
        object TPCheque: TdmkEditDateTimePicker
          Left = 8
          Top = 16
          Width = 112
          Height = 21
          Date = 38668.514693287000000000
          Time = 38668.514693287000000000
          TabOrder = 0
          OnChange = TPChequeChange
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
        object EdQtdeTO: TdmkEdit
          Left = 272
          Top = 0
          Width = 41
          Height = 21
          Alignment = taRightJustify
          ReadOnly = True
          TabOrder = 1
          Visible = False
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
        object EdValorTO: TdmkEdit
          Left = 296
          Top = 0
          Width = 25
          Height = 21
          Alignment = taRightJustify
          ReadOnly = True
          TabOrder = 2
          Visible = False
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
        object BtFechar: TBitBtn
          Tag = 15
          Left = 296
          Top = 5
          Width = 120
          Height = 40
          Caption = '&Volta'
          TabOrder = 3
          OnClick = BtFecharClick
        end
        object DBEdit27: TDBEdit
          Left = 124
          Top = 16
          Width = 60
          Height = 21
          DataField = 'TOTAL_Q'
          DataSource = DmLot2.DsSDUs
          TabOrder = 4
        end
        object DBEdit28: TDBEdit
          Left = 188
          Top = 16
          Width = 100
          Height = 21
          DataField = 'TOTAL_V'
          DataSource = DmLot2.DsSDUs
          TabOrder = 5
        end
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 488
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 488
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 488
        Align = alClient
        TabOrder = 0
        object Pagina: TPageControl
          Left = 2
          Top = 15
          Width = 1004
          Height = 376
          ActivePage = TabSheet1
          Align = alTop
          TabOrder = 0
          object TabSheet1: TTabSheet
            Caption = 'C&heques'
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel5: TPanel
              Left = 0
              Top = 0
              Width = 996
              Height = 348
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Panel12: TPanel
                Left = 0
                Top = 0
                Width = 996
                Height = 348
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Splitter1: TSplitter
                  Left = 0
                  Top = 242
                  Width = 996
                  Height = 3
                  Cursor = crVSplit
                  Align = alBottom
                  ExplicitWidth = 1006
                end
                object GradeC_L: TDBGrid
                  Left = 0
                  Top = 187
                  Width = 996
                  Height = 55
                  Align = alClient
                  DataSource = DmLot2.DsLCHs
                  TabOrder = 0
                  TitleFont.Charset = ANSI_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  OnDrawColumnCell = GradeC_LDrawColumnCell
                  OnTitleClick = GradeC_LTitleClick
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'Ordem'
                      Title.Caption = 'Seq.'
                      Width = 24
                      Visible = True
                    end
                    item
                      Alignment = taCenter
                      Expanded = False
                      FieldName = 'NOMEDUPLICADO'
                      Title.Alignment = taCenter
                      Title.Caption = 'D'
                      Width = 15
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'CPF_TXT'
                      Title.Caption = 'CNPJ / CPF'
                      Width = 128
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Emitente'
                      Width = 200
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'TEXTO_SPC'
                      Title.Caption = 'Consulta ao SPC'
                      Width = 240
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Valor'
                      Width = 72
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'DVence'
                      Title.Caption = 'Vencimento'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'DIAS'
                      Title.Caption = 'Dias'
                      Width = 34
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'RISCO'
                      Title.Caption = 'Lim.Operac.'
                      Width = 72
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Aberto'
                      Title.Caption = 'Risco atual'
                      Width = 72
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'AberCH'
                      Title.Caption = 'Ch.A'
                      Width = 32
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Disponiv'
                      Title.Caption = 'Dispon'#237'vel'
                      Width = 72
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'QtdeCH'
                      Title.Caption = 'Ch.D'
                      Width = 32
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'OcorrT'
                      Title.Caption = 'Val.Devolv.'
                      Width = 68
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'OcorrA'
                      Title.Caption = 'Saldo Dev.'
                      Width = 68
                      Visible = True
                    end>
                end
                object PGAbertos_CH: TPageControl
                  Left = 0
                  Top = 245
                  Width = 996
                  Height = 103
                  ActivePage = TabSheet3
                  Align = alBottom
                  TabOrder = 1
                  object TabSheet3: TTabSheet
                    Caption = ' Devolu'#231#245'es '
                    ExplicitLeft = 0
                    ExplicitTop = 0
                    ExplicitWidth = 0
                    ExplicitHeight = 0
                    object GradeDev: TDBGrid
                      Left = 0
                      Top = 0
                      Width = 988
                      Height = 75
                      Align = alClient
                      DataSource = DmLot.DsAlinIts
                      TabOrder = 0
                      TitleFont.Charset = ANSI_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -11
                      TitleFont.Name = 'MS Sans Serif'
                      TitleFont.Style = []
                      OnDrawColumnCell = GradeDevDrawColumnCell
                      Columns = <
                        item
                          Expanded = False
                          FieldName = 'DATA1_TXT'
                          Title.Caption = '1'#170' Devol.'
                          Width = 56
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'Alinea1'
                          Title.Caption = 'Ali 1'
                          Width = 24
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'NOMECLIENTE'
                          Title.Caption = 'Cliente'
                          Width = 120
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'Emitente'
                          Title.Caption = 'Nome emitente'
                          Width = 120
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'CPF_TXT'
                          Title.Caption = 'CPF / CNPJ emitente'
                          Width = 120
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'Banco'
                          Width = 35
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'Agencia'
                          Title.Caption = 'Ag'#234'ncia'
                          Width = 42
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'Conta'
                          Width = 98
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'Cheque'
                          Width = 45
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'DATA2_TXT'
                          Title.Caption = '2'#170' Devol.'
                          Width = 56
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'Alinea2'
                          Title.Caption = 'Ali 2'
                          Width = 24
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'Valor'
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'Taxas'
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'DATA3_TXT'
                          Title.Caption = #218'lt.Pgto'
                          Width = 56
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'ValPago'
                          Title.Caption = 'Pago'
                          Visible = True
                        end>
                    end
                  end
                  object TabSheet4: TTabSheet
                    Caption = ' Abertos '
                    ImageIndex = 1
                    ExplicitLeft = 0
                    ExplicitTop = 0
                    ExplicitWidth = 0
                    ExplicitHeight = 0
                    object DBGrid3: TDBGrid
                      Left = 0
                      Top = 0
                      Width = 988
                      Height = 75
                      Align = alClient
                      DataSource = DmLot.DsCHOpen
                      TabOrder = 0
                      TitleFont.Charset = ANSI_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -11
                      TitleFont.Name = 'MS Sans Serif'
                      TitleFont.Style = []
                      Columns = <
                        item
                          Expanded = False
                          FieldName = 'Banco'
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'Agencia'
                          Title.Caption = 'Ag'#234'ncia'
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'Conta'
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'DCompra'
                          Title.Caption = 'D.Compra'
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'Valor'
                          Title.Alignment = taRightJustify
                          Width = 80
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'DDeposito'
                          Title.Caption = 'Vencimento'
                          Visible = True
                        end>
                    end
                  end
                  object TabSheet33: TTabSheet
                    Caption = ' SPC / SERASA '
                    ImageIndex = 2
                    ExplicitLeft = 0
                    ExplicitTop = 0
                    ExplicitWidth = 0
                    ExplicitHeight = 0
                    object Panel49: TPanel
                      Left = 0
                      Top = 0
                      Width = 988
                      Height = 75
                      Align = alClient
                      BevelOuter = bvNone
                      ParentBackground = False
                      TabOrder = 0
                      object DBGrid23: TDBGrid
                        Left = 0
                        Top = 17
                        Width = 988
                        Height = 58
                        Align = alClient
                        DataSource = DmLot.DsSPC_Result
                        TabOrder = 0
                        TitleFont.Charset = ANSI_CHARSET
                        TitleFont.Color = clWindowText
                        TitleFont.Height = -11
                        TitleFont.Name = 'MS Sans Serif'
                        TitleFont.Style = []
                        Columns = <
                          item
                            Expanded = False
                            FieldName = 'Linha'
                            Width = 34
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'Texto'
                            Font.Charset = DEFAULT_CHARSET
                            Font.Color = clWindowText
                            Font.Height = -11
                            Font.Name = 'Courier New'
                            Font.Style = []
                            Width = 790
                            Visible = True
                          end>
                      end
                      object StCPF_CH: TStaticText
                        Left = 0
                        Top = 0
                        Width = 320
                        Height = 17
                        Align = alTop
                        Alignment = taCenter
                        Caption = 'AGUARDE... Consultado CPF / CNPJ 111.111.111-11...'
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clHotLight
                        Font.Height = -11
                        Font.Name = 'MS Sans Serif'
                        Font.Style = [fsBold]
                        ParentFont = False
                        TabOrder = 1
                        Visible = False
                      end
                    end
                  end
                end
                object Panel7: TPanel
                  Left = 0
                  Top = 0
                  Width = 996
                  Height = 126
                  Align = alTop
                  BevelOuter = bvNone
                  TabOrder = 3
                  object Panel42: TPanel
                    Left = 0
                    Top = 0
                    Width = 758
                    Height = 126
                    Align = alClient
                    BevelOuter = bvNone
                    TabOrder = 0
                    object Panel43: TPanel
                      Left = 0
                      Top = 0
                      Width = 758
                      Height = 86
                      Align = alClient
                      BevelOuter = bvNone
                      ParentBackground = False
                      TabOrder = 0
                      object Label29: TLabel
                        Left = 100
                        Top = 4
                        Width = 116
                        Height = 13
                        Caption = 'Quantidade e valor total:'
                      end
                      object Label131: TLabel
                        Left = 716
                        Top = 8
                        Width = 89
                        Height = 13
                        Caption = 'Vers'#227'o do arquivo:'
                      end
                      object LaVerSuit1: TLabel
                        Left = 716
                        Top = 24
                        Width = 41
                        Height = 13
                        Caption = '0.0.0.0'
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -11
                        Font.Name = 'MS Sans Serif'
                        Font.Style = [fsBold]
                        ParentFont = False
                      end
                      object BtConfirma1: TBitBtn
                        Tag = 14
                        Left = 4
                        Top = 4
                        Width = 90
                        Height = 40
                        Caption = '&Confirma'
                        Enabled = False
                        TabOrder = 0
                        OnClick = BtConfirma1Click
                      end
                      object BtEntidades: TBitBtn
                        Tag = 110
                        Left = 228
                        Top = 4
                        Width = 90
                        Height = 40
                        Caption = 'Ca&dastra'
                        Enabled = False
                        TabOrder = 1
                        OnClick = BtEntidadesClick
                      end
                      object BtPagtosExclui: TBitBtn
                        Tag = 114
                        Left = 516
                        Top = 4
                        Width = 90
                        Height = 40
                        Caption = '&Remove'
                        Enabled = False
                        TabOrder = 4
                        OnClick = BtPagtosExcluiClick
                      end
                      object EdQtdeCh: TdmkEdit
                        Left = 704
                        Top = 4
                        Width = 9
                        Height = 21
                        Alignment = taRightJustify
                        ReadOnly = True
                        TabOrder = 6
                        Visible = False
                        FormatType = dmktfString
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = ''
                      end
                      object EdValorCH: TdmkEdit
                        Left = 704
                        Top = 24
                        Width = 9
                        Height = 21
                        Alignment = taRightJustify
                        ReadOnly = True
                        TabOrder = 7
                        Visible = False
                        FormatType = dmktfString
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = ''
                      end
                      object DBEdit25: TDBEdit
                        Left = 100
                        Top = 20
                        Width = 40
                        Height = 21
                        DataField = 'Itens'
                        DataSource = DmLot2.DsSCHs
                        TabOrder = 8
                      end
                      object DBEdit26: TDBEdit
                        Left = 144
                        Top = 20
                        Width = 80
                        Height = 21
                        DataField = 'Valor'
                        DataSource = DmLot2.DsSCHs
                        TabOrder = 9
                      end
                      object BtCuidado: TBitBtn
                        Tag = 111
                        Left = 324
                        Top = 4
                        Width = 90
                        Height = 40
                        Caption = '&Ignora'
                        Enabled = False
                        TabOrder = 2
                        OnClick = BtCuidadoClick
                      end
                      object BtEntiTroca1: TBitBtn
                        Tag = 169
                        Left = 420
                        Top = 4
                        Width = 90
                        Height = 40
                        Caption = '&Altera'
                        Enabled = False
                        TabOrder = 3
                        OnClick = BtEntiTroca1Click
                      end
                      object BtSPC_CH: TBitBtn
                        Tag = 328
                        Left = 612
                        Top = 4
                        Width = 90
                        Height = 40
                        Caption = 'S&PC'
                        Enabled = False
                        TabOrder = 5
                        OnClick = BtSPC_CHClick
                      end
                    end
                    object DBGrid1: TDBGrid
                      Left = 0
                      Top = 86
                      Width = 758
                      Height = 40
                      Align = alBottom
                      DataSource = DmLot2.DsLCHs
                      Font.Charset = ANSI_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Univers Light Condensed'
                      Font.Style = [fsBold]
                      ParentFont = False
                      TabOrder = 1
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -11
                      TitleFont.Name = 'MS Sans Serif'
                      TitleFont.Style = []
                      Columns = <
                        item
                          Expanded = False
                          FieldName = 'Emitente'
                          Title.Caption = 'Emitente importado'
                          Width = 240
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'Nome_2'
                          Title.Caption = 'Emitente cadastrado'
                          Width = 210
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'Comp'
                          Width = 30
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'Banco'
                          Width = 30
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'Agencia'
                          Width = 36
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'Conta'
                          Width = 78
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'Cheque'
                          Width = 48
                          Visible = True
                        end>
                    end
                  end
                  object GroupBox5: TGroupBox
                    Left = 758
                    Top = 0
                    Width = 238
                    Height = 126
                    Align = alRight
                    TabOrder = 1
                    object Label30: TLabel
                      Left = 4
                      Top = 26
                      Width = 18
                      Height = 13
                      Caption = 'CH:'
                    end
                    object Label32: TLabel
                      Left = 4
                      Top = 48
                      Width = 19
                      Height = 13
                      Caption = 'DU:'
                    end
                    object Label142: TLabel
                      Left = 24
                      Top = 8
                      Width = 30
                      Height = 13
                      Caption = 'Risco:'
                    end
                    object Label143: TLabel
                      Left = 93
                      Top = 8
                      Width = 42
                      Height = 13
                      Caption = 'Vencido:'
                    end
                    object Label144: TLabel
                      Left = 162
                      Top = 8
                      Width = 56
                      Height = 13
                      Caption = 'Sub-total:'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'MS Sans Serif'
                      Font.Style = [fsBold]
                      ParentFont = False
                    end
                    object Label145: TLabel
                      Left = 4
                      Top = 70
                      Width = 21
                      Height = 13
                      Caption = 'ST:'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'MS Sans Serif'
                      Font.Style = [fsBold]
                      ParentFont = False
                    end
                    object Label146: TLabel
                      Left = 4
                      Top = 107
                      Width = 18
                      Height = 13
                      Caption = 'OC:'
                    end
                    object Label149: TLabel
                      Left = 91
                      Top = 88
                      Width = 62
                      Height = 13
                      Caption = 'Lim.Operaci.:'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentFont = False
                    end
                    object Label150: TLabel
                      Left = 160
                      Top = 88
                      Width = 45
                      Height = 13
                      Caption = 'TOTAL:'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'MS Sans Serif'
                      Font.Style = [fsBold]
                      ParentFont = False
                    end
                    object EdDR_C: TdmkEdit
                      Left = 24
                      Top = 44
                      Width = 68
                      Height = 21
                      Alignment = taRightJustify
                      Font.Charset = ANSI_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentFont = False
                      TabOrder = 3
                      FormatType = dmktfDouble
                      MskType = fmtNone
                      DecimalSize = 2
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0,00'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0.000000000000000000
                      OnChange = EdDR_CChange
                    end
                    object EdDT_C: TdmkEdit
                      Left = 162
                      Top = 44
                      Width = 68
                      Height = 21
                      Alignment = taRightJustify
                      Font.Charset = ANSI_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentFont = False
                      TabOrder = 5
                      FormatType = dmktfDouble
                      MskType = fmtNone
                      DecimalSize = 2
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0,00'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0.000000000000000000
                      OnChange = EdDT_CChange
                    end
                    object EdRT_C: TdmkEdit
                      Left = 24
                      Top = 66
                      Width = 68
                      Height = 21
                      Alignment = taRightJustify
                      Font.Charset = ANSI_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentFont = False
                      TabOrder = 6
                      FormatType = dmktfDouble
                      MskType = fmtNone
                      DecimalSize = 2
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0,00'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0.000000000000000000
                      OnChange = EdRT_CChange
                    end
                    object EdVT_C: TdmkEdit
                      Left = 93
                      Top = 66
                      Width = 68
                      Height = 21
                      Alignment = taRightJustify
                      Font.Charset = ANSI_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentFont = False
                      TabOrder = 7
                      FormatType = dmktfDouble
                      MskType = fmtNone
                      DecimalSize = 2
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0,00'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0.000000000000000000
                      OnChange = EdVT_CChange
                    end
                    object EdST_C: TdmkEdit
                      Left = 162
                      Top = 66
                      Width = 68
                      Height = 21
                      Alignment = taRightJustify
                      Font.Charset = ANSI_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentFont = False
                      TabOrder = 8
                      FormatType = dmktfDouble
                      MskType = fmtNone
                      DecimalSize = 2
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0,00'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0.000000000000000000
                      OnChange = EdST_CChange
                    end
                    object EdCT_C: TdmkEdit
                      Left = 162
                      Top = 22
                      Width = 68
                      Height = 21
                      Alignment = taRightJustify
                      Font.Charset = ANSI_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentFont = False
                      TabOrder = 2
                      FormatType = dmktfDouble
                      MskType = fmtNone
                      DecimalSize = 2
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0,00'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0.000000000000000000
                      OnChange = EdCT_CChange
                    end
                    object EdDV_C: TdmkEdit
                      Left = 93
                      Top = 44
                      Width = 68
                      Height = 21
                      Alignment = taRightJustify
                      Font.Charset = ANSI_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentFont = False
                      TabOrder = 4
                      FormatType = dmktfDouble
                      MskType = fmtNone
                      DecimalSize = 2
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0,00'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0.000000000000000000
                      OnChange = EdDV_CChange
                    end
                    object EdCR_C: TdmkEdit
                      Left = 24
                      Top = 22
                      Width = 68
                      Height = 21
                      Alignment = taRightJustify
                      Font.Charset = ANSI_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentFont = False
                      TabOrder = 0
                      FormatType = dmktfDouble
                      MskType = fmtNone
                      DecimalSize = 2
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0,00'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0.000000000000000000
                      OnChange = EdCR_CChange
                    end
                    object EdCV_C: TdmkEdit
                      Left = 93
                      Top = 22
                      Width = 68
                      Height = 21
                      Alignment = taRightJustify
                      Font.Charset = ANSI_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentFont = False
                      TabOrder = 1
                      FormatType = dmktfDouble
                      MskType = fmtNone
                      DecimalSize = 2
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0,00'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0.000000000000000000
                      OnChange = EdCV_CChange
                    end
                    object EdOA_C: TdmkEdit
                      Left = 24
                      Top = 102
                      Width = 68
                      Height = 21
                      Alignment = taRightJustify
                      Font.Charset = ANSI_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentFont = False
                      TabOrder = 9
                      FormatType = dmktfDouble
                      MskType = fmtNone
                      DecimalSize = 2
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0,00'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0.000000000000000000
                      OnChange = EdOA_CChange
                    end
                    object EdTT_C: TdmkEdit
                      Left = 162
                      Top = 102
                      Width = 68
                      Height = 21
                      Alignment = taRightJustify
                      Font.Charset = ANSI_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentFont = False
                      TabOrder = 10
                      FormatType = dmktfDouble
                      MskType = fmtNone
                      DecimalSize = 2
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0,00'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0.000000000000000000
                      OnChange = EdTT_CChange
                    end
                    object DBEdit30: TDBEdit
                      Left = 93
                      Top = 102
                      Width = 68
                      Height = 21
                      DataField = 'LimiCred'
                      Font.Charset = ANSI_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentFont = False
                      TabOrder = 11
                    end
                  end
                end
                object GradeC: TStringGrid
                  Left = 0
                  Top = 126
                  Width = 996
                  Height = 61
                  Align = alTop
                  ColCount = 11
                  DefaultRowHeight = 18
                  RowCount = 2
                  TabOrder = 2
                  RowHeights = (
                    18
                    18)
                end
              end
            end
          end
          object TabSheet2: TTabSheet
            Caption = 'D&uplicatas'
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Splitter2: TSplitter
              Left = 0
              Top = 242
              Width = 996
              Height = 3
              Cursor = crVSplit
              Align = alBottom
              ExplicitWidth = 1006
            end
            object PGAbertos_DU: TPageControl
              Left = 0
              Top = 245
              Width = 996
              Height = 103
              ActivePage = TabSheet5
              Align = alBottom
              TabOrder = 0
              object TabSheet5: TTabSheet
                Caption = 'Duplicatas vencidas e abertas'
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object Panel16: TPanel
                  Left = 329
                  Top = 0
                  Width = 659
                  Height = 75
                  Align = alClient
                  Caption = 'Panel16'
                  TabOrder = 0
                  object DBGrid4: TDBGrid
                    Left = 1
                    Top = 1
                    Width = 657
                    Height = 73
                    Align = alClient
                    DataSource = DmLot.DsDUOpen
                    TabOrder = 0
                    TitleFont.Charset = ANSI_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -11
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    Columns = <
                      item
                        Expanded = False
                        FieldName = 'Duplicata'
                        Title.Caption = 'Abertas'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'DCompra'
                        Title.Caption = 'D.Compra'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Valor'
                        Title.Alignment = taRightJustify
                        Width = 80
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'DDeposito'
                        Title.Caption = 'Vencimento'
                        Visible = True
                      end>
                  end
                end
                object Panel17: TPanel
                  Left = 0
                  Top = 0
                  Width = 329
                  Height = 75
                  Align = alLeft
                  TabOrder = 1
                  object DBGrid2: TDBGrid
                    Left = 1
                    Top = 1
                    Width = 329
                    Height = 73
                    Align = alLeft
                    DataSource = DmLot.DsDUVenc
                    TabOrder = 0
                    TitleFont.Charset = ANSI_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -11
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    Columns = <
                      item
                        Expanded = False
                        FieldName = 'Duplicata'
                        Title.Caption = 'Vencidas'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'DCompra'
                        Title.Caption = 'D.Compra'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Valor'
                        Title.Alignment = taRightJustify
                        Width = 80
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'DDeposito'
                        Title.Caption = 'Vencimento'
                        Visible = True
                      end>
                  end
                end
              end
              object TabSheet34: TTabSheet
                Caption = ' SPC / SERASA '
                ImageIndex = 1
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object Panel50: TPanel
                  Left = 0
                  Top = 0
                  Width = 998
                  Height = 75
                  Align = alClient
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 0
                  object DBGrid24: TDBGrid
                    Left = 0
                    Top = 17
                    Width = 998
                    Height = 58
                    Align = alClient
                    DataSource = DmLot.DsSPC_Result
                    TabOrder = 0
                    TitleFont.Charset = ANSI_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -11
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    Columns = <
                      item
                        Expanded = False
                        FieldName = 'Linha'
                        Width = 34
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Texto'
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -11
                        Font.Name = 'Courier New'
                        Font.Style = []
                        Width = 790
                        Visible = True
                      end>
                  end
                  object StCPF_DU: TStaticText
                    Left = 0
                    Top = 0
                    Width = 320
                    Height = 17
                    Align = alTop
                    Alignment = taCenter
                    Caption = 'AGUARDE... Consultado CPF / CNPJ 111.111.111-11...'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clHotLight
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = [fsBold]
                    ParentFont = False
                    TabOrder = 1
                    Visible = False
                  end
                end
              end
            end
            object GradeD_L: TDBGrid
              Left = 0
              Top = 187
              Width = 996
              Height = 55
              Align = alClient
              DataSource = DmLot2.DsLDUs
              TabOrder = 1
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              OnDrawColumnCell = GradeD_LDrawColumnCell
              OnTitleClick = GradeD_LTitleClick
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Ordem'
                  Title.Caption = 'Seq.'
                  Width = 24
                  Visible = True
                end
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'NOMEDUPLICADO'
                  Title.Alignment = taCenter
                  Title.Caption = 'D'
                  Width = 15
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Valor'
                  Title.Caption = 'Valor Vcto'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DVence'
                  Title.Caption = 'Vencimento'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DIAS'
                  Title.Caption = 'Dias'
                  Width = 34
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'AberCH'
                  Title.Caption = 'Ch.A'
                  Width = 32
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'RISCO'
                  Title.Caption = 'Lim.Operac.'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Aberto'
                  Title.Caption = 'Risco atual'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Disponiv'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'QtdeCH'
                  Title.Caption = 'Ch.D'
                  Width = 32
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'OcorrT'
                  Title.Caption = 'Val.Devolv.'
                  Width = 68
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'OcorrA'
                  Title.Caption = 'Val.Aberto'
                  Width = 68
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CPF_TXT'
                  Title.Caption = 'CNPJ / CPF'
                  Width = 128
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Emitente'
                  Width = 240
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'TEXTO_SPC'
                  Title.Caption = 'Consulta ao SPC'
                  Width = 240
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'IE'
                  Width = 128
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Bruto'
                  Title.Caption = 'Valor Total'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Desco'
                  Title.Caption = 'Desc. Vcto'
                  Visible = True
                end>
            end
            object Panel8: TPanel
              Left = 0
              Top = 0
              Width = 996
              Height = 126
              Align = alTop
              BevelOuter = bvNone
              Caption = 'Panel8'
              TabOrder = 2
              object Panel39: TPanel
                Left = 0
                Top = 0
                Width = 762
                Height = 126
                Align = alClient
                BevelOuter = bvNone
                Caption = 'Panel39'
                TabOrder = 0
                object Panel40: TPanel
                  Left = 0
                  Top = 0
                  Width = 762
                  Height = 47
                  Align = alClient
                  ParentBackground = False
                  TabOrder = 0
                  object Label31: TLabel
                    Left = 100
                    Top = 4
                    Width = 116
                    Height = 13
                    Caption = 'Quantidade e valor total:'
                  end
                  object Label132: TLabel
                    Left = 716
                    Top = 8
                    Width = 89
                    Height = 13
                    Caption = 'Vers'#227'o do arquivo:'
                  end
                  object LaVerSuit2: TLabel
                    Left = 716
                    Top = 24
                    Width = 41
                    Height = 13
                    Caption = '0.0.0.0'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = [fsBold]
                    ParentFont = False
                  end
                  object BtConfirma3: TBitBtn
                    Tag = 14
                    Left = 4
                    Top = 4
                    Width = 90
                    Height = 40
                    Caption = '&Confirma'
                    Enabled = False
                    TabOrder = 0
                    OnClick = BtConfirma3Click
                  end
                  object BtPagtosExclui2: TBitBtn
                    Tag = 113
                    Left = 516
                    Top = 4
                    Width = 90
                    Height = 40
                    Caption = '&Remove'
                    Enabled = False
                    TabOrder = 1
                    OnClick = BtPagtosExclui2Click
                  end
                  object EdQtdeDU: TdmkEdit
                    Left = 704
                    Top = 4
                    Width = 9
                    Height = 21
                    Alignment = taRightJustify
                    ReadOnly = True
                    TabOrder = 2
                    Visible = False
                    FormatType = dmktfString
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = ''
                  end
                  object EdValorDU: TdmkEdit
                    Left = 704
                    Top = 24
                    Width = 9
                    Height = 21
                    Alignment = taRightJustify
                    ReadOnly = True
                    TabOrder = 3
                    Visible = False
                    FormatType = dmktfString
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = ''
                  end
                  object DBEdit14: TDBEdit
                    Left = 100
                    Top = 20
                    Width = 40
                    Height = 21
                    DataField = 'Itens'
                    DataSource = DmLot2.DsSDUs
                    TabOrder = 4
                  end
                  object DBEdit24: TDBEdit
                    Left = 144
                    Top = 20
                    Width = 80
                    Height = 21
                    DataField = 'Valor'
                    DataSource = DmLot2.DsSDUs
                    TabOrder = 5
                  end
                  object BtEntidades2: TBitBtn
                    Tag = 110
                    Left = 228
                    Top = 4
                    Width = 90
                    Height = 40
                    Caption = 'Ca&dastra'
                    Enabled = False
                    TabOrder = 6
                    OnClick = BtEntidades2Click
                  end
                  object BtCuidado2: TBitBtn
                    Tag = 111
                    Left = 324
                    Top = 4
                    Width = 90
                    Height = 40
                    Caption = '&Ignora'
                    Enabled = False
                    TabOrder = 7
                    OnClick = BtCuidado2Click
                  end
                  object BtEntiTroca2: TBitBtn
                    Tag = 169
                    Left = 420
                    Top = 4
                    Width = 90
                    Height = 40
                    Caption = '&Altera'
                    Enabled = False
                    TabOrder = 8
                    OnClick = BtEntiTroca2Click
                  end
                  object BtSPC_DU: TBitBtn
                    Tag = 328
                    Left = 612
                    Top = 4
                    Width = 90
                    Height = 40
                    Caption = 'S&PC'
                    Enabled = False
                    TabOrder = 9
                    OnClick = BtSPC_DUClick
                  end
                end
                object GradeLDU1: TDBGrid
                  Left = 0
                  Top = 47
                  Width = 762
                  Height = 40
                  Align = alBottom
                  DataSource = DmLot2.DsLDUs
                  Font.Charset = ANSI_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Univers Light Condensed'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 1
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  OnDrawColumnCell = GradeLDU1DrawColumnCell
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'Emitente'
                      Title.Caption = 'Emitente importado'
                      Width = 172
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Rua'
                      Width = 105
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NUMERO_TXT'
                      Title.Caption = 'N'#250'mero'
                      Width = 41
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Compl'
                      Title.Caption = 'Complemento'
                      Width = 81
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Birro'
                      Title.Caption = 'Bairro'
                      Width = 80
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Cidade'
                      Width = 80
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'CEP_TXT'
                      Title.Caption = 'CEP'
                      Width = 63
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'UF'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'TEL1_TXT'
                      Title.Caption = 'Telefone'
                      Width = 92
                      Visible = True
                    end>
                end
                object GradeLDU2: TDBGrid
                  Left = 0
                  Top = 87
                  Width = 762
                  Height = 39
                  Align = alBottom
                  DataSource = DmLot2.DsLDUs
                  Font.Charset = ANSI_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Univers Light Condensed'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 2
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  OnDrawColumnCell = GradeLDU2DrawColumnCell
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'NOME_2'
                      Title.Caption = 'Emitente cadastrado'
                      Width = 172
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'RUA_2'
                      Title.Caption = 'Logradouro'
                      Width = 105
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NUMERO_2_TXT'
                      Title.Caption = 'N'#250'mero'
                      Width = 41
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'COMPL_2'
                      Title.Caption = 'Complemento'
                      Width = 80
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'BAIRRO_2'
                      Title.Caption = 'Bairro'
                      Width = 80
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'CIDADE_2'
                      Title.Caption = 'Cidade'
                      Width = 80
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'CEP_2_TXT'
                      Title.Caption = 'CEP'
                      Width = 63
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'UF_2'
                      Title.Caption = 'UF'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'TEL1_2_TXT'
                      Title.Caption = 'Telefone'
                      Width = 92
                      Visible = True
                    end>
                end
              end
              object GroupBox4: TGroupBox
                Left = 762
                Top = 0
                Width = 234
                Height = 126
                Align = alRight
                TabOrder = 1
                object Label135: TLabel
                  Left = 4
                  Top = 26
                  Width = 18
                  Height = 13
                  Caption = 'CH:'
                end
                object Label136: TLabel
                  Left = 4
                  Top = 48
                  Width = 19
                  Height = 13
                  Caption = 'DU:'
                end
                object Label137: TLabel
                  Left = 24
                  Top = 8
                  Width = 30
                  Height = 13
                  Caption = 'Risco:'
                end
                object Label138: TLabel
                  Left = 93
                  Top = 8
                  Width = 42
                  Height = 13
                  Caption = 'Vencido:'
                end
                object Label139: TLabel
                  Left = 162
                  Top = 8
                  Width = 56
                  Height = 13
                  Caption = 'Sub-total:'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object Label140: TLabel
                  Left = 4
                  Top = 70
                  Width = 21
                  Height = 13
                  Caption = 'ST:'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object Label141: TLabel
                  Left = 4
                  Top = 107
                  Width = 18
                  Height = 13
                  Caption = 'OC:'
                end
                object Label147: TLabel
                  Left = 160
                  Top = 88
                  Width = 45
                  Height = 13
                  Caption = 'TOTAL:'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object Label148: TLabel
                  Left = 91
                  Top = 88
                  Width = 62
                  Height = 13
                  Caption = 'Lim.Operaci.:'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                end
                object EdDR_D: TdmkEdit
                  Left = 24
                  Top = 44
                  Width = 68
                  Height = 21
                  Alignment = taRightJustify
                  Font.Charset = ANSI_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Univers Light Condensed'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 3
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  OnChange = EdDR_CChange
                end
                object EdDT_D: TdmkEdit
                  Left = 162
                  Top = 44
                  Width = 68
                  Height = 21
                  Alignment = taRightJustify
                  Font.Charset = ANSI_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Univers Light Condensed'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 5
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  OnChange = EdDT_CChange
                end
                object EdRT_D: TdmkEdit
                  Left = 24
                  Top = 66
                  Width = 68
                  Height = 21
                  Alignment = taRightJustify
                  Font.Charset = ANSI_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Univers Light Condensed'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 6
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  OnChange = EdRT_CChange
                end
                object EdVT_D: TdmkEdit
                  Left = 93
                  Top = 66
                  Width = 68
                  Height = 21
                  Alignment = taRightJustify
                  Font.Charset = ANSI_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Univers Light Condensed'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 7
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  OnChange = EdVT_CChange
                end
                object EdST_D: TdmkEdit
                  Left = 162
                  Top = 66
                  Width = 68
                  Height = 21
                  Alignment = taRightJustify
                  Font.Charset = ANSI_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Univers Light Condensed'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 8
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  OnChange = EdST_CChange
                end
                object EdCT_D: TdmkEdit
                  Left = 162
                  Top = 22
                  Width = 68
                  Height = 21
                  Alignment = taRightJustify
                  Font.Charset = ANSI_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Univers Light Condensed'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 2
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  OnChange = EdCT_CChange
                end
                object EdDV_D: TdmkEdit
                  Left = 93
                  Top = 44
                  Width = 68
                  Height = 21
                  Alignment = taRightJustify
                  Font.Charset = ANSI_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Univers Light Condensed'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 4
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  OnChange = EdDV_CChange
                end
                object EdCR_D: TdmkEdit
                  Left = 24
                  Top = 22
                  Width = 68
                  Height = 21
                  Alignment = taRightJustify
                  Font.Charset = ANSI_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Univers Light Condensed'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 0
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  OnChange = EdCR_CChange
                end
                object EdCV_D: TdmkEdit
                  Left = 93
                  Top = 22
                  Width = 68
                  Height = 21
                  Alignment = taRightJustify
                  Font.Charset = ANSI_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Univers Light Condensed'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 1
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  OnChange = EdCV_CChange
                end
                object EdOA_D: TdmkEdit
                  Left = 24
                  Top = 102
                  Width = 68
                  Height = 21
                  Alignment = taRightJustify
                  Font.Charset = ANSI_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Univers Light Condensed'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 9
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  OnChange = EdOA_CChange
                end
                object EdTT_D: TdmkEdit
                  Left = 162
                  Top = 102
                  Width = 68
                  Height = 21
                  Alignment = taRightJustify
                  Font.Charset = ANSI_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Univers Light Condensed'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 10
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  OnChange = EdTT_CChange
                end
                object DBEdit31: TDBEdit
                  Left = 93
                  Top = 102
                  Width = 68
                  Height = 21
                  DataField = 'LimiCred'
                  TabOrder = 11
                end
              end
            end
            object GradeD: TStringGrid
              Left = 0
              Top = 126
              Width = 996
              Height = 61
              Align = alTop
              ColCount = 11
              DefaultRowHeight = 18
              RowCount = 2
              TabOrder = 3
              Visible = False
              RowHeights = (
                18
                18)
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 536
    Width = 1008
    Height = 60
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 43
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 12
        Top = 22
        Width = 981
        Height = 17
        TabOrder = 0
      end
    end
  end
  object PMCadastraCH: TPopupMenu
    Left = 856
    Top = 172
    object Emitenteselecionado1: TMenuItem
      Caption = '&Emitente selecionado'
      OnClick = Emitenteselecionado1Click
    end
    object Todosemitentesnocadastrados1: TMenuItem
      Caption = '&Todos emitentes n'#227'o cadastrados'
      OnClick = Todosemitentesnocadastrados1Click
    end
  end
  object PMCadastraDU: TPopupMenu
    Left = 884
    Top = 172
    object Sacadoselecionado1: TMenuItem
      Caption = '&Sacado selecionado'
      OnClick = Sacadoselecionado1Click
    end
    object Todossacadosnocadastrados1: TMenuItem
      Caption = '&Todos sacados n'#227'o cadastrados'
      OnClick = Todossacadosnocadastrados1Click
    end
  end
  object PMSPC_Import: TPopupMenu
    Left = 968
    Top = 200
    object VerificatodosCPFCNPJpelovalormnimoconfigurao1: TMenuItem
      Caption = 'Consulta CPF / CNPJ pelo valor &M'#237'nimo da config.'
      OnClick = VerificatodosCPFCNPJpelovalormnimoconfigurao1Click
    end
    object VerificatodosCPFCNPJaindanoverificados1: TMenuItem
      Caption = 'Consulta &Todos CPF / CNPJ ainda n'#227'o consultados'
      OnClick = VerificatodosCPFCNPJaindanoverificados1Click
    end
    object N7: TMenuItem
      Caption = '-'
    end
    object VerificaCPFCNPJatual1: TMenuItem
      Caption = 'Consulta o CPF / CNPJ &Atual'
      OnClick = VerificaCPFCNPJatual1Click
    end
    object ConsultaosCPFCNPJdositensselecionados1: TMenuItem
      Caption = 'Consulta os CPF / CNPJ dos itens &Selecionados'
      OnClick = ConsultaosCPFCNPJdositensselecionados1Click
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object Abrejaneladeconsulta1: TMenuItem
      Caption = 'Abre &Janela de consulta manual (configur'#225'vel)'
      OnClick = Abrejaneladeconsulta1Click
    end
    object N8: TMenuItem
      Caption = '-'
    end
    object Imprimeconsultaselecionada1: TMenuItem
      Caption = '&Imprime consulta selecionada'
      OnClick = Imprimeconsultaselecionada1Click
    end
  end
end
