unit UCreateCredito;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db, DbCtrls, UnMLAGeral,
  UnInternalConsts2, ComCtrls, Registry, mySQLDbTables,(* DbTables,*) dmkGeral;

type
  TNomeTabRecriaTempTableCredito = (ntrtt_Lista_TED_, ntrtt_OLC_2_,
    ntrtt_sobr_cli_);
  TAcaoCreateCredito = (acDrop, acCreate, acFind);
  TUCreateCredito = class(TObject)

  private
    { Private declarations }
    procedure Cria_ntrtt_Lista_TED_(Qry: TmySQLQuery);
    procedure Cria_ntrtt_OLC_2_(Qry: TmySQLQuery);
    procedure Cria_ntrtt_sobr_cli_(Qry: TmySQLQuery);

  public
    { Public declarations }
    function RecriaTempTableNovo(Tabela: TNomeTabRecriaTempTableCredito;
             Qry: TmySQLQuery; UniqueTableName: Boolean = False; Repeticoes:
             Integer = 1; NomeTab: String = ''): String;
  end;

var
  UCriarCredito: TUCreateCredito;

implementation

uses UnMyObjects, Module, ModuleGeral;

procedure TUCreateCredito.Cria_ntrtt_Lista_TED_(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  DataHora             datetime     NOT NULL DEFAULT "0000-00-00 00:00:00" ,');
  Qry.SQL.Add('  Layout               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  TedCRetCab           int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NoLayout             varchar(100) NOT NULL DEFAULT "?"          ,');
  Qry.SQL.Add('  NoArquivo            varchar(255) NOT NULL DEFAULT "?"          ,');
  Qry.SQL.Add('  NoErro               varchar(255) NOT NULL DEFAULT "?"          ,');
  Qry.SQL.Add('  Erro                 int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Step                 int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Itens                int(11)      NOT NULL DEFAULT "0"          ,');
  //
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"          ');
  Qry.SQL.Add(') TYPE=MyISAM');
  Qry.ExecSQL;
  //
end;

procedure TUCreateCredito.Cria_ntrtt_OLC_2_(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  FatNum               double(15,0) NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Lote                 int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Data                 date         NOT NULL DEFAULT "0000-00-00" ,');
  Qry.SQL.Add('  CodCli               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NomCli               varchar(100) NOT NULL DEFAULT "?"          ,');
  Qry.SQL.Add('  Entradas             double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  Saidas               double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  Erro                 double(15,2) NOT NULL DEFAULT "0.00"       ,');
  //
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"          ');
  Qry.SQL.Add(') TYPE=MyISAM');
  Qry.ExecSQL;
  //
end;

procedure TUCreateCredito.Cria_ntrtt_sobr_cli_(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Cliente              int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Lote                 int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Codigo               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Data                 date         NOT NULL DEFAULT "0000-00-00" ,');
  Qry.SQL.Add('  NomCli               varchar(100) NOT NULL DEFAULT "?"          ,');
  Qry.SQL.Add('  SobraAnt             double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  SobraIni             double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  SobraNow             double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  SobraErr             double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  Checado              int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  CodAnt               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  LotAnt               int(11)      NOT NULL DEFAULT "0"          ,');
  //
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"          ');
  Qry.SQL.Add(') TYPE=MyISAM');
  Qry.ExecSQL;
  //
end;

function TUCreateCredito.RecriaTempTableNovo(
  Tabela: TNomeTabRecriaTempTableCredito; Qry: TmySQLQuery; UniqueTableName:
  Boolean = False; Repeticoes: Integer = 1; NomeTab: String = ''): String;
var
  Nome, TabNo: String;
  P: Integer;
begin
  TabNo := '';
  if NomeTab = '' then
  begin
    case Tabela of
      ntrtt_Lista_TED_:     Nome := Lowercase('_Lista_TED_');
      ntrtt_OLC_2_:         Nome := Lowercase('_OLC_2_');
      ntrtt_sobr_cli_:      Nome := Lowercase('_sobr_cli_');
      //
      // ...
      else Nome := '';
    end;
  end else
    Nome := Lowercase(NomeTab);
  //
  if Nome = '' then
  begin
    Geral.MensagemBox(
    'Tabela tempor�ria sem nome definido! (RecriaTemTableNovo)',
    'Erro', MB_OK+MB_ICONERROR);
    Result := '';
    Exit;
  end;
  if UniqueTableName then
  begin
    TabNo := '_' + FormatFloat('0', VAR_USUARIO) + '_' + Nome;
    //  caso for Master ou Admin (n�meros negativos)
    P := pos('-', TabNo);
    if P > 0 then
      TabNo[P] := '_';
  end else TabNo := Nome;
  Qry.Close;
  Qry.SQL.Clear;
  Qry.SQL.Add('DROP TABLE IF EXISTS ' + TabNo);
  Qry.ExecSQL;
  //
  Qry.Close;
  Qry.SQL.Clear;
  Qry.SQL.Add('CREATE TABLE ' + TabNo +' (');
  //
  case Tabela of
    ntrtt_Lista_TED_:    Cria_ntrtt_Lista_TED_(Qry);
    ntrtt_OLC_2_:        Cria_ntrtt_OLC_2_(Qry);
    ntrtt_sobr_cli_:     Cria_ntrtt_sobr_cli_(Qry);
    //
    else Geral.MensagemBox('N�o foi poss�vel criar a tabela tempor�ria "' +
    Nome + '" por falta de implementa��o!', 'Erro', MB_OK+MB_ICONERROR);
  end;
  Result := TabNo;
end;

end.

