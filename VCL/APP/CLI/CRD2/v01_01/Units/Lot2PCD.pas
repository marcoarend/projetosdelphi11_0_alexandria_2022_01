unit Lot2PCD;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, UnDmkEnums;

type
  TFmLot2PCD = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    DBGrid9: TDBGrid;
    Panel28: TPanel;
    Label102: TLabel;
    Label103: TLabel;
    Label104: TLabel;
    Label105: TLabel;
    TPDataBase7: TdmkEditDateTimePicker;
    EdValorBase7: TdmkEdit;
    EdJurosBase7: TdmkEdit;
    EdJurosPeriodo7: TdmkEdit;
    Label106: TLabel;
    TPPagto7: TdmkEditDateTimePicker;
    Label107: TLabel;
    EdJuros7: TdmkEdit;
    Label109: TLabel;
    EdAPagar7: TdmkEdit;
    Label108: TLabel;
    EdValPagar7: TdmkEdit;
    EdDesco7: TdmkEdit;
    Label202: TLabel;
    EdCorrigido7: TdmkEdit;
    Label2: TLabel;
    EdPendente7: TdmkEdit;
    Label9: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdValorBase7Change(Sender: TObject);
    procedure EdJurosBase7Change(Sender: TObject);
    procedure TPPagto7Change(Sender: TObject);
    procedure EdJuros7Change(Sender: TObject);
    procedure EdAPagar7Change(Sender: TObject);
    procedure EdValPagar7Change(Sender: TObject);
    procedure EdDesco7Exit(Sender: TObject);
  private
    { Private declarations }
    //
    procedure CalculaPendente();
  public
    { Public declarations }
    procedure CalculaAPagarCHDev();
    procedure CalculaJurosCHDev();
  end;

  var
  FmLot2PCD: TFmLot2PCD;

implementation

uses UnMyObjects, Module, ModuleLot, UMySQLModule, Lot2Cab, ModuleFin, MyListas;

{$R *.DFM}

procedure TFmLot2PCD.BtOKClick(Sender: TObject);
{
var
  LotePg, AlinIts, Codigo: Integer;
  Data: String;
  Juros, Pago, Desco: Double;
begin
  AlinIts := DmLot.QrCHDevACodigo.Value;
  Data := Geral.FDT(TPPagto7.Date, 1);
  Juros := EdJuros7.ValueVariant;
  Pago := EdPago7.ValueVariant;
  Desco := EdDesco7.ValueVariant;
  LotePg := FmLot2Cab.QrLotCodigo.Value;
  (*
  APCD :=
  DataCh :=
  *)
  Codigo := UMyMod.BuscaEmLivreY_Def('alin pgs', 'Codigo', ImgTipo.SQLType, 0);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'alin pgs', False, [
  'AlinIts', 'Data', 'Juros',
  'Pago', 'Desco', 'LotePg'(*,
  'APCD', 'DataCh'*)], [
  'Codigo'], [
  AlinIts, Data, Juros,
  Pago, Desco, LotePg(*,
  APCD, DataCh*)], [
  Codigo], True) then
  begin
    with FmLot2Cab do
    begin
      CalculaLote(QrLotCodigo.Value, False);
      CalculaPagtoAlinIts(TPPagto7.Date, DmLot.QrCHDevACodigo.Value);
      FCHDevA := DmLot.QrCHDevACodigo.Value;
      FCHDevP := Codigo;
      LocCod(QrLotCodigo.Value, QrLotCodigo.Value);
      ReopenCHDevA(QrLotCliente.Value);
      ReopenCHDevP();
    end;
    Close;
  end;
}
const
  OcorAPCD = -1;
var
{
  ValQuit: Double;
  Localiz, DevolPg, OcorP, AlinIts: Integer;
  //
}
  Ocorreu,
  //LocFP, Desco, Genero, MoraVal,
  CartDep, FatParcela, Controle, FatID_Sub, Cliente, FatParcRef: Integer;
  FatNum: Double;
  DataCH, Dta: TDateTime;
  //
  Gen_0303, Gen_0304, Gen_0305, Gen_0307: Integer;
  ValPagar, ValTaxas, ValMulta, ValJuros, ValPrinc, ValDesco, ValPende: Double;
begin
  FatNum := FmLot2Cab.QrLotCodigo.Value;
  FatParcela := 0;
  Controle := 0;
  FatID_Sub := DmLot.QrCHDevAAlinea1.Value;
  Cliente := DmLot.QrCHDevACliente.Value;
  Ocorreu := OcorAPCD;
  FatParcRef := DmLot.QrCHDevACodigo.Value;
  //MoraVal := EdJuros7.ValueVariant;
  Dta := TPPagto7.Date;
  DataCH := 0; // ?????
  //Desco := EdDesco7.ValueVariant;
  //
  //
  if not DModFin.ObtemGeneroDeFatID(VAR_FATID_0303, Gen_0303, tpCred, True) then
    Exit;
  if not DModFin.ObtemGeneroDeFatID(VAR_FATID_0304, Gen_0304, tpCred, True) then
    Exit;
  if not DModFin.ObtemGeneroDeFatID(VAR_FATID_0305, Gen_0305, tpCred, True) then
    Exit;
  if not DModFin.ObtemGeneroDeFatID(VAR_FATID_0307, Gen_0307, tpDeb, True) then
    Exit;
  //
  ValPagar := EdValPagar7.ValueVariant;
  if DmLot.QtdDePagtosDeChqDev(DmLot.QrCHDevACodigo.Value) = 0 then
  begin
    ValTaxas := DmLot.QrCHDevATaxas.Value;
    ValMulta := DmLot.QrCHDevAMulta.Value;
  end else begin
    ValTaxas := 0;
    ValMulta := 0;
  end;
  ValDesco := EdDesco7.ValueVariant;
  ValJuros := EdJuros7.ValueVariant;
  ValPrinc := ValPagar - ValTaxas - ValMulta - ValJuros + ValDesco;
  ValPende := EdPendente7.ValueVariant;
  // N�o precisa, porque aqui est� quitando?:
  CartDep := 0;

{
  if DmLot.SQL_ChqPg(Dmod.QrUpd, stIns, FatParcela, Controle, FatID_Sub, Genero,
  Cliente, Ocorreu, FatParcRef, FatNum, Pago, MoraVal, Desco, DataCH, Dta) then
}
  if DmLot.InsUpd_ChqPg(Dmod.QrUpd, stIns, FatParcela, Controle, FatID_Sub,
  Gen_0303, Gen_0304, Gen_0305, Gen_0307, Cliente, Ocorreu, FatParcRef,
  FatNum, ValPagar(*, MoraVal, Desco*), DataCH, Dta,
  ValPrinc, ValTaxas, ValMulta, ValJuros, ValDesco, ValPende,
  CartDep) then
  begin
    with FmLot2Cab do
    begin
      CalculaLote(QrLotCodigo.Value, False);
      DmLot.CalculaPagtoAlinIts(TPPagto7.Date, DmLot.QrCHDevACodigo.Value);
      FCHDevA := DmLot.QrCHDevACodigo.Value;
      FCHDevP := FatParcela;
      LocCod(QrLotCodigo.Value, QrLotCodigo.Value);
      ReopenCHDevA(QrLotCliente.Value);
      ReopenCHDevP();
    end;
    Close;
  end;
end;

procedure TFmLot2PCD.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLot2PCD.CalculaAPagarCHDev();
{
var
  Base, Juro: Double;
begin
  Base := EdValorBase7.ValueVariant;
  Juro := EdJuros7.ValueVariant;
  //
  EdAPagar7.ValueVariant := Base + Juro;
  EdPago7.ValueVariant   := Base + Juro;
end;
}
var
  Base, Juro, Desc: Double;
begin
  Base := EdValorBase7.ValueVariant;
  Juro := EdJuros7.ValueVariant;
  Desc := EdDesco7.ValueVariant;
  //
  EdCorrigido7.ValueVariant := Base + Juro;
  EdAPagar7.ValueVariant  := EdCorrigido7.ValueVariant - Desc;
  EdValPagar7.ValueVariant  := EdAPagar7.ValueVariant;
end;

procedure TFmLot2PCD.CalculaJurosCHDev();
var
  Prazo: Integer;
  Taxa, Juros, Valor: Double;
begin
  Juros := 0;
  Prazo := Trunc(Int(TPPagto7.Date) - Int(TPDataBase7.Date));
  if Prazo > 0 then
  begin
    Taxa  := EdJurosBase7.ValueVariant;
    Juros := MLAGeral.CalculaJuroComposto(Taxa, Prazo);
  end;
  Valor := EdValorBase7.ValueVariant;
  EdJurosPeriodo7.ValueVariant := Juros;
  EdJuros7.ValueVariant := Juros * Valor / 100;
end;

procedure TFmLot2PCD.CalculaPendente();
begin
  EdPendente7.ValueVariant := EdAPagar7.ValueVariant - EdValPagar7.ValueVariant;
end;

procedure TFmLot2PCD.EdAPagar7Change(Sender: TObject);
begin
  CalculaPendente();
end;

procedure TFmLot2PCD.EdDesco7Exit(Sender: TObject);
begin
  CalculaAPagarCHDev();
end;

procedure TFmLot2PCD.EdJuros7Change(Sender: TObject);
begin
  CalculaAPagarCHDev();
end;

procedure TFmLot2PCD.EdJurosBase7Change(Sender: TObject);
begin
  CalculaJurosCHDev();
end;

procedure TFmLot2PCD.EdValorBase7Change(Sender: TObject);
begin
  CalculaAPagarCHDev();
end;

procedure TFmLot2PCD.EdValPagar7Change(Sender: TObject);
begin
  CalculaPendente();
end;

procedure TFmLot2PCD.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLot2PCD.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLot2PCD.TPPagto7Change(Sender: TObject);
begin
  CalculaJurosCHDev();
end;

end.
