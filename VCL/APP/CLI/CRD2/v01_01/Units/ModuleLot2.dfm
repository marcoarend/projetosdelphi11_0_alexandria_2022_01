object DmLot2: TDmLot2
  OnCreate = DataModuleCreate
  Height = 500
  Width = 922
  PixelsPerInch = 96
  object QrLocEmiBAC: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM emitbac'
      'WHERE CONCAT(LEFT(BAC, 7), RIGHT(BAC, :P0)) = :P1'
      'AND CPF <> ""')
    Left = 24
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLocEmiBACBAC: TWideStringField
      FieldName = 'BAC'
    end
    object QrLocEmiBACCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
  end
  object QrLocEmiCPF: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM emitcpf'
      'WHERE CPF=:P0'
      '')
    Left = 24
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocEmiCPFCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrLocEmiCPFNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrLocEmiCPFLimite: TFloatField
      FieldName = 'Limite'
    end
    object QrLocEmiCPFLastAtz: TDateField
      FieldName = 'LastAtz'
    end
    object QrLocEmiCPFAcumCHComV: TFloatField
      FieldName = 'AcumCHComV'
    end
    object QrLocEmiCPFAcumCHComQ: TIntegerField
      FieldName = 'AcumCHComQ'
    end
    object QrLocEmiCPFAcumCHDevV: TFloatField
      FieldName = 'AcumCHDevV'
    end
    object QrLocEmiCPFAcumCHDevQ: TIntegerField
      FieldName = 'AcumCHDevQ'
    end
  end
  object QrLCHs: TMySQLQuery
    Database = Dmod.MyLocDatabase
    AfterScroll = QrLCHsAfterScroll
    OnCalcFields = QrLCHsCalcFields
    SQL.Strings = (
      'SELECT  il.*  '
      'FROM importlote il'
      'WHERE il.Tipo=0'
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      '')
    Left = 24
    Top = 148
    object QrLCHsTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrLCHsOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrLCHsDEmiss: TDateField
      FieldName = 'DEmiss'
    end
    object QrLCHsDCompra: TDateField
      FieldName = 'DCompra'
    end
    object QrLCHsDVence: TDateField
      FieldName = 'DVence'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLCHsDeposito: TDateField
      FieldName = 'Deposito'
    end
    object QrLCHsComp: TIntegerField
      FieldName = 'Comp'
      DisplayFormat = '000'
    end
    object QrLCHsBanco: TIntegerField
      FieldName = 'Banco'
      DisplayFormat = '000'
    end
    object QrLCHsAgencia: TIntegerField
      FieldName = 'Agencia'
      DisplayFormat = '0000'
    end
    object QrLCHsConta: TWideStringField
      FieldName = 'Conta'
      Size = 8
    end
    object QrLCHsCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrLCHsEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrLCHsCheque: TIntegerField
      FieldName = 'Cheque'
      DisplayFormat = '000000'
    end
    object QrLCHsDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 12
    end
    object QrLCHsValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLCHsRua: TWideStringField
      FieldName = 'Rua'
      Size = 30
    end
    object QrLCHsNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrLCHsCompl: TWideStringField
      FieldName = 'Compl'
      Size = 30
    end
    object QrLCHsBirro: TWideStringField
      FieldName = 'Birro'
      Size = 30
    end
    object QrLCHsCidade: TWideStringField
      FieldName = 'Cidade'
      Size = 30
    end
    object QrLCHsUF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrLCHsCEP: TIntegerField
      FieldName = 'CEP'
    end
    object QrLCHsTel1: TWideStringField
      FieldName = 'Tel1'
    end
    object QrLCHsBAC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'BAC'
      Calculated = True
    end
    object QrLCHsDIAS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'DIAS'
      Calculated = True
    end
    object QrLCHsCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Size = 40
      Calculated = True
    end
    object QrLCHsSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrLCHsMEU_SIT: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'MEU_SIT'
      Calculated = True
    end
    object QrLCHsAberto: TFloatField
      FieldName = 'Aberto'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLCHsOcorrA: TFloatField
      FieldName = 'OcorrA'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLCHsOcorrT: TFloatField
      FieldName = 'OcorrT'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLCHsQtdeCH: TIntegerField
      FieldName = 'QtdeCH'
    end
    object QrLCHsAberCH: TIntegerField
      FieldName = 'AberCH'
    end
    object QrLCHsValAcum: TFloatField
      FieldName = 'ValAcum'
    end
    object QrLCHsIE: TWideStringField
      FieldName = 'IE'
      Size = 25
    end
    object QrLCHsDesco: TFloatField
      FieldName = 'Desco'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLCHsBruto: TFloatField
      FieldName = 'Bruto'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLCHsDuplicado: TIntegerField
      FieldName = 'Duplicado'
    end
    object QrLCHsNOMEDUPLICADO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEDUPLICADO'
      Size = 1
      Calculated = True
    end
    object QrLCHsPraca: TIntegerField
      FieldName = 'Praca'
    end
    object QrLCHsDisponiv: TFloatField
      FieldName = 'Disponiv'
      DisplayFormat = '0'
    end
    object QrLCHsRISCO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'RISCO'
      Calculated = True
    end
    object QrLCHsCPF_2: TWideStringField
      FieldName = 'CPF_2'
      Size = 15
    end
    object QrLCHsNome_2: TWideStringField
      FieldName = 'Nome_2'
      Size = 100
    end
    object QrLCHsRISCOEM: TFloatField
      FieldName = 'RISCOEM'
    end
    object QrLCHsRISCOSA: TFloatField
      FieldName = 'RISCOSA'
    end
    object QrLCHsRua_2: TWideStringField
      FieldName = 'Rua_2'
      Size = 30
    end
    object QrLCHsNumero_2: TIntegerField
      FieldName = 'Numero_2'
    end
    object QrLCHsCompl_2: TWideStringField
      FieldName = 'Compl_2'
      Size = 30
    end
    object QrLCHsIE_2: TWideStringField
      FieldName = 'IE_2'
    end
    object QrLCHsBairro_2: TWideStringField
      FieldName = 'Bairro_2'
      Size = 30
    end
    object QrLCHsCidade_2: TWideStringField
      FieldName = 'Cidade_2'
      Size = 25
    end
    object QrLCHsUF_2: TWideStringField
      FieldName = 'UF_2'
      Size = 2
    end
    object QrLCHsCEP_2: TIntegerField
      FieldName = 'CEP_2'
    end
    object QrLCHsTEL1_2: TWideStringField
      FieldName = 'TEL1_2'
    end
    object QrLCHsStatusSPC: TSmallintField
      FieldName = 'StatusSPC'
    end
    object QrLCHsTEXTO_SPC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEXTO_SPC'
      Size = 30
      Calculated = True
    end
  end
  object DsLCHs: TDataSource
    DataSet = QrLCHs
    Left = 80
    Top = 148
  end
  object QrLDUs: TMySQLQuery
    Database = Dmod.MyLocDatabase
    AfterScroll = QrLDUsAfterScroll
    OnCalcFields = QrLDUsCalcFields
    SQL.Strings = (
      'SELECT  il.*  '
      'FROM importlote il'
      'WHERE il.Tipo=1'
      ''
      '')
    Left = 24
    Top = 196
    object QrLDUsTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrLDUsOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrLDUsDEmiss: TDateField
      FieldName = 'DEmiss'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLDUsDCompra: TDateField
      FieldName = 'DCompra'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLDUsDVence: TDateField
      FieldName = 'DVence'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLDUsDeposito: TDateField
      FieldName = 'Deposito'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLDUsComp: TIntegerField
      FieldName = 'Comp'
      DisplayFormat = '000'
    end
    object QrLDUsBanco: TIntegerField
      FieldName = 'Banco'
      DisplayFormat = '000'
    end
    object QrLDUsAgencia: TIntegerField
      FieldName = 'Agencia'
      DisplayFormat = '0000'
    end
    object QrLDUsConta: TWideStringField
      FieldName = 'Conta'
      Size = 8
    end
    object QrLDUsCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrLDUsEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrLDUsCheque: TIntegerField
      FieldName = 'Cheque'
    end
    object QrLDUsDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 12
    end
    object QrLDUsValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLDUsRua: TWideStringField
      FieldName = 'Rua'
      Size = 30
    end
    object QrLDUsNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrLDUsCompl: TWideStringField
      FieldName = 'Compl'
      Size = 30
    end
    object QrLDUsBirro: TWideStringField
      FieldName = 'Birro'
      Size = 30
    end
    object QrLDUsCidade: TWideStringField
      FieldName = 'Cidade'
      Size = 30
    end
    object QrLDUsUF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrLDUsCEP: TIntegerField
      FieldName = 'CEP'
    end
    object QrLDUsTel1: TWideStringField
      FieldName = 'Tel1'
    end
    object QrLDUsCPF_2: TWideStringField
      FieldName = 'CPF_2'
      Required = True
      Size = 15
    end
    object QrLDUsNOME_2: TWideStringField
      FieldName = 'NOME_2'
      Size = 50
    end
    object QrLDUsDIAS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'DIAS'
      Calculated = True
    end
    object QrLDUsCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Size = 40
      Calculated = True
    end
    object QrLDUsSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrLDUsMEU_SIT: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'MEU_SIT'
      Calculated = True
    end
    object QrLDUsAberto: TFloatField
      FieldName = 'Aberto'
      DisplayFormat = '#,###,##0'
    end
    object QrLDUsOcorrA: TFloatField
      FieldName = 'OcorrA'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLDUsOcorrT: TFloatField
      FieldName = 'OcorrT'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLDUsAberCH: TIntegerField
      FieldName = 'AberCH'
      DisplayFormat = '0'
    end
    object QrLDUsQtdeCH: TIntegerField
      FieldName = 'QtdeCH'
      DisplayFormat = '0'
    end
    object QrLDUsRUA_2: TWideStringField
      FieldName = 'RUA_2'
      Size = 30
    end
    object QrLDUsCOMPL_2: TWideStringField
      FieldName = 'COMPL_2'
      Size = 30
    end
    object QrLDUsBAIRRO_2: TWideStringField
      FieldName = 'BAIRRO_2'
      Size = 30
    end
    object QrLDUsCIDADE_2: TWideStringField
      FieldName = 'CIDADE_2'
      Size = 25
    end
    object QrLDUsUF_2: TWideStringField
      FieldName = 'UF_2'
      Size = 2
    end
    object QrLDUsCEP_2: TIntegerField
      FieldName = 'CEP_2'
    end
    object QrLDUsTEL1_2: TWideStringField
      FieldName = 'TEL1_2'
    end
    object QrLDUsTEL1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEL1_TXT'
      Size = 40
      Calculated = True
    end
    object QrLDUsTEL1_2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEL1_2_TXT'
      Size = 40
      Calculated = True
    end
    object QrLDUsCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Calculated = True
    end
    object QrLDUsCEP_2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_2_TXT'
      Calculated = True
    end
    object QrLDUsNUMERO_2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_2_TXT'
      Calculated = True
    end
    object QrLDUsNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Calculated = True
    end
    object QrLDUsIE: TWideStringField
      FieldName = 'IE'
      Size = 25
    end
    object QrLDUsIE_2: TWideStringField
      FieldName = 'IE_2'
      Size = 25
    end
    object QrLDUsDesco: TFloatField
      FieldName = 'Desco'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLDUsBruto: TFloatField
      FieldName = 'Bruto'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLDUsValAcum: TFloatField
      FieldName = 'ValAcum'
    end
    object QrLDUsDuplicado: TIntegerField
      FieldName = 'Duplicado'
    end
    object QrLDUsNOMEDUPLICADO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEDUPLICADO'
      Size = 1
      Calculated = True
    end
    object QrLDUsPraca: TIntegerField
      FieldName = 'Praca'
    end
    object QrLDUsDisponiv: TFloatField
      FieldName = 'Disponiv'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLDUsRISCOEM: TFloatField
      FieldName = 'RISCOEM'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLDUsRISCOSA: TFloatField
      FieldName = 'RISCOSA'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLDUsRISCO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'RISCO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrLDUsBAC: TWideStringField
      FieldName = 'BAC'
      Size = 30
    end
    object QrLDUsNumero_2: TIntegerField
      FieldName = 'Numero_2'
    end
    object QrLDUsStatusSPC: TSmallintField
      FieldName = 'StatusSPC'
    end
    object QrLDUsTEXTO_SPC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEXTO_SPC'
      Size = 30
      Calculated = True
    end
  end
  object DsLDUs: TDataSource
    DataSet = QrLDUs
    Left = 80
    Top = 196
  end
  object QrEmitBAC: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM emitbac'
      'WHERE BAC=:P0'
      'AND CPF=:P1')
    Left = 24
    Top = 244
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrEmitBACBAC: TWideStringField
      FieldName = 'BAC'
    end
    object QrEmitBACCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
  end
  object QrValAcum: TMySQLQuery
    Database = Dmod.MyLocDatabase
    SQL.Strings = (
      'SELECT SUM(il.Valor) Valor, il.CPF, il.Aberto, '
      'il.RISCOEM, il.RISCOSA'
      'FROM loccredr.importlote il'
      'GROUP BY il.CPF')
    Left = 24
    Top = 292
    object QrValAcumValor: TFloatField
      FieldName = 'Valor'
    end
    object QrValAcumCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrValAcumAberto: TFloatField
      FieldName = 'Aberto'
    end
    object QrValAcumRISCOEM: TFloatField
      FieldName = 'RISCOEM'
    end
    object QrValAcumRISCOSA: TFloatField
      FieldName = 'RISCOSA'
    end
  end
  object QrBanco: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nome, DVCC '
      'FROM bancos'
      'WHERE Codigo=:P0')
    Left = 136
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrBancoNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrBancoDVCC: TSmallintField
      FieldName = 'DVCC'
    end
  end
  object DsBanco: TDataSource
    DataSet = QrBanco
    Left = 200
    Top = 148
  end
  object QrSobraIni: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Data, Codigo, SobraNow'
      'FROM lot es'
      'WHERE Cliente=:P0'
      'AND (Data < :P1'
      'OR (Data = :P2 AND Codigo<:P3)) '
      'ORDER BY Data DESC, Codigo DESC')
    Left = 136
    Top = 244
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrSobraIniData: TDateField
      FieldName = 'Data'
    end
    object QrSobraIniCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSobraIniSobraNow: TFloatField
      FieldName = 'SobraNow'
    end
  end
  object QrOutros: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Data, Codigo, Lote, SobraNow'
      'FROM lot es'
      'WHERE Cliente=:P0'
      'AND (Data > :P1'
      'OR (Data = :P2 AND Codigo>:P3)) '
      'ORDER BY Data, Codigo')
    Left = 136
    Top = 292
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrOutrosData: TDateField
      FieldName = 'Data'
    end
    object QrOutrosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOutrosLote: TIntegerField
      FieldName = 'Lote'
    end
    object QrOutrosSobraNow: TFloatField
      FieldName = 'SobraNow'
    end
  end
  object frxOutros: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39137.702415000000000000
    ReportOptions.LastChange = 39137.702415000000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 260
    Top = 292
    Datasets = <
      item
        DataSet = frxDsOutros
        DataSetName = 'frxOutros'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 75.590600000000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 52.913420000000000000
          Width = 718.110700000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'O(s) seguinte(s) lote(s) deve(m) ser recalculado(s) manualmente!')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 200.315090000000000000
        Width = 793.701300000000000000
        DataSet = frxDsOutros
        DataSetName = 'frxOutros'
        RowCount = 0
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DataField = 'Data'
          DataSet = frxDsOutros
          DataSetName = 'frxOutros'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxOutros."Data"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 117.165430000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DataField = 'Codigo'
          DataSet = frxDsOutros
          DataSetName = 'frxOutros'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxOutros."Codigo"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 196.535560000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DataField = 'Lote'
          DataSet = frxDsOutros
          DataSetName = 'frxOutros'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxOutros."Lote"]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 117.165430000000000000
        Width = 793.701300000000000000
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 3.779529999999990000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DataSet = frxDsOutros
          DataSetName = 'frxOutros'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 117.165430000000000000
          Top = 3.779529999999990000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DataSet = frxDsOutros
          DataSetName = 'frxOutros'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'Lote')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 196.535560000000000000
          Top = 3.779529999999990000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DataSet = frxDsOutros
          DataSetName = 'frxOutros'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'Border'#244)
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 86.929190000000000000
        Top = 279.685220000000000000
        Width = 793.701300000000000000
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 11.338590000000000000
          Width = 718.110700000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            
              'Se voc'#234' n'#227'o trabalha com "sobras" de lote, desconsidere este avi' +
              'so!')
          ParentFont = False
        end
      end
    end
  end
  object frxDsOutros: TfrxDBDataset
    UserName = 'frxOutros'
    CloseDataSource = False
    DataSet = QrOutros
    BCDToCurrency = False
    
    Left = 200
    Top = 292
  end
  object Qrwlc: TMySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT * FROM wlotecab'
      'WHERE Baixado=1')
    Left = 136
    Top = 52
    object QrwlcCodigo: TAutoIncField
      FieldName = 'Codigo'
    end
    object QrwlcCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrwlcLote: TIntegerField
      FieldName = 'Lote'
    end
    object QrwlcTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrwlcData: TDateField
      FieldName = 'Data'
    end
    object QrwlcTotal: TFloatField
      FieldName = 'Total'
    end
    object QrwlcDias: TFloatField
      FieldName = 'Dias'
    end
    object QrwlcItens: TIntegerField
      FieldName = 'Itens'
    end
    object QrwlcBaixado: TIntegerField
      FieldName = 'Baixado'
    end
    object QrwlcCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
  end
  object Qrwli: TMySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT wli.* FROM wloteits wli'
      'LEFT JOIN wlotecab wlc ON wli.Codigo=wlc.Codigo'
      'WHERE wlc.Baixado=1'
      'AND wlc.Codigo=:P0')
    Left = 136
    Top = 100
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrwliCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrwliControle: TAutoIncField
      FieldName = 'Controle'
    end
    object QrwliComp: TIntegerField
      FieldName = 'Comp'
      Required = True
    end
    object QrwliPraca: TIntegerField
      FieldName = 'Praca'
      Required = True
    end
    object QrwliBanco: TIntegerField
      FieldName = 'Banco'
      Required = True
    end
    object QrwliAgencia: TIntegerField
      FieldName = 'Agencia'
      Required = True
    end
    object QrwliConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrwliCheque: TIntegerField
      FieldName = 'Cheque'
      Required = True
    end
    object QrwliCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrwliEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrwliBruto: TFloatField
      FieldName = 'Bruto'
      Required = True
    end
    object QrwliDesco: TFloatField
      FieldName = 'Desco'
      Required = True
    end
    object QrwliValor: TFloatField
      FieldName = 'Valor'
      Required = True
    end
    object QrwliEmissao: TDateField
      FieldName = 'Emissao'
      Required = True
    end
    object QrwliDCompra: TDateField
      FieldName = 'DCompra'
      Required = True
    end
    object QrwliDDeposito: TDateField
      FieldName = 'DDeposito'
      Required = True
    end
    object QrwliVencto: TDateField
      FieldName = 'Vencto'
      Required = True
    end
    object QrwliDias: TIntegerField
      FieldName = 'Dias'
      Required = True
    end
    object QrwliDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Required = True
      Size = 12
    end
    object QrwliUser_ID: TIntegerField
      FieldName = 'User_ID'
      Required = True
    end
    object QrwliPasso: TIntegerField
      FieldName = 'Passo'
      Required = True
    end
    object QrwliRua: TWideStringField
      FieldName = 'Rua'
      Size = 30
    end
    object QrwliNumero: TLargeintField
      FieldName = 'Numero'
    end
    object QrwliCompl: TWideStringField
      FieldName = 'Compl'
      Size = 30
    end
    object QrwliBairro: TWideStringField
      FieldName = 'Bairro'
      Size = 30
    end
    object QrwliCidade: TWideStringField
      FieldName = 'Cidade'
      Size = 25
    end
    object QrwliUF: TWideStringField
      FieldName = 'UF'
      Required = True
      Size = 2
    end
    object QrwliCEP: TIntegerField
      FieldName = 'CEP'
    end
    object QrwliTel1: TWideStringField
      FieldName = 'Tel1'
    end
    object QrwliIE: TWideStringField
      FieldName = 'IE'
      Required = True
      Size = 25
    end
  end
  object QrSumLLC: TMySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT SUM(Bruto) Total, COUNT(*) + 0.00 Itens'
      'FROM wloteits lli'
      'WHERE lli.Codigo=:P0'
      '')
    Left = 260
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumLLCTotal: TFloatField
      FieldName = 'Total'
    end
    object QrSumLLCItens: TFloatField
      FieldName = 'Itens'
      Required = True
    end
  end
  object QrLLC: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE ent.Nome'
      
        'END NOMECLI, IF(llc.Tipo=0, "Cheque", "Duplicata") NOMETIPO, llc' +
        '.* '
      'FROM llotecab llc'
      'LEFT JOIN entidades ent ON ent.Codigo=llc.Cliente'
      'WHERE llc.Baixado=2')
    Left = 260
    Top = 52
    object QrLLCCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLLCCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrLLCLote: TIntegerField
      FieldName = 'Lote'
      DisplayFormat = '000000'
    end
    object QrLLCTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrLLCData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLLCTotal: TFloatField
      FieldName = 'Total'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLLCDias: TFloatField
      FieldName = 'Dias'
    end
    object QrLLCItens: TIntegerField
      FieldName = 'Itens'
      DisplayFormat = '00'
    end
    object QrLLCBaixado: TIntegerField
      FieldName = 'Baixado'
    end
    object QrLLCCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrLLCNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrLLCNOMETIPO: TWideStringField
      FieldName = 'NOMETIPO'
      Required = True
      Size = 9
    end
  end
  object QrLLI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM lloteits'
      'WHERE Codigo=:P0')
    Left = 260
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLLICodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'lloteits.Codigo'
    end
    object QrLLIControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'lloteits.Controle'
    end
    object QrLLIComp: TIntegerField
      FieldName = 'Comp'
      Origin = 'lloteits.Comp'
    end
    object QrLLIPraca: TIntegerField
      FieldName = 'Praca'
      Origin = 'lloteits.Praca'
    end
    object QrLLIBanco: TIntegerField
      FieldName = 'Banco'
      Origin = 'lloteits.Banco'
    end
    object QrLLIAgencia: TIntegerField
      FieldName = 'Agencia'
      Origin = 'lloteits.Agencia'
    end
    object QrLLIConta: TWideStringField
      FieldName = 'Conta'
      Origin = 'lloteits.Conta'
    end
    object QrLLICheque: TIntegerField
      FieldName = 'Cheque'
      Origin = 'lloteits.Cheque'
    end
    object QrLLICPF: TWideStringField
      FieldName = 'CPF'
      Origin = 'lloteits.CPF'
      Size = 15
    end
    object QrLLIEmitente: TWideStringField
      FieldName = 'Emitente'
      Origin = 'lloteits.Emitente'
      Size = 50
    end
    object QrLLIBruto: TFloatField
      FieldName = 'Bruto'
      Origin = 'lloteits.Bruto'
    end
    object QrLLIDesco: TFloatField
      FieldName = 'Desco'
      Origin = 'lloteits.Desco'
    end
    object QrLLIValor: TFloatField
      FieldName = 'Valor'
      Origin = 'lloteits.Valor'
    end
    object QrLLIEmissao: TDateField
      FieldName = 'Emissao'
      Origin = 'lloteits.Emissao'
    end
    object QrLLIDCompra: TDateField
      FieldName = 'DCompra'
      Origin = 'lloteits.DCompra'
    end
    object QrLLIDDeposito: TDateField
      FieldName = 'DDeposito'
      Origin = 'lloteits.DDeposito'
    end
    object QrLLIVencto: TDateField
      FieldName = 'Vencto'
      Origin = 'lloteits.Vencto'
    end
    object QrLLIDias: TIntegerField
      FieldName = 'Dias'
      Origin = 'lloteits.Dias'
    end
    object QrLLIDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Origin = 'lloteits.Duplicata'
      Size = 12
    end
    object QrLLIIE: TWideStringField
      FieldName = 'IE'
      Origin = 'lloteits.IE'
      Size = 25
    end
    object QrLLIRua: TWideStringField
      FieldName = 'Rua'
      Origin = 'lloteits.Rua'
      Size = 30
    end
    object QrLLINumero: TLargeintField
      FieldName = 'Numero'
      Origin = 'lloteits.Numero'
    end
    object QrLLICompl: TWideStringField
      FieldName = 'Compl'
      Origin = 'lloteits.Compl'
      Size = 30
    end
    object QrLLIBairro: TWideStringField
      FieldName = 'Bairro'
      Origin = 'lloteits.Bairro'
      Size = 30
    end
    object QrLLICidade: TWideStringField
      FieldName = 'Cidade'
      Origin = 'lloteits.Cidade'
      Size = 25
    end
    object QrLLIUF: TWideStringField
      FieldName = 'UF'
      Origin = 'lloteits.UF'
      Size = 2
    end
    object QrLLICEP: TIntegerField
      FieldName = 'CEP'
      Origin = 'lloteits.CEP'
    end
    object QrLLITel1: TWideStringField
      FieldName = 'Tel1'
      Origin = 'lloteits.Tel1'
    end
    object QrLLIUser_ID: TIntegerField
      FieldName = 'User_ID'
      Origin = 'lloteits.User_ID'
    end
    object QrLLIPasso: TIntegerField
      FieldName = 'Passo'
      Origin = 'lloteits.Passo'
    end
  end
  object DsLLC: TDataSource
    DataSet = QrLLC
    Left = 320
    Top = 4
  end
  object QrUDLC: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'UPDATE lot es SET '
      'AlterWeb=1 '
      'WHERE Codigo=:P0 '
      '')
    Left = 260
    Top = 100
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrVeri: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MAX(Controle) Controle'
      'FROM lloteits')
    Left = 260
    Top = 196
    object QrVeriControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
  end
  object QrCtrl: TMySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT wli.Codigo, wli.Controle '
      'FROM wloteits wli'
      'LEFT JOIN wlotecab wlc ON wli.Codigo=wlc.Codigo'
      'WHERE wlc.Baixado=1'
      'AND wli.Controle < :P0')
    Left = 136
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCtrlCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCtrlControle: TAutoIncField
      FieldName = 'Controle'
    end
  end
  object QrLocSacados: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM sacados'
      'WHERE CNPJ=:P0'
      '')
    Left = 24
    Top = 100
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocSacadosCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 15
    end
    object QrLocSacadosIE: TWideStringField
      FieldName = 'IE'
      Size = 25
    end
    object QrLocSacadosNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrLocSacadosRua: TWideStringField
      FieldName = 'Rua'
      Size = 30
    end
    object QrLocSacadosNumero: TLargeintField
      FieldName = 'Numero'
    end
    object QrLocSacadosCompl: TWideStringField
      FieldName = 'Compl'
      Size = 30
    end
    object QrLocSacadosBairro: TWideStringField
      FieldName = 'Bairro'
      Size = 30
    end
    object QrLocSacadosCidade: TWideStringField
      FieldName = 'Cidade'
      Size = 25
    end
    object QrLocSacadosUF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrLocSacadosCEP: TIntegerField
      FieldName = 'CEP'
    end
    object QrLocSacadosTel1: TWideStringField
      FieldName = 'Tel1'
    end
    object QrLocSacadosRisco: TFloatField
      FieldName = 'Risco'
    end
    object QrLocSacadosAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrLocSacadosAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrSPC_Cfg: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT sm.Valor VALCONSULTA,sc.Codigo, sp.ValMin SPC_ValMin, '
      'sp.ValMax SPC_ValMax, sc.Servidor, sc.Porta SPC_Porta, '
      'sc.Pedinte, sc.CodigSocio, sc.SenhaSocio, sc.Modalidade, '
      'sc.InfoExtra, sc.BAC_CMC7, sc.TipoCred, ValAviso'
      'FROM spc_config sc '
      'LEFT JOIN spc_entida sp ON sp.SPC_Config=sc.Codigo '
      'LEFT JOIN spc_modali sm ON sm.Codigo=sc.Modalidade '
      'WHERE sp.Entidade=:P0')
    Left = 24
    Top = 340
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSPC_CfgSPC_ValMin: TFloatField
      FieldName = 'SPC_ValMin'
    end
    object QrSPC_CfgSPC_ValMax: TFloatField
      FieldName = 'SPC_ValMax'
    end
    object QrSPC_CfgServidor: TWideStringField
      FieldName = 'Servidor'
      Size = 100
    end
    object QrSPC_CfgSPC_Porta: TSmallintField
      FieldName = 'SPC_Porta'
      Required = True
    end
    object QrSPC_CfgPedinte: TWideStringField
      FieldName = 'Pedinte'
      Size = 15
    end
    object QrSPC_CfgCodigSocio: TIntegerField
      FieldName = 'CodigSocio'
      Required = True
    end
    object QrSPC_CfgModalidade: TIntegerField
      FieldName = 'Modalidade'
      Required = True
    end
    object QrSPC_CfgInfoExtra: TIntegerField
      FieldName = 'InfoExtra'
      Required = True
    end
    object QrSPC_CfgBAC_CMC7: TSmallintField
      FieldName = 'BAC_CMC7'
      Required = True
    end
    object QrSPC_CfgTipoCred: TSmallintField
      FieldName = 'TipoCred'
      Required = True
    end
    object QrSPC_CfgSenhaSocio: TWideStringField
      FieldName = 'SenhaSocio'
      Required = True
      Size = 15
    end
    object QrSPC_CfgCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrSPC_CfgValAviso: TFloatField
      FieldName = 'ValAviso'
      Required = True
    end
    object QrSPC_CfgVALCONSULTA: TFloatField
      FieldName = 'VALCONSULTA'
    end
  end
  object QrSCHs: TMySQLQuery
    Database = Dmod.MyLocDatabase
    AfterOpen = QrSCHsAfterOpen
    BeforeClose = QrSCHsBeforeClose
    SQL.Strings = (
      'SELECT COUNT(*) Itens, SUM(Valor) Valor'
      'FROM importlote '
      'WHERE Tipo=0')
    Left = 380
    Top = 4
    object QrSCHsValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSCHsItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
  end
  object DsSCHs: TDataSource
    DataSet = QrSCHs
    Left = 444
    Top = 52
  end
  object DsSDUs: TDataSource
    DataSet = QrSDUs
    Left = 444
    Top = 4
  end
  object QrSDUs: TMySQLQuery
    Database = Dmod.MyLocDatabase
    AfterOpen = QrSDUsAfterOpen
    BeforeClose = QrSDUsBeforeClose
    SQL.Strings = (
      'SELECT COUNT(*) Itens, SUM(Valor) Valor'
      'FROM importlote '
      'WHERE Tipo=1')
    Left = 380
    Top = 52
    object QrSDUsValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSDUsItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
    object QrSDUsTOTAL_V: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TOTAL_V'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrSDUsTOTAL_Q: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TOTAL_Q'
      Calculated = True
    end
  end
  object QrILd: TMySQLQuery
    Database = Dmod.MyLocDatabase
    AfterScroll = QrLCHsAfterScroll
    OnCalcFields = QrLCHsCalcFields
    SQL.Strings = (
      'SELECT  DISTINCT CPF'
      'FROM importlote'
      'WHERE Tipo=:P0')
    Left = 24
    Top = 388
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrILdCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
  end
  object QrUpdLocDB: TMySQLQuery
    Database = Dmod.MyLocDatabase
    Left = 544
    Top = 300
  end
  object QrLot: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrLotBeforeClose
    OnCalcFields = QrLotCalcFields
    SQL.Strings = (
      'SELECT CASE WHEN en.Tipo=0 THEN en.RazaoSocial'
      'ELSE en.Nome END NOMECLIENTE, '
      'CASE WHEN en.Tipo=0 THEN en.Fantasia '
      'ELSE en.Apelido END NOMEFANCLIENTE,'
      'CASE WHEN en.Tipo=0 THEN en.CNPJ'
      'ELSE en.CPF END CNPJCPF, lo.*, en.LimiCred, '
      'en.CBE CBECli, en.SCB SCBCli, QuantN1, QuantI1'
      'FROM lot es lo'
      'LEFT JOIN entidades en ON en.Codigo=lo.Cliente'
      'WHERE lo.Codigo > 0')
    Left = 732
    Top = 4
    object QrLotCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLotTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrLotCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrLotLote: TSmallintField
      FieldName = 'Lote'
      DisplayFormat = '000'
    end
    object QrLotData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLotTotal: TFloatField
      FieldName = 'Total'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLotDias: TFloatField
      FieldName = 'Dias'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLotPeCompra: TFloatField
      FieldName = 'PeCompra'
      Required = True
    end
    object QrLotTxCompra: TFloatField
      FieldName = 'TxCompra'
    end
    object QrLotAdValorem: TFloatField
      FieldName = 'AdValorem'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrLotIOC: TFloatField
      FieldName = 'IOC'
    end
    object QrLotCPMF: TFloatField
      FieldName = 'CPMF'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrLotLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLotDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLotDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLotUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrLotUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrLotNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrLotTipoAdV: TIntegerField
      FieldName = 'TipoAdV'
      Required = True
    end
    object QrLotADVALOREM_TOTAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ADVALOREM_TOTAL'
      DisplayFormat = '#,##0.000000'
      Calculated = True
    end
    object QrLotVALVALOREM_TOTAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VALVALOREM_TOTAL'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrLotValValorem: TFloatField
      FieldName = 'ValValorem'
      Required = True
    end
    object QrLotSpread: TSmallintField
      FieldName = 'Spread'
      Required = True
    end
    object QrLotTAXAVALOR_TOTAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TAXAVALOR_TOTAL'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrLotTAXAPERCE_TOTAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TAXAPERCE_TOTAL'
      DisplayFormat = '#,##0.000000'
      Calculated = True
    end
    object QrLotTAXAPERCE_PROPR: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TAXAPERCE_PROPR'
      Calculated = True
    end
    object QrLotIOC_VAL: TFloatField
      FieldName = 'IOC_VAL'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLotCPMF_VAL: TFloatField
      FieldName = 'CPMF_VAL'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLotSUB_TOTAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SUB_TOTAL'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrLotVAL_LIQUIDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VAL_LIQUIDO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrLotTOT_ISS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TOT_ISS'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrLotTOT_IRR: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TOT_IRR'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrLotISS: TFloatField
      FieldName = 'ISS'
      Required = True
    end
    object QrLotISS_Val: TFloatField
      FieldName = 'ISS_Val'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrLotPIS_R: TFloatField
      FieldName = 'PIS_R'
      Required = True
    end
    object QrLotPIS_R_Val: TFloatField
      FieldName = 'PIS_R_Val'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrLotIRRF: TFloatField
      FieldName = 'IRRF'
      Required = True
    end
    object QrLotIRRF_Val: TFloatField
      FieldName = 'IRRF_Val'
      Required = True
    end
    object QrLotVAL_LIQUIDO_MEU: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VAL_LIQUIDO_MEU'
      Calculated = True
    end
    object QrLotSUB_TOTAL_MEU: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SUB_TOTAL_MEU'
      Calculated = True
    end
    object QrLotCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 18
    end
    object QrLotCNPJ_CPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_CPF_TXT'
      Size = 40
      Calculated = True
    end
    object QrLotTAXA_AM_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TAXA_AM_TXT'
      Size = 30
      Calculated = True
    end
    object QrLotADVAL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ADVAL_TXT'
      Size = 30
      Calculated = True
    end
    object QrLotTarifas: TFloatField
      FieldName = 'Tarifas'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLotOcorP: TFloatField
      FieldName = 'OcorP'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLotSUB_TOTAL_2: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SUB_TOTAL_2'
      Calculated = True
    end
    object QrLotCOFINS_R: TFloatField
      FieldName = 'COFINS_R'
      Required = True
      DisplayFormat = '#,###,##0.0000'
    end
    object QrLotCOFINS_R_Val: TFloatField
      FieldName = 'COFINS_R_Val'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrLotPIS: TFloatField
      FieldName = 'PIS'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLotCOFINS: TFloatField
      FieldName = 'COFINS'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLotMEU_PISCOFINS_VAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'MEU_PISCOFINS_VAL'
      Calculated = True
    end
    object QrLotPIS_Val: TFloatField
      FieldName = 'PIS_Val'
      Required = True
    end
    object QrLotCOFINS_Val: TFloatField
      FieldName = 'COFINS_Val'
      Required = True
    end
    object QrLotMEU_PISCOFINS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'MEU_PISCOFINS'
      Calculated = True
    end
    object QrLotMaxVencto: TDateField
      FieldName = 'MaxVencto'
      Required = True
    end
    object QrLotSUB_TOTAL_3: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SUB_TOTAL_3'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrLotCHDevPg: TFloatField
      FieldName = 'CHDevPg'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrLotMINTC: TFloatField
      FieldName = 'MINTC'
      Required = True
    end
    object QrLotMINAV: TFloatField
      FieldName = 'MINAV'
      Required = True
    end
    object QrLotMINTC_AM: TFloatField
      FieldName = 'MINTC_AM'
      Required = True
    end
    object QrLotPIS_T_Val: TFloatField
      FieldName = 'PIS_T_Val'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrLotCOFINS_T_Val: TFloatField
      FieldName = 'COFINS_T_Val'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrLotPgLiq: TFloatField
      FieldName = 'PgLiq'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrLotA_PG_LIQ: TFloatField
      FieldKind = fkCalculated
      FieldName = 'A_PG_LIQ'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrLotDUDevPg: TFloatField
      FieldName = 'DUDevPg'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrLotNOMETIPOLOTE: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMETIPOLOTE'
      Calculated = True
    end
    object QrLotNF: TIntegerField
      FieldName = 'NF'
      Required = True
      DisplayFormat = '000000;-000000; '
    end
    object QrLotTAXAPERCEMENSAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TAXAPERCEMENSAL'
      DisplayFormat = '#,##0.00'
      Calculated = True
    end
    object QrLotItens: TIntegerField
      FieldName = 'Itens'
      Required = True
    end
    object QrLotConferido: TIntegerField
      FieldName = 'Conferido'
      Required = True
    end
    object QrLotECartaSac: TSmallintField
      FieldName = 'ECartaSac'
      Required = True
    end
    object QrLotNOMECOFECARTA: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMECOFECARTA'
      Size = 3
      Calculated = True
    end
    object QrLotCAPTIONCONFCARTA: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CAPTIONCONFCARTA'
      Calculated = True
    end
    object QrLotNOMESPREAD: TWideStringField
      DisplayWidth = 5
      FieldKind = fkCalculated
      FieldName = 'NOMESPREAD'
      Size = 5
      Calculated = True
    end
    object QrLotLimiCred: TFloatField
      FieldName = 'LimiCred'
      Origin = 'entidades.LimiCred'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLotCBECli: TIntegerField
      FieldName = 'CBECli'
      Origin = 'entidades.CBE'
    end
    object QrLotSCBCli: TIntegerField
      FieldName = 'SCBCli'
      Origin = 'entidades.SCB'
    end
    object QrLotCBE: TIntegerField
      FieldName = 'CBE'
      Required = True
    end
    object QrLotSCB: TIntegerField
      FieldName = 'SCB'
      Required = True
    end
    object QrLotSALDOAPAGAR: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDOAPAGAR'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrLotQuantN1: TFloatField
      FieldName = 'QuantN1'
      Origin = 'entidades.QuantN1'
    end
    object QrLotQuantI1: TIntegerField
      FieldName = 'QuantI1'
      Origin = 'entidades.QuantI1'
    end
    object QrLotAllQuit: TSmallintField
      FieldName = 'AllQuit'
      Required = True
    end
    object QrLotSobraIni: TFloatField
      FieldName = 'SobraIni'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrLotSobraNow: TFloatField
      FieldName = 'SobraNow'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrLotIOC_VALNEG: TFloatField
      FieldKind = fkCalculated
      FieldName = 'IOC_VALNEG'
      Calculated = True
    end
    object QrLotCPMF_VALNEG: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CPMF_VALNEG'
      Calculated = True
    end
    object QrLotTAXAVALOR_TOTALNEG: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TAXAVALOR_TOTALNEG'
      Calculated = True
    end
    object QrLotVAVALOREM_TOTALNEG: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VAVALOREM_TOTALNEG'
      Calculated = True
    end
    object QrLotCHDevPgNEG: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CHDevPgNEG'
      Calculated = True
    end
    object QrLotDUDevPgNEG: TFloatField
      FieldKind = fkCalculated
      FieldName = 'DUDevPgNEG'
      Calculated = True
    end
    object QrLotIOFd_VAL: TFloatField
      FieldName = 'IOFd_VAL'
      Required = True
    end
    object QrLotIOFv_VAL: TFloatField
      FieldName = 'IOFv_VAL'
      Required = True
    end
    object QrLotNOMEFANCLIENTE: TWideStringField
      FieldName = 'NOMEFANCLIENTE'
      Size = 60
    end
    object QrLotWebCod: TIntegerField
      FieldName = 'WebCod'
    end
    object QrLotAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrLotIOFd: TFloatField
      FieldName = 'IOFd'
    end
    object QrLotIOFv: TFloatField
      FieldName = 'IOFv'
    end
    object QrLotTipoIOF: TSmallintField
      FieldName = 'TipoIOF'
    end
    object QrLotAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrLotLctsAuto: TIntegerField
      FieldName = 'LctsAuto'
    end
  end
  object QrEmLot1: TMySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT * FROM emlot'
      'WHERE Codigo=:P0')
    Left = 732
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmLot1IRRF: TFloatField
      FieldName = 'IRRF'
    end
    object QrEmLot1IRRF_Val: TFloatField
      FieldName = 'IRRF_Val'
    end
    object QrEmLot1ISS: TFloatField
      FieldName = 'ISS'
    end
    object QrEmLot1ISS_Val: TFloatField
      FieldName = 'ISS_Val'
    end
    object QrEmLot1PIS_R: TFloatField
      FieldName = 'PIS_R'
    end
    object QrEmLot1PIS_R_Val: TFloatField
      FieldName = 'PIS_R_Val'
    end
    object QrEmLot1TaxaVal: TFloatField
      FieldName = 'TaxaVal'
    end
    object QrEmLot1ValValorem: TFloatField
      FieldName = 'ValValorem'
    end
    object QrEmLot1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmLot1AdValorem: TFloatField
      FieldName = 'AdValorem'
    end
  end
  object QrEmLot2: TMySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT * FROM emlot'
      'WHERE Codigo=:P0')
    Left = 732
    Top = 104
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmLot2IRRF: TFloatField
      FieldName = 'IRRF'
    end
    object QrEmLot2IRRF_Val: TFloatField
      FieldName = 'IRRF_Val'
    end
    object QrEmLot2ISS: TFloatField
      FieldName = 'ISS'
    end
    object QrEmLot2ISS_Val: TFloatField
      FieldName = 'ISS_Val'
    end
    object QrEmLot2PIS_R: TFloatField
      FieldName = 'PIS_R'
    end
    object QrEmLot2PIS_R_Val: TFloatField
      FieldName = 'PIS_R_Val'
    end
    object QrEmLot2TaxaVal: TFloatField
      FieldName = 'TaxaVal'
    end
    object QrEmLot2ValValorem: TFloatField
      FieldName = 'ValValorem'
    end
    object QrEmLot2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmLot2AdValorem: TFloatField
      FieldName = 'AdValorem'
    end
  end
  object QrEmLot3: TMySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT * FROM emlot'
      'WHERE Codigo=:P0')
    Left = 732
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmLot3IRRF: TFloatField
      FieldName = 'IRRF'
    end
    object QrEmLot3IRRF_Val: TFloatField
      FieldName = 'IRRF_Val'
    end
    object QrEmLot3ISS: TFloatField
      FieldName = 'ISS'
    end
    object QrEmLot3ISS_Val: TFloatField
      FieldName = 'ISS_Val'
    end
    object QrEmLot3PIS_R: TFloatField
      FieldName = 'PIS_R'
    end
    object QrEmLot3PIS_R_Val: TFloatField
      FieldName = 'PIS_R_Val'
    end
    object QrEmLot3TaxaVal: TFloatField
      FieldName = 'TaxaVal'
    end
    object QrEmLot3ValValorem: TFloatField
      FieldName = 'ValValorem'
    end
    object QrEmLot3Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmLot3AdValorem: TFloatField
      FieldName = 'AdValorem'
    end
  end
  object QrEmLot4: TMySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT * FROM emlot'
      'WHERE Codigo=:P0')
    Left = 732
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmLot4IRRF: TFloatField
      FieldName = 'IRRF'
    end
    object QrEmLot4IRRF_Val: TFloatField
      FieldName = 'IRRF_Val'
    end
    object QrEmLot4ISS: TFloatField
      FieldName = 'ISS'
    end
    object QrEmLot4ISS_Val: TFloatField
      FieldName = 'ISS_Val'
    end
    object QrEmLot4PIS_R: TFloatField
      FieldName = 'PIS_R'
    end
    object QrEmLot4PIS_R_Val: TFloatField
      FieldName = 'PIS_R_Val'
    end
    object QrEmLot4TaxaVal: TFloatField
      FieldName = 'TaxaVal'
    end
    object QrEmLot4ValValorem: TFloatField
      FieldName = 'ValValorem'
    end
    object QrEmLot4Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmLot4AdValorem: TFloatField
      FieldName = 'AdValorem'
    end
  end
  object QrEmLot5: TMySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT * FROM emlot'
      'WHERE Codigo=:P0')
    Left = 732
    Top = 244
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmLot5IRRF: TFloatField
      FieldName = 'IRRF'
    end
    object QrEmLot5IRRF_Val: TFloatField
      FieldName = 'IRRF_Val'
    end
    object QrEmLot5ISS: TFloatField
      FieldName = 'ISS'
    end
    object QrEmLot5ISS_Val: TFloatField
      FieldName = 'ISS_Val'
    end
    object QrEmLot5PIS_R: TFloatField
      FieldName = 'PIS_R'
    end
    object QrEmLot5PIS_R_Val: TFloatField
      FieldName = 'PIS_R_Val'
    end
    object QrEmLot5TaxaVal: TFloatField
      FieldName = 'TaxaVal'
    end
    object QrEmLot5ValValorem: TFloatField
      FieldName = 'ValValorem'
    end
    object QrEmLot5Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmLot5AdValorem: TFloatField
      FieldName = 'AdValorem'
    end
  end
  object QrEmLot6: TMySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT * FROM emlot'
      'WHERE Codigo=:P0')
    Left = 732
    Top = 292
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmLot6IRRF: TFloatField
      FieldName = 'IRRF'
    end
    object QrEmLot6IRRF_Val: TFloatField
      FieldName = 'IRRF_Val'
    end
    object QrEmLot6ISS: TFloatField
      FieldName = 'ISS'
    end
    object QrEmLot6ISS_Val: TFloatField
      FieldName = 'ISS_Val'
    end
    object QrEmLot6PIS_R: TFloatField
      FieldName = 'PIS_R'
    end
    object QrEmLot6PIS_R_Val: TFloatField
      FieldName = 'PIS_R_Val'
    end
    object QrEmLot6TaxaVal: TFloatField
      FieldName = 'TaxaVal'
    end
    object QrEmLot6ValValorem: TFloatField
      FieldName = 'ValValorem'
    end
    object QrEmLot6Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmLot6AdValorem: TFloatField
      FieldName = 'AdValorem'
    end
  end
  object QrIDLct: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Credito, Controle, '
      'FatID, FatNum, FatParcela'
      'FROM lct0001a'
      'WHERE FatID=322'
      'AND FatNum=1234567')
    Left = 732
    Top = 344
    object QrIDLctCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrIDLctControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrIDLctFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrIDLctFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrIDLctFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
  end
end
