object FmCNAB_Rem2: TFmCNAB_Rem2
  Left = 377
  Top = 168
  Caption = 'FBB-CNABC-007 :: Remessa CNAB B'
  ClientHeight = 622
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 460
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 48
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label23: TLabel
        Left = 4
        Top = 4
        Width = 110
        Height = 13
        Caption = 'Configura'#231#227'o de envio:'
        FocusControl = DBEdit1
      end
      object Label25: TLabel
        Left = 480
        Top = 8
        Width = 46
        Height = 13
        Caption = 'Posi'#231#245'es:'
        FocusControl = DBEdit3
      end
      object DBEdit1: TDBEdit
        Left = 4
        Top = 20
        Width = 41
        Height = 21
        DataField = 'Codigo'
        DataSource = DmBco.DsCNAB_Cfg
        TabOrder = 0
      end
      object DBEdit2: TDBEdit
        Left = 48
        Top = 20
        Width = 304
        Height = 21
        DataField = 'Nome'
        DataSource = DmBco.DsCNAB_Cfg
        TabOrder = 1
      end
      object DBRadioGroup1: TDBRadioGroup
        Left = 356
        Top = 1
        Width = 117
        Height = 46
        Caption = ' CNAB: '
        Columns = 2
        DataField = 'CNAB'
        DataSource = DmBco.DsCNAB_Cfg
        Items.Strings = (
          '240'
          '400')
        ParentBackground = True
        TabOrder = 2
        Values.Strings = (
          '240'
          '400')
      end
      object DBEdit3: TDBEdit
        Left = 480
        Top = 24
        Width = 49
        Height = 21
        DataField = 'PosicoesBB'
        DataSource = DmBco.DsCNAB_Cfg
        TabOrder = 3
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 48
      Width = 1008
      Height = 412
      ActivePage = TabSheet13
      Align = alClient
      TabOrder = 1
      object TabSheet1: TTabSheet
        Caption = 'Arquivos'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 384
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Panel10: TPanel
            Left = 0
            Top = 335
            Width = 1000
            Height = 49
            Align = alBottom
            TabOrder = 0
            object Label11: TLabel
              Left = 4
              Top = 4
              Width = 29
              Height = 13
              Caption = 'Linha:'
            end
            object Label12: TLabel
              Left = 44
              Top = 4
              Width = 36
              Height = 13
              Caption = 'Coluna:'
            end
            object Label13: TLabel
              Left = 84
              Top = 4
              Width = 27
              Height = 13
              Caption = 'Tam.:'
            end
            object Label14: TLabel
              Left = 124
              Top = 4
              Width = 87
              Height = 13
              Caption = 'Texto pesquisado:'
            end
            object Edit2: TEdit
              Left = 4
              Top = 20
              Width = 37
              Height = 21
              TabOrder = 0
              Text = '0'
              OnChange = Edit2Change
            end
            object Edit3: TEdit
              Left = 44
              Top = 20
              Width = 37
              Height = 21
              TabOrder = 1
              Text = '0'
              OnChange = Edit3Change
            end
            object Edit4: TEdit
              Left = 84
              Top = 20
              Width = 37
              Height = 21
              TabOrder = 2
              Text = '0'
              OnChange = Edit4Change
            end
            object Edit6: TEdit
              Left = 124
              Top = 20
              Width = 653
              Height = 21
              ReadOnly = True
              TabOrder = 3
            end
          end
          object PageControl2: TPageControl
            Left = 0
            Top = 0
            Width = 1000
            Height = 335
            ActivePage = TabSheet10
            Align = alClient
            TabOrder = 1
            OnChange = PageControl2Change
            object TabSheet10: TTabSheet
              Caption = 'Gerado'
              object Panel11: TPanel
                Left = 0
                Top = 0
                Width = 992
                Height = 307
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object Panel8: TPanel
                  Left = 0
                  Top = 0
                  Width = 992
                  Height = 29
                  Align = alTop
                  Caption = 'Panel8'
                  TabOrder = 0
                  object Label15: TLabel
                    Left = 8
                    Top = 8
                    Width = 39
                    Height = 13
                    Caption = 'Arquivo:'
                  end
                  object EdArqGerado: TEdit
                    Left = 52
                    Top = 4
                    Width = 717
                    Height = 21
                    ReadOnly = True
                    TabOrder = 0
                  end
                end
                object MeGerado: TMemo
                  Left = 0
                  Top = 29
                  Width = 992
                  Height = 257
                  Align = alClient
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Courier New'
                  Font.Style = []
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 1
                  WordWrap = False
                  OnEnter = MeGeradoEnter
                  OnExit = MeGeradoExit
                end
                object dmkMBGravado: TdmkMemoBar
                  Left = 0
                  Top = 286
                  Width = 992
                  Height = 21
                  Align = alBottom
                  BevelOuter = bvNone
                  TabOrder = 2
                  Memo = MeGerado
                  OverwriteCaretWidth = 12
                  OverwriteCaretHeight = 16
                end
              end
            end
            object TabSheet11: TTabSheet
              Caption = 'Arquivo a ser comparado'
              ImageIndex = 1
              object Panel7: TPanel
                Left = 0
                Top = 0
                Width = 992
                Height = 307
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object Panel12: TPanel
                  Left = 0
                  Top = 0
                  Width = 992
                  Height = 29
                  Align = alTop
                  BevelOuter = bvNone
                  TabOrder = 0
                  object Label28: TLabel
                    Left = 8
                    Top = 8
                    Width = 39
                    Height = 13
                    Caption = 'Arquivo:'
                  end
                  object SpeedButton1: TSpeedButton
                    Left = 748
                    Top = 4
                    Width = 21
                    Height = 21
                    Caption = '...'
                    OnClick = SpeedButton1Click
                  end
                  object EdArqCompar: TEdit
                    Left = 52
                    Top = 4
                    Width = 693
                    Height = 21
                    ReadOnly = True
                    TabOrder = 0
                  end
                end
                object MeCompar: TMemo
                  Left = 0
                  Top = 29
                  Width = 992
                  Height = 257
                  Align = alClient
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Courier New'
                  Font.Style = []
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 1
                  WordWrap = False
                  OnEnter = MeComparEnter
                  OnExit = MeComparExit
                end
                object dmkMBCompar: TdmkMemoBar
                  Left = 0
                  Top = 286
                  Width = 992
                  Height = 21
                  Align = alBottom
                  BevelOuter = bvNone
                  TabOrder = 2
                  Memo = MeCompar
                  OverwriteCaretWidth = 12
                  OverwriteCaretHeight = 16
                end
              end
            end
            object TabSheet7: TTabSheet
              Caption = 'Compara'#231#227'o'
              ImageIndex = 2
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object Panel13: TPanel
                Left = 0
                Top = 0
                Width = 992
                Height = 307
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object GradeG: TStringGrid
                  Left = 0
                  Top = 86
                  Width = 992
                  Height = 161
                  Align = alClient
                  ColCount = 240
                  DefaultColWidth = 7
                  DefaultRowHeight = 13
                  FixedCols = 0
                  RowCount = 2
                  FixedRows = 0
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Courier New'
                  Font.Style = []
                  Options = [goFixedVertLine, goFixedHorzLine, goEditing]
                  ParentFont = False
                  TabOrder = 0
                  OnDrawCell = GradeGDrawCell
                  OnSelectCell = GradeGSelectCell
                end
                object Panel17: TPanel
                  Left = 0
                  Top = 0
                  Width = 992
                  Height = 69
                  Align = alTop
                  BevelOuter = bvNone
                  TabOrder = 1
                  object Panel18: TPanel
                    Left = 0
                    Top = 0
                    Width = 101
                    Height = 69
                    Align = alLeft
                    BevelOuter = bvNone
                    TabOrder = 0
                    object Label22: TLabel
                      Left = 0
                      Top = 48
                      Width = 96
                      Height = 13
                      Caption = 'Endere'#231'o, tamanho:'
                    end
                    object BitBtn1: TBitBtn
                      Left = 0
                      Top = 1
                      Width = 90
                      Height = 40
                      Caption = '&Compara'
                      NumGlyphs = 2
                      TabOrder = 0
                      OnClick = BitBtn1Click
                    end
                  end
                  object Panel19: TPanel
                    Left = 101
                    Top = 0
                    Width = 891
                    Height = 69
                    Align = alClient
                    BevelOuter = bvNone
                    TabOrder = 1
                    object Label16: TLabel
                      Left = 0
                      Top = 4
                      Width = 29
                      Height = 13
                      Caption = 'Linha:'
                    end
                    object Label17: TLabel
                      Left = 0
                      Top = 26
                      Width = 36
                      Height = 13
                      Caption = 'Coluna:'
                    end
                    object Label18: TLabel
                      Left = 88
                      Top = 48
                      Width = 36
                      Height = 13
                      Caption = 'Campo:'
                    end
                    object Label19: TLabel
                      Left = 88
                      Top = 26
                      Width = 83
                      Height = 13
                      Caption = 'Valor comparado:'
                    end
                    object Label20: TLabel
                      Left = 88
                      Top = 4
                      Width = 63
                      Height = 13
                      Caption = 'Valor gerado:'
                    end
                    object dmkEdVerifLin: TdmkEdit
                      Left = 40
                      Top = 0
                      Width = 44
                      Height = 21
                      Alignment = taRightJustify
                      ReadOnly = True
                      TabOrder = 0
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 3
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '000'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                    end
                    object dmkEdVerifCol: TdmkEdit
                      Left = 40
                      Top = 22
                      Width = 44
                      Height = 21
                      Alignment = taRightJustify
                      ReadOnly = True
                      TabOrder = 1
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 3
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '000'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                    end
                    object dmkEdVerifValCompar: TdmkEdit
                      Left = 172
                      Top = 22
                      Width = 1920
                      Height = 21
                      ReadOnly = True
                      TabOrder = 2
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 3
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object dmkEdVerifCampoCod: TdmkEdit
                      Left = 128
                      Top = 44
                      Width = 44
                      Height = 21
                      Alignment = taRightJustify
                      ReadOnly = True
                      TabOrder = 3
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 3
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '-1000'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '000'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                    end
                    object dmkEdVerifValGerado: TdmkEdit
                      Left = 172
                      Top = 0
                      Width = 1920
                      Height = 21
                      ReadOnly = True
                      TabOrder = 4
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 3
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object dmkEdVerifCampoTxt: TdmkEdit
                      Left = 172
                      Top = 44
                      Width = 1920
                      Height = 21
                      ReadOnly = True
                      TabOrder = 5
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 3
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object dmkEdVerifPos: TdmkEdit
                      Left = 0
                      Top = 44
                      Width = 84
                      Height = 21
                      ReadOnly = True
                      TabOrder = 6
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 3
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '000 a 000 = 000'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = '000 a 000 = 000'
                      ValWarn = False
                    end
                  end
                end
                object GradeO: TStringGrid
                  Left = 5
                  Top = 136
                  Width = 108
                  Height = 65
                  ColCount = 240
                  DefaultColWidth = 7
                  DefaultRowHeight = 13
                  FixedCols = 0
                  RowCount = 2
                  FixedRows = 0
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Courier New'
                  Font.Style = []
                  Options = [goFixedVertLine, goFixedHorzLine, goEditing]
                  ParentFont = False
                  TabOrder = 2
                  Visible = False
                end
                object GradeC: TStringGrid
                  Left = 0
                  Top = 287
                  Width = 992
                  Height = 20
                  Align = alBottom
                  ColCount = 240
                  DefaultColWidth = 24
                  DefaultRowHeight = 13
                  FixedCols = 0
                  RowCount = 2
                  FixedRows = 0
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Courier New'
                  Font.Style = []
                  Options = [goFixedVertLine, goFixedHorzLine, goEditing]
                  ParentFont = False
                  ScrollBars = ssNone
                  TabOrder = 3
                  Visible = False
                end
                object GradeI: TStringGrid
                  Left = 0
                  Top = 247
                  Width = 992
                  Height = 20
                  Align = alBottom
                  ColCount = 240
                  DefaultColWidth = 24
                  DefaultRowHeight = 13
                  FixedCols = 0
                  RowCount = 2
                  FixedRows = 0
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Courier New'
                  Font.Style = []
                  Options = [goFixedVertLine, goFixedHorzLine, goEditing]
                  ParentFont = False
                  ScrollBars = ssNone
                  TabOrder = 4
                  Visible = False
                end
                object GradeF: TStringGrid
                  Left = 0
                  Top = 267
                  Width = 992
                  Height = 20
                  Align = alBottom
                  ColCount = 240
                  DefaultColWidth = 24
                  DefaultRowHeight = 13
                  FixedCols = 0
                  RowCount = 2
                  FixedRows = 0
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Courier New'
                  Font.Style = []
                  Options = [goFixedVertLine, goFixedHorzLine, goEditing]
                  ParentFont = False
                  ScrollBars = ssNone
                  TabOrder = 5
                  Visible = False
                end
                object PB1: TProgressBar
                  Left = 0
                  Top = 69
                  Width = 992
                  Height = 17
                  Align = alTop
                  TabOrder = 6
                end
              end
            end
          end
        end
      end
      object TabSheet4: TTabSheet
        Caption = '  CNAB 240  '
        ImageIndex = 3
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object PageControl4: TPageControl
          Left = 0
          Top = 0
          Width = 1000
          Height = 384
          ActivePage = TabSheet17
          Align = alClient
          TabOrder = 0
          object TabSheet9: TTabSheet
            Caption = ' [0] Header de arquivo '
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Grade_240_0: TStringGrid
              Left = 0
              Top = 0
              Width = 990
              Height = 355
              Align = alClient
              ColCount = 2
              DefaultColWidth = 32
              DefaultRowHeight = 18
              FixedCols = 0
              RowCount = 2
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
              TabOrder = 0
              OnSelectCell = Grade_1SelectCell
            end
          end
          object TabSheet2: TTabSheet
            Caption = '[1] Header de lote '
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Grade_240_1: TStringGrid
              Left = 0
              Top = 0
              Width = 990
              Height = 355
              Align = alClient
              ColCount = 2
              DefaultColWidth = 32
              DefaultRowHeight = 18
              FixedCols = 0
              RowCount = 2
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
              TabOrder = 0
              OnSelectCell = Grade_1SelectCell
            end
          end
          object TabSheet17: TTabSheet
            Caption = '[3] Detalhes '
            ImageIndex = 2
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object PageControl5: TPageControl
              Left = 0
              Top = 0
              Width = 992
              Height = 356
              ActivePage = TabSheet20
              Align = alClient
              TabOrder = 0
              object TabSheet18: TTabSheet
                Caption = ' [P] '
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object Grade_240_3_P: TStringGrid
                  Left = 0
                  Top = 0
                  Width = 982
                  Height = 327
                  Align = alClient
                  ColCount = 2
                  DefaultColWidth = 32
                  DefaultRowHeight = 18
                  FixedCols = 0
                  RowCount = 2
                  Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
                  TabOrder = 0
                  OnSelectCell = Grade_1SelectCell
                end
              end
              object TabSheet19: TTabSheet
                Caption = ' [Q] '
                ImageIndex = 1
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object Grade_240_3_Q: TStringGrid
                  Left = 0
                  Top = 0
                  Width = 982
                  Height = 327
                  Align = alClient
                  ColCount = 2
                  DefaultColWidth = 32
                  DefaultRowHeight = 18
                  FixedCols = 0
                  RowCount = 2
                  Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
                  TabOrder = 0
                  OnSelectCell = Grade_1SelectCell
                end
              end
              object TabSheet20: TTabSheet
                Caption = ' [R] '
                ImageIndex = 2
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object Grade_240_3_R: TStringGrid
                  Left = 0
                  Top = 0
                  Width = 984
                  Height = 328
                  Align = alClient
                  ColCount = 2
                  DefaultColWidth = 32
                  DefaultRowHeight = 18
                  FixedCols = 0
                  RowCount = 2
                  Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
                  TabOrder = 0
                  OnSelectCell = Grade_1SelectCell
                end
              end
            end
          end
          object TabSheet21: TTabSheet
            Caption = '[5] Trailer de lote'
            ImageIndex = 3
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Grade_240_5: TStringGrid
              Left = 0
              Top = 0
              Width = 990
              Height = 355
              Align = alClient
              ColCount = 2
              DefaultColWidth = 32
              DefaultRowHeight = 18
              FixedCols = 0
              RowCount = 2
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
              TabOrder = 0
              OnSelectCell = Grade_1SelectCell
            end
          end
          object TabSheet22: TTabSheet
            Caption = ' [9] Trailer do arquivo '
            ImageIndex = 4
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Grade_240_9: TStringGrid
              Left = 0
              Top = 0
              Width = 990
              Height = 355
              Align = alClient
              ColCount = 2
              DefaultColWidth = 32
              DefaultRowHeight = 18
              FixedCols = 0
              RowCount = 2
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
              TabOrder = 0
              OnSelectCell = Grade_1SelectCell
            end
          end
        end
      end
      object TabSheet13: TTabSheet
        Caption = ' CNAB 400 '
        ImageIndex = 9
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object PageControl3: TPageControl
          Left = 0
          Top = 0
          Width = 1000
          Height = 384
          ActivePage = TabSheet16
          Align = alClient
          TabOrder = 0
          object TabSheet16: TTabSheet
            Caption = ' [0] Header '
            ImageIndex = 6
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel4: TPanel
              Left = 0
              Top = 0
              Width = 992
              Height = 356
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object GroupBox1: TGroupBox
                Left = 0
                Top = 0
                Width = 992
                Height = 105
                Align = alTop
                Caption = ' Dados do registro'
                TabOrder = 0
                object Label1: TLabel
                  Left = 8
                  Top = 16
                  Width = 29
                  Height = 13
                  Caption = 'Linha:'
                end
                object Label2: TLabel
                  Left = 276
                  Top = 16
                  Width = 42
                  Height = 13
                  Caption = 'Registro:'
                end
                object Label3: TLabel
                  Left = 48
                  Top = 16
                  Width = 39
                  Height = 13
                  Caption = 'Servi'#231'o:'
                end
                object Label7: TLabel
                  Left = 300
                  Top = 56
                  Width = 44
                  Height = 13
                  Caption = 'Empresa:'
                end
                object Label9: TLabel
                  Left = 404
                  Top = 16
                  Width = 169
                  Height = 13
                  Caption = 'Identifica'#231#227'o do cedente no banco:'
                end
                object Label24: TLabel
                  Left = 840
                  Top = 16
                  Width = 58
                  Height = 13
                  Caption = 'Dt Gera'#231#227'o:'
                end
                object Label26: TLabel
                  Left = 912
                  Top = 16
                  Width = 74
                  Height = 13
                  Caption = 'Conv'#234'nio Lider:'
                end
                object Label27: TLabel
                  Left = 780
                  Top = 16
                  Width = 53
                  Height = 13
                  Caption = 'h Gera'#231#227'o:'
                end
                object Label29: TLabel
                  Left = 8
                  Top = 56
                  Width = 142
                  Height = 13
                  Caption = 'Tipo e n'#250'mero de documento:'
                end
                object dmkEd_0_000: TdmkEdit
                  Left = 8
                  Top = 32
                  Width = 37
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 4
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0001'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 1
                  ValWarn = False
                end
                object dmkEd_0_005: TdmkEdit
                  Left = 276
                  Top = 32
                  Width = 21
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 3
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '1'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 1
                  ValWarn = False
                end
                object dmkEd_0_006: TdmkEdit
                  Left = 300
                  Top = 32
                  Width = 100
                  Height = 21
                  TabOrder = 4
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = 'REMESSA'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 'REMESSA'
                  ValWarn = False
                end
                object dmkEd_0_003: TdmkEdit
                  Left = 48
                  Top = 32
                  Width = 21
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 2
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '01'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 1
                  ValWarn = False
                end
                object dmkEd_0_004: TdmkEdit
                  Left = 72
                  Top = 32
                  Width = 200
                  Height = 21
                  TabOrder = 2
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = 'COBRANCA'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 'COBRANCA'
                  ValWarn = False
                end
                object dmkEd_0_402: TdmkEdit
                  Left = 300
                  Top = 72
                  Width = 693
                  Height = 21
                  TabOrder = 11
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object dmkEd_0_990: TdmkEdit
                  Left = 840
                  Top = 32
                  Width = 68
                  Height = 21
                  TabOrder = 7
                  FormatType = dmktfDate
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0d
                  ValWarn = False
                end
                object dmkEd_0_410: TdmkEdit
                  Left = 404
                  Top = 32
                  Width = 373
                  Height = 21
                  TabOrder = 5
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object dmkEd_0_641: TdmkEdit
                  Left = 912
                  Top = 32
                  Width = 80
                  Height = 21
                  TabOrder = 8
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object dmkEd_0_991: TdmkEdit
                  Left = 780
                  Top = 32
                  Width = 56
                  Height = 21
                  TabOrder = 6
                  FormatType = dmktfTime
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfLong
                  Texto = '00:00:00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object dmkEd_0_400: TdmkEdit
                  Left = 8
                  Top = 72
                  Width = 20
                  Height = 21
                  TabOrder = 9
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object dmkEd_0_401: TdmkEdit
                  Left = 32
                  Top = 72
                  Width = 264
                  Height = 21
                  TabOrder = 10
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
              end
              object GroupBox2: TGroupBox
                Left = 0
                Top = 105
                Width = 992
                Height = 60
                Align = alTop
                Caption = ' Dados banc'#225'rios: '
                TabOrder = 1
                object Label4: TLabel
                  Left = 8
                  Top = 16
                  Width = 42
                  Height = 13
                  Caption = 'Ag'#234'ncia:'
                end
                object Label5: TLabel
                  Left = 80
                  Top = 16
                  Width = 73
                  Height = 13
                  Caption = 'Conta corrente:'
                end
                object Label6: TLabel
                  Left = 188
                  Top = 16
                  Width = 25
                  Height = 13
                  Caption = 'DAC:'
                end
                object Label8: TLabel
                  Left = 228
                  Top = 16
                  Width = 95
                  Height = 13
                  Caption = 'Institui'#231#227'o banc'#225'ria:'
                end
                object Label21: TLabel
                  Left = 856
                  Top = 16
                  Width = 116
                  Height = 13
                  Caption = 'C'#243'digo oculto do banco:'
                end
                object dmkEd_0_020: TdmkEdit
                  Left = 8
                  Top = 32
                  Width = 37
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 4
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0000'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                end
                object dmkEd_0_021: TdmkEdit
                  Left = 80
                  Top = 32
                  Width = 73
                  Height = 21
                  TabOrder = 2
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object dmkEd_0_022: TdmkEdit
                  Left = 48
                  Top = 32
                  Width = 17
                  Height = 21
                  CharCase = ecUpperCase
                  MaxLength = 1
                  TabOrder = 1
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = True
                  ValueVariant = ''
                  ValWarn = False
                end
                object dmkEd_0_023: TdmkEdit
                  Left = 156
                  Top = 32
                  Width = 17
                  Height = 21
                  CharCase = ecUpperCase
                  MaxLength = 1
                  TabOrder = 3
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = True
                  ValueVariant = ''
                  ValWarn = False
                end
                object dmkEd_0_024: TdmkEdit
                  Left = 188
                  Top = 32
                  Width = 25
                  Height = 21
                  CharCase = ecUpperCase
                  MaxLength = 1
                  TabOrder = 4
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = True
                  ValueVariant = ''
                  ValWarn = False
                end
                object dmkEdNomeBanco: TdmkEdit
                  Left = 260
                  Top = 32
                  Width = 593
                  Height = 21
                  TabOrder = 6
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object dmkEd_0_001: TdmkEdit
                  Left = 228
                  Top = 32
                  Width = 29
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 5
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 3
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '000'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  OnChange = dmkEd_0_001Change
                end
                object dmkEd_0_699: TdmkEdit
                  Left = 856
                  Top = 32
                  Width = 137
                  Height = 21
                  TabOrder = 7
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
              end
            end
          end
          object TabSheet14: TTabSheet
            Caption = ' [1] Detalhe '
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel9: TPanel
              Left = 0
              Top = 0
              Width = 992
              Height = 356
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Grade_1: TStringGrid
                Left = 0
                Top = 0
                Width = 992
                Height = 326
                Align = alClient
                ColCount = 2
                DefaultColWidth = 32
                DefaultRowHeight = 18
                RowCount = 2
                Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
                TabOrder = 0
                OnSelectCell = Grade_1SelectCell
              end
              object Panel5: TPanel
                Left = 0
                Top = 326
                Width = 992
                Height = 30
                Align = alBottom
                ParentBackground = False
                TabOrder = 1
                object Label10: TLabel
                  Left = 8
                  Top = 8
                  Width = 183
                  Height = 13
                  Caption = 'C'#243'digo de item da coluna selecionada:'
                end
                object LaCodDmk_1: TLabel
                  Left = 196
                  Top = 8
                  Width = 22
                  Height = 13
                  Caption = '000'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
              end
            end
          end
          object TabSheet3: TTabSheet
            Caption = ' [2] Mensagem '
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel14: TPanel
              Left = 0
              Top = 0
              Width = 992
              Height = 356
              Align = alClient
              BevelOuter = bvNone
              Caption = 'Sem implementa'#231#227'o'
              ParentBackground = False
              TabOrder = 0
            end
          end
          object TabSheet15: TTabSheet
            Caption = '[3] Rateio de Cr'#233'dito '
            ImageIndex = 2
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel15: TPanel
              Left = 0
              Top = 0
              Width = 992
              Height = 356
              Align = alClient
              BevelOuter = bvNone
              Caption = 'Sem implementa'#231#227'o'
              ParentBackground = False
              TabOrder = 0
            end
          end
          object TabSheet5: TTabSheet
            Caption = ' [5] Detalhe '
            ImageIndex = 3
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Grade_5: TStringGrid
              Left = 0
              Top = 0
              Width = 992
              Height = 356
              Align = alClient
              ColCount = 2
              DefaultColWidth = 32
              DefaultRowHeight = 18
              RowCount = 2
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
              TabOrder = 0
            end
          end
          object TabSheet6: TTabSheet
            Caption = ' [7] Detalhe '
            ImageIndex = 4
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Grade_7: TStringGrid
              Left = 0
              Top = 0
              Width = 992
              Height = 356
              Align = alClient
              ColCount = 2
              DefaultColWidth = 32
              DefaultRowHeight = 18
              RowCount = 2
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
              TabOrder = 0
              OnSelectCell = Grade_1SelectCell
            end
          end
          object TabSheet8: TTabSheet
            Caption = ' [9] Trailer '
            ImageIndex = 5
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel16: TPanel
              Left = 0
              Top = 0
              Width = 992
              Height = 356
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
            end
          end
        end
      end
      object TabSheet12: TTabSheet
        Caption = 'Listar campos'
        ImageIndex = 8
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel20: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 48
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object BtListar: TBitBtn
            Tag = 14
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Caption = '&Lista'
            NumGlyphs = 2
            TabOrder = 0
            Visible = False
            OnClick = BtListarClick
          end
        end
        object Grade_L: TStringGrid
          Left = 0
          Top = 48
          Width = 1000
          Height = 336
          Align = alClient
          ColCount = 12
          DefaultColWidth = 32
          DefaultRowHeight = 18
          FixedCols = 4
          RowCount = 2
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
          TabOrder = 1
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 224
        Height = 32
        Caption = 'Remessa CNAB B'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 224
        Height = 32
        Caption = 'Remessa CNAB B'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 224
        Height = 32
        Caption = 'Remessa CNAB B'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 552
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel21: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Gera'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 508
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel22: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object OpenDialog1: TOpenDialog
    Left = 9
    Top = 11
  end
  object QrArqui: TmySQLQuery
    Database = Dmod.MyDB
    Left = 37
    Top = 11
    object QrArquiNUM_REM: TIntegerField
      FieldName = 'NUM_REM'
    end
    object QrArquiHoraG: TTimeField
      FieldName = 'HoraG'
      Required = True
    end
    object QrArquiDataG: TDateField
      FieldName = 'DataG'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrArquiMensagem1: TWideStringField
      FieldName = 'Mensagem1'
      Size = 40
    end
    object QrArquiMensagem2: TWideStringField
      FieldName = 'Mensagem2'
      Size = 40
    end
  end
  object QrLots: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrLotsAfterScroll
    SQL.Strings = (
      'SELECT con.Nome NOMECONFIG, cob.Codigo NUM_LOT,'
      'cob.*, con.*'
      'FROM cnab_lot cob'
      'LEFT JOIN cnab_cfg con ON con.Codigo=cob.CNAB_Cfg')
    Left = 65
    Top = 11
    object QrLotsCedBanco: TIntegerField
      FieldName = 'CedBanco'
      Origin = 'cnab_cfg.CedBanco'
    end
    object QrLotsCedAgencia: TIntegerField
      FieldName = 'CedAgencia'
      Origin = 'cnab_cfg.CedAgencia'
    end
    object QrLotsCedConta: TWideStringField
      FieldName = 'CedConta'
      Origin = 'cnab_cfg.CedConta'
      Size = 30
    end
    object QrLotsCedDAC_A: TWideStringField
      FieldName = 'CedDAC_A'
      Origin = 'cnab_cfg.CedDAC_A'
      Size = 1
    end
    object QrLotsCedDAC_C: TWideStringField
      FieldName = 'CedDAC_C'
      Origin = 'cnab_cfg.CedDAC_C'
      Size = 1
    end
    object QrLotsCedDAC_AC: TWideStringField
      FieldName = 'CedDAC_AC'
      Origin = 'cnab_cfg.CedDAC_AC'
      Size = 1
    end
    object QrLotsComando: TIntegerField
      FieldName = 'Comando'
      Origin = 'cnab_cfg.Comando'
    end
    object QrLotsQuemDistrb: TWideStringField
      FieldName = 'QuemDistrb'
      Origin = 'cnab_cfg.QuemDistrb'
      Size = 1
    end
    object QrLotsTipoCart: TSmallintField
      FieldName = 'TipoCart'
      Origin = 'cnab_cfg.TipoCart'
    end
    object QrLotsEspecieTit: TWideStringField
      FieldName = 'EspecieTit'
      Origin = 'cnab_cfg.EspecieTit'
      Size = 6
    end
    object QrLotsQuemPrint: TWideStringField
      FieldName = 'QuemPrint'
      Origin = 'cnab_cfg.QuemPrint'
      Size = 1
    end
    object QrLotsAceiteTit: TSmallintField
      FieldName = 'AceiteTit'
      Origin = 'cnab_cfg.AceiteTit'
    end
    object QrLotsJurosTipo: TSmallintField
      FieldName = 'JurosTipo'
      Origin = 'cnab_cfg.JurosTipo'
    end
    object QrLotsJurosDias: TSmallintField
      FieldName = 'JurosDias'
      Origin = 'cnab_cfg.JurosDias'
    end
    object QrLotsJurosPerc: TFloatField
      FieldName = 'JurosPerc'
      Origin = 'cnab_cfg.JurosPerc'
    end
    object QrLotsMultaPerc: TFloatField
      FieldName = 'MultaPerc'
      Origin = 'cnab_cfg.MultaPerc'
    end
    object QrLotsMoedaCod: TWideStringField
      FieldName = 'MoedaCod'
      Origin = 'cnab_cfg.MoedaCod'
    end
    object QrLotsDesco1Cod: TSmallintField
      FieldName = 'Desco1Cod'
      Origin = 'cnab_cfg.Desco1Cod'
    end
    object QrLotsDesco1Fat: TFloatField
      FieldName = 'Desco1Fat'
      Origin = 'cnab_cfg.Desco1Fat'
    end
    object QrLotsDesco1Dds: TIntegerField
      FieldName = 'Desco1Dds'
      Origin = 'cnab_cfg.Desco1Dds'
    end
    object QrLotsDesco2Cod: TSmallintField
      FieldName = 'Desco2Cod'
      Origin = 'cnab_cfg.Desco2Cod'
    end
    object QrLotsDesco2Fat: TFloatField
      FieldName = 'Desco2Fat'
      Origin = 'cnab_cfg.Desco2Fat'
    end
    object QrLotsDesco2Dds: TIntegerField
      FieldName = 'Desco2Dds'
      Origin = 'cnab_cfg.Desco2Dds'
    end
    object QrLotsDesco3Cod: TSmallintField
      FieldName = 'Desco3Cod'
      Origin = 'cnab_cfg.Desco3Cod'
    end
    object QrLotsDesco3Fat: TFloatField
      FieldName = 'Desco3Fat'
      Origin = 'cnab_cfg.Desco3Fat'
    end
    object QrLotsDesco3Dds: TIntegerField
      FieldName = 'Desco3Dds'
      Origin = 'cnab_cfg.Desco3Dds'
    end
    object QrLotsProtesCod: TSmallintField
      FieldName = 'ProtesCod'
      Origin = 'cnab_cfg.ProtesCod'
    end
    object QrLotsProtesDds: TSmallintField
      FieldName = 'ProtesDds'
      Origin = 'cnab_cfg.ProtesDds'
    end
    object QrLotsBxaDevCod: TSmallintField
      FieldName = 'BxaDevCod'
      Origin = 'cnab_cfg.BxaDevCod'
    end
    object QrLotsBxaDevDds: TIntegerField
      FieldName = 'BxaDevDds'
      Origin = 'cnab_cfg.BxaDevDds'
    end
    object QrLotsContrato: TFloatField
      FieldName = 'Contrato'
      Origin = 'cnab_cfg.Contrato'
    end
    object QrLotsMultaTipo: TSmallintField
      FieldName = 'MultaTipo'
      Origin = 'cnab_cfg.MultaTipo'
    end
    object QrLotsMultaDias: TSmallintField
      FieldName = 'MultaDias'
      Origin = 'cnab_cfg.MultaDias'
    end
    object QrLotsConvCartCobr: TWideStringField
      FieldName = 'ConvCartCobr'
      Origin = 'cnab_cfg.ConvCartCobr'
      Size = 2
    end
    object QrLotsConvVariCart: TWideStringField
      FieldName = 'ConvVariCart'
      Origin = 'cnab_cfg.ConvVariCart'
      Size = 3
    end
    object QrLotsNUM_LOT: TIntegerField
      FieldName = 'NUM_LOT'
      Origin = 'cnab_lot.Codigo'
    end
    object QrLotsCedPosto: TIntegerField
      FieldName = 'CedPosto'
      Origin = 'cnab_cfg.CedPosto'
    end
    object QrLotsCartNum: TWideStringField
      FieldName = 'CartNum'
      Origin = 'cnab_cfg.CartNum'
      Size = 3
    end
    object QrLotsCodEmprBco: TWideStringField
      FieldName = 'CodEmprBco'
      Origin = 'cnab_cfg.CodEmprBco'
    end
    object QrLotsVariacao: TIntegerField
      FieldName = 'Variacao'
    end
    object QrLotsCartCod: TWideStringField
      FieldName = 'CartCod'
      Size = 3
    end
    object QrLots_2401_208: TWideStringField
      FieldName = '_2401_208'
      Size = 33
    end
  end
  object QrItens: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrItensCalcFields
    SQL.Strings = (
      'SELECT loi.Duplicata, loi.Data, loi.Vencimento,'
      'loi.Bruto, loi.Desco Abatimento,'
      'loi.FatParcela + 0.000000000000000000 SeuNumero,'
      'loi.FatParcela + 0.000000000000000000 Sequencial,'
      'ent.Tipo AVALISTA_TIPO,'
      
        'CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE ent.Nome END AVAL' +
        'ISTA_NOME,'
      
        'CASE WHEN ent.Tipo=0 THEN ent.CNPJ    ELSE ent.CPF     END AVALI' +
        'STA_CNPJ,'
      
        'CASE WHEN ent.Tipo=0 THEN ent.ERua    ELSE ent.PRua    END AVALI' +
        'STA_RUA,'
      
        'CASE WHEN ent.Tipo=0 THEN ent.ENumero+0.000 ELSE ent.PNumero+0.0' +
        '00 END AVALISTA_NUMERO,'
      
        'CASE WHEN ent.Tipo=0 THEN ent.ECompl  ELSE ent.PCompl  END AVALI' +
        'STA_COMPL,'
      
        'CASE WHEN ent.Tipo=0 THEN ent.EBairro ELSE ent.PBairro END AVALI' +
        'STA_BAIRRO,'
      
        'CASE WHEN ent.Tipo=0 THEN ent.ECEP    ELSE ent.PCEP    END AVALI' +
        'STA_CEP,'
      
        'CASE WHEN ent.Tipo=0 THEN ent.ECidade ELSE ent.PCidade END AVALI' +
        'STA_CIDADE,'
      
        'CASE WHEN ent.Tipo=0 THEN ufe.Nome    ELSE ufp.Nome    END AVALI' +
        'STA_xUF,'
      'sac.CNPJ SACADO_CNPJ, sac.Nome SACADO_NOME, sac.Rua SACADO_RUA,'
      'sac.Numero SACADO_NUMERO, sac.Compl SACADO_COMPL,'
      'sac.Bairro SACADO_BAIRRO,'
      'sac.Cidade SACADO_CIDADE, sac.UF SACADO_xUF,'
      'sac.CEP SACADO_CEP, IF(LENGTH(sac.CNPJ) >=14, 0, 1) SACADO_TIPO'
      'FROM lct0001a loi'
      'LEFT JOIN lot0001a     lot ON lot.Codigo=loi.FatNum'
      'LEFT JOIN entidades ent ON ent.Codigo=lot.Cliente'
      'LEFT JOIN sacados   sac ON sac.CNPJ=loi.CNPJCPF'
      'LEFT JOIN ufs       ufe ON ent.EUF=ufe.Codigo'
      'LEFT JOIN ufs       ufp ON ent.PUF=ufp.Codigo'
      'WHERE loi.FatID=301'
      'AND loi.CNAB_Lot=1'
      'ORDER BY Duplicata'
      '')
    Left = 93
    Top = 11
    object QrItensDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 12
    end
    object QrItensBruto: TFloatField
      FieldName = 'Bruto'
    end
    object QrItensAbatimento: TFloatField
      FieldName = 'Abatimento'
    end
    object QrItensSeuNumero: TFloatField
      FieldName = 'SeuNumero'
      Required = True
    end
    object QrItensSequencial: TFloatField
      FieldName = 'Sequencial'
      Required = True
    end
    object QrItensAVALISTA_TIPO: TSmallintField
      FieldName = 'AVALISTA_TIPO'
    end
    object QrItensAVALISTA_NOME: TWideStringField
      FieldName = 'AVALISTA_NOME'
      Size = 100
    end
    object QrItensAVALISTA_CNPJ: TWideStringField
      FieldName = 'AVALISTA_CNPJ'
      Size = 18
    end
    object QrItensAVALISTA_RUA: TWideStringField
      FieldName = 'AVALISTA_RUA'
      Size = 30
    end
    object QrItensAVALISTA_NUMERO: TFloatField
      FieldName = 'AVALISTA_NUMERO'
    end
    object QrItensAVALISTA_COMPL: TWideStringField
      FieldName = 'AVALISTA_COMPL'
      Size = 30
    end
    object QrItensAVALISTA_BAIRRO: TWideStringField
      FieldName = 'AVALISTA_BAIRRO'
      Size = 30
    end
    object QrItensAVALISTA_CEP: TLargeintField
      FieldName = 'AVALISTA_CEP'
    end
    object QrItensAVALISTA_CIDADE: TWideStringField
      FieldName = 'AVALISTA_CIDADE'
      Size = 25
    end
    object QrItensAVALISTA_xUF: TWideStringField
      FieldName = 'AVALISTA_xUF'
      Size = 2
    end
    object QrItensSACADO_CNPJ: TWideStringField
      FieldName = 'SACADO_CNPJ'
      Required = True
      Size = 15
    end
    object QrItensSACADO_NOME: TWideStringField
      FieldName = 'SACADO_NOME'
      Size = 50
    end
    object QrItensSACADO_RUA: TWideStringField
      FieldName = 'SACADO_RUA'
      Size = 30
    end
    object QrItensSACADO_NUMERO: TLargeintField
      FieldName = 'SACADO_NUMERO'
    end
    object QrItensSACADO_COMPL: TWideStringField
      FieldName = 'SACADO_COMPL'
      Size = 30
    end
    object QrItensSACADO_BAIRRO: TWideStringField
      FieldName = 'SACADO_BAIRRO'
      Size = 30
    end
    object QrItensSACADO_CIDADE: TWideStringField
      FieldName = 'SACADO_CIDADE'
      Size = 25
    end
    object QrItensSACADO_xUF: TWideStringField
      FieldName = 'SACADO_xUF'
      Size = 2
    end
    object QrItensSACADO_CEP: TIntegerField
      FieldName = 'SACADO_CEP'
    end
    object QrItensSACADO_TIPO: TLargeintField
      FieldName = 'SACADO_TIPO'
      Required = True
    end
    object QrItensENDERECO_SACADO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ENDERECO_SACADO'
      Size = 255
      Calculated = True
    end
    object QrItensENDERECO_AVALISTA: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ENDERECO_AVALISTA'
      Size = 255
      Calculated = True
    end
    object QrItensData: TDateField
      FieldName = 'Data'
    end
    object QrItensVencimento: TDateField
      FieldName = 'Vencimento'
    end
  end
end
