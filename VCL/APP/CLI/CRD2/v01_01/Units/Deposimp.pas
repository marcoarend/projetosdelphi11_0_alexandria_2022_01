unit Deposimp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DBCtrls, Db, mySQLDbTables, ComCtrls,
  Grids, DBGrids, Mask, Menus, frxClass, frxDBSet, Variants, dmkGeral, dmkEdit,
  dmkEditCB, dmkDBLookupComboBox, DmkDAC_PF, dmkImage, UnDmkEnums;

type
  TFmDeposimp = class(TForm)
    QrClientes: TmySQLQuery;
    DsClientes: TDataSource;
    QrLotIts: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrLotItsPERIODO: TFloatField;
    QrLotItsBanco: TIntegerField;
    QrLotItsEmitente: TWideStringField;
    QrLotItsDDeposito: TDateField;
    QrClientesNOMECLIENTE: TWideStringField;
    QrLotItsDDeposito_TXT: TWideStringField;
    QrLotItsMES_TXT: TWideStringField;
    QrLotItsCPF_TXT: TWideStringField;
    QrLotItsValor_TXT: TWideStringField;
    QrLotItsCONTAGEM: TIntegerField;
    QrLotItsNOMECLIENTE: TWideStringField;
    QrLotItsNOMESTATUS: TWideStringField;
    Timer1: TTimer;
    QrLotItsNF: TIntegerField;
    QrColigados: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsColigados: TDataSource;
    QrLotItsNOMECOLIGADO: TWideStringField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    RGAgrupa: TRadioGroup;
    TabSheet2: TTabSheet;
    Panel1: TPanel;
    BtLimpar: TBitBtn;
    Panel2: TPanel;
    RGOrdem1: TRadioGroup;
    RGOrdem2: TRadioGroup;
    RGOrdem3: TRadioGroup;
    RGOrdem4: TRadioGroup;
    PainelDados: TPanel;
    Label3: TLabel;
    Label34: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    LaColigado: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    TPIni: TDateTimePicker;
    TPFim: TDateTimePicker;
    EdEmitente: TdmkEdit;
    EdCPF: TdmkEdit;
    RGMascara: TRadioGroup;
    GroupBox1: TGroupBox;
    CkP0: TCheckBox;
    EdColigado: TdmkEditCB;
    CBColigado: TdmkDBLookupComboBox;
    Panel3: TPanel;
    Label36: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    Label32: TLabel;
    Label10: TLabel;
    EdBanda: TdmkEdit;
    EdBanco: TdmkEdit;
    EdAgencia: TdmkEdit;
    EdConta: TdmkEdit;
    EdCheque: TdmkEdit;
    DBEdItens: TDBEdit;
    DBEdValor: TDBEdit;
    DBG1: TDBGrid;
    RGOQue: TRadioGroup;
    DsSumD: TDataSource;
    EdValor: TdmkEdit;
    Label11: TLabel;
    QrSumT: TmySQLQuery;
    QrSumTValor: TFloatField;
    QrSumD: TmySQLQuery;
    QrSumDValor: TFloatField;
    QrSumDQTDE: TLargeintField;
    QrSumTQTDE: TLargeintField;
    DsLotIts: TDataSource;
    QrLotItsDepositado: TSmallintField;
    DsSumT: TDataSource;
    Label9: TLabel;
    DBEdit2: TDBEdit;
    DBEdit1: TDBEdit;
    CkP1: TCheckBox;
    CkP2: TCheckBox;
    QrLotItsValDeposito: TFloatField;
    QrLotItsCONF_V: TIntegerField;
    Timer2: TTimer;
    QrLotItsCONF_I: TIntegerField;
    QrLotItsNF_TXT: TWideStringField;
    CkP3: TCheckBox;
    QrLotItsReforcoCxa: TSmallintField;
    RGRepasse: TRadioGroup;
    RGReforco: TRadioGroup;
    BtConfigura: TBitBtn;
    PMDeposito: TPopupMenu;
    Selecionados1: TMenuItem;
    Tudo1: TMenuItem;
    QrLotItsNOMECART: TWideStringField;
    Timer3: TTimer;
    BtDeposito: TBitBtn;
    Label7: TLabel;
    EdSoma: TdmkEdit;
    RGTipo: TRadioGroup;
    Query1: TmySQLQuery;
    Query1Controle: TIntegerField;
    RGAlinIts: TRadioGroup;
    frxDsLotIts: TfrxDBDataset;
    frxDeposIts: TfrxReport;
    QrLotItsCliente: TIntegerField;
    Label12: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BitBtn1: TBitBtn;
    Panel5: TPanel;
    BtPesquisa: TBitBtn;
    BtImprime: TBitBtn;
    QrLotItsContaCorrente: TWideStringField;
    QrLotItsDocumento: TFloatField;
    QrLotItsFatNum: TFloatField;
    QrLotItsFatParcela: TIntegerField;
    QrLotItsCNPJCPF: TWideStringField;
    QrLotItsVencimento: TDateField;
    QrLotItsCredito: TFloatField;
    QrLotItsCREDITO_TXT: TWideStringField;
    QrLotItsAgencia: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure QrLotItsCalcFields(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Timer1Timer(Sender: TObject);
    procedure EdColigadoChange(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure RGOrdem1Click(Sender: TObject);
    procedure TPIniChange(Sender: TObject);
    procedure TPFimChange(Sender: TObject);
    procedure EdEmitenteChange(Sender: TObject);
    procedure RGMascaraClick(Sender: TObject);
    procedure EdCPFChange(Sender: TObject);
    procedure CkQuitadoClick(Sender: TObject);
    procedure EdBandaChange(Sender: TObject);
    procedure DBEdItensChange(Sender: TObject);
    procedure DBEdValorChange(Sender: TObject);
    procedure BtLimparClick(Sender: TObject);
    procedure DBG1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBG1CellClick(Column: TColumn);
    procedure QrLotItsAfterOpen(DataSet: TDataSet);
    procedure CkP0Click(Sender: TObject);
    procedure CkP1Click(Sender: TObject);
    procedure CkP2Click(Sender: TObject);
    procedure CkR0Click(Sender: TObject);
    procedure QrLotItsAfterClose(DataSet: TDataSet);
    procedure EdValorExit(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
    procedure RGRepasseClick(Sender: TObject);
    procedure RGReforcoClick(Sender: TObject);
    procedure BtConfiguraClick(Sender: TObject);
    procedure Selecionados1Click(Sender: TObject);
    procedure Tudo1Click(Sender: TObject);
    procedure DBG1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Timer3Timer(Sender: TObject);
    procedure BtDepositoClick(Sender: TObject);
    procedure frxDeposIts_GetValue(const VarName: String;
      var Value: Variant);
  private
    { Private declarations }
    FEmitente, FCPF: String;
    FLOIS: Integer;
    FAscendente: Boolean;
    procedure ConfiguracaoInicial;
    function LocalizaItem: Boolean;
    procedure ConfereCheque(Status: Integer);
    procedure ReopenLotIts();
    procedure InsereValDeposito(Valor, Real: Double; FatParcela: Integer;
              Avisa: Boolean);
    procedure VoltaABanda;
    procedure ReforcoCxa(Status: Integer);
    procedure ContaDeposito(Tipo: Integer);
    procedure DepositaSelecionados;
    procedure DepositaAtual;
    procedure SomaLinhas(Ascendente: Boolean);
    procedure PreparaSoma(Ascendente: Boolean);
  public
    { Public declarations }
    FImprime: Boolean;
  end;

  var
  FmDeposimp: TFmDeposimp;

implementation

{$R *.DFM}

uses UnMyObjects, Module, UnInternalConsts, Principal, MyVCLSkin, UMySQLModule, CartDep,
  MyListas, TedC_Aux;

procedure TFmDeposimp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmDeposimp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  if FImprime then Timer1.Enabled := True;
end;

procedure TFmDeposimp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmDeposimp.EdClienteChange(Sender: TObject);
begin
  BtImprime.Enabled := False;
end;

procedure TFmDeposimp.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stNil;
  //
  ConfiguracaoInicial;
  //
  UMyMod.AbreQuery(QrClientes, Dmod.MyDB);
  UMyMod.AbreQuery(QrColigados, Dmod.MyDB);
  TPIni.Date := Date;
  TPFim.Date := Date + 365;
  //
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmDeposimp.ConfiguracaoInicial;
var
  Cam: String;
begin
  Cam := Application.Title + '\XYConfig\DeposImp';
  RGOrdem1.ItemIndex := Geral.ReadAppKey('Ordem1', Cam, ktInteger, 0, HKEY_LOCAL_MACHINE);
  RGOrdem2.ItemIndex := Geral.ReadAppKey('Ordem2', Cam, ktInteger, 2, HKEY_LOCAL_MACHINE);
  RGOrdem3.ItemIndex := Geral.ReadAppKey('Ordem3', Cam, ktInteger, 1, HKEY_LOCAL_MACHINE);
  RGOrdem4.ItemIndex := Geral.ReadAppKey('Ordem4', Cam, ktInteger, 3, HKEY_LOCAL_MACHINE);
  RGAgrupa.ItemIndex := Geral.ReadAppKey('Agrupa', Cam, ktInteger, 0, HKEY_LOCAL_MACHINE);
  //CkQuitado.Checked := Geral.ReadAppKey('Sit000', Cam, ktBoolean, True , HKEY_LOCAL_MACHINE);
  CkP0.Checked := Geral.ReadAppKey('P0', Cam, ktBoolean, False, HKEY_LOCAL_MACHINE);
  CkP1.Checked := Geral.ReadAppKey('P1', Cam, ktBoolean, False, HKEY_LOCAL_MACHINE);
  CkP2.Checked := Geral.ReadAppKey('P2', Cam, ktBoolean, False, HKEY_LOCAL_MACHINE);
  RGRepasse.ItemIndex := Geral.ReadAppKey('Repasse', Cam, ktInteger, 0, HKEY_LOCAL_MACHINE);
  RGReforco.ItemIndex := Geral.ReadAppKey('Reforco', Cam, ktInteger, 0, HKEY_LOCAL_MACHINE);
end;

procedure TFmDeposimp.BtPesquisaClick(Sender: TObject);
begin
  ReopenLotIts();
end;

procedure TFmDeposimp.ReopenLotIts();
const
  ItensOrdem: array[0..4] of String = ('NOMECLIENTE', 'DDeposito', 'PERIODO',
  'Emitente', 'CNPJCPF');
var
  Cliente, Coligado: Integer;
  P0, P1, P2, P3, R0, R1, D0, D1: Integer;
  PT0, PT1, PT2, RT0, RT1, DT0, DT1, DataI, DataF: String;
begin
  Screen.Cursor := crHourGlass;
  if Trim(EdEmitente.Text) <> '' then
  begin
    FEmitente := EdEmitente.Text;
    if RGMascara.ItemIndex in ([0,1]) then FEmitente := '%'+FEmitente;
    if RGMascara.ItemIndex in ([0,2]) then FEmitente := FEmitente+'%';
  end else FEmitente := '';
  if Trim(EdCPF.Text) <> '' then FCPF := Geral.SoNumero_TT(EdCPF.Text)
  else FCPF := '';
  P0 := MLAGeral.BoolToIntDef(CkP0.Checked, 0, -1000);
  P1 := MLAGeral.BoolToIntDef(CkP1.Checked, 1, -1000);
  P2 := MLAGeral.BoolToIntDef(CkP2.Checked, 2, -1000);
  P3 := MLAGeral.BoolToIntDef(CkP3.Checked, 4, -1000);
  if RGRepasse.ItemIndex in ([0,2]) then R0 := 0 else R0 := -1000;
  //R0 := MLAGeral.BoolToIntDef(CkR0.Checked, 0, -1000);
  if RGRepasse.ItemIndex in ([1,2]) then R1 := 1 else R1 := -1000;
  //R1 := MLAGeral.BoolToIntDef(CkR1.Checked, 1, -1000);
  if RGReforco.ItemIndex in ([0,2]) then D0 := 0 else D0 := -1000;
  if RGReforco.ItemIndex in ([1,2]) then D1 := 1 else D1 := -1000;
  //
  PT0 := IntToStr(P0);
  PT1 := IntToStr(P1);
  PT2 := IntToStr(P2);
  //PT3 := IntToStr(P3);
  RT0 := IntToStr(R0);
  RT1 := IntToStr(R1);
  DT0 := IntToStr(D0);
  DT1 := IntToStr(D1);
  if (P0+P1+P2+P3=-4000) then
  begin
    Application.MessageBox('Informe pelo menos um status de pagamento!', 'Erro',
      MB_OK+MB_ICONERROR);
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (R0+R1=-2000) then
  begin
    Application.MessageBox('Informe pelo menos um status de repasse!', 'Erro',
      MB_OK+MB_ICONERROR);
    Screen.Cursor := crDefault;
    Exit;
  end;
  Cliente := EdCliente.ValueVariant;
  Coligado := EdColigado.ValueVariant;
  //
{
  QrLotIts.Close;
  QrLotIts.SQL.Clear;
  QrLotIts.SQL.Add('SELECT MONTH(li.DDeposito)+YEAR(li.DDeposito)*100+0.00 PERIODO,');
  QrLotIts.SQL.Add('CASE WHEN cl.Tipo=0 THEN cl.RazaoSocial ELSE cl.Nome END NOMECLIENTE,');
  QrLotIts.SQL.Add('CASE WHEN co.Tipo=0 THEN co.RazaoSocial ELSE co.Nome END NOMECOLIGADO,');
  QrLotIts.SQL.Add('li.Banco, li.Agencia, li.Conta, li.Cheque, li.Codigo, ');
  QrLotIts.SQL.Add('li.Controle, li.Depositado, li.Emitente, li.CPF, ');
  QrLotIts.SQL.Add('li.DDeposito, li.Vencto, li.Valor, li.ValDeposito, ');
  QrLotIts.SQL.Add('li.ReforcoCxa, lo.NF, ca.Nome NOMECART, lo.Cliente');
  QrLotIts.SQL.Add('FROM lot esits li');
  QrLotIts.SQL.Add('LEFT JOIN lot es     lo ON lo.Codigo=li.Codigo');
  QrLotIts.SQL.Add('LEFT JOIN repasits  ri ON ri.Origem=li.Controle');
  QrLotIts.SQL.Add('LEFT JOIN repas     re ON re.Codigo=ri.Codigo');
  QrLotIts.SQL.Add('LEFT JOIN entidades cl ON cl.Codigo=lo.Cliente');
  QrLotIts.SQL.Add('LEFT JOIN entidades co ON co.Codigo=re.Coligado');
  QrLotIts.SQL.Add('LEFT JOIN carteiras ca ON ca.Codigo=li.CartDep');
  if RGAlinIts.ItemIndex in ([0,1]) then
    QrLotIts.SQL.Add('LEFT JOIN alinits   ai ON ai.ChequeOrigem=li.Controle');

  QrLotIts.SQL.Add('WHERE lo.Tipo in (0, 2)');
  QrLotIts.SQL.Add('AND lo.TxCompra+lo.ValValorem+ '+
      IntToStr(FmPrincipal.FConnections)+' >= 0.01');
  QrLotIts.SQL.Add('AND li.DDeposito BETWEEN :P0 AND :P1');
  QrLotIts.SQL.Add('AND li.Quitado in (-1,'+PT0+', '+PT1+', '+PT2+')');
  if Coligado <> 0 then
  begin
    QrLotIts.SQL.Add('AND ((li.Repassado = '+RT1+' AND re.Coligado='+
    IntToStr(Coligado)+') OR li.Repassado ='+RT0+')');
  end else
    QrLotIts.SQL.Add('AND li.Repassado in ('+RT0+', '+RT1+')');
  QrLotIts.SQL.Add('AND li.ReforcoCxa in ('+DT0+', '+DT1+')');
  case RGAlinIts.ItemIndex of
    0: QrLotIts.SQL.Add('AND ai.Status IS NULL');
    1: QrLotIts.SQL.Add('AND ai.Status > -1000');
    2: QrLotIts.SQL.Add('');// Ambos
  end;
////////////////////////////////////////////////////////////////////////////////
  QrSumT.Close;
  QrSumT.SQL.Clear;
  QrSumT.SQL.Add('SELECT SUM(li.Valor) Valor, COUNT(li.Controle) QTDE');
  QrSumT.SQL.Add('FROM lot esits li');
  QrSumT.SQL.Add('LEFT JOIN lot es     lo ON lo.Codigo=li.Codigo');
  QrSumT.SQL.Add('LEFT JOIN repasits  ri ON ri.Origem=li.Controle');
  QrSumT.SQL.Add('LEFT JOIN repas     re ON re.Codigo=ri.Codigo');
  QrSumT.SQL.Add('LEFT JOIN entidades cl ON cl.Codigo=lo.Cliente');
  QrSumT.SQL.Add('LEFT JOIN entidades co ON co.Codigo=re.Coligado');
  if RGAlinIts.ItemIndex in ([0,1]) then
    QrSumT.SQL.Add('LEFT JOIN alinits   ai ON ai.ChequeOrigem=li.Controle');
  QrSumT.SQL.Add('WHERE lo.Tipo=0');
  QrSumT.SQL.Add('AND lo.TxCompra+lo.ValValorem+ '+
      IntToStr(FmPrincipal.FConnections)+' >= 0.01');
  QrSumT.SQL.Add('AND li.DDeposito BETWEEN :P0 AND :P1');
  QrSumT.SQL.Add('AND li.Quitado in (-1,'+PT0+', '+PT1+', '+PT2+')');
  if Coligado <> 0 then
  begin
    QrSumT.SQL.Add('AND ((li.Repassado = '+RT1+' AND re.Coligado='+
    IntToStr(Coligado)+') OR li.Repassado ='+RT0+')');
  end else
    QrSumT.SQL.Add('AND li.Repassado in ('+RT0+', '+RT1+')');
  QrSumT.SQL.Add('AND li.ReforcoCxa in ('+DT0+', '+DT1+')');
  case RGAlinIts.ItemIndex of
    0: QrSumT.SQL.Add('AND ai.Status IS NULL');
    1: QrSumT.SQL.Add('AND ai.Status > -1000');
    2: QrSumT.SQL.Add('');// Ambos
  end;
////////////////////////////////////////////////////////////////////////////////
  QrSumD.Close;
  QrSumD.SQL.Clear;
  QrSumD.SQL.Add('SELECT SUM(li.ValDeposito) Valor, COUNT(li.Controle) QTDE');
  QrSumD.SQL.Add('FROM lot esits li');
  QrSumD.SQL.Add('LEFT JOIN lot es     lo ON lo.Codigo=li.Codigo');
  QrSumD.SQL.Add('LEFT JOIN repasits  ri ON ri.Origem=li.Controle');
  QrSumD.SQL.Add('LEFT JOIN repas     re ON re.Codigo=ri.Codigo');
  QrSumD.SQL.Add('LEFT JOIN entidades cl ON cl.Codigo=lo.Cliente');
  QrSumD.SQL.Add('LEFT JOIN entidades co ON co.Codigo=re.Coligado');
  if RGAlinIts.ItemIndex in ([0,1]) then
    QrSumD.SQL.Add('LEFT JOIN alinits   ai ON ai.ChequeOrigem=li.Controle');
  QrSumD.SQL.Add('WHERE lo.Tipo=0 AND Depositado=1');
  QrSumD.SQL.Add('AND lo.TxCompra+lo.ValValorem+ '+
      IntToStr(FmPrincipal.FConnections)+' >= 0.01');
  QrSumD.SQL.Add('AND li.DDeposito BETWEEN :P0 AND :P1');
  QrSumD.SQL.Add('AND li.Quitado in (-1,'+PT0+', '+PT1+', '+PT2+')');
  if Coligado <> 0 then
  begin
    QrSumD.SQL.Add('AND ((li.Repassado = '+RT1+' AND re.Coligado='+
    IntToStr(Coligado)+') OR li.Repassado ='+RT0+')');
  end else
    QrSumD.SQL.Add('AND li.Repassado in ('+RT0+', '+RT1+')');
  QrSumD.SQL.Add('AND li.ReforcoCxa in ('+DT0+', '+DT1+')');
  case RGAlinIts.ItemIndex of
    0: QrSumD.SQL.Add('AND ai.Status IS NULL');
    1: QrSumD.SQL.Add('AND ai.Status > -1000');
    2: QrSumD.SQL.Add('');// Ambos
  end;
////////////////////////////////////////////////////////////////////////////////
  if Cliente <> 0 then
  begin
    QrLotIts.SQL.Add('AND lo.Cliente='+IntToStr(Cliente));
    QrSumT.SQL.Add('AND lo.Cliente='+IntToStr(Cliente));
    QrSumD.SQL.Add('AND lo.Cliente='+IntToStr(Cliente));
  end;
  if Trim(FEmitente) <> '' then
  begin
    QrLotIts.SQL.Add('AND li.Emitente LIKE "'+FEmitente+'"');
    QrSumT.SQL.Add('AND li.Emitente LIKE "'+FEmitente+'"');
    QrSumD.SQL.Add('AND li.Emitente LIKE "'+FEmitente+'"');
  end;
  if Trim(FCPF) <> '' then
  begin
    QrLotIts.SQL.Add('AND li.CPF = "'+FCPF+'"');
    QrSumT.SQL.Add('AND li.CPF = "'+FCPF+'"');
    QrSumD.SQL.Add('AND li.CPF = "'+FCPF+'"');
  end;
  if CkP3.Checked = False then
  begin
    QrLotIts.SQL.Add('AND li.NaoDeposita=0');
    QrSumT.SQL.Add('AND li.NaoDeposita=0');
    QrSumD.SQL.Add('AND li.NaoDeposita=0');
  end;
  QrLotIts.SQL.Add('ORDER BY '+
    ItensOrdem[RGOrdem1.ItemIndex]+', '+
    ItensOrdem[RGOrdem2.ItemIndex]+', '+
    ItensOrdem[RGOrdem3.ItemIndex]+', '+
    ItensOrdem[RGOrdem4.ItemIndex]);
////////////////////////////////////////////////////////////////////////////////
  QrLotIts.Params[00].AsString  := FormatDateTime(VAR_FORMATDATE, TPIni.Date);
  QrLotIts.Params[01].AsString  := FormatDateTime(VAR_FORMATDATE, TPFim.Date);
  UMyMod.AbreQuery(QrLotIts);
////////////////////////////////////////////////////////////////////////////////
  QrSumT.Params[00].AsString  := FormatDateTime(VAR_FORMATDATE, TPIni.Date);
  QrSumT.Params[01].AsString  := FormatDateTime(VAR_FORMATDATE, TPFim.Date);
  UMyMod.AbreQuery(QrSumT);
////////////////////////////////////////////////////////////////////////////////
  QrSumD.Params[00].AsString  := FormatDateTime(VAR_FORMATDATE, TPIni.Date);
  QrSumD.Params[01].AsString  := FormatDateTime(VAR_FORMATDATE, TPFim.Date);
  UMyMod.AbreQuery(QrSumD);
////////////////////////////////////////////////////////////////////////////////
}
  DataI  := Geral.FDT(TPIni.Date, 1);
  DataF  := Geral.FDT(TPFim.Date, 1);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrlotIts, Dmod.MyDB, [
  'SELECT ca.Nome NOMECART, ',
  'MONTH(li.DDeposito) + YEAR(li.DDeposito) * 100 + 0.0 PERIODO, ',
  'IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMECLIENTE, ',
  'IF(co.Tipo=0, co.RazaoSocial, co.Nome) NOMECOLIGADO, ',
  'li.Banco, li.Agencia, li.ContaCorrente, li.Documento, ',
  'li.FatNum, li.FatParcela, li.Depositado, li.Emitente, ',
  'li.CNPJCPF, li.DDeposito, li.Vencimento, li.Credito, ',
  'li.ValDeposito, li.ReforcoCxa, lo.NF, lo.Cliente ',
  'FROM ' + CO_TabLctA + ' li ',
  'LEFT JOIN ' + CO_TabLotA + ' lo ON lo.Codigo=li.FatNum ',
  'LEFT JOIN repasits  ri ON ri.Origem=li.FatParcela ',
  'LEFT JOIN repas     re ON re.Codigo=ri.Codigo ',
  'LEFT JOIN entidades cl ON cl.Codigo=lo.Cliente ',
  'LEFT JOIN entidades co ON co.Codigo=re.Coligado ',
  'LEFT JOIN carteiras ca ON ca.Codigo=li.CartDep ',
  Geral.ATS_if(RGAlinIts.ItemIndex in ([0,1]), [
  'LEFT JOIN alinits   ai ON ai.ChequeOrigem=li.FatParcela']),
  'WHERE li.FatID=' + FormatFloat('0', VAR_FATID_0301),
  'AND lo.Tipo in (0, 2) ',
  'AND lo.TxCompra + lo.ValValorem + ' +
  FormatFloat('0', FmPrincipal.FConnections)+' >= 0.01 ' ,
  'AND li.DDeposito BETWEEN "' + DataI + '" AND "' + DataF + '" ',
  'AND li.Quitado in (-1,'+PT0+', '+PT1+', '+PT2+') ',
  Geral.ATS_if(Coligado <> 0, [
  'AND ((li.Repassado = ' + RT1 + ' AND re.Coligado=' +
  FormatFloat('0', Coligado) + ') OR li.Repassado =' + RT0 + ') ']),
  Geral.ATS_if(Coligado = 0, [
  'AND li.Repassado in (' + RT0 + ', ' + RT1 + ') ']),
  'AND li.ReforcoCxa in ('+DT0+', '+DT1+') ',
  Geral.ATS_if(RGAlinIts.ItemIndex = 0, [
  'AND ai.Status IS NULL']),
  Geral.ATS_if(RGAlinIts.ItemIndex = 1, [
  'AND ai.Status > -1000']),
  Geral.ATS_if(RGAlinIts.ItemIndex = 2, ['']),
  //////////////////////////////
  Geral.ATS_if(Cliente <> 0, [
  'AND lo.Cliente= ' + FormatFloat('0', Cliente)]),
  Geral.ATS_if(Trim(FEmitente) <> '', [
  'AND li.Emitente LIKE "' + FEmitente + '"']),
  Geral.ATS_if(Trim(FCPF) <> '', [
  'AND li.CPF = "' + FCPF + '"']),
  Geral.ATS_if(CkP3.Checked = False, [
  'AND li.NaoDeposita=0']),
  'ORDER BY '+
    ItensOrdem[RGOrdem1.ItemIndex]+', '+
    ItensOrdem[RGOrdem2.ItemIndex]+', '+
    ItensOrdem[RGOrdem3.ItemIndex]+', '+
    ItensOrdem[RGOrdem4.ItemIndex],
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumT, Dmod.MyDB, [
  'SELECT SUM(li.Credito) Valor, COUNT(li.FatParcela) QTDE',
  'FROM ' + CO_TabLctA + ' li',
  'LEFT JOIN ' + CO_TabLotA + ' lo ON lo.Codigo=li.FatNum',
  'LEFT JOIN repasits  ri ON ri.Origem=li.FatParcela',
  'LEFT JOIN repas     re ON re.Codigo=ri.Codigo',
  'LEFT JOIN entidades cl ON cl.Codigo=lo.Cliente',
  'LEFT JOIN entidades co ON co.Codigo=re.Coligado',
  Geral.ATS_if(RGAlinIts.ItemIndex in ([0,1]), [
  'LEFT JOIN alinits   ai ON ai.ChequeOrigem=li.FatParcela']),
  'WHERE li.FatID=' + FormatFloat('0', VAR_FATID_0301),
  'AND lo.Tipo=0',
  'AND lo.TxCompra + lo.ValValorem + ' +
  FormatFloat('0', FmPrincipal.FConnections) + ' >= 0.01',
  'AND li.DDeposito BETWEEN "' + DataI + '" AND "' + DataF + '"',
  'AND li.Quitado in (-1,' + PT0 + ', ' + PT1 + ', ' + PT2 + ') ',
  Geral.ATS_if(Coligado <> 0, [
  'AND ((li.Repassado = ' + RT1 + ' AND re.Coligado=' +
  FormatFloat('0', Coligado) + ') OR li.Repassado =' + RT0 + ') ']),
  Geral.ATS_if(Coligado = 0, [
  'AND li.Repassado in (' + RT0 + ', ' + RT1 + ') ',
  'AND li.ReforcoCxa in (' + DT0 + ', ' + DT1 + ') ']),
  Geral.ATS_if(RGAlinIts.ItemIndex = 0, [
  'AND ai.Status IS NULL']),
  Geral.ATS_if(RGAlinIts.ItemIndex = 1, [
  'AND ai.Status > -1000']),
  Geral.ATS_if(RGAlinIts.ItemIndex = 2, ['']),
  Geral.ATS_if(Cliente <> 0, [
  'AND lo.Cliente= ' + FormatFloat('0', Cliente)]),
  Geral.ATS_if(Trim(FEmitente) <> '', [
  'AND li.Emitente LIKE "' + FEmitente + '"']),
  Geral.ATS_if(Trim(FCPF) <> '', [
  'AND li.CPF = "' + FCPF + '"']),
  Geral.ATS_if(CkP3.Checked = False, [
  'AND li.NaoDeposita=0']),
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumT, Dmod.MyDB, [
  'SELECT SUM(li.ValDeposito) Valor, COUNT(li.FatParcela) QTDE',
  'FROM ' + CO_TabLctA + ' li',
  'LEFT JOIN ' + CO_TabLotA + ' lo ON lo.Codigo=li.FatNum',
  'LEFT JOIN repasits  ri ON ri.Origem=li.FatParcela',
  'LEFT JOIN repas     re ON re.Codigo=ri.Codigo',
  'LEFT JOIN entidades cl ON cl.Codigo=lo.Cliente',
  'LEFT JOIN entidades co ON co.Codigo=re.Coligado',
  Geral.ATS_if(RGAlinIts.ItemIndex in ([0,1]),  [
  'LEFT JOIN alinits   ai ON ai.ChequeOrigem=li.FatParcela']),
  'WHERE li.FatID=' + FormatFloat('0', VAR_FATID_0301),
  'AND lo.Tipo=0 AND Depositado=' + Geral.FF0(CO_CH_DEPOSITADO_MANUAL),
  'AND lo.TxCompra + lo.ValValorem + ' +
  FormatFloat('0', FmPrincipal.FConnections)+' >= 0.01',
  'AND li.DDeposito BETWEEN "' + DataI + '" AND "' + DataF + '"',
  'AND li.Quitado in (-1,' + PT0 + ', ' + PT1 + ', ' + PT2 + ') ',
  Geral.ATS_if(Coligado <> 0, [
  'AND ((li.Repassado = ' + RT1 + ' AND re.Coligado=' +
  FormatFloat('0', Coligado) + ') OR li.Repassado =' + RT0 + ')']),
  Geral.ATS_if(Coligado = 0, [
  'AND li.Repassado in (' + RT0 + ', ' + RT1 + ') ']),
  'AND li.ReforcoCxa in (' + DT0 + ', ' + DT1 + ') ',
  Geral.ATS_if(RGAlinIts.ItemIndex = 0, [
  'AND ai.Status IS NULL']),
  Geral.ATS_if(RGAlinIts.ItemIndex = 1, [
  'AND ai.Status > -1000']),
  Geral.ATS_if(RGAlinIts.ItemIndex = 2, ['']),
  Geral.ATS_if(Cliente <> 0, [
  'AND lo.Cliente= ' + FormatFloat('0', Cliente)]),
  Geral.ATS_if(Trim(FEmitente) <> '', [
  'AND li.Emitente LIKE "' + FEmitente + '"']),
  Geral.ATS_if(Trim(FCPF) <> '', [
  'AND li.CPF = "' + FCPF + '"']),
  Geral.ATS_if(CkP3.Checked = False, [
  'AND li.NaoDeposita=0']),
  '']);
  //

  BtImprime.Enabled := True;
  Screen.Cursor := crDefault;
end;

procedure TFmDeposimp.QrLotItsCalcFields(DataSet: TDataSet);
var
  Ano, Mes: Integer;
begin
  if QrLotItsValDeposito.Value = QrLotItsCredito.Value then
    QrLotItsCONF_V.Value := 1 else QrLotItsCONF_V.Value := 0;
  //if QrLotItsValDeposito.Value > 0 then
    //QrLotItsCONF_I.Value := 1 else QrLotItsCONF_I.Value := 0;
  QrLotItsCPF_TXT.Value := Geral.FormataCNPJ_TT(QrLotItsCNPJCPF.Value);
  //
  QrLotItsDDeposito_TXT.Value :=
    FormatDateTime(VAR_FORMATDATE3, QrLotItsDDeposito.Value);
  QrLotItsCredito_TXT.Value :=
    FormatFloat('#,###,##0.00', QrLotItsCredito.Value);
  //
  QrLotItsCONTAGEM.Value := 1;
  //
  Ano := Trunc(QrLotItsPERIODO.Value) div 100;
  Mes := Trunc(QrLotItsPERIODO.Value) mod 100;
  QrLotItsMES_TXT.Value := FormatDateTime(VAR_FORMATDATE7,
    EncodeDate(Ano, Mes, 1));
  if QrLotItsFatNum.Value = -1 then QrLotItsNF_TXT.Value := 'Pg ch dv'
  else QrLotItsNF_TXT.Value := FormatFloat('000000', QrLotItsNF.Value);
end;

procedure TFmDeposimp.FormClose(Sender: TObject; var Action: TCloseAction);
var
  Cam: String;
begin
  Cam := Application.Title + '\XYConfig\DeposImp';
  Geral.WriteAppKey('Ordem1', Cam, RGOrdem1.ItemIndex, ktInteger, HKEY_LOCAL_MACHINE);
  Geral.WriteAppKey('Ordem2', Cam, RGOrdem2.ItemIndex, ktInteger, HKEY_LOCAL_MACHINE);
  Geral.WriteAppKey('Ordem3', Cam, RGOrdem3.ItemIndex, ktInteger, HKEY_LOCAL_MACHINE);
  Geral.WriteAppKey('Ordem4', Cam, RGOrdem4.ItemIndex, ktInteger, HKEY_LOCAL_MACHINE);
  Geral.WriteAppKey('Agrupa', Cam, RGAgrupa.ItemIndex, ktInteger, HKEY_LOCAL_MACHINE);
  //Geral.WriteAppKey('Sit000', Cam, CkQuitado.Checked, ktBoolean, HKEY_LOCAL_MACHINE);
  Geral.WriteAppKey('P0', Cam, CkP0.Checked, ktBoolean, HKEY_LOCAL_MACHINE);
  Geral.WriteAppKey('P1', Cam, CkP1.Checked, ktBoolean, HKEY_LOCAL_MACHINE);
  Geral.WriteAppKey('P2', Cam, CkP2.Checked, ktBoolean, HKEY_LOCAL_MACHINE);
  Geral.WriteAppKey('Repasse', Cam, RGRepasse.ItemIndex, ktInteger, HKEY_LOCAL_MACHINE);
  Geral.WriteAppKey('Reforco', Cam, RGReforco.ItemIndex, ktInteger, HKEY_LOCAL_MACHINE);
end;

procedure TFmDeposimp.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  FImprime := False;
  BtPesquisaClick(Self);
  BtImprimeClick(Self);
  ConfiguracaoInicial;
  BtPesquisaClick(Self);
end;

procedure TFmDeposimp.EdColigadoChange(Sender: TObject);
begin
  BtImprime.Enabled := False;
end;

procedure TFmDeposimp.BtImprimeClick(Sender: TObject);
begin
  MyObjects.frxMostra(frxDeposIts, 'Dep�sito de cheques');
end;

procedure TFmDeposimp.RGOrdem1Click(Sender: TObject);
begin
  BtImprime.Enabled := False;
end;

procedure TFmDeposimp.TPIniChange(Sender: TObject);
begin
  BtImprime.Enabled := False;
end;

procedure TFmDeposimp.TPFimChange(Sender: TObject);
begin
  BtImprime.Enabled := False;
end;

procedure TFmDeposimp.EdEmitenteChange(Sender: TObject);
begin
  BtImprime.Enabled := False;
end;

procedure TFmDeposimp.RGMascaraClick(Sender: TObject);
begin
  BtImprime.Enabled := False;
end;

procedure TFmDeposimp.EdCPFChange(Sender: TObject);
begin
  BtImprime.Enabled := False;
end;

procedure TFmDeposimp.CkQuitadoClick(Sender: TObject);
begin
  BtImprime.Enabled := False;
end;

procedure TFmDeposimp.EdBandaChange(Sender: TObject);
var
  CMC7: String;
  Banda: TBandaMagnetica;
  //Ban: Integer;
begin
  CMC7 := Geral.SoNumero_TT(EdBanda.Text);
  //Ban := Length(EdBanda.Text);
  if MLAGeral.LeuTodoCMC7(EdBanda.Text) then
  begin
    if MLAGeral.CalculaCMC7(CMC7) = 0 then
    begin
      Banda := TBandaMagnetica.Create;
      Banda.BandaMagnetica := CMC7;
      EdBanco.Text   := Banda.Banco;
      EdAgencia.Text := Banda.Agencia;
      EdConta.Text   := Banda.Conta;
      EdCheque.Text  := Banda.Numero;
      //
      if LocalizaItem then
      begin
        ConfereCheque(1);
        Timer2.Enabled := True;
      end else begin
        Beep;
        Application.MessageBox('Cheque n�o localizado!', 'Erro',
          MB_OK+MB_ICONERROR);
      end;
    end;
  end;
end;

procedure TFmDeposimp.DBEdItensChange(Sender: TObject);
begin
  DBEdItens.Font.Color := MLAGeral.BoolToInt2(QrSumTQTDE.Value =
    QrSumDQTDE.Value, clGreen, clRed);
end;

procedure TFmDeposimp.DBEdValorChange(Sender: TObject);
begin
  DBEdValor.Font.Color := MLAGeral.BoolToInt2(QrSumTValor.Value =
    QrSumDValor.Value, clGreen, clRed);
end;

function TFmDeposimp.LocalizaItem: Boolean;
begin
  Result := QrLotIts.Locate('Banco;Agencia;Conta;Cheque', VarArrayOf([
  Geral.IMV(EdBanco.Text), Geral.IMV(EdAgencia.Text), EdConta.Text,
  Geral.IMV(EdCheque.Text)]), []);
  if Result then FLOIS := QrLotItsFatParcela.Value;
end;

procedure TFmDeposimp.ConfereCheque(Status: Integer);
begin
{
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE lot esits SET AlterWeb=1,   Depositado=:P0 WHERE Controle=:P1');
  Dmod.QrUpd.Params[0].AsInteger := Status;
  Dmod.QrUpd.Params[1].AsInteger := QrLotItsFatParcela.Value;
  Dmod.QrUpd.ExecSQL;
}
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_TabLctA, False, [
  'Depositado'], ['FatID', 'FatParcela'], [
  Status], [VAR_FATID_0301, QrLotItsFatParcela.Value], True);
  //
  ReopenLotIts();
end;

procedure TFmDeposimp.BtLimparClick(Sender: TObject);
const
  Depositado = CO_CH_DEPOSITADO_NAO;
begin
  Screen.Cursor := crHourGlass;
  //
  QrLotIts.First;
  while not QrLotIts.Eof do
  begin
{
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE lot esits SET AlterWeb=1,   Depositado=0 WHERE Controle=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrLotItsFatParcela.Value;
    Dmod.QrUpd.ExecSQL;
}
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_TabLctA, False, [
    'Depositado'], ['FatID', 'FatParcela'], [
    Depositado], [VAR_FATID_0301, QrLotItsFatParcela.Value], True);
    //
    QrLotIts.Next;
  end;
  //
  ReopenLotIts();
  Screen.Cursor := crDefault;
end;

procedure TFmDeposimp.DBG1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if Column.FieldName = 'Depositado' then
    MeuVCLSkin.DrawGrid(DBG1, Rect, 1, QrLotItsDepositado.Value);
  if Column.FieldName = 'CONF_V' then
    MeuVCLSkin.DrawGrid(DBG1, Rect, 1, QrLotItsCONF_V.Value);
  //if Column.FieldName = 'CONF_I' then
    //MeuVCLSkin.DrawGrid(DBG1, Rect, 1, QrLotItsCONF_I.Value);
  if Column.FieldName = 'ReforcoCxa' then
    MeuVCLSkin.DrawGrid(DBG1, Rect, 1, QrLotItsReforcoCxa.Value);
end;

procedure TFmDeposimp.DBG1CellClick(Column: TColumn);
var
  Valor: Double;
begin
  FLOIS := QrLotItsFatParcela.Value;
  if (Column.FieldName = 'Depositado') then
    ConfereCheque(MLAGeral.IntBool_Inverte(QrLotItsDepositado.Value));
  if (Column.FieldName = 'CONF_V') then
  //or (Column.FieldName = 'CONF_I') then
  begin
    if QrLotItsCONF_V.Value = 1 then Valor := 0 else
      Valor := QrLotItsCredito.Value;
    InsereValDeposito(Valor, QrLotItsCredito.Value, QrLotItsFatParcela.Value,
      False);
  end;
  if (Column.FieldName = 'ReforcoCxa') then
    ReforcoCxa(MLAGeral.IntBool_Inverte(QrLotItsReforcoCxa.Value));
  SomaLinhas(True);
end;

procedure TFmDeposimp.QrLotItsAfterOpen(DataSet: TDataSet);
begin
  QrLotIts.Locate('FatParcela', FLOIS, []);
  EdBanda.Enabled := True;
end;

procedure TFmDeposimp.CkP0Click(Sender: TObject);
begin
  BtImprime.Enabled := False;
end;

procedure TFmDeposimp.CkP1Click(Sender: TObject);
begin
  BtImprime.Enabled := False;
end;

procedure TFmDeposimp.CkP2Click(Sender: TObject);
begin
  BtImprime.Enabled := False;
end;

procedure TFmDeposimp.CkR0Click(Sender: TObject);
begin
  BtImprime.Enabled := False;
end;

procedure TFmDeposimp.QrLotItsAfterClose(DataSet: TDataSet);
begin
  EdBanda.Enabled := False;
end;

procedure TFmDeposimp.EdValorExit(Sender: TObject);
begin
  case RGOQue.ItemIndex of
    0:
    begin
      if LocalizaItem then
      //if QrLoc.RecordCount > 0 then
      begin
        InsereValDeposito(Geral.DMV(EdValor.Text), QrLotItsCredito.Value,
          QrLotItsFatParcela.Value, True);
        VoltaABanda;
      end;
    end;
    1: EdBanda.SetFocus;
  end;
end;

procedure TFmDeposimp.InsereValDeposito(Valor, Real: Double; FatParcela: Integer;
  Avisa: Boolean);
begin
{
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE lot esits SET AlterWeb=1,   ValDeposito=:P0 ');
  Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
  Dmod.QrUpd.Params[0].AsFloat   := Valor;
  Dmod.QrUpd.Params[1].AsInteger := Controle;
  Dmod.QrUpd.ExecSQL;
}
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_TabLctA, False, [
  'ValDeposito'], ['FatID', 'FatParcela'], [
  Valor], [VAR_FATID_0301, FatParcela], True);
  //
  //
  ReopenLotIts();
  //
  if (Int(Valor*100) <> (Real*100)) and Avisa  then
  begin
    Beep;
    Geral.MensagemBox('Valor n�o confere!', 'Aviso', MB_OK+MB_ICONWARNING);
  end;
end;

procedure TFmDeposimp.VoltaABanda;
begin
  EdBanda.Text := '';
  EdBanda.SetFocus;
end;

procedure TFmDeposimp.Timer2Timer(Sender: TObject);
begin
  Timer2.Enabled := False;
  case RGOque.ItemIndex of
   0: EdValor.SetFocus;
   1: VoltaABanda;
   2: EdValor.SetFocus;
  end;
end;

procedure TFmDeposimp.ReforcoCxa(Status: Integer);
begin
{
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE lot esits SET AlterWeb=1, ReforcoCxa=:P0 WHERE Controle=:P1');
  Dmod.QrUpd.Params[0].AsInteger := Status;
  Dmod.QrUpd.Params[1].AsInteger := QrLotItsFatParcela.Value;
  Dmod.QrUpd.ExecSQL;
}
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_TabLctA, False, [
  'ReforcoCxa'], ['FatID', 'FatParcela'], [
  Status], [VAR_FATID_0301, QrLotItsFatParcela.Value], True);
  //
  //
  ReopenLotIts();
end;

procedure TFmDeposimp.RGRepasseClick(Sender: TObject);
begin
  LaColigado.Enabled := RGRepasse.ItemIndex > 0;
  EdColigado.Enabled := RGRepasse.ItemIndex > 0;
  CBColigado.Enabled := RGRepasse.ItemIndex > 0;
  BtImprime.Enabled := False;
end;

procedure TFmDeposimp.RGReforcoClick(Sender: TObject);
begin
  BtImprime.Enabled := False;
end;

procedure TFmDeposimp.BtConfiguraClick(Sender: TObject);
begin
  UMyMod.ConfigJanela20(Name, EdCliente, CBCliente, TPIni, TPFim,
    RGOrdem1, RGOrdem2, RGOrdem3, RGOrdem4, EdColigado, CBColigado,
    RGMascara, CkP0, CkP1, CkP2, CkP3, RGRepasse, RGReforco, RGAgrupa,
    nil, nil);
end;

procedure TFmDeposimp.Selecionados1Click(Sender: TObject);
begin
  ContaDeposito(1);
end;

procedure TFmDeposimp.Tudo1Click(Sender: TObject);
begin
  ContaDeposito(2);
end;

procedure TFmDeposimp.ContaDeposito(Tipo: Integer);
begin
  FLOIS := QrLotItsFatParcela.Value;
  VAR_CARTDEP := -2;
  Application.CreateForm(TFmCartDep, FmCartDep);
  FmCartDep.ShowModal;
  FmCartDep.Destroy;
  Screen.Cursor := crHourGlass;
  if VAR_CARTDEP <> -2 then
  begin
    case Tipo of
      1:
      begin
        if DBG1.SelectedRows.Count = 0 then DepositaAtual
        else DepositaSelecionados;
      end;
      2:
      begin
        QrLotIts.DisableControls;
        QrLotIts.First;
        while not QrLotIts.Eof do
        begin
          DepositaAtual;
          QrLotIts.Next;
        end;
        QrLotIts.EnableControls;
      end;
      else Application.MessageBox('Op��o de conta n�o implementada!', 'Aviso',
        MB_OK+MB_ICONINFORMATION);
    end;
    ReopenLotIts();
  end;
  Screen.Cursor := crDefault;
end;

procedure TFmDeposimp.DepositaSelecionados;
var
  i : Integer;
begin
  with DBG1.DataSource.DataSet do
  for i:= 0 to DBG1.SelectedRows.Count-1 do
  begin
    //GotoBookmark(pointer(DBG1.SelectedRows.Items[i]));
    GotoBookmark(DBG1.SelectedRows.Items[i]);
    DepositaAtual;
  end;
end;

procedure TFmDeposimp.DepositaAtual;
begin
{
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('UPDATE lot esits SET AlterWeb=1, CartDep=:P0 WHERE Controle=:P1 ');
  //
  Dmod.QrUpdM.Params[0].AsInteger := VAR_CARTDEP;
  Dmod.QrUpdM.Params[1].AsInteger := QrLotItsFatParcela.Value;
  //
  Dmod.QrUpdM.ExecSQL;
}
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_TabLctA, False, [
  'CartDep'], ['FatID', 'FatParcela'], [
  VAR_CARTDEP], [VAR_FATID_0301, QrLotItsFatParcela.Value], True);
  //
end;

procedure TFmDeposimp.DBG1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_DOWN) and (Shift=[ssShift]) then PreparaSoma(True)  else
  if (Key=VK_UP)   and (Shift=[ssShift]) then PreparaSoma(False) else
  if (Key=VK_DOWN)                       then PreparaSoma(True)  else
  if (Key=VK_UP)                         then PreparaSoma(False); 
end;

procedure TFmDeposimp.PreparaSoma(Ascendente: Boolean);
begin
  FAscendente := Ascendente;
  if Timer3.Enabled then Timer3.Enabled := False;
  Timer3.Enabled := True;
end;

procedure TFmDeposimp.SomaLinhas(Ascendente: Boolean);
var
  Valor: Double;
  i, k: Integer;
begin
  k := QrLotItsFatParcela.Value;
  if DBG1.SelectedRows.Count = 0 then Valor := QrLotItsCredito.Value else
  begin
    Valor := 0;
    with DBG1.DataSource.DataSet do
    begin
      if Ascendente then
      begin
        for i:= 0 to DBG1.SelectedRows.Count-1 do
        begin
          //GotoBookmark(pointer(DBG1.SelectedRows.Items[i]));
          GotoBookmark(DBG1.SelectedRows.Items[i]);
          Valor := Valor + QrLotItsCredito.Value;
        end;
      end else begin
        for i:= DBG1.SelectedRows.Count-1 downto 0 do
        begin
          //GotoBookmark(pointer(DBG1.SelectedRows.Items[i]));
          GotoBookmark(DBG1.SelectedRows.Items[i]);
          Valor := Valor + QrLotItsCredito.Value;
        end;
      end;
    end;
    QrLotIts.Locate('FatParcela', k, []);
  end;
  EdSoma.Text := Geral.FFT(Valor, 2, siNegativo);
end;

procedure TFmDeposimp.Timer3Timer(Sender: TObject);
begin
  SomaLinhas(FAscendente);
end;

procedure TFmDeposimp.BtDepositoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMDeposito, BtDeposito);
end;

procedure TFmDeposimp.frxDeposIts_GetValue(const VarName: String;
  var Value: Variant);
begin
  if VarName = 'VARF_CLIENTE' then
  begin
    if CBCliente.KeyValue = NULL then Value := ' ' else
    Value := CBCliente.Text;
  end else if VarName = 'VARF_TODOS' then
  begin
    if CBCliente.KeyValue = NULL then Value := 'TODOS CLIENTES' else
    Value := ' ';
  end else if VarName = 'VARF_REFORCO' then
  begin
    case RGReforco.ItemIndex of
      0: Value := 'N�o';
      1: Value := 'Sim';
      2: Value := 'N+S';
      else Value := '???';
    end;
  end else if VarName = 'VARF_REPASSE' then
  begin
    case RGRepasse.ItemIndex of
      0: Value := 'N�o';
      1: Value := 'Sim';
      2: Value := 'N+S';
      else Value := '???';
    end;
  end else if VarName = 'VARF_PERIODO' then Value :=
    FormatDateTime(VAR_FORMATDATE3, TPIni.Date) + CO_ATE +
    FormatDateTime(VAR_FORMATDATE3, TPFim.Date)
  else if VarName = 'VARF_QTD_CHEQUES' then Value := QrLotIts.RecordCount
  else if VarName = 'VFR_LA1NOME' then
  begin
    case RGOrdem1.ItemIndex of
      0: Value := 'Cliente: '+QrLotItsNOMECLIENTE.Value;
      1: Value := 'Dep�sito no dia  '+QrLotItsDDeposito_TXT.Value;
      2: Value := 'Dep�sito no m�s de '+QrLotItsMES_TXT.Value;
      3: Value := 'Emitido por: '+QrLotItsEmitente.Value;
      4: Value := 'CPF/CNPJ: '+QrLotItsCPF_TXT.Value;
    end;
  end
  else if VarName = 'VFR_LA2NOME' then
  begin
    case RGOrdem2.ItemIndex of
      0: Value := 'Cliente: '+QrLotItsNOMECLIENTE.Value;
      1: Value := 'Dep�sito no dia  '+QrLotItsDDeposito_TXT.Value;
      2: Value := 'Dep�sito no m�s de '+QrLotItsMES_TXT.Value;
      3: Value := 'Emitido por: '+QrLotItsEmitente.Value;
      4: Value := 'CPF/CNPJ: '+QrLotItsCPF_TXT.Value;
    end;
  end
  else if VarName = 'VFR_LA3NOME' then
  begin
    case RGOrdem3.ItemIndex of
      0: Value := 'Cliente: '+QrLotItsNOMECLIENTE.Value;
      1: Value := 'Dep�sito no dia  '+QrLotItsDDeposito_TXT.Value;
      2: Value := 'Dep�sito no m�s de '+QrLotItsMES_TXT.Value;
      3: Value := 'Emitido por: '+QrLotItsEmitente.Value;
      4: Value := 'CPF/CNPJ: '+QrLotItsCPF_TXT.Value;
    end;
  end
  else if VarName = 'VARF_SITUACOES' then
  begin
    Value := ' ';
    //if CkQuitado.Checked then Value := Value + CkQuitado.Caption;
    //if CkRepassado.Checked then Value := Value + '(incluindo repassados)';
    //if Trim(Value) = '' then Value := '(??????)';
  end
  else if VarName = 'VARF_FILTROS' then
  begin
    Value := '';
    if Geral.IMV(EdColigado.Text) <> 0 then Value := Value +
      'Coligado: '+CBColigado.Text+ '  ';
    if FEmitente <> '' then Value := Value + '  {Emitente: '+ FEmitente+'}';
    if FCPF <> '' then Value := Value + '  {CPF/CNPJ: '+
    Geral.FormataCNPJ_TT(FCPF)+'}';
    if Value <> '' then Value := 'FILTROS : '+Value;
  end;


  if VarName = 'VFR_ORD1' then
  begin
    if RGAgrupa.ItemIndex < 1 then Value := 0 else
    Value := RGOrdem1.ItemIndex + 1;
  end else
  if VarName = 'VFR_ORD2' then
  begin
    if RGAgrupa.ItemIndex < 2 then Value := 0 else
    Value := RGOrdem2.ItemIndex + 1;
  end else
  if VarName = 'VFR_ORD3' then
  begin
    if RGAgrupa.ItemIndex < 3 then Value := 0 else
    Value := RGOrdem3.ItemIndex + 1;
  end else
  if VarName = 'VARF_ANALITICO' then
  begin
    if RGTipo.ItemIndex = 1 then Value := 0 else Value := 17;
  end
end;

end.

