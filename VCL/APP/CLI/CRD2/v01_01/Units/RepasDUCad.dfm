object FmRepasDUCad: TFmRepasDUCad
  Left = 368
  Top = 194
  Caption = 'REP-GEREN-002 :: Repasse de Duplicatas'
  ClientHeight = 661
  ClientWidth = 958
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 105
    Width = 958
    Height = 556
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBCntrl: TGroupBox
      Left = 0
      Top = 492
      Width = 958
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
          NumGlyphs = 2
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
          NumGlyphs = 2
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
          NumGlyphs = 2
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
          NumGlyphs = 2
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 435
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 412
          Top = 0
          Width = 109
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
            NumGlyphs = 2
          end
        end
        object BtReceitas: TBitBtn
          Tag = 143
          Left = 8
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Inclui novo banco'
          Caption = '&Lote'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtReceitasClick
          NumGlyphs = 2
        end
        object BtCheque: TBitBtn
          Tag = 146
          Left = 99
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Altera banco atual'
          Caption = '&Duplicata'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtChequeClick
          NumGlyphs = 2
        end
        object BtImportar: TBitBtn
          Tag = 147
          Left = 190
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = 'Im&porta'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtImportarClick
          NumGlyphs = 2
        end
      end
    end
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 958
      Height = 205
      Align = alTop
      TabOrder = 1
      object PainelData: TPanel
        Left = 2
        Top = 15
        Width = 954
        Height = 47
        Align = alTop
        BevelOuter = bvNone
        Enabled = False
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 4
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          FocusControl = DBEdCodigo
        end
        object Label2: TLabel
          Left = 76
          Top = 4
          Width = 44
          Height = 13
          Caption = 'Coligado:'
          FocusControl = DBEdNome
        end
        object Label4: TLabel
          Left = 644
          Top = 4
          Width = 26
          Height = 13
          Caption = 'Data:'
          FocusControl = DBEdit1
        end
        object Label5: TLabel
          Left = 712
          Top = 4
          Width = 53
          Height = 13
          Caption = 'Valor base:'
          FocusControl = DBEdit2
        end
        object Label6: TLabel
          Left = 788
          Top = 4
          Width = 37
          Height = 13
          Caption = '$ Juros:'
          FocusControl = DBEdit3
        end
        object Label7: TLabel
          Left = 864
          Top = 4
          Width = 39
          Height = 13
          Caption = 'L'#237'quido:'
          FocusControl = DBEdit4
        end
        object DBEdCodigo: TDBEdit
          Left = 8
          Top = 20
          Width = 65
          Height = 21
          Hint = 'N'#186' do banco'
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsRepas
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8281908
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
        end
        object DBEdNome: TDBEdit
          Left = 76
          Top = 20
          Width = 565
          Height = 21
          Hint = 'Nome do banco'
          Color = clWhite
          DataField = 'NOMECOLIGADO'
          DataSource = DsRepas
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
        end
        object DBEdit1: TDBEdit
          Left = 644
          Top = 20
          Width = 64
          Height = 21
          DataField = 'Data'
          DataSource = DsRepas
          TabOrder = 2
        end
        object DBEdit2: TDBEdit
          Left = 712
          Top = 20
          Width = 72
          Height = 21
          DataField = 'Total'
          DataSource = DsRepas
          TabOrder = 3
        end
        object DBEdit3: TDBEdit
          Left = 788
          Top = 20
          Width = 72
          Height = 21
          DataField = 'JurosV'
          DataSource = DsRepas
          TabOrder = 4
        end
        object DBEdit4: TDBEdit
          Left = 864
          Top = 20
          Width = 72
          Height = 21
          DataField = 'SALDO'
          DataSource = DsRepas
          TabOrder = 5
        end
      end
      object GradeRepas: TDBGrid
        Left = 2
        Top = 62
        Width = 954
        Height = 141
        Align = alClient
        DataSource = DsRepasIts
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Duplicata'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Banco'
            Title.Caption = 'Bco.'
            Width = 27
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Agencia'
            Title.Caption = 'Ag.'
            Width = 31
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Valor'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Taxa'
            Title.Caption = '% Taxa'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'JurosP'
            Title.Caption = '% Juros'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'JurosV'
            Title.Caption = '$ Juros'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LIQUIDO'
            Title.Caption = 'L'#205'QUIDO'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CPF_TXT'
            Title.Caption = 'CPF / CNPJ'
            Width = 120
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Emitente'
            Width = 337
            Visible = True
          end>
      end
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 105
    Width = 958
    Height = 556
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 958
      Height = 393
      Align = alTop
      TabOrder = 0
      object Label9: TLabel
        Left = 12
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label75: TLabel
        Left = 116
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Coligado:'
      end
      object Label3: TLabel
        Left = 608
        Top = 16
        Width = 66
        Height = 13
        Caption = 'Data repasse:'
      end
      object EdCodigo: TdmkEdit
        Left = 12
        Top = 32
        Width = 100
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 2
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdColigado: TdmkEditCB
        Left = 116
        Top = 32
        Width = 65
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBColigado
      end
      object CBColigado: TdmkDBLookupComboBox
        Left = 184
        Top = 32
        Width = 421
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMECOLIGADO'
        ListSource = DsColigado
        TabOrder = 2
        dmkEditCB = EdColigado
        UpdType = utYes
      end
      object TPData: TDateTimePicker
        Left = 608
        Top = 32
        Width = 112
        Height = 21
        Date = 38724.439835069400000000
        Time = 38724.439835069400000000
        TabOrder = 3
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 493
      Width = 958
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
        NumGlyphs = 2
      end
      object Panel6: TPanel
        Left = 848
        Top = 15
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
          NumGlyphs = 2
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 958
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 910
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        TabOrder = 0
        OnClick = SbNumeroClick
        NumGlyphs = 2
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        TabOrder = 1
        OnClick = SbNomeClick
        NumGlyphs = 2
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        TabOrder = 2
        OnClick = SbQueryClick
        NumGlyphs = 2
      end
      object BtImpCalculado: TBitBtn
        Tag = 144
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        TabOrder = 3
        OnClick = BtImpCalculadoClick
        NumGlyphs = 2
      end
      object BtImpPesquisa: TBitBtn
        Tag = 145
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        TabOrder = 4
        OnClick = BtImpPesquisaClick
        NumGlyphs = 2
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 694
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 282
        Height = 32
        Caption = 'Repasse de Duplicatas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 282
        Height = 32
        Caption = 'Repasse de Duplicatas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 282
        Height = 32
        Caption = 'Repasse de Duplicatas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 958
    Height = 53
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 954
      Height = 36
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Progress: TProgressBar
        Left = 0
        Top = 19
        Width = 954
        Height = 17
        Align = alBottom
        TabOrder = 0
        Visible = False
      end
    end
  end
  object DsRepas: TDataSource
    DataSet = QrRepas
    Left = 544
    Top = 21
  end
  object QrRepas: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrRepasBeforeOpen
    AfterOpen = QrRepasAfterOpen
    AfterScroll = QrRepasAfterScroll
    SQL.Strings = (
      'SELECT CASE WHEN en.Tipo=0 THEN en.RazaoSocial'
      'ELSE en.Nome END NOMECOLIGADO, en.FatorCompra,'
      're.*, (re.Total-re.JurosV) SALDO'
      'FROM repas re'
      'LEFT JOIN entidades en ON en.Codigo=re.Coligado'
      'WHERE re.Codigo > 0')
    Left = 516
    Top = 21
    object QrRepasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrRepasColigado: TIntegerField
      FieldName = 'Coligado'
    end
    object QrRepasData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrRepasTotal: TFloatField
      FieldName = 'Total'
      DisplayFormat = '#,###,##0.00'
    end
    object QrRepasJurosV: TFloatField
      FieldName = 'JurosV'
      DisplayFormat = '#,###,##0.00'
    end
    object QrRepasNOMECOLIGADO: TWideStringField
      FieldName = 'NOMECOLIGADO'
      Size = 100
    end
    object QrRepasSALDO: TFloatField
      FieldName = 'SALDO'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrRepasFatorCompra: TFloatField
      FieldName = 'FatorCompra'
      DisplayFormat = '#,###,##0.000000'
    end
  end
  object QrColigado: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrRepasBeforeOpen
    AfterOpen = QrRepasAfterOpen
    AfterScroll = QrRepasAfterScroll
    SQL.Strings = (
      'SELECT Codigo, CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMECOLIGADO'
      'FROM entidades'
      'WHERE Fornece2="V"')
    Left = 140
    Top = 69
    object QrColigadoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrColigadoNOMECOLIGADO: TWideStringField
      FieldName = 'NOMECOLIGADO'
      Size = 100
    end
  end
  object DsColigado: TDataSource
    DataSet = QrColigado
    Left = 168
    Top = 65
  end
  object QrRepasIts: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrRepasItsAfterOpen
    OnCalcFields = QrRepasItsCalcFields
    SQL.Strings = (
      'SELECT li.Duplicata, li.Banco, li.Agencia,'
      'li.Emitente, li.CNPJCPF, li.DDeposito, li.Vencimento, '
      '(ri.Valor - ri.JurosV) LIQUIDO, ri.* '
      'FROM repasits ri'
      'LEFT JOIN lct0001a li ON li.FatParcela=ri.Origem'
      'WHERE li.FatID=301'
      'AND ri.Codigo=:P0'
      ''
      '')
    Left = 572
    Top = 21
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrRepasItsDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 12
    end
    object QrRepasItsBanco: TIntegerField
      FieldName = 'Banco'
      DisplayFormat = '000'
    end
    object QrRepasItsAgencia: TIntegerField
      FieldName = 'Agencia'
      DisplayFormat = '0000'
    end
    object QrRepasItsEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrRepasItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrRepasItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrRepasItsOrigem: TIntegerField
      FieldName = 'Origem'
      Required = True
    end
    object QrRepasItsDias: TIntegerField
      FieldName = 'Dias'
      Required = True
    end
    object QrRepasItsTaxa: TFloatField
      FieldName = 'Taxa'
      Required = True
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrRepasItsJurosP: TFloatField
      FieldName = 'JurosP'
      Required = True
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrRepasItsJurosV: TFloatField
      FieldName = 'JurosV'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrRepasItsCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Size = 40
      Calculated = True
    end
    object QrRepasItsDDeposito: TDateField
      FieldName = 'DDeposito'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrRepasItsLIQUIDO: TFloatField
      FieldName = 'LIQUIDO'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrRepasItsCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrRepasItsVencimento: TDateField
      FieldName = 'Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrRepasItsValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###.###,##0.00'
    end
  end
  object DsRepasIts: TDataSource
    DataSet = QrRepasIts
    Left = 600
    Top = 21
  end
  object QrBanco: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM bancos'
      'WHERE Codigo=:P0')
    Left = 633
    Top = 21
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrBancoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrBancoNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrBancoLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrBancoDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrBancoDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrBancoUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrBancoUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrBancoSite: TWideStringField
      FieldName = 'Site'
      Size = 255
    end
  end
  object DsBanco: TDataSource
    DataSet = QrBanco
    Left = 661
    Top = 21
  end
  object PMLote: TPopupMenu
    Left = 268
    Top = 408
    object Incluinovolote1: TMenuItem
      Caption = '&Inclui novo lote'
      OnClick = Incluinovolote1Click
    end
    object Alteraloteatual1: TMenuItem
      Caption = '&Altera lote atual'
      OnClick = Alteraloteatual1Click
    end
    object Excluiloteatual1: TMenuItem
      Caption = '&Exclui lote atual'
      Enabled = False
    end
  end
  object PMCheque: TPopupMenu
    Left = 372
    Top = 416
    object Adicionachequeaoloteatual1: TMenuItem
      Caption = '&Adiciona duplicata ao lote atual'
      OnClick = Adicionachequeaoloteatual1Click
    end
    object Alterarepassedochequeatual1: TMenuItem
      Caption = '&Edita repasse da duplicata atual'
      Enabled = False
    end
    object Retirachequedoloteatual1: TMenuItem
      Caption = '&Retira duplicata do lote atual'
      OnClick = Retirachequedoloteatual1Click
    end
  end
  object QrSum: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Valor) Valor, SUM(JurosV) JurosV'
      'FROM repasits'
      'WHERE Codigo=:P0'
      '')
    Left = 201
    Top = 69
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumValor: TFloatField
      FieldName = 'Valor'
    end
    object QrSumJurosV: TFloatField
      FieldName = 'JurosV'
    end
  end
  object QrLI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lo.Tipo, li.Repassado, li.FatNum, li.FatParcela,'
      'li.DDeposito, li.DCompra, li.Credito, li.Vencimento,'
      'li.Banco, li.Agencia, li.Duplicata '
      'FROM lct0001a li'
      'LEFT JOIN lot0001a lo ON lo.Codigo=li.FatNum'
      'WHERE li.FatID=301'
      'AND lo.Codigo=:P0'
      ''
      '')
    Left = 108
    Top = 233
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLITipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrLIRepassado: TSmallintField
      FieldName = 'Repassado'
    end
    object QrLIFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrLIFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrLIDDeposito: TDateField
      FieldName = 'DDeposito'
    end
    object QrLIDCompra: TDateField
      FieldName = 'DCompra'
    end
    object QrLICredito: TFloatField
      FieldName = 'Credito'
    end
    object QrLIVencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrLIBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrLIAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrLIDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 13
    end
  end
  object frxRepasIts: TfrxReport
    Version = '4.10.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.990536921300000000
    ReportOptions.LastChange = 39717.990536921300000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxRepasItsGetValue
    Left = 246
    Top = 345
    Datasets = <
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsRepas
        DataSetName = 'frxDsRepas'
      end
      item
        DataSet = frxDsRepasIts
        DataSetName = 'frxDsRepasIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        Height = 72.000000000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo32: TfrxMemoView
          Left = 538.661410000000000000
          Top = 4.102350000000000000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Picture1: TfrxPictureView
          Left = 2.661410000000000000
          Top = 0.102350000000001000
          Width = 168.000000000000000000
          Height = 64.000000000000000000
          ShowHint = False
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo49: TfrxMemoView
          Left = 174.661410000000000000
          Top = 28.102350000000000000
          Width = 520.000000000000000000
          Height = 26.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDsMaster."Em"]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 36.000000000000000000
        Top = 385.512060000000000000
        Width = 718.110700000000000000
        object Memo31: TfrxMemoView
          Left = 553.779530000000000000
          Top = 2.487630000000020000
          Width = 88.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[PAGE#] de ')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 645.559060000000000000
          Top = 2.487630000000020000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            '[TOTALPAGES]')
          ParentFont = False
        end
      end
      object Band4: TfrxMasterData
        Height = 17.000000000000000000
        Top = 260.787570000000000000
        Width = 718.110700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsRepasIts
        DataSetName = 'frxDsRepasIts'
        RowCount = 0
        object Memo6: TfrxMemoView
          Left = 70.661410000000000000
          Width = 24.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[FormatFloat('#39'000'#39', <frxDsRepasIts."Banco">)]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 94.661410000000000000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[FormatFloat('#39'0000'#39', <frxDsRepasIts."Agencia">)]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 2.661410000000000000
          Width = 68.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8 = (
            '[frxDsRepasIts."Duplicata"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 122.661410000000000000
          Width = 184.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8 = (
            '[frxDsRepasIts."Emitente"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 306.661410000000000000
          Width = 92.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8 = (
            '[frxDsRepasIts."CPF_TXT"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 398.661410000000000000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsRepasIts."Valor"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 458.661410000000000000
          Width = 48.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsRepasIts."Vencto"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 506.661410000000000000
          Width = 48.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsRepasIts."DDeposito"]')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Left = 554.661410000000000000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsRepasIts."Dias"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 582.661410000000000000
          Width = 56.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsRepasIts."JurosV"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 638.661410000000000000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsRepasIts."LIQUIDO"]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 87.614100000000000000
        Top = 113.385900000000000000
        Width = 718.110700000000000000
        object Memo3: TfrxMemoView
          Left = 598.661410000000000000
          Top = 45.614100000000000000
          Width = 100.000000000000000000
          Height = 20.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDsRepas."Data"]')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Left = 2.661410000000000000
          Top = 43.614100000000000000
          Width = 696.000000000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.500000000000000000
        end
        object Memo24: TfrxMemoView
          Left = 2.661410000000000000
          Top = 45.614100000000000000
          Width = 436.000000000000000000
          Height = 20.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            'Coligado: [frxDsRepas."Coligado"] - [frxDsRepas."NOMECOLIGADO"]')
          ParentFont = False
        end
        object Line2: TfrxLineView
          Left = 2.661410000000000000
          Top = 66.614100000000000000
          Width = 696.000000000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.500000000000000000
        end
        object Memo69: TfrxMemoView
          Left = 494.661410000000000000
          Top = 45.614100000000000000
          Width = 100.000000000000000000
          Height = 20.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            'Data do repasse:')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 70.661410000000000000
          Top = 70.614100000000000000
          Width = 24.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            'Bco.')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 94.661410000000000000
          Top = 70.614100000000000000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            'Ag.')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 2.661410000000000000
          Top = 70.614100000000000000
          Width = 68.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8 = (
            'Duplicata')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 122.661410000000000000
          Top = 70.614100000000000000
          Width = 184.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8 = (
            'Sacado')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 306.661410000000000000
          Top = 70.614100000000000000
          Width = 92.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8 = (
            'CPF / CNPJ')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 398.661410000000000000
          Top = 70.614100000000000000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            'Valor')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 458.661410000000000000
          Top = 70.614100000000000000
          Width = 48.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            'Vencto')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 506.661410000000000000
          Top = 70.614100000000000000
          Width = 48.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            'Resgate')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 554.661410000000000000
          Top = 70.614100000000000000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            'Dias')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 582.661410000000000000
          Top = 70.614100000000000000
          Width = 56.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '$ Juros')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 638.661410000000000000
          Top = 70.614100000000000000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '$ L'#195#173'quido')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 2.661410000000000000
          Top = 6.173160000000000000
          Width = 696.000000000000000000
          Height = 20.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8 = (
            'REPASSE DE DUPLICATAS')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 634.661410000000000000
          Top = 6.173160000000000000
          Width = 64.000000000000000000
          Height = 20.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsRepas."Codigo"]')
          ParentFont = False
        end
        object Memo68: TfrxMemoView
          Left = 570.661410000000000000
          Top = 6.173160000000000000
          Width = 64.000000000000000000
          Height = 20.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            'Controle:')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        Height = 24.000000000000000000
        Top = 340.157700000000000000
        Width = 718.110700000000000000
        object Memo34: TfrxMemoView
          Left = 2.661410000000000000
          Top = 4.535250000000020000
          Width = 396.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8 = (
            'Total dos [VARF_QTD_CHEQUES] cheques deste border'#195#180':')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 398.661410000000000000
          Top = 4.535250000000020000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsRepasIts."Valor">)]')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Left = 582.661410000000000000
          Top = 4.535250000000020000
          Width = 56.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsRepasIts."JurosV">)]')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 638.661410000000000000
          Top = 4.535250000000020000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsRepasIts."LIQUIDO">)]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 458.661410000000000000
          Top = 4.535250000000020000
          Width = 124.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          ParentFont = False
        end
      end
    end
  end
  object frxDsRepasIts: TfrxDBDataset
    UserName = 'frxDsRepasIts'
    CloseDataSource = False
    DataSet = QrRepasIts
    BCDToCurrency = False
    Left = 274
    Top = 345
  end
  object frxDsRepas: TfrxDBDataset
    UserName = 'frxDsRepas'
    CloseDataSource = False
    DataSet = QrRepas
    BCDToCurrency = False
    Left = 302
    Top = 345
  end
end
