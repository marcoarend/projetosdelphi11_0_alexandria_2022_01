unit RepasCHIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Mask, DmkDAC_PF, UnDmkEnums, UnDmkProcFunc;

type
  TFmRepasCHIts = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesNOMECLIENTE: TWideStringField;
    DsClientes: TDataSource;
    PainelItens: TPanel;
    GradeCHs: TDBGrid;
    Panel5: TPanel;
    Label36: TLabel;
    Label11: TLabel;
    Label10: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label32: TLabel;
    Label8: TLabel;
    Label31: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    EdBanda: TdmkEdit;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    EdBanco: TdmkEdit;
    EdAgencia: TdmkEdit;
    EdConta: TdmkEdit;
    EdCheque: TdmkEdit;
    EdCPF: TdmkEdit;
    DBEdit5: TDBEdit;
    EdTaxa: TdmkEdit;
    CkInap: TCheckBox;
    EdBordero: TdmkEdit;
    MeAviso: TMemo;
    QrCheques: TmySQLQuery;
    DsCheques: TDataSource;
    GBConfirma: TGroupBox;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    BtConfirma2: TBitBtn;
    BtExclui2: TBitBtn;
    QrBanco: TmySQLQuery;
    QrBancoCodigo: TIntegerField;
    QrBancoNome: TWideStringField;
    QrBancoLk: TIntegerField;
    QrBancoDataCad: TDateField;
    QrBancoDataAlt: TDateField;
    QrBancoUserCad: TIntegerField;
    QrBancoUserAlt: TIntegerField;
    QrBancoSite: TWideStringField;
    DsBanco: TDataSource;
    Timer1: TTimer;
    QrChequesLote: TIntegerField;
    QrChequesCredito: TFloatField;
    QrChequesFatID: TIntegerField;
    QrChequesFatID_Sub: TIntegerField;
    QrChequesFatNum: TFloatField;
    QrChequesFatParcela: TIntegerField;
    QrChequesEmitente: TWideStringField;
    QrChequesBanco: TIntegerField;
    QrChequesAgencia: TIntegerField;
    QrChequesContaCorrente: TWideStringField;
    QrChequesCNPJCPF: TWideStringField;
    QrChequesDocumento: TFloatField;
    QrChequesDCompra: TDateField;
    QrChequesDDeposito: TDateField;
    QrChequesCPF_TXT: TWideStringField;
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdBandaChange(Sender: TObject);
    procedure EdBandaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdClienteChange(Sender: TObject);
    procedure EdBancoChange(Sender: TObject);
    procedure EdBancoExit(Sender: TObject);
    procedure EdAgenciaExit(Sender: TObject);
    procedure EdContaExit(Sender: TObject);
    procedure EdChequeExit(Sender: TObject);
    procedure EdCPFExit(Sender: TObject);
    procedure EdBorderoExit(Sender: TObject);
    procedure EdTaxaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure CkInapClick(Sender: TObject);
    procedure QrChequesAfterClose(DataSet: TDataSet);
    procedure QrChequesAfterOpen(DataSet: TDataSet);
    procedure QrChequesCalcFields(DataSet: TDataSet);
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirma2Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }
    procedure RepassaCheque();
    procedure ReopenCheques(LocCod: Double);
    procedure ReopenBanco();
    procedure CalculaCMC_7();
  public
    { Public declarations }
  end;

  var
  FmRepasCHIts: TFmRepasCHIts;

implementation

uses UnMyObjects, Module, RepasCHCad, UMySQLModule, Principal, MyListas;

{$R *.DFM}

procedure TFmRepasCHIts.BtConfirma2Click(Sender: TObject);
var
  Codigo, n, i: integer;
begin
  n := GradeCHs.SelectedRows.Count;
  if n > 0 then
  begin
    if Application.MessageBox(PChar('Confirma o repasse dos '+IntToStr(n)+
    ' cheques selecionados?'), 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) =
    ID_YES then
    begin
      with GradeCHs.DataSource.DataSet do
      for i:= 0 to GradeCHs.SelectedRows.Count-1 do
      begin
        //GotoBookmark(pointer(GradeCHs.SelectedRows.Items[i]));
        GotoBookmark(GradeCHs.SelectedRows.Items[i]);
        RepassaCheque;
      end;
    end;
  end else begin
    if Application.MessageBox('Confirma o repasse do cheque selecionado?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then RepassaCheque;
  end;
  Codigo := FmRepasCHCad.QrRepasCodigo.Value;
  FmRepasCHCad.CalculaLote(Codigo);
  FmRepasCHCad.LocCod(Codigo, Codigo);
  if EdBanda.Text <> '' then EdBanda.SetFocus else EdBanco.Text;
  EdBanda.Text := '';
  ReopenCheques(0);
  if ImgTipo.SQLType = stUpd then
    Close;
end;

procedure TFmRepasCHIts.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmRepasCHIts.CalculaCMC_7();
var
  Banda: TBandaMagnetica;
  CMC_7: String;
begin
  CMC_7 := Geral.SoNumero_TT(EdBanda.Text);
  if Trim(CMC_7) = '' then
  begin
    //EdComp.Text    := '';
    EdBanco.Text   := '';
    EdAgencia.Text := '';
    EdConta.Text   := '';
    EdCheque.Text  := '';
    //
    ReopenBanco();
    ReopenCheques(0);
  end;
  //Tam1 := Length(CMC_7);
  //Tam0 := Length(EdBanda.Text);
  if MLAGeral.LeuTodoCMC7(EdBanda.Text) then
  begin
    if MLAGeral.CalculaCMC7(CMC_7) = 0 then
    begin
      Banda := TBandaMagnetica.Create;
      Banda.BandaMagnetica := CMC_7;
      //EdComp.Text    := Banda.Compe;
      EdBanco.Text   := Banda.Banco;
      EdAgencia.Text := Banda.Agencia;
      EdConta.Text   := Banda.Conta;
      EdCheque.Text  := Banda.Numero;
      //
      ReopenBanco();
      ReopenCheques(0);
      Timer1.Enabled := True;
      //EdTaxa.SetFocus; ser� feito pelo Timer1
    end;
  end;
end;

procedure TFmRepasCHIts.CkInapClick(Sender: TObject);
begin
  ReopenCheques(QrChequesFatNum.Value);
end;

procedure TFmRepasCHIts.EdAgenciaExit(Sender: TObject);
begin
  ReopenCheques(0);
end;

procedure TFmRepasCHIts.EdBancoChange(Sender: TObject);
begin
  if not EdBanco.Focused then
    ReopenBanco();
end;

procedure TFmRepasCHIts.EdBancoExit(Sender: TObject);
begin
  ReopenBanco();
  ReopenCheques(0);
end;

procedure TFmRepasCHIts.EdBandaChange(Sender: TObject);
begin
  CalculaCMC_7();
end;

procedure TFmRepasCHIts.EdBandaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_ESCAPE then
    Close;
end;

procedure TFmRepasCHIts.EdBorderoExit(Sender: TObject);
begin
  ReopenCheques(QrChequesFatNum.Value);
end;

procedure TFmRepasCHIts.EdChequeExit(Sender: TObject);
begin
  ReopenCheques(0);
end;

procedure TFmRepasCHIts.EdClienteChange(Sender: TObject);
begin
  EdBordero.Enabled := EdCliente.ValueVariant <> 0;
  ReopenCheques(QrChequesFatNum.Value);
end;

procedure TFmRepasCHIts.EdContaExit(Sender: TObject);
begin
  ReopenCheques(0);
end;

procedure TFmRepasCHIts.EdCPFExit(Sender: TObject);
begin
  ReopenCheques(0);
end;

procedure TFmRepasCHIts.EdTaxaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F12 then
  begin
    if BtConfirma2.Enabled then BtConfirma2Click(Self) else
      Application.MessageBox('Nenhum cheque foi localizado!', 'Aviso',
      MB_OK+MB_ICONWARNING);
      if EdBanda.Text <> '' then EdBanda.SetFocus else EdBanco.Text;
      EdBanda.Text := '';
  end;
end;

procedure TFmRepasCHIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  BtExclui2.Enabled := FmRepasCHCad.QrRepasIts.RecordCount > 0;
end;

procedure TFmRepasCHIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stNil;
  UMyMod.AbreQuery(QrClientes, Dmod.MyDB);
end;

procedure TFmRepasCHIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmRepasCHIts.QrChequesAfterClose(DataSet: TDataSet);
begin
  BtConfirma2.Enabled := False;
end;

procedure TFmRepasCHIts.QrChequesAfterOpen(DataSet: TDataSet);
begin
  if QrCheques.RecordCount > 0 then BtConfirma2.Enabled := True else
  BtConfirma2.Enabled := False;
end;

procedure TFmRepasCHIts.QrChequesCalcFields(DataSet: TDataSet);
begin
  QrChequesCPF_TXT.Value := Geral.FormataCNPJ_TT(QrChequesCNPJCPF.Value);
end;

procedure TFmRepasCHIts.ReopenBanco();
begin
  QrBanco.Close;
  QrBanco.Params[0].AsString := EdBanco.Text;
  UMyMod.AbreQuery(QrBanco, Dmod.MyDB);
end;

procedure TFmRepasCHIts.ReopenCheques(LocCod: Double);
var
  Cheque, Cliente, Banco, Agencia, Bordero: Integer;
begin
{
  QrCheques.Close;
  QrCheques.SQL.Clear;
  QrCheques.SQL.Add('SELECT lo.Lote, li.*');
  QrCheques.SQL.Add('FROM lot esits li');
  QrCheques.SQL.Add('LEFT JOIN lot es lo ON lo.Codigo=li.Codigo');
  QrCheques.SQL.Add('WHERE lo.Tipo=0');
  QrCheques.SQL.Add('AND li.DDeposito > "'+
    FormatDateTime(VAR_FORMATDATE, FmRepasCHCad.QrRepasData.Value)+'"');
  QrCheques.SQL.Add('AND li.Quitado = 0');
  QrCheques.SQL.Add('AND li.Repassado = 0');
  //
  Cliente := EdCliente.ValueVariant;
  if Cliente > 0 then
    QrCheques.SQL.Add('AND lo.Cliente='+IntToStr(Cliente));
  //
  Banco := EdBanco.ValueVariant;
  if Banco > 0 then
    QrCheques.SQL.Add('AND li.Banco='+IntToStr(Banco));
  //
  Agencia := EdAgencia.ValueVariant;
  if Agencia > 0 then
    QrCheques.SQL.Add('AND li.Agencia='+IntToStr(Agencia));
  //
  if Trim(EdConta.Text) <> '' then
    QrCheques.SQL.Add('AND li.Conta='+EdConta.Text);
  //
  Cheque := EdCheque.ValueVariant;
  if Cheque > 0 then
    QrCheques.SQL.Add('AND li.Cheque='+IntToStr(Cheque));
  //
  if Trim(EdCPF.Text) <> '' then
    QrCheques.SQL.Add('AND li.CPF='+Geral.SoNumero_TT(EdCPF.Text));
  //
  Cliente := EdCliente.ValueVariant;
  if Cliente <> 0 then
    QrCheques.SQL.Add('AND lo.Cliente='+IntToStr(Cliente));
  //
  if EdBordero.Enabled then
  begin
    Bordero := EdBordero.ValueVariant;
    if Bordero <> 0 then
      QrCheques.SQL.Add('AND lo.Lote='+IntToStr(Bordero));
  end;
  //
  QrCheques. Open;
}
  Cliente := EdCliente.ValueVariant;
  Banco := EdBanco.ValueVariant;
  Agencia := EdAgencia.ValueVariant;
  Cheque := EdCheque.ValueVariant;
  if EdBordero.Enabled then
    Bordero := EdBordero.ValueVariant
  else
    Bordero := 0;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrCheques, Dmod.MyDB, [
  'SELECT lo.Lote, li.* ',
  'FROM ' + CO_TabLctA + ' li ',
  'LEFT JOIN ' + CO_TabLotA + ' lo ON lo.Codigo=li.FatNum ',
  'WHERE li.FatID=' + Geral.FF0(VAR_FATID_0301),
  'AND lo.Tipo=0 ',
  'AND li.DDeposito > "' + Geral.FDT(FmRepasCHCad.QrRepasData.Value, 1)+'" ',
  'AND li.Quitado = 0 ',
  'AND li.Repassado = 0 ',
  Geral.ATS_if(Cliente > 0, [
  'AND lo.Cliente=' + Geral.FF0(Cliente)]),
  Geral.ATS_if(Banco > 0, [
  'AND li.Banco=' + Geral.FF0(Banco)]),
  Geral.ATS_if(Agencia > 0, [
  'AND li.Agencia=' + Geral.FF0(Agencia)]),
  Geral.ATS_if(Trim(EdConta.Text) <> '', [
  'AND li.ContaCorrente=' + EdConta.Text]),
  Geral.ATS_if(Cheque > 0, [
  'AND li.Documento=' + Geral.FF0(Cheque)]),
  Geral.ATS_if(Trim(EdCPF.Text) <> '', [
  'AND li.CNPJCPF="' + Geral.SoNumero_TT(EdCPF.Text) + '"']),
  Geral.ATS_if(Bordero <> 0, [
  'AND lo.Lote=' + Geral.FF0(Bordero)]),
  '']);
  //
  if LocCod > 0 then
    QrCheques.Locate('FatNum', LocCod, []);
end;

procedure TFmRepasCHIts.RepassaCheque();
var
  Controle, Codigo, Origem, Dias: Integer;
  Valor, Taxa, JurosP, JurosV: Double;
begin
  Codigo := FmRepasCHCad.QrRepasCodigo.Value;
  Origem := QrChequesFatParcela.Value;
  Taxa   := Geral.DMV(EdTaxa.Text);
  Dias   := Trunc(int(QrChequesDDeposito.Value) - int(FmRepasCHCad.QrRepasData.Value));
  JurosP := MLAGeral.CalculaJuroComposto(Taxa, Dias);
  Valor  := QrChequesCredito.Value;
  JurosV := (Trunc(Valor * JurosP))/100;
  //
  Dmod.QrUpdU.SQL.Clear;
  if ImgTipo.SQLType = stIns then
  begin
    Dmod.QrUpdU.SQL.Add('INSERT INTO repasits SET ');
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'RepasIts', 'RepasIts', 'Codigo');
  end else begin
    Dmod.QrUpdU.SQL.Add('UPDATE repasits SET ');
    Controle := FmRepasCHCad.QrRepasItsControle.Value;
  end;
  Dmod.QrUpdU.SQL.Add('Origem=:P0, Taxa=:P1, JurosP=:P2, JurosV=:P3, ');
  Dmod.QrUpdU.SQL.Add('Dias=:P4, Valor=:P5, ');
  //
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('DataCad=:Pa, UserCad=:Pb, Codigo=:Pc, Controle=:Pd')
  else Dmod.QrUpdU.SQL.Add('DataAlt=:Pa, UserAlt=:Pb WHERE Codigo=:Pc AND Controle=:Pd');
  Dmod.QrUpdU.Params[00].AsInteger := Origem;
  Dmod.QrUpdU.Params[01].AsFloat   := Taxa;
  Dmod.QrUpdU.Params[02].AsFloat   := JurosP;
  Dmod.QrUpdU.Params[03].AsFloat   := JurosV;
  Dmod.QrUpdU.Params[04].AsInteger := Dias;
  Dmod.QrUpdU.Params[05].AsFloat   := Valor;
  //
  Dmod.QrUpdU.Params[06].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[07].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[08].AsInteger := Codigo;
  Dmod.QrUpdU.Params[09].AsInteger := Controle;
  Dmod.QrUpdU.ExecSQL;
  //
{
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE lot esits SET AlterWeb=1,   Repassado=1 ');
  Dmod.QrUpd.SQL.Add('WHERE Controle=:P0');
  Dmod.QrUpd.Params[00].AsInteger := Origem;
  Dmod.QrUpd.ExecSQL;
}
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_TabLctA, False,[
  'Repassado'], ['FatID', 'FatParcela'], [
  1], [VAR_FATID_0301, Origem], True);
  //
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Repas', 'Codigo');
  FmRepasCHCad.FRepasIts := Controle;
end;

procedure TFmRepasCHIts.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  EdTaxa.SetFocus;
end;

end.
