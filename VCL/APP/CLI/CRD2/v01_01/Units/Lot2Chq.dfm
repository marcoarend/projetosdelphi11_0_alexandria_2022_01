object FmLot2Chq: TFmLot2Chq
  Left = 339
  Top = 185
  Caption = 'BDR-GEREN-004 :: Lote - Cheque'
  ClientHeight = 602
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 173
        Height = 32
        Caption = 'Lote - Cheque'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 173
        Height = 32
        Caption = 'Lote - Cheque'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 173
        Height = 32
        Caption = 'Lote - Cheque'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 532
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 1
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sair'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtConfirma2: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Confirma'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtConfirma2Click
      end
      object BtCalendario: TBitBtn
        Tag = 107
        Left = 372
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = 'Ca&lend'#225'rio'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtCalendarioClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 440
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 440
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 440
        Align = alClient
        TabOrder = 0
        object PainelBanda: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 300
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Painelx: TPanel
            Left = 0
            Top = 0
            Width = 1004
            Height = 148
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object Label36: TLabel
              Left = 4
              Top = 8
              Width = 143
              Height = 13
              Caption = 'Leitura pela banda magn'#233'tica:'
            end
            object Label37: TLabel
              Left = 316
              Top = 8
              Width = 33
              Height = 13
              Caption = 'Comp.:'
            end
            object Label38: TLabel
              Left = 352
              Top = 8
              Width = 34
              Height = 13
              Caption = 'Banco:'
            end
            object Label39: TLabel
              Left = 388
              Top = 8
              Width = 42
              Height = 13
              Caption = 'Ag'#234'ncia:'
            end
            object Label40: TLabel
              Left = 432
              Top = 8
              Width = 73
              Height = 13
              Caption = 'Conta corrente:'
            end
            object Label41: TLabel
              Left = 512
              Top = 8
              Width = 42
              Height = 13
              Caption = 'N'#186' cheq:'
            end
            object Label42: TLabel
              Left = 564
              Top = 8
              Width = 27
              Height = 13
              Caption = 'Valor:'
            end
            object Label43: TLabel
              Left = 636
              Top = 8
              Width = 59
              Height = 13
              Caption = 'Vencimento:'
            end
            object Label44: TLabel
              Left = 4
              Top = 52
              Width = 113
              Height = 13
              Caption = 'CGC / CPF [F4 - Pesq.]:'
            end
            object Label45: TLabel
              Left = 124
              Top = 52
              Width = 44
              Height = 13
              Caption = 'Emitente:'
            end
            object Label46: TLabel
              Left = 612
              Top = 52
              Width = 64
              Height = 13
              Caption = 'Tx compra %:'
            end
            object Label47: TLabel
              Left = 680
              Top = 52
              Width = 17
              Height = 13
              Caption = 'D+:'
            end
            object Label51: TLabel
              Left = 4
              Top = 96
              Width = 30
              Height = 13
              Caption = 'Prazo:'
            end
            object Label52: TLabel
              Left = 72
              Top = 96
              Width = 30
              Height = 13
              Caption = 'Comp:'
            end
            object Label53: TLabel
              Left = 140
              Top = 96
              Width = 49
              Height = 13
              Caption = 'Total dias:'
            end
            object Label6: TLabel
              Left = 208
              Top = 96
              Width = 71
              Height = 13
              Caption = 'Total Taxa (%):'
            end
            object Label25: TLabel
              Left = 632
              Top = 96
              Width = 64
              Height = 13
              Caption = 'Data compra:'
            end
            object Label94: TLabel
              Left = 292
              Top = 96
              Width = 79
              Height = 13
              Caption = 'Nome do banco:'
              FocusControl = DBEdit22
            end
            object Label210: TLabel
              Left = 292
              Top = 8
              Width = 24
              Height = 13
              Caption = 'Tipo:'
            end
            object Label208: TLabel
              Left = 440
              Top = 52
              Width = 104
              Height = 13
              Caption = 'Consulta ao SPC [F6]:'
            end
            object Label211: TLabel
              Left = 552
              Top = 96
              Width = 48
              Height = 13
              Caption = 'C/C real?:'
            end
            object EdBanda: TdmkEdit
              Left = 4
              Top = 24
              Width = 285
              Height = 21
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              MaxLength = 34
              ParentFont = False
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              OnChange = EdBandaChange
              OnKeyDown = EdBandaKeyDown
            end
            object EdRegiaoCompe: TdmkEdit
              Left = 316
              Top = 24
              Width = 33
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 3
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              OnChange = EdRegiaoCompeChange
              OnKeyDown = EdRegiaoCompeKeyDown
            end
            object EdBanco: TdmkEdit
              Left = 352
              Top = 24
              Width = 33
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 3
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 3
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              OnChange = EdBancoChange
            end
            object EdAgencia: TdmkEdit
              Left = 388
              Top = 24
              Width = 41
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 4
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 4
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              OnChange = EdAgenciaChange
            end
            object EdConta: TdmkEdit
              Left = 432
              Top = 24
              Width = 77
              Height = 21
              Alignment = taCenter
              Enabled = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 5
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              OnChange = EdContaChange
            end
            object EdCheque: TdmkEdit
              Left = 512
              Top = 24
              Width = 48
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 6
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 6
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '000000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
            end
            object EdValor: TdmkEdit
              Left = 564
              Top = 24
              Width = 69
              Height = 21
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 7
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              OnExit = EdValorExit
            end
            object EdVencto: TdmkEdit
              Left = 636
              Top = 24
              Width = 68
              Height = 21
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 8
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              OnEnter = EdVenctoEnter
              OnExit = EdVenctoExit
            end
            object EdCPF: TdmkEdit
              Left = 4
              Top = 68
              Width = 117
              Height = 21
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 9
              FormatType = dmktfString
              MskType = fmtCPFJ
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              OnChange = EdCPFChange
              OnExit = EdCPFExit
              OnKeyDown = EdCPFKeyDown
            end
            object EdEmitente: TdmkEdit
              Left = 124
              Top = 68
              Width = 313
              Height = 21
              CharCase = ecUpperCase
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 10
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
            end
            object EdTxaCompra: TdmkEdit
              Left = 612
              Top = 68
              Width = 65
              Height = 21
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 13
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 4
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,0000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              OnExit = EdTxaCompraExit
            end
            object EdDMaisC: TdmkEdit
              Left = 680
              Top = 68
              Width = 25
              Height = 21
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 14
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              OnExit = EdDMaisCExit
            end
            object EdPrazo: TdmkEdit
              Left = 4
              Top = 112
              Width = 64
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              ReadOnly = True
              TabOrder = 15
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
            end
            object EdCompensacao: TdmkEdit
              Left = 72
              Top = 112
              Width = 64
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              ReadOnly = True
              TabOrder = 16
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
            end
            object EdDias: TdmkEdit
              Left = 140
              Top = 112
              Width = 64
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              ReadOnly = True
              TabOrder = 17
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
            end
            object EdTotalCompraPer: TdmkEdit
              Left = 208
              Top = 112
              Width = 81
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              ReadOnly = True
              TabOrder = 18
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 6
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,000000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
            end
            object EdData: TdmkEdit
              Left = 632
              Top = 112
              Width = 73
              Height = 21
              TabStop = False
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 21
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              OnExit = EdDataExit
            end
            object DBEdit22: TDBEdit
              Left = 292
              Top = 112
              Width = 257
              Height = 21
              TabStop = False
              DataField = 'Nome'
              DataSource = DmLot2.DsBanco
              TabOrder = 19
            end
            object GroupBox3: TGroupBox
              Left = 707
              Top = 0
              Width = 297
              Height = 148
              Align = alRight
              Enabled = False
              TabOrder = 22
              object Label116: TLabel
                Left = 4
                Top = 26
                Width = 45
                Height = 13
                Caption = 'Cheques:'
              end
              object Label117: TLabel
                Left = 4
                Top = 48
                Width = 53
                Height = 13
                Caption = 'Duplicatas:'
              end
              object Label118: TLabel
                Left = 60
                Top = 8
                Width = 30
                Height = 13
                Caption = 'Risco:'
              end
              object Label119: TLabel
                Left = 137
                Top = 8
                Width = 42
                Height = 13
                Caption = 'Vencido:'
              end
              object Label120: TLabel
                Left = 214
                Top = 8
                Width = 56
                Height = 13
                Caption = 'Sub-total:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object Label121: TLabel
                Left = 4
                Top = 70
                Width = 56
                Height = 13
                Caption = 'Sub-total:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object Label122: TLabel
                Left = 4
                Top = 92
                Width = 60
                Height = 13
                Caption = 'Ocorr'#234'ncias:'
              end
              object EdDR_E: TdmkEdit
                Left = 60
                Top = 44
                Width = 76
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 3
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
              end
              object EdDT_E: TdmkEdit
                Left = 214
                Top = 44
                Width = 76
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 5
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
              end
              object EdRT_E: TdmkEdit
                Left = 60
                Top = 66
                Width = 76
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 6
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
              end
              object EdVT_E: TdmkEdit
                Left = 137
                Top = 66
                Width = 76
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 7
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
              end
              object EdST_E: TdmkEdit
                Left = 214
                Top = 66
                Width = 76
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 8
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
              end
              object EdCT_E: TdmkEdit
                Left = 214
                Top = 22
                Width = 76
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 2
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
              end
              object EdDV_E: TdmkEdit
                Left = 137
                Top = 44
                Width = 76
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 4
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
              end
              object EdCR_E: TdmkEdit
                Left = 60
                Top = 22
                Width = 76
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 0
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
              end
              object EdCV_E: TdmkEdit
                Left = 137
                Top = 22
                Width = 76
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 1
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
              end
              object EdOA_E: TdmkEdit
                Left = 137
                Top = 88
                Width = 76
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 9
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
              end
              object EdTT_E: TdmkEdit
                Left = 214
                Top = 88
                Width = 76
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 10
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
              end
            end
            object EdCMC_7: TdmkEdit
              Left = 256
              Top = 4
              Width = 33
              Height = 21
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 23
              Visible = False
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              OnChange = EdCMC_7Change
            end
            object EdRealCC: TdmkEdit
              Left = 552
              Top = 112
              Width = 77
              Height = 21
              TabStop = False
              Alignment = taCenter
              Enabled = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 20
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
            end
            object EdTipific: TdmkEdit
              Left = 292
              Top = 24
              Width = 21
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              OnChange = EdRegiaoCompeChange
              OnKeyDown = EdRegiaoCompeKeyDown
            end
            object EdStatusSPC: TdmkEdit
              Left = 440
              Top = 68
              Width = 20
              Height = 21
              Alignment = taRightJustify
              TabOrder = 11
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-1'
              ValMax = '2'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '-1'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = -1
              OnChange = EdStatusSPCChange
              OnKeyDown = EdStatusSPCKeyDown
            end
            object EdStatusSPC_TXT: TEdit
              Left = 460
              Top = 68
              Width = 149
              Height = 21
              TabStop = False
              ReadOnly = True
              TabOrder = 12
              Text = 'N'#227'o Pesquisado'
              OnKeyDown = EdStatusSPC_TXTKeyDown
            end
          end
          object PageControl7: TPageControl
            Left = 0
            Top = 148
            Width = 1004
            Height = 152
            ActivePage = TabSheet23
            Align = alClient
            TabOrder = 1
            TabStop = False
            ExplicitHeight = 72
            object TabSheet23: TTabSheet
              Caption = 'Risco cheques'
              ExplicitHeight = 44
              object DBGrid19: TDBGrid
                Left = 0
                Top = 0
                Width = 996
                Height = 124
                TabStop = False
                Align = alClient
                DataSource = DmLot.DsSacRiscoC
                TabOrder = 0
                TitleFont.Charset = ANSI_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Banco'
                    Width = 38
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Agencia'
                    Title.Caption = 'Ag'#234'ncia'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Conta'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DCompra'
                    Title.Caption = 'D.Compra'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Valor'
                    Title.Alignment = taRightJustify
                    Width = 80
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DDeposito'
                    Title.Caption = 'Vencimento'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Emitente'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'CPF_TXT'
                    Title.Caption = 'CPF / CNPJ'
                    Width = 113
                    Visible = True
                  end>
              end
            end
            object TabSheet24: TTabSheet
              Caption = 'Cheques devolvidos'
              ImageIndex = 1
              ExplicitHeight = 44
              object DBGrid20: TDBGrid
                Left = 0
                Top = 0
                Width = 996
                Height = 124
                TabStop = False
                Align = alClient
                DataSource = DmLot.DsSacCHDevA
                TabOrder = 0
                TitleFont.Charset = ANSI_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'DATA1_TXT'
                    Title.Caption = '1'#170' Devol.'
                    Width = 56
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Alinea1'
                    Title.Caption = 'Ali 1'
                    Width = 24
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Emitente'
                    Title.Caption = 'Nome emitente'
                    Width = 114
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'CPF_TXT'
                    Title.Caption = 'CPF / CNPJ emitente'
                    Width = 113
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Banco'
                    Title.Caption = 'Bco.'
                    Width = 28
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Agencia'
                    Title.Caption = 'Ag.'
                    Width = 32
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Conta'
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Cheque'
                    Width = 45
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Valor'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Taxas'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DATA3_TXT'
                    Title.Caption = #218'lt.Pgto'
                    Width = 56
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValPago'
                    Title.Caption = 'Pago'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SALDO'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ATUAL'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DATA2_TXT'
                    Title.Caption = '2'#170' Devol.'
                    Width = 56
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Alinea2'
                    Title.Caption = 'Ali 2'
                    Width = 24
                    Visible = True
                  end>
              end
            end
            object TabSheet25: TTabSheet
              Caption = 'Duplicatas'
              ImageIndex = 2
              ExplicitHeight = 44
              object DBGrid21: TDBGrid
                Left = 0
                Top = 0
                Width = 996
                Height = 124
                TabStop = False
                Align = alClient
                DataSource = DmLot.DsSacDOpen
                TabOrder = 0
                TitleFont.Charset = ANSI_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Duplicata'
                    Title.Caption = 'Vencidas'
                    Width = 68
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DCompra'
                    Title.Caption = 'D.Compra'
                    Width = 52
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Valor'
                    Title.Alignment = taRightJustify
                    Width = 68
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DDeposito'
                    Title.Caption = 'Vencto.'
                    Width = 52
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Emitente'
                    Width = 310
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SALDO_DESATUALIZ'
                    Title.Caption = 'Saldo'
                    Width = 68
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SALDO_ATUALIZADO'
                    Title.Caption = 'Atualizado'
                    Width = 68
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMESTATUS'
                    Title.Caption = 'Status'
                    Width = 90
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'STATUS'
                    Title.Caption = #218'ltima posi'#231#227'o no hist'#243'rico'
                    Width = 184
                    Visible = True
                  end>
              end
            end
            object TabSheet26: TTabSheet
              Caption = 'Ocorr'#234'ncias'
              ImageIndex = 3
              ExplicitHeight = 44
              object DBGrid22: TDBGrid
                Left = 0
                Top = 0
                Width = 996
                Height = 124
                TabStop = False
                Align = alClient
                DataSource = DmLot.DsSacOcorA
                TabOrder = 0
                TitleFont.Charset = ANSI_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'TIPODOC'
                    Title.Caption = 'TD'
                    Width = 20
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Ocorrencia'
                    Title.Caption = 'C'#243'd'
                    Width = 40
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMEOCORRENCIA'
                    Title.Caption = 'Ocorr'#234'ncia'
                    Width = 210
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DataO'
                    Title.Caption = 'Data'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Valor'
                    Width = 80
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'TaxaV'
                    Title.Caption = '$ Taxa'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Pago'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SALDO'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Data3'
                    Title.Caption = 'Ult.Pg'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ATUALIZADO'
                    Visible = True
                  end>
              end
            end
            object TabSheet27: TTabSheet
              Caption = 'Dados lote'
              ImageIndex = 4
              ExplicitHeight = 44
              object Panel44: TPanel
                Left = 0
                Top = 0
                Width = 996
                Height = 126
                Align = alTop
                Enabled = False
                TabOrder = 0
                object Label156: TLabel
                  Left = 4
                  Top = 4
                  Width = 36
                  Height = 13
                  Caption = 'C'#243'digo:'
                  FocusControl = LM_DBEdit1
                end
                object Label157: TLabel
                  Left = 72
                  Top = 4
                  Width = 35
                  Height = 13
                  Caption = 'Cliente:'
                  FocusControl = DBEdit38
                end
                object Label158: TLabel
                  Left = 231
                  Top = 44
                  Width = 24
                  Height = 13
                  Caption = 'Lote:'
                  FocusControl = DBEdit40
                end
                object Label159: TLabel
                  Left = 292
                  Top = 44
                  Width = 68
                  Height = 13
                  Caption = 'Ad Valorem %:'
                end
                object Label160: TLabel
                  Left = 4
                  Top = 44
                  Width = 74
                  Height = 13
                  Caption = 'Valor negociad:'
                  FocusControl = DBEdit42
                end
                object Label161: TLabel
                  Left = 80
                  Top = 44
                  Width = 76
                  Height = 13
                  Caption = 'Prazo m'#233'dio dd:'
                  FocusControl = DBEdit43
                end
                object Label162: TLabel
                  Left = 156
                  Top = 44
                  Width = 68
                  Height = 13
                  Caption = 'Taxa real per.:'
                  FocusControl = DBEdit44
                end
                object Label163: TLabel
                  Left = 4
                  Top = 84
                  Width = 65
                  Height = 13
                  Caption = 'Tx. compra $:'
                  FocusControl = DBEdit45
                end
                object Label164: TLabel
                  Left = 76
                  Top = 84
                  Width = 66
                  Height = 13
                  Caption = 'Ad Valorem $:'
                  FocusControl = DBEdit46
                end
                object Label165: TLabel
                  Left = 456
                  Top = 84
                  Width = 62
                  Height = 13
                  Caption = 'Valor l'#237'quido:'
                  FocusControl = DBEdit47
                end
                object Label166: TLabel
                  Left = 380
                  Top = 44
                  Width = 26
                  Height = 13
                  Caption = 'Data:'
                end
                object Label167: TLabel
                  Left = 152
                  Top = 84
                  Width = 44
                  Height = 13
                  Caption = 'Tarifas $:'
                  FocusControl = DBEdit49
                end
                object Label168: TLabel
                  Left = 228
                  Top = 84
                  Width = 52
                  Height = 13
                  Caption = 'Pg ocor. $:'
                  FocusControl = DBEdit50
                end
                object Label169: TLabel
                  Left = 304
                  Top = 84
                  Width = 51
                  Height = 13
                  Caption = '$ CH dev.:'
                  FocusControl = DBEdit51
                end
                object Label170: TLabel
                  Left = 540
                  Top = 84
                  Width = 40
                  Height = 13
                  Caption = 'A pagar:'
                end
                object Label171: TLabel
                  Left = 380
                  Top = 84
                  Width = 52
                  Height = 13
                  Caption = '$ DU dev.:'
                  FocusControl = DBEdit53
                end
                object Label172: TLabel
                  Left = 464
                  Top = 4
                  Width = 100
                  Height = 13
                  Caption = 'Tipo de documentos:'
                  FocusControl = DBEdit54
                end
                object Label173: TLabel
                  Left = 568
                  Top = 4
                  Width = 40
                  Height = 13
                  Caption = 'Border'#244':'
                  FocusControl = DBEdit55
                end
                object Label174: TLabel
                  Left = 624
                  Top = 4
                  Width = 23
                  Height = 13
                  Caption = 'N.F.:'
                  FocusControl = DBEdit56
                end
                object Label175: TLabel
                  Left = 568
                  Top = 44
                  Width = 26
                  Height = 13
                  Caption = 'Itens:'
                  FocusControl = DBEdit57
                end
                object Label176: TLabel
                  Left = 624
                  Top = 44
                  Width = 57
                  Height = 13
                  Caption = 'Taxa / m'#234's:'
                  FocusControl = DBEdit58
                end
                object Label177: TLabel
                  Left = 612
                  Top = 84
                  Width = 24
                  Height = 13
                  Caption = 'Tipo:'
                  FocusControl = DBEdit59
                end
                object DBText2: TDBText
                  Left = 447
                  Top = 44
                  Width = 122
                  Height = 17
                  DataField = 'CAPTIONCONFCARTA'
                end
                object Label178: TLabel
                  Left = 660
                  Top = 84
                  Width = 24
                  Height = 13
                  Caption = 'CBE:'
                  FocusControl = DBEdit61
                end
                object LM_DBEdit1: TDBEdit
                  Left = 4
                  Top = 20
                  Width = 65
                  Height = 21
                  Hint = 'N'#186' do banco'
                  TabStop = False
                  DataField = 'Codigo'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = 8281908
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  ParentShowHint = False
                  ReadOnly = True
                  ShowHint = True
                  TabOrder = 0
                end
                object DBEdit38: TDBEdit
                  Left = 72
                  Top = 20
                  Width = 57
                  Height = 21
                  DataField = 'Cliente'
                  TabOrder = 1
                end
                object DBEdit39: TDBEdit
                  Left = 132
                  Top = 20
                  Width = 329
                  Height = 21
                  DataField = 'NOMECLIENTE'
                  TabOrder = 2
                end
                object DBEdit40: TDBEdit
                  Left = 231
                  Top = 60
                  Width = 58
                  Height = 21
                  DataField = 'Lote'
                  TabOrder = 3
                end
                object DBEdit41: TDBEdit
                  Left = 292
                  Top = 60
                  Width = 85
                  Height = 21
                  DataField = 'ADVALOREM_TOTAL'
                  TabOrder = 4
                end
                object DBEdit42: TDBEdit
                  Left = 4
                  Top = 60
                  Width = 72
                  Height = 21
                  DataField = 'Total'
                  TabOrder = 5
                end
                object DBEdit43: TDBEdit
                  Left = 80
                  Top = 60
                  Width = 72
                  Height = 21
                  DataField = 'Dias'
                  TabOrder = 6
                end
                object DBEdit44: TDBEdit
                  Left = 156
                  Top = 60
                  Width = 72
                  Height = 21
                  DataField = 'TAXAPERCE_TOTAL'
                  TabOrder = 7
                end
                object DBEdit45: TDBEdit
                  Left = 4
                  Top = 100
                  Width = 69
                  Height = 21
                  DataField = 'TAXAVALOR_TOTAL'
                  TabOrder = 8
                end
                object DBEdit46: TDBEdit
                  Left = 76
                  Top = 100
                  Width = 72
                  Height = 21
                  DataField = 'VALVALOREM_TOTAL'
                  TabOrder = 9
                end
                object DBEdit47: TDBEdit
                  Left = 456
                  Top = 100
                  Width = 81
                  Height = 21
                  DataField = 'VAL_LIQUIDO'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 10
                end
                object DBEdit48: TDBEdit
                  Left = 380
                  Top = 60
                  Width = 64
                  Height = 21
                  DataField = 'Data'
                  TabOrder = 11
                end
                object DBEdit49: TDBEdit
                  Left = 152
                  Top = 100
                  Width = 72
                  Height = 21
                  DataField = 'Tarifas'
                  TabOrder = 12
                end
                object DBEdit50: TDBEdit
                  Left = 228
                  Top = 100
                  Width = 72
                  Height = 21
                  DataField = 'OcorP'
                  TabOrder = 13
                end
                object DBEdit51: TDBEdit
                  Left = 304
                  Top = 100
                  Width = 72
                  Height = 21
                  DataField = 'CHDevPg'
                  TabOrder = 14
                end
                object DBEdit52: TDBEdit
                  Left = 540
                  Top = 100
                  Width = 69
                  Height = 21
                  DataField = 'A_PG_LIQ'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 15
                end
                object DBEdit53: TDBEdit
                  Left = 380
                  Top = 100
                  Width = 72
                  Height = 21
                  DataField = 'DUDevPg'
                  TabOrder = 16
                end
                object DBEdit54: TDBEdit
                  Left = 464
                  Top = 20
                  Width = 101
                  Height = 21
                  DataField = 'NOMETIPOLOTE'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = 8388672
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 17
                end
                object DBEdit55: TDBEdit
                  Left = 568
                  Top = 20
                  Width = 52
                  Height = 21
                  DataField = 'Lote'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = 8388672
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 18
                end
                object DBEdit56: TDBEdit
                  Left = 624
                  Top = 20
                  Width = 60
                  Height = 21
                  DataField = 'NF'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = 8388672
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 19
                end
                object DBEdit57: TDBEdit
                  Left = 568
                  Top = 60
                  Width = 52
                  Height = 21
                  DataField = 'Itens'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = 8388672
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 20
                end
                object DBEdit58: TDBEdit
                  Left = 624
                  Top = 60
                  Width = 61
                  Height = 21
                  DataField = 'TAXAPERCEMENSAL'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = 8388672
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 21
                end
                object DBEdit59: TDBEdit
                  Left = 612
                  Top = 100
                  Width = 45
                  Height = 21
                  DataField = 'NOMESPREAD'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 22
                end
                object DBEdit60: TDBEdit
                  Left = 447
                  Top = 60
                  Width = 118
                  Height = 21
                  DataField = 'NOMECOFECARTA'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 23
                end
                object DBEdit61: TDBEdit
                  Left = 660
                  Top = 100
                  Width = 25
                  Height = 21
                  DataField = 'CBE'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 24
                end
              end
            end
            object TabSheet32: TTabSheet
              Caption = 'SPC'
              ImageIndex = 5
              ExplicitHeight = 44
              object MeSPC: TMemo
                Left = 0
                Top = 0
                Width = 996
                Height = 124
                TabStop = False
                Align = alClient
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Courier New'
                Font.Style = []
                ParentFont = False
                TabOrder = 0
                WordWrap = False
                ExplicitHeight = 44
              end
            end
          end
        end
        object GradeCH: TStringGrid
          Left = 2
          Top = 321
          Width = 1004
          Height = 117
          Align = alBottom
          DefaultRowHeight = 19
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
          TabOrder = 1
          OnDrawCell = GradeCHDrawCell
          OnKeyDown = GradeCHKeyDown
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 488
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
end
