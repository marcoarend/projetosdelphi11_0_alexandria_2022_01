object FmAlineasDup: TFmAlineasDup
  Left = 368
  Top = 194
  Caption = 'DUP-ALINE-001 :: Al'#237'neas de Duplicatas'
  ClientHeight = 362
  ClientWidth = 801
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 105
    Width = 801
    Height = 257
    Align = alClient
    BevelOuter = bvNone
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdit: TGroupBox
      Left = 0
      Top = 0
      Width = 801
      Height = 165
      Align = alTop
      TabOrder = 1
      object Label7: TLabel
        Left = 12
        Top = 16
        Width = 35
        Height = 13
        Caption = 'ID [F4]:'
      end
      object Label9: TLabel
        Left = 73
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object Label3: TLabel
        Left = 515
        Top = 16
        Width = 53
        Height = 13
        Caption = 'Valor base:'
      end
      object Label4: TLabel
        Left = 12
        Top = 60
        Width = 125
        Height = 13
        Caption = 'Ocorr'#234'ncia banc'#225'ria base:'
      end
      object SpeedButton5: TSpeedButton
        Left = 574
        Top = 76
        Width = 21
        Height = 21
        OnClick = SpeedButton5Click
      end
      object EdCodigo: TdmkEdit
        Left = 12
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnKeyDown = EdCodigoKeyDown
      end
      object EdNome: TdmkEdit
        Left = 73
        Top = 32
        Width = 436
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdBase: TdmkEdit
        Left = 515
        Top = 32
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'Base'
        UpdCampo = 'Base'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
      object RGDataPBase: TdmkRadioGroup
        Left = 601
        Top = 16
        Width = 185
        Height = 81
        Caption = ' Data base: '
        ItemIndex = 0
        Items.Strings = (
          'Inclus'#227'o da al'#237'nea'
          'Vencimento da duplicata')
        TabOrder = 3
        QryCampo = 'Datapbase'
        UpdCampo = 'Datapbase'
        UpdType = utYes
        OldValor = 0
      end
      object EdOcorrencia: TdmkEditCB
        Left = 12
        Top = 76
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'OcorrBase'
        UpdCampo = 'OcorrBase'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBOcorrencia
        IgnoraDBLookupComboBox = False
      end
      object CBOcorrencia: TdmkDBLookupComboBox
        Left = 68
        Top = 76
        Width = 504
        Height = 21
        DragCursor = crHandPoint
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsOcorBank
        TabOrder = 5
        dmkEditCB = EdOcorrencia
        QryCampo = 'OcorrBase'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object RGStatusBase: TdmkRadioGroup
        Left = 12
        Top = 103
        Width = 774
        Height = 45
        Caption = ' Status base: '
        Columns = 4
        ItemIndex = 0
        Items.Strings = (
          'Nenhum'
          'Prorrogado'
          'Baixado'
          'Moroso')
        TabOrder = 6
        QryCampo = 'StatusBase'
        UpdCampo = 'StatusBase'
        UpdType = utYes
        OldValor = 0
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 194
      Width = 801
      Height = 63
      Align = alBottom
      TabOrder = 0
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 664
        Top = 15
        Width = 135
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 105
    Width = 801
    Height = 257
    Align = alClient
    BevelOuter = bvNone
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object GBData: TGroupBox
      Left = 0
      Top = 0
      Width = 801
      Height = 164
      Align = alTop
      TabOrder = 0
      object Label1: TLabel
        Left = 12
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 72
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label5: TLabel
        Left = 515
        Top = 16
        Width = 53
        Height = 13
        Caption = 'Valor base:'
        FocusControl = dmkDBEdit1
      end
      object Label6: TLabel
        Left = 12
        Top = 60
        Width = 125
        Height = 13
        Caption = 'Ocorr'#234'ncia banc'#225'ria base:'
        FocusControl = dmkDBEdit2
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 12
        Top = 32
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsOcorDupl
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 72
        Top = 32
        Width = 436
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsOcorDupl
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit1: TdmkDBEdit
        Left = 515
        Top = 32
        Width = 80
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Base'
        DataSource = DsOcorDupl
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit2: TdmkDBEdit
        Left = 12
        Top = 76
        Width = 583
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'NOMEOCORBANK'
        DataSource = DsOcorDupl
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBRadioGroup1: TDBRadioGroup
        Left = 601
        Top = 16
        Width = 185
        Height = 81
        Caption = 'Data base'
        DataField = 'Datapbase'
        DataSource = DsOcorDupl
        Items.Strings = (
          'Inclus'#227'o da al'#237'nea'
          'Vencimento da duplicata')
        ParentBackground = True
        TabOrder = 4
        Values.Strings = (
          '0'
          '1')
      end
      object DBRadioGroup2: TDBRadioGroup
        Left = 12
        Top = 103
        Width = 774
        Height = 45
        Caption = ' Status base: '
        Columns = 4
        DataField = 'Statusbase'
        DataSource = DsOcorDupl
        Items.Strings = (
          'Nenhum'
          'Prorrogado'
          'Baixado'
          'Moroso')
        ParentBackground = True
        TabOrder = 5
        Values.Strings = (
          '0'
          '1'
          '2'
          '3')
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 193
      Width = 801
      Height = 64
      Align = alBottom
      TabOrder = 1
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 104
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 1
      end
      object Panel6: TPanel
        Left = 278
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 2
        object BtExclui: TBitBtn
          Tag = 12
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtIncluiClick
        end
        object Panel7: TPanel
          Left = 412
          Top = 0
          Width = 109
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BitBtn1: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtImportar: TBitBtn
          Tag = 19
          Left = 300
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = 'I&mporta'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          Visible = False
          OnClick = BtImportarClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 801
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 753
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 537
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 263
        Height = 32
        Caption = 'Al'#237'neas de Duplicatas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 263
        Height = 32
        Caption = 'Al'#237'neas de Duplicatas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 263
        Height = 32
        Caption = 'Al'#237'neas de Duplicatas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 801
    Height = 53
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 797
      Height = 36
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Progress: TProgressBar
        Left = 0
        Top = 19
        Width = 797
        Height = 17
        Align = alBottom
        TabOrder = 0
        Visible = False
      end
    end
  end
  object DsOcorDupl: TDataSource
    DataSet = QrOcorDupl
    Left = 560
    Top = 12
  end
  object QrOcorDupl: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrOcorDuplBeforeOpen
    AfterOpen = QrOcorDuplAfterOpen
    SQL.Strings = (
      'SELECT ok.Nome NOMEOCORBANK, od.* '
      'FROM ocordupl od'
      'LEFT JOIN ocorbank ok ON ok.Codigo=od.Ocorrbase')
    Left = 532
    Top = 12
    object QrOcorDuplCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOcorDuplNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrOcorDuplBase: TFloatField
      FieldName = 'Base'
      DisplayFormat = '#,###,##0.00'
    end
    object QrOcorDuplLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOcorDuplDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOcorDuplDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOcorDuplUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOcorDuplUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOcorDuplDatapbase: TIntegerField
      FieldName = 'Datapbase'
    end
    object QrOcorDuplOcorrbase: TIntegerField
      FieldName = 'Ocorrbase'
    end
    object QrOcorDuplStatusbase: TIntegerField
      FieldName = 'Statusbase'
    end
    object QrOcorDuplEnvio: TIntegerField
      FieldName = 'Envio'
    end
    object QrOcorDuplMovimento: TIntegerField
      FieldName = 'Movimento'
    end
    object QrOcorDuplAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrOcorDuplAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrOcorDuplNOMEOCORBANK: TWideStringField
      FieldName = 'NOMEOCORBANK'
      Size = 50
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 588
    Top = 12
  end
  object QrOcorBank: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome FROM ocorbank'
      'ORDER BY Nome')
    Left = 616
    Top = 12
    object QrOcorBankCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOcorBankNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsOcorBank: TDataSource
    DataSet = QrOcorBank
    Left = 644
    Top = 12
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = '*.txt'
    Filter = 'Arquivos Texto|*.txt'
    Left = 672
    Top = 12
  end
end
