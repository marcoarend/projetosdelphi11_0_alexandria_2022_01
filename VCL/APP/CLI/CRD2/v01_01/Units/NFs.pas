unit NFs;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, ComCtrls,
  OleCtrls, SHDocVw, Grids, DBGrids, Menus, frxClass, frxDBSet, dmkGeral,
  dmkEdit, dmkImage, UnDmkProcFunc, DmkDAC_PF, UnDmkEnums;

type
  TFmNFs = class(TForm)
    PainelDados: TPanel;
    DsNFs: TDataSource;
    QrNFs: TmySQLQuery;
    PainelEdita: TPanel;
    OpenDialog1: TOpenDialog;
    QrNFsCodigo: TIntegerField;
    QrNFsLk: TIntegerField;
    QrNFsDataCad: TDateField;
    QrNFsDataAlt: TDateField;
    QrNFsUserCad: TIntegerField;
    QrNFsUserAlt: TIntegerField;
    DBGrid1: TDBGrid;
    QrLot: TmySQLQuery;
    QrLotCodigo: TIntegerField;
    QrLotTipo: TSmallintField;
    QrLotCliente: TIntegerField;
    QrLotLote: TSmallintField;
    QrLotData: TDateField;
    QrLotTotal: TFloatField;
    QrLotDias: TFloatField;
    QrLotPeCompra: TFloatField;
    QrLotTxCompra: TFloatField;
    QrLotAdValorem: TFloatField;
    QrLotIOC: TFloatField;
    QrLotCPMF: TFloatField;
    QrLotLk: TIntegerField;
    QrLotDataCad: TDateField;
    QrLotDataAlt: TDateField;
    QrLotUserCad: TIntegerField;
    QrLotUserAlt: TIntegerField;
    QrLotNOMECLIENTE: TWideStringField;
    QrLotTipoAdV: TIntegerField;
    QrLotValValorem: TFloatField;
    QrLotSpread: TSmallintField;
    QrLotIOC_VAL: TFloatField;
    QrLotCPMF_VAL: TFloatField;
    QrLotISS: TFloatField;
    QrLotISS_Val: TFloatField;
    QrLotPIS_R: TFloatField;
    QrLotPIS_R_Val: TFloatField;
    QrLotIRRF: TFloatField;
    QrLotIRRF_Val: TFloatField;
    QrLotCNPJCPF: TWideStringField;
    QrLotTarifas: TFloatField;
    QrLotOcorP: TFloatField;
    QrLotCOFINS_R: TFloatField;
    QrLotCOFINS_R_Val: TFloatField;
    QrLotPIS: TFloatField;
    QrLotCOFINS: TFloatField;
    QrLotPIS_Val: TFloatField;
    QrLotCOFINS_Val: TFloatField;
    QrLotMaxVencto: TDateField;
    QrLotCHDevPg: TFloatField;
    QrLotMINTC: TFloatField;
    QrLotMINAV: TFloatField;
    QrLotMINTC_AM: TFloatField;
    QrLotPIS_T_Val: TFloatField;
    QrLotCOFINS_T_Val: TFloatField;
    QrLotPgLiq: TFloatField;
    QrLotDUDevPg: TFloatField;
    DsLot: TDataSource;
    QrSUM: TmySQLQuery;
    QrSUMMINAV: TFloatField;
    QrSUMISS_Val: TFloatField;
    QrNFsAdValor: TFloatField;
    QrNFsISS_Val: TFloatField;
    PMBordero: TPopupMenu;
    Adiona1: TMenuItem;
    Retira1: TMenuItem;
    QrMax: TmySQLQuery;
    QrMaxCodigo: TIntegerField;
    frxNF_Dados: TfrxReport;
    frxDsNF: TfrxDBDataset;
    PMNF: TPopupMenu;
    IncluinovaNF1: TMenuItem;
    AlteraNFatual1: TMenuItem;
    ExcluiNFatual1: TMenuItem;
    frxNF_Tudo: TfrxReport;
    QrLotNF: TIntegerField;
    QrLotItens: TIntegerField;
    QrLotConferido: TIntegerField;
    QrLotECartaSac: TSmallintField;
    QrLotCBE: TIntegerField;
    QrLotSCB: TIntegerField;
    QrLotAllQuit: TSmallintField;
    QrLotSobraIni: TFloatField;
    QrLotSobraNow: TFloatField;
    QrLotWebCod: TIntegerField;
    QrLotAlterWeb: TSmallintField;
    QrLotIOFd: TFloatField;
    QrLotIOFd_VAL: TFloatField;
    QrLotIOFv: TFloatField;
    QrLotIOFv_VAL: TFloatField;
    QrLotTipoIOF: TSmallintField;
    QrLotAtivo: TSmallintField;
    QrLotADVAL_TXT: TWideStringField;
    QrLotTAXA_AM_TXT: TWideStringField;
    QrLotSUB_TOTAL_MEU: TFloatField;
    QrLotVAL_LIQUIDO_MEU: TFloatField;
    QrLotA_PG_LIQ: TFloatField;
    PMImprime: TPopupMenu;
    DadosApenaspreencherimpresso1: TMenuItem;
    udoFolhaembranco1: TMenuItem;
    Semomeuendereo1: TMenuItem;
    Comomeuendereo1: TMenuItem;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BTNF: TBitBtn;
    BtEuro: TBitBtn;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel3: TPanel;
    BtDesiste: TBitBtn;
    GBEdita: TGroupBox;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    GBDados: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BTNFClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrNFsAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrNFsAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrNFsBeforeOpen(DataSet: TDataSet);
    procedure BtEuroClick(Sender: TObject);
    procedure Adiona1Click(Sender: TObject);
    procedure Retira1Click(Sender: TObject);
    procedure IncluinovaNF1Click(Sender: TObject);
    procedure AlteraNFatual1Click(Sender: TObject);
    procedure ExcluiNFatual1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure frxNF_TudoGetValue(const VarName: string; var Value: Variant);
    procedure PMNFPopup(Sender: TObject);
    procedure QrLotCalcFields(DataSet: TDataSet);
    procedure udoFolhaembranco1Click(Sender: TObject);
    procedure Semomeuendereo1Click(Sender: TObject);
    procedure Comomeuendereo1Click(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
  private
    FMEU_ENDERECO: Boolean;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
    procedure MostraEdicao(Mostra: Boolean; SQLTYpe: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    //
    procedure ReopenLot();
    procedure AtualizaNF;
    procedure ImprimeNF(MeuEndereco: Boolean);
  public
    { Public declarations }
  end;

var
  FmNFs: TFmNFs;

const
  FFormatFloat = '000000';  

implementation

uses UnMyObjects, Module, Principal, Lot0Loc, MyListas, ModuleLot, ModuleGeral;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmNFs.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmNFs.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrNFsCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmNFs.DefParams;
begin
  VAR_GOTOTABELA := 'NFs';
  VAR_GOTOMYSQLTABLE := QrNFs;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM nfs');
  VAR_SQLx.Add('WHERE Codigo > -1000');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmNFs.ExcluiNFatual1Click(Sender: TObject);
begin
  if QrLot.RecordCount > 0 then
  begin
    Application.MessageBox('Esta Nota Fiscal n�o pode ser excluida pois '+
    'cont�m border�s vinculados!', 'Erro', MB_OK+MB_ICONERROR);
  end else begin
    if Application.MessageBox('Confirma a exclus�o desta Nota Fiscal?', 'Pergunta',
      MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM nfs WHERE Codigo=:P0');
      Dmod.QrUpd.Params[0].AsInteger := QrNFsCodigo.Value;
      Dmod.QrUpd.ExecSQL;
      //
      LocCod(QrNFsCodigo.Value, QrNFsCodigo.Value);
    end;
  end;
end;

procedure TFmNFs.MostraEdicao(Mostra : Boolean; SQLType: TSQLTYpe; Codigo : Integer);
begin
  if Mostra then
  begin
    PainelEdita.Visible := True;
    PainelDados.Visible := False;
    if SQLType = stIns then
    begin
      QrMax.Close;
      UMyMod.AbreQuery(QrMax, Dmod.MyDB);
      EdCodigo.Text := IntToStr(QrMaxCodigo.Value+1);
      EdCodigo.ReadOnly := False;
      EdCodigo.SetFocus;
    end else begin
      EdCodigo.ReadOnly := True;
      EdCodigo.Text := DBEdCodigo.Text;
    end;
  end else begin
    PainelDados.Visible := True;
    PainelEdita.Visible := False;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmNFs.PMNFPopup(Sender: TObject);
begin
  AlteraNFAtual1.Enabled := QrNFsCodigo.Value > 0;
end;

procedure TFmNFs.Comomeuendereo1Click(Sender: TObject);
begin
  ImprimeNF(True);
end;

procedure TFmNFs.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmNFs.AlteraNFatual1Click(Sender: TObject);
begin
  AlteraRegistro;
end;

procedure TFmNFs.AlteraRegistro;
var
  NFs : Integer;
begin
  NFs := QrNFsCodigo.Value;
  if not UMyMod.SelLockY(NFs, Dmod.MyDB, 'NFs', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(NFs, Dmod.MyDB, 'NFs', 'Codigo');
      MostraEdicao(True, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmNFs.IncluinovaNF1Click(Sender: TObject);
begin
  IncluiRegistro;
end;

procedure TFmNFs.IncluiRegistro;
var
  Cursor : TCursor;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    MostraEdicao(True, stIns, 0);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmNFs.QueryPrincipalAfterOpen;
begin
end;

procedure TFmNFs.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmNFs.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmNFs.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmNFs.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmNFs.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmNFs.udoFolhaembranco1Click(Sender: TObject);
begin
  GOTOy.EnderecoDeEntidade(QrLotCliente.Value, 0);
  MyObjects.frxMostra(frxNF_Tudo, 'Nota Fiscal n� '+IntToStr(QrLotNF.Value));
end;

procedure TFmNFs.BTNFClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMNF, BtNF);
end;

procedure TFmNFs.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrNFsCodigo.Value;
  Close;
end;

procedure TFmNFs.BtConfirmaClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
(*
  Dmod.QrUpdU.SQL.Clear;
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('INSERT INTO nfs SET ')
  else Dmod.QrUpdU.SQL.Add('UPDATE nfs SET ');
  //
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else Dmod.QrUpdU.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  //
  Dmod.QrUpdU.Params[00].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[01].AsInteger := VAR_USUARIO;
  //
  Dmod.QrUpdU.Params[02].AsInteger := Codigo;
  Dmod.QrUpdU.ExecSQL;
*)
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'nfs', False, [
  'Codigo'], [], [Codigo], [], True) then
  begin
    UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'NFs', 'Codigo');
    MostraEdicao(False, stLok, 0);
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmNFs.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'NFs', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'NFs', 'Codigo');
  MostraEdicao(False, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'NFs', 'Codigo');
end;

procedure TFmNFs.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  GBEdita.Align := alClient;
  DBGrid1.Align := alClient;
  CriaOForm;
end;

procedure TFmNFs.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrNFsCodigo.Value,LaRegistro.Caption);
end;

procedure TFmNFs.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmNFs.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
  FmPrincipal.FLoteLoc := 0;
  Application.CreateForm(TFmLot0Loc, FmLot0Loc);
  FmLot0Loc.FFormCall := 3;
  FmLot0Loc.ShowModal;
  FmLot0Loc.Destroy;
  if FmPrincipal.FLoteLoc <> 0 then
  LocCod(FmPrincipal.FLoteLoc, FmPrincipal.FLoteLoc);
end;

procedure TFmNFs.SbNovoClick(Sender: TObject);
begin
//
end;

procedure TFmNFs.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmNFs.QrLotCalcFields(DataSet: TDataSet);
begin
  ///////// M�nimos ///////////////////
  if QrLotTotal.Value > 0 then
  begin
    QrLotADVAL_TXT.Value := Geral.FFT(QrLotMINAV.Value /
      QrLotTotal.Value * 100, 2, siPositivo);
  end else begin
    QrLotADVAL_TXT.Value := '0,00';
  end;
  QrLotTAXA_AM_TXT.Value := Geral.FFT(QrLotMINTC_AM.Value, 2, siPositivo);
  //
  QrLotSUB_TOTAL_MEU.Value := QrLotTotal.Value -
    QrLotMINAV.Value - QrLotMINTC.Value;
  QrLotVAL_LIQUIDO_MEU.Value := QrLotSUB_TOTAL_MEU.Value -
    QrLotIOC_VAL.Value - QrLotIOFd_VAL.Value - QrLotIOFv_VAL.Value;
  //
  QrLotA_PG_LIQ.Value := QrLotVAL_LIQUIDO_MEU.Value - QrLotPgLiq.Value;
  ///////// Fim M�nimos ///////////////////
end;

procedure TFmNFs.QrNFsAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
  ReopenLot();
end;

procedure TFmNFs.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'NFs', 'Livres', 99) then
  //BtInclui.Enabled := False;
  EdCodigo.MaxLength := Length(FFormatFloat);
end;

procedure TFmNFs.QrNFsAfterScroll(DataSet: TDataSet);
begin
  ReopenLot();
end;

procedure TFmNFs.SbQueryClick(Sender: TObject);
begin
  SbNomeClick(Self);
  //LocCod(QrNFsCodigo.Value,
  //CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'NFs', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmNFs.Semomeuendereo1Click(Sender: TObject);
begin
  ImprimeNF(False);
end;

procedure TFmNFs.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFs.frxNF_TudoGetValue(const VarName: string; var Value: Variant);
var
  Txt: String;
begin
  if AnsiCompareText(VarName, 'VARF_NFLINHA1') = 0 then
  begin
    Txt := MLAGeral.TrocaTexto(Dmod.QrControleNFLinha1.Value,
      '[TAXA_ADVAL]', QrLotADVAL_TXT.Value);
    Txt := MLAGeral.TrocaTexto(Txt, '[VALOR_DOC]',
      Geral.FFT(QrLotTotal.Value, 2, siPositivo));
    Txt := MLAGeral.TrocaTexto(Txt, '[NUM_BORDERO]',
      IntToStr(QrLotLote.Value));
    Txt := MLAGeral.TrocaTexto(Txt, '[IMPOSTO]',
      Geral.FFT(QrLotISS.Value, 2, siPositivo));
    Value := Txt;
  end
  else if AnsiCompareText(VarName, 'VARF_NFLINHA2') = 0 then
  begin
    Txt := MLAGeral.TrocaTexto(Dmod.QrControleNFLinha2.Value,
      '[TAXA_ADVAL]', QrLotADVAL_TXT.Value);
    Txt := MLAGeral.TrocaTexto(Txt, '[VALOR_DOC]',
      Geral.FFT(QrLotTotal.Value, 2, siPositivo));
    Txt := MLAGeral.TrocaTexto(Txt, '[NUM_BORDERO]',
      IntToStr(QrLotLote.Value));
    Txt := MLAGeral.TrocaTexto(Txt, '[IMPOSTO]',
      Geral.FFT(QrLotISS.Value, 2, siPositivo));
    Value := Txt;
  end
  else if AnsiCompareText(VarName, 'VARF_NFLINHA3') = 0 then
  begin
    Txt := MLAGeral.TrocaTexto(Dmod.QrControleNFLinha3.Value,
      '[TAXA_ADVAL]', QrLotADVAL_TXT.Value);
    Txt := MLAGeral.TrocaTexto(Txt, '[VALOR_DOC]',
      Geral.FFT(QrLotTotal.Value, 2, siPositivo));
    Txt := MLAGeral.TrocaTexto(Txt, '[NUM_BORDERO]',
      IntToStr(QrLotLote.Value));
    Txt := MLAGeral.TrocaTexto(Txt, '[IMPOSTO]',
      Geral.FFT(QrLotISS.Value, 2, siPositivo));
    Value := Txt;
  end
  else if AnsiCompareText(VarName, 'IMPOSTO') = 0 then
  begin
    Txt := MLAGeral.TrocaTexto(Dmod.QrControleNFAliq.Value, '[IMPOSTO]',
      Geral.FFT(QrLotISS.Value, 2, siPositivo));
    Value := Txt;
  end
  else if AnsiCompareText(VarName, 'AlturaInicial') = 0 then
  begin
    Value := Int(Dmod.QrControleNF_Cabecalho.Value / VAR_frCM);//37.7953);
  end
  else if AnsiCompareText(VarName, 'VARF_MEUENDERECO') = 0 then
  begin
    if FMEU_ENDERECO then Value := DModG.QrDonoE_CUC.Value
    else Value := ' ';
  end
end;

procedure TFmNFs.QrNFsBeforeOpen(DataSet: TDataSet);
begin
  QrNFsCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmNFs.ReopenLot();
begin
  QrLot.Close;
  if QrNFsCodigo.Value > 0 then
  begin
{
    QrLot.Params[0].AsInteger := QrNFsCodigo.Value;
    QrLot. Open;
}
    UnDmkDAC_PF.AbreMySQLQuery0(QrLot, Dmod.MyDB, [
    'SELECT IF(en.Tipo=0, en.RazaoSocial, ',
    'en.Nome) NOMECLIENTE, ',
    'IF(en.Tipo=0, en.CNPJ, ',
    'en.CPF) CNPJCPF, lo.* ',
    'FROM ' + CO_TabLotA + ' lo ',
    'LEFT JOIN entidades en ON en.Codigo=lo.Cliente ',
    'WHERE lo.NF=' + Geral.FF0(QrNFsCodigo.Value),
    '']);
  end;
end;

procedure TFmNFs.BtEuroClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMBordero, BtEuro);
end;

procedure TFmNFs.AtualizaNF;
begin
{
  QrSum.Close;
  QrSum.Params[0].AsInteger := QrNFsCodigo.Value;
  QrSum. Open;
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrSum, Dmod.MyDB, [
  'SELECT SUM(MINAV) MINAV, SUM(ISS_Val) ISS_Val ',
  'FROM ' + CO_TabLotA,
  'WHERE NF=' + Geral.FF0(QrNFsCodigo.Value),
  '']);
  //
  DMod.QrUpd.SQL.Clear;
  DMod.QrUpd.SQL.Add('UPDATE nfs SET AdValor=:P0, ISS_Val=:P1');
  DMod.QrUpd.SQL.Add('WHERE Codigo=:P2');
  //
  DMod.QrUpd.Params[0].AsFloat   := QrSUMMINAV.Value;
  DMod.QrUpd.Params[1].AsFloat   := QrSUMISS_Val.Value;
  DMod.QrUpd.Params[2].AsInteger := QrNFsCodigo.Value;
  //
  DMod.QrUpd.ExecSQL;
  QrSum.Close;
end;

procedure TFmNFs.Adiona1Click(Sender: TObject);
begin
  try
    FmPrincipal.FLoteLoc := 0;
    Application.CreateForm(TFmLot0Loc, FmLot0Loc);
    FmLot0Loc.FFormCall := 2;
    FmLot0Loc.FNF := QrNFsCodigo.Value;
    FmLot0Loc.ReopenLoc;
    FmLot0Loc.ShowModal;
  finally
    Screen.Cursor := crDefault;
  end;
  FmLot0Loc.Destroy;
  Screen.Cursor := crHourGlass;
  try
    AtualizaNF;
    LocCod(QrNFsCodigo.Value, QrNFsCodigo.Value);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmNFs.Retira1Click(Sender: TObject);
begin
  if Application.MessageBox('Confirma a retirada do Border� selecionado desta '+
  'Nota Fiscal?', 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    try
{
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE lot es SET AlterWeb=1,  NF=0 WHERE Codigo=:P0');
      Dmod.QrUpd.Params[0].AsInteger := QrLotCodigo.Value;
      Dmod.QrUpd.ExecSQL;
}
      if DmLot.LOT_Ins_Upd(Dmod.QrUpd, stUpd, False, [
      'NF'], ['Codigo'], [0], [QrLotCodigo.Value], True) then
      begin
        AtualizaNF;
        LocCod(QrNFsCodigo.Value, QrNFsCodigo.Value);
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmNFs.ImprimeNF(MeuEndereco: Boolean);
begin
  FMEU_ENDERECO := MeuEndereco;
  GOTOy.EnderecoDeEntidade(QrLotCliente.Value, 0);
  MyObjects.frxMostra(frxNF_Dados, 'Nota Fiscal n� '+IntToStr(QrLotNF.Value));
end;

end.

