object FmEvolucap2: TFmEvolucap2
  Left = 335
  Top = 176
  Caption = 'CAP-EVOLU-002 :: Evolu'#231#227'o do Capital (2)'
  ClientHeight = 540
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 92
    Width = 784
    Height = 448
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Dados'
      object Panel1: TPanel
        Left = 0
        Top = 346
        Width = 776
        Height = 74
        Align = alBottom
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object PnEditSdoCC: TPanel
          Left = 0
          Top = 0
          Width = 776
          Height = 74
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          Visible = False
          object Label3: TLabel
            Left = 4
            Top = 6
            Width = 69
            Height = 13
            Caption = '$ Saldo conta:'
          end
          object Label6: TLabel
            Left = 88
            Top = 6
            Width = 67
            Height = 13
            Caption = '$ Saldo caixa:'
          end
          object BitBtn1: TBitBtn
            Tag = 14
            Left = 186
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Sai da janela atual'
            Caption = '&Confirma'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BitBtn1Click
          end
          object EdSaldoConta: TdmkEdit
            Left = 4
            Top = 23
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object BitBtn2: TBitBtn
            Tag = 15
            Left = 282
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Sai da janela atual'
            Caption = '&Desiste'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = BitBtn2Click
          end
          object EdSaldoCaixa: TdmkEdit
            Left = 88
            Top = 23
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
        end
        object PnAtualiza: TPanel
          Left = 0
          Top = 0
          Width = 776
          Height = 74
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object GroupBox2: TGroupBox
            Left = 0
            Top = 0
            Width = 776
            Height = 64
            Align = alTop
            Caption = ' Atualizar o gr'#225'fico: '
            TabOrder = 0
            Visible = False
            object Label4: TLabel
              Left = 8
              Top = 20
              Width = 93
              Height = 13
              Caption = 'Data compra inicial:'
            end
            object Label5: TLabel
              Left = 112
              Top = 20
              Width = 86
              Height = 13
              Caption = 'Data compra final:'
            end
            object TPIniB: TDateTimePicker
              Left = 8
              Top = 36
              Width = 101
              Height = 21
              Date = 38675.714976851900000000
              Time = 38675.714976851900000000
              TabOrder = 0
            end
            object TPFimB: TDateTimePicker
              Left = 112
              Top = 36
              Width = 101
              Height = 21
              Date = 38675.714976851900000000
              Time = 38675.714976851900000000
              TabOrder = 1
            end
            object BtGrafico: TBitBtn
              Tag = 262
              Left = 300
              Top = 16
              Width = 90
              Height = 40
              Caption = 'Atuali&za'
              NumGlyphs = 2
              TabOrder = 2
              OnClick = BtGraficoClick
            end
            object RGIntervalo: TRadioGroup
              Left = 218
              Top = 8
              Width = 75
              Height = 54
              Caption = ' Intervalo: '
              ItemIndex = 0
              Items.Strings = (
                'Di'#225'rio'
                'Mensal')
              TabOrder = 3
            end
          end
        end
      end
      object PnDados: TPanel
        Left = 0
        Top = 0
        Width = 776
        Height = 346
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 1
        object GroupBox1: TGroupBox
          Left = 0
          Top = 0
          Width = 776
          Height = 64
          Align = alTop
          Caption = ' Atualizar os dados do per'#237'odo: '
          TabOrder = 0
          object Label34: TLabel
            Left = 8
            Top = 20
            Width = 93
            Height = 13
            Caption = 'Data compra inicial:'
          end
          object Label1: TLabel
            Left = 112
            Top = 20
            Width = 86
            Height = 13
            Caption = 'Data compra final:'
          end
          object Label2: TLabel
            Left = 324
            Top = 20
            Width = 86
            Height = 13
            Caption = 'Coligado especial:'
            Visible = False
          end
          object TPIniA: TDateTimePicker
            Left = 8
            Top = 36
            Width = 101
            Height = 21
            Date = 38675.714976851900000000
            Time = 38675.714976851900000000
            TabOrder = 0
          end
          object TPFimA: TDateTimePicker
            Left = 112
            Top = 36
            Width = 101
            Height = 21
            Date = 38675.714976851900000000
            Time = 38675.714976851900000000
            TabOrder = 1
          end
          object EdColigado: TdmkEditCB
            Left = 324
            Top = 36
            Width = 52
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            Visible = False
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            DBLookupComboBox = CBColigado
            IgnoraDBLookupComboBox = False
          end
          object CBColigado: TdmkDBLookupComboBox
            Left = 378
            Top = 36
            Width = 327
            Height = 21
            KeyField = 'Codigo'
            ListField = 'NOMECOLIGADO'
            ListSource = DsColigado
            TabOrder = 3
            Visible = False
            dmkEditCB = EdColigado
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object BtOK: TBitBtn
            Tag = 14
            Left = 216
            Top = 16
            Width = 90
            Height = 40
            Caption = '&Atualiza'
            NumGlyphs = 2
            TabOrder = 4
            OnClick = BtOKClick
          end
          object CkMoroso: TCheckBox
            Left = 712
            Top = 36
            Width = 61
            Height = 17
            Caption = 'Moroso'
            TabOrder = 5
            Visible = False
          end
        end
        object ProgressBar1: TProgressBar
          Left = 0
          Top = 329
          Width = 776
          Height = 17
          Align = alBottom
          TabOrder = 1
          Visible = False
        end
        object DBGrid1: TDBGrid
          Left = 0
          Top = 64
          Width = 776
          Height = 265
          Align = alClient
          DataSource = DsEvolucap2
          TabOrder = 2
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDblClick = DBGrid1DblClick
          OnKeyDown = DBGrid1KeyDown
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Gr'#225'fico'
      ImageIndex = 1
    end
    object TabSheet3: TTabSheet
      Caption = 'Cheques devolvidos orf'#227'os'
      ImageIndex = 2
      object DBGrid2: TDBGrid
        Left = 0
        Top = 0
        Width = 776
        Height = 372
        Align = alClient
        DataSource = DsCHNull
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
      object Panel2: TPanel
        Left = 0
        Top = 372
        Width = 776
        Height = 48
        Align = alBottom
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 1
        object BtExclui1: TBitBtn
          Tag = 12
          Left = 11
          Top = 4
          Width = 96
          Height = 40
          Cursor = crHandPoint
          Hint = 'Exclui entidade atual'
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtExclui1Click
        end
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'Cheques devolvidos precocemente'
      ImageIndex = 3
      object DBGrid3: TDBGrid
        Left = 0
        Top = 0
        Width = 776
        Height = 372
        Align = alClient
        DataSource = DsCHPrecoce
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
      object Panel3: TPanel
        Left = 0
        Top = 372
        Width = 776
        Height = 48
        Align = alBottom
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 1
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 283
        Height = 32
        Caption = 'Evolu'#231#227'o do Capital (2)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 283
        Height = 32
        Caption = 'Evolu'#231#227'o do Capital (2)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 283
        Height = 32
        Caption = 'Evolu'#231#227'o do Capital (2)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 48
    Width = 784
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrEvolucap2: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEvolucap2CalcFields
    SQL.Strings = (
      'SELECT * FROM evolucap2'
      'ORDER BY DataE')
    Left = 532
    Top = 32
    object QrEvolucap2DataE: TDateField
      FieldName = 'DataE'
    end
    object QrEvolucap2CHAbeA: TFloatField
      FieldName = 'CHAbeA'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2CHAbeB: TFloatField
      FieldName = 'CHAbeB'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2CHAbeC: TFloatField
      FieldName = 'CHAbeC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2DUAbeA: TFloatField
      FieldName = 'DUAbeA'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2DUAbeB: TFloatField
      FieldName = 'DUAbeB'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2DUAbeC: TFloatField
      FieldName = 'DUAbeC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2CHDevA: TFloatField
      FieldName = 'CHDevA'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2CHDevB: TFloatField
      FieldName = 'CHDevB'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2CHDevC: TFloatField
      FieldName = 'CHDevC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2DUDevA: TFloatField
      FieldName = 'DUDevA'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2DUDevB: TFloatField
      FieldName = 'DUDevB'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2DUDevC: TFloatField
      FieldName = 'DUDevC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2CHPgdA: TFloatField
      FieldName = 'CHPgdA'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2CHPgdB: TFloatField
      FieldName = 'CHPgdB'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2CHPgdC: TFloatField
      FieldName = 'CHPgdC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2DUPgdA: TFloatField
      FieldName = 'DUPgdA'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2DUPgdB: TFloatField
      FieldName = 'DUPgdB'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2DUPgdC: TFloatField
      FieldName = 'DUPgdC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2CHMorA: TFloatField
      FieldName = 'CHMorA'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2CHMorB: TFloatField
      FieldName = 'CHMorB'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2CHMorC: TFloatField
      FieldName = 'CHMorC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2DUMorA: TFloatField
      FieldName = 'DUMorA'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2DUMorB: TFloatField
      FieldName = 'DUMorB'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2DUMorC: TFloatField
      FieldName = 'DUMorC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2CHPgmA: TFloatField
      FieldName = 'CHPgmA'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2CHPgmB: TFloatField
      FieldName = 'CHPgmB'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2CHPgmC: TFloatField
      FieldName = 'CHPgmC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2DUPgmA: TFloatField
      FieldName = 'DUPgmA'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2DUPgmB: TFloatField
      FieldName = 'DUPgmB'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2DUPgmC: TFloatField
      FieldName = 'DUPgmC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsEvolucap2: TDataSource
    DataSet = QrEvolucap2
    Left = 560
    Top = 32
  end
  object QrColigado: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMECOLIGADO'
      'FROM entidades'
      'WHERE Fornece2="V"')
    Left = 724
    Top = 5
    object QrColigadoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrColigadoNOMECOLIGADO: TWideStringField
      FieldName = 'NOMECOLIGADO'
      Size = 100
    end
  end
  object DsColigado: TDataSource
    DataSet = QrColigado
    Left = 752
    Top = 5
  end
  object QrCHDevolAbe: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Valor+JurosV-ValPago) Valor'
      'FROM alinits'
      'WHERE Data1 < :P0'
      'AND ((Data3=0) OR (Data3 >:P1)) ')
    Left = 616
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrCHDevolAbeValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrEvolper: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEvolperCalcFields
    SQL.Strings = (
      'SELECT * FROM evolucap2'
      'WHERE DataE BETWEEN :P0 AND :P1'
      'AND MONTH(DataE) <> MONTH(DATE_ADD(DataE, INTERVAL 1 DAY))'
      'ORDER BY DataE')
    Left = 588
    Top = 32
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrEvolperCARTEIREAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CARTEIREAL'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrEvolperVALOR_SUB_T: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VALOR_SUB_T'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrEvolperVALOR_TOTAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VALOR_TOTAL'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrEvolperDataE: TDateField
      FieldName = 'DataE'
    end
    object QrEvolperCHAbeA: TFloatField
      FieldName = 'CHAbeA'
    end
    object QrEvolperCHAbeB: TFloatField
      FieldName = 'CHAbeB'
    end
    object QrEvolperCHAbeC: TFloatField
      FieldName = 'CHAbeC'
    end
    object QrEvolperDUAbeA: TFloatField
      FieldName = 'DUAbeA'
    end
    object QrEvolperDUAbeB: TFloatField
      FieldName = 'DUAbeB'
    end
    object QrEvolperDUAbeC: TFloatField
      FieldName = 'DUAbeC'
    end
    object QrEvolperCHDevA: TFloatField
      FieldName = 'CHDevA'
    end
    object QrEvolperCHDevB: TFloatField
      FieldName = 'CHDevB'
    end
    object QrEvolperCHDevC: TFloatField
      FieldName = 'CHDevC'
    end
    object QrEvolperDUDevA: TFloatField
      FieldName = 'DUDevA'
    end
    object QrEvolperDUDevB: TFloatField
      FieldName = 'DUDevB'
    end
    object QrEvolperDUDevC: TFloatField
      FieldName = 'DUDevC'
    end
    object QrEvolperCHPgdA: TFloatField
      FieldName = 'CHPgdA'
    end
    object QrEvolperCHPgdB: TFloatField
      FieldName = 'CHPgdB'
    end
    object QrEvolperCHPgdC: TFloatField
      FieldName = 'CHPgdC'
    end
    object QrEvolperDUPgdA: TFloatField
      FieldName = 'DUPgdA'
    end
    object QrEvolperDUPgdB: TFloatField
      FieldName = 'DUPgdB'
    end
    object QrEvolperDUPgdC: TFloatField
      FieldName = 'DUPgdC'
    end
    object QrEvolperCHMorA: TFloatField
      FieldName = 'CHMorA'
    end
    object QrEvolperCHMorB: TFloatField
      FieldName = 'CHMorB'
    end
    object QrEvolperCHMorC: TFloatField
      FieldName = 'CHMorC'
    end
    object QrEvolperDUMorA: TFloatField
      FieldName = 'DUMorA'
    end
    object QrEvolperDUMorB: TFloatField
      FieldName = 'DUMorB'
    end
    object QrEvolperDUMorC: TFloatField
      FieldName = 'DUMorC'
    end
    object QrEvolperCHPgmA: TFloatField
      FieldName = 'CHPgmA'
    end
    object QrEvolperCHPgmB: TFloatField
      FieldName = 'CHPgmB'
    end
    object QrEvolperCHPgmC: TFloatField
      FieldName = 'CHPgmC'
    end
    object QrEvolperDUPgmA: TFloatField
      FieldName = 'DUPgmA'
    end
    object QrEvolperDUPgmB: TFloatField
      FieldName = 'DUPgmB'
    end
    object QrEvolperDUPgmC: TFloatField
      FieldName = 'DUPgmC'
    end
  end
  object QrMax: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MAX(CarteiTudo+CHDevolAbe+DUDevolAbe+SaldoConta) TOTAL'
      'FROM evolucap2'
      'WHERE DataE BETWEEN :P0 AND :P1')
    Left = 616
    Top = 32
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrMaxTOTAL: TFloatField
      FieldName = 'TOTAL'
    end
  end
  object QrCHNull: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrCHNullAfterOpen
    BeforeClose = QrCHNullBeforeClose
    SQL.Strings = (
      'SELECT ai.Cliente, ai.Emitente, ai.CPF, '
      'ai.Banco, ai.Agencia, ai.Conta, ai.Cheque, '
      'ai.Valor, ai.Taxas, ai.Multa, ai.JurosV Juros, '
      'ai.Desconto, ai.ValPago, ai.ChequeOrigem, '
      'ai.LoteOrigem, ai.Codigo'
      'FROM alinits ai'
      'LEFT JOIN lot esits li ON li.Controle=ai.ChequeOrigem'
      'WHERE ai.Data3=0'
      'AND li.Quitado IS NULL')
    Left = 140
    Top = 112
    object QrCHNullCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrCHNullEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrCHNullCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrCHNullBanco: TIntegerField
      FieldName = 'Banco'
      Required = True
    end
    object QrCHNullAgencia: TIntegerField
      FieldName = 'Agencia'
      Required = True
    end
    object QrCHNullConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrCHNullCheque: TIntegerField
      FieldName = 'Cheque'
      Required = True
    end
    object QrCHNullValor: TFloatField
      FieldName = 'Valor'
      Required = True
    end
    object QrCHNullTaxas: TFloatField
      FieldName = 'Taxas'
      Required = True
    end
    object QrCHNullMulta: TFloatField
      FieldName = 'Multa'
      Required = True
    end
    object QrCHNullJuros: TFloatField
      FieldName = 'Juros'
      Required = True
    end
    object QrCHNullDesconto: TFloatField
      FieldName = 'Desconto'
      Required = True
    end
    object QrCHNullValPago: TFloatField
      FieldName = 'ValPago'
      Required = True
    end
    object QrCHNullChequeOrigem: TIntegerField
      FieldName = 'ChequeOrigem'
      Required = True
    end
    object QrCHNullLoteOrigem: TIntegerField
      FieldName = 'LoteOrigem'
      Required = True
    end
    object QrCHNullCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object DsCHNull: TDataSource
    DataSet = QrCHNull
    Left = 168
    Top = 112
  end
  object QrCHPrecoce: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM alinits ai'
      'LEFT JOIN lct0001a li ON li.FatParcela=ai.ChequeOrigem'
      'WHERE li.DDeposito >= SYSDATE()'
      'AND ai.Data3=0')
    Left = 140
    Top = 140
  end
  object DsCHPrecoce: TDataSource
    DataSet = QrCHPrecoce
    Left = 168
    Top = 140
  end
  object QrDUVencido: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(li.Repassado = 0, 0, re.Coligado) Coligado,'
      'lo.Tipo, SUM(li.Valor) Valor'
      ''
      'FROM lot esits li'
      'LEFT JOIN lot es    lo ON lo.Codigo=li.Codigo'
      'LEFT JOIN repasits ri ON ri.Origem=li.Controle '
      'LEFT JOIN repas    re ON re.Codigo=ri.Codigo'
      'WHERE lo.Tipo=1'
      'AND li.DDeposito < :P0'
      'AND ((li.Data3 > :P1) OR (li.Data3 = 0))'
      'AND li.Quitado <> -3'
      ''
      'GROUP BY lo.Tipo, Coligado')
    Left = 644
    Top = 248
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrDUVencidoValor: TFloatField
      FieldName = 'Valor'
    end
    object QrDUVencidoColigado: TLargeintField
      FieldName = 'Coligado'
    end
    object QrDUVencidoTipo: TSmallintField
      FieldName = 'Tipo'
    end
  end
  object QrDUMoroso: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(li.Repassado = 0, 0, re.Coligado) Coligado,'
      'lo.Tipo, SUM(li.Valor) Valor'
      ''
      'FROM lot esits li'
      'LEFT JOIN lot es    lo ON lo.Codigo=li.Codigo'
      'LEFT JOIN repasits ri ON ri.Origem=li.Controle '
      'LEFT JOIN repas    re ON re.Codigo=ri.Codigo'
      'WHERE lo.Tipo=1'
      'AND li.DDeposito < :P0'
      'AND ((li.Data3 > :P1) OR (li.Data3 = 0))'
      'AND li.Quitado = -3'
      ''
      'GROUP BY lo.Tipo, Coligado')
    Left = 644
    Top = 276
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrDUMorosoColigado: TLargeintField
      FieldName = 'Coligado'
    end
    object QrDUMorosoTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrDUMorosoValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrDUVencPag: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(li.Repassado = 0, 0, re.Coligado) Coligado,'
      'lo.Tipo, SUM(ad.Pago) Valor'
      'FROM adup pgs ad'
      'LEFT JOIN lot esits  li ON li.Controle=ad.Lot esIts'
      'LEFT JOIN lot es     lo ON lo.Codigo=li.Codigo'
      'LEFT JOIN repasits ri ON ri.Origem=li.Controle '
      'LEFT JOIN repas    re ON re.Codigo=ri.Codigo'
      'WHERE lo.Tipo=1'
      'AND li.DDeposito < :P0'
      'AND ((li.Data3 > :P1) OR (li.Data3 = 0))'
      'AND li.Quitado <> -3'
      'AND ad.Data <= :P2'
      'GROUP BY lo.Tipo, Coligado')
    Left = 672
    Top = 248
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrDUVencPagColigado: TLargeintField
      FieldName = 'Coligado'
    end
    object QrDUVencPagTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrDUVencPagValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrDUMoroPag: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(li.Repassado = 0, 0, re.Coligado) Coligado,'
      'lo.Tipo, SUM(ad.Pago) Valor'
      'FROM adup pgs ad'
      'LEFT JOIN lot esits  li ON li.Controle=ad.Lot esIts'
      'LEFT JOIN lot es     lo ON lo.Codigo=li.Codigo'
      'LEFT JOIN repasits ri ON ri.Origem=li.Controle '
      'LEFT JOIN repas    re ON re.Codigo=ri.Codigo'
      'WHERE lo.Tipo=1'
      'AND li.DDeposito < :P0'
      'AND ((li.Data3 > :P1) OR (li.Data3 = 0))'
      'AND li.Quitado = -3'
      'AND ad.Data <= :P2'
      'GROUP BY lo.Tipo, Coligado')
    Left = 672
    Top = 276
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrDUMoroPagColigado: TLargeintField
      FieldName = 'Coligado'
    end
    object QrDUMoroPagTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrDUMoroPagValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrEmcart: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(li.Repassado = 0, 0, re.Coligado) Coligado,'
      'lo.Tipo, SUM(li.Valor) Valor'
      'FROM lot esits li'
      'LEFT JOIN lot es lo ON lo.Codigo=li.Codigo'
      'LEFT JOIN alinits ai ON ai.ChequeOrigem=li.Controle'
      'LEFT JOIN repasits ri ON ri.Origem=li.Controle '
      'LEFT JOIN repas    re ON re.Codigo=ri.Codigo'
      'WHERE li.DCompra <= :P1'
      'AND li.DDeposito >= :P2'
      'AND ai.ChequeOrigem IS NULL'
      'GROUP BY lo.Tipo, Coligado')
    Left = 644
    Top = 220
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrEmcartColigado: TLargeintField
      FieldName = 'Coligado'
    end
    object QrEmcartTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEmcartValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrCHVencido: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(li.Repassado = 0, 0, re.Coligado) Coligado,'
      'lo.Tipo, SUM(li.Valor) Valor'
      'FROM lot esits li'
      'LEFT JOIN lot es lo ON lo.Codigo=li.Codigo'
      'LEFT JOIN repasits  ri ON ri.Origem=li.Controle'
      'LEFT JOIN repas     re ON re.Codigo=ri.Codigo'
      'LEFT JOIN alinits   ai ON ai.ChequeOrigem=li.Controle'
      'WHERE lo.Tipo=0'
      'AND li.DCompra < :P0'
      'AND li.DDeposito < :P1'
      'AND ai.ChequeOrigem <> 0'
      'AND ( (ai.Data3 = 0) OR (ai.Data3 > :P2) )'
      'AND li.Quitado<>-3'
      'GROUP BY lo.Tipo, Coligado'
      '')
    Left = 644
    Top = 304
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrCHVencidoColigado: TLargeintField
      FieldName = 'Coligado'
    end
    object QrCHVencidoTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrCHVencidoValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrCHVencPag: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(li.Repassado = 0, 0, re.Coligado) Coligado,'
      'lo.Tipo, SUM(ap.Pago) Valor'
      'FROM lot esits li'
      'LEFT JOIN lot es     lo ON lo.Codigo=li.Codigo'
      'LEFT JOIN repasits  ri ON ri.Origem=li.Controle'
      'LEFT JOIN repas     re ON re.Codigo=ri.Codigo'
      'LEFT JOIN alinits   ai ON ai.ChequeOrigem=li.Controle'
      'LEFT JOIN alin pgs   ap ON ap.AlinIts=ai.Codigo'
      'WHERE lo.Tipo=0'
      'AND li.DCompra < :P0'
      'AND li.DDeposito < :P1'
      'AND ai.ChequeOrigem <> 0'
      'AND ( (ai.Data3 = 0) OR (ai.Data3 > :P2) )'
      'AND ap.Pago <> 0'
      'AND li.Quitado<>-3'
      'GROUP BY lo.Tipo, Coligado')
    Left = 672
    Top = 304
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrCHVencPagColigado: TLargeintField
      FieldName = 'Coligado'
    end
    object QrCHVencPagTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrCHVencPagValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrCHMoroso: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(li.Repassado = 0, 0, re.Coligado) Coligado,'
      'lo.Tipo, SUM(li.Valor) Valor'
      'FROM lot esits li'
      'LEFT JOIN lot es lo ON lo.Codigo=li.Codigo'
      'LEFT JOIN repasits  ri ON ri.Origem=li.Controle'
      'LEFT JOIN repas     re ON re.Codigo=ri.Codigo'
      'LEFT JOIN alinits   ai ON ai.ChequeOrigem=li.Controle'
      'WHERE lo.Tipo=0'
      'AND li.DCompra < :P0'
      'AND li.DDeposito < :P1'
      'AND ai.ChequeOrigem <> 0'
      'AND ( (ai.Data3 = 0) OR (ai.Data3 > :P2) )'
      'AND li.Quitado=-3'
      'GROUP BY lo.Tipo, Coligado'
      '')
    Left = 644
    Top = 332
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object LargeintField1: TLargeintField
      FieldName = 'Coligado'
    end
    object SmallintField1: TSmallintField
      FieldName = 'Tipo'
    end
    object FloatField1: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrCHMoroPag: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(li.Repassado = 0, 0, re.Coligado) Coligado,'
      'lo.Tipo, SUM(ap.Pago) Valor'
      'FROM lot esits li'
      'LEFT JOIN lot es     lo ON lo.Codigo=li.Codigo'
      'LEFT JOIN repasits  ri ON ri.Origem=li.Controle'
      'LEFT JOIN repas     re ON re.Codigo=ri.Codigo'
      'LEFT JOIN alinits   ai ON ai.ChequeOrigem=li.Controle'
      'LEFT JOIN alin pgs   ap ON ap.AlinIts=ai.Codigo'
      'WHERE lo.Tipo=0'
      'AND li.DCompra < :P0'
      'AND li.DDeposito < :P1'
      'AND ai.ChequeOrigem <> 0'
      'AND ( (ai.Data3 = 0) OR (ai.Data3 > :P2) )'
      'AND ap.Pago <> 0'
      'AND li.Quitado=-3'
      'GROUP BY lo.Tipo, Coligado')
    Left = 672
    Top = 332
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object LargeintField2: TLargeintField
      FieldName = 'Coligado'
    end
    object SmallintField2: TSmallintField
      FieldName = 'Tipo'
    end
    object FloatField2: TFloatField
      FieldName = 'Valor'
    end
  end
end
