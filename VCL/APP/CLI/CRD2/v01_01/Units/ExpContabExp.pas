unit ExpContabExp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, ComCtrls, Db, mySQLDbTables, ABSMain,
  Grids, DBGrids, dmkGeral, dmkImage, DBCtrls, dmkDBLookupComboBox, dmkEdit,
  dmkEditCB, DmkDAC_PF, UnDmkEnums;

type
  TFmExpContabExp = class(TForm)
    QrPagtos: TmySQLQuery;
    QrConfig: TmySQLQuery;
    QrConfigDiaI: TIntegerField;
    QrConfigDiaF: TIntegerField;
    QrConfigDiaP: TWideStringField;
    QrConfigMesI: TIntegerField;
    QrConfigMesF: TIntegerField;
    QrConfigMesP: TWideStringField;
    QrConfigDevI: TIntegerField;
    QrConfigDevF: TIntegerField;
    QrConfigDevP: TWideStringField;
    QrConfigCreI: TIntegerField;
    QrConfigCreF: TIntegerField;
    QrConfigCreP: TWideStringField;
    QrConfigComI: TIntegerField;
    QrConfigComF: TIntegerField;
    QrConfigComP: TWideStringField;
    QrConfigValI: TIntegerField;
    QrConfigValF: TIntegerField;
    QrConfigValP: TWideStringField;
    QrConfigDocI: TIntegerField;
    QrConfigDocF: TIntegerField;
    QrConfigDocP: TWideStringField;
    QrPagtosDocumento: TFloatField;
    QrPagtosNF: TIntegerField;
    QrPagtosData: TDateField;
    QrPagtosTipo: TSmallintField;
    QrPagtosCREDORA: TWideStringField;
    QrPagtosDEVEDORA: TWideStringField;
    QrPagtosDIA: TLargeintField;
    QrPagtosMES: TLargeintField;
    QrPagtosDebito: TFloatField;
    QrDeposi: TmySQLQuery;
    QrConfigCartPadr: TIntegerField;
    QrConfigContab: TWideStringField;
    QrImpost: TmySQLQuery;
    QrImpostDIA: TLargeintField;
    QrImpostMES: TLargeintField;
    QrImpostNF: TIntegerField;
    QrImpostIOC_VAL: TFloatField;
    QrImpostMINTC: TFloatField;
    QrImpostMINAV: TFloatField;
    QrImpostDEVEDORA: TWideStringField;
    SaveDialog1: TSaveDialog;
    QrImpostCodigo: TIntegerField;
    QrPagtosControle: TIntegerField;
    QrImpostIOFd_VAL: TFloatField;
    QrImpostIOFv_VAL: TFloatField;
    QrImpostIOF_TOT: TFloatField;
    QrPagtosFatNum: TFloatField;
    QrPesq1: TmySQLQuery;
    QrPesq1CODIGO: TIntegerField;
    QrPesq1NOME: TWideStringField;
    Query: TABSQuery;
    Source: TDataSource;
    QueryCodigo: TIntegerField;
    QueryNome: TWideStringField;
    QueryTabela: TWideStringField;
    QrDepErr: TmySQLQuery;
    DsDepErr: TDataSource;
    QrDepErrCliente: TIntegerField;
    QrDepErrCartDep: TIntegerField;
    QrDepErrNOMRCART: TWideStringField;
    QrDepErrCONTAB_ENTI: TWideStringField;
    QrDepErrCONTAB_CART: TWideStringField;
    QrDepErrNOMECLI: TWideStringField;
    QrTaxErr: TmySQLQuery;
    DsTaxErr: TDataSource;
    QrPagErr: TmySQLQuery;
    QrPagErrCodigo: TIntegerField;
    QrPagErrDebito: TFloatField;
    QrPagErrDocumento: TFloatField;
    QrPagErrNF: TIntegerField;
    QrPagErrData: TDateField;
    QrPagErrCliente: TIntegerField;
    QrPagErrControle: TIntegerField;
    QrPagErrCREDORA: TWideStringField;
    QrPagErrDEVEDORA: TWideStringField;
    QrPagErrNOMEENT: TWideStringField;
    DsPagErr: TDataSource;
    Panel4: TPanel;
    GroupBox1: TGroupBox;
    Panel3: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    TPIni: TDateTimePicker;
    TPFim: TDateTimePicker;
    CkPag: TCheckBox;
    CkDep: TCheckBox;
    CkImp: TCheckBox;
    CkAdV: TCheckBox;
    CkFaC: TCheckBox;
    CkIOF: TCheckBox;
    CkChe: TCheckBox;
    CkDup: TCheckBox;
    GroupBox2: TGroupBox;
    CkTeste: TCheckBox;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel6: TPanel;
    LbSeq: TListBox;
    Memo1: TMemo;
    TabSheet2: TTabSheet;
    Panel7: TPanel;
    DBGrid1: TDBGrid;
    TabSheet4: TTabSheet;
    PageControl2: TPageControl;
    TabSheet5: TTabSheet;
    Panel10: TPanel;
    DBGrid4: TDBGrid;
    TabSheet6: TTabSheet;
    Panel8: TPanel;
    DBGrid2: TDBGrid;
    TabSheet7: TTabSheet;
    Panel9: TPanel;
    DBGrid3: TDBGrid;
    GBAvisos1: TGroupBox;
    Panel1: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BitBtn1: TBitBtn;
    Panel5: TPanel;
    BtGera: TBitBtn;
    BtSalva: TBitBtn;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Progress: TProgressBar;
    PainelA: TPanel;
    Label9: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    Label3: TLabel;
    Label4: TLabel;
    QrDeposiDIA0: TLargeintField;
    QrDeposiMES0: TLargeintField;
    QrDeposiDIA1: TLargeintField;
    QrDeposiMES1: TLargeintField;
    QrDeposiDocumento: TFloatField;
    QrDeposiCredito: TFloatField;
    QrDeposiValDeposito: TFloatField;
    QrDeposiNF: TIntegerField;
    QrDeposiCREDORA: TWideStringField;
    QrDeposiTipo: TSmallintField;
    QrDeposiDEVEDORA: TWideStringField;
    QrDeposiFatNum: TFloatField;
    QrDeposiFatParcela: TIntegerField;
    QrDepErrFatNum: TFloatField;
    QrDepErrFatParcela: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtGeraClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtSalvaClick(Sender: TObject);
    procedure TPIniChange(Sender: TObject);
    procedure TPFimChange(Sender: TObject);
    procedure CkTesteClick(Sender: TObject);
    procedure CkImpClick(Sender: TObject);
    procedure QrImpostCalcFields(DataSet: TDataSet);
    procedure EdEmpresaChange(Sender: TObject);
  private
    { Private declarations }
    FATID_0302_Txt: String;
    FCriou: Boolean;
    FItens: Integer;
    //FEmpresa, FEntidade_TXT,
    FCliInt, FEntidade: Integer;
    FTabLctA, FTabLctB, FTabLctD,
    FTabLotA, FTabLotB, FTabLotD: String;
    FDtEncer, FDtMorto: TDateTime;
    procedure AvisaFaltaDeCodigoContab(DtI, DtF: TDateTime; DataI, DataF: String; Ch, Du: Integer);
    procedure ReopenPagtos(DtI, DtF: TDateTime; DataI, DataF: String; Ch, Du: Integer);
    procedure ReopenPagErr(DtI, DtF: TDateTime; DataI, DataF: String; Ch, Du: Integer);
  public
    { Public declarations }
    procedure ConfigNovaPesq;
  end;

  var
  FmExpContabExp: TFmExpContabExp;

implementation

{$R *.DFM}

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, ModuleGeral, UnDmkABS_PF;

procedure TFmExpContabExp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmExpContabExp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
end;

procedure TFmExpContabExp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmExpContabExp.BtGeraClick(Sender: TObject);
var
  Dia, Mes1, Mes2, Ano1, Ano2: Word;
  DataI, DataF, Texto, Txt, Cpl, Sub: String;
  DiaI, DiaF, MesI, MesF, DevI, DevF, CreI, CreF,
  ComI, ComF, ValI, ValF, DocI, DocF,
  i, t, Its, Ini, Fim, Itens, Ch, Du: Integer;
begin
  if MyObjects.FIC(FEntidade = 0, EdEmpresa, 'Informe a empresa!') then Exit;
  //
  QrPagErr.Close;
  QrDepErr.Close;
  QrTaxErr.Close;
  //
  FCriou := False;
  FItens := 0;
  if CkChe.Checked then Ch := 0 else Ch := -10000;
  if CkDup.Checked then Du := 1 else Du := -10000;
  BtSalva.Enabled := False;
  Memo1.Lines.Clear;
  LbSeq.Items.Clear;
  LbSeq.Sorted := False;
  DecodeDate(TPIni.Date, Ano1, Mes1, Dia);
  DecodeDate(TPFim.Date, Ano2, Mes2, Dia);
  if ((Ano1 * 100) + Mes1) <> ((Ano2 * 100) + Mes2) then
  begin
    Geral.MensagemBox('Data inicial e final devem pertencer ao ' +
    'mesmo m�s e mesmo ano!', 'Aviso!', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  try
    t := 0;
    //
    if (QrConfigDiaI.Value <> 0) and (QrConfigDiaF.Value <> 0) then
    begin
      DiaI := QrConfigDiaI.Value;
      DiaF := QrConfigDiaI.Value + QrConfigDiaF.Value - 1;
      if DiaF > t then t := DiaF;
      LbSeq.Items.Add(FormatFloat('00000000', DiaI) +  ' ' +
                      FormatFloat('00000000', DiaF) + ' 001');
    end;
    //
    if (QrConfigMesI.Value <> 0) and (QrConfigMesF.Value <> 0) then
    begin
      MesI := QrConfigMesI.Value;
      MesF := QrConfigMesI.Value + QrConfigMesF.Value - 1;
      if MesF > t then t := MesF;
      LbSeq.Items.Add(FormatFloat('00000000', MesI) + ' ' +
                      FormatFloat('00000000', MesF) + ' 002');
    end;
    //
    if (QrConfigDevI.Value <> 0) and (QrConfigDevF.Value <> 0) then
    begin
      DevI := QrConfigDevI.Value;
      DevF := QrConfigDevI.Value + QrConfigDevF.Value - 1;
      if DevF > t then t := DevF;
      LbSeq.Items.Add(FormatFloat('00000000', DevI) +  ' ' +
                      FormatFloat('00000000', DevF) + ' 003');
    end;
    //
    if (QrConfigCreI.Value <> 0) and (QrConfigCreF.Value <> 0) then
    begin
      CreI := QrConfigCreI.Value;
      CreF := QrConfigCreI.Value + QrConfigCreF.Value - 1;
      if CreF > t then t := CreF;
      LbSeq.Items.Add(FormatFloat('00000000', CreI) +  ' ' +
                      FormatFloat('00000000', CreF) + ' 004');
    end;
    //
    if (QrConfigComI.Value <> 0) and (QrConfigComF.Value <> 0) then
    begin
      ComI := QrConfigComI.Value;
      ComF := QrConfigComI.Value + QrConfigComF.Value - 1;
      if ComF > t then t := ComF;
      LbSeq.Items.Add(FormatFloat('00000000', ComI) +  ' ' +
                      FormatFloat('00000000', ComF) + ' 005');
    end;
    //
    if (QrConfigValI.Value <> 0) and (QrConfigValF.Value <> 0) then
    begin
      ValI := QrConfigValI.Value;
      ValF := QrConfigValI.Value + QrConfigValF.Value - 1;
      if ValF > t then t := ValF;
      LbSeq.Items.Add(FormatFloat('00000000', ValI) +  ' ' +
                      FormatFloat('00000000', ValF) + ' 006');
    end;
    //
    if (QrConfigDocI.Value <> 0) and (QrConfigDocF.Value <> 0) then
    begin
      DocI := QrConfigDocI.Value;
      DocF := QrConfigDocI.Value + QrConfigDocF.Value - 1;
      if DocF > t then t := DocF;
      LbSeq.Items.Add(FormatFloat('00000000', DocI) +  ' ' +
                      FormatFloat('00000000', DocF) + ' 007');
    end;
    LbSeq.Sorted := True;
    //
    if t > 0 then Texto := ''
    else begin
      Geral.MensagemBox('N�o h� configura��o para exporta��o!', 'Aviso',
      MB_OK+MB_ICONWARNING);
      Screen.Cursor := crDefault;
      Exit;
    end;
    DataI := Geral.FDT(TPIni.Date, 1);
    DataF := Geral.FDT(TPFim.Date, 1);
    Itens := 0;
    //
    AvisaFaltaDeCodigoContab(TPIni.Date, TPFim.Date, DataI, DataF, Ch, Du);
    //
    QrPagtos.Close;
    if CkPag.Checked then
    begin
      // mudei 2011-11-05
      ReopenPagtos(TPIni.Date, TPFim.Date, DataI, DataF, Ch, Du);
      {
      QrPagtos.Params[0].AsString  := DataI;
      QrPagtos.Params[1].AsString  := DataF;
      QrPagtos.Params[2].AsInteger := Ch;
      QrPagtos.Params[3].AsInteger := Du;
      QrPagtos. Open;
      }
      Itens := Itens + QrPagtos.RecordCount;
    end;
    //
    QrDeposi.Close;
    if CkDep.Checked then
    begin
{
      QrDeposi.Params[0].AsString  := DataI;
      QrDeposi.Params[1].AsString  := DataF;
      QrDeposi.Params[2].AsString  := DataI;
      QrDeposi.Params[3].AsString  := DataF;
      QrDeposi.Params[4].AsInteger := Ch;
      QrDeposi.Params[5].AsInteger := Du;
      QrDeposi. Open;
}
      UnDmkDAC_PF.AbreMySQLQuery0(QrDeposi, Dmod.MyDB, [
      'SELECT DAY(li.DDeposito) DIA0, MONTH(li.DDeposito) MES0, ',
      'DAY(li.Data3) DIA1, MONTH(li.Data3) MES1, li.Documento, ',
      'li.Credito, li.ValDeposito, lo.NF, cl.Contab CREDORA, ',
      'lo.Tipo, ca.Contab DEVEDORA, li.FatNum, li.FatParcela ',
      'FROM ' + FTabLctA + ' li ',
      'LEFT JOIN ' + FTabLotA + ' lo ON lo.Codigo=li.FatNum ',
      'LEFT JOIN entidades cl ON cl.Codigo=lo.Cliente ',
      'LEFT JOIN carteiras ca ON ca.Codigo=li.CartDep ',
      'WHERE li.FatID=' + FormatFloat('0', VAR_FATID_0301),
      'AND lo.Codigo > 0 ',
      'AND (lo.TxCompra + lo.ValValorem)>=0.01 ',
      'AND ((lo.Tipo=0 AND li.DDeposito BETWEEN "' +
      DataI + '" AND "' + DataF + '") ',
      'OR (lo.Tipo=1 AND (li.Data3 BETWEEN "' +
      DataI + '" AND "' + DataF + '"))) ',
      'AND lo.Tipo IN (' + FormatFloat('0', Ch) +
      ', ' + FormatFloat('0', Du) + ') ',
      'ORDER BY lo.Data, lo.NF ',
      '']);
      Itens := Itens + QrDeposi.RecordCount;
    end;
    //
    QrImpost.Close;
    if CkImp.Checked then
    begin
{
      QrImpost.Params[0].AsString  := DataI;
      QrImpost.Params[1].AsString  := DataF;
      QrImpost.Params[2].AsInteger := Ch;
      QrImpost.Params[3].AsInteger := Du;
      QrImpost. Open;
}
      UnDmkDAC_PF.AbreMySQLQuery0(QrImpost, Dmod.MyDB, [
      'SELECT DAY(lo.Data) DIA, MONTH(lo.Data) MES, lo.NF, ',
      'lo.MINTC, lo.MINAV, en.Contab DEVEDORA, lo.Codigo, ',
      'lo.IOC_VAL, lo.IOFd_VAL, lo.IOFv_VAL ',
      'FROM ' + FTabLotA + ' lo ',
      'LEFT JOIN entidades en ON en.Codigo=lo.Cliente ',
      'WHERE lo.data BETWEEN "' +
      DataI + '" AND "' + DataF + '" ',
      'AND (TxCompra + ValValorem) >= 0.01 ',
      'AND lo.Tipo IN (' + FormatFloat('0', Ch) +
      ', ' + FormatFloat('0', Du) + ') ',
      'ORDER BY lo.Data, lo.NF ',
      '']);
      //
      if CkIOF.Checked then Itens := Itens + QrImpost.RecordCount;
      if CkFaC.Checked then Itens := Itens + QrImpost.RecordCount;
      if CkAdV.Checked then Itens := Itens + QrImpost.RecordCount;
    end;
    //
    Progress.Position := 0;
    Progress.Visible := True;
    Progress.Max := Itens;

    ////////////////////////////////////////////////////////////////////////////

    if CkPag.Checked then
    begin
      QrPagtos.First;
      while not QrPagtos.Eof do
      begin
        Progress.Position := Progress.Position + 1;
        Progress.Update;
        Application.ProcessMessages;
        //
        Texto := '';
        for i := 0 to LbSeq.Items.Count - 1 do
        begin
          Ini := StrToInt(Copy(LbSeq.Items[i], 01, 8));
          Fim := StrToInt(Copy(LbSeq.Items[i], 10, 8));
          Its := StrToInt(Copy(LbSeq.Items[i], 19, 3));
          case Its of
            1: Txt := IntToStr(QrPagtosDIA.Value);
            2: Txt := IntToStr(QrPagtosMES.Value);
            3: Txt := QrPagtosDEVEDORA.Value;
            4: Txt := QrPagtosCREDORA.Value;
            5:
            begin
              case QrPagtosTipo.Value of
                0: Sub := 'cheque';
                1: Sub := 'duplicata'
                else Sub := '???';
              end;
              Txt := 'Pagto desconto '+Sub+' ref. NF '+ FormatFloat('000000',
                QrPagtosNF.Value);
            end;
            6: Txt := Geral.SoNumero_TT(Geral.FFT(QrPagtosDebito.Value, 2,
              siPositivo));
            7:
            begin
              if QrPagtosDocumento.Value = 0 then Txt := '' else
              Txt := FloatToStr(QrPagtosDocumento.Value);
            end;
            else Txt := '';
          end;
          case Its of
            1: Cpl := QrConfigDiaP.Value;
            2: Cpl := QrConfigMesP.Value;
            3: Cpl := QrConfigDevP.Value;
            4: Cpl := QrConfigCreP.Value;
            5: Cpl := QrConfigComP.Value;
            6: Cpl := QrConfigValP.Value;
            7: Cpl := QrConfigDocP.Value;
            else Cpl := ' ';
          end;
          if cpl = '' then Cpl := ' ';
          if cpl = '0' then
            Txt := Geral.CompletaString(Txt, Cpl, Fim - Ini + 1, taRightJustify, True)
          else
            Txt := Geral.CompletaString(Txt, Cpl, Fim - Ini + 1, taLeftJustify, True);
          while Length(Texto) < Ini-1 do Texto := Texto + ' ';
          Texto := Texto + Txt;
        end;
        if CkTeste.Checked then
        begin
          Texto := Texto + ' Lote '+
            FormatFloat('00000000000', QrPagtosFatNum.Value) + ' Pgto ' +
            FormatFloat('00000000000', QrPagtosControle.Value);
        end;
        if Trim(Texto) <> '' then
          Memo1.Lines.Add(Texto);
        QrPagtos.Next;
      end;
    end;
    ////////////////////////////////////////////////////////////////////////////

    if CkDep.Checked then
    begin
      QrDeposi.First;
      while not QrDeposi.Eof do
      begin
        Progress.Position := Progress.Position + 1;
        Progress.Update;
        Application.ProcessMessages;
        //
        Texto := '';
        for i := 0 to LbSeq.Items.Count - 1 do
        begin
          Ini := StrToInt(Copy(LbSeq.Items[i], 01, 8));
          Fim := StrToInt(Copy(LbSeq.Items[i], 10, 8));
          Its := StrToInt(Copy(LbSeq.Items[i], 19, 3));
          case Its of
            1:
            begin
              case QrDeposiTipo.Value of
                0: Txt := IntToStr(QrDeposiDIA0.Value);
                1: Txt := IntToStr(QrDeposiDIA1.Value);
              end;
            end;
            2:
            begin
              case QrDeposiTipo.Value of
                0: Txt := IntToStr(QrDeposiMES0.Value);
                1: Txt := IntToStr(QrDeposiMES1.Value);
              end;
            end;
            3:
            begin
              // Parei aqui. Fazer dep�sito de cheques?
              (*case QrDeposiTipo.Value of
                0:
                begin*)
                  //if QrDeposiFatParcela.Value = 8458 then ShowMessage('Ver aqui');
                  Txt := QrDeposiDEVEDORA.Value;
                  if Txt = '' then
                    Txt := QrConfigContab.Value;
                (*end;
                1:
                begin
                end;
              end;*)
            end;
            4: Txt := QrDeposiCREDORA.Value;
            5:
            begin
              case QrDeposiTipo.Value of
                0: Sub := 'cheque';
                1: Sub := 'duplicata'
                else Sub := '???';
              end;
              Txt := 'Recebimento '+Sub+' ref. NF '+ FormatFloat('000000',
                QrDeposiNF.Value);
            end;
            6: Txt := Geral.SoNumero_TT(Geral.FFT(QrDeposiCredito.Value, 2,
              siPositivo));
            7:
            begin
              if QrDeposiDocumento.Value = 0 then Txt := '' else
              Txt := FloatToStr(QrDeposiDocumento.Value);
            end;
            else Txt := '';
          end;
          case Its of
            1: Cpl := QrConfigDiaP.Value;
            2: Cpl := QrConfigMesP.Value;
            3: Cpl := QrConfigDevP.Value;
            4: Cpl := QrConfigCreP.Value;
            5: Cpl := QrConfigComP.Value;
            6: Cpl := QrConfigValP.Value;
            7: Cpl := QrConfigDocP.Value;
            else Cpl := ' ';
          end;
          if cpl = '' then Cpl := ' ';
          if cpl = '0' then
            Txt := Geral.CompletaString(Txt, Cpl, Fim - Ini + 1, taRightJustify, True)
          else
            Txt := Geral.CompletaString(Txt, Cpl, Fim - Ini + 1, taLeftJustify, True);
          while Length(Texto) < Ini-1 do Texto := Texto + ' ';
          Texto := Texto + Txt;
        end;
        if CkTeste.Checked then
        begin
          Texto := Texto + ' Lote '+
            FormatFloat('00000000000', QrDeposiFatNum.Value) + ' Item ' +
            FormatFloat('00000000000', QrDeposiFatParcela.Value);
        end;
        if Trim(Texto) <> '' then
          Memo1.Lines.Add(Texto);
        QrDeposi.Next;
      end;
    end;
    ////////////////////////////////////////////////////////////////////////////
    if CkImp.Checked then
    begin
      //  Imposto IOF / IOC
      if CkIOF.Enabled and CkIOF.Checked then
      begin
        QrImpost.First;
        while not QrImpost.Eof do
        begin
          Progress.Position := Progress.Position + 1;
          Progress.Update;
          Application.ProcessMessages;
          //
          Texto := '';
          for i := 0 to LbSeq.Items.Count - 1 do
          begin
            Ini := StrToInt(Copy(LbSeq.Items[i], 01, 8));
            Fim := StrToInt(Copy(LbSeq.Items[i], 10, 8));
            Its := StrToInt(Copy(LbSeq.Items[i], 19, 3));
            case Its of
              1: Txt := IntToStr(QrImpostDIA.Value);
              2: Txt := IntToStr(QrImpostMES.Value);
              3: Txt := QrImpostDEVEDORA.Value;
              4: Txt := Dmod.QrControleContabIOF.Value;
              5: Txt := 'IOF ref. NF '+ FormatFloat('000000', QrImpostNF.Value);
              6: Txt := Geral.SoNumero_TT(Geral.FFT(QrImpostIOF_TOT.Value, 2, siPositivo));
              7: Txt := '';
              else Txt := '';
            end;
            case Its of
              1: Cpl := QrConfigDiaP.Value;
              2: Cpl := QrConfigMesP.Value;
              3: Cpl := QrConfigDevP.Value;
              4: Cpl := QrConfigCreP.Value;
              5: Cpl := QrConfigComP.Value;
              6: Cpl := QrConfigValP.Value;
              7: Cpl := QrConfigDocP.Value;
              else Cpl := ' ';
            end;
            if cpl = '' then Cpl := ' ';
            if cpl = '0' then
              Txt := Geral.CompletaString(Txt, Cpl, Fim - Ini + 1, taRightJustify, True)
            else
              Txt := Geral.CompletaString(Txt, Cpl, Fim - Ini + 1, taLeftJustify, True);
            while Length(Texto) < Ini-1 do Texto := Texto + ' ';
            Texto := Texto + Txt;
          end;
          if CkTeste.Checked then
          begin
            Texto := Texto + ' Lote '+
              FormatFloat('00000000000', QrImpostCodigo.Value);
          end;
          if Trim(Texto) <> '' then
            Memo1.Lines.Add(Texto);
          QrImpost.Next;
        end;
      end;
      ////////////////////////////////////////////////////////////////////////////
      //  Imposto Fator de compra
      if CkFaC.Enabled and CkFaC.Checked then
      begin
        QrImpost.First;
        while not QrImpost.Eof do
        begin
          Progress.Position := Progress.Position + 1;
          Progress.Update;
          Application.ProcessMessages;
          //
          Texto := '';
          for i := 0 to LbSeq.Items.Count - 1 do
          begin
            Ini := StrToInt(Copy(LbSeq.Items[i], 01, 8));
            Fim := StrToInt(Copy(LbSeq.Items[i], 10, 8));
            Its := StrToInt(Copy(LbSeq.Items[i], 19, 3));
            case Its of
              1: Txt := IntToStr(QrImpostDIA.Value);
              2: Txt := IntToStr(QrImpostMES.Value);
              3: Txt := QrImpostDEVEDORA.Value;
              4: Txt := Dmod.QrControleContabFaC.Value;
              5: Txt := 'Fator de compra ref. NF '+ FormatFloat('000000', QrImpostNF.Value);
              6: Txt := Geral.SoNumero_TT(Geral.FFT(QrImpostMINTC.Value, 2, siPositivo));
              7: Txt := '';
              else Txt := '';
            end;
            case Its of
              1: Cpl := QrConfigDiaP.Value;
              2: Cpl := QrConfigMesP.Value;
              3: Cpl := QrConfigDevP.Value;
              4: Cpl := QrConfigCreP.Value;
              5: Cpl := QrConfigComP.Value;
              6: Cpl := QrConfigValP.Value;
              7: Cpl := QrConfigDocP.Value;
              else Cpl := ' ';
            end;
            if cpl = '' then Cpl := ' ';
            if cpl = '0' then
              Txt := Geral.CompletaString(Txt, Cpl, Fim - Ini + 1, taRightJustify, True)
            else
              Txt := Geral.CompletaString(Txt, Cpl, Fim - Ini + 1, taLeftJustify, True);
            while Length(Texto) < Ini-1 do Texto := Texto + ' ';
            Texto := Texto + Txt;
          end;
          if CkTeste.Checked then
          begin
            Texto := Texto + ' Lote '+
              FormatFloat('00000000000', QrImpostCodigo.Value);
          end;
          if Trim(Texto) <> '' then
            Memo1.Lines.Add(Texto);
          QrImpost.Next;
        end;
      end;
      ////////////////////////////////////////////////////////////////////////////
      //  Imposto Ad Valorem
      if CkAdV.Enabled and CkAdV.Checked then
      begin
        QrImpost.First;
        while not QrImpost.Eof do
        begin
          Progress.Position := Progress.Position + 1;
          Progress.Update;
          Application.ProcessMessages;
          //
          Texto := '';
          for i := 0 to LbSeq.Items.Count - 1 do
          begin
            Ini := StrToInt(Copy(LbSeq.Items[i], 01, 8));
            Fim := StrToInt(Copy(LbSeq.Items[i], 10, 8));
            Its := StrToInt(Copy(LbSeq.Items[i], 19, 3));
            case Its of
              1: Txt := IntToStr(QrImpostDIA.Value);
              2: Txt := IntToStr(QrImpostMES.Value);
              3: Txt := QrImpostDEVEDORA.Value;
              4: Txt := Dmod.QrControleContabAdV.Value;
              5: Txt := 'Ad Valorem ref. NF '+ FormatFloat('000000', QrImpostNF.Value);
              6: Txt := Geral.SoNumero_TT(Geral.FFT(QrImpostMINAV.Value, 2, siPositivo));
              7:Txt := '';
              else Txt := '';
            end;
            case Its of
              1: Cpl := QrConfigDiaP.Value;
              2: Cpl := QrConfigMesP.Value;
              3: Cpl := QrConfigDevP.Value;
              4: Cpl := QrConfigCreP.Value;
              5: Cpl := QrConfigComP.Value;
              6: Cpl := QrConfigValP.Value;
              7: Cpl := QrConfigDocP.Value;
              else Cpl := ' ';
            end;
            if cpl = '' then Cpl := ' ';
            if cpl = '0' then
              Txt := Geral.CompletaString(Txt, Cpl, Fim - Ini + 1, taRightJustify, True)
            else
              Txt := Geral.CompletaString(Txt, Cpl, Fim - Ini + 1, taLeftJustify, True);
            while Length(Texto) < Ini-1 do Texto := Texto + ' ';
            Texto := Texto + Txt;
          end;
          if CkTeste.Checked then
          begin
            Texto := Texto + ' Lote '+
              FormatFloat('00000000000', QrImpostCodigo.Value);
          end;
          if Trim(Texto) <> '' then
            Memo1.Lines.Add(Texto);
          QrImpost.Next;
        end;
      end;

      ////////////////////////////////////////////////////////////////////////////
    end;
    if Memo1.Lines.Count > 0 then BtSalva.Enabled := True;
    //
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmExpContabExp.FormCreate(Sender: TObject);
begin
  FATID_0302_Txt := FormatFloat('0', VAR_FATID_0302);
  //
  PageControl1.ActivePageIndex := 0;
  UMyMod.AbreQuery(QrConfig, Dmod.MyDB);
  TPIni.Date := Int(Geral.PrimeiroDiaDoMes(Date));
  TPFim.Date := Int(Date);
  //
  QrPagtos.DataBase := DModG.MyPID_DB;
  QrPagErr.DataBase := DModG.MyPID_DB;
end;

procedure TFmExpContabExp.BtSalvaClick(Sender: TObject);
var
  Nome: String;
  posi: Integer;
begin
  Nome := Dmod.QrMasterEm.Value;
  if Length(Nome) > 0 then
  begin
    Posi := pos(' ', Nome);
    if Posi > 0 then Nome := Copy(Nome, 1, Posi-1);
  end;
  if Nome = '' then Nome := TMeuDB;
  Nome := Nome + '_' + FormatDateTime('yymmdd', TPIni.Date) + '_' +
                       FormatDateTime('yymmdd', TPFim.Date);
  SaveDialog1.DefaultExt := '.TXT';
  SaveDialog1.Filter := 'texto (*.txt)|*.TXT';
  SaveDialog1.FileName := Nome;
  if SaveDialog1.Execute then
    MLAGeral.ExportaMemoToFile(Memo1, SaveDialog1.FileName, True, True, False);
end;

procedure TFmExpContabExp.TPIniChange(Sender: TObject);
var
  Data: TDateTime;
begin
  Data := Geral.UltimoDiaDoMes(TPIni.Date);
  if Data > Date then Data := Date;
  TPFim.Date := Data;
  ConfigNovaPesq;
end;

procedure TFmExpContabExp.TPFimChange(Sender: TObject);
begin
  ConfigNovaPesq;
end;

procedure TFmExpContabExp.ConfigNovaPesq;
begin
  BtSalva.Enabled := False;
  Memo1.Lines.Clear;
  Progress.Position := 0;
end;

procedure TFmExpContabExp.EdEmpresaChange(Sender: TObject);
begin
  FEntidade := DModG.QrEmpresasCodigo.Value;
  if FEntidade <> 0 then
  begin
    FCliInt   := DModG.QrEmpresasFilial.Value;
    //
    DModG.Def_EM_ABD(TMeuDB, FEntidade, FCliInt, FDtEncer, FDtMorto, FTabLctA, FTabLctB, FTabLctD);
  end else
    DModG.Def_EM_ABD_Limpa(FDtEncer, FDtMorto, FTabLctA, FTabLctB, FTabLctD);
  //  
  DModG.Def_EM_xID(FTabLctA, FTabLctB, FTabLctD, 'lot', FTabLotA, FTabLotB, FTabLotD);
end;

procedure TFmExpContabExp.CkTesteClick(Sender: TObject);
begin
  Label3.Visible := CkTeste.Checked;
end;

procedure TFmExpContabExp.CkImpClick(Sender: TObject);
begin
  CkIOF.Enabled := CkImp.Checked;
  CkFaC.Enabled := CkImp.Checked;
  CkAdV.Enabled := CkImp.Checked;
  if not CkImp.Checked then
  begin
    CkIOF.Checked := False;
    CkFaC.Checked := False;
    CkAdV.Checked := False;
  end;
end;

procedure TFmExpContabExp.QrImpostCalcFields(DataSet: TDataSet);
begin
  QrImpostIOF_TOT.Value := QrImpostIOC_VAL.Value +
    QrImpostIOFd_VAL.Value + QrImpostIOFv_VAL.Value;
end;

procedure TFmExpContabExp.ReopenPagErr(DtI, DtF: TDateTime; DataI, DataF: String; Ch, Du: Integer);
begin
  UMyMod.AbreSQL_ABD(QrPagErr, '_PAGERR_EXP_CTB_', FTabLctA, FTabLctB, FTabLctD,
  FTabLotA, FTabLotB, FTabLotD, DtI, DtF, FDtEncer, FDtMorto, [
  'SELECT la.FatParcela, la.Vencimento,',
  'lo.Codigo, la.Debito, la.Documento,',
  'lo.NF, lo.Data, lo.Cliente, la.Controle,',
  'ca.Contab CREDORA, fo.Contab DEVEDORA,',
  'IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome) NOMEENT',
  'FROM ' + LCT_MASK + ' la',
  'LEFT JOIN ' + TMeuDB + '.carteiras ca ON ca.Codigo=la.Carteira',
  'LEFT JOIN ' + _ID_MASK + ' lo ON lo.Codigo=la.FatNum',
  'LEFT JOIN ' + TMeuDB + '.entidades fo ON fo.Codigo=lo.Cliente',
  'WHERE FatID IN (' + FATID_0302_Txt + ')',
  'AND lo.Data BETWEEN "' + DataI + '" AND "' + DataF + '"',
  'AND (lo.TxCompra+lo.ValValorem)>=0.01',
  'AND lo.Tipo in (' + FormatFloat('0', Ch) + ', ' + FormatFloat('0', Du) + ')',
  'AND (TRIM(ca.Contab) = "" OR TRIM(fo.Contab) = "")',
  ''], [
  'ORDER BY FatParcela, Vencimento',
  ''], tetExcluiAntesEDepois, 'ExpContabExp.ReopenPagErr()');
end;

procedure TFmExpContabExp.ReopenPagtos(DtI, DtF: TDateTime; DataI, DataF: String; Ch, Du: Integer);
begin
  UMyMod.AbreSQL_ABD(QrPagtos, '_PAGTOS_EXP_CTB_', FTabLctA, FTabLctB, FTabLctD,
  FTabLotA, FTabLotB, FTabLotD, DtI, DtF, FDtEncer, FDtMorto, [
  'SELECT la.FatParcela, la.Vencimento,',
  'la.Debito, la.Documento, lo.NF, lo.Data, lo.Tipo,',
  'ca.Contab CREDORA, fo.Contab DEVEDORA,',
  'DAY(lo.Data) + 0 DIA, MONTH(lo.Data) + 0 MES, la.FatNum, la.Controle',
  'FROM ' + LCT_MASK + ' la',
  'LEFT JOIN ' + TMeuDB + '.carteiras ca ON ca.Codigo=la.Carteira',
  'LEFT JOIN ' + _ID_MASK + ' lo ON lo.Codigo=la.FatNum',
  'LEFT JOIN ' + TMeuDB + '.entidades fo ON fo.Codigo=lo.Cliente',
  'WHERE FatID IN (' + FATID_0302_Txt + ')',
  'AND lo.Data BETWEEN "' + DataI + '" AND "' + DataF + '"',
  'AND (lo.TxCompra+lo.ValValorem)>=0.01',
  'AND lo.Tipo in (' + FormatFloat('0', Ch) + ', ' + FormatFloat('0', Du) + ')',
  ''], [
  'ORDER BY FatParcela, Vencimento',
  ''], tetExcluiAntesEDepois, 'ExpContabExp.ReopenPagtos()');
end;

procedure TFmExpContabExp.AvisaFaltaDeCodigoContab(DtI, DtF: TDateTime; DataI, DataF: String;
Ch, Du: Integer);
  procedure ReabreTabela(const Tabela, SQL: String; Tipo: Integer);
    procedure InsereItens(const QrSemContab: TmySQLQuery; const Tabela: String);
    begin
      if not FCriou then
      begin
        Query.Close;
        Query.SQL.Clear;
        Query.SQL.Add('DROP TABLE semctb; ');
        Query.SQL.Add('CREATE TABLE semctb (');
        Query.SQL.Add('  Tabela varchar(30)  ,');
        Query.SQL.Add('  Nome   varchar(100) ,');
        Query.SQL.Add('  Codigo integer       ');
        Query.SQL.Add(');');
        //
        FCriou := True;
      end;
      while not QrSemContab.Eof do
      begin
        Query.SQL.Add('INSERT INTO semctb (' +
        'Codigo, Nome, Tabela) VALUES (' +
          FormatFloat('0', QrSemContab.FieldByName('Codigo').AsInteger) + ',' +
          '"' + QrSemContab.FieldByName('Nome').AsString + '","' + Tabela + '");');
        QrSemContab.Next;
      end;
    end;
  begin
    QrPesq1.Close;
    QrPesq1.SQL.Clear;
    QrPesq1.SQL.Add(SQL);
    //
    case Tipo of
      1:
      begin
        QrPesq1.Params[0].AsString  := DataI;
        QrPesq1.Params[1].AsString  := DataF;
        QrPesq1.Params[2].AsInteger := Ch;
        QrPesq1.Params[3].AsInteger := Du;
      end;
      2:
      begin
        QrPesq1.Params[00].AsString  := DataI;
        QrPesq1.Params[01].AsString  := DataF;
        QrPesq1.Params[02].AsString  := DataI;
        QrPesq1.Params[03].AsString  := DataF;
        QrPesq1.Params[04].AsInteger := Ch;
        QrPesq1.Params[05].AsInteger := Du;
      end;
      else Geral.MensagemBox('Tipo n�o definido na procedure '+
      '"AvisaFaltaDeCodigoContab"!', 'Aviso', MB_OK+MB_ICONWARNING);
    end;
    UMyMod.AbreQuery(QrPesq1, Dmod.MyDB, 'TFmExpContabExp.AvisaFaltaDeCodigoContab()');
    if QrPesq1.RecordCount > 0 then
    begin
      FItens := FItens + QrPesq1.RecordCount;
      InsereItens(QrPesq1, Tabela);
    end;
  end;
var
  SQL: String;
begin
  QrPesq1.Close;
  if CkPag.Checked then
  begin
    { mudei 2011-11-05
    QrPagErr.Close;
    QrPagErr.Params[00].AsString  := DataI;
    QrPagErr.Params[01].AsString  := DataF;
    QrPagErr.Params[02].AsInteger := Ch;
    QrPagErr.Params[03].AsInteger := Du;
    UMyMod.AbreQuery(QrPagErr, 'TFmExpContabExp.AvisaFaltaDeCodigoContab()');
    }
    ReopenPagErr(Dti, DtF, DataI, DataF, Ch, Du);
    if QrPagErr.RecordCount > 0 then
    begin
      PageControl1.ActivePageIndex := 2;
      PageControl2.ActivePageIndex := 0;
      Geral.MensagemBox('Existem ' + IntToStr(QrPagErr.RecordCount) +
      ' pagamentos sem identificador de contabilidade nesta pesquisa! ' +
      'Na guia "Cadatros sem identificador de contabilidade" constam ' +
      'os cadastros que precisam de identificador!',
      'Aviso', MB_OK+MB_ICONWARNING);
    end;
    SQL :=
    'SELECT DISTINCT ca.Codigo CODIGO, ca.Nome NOME' + #13#10 +
    'FROM ' + FTabLctA  + ' la' + #13#10 +
    'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira' + #13#10 +
    'LEFT JOIN ' + FTabLotA + ' lo ON lo.Codigo=la.FatNum' + #13#10 +
    'WHERE FatID IN (' + FATID_0302_Txt + ')' + #13#10 +
    'AND lo.Data BETWEEN :P0 AND :P1' + #13#10 +
    'AND (lo.TxCompra+lo.ValValorem)>=0.01' + #13#10 +
    'AND lo.Tipo in (:P2, :P3)' + #13#10 +
    'AND TRIM(ca.Contab) = ""';
    ReabreTabela('Carteira', SQL, 1);
    //
    SQL :=
    'SELECT DISTINCT fo.Codigo CODIGO,' + #13#10 +
    'IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome) NOME' + #13#10 +
    'FROM ' + FTabLctA  + ' la' + #13#10 +
    'LEFT JOIN ' + FTabLotA + ' lo ON lo.Codigo=la.FatNum' + #13#10 +
    'LEFT JOIN entidades fo ON fo.Codigo=lo.Cliente' + #13#10 +
    'WHERE FatID IN (' + FATID_0302_Txt + ')' + #13#10 +
    'AND (lo.TxCompra+lo.ValValorem)>=0.01' + #13#10 +
    'AND lo.Data BETWEEN :P0 AND :P1' + #13#10 +
    'AND lo.Tipo in (:P2, :P3)' + #13#10 +
    'AND TRIM(fo.Contab) = ""';
    ReabreTabela('Entidades', SQL, 1);
  end;
  //
  if CkDep.Checked then
  begin
{
    QrDepErr.Close;
    QrDepErr.Params[00].AsString  := DataI;
    QrDepErr.Params[01].AsString  := DataF;
    QrDepErr.Params[02].AsString  := DataI;
    QrDepErr.Params[03].AsString  := DataF;
    QrDepErr.Params[04].AsInteger := Ch;
    QrDepErr.Params[05].AsInteger := Du;
    UMyMod.AbreQuery(QrDepErr, 'TFmExpContabExp.AvisaFaltaDeCodigoContab()');
}
    UnDmkDAC_PF.AbreMySQLQuery0(QrDepErr, Dmod.MyDB, [
    'SELECT li.FatNum, li.FatParcela, lo.Cliente, ',
    'li.CartDep, ca.Nome NOMRCART, ',
    'cl.Contab CONTAB_ENTI, ',
    'ca.Contab CONTAB_CART, ',
    'IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMECLI ',
    'FROM ' + FTabLctA + ' li ',
    'LEFT JOIN ' + FTabLotA + ' lo ON lo.Codigo=li.FatNum ',
    'LEFT JOIN entidades cl ON cl.Codigo=lo.Cliente ',
    'LEFT JOIN carteiras ca ON ca.Codigo=li.CartDep ',
    'WHERE li.FatID=' + FormatFloat('0', VAR_FATID_0301),
    'AND lo.Codigo > 0 ',
    'AND (lo.TxCompra + lo.ValValorem) >= 0.01 ',
    'AND ((lo.Tipo=0 AND li.DDeposito BETWEEN "' +
    DataI + '" AND "' + DataF + '") ',
    'OR (lo.Tipo=1 AND (li.Data3 BETWEEN "' +
    DataI + '" AND "' + DataF + '"))) ',
    'AND lo.Tipo in (' + FormatFloat('0', Ch) + ', ' +
    FormatFloat('0', Du) + ') ',
    'AND (cl.Contab="" OR ca.Contab="") ',
    'ORDER BY li.FatNum, li.FatParcela ',
    '']);
    //
    if QrDepErr.RecordCount > 0 then
    begin
      PageControl1.ActivePageIndex := 2;
      PageControl2.ActivePageIndex := 1;
      Geral.MensagemBox('Existem ' + IntToStr(QrDepErr.RecordCount) +
      ' dep�sitos sem identificador de contabilidade nesta pesquisa! ' +
      'Na guia "Cadatros sem identificador de contabilidade" constam ' +
      'os cadastros que precisam de identificador!',
      'Aviso', MB_OK+MB_ICONWARNING);
    end;
    SQL :=
    'SELECT DISTINCT lo.Cliente CODIGO, ' + #13#10 +
    'IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOME ' + #13#10 +
    'FROM ' + FTabLctA + ' li ' + #13#10 +
    'LEFT JOIN ' + FTabLotA + ' lo ON lo.Codigo=li.FatNum ' + #13#10 +
    'LEFT JOIN entidades cl ON cl.Codigo=lo.Cliente ' + #13#10 +
    'WHERE li.FatID=' + FormatFloat('0', VAR_FATID_0301) + #13#10 +
    'AND lo.Codigo > 0 ' + #13#10 +
    'AND (lo.TxCompra+lo.ValValorem)>=0.01' + #13#10 +
    'AND ((lo.Tipo=0 AND li.DDeposito BETWEEN :P0 AND :P1) ' + #13#10 +
    'OR (lo.Tipo=1 AND (li.Data3 BETWEEN :P2 AND :P3))) ' + #13#10 +
    'AND lo.Tipo in (:P4, :P5) ' + #13#10 +
    'AND cl.Contab="" ';
    ReabreTabela('Entidades', SQL, 2);
  end;
  //
  if CkImp.Checked then
  begin
{
    QrTaxErr.Close;
    QrTaxErr.Params[00].AsString  := DataI;
    QrTaxErr.Params[01].AsString  := DataF;
    QrTaxErr.Params[02].AsInteger := Ch;
    QrTaxErr.Params[03].AsInteger := Du;
    UMyMod.AbreQuery(QrTaxErr, 'TFmExpContabExp.AvisaFaltaDeCodigoContab()');
}
    UnDmkDAC_PF.AbreMySQLQuery0(QrTaxErr, Dmod.MyDB, [
    'SELECT lo.Codigo, lo.Cliente, ',
    'IF(en.Tipo=0, en.RazaoSocial, en.Nome) NOMECLI, ',
    'lo.MINTC, lo.MINAV, en.Contab DEVEDORA, ',
    'lo.IOC_VAL, lo.IOFd_VAL, lo.IOFv_VAL ',
    'FROM ' + FTabLotA + ' lo ',
    'LEFT JOIN entidades en ON en.Codigo=lo.Cliente ',
    'WHERE lo.data BETWEEN "' +
    DataI + '" AND "' + DataF + '"',
    'AND (TxCompra + ValValorem) >= 0.01 ',
    'AND lo.Tipo IN (' + FormatFloat('0', Ch) +
    ', ' + FormatFloat('0', Du) + ') ',
    'AND (lo.TxCompra + lo.ValValorem) >= 0.01 ',
    'AND TRIM(en.Contab) = "" ',
    'ORDER BY lo.Data, lo.NF ',
    '']);
    //
    if QrTaxErr.RecordCount > 0 then
    begin
      PageControl1.ActivePageIndex := 2;
      PageControl2.ActivePageIndex := 2;
      Geral.MensagemBox('Existem ' + IntToStr(QrTaxErr.RecordCount) +
      ' impostos sem identificador de contabilidade nesta pesquisa! ' +
      'Na guia "Cadatros sem identificador de contabilidade" constam ' +
      'os cadastros que precisam de identificador!',
      'Aviso', MB_OK+MB_ICONWARNING);
    end;
    SQL :=
    'SELECT DISTINCT lo.Cliente CODIGO, ' + #13#10 +
    'IF(en.Tipo=0, en.RazaoSocial, en.Nome) NOME ' + #13#10 +
    'FROM ' + FTabLotA + ' lo ' + #13#10 +
    'LEFT JOIN entidades en ON en.Codigo=lo.Cliente ' + #13#10 +
    'WHERE lo.data BETWEEN :P0 AND :P1 ' + #13#10 +
    'AND lo.Tipo in (:P2, :P3) ' + #13#10 +
    'AND (lo.TxCompra+lo.ValValorem)>=0.01' + #13#10 +
    'AND TRIM(en.Contab) = "" ' + #13#10 +
    'ORDER BY lo.Data, lo.NF ';
    ReabreTabela('Entidades', SQL, 1);
  end;
  //
  if FItens > 0 then
  begin
    Query.SQL.Add('SELECT DISTINCT * FROM semctb;');
    DmkABS_PF.AbreQuery(Query);
    //
    PageControl1.ActivePageIndex := 1;
    Geral.MensagemBox('Existem ' + IntToStr(Query.RecordCount) +
    ' cadastros sem identificador de contabilidade que possuem lan�amentos ' +
    'nesta pesquisa! N�o � aconselhavel enviar estes dados � contabilidade. ' +
    '� aconselhavel cadastrar os referidos identificadores antes de exportar ' +
    'os dados. Na guia "Cadatros sem identificador de contabilidade" constam ' +
    'os cadastros que precisam de identificador!', 'Aviso', MB_OK+MB_ICONWARNING);
  end;
end;

end.

