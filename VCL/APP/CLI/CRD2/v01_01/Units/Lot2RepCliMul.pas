unit Lot2RepCliMul;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, Db, mySQLDbTables,
  dmkEdit, Mask, DBCtrls, ComCtrls, Variants, dmkGeral, dmkImage, UnDmkProcFunc,
  DmkDAC_PF, UnDmkEnums;

type
  TFmLot2RepCliMul = class(TForm)
    QrCheque7: TmySQLQuery;
    DsCheque7: TDataSource;
    Timer1: TTimer;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel2: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    Panel7: TPanel;
    CkDescende1: TCheckBox;
    CkDescende2: TCheckBox;
    CkDescende3: TCheckBox;
    Panel6: TPanel;
    RGOrdem3: TRadioGroup;
    RGOrdem2: TRadioGroup;
    RGOrdem1: TRadioGroup;
    Panel3: TPanel;
    DBG1: TDBGrid;
    Panel9: TPanel;
    Panel10: TPanel;
    Label43: TLabel;
    Label49: TLabel;
    Label50: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    Label47: TLabel;
    Label48: TLabel;
    Label42: TLabel;
    Label3: TLabel;
    EdCMC_7_1: TEdit;
    EdCPF_2: TdmkEdit;
    EdEmitente2: TdmkEdit;
    EdRegiaoCompe: TdmkEdit;
    EdBanco: TdmkEdit;
    EdAgencia: TdmkEdit;
    EdConta: TdmkEdit;
    EdRealCC: TdmkEdit;
    EdCheque: TdmkEdit;
    EdCredito: TdmkEdit;
    EdVencto: TdmkEdit;
    EdBanda: TdmkEdit;
    TabSheet2: TTabSheet;
    Panel11: TPanel;
    Label5: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    EdCPFM: TdmkEdit;
    EdEmitM: TdmkEdit;
    EdBancoM: TdmkEdit;
    EdAgenciaM: TdmkEdit;
    EdContaM: TdmkEdit;
    EdChequeM: TdmkEdit;
    EdCreditoM: TdmkEdit;
    EdVctoM: TdmkEdit;
    DBG2: TDBGrid;
    QrChequeM: TmySQLQuery;
    DsChequeM: TDataSource;
    CkDepositados: TCheckBox;
    TabSheet3: TTabSheet;
    Panel12: TPanel;
    EdCreditoV: TdmkEdit;
    Label4: TLabel;
    DBG3: TDBGrid;
    QrChequeV: TmySQLQuery;
    DsChequeV: TDataSource;
    CkAuto: TCheckBox;
    QrCaixa: TmySQLQuery;
    QrCaixaTOTAL: TFloatField;
    DsCaixa: TDataSource;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GroupBox1: TGroupBox;
    EdSoma: TdmkEdit;
    EdFalta0: TDBEdit;
    Label1: TLabel;
    Label2: TLabel;
    EdSaldo: TdmkEdit;
    GroupBox2: TGroupBox;
    EdSoma2: TdmkEdit;
    Label14: TLabel;
    EdFalta2: TDBEdit;
    Label15: TLabel;
    EdSaldo2: TdmkEdit;
    GBAvisos1: TGroupBox;
    Panel8: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BitBtn3: TBitBtn;
    Panel13: TPanel;
    BtOK: TBitBtn;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Label16: TLabel;
    DBEdit2: TDBEdit;
    QrCheque7NOMECLI: TWideStringField;
    QrCheque7Lote: TIntegerField;
    QrCheque7Data: TDateField;
    QrCheque7Credito: TFloatField;
    QrCheque7Documento: TFloatField;
    QrCheque7Vencimento: TDateField;
    QrCheque7FatNum: TFloatField;
    QrCheque7FatParcela: TIntegerField;
    QrCheque7Emitente: TWideStringField;
    QrCheque7Banco: TIntegerField;
    QrCheque7ContaCorrente: TWideStringField;
    QrCheque7CNPJCPF: TWideStringField;
    QrCheque7DCompra: TDateField;
    QrCheque7DDeposito: TDateField;
    QrChequeMNOMECLI: TWideStringField;
    QrChequeMLote: TIntegerField;
    QrChequeMData: TDateField;
    QrChequeMCredito: TFloatField;
    QrChequeMDocumento: TFloatField;
    QrChequeMVencimento: TDateField;
    QrChequeMFatNum: TFloatField;
    QrChequeMFatParcela: TIntegerField;
    QrChequeMEmitente: TWideStringField;
    QrChequeMBanco: TIntegerField;
    QrChequeMContaCorrente: TWideStringField;
    QrChequeMCNPJCPF: TWideStringField;
    QrChequeMDCompra: TDateField;
    QrChequeMDDeposito: TDateField;
    QrChequeVNOMECLI: TWideStringField;
    QrChequeVLote: TIntegerField;
    QrChequeVData: TDateField;
    QrChequeVCredito: TFloatField;
    QrChequeVDocumento: TFloatField;
    QrChequeVVencimento: TDateField;
    QrChequeVFatNum: TFloatField;
    QrChequeVFatParcela: TIntegerField;
    QrChequeVEmitente: TWideStringField;
    QrChequeVBanco: TIntegerField;
    QrChequeVContaCorrente: TWideStringField;
    QrChequeVCNPJCPF: TWideStringField;
    QrChequeVDCompra: TDateField;
    QrChequeVDDeposito: TDateField;
    QrCheque7Agencia: TIntegerField;
    QrChequeMAgencia: TIntegerField;
    QrChequeVAgencia: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure RGOrdem1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure DBG1CellClick(Column: TColumn);
    procedure DBG1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdSomaChange(Sender: TObject);
    procedure EdFalta0Change(Sender: TObject);
    procedure EdBandaChange(Sender: TObject);
    procedure EdCMC_7_1Change(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure EdBancoMExit(Sender: TObject);
    procedure EdAgenciaMExit(Sender: TObject);
    procedure EdContaMExit(Sender: TObject);
    procedure EdChequeMExit(Sender: TObject);
    procedure EdCreditoMExit(Sender: TObject);
    procedure EdVctoMExit(Sender: TObject);
    procedure EdCPFMExit(Sender: TObject);
    procedure EdEmitMExit(Sender: TObject);
    procedure CkDepositadosClick(Sender: TObject);
    procedure EdCreditoVExit(Sender: TObject);
  private
    { Private declarations }
    FAscendente: Boolean;
    procedure ReopenCheque7(FatParcela: Integer);
    procedure ReopenChequeM;
    procedure ReopenChequeV(Automatico: Boolean);
    procedure ReopenCaixa;
    procedure SomaLinhas(Ascendente: Boolean);
    procedure PreparaSoma(Ascendente: Boolean);
    procedure ExecutaSelecionados(Acao: Integer);
    procedure ExecutaAtual(Acao: Integer);
    procedure Executa(Acao: Integer);
    procedure CalculaSaldo;
  public
    { Public declarations }
    FRepCli: Integer;
  end;

  var
  FmLot2RepCliMul: TFmLot2RepCliMul;

implementation

uses UnMyObjects, Module, Principal, Lot0Dep, Lot2Cab, UMySQLModule, MyListas,
  UnInternalConsts, ModuleLot, TedC_Aux;

{$R *.DFM}

procedure TFmLot2RepCliMul.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLot2RepCliMul.FormActivate(Sender: TObject);
begin
  ImgTipo.SQLType := stIns;
  MyObjects.CorIniComponente();
  EdBanda.SetFocus;
end;

procedure TFmLot2RepCliMul.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLot2RepCliMul.RGOrdem1Click(Sender: TObject);
begin
  ReopenCheque7(QrCheque7FatParcela.Value);
end;

procedure TFmLot2RepCliMul.PreparaSoma(Ascendente: Boolean);
begin
  FAscendente := Ascendente;
  if Timer1.Enabled then Timer1.Enabled := False;
  Timer1.Enabled := True;
end;

procedure TFmLot2RepCliMul.SomaLinhas(Ascendente: Boolean);
var
  Credito: Double;
  i, k: Integer;
begin
  k := QrCheque7FatParcela.Value;
  if DBG1.SelectedRows.Count = 0 then Credito := QrCheque7Credito.Value else
  begin
    Credito := 0;
    with DBG1.DataSource.DataSet do
    begin
      if Ascendente then
      begin
        for i:= 0 to DBG1.SelectedRows.Count-1 do
        begin
          //GotoBookmark(pointer(DBG1.SelectedRows.Items[i]));
          GotoBookmark(DBG1.SelectedRows.Items[i]);
          Credito := Credito + QrCheque7Credito.Value;
        end;
      end else begin
        for i:= DBG1.SelectedRows.Count-1 downto 0 do
        begin
          //GotoBookmark(pointer(DBG1.SelectedRows.Items[i]));
          GotoBookmark(DBG1.SelectedRows.Items[i]);
          Credito := Credito + QrCheque7Credito.Value;
        end;
      end;
    end;
    QrCheque7.Locate('FatParcela', k, []);
  end;
  EdSoma.ValueVariant := Credito;
  EdSoma2.ValueVariant := Credito;
end;

procedure TFmLot2RepCliMul.Timer1Timer(Sender: TObject);
begin
  SomaLinhas(FAscendente);
end;

procedure TFmLot2RepCliMul.DBG1CellClick(Column: TColumn);
begin
  SomaLinhas(True);
end;

procedure TFmLot2RepCliMul.DBG1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_DOWN) and (Shift=[ssShift]) then PreparaSoma(True)  else
  if (Key=VK_UP)   and (Shift=[ssShift]) then PreparaSoma(False) else
  if (Key=VK_DOWN)                       then PreparaSoma(True)  else
  if (Key=VK_UP)                         then PreparaSoma(False);
end;

procedure TFmLot2RepCliMul.FormCreate(Sender: TObject);
begin
  case FmPrincipal.FFormLotShow of
{
    0:
    begin
      EdFalta0.DataSource := FmLot0.DsLot;
      EdFalta2.DataSource := FmLot0.DsLot;
    end;
    1:
    begin
      EdFalta0.DataSource := FmLot1.DsLot;
      EdFalta2.DataSource := FmLot1.DsLot;
    end;
}
    2:
    begin
      EdFalta0.DataSource := FmLot2Cab.DsLot;
      EdFalta2.DataSource := FmLot2Cab.DsLot;
    end;
    else Geral.MensagemBox('Janela de gerenciamento de border�s ' +
    'n�o definida! (14)', 'ERRO', MB_OK+MB_ICONERROR);
  end;
  ReopenCheque7(0);
  // Marcelo 070310
  ReopenChequeM;
  ReopenCaixa;
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmLot2RepCliMul.ExecutaSelecionados(Acao: Integer);
var
  i : Integer;
begin
  case PageControl1.ActivePageIndex of
    0:
    begin
      with DBG1.DataSource.DataSet do
      for i:= 0 to DBG1.SelectedRows.Count-1 do
      begin
        //GotoBookmark(pointer(DBG1.SelectedRows.Items[i]));
        GotoBookmark(DBG1.SelectedRows.Items[i]);
        ExecutaAtual(Acao);
      end;
    end;
    1:
    begin
      with DBG2.DataSource.DataSet do
      for i:= 0 to DBG2.SelectedRows.Count-1 do
      begin
        //GotoBookmark(pointer(DBG2.SelectedRows.Items[i]));
        GotoBookmark(DBG2.SelectedRows.Items[i]);
        ExecutaAtual(Acao);
      end;
    end;
    2:
    begin
      with DBG3.DataSource.DataSet do
      for i:= 0 to DBG3.SelectedRows.Count-1 do
      begin
        //GotoBookmark(pointer(DBG3.SelectedRows.Items[i]));
        GotoBookmark(DBG3.SelectedRows.Items[i]);
        ExecutaAtual(Acao);
      end;
    end;
  end;
end;

procedure TFmLot2RepCliMul.ExecutaAtual(Acao: Integer);
  procedure Executa0(Depositado, RepCli: Integer; Qry: TmySQLQuery);
  begin
    if Qry.RecordCount > 0 then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpdM, stUpd, CO_TabLctA, False, [
      'Depositado', 'RepCli'], ['FatID', 'FatParcela'], [
      Depositado, RepCli], [VAR_FATID_0301,
      Qry.FieldByName('FatParcela').AsInteger], True);
    end;
  end;
  procedure Executa1(Depositado: Integer; Qry: TmySQLQuery);
  begin
    if Qry.RecordCount > 0 then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpdM, stUpd, CO_TabLctA, False, [
      'Depositado'], ['FatID', 'FatParcela'], [
      Depositado], [VAR_FATID_0301,
      Qry.FieldByName('FatParcela').AsInteger], True);
    end;
  end;
begin
 Dmod.QrUpdM.SQL.Clear;
  case acao of
    0:
    begin
      //Dmod.QrUpdM.SQL.Add('UPDATE lot esits SET AlterWeb=1,   Depositado=1 WHERE Controle=:P0 ');
      case PageControl1.ActivePageIndex of
        0: Executa1(CO_CH_DEPOSITADO_MANUAL, QrCheque7);
        1: Executa1(CO_CH_DEPOSITADO_MANUAL, QrChequeM);
        2: Executa1(CO_CH_DEPOSITADO_MANUAL, QrChequeV);
      end;
    end;
    1:
    begin
      {
      Dmod.QrUpdM.SQL.Add('UPDATE lot esits SET AlterWeb=1,   Depositado=0, RepCli=:P0 WHERE Controle=:P1 ');
      Dmod.QrUpdM.Params[0].AsInteger := FRepCli;
      }
      case PageControl1.ActivePageIndex of
        //0: Dmod.QrUpdM.Params[1].AsInteger := QrCheque7FatParcela.Value;
        0: Executa0(CO_CH_DEPOSITADO_NAO, FRepCli, QrCheque7);
        1: Executa0(CO_CH_DEPOSITADO_NAO, FRepCli, QrChequeM);
        2: Executa0(CO_CH_DEPOSITADO_NAO, FRepCli, QrChequeV);
      end;
    end;
  end;
  //
{
  case PageControl1.ActivePageIndex of
    0: if QrCheque7.RecordCount > 0 then Dmod.QrUpdM.ExecSQL;
    1: if QrChequeM.RecordCount > 0 then Dmod.QrUpdM.ExecSQL;
    2: if QrChequeV.RecordCount > 0 then Dmod.QrUpdM.ExecSQL;
  end;
}
end;

procedure TFmLot2RepCliMul.Executa(Acao: Integer);
var
  Itens: Integer;
begin
  Itens := 0;
  Screen.Cursor := crHourGlass;
  case PageControl1.ActivePageIndex of
    0: itens := DBG1.SelectedRows.Count;
    1: itens := DBG2.SelectedRows.Count;
    2: itens := DBG3.SelectedRows.Count;
  end;
  if Itens = 0 then
    ExecutaAtual(Acao)
  else
    ExecutaSelecionados(Acao);
  case PageControl1.ActivePageIndex of
    0: ReopenCheque7(0);
    1: ReopenChequeM;
    2:
    begin
      EdCreditoV.ValueVariant := 0;
      ReopenChequeV(False);
    end;
  end;
  ReopenCaixa;
  if Acao = 1 then
  begin
    case FmPrincipal.FFormLotShow of
      {
      0:
      begin
        FmLot0.CalculaLote(FRepCli, False);
        FmLot0.FMudouSaldo := True;
        FmLot0.LocCod(FRepCli, FRepCli);
      end;
      1:
      begin
        FmLot1.CalculaLote(FRepCli, False);
        FmLot1.FMudouSaldo := True;
        FmLot1.LocCod(FRepCli, FRepCli);
      end;
      }
      2:
      begin
        FmLot2Cab.CalculaLote(FRepCli, False);
        FmLot2Cab.FMudouSaldo := True;
        FmLot2Cab.LocCod(FRepCli, FRepCli);
      end;
      else Geral.MensagemBox('Janela de gerenciamento de border�s ' +
      'n�o definida! (10)', 'ERRO', MB_OK+MB_ICONERROR);
    end;
  end;
  EdBanda.Text := '';
  case PageControl1.ActivePageIndex of
    0: EdBanda.SetFocus;
    2: EdCreditoV.SetFocus;
  end;
  Screen.Cursor := crDefault;
end;

procedure TFmLot2RepCliMul.BitBtn1Click(Sender: TObject);
var
  MyCursor: TCursor;
begin
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    Application.CreateForm(TFmLot0Dep, FmLot0Dep);
    FmLot0Dep.ShowModal;
    FmLot0Dep.Destroy;
    Screen.Cursor := MyCursor
  except
    Screen.Cursor := MyCursor;
    raise;
  end;
end;

procedure TFmLot2RepCliMul.BtOKClick(Sender: TObject);
begin
  Executa(1);
end;

procedure TFmLot2RepCliMul.EdSomaChange(Sender: TObject);
begin
  CalculaSaldo;
end;

procedure TFmLot2RepCliMul.EdFalta0Change(Sender: TObject);
begin
  CalculaSaldo;
end;

procedure TFmLot2RepCliMul.CalculaSaldo;
begin
  EdSaldo.ValueVariant := Geral.DMV(EdSoma.Text) - Geral.DMV(
    EdFalta0.Text);
  EdSaldo2.ValueVariant := EdSaldo.ValueVariant;
end;

procedure TFmLot2RepCliMul.EdBandaChange(Sender: TObject);
begin
  if MLAGeral.LeuTodoCMC7(EdBanda.Text) then
    EdCMC_7_1.Text := Geral.SoNumero_TT(EdBanda.Text);
end;

procedure TFmLot2RepCliMul.EdCMC_7_1Change(Sender: TObject);
var
  Banda: TBandaMagnetica;
begin
  if MLAGeral.CalculaCMC7(EdCMC_7_1.Text) = 0 then
  begin
    Banda := TBandaMagnetica.Create;
    Banda.BandaMagnetica := EdCMC_7_1.Text;
    //EdComp.Text    := Banda.Compe;
    EdBanco.Text   := Banda.Banco;
    EdAgencia.Text := Banda.Agencia;
    EdConta.Text   := Banda.Conta;
    EdCheque.Text  := Banda.Numero;
    //
    if QrCheque7.Locate('Banco;Agencia;ContaCorrente;Documento', VarArrayOf([
    EdBanco.Text, EdAgencia.Text, EdConta.Text, EdCheque.Text]), []) then
    begin
      EdCredito.ValueVariant := QrCheque7Credito.Value;
      EdVencto.Text := Geral.FDT(QrCheque7Vencimento.Value, 2);
      EdEmitente2.text := QrCheque7Emitente.Value;
      EdCPF_2.Text := MLAGeral.FormataCNPJ_TFT(QrCheque7CNPJCPF.Value);
    end else Geral.MensagemBox('Cheque n�o localizado!', 'Aviso',
      MB_OK+MB_ICONWARNING);
    //Pesquisa(False, False);
  end;
end;

procedure TFmLot2RepCliMul.BitBtn2Click(Sender: TObject);
var
  Sobra: String;
begin
  Sobra := EdFalta0.Text;
  if InputQuery('Sobra de Border�', 'Informe o valor da sobra:', Sobra) then
  begin
{
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE lot es SET AlterWeb=1,  SobraNow=:P0 WHERE Codigo=:P1');
    Dmod.QrUpd.Params[00].AsFloat   := Geral.DMV(Sobra);
    Dmod.QrUpd.Params[01].AsInteger := FRepCli;
    Dmod.QrUpd.ExecSQL;
}
    DmLot.LOT_Ins_Upd(Dmod.QrUpdM, stUpd, True, [
    'SobraNow'], ['Codigo'], [
    Geral.DMV(Sobra)], [FRepCli], True);
    //
    case FmPrincipal.FFormLotShow of
      {
      0:
      begin
        FmLot0.CalculaLote(FRepCli, False);
        FmLot0.LocCod(FRepCli, FRepCli);
      end;
      1:
      begin
        FmLot1.CalculaLote(FRepCli, False);
        FmLot1.LocCod(FRepCli, FRepCli);
      end;
      }
      2:
      begin
        FmLot2Cab.CalculaLote(FRepCli, False);
        FmLot2Cab.LocCod(FRepCli, FRepCli);
      end;
      else Geral.MensagemBox('Janela de gerenciamento de border�s ' +
      'n�o definida! (11)', 'ERRO', MB_OK+MB_ICONERROR);
    end;
  end;
end;

procedure TFmLot2RepCliMul.ReopenCheque7(FatParcela: Integer);
const
  NomeA: array[0..5] of string = ('DDeposito','Credito','Documento',
    'Banco , Agencia , ContaCorrente ','Emitente','NOMECLI');
  NomeD: array[0..5] of string = ('DDeposito DESC','Documento DESC',
    'Credito DESC','Banco DESC, Agencia DESC, ContaCorrente DESC',
    'Emitente', 'NOMECLI');
var
  Ordem: String;
begin
  Ordem := 'ORDER BY ';
  if CkDescende1.Checked then
    Ordem := Ordem + NomeD[RGOrdem1.ItemIndex]+', '
  else
    Ordem := Ordem + NomeA[RGOrdem1.ItemIndex]+', ';
  if CkDescende2.Checked then
    Ordem := Ordem + NomeD[RGOrdem2.ItemIndex]+', '
  else
    Ordem := Ordem + NomeA[RGOrdem2.ItemIndex]+', ';
  if CkDescende3.Checked then
    Ordem := Ordem + NomeD[RGOrdem3.ItemIndex]
  else
    Ordem := Ordem + NomeA[RGOrdem3.ItemIndex];
  //
{
  QrCheque7.Close;
  QrCheque7.SQL.Clear;
  QrCheque7.SQL.Add('SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
  QrCheque7.SQL.Add('ELSE ent.Nome END NOMECLI, lot.Lote, loi.*');
  QrCheque7.SQL.Add('FROM lot esits loi');
  QrCheque7.SQL.Add('LEFT JOIN lot es lot ON lot.Codigo=loi.Codigo');
  QrCheque7.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=lot.Cliente');
  QrCheque7.SQL.Add('WHERE lot.Tipo = 0');
  QrCheque7.SQL.Add('AND loi.RepCli = 0');
  QrCheque7.SQL.Add('AND loi.Repassado = 0');
  QrCheque7.SQL.Add('AND loi.Devolucao = 0');
  QrCheque7.SQL.Add('AND loi.Depositado = 0');
  QrCheque7.SQL.Add('');
  QrCheque7.SQL.Add(dmkPF.SQL_Periodo('AND loi.DDeposito ', 0, Date, False, True));
  //
  QrCheque7.SQL.Add(Ordem);
  QrCheque7. Open;
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrCheque7, Dmod.MyDB, [
  'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ',
  'ent.Nome) NOMECLI, lot.Lote, loi.* ',
  'FROM ' + CO_TabLctA + ' loi ',
  'LEFT JOIN ' + CO_TabLotA + ' lot ON lot.Codigo=loi.FatNum',
  'LEFT JOIN entidades ent ON ent.Codigo=lot.Cliente ',
  'WHERE loi.FatID=' + FormatFloat('0', VAR_FATID_0301),
  'AND lot.Tipo = 0 ',
  'AND loi.RepCli = 0 ',
  'AND loi.Repassado = 0 ',
  'AND loi.Devolucao = 0 ',
  'AND loi.Depositado = ' + Geral.FF0(CO_CH_DEPOSITADO_NAO),
  dmkPF.SQL_Periodo('AND loi.DDeposito ', 0, Date, False, True), 
  Ordem,
  '']);
end;

procedure TFmLot2RepCliMul.ReopenChequeM;
const
  NomeA: array[0..5] of string = ('DDeposito','Credito','Documento',
   'Banco , Agencia , ContaCorrente ','Emitente','NOMECLI');
  NomeD: array[0..5] of string = ('DDeposito DESC','Documento DESC',
    'Credito DESC', 'Banco DESC, Agencia DESC, ContaCorrente DESC',
    'Emitente', 'NOMECLI');
var
  Ordem: String;
  FatParcela, Banco, Agencia, Cheque: Integer;
  Conta, Emit, CPF: String;
  Credito: Double;
  Vcto: TDateTime;
begin
  if QrChequeM.State = dsBrowse then
    FatParcela := QrChequeMFatParcela.Value
  else
    FatParcela := 0;
  Ordem := 'ORDER BY ';
  if CkDescende1.Checked then
    Ordem := Ordem + NomeD[RGOrdem1.ItemIndex]+', '
  else
    Ordem := Ordem + NomeA[RGOrdem1.ItemIndex]+', ';
  if CkDescende2.Checked then
    Ordem := Ordem + NomeD[RGOrdem2.ItemIndex]+', '
  else
    Ordem := Ordem + NomeA[RGOrdem2.ItemIndex]+', ';
  if CkDescende3.Checked then
    Ordem := Ordem + NomeD[RGOrdem3.ItemIndex]
  else
    Ordem := Ordem + NomeA[RGOrdem3.ItemIndex];
  //
  Banco    := EdBancoM.ValueVariant;
  Agencia  := EdAgenciaM.ValueVariant;
  Conta    := EdContaM.Text;
  Cheque   := EdChequeM.ValueVariant;
  Credito    := EdCreditoM.ValueVariant;
  Vcto     := Geral.ValidaDataSimples(EdVctoM.Text, True);
  Emit     := EdEmitM.Text;
  CPF      := Geral.SoNumero_TT(EdCPFM.Text);

{
  QrChequeM.Close;
  QrChequeM.SQL.Clear;
  QrChequeM.SQL.Add('SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
  QrChequeM.SQL.Add('ELSE ent.Nome END NOMECLI, lot.Lote, loi.*');
  QrChequeM.SQL.Add('FROM lot esits loi');
  QrChequeM.SQL.Add('LEFT JOIN lot es lot ON lot.Codigo=loi.Codigo');
  QrChequeM.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=lot.Cliente');
  QrChequeM.SQL.Add('WHERE lot.Tipo = 0');
  QrChequeM.SQL.Add('AND loi.RepCli = 0');
  QrChequeM.SQL.Add('AND loi.Repassado = 0');
  QrChequeM.SQL.Add('AND loi.Devolucao = 0');
  if not CkDepositados.Checked then
    QrChequeM.SQL.Add('AND loi.Depositado = 0');

  if Banco > 0 then
    QrChequeM.SQL.Add('AND loi.Banco='+IntToStr(Banco));

  if Agencia > 0 then
    QrChequeM.SQL.Add('AND loi.Agencia='+IntToStr(Agencia));

  if Conta <> '0000000000' then
    QrChequeM.SQL.Add('AND loi.ContaCorrente="'+Conta+'"');

  if Cheque > 0 then
    QrChequeM.SQL.Add('AND loi.Cheque='+IntToStr(Cheque));

  if Credito > 0 then
  begin
    QrChequeM.SQL.Add('AND loi.Credito=:PVal');
    QrChequeM.ParamByName('PVal').AsFloat := Credito;
  end;

  if Vcto > 0 then
  begin
    QrChequeM.SQL.Add('AND loi.Vencto=:PVct');
    QrChequeM.ParamByName('PVct').AsString := Geral.FDT(Vcto, 1);
  end;
  if Emit <> '' then
    QrChequeM.SQL.Add('AND loi.Emitente LIKE "%'+Emit+'%"');

  if CPF <> '' then
    QrChequeM.SQL.Add('AND loi.CPF="'+CPF+'"');

  QrChequeM.SQL.Add(dmkPF.SQL_Periodo('AND loi.DDeposito ', 0, Date, False, True));
  QrChequeM.SQL.Add('');
  //
  QrChequeM.SQL.Add(Ordem);
  QrChequeM. Open;
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrChequeM, Dmod.MyDB, [
  'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ',
  'ent.Nome) NOMECLI, lot.Lote, loi.* ',
  'FROM ' + CO_TabLctA + ' loi ',
  'LEFT JOIN ' + CO_TabLotA + ' lot ON lot.Codigo=loi.FatNum ',
  'LEFT JOIN entidades ent ON ent.Codigo=lot.Cliente ',
  'WHERE loi.FatID=' + FormatFloat('0', VAR_FATID_0301),
  'AND lot.Tipo = 0 ',
  'AND loi.RepCli = 0 ',
  'AND loi.Repassado = 0 ',
  'AND loi.Devolucao = 0 ',
  Geral.ATS_if(not CkDepositados.Checked, [
  'AND loi.Depositado = ' + Geral.FF0(CO_CH_DEPOSITADO_NAO)]),
  Geral.ATS_if(Banco > 0, [
  'AND loi.Banco=' + FormatFloat('0', Banco)]),
  Geral.ATS_if(Agencia > 0, [
  'AND loi.Agencia="' + Geral.FF0(Agencia) + '"']),
  Geral.ATS_if(Conta <> '0000000000', [
  'AND loi.ContaCorrente="' + Conta + '"']),
  Geral.ATS_if(Cheque > 0, [
  'AND loi.Cheque=' + Geral.FF0(Cheque)]),
  Geral.ATS_if(Credito > 0, [
  'AND loi.Credito=' + Geral.FFT_Dot(Credito, 2, siNegativo)]),
  Geral.ATS_if(Vcto > 0, [
  'AND loi.Vencimento="' + Geral.FDT(Vcto, 1) + '"']),
  Geral.ATS_if(Emit <> '', [
  'AND loi.Emitente LIKE "%' + Emit + '%"']),
  Geral.ATS_if(CPF <> '', [
    'AND loi.CPF="' + CPF + '"']),
  dmkPF.SQL_Periodo('AND loi.DDeposito ', 0, Date, False, True),
  Ordem,
  '']);
  //
  if FatParcela > 0 then QrChequeM.Locate('FatParcela', FatParcela, []);
end;

procedure TFmLot2RepCliMul.ReopenChequeV(Automatico: Boolean);
const
  NomeA: array[0..5] of string = ('Vencimento','Credito','Documento','Banco , Agencia , ContaCorrente ','Emitente','NOMECLI');
  NomeD: array[0..5] of string = ('Vencimento DESC','Documento DESC','Credito DESC','Banco DESC, Agencia DESC, ContaCorrente DESC','Emitente','NOMECLI');
var
  Ordem: String;
  FatParcela: Integer;
  Credito: Double;
begin
  if QrChequeV.State = dsBrowse then
    FatParcela := QrChequeVFatParcela.Value
  else
    FatParcela := 0;
  Ordem := 'ORDER BY ';
  if CkDescende1.Checked then
    Ordem := Ordem + NomeD[RGOrdem1.ItemIndex]+', '
  else
    Ordem := Ordem + NomeA[RGOrdem1.ItemIndex]+', ';
  if CkDescende2.Checked then
    Ordem := Ordem + NomeD[RGOrdem2.ItemIndex]+', '
  else
    Ordem := Ordem + NomeA[RGOrdem2.ItemIndex]+', ';
  if CkDescende3.Checked then
    Ordem := Ordem + NomeD[RGOrdem3.ItemIndex]
  else
    Ordem := Ordem + NomeA[RGOrdem3.ItemIndex];
  //
  Credito    := EdCreditoV.ValueVariant;
{  QrChequeV.Close;
  QrChequeV.SQL.Clear;
  QrChequeV.SQL.Add('SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
  QrChequeV.SQL.Add('ELSE ent.Nome END NOMECLI, lot.Lote, loi.*');
  QrChequeV.SQL.Add('FROM lot esits loi');
  QrChequeV.SQL.Add('LEFT JOIN lot es lot ON lot.Codigo=loi.Codigo');
  QrChequeV.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=lot.Cliente');
  QrChequeV.SQL.Add('WHERE lot.Tipo = 0');
  QrChequeV.SQL.Add('AND loi.RepCli = 0');
  QrChequeV.SQL.Add('AND loi.Repassado = 0');
  QrChequeV.SQL.Add('AND loi.Devolucao = 0');
  QrChequeV.SQL.Add('AND loi.Depositado = 0');
    QrChequeV.SQL.Add('AND loi.Credito=:PVal');
    QrChequeV.ParamByName('PVal').AsFloat := Credito;
  QrChequeV.SQL.Add(dmkPF.SQL_Periodo('AND loi.DDeposito ', 0, Date, False, True));
  QrChequeV.SQL.Add('');
  //
  QrChequeV.SQL.Add(Ordem);
  QrChequeV. Open;
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrChequeV, Dmod.MyDB, [
  'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ',
  'ent.Nome) NOMECLI, lot.Lote, loi.* ',
  'FROM ' + CO_TabLctA + ' loi ',
  'LEFT JOIN ' + CO_TabLotA + ' lot ON lot.Codigo=loi.FatNum',
  'LEFT JOIN entidades ent ON ent.Codigo=lot.Cliente ',
  'WHERE loi.FatID=' + Geral.FF0(VAR_FATID_0301),
  'AND lot.Tipo = 0 ',
  'AND loi.RepCli = 0 ',
  'AND loi.Repassado = 0 ',
  'AND loi.Devolucao = 0 ',
  'AND loi.Depositado = ' + Geral.FF0(CO_CH_DEPOSITADO_NAO),
  'AND loi.Credito=' + Geral.FFT_Dot(Credito, 2, siNegativo),
  dmkPF.SQL_Periodo('AND loi.DDeposito ', 0, Date, False, True),
  Ordem,
  '']);
  //
  if FatParcela > 0 then QrChequeV.Locate('FatParcela', FatParcela, []);
  if (QrChequeV.RecordCount = 1) and CkAuto.Checked and Automatico then
  begin
    Executa(1);
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'R$ ' +
      Geral.FFT(QrChequeVCredito.Value, 2, siPositivo) +
      ' localizado e lan�ado');
    LaAviso2.Font.Color := clBlue;
    EdCreditoV.SetFocus;
  end else begin
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Encontrado ' +
      IntToStr(QrChequeV.RecordCount) + ' registro(s)');
    LaAviso2.Font.Color := clRed;
    Beep;
  end;
end;

procedure TFmLot2RepCliMul.ReopenCaixa;
begin
{
  QrCaixa.Close;
  QrCaixa.Params[0].AsString := Geral.FDT(Date, 1);
  QrCaixa. Open;
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrCaixa, Dmod.MyDB, [
  'SELECT SUM(Credito) TOTAL ',
  'FROM ' + CO_TabLctA + ' loi ',
  'LEFT JOIN ' + CO_TabLotA + ' lot ON lot.Codigo=loi.FatNum ',
  'WHERE loi.FatID=' + Geral.FF0(VAR_FATID_0301),
  'AND lot.Tipo = 0 ',
  'AND loi.RepCli = 0 ',
  'AND loi.Repassado = 0 ',
  'AND loi.Depositado = ' + Geral.FF0(CO_CH_DEPOSITADO_NAO),
  'AND loi.Devolucao = 0 ',
  'AND loi.DDeposito <= "' + Geral.FDT(Date, 1) + '" ',
  '']);
end;

procedure TFmLot2RepCliMul.EdBancoMExit(Sender: TObject);
begin
  ReopenChequeM;
end;

procedure TFmLot2RepCliMul.EdAgenciaMExit(Sender: TObject);
begin
  ReopenChequeM;
end;

procedure TFmLot2RepCliMul.EdContaMExit(Sender: TObject);
begin
  ReopenChequeM;
end;

procedure TFmLot2RepCliMul.EdChequeMExit(Sender: TObject);
begin
  ReopenChequeM;
end;

procedure TFmLot2RepCliMul.EdCreditoMExit(Sender: TObject);
begin
  ReopenChequeM;
end;

procedure TFmLot2RepCliMul.EdVctoMExit(Sender: TObject);
var
  Data: TDateTime;
begin
  Data := Geral.ValidaDataSimples(EdVctoM.Text, True);
  if Data > 0 then
    EdVctoM.Text := FormatDateTime('dd/mm/yyyy', Data)
  else
    EdVctoM.Text := '000000';
  ReopenChequeM;
end;

procedure TFmLot2RepCliMul.EdCPFMExit(Sender: TObject);
begin
  ReopenChequeM;
end;

procedure TFmLot2RepCliMul.EdEmitMExit(Sender: TObject);
begin
  ReopenChequeM;
end;

procedure TFmLot2RepCliMul.CkDepositadosClick(Sender: TObject);
begin
  ReopenChequeM;
end;

procedure TFmLot2RepCliMul.EdCreditoVExit(Sender: TObject);
begin
  ReopenChequeV(True);
end;

end.

