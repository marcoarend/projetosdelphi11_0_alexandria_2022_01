unit Inadimps;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DBCtrls, Db, mySQLDbTables, ComCtrls,
  Grids, DBGrids, Mask, frxClass, frxDBSet, Variants, dmkGeral, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, dmkImage, UnDmkProcFunc, DmkDAC_PF, UnDmkEnums;

type
  TFmInadimps = class(TForm)
    QrClientes: TmySQLQuery;
    QrClientesNOMECLIENTE: TWideStringField;
    QrClientesCodigo: TIntegerField;
    DsClientes: TDataSource;
    QrBancos: TmySQLQuery;
    DsBancos: TDataSource;
    QrBancosCodigo: TIntegerField;
    QrBancosNome: TWideStringField;
    QrBancosSite: TWideStringField;
    QrBancosLk: TIntegerField;
    QrBancosDataCad: TDateField;
    QrBancosDataAlt: TDateField;
    QrBancosUserCad: TIntegerField;
    QrBancosUserAlt: TIntegerField;
    QrBancosDVCC: TSmallintField;
    QrPesq1: TmySQLQuery;
    QrPesq1Valor: TFloatField;
    QrPesq1Pago: TFloatField;
    QrPesq1PGDDeposito: TFloatField;
    QrPesq1PGATRAZO: TFloatField;
    QrPesq1PGABERTO: TFloatField;
    QrPesq1PERIODO: TFloatField;
    QrPesq1NOMECLI: TWideStringField;
    QrPesq1NOMEBANCO: TWideStringField;
    QrPesq1DDeposito: TDateField;
    QrPesq1NOMEEMITENTE: TWideStringField;
    QrDef: TmySQLQuery;
    QrDefMinimo: TDateField;
    QrDefMaximo: TDateField;
    QrEmitCPF: TmySQLQuery;
    QrEmitCPFNome: TWideStringField;
    DsEmitCPF: TDataSource;
    QrPesq1ITENS: TLargeintField;
    QrPesq1ITENS_AVencer: TFloatField;
    QrPesq1ITENS_Vencidos: TFloatField;
    QrPesq1ITENS_PgVencto: TFloatField;
    QrPesq1ITENS_Devolvid: TFloatField;
    QrPesq1ITENS_DevPgTot: TFloatField;
    QrPesq1ITENS_DevNaoQt: TFloatField;
    PageControl1: TPageControl;
    Panel1: TPanel;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel2: TPanel;
    Panel4: TPanel;
    Label2: TLabel;
    EdCPF1: TdmkEdit;
    DBEdEmitente: TDBEdit;
    Label14: TLabel;
    EdCPF2: TdmkEdit;
    DBEdSacado: TDBEdit;
    QrSacado: TmySQLQuery;
    DsSacado: TDataSource;
    QrSacadoNome: TWideStringField;
    Panel3: TPanel;
    Panel6: TPanel;
    Label75: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    TPIni: TDateTimePicker;
    Label4: TLabel;
    Label5: TLabel;
    TPFim: TDateTimePicker;
    EdBanco: TdmkEditCB;
    Label3: TLabel;
    CBBanco: TdmkDBLookupComboBox;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    QrPesq1EXPIRADO: TFloatField;
    QrPesq1AEXPIRAR: TFloatField;
    QrPesq1VENCIDO: TFloatField;
    TabSheet3: TTabSheet;
    Label15: TLabel;
    frxInad1: TfrxReport;
    frxDsPesq1: TfrxDBDataset;
    frxInad2: TfrxReport;
    RGOrdem1: TRadioGroup;
    RGOrdem2: TRadioGroup;
    RGOrdem3: TRadioGroup;
    RGOrdem4: TRadioGroup;
    RGGrupos: TRadioGroup;
    RGSintetico: TRadioGroup;
    RGImpressao: TRadioGroup;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel5: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel7: TPanel;
    BtOK: TBitBtn;
    Edit1: TEdit;
    Label6: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdCPF1Exit(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure frInad1_2UserFunction(const Name: String; p1, p2, p3: Variant;
      var Val: Variant);
    procedure frInad1_1GetValue(const ParName: String;
      var ParValue: Variant);
    procedure QrPesq1AfterOpen(DataSet: TDataSet);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCPF2Exit(Sender: TObject);
    procedure frxInad1GetValue(const VarName: String;
      var Value: Variant);
  private
    { Private declarations }
    procedure Pesquisa(Abre: Boolean);
    procedure Pesquisa1(Abre: Boolean);
    procedure Pesquisa2(Abre: Boolean);
    procedure Pesquisa3(Abre: Boolean);
    procedure ReopenEmitente(CPF: String);
    procedure ReopenSacado(CNPJ: String);
  public
    { Public declarations }
  end;

  var
  FmInadimps: TFmInadimps;

implementation

uses UnInternalConsts, PesqCPF, Module, PesqCNPJ, UnMyObjects, UMySQLModule,
  MyListas;

{$R *.DFM}

procedure TFmInadimps.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmInadimps.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmInadimps.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmInadimps.EdClienteChange(Sender: TObject);
begin
  Pesquisa(False);
end;

procedure TFmInadimps.FormCreate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  ImgTipo.SQLType := stPsq;
  //
(*
  VAR_TEXT_FormMsg := '';
  VAR_INFO_FormMsg := True;
  UMyMod.AbreQuery(QrClientes, 'TFmInadimps.FormCreate()');
  VAR_INFO_FormMsg := False;
  Geral.MensagemBox(VAR_TEXT_FormMsg, 'Teste', MB_OK+MB_ICONINFORMATION);
*)
  UMyMod.AbreQuery(QrClientes, Dmod.MyDB, 'TFmInadimps.FormCreate()');
  UMyMod.AbreQuery(QrBancos, Dmod.MyDB, 'TFmInadimps.FormCreate()');
  //
  TPIni.Date := Date - 30;
  TPFim.Date := Date;
  //
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmInadimps.EdCPF1Exit(Sender: TObject);
begin
  ReopenEmitente(Geral.SoNumero_TT(EdCPF1.Text));
  Pesquisa(False);
end;

procedure TFmInadimps.BtOKClick(Sender: TObject);
begin
  Pesquisa(True);
end;

procedure TFmInadimps.Pesquisa(Abre: Boolean);
begin
  case PageControl1.ActivePageIndex of
    0: Pesquisa1(Abre);
    1: Pesquisa2(Abre);
    2: Pesquisa3(Abre);
  end;
end;

procedure TFmInadimps.Pesquisa1(Abre: Boolean);
const
  ItensOrdem: array[0..4] of String = ('NOMECLI', 'NOMEEMITENTE', 'NOMEBANCO',
  'Periodo', 'DDeposito');
var
  i: Integer;
  Grupos: String;
begin
  QrPesq1.Close;
  if not Abre then Exit;
  //////////////////////////////////////////////////////////////////////////////
  Screen.Cursor := crHourGlass;
{
  QrPesq1.SQL.Clear;
  QrPesq1.SQL.Add('SELECT SUM(loi.Valor) Valor, COUNT(loi.Controle) ITENS,');
  QrPesq1.SQL.Add('SUM(IF(loi.DDeposito > SYSDATE(), 1, 0)) ITENS_AVencer,');
  QrPesq1.SQL.Add('SUM(IF((loi.DDeposito < SYSDATE()), 1, 0)) ITENS_Vencidos,');
  QrPesq1.SQL.Add('SUM(IF((loi.DDeposito < SYSDATE()), 1, 0)) -');
  QrPesq1.SQL.Add('SUM(IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) > 0),');
  QrPesq1.SQL.Add('1, 0)) ITENS_PgVencto,');
  QrPesq1.SQL.Add('SUM(IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) > 0),');
  QrPesq1.SQL.Add('1, 0)) ITENS_Devolvid,');
  QrPesq1.SQL.Add('SUM(IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) = 12),');
  QrPesq1.SQL.Add('1, 0)) ITENS_DevPgTot,');
  QrPesq1.SQL.Add('SUM(IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) in (10, 11)),');
  QrPesq1.SQL.Add('1, 0)) ITENS_DevNaoQt,');
  QrPesq1.SQL.Add('SUM((chd.ValPago - chd.Taxas - chd.JurosV + chd.Desconto)) Pago,');
  QrPesq1.SQL.Add('SUM(IF(loi.DDeposito < SYSDATE(), loi.Valor, 0)) EXPIRADO,');
  QrPesq1.SQL.Add('SUM(IF(loi.DDeposito < SYSDATE(), 0, loi.Valor)) AEXPIRAR,');
  QrPesq1.SQL.Add('SUM(IF(loi.DDeposito < SYSDATE() AND (chd.Status+10) > 0, ');
  QrPesq1.SQL.Add('loi.Valor - IF(loi.DDeposito < SYSDATE(), 0, loi.Valor), 0)) VENCIDO,');
  QrPesq1.SQL.Add('SUM(IF(loi.DDeposito < SYSDATE() AND (chd.Status+10) > 0, 0,');
  QrPesq1.SQL.Add('loi.Valor - IF(loi.DDeposito < SYSDATE(), 0, loi.Valor))) PGDDEPOSITO,');
  QrPesq1.SQL.Add('SUM(IF(loi.DDeposito < SYSDATE() AND (chd.Status+10) > 0,');
  QrPesq1.SQL.Add('chd.ValPago - chd.Taxas - chd.JurosV + chd.Desconto,0)) PGATRAZO,');
  QrPesq1.SQL.Add('SUM(IF(loi.DDeposito < SYSDATE() AND (chd.Status+10) > 0,');
  QrPesq1.SQL.Add('loi.Valor - (chd.ValPago - chd.Taxas - chd.JurosV) +');
  QrPesq1.SQL.Add('chd.Desconto, 0)) PGABERTO,');
  QrPesq1.SQL.Add('MONTH(loi.DDeposito)+YEAR(loi.DDeposito)*100+0.00 PERIODO,');
  QrPesq1.SQL.Add('CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial ELSE cli.Nome END NOMECLI,');
  QrPesq1.SQL.Add('ban.Nome NOMEBANCO, loi.DDeposito, loi.Emitente NOMEEMITENTE');
  QrPesq1.SQL.Add('FROM lot esits loi');
  QrPesq1.SQL.Add('LEFT JOIN lot es     lot ON lot.Codigo=loi.Codigo');
  QrPesq1.SQL.Add('LEFT JOIN entidades cli ON cli.Codigo=lot.Cliente');
  QrPesq1.SQL.Add('LEFT JOIN alinits   chd ON chd.ChequeOrigem=loi.Controle');
  QrPesq1.SQL.Add('LEFT JOIN bancos    ban ON ban.Codigo=loi.Banco');
  QrPesq1.SQL.Add('WHERE lot.Tipo in (0, 2)');
  QrPesq1.SQL.Add('');
  QrPesq1.SQL.Add(dmkPF.SQL_Periodo('AND loi.DDeposito ', TPIni.Date, TPFim.Date, True, True));
  QrPesq1.SQL.Add('');
  //////////////////////////////////////////////////////////////////////////////
  if Trim(EdCliente.Text) <> '' then
    QrPesq1.SQL.Add('AND lot.Cliente='+EdCliente.Text);
  if Geral.IMV(EdBanco.Text) <> 0 then
    QrPesq1.SQL.Add('AND loi.Banco='+EdBanco.Text);
  if EdCPF1.Text <> '' then
    QrPesq1.SQL.Add('AND loi.CPF="'+Geral.SoNumero_TT(EdCPF1.Text)+'"');
  //////////////////////////////////////////////////////////////////////////////
  Grupos := 'GROUP BY ';
  for i := 0 to RGGrupos.ItemIndex do
  begin
    if i > 0 then Grupos := Grupos + ', ';
    //Grupos := Grupos + ItensOrdem[TRadioGroup('RGOrdem'+IntToStr(i+1)).ItemIndex];
    case i of
      0: Grupos := Grupos + ItensOrdem[RGOrdem1.ItemIndex];
      1: Grupos := Grupos + ItensOrdem[RGOrdem2.ItemIndex];
      2: Grupos := Grupos + ItensOrdem[RGOrdem3.ItemIndex];
      3: Grupos := Grupos + ItensOrdem[RGOrdem4.ItemIndex];
      //4: Grupos := Grupos + ItensOrdem[RGOrdem5.ItemIndex];
    end;
  end;
  QrPesq1.SQL.Add(Grupos);
  //////////////////////////////////////////////////////////////////////////////
  QrPesq1.SQL.Add('ORDER BY '+
    ItensOrdem[RGOrdem1.ItemIndex]+', '+
    ItensOrdem[RGOrdem2.ItemIndex]+', '+
    ItensOrdem[RGOrdem3.ItemIndex]+', '+
    ItensOrdem[RGOrdem4.ItemIndex]);
  QrPesq1. Open;
}
 //////////////////////////////////////////////////////////////////////////////
  Grupos := 'GROUP BY ';
  for i := 0 to RGGrupos.ItemIndex do
  begin
    if i > 0 then Grupos := Grupos + ', ';
    //Grupos := Grupos + ItensOrdem[TRadioGroup('RGOrdem'+IntToStr(i+1)).ItemIndex];
    case i of
      0: Grupos := Grupos + ItensOrdem[RGOrdem1.ItemIndex];
      1: Grupos := Grupos + ItensOrdem[RGOrdem2.ItemIndex];
      2: Grupos := Grupos + ItensOrdem[RGOrdem3.ItemIndex];
      3: Grupos := Grupos + ItensOrdem[RGOrdem4.ItemIndex];
      //4: Grupos := Grupos + ItensOrdem[RGOrdem5.ItemIndex];
    end;
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq1, Dmod.MyDB, [
  'SELECT SUM(loi.Credito) Valor, COUNT(loi.FatParcela) ITENS, ',
  'SUM(IF(loi.DDeposito > SYSDATE(), 1, 0)) ITENS_AVencer, ',
  'SUM(IF((loi.DDeposito < SYSDATE()), 1, 0)) ITENS_Vencidos, ',
  'SUM(IF((loi.Quitado>1) AND (loi.DDeposito>=loi.Data3), 1, 0)) ',
  'ITENS_PgVencto, ',
  'SUM(IF((loi.Quitado<2) AND (loi.DDeposito< SYSDATE()), 1, 0)) + ',
  'SUM(IF((loi.Quitado>1) AND (loi.DDeposito< loi.Data3), 1, 0)) ',
  'ITENS_Devolvid, ',
  'SUM(IF((loi.Quitado>1) AND (loi.DDeposito< loi.Data3), 1, 0)) ',
  'ITENS_DevPgTot, ',
  'SUM(IF((loi.Quitado<2) AND (loi.DDeposito< SYSDATE()), 1, 0)) ',
  'ITENS_DevNaoQt, ',
  'SUM((loi.TotalPg - loi.TotalJr + loi.TotalDs)) Pago, ',
  'SUM(IF(loi.DDeposito < SYSDATE(), loi.Credito, 0)) EXPIRADO, ',
  'SUM(IF(loi.DDeposito < SYSDATE(), 0, loi.Credito)) AEXPIRAR, ',
  'SUM(IF((loi.DDeposito<SYSDATE()) AND ((loi.DDeposito= 0) ',
  '  OR (loi.DDeposito<loi.Data3)), loi.Credito, 0)) VENCIDO, ',
  'SUM(IF((loi.Quitado>1) AND (loi.DDeposito>=loi.Data3), ',
  '  loi.TotalPg - loi.TotalJr + loi.TotalDs, 0)) PGDDEPOSITO, ',
  'SUM(IF(/*(loi.Quitado> 1)AND*/ (loi.DDeposito< loi.Data3), ',
  '  loi.TotalPg - loi.TotalJr + loi.TotalDs, 0)) PGATRAZO, ',
  'SUM(IF((loi.Quitado< 2)AND (loi.DDeposito< SYSDATE()), ',
  '  loi.Credito - (loi.TotalPg - loi.TotalJr) + loi.TotalDs, 0)) ',
  '  PGABERTO, ',
  'MONTH(loi.DDeposito)+YEAR(loi.DDeposito)*100+0.00 PERIODO, ',
  'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NOMECLI, ',
  'ban.Nome NOMEBANCO, loi.DDeposito, loi.Emitente NOMEEMITENTE ',
  'FROM ' + CO_TabLctA + ' loi ',
  'LEFT JOIN ' + CO_TabLotA + ' lot ON lot.Codigo=loi.FatNum ',
  'LEFT JOIN entidades cli ON cli.Codigo=lot.Cliente ',
  'LEFT JOIN bancos    ban ON ban.Codigo=loi.Banco ',
  'WHERE loi.FatID=' + FormatFloat('0', VAR_FATID_0301),
  'AND lot.Tipo in (0, 2) ',
  dmkPF.SQL_Periodo('AND loi.DDeposito ', TPIni.Date, TPFim.Date, True, True),
  //////////////////////////////////////////////////////////////////////////////
  Geral.ATS_if(Trim(EdCliente.Text) <> '', [
  'AND lot.Cliente=' + EdCliente.Text]),
  Geral.ATS_if(Geral.IMV(EdBanco.Text) <> 0, [
    'AND loi.Banco=' + EdBanco.Text]),
  Geral.ATS_if(EdCPF1.Text <> '', [
    'AND loi.CNPJCPF="' + Geral.SoNumero_TT(EdCPF1.Text) + '"']),
  Grupos,
  //////////////////////////////////////////////////////////////////////////////
  'ORDER BY '+
    ItensOrdem[RGOrdem1.ItemIndex]+', '+
    ItensOrdem[RGOrdem2.ItemIndex]+', '+
    ItensOrdem[RGOrdem3.ItemIndex]+', '+
    ItensOrdem[RGOrdem4.ItemIndex],
  '']);
  //
  Screen.Cursor := crDefault;
  //emitentes obtem registros duplicados pelo CPF!!!
  case RGImpressao.ItemIndex of
      //valor revendido e pendente sobre o vencido (n�o pago)
    0: MyObjects.frxMostra(frxInad1, 'Inadimpl�ncia');
    1: MyObjects.frxMostra(frxInad2, 'Inadimpl�ncia');
  end;
end;

procedure TFmInadimps.Pesquisa2(Abre: Boolean);
const
  ItensOrdem: array[0..4] of String = ('NOMECLI', 'NOMEEMITENTE', 'NOMEBANCO',
  'Periodo', 'DDeposito');
var
  i: Integer;
  Grupos: String;
begin
  //Data3<=DDeposito para vencidos n�o pagos em Lot esIts
  QrPesq1.Close;
  if not Abre then Exit;
  //////////////////////////////////////////////////////////////////////////////
  Screen.Cursor := crHourGlass;
{
  QrPesq1.SQL.Clear;
  QrPesq1.SQL.Add('SELECT SUM(loi.Valor) Valor, COUNT(loi.Controle) ITENS,');
  QrPesq1.SQL.Add('SUM(IF(loi.DDeposito > SYSDATE(), 1, 0)) ITENS_AVencer,');
  QrPesq1.SQL.Add('SUM(IF((loi.DDeposito < SYSDATE()), 1, 0)) ITENS_Vencidos,');
  QrPesq1.SQL.Add('SUM(IF((loi.Quitado>1) AND (loi.DDeposito>=loi.Data3), 1, 0)) ITENS_PgVencto,');
  QrPesq1.SQL.Add('SUM(IF((loi.Quitado<2) AND (loi.DDeposito< SYSDATE()), 1, 0)) +');
  QrPesq1.SQL.Add('SUM(IF((loi.Quitado>1) AND (loi.DDeposito< loi.Data3), 1, 0)) ITENS_Devolvid,');
  QrPesq1.SQL.Add('SUM(IF((loi.Quitado>1) AND (loi.DDeposito< loi.Data3), 1, 0)) ITENS_DevPgTot,');
  QrPesq1.SQL.Add('SUM(IF((loi.Quitado<2) AND (loi.DDeposito< SYSDATE()), 1, 0)) ITENS_DevNaoQt,');
  QrPesq1.SQL.Add('SUM((loi.TotalPg - loi.TotalJr + loi.TotalDs)) Pago,');
  QrPesq1.SQL.Add('SUM(IF(loi.DDeposito < SYSDATE(), loi.Valor, 0)) EXPIRADO,');
  QrPesq1.SQL.Add('SUM(IF(loi.DDeposito < SYSDATE(), 0, loi.Valor)) AEXPIRAR,');
  QrPesq1.SQL.Add('SUM(IF((loi.DDeposito<SYSDATE()) AND ((loi.Data3<2)');
  QrPesq1.SQL.Add('  OR (loi.DDeposito<loi.Data3)), loi.Valor, 0)) VENCIDO,');
  QrPesq1.SQL.Add('SUM(IF((loi.Quitado>1) AND (loi.DDeposito>=loi.Data3),');
  QrPesq1.SQL.Add('  loi.TotalPg - loi.TotalJr + loi.TotalDs, 0)) PGDDEPOSITO,');
  QrPesq1.SQL.Add('SUM(IF(/*(loi.Quitado> 1)AND*/ (loi.DDeposito< loi.Data3),');
  QrPesq1.SQL.Add('  loi.TotalPg - loi.TotalJr + loi.TotalDs, 0)) PGATRAZO,');
  QrPesq1.SQL.Add('SUM(IF((loi.Quitado< 2)AND (loi.DDeposito< SYSDATE()),');
  QrPesq1.SQL.Add('  loi.Valor - (loi.TotalPg - loi.TotalJr) + loi.TotalDs, 0)) PGABERTO,');
  QrPesq1.SQL.Add('MONTH(loi.DDeposito)+YEAR(loi.DDeposito)*100+0.00 PERIODO,');
  QrPesq1.SQL.Add('CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial ELSE cli.Nome END NOMECLI,');
  QrPesq1.SQL.Add('ban.Nome NOMEBANCO, loi.DDeposito, loi.Emitente NOMEEMITENTE');
  QrPesq1.SQL.Add('');
  QrPesq1.SQL.Add('FROM lot esits loi');
  QrPesq1.SQL.Add('LEFT JOIN lot es     lot ON lot.Codigo=loi.Codigo');
  QrPesq1.SQL.Add('LEFT JOIN entidades cli ON cli.Codigo=lot.Cliente');
  QrPesq1.SQL.Add('LEFT JOIN bancos    ban ON ban.Codigo=loi.Banco');
  QrPesq1.SQL.Add('WHERE lot.Tipo=1');
  QrPesq1.SQL.Add('');
  QrPesq1.SQL.Add(dmkPF.SQL_Periodo('AND loi.DDeposito ', TPIni.Date, TPFim.Date, True, True));
  QrPesq1.SQL.Add('');
  //////////////////////////////////////////////////////////////////////////////
  if Trim(EdCliente.Text) <> '' then
    QrPesq1.SQL.Add('AND lot.Cliente='+EdCliente.Text);
  if Geral.IMV(EdBanco.Text) <> 0 then
    QrPesq1.SQL.Add('AND loi.Banco='+EdBanco.Text);
  if EdCPF2.Text <> '' then
    QrPesq1.SQL.Add('AND loi.CPF="'+Geral.SoNumero_TT(EdCPF2.Text)+'"');
  //////////////////////////////////////////////////////////////////////////////
  Grupos := 'GROUP BY ';
  for i := 0 to RGGrupos.ItemIndex do
  begin
    if i > 0 then Grupos := Grupos + ', ';
    //Grupos := Grupos + ItensOrdem[TRadioGroup('RGOrdem'+IntToStr(i+1)).ItemIndex];
    case i of
      0: Grupos := Grupos + ItensOrdem[RGOrdem1.ItemIndex];
      1: Grupos := Grupos + ItensOrdem[RGOrdem2.ItemIndex];
      2: Grupos := Grupos + ItensOrdem[RGOrdem3.ItemIndex];
      3: Grupos := Grupos + ItensOrdem[RGOrdem4.ItemIndex];
      //4: Grupos := Grupos + ItensOrdem[RGOrdem5.ItemIndex];
    end;
  end;
  QrPesq1.SQL.Add(Grupos);
  //////////////////////////////////////////////////////////////////////////////
  QrPesq1.SQL.Add('ORDER BY '+
    ItensOrdem[RGOrdem1.ItemIndex]+', '+
    ItensOrdem[RGOrdem2.ItemIndex]+', '+
    ItensOrdem[RGOrdem3.ItemIndex]+', '+
    ItensOrdem[RGOrdem4.ItemIndex]);
  UMyMod.AbreQuery(QrPesq1);
}
  //////////////////////////////////////////////////////////////////////////////
  Grupos := 'GROUP BY ';
  for i := 0 to RGGrupos.ItemIndex do
  begin
    if i > 0 then Grupos := Grupos + ', ';
    //Grupos := Grupos + ItensOrdem[TRadioGroup('RGOrdem'+IntToStr(i+1)).ItemIndex];
    case i of
      0: Grupos := Grupos + ItensOrdem[RGOrdem1.ItemIndex];
      1: Grupos := Grupos + ItensOrdem[RGOrdem2.ItemIndex];
      2: Grupos := Grupos + ItensOrdem[RGOrdem3.ItemIndex];
      3: Grupos := Grupos + ItensOrdem[RGOrdem4.ItemIndex];
      //4: Grupos := Grupos + ItensOrdem[RGOrdem5.ItemIndex];
    end;
  end;
  //////////////////////////////////////////////////////////////////////////////
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq1, Dmod.MyDB, [
  'SELECT SUM(loi.Credito) Valor, COUNT(loi.FatParcela) ITENS, ',
  'SUM(IF(loi.DDeposito > SYSDATE(), 1, 0)) ITENS_AVencer, ',
  'SUM(IF((loi.DDeposito < SYSDATE()), 1, 0)) ITENS_Vencidos, ',
  'SUM(IF((loi.Quitado>1) AND (loi.DDeposito>=loi.Data3), 1, 0)) ',
  'ITENS_PgVencto, ',
  'SUM(IF((loi.Quitado<2) AND (loi.DDeposito< SYSDATE()), 1, 0)) + ',
  'SUM(IF((loi.Quitado>1) AND (loi.DDeposito< loi.Data3), 1, 0)) ',
  'ITENS_Devolvid, ',
  'SUM(IF((loi.Quitado>1) AND (loi.DDeposito< loi.Data3), 1, 0)) ',
  'ITENS_DevPgTot, ',
  'SUM(IF((loi.Quitado<2) AND (loi.DDeposito< SYSDATE()), 1, 0)) ',
  'ITENS_DevNaoQt, ',
  'SUM((loi.TotalPg - loi.TotalJr + loi.TotalDs)) Pago, ',
  'SUM(IF(loi.DDeposito < SYSDATE(), loi.Credito, 0)) EXPIRADO, ',
  'SUM(IF(loi.DDeposito < SYSDATE(), 0, loi.Credito)) AEXPIRAR, ',
  'SUM(IF((loi.DDeposito<SYSDATE()) AND ((loi.Data3<2) ',
  '  OR (loi.DDeposito<loi.Data3)), loi.Credito, 0)) VENCIDO, ',
  'SUM(IF((loi.Quitado>1) AND (loi.DDeposito>=loi.Data3), ',
  '  loi.TotalPg - loi.TotalJr + loi.TotalDs, 0)) PGDDEPOSITO, ',
  'SUM(IF(/*(loi.Quitado> 1)AND*/ (loi.DDeposito< loi.Data3), ',
  '  loi.TotalPg - loi.TotalJr + loi.TotalDs, 0)) PGATRAZO, ',
  'SUM(IF((loi.Quitado< 2)AND (loi.DDeposito< SYSDATE()), ',
  '  loi.Credito - (loi.TotalPg - loi.TotalJr) + loi.TotalDs, 0))  ',
  'PGABERTO, MONTH(loi.DDeposito)+YEAR(loi.DDeposito)*100+0.00 PERIODO, ',
  'CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial ELSE cli.Nome END NOMECLI, ',
  'ban.Nome NOMEBANCO, loi.DDeposito, loi.Emitente NOMEEMITENTE ',
  'FROM ' + CO_TabLctA + ' loi ',
  'LEFT JOIN ' + CO_TabLotA + ' lot ON lot.Codigo=loi.FatNum ',
  'LEFT JOIN entidades cli ON cli.Codigo=lot.Cliente ',
  'LEFT JOIN bancos    ban ON ban.Codigo=loi.Banco ',
  'WHERE loi.FatID=' + FormatFloat('0', VAR_FATID_0301),
  'AND lot.Tipo=1 ',
  dmkPF.SQL_Periodo('AND loi.DDeposito ', TPIni.Date, TPFim.Date, True, True),
  //////////////////////////////////////////////////////////////////////////////
  Geral.ATS_if(Trim(EdCliente.Text) <> '', [
  'AND lot.Cliente=' + EdCliente.Text]),
  Geral.ATS_if(Geral.IMV(EdBanco.Text) <> 0, [
    'AND loi.Banco=' + EdBanco.Text]),
  Geral.ATS_if(EdCPF2.Text <> '', [
    'AND loi.CNPJCPF="' + Geral.SoNumero_TT(EdCPF2.Text) + '"']),
  Grupos,
  //////////////////////////////////////////////////////////////////////////////
  'ORDER BY '+
    ItensOrdem[RGOrdem1.ItemIndex]+', '+
    ItensOrdem[RGOrdem2.ItemIndex]+', '+
    ItensOrdem[RGOrdem3.ItemIndex]+', '+
    ItensOrdem[RGOrdem4.ItemIndex],
  '']);
  //
  Screen.Cursor := crDefault;
  //emitentes obtem registros duplicados pelo CPF!!!
  case RGImpressao.ItemIndex of
    0: MyObjects.frxMostra(frxInad1, 'Inadimpl�ncia');
    1: MyObjects.frxMostra(frxInad2, 'Inadimpl�ncia');
  end;
end;

procedure TFmInadimps.Pesquisa3(Abre: Boolean);
const
  ItensOrdem: array[0..4] of String = ('NOMECLI', 'NOMEEMITENTE', 'NOMEBANCO',
  'Periodo', 'DDeposito');
var
  i: Integer;
  Grupos: String;
begin
  QrPesq1.Close;
  if not Abre then Exit;
  //////////////////////////////////////////////////////////////////////////////
  Screen.Cursor := crHourGlass;
{
  QrPesq1.SQL.Clear;
  QrPesq1.SQL.Add('SELECT SUM(loi.Valor) Valor, COUNT(loi.Controle) ITENS,');
  QrPesq1.SQL.Add('SUM(IF(loi.DDeposito > SYSDATE(), 1, 0)) ITENS_AVencer,');
  QrPesq1.SQL.Add('SUM(IF((loi.DDeposito < SYSDATE()), 1, 0)) ITENS_Vencidos,');
  QrPesq1.SQL.Add('SUM(IF((loi.DDeposito < SYSDATE()) AND (lot.Tipo in (0,2)), 1, 0)) -');
  QrPesq1.SQL.Add('SUM(IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) > 0)');
  QrPesq1.SQL.Add('AND (lot.Tipo in (0,2)), 1, 0)) + SUM(IF((loi.Quitado>1) AND');
  QrPesq1.SQL.Add('(loi.DDeposito>=loi.Data3) AND (lot.Tipo=1), 1, 0)) ITENS_PgVencto,');
  QrPesq1.SQL.Add('SUM(IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) > 0)');
  QrPesq1.SQL.Add('AND (lot.Tipo in (0,2)), 1, 0)) + SUM(IF((loi.Quitado<2) AND');
  QrPesq1.SQL.Add('(loi.DDeposito< SYSDATE()) AND (lot.Tipo=1), 1, 0)) +');
  QrPesq1.SQL.Add('SUM(IF((loi.Quitado>1) AND (loi.DDeposito< loi.Data3) AND');
  QrPesq1.SQL.Add('(lot.Tipo=1), 1, 0)) ITENS_Devolvid,');
  QrPesq1.SQL.Add('SUM(IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) = 12)');
  QrPesq1.SQL.Add('AND (lot.Tipo in (0,2)), 1, 0)) + SUM(IF((loi.Quitado>1) AND');
  QrPesq1.SQL.Add('(loi.DDeposito< loi.Data3) AND (lot.Tipo=1), 1, 0)) ITENS_DevPgTot,');
  QrPesq1.SQL.Add('SUM(IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) in (10, 11))');
  QrPesq1.SQL.Add('AND (lot.Tipo in (0,2)), 1, 0)) + SUM(IF((loi.Quitado<2) AND');
  QrPesq1.SQL.Add('(loi.DDeposito< SYSDATE()) AND (lot.Tipo=1), 1, 0)) ITENS_DevNaoQt,');
  QrPesq1.SQL.Add('SUM(IF(lot.Tipo in (0,2), (chd.ValPago - chd.Taxas - chd.JurosV +');
  QrPesq1.SQL.Add('chd.Desconto), 0)) + SUM(IF(lot.Tipo = 1, (loi.TotalPg -');
  QrPesq1.SQL.Add('loi.TotalJr + loi.TotalDs), 0)) Pago,');
  QrPesq1.SQL.Add('SUM(IF(loi.DDeposito < SYSDATE(), loi.Valor, 0)) EXPIRADO,');
  QrPesq1.SQL.Add('SUM(IF(loi.DDeposito < SYSDATE(), 0, loi.Valor)) AEXPIRAR,');
  QrPesq1.SQL.Add('SUM(IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) > 0)');
  QrPesq1.SQL.Add('AND (lot.Tipo in (0,2)),  loi.Valor - IF((loi.DDeposito < SYSDATE())');
  QrPesq1.SQL.Add('AND (lot.Tipo in (0,2)), 0, loi.Valor), 0)) +');
  QrPesq1.SQL.Add('SUM(IF((loi.DDeposito<SYSDATE()) AND ((loi.Data3<2) OR');
  QrPesq1.SQL.Add('(loi.DDeposito<loi.Data3)) AND (lot.Tipo = 1), loi.Valor, 0)) VENCIDO,');
  QrPesq1.SQL.Add('SUM(IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) > 0)');
  QrPesq1.SQL.Add('AND (lot.Tipo in (0,2)), 0, loi.Valor - IF((loi.DDeposito < SYSDATE())');
  QrPesq1.SQL.Add('AND (lot.Tipo in (0,2)), 0, loi.Valor))) +');
  QrPesq1.SQL.Add('SUM(IF((loi.Quitado>1) AND (loi.DDeposito>=loi.Data3) AND (lot.Tipo=1),');
  QrPesq1.SQL.Add('  loi.TotalPg - loi.TotalJr + loi.TotalDs, 0)) PGDDEPOSITO,');
  QrPesq1.SQL.Add('SUM(IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) > 0)');
  QrPesq1.SQL.Add('AND (lot.Tipo in (0,2)), chd.ValPago - chd.Taxas - chd.JurosV +');
  QrPesq1.SQL.Add('chd.Desconto,0)) + SUM(IF(/*(loi.Quitado> 1)AND*/');
  QrPesq1.SQL.Add('(loi.DDeposito< loi.Data3) AND (lot.Tipo=1), loi.TotalPg -');
  QrPesq1.SQL.Add('loi.TotalJr + loi.TotalDs, 0)) PGATRAZO,');
  QrPesq1.SQL.Add('SUM(IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) > 0)');
  QrPesq1.SQL.Add('AND (lot.Tipo in (0,2)), loi.Valor - (chd.ValPago - chd.Taxas -');
  QrPesq1.SQL.Add('chd.JurosV) + chd.Desconto, 0)) +');
  QrPesq1.SQL.Add('SUM(IF((loi.Quitado< 2) AND (loi.DDeposito< SYSDATE() AND (lot.Tipo=1)),');
  QrPesq1.SQL.Add('  loi.Valor - (loi.TotalPg - loi.TotalJr) + loi.TotalDs, 0)) PGABERTO,');
  QrPesq1.SQL.Add('MONTH(loi.DDeposito)+YEAR(loi.DDeposito)*100+0.00 PERIODO,');
  QrPesq1.SQL.Add('CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial ELSE cli.Nome END NOMECLI,');
  QrPesq1.SQL.Add('ban.Nome NOMEBANCO, loi.DDeposito, loi.Emitente NOMEEMITENTE');
  QrPesq1.SQL.Add('FROM lot esits loi');
  QrPesq1.SQL.Add('LEFT JOIN lot es     lot ON lot.Codigo=loi.Codigo');
  QrPesq1.SQL.Add('LEFT JOIN entidades cli ON cli.Codigo=lot.Cliente');
  QrPesq1.SQL.Add('LEFT JOIN alinits   chd ON chd.ChequeOrigem=loi.Controle');
  QrPesq1.SQL.Add('LEFT JOIN bancos    ban ON ban.Codigo=loi.Banco');
  QrPesq1.SQL.Add(dmkPF.SQL_Periodo('WHERE loi.DDeposito ', TPIni.Date, TPFim.Date, True, True));
  QrPesq1.SQL.Add('');
  //////////////////////////////////////////////////////////////////////////////
  if Trim(EdCliente.Text) <> '' then
    QrPesq1.SQL.Add('AND lot.Cliente='+EdCliente.Text);
  if Geral.IMV(EdBanco.Text) <> 0 then
    QrPesq1.SQL.Add('AND loi.Banco='+EdBanco.Text);
  if EdCPF1.Text <> '' then
    QrPesq1.SQL.Add('AND loi.CPF="'+Geral.SoNumero_TT(EdCPF1.Text)+'"');

  Grupos := 'GROUP BY ';
  for i := 0 to RGGrupos.ItemIndex do
  begin
    if i > 0 then Grupos := Grupos + ', ';
    //Grupos := Grupos + ItensOrdem[TRadioGroup('RGOrdem'+IntToStr(i+1)).ItemIndex];
    case i of
      0: Grupos := Grupos + ItensOrdem[RGOrdem1.ItemIndex];
      1: Grupos := Grupos + ItensOrdem[RGOrdem2.ItemIndex];
      2: Grupos := Grupos + ItensOrdem[RGOrdem3.ItemIndex];
      3: Grupos := Grupos + ItensOrdem[RGOrdem4.ItemIndex];
      //4: Grupos := Grupos + ItensOrdem[RGOrdem5.ItemIndex];
    end;
  end;
  QrPesq1.SQL.Add(Grupos);
  //////////////////////////////////////////////////////////////////////////////
  QrPesq1.SQL.Add('ORDER BY '+
    ItensOrdem[RGOrdem1.ItemIndex]+', '+
    ItensOrdem[RGOrdem2.ItemIndex]+', '+
    ItensOrdem[RGOrdem3.ItemIndex]+', '+
    ItensOrdem[RGOrdem4.ItemIndex]);
  UMyMod.AbreQuery(QrPesq1);
}
  Grupos := 'GROUP BY ';
  for i := 0 to RGGrupos.ItemIndex do
  begin
    if i > 0 then Grupos := Grupos + ', ';
    //Grupos := Grupos + ItensOrdem[TRadioGroup('RGOrdem'+IntToStr(i+1)).ItemIndex];
    case i of
      0: Grupos := Grupos + ItensOrdem[RGOrdem1.ItemIndex];
      1: Grupos := Grupos + ItensOrdem[RGOrdem2.ItemIndex];
      2: Grupos := Grupos + ItensOrdem[RGOrdem3.ItemIndex];
      3: Grupos := Grupos + ItensOrdem[RGOrdem4.ItemIndex];
      //4: Grupos := Grupos + ItensOrdem[RGOrdem5.ItemIndex];
    end;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq1, Dmod.MyDB, [
  'SELECT SUM(loi.Credito) Valor, COUNT(loi.FatParcela) ITENS, ',
  'SUM(IF(loi.DDeposito > SYSDATE(), 1, 0)) ITENS_AVencer, ',
  'SUM(IF((loi.DDeposito < SYSDATE()), 1, 0)) ITENS_Vencidos, ',
  'SUM(IF((loi.DDeposito < SYSDATE()) AND (lot.Tipo in (0,2)), 1, 0)) - ',
  'SUM(IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) > 0) ',
  'AND (lot.Tipo in (0,2)), 1, 0)) + SUM(IF((loi.Quitado>1) AND ',
  '(loi.DDeposito>=loi.Data3) AND (lot.Tipo=1), 1, 0)) ITENS_PgVencto, ',
  'SUM(IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) > 0) ',
  'AND (lot.Tipo in (0,2)), 1, 0)) + SUM(IF((loi.Quitado<2) AND ',
  '(loi.DDeposito< SYSDATE()) AND (lot.Tipo=1), 1, 0)) + ',
  'SUM(IF((loi.Quitado>1) AND (loi.DDeposito< loi.Data3) AND ',
  '(lot.Tipo=1), 1, 0)) ITENS_Devolvid, ',
  'SUM(IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) = 12) ',
  'AND (lot.Tipo in (0,2)), 1, 0)) + SUM(IF((loi.Quitado>1) AND ',
  '(loi.DDeposito< loi.Data3) AND (lot.Tipo=1), 1, 0)) ITENS_DevPgTot, ',
  'SUM(IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) in (10, 11)) ',
  'AND (lot.Tipo in (0,2)), 1, 0)) + SUM(IF((loi.Quitado<2) AND ',
  '(loi.DDeposito< SYSDATE()) AND (lot.Tipo=1), 1, 0)) ITENS_DevNaoQt, ',
  'SUM(IF(lot.Tipo in (0,2), (chd.ValPago - chd.Taxas - chd.JurosV + ',
  'chd.Desconto), 0)) + SUM(IF(lot.Tipo = 1, (loi.TotalPg - ',
  'loi.TotalJr + loi.TotalDs), 0)) Pago, ',
  'SUM(IF(loi.DDeposito < SYSDATE(), loi.Credito, 0)) EXPIRADO, ',
  'SUM(IF(loi.DDeposito < SYSDATE(), 0, loi.Credito)) AEXPIRAR, ',
  'SUM(IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) > 0) ',
  'AND (lot.Tipo in (0,2)),  loi.Credito - IF((loi.DDeposito < SYSDATE()) ',
  'AND (lot.Tipo in (0,2)), 0, loi.Credito), 0)) + ',
  'SUM(IF((loi.DDeposito<SYSDATE()) AND ((loi.Data3<2) OR ',
  '(loi.DDeposito<loi.Data3)) AND (lot.Tipo = 1), loi.Credito, 0)) VENCIDO, ',
  'SUM(IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) > 0) ',
  'AND (lot.Tipo in (0,2)), 0, loi.Credito - IF((loi.DDeposito < SYSDATE()) ',
  'AND (lot.Tipo in (0,2)), 0, loi.Credito))) + ',
  'SUM(IF((loi.Quitado>1) AND (loi.DDeposito>=loi.Data3) AND (lot.Tipo=1), ',
  '  loi.TotalPg - loi.TotalJr + loi.TotalDs, 0)) PGDDEPOSITO, ',
  'SUM(IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) > 0) ',
  'AND (lot.Tipo in (0,2)), chd.ValPago - chd.Taxas - chd.JurosV + ',
  'chd.Desconto,0)) + SUM(IF(/*(loi.Quitado> 1)AND*/ ',
  '(loi.DDeposito< loi.Data3) AND (lot.Tipo=1), loi.TotalPg - ',
  'loi.TotalJr + loi.TotalDs, 0)) PGATRAZO, ',
  'SUM(IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) > 0) ',
  'AND (lot.Tipo in (0,2)), loi.Credito - (chd.ValPago - chd.Taxas - ',
  'chd.JurosV) + chd.Desconto, 0)) + ',
  'SUM(IF((loi.Quitado< 2) AND (loi.DDeposito< SYSDATE() AND (lot.Tipo=1)), ',
  '  loi.Credito - (loi.TotalPg - loi.TotalJr) + loi.TotalDs, 0)) PGABERTO, ',
  'MONTH(loi.DDeposito)+YEAR(loi.DDeposito)*100+0.00 PERIODO, ',
  'CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial ELSE cli.Nome END NOMECLI, ',
  'ban.Nome NOMEBANCO, loi.DDeposito, loi.Emitente NOMEEMITENTE ',
  'FROM ' + CO_TabLctA + ' loi ',
  'LEFT JOIN ' + CO_TabLotA + ' lot ON lot.Codigo=loi.FatNum ',
  'LEFT JOIN entidades cli ON cli.Codigo=lot.Cliente ',
  'LEFT JOIN alinits   chd ON chd.ChequeOrigem=loi.FatParcela ',
  'LEFT JOIN bancos    ban ON ban.Codigo=loi.Banco ',
  'WHERE loi.FatID=' + FormatFloat('0', VAR_FATID_0301),
  dmkPF.SQL_Periodo('AND loi.DDeposito ', TPIni.Date, TPFim.Date, True, True),
  //////////////////////////////////////////////////////////////////////////////
  Geral.ATS_if(Trim(EdCliente.Text) <> '', [
  'AND lot.Cliente=' + EdCliente.Text]),
  Geral.ATS_if(Geral.IMV(EdBanco.Text) <> 0, [
    'AND loi.Banco=' + EdBanco.Text]),
  Geral.ATS_if((EdCPF1.Text <> '') and (EdCPF2.Text <> ''), [
    'AND (loi.CNPJCPF="' + Geral.SoNumero_TT(EdCPF1.Text) + '"',
    'OR loi.CNPJCPF="' + Geral.SoNumero_TT(EdCPF2.Text) + '")']),
  Geral.ATS_if((EdCPF1.Text <> '') and (EdCPF2.Text = ''), [
    'AND loi.CNPJCPF="' + Geral.SoNumero_TT(EdCPF1.Text) + '"']),
  Geral.ATS_if((EdCPF1.Text = '') and (EdCPF2.Text <> ''), [
    'AND loi.CNPJCPF="' + Geral.SoNumero_TT(EdCPF2.Text) + '"']),
  Grupos,
  //////////////////////////////////////////////////////////////////////////////
  'ORDER BY '+
    ItensOrdem[RGOrdem1.ItemIndex]+', '+
    ItensOrdem[RGOrdem2.ItemIndex]+', '+
    ItensOrdem[RGOrdem3.ItemIndex]+', '+
    ItensOrdem[RGOrdem4.ItemIndex],
  '']);
  //
  Screen.Cursor := crDefault;
  //emitentes obtem registros duplicados pelo CPF!!!
  case RGImpressao.ItemIndex of
    0: MyObjects.frxMostra(frxInad1, 'Inadimpl�ncia');
    1: MyObjects.frxMostra(frxInad2, 'Inadimpl�ncia');
  end;
end;

procedure TFmInadimps.frInad1_2UserFunction(const Name: String; p1, p2,
  p3: Variant; var Val: Variant);
begin
  if Name = 'VFR_ORD1' then
  begin
    if RGGrupos.ItemIndex < 1 then Val := 0 else
    Val := RGOrdem1.ItemIndex + 1;
  end else
  if Name = 'VFR_ORD2' then
  begin
    if RGGrupos.ItemIndex < 2 then Val := 0 else
    Val := RGOrdem2.ItemIndex + 1;
  end else
  if Name = 'VFR_SEQ1' then
  begin
    case RGGrupos.ItemIndex of
      0: Val := RGOrdem1.ItemIndex;
      1: Val := RGOrdem2.ItemIndex;
      2: Val := RGOrdem3.ItemIndex;
      3: Val := RGOrdem4.ItemIndex;
      //4: Val := RGOrdem5.ItemIndex;
    end;
  end
end;

procedure TFmInadimps.frInad1_1GetValue(const ParName: String;
  var ParValue: Variant);
begin
  if ParName = 'VARF_EMITENTE' then ParValue :=
    dmkPF.ParValueCodTxt('', DBEdEmitente.Text, Null)
  else if ParName = 'VARF_BANCO' then ParValue :=
    dmkPF.ParValueCodTxt('', CBBanco.Text, EdBanco.Text)
  else if ParName = 'VARF_NOMEMES' then ParValue :=
    MLAGeral.FormatAno4Mes2(Trunc(QrPesq1PERIODO.Value));
  if ParName = 'VARF_CLIENTE' then
  begin
    if CbCliente.KeyValue = NULL then ParValue := 'TODOS' else
    ParValue := CBCliente.Text;
  end else if ParName = 'VARF_PERIODO' then ParValue :=
    FormatDateTime(VAR_FORMATDATE3, TPIni.Date) + CO_ATE +
    FormatDateTime(VAR_FORMATDATE3, TPFim.Date);
  if ParName = 'VAR_TESTE' then
  begin
    ParValue := 1;
  end
  else if ParName = 'VFR_LA1NOME' then
  begin
    if RGSintetico.ItemIndex = 0 then
    begin
      case RGOrdem1.ItemIndex of
        0: ParValue := 'Cliente: '+QrPesq1NOMECLI.Value;
        1: ParValue := 'Emitente: '+QrPesq1NOMEEMITENTE.Value;
        2: ParValue := 'Banco: '+QrPesq1NOMEBANCO.Value;
        3: ParValue := 'M�s: '+MLAGeral.FormatAno4Mes2(Trunc(QrPesq1PERIODO.Value));
        4: ParValue := 'Dia dep�sito: '+Geral.FDT(QrPesq1DDeposito.Value, 2);
      end;
    end else begin
      case RGOrdem1.ItemIndex of
        0: ParValue := 'Cliente: '+QrPesq1NOMECLI.Value;
        1: ParValue := 'Emitente: '+QrPesq1NOMEEMITENTE.Value;
        2: ParValue := 'Banco: '+QrPesq1NOMEBANCO.Value;
        3: ParValue := 'M�s: '+MLAGeral.FormatAno4Mes2(Trunc(QrPesq1PERIODO.Value));
        4: ParValue := 'Dia dep�sito: '+Geral.FDT(QrPesq1DDeposito.Value, 2);
      end;
    end;
  end
  else if ParName = 'VFR_LA2NOME' then
  begin
    if RGSintetico.ItemIndex = 0 then
    begin
      case RGOrdem2.ItemIndex of
        0: ParValue := 'Cliente: '+QrPesq1NOMECLI.Value;
        1: ParValue := 'Emitente: '+QrPesq1NOMEEMITENTE.Value;
        2: ParValue := 'Banco: '+QrPesq1NOMEBANCO.Value;
        3: ParValue := 'M�s: '+MLAGeral.FormatAno4Mes2(Trunc(QrPesq1PERIODO.Value));
        4: ParValue := 'Dia dep�sito: '+Geral.FDT(QrPesq1DDeposito.Value, 2);
      end;
    end else begin
      case RGOrdem2.ItemIndex of
        0: ParValue := 'Cliente: '+QrPesq1NOMECLI.Value;
        1: ParValue := 'Emitente: '+QrPesq1NOMEEMITENTE.Value;
        2: ParValue := 'Banco: '+QrPesq1NOMEBANCO.Value;
        3: ParValue := 'M�s: '+MLAGeral.FormatAno4Mes2(Trunc(QrPesq1PERIODO.Value));
        4: ParValue := 'Dia dep�sito: '+Geral.FDT(QrPesq1DDeposito.Value, 2);
      end;
    end;
  end
  else if ParName = 'VFR_TITULO' then
  begin
    case PageControl1.ActivePageIndex of
      0: ParValue := 'RELAT�RIO DE INADIMPL�NCIAS DE CHEQUES';
      1: ParValue := 'RELAT�RIO DE INADIMPL�NCIAS DE DUPLICATAS';
      2: ParValue := 'RELAT�RIO DE INADIMPL�NCIAS DE CHEQUES E DUPLICATAS';
    end;
  end;
end;

procedure TFmInadimps.QrPesq1AfterOpen(DataSet: TDataSet);
begin
  Edit1.Text := IntToStr(QrPesq1.RecordCount);
end;

procedure TFmInadimps.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
  procedure ReabreDef();
  begin
{
    QrDef.Close;
    QrDef. Open;
}
    UnDmkDAC_PF.AbreMySQLQuery0(QrDef, Dmod.MyDB, [
    'SELECT MIN(DDeposito) Minimo, ',
    'MAX(DDeposito) Maximo ',
    'FROM ' + CO_TabLctA,
    'WHERE FatID=' + FormatFloat('0', VAR_FATID_0301),
    '']);
  end;
begin
  if Key = VK_F5 then
  begin
    ReabreDef();
    TPIni.Date := QrDefMinimo.Value;
  end else
  if Key = VK_F6 then
  begin
    ReabreDef();
    TPFim.Date := QrDefMaximo.Value;
  end else
  if Key = VK_F7 then
  begin
    ReabreDef();
    TPIni.Date := QrDefMinimo.Value;
    TPFim.Date := QrDefMaximo.Value;
  end else
  if Key = VK_F8 then
  begin
    case PageControl1.ActivePageIndex of
      0:
      begin
        Application.CreateForm(TFmPesqCPF, FmPesqCPF);
        FmPesqCPF.ShowModal;
        FmPesqCPF.Destroy;
        if VAR_CPF_PESQ <> '' then
        begin
          EdCPF1.Text := VAR_CPF_PESQ;
        end;
        ReopenEmitente(VAR_CPF_PESQ);
      end;
      1:
      begin
        Application.CreateForm(TFmPesqCNPJ, FmPesqCNPJ);
        FmPesqCNPJ.ShowModal;
        FmPesqCNPJ.Destroy;
        if VAR_CPF_PESQ <> '' then
        begin
          EdCPF2.Text := VAR_CPF_PESQ;
        end;
        ReopenSacado(VAR_CPF_PESQ);
      end;
    end;
  end;
end;

procedure TFmInadimps.ReopenEmitente(CPF: String);
var
 CPFx: String;
begin
  QrEmitCPF.Close;
  CPFx := Geral.SoNumero_TT(CPF);
  if Trim(CPFx) <> '' then
  begin
    QrEmitCPF.Params[0].AsString := CPFx;
    UMyMod.AbreQuery(QrEmitCPF, Dmod.MyDB);
  end;
end;

procedure TFmInadimps.ReopenSacado(CNPJ: String);
var
 CPFx: String;
begin
  QrSacado.Close;
  CPFx := Geral.SoNumero_TT(CNPJ);
  if Trim(CPFx) <> '' then
  begin
    QrSacado.Params[0].AsString := CPFx;
    UMyMod.AbreQuery(QrSacado, Dmod.MyDB);
  end;
end;

procedure TFmInadimps.EdCPF2Exit(Sender: TObject);
var
  CPF : String;
begin
  CPF := Geral.SoNumero_TT(EdCPF2.Text);
  ReopenSacado(CPF);
  Pesquisa(False);
end;

procedure TFmInadimps.frxInad1GetValue(const VarName: String;
  var Value: Variant);
begin
  if VarName = 'VARF_EMITENTE' then Value :=
    dmkPF.ParValueCodTxt('', DBEdEmitente.Text, Null)
  else if VarName = 'VARF_BANCO' then Value :=
    dmkPF.ParValueCodTxt('', CBBanco.Text, EdBanco.Text)
  else if VarName = 'VARF_NOMEMES' then Value :=
    MLAGeral.FormatAno4Mes2(Trunc(QrPesq1PERIODO.Value));
  if VarName = 'VARF_CLIENTE' then
  begin
    if CbCliente.KeyValue = NULL then Value := 'TODOS' else
    Value := CBCliente.Text;
  end else if VarName = 'VARF_PERIODO' then Value :=
    FormatDateTime(VAR_FORMATDATE3, TPIni.Date) + CO_ATE +
    FormatDateTime(VAR_FORMATDATE3, TPFim.Date);
  if VarName = 'VAR_TESTE' then
  begin
    Value := 1;
  end
  else if VarName = 'VFR_LA1NOME' then
  begin
    if RGSintetico.ItemIndex = 0 then
    begin
      case RGOrdem1.ItemIndex of
        0: Value := 'Cliente: '+QrPesq1NOMECLI.Value;
        1: Value := 'Emitente: '+QrPesq1NOMEEMITENTE.Value;
        2: Value := 'Banco: '+QrPesq1NOMEBANCO.Value;
        3: Value := 'M�s: '+MLAGeral.FormatAno4Mes2(Trunc(QrPesq1PERIODO.Value));
        4: Value := 'Dia dep�sito: '+Geral.FDT(QrPesq1DDeposito.Value, 2);
      end;
    end else begin
      case RGOrdem1.ItemIndex of
        0: Value := 'Cliente: '+QrPesq1NOMECLI.Value;
        1: Value := 'Emitente: '+QrPesq1NOMEEMITENTE.Value;
        2: Value := 'Banco: '+QrPesq1NOMEBANCO.Value;
        3: Value := 'M�s: '+MLAGeral.FormatAno4Mes2(Trunc(QrPesq1PERIODO.Value));
        4: Value := 'Dia dep�sito: '+Geral.FDT(QrPesq1DDeposito.Value, 2);
      end;
    end;
  end
  else if VarName = 'VFR_LA2NOME' then
  begin
    if RGSintetico.ItemIndex = 0 then
    begin
      case RGOrdem2.ItemIndex of
        0: Value := 'Cliente: '+QrPesq1NOMECLI.Value;
        1: Value := 'Emitente: '+QrPesq1NOMEEMITENTE.Value;
        2: Value := 'Banco: '+QrPesq1NOMEBANCO.Value;
        3: Value := 'M�s: '+MLAGeral.FormatAno4Mes2(Trunc(QrPesq1PERIODO.Value));
        4: Value := 'Dia dep�sito: '+Geral.FDT(QrPesq1DDeposito.Value, 2);
      end;
    end else begin
      case RGOrdem2.ItemIndex of
        0: Value := 'Cliente: '+QrPesq1NOMECLI.Value;
        1: Value := 'Emitente: '+QrPesq1NOMEEMITENTE.Value;
        2: Value := 'Banco: '+QrPesq1NOMEBANCO.Value;
        3: Value := 'M�s: '+MLAGeral.FormatAno4Mes2(Trunc(QrPesq1PERIODO.Value));
        4: Value := 'Dia dep�sito: '+Geral.FDT(QrPesq1DDeposito.Value, 2);
      end;
    end;
  end
  else if VarName = 'VFR_TITULO' then
  begin
    case PageControl1.ActivePageIndex of
      0: Value := 'RELAT�RIO DE INADIMPL�NCIAS DE CHEQUES';
      1: Value := 'RELAT�RIO DE INADIMPL�NCIAS DE DUPLICATAS';
      2: Value := 'RELAT�RIO DE INADIMPL�NCIAS DE CHEQUES E DUPLICATAS';
    end;
  end;


  // User function

  if VarName = 'VFR_ORD1' then
  begin
    if RGGrupos.ItemIndex < 1 then Value := 0 else
    Value := RGOrdem1.ItemIndex + 1;
  end else
  if VarName = 'VFR_ORD2' then
  begin
    if RGGrupos.ItemIndex < 2 then Value := 0 else
    Value := RGOrdem2.ItemIndex + 1;
  end else
  if VarName = 'VFR_SEQ1' then
  begin
    case RGGrupos.ItemIndex of
      0: Value := RGOrdem1.ItemIndex;
      1: Value := RGOrdem2.ItemIndex;
      2: Value := RGOrdem3.ItemIndex;
      3: Value := RGOrdem4.ItemIndex;
      //4: Value := RGOrdem5.ItemIndex;
    end;
  end
end;

end.

