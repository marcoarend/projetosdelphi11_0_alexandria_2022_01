unit Lot2Txa;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, UnDmkEnums;

type
  TFmLot2Txa = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    Panel15: TPanel;
    Label75: TLabel;
    Label77: TLabel;
    Label76: TLabel;
    Label78: TLabel;
    EdTaxaCod5: TdmkEditCB;
    CBTaxaCod5: TdmkDBLookupComboBox;
    EdTaxaTxa5: TdmkEdit;
    RGForma: TRadioGroup;
    EdTaxaQtd5: TdmkEdit;
    EdTaxaTot5: TdmkEdit;
    SpeedButton1: TSpeedButton;
    TPData: TdmkEditDateTimePicker;
    Label1: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure EdTaxaQtd5Change(Sender: TObject);
    procedure RGFormaClick(Sender: TObject);
    procedure EdTaxaTxa5Change(Sender: TObject);
    procedure EdTaxaCod5Change(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure CalculaTotalTaxa();
    procedure ConfiguraValorTaxa();
  public
    { Public declarations }
  end;

  var
  FmLot2Txa: TFmLot2Txa;

implementation

uses UnMyObjects, Module, Lot2Cab, UMySQLModule, Principal, MyListas, ModuleLot,
  MyDBCheck, Taxas;

{$R *.DFM}

procedure TFmLot2Txa.BtOKClick(Sender: TObject);
const
  Qtde = 0;
var
{
  TaxaCod: Integer;
  TaxaTxa, TaxaVal, TaxaQtd: Double;
  //
}
  FatID_Sub, FatParcela, Controle, Genero, Cliente, Tipific: Integer;
  FatNum, MoraTxa, MoraDia, MoraVal, Credito: Double;
begin
  //TaxaCod := Geral.IMV(EdTaxaCod5.Text);
  FatID_Sub := Geral.IMV(EdTaxaCod5.Text);
  if MyObjects.FIC(FatID_Sub < 1, EdTaxaCod5,
  'Informe o tipo de taxa a ser utilizada!') then
    Exit;
  //
  Genero := DmLot.QrTaxasPlaGen.Value;
  if MyObjects.FIC(Genero = 0, EdTaxaCod5,
  'A taxa selecionada n�o possui conta do plano de contas atrelada!' + #13#10 +
  'Informe uma conta no cadastro da taxa!') then
    Exit;
  //
  //TaxaTxa := Geral.DMV(EdTaxaTxa5.Text);
  MoraTxa := Geral.DMV(EdTaxaTxa5.Text);
  if MyObjects.FIC(MoraTxa <= 0, EdTaxaTxa5,
  'Informe o valor da taxa a ser utilizada!') then
    Exit;
  //
  //TaxaQtd := Geral.DMV(EdTaxaQtd5.Text);
  MoraDia := Geral.DMV(EdTaxaQtd5.Text);
  Tipific := RGForma.ItemIndex;
  if Tipific = 0 then
    //TaxaVal := TaxaTxa * TaxaQtd
    MoraVal := MoraTxa * MoraDia
  else
    MoraVal := 0;
  //
  Cliente := FmLot2Cab.QrLotCliente.Value;
  Credito := MoraVal;
  //
  Screen.Cursor := crHourGlass;
  try
{
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('');
    if ImgTipo.SQLType = stIns then
    begin
      Dmod.QrUpd.SQL.Add('INSERT INTO ' + CO TabLotTxs + ' SET AlterWeb=1, ');
      Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
        CO TabLotTxs, CO TabLotTxs, 'Controle');
    end else begin
      Dmod.QrUpd.SQL.Add('UPDATE ' + CO TabLotTxs + ' SET AlterWeb=1, ');
      Controle := FmLot2Cab.QrLotTxsFatParcela.Value;
    end;
    Dmod.QrUpd.SQL.Add('TaxaCod=:P0, TaxaTxa=:P1, TaxaVal=:P2, Forma=:P3, ');
    Dmod.QrUpd.SQL.Add('TaxaQtd=:P4, ');
    if ImgTipo.SQLType = stUpd then
      Dmod.QrUpd.SQL.Add('UserCad=:Pa, DataCad=:Pb, Codigo=:Pc, Controle=:Pd')
    else
      Dmod.QrUpd.SQL.Add('UserAlt=:Pa, DataAlt=:Pb WHERE Codigo=:Pc AND Controle=:Pd');
    Dmod.QrUpd.Params[00].AsInteger := TaxaCod;
    Dmod.QrUpd.Params[01].AsFloat   := TaxaTxa;
    Dmod.QrUpd.Params[02].AsFloat   := TaxaVal;
    Dmod.QrUpd.Params[03].AsInteger := RGForma.ItemIndex;
    Dmod.QrUpd.Params[04].AsFloat   := Geral.DMV(EdTaxaQtd5.Text);
    //
    Dmod.QrUpd.Params[05].AsInteger := VAR_USUARIO;
    Dmod.QrUpd.Params[06].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
    Dmod.QrUpd.Params[07].AsInteger := FmLot2Cab.QrLotCodigo.Value;
    Dmod.QrUpd.Params[08].AsInteger := Controle;
    Dmod.QrUpd.ExecSQL;
    FmLot2Cab.FControlTxs := Controle;
}
    FatNum := FmLot2Cab.QrLotCodigo.Value;
    if ImgTipo.SQLType = stUpd then
    begin
      Controle := FmLot2Cab.QrLotTxsControle.Value;
      FatParcela := FmLot2Cab.QrLotTxsFatParcela.Value;
    end else begin
      Controle := 0;
      FatParcela := 0;
    end;
    if DmLot.SQL_Txa(Dmod.QrUpd, ImgTipo.SQLType,
    FatParcela, Controle, FatID_Sub, Genero, Cliente, Tipific,
    FatNum, Qtde, Credito, MoraTxa, MoraDia, MoraVal, TPData.Date, 0, 0) then
    begin
      FmLot2Cab.FControlTxs := FatParcela;
      // 2011-12-12
      FmLot2Cab.CalculaLote(Trunc(FatNum), True);
      FmLot2Cab.LocCod(Trunc(FatNum), Trunc(FatNum));
      // fim 2011-12-12
      Screen.Cursor := crDefault;
      //
      Close;
    end;
  except
    Screen.Cursor := crDefault;
    raise;
  end;
end;

procedure TFmLot2Txa.BtSaidaClick(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
    FmLot2Cab.CalculaLote(FmLot2Cab.QrLotCodigo.Value, True);
    FmLot2Cab.LocCod(FmLot2Cab.QrLotCodigo.Value, FmLot2Cab.QrLotCodigo.Value);
    //
    Screen.Cursor := crDefault;
    Close;
  except
    Screen.Cursor := crDefault;
    raise;
  end;
end;

procedure TFmLot2Txa.CalculaTotalTaxa();
var
  Qtd, Txa, Tot: Double;
begin
  Qtd := EdTaxaQtd5.ValueVariant;
  Txa := EdTaxaTxa5.ValueVariant;
  Tot := Qtd * Txa;
  EdTaxaTot5.ValueVariant := Tot;
end;

procedure TFmLot2Txa.EdTaxaCod5Change(Sender: TObject);
begin
  ConfiguraValorTaxa();
end;

procedure TFmLot2Txa.EdTaxaQtd5Change(Sender: TObject);
begin
  CalculaTotalTaxa();
end;

procedure TFmLot2Txa.EdTaxaTxa5Change(Sender: TObject);
begin
  CalculaTotalTaxa();
end;

procedure TFmLot2Txa.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLot2Txa.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  DmLot.ReopenTaxas();
end;

procedure TFmLot2Txa.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLot2Txa.FormShow(Sender: TObject);
begin
  EdTaxaCod5.SetFocus;
end;

procedure TFmLot2Txa.RGFormaClick(Sender: TObject);
begin
  CalculaTotalTaxa();
end;

procedure TFmLot2Txa.SpeedButton1Click(Sender: TObject);
var
  Taxa: Integer;
begin
  VAR_CADASTRO := 0;
  Taxa         := EdTaxaCod5.ValueVariant;
  //
  if DBCheck.CriaFm(TFmTaxas, FmTaxas, afmoNegarComAviso) then
  begin
    if Taxa <> 0 then
      FmTaxas.LocCod(Taxa, Taxa);
    FmTaxas.ShowModal;
    FmTaxas.Destroy;
    //
    UMyMod.SetaCodigoPesquisado(EdTaxaCod5, CBTaxaCod5, DmLot.QrTaxas, VAR_CADASTRO);
    //
    if EdTaxaCod5.ValueVariant <> 0 then
      EdTaxaTxa5.ValueVariant := DmLot.QrTaxasValor.Value;
  end;
end;

procedure TFmLot2Txa.ConfiguraValorTaxa();
begin
  RGForma.ItemIndex := DmLot.QrTaxasForma.Value;
  EdTaxaTxa5.ValueVariant := DmLot.QrTaxasValor.Value
end;

end.
