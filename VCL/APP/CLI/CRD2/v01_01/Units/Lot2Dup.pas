unit Lot2Dup;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Mask, DmkDAC_PF, UnDmkEnums;

type
  TFmLot2Dup = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSair4: TBitBtn;
    BtConfirma4: TBitBtn;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    PainelDigitaDup: TPanel;
    Panel37: TPanel;
    Label66: TLabel;
    Label58: TLabel;
    Label62: TLabel;
    Label69: TLabel;
    Label63: TLabel;
    Label71: TLabel;
    Label72: TLabel;
    Label64: TLabel;
    Label59: TLabel;
    Label73: TLabel;
    Label93: TLabel;
    Label92: TLabel;
    Label60: TLabel;
    Label61: TLabel;
    Label18: TLabel;
    Label57: TLabel;
    Label35: TLabel;
    Label67: TLabel;
    Label56: TLabel;
    Label79: TLabel;
    Label55: TLabel;
    Label54: TLabel;
    Label68: TLabel;
    Label65: TLabel;
    Label204: TLabel;
    Label207: TLabel;
    Label70: TLabel;
    EdEmiss4: TdmkEdit;
    EdSacado: TdmkEdit;
    EdBairro: TdmkEdit;
    EdPrazo4: TdmkEdit;
    EdCidade: TdmkEdit;
    EdDias4: TdmkEdit;
    EdTotalCompraPer4: TdmkEdit;
    EdCEP: TdmkEdit;
    EdRua: TdmkEdit;
    EdTel1: TdmkEdit;
    EdNumero: TdmkEdit;
    EdCompl: TdmkEdit;
    EdIE: TdmkEdit;
    EdCNPJ: TdmkEdit;
    EdTxaCompra4: TdmkEdit;
    EdDMaisD: TdmkEdit;
    EdVence4: TdmkEdit;
    EdDesco4: TdmkEdit;
    EdValor4: TdmkEdit;
    EdDuplicata: TdmkEdit;
    EdData4: TdmkEdit;
    CBUF: TComboBox;
    EdCartDep4: TdmkEditCB;
    CBCartDep4: TdmkDBLookupComboBox;
    DBEdBanco4: TDBEdit;
    DBEdAgencia4: TDBEdit;
    DBEdConta4: TDBEdit;
    dmkEdTPDescAte: TdmkEditDateTimePicker;
    PageControl6: TPageControl;
    TabSheet19: TTabSheet;
    DBGrid15: TDBGrid;
    TabSheet20: TTabSheet;
    DBGrid16: TDBGrid;
    TabSheet21: TTabSheet;
    DBGrid17: TDBGrid;
    TabSheet22: TTabSheet;
    DBGrid18: TDBGrid;
    TabSheet28: TTabSheet;
    Panel45: TPanel;
    Label179: TLabel;
    Label180: TLabel;
    Label181: TLabel;
    Label182: TLabel;
    Label183: TLabel;
    Label184: TLabel;
    Label185: TLabel;
    Label186: TLabel;
    Label187: TLabel;
    Label188: TLabel;
    Label189: TLabel;
    Label190: TLabel;
    Label191: TLabel;
    Label192: TLabel;
    Label193: TLabel;
    Label194: TLabel;
    Label195: TLabel;
    Label196: TLabel;
    Label197: TLabel;
    Label198: TLabel;
    Label199: TLabel;
    Label200: TLabel;
    DBText3: TDBText;
    Label201: TLabel;
    LM_DBEdit2: TDBEdit;
    DBEdit36: TDBEdit;
    DBEdit37: TDBEdit;
    DBEdit62: TDBEdit;
    DBEdit63: TDBEdit;
    DBEdit64: TDBEdit;
    DBEdit65: TDBEdit;
    DBEdit66: TDBEdit;
    DBEdit67: TDBEdit;
    DBEdit68: TDBEdit;
    DBEdit69: TDBEdit;
    DBEdit70: TDBEdit;
    DBEdit71: TDBEdit;
    DBEdit72: TDBEdit;
    DBEdit73: TDBEdit;
    DBEdit74: TDBEdit;
    DBEdit75: TDBEdit;
    DBEdit76: TDBEdit;
    DBEdit77: TDBEdit;
    DBEdit78: TDBEdit;
    DBEdit79: TDBEdit;
    DBEdit80: TDBEdit;
    DBEdit81: TDBEdit;
    DBEdit82: TDBEdit;
    DBEdit83: TDBEdit;
    GradeDU: TStringGrid;
    BtCalendario2: TBitBtn;
    DBEdit23: TDBEdit;
    Label95: TLabel;
    GroupBox2: TGroupBox;
    Label7: TLabel;
    Label16: TLabel;
    Label111: TLabel;
    Label112: TLabel;
    Label113: TLabel;
    Label114: TLabel;
    Label115: TLabel;
    EdDT_S: TdmkEdit;
    EdRT_S: TdmkEdit;
    EdVT_S: TdmkEdit;
    EdST_S: TdmkEdit;
    EdCT_S: TdmkEdit;
    EdDV_S: TdmkEdit;
    EdCR_S: TdmkEdit;
    EdCV_S: TdmkEdit;
    EdOA_S: TdmkEdit;
    EdTT_S: TdmkEdit;
    EdDR_S: TdmkEdit;
    Label1: TLabel;
    EdEmail: TdmkEdit;
    procedure BtSair4Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdEmiss4Exit(Sender: TObject);
    procedure EdData4Exit(Sender: TObject);
    procedure EdDuplicataExit(Sender: TObject);
    procedure EdValor4Exit(Sender: TObject);
    procedure EdDesco4Exit(Sender: TObject);
    procedure dmkEdTPDescAteExit(Sender: TObject);
    procedure EdDMaisDExit(Sender: TObject);
    procedure EdTxaCompra4Exit(Sender: TObject);
    procedure EdCNPJChange(Sender: TObject);
    procedure EdCNPJExit(Sender: TObject);
    procedure EdCNPJKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure CBUFKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure DBEdBanco4Change(Sender: TObject);
    procedure GradeDUDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeDUKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtCalendario2Click(Sender: TObject);
    procedure BtConfirma4Click(Sender: TObject);
    procedure EdVence4Exit(Sender: TObject);
  private
    { Private declarations }
    procedure CalculaValoresDuplicataAlterado();
    procedure ReopenBanco4();
  public
    { Public declarations }
    FTabLctA: String;
    procedure CalculaDiasDU();
  end;

  var
  FmLot2Dup: TFmLot2Dup;

implementation

uses UnMyObjects, Module, ModuleLot, ModuleLot2, Principal, Lot2Cab,
  MyListas, SomaDiasCred, PesqCPFCNPJ, UMySQLModule, GerDup1Main;

{$R *.DFM}

procedure TFmLot2Dup.BtCalendario2Click(Sender: TObject);
begin
  Application.CreateForm(TFmSomaDiasCred, FmSomaDiasCred);
  FmSomaDiasCred.TPDataI.Date := Geral.ValidaDataSimples(EdData4.Text, True);
  FmSomaDiasCred.TPDataF.Date := Geral.ValidaDataSimples(EdVence4.Text, True);
  FmSomaDiasCred.EdComp.Text  := '0';
  FmSomaDiasCred.EdDMais.Text := EdDMaisD.Text;
  FmSomaDiasCred.ShowModal;
  FmSomaDiasCred.Destroy;
end;

procedure TFmLot2Dup.BtConfirma4Click(Sender: TObject);
var
  Vence, Data_, Emiss_: TDateTime;
  Lote, i, z, Erros, DMais, Dias, FatParcela, FatNum, Numero, Banco, Agencia, 
  CartDep, Cliente, TipoCart, Carteira, Controle, Sub: Integer;
  Credito, Desco, Bruto: Double;
  Duplic, DDeposito, DCompra, Emissao, Vencimento, CNPJCPF, Sacado, Rua,
  CEP, Compl, Bairro, Cidade, UF, IE, Email, DescAte: String;
begin
  DmLot.QrDupsAtencao.Close;
  DmLot.QrDupsAtencao.SQL.Clear;
  DmLot.QrDupsAtencao.SQL.Add('SELECT oc.* ');
  DmLot.QrDupsAtencao.SQL.Add('FROM ocorreu oc ');
  DmLot.QrDupsAtencao.SQL.Add('LEFT JOIN ocorbank ob ON ob.Codigo=oc.Ocorrencia ');
  DmLot.QrDupsAtencao.SQL.Add('LEFT JOIN ' + FTabLctA + ' it ON it.Controle=oc.LotesIts');
  DmLot.QrDupsAtencao.SQL.Add('WHERE ob.Atencao = 1 ');
  DmLot.QrDupsAtencao.SQL.Add('AND it.CNPJCPF="' + Geral.SoNumero_TT(EdCNPJ.Text) + '"');
  DmLot.QrDupsAtencao.Open;
  if DmLot.QrDupsAtencao.RecordCount > 0 then
  begin
    PageControl6.ActivePageIndex := 2;
    if Geral.MensagemBox('Aten��o! Sacado com movimenta��o de ' +
    'duplicata(s) com al�nea(s) que exigem aten��o. Deseja aceitar assim mesmo?',
    'ATEN��O!', MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then
    begin
      Application.CreateForm(TFmGerDup1Main, FmGerDup1Main);
      FmGerDup1Main.EdCPF.Text := EdCNPJ.Text;
      FmGerDup1Main.EdCPFExit(Self);
      FmGerDup1Main.WindowState := wsMaximized;
      FmGerDup1Main.ShowModal;
      FmGerDup1Main.Destroy;
      Exit;
    end;
  end;
  Screen.Cursor := crHourGlass;
  try
    CalculaValoresDuplicataAlterado();
    Erros      := 0;
    Bruto      := Geral.DMV(EdValor4.Text);
    Desco      := Geral.DMV(EdDesco4.Text);
    DescAte    := Geral.FDT(dmkEdTPDescAte.Date, 1);
    Credito    := Bruto-Desco;
    Vence      := Geral.ValidaDataSimples(EdVence4.Text, True);
    Vencimento := FormatDateTime(VAR_FORMATDATE, Vence);
    Data_      := Geral.ValidaDataSimples(EdData4.Text, True);
    DCompra    := FormatDateTime(VAR_FORMATDATE, Data_);
    Emiss_     := Geral.ValidaDataSimples(EdEmiss4.Text, True);
    Emissao    := FormatDateTime(VAR_FORMATDATE, Emiss_);
    Duplic     := EdDuplicata.Text;
    CNPJCPF    := Geral.SoNumero_TT(EdCNPJ.Text);
    IE         := Geral.SoNumero_TT(EdIE.Text);
    Sacado     := EdSacado.Text;
    Rua        := EdRua.Text;
    Numero     := Geral.IMV(Geral.SoNumero_TT(EdNumero.Text));
    Compl      := EdCompl.Text;
    Bairro     := EdBairro.Text;
    Cidade     := EdCidade.Text;
    CEP        := Geral.SoNumero_TT(EdCEP.Text);
    UF         := CBUF.Text;
    CartDep    := EdCartDep4.ValueVariant;
    Email      := EdEmail.ValueVariant;
    // Manter informa��es de antes de 01/04/2007 no Lot esIts
    Banco     := Geral.IMV(DBEdBanco4.Text);
    Agencia   := Geral.IMV(DBEdAgencia4.Text);
    // F I M   Manter informa��es
    //
    Cliente  := FmLot2Cab.QrLotCliente.Value;
    Carteira := FmLot2Cab.QrLotItsCarteira.Value;
    Controle := FmLot2Cab.QrLotItsControle.Value;
    Sub      := FmLot2Cab.QrLotItsSub.Value;
    ///
    Geral.Valida_IE(EdIE.Text, MLAGeral.GetCodigoUF_da_SiglaUF(CBUF.Text), '??');
    //
    z := 10;
    for i := 1 to z do
    begin
      case i of
        1: if Credito <= 0 then Erros := Erros +
          MLAGeral.MostraErroControle('Informe um valor v�lido!', EdValor4);
        2: if Bruto <= 0 then Erros := Erros +
          MLAGeral.MostraErroControle('Informe o valor!', EdValor4);
        3: if Vence < Data_ then Erros := Erros +
          MLAGeral.MostraErroControle('Prazo inv�lido!', EdVence4);
        4: if Duplic = '' then Erros := Erros +
          MLAGeral.MostraErroControle('Informe a duplicata!', EdDuplicata);
        5: if CNPJCPF = '' then Erros := Erros +
          MLAGeral.MostraErroControle('Informe o CNPJ!', EdCNPJ);
        6: if Sacado = '' then Erros := Erros +
          MLAGeral.MostraErroControle('Informe o Sacado!', EdSacado);
        7: if Rua = '' then Erros := Erros +
          MLAGeral.MostraErroControle('Informe o logradouro!', EdRua);
        8: if Cidade = '' then Erros := Erros +
          MLAGeral.MostraErroControle('Informe a Cidade!', EdCidade);
        9: if CEP = '' then Erros := Erros +
          MLAGeral.MostraErroControle('Informe o CEP!', EdCEP);
       10: if CartDep = 0 then Erros := Erros +
          MLAGeral.MostraErroControle('Informe a carteira de recebimento!', EdCartDep4);
      end;
      if Erros > 0 then
      begin
        Screen.Cursor := crDefault;
        Exit;
      end;
    end;
    DMais := EdDMaisD.ValueVariant;
    Dias := EdDias4.ValueVariant;
    DDeposito := FormatDateTime(VAR_FORMATDATE, UMyMod.CalculaDataDeposito(Vence));
    //
    FatNum := FmLot2Cab.QrLotCodigo.Value;
    if ImgTipo.SQLType = stUpd then
    begin
      FatParcela := FmLot2Cab.QrLotItsFatParcela.Value;
      TipoCart  := FmLot2Cab.QrLotItsTipo.Value;
      Carteira := FmLot2Cab.QrLotItsCarteira.Value;
      Controle := FmLot2Cab.QrLotItsControle.Value;
      Sub := FmLot2Cab.QrLotItsSub.Value;
    end else begin
      FatParcela := 0;
      TipoCart := 2;
      Carteira := CO_CARTEIRA_DUP_BORDERO;
      Controle := 0;
      Sub := 0;
    end;
    //
    FatParcela := FmLot2Cab.SQL_DU(FatNum, FatParcela, DMais, Dias, Banco,
                Agencia, Credito, Desco, Bruto, Duplic, CNPJCPF, Sacado,
                Vencimento, DCompra, DDeposito, Emissao, DescAte, ImgTipo.SQLType,
                Cliente, CartDep, TipoCart, Carteira, Controle, Sub);
    //
    FmPrincipal.FControlIts := FatParcela;
    FmPrincipal.AtualizaSacado(CNPJCPF, Sacado, Rua, Numero, Compl, Bairro,
      Cidade, UF, CEP, MlaGeral.SoNumeroESinal_TT(EdTel1.Text), IE, Email, -1);
    //CalculaLote;
    //LocCod(Codigo, Codigo);
    EdEmiss4.SetFocus;
    if ImgTipo.SQLType = stUpd then BtSair4Click(Self)
    else begin
      Lote := FmLot2Cab.QrLotCodigo.Value;
      FmLot2Cab.CalculaLote(Lote, True);
      FmLot2Cab.LocCod(Lote, Lote);
      if Lote <> FmLot2Cab.QrLotCodigo.Value then
      begin
        Geral.MensagemBox('Erro no refresh do lote. O aplicativo ' +
        'dever� ser encerrado!', 'Erro', MB_OK+MB_ICONERROR);
        Application.Terminate;
      end;
    end;
    Screen.Cursor := crDefault;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmLot2Dup.BtSair4Click(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := FmLot2Cab.QrLotCodigo.Value;
  FmLot2Cab.CalculaLote(Codigo, True);
  FmLot2Cab.LocCod(Codigo, Codigo);
  //
  Close;
end;

procedure TFmLot2Dup.CalculaDiasDU();
var
  Comp, DMai: Integer;
  Vcto, Data: TDateTime;
begin
  Vcto := Geral.ValidaDataSimples(EdVence4.Text, True);
  Data := Geral.ValidaDataSimples(EdData4.Text, True);
  DMai := EdDMaisD.ValueVariant;
  Comp := 0;  // S� para cheques
  EdDias4.ValueVariant := UMyMod.CalculaDias(Int(Data), Int(Vcto), DMai, Comp,
    Dmod.QrControleTipoPrazoDesc.Value, 0);
  EdPrazo4.Text := IntToStr(Trunc(Int(Vcto)-Int(Data)));
  //
  CalculaValoresDuplicataAlterado();
end;

procedure TFmLot2Dup.CalculaValoresDuplicataAlterado();
var
  TaxaT, Valor, JuroT: Double;
  Dias, i: Integer;
begin
  FmLot2Cab.FTaxa[0] := Geral.DMV(EdTxaCompra4.Text);
  for i := 1 to GradeDU.RowCount -1 do FmLot2Cab.FTaxa[i] :=
    Geral.DMV(GradeDU.Cells[1, i]);
  // serar n�o setados
  for i := GradeDU.RowCount to 6 do FmLot2Cab.FTaxa[i] := 0;
  TaxaT := 0;
  for i := 0 to 6 do TaxaT := TaxaT + FmLot2Cab.FTaxa[i];
  Valor := EdValor4.ValueVariant;
  Dias := EdDias4.ValueVariant;
  //juros compostos ?
  JuroT   := MLAGeral.CalculaJuroComposto(TaxaT, Dias);
  //
  EdTotalCompraPer4.Text := Geral.FFT(TaxaT, 6, siPositivo);
  //
  (*for i := 0 to 6 do
  begin
    if TaxaT = 0 then FJuro[i] := 0 else
      FJuro[i] := FTaxa[i] / TaxaT * JuroT;
  end;
  for i := 0 to 6 do
      FValr[i] := FJuro[i] * Base / 100;*)
  //////////////////////////////////////////////////////////////////////////////
  if TaxaT > FmLot2Cab.FTaxa[0] then
  begin
    if Dmod.QrControleCalcMyJuro.Value = 1 then
    begin
      for i := 0 to 6 do
      begin
        if TaxaT = 0 then FmLot2Cab.FJuro[i] := 0 else
          FmLot2Cab.FJuro[i] := FmLot2Cab.FTaxa[i] / TaxaT * JuroT;
      end;
      for i := 0 to 6 do
          FmLot2Cab.FValr[i] := FmLot2Cab.FJuro[i] * Valor / 100;
    end else begin
      FmLot2Cab.FJuro[0] := MLAGeral.CalculaJuroComposto(FmLot2Cab.FTaxa[0], Dias);
      FmLot2Cab.FValr[0] := FmLot2Cab.FJuro[0] * Valor / 100;
      JuroT := JuroT - FmLot2Cab.FJuro[0];
      TaxaT := TaxaT - FmLot2Cab.FTaxa[0];
      for i := 1 to 6 do
      begin
        if TaxaT = 0 then FmLot2Cab.FJuro[i] := 0 else
          FmLot2Cab.FJuro[i] := FmLot2Cab.FTaxa[i] / TaxaT * JuroT;
      end;
      for i := 1 to 6 do
          FmLot2Cab.FValr[i] := FmLot2Cab.FJuro[i] * Valor / 100;
    end;
  end else begin
    FmLot2Cab.FJuro[0] := JuroT;
    FmLot2Cab.FValr[0] := JuroT * Valor / 100;
  end;
end;

procedure TFmLot2Dup.CBUFKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_RETURN then
    if CBUF.DroppedDown = False then
      SendMessage(Self.Handle, WM_NEXTDLGCTL, 0, 0);
end;

procedure TFmLot2Dup.DBEdBanco4Change(Sender: TObject);
begin
  ReopenBanco4();
end;

procedure TFmLot2Dup.dmkEdTPDescAteExit(Sender: TObject);
var
  Data, Emis: TDateTime;
  Casas: Integer;
begin
  Casas := Length(EdVence4.Text);
  Data := Geral.ValidaDataSimples(EdVence4.Text, False);
  Emis := Geral.ValidaDataSimples(EdEmiss4.Text, False);
  if Data > 2 then
  begin
    if Data <= Emis then
      if Casas < 6 then Data := IncMonth(Data, 12);
    if Data < Emis then
    begin
      if Geral.MensagemBox('Data j� passada! Deseja incrementar um ano?',
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
      begin
        Data := IncMonth(Data, 12);
        EdVence4.SetFocus;
      end else begin
        EdVence4.SetFocus;
        Exit;
      end;
    end;
    EdVence4.Text := FormatDateTime('dd/mm/yyyy', Data);
  end else EdVence4.SetFocus;
  CalculaDiasDU();
end;

procedure TFmLot2Dup.EdCNPJChange(Sender: TObject);
begin
  EdCR_S.ValueVariant := 0;
  EdCV_S.ValueVariant := 0;
  EdCT_S.ValueVariant := 0;
  EdDR_S.ValueVariant := 0;
  EdDV_S.ValueVariant := 0;
  EdDT_S.ValueVariant := 0;
  EdRT_S.ValueVariant := 0;
  EdVT_S.ValueVariant := 0;
  EdST_S.ValueVariant := 0;
  EdOA_S.ValueVariant := 0;
  EdTT_S.ValueVariant := 0;
  //
  EdIE.Text      := '';
  EdSacado.Text  := '';
  EdRua.Text     := '';
  EdNumero.Text  := '';
  EdCompl.Text   := '';
  EdBairro.Text  := '';
  EdCidade.Text  := '';
  CBUF.ItemIndex := -1;
  CBUF.Text      := '';
  EdCEP.Text     := '';
  DmLot.FechaTudoSac();
end;

procedure TFmLot2Dup.EdCNPJExit(Sender: TObject);
var
  //Num : String;
  CNPJ : String;
begin
  CNPJ := Geral.SoNumero_TT(EdCNPJ.Text);
  if (CNPJ <> '') and (CNPJ = Geral.CalculaCNPJCPF(CNPJ)) then
  begin
    DmLot2.LocalizaSacado(True, EdCNPJ, EdSacado, EdRua, EdNumero, EdCompl,
      EdBairro, EdCidade, EdCEP, EdTel1, EdEmail, CBUF);
    DmLot.PesquisaRiscoSacado(EdCNPJ.Text,
      EdCR_S, EdCV_S, EdCT_S,
      EdDR_S, EdDV_S, EdDT_S,
      EdRT_S, EdVT_S, EdST_S,
      EdOA_S, EdTT_S);
  end;
end;

procedure TFmLot2Dup.EdCNPJKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F8 then
  begin
    Application.CreateForm(TFmPesqCPFCNPJ, FmPesqCPFCNPJ);
    FmPesqCPFCNPJ.ShowModal;
    FmPesqCPFCNPJ.Destroy;
    if VAR_CPF_PESQ <> '' then EdCNPJ.Text := VAR_CPF_PESQ;
    {
    Dmod.QrLocCPF.Close;
    Dmod.QrLocCPF.Params[0].AsString := Geral.SoNumero_TT(EdCNPJ.Text);
    UMyMod.AbreQuery(Dmod.QrLocCPF);
    if Dmod.QrLocCPF.RecordCount = 0 then EdEmitente1.Text := ''
    else EdEmitente1.Text := Dmod.QrLocCPFNome.Value;
    }
  end;
end;

procedure TFmLot2Dup.EdData4Exit(Sender: TObject);
var
  Data: TDateTime;
begin
  Data := Geral.ValidaDataSimples(EdData4.Text, False);
  if Data > 2 then
  begin
    EdData4.Text := FormatDateTime('dd/mm/yyyy', Data);
  end else EdData4.SetFocus;
  CalculaDiasDU();
end;

procedure TFmLot2Dup.EdDesco4Exit(Sender: TObject);
begin
  CalculaDiasDU();
end;

procedure TFmLot2Dup.EdDMaisDExit(Sender: TObject);
begin
  CalculaDiasDU();
end;

procedure TFmLot2Dup.EdDuplicataExit(Sender: TObject);
begin
{
  DmLot.QrDupNeg.Close;
  DmLot.QrDupNeg.Params[00].AsInteger := FmLot2Cab.QrLotCliente.Value;
  DmLot.QrDupNeg.Params[01].AsString  := EdDuplicata.Text;
  DmLot.QrDupNeg.Params[02].AsInteger := FmLot2Cab.QrLotCodigo.Value;
  DmLot.QrDupNeg. Open;
}
  UnDmkDAC_PF.AbreMySQLQuery0(DmLot.QrDupNeg, Dmod.MyDB, [
  'SELECT lo.Lote, li.DCompra ',
  'FROM ' + CO_TabLctA + ' li ',
  'LEFT JOIN ' + CO_TabLotA + ' lo ON lo.Codigo=li.FatNum ',
  'WHERE li.FatID=' + Geral.FF0(VAR_FATID_0301),
  'AND lo.Cliente=' + Geral.FF0(FmLot2Cab.QrLotCliente.Value),
  'AND li.Duplicata = "' + EdDuplicata.Text + '" ',
  'AND li.FatNum<>' + Geral.FF0(FmLot2Cab.QrLotCodigo.Value),
  '']);
  //
  if DmLot.QrDupNeg.RecordCount > 0 then
  begin
    Geral.MensagemBox('Duplicata j� negociada em ' + FormatDateTime(
      VAR_FORMATDATE2, DmLot.QrDupNegDCompra.Value) + ' - Lote: ' + IntToStr(
      DmLot.QrDupNegLote.Value) + '.', 'Aviso', MB_OK+MB_ICONWARNING);
    EdDuplicata.SetFocus;
  end;
end;

procedure TFmLot2Dup.EdEmiss4Exit(Sender: TObject);
var
  Data: TDateTime;
begin
  Data := Geral.ValidaDataSimples(EdEmiss4.Text, False);
  if Data > 2 then
  begin
    EdEmiss4.Text := FormatDateTime('dd/mm/yyyy', Data);
  end else EdEmiss4.SetFocus;
  CalculaDiasDU();
end;

procedure TFmLot2Dup.EdTxaCompra4Exit(Sender: TObject);
begin
  CalculaValoresDuplicataAlterado();
end;

procedure TFmLot2Dup.EdValor4Exit(Sender: TObject);
begin
  CalculaDiasDU();
end;

procedure TFmLot2Dup.EdVence4Exit(Sender: TObject);
var
  Data, Emis: TDateTime;
  Casas: Integer;
begin
  Casas := Length(EdVence4.Text);
  Data := Geral.ValidaDataSimples(EdVence4.Text, False);
  Emis := Geral.ValidaDataSimples(EdEmiss4.Text, False);
  if Data > 2 then
  begin
    if Data <= Emis then
      if Casas < 6 then Data := IncMonth(Data, 12);
    if Data < Emis then
    begin
      if Geral.MensagemBox('Data j� passada! Deseja incrementar um ano?',
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
      begin
        Data := IncMonth(Data, 12);
        EdVence4.SetFocus;
      end else begin
        EdVence4.SetFocus;
        Exit;
      end;
    end;
    EdVence4.Text := FormatDateTime('dd/mm/yyyy', Data);
  end else EdVence4.SetFocus;
  CalculaDiasDU;
end;

procedure TFmLot2Dup.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLot2Dup.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLot2Dup.GradeDUDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  OldAlign, Color: Integer;
begin
  if GradeDU = nil then Exit;
  Color := clBlack;
  SetTextColor(GradeDU.Canvas.Handle, Color);
  if ARow = 0 then
  begin
    //
  end else if ACol = 0 then begin
    if ARow <> 0 then begin
      OldAlign := SetTextAlign(GradeDU.Canvas.Handle, TA_LEFT);
      GradeDU.Canvas.TextRect(Rect, Rect.Left+2, Rect.Top + 2,
        GradeDU.Cells[Acol, ARow]);
      SetTextAlign(GradeDU.Canvas.Handle, OldAlign);
    end;
  end else if ACol = 01 then begin
      OldAlign := SetTextAlign(GradeDU.Canvas.Handle, TA_RIGHT);
      GradeDU.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Geral.TFT(GradeDU.Cells[Acol, ARow], 6, siPositivo));
      SetTextAlign(GradeDU.Canvas.Handle, OldAlign);
  end //else if ACol = 02 then begin
end;

procedure TFmLot2Dup.GradeDUKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  CalculaValoresDuplicataAlterado();
  if key=VK_RETURN then
  begin
    if GradeDU.EditorMode = True then
    begin
      if (GradeDU.Row < GradeDU.RowCount-1) then
        GradeDU.Row := GradeDU.Row +1
      else BtConfirma4.SetFocus;
    end;
  end
end;

procedure TFmLot2Dup.ReopenBanco4();
begin
  DmLot.QrBanco4.Close;
  DmLot.QrBanco4.Params[0].AsString := DBEdBanco4.Text;
  UMyMod.AbreQuery(DmLot.QrBanco4, Dmod.MyDB);
end;

end.
