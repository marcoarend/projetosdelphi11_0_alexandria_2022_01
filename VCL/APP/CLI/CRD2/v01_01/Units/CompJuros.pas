unit CompJuros;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, dmkEdit;

type
  TFmCompJuros = class(TForm)
    Panel1: TPanel;
    PainelDecomp: TPanel;
    Label11: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    EdJuros: TdmkEdit;
    EdPrazD: TdmkEdit;
    EdCasas: TdmkEdit;
    EdTaxa: TdmkEdit;
    PainelDados: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdBaseT: TdmkEdit;
    EdPrazC: TdmkEdit;
    EdJuroV: TdmkEdit;
    EdJuroP: TdmkEdit;
    EdLiqui: TdmkEdit;
    EdTaxaM: TdmkEdit;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BitBtn1: TBitBtn;
    Panel2: TPanel;
    GB_M1: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GB_M2: TGroupBox;
    LaTitulo2A: TLabel;
    LaTitulo2B: TLabel;
    LaTitulo2C: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdBaseTChange(Sender: TObject);
    procedure EdPrazCChange(Sender: TObject);
    procedure EdTaxaMChange(Sender: TObject);
    procedure EdJurosChange(Sender: TObject);
    procedure EdPrazDChange(Sender: TObject);
    procedure EdCasasChange(Sender: TObject);
  private
    { Private declarations }
    procedure CompoeJuros;
    procedure DecompoeJuros;

  public
    { Public declarations }
  end;

  var
  FmCompJuros: TFmCompJuros;

implementation

uses unMyObjects;

{$R *.DFM}

procedure TFmCompJuros.DecompoeJuros();
var
  Casas: Integer;
  Juros, PrazD, TaxaM: Double;
begin
  Casas := EdCasas.ValueVariant;
  Juros := EdJuros.ValueVariant;
  PrazD := EdPrazD.ValueVariant;
  //
  TaxaM := MLAGeral.DescobreJuroComposto(Juros, PrazD, Casas);
  EdTaxa.DecimalSize := Casas;
  EdTaxa.ValueVariant := TaxaM;
end;

procedure TFmCompJuros.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCompJuros.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCompJuros.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M1, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], 'Comp�em Juros', False, taCenter, 2, 10, 20);
  //
  MyObjects.Entitula(GB_M2, [LaTitulo2A, LaTitulo2B, LaTitulo2C],
  [LaAviso1, LaAviso2], 'Decomp�em Juros', False, taCenter, 2, 10, 20);
end;

procedure TFmCompJuros.EdBaseTChange(Sender: TObject);
begin
  CompoeJuros;
end;

procedure TFmCompJuros.EdPrazCChange(Sender: TObject);
begin
  CompoeJuros;
end;

procedure TFmCompJuros.EdTaxaMChange(Sender: TObject);
begin
  CompoeJuros;
end;

procedure TFmCompJuros.CompoeJuros();
var
  BaseT, JuroP, JuroV, PrazC, TaxaM, Liqui: Double;
begin
  BaseT := EdBaseT.ValueVariant;
  PrazC := EdPrazC.ValueVariant;
  TaxaM := EdTaxaM.ValueVariant;
  //
  JuroP := MLAGeral.CalculaJuroComposto(TaxaM, PrazC);
  JuroV := Trunc(JuroP * BaseT) / 100;
  Liqui := BaseT - JuroV;
  //
  EdJuroP.ValueVariant := JuroP;
  EdJuroV.ValueVariant := JuroV;
  EdLiqui.ValueVariant := Liqui;
end;

procedure TFmCompJuros.EdJurosChange(Sender: TObject);
begin
  DecompoeJuros;
end;

procedure TFmCompJuros.EdPrazDChange(Sender: TObject);
begin
  DecompoeJuros;
end;

procedure TFmCompJuros.EdCasasChange(Sender: TObject);
begin
  DecompoeJuros;
end;

end.
