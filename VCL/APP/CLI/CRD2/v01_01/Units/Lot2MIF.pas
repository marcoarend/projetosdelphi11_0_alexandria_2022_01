unit Lot2MIF;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  DmkDAC_PF, dmkImage, UnDmkEnums;

type
  TFmLot2MIF = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrLotIts: TmySQLQuery;
    DsLotIts: TDataSource;
    QrLotTxs: TmySQLQuery;
    QrLotTxsCodigo: TIntegerField;
    QrLotTxsControle: TIntegerField;
    QrLotTxsForma: TSmallintField;
    QrLotTxsTaxaCod: TIntegerField;
    QrLotTxsTaxaTxa: TFloatField;
    QrLotTxsTaxaQtd: TFloatField;
    QrLotTxsSysAQtd: TFloatField;
    QrLotTxsTaxaVal: TFloatField;
    QrLotTxsLk: TIntegerField;
    QrLotTxsDataCad: TDateField;
    QrLotTxsDataAlt: TDateField;
    QrLotTxsUserCad: TIntegerField;
    QrLotTxsUserAlt: TIntegerField;
    QrLotTxsAlterWeb: TSmallintField;
    QrLotTxsAtivo: TSmallintField;
    QrLotTxsData: TDateField;
    QrLotTxsCliente: TIntegerField;
    QrLotTxsPlaGen: TIntegerField;
    QrOcorP: TmySQLQuery;
    QrOcorPCodigo: TIntegerField;
    QrOcorPOcorreu: TIntegerField;
    QrOcorPData: TDateField;
    QrOcorPJuros: TFloatField;
    QrOcorPPago: TFloatField;
    QrOcorPLotePg: TIntegerField;
    QrOcorPLk: TIntegerField;
    QrOcorPDataCad: TDateField;
    QrOcorPDataAlt: TDateField;
    QrOcorPUserCad: TIntegerField;
    QrOcorPUserAlt: TIntegerField;
    QrOcorPAlterWeb: TSmallintField;
    QrOcorPAtivo: TSmallintField;
    QrOcorPPlaGen: TIntegerField;
    QrOcorPLOIS: TIntegerField;
    QrOcorPCliente: TIntegerField;
    QrOcorPOcorrencia: TIntegerField;
    QrPCH: TmySQLQuery;
    QrPCHCodigo: TIntegerField;
    QrPCHAlinIts: TIntegerField;
    QrPCHData: TDateField;
    QrPCHJuros: TFloatField;
    QrPCHPago: TFloatField;
    QrPCHDesco: TFloatField;
    QrPCHLotePg: TIntegerField;
    QrPCHAPCD: TIntegerField;
    QrPCHDataCh: TDateField;
    QrPCHLk: TIntegerField;
    QrPCHDataCad: TDateField;
    QrPCHDataAlt: TDateField;
    QrPCHUserCad: TIntegerField;
    QrPCHUserAlt: TIntegerField;
    QrPCHAlterWeb: TSmallintField;
    QrPCHAtivo: TSmallintField;
    QrPCHCliente: TIntegerField;
    QrPCHAlinea1: TIntegerField;
    QrPCHAlinea2: TIntegerField;
    QrPDU: TmySQLQuery;
    QrPDUControle: TIntegerField;
    QrPDUData: TDateField;
    QrPDUJuros: TFloatField;
    QrPDUDesco: TFloatField;
    QrPDUPago: TFloatField;
    QrPDULotePg: TIntegerField;
    QrPDULk: TIntegerField;
    QrPDUDataCad: TDateField;
    QrPDUDataAlt: TDateField;
    QrPDUUserCad: TIntegerField;
    QrPDUUserAlt: TIntegerField;
    QrPDUAlterWeb: TSmallintField;
    QrPDUAtivo: TSmallintField;
    QrPDULOIS: TIntegerField;
    QrLotItsChqPgs: TIntegerField;
    QrLotItsCodigo: TIntegerField;
    QrLotItsControle: TIntegerField;
    QrLotItsComp: TIntegerField;
    QrLotItsPraca: TIntegerField;
    QrLotItsBanco: TIntegerField;
    QrLotItsAgencia: TIntegerField;
    QrLotItsConta: TWideStringField;
    QrLotItsCheque: TIntegerField;
    QrLotItsCPF: TWideStringField;
    QrLotItsEmitente: TWideStringField;
    QrLotItsBruto: TFloatField;
    QrLotItsDesco: TFloatField;
    QrLotItsValor: TFloatField;
    QrLotItsEmissao: TDateField;
    QrLotItsDCompra: TDateField;
    QrLotItsDDeposito: TDateField;
    QrLotItsVencto: TDateField;
    QrLotItsTxaCompra: TFloatField;
    QrLotItsTxaJuros: TFloatField;
    QrLotItsTxaAdValorem: TFloatField;
    QrLotItsVlrCompra: TFloatField;
    QrLotItsVlrAdValorem: TFloatField;
    QrLotItsDMais: TIntegerField;
    QrLotItsDias: TIntegerField;
    QrLotItsDuplicata: TWideStringField;
    QrLotItsDevolucao: TIntegerField;
    QrLotItsProrrVz: TIntegerField;
    QrLotItsProrrDd: TIntegerField;
    QrLotItsQuitado: TIntegerField;
    QrLotItsBcoCobra: TIntegerField;
    QrLotItsAgeCobra: TIntegerField;
    QrLotItsTotalJr: TFloatField;
    QrLotItsTotalDs: TFloatField;
    QrLotItsTotalPg: TFloatField;
    QrLotItsData3: TDateField;
    QrLotItsRepassado: TSmallintField;
    QrLotItsDepositado: TSmallintField;
    QrLotItsValQuit: TFloatField;
    QrLotItsValDeposito: TFloatField;
    QrLotItsTipo: TIntegerField;
    QrLotItsAliIts: TIntegerField;
    QrLotItsAlinPgs: TIntegerField;
    QrLotItsNaoDeposita: TSmallintField;
    QrLotItsReforcoCxa: TSmallintField;
    QrLotItsCartDep: TIntegerField;
    QrLotItsCobranca: TIntegerField;
    QrLotItsRepCli: TIntegerField;
    QrLotItsMotivo: TWideStringField;
    QrLotItsObservacao: TWideStringField;
    QrLotItsObsGerais: TWideStringField;
    QrLotItsLk: TIntegerField;
    QrLotItsDataCad: TDateField;
    QrLotItsDataAlt: TDateField;
    QrLotItsUserCad: TIntegerField;
    QrLotItsUserAlt: TIntegerField;
    QrLotItsAlterWeb: TSmallintField;
    QrLotItsAtivo: TSmallintField;
    QrLotItsCliente: TIntegerField;
    QrLotItsData4: TDateField;
    QrLotItsDescAte: TDateField;
    QrLotItsTipific: TSmallintField;
    QrLotItsStatusSPC: TSmallintField;
    QrLotItsCNAB_Lot: TIntegerField;
    Memo1: TMemo;
    QrAIs: TmySQLQuery;
    QrAIsCodigo: TIntegerField;
    QrAIsValor: TFloatField;
    QrAIsTaxas: TFloatField;
    QrAIsMulta: TFloatField;
    QrAIsJurosV: TFloatField;
    QrAIsPgDesc: TFloatField;
    QrAIsValPago: TFloatField;
    QrAPs: TmySQLQuery;
    QrAPsCodigo: TIntegerField;
    QrAPsLotePg: TIntegerField;
    QrAPsPago: TFloatField;
    QrAPsData: TDateField;
    QrAPsDataCH: TDateField;
    QrAPsJuros: TFloatField;
    QrAPsDesco: TFloatField;
    QrAPsAlinIts: TIntegerField;
    QrAPsAPCD: TIntegerField;
    QrAPsLk: TIntegerField;
    QrAPsDataCad: TDateField;
    QrAPsDataAlt: TDateField;
    QrAPsUserCad: TIntegerField;
    QrAPsUserAlt: TIntegerField;
    QrAPsAlterWeb: TSmallintField;
    QrAPsAtivo: TSmallintField;
    QrAIsAlinea1: TIntegerField;
    QrAIsAlinea2: TIntegerField;
    QrAIsCliente: TIntegerField;
    QrDIs: TmySQLQuery;
    QrDIsLctCtrl: TIntegerField;
    QrDIsLctSub: TSmallintField;
    QrDIsFatParcela: TIntegerField;
    QrDIsCliente: TIntegerField;
    QrDIsCredito: TFloatField;
    QrDIsDebito: TFloatField;
    QrDIsVencimento: TDateField;
    QrDPs: TmySQLQuery;
    QrDPsLotesIts: TIntegerField;
    QrDPsControle: TIntegerField;
    QrDPsData: TDateField;
    QrDPsJuros: TFloatField;
    QrDPsDesco: TFloatField;
    QrDPsPago: TFloatField;
    QrDPsLotePg: TIntegerField;
    QrDPsLk: TIntegerField;
    QrDPsDataCad: TDateField;
    QrDPsDataAlt: TDateField;
    QrDPsUserCad: TIntegerField;
    QrDPsUserAlt: TIntegerField;
    QrDPsAlterWeb: TSmallintField;
    QrDPsAtivo: TSmallintField;
    QrDIsDuplicata: TWideStringField;
    QrDIsCartDep: TIntegerField;
    Panel2: TPanel;
    CGMigra: TdmkCheckGroup;
    DBGrid1: TDBGrid;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PB1: TProgressBar;
    PB0: TProgressBar;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ObtemItensDeTabelas();
  public
    { Public declarations }
    procedure MigraItens(Origem, Destino, Arquivo: String);
    procedure MigraTaxas();
    procedure MigraOcorP();
    procedure MigraPgsCH();
    procedure MigraPgsDU();
  end;

  var
  FmLot2MIF: TFmLot2MIF;

implementation

uses UnMyObjects, Module, UMySQLModule, MyListas, ModuleLot, ModuleFin;

{$R *.DFM}

const
// somente aqui!
  FPermiteQuitarControleZero = True;

procedure TFmLot2MIF.BtOKClick(Sender: TObject);
var
  Campos: String;
  //
  A, B: Integer;
  QtdFlds: Integer;
begin
  Memo1.Lines.Clear;
  if DModFin.FaltaPlaGenTaxas(LaAviso1, LaAviso2) > 0 then Exit;
  if DModFin.FaltaPlaGenOcorP(LaAviso1, LaAviso2) > 0 then Exit;
  if DModFin.FaltaPlaGenFatID(LaAviso1, LaAviso2) > 0 then Exit;
  //
  ObtemItensDeTabelas();
  //
  if Geral.IntInConjunto(1, CGMigra.Value) then
    MigraItens('lotezits', 'lct0001d', 'morto');
  //
  if Geral.IntInConjunto(2, CGMigra.Value) then
    MigraItens(TAB_LOI, CO_TabLctA, 'ativo');
  //
  if Geral.IntInConjunto(4, CGMigra.Value) then
    MigraTaxas();
  //
  if Geral.IntInConjunto(8, CGMigra.Value) then
    MigraOcorP();
  //
  if Geral.IntInConjunto(16, CGMigra.Value) then
  begin
    {
    VAR_EM_MIGRACAO_LIBERA_INCLUSAO_LIVRE := True;
    VAR_EM_MIGRACAO_LABEL_AVISO1 :=
    VAR_EM_MIGRACAO_LABEL_AVISO2
    }
    MigraPgsCH();
  end;
  //
  if Geral.IntInConjunto(32, CGMigra.Value) then
    MigraPgsDU();
  //
  if Geral.IntInConjunto(64, CGMigra.Value) then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Migrando border�s do arquivo morto');
    Campos := UMyMod.ObtemCamposDeTabelaIdentica(DMod.MyDB, 'lot0001d', '', QtdFlds);
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM lot0001d ');
    Dmod.QrUpd.SQL.Add('WHERE Codigo=0');
    Dmod.QrUpd.ExecSQL;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO lot0001d ');
    Dmod.QrUpd.SQL.Add('SELECT ');
    Dmod.QrUpd.SQL.Add(Campos);
    Dmod.QrUpd.SQL.Add('FROM lotez');
    Dmod.QrUpd.ExecSQL;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Excluindo tabelas Lotez');
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT * FROM lotez');
    UMyMod.AbreQuery(Dmod.QrAux, Dmod.MyDB);
    A := Dmod.QrAux.RecordCount;
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT * FROM lot0001d');
    UMyMod.AbreQuery(Dmod.QrAux, Dmod.MyDB);
    B := Dmod.QrAux.RecordCount;
    if A = B then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DROP TABLE lotez');
      Dmod.QrUpd.ExecSQL;
    end;
  end;
  //
  if Geral.IntInConjunto(128, CGMigra.Value) then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Migrando border�es do arquivo ativo');
    Campos := UMyMod.ObtemCamposDeTabelaIdentica(DMod.MyDB, 'lot0001a', '', QtdFlds);
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM lot0001a ');
    Dmod.QrUpd.SQL.Add('WHERE Codigo=0');
    Dmod.QrUpd.ExecSQL;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO lot0001a ');
    Dmod.QrUpd.SQL.Add('SELECT ');
    Dmod.QrUpd.SQL.Add(Campos);
    Dmod.QrUpd.SQL.Add('FROM ' + TAB_LOT);
    Dmod.QrUpd.ExecSQL;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Excluindo tabela Lot' + 'es');
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT * FROM ' + TAB_LOT);
    UMyMod.AbreQuery(Dmod.QrAux, Dmod.MyDB);
    A := Dmod.QrAux.RecordCount;
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT * FROM lot0001a');
    UMyMod.AbreQuery(Dmod.QrAux, Dmod.MyDB);
    B := Dmod.QrAux.RecordCount;
    if A = B then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DROP TABLE ' + TAB_LOT);
      Dmod.QrUpd.ExecSQL;
    end;
  end;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Migra��o finalizada!');
end;

procedure TFmLot2MIF.MigraItens(Origem, Destino, Arquivo: String);
var
  Data, Descricao, SerieNF, Compensado, SerieCH, Vencimento, Fatura, Emitente,
  ContaCorrente, CNPJCPF, Protesto, DataDoc, Duplicata, Antigo, Doc2,
  DCompra, DDeposito, DescAte, Data3, Data4, DataCad, DataAlt: String;
  //
  Tipo, Carteira, Controle, Sub, Autorizacao, Genero, NotaFiscal, Sit, FatID,
  FatID_Sub, FatParcela, ID_Pgto, ID_Quit, ID_Sub, Banco, Local, Cartao, Linha,
  OperCount, Lancto, Mez, Fornecedor, Cliente, CliInt, ForneceI, CtrlIni, Nivel,
  Vendedor, Account, Depto, DescoPor, DescoControle, Unidade, ExcelGru, CNAB_Sit,
  TipoCH, Reparcel, Atrelado, SubPgto1, MultiPgto, Protocolo, CtrlQuitPg,
  Endossas, Cancelado, EventosCad, Encerrado, ErrCtrl, IndiPag, Comp, Praca,
  DMais, Dias, Devolucao, ProrrVz, ProrrDd, Quitado, BcoCobra, AgeCobra,
  Repassado, Depositado, TpAlin, AliIts, ChqPgs, NaoDeposita, ReforcoCxa,
  CartDep, Cobranca, RepCli, (*Teste,*) Tipific, StatusSPC, CNAB_Lot, Agencia,
  Lk, UserCad, UserAlt, AlterWeb, Ativo: Integer;
  //
  Qtde, Debito, Credito, Documento, FatNum, Pago, MoraDia, Multa, MoraVal,
  MultaVal, ICMS_P, ICMS_V, DescoVal, NFVal, PagMul, PagJur, Endossan, Endossad,
  Bruto, Desco, TxaCompra, TxaJuros, TxaAdValorem, VlrCompra, VlrAdValorem,
  TotalJr, TotalDs, TotalPg, ValQuit, ValDeposito: Double;
  //
begin
  //Genero         := - 3 9 8;
  if not DModFin.ObtemGeneroDeFatID(VAR_FATID_0301, Genero, tpCred, True) then
    Exit;
  UnDmkDAC_PF.AbreMySQLQuery0(QrLotIts, Dmod.MyDB, [
  'SELECT loi.'+ FLD_CHQ_PGS + ' ChqPgs, loi.* ',
  'FROM ' + Origem + ' loi',
  'ORDER BY Codigo, Controle ',
  '']);
  //
  PB1.Max := QrLotIts.RecordCount;
  PB1.Position := 0;
  while not QrLotIts.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    PB0.Position := PB0.Position + 1;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Migrando itens do Lote ' +
      Arquivo + ' ' +
      FormatFloat('000000', QrlotItsCodigo.Value) + ' ID Chq/Dup = ' +
      FormatFloat('000000', QrlotItsControle.Value));
    //
    if QrLotItsEmissao.Value > 2 then
      Data := Geral.FDT(QrLotItsEmissao.Value, 1)
    else
      Data := Geral.FDT(QrLotItsDCompra.Value, 1);
    //  
    Tipo           := 2;
    if QrLotItsDuplicata.Value <> '' then
      Carteira       := CO_CARTEIRA_DUP_BORDERO
    else
    begin
      Carteira       := CO_CARTEIRA_CHQ_BORDERO;
      if QrLotItsCheque.Value = 0 then
        Memo1.Lines.Add('Cheque do item ' + Geral.FFN(QrlotItsControle.Value, 4) +
        ' sem n�mero de documento ser� considerado a carteira ' +
        Geral.FF0(Carteira));
    end;
    //Controle       := ; // � por �ltimo
    Sub            := 0;
    Autorizacao    := 0;
    Qtde           := 0;
    Descricao      := QrLotItsObsGerais.Value + QrLotItsMotivo.Value + QrLotItsObservacao.Value;
    SerieNF        := '';
    NotaFiscal     := 0;
    if QrLotItsValor.Value > 0 then
    begin
      Debito         := 0;
      Credito        := QrLotItsValor.Value;
    end else begin
      Debito         := -QrLotItsValor.Value;
      Credito        := 0;
    end;
    Compensado     := Geral.FDT(0, 1);
    SerieCH        := '';
    Documento      := QrLotItsCheque.Value;
    Sit            := 0;
    Vencimento     := Geral.FDT(QrLotItsVencto.Value, 1);
    FatID          := VAR_FATID_0301;
    FatID_Sub      := 0;
    FatNum         := QrLotItsCodigo.Value;
    FatParcela     := QrLotItsControle.Value;
    ID_Pgto        := 0;
    ID_Quit        := 0;
    ID_Sub         := 0;
    Fatura         := '';
    Emitente       := QrLotItsEmitente.Value;
    Banco          := QrLotItsBanco.Value;
    Agencia        := QrLotItsAgencia.Value;
    ContaCorrente  := QrLotItsConta.Value;
    CNPJCPF        := QrLotItsCPF.Value;
    Local          := 0;
    Cartao         := 0;
    Linha          := 0;
    OperCount      := 0;
    Lancto         := 0;
    Pago           := 0;
    Mez            := 0;
    Fornecedor     := 0;
    Cliente        := QrLotItsCliente.Value;
    CliInt         := -11;
    ForneceI       := 0;
    MoraDia        := 0;
    Multa          := 0;
    MoraVal        := 0;
    MultaVal       := 0;
    Protesto       := '';
    DataDoc        := DCompra;
    CtrlIni        := 0;
    Nivel          := 0;
    Vendedor       := 0;
    Account        := 0;
    ICMS_P         := 0;
    ICMS_V         := 0;
    Duplicata      := QrLotItsDuplicata.Value;
    Depto          := 0;
    DescoPor       := 0;
    DescoVal       := 0;
    DescoControle  := 0;
    Unidade        := 0;
    NFVal          := 0;
    Antigo         := '!'; // marcar como migrado
    ExcelGru       := 0;
    Doc2           := '';
    CNAB_Sit       := 0;
    TipoCH         := 0;
    Reparcel       := 0;
    Atrelado       := 0;
    PagMul         := 0;
    PagJur         := 0;
    SubPgto1       := 0;
    MultiPgto      := 0;
    Protocolo      := 0;
    CtrlQuitPg     := 0;
    Endossas       := 0;
    Endossan       := 0;
    Endossad       := 0;
    Cancelado      := 0;
    EventosCad     := 0;
    Encerrado      := 0;
    ErrCtrl        := 0;
    IndiPag        := 0;
    Comp           := QrLotItsComp.Value;
    Praca          := QrLotItsPraca.Value;
    Bruto          := QrLotItsBruto.Value;
    Desco          := QrLotItsDesco.Value;
    DCompra        := Geral.FDT(QrLotItsDCompra.Value, 1);
    DDeposito      := Geral.FDT(QrLotItsDDeposito.Value, 1);
    DescAte        := Geral.FDT(QrLotItsDescAte.Value, 1);
    TxaCompra      := QrLotItsTxaCompra.Value;
    TxaJuros       := QrLotItsTxaJuros.Value;
    TxaAdValorem   := QrLotItsTxaAdValorem.Value;
    VlrCompra      := QrLotItsVlrCompra.Value;
    VlrAdValorem   := QrLotItsVlrAdValorem.Value;
    DMais          := QrLotItsDMais.Value;
    Dias           := QrLotItsDias.Value;
    Devolucao      := QrLotItsDevolucao.Value;
    ProrrVz        := QrLotItsProrrVz.Value;
    ProrrDd        := QrLotItsProrrDd.Value;
    Quitado        := QrLotItsQuitado.Value;
    BcoCobra       := QrLotItsBcoCobra.Value;
    AgeCobra       := QrLotItsAgeCobra.Value;
    TotalJr        := QrLotItsTotalJr.Value;
    TotalDs        := QrLotItsTotalDs.Value;
    TotalPg        := QrLotItsTotalPg.Value;
    Data3          := Geral.FDT(QrLotItsData3.Value, 1);
    Data4          := Geral.FDT(QrLotItsData4.Value, 1);
    Repassado      := QrLotItsRepassado.Value;
    Depositado     := QrLotItsDepositado.Value;
    ValQuit        := QrLotItsValQuit.Value;
    ValDeposito    := QrLotItsValDeposito.Value;
    TpAlin         := QrLotItsTipo.Value;
    AliIts         := QrLotItsAliIts.Value;
    ChqPgs        := QrLotItsChqPgs.Value; // Alin#Pgs
    NaoDeposita    := QrLotItsNaoDeposita.Value;
    ReforcoCxa     := QrLotItsReforcoCxa.Value;
    CartDep        := QrLotItsCartDep.Value;
    Cobranca       := QrLotItsCobranca.Value;
    RepCli         := QrLotItsRepCli.Value;
    //Teste          := QrLotItsTeste.Value;
    Tipific        := QrLotItsTipific.Value;
    StatusSPC      := QrLotItsStatusSPC.Value;
    CNAB_Lot       := QrLotItsCNAB_Lot.Value;
    //
    Lk             := QrLotItsLk.Value;
    DataCad        := Geral.FDT(QrLotItsDataCad.Value, 1);
    DataAlt        := Geral.FDT(QrLotItsDataAlt.Value, 1);
    UserCad        := QrLotItsUserCad.Value;
    UserAlt        := QrLotItsUserAlt.Value;
    AlterWeb       := QrLotItsAlterWeb.Value;
    Ativo          := QrLotItsAtivo.Value;
    //
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', CO_TabLctA, LAN_CTOS, 'Controle');
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, Destino, False, [
    'Autorizacao', 'Genero', 'Qtde',
    'Descricao', 'SerieNF', 'NotaFiscal',
    'Debito', 'Credito', 'Compensado',
    'SerieCH', 'Documento', 'Sit',
    'Vencimento', 'FatID', 'FatID_Sub',
    'FatNum', 'FatParcela', 'ID_Pgto',
    'ID_Quit', 'ID_Sub', 'Fatura',
    'Emitente', 'Banco', 'Agencia',
    'ContaCorrente', 'CNPJCPF', 'Local',
    'Cartao', 'Linha', 'OperCount',
    'Lancto', 'Pago', 'Mez',
    'Fornecedor', 'Cliente', 'CliInt',
    'ForneceI', 'MoraDia', 'Multa',
    'MoraVal', 'MultaVal', 'Protesto',
    'DataDoc', 'CtrlIni', 'Nivel',
    'Vendedor', 'Account', 'ICMS_P',
    'ICMS_V', 'Duplicata', 'Depto',
    'DescoPor', 'DescoVal', 'DescoControle',
    'Unidade', 'NFVal', 'Antigo',
    'ExcelGru', 'Doc2', 'CNAB_Sit',
    'TipoCH', 'Reparcel', 'Atrelado',
    'PagMul', 'PagJur', 'SubPgto1',
    'MultiPgto', 'Protocolo', 'CtrlQuitPg',
    'Endossas', 'Endossan', 'Endossad',
    'Cancelado', 'EventosCad', 'Encerrado',
    'ErrCtrl', 'IndiPag', 'Comp',
    'Praca', 'Bruto', 'Desco',
    'DCompra', 'DDeposito', 'DescAte',
    'TxaCompra', 'TxaJuros', 'TxaAdValorem',
    'VlrCompra', 'VlrAdValorem', 'DMais',
    'Dias', 'Devolucao', 'ProrrVz',
    'ProrrDd', 'Quitado', 'BcoCobra',
    'AgeCobra', 'TotalJr', 'TotalDs',
    'TotalPg', 'Data3', 'Data4',
    'Repassado', 'Depositado', 'ValQuit',
    'ValDeposito', 'TpAlin', 'AliIts',
    FLD_CHQ_PGS, 'NaoDeposita', 'ReforcoCxa',
    'CartDep', 'Cobranca', 'RepCli',
    (*'Teste',*) 'Tipific', 'StatusSPC',
    'CNAB_Lot',
      'Lk', 'UserCad', 'UserAlt', 'AlterWeb', 'Ativo',
      'DataCad', 'DataAlt'
    ], [
    'Data', 'Tipo', 'Carteira', 'Controle', 'Sub'], [
    Autorizacao, Genero, Qtde,
    Descricao, SerieNF, NotaFiscal,
    Debito, Credito, Compensado,
    SerieCH, Documento, Sit,
    Vencimento, FatID, FatID_Sub,
    FatNum, FatParcela, ID_Pgto,
    ID_Quit, ID_Sub, Fatura,
    Emitente, Banco, Agencia,
    ContaCorrente, CNPJCPF, Local,
    Cartao, Linha, OperCount,
    Lancto, Pago, Mez,
    Fornecedor, Cliente, CliInt,
    ForneceI, MoraDia, Multa,
    MoraVal, MultaVal, Protesto,
    DataDoc, CtrlIni, Nivel,
    Vendedor, Account, ICMS_P,
    ICMS_V, Duplicata, Depto,
    DescoPor, DescoVal, DescoControle,
    Unidade, NFVal, Antigo,
    ExcelGru, Doc2, CNAB_Sit,
    TipoCH, Reparcel, Atrelado,
    PagMul, PagJur, SubPgto1,
    MultiPgto, Protocolo, CtrlQuitPg,
    Endossas, Endossan, Endossad,
    Cancelado, EventosCad, Encerrado,
    ErrCtrl, IndiPag, Comp,
    Praca, Bruto, Desco,
    DCompra, DDeposito, DescAte,
    TxaCompra, TxaJuros, TxaAdValorem,
    VlrCompra, VlrAdValorem, DMais,
    Dias, Devolucao, ProrrVz,
    ProrrDd, Quitado, BcoCobra,
    AgeCobra, TotalJr, TotalDs,
    TotalPg, Data3, Data4,
    Repassado, Depositado, ValQuit,
    ValDeposito, TpAlin, AliIts,
    ChqPgs, NaoDeposita, ReforcoCxa,
    CartDep, Cobranca, RepCli,
    (*Teste,*) Tipific, StatusSPC,
    CNAB_Lot,
      Lk, UserCad, UserAlt, AlterWeb, Ativo,
      DataCad, DataAlt
    ], [
    Data, Tipo, Carteira, Controle, Sub], False) then
    begin
      DMod.QrAux.Close;
      DMod.QrAux.SQL.Clear;
      DMod.QrAux.SQL.Add('SELECT Controle');
      DMod.QrAux.SQL.Add('FROM ' + Destino);
      DMod.QrAux.SQL.Add('WHERE FatID=' + FormatFloat('0', VAR_FATID_0301));
      DMod.QrAux.SQL.Add('AND FatNum=' + FormatFloat('0', QrLotItsCodigo.Value));
      DMod.QrAux.SQL.Add('AND FatParcela=' + FormatFloat('0', QrLotItsControle.Value));
      DMod.QrAux.SQL.Add('');
      UMyMod.AbreQuery(DMod.QrAux, Dmod.MyDB);
      if DMod.QrAux.RecordCount = 1 then
      begin
        DMod.QrUpd.SQL.Clear;
        DMod.QrUpd.SQL.Add('DELETE FROM ' + Origem);
        DMod.QrUpd.SQL.Add('WHERE Codigo=' + FormatFloat('0', QrLotItsCodigo.Value));
        DMod.QrUpd.SQL.Add('AND Controle=' + FormatFloat('0', QrLotItsControle.Value));
        DMod.QrUpd.ExecSQL;
      end else
      begin
        Geral.MensagemBox('ERRO ao excluir lan�amento na tabela "' + Origem + '"',
        'ERRO', MB_OK+MB_ICONERROR);
        MyObjects.Informa2(LaAviso1, LaAviso2, True,
        'ERRO ao excluir lan�amento na tabela "' + Origem + '"');
      end;
    end;
    //
    QrLotIts.Next;
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Excluindo tabela "' + Origem + '"');
  QrLotIts.Close;
  UMyMod.AbreQuery(QrLotIts, Dmod.MyDB);
  if QrLotIts.RecordCount = 0 then
  begin
    DMod.QrUpd.SQL.Clear;
    DMod.QrUpd.SQL.Add('DROP TABLE ' + Origem);
    DMod.QrUpd.ExecSQL;
    //
  end else
  begin
    Geral.MensagemBox('ERRO ao excluir tabela "' + Origem + '"!',
    'ERRO', MB_OK+MB_ICONERROR);
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'ERRO ao excluir tabela "' + Origem + '"!');
  end;
end;

procedure TFmLot2MIF.MigraTaxas();
var
  Data, Descricao, SerieNF, Compensado, SerieCH, Vencimento, Fatura, Emitente,
  ContaCorrente, CNPJCPF, Protesto, DataDoc, Duplicata, Antigo, Doc2,
  DCompra, DDeposito, DescAte, Data3, Data4, DataCad, DataAlt: String;
  //
  Tipo, Carteira, Controle, Sub, Autorizacao, Genero, NotaFiscal, Sit, FatID,
  FatID_Sub, FatParcela, ID_Pgto, ID_Quit, ID_Sub, Banco, Local, Cartao, Linha,
  OperCount, Lancto, Mez, Fornecedor, Cliente, CliInt, ForneceI, CtrlIni, Nivel,
  Vendedor, Account, Depto, DescoPor, DescoControle, Unidade, ExcelGru, CNAB_Sit,
  TipoCH, Reparcel, Atrelado, SubPgto1, MultiPgto, Protocolo, CtrlQuitPg,
  Endossas, Cancelado, EventosCad, Encerrado, ErrCtrl, IndiPag, Comp, Praca,
  DMais, Dias, Devolucao, ProrrVz, ProrrDd, Quitado, BcoCobra, AgeCobra,
  Repassado, Depositado, TpAlin, AliIts, ChqPgs, NaoDeposita, ReforcoCxa,
  CartDep, Cobranca, RepCli, (*Teste,*) Tipific, StatusSPC, CNAB_Lot, Agencia,
  Lk, UserCad, UserAlt, AlterWeb, Ativo: Integer;
  //
  Qtde, Debito, Credito, Documento, FatNum, Pago, MoraDia, Multa, MoraVal,
  MultaVal, ICMS_P, ICMS_V, DescoVal, NFVal, PagMul, PagJur, Endossan, Endossad,
  Bruto, Desco, TxaCompra, TxaJuros, TxaAdValorem, VlrCompra, VlrAdValorem,
  TotalJr, TotalDs, TotalPg, ValQuit, ValDeposito, MoraTxa: Double;
  //
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLotTxs, Dmod.MyDB, [
  'SELECT lot.Data, lot.Cliente, txa.PlaGen, txs.* ',
  'FROM ' + TAB_LOT_TXS + ' txs',
  'LEFT JOIN ' + TAB_LOT + ' lot ON lot.Codigo=txs.Codigo ',
//  'LEFT JOIN ' + CO_TabLotA + ' lot ON lot.Codigo=txs.Codigo ',
  'LEFT JOIN taxas txa ON txa.Codigo=txs.TaxaCod ',
  'ORDER BY txs.Codigo, txs.Controle ',
  '']);
  //
  PB1.Max := QrLotTxs.RecordCount;
  PB1.Position := 0;
  while not QrLotTxs.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    PB0.Position := PB0.Position + 1;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Migrando Taxas do Lote ' +
      FormatFloat('000000', QrLotTxsCodigo.Value) + ' ID item taxa = ' +
      FormatFloat('000000', QrLotTxsControle.Value));
    //
    Data           := Geral.FDT(QrLotTxsData.Value, 1);
    Tipo           := 0;
    Carteira       := -3;
    //Controle       := ; // � por �ltimo
    Sub            := 0;
    Autorizacao    := 0;
    Genero         := QrLotTxsPlaGen.Value;
    if Genero = 0 then
      if not DModFin.ObtemGeneroDeFatID(VAR_FATID_0303, Genero, tpCred, True) then
        Exit;
    Qtde           := QrLotTxsSysAQtd.Value;
    Descricao      := '';
    SerieNF        := '';
    NotaFiscal     := 0;
    if QrLotTxsTaxaVal.Value > 0 then
    begin
      Debito         := 0;
      Credito        := QrLotTxsTaxaVal.Value;
    end else begin
      Debito         := -QrLotTxsTaxaVal.Value;
      Credito        := 0;
    end;
    Compensado     := Data;
    SerieCH        := '';
    Documento      := 0;
    Sit            := 0;
    Vencimento     := Data;
    FatID          := VAR_FATID_0303;
    FatID_Sub      := QrLotTxsTaxaCod.Value;
    FatNum         := QrLotTxsCodigo.Value;
    FatParcela     := QrLotTxsControle.Value;
    ID_Pgto        := 0;
    ID_Quit        := 0;
    ID_Sub         := 0;
    Fatura         := '';
    Emitente       := '';
    Banco          := 0;
    Agencia        := 0;
    ContaCorrente  := '';
    CNPJCPF        := '';
    Local          := 0;
    Cartao         := 0;
    Linha          := 0;
    OperCount      := 0;
    Lancto         := 0;
    Pago           := 0;
    Mez            := 0;
    Fornecedor     := 0;
    Cliente        := QrLotTxsCliente.Value;
    CliInt         := -11;
    ForneceI       := 0;
    MoraDia        := QrLotTxsTaxaQtd.Value;
    Multa          := 0;
    MoraVal        := Credito;
    MultaVal       := 0;
    Protesto       := '';
    DataDoc        := '';
    CtrlIni        := 0;
    Nivel          := 0;
    Vendedor       := 0;
    Account        := 0;
    ICMS_P         := 0;
    ICMS_V         := 0;
    Duplicata      := '';
    Depto          := 0;
    DescoPor       := 0;
    DescoVal       := 0;
    DescoControle  := 0;
    Unidade        := 0;
    NFVal          := 0;
    Antigo         := '!'; // marcar como migrado
    ExcelGru       := 0;
    Doc2           := '';
    CNAB_Sit       := 0;
    TipoCH         := 0;
    Reparcel       := 0;
    Atrelado       := 0;
    PagMul         := 0;
    PagJur         := 0;
    SubPgto1       := 0;
    MultiPgto      := 0;
    Protocolo      := 0;
    CtrlQuitPg     := 0;
    Endossas       := 0;
    Endossan       := 0;
    Endossad       := 0;
    Cancelado      := 0;
    EventosCad     := 0;
    Encerrado      := 0;
    ErrCtrl        := 0;
    IndiPag        := 0;
    Comp           := 0;
    Praca          := 0;
    Bruto          := 0;
    Desco          := 0;
    DCompra        := '0000-00-00';
    DDeposito      := '0000-00-00';
    DescAte        := '0000-00-00';
    TxaCompra      := 0;
    TxaJuros       := 0;
    TxaAdValorem   := 0;
    VlrCompra      := 0;
    VlrAdValorem   := 0;
    DMais          := 0;
    Dias           := 0;
    Devolucao      := 0;
    ProrrVz        := 0;
    ProrrDd        := 0;
    Quitado        := 0;
    BcoCobra       := 0;
    AgeCobra       := 0;
    TotalJr        := 0;
    TotalDs        := 0;
    TotalPg        := 0;
    Data3          := '0000-00-00';
    Data4          := '0000-00-00';
    Repassado      := 0;
    Depositado     := 0;
    ValQuit        := 0;
    ValDeposito    := 0;
    TpAlin         := 0;
    AliIts         := 0;
    ChqPgs        := 0;
    NaoDeposita    := 0;
    ReforcoCxa     := 0;
    CartDep        := 0;
    Cobranca       := 0;
    RepCli         := 0;
    //Teste          := QrLotTxsTeste.Value;
    Tipific        := QrLotTxsForma.Value;
    StatusSPC      := 0;
    CNAB_Lot       := 0;
    //
    Lk             := QrLotTxsLk.Value;
    DataCad        := Geral.FDT(QrLotTxsDataCad.Value, 1);
    DataAlt        := Geral.FDT(QrLotTxsDataAlt.Value, 1);
    UserCad        := QrLotTxsUserCad.Value;
    UserAlt        := QrLotTxsUserAlt.Value;
    AlterWeb       := QrLotTxsAlterWeb.Value;
    Ativo          := QrLotTxsAtivo.Value;
    //
    MoraTxa        := QrLotTxsTaxaTxa.Value;
    //
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', CO_TabLctA, LAN_CTOS, 'Controle');
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, CO_TabLctA, False, [
    'Autorizacao', 'Genero', 'Qtde',
    'Descricao', 'SerieNF', 'NotaFiscal',
    'Debito', 'Credito', 'Compensado',
    'SerieCH', 'Documento', 'Sit',
    'Vencimento', 'FatID', 'FatID_Sub',
    'FatNum', 'FatParcela', 'ID_Pgto',
    'ID_Quit', 'ID_Sub', 'Fatura',
    'Emitente', 'Banco', 'Agencia',
    'ContaCorrente', 'CNPJCPF', 'Local',
    'Cartao', 'Linha', 'OperCount',
    'Lancto', 'Pago', 'Mez',
    'Fornecedor', 'Cliente', 'CliInt',
    'ForneceI', 'MoraDia', 'Multa',
    'MoraVal', 'MultaVal', 'Protesto',
    'DataDoc', 'CtrlIni', 'Nivel',
    'Vendedor', 'Account', 'ICMS_P',
    'ICMS_V', 'Duplicata', 'Depto',
    'DescoPor', 'DescoVal', 'DescoControle',
    'Unidade', 'NFVal', 'Antigo',
    'ExcelGru', 'Doc2', 'CNAB_Sit',
    'TipoCH', 'Reparcel', 'Atrelado',
    'PagMul', 'PagJur', 'SubPgto1',
    'MultiPgto', 'Protocolo', 'CtrlQuitPg',
    'Endossas', 'Endossan', 'Endossad',
    'Cancelado', 'EventosCad', 'Encerrado',
    'ErrCtrl', 'IndiPag', 'Comp',
    'Praca', 'Bruto', 'Desco',
    'DCompra', 'DDeposito', 'DescAte',
    'TxaCompra', 'TxaJuros', 'TxaAdValorem',
    'VlrCompra', 'VlrAdValorem', 'DMais',
    'Dias', 'Devolucao', 'ProrrVz',
    'ProrrDd', 'Quitado', 'BcoCobra',
    'AgeCobra', 'TotalJr', 'TotalDs',
    'TotalPg', 'Data3', 'Data4',
    'Repassado', 'Depositado', 'ValQuit',
    'ValDeposito', 'TpAlin', 'AliIts',
    FLD_CHQ_PGS, 'NaoDeposita', 'ReforcoCxa',
    'CartDep', 'Cobranca', 'RepCli',
    (*'Teste',*) 'Tipific', 'StatusSPC',
    'CNAB_Lot', 'MoraTxa',
      'Lk', 'UserCad', 'UserAlt', 'AlterWeb', 'Ativo',
      'DataCad', 'DataAlt'
    ], [
    'Data', 'Tipo', 'Carteira', 'Controle', 'Sub'], [
    Autorizacao, Genero, Qtde,
    Descricao, SerieNF, NotaFiscal,
    Debito, Credito, Compensado,
    SerieCH, Documento, Sit,
    Vencimento, FatID, FatID_Sub,
    FatNum, FatParcela, ID_Pgto,
    ID_Quit, ID_Sub, Fatura,
    Emitente, Banco, Agencia,
    ContaCorrente, CNPJCPF, Local,
    Cartao, Linha, OperCount,
    Lancto, Pago, Mez,
    Fornecedor, Cliente, CliInt,
    ForneceI, MoraDia, Multa,
    MoraVal, MultaVal, Protesto,
    DataDoc, CtrlIni, Nivel,
    Vendedor, Account, ICMS_P,
    ICMS_V, Duplicata, Depto,
    DescoPor, DescoVal, DescoControle,
    Unidade, NFVal, Antigo,
    ExcelGru, Doc2, CNAB_Sit,
    TipoCH, Reparcel, Atrelado,
    PagMul, PagJur, SubPgto1,
    MultiPgto, Protocolo, CtrlQuitPg,
    Endossas, Endossan, Endossad,
    Cancelado, EventosCad, Encerrado,
    ErrCtrl, IndiPag, Comp,
    Praca, Bruto, Desco,
    DCompra, DDeposito, DescAte,
    TxaCompra, TxaJuros, TxaAdValorem,
    VlrCompra, VlrAdValorem, DMais,
    Dias, Devolucao, ProrrVz,
    ProrrDd, Quitado, BcoCobra,
    AgeCobra, TotalJr, TotalDs,
    TotalPg, Data3, Data4,
    Repassado, Depositado, ValQuit,
    ValDeposito, TpAlin, AliIts,
    ChqPgs, NaoDeposita, ReforcoCxa,
    CartDep, Cobranca, RepCli,
    (*Teste,*) Tipific, StatusSPC,
    CNAB_Lot, MoraTxa,
      Lk, UserCad, UserAlt, AlterWeb, Ativo,
      DataCad, DataAlt
    ], [
    Data, Tipo, Carteira, Controle, Sub], False) then
    begin
      DMod.QrAux.Close;
      DMod.QrAux.SQL.Clear;
      DMod.QrAux.SQL.Add('SELECT Controle');
      DMod.QrAux.SQL.Add('FROM ' + TAB_LOT_TXS);
      DMod.QrAux.SQL.Add('WHERE Controle=' +
        FormatFloat('0', QrLotTxsControle.Value));
      DMod.QrAux.SQL.Add('');
      UMyMod.AbreQuery(DMod.QrAux, Dmod.MyDB);
      if DMod.QrAux.RecordCount = 1 then
      begin
        DMod.QrUpd.SQL.Clear;
        DMod.QrUpd.SQL.Add('DELETE FROM ' + TAB_LOT_TXS);
        DMod.QrUpd.SQL.Add('WHERE Controle=' + FormatFloat('0', QrLotTxsControle.Value));
        DMod.QrUpd.ExecSQL;
      end else
      begin
        Geral.MensagemBox('ERRO ao excluir lan�amento na tabela "' + TAB_LOT_TXS + '"',
        'ERRO', MB_OK+MB_ICONERROR);
        MyObjects.Informa2(LaAviso1, LaAviso2, True,
        'ERRO ao excluir lan�amento na tabela "' + TAB_LOT_TXS + '"');
      end;
    end;
    //
    QrLotTxs.Next;
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Excluindo tabela "' + TAB_LOT_TXS + '"');
  QrLotTxs.Close;
  UMyMod.AbreQuery(QrLotTxs, Dmod.MyDB);
  if QrLotTxs.RecordCount = 0 then
  begin
    DMod.QrUpd.SQL.Clear;
    DMod.QrUpd.SQL.Add('DROP TABLE ' + TAB_LOT_TXS);
    DMod.QrUpd.ExecSQL;
    //
  end else
  begin
    Geral.MensagemBox('ERRO ao excluir tabela "' + TAB_LOT_TXS + '"!',
    'ERRO', MB_OK+MB_ICONERROR);
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'ERRO ao excluir tabela "' + TAB_LOT_TXS + '"!');
  end;
end;

procedure TFmLot2MIF.ObtemItensDeTabelas();
var
  Liga: String;
  //
  function Migra001(var Itens: Integer): String;
  begin
    if Geral.IntInConjunto(001, CGMigra.Value) then
    begin
      Result := 'SELECT COUNT(*) Itens FROM lot' + 'esits ';
      //
      Liga := 'UNION';
    end else Result := '';
  end;
  //
  function Migra002(var Itens: Integer): String;
  begin
    if Geral.IntInConjunto(002, CGMigra.Value) then
    begin
      Result := Liga + #13#10 + 'SELECT COUNT(*) Itens FROM lot' + 'ezits ';
      //
      Liga := 'UNION';
      Itens := Itens + 1;
    end else Result := '';
  end;
  function Migra004(var Itens: Integer): String;
  begin
    if Geral.IntInConjunto(004, CGMigra.Value) then
    begin
      Result := Liga + #13#10 + 'SELECT COUNT(*) Itens FROM lot' + 'estxs ';
      //
      Liga := 'UNION';
      Itens := Itens + 1;
    end else Result := '';
  end;
  function Migra008(var Itens: Integer): String;
  begin
    if Geral.IntInConjunto(008, CGMigra.Value) then
    begin
      Result := Liga + #13#10 + 'SELECT COUNT(*) Itens FROM ocorrpg ';
      //
      Liga := 'UNION';
      Itens := Itens + 1;
    end else Result := '';
  end;
  function Migra016(var Itens: Integer): String;
  begin
    if Geral.IntInConjunto(016, CGMigra.Value) then
    begin
      Result := Liga + #13#10 + 'SELECT COUNT(*) Itens FROM alinpgs ';
      //
      Liga := 'UNION';
      Itens := Itens + 1;
    end else Result := '';
  end;
  function Migra032(var Itens: Integer): String;
  begin
    if Geral.IntInConjunto(032, CGMigra.Value) then
    begin
      Result := Liga + #13#10 + 'SELECT COUNT(*) Itens FROM aduppgs ';
      //
      Liga := 'UNION';
      Itens := Itens + 1;
    end else Result := '';
  end;
  //
var
  Itens: Integer;
begin
  Liga := '';
  Itens := 0;
  //
  try
    if UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    Migra001(Itens),
    Migra002(Itens),
    Migra004(Itens),
    Migra008(Itens),
    Migra016(Itens),
    Migra032(Itens),
    '']) then
    begin
      Itens := 0;
      Dmod.QrAux.First;
      while not Dmod.QrAux.Eof do
      begin
        Itens := Itens + Dmod.QrAux.FieldByName('Itens').AsInteger;
        Dmod.QrAux.Next;
      end;
    end;
  except
    ; // nada
  end;
  PB0.Position := 0;
  PB0.Max := Itens;
end;

procedure TFmLot2MIF.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLot2MIF.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLot2MIF.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stUpd;
  CGMigra.SetMaxValue;
  DBGrid1.DataSource := DModFin.DsSemConta;
end;

procedure TFmLot2MIF.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLot2MIF.MigraOcorP();
var
  Data, Descricao, SerieNF, Compensado, SerieCH, Vencimento, Fatura, Emitente,
  ContaCorrente, CNPJCPF, Protesto, DataDoc, Duplicata, Antigo, Doc2,
  DCompra, DDeposito, DescAte, Data3, Data4, DataCad, DataAlt: String;
  //
  Tipo, Carteira, Controle, Sub, Autorizacao, Genero, NotaFiscal, Sit, FatID,
  FatID_Sub, FatParcela, ID_Pgto, ID_Quit, ID_Sub, Banco, Local, Cartao, Linha,
  OperCount, Lancto, Mez, Fornecedor, Cliente, CliInt, ForneceI, CtrlIni, Nivel,
  Vendedor, Account, Depto, DescoPor, DescoControle, Unidade, ExcelGru, CNAB_Sit,
  TipoCH, Reparcel, Atrelado, SubPgto1, MultiPgto, Protocolo, CtrlQuitPg,
  Endossas, Cancelado, EventosCad, Encerrado, ErrCtrl, IndiPag, Comp, Praca,
  DMais, Dias, Devolucao, ProrrVz, ProrrDd, Quitado, BcoCobra, AgeCobra,
  Repassado, Depositado, TpAlin, AliIts, ChqPgs, NaoDeposita, ReforcoCxa,
  CartDep, Cobranca, RepCli, (*Teste,*) Tipific, StatusSPC, CNAB_Lot, Agencia,
  FatParcRef, Ocorreu, Lk, UserCad, UserAlt, AlterWeb, Ativo: Integer;
  //
  Qtde, Debito, Credito, Documento, FatNum, Pago, MoraDia, Multa, MoraVal,
  MultaVal, ICMS_P, ICMS_V, DescoVal, NFVal, PagMul, PagJur, Endossan, Endossad,
  Bruto, Desco, TxaCompra, TxaJuros, TxaAdValorem, VlrCompra, VlrAdValorem,
  TotalJr, TotalDs, TotalPg, ValQuit, ValDeposito, MoraTxa: Double;
  //
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOcorP, Dmod.MyDB, [
  'SELECT ocp.*, ocb.PlaGen, ',
  'oco.' + FLD_LOIS + ' LOIS, oco.Cliente, oco.Ocorrencia ',
  'FROM ' + CO_TabOcoP + ' ocp ',
  'LEFT JOIN ocorreu oco ON oco.Codigo=ocp.Ocorreu ',
  'LEFT JOIN ocorbank ocb ON ocb.Codigo=oco.Ocorrencia ',
  'ORDER BY ocp.Data, ocp.Codigo ',
  '']);
  //
  PB1.Max := QrOcorP.RecordCount;
  PB1.Position := 0;
  while not QrOcorP.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    PB0.Position := PB0.Position + 1;
    MyObjects.Informa2(LaAviso1, LaAviso2, True,
      'Migrando pagamento de ocorrencias do Lote ' +
      FormatFloat('000000', QrOcorPLotePg.Value) + ' ID item pagamento = ' +
      FormatFloat('000000', QrOcorPCodigo.Value));
    //
    Data           := Geral.FDT(QrOcorPData.Value, 1);
    Tipo           := 0;
    Carteira       := -3;
    //Controle       := ; // � por �ltimo
    Sub            := 0;
    Autorizacao    := 0;
    Genero         := QrOcorPPlaGen.Value;
    if Genero = 0 then
      if not DModFin.ObtemGeneroDeFatID(VAR_FATID_0304, Genero, tpCred, True) then
        Exit;
    Qtde           := 0;
    Descricao      := '';
    SerieNF        := '';
    NotaFiscal     := 0;
    if QrOcorPPago.Value > 0 then
    begin
      Debito         := 0;
      Credito        := QrOcorPPago.Value;
    end else begin
      Debito         := -QrOcorPPago.Value;
      Credito        := 0;
    end;
    Compensado     := Data;
    SerieCH        := '';
    Documento      := 0;
    Sit            := 0;
    Vencimento     := Data;
    FatID          := VAR_FATID_0304;
    FatID_Sub      := QrOcorPOcorrencia.Value;
    FatNum         := QrOcorPLotePg.Value;
    FatParcela     := QrOcorPCodigo.Value;
    ID_Pgto        := 0;
    ID_Quit        := 0;
    ID_Sub         := 0;
    Fatura         := '';
    Emitente       := '';
    Banco          := 0;
    Agencia        := 0;
    ContaCorrente  := '';
    CNPJCPF        := '';
    Local          := 0;
    Cartao         := 0;
    Linha          := 0;
    OperCount      := 0;
    Lancto         := 0;
    Pago           := 0;
    Mez            := 0;
    Fornecedor     := 0;
    Cliente        := QrOcorPCliente.Value;
    CliInt         := -11;
    ForneceI       := 0;
    MoraDia        := 0;
    Multa          := 0;
    MoraVal        := QrOcorPJuros.Value;
    MultaVal       := 0;
    Protesto       := '';
    DataDoc        := '';
    CtrlIni        := 0;
    Nivel          := 0;
    Vendedor       := 0;
    Account        := 0;
    ICMS_P         := 0;
    ICMS_V         := 0;
    Duplicata      := '';
    Depto          := 0;
    DescoPor       := 0;
    DescoVal       := 0;
    DescoControle  := 0;
    Unidade        := 0;
    NFVal          := 0;
    Antigo         := '!'; // marcar como migrado
    ExcelGru       := 0;
    Doc2           := '';
    CNAB_Sit       := 0;
    TipoCH         := 0;
    Reparcel       := 0;
    Atrelado       := 0;
    PagMul         := 0;
    PagJur         := 0;
    SubPgto1       := 0;
    MultiPgto      := 0;
    Protocolo      := 0;
    CtrlQuitPg     := 0;
    Endossas       := 0;
    Endossan       := 0;
    Endossad       := 0;
    Cancelado      := 0;
    EventosCad     := 0;
    Encerrado      := 0;
    ErrCtrl        := 0;
    IndiPag        := 0;
    Comp           := 0;
    Praca          := 0;
    Bruto          := 0;
    Desco          := 0;
    DCompra        := '0000-00-00';
    DDeposito      := '0000-00-00';
    DescAte        := '0000-00-00';
    TxaCompra      := 0;
    TxaJuros       := 0;
    TxaAdValorem   := 0;
    VlrCompra      := 0;
    VlrAdValorem   := 0;
    DMais          := 0;
    Dias           := 0;
    Devolucao      := 0;
    ProrrVz        := 0;
    ProrrDd        := 0;
    Quitado        := 0;
    BcoCobra       := 0;
    AgeCobra       := 0;
    TotalJr        := 0;
    TotalDs        := 0;
    TotalPg        := 0;
    Data3          := '0000-00-00';
    Data4          := '0000-00-00';
    Repassado      := 0;
    Depositado     := 0;
    ValQuit        := 0;
    ValDeposito    := 0;
    TpAlin         := 0;
    AliIts         := 0;
    ChqPgs        := 0;
    NaoDeposita    := 0;
    ReforcoCxa     := 0;
    CartDep        := 0;
    Cobranca       := 0;
    RepCli         := 0;
    //Teste          := QrOcorPTeste.Value;
    Tipific        := 0;
    StatusSPC      := 0;
    CNAB_Lot       := 0;
    //
    Lk             := QrOcorPLk.Value;
    DataCad        := Geral.FDT(QrOcorPDataCad.Value, 1);
    DataAlt        := Geral.FDT(QrOcorPDataAlt.Value, 1);
    UserCad        := QrOcorPUserCad.Value;
    UserAlt        := QrOcorPUserAlt.Value;
    AlterWeb       := QrOcorPAlterWeb.Value;
    Ativo          := QrOcorPAtivo.Value;
    //
    MoraTxa        := 0;
    FatParcRef     := QrOcorPLOIS.Value;
    Ocorreu        := QrOcorPOcorreu.Value;
    //
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', CO_TabLctA, LAN_CTOS, 'Controle');
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, CO_TabLctA, False, [
    'Autorizacao', 'Genero', 'Qtde',
    'Descricao', 'SerieNF', 'NotaFiscal',
    'Debito', 'Credito', 'Compensado',
    'SerieCH', 'Documento', 'Sit',
    'Vencimento', 'FatID', 'FatID_Sub',
    'FatNum', 'FatParcela', 'ID_Pgto',
    'ID_Quit', 'ID_Sub', 'Fatura',
    'Emitente', 'Banco', 'Agencia',
    'ContaCorrente', 'CNPJCPF', 'Local',
    'Cartao', 'Linha', 'OperCount',
    'Lancto', 'Pago', 'Mez',
    'Fornecedor', 'Cliente', 'CliInt',
    'ForneceI', 'MoraDia', 'Multa',
    'MoraVal', 'MultaVal', 'Protesto',
    'DataDoc', 'CtrlIni', 'Nivel',
    'Vendedor', 'Account', 'ICMS_P',
    'ICMS_V', 'Duplicata', 'Depto',
    'DescoPor', 'DescoVal', 'DescoControle',
    'Unidade', 'NFVal', 'Antigo',
    'ExcelGru', 'Doc2', 'CNAB_Sit',
    'TipoCH', 'Reparcel', 'Atrelado',
    'PagMul', 'PagJur', 'SubPgto1',
    'MultiPgto', 'Protocolo', 'CtrlQuitPg',
    'Endossas', 'Endossan', 'Endossad',
    'Cancelado', 'EventosCad', 'Encerrado',
    'ErrCtrl', 'IndiPag', 'Comp',
    'Praca', 'Bruto', 'Desco',
    'DCompra', 'DDeposito', 'DescAte',
    'TxaCompra', 'TxaJuros', 'TxaAdValorem',
    'VlrCompra', 'VlrAdValorem', 'DMais',
    'Dias', 'Devolucao', 'ProrrVz',
    'ProrrDd', 'Quitado', 'BcoCobra',
    'AgeCobra', 'TotalJr', 'TotalDs',
    'TotalPg', 'Data3', 'Data4',
    'Repassado', 'Depositado', 'ValQuit',
    'ValDeposito', 'TpAlin', 'AliIts',
    FLD_CHQ_PGS, 'NaoDeposita', 'ReforcoCxa',
    'CartDep', 'Cobranca', 'RepCli',
    (*'Teste',*) 'Tipific', 'StatusSPC',
    'CNAB_Lot', 'MoraTxa', 'FatParcRef',
    'Ocorreu',
      'Lk', 'UserCad', 'UserAlt', 'AlterWeb', 'Ativo',
      'DataCad', 'DataAlt'
    ], [
    'Data', 'Tipo', 'Carteira', 'Controle', 'Sub'], [
    Autorizacao, Genero, Qtde,
    Descricao, SerieNF, NotaFiscal,
    Debito, Credito, Compensado,
    SerieCH, Documento, Sit,
    Vencimento, FatID, FatID_Sub,
    FatNum, FatParcela, ID_Pgto,
    ID_Quit, ID_Sub, Fatura,
    Emitente, Banco, Agencia,
    ContaCorrente, CNPJCPF, Local,
    Cartao, Linha, OperCount,
    Lancto, Pago, Mez,
    Fornecedor, Cliente, CliInt,
    ForneceI, MoraDia, Multa,
    MoraVal, MultaVal, Protesto,
    DataDoc, CtrlIni, Nivel,
    Vendedor, Account, ICMS_P,
    ICMS_V, Duplicata, Depto,
    DescoPor, DescoVal, DescoControle,
    Unidade, NFVal, Antigo,
    ExcelGru, Doc2, CNAB_Sit,
    TipoCH, Reparcel, Atrelado,
    PagMul, PagJur, SubPgto1,
    MultiPgto, Protocolo, CtrlQuitPg,
    Endossas, Endossan, Endossad,
    Cancelado, EventosCad, Encerrado,
    ErrCtrl, IndiPag, Comp,
    Praca, Bruto, Desco,
    DCompra, DDeposito, DescAte,
    TxaCompra, TxaJuros, TxaAdValorem,
    VlrCompra, VlrAdValorem, DMais,
    Dias, Devolucao, ProrrVz,
    ProrrDd, Quitado, BcoCobra,
    AgeCobra, TotalJr, TotalDs,
    TotalPg, Data3, Data4,
    Repassado, Depositado, ValQuit,
    ValDeposito, TpAlin, AliIts,
    ChqPgs, NaoDeposita, ReforcoCxa,
    CartDep, Cobranca, RepCli,
    (*Teste,*) Tipific, StatusSPC,
    CNAB_Lot, MoraTxa, FatParcRef,
    Ocorreu,
      Lk, UserCad, UserAlt, AlterWeb, Ativo,
      DataCad, DataAlt
    ], [
    Data, Tipo, Carteira, Controle, Sub], False) then
    begin
      DMod.QrAux.Close;
      DMod.QrAux.SQL.Clear;
      DMod.QrAux.SQL.Add('SELECT Codigo');
      DMod.QrAux.SQL.Add('FROM ' + CO_TabOcoP);
      DMod.QrAux.SQL.Add('WHERE Codigo=' +
        FormatFloat('0', QrOcorPCodigo.Value));
      DMod.QrAux.SQL.Add('');
      UMyMod.AbreQuery(DMod.QrAux, Dmod.MyDB);
      if DMod.QrAux.RecordCount = 1 then
      begin
        DMod.QrUpd.SQL.Clear;
        DMod.QrUpd.SQL.Add('DELETE FROM ' + CO_TabOcoP);
        DMod.QrUpd.SQL.Add('WHERE Codigo=' + FormatFloat('0', QrOcorPCodigo.Value));
        DMod.QrUpd.ExecSQL;
      end else
      begin
        Geral.MensagemBox('ERRO ao excluir lan�amento na tabela "' + CO_TabOcoP + '"',
        'ERRO', MB_OK+MB_ICONERROR);
        MyObjects.Informa2(LaAviso1, LaAviso2, True,
        'ERRO ao excluir lan�amento na tabela "' + CO_TabOcoP + '"');
      end;
    end;
    //
    QrOcorP.Next;
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Excluindo tabela "' + CO_TabOcoP + '"');
  QrOcorP.Close;
  UMyMod.AbreQuery(QrOcorP, Dmod.MyDB);
  if QrOcorP.RecordCount = 0 then
  begin
    DMod.QrUpd.SQL.Clear;
    DMod.QrUpd.SQL.Add('DROP TABLE ' + CO_TabOcoP);
    DMod.QrUpd.ExecSQL;
    //
  end else
  begin
    Geral.MensagemBox('ERRO ao excluir tabela "' + CO_TabOcoP + '"!',
    'ERRO', MB_OK+MB_ICONERROR);
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'ERRO ao excluir tabela "' + CO_TabOcoP + '"!');
  end;
end;

(*
procedure TFmLot2MIF.MigraPgsCH();
const
  OcorAPCD = 0;
var
  Data, Descricao, SerieNF, Compensado, SerieCH, Vencimento, Fatura, Emitente,
  ContaCorrente, CNPJCPF, Protesto, DataDoc, Duplicata, Antigo, Doc2,
  DCompra, DDeposito, DescAte, Data3, Data4, DataCad, DataAlt, DataCH: String;
  //
  Tipo, Carteira, Controle, Sub, Autorizacao, Genero, NotaFiscal, Sit, FatID,
  FatID_Sub, FatParcela, ID_Pgto, ID_Quit, ID_Sub, Banco, Local, Cartao, Linha,
  OperCount, Lancto, Mez, Fornecedor, Cliente, CliInt, ForneceI, CtrlIni, Nivel,
  Vendedor, Account, Depto, DescoPor, DescoControle, Unidade, ExcelGru, CNAB_Sit,
  TipoCH, Reparcel, Atrelado, SubPgto1, MultiPgto, Protocolo, CtrlQuitPg,
  Endossas, Cancelado, EventosCad, Encerrado, ErrCtrl, IndiPag, Comp, Praca,
  DMais, Dias, Devolucao, ProrrVz, ProrrDd, Quitado, BcoCobra, AgeCobra,
  Repassado, Depositado, TpAlin, AliIts, ChqPgs, NaoDeposita, ReforcoCxa,
  CartDep, Cobranca, RepCli, {Teste,} Tipific, StatusSPC, CNAB_Lot, Agencia,
  FatParcRef, Ocorreu, Lk, UserCad, UserAlt, AlterWeb, Ativo: Integer;
  //
  Qtde, Debito, Credito, Documento, FatNum, Pago, MoraDia, Multa, MoraVal,
  MultaVal, ICMS_P, ICMS_V, DescoVal, NFVal, PagMul, PagJur, Endossan, Endossad,
  Bruto, Desco, TxaCompra, TxaJuros, TxaAdValorem, VlrCompra, VlrAdValorem,
  TotalJr, TotalDs, TotalPg, ValQuit, ValDeposito, MoraTxa: Double;
  //
begin
{ TODO : ZZZ  Mudar para 303 + 304 + 305 + 307 + 311 }
//
  UnDmkDAC_PF.AbreMySQLQuery0(QrPgsCH, Dmod.MyDB, [
  'SELECT /*apc.PlaGen,*/ ali.Cliente, ',
  'ali.Alinea1, ali.Alinea2, apg.* ',
  'FROM '  + CO_TabPgCH + ' apg ',
  'LEFT JOIN alinits ali ON ali.Codigo=apg.AlinIts ',
  'LEFT JOIN apcd apc ON apc.Codigo=apg.APCD ',
  'ORDER BY apg.Codigo, apg.Data ',
  '']);
  //
  PB1.Max := QrPgsCH.RecordCount;
  PB1.Position := 0;
  while not QrPgsCH.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    PB0.Position := PB0.Position + 1;
    MyObjects.Informa2(LaAviso1, LaAviso2, True,
      'Migrando pagamento de cheques devolvidos do Lote ' +
      FormatFloat('000000', QrPgsCHLotePg.Value) + ' ID item pagamento = ' +
      FormatFloat('000000', QrPgsCHCodigo.Value));
    //
    if not DmLot.BuscaGenAPCD(OcorAPCD, QrPgsCHPago.Value, Genero) then
      Halt(0);
    Data           := Geral.FDT(QrPgsCHData.Value, 1);
    DataCH         := Geral.FDT(QrPgsCHDataCH.Value, 1);
    Tipo           := 0;
    Carteira       := -3;
    //Controle       := ; // � por �ltimo
    Sub            := 0;
    Autorizacao    := 0;
    if not DmLot.BuscaGenAPCD(OcorAPCD, QrPgsCHPago.Value, Genero) then
      Halt(0);
    Qtde           := 0;
    Descricao      := '';
    SerieNF        := '';
    NotaFiscal     := 0;
    Debito         := 0;
    Credito        := QrPgsCHPago.Value;
    Compensado     := Data;
    SerieCH        := '';
    Documento      := 0;
    Sit            := 6; // Devolvido
    Vencimento     := DataCH;
    FatID          := VAR_FATID_0305;
    FatID_Sub      := QrPgsCHAlinea1.Value;
    FatNum         := QrPgsCHLotePg.Value;
    FatParcela     := QrPgsCHCodigo.Value;
    ID_Pgto        := 0;
    ID_Quit        := 0;
    ID_Sub         := 0;
    Fatura         := '';
    Emitente       := '';
    Banco          := 0;
    Agencia        := 0;
    ContaCorrente  := '';
    CNPJCPF        := '';
    Local          := 0;
    Cartao         := 0;
    Linha          := 0;
    OperCount      := 0;
    Lancto         := 0;
    Pago           := 0;
    Mez            := 0;
    Fornecedor     := 0;
    Cliente        := QrPgsCHCliente.Value;
    CliInt         := -11;
    ForneceI       := 0;
    MoraDia        := 0;
    Multa          := 0;
    MoraVal        := QrPgsCHJuros.Value;
    MultaVal       := 0;
    Protesto       := '';
    DataDoc        := '';
    CtrlIni        := 0;
    Nivel          := 0;
    Vendedor       := 0;
    Account        := 0;
    ICMS_P         := 0;
    ICMS_V         := 0;
    Duplicata      := '';
    Depto          := 0;
    DescoPor       := 0;
    DescoVal       := 0;
    DescoControle  := 0;
    Unidade        := 0;
    NFVal          := 0;
    Antigo         := '!'; // marcar como migrado
    ExcelGru       := 0;
    Doc2           := '';
    CNAB_Sit       := 0;
    TipoCH         := 0;
    Reparcel       := 0;
    Atrelado       := 0;
    PagMul         := 0;
    PagJur         := 0;
    SubPgto1       := 0;
    MultiPgto      := 0;
    Protocolo      := 0;
    CtrlQuitPg     := 0;
    Endossas       := 0;
    Endossan       := 0;
    Endossad       := 0;
    Cancelado      := 0;
    EventosCad     := 0;
    Encerrado      := 0;
    ErrCtrl        := 0;
    IndiPag        := 0;
    Comp           := 0;
    Praca          := 0;
    Bruto          := 0;
    Desco          := QrPgsCHDesco.Value;
    DCompra        := '0000-00-00';
    DDeposito      := '0000-00-00';
    DescAte        := '0000-00-00';
    TxaCompra      := 0;
    TxaJuros       := 0;
    TxaAdValorem   := 0;
    VlrCompra      := 0;
    VlrAdValorem   := 0;
    DMais          := 0;
    Dias           := 0;
    Devolucao      := 0;
    ProrrVz        := 0;
    ProrrDd        := 0;
    Quitado        := 0;
    BcoCobra       := 0;
    AgeCobra       := 0;
    TotalJr        := 0;
    TotalDs        := 0;
    TotalPg        := 0;
    Data3          := '0000-00-00';
    Data4          := '0000-00-00';
    Repassado      := 0;
    Depositado     := 0;
    ValQuit        := 0;
    ValDeposito    := 0;
    TpAlin         := 0;
    AliIts         := 0;
    ChqPgs        := 0;
    NaoDeposita    := 0;
    ReforcoCxa     := 0;
    CartDep        := 0;
    Cobranca       := 0;
    RepCli         := 0;
    //Teste          := QrPgsCHTeste.Value;
    Tipific        := 0;
    StatusSPC      := 0;
    CNAB_Lot       := 0;
    //
    Lk             := QrPgsCHLk.Value;
    DataCad        := Geral.FDT(QrPgsCHDataCad.Value, 1);
    DataAlt        := Geral.FDT(QrPgsCHDataAlt.Value, 1);
    UserCad        := QrPgsCHUserCad.Value;
    UserAlt        := QrPgsCHUserAlt.Value;
    AlterWeb       := QrPgsCHAlterWeb.Value;
    Ativo          := QrPgsCHAtivo.Value;
    //
    MoraTxa        := 0;
    FatParcRef     := QrPgsCHAlinIts.Value;
    Ocorreu        := QrPgsCHAPCD.Value;
    //
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', CO_TabLctA, LAN_CTOS, 'Controle');
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, CO_TabLctA, False, [
    'Autorizacao', 'Genero', 'Qtde',
    'Descricao', 'SerieNF', 'NotaFiscal',
    'Debito', 'Credito', 'Compensado',
    'SerieCH', 'Documento', 'Sit',
    'Vencimento', 'FatID', 'FatID_Sub',
    'FatNum', 'FatParcela', 'ID_Pgto',
    'ID_Quit', 'ID_Sub', 'Fatura',
    'Emitente', 'Banco', 'Agencia',
    'ContaCorrente', 'CNPJCPF', 'Local',
    'Cartao', 'Linha', 'OperCount',
    'Lancto', 'Pago', 'Mez',
    'Fornecedor', 'Cliente', 'CliInt',
    'ForneceI', 'MoraDia', 'Multa',
    'MoraVal', 'MultaVal', 'Protesto',
    'DataDoc', 'CtrlIni', 'Nivel',
    'Vendedor', 'Account', 'ICMS_P',
    'ICMS_V', 'Duplicata', 'Depto',
    'DescoPor', 'DescoVal', 'DescoControle',
    'Unidade', 'NFVal', 'Antigo',
    'ExcelGru', 'Doc2', 'CNAB_Sit',
    'TipoCH', 'Reparcel', 'Atrelado',
    'PagMul', 'PagJur', 'SubPgto1',
    'MultiPgto', 'Protocolo', 'CtrlQuitPg',
    'Endossas', 'Endossan', 'Endossad',
    'Cancelado', 'EventosCad', 'Encerrado',
    'ErrCtrl', 'IndiPag', 'Comp',
    'Praca', 'Bruto', 'Desco',
    'DCompra', 'DDeposito', 'DescAte',
    'TxaCompra', 'TxaJuros', 'TxaAdValorem',
    'VlrCompra', 'VlrAdValorem', 'DMais',
    'Dias', 'Devolucao', 'ProrrVz',
    'ProrrDd', 'Quitado', 'BcoCobra',
    'AgeCobra', 'TotalJr', 'TotalDs',
    'TotalPg', 'Data3', 'Data4',
    'Repassado', 'Depositado', 'ValQuit',
    'ValDeposito', 'TpAlin', 'AliIts',
    FLD_CHQ_PGS, 'NaoDeposita', 'ReforcoCxa',
    'CartDep', 'Cobranca', 'RepCli',
    {'Teste',} 'Tipific', 'StatusSPC',
    'CNAB_Lot', 'MoraTxa', 'FatParcRef',
    'Ocorreu',
      'Lk', 'UserCad', 'UserAlt', 'AlterWeb', 'Ativo',
      'DataCad', 'DataAlt'
    ], [
    'Data', 'Tipo', 'Carteira', 'Controle', 'Sub'], [
    Autorizacao, Genero, Qtde,
    Descricao, SerieNF, NotaFiscal,
    Debito, Credito, Compensado,
    SerieCH, Documento, Sit,
    Vencimento, FatID, FatID_Sub,
    FatNum, FatParcela, ID_Pgto,
    ID_Quit, ID_Sub, Fatura,
    Emitente, Banco, Agencia,
    ContaCorrente, CNPJCPF, Local,
    Cartao, Linha, OperCount,
    Lancto, Pago, Mez,
    Fornecedor, Cliente, CliInt,
    ForneceI, MoraDia, Multa,
    MoraVal, MultaVal, Protesto,
    DataDoc, CtrlIni, Nivel,
    Vendedor, Account, ICMS_P,
    ICMS_V, Duplicata, Depto,
    DescoPor, DescoVal, DescoControle,
    Unidade, NFVal, Antigo,
    ExcelGru, Doc2, CNAB_Sit,
    TipoCH, Reparcel, Atrelado,
    PagMul, PagJur, SubPgto1,
    MultiPgto, Protocolo, CtrlQuitPg,
    Endossas, Endossan, Endossad,
    Cancelado, EventosCad, Encerrado,
    ErrCtrl, IndiPag, Comp,
    Praca, Bruto, Desco,
    DCompra, DDeposito, DescAte,
    TxaCompra, TxaJuros, TxaAdValorem,
    VlrCompra, VlrAdValorem, DMais,
    Dias, Devolucao, ProrrVz,
    ProrrDd, Quitado, BcoCobra,
    AgeCobra, TotalJr, TotalDs,
    TotalPg, Data3, Data4,
    Repassado, Depositado, ValQuit,
    ValDeposito, TpAlin, AliIts,
    ChqPgs, NaoDeposita, ReforcoCxa,
    CartDep, Cobranca, RepCli,
    {Teste,} Tipific, StatusSPC,
    CNAB_Lot, MoraTxa, FatParcRef,
    Ocorreu,
      Lk, UserCad, UserAlt, AlterWeb, Ativo,
      DataCad, DataAlt
    ], [
    Data, Tipo, Carteira, Controle, Sub], False) then
    begin
      DMod.QrAux.Close;
      DMod.QrAux.SQL.Clear;
      DMod.QrAux.SQL.Add('SELECT Codigo');
      DMod.QrAux.SQL.Add('FROM ' + CO_TabPgCH);
      DMod.QrAux.SQL.Add('WHERE Codigo=' +
        FormatFloat('0', QrPgsCHCodigo.Value));
      DMod.QrAux.SQL.Add('');
      UMyMod.AbreQuery(DMod.QrAux);
      if DMod.QrAux.RecordCount = 1 then
      begin
        DMod.QrUpd.SQL.Clear;
        DMod.QrUpd.SQL.Add('DELETE FROM ' + CO_TabPgCH);
        DMod.QrUpd.SQL.Add('WHERE Codigo=' + FormatFloat('0', QrPgsCHCodigo.Value));
        DMod.QrUpd.ExecSQL;
      end else
      begin
        Geral.MensagemBox('ERRO ao excluir lan�amento na tabela "' + CO_TabPgCH + '"',
        'ERRO', MB_OK+MB_ICONERROR);
        MyObjects.Informa2(LaAviso1, LaAviso2, True,
        'ERRO ao excluir lan�amento na tabela "' + CO_TabPgCH + '"');
      end;
    end;
    //
    QrPgsCH.Next;
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Excluindo tabela "' + CO_TabPgCH + '"');
  QrPgsCH.Close;
  UMyMod.AbreQuery(QrPgsCH);
  if QrPgsCH.RecordCount = 0 then
  begin
    DMod.QrUpd.SQL.Clear;
    DMod.QrUpd.SQL.Add('DROP TABLE ' + CO_TabPgCH);
    DMod.QrUpd.ExecSQL;
    //
  end else
  begin
    Geral.MensagemBox('ERRO ao excluir tabela "' + CO_TabPgCH + '"!',
    'ERRO', MB_OK+MB_ICONERROR);
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'ERRO ao excluir tabela "' + CO_TabPgCH + '"!');
  end;
end;
*)

procedure TFmLot2MIF.MigraPgsCH();
(*
const
  OcorAPCD = 0;
*)
var
(*
  Data, Descricao, SerieNF, Compensado, SerieCH, Vencimento, Fatura, Emitente,
  ContaCorrente, CNPJCPF, Protesto, DataDoc, Duplicata, Antigo, Doc2,
  DCompra, DDeposito, DescAte, Data3, Data4, DataCad, DataAlt, DataCH: String;
  //
  Tipo, Carteira, Controle, Sub, Autorizacao, Genero, NotaFiscal, Sit, FatID,
  ID_Pgto, ID_Quit, ID_Sub, Banco, Local, Cartao, Linha,
  OperCount, Lancto, Mez, Fornecedor, Cliente, CliInt, ForneceI, CtrlIni, Nivel,
  Vendedor, Account, Depto, DescoPor, DescoControle, Unidade, ExcelGru, CNAB_Sit,
  TipoCH, Reparcel, Atrelado, SubPgto1, MultiPgto, Protocolo, CtrlQuitPg,
  Endossas, Cancelado, EventosCad, Encerrado, ErrCtrl, IndiPag, Comp, Praca,
  DMais, Dias, Devolucao, ProrrVz, ProrrDd, Quitado, BcoCobra, AgeCobra,
  Repassado, Depositado, TpAlin, AliIts, ChqPgs, NaoDeposita, ReforcoCxa,
  Cobranca, RepCli, {Teste,} Tipific, StatusSPC, CNAB_Lot, Agencia,
  FatParcRef, Ocorreu, Lk, UserCad, UserAlt, AlterWeb, Ativo: Integer;
  //
  Qtde, Debito, Credito, Documento, Pago, MoraDia, Multa, MoraVal,
  MultaVal, ICMS_P, ICMS_V, DescoVal, NFVal, PagMul, PagJur, Endossan, Endossad,
  Bruto, Desco, TxaCompra, TxaJuros, TxaAdValorem, VlrCompra, VlrAdValorem,
  TotalJr, TotalDs, TotalPg, ValQuit, ValDeposito, MoraTxa: Double;
  //
*)
  Dta, DataCH: TDateTime;
  //
  Cliente, Ocorreu, FatParcRef, Controle, FatID_Sub, FatParcela, CartDep,
  Gen_0303, Gen_0304, Gen_0305, Gen_0307: Integer;
  //
  Base, FatNum,
  ValPagar, ValTaxas, ValMulta, ValDesco, ValJuros, ValPrinc, ValPende: Double;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrAIs, Dmod.MyDB, [
  'SELECT Codigo, Valor, Taxas, ',
  'Multa, JurosV, PgDesc, ValPago, ',
  'Alinea1, Alinea2, Cliente ',
  'FROM AlinIts ',
  'WHERE Codigo IN ',
  '     ( ',
  '     SELECT DISTINCT AlinIts ',
  '     FROM alinpgs ',
  '     ) ',
  'ORDER BY Codigo ',
  '']);
  PB1.Max := QrAIs.RecordCount;
  PB1.Position := 0;
  QrAIs.First;
  while not QrAIs.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    PB0.Position := PB0.Position + 1;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrAPs, Dmod.MyDB, [
    'SELECT Codigo, LotePg, Pago, Data, DataCH, ',
    'Juros, Desco, AlinIts, APCD, ',
    'Lk, DataCad, DataAlt, UserCad, UserAlt, AlterWeb, Ativo ',
    'FROM ' + CO_TabPgCH,
    'WHERE AlinIts=' + Geral.FF0(QrAIsCodigo.Value),
    'ORDER BY Data, Codigo ',
    '']);
    QrAPs.First;
    Base := QrAIsValor.Value;
    while not QrAPs.Eof do
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, True,
        'Migrando pagamento de cheques devolvidos do Lote ' +
        FormatFloat('000000', QrAPsLotePg.Value) + ' ID item pagamento = ' +
        FormatFloat('000000', QrAPsCodigo.Value));
      //
      FatID_Sub      := QrAIsAlinea1.Value;
      FatNum         := QrAPsLotePg.Value;
      FatParcela     := QrAPsCodigo.Value;
      Controle       := 0;
      Cliente        := QrAIsCliente.Value;
      Ocorreu        := QrAPsAPCD.Value;
      FatParcRef     := QrAIsCodigo.Value;
      Dta            := QrAPsData.Value;
      DataCH         := QrAPsDataCH.Value;
      //Desco := Geral.DMV(EdDesco2.Text);
      //
      if not DModFin.ObtemGeneroDeFatID(VAR_FATID_0303, Gen_0303, tpCred, True) then
        Exit;
      if not DModFin.ObtemGeneroDeFatID(VAR_FATID_0304, Gen_0304, tpCred, True) then
        Exit;
      if not DModFin.ObtemGeneroDeFatID(VAR_FATID_0305, Gen_0305, tpCred, True) then
        Exit;
      if not DModFin.ObtemGeneroDeFatID(VAR_FATID_0307, Gen_0307, tpDeb, True) then
        Exit;

      //

      ValPagar := QrAPsPago.Value;
      if QrAPs.recNo = 1 then
      begin
        ValTaxas := QrAIsTaxas.Value;
        ValMulta := QrAIsMulta.Value;
      end else begin
        ValTaxas := 0;
        ValMulta := 0;
      end;
      ValDesco := QrAPsDesco.Value;
      ValJuros := QrAPsDesco.Value - ValTaxas - ValMulta;
      ValPrinc := ValPagar - ValTaxas - ValMulta - ValJuros + ValDesco;
      Base := Base - ValPrinc;
      if Base > 1.00 then // dar uma margem de erro!?
        ValPende := Base
      else
        ValPende := 0;
      // N�o precisa, porque aqui est� quitando?:
      CartDep := 0;
      //
      if DmLot.InsUpd_ChqPg(Dmod.QrUpd, stIns, FatParcela, Controle, FatID_Sub,
      Gen_0303, Gen_0304, Gen_0305, Gen_0307,
      Cliente, Ocorreu, FatParcRef, FatNum, ValPagar(*, MoraVal, Desco*), DataCH, Dta,
      ValPrinc, ValTaxas, ValMulta, ValJuros, ValDesco, ValPende, CartDep,
      '!', FPermiteQuitarControleZero) then
      begin
        DMod.QrAux.Close;
        DMod.QrAux.SQL.Clear;
        DMod.QrAux.SQL.Add('SELECT Codigo');
        DMod.QrAux.SQL.Add('FROM ' + CO_TabPgCH);
        DMod.QrAux.SQL.Add('WHERE Codigo=' + Geral.FF0(QrAPsCodigo.Value));
        DMod.QrAux.SQL.Add('');
        UMyMod.AbreQuery(DMod.QrAux, Dmod.MyDB);
        if DMod.QrAux.RecordCount = 1 then
        begin
          DMod.QrUpd.SQL.Clear;
          DMod.QrUpd.SQL.Add('DELETE FROM ' + CO_TabPgCH);
          DMod.QrUpd.SQL.Add('WHERE Codigo=' + Geral.FF0(QrAPsCodigo.Value));
          DMod.QrUpd.ExecSQL;
        end else
        begin
          Geral.MensagemBox('ERRO ao excluir lan�amento na tabela "' + CO_TabPgCH + '"',
          'ERRO', MB_OK+MB_ICONERROR);
          MyObjects.Informa2(LaAviso1, LaAviso2, True,
          'ERRO ao excluir lan�amento na tabela "' + CO_TabPgCH + '"');
        end;
      end;
      //
      QrAPs.Next;
    end;
    //
    QrAIs.Next;
  end;

(*
  EXIT;

  PB1.Max := QrPgsCH.RecordCount;
  PB1.Position := 0;
  while not QrPgsCH.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    PB0.Position := PB0.Position + 1;
    MyObjects.Informa2(LaAviso1, LaAviso2, True,
      'Migrando pagamento de cheques devolvidos do Lote ' +
      FormatFloat('000000', QrPgsCHLotePg.Value) + ' ID item pagamento = ' +
      FormatFloat('000000', QrPgsCHCodigo.Value));
    //
    if not DmLot.BuscaGenAPCD(OcorAPCD, QrPgsCHPago.Value, Genero) then
      Halt(0);
    Data           := Geral.FDT(QrPgsCHData.Value, 1);
    DataCH         := Geral.FDT(QrPgsCHDataCH.Value, 1);
    Tipo           := 0;
    Carteira       := -3;
    //Controle       := ; // � por �ltimo
    Sub            := 0;
    Autorizacao    := 0;
    if not DmLot.BuscaGenAPCD(OcorAPCD, QrPgsCHPago.Value, Genero) then
      Halt(0);
    Qtde           := 0;
    Descricao      := '';
    SerieNF        := '';
    NotaFiscal     := 0;
    Debito         := 0;
    Credito        := QrPgsCHPago.Value;
    Compensado     := Data;
    SerieCH        := '';
    Documento      := 0;
    Sit            := 2; // Pago
    Vencimento     := DataCH;
    /
    FatID          := VAR_FATID_0305;
    FatID_Sub      := QrPgsCHAlinea1.Value;
    FatNum         := QrPgsCHLotePg.Value;
    FatParcela     := QrPgsCHCodigo.Value;
    ID_Pgto        := 0;
    ID_Quit        := 0;
    ID_Sub         := 0;
    Fatura         := '';
    Emitente       := '';
    Banco          := 0;
    Agencia        := 0;
    ContaCorrente  := '';
    CNPJCPF        := '';
    Local          := 0;
    Cartao         := 0;
    Linha          := 0;
    OperCount      := 0;
    Lancto         := 0;
    Pago           := 0;
    Mez            := 0;
    Fornecedor     := 0;
    Cliente        := QrPgsCHCliente.Value;
    CliInt         := -11;
    ForneceI       := 0;
    MoraDia        := 0;
    Multa          := 0;
    MoraVal        := QrPgsCHJuros.Value;
    MultaVal       := 0;
    Protesto       := '';
    DataDoc        := '';
    CtrlIni        := 0;
    Nivel          := 0;
    Vendedor       := 0;
    Account        := 0;
    ICMS_P         := 0;
    ICMS_V         := 0;
    Duplicata      := '';
    Depto          := 0;
    DescoPor       := 0;
    DescoVal       := 0;
    DescoControle  := 0;
    Unidade        := 0;
    NFVal          := 0;
    Antigo         := '!'; // marcar como migrado
    ExcelGru       := 0;
    Doc2           := '';
    CNAB_Sit       := 0;
    TipoCH         := 0;
    Reparcel       := 0;
    Atrelado       := 0;
    PagMul         := 0;
    PagJur         := 0;
    SubPgto1       := 0;
    MultiPgto      := 0;
    Protocolo      := 0;
    CtrlQuitPg     := 0;
    Endossas       := 0;
    Endossan       := 0;
    Endossad       := 0;
    Cancelado      := 0;
    EventosCad     := 0;
    Encerrado      := 0;
    ErrCtrl        := 0;
    IndiPag        := 0;
    Comp           := 0;
    Praca          := 0;
    Bruto          := 0;
    Desco          := QrPgsCHDesco.Value;
    DCompra        := '0000-00-00';
    DDeposito      := '0000-00-00';
    DescAte        := '0000-00-00';
    TxaCompra      := 0;
    TxaJuros       := 0;
    TxaAdValorem   := 0;
    VlrCompra      := 0;
    VlrAdValorem   := 0;
    DMais          := 0;
    Dias           := 0;
    Devolucao      := 0;
    ProrrVz        := 0;
    ProrrDd        := 0;
    Quitado        := 0;
    BcoCobra       := 0;
    AgeCobra       := 0;
    TotalJr        := 0;
    TotalDs        := 0;
    TotalPg        := 0;
    Data3          := '0000-00-00';
    Data4          := '0000-00-00';
    Repassado      := 0;
    Depositado     := 0;
    ValQuit        := 0;
    ValDeposito    := 0;
    TpAlin         := 0;
    AliIts         := 0;
    ChqPgs        := 0;
    NaoDeposita    := 0;
    ReforcoCxa     := 0;
    CartDep        := 0;
    Cobranca       := 0;
    RepCli         := 0;
    //Teste          := QrPgsCHTeste.Value;
    Tipific        := 0;
    StatusSPC      := 0;
    CNAB_Lot       := 0;
    //
    Lk             := QrPgsCHLk.Value;
    DataCad        := Geral.FDT(QrPgsCHDataCad.Value, 1);
    DataAlt        := Geral.FDT(QrPgsCHDataAlt.Value, 1);
    UserCad        := QrPgsCHUserCad.Value;
    UserAlt        := QrPgsCHUserAlt.Value;
    AlterWeb       := QrPgsCHAlterWeb.Value;
    Ativo          := QrPgsCHAtivo.Value;
    //
    MoraTxa        := 0;
    FatParcRef     := QrPgsCHAlinIts.Value;
    Ocorreu        := QrPgsCHAPCD.Value;
    //
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', CO_TabLctA, LAN_CTOS, 'Controle');
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, CO_TabLctA, False, [
    'Autorizacao', 'Genero', 'Qtde',
    'Descricao', 'SerieNF', 'NotaFiscal',
    'Debito', 'Credito', 'Compensado',
    'SerieCH', 'Documento', 'Sit',
    'Vencimento', 'FatID', 'FatID_Sub',
    'FatNum', 'FatParcela', 'ID_Pgto',
    'ID_Quit', 'ID_Sub', 'Fatura',
    'Emitente', 'Banco', 'Agencia',
    'ContaCorrente', 'CNPJCPF', 'Local',
    'Cartao', 'Linha', 'OperCount',
    'Lancto', 'Pago', 'Mez',
    'Fornecedor', 'Cliente', 'CliInt',
    'ForneceI', 'MoraDia', 'Multa',
    'MoraVal', 'MultaVal', 'Protesto',
    'DataDoc', 'CtrlIni', 'Nivel',
    'Vendedor', 'Account', 'ICMS_P',
    'ICMS_V', 'Duplicata', 'Depto',
    'DescoPor', 'DescoVal', 'DescoControle',
    'Unidade', 'NFVal', 'Antigo',
    'ExcelGru', 'Doc2', 'CNAB_Sit',
    'TipoCH', 'Reparcel', 'Atrelado',
    'PagMul', 'PagJur', 'SubPgto1',
    'MultiPgto', 'Protocolo', 'CtrlQuitPg',
    'Endossas', 'Endossan', 'Endossad',
    'Cancelado', 'EventosCad', 'Encerrado',
    'ErrCtrl', 'IndiPag', 'Comp',
    'Praca', 'Bruto', 'Desco',
    'DCompra', 'DDeposito', 'DescAte',
    'TxaCompra', 'TxaJuros', 'TxaAdValorem',
    'VlrCompra', 'VlrAdValorem', 'DMais',
    'Dias', 'Devolucao', 'ProrrVz',
    'ProrrDd', 'Quitado', 'BcoCobra',
    'AgeCobra', 'TotalJr', 'TotalDs',
    'TotalPg', 'Data3', 'Data4',
    'Repassado', 'Depositado', 'ValQuit',
    'ValDeposito', 'TpAlin', 'AliIts',
    FLD_CHQ_PGS, 'NaoDeposita', 'ReforcoCxa',
    'CartDep', 'Cobranca', 'RepCli',
    {'Teste',} 'Tipific', 'StatusSPC',
    'CNAB_Lot', 'MoraTxa', 'FatParcRef',
    'Ocorreu',
      'Lk', 'UserCad', 'UserAlt', 'AlterWeb', 'Ativo',
      'DataCad', 'DataAlt'
    ], [
    'Data', 'Tipo', 'Carteira', 'Controle', 'Sub'], [
    Autorizacao, Genero, Qtde,
    Descricao, SerieNF, NotaFiscal,
    Debito, Credito, Compensado,
    SerieCH, Documento, Sit,
    Vencimento, FatID, FatID_Sub,
    FatNum, FatParcela, ID_Pgto,
    ID_Quit, ID_Sub, Fatura,
    Emitente, Banco, Agencia,
    ContaCorrente, CNPJCPF, Local,
    Cartao, Linha, OperCount,
    Lancto, Pago, Mez,
    Fornecedor, Cliente, CliInt,
    ForneceI, MoraDia, Multa,
    MoraVal, MultaVal, Protesto,
    DataDoc, CtrlIni, Nivel,
    Vendedor, Account, ICMS_P,
    ICMS_V, Duplicata, Depto,
    DescoPor, DescoVal, DescoControle,
    Unidade, NFVal, Antigo,
    ExcelGru, Doc2, CNAB_Sit,
    TipoCH, Reparcel, Atrelado,
    PagMul, PagJur, SubPgto1,
    MultiPgto, Protocolo, CtrlQuitPg,
    Endossas, Endossan, Endossad,
    Cancelado, EventosCad, Encerrado,
    ErrCtrl, IndiPag, Comp,
    Praca, Bruto, Desco,
    DCompra, DDeposito, DescAte,
    TxaCompra, TxaJuros, TxaAdValorem,
    VlrCompra, VlrAdValorem, DMais,
    Dias, Devolucao, ProrrVz,
    ProrrDd, Quitado, BcoCobra,
    AgeCobra, TotalJr, TotalDs,
    TotalPg, Data3, Data4,
    Repassado, Depositado, ValQuit,
    ValDeposito, TpAlin, AliIts,
    ChqPgs, NaoDeposita, ReforcoCxa,
    CartDep, Cobranca, RepCli,
    {Teste,} Tipific, StatusSPC,
    CNAB_Lot, MoraTxa, FatParcRef,
    Ocorreu,
      Lk, UserCad, UserAlt, AlterWeb, Ativo,
      DataCad, DataAlt
    ], [
    Data, Tipo, Carteira, Controle, Sub], False) then
    begin
      DMod.QrAux.Close;
      DMod.QrAux.SQL.Clear;
      DMod.QrAux.SQL.Add('SELECT Codigo');
      DMod.QrAux.SQL.Add('FROM ' + CO_TabPgCH);
      DMod.QrAux.SQL.Add('WHERE Codigo=' +
        FormatFloat('0', QrPgsCHCodigo.Value));
      DMod.QrAux.SQL.Add('');
      UMyMod.AbreQuery(DMod.QrAux);
      if DMod.QrAux.RecordCount = 1 then
      begin
        DMod.QrUpd.SQL.Clear;
        DMod.QrUpd.SQL.Add('DELETE FROM ' + CO_TabPgCH);
        DMod.QrUpd.SQL.Add('WHERE Codigo=' + FormatFloat('0', QrPgsCHCodigo.Value));
        DMod.QrUpd.ExecSQL;
      end else
      begin
        Geral.MensagemBox('ERRO ao excluir lan�amento na tabela "' + CO_TabPgCH + '"',
        'ERRO', MB_OK+MB_ICONERROR);
        MyObjects.Informa2(LaAviso1, LaAviso2, True,
        'ERRO ao excluir lan�amento na tabela "' + CO_TabPgCH + '"');
      end;
    end;
    //
    QrPgsCH.Next;
  end;
*)
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Excluindo tabela "' + CO_TabPgCH + '"');
  UnDmkDAC_PF.AbreMySQLQuery0(QrPCH, Dmod.MyDB, [
  'SELECT /*apc.PlaGen,*/ ali.Cliente, ',
  'ali.Alinea1, ali.Alinea2, apg.* ',
  'FROM '  + CO_TabPgCH + ' apg ',
  'LEFT JOIN alinits ali ON ali.Codigo=apg.AlinIts ',
  'LEFT JOIN apcd apc ON apc.Codigo=apg.APCD ',
  'ORDER BY apg.Codigo, apg.Data ',
  '']);
  //
  UMyMod.AbreQuery(QrPCH, Dmod.MyDB);
  if QrPCH.RecordCount = 0 then
  begin
    DMod.QrUpd.SQL.Clear;
    DMod.QrUpd.SQL.Add('DROP TABLE ' + CO_TabPgCH);
    DMod.QrUpd.ExecSQL;
    //
  end else
  begin
    Geral.MensagemBox('ERRO ao excluir tabela "' + CO_TabPgCH + '"!',
    'ERRO', MB_OK+MB_ICONERROR);
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'ERRO ao excluir tabela "' + CO_TabPgCH + '"!');
  end;
end;

(*
procedure TFmLot2MIF.MigraPgsDU();
var
  Data, Descricao, SerieNF, Compensado, SerieCH, Vencimento, Fatura, Emitente,
  ContaCorrente, CNPJCPF, Protesto, DataDoc, Duplicata, Antigo, Doc2,
  DCompra, DDeposito, DescAte, Data3, Data4, DataCad, DataAlt, DataDU: String;
  //
  Tipo, Carteira, Controle, Sub, Autorizacao, Genero, NotaFiscal, Sit, FatID,
  FatID_Sub, FatParcela, ID_Pgto, ID_Quit, ID_Sub, Banco, Local, Cartao, Linha,
  OperCount, Lancto, Mez, Fornecedor, Cliente, CliInt, ForneceI, CtrlIni, Nivel,
  Vendedor, Account, Depto, DescoPor, DescoControle, Unidade, ExcelGru, CNAB_Sit,
  TipoCH, Reparcel, Atrelado, SubPgto1, MultiPgto, Protocolo, CtrlQuitPg,
  Endossas, Cancelado, EventosCad, Encerrado, ErrCtrl, IndiPag, Comp, Praca,
  DMais, Dias, Devolucao, ProrrVz, ProrrDd, Quitado, BcoCobra, AgeCobra,
  Repassado, Depositado, TpAlin, AliIts, ChqPgs, NaoDeposita, ReforcoCxa,
  CartDep, Cobranca, RepCli, {Teste,} Tipific, StatusSPC, CNAB_Lot, Agencia,
  FatParcRef, Ocorreu, Lk, UserCad, UserAlt, AlterWeb, Ativo: Integer;
  //
  Qtde, Debito, Credito, Documento, FatNum, Pago, MoraDia, Multa, MoraVal,
  MultaVal, ICMS_P, ICMS_V, DescoVal, NFVal, PagMul, PagJur, Endossan, Endossad,
  Bruto, Desco, TxaCompra, TxaJuros, TxaAdValorem, VlrCompra, VlrAdValorem,
  TotalJr, TotalDs, TotalPg, ValQuit, ValDeposito, MoraTxa: Double;
  //
begin
  //Genero         := - 3 9 3; // Pagamento de Duplicata Vencida
{ TODO :   ZZZ  Mudar para 303 + 304 + 306 + 308 + 312 ??? }

  if not DModFin.ObtemGeneroDeFatID(VAR_FATID_0306, Genero, tpCred, True) then
    Exit;
  UnDmkDAC_PF.AbreMySQLQuery0(QrPgsDU, Dmod.MyDB, [
  'SELECT apg.' + FLD_LOIS + ' LOIS, apg.* ',
  'FROM ' + CO_TabPgDU + ' apg ',
  'ORDER BY Controle, Data ',
  '']);
  //
  PB1.Max := QrPgsDU.RecordCount;
  PB1.Position := 0;
  while not QrPgsDU.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    PB0.Position := PB0.Position + 1;
    MyObjects.Informa2(LaAviso1, LaAviso2, True,
      'Migrando pagamento de duplicatas atrasadas do Lote ' +
      FormatFloat('000000', QrPgsDULotePg.Value) + ' ID item pagamento = ' +
      FormatFloat('000000', QrPgsDUControle.Value));
    //
    DmLot.ReopenADupIts(QrPgsDULOIS.Value);
    DmLot.ReopenLOI(QrPgsDULOIS.Value);
    //
    Data           := Geral.FDT(QrPgsDUData.Value, 1);
    Vencimento     := Geral.FDT(DmLot.QrLOIVencimento.Value, 1);
    Tipo           := 0;
    Carteira       := -3;
    //Controle       := ; // � por �ltimo
    Sub            := 0;
    Autorizacao    := 0;
    Qtde           := 0;
    Descricao      := '';
    SerieNF        := '';
    NotaFiscal     := 0;
    Debito         := 0;
    Credito        := QrPgsDUPago.Value;
    Compensado     := Data;
    SerieCH        := '';
    Documento      := 0;
    Sit            := 0;
    Vencimento     := Vencimento;
    FatID          := VAR_FATID_03??;
    FatID_Sub      := DmLot.QrAdupItsAlinea.Value;
    FatNum         := QrPgsDULotePg.Value;
    FatParcela     := QrPgsDUControle.Value;
    ID_Pgto        := 0;
    ID_Quit        := 0;
    ID_Sub         := 0;
    Fatura         := '';
    Emitente       := '';
    Banco          := 0;
    Agencia        := 0;
    ContaCorrente  := '';
    CNPJCPF        := '';
    Local          := 0;
    Cartao         := 0;
    Linha          := 0;
    OperCount      := 0;
    Lancto         := 0;
    Pago           := 0;
    Mez            := 0;
    Fornecedor     := 0;
    Cliente        := DmLot.QrLOICliente.Value;
    CliInt         := -11;
    ForneceI       := 0;
    MoraDia        := 0;
    Multa          := 0;
    MoraVal        := QrPgsDUJuros.Value;
    MultaVal       := 0;
    Protesto       := '';
    DataDoc        := '';
    CtrlIni        := 0;
    Nivel          := 0;
    Vendedor       := 0;
    Account        := 0;
    ICMS_P         := 0;
    ICMS_V         := 0;
    Duplicata      := '';
    Depto          := 0;
    DescoPor       := 0;
    DescoVal       := 0;
    DescoControle  := 0;
    Unidade        := 0;
    NFVal          := 0;
    Antigo         := '!'; // marcar como migrado
    ExcelGru       := 0;
    Doc2           := '';
    CNAB_Sit       := 0;
    TipoCH         := 0;
    Reparcel       := 0;
    Atrelado       := 0;
    PagMul         := 0;
    PagJur         := 0;
    SubPgto1       := 0;
    MultiPgto      := 0;
    Protocolo      := 0;
    CtrlQuitPg     := 0;
    Endossas       := 0;
    Endossan       := 0;
    Endossad       := 0;
    Cancelado      := 0;
    EventosCad     := 0;
    Encerrado      := 0;
    ErrCtrl        := 0;
    IndiPag        := 0;
    Comp           := 0;
    Praca          := 0;
    Bruto          := 0;
    Desco          := QrPgsDUDesco.Value;
    DCompra        := '0000-00-00';
    DDeposito      := '0000-00-00';
    DescAte        := '0000-00-00';
    TxaCompra      := 0;
    TxaJuros       := 0;
    TxaAdValorem   := 0;
    VlrCompra      := 0;
    VlrAdValorem   := 0;
    DMais          := 0;
    Dias           := 0;
    Devolucao      := 0;
    ProrrVz        := 0;
    ProrrDd        := 0;
    Quitado        := 0;
    BcoCobra       := 0;
    AgeCobra       := 0;
    TotalJr        := 0;
    TotalDs        := 0;
    TotalPg        := 0;
    Data3          := '0000-00-00';
    Data4          := '0000-00-00';
    Repassado      := 0;
    Depositado     := 0;
    ValQuit        := 0;
    ValDeposito    := 0;
    TpAlin         := 0;
    AliIts         := 0;
    ChqPgs         := 0;
    NaoDeposita    := 0;
    ReforcoCxa     := 0;
    CartDep        := 0;
    Cobranca       := 0;
    RepCli         := 0;
    //Teste          := QrPgsDUTeste.Value;
    Tipific        := 0;
    StatusSPC      := 0;
    CNAB_Lot       := 0;
    //
    Lk             := QrPgsDULk.Value;
    DataCad        := Geral.FDT(QrPgsDUDataCad.Value, 1);
    DataAlt        := Geral.FDT(QrPgsDUDataAlt.Value, 1);
    UserCad        := QrPgsDUUserCad.Value;
    UserAlt        := QrPgsDUUserAlt.Value;
    AlterWeb       := QrPgsDUAlterWeb.Value;
    Ativo          := QrPgsDUAtivo.Value;
    //
    MoraTxa        := 0;
    FatParcRef     := QrPgsDULOIS.Value;
    Ocorreu        := DmLot.QrAdupItsControle.Value;
    //
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', CO_TabLctA, LAN_CTOS, 'Controle');
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, CO_TabLctA, False, [
    'Autorizacao', 'Genero', 'Qtde',
    'Descricao', 'SerieNF', 'NotaFiscal',
    'Debito', 'Credito', 'Compensado',
    'SerieCH', 'Documento', 'Sit',
    'Vencimento', 'FatID', 'FatID_Sub',
    'FatNum', 'FatParcela', 'ID_Pgto',
    'ID_Quit', 'ID_Sub', 'Fatura',
    'Emitente', 'Banco', 'Agencia',
    'ContaCorrente', 'CNPJCPF', 'Local',
    'Cartao', 'Linha', 'OperCount',
    'Lancto', 'Pago', 'Mez',
    'Fornecedor', 'Cliente', 'CliInt',
    'ForneceI', 'MoraDia', 'Multa',
    'MoraVal', 'MultaVal', 'Protesto',
    'DataDoc', 'CtrlIni', 'Nivel',
    'Vendedor', 'Account', 'ICMS_P',
    'ICMS_V', 'Duplicata', 'Depto',
    'DescoPor', 'DescoVal', 'DescoControle',
    'Unidade', 'NFVal', 'Antigo',
    'ExcelGru', 'Doc2', 'CNAB_Sit',
    'TipoCH', 'Reparcel', 'Atrelado',
    'PagMul', 'PagJur', 'SubPgto1',
    'MultiPgto', 'Protocolo', 'CtrlQuitPg',
    'Endossas', 'Endossan', 'Endossad',
    'Cancelado', 'EventosCad', 'Encerrado',
    'ErrCtrl', 'IndiPag', 'Comp',
    'Praca', 'Bruto', 'Desco',
    'DCompra', 'DDeposito', 'DescAte',
    'TxaCompra', 'TxaJuros', 'TxaAdValorem',
    'VlrCompra', 'VlrAdValorem', 'DMais',
    'Dias', 'Devolucao', 'ProrrVz',
    'ProrrDd', 'Quitado', 'BcoCobra',
    'AgeCobra', 'TotalJr', 'TotalDs',
    'TotalPg', 'Data3', 'Data4',
    'Repassado', 'Depositado', 'ValQuit',
    'ValDeposito', 'TpAlin', 'AliIts',
    FLD_CHQ_PGS, 'NaoDeposita', 'ReforcoCxa',
    'CartDep', 'Cobranca', 'RepCli',
    {'Teste',} 'Tipific', 'StatusSPC',
    'CNAB_Lot', 'MoraTxa', 'FatParcRef',
    'Ocorreu',
      'Lk', 'UserCad', 'UserAlt', 'AlterWeb', 'Ativo',
      'DataCad', 'DataAlt'
    ], [
    'Data', 'Tipo', 'Carteira', 'Controle', 'Sub'], [
    Autorizacao, Genero, Qtde,
    Descricao, SerieNF, NotaFiscal,
    Debito, Credito, Compensado,
    SerieCH, Documento, Sit,
    Vencimento, FatID, FatID_Sub,
    FatNum, FatParcela, ID_Pgto,
    ID_Quit, ID_Sub, Fatura,
    Emitente, Banco, Agencia,
    ContaCorrente, CNPJCPF, Local,
    Cartao, Linha, OperCount,
    Lancto, Pago, Mez,
    Fornecedor, Cliente, CliInt,
    ForneceI, MoraDia, Multa,
    MoraVal, MultaVal, Protesto,
    DataDoc, CtrlIni, Nivel,
    Vendedor, Account, ICMS_P,
    ICMS_V, Duplicata, Depto,
    DescoPor, DescoVal, DescoControle,
    Unidade, NFVal, Antigo,
    ExcelGru, Doc2, CNAB_Sit,
    TipoCH, Reparcel, Atrelado,
    PagMul, PagJur, SubPgto1,
    MultiPgto, Protocolo, CtrlQuitPg,
    Endossas, Endossan, Endossad,
    Cancelado, EventosCad, Encerrado,
    ErrCtrl, IndiPag, Comp,
    Praca, Bruto, Desco,
    DCompra, DDeposito, DescAte,
    TxaCompra, TxaJuros, TxaAdValorem,
    VlrCompra, VlrAdValorem, DMais,
    Dias, Devolucao, ProrrVz,
    ProrrDd, Quitado, BcoCobra,
    AgeCobra, TotalJr, TotalDs,
    TotalPg, Data3, Data4,
    Repassado, Depositado, ValQuit,
    ValDeposito, TpAlin, AliIts,
    ChqPgs, NaoDeposita, ReforcoCxa,
    CartDep, Cobranca, RepCli,
    {Teste,} Tipific, StatusSPC,
    CNAB_Lot, MoraTxa, FatParcRef,
    Ocorreu,
      Lk, UserCad, UserAlt, AlterWeb, Ativo,
      DataCad, DataAlt
    ], [
    Data, Tipo, Carteira, Controle, Sub], False) then
    begin
      DMod.QrAux.Close;
      DMod.QrAux.SQL.Clear;
      DMod.QrAux.SQL.Add('SELECT Controle');
      DMod.QrAux.SQL.Add('FROM ' + CO_TabPgDU);
      DMod.QrAux.SQL.Add('WHERE Controle=' +
        FormatFloat('0', QrPgsDUControle.Value));
      DMod.QrAux.SQL.Add('');
      UMyMod.AbreQuery(DMod.QrAux);
      if DMod.QrAux.RecordCount = 1 then
      begin
        DMod.QrUpd.SQL.Clear;
        DMod.QrUpd.SQL.Add('DELETE FROM ' + CO_TabPgDU);
        DMod.QrUpd.SQL.Add('WHERE Controle=' + FormatFloat('0', QrPgsDUControle.Value));
        DMod.QrUpd.ExecSQL;
      end else
      begin
        Geral.MensagemBox('ERRO ao excluir lan�amento na tabela "' + CO_TabPgDU + '"',
        'ERRO', MB_OK+MB_ICONERROR);
        MyObjects.Informa2(LaAviso1, LaAviso2, True,
        'ERRO ao excluir lan�amento na tabela "' + CO_TabPgDU + '"');
      end;
    end;
    //
    QrPgsDU.Next;
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Excluindo tabela "' + CO_TabPgDU + '"');
  QrPgsDU.Close;
  UMyMod.AbreQuery(QrPgsDU);
  if QrPgsDU.RecordCount = 0 then
  begin
    DMod.QrUpd.SQL.Clear;
    DMod.QrUpd.SQL.Add('DROP TABLE ' + CO_TabPgDU);
    DMod.QrUpd.ExecSQL;
    //
  end else
  begin
    Geral.MensagemBox('ERRO ao excluir tabela "' + CO_TabPgDU + '"!',
    'ERRO', MB_OK+MB_ICONERROR);
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'ERRO ao excluir tabela "' + CO_TabPgDU + '"!');
  end;
end;
*)

procedure TFmLot2MIF.MigraPgsDU();
var
{
  Data, Descricao, SerieNF, Compensado, SerieCH, Vencimento, Fatura, Emitente,
  ContaCorrente, CNPJCPF, Protesto, DataDoc, Duplicata, Antigo, Doc2,
  DCompra, DDeposito, DescAte, Data3, Data4, DataCad, DataAlt, DataDU: String;
  //
  Tipo, Carteira, Controle, Sub, Autorizacao, Genero, NotaFiscal, Sit, FatID,
  FatID_Sub, FatParcela, ID_Pgto, ID_Quit, ID_Sub, Banco, Local, Cartao, Linha,
  OperCount, Lancto, Mez, Fornecedor, Cliente, CliInt, ForneceI, CtrlIni, Nivel,
  Vendedor, Account, Depto, DescoPor, DescoControle, Unidade, ExcelGru, CNAB_Sit,
  TipoCH, Reparcel, Atrelado, SubPgto1, MultiPgto, Protocolo, CtrlQuitPg,
  Endossas, Cancelado, EventosCad, Encerrado, ErrCtrl, IndiPag, Comp, Praca,
  DMais, Dias, Devolucao, ProrrVz, ProrrDd, Quitado, BcoCobra, AgeCobra,
  Repassado, Depositado, TpAlin, AliIts, ChqPgs, NaoDeposita, ReforcoCxa,
  CartDep, Cobranca, RepCli, (*Teste,*) Tipific, StatusSPC, CNAB_Lot, Agencia,
  FatParcRef, Ocorreu, Lk, UserCad, UserAlt, AlterWeb, Ativo: Integer;
  //
  Qtde, Debito, Credito, Documento, FatNum, Pago, MoraDia, Multa, MoraVal,
  MultaVal, ICMS_P, ICMS_V, DescoVal, NFVal, PagMul, PagJur, Endossan, Endossad,
  Bruto, Desco, TxaCompra, TxaJuros, TxaAdValorem, VlrCompra, VlrAdValorem,
  TotalJr, TotalDs, TotalPg, ValQuit, ValDeposito, MoraTxa: Double;
  //
}
  FatParcref, FatParcela, Controle, FatID_Sub, Cliente, Ocorreu, LctCtrl,
  LctSub, CartDep,
  Gen_0303, Gen_0304, Gen_0306, Gen_0308: Integer;
  //
  Base, FatNum,
  ValPagar, ValTaxas, ValMulta, ValDesco, ValJuros, ValPrinc, ValPende: Double;
  Dta, dataDU: TDateTime;
  Duplicata: String;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo tabela de duplicatas');
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrDIs, Dmod.MyDB, [
  'SELECT Controle LctCtrl, Sub LctSub, ',
  'FatParcela, Cliente, Credito, Debito, ',
  'Vencimento, Duplicata, CartDep ',
  'FROM ' + CO_TabLctA,
  'WHERE FatID=' + TXT_VAR_FATID_0301,
  'AND FatParcela IN ',
  '     ( ',
  '     SELECT DISTINCT LotesIts ',
  '     FROM aduppgs ',
  '     ) ',
  'ORDER BY FatParcela ',
  '']);
  //
  PB1.Max := QrDIs.RecordCount;
  PB1.Position := 0;
  QrDIs.First;
  while not QrDIs.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    PB0.Position := PB0.Position + 1;
    FatParcRef := QrDIsFatParcela.Value;
    UnDmkDAC_PF.AbreMySQLQuery0(QrDPs, Dmod.MyDB, [
    'SELECT * ',
    'FROM aduppgs ',
    'WHERE LotesIts=' + Geral.FF0(FatParcRef),
    'ORDER BY Data, LotesIts ',
    '']);
    //
    Base := QrDIsCredito.Value;
    QrDPs.First;
    while not QrDPs.Eof do
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, True,
        'Migrando pagamento de duplicatas atrasadas do Lote ' +
        FormatFloat('000000', QrDPsLotePg.Value) + ' ID item pagamento = ' +
        FormatFloat('000000', QrDPsControle.Value));
        //
      DmLot.ReopenAdupIts(FatParcRef);
      FatNum := QrDPsLotePg.Value;

      FatID_Sub      := DmLot.QrAdupItsAlinea.Value;
      FatParcela     := QrDPsControle.Value;
      Controle := 0;
      Cliente := QrDIsCliente.Value;
      Ocorreu := DmLot.QrAdupItsControle.Value;
      Dta := QrDPsData.Value;
      DataDU := Dta;
      //
      if not DModFin.ObtemGeneroDeFatID(VAR_FATID_0303, Gen_0303, tpCred, True) then
        Exit;
      if not DModFin.ObtemGeneroDeFatID(VAR_FATID_0304, Gen_0304, tpCred, True) then
        Exit;
      if not DModFin.ObtemGeneroDeFatID(VAR_FATID_0306, Gen_0306, tpCred, True) then
        Exit;
      if not DModFin.ObtemGeneroDeFatID(VAR_FATID_0308, Gen_0308, tpDeb, True) then
        Exit;
      //
      ValPagar := QrDPsPago.Value;
      ValTaxas := 0; // N�o tem aqui! � separado!
      ValMulta := 0; // N�o tem aqui! � separado!
      ValDesco := QrDPsDesco.Value;
      ValJuros := QrDPsJuros.Value;
      ValPrinc := ValPagar - ValTaxas - ValMulta - ValJuros + ValDesco;
      Base := Base - ValPrinc;
      if Base > 1.00 then // dar uma margem de erro!?
        ValPende := Base
      else
        ValPende := 0;
      CartDep := 2; //QrDIsCartDep.Value;  2 = "Banco ?"
      //
      LctCtrl := QrDIsLctCtrl.Value;
      LctSub := QrDIsLctSub.Value;
      Duplicata := QrDIsDuplicata.Value;

      if DmLot.InsUpd_DupPg(Dmod.QrUpd, stIns, LctCtrl, LctSub, FatParcela, Controle,
      FatID_Sub, Gen_0303, Gen_0304, Gen_0306, Gen_0308, Cliente, Ocorreu,
      FatParcRef, FatNum, ValPagar(*, MoraVal, Desco*), DataDU, Dta, Duplicata,
      ValPrinc, ValTaxas, ValMulta, ValJuros, ValDesco, ValPende, CartDep,
      '!', FPermiteQuitarControleZero) then
      begin
        DMod.QrAux.Close;
        DMod.QrAux.SQL.Clear;
        DMod.QrAux.SQL.Add('SELECT Controle');
        DMod.QrAux.SQL.Add('FROM ' + CO_TabPgDU);
        DMod.QrAux.SQL.Add('WHERE Controle=' +
          FormatFloat('0', QrDPsControle.Value));
        DMod.QrAux.SQL.Add('');
        UMyMod.AbreQuery(DMod.QrAux, Dmod.MyDB);
        if DMod.QrAux.RecordCount = 1 then
        begin
          DMod.QrUpd.SQL.Clear;
          DMod.QrUpd.SQL.Add('DELETE FROM ' + CO_TabPgDU);
          DMod.QrUpd.SQL.Add('WHERE Controle=' + FormatFloat('0', QrDPsControle.Value));
          DMod.QrUpd.ExecSQL;
        end else
        begin
          Geral.MensagemBox('ERRO ao excluir lan�amento na tabela "' + CO_TabPgDU + '"',
          'ERRO', MB_OK+MB_ICONERROR);
          MyObjects.Informa2(LaAviso1, LaAviso2, True,
          'ERRO ao excluir lan�amento na tabela "' + CO_TabPgDU + '"');
        end;
      end;
      //
      QrDPs.Next;
    end;
    QrDIs.Next;
  end;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Excluindo tabela "' + CO_TabPgDU + '"');
  UnDmkDAC_PF.AbreMySQLQuery0(QrPDU, Dmod.MyDB, [
  'SELECT apg.' + FLD_LOIS + ' LOIS, apg.* ',
  'FROM ' + CO_TabPgDU + ' apg ',
  'ORDER BY Controle, Data ',
  '']);
  if QrPDU.RecordCount = 0 then
  begin
    DMod.QrUpd.SQL.Clear;
    DMod.QrUpd.SQL.Add('DROP TABLE ' + CO_TabPgDU);
    DMod.QrUpd.ExecSQL;
    //
  end else
  begin
    Geral.MensagemBox('ERRO ao excluir tabela "' + CO_TabPgDU + '"!' + #13#10 +
    'Existem ' + Geral.FF0(QrPDU.RecordCount) + ' itens impedindo sua exclus�o!',
    'ERRO', MB_OK+MB_ICONERROR);
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'ERRO ao excluir tabela "' + CO_TabPgDU + '"!');
  end;

  //
(*
  if not DModFin.ObtemGeneroDeFatID(VAR_FATID_0306, Genero, tpCred, True) then
    Exit;
  UnDmkDAC_PF.AbreMySQLQuery0(QrPgsDU, Dmod.MyDB, [
  'SELECT apg.' + FLD_LOIS + ' LOIS, apg.* ',
  'FROM ' + CO_TabPgDU + ' apg ',
  'ORDER BY Controle, Data ',
  '']);
  //
  PB1.Max := QrPgsDU.RecordCount;
  PB1.Position := 0;
  while not QrPgsDU.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    PB0.Position := PB0.Position + 1;
    MyObjects.Informa2(LaAviso1, LaAviso2, True,
      'Migrando pagamento de duplicatas atrasadas do Lote ' +
      FormatFloat('000000', QrPgsDULotePg.Value) + ' ID item pagamento = ' +
      FormatFloat('000000', QrPgsDUControle.Value));
    //
    DmLot.ReopenADupIts(QrPgsDULOIS.Value);
    DmLot.ReopenLOI(QrPgsDULOIS.Value);
    //
    Data           := Geral.FDT(QrPgsDUData.Value, 1);
    Vencimento     := Geral.FDT(DmLot.QrLOIVencimento.Value, 1);
    Tipo           := 0;
    Carteira       := -3;
    //Controle       := ; // � por �ltimo
    Sub            := 0;
    Autorizacao    := 0;
    Qtde           := 0;
    Descricao      := '';
    SerieNF        := '';
    NotaFiscal     := 0;
    Debito         := 0;
    Credito        := QrPgsDUPago.Value;
    Compensado     := Data;
    SerieCH        := '';
    Documento      := 0;
    Sit            := 0;
    Vencimento     := Vencimento;
    FatID          := VAR_FATID_03??;
    FatID_Sub      := DmLot.QrAdupItsAlinea.Value;
    FatNum         := QrPgsDULotePg.Value;
    FatParcela     := QrPgsDUControle.Value;
    ID_Pgto        := 0;
    ID_Quit        := 0;
    ID_Sub         := 0;
    Fatura         := '';
    Emitente       := '';
    Banco          := 0;
    Agencia        := 0;
    ContaCorrente  := '';
    CNPJCPF        := '';
    Local          := 0;
    Cartao         := 0;
    Linha          := 0;
    OperCount      := 0;
    Lancto         := 0;
    Pago           := 0;
    Mez            := 0;
    Fornecedor     := 0;
    Cliente        := DmLot.QrLOICliente.Value;
    CliInt         := -11;
    ForneceI       := 0;
    MoraDia        := 0;
    Multa          := 0;
    MoraVal        := QrPgsDUJuros.Value;
    MultaVal       := 0;
    Protesto       := '';
    DataDoc        := '';
    CtrlIni        := 0;
    Nivel          := 0;
    Vendedor       := 0;
    Account        := 0;
    ICMS_P         := 0;
    ICMS_V         := 0;
    Duplicata      := '';
    Depto          := 0;
    DescoPor       := 0;
    DescoVal       := 0;
    DescoControle  := 0;
    Unidade        := 0;
    NFVal          := 0;
    Antigo         := '!'; // marcar como migrado
    ExcelGru       := 0;
    Doc2           := '';
    CNAB_Sit       := 0;
    TipoCH         := 0;
    Reparcel       := 0;
    Atrelado       := 0;
    PagMul         := 0;
    PagJur         := 0;
    SubPgto1       := 0;
    MultiPgto      := 0;
    Protocolo      := 0;
    CtrlQuitPg     := 0;
    Endossas       := 0;
    Endossan       := 0;
    Endossad       := 0;
    Cancelado      := 0;
    EventosCad     := 0;
    Encerrado      := 0;
    ErrCtrl        := 0;
    IndiPag        := 0;
    Comp           := 0;
    Praca          := 0;
    Bruto          := 0;
    Desco          := QrPgsDUDesco.Value;
    DCompra        := '0000-00-00';
    DDeposito      := '0000-00-00';
    DescAte        := '0000-00-00';
    TxaCompra      := 0;
    TxaJuros       := 0;
    TxaAdValorem   := 0;
    VlrCompra      := 0;
    VlrAdValorem   := 0;
    DMais          := 0;
    Dias           := 0;
    Devolucao      := 0;
    ProrrVz        := 0;
    ProrrDd        := 0;
    Quitado        := 0;
    BcoCobra       := 0;
    AgeCobra       := 0;
    TotalJr        := 0;
    TotalDs        := 0;
    TotalPg        := 0;
    Data3          := '0000-00-00';
    Data4          := '0000-00-00';
    Repassado      := 0;
    Depositado     := 0;
    ValQuit        := 0;
    ValDeposito    := 0;
    TpAlin         := 0;
    AliIts         := 0;
    ChqPgs         := 0;
    NaoDeposita    := 0;
    ReforcoCxa     := 0;
    CartDep        := 0;
    Cobranca       := 0;
    RepCli         := 0;
    //Teste          := QrPgsDUTeste.Value;
    Tipific        := 0;
    StatusSPC      := 0;
    CNAB_Lot       := 0;
    //
    Lk             := QrPgsDULk.Value;
    DataCad        := Geral.FDT(QrPgsDUDataCad.Value, 1);
    DataAlt        := Geral.FDT(QrPgsDUDataAlt.Value, 1);
    UserCad        := QrPgsDUUserCad.Value;
    UserAlt        := QrPgsDUUserAlt.Value;
    AlterWeb       := QrPgsDUAlterWeb.Value;
    Ativo          := QrPgsDUAtivo.Value;
    //
    MoraTxa        := 0;
    FatParcRef     := QrPgsDULOIS.Value;
    Ocorreu        := DmLot.QrAdupItsControle.Value;
    //
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', CO_TabLctA, LAN_CTOS, 'Controle');
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, CO_TabLctA, False, [
    'Autorizacao', 'Genero', 'Qtde',
    'Descricao', 'SerieNF', 'NotaFiscal',
    'Debito', 'Credito', 'Compensado',
    'SerieCH', 'Documento', 'Sit',
    'Vencimento', 'FatID', 'FatID_Sub',
    'FatNum', 'FatParcela', 'ID_Pgto',
    'ID_Quit', 'ID_Sub', 'Fatura',
    'Emitente', 'Banco', 'Agencia',
    'ContaCorrente', 'CNPJCPF', 'Local',
    'Cartao', 'Linha', 'OperCount',
    'Lancto', 'Pago', 'Mez',
    'Fornecedor', 'Cliente', 'CliInt',
    'ForneceI', 'MoraDia', 'Multa',
    'MoraVal', 'MultaVal', 'Protesto',
    'DataDoc', 'CtrlIni', 'Nivel',
    'Vendedor', 'Account', 'ICMS_P',
    'ICMS_V', 'Duplicata', 'Depto',
    'DescoPor', 'DescoVal', 'DescoControle',
    'Unidade', 'NFVal', 'Antigo',
    'ExcelGru', 'Doc2', 'CNAB_Sit',
    'TipoCH', 'Reparcel', 'Atrelado',
    'PagMul', 'PagJur', 'SubPgto1',
    'MultiPgto', 'Protocolo', 'CtrlQuitPg',
    'Endossas', 'Endossan', 'Endossad',
    'Cancelado', 'EventosCad', 'Encerrado',
    'ErrCtrl', 'IndiPag', 'Comp',
    'Praca', 'Bruto', 'Desco',
    'DCompra', 'DDeposito', 'DescAte',
    'TxaCompra', 'TxaJuros', 'TxaAdValorem',
    'VlrCompra', 'VlrAdValorem', 'DMais',
    'Dias', 'Devolucao', 'ProrrVz',
    'ProrrDd', 'Quitado', 'BcoCobra',
    'AgeCobra', 'TotalJr', 'TotalDs',
    'TotalPg', 'Data3', 'Data4',
    'Repassado', 'Depositado', 'ValQuit',
    'ValDeposito', 'TpAlin', 'AliIts',
    FLD_CHQ_PGS, 'NaoDeposita', 'ReforcoCxa',
    'CartDep', 'Cobranca', 'RepCli',
    {'Teste',} 'Tipific', 'StatusSPC',
    'CNAB_Lot', 'MoraTxa', 'FatParcRef',
    'Ocorreu',
      'Lk', 'UserCad', 'UserAlt', 'AlterWeb', 'Ativo',
      'DataCad', 'DataAlt'
    ], [
    'Data', 'Tipo', 'Carteira', 'Controle', 'Sub'], [
    Autorizacao, Genero, Qtde,
    Descricao, SerieNF, NotaFiscal,
    Debito, Credito, Compensado,
    SerieCH, Documento, Sit,
    Vencimento, FatID, FatID_Sub,
    FatNum, FatParcela, ID_Pgto,
    ID_Quit, ID_Sub, Fatura,
    Emitente, Banco, Agencia,
    ContaCorrente, CNPJCPF, Local,
    Cartao, Linha, OperCount,
    Lancto, Pago, Mez,
    Fornecedor, Cliente, CliInt,
    ForneceI, MoraDia, Multa,
    MoraVal, MultaVal, Protesto,
    DataDoc, CtrlIni, Nivel,
    Vendedor, Account, ICMS_P,
    ICMS_V, Duplicata, Depto,
    DescoPor, DescoVal, DescoControle,
    Unidade, NFVal, Antigo,
    ExcelGru, Doc2, CNAB_Sit,
    TipoCH, Reparcel, Atrelado,
    PagMul, PagJur, SubPgto1,
    MultiPgto, Protocolo, CtrlQuitPg,
    Endossas, Endossan, Endossad,
    Cancelado, EventosCad, Encerrado,
    ErrCtrl, IndiPag, Comp,
    Praca, Bruto, Desco,
    DCompra, DDeposito, DescAte,
    TxaCompra, TxaJuros, TxaAdValorem,
    VlrCompra, VlrAdValorem, DMais,
    Dias, Devolucao, ProrrVz,
    ProrrDd, Quitado, BcoCobra,
    AgeCobra, TotalJr, TotalDs,
    TotalPg, Data3, Data4,
    Repassado, Depositado, ValQuit,
    ValDeposito, TpAlin, AliIts,
    ChqPgs, NaoDeposita, ReforcoCxa,
    CartDep, Cobranca, RepCli,
    {Teste,} Tipific, StatusSPC,
    CNAB_Lot, MoraTxa, FatParcRef,
    Ocorreu,
      Lk, UserCad, UserAlt, AlterWeb, Ativo,
      DataCad, DataAlt
    ], [
    Data, Tipo, Carteira, Controle, Sub], False) then
    begin
      DMod.QrAux.Close;
      DMod.QrAux.SQL.Clear;
      DMod.QrAux.SQL.Add('SELECT Controle');
      DMod.QrAux.SQL.Add('FROM ' + CO_TabPgDU);
      DMod.QrAux.SQL.Add('WHERE Controle=' +
        FormatFloat('0', QrPgsDUControle.Value));
      DMod.QrAux.SQL.Add('');
      UMyMod.AbreQuery(DMod.QrAux);
      if DMod.QrAux.RecordCount = 1 then
      begin
        DMod.QrUpd.SQL.Clear;
        DMod.QrUpd.SQL.Add('DELETE FROM ' + CO_TabPgDU);
        DMod.QrUpd.SQL.Add('WHERE Controle=' + FormatFloat('0', QrPgsDUControle.Value));
        DMod.QrUpd.ExecSQL;
      end else
      begin
        Geral.MensagemBox('ERRO ao excluir lan�amento na tabela "' + CO_TabPgDU + '"',
        'ERRO', MB_OK+MB_ICONERROR);
        MyObjects.Informa2(LaAviso1, LaAviso2, True,
        'ERRO ao excluir lan�amento na tabela "' + CO_TabPgDU + '"');
      end;
    end;
    //
    QrPgsDU.Next;
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Excluindo tabela "' + CO_TabPgDU + '"');
  QrPgsDU.Close;
  UMyMod.AbreQuery(QrPgsDU);
  if QrPgsDU.RecordCount = 0 then
  begin
    DMod.QrUpd.SQL.Clear;
    DMod.QrUpd.SQL.Add('DROP TABLE ' + CO_TabPgDU);
    DMod.QrUpd.ExecSQL;
    //
  end else
  begin
    Geral.MensagemBox('ERRO ao excluir tabela "' + CO_TabPgDU + '"!',
    'ERRO', MB_OK+MB_ICONERROR);
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'ERRO ao excluir tabela "' + CO_TabPgDU + '"!');
  end;
*)
end;

end.
