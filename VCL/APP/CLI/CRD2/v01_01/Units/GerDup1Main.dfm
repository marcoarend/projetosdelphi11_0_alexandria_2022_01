object FmGerDup1Main: TFmGerDup1Main
  Left = 258
  Top = 164
  Caption = 'DUP-CNTRL-001 :: Controle de Duplicatas'
  ClientHeight = 701
  ClientWidth = 1016
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 52
    Width = 1016
    Height = 518
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 225
      Width = 1016
      Height = 293
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Splitter1: TSplitter
        Left = 0
        Top = 168
        Width = 1016
        Height = 3
        Cursor = crVSplit
        Align = alTop
        ExplicitLeft = 1
        ExplicitTop = 169
        ExplicitWidth = 1014
      end
      object Panel8: TPanel
        Left = 0
        Top = 0
        Width = 1016
        Height = 168
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object GradeItens: TdmkDBGrid
          Left = 0
          Top = 0
          Width = 1016
          Height = 168
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'DATA3_TXT'
              Title.Caption = 'Quitado em'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Juridico'
              Title.Caption = 'Jur'#237'dico'
              Width = 50
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMELOCAL'
              Title.Caption = 'Local'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Duplicata'
              Width = 65
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DCompra'
              Title.Caption = 'Compra'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Credito'
              Title.Alignment = taRightJustify
              Title.Caption = 'Valor'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DDeposito'
              Title.Caption = 'Resgate'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECLIENTE'
              Title.Caption = 'Cliente'
              Width = 104
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Emitente'
              Width = 129
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TOTALATUALIZ'
              Title.Caption = 'Tot.Atz'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SALDO_DESATUALIZ'
              Title.Caption = 'Saldo'
              Width = 68
              Visible = True
            end
            item
              Alignment = taRightJustify
              Expanded = False
              FieldName = 'SALDO_TEXTO'
              Title.Caption = 'Atualizado'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMESTATUS'
              Title.Caption = 'Status'
              Width = 79
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'STATUS'
              Title.Caption = #218'ltima posi'#231#227'o no hist'#243'rico'
              Width = 196
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TotalJr'
              Title.Caption = 'Juros'
              Width = 66
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TotalDs'
              Title.Caption = 'Descontos'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TotalPg'
              Title.Caption = 'Pago'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OCORRENCIAS'
              Title.Caption = 'Ocorr.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OCORATUALIZ'
              Title.Caption = 'Ocorr.Atz.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NF'
              Title.Caption = 'N.F.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CNAB_Lot'
              Title.Caption = 'Lote rem.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FatParcela'
              Title.Caption = 'ID item lote'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CartDep'
              Title.Caption = 'Cart.Dep.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'ID Lct'
              Visible = True
            end>
          Color = clWindow
          DataSource = DsPesq
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
          PopupMenu = PMDiario
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDrawColumnCell = GradeItensDrawColumnCell
          OnDblClick = GradeItensDblClick
          FieldsCalcToOrder.Strings = (
            'DATA3_TXT=Data3'
            'NOMELOCAL=LOCALT'
            'TOTALATUALIZ=Credito'
            'SALDO_DESATUALIZ=Controle'
            'SALDO_TEXTO=Controle'
            'NOMESTATUS=Controle'
            'OCORRENCIAS=Controle'
            'OCORATUALIZ=Controle')
          Columns = <
            item
              Expanded = False
              FieldName = 'DATA3_TXT'
              Title.Caption = 'Quitado em'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Juridico'
              Title.Caption = 'Jur'#237'dico'
              Width = 50
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMELOCAL'
              Title.Caption = 'Local'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Duplicata'
              Width = 65
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DCompra'
              Title.Caption = 'Compra'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Credito'
              Title.Alignment = taRightJustify
              Title.Caption = 'Valor'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DDeposito'
              Title.Caption = 'Resgate'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECLIENTE'
              Title.Caption = 'Cliente'
              Width = 104
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Emitente'
              Width = 129
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TOTALATUALIZ'
              Title.Caption = 'Tot.Atz'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SALDO_DESATUALIZ'
              Title.Caption = 'Saldo'
              Width = 68
              Visible = True
            end
            item
              Alignment = taRightJustify
              Expanded = False
              FieldName = 'SALDO_TEXTO'
              Title.Caption = 'Atualizado'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMESTATUS'
              Title.Caption = 'Status'
              Width = 79
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'STATUS'
              Title.Caption = #218'ltima posi'#231#227'o no hist'#243'rico'
              Width = 196
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TotalJr'
              Title.Caption = 'Juros'
              Width = 66
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TotalDs'
              Title.Caption = 'Descontos'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TotalPg'
              Title.Caption = 'Pago'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OCORRENCIAS'
              Title.Caption = 'Ocorr.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OCORATUALIZ'
              Title.Caption = 'Ocorr.Atz.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NF'
              Title.Caption = 'N.F.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CNAB_Lot'
              Title.Caption = 'Lote rem.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FatParcela'
              Title.Caption = 'ID item lote'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CartDep'
              Title.Caption = 'Cart.Dep.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'ID Lct'
              Visible = True
            end>
        end
      end
      object Panel7: TPanel
        Left = 0
        Top = 171
        Width = 1016
        Height = 122
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object PageControl1: TPageControl
          Left = 0
          Top = 0
          Width = 1016
          Height = 122
          ActivePage = TabSheet6
          Align = alClient
          TabOrder = 0
          OnChange = PageControl1Change
          object TabSheet1: TTabSheet
            Caption = 'Hist'#243'rico e pagamentos  '
            object DBGrid4: TDBGrid
              Left = 0
              Top = 0
              Width = 404
              Height = 94
              Align = alLeft
              DataSource = DsADupIts
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'DataA'
                  Title.Caption = 'Data'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMESTATUS'
                  Title.Caption = 'Hist'#243'rico'
                  Visible = True
                end>
            end
            object DBGrid3: TDBGrid
              Left = 404
              Top = 0
              Width = 604
              Height = 94
              Align = alClient
              DataSource = DsDupPgs
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Data'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'MoraVal'
                  Title.Caption = '$ Juros'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DescoVal'
                  Title.Caption = 'Desconto'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Credito'
                  Title.Caption = 'Pago'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Lote'
                  Title.Caption = 'Border'#244
                  Width = 51
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'FatNum'
                  Title.Caption = 'Lote Pgto'
                  Width = 56
                  Visible = True
                end>
            end
          end
          object TabSheet2: TTabSheet
            Caption = 'Ocorr'#234'ncias  '
            ImageIndex = 1
            object DBGrid5: TDBGrid
              Left = 738
              Top = 0
              Width = 270
              Height = 94
              Align = alRight
              DataSource = DsOcorP
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Data'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'MoraVal'
                  Title.Caption = 'Juros'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Pago'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'FatNum'
                  Title.Caption = 'Lote pgto'
                  Width = 48
                  Visible = True
                end>
            end
            object DBGrid1: TDBGrid
              Left = 0
              Top = 0
              Width = 738
              Height = 94
              TabStop = False
              Align = alClient
              DataSource = DsOcorreu
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'SEQ'
                  Title.Caption = 'Item'
                  Width = 26
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Title.Caption = 'Controle'
                  Width = 47
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DataO'
                  Title.Caption = 'Data'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMEOCORRENCIA'
                  Title.Caption = 'Ocorr'#234'ncia'
                  Width = 166
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Valor'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SALDO'
                  Title.Caption = 'Saldo'
                  Visible = True
                end
                item
                  Alignment = taRightJustify
                  Expanded = False
                  FieldName = 'ATZ_TEXTO'
                  Title.Caption = 'Atualizado'
                  Width = 76
                  Visible = True
                end>
            end
          end
          object TabSheet3: TTabSheet
            Caption = 'Prorroga'#231#245'es  '
            ImageIndex = 2
            object DBGrid2: TDBGrid
              Left = 0
              Top = 0
              Width = 1008
              Height = 94
              TabStop = False
              Align = alClient
              DataSource = DsLotPrr
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Data1'
                  Title.Caption = 'D.Original'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Data2'
                  Title.Caption = 'A.Anterior'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Data3'
                  Title.Caption = 'N.Vencto'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DIAS_2_3'
                  Title.Caption = 'Dias'
                  Width = 48
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DIAS_1_3'
                  Title.Caption = 'Acumul.'
                  Width = 48
                  Visible = True
                end>
            end
          end
          object TabSheet6: TTabSheet
            Caption = 'Hist'#243'rico banc'#225'rio (CNAB)'
            ImageIndex = 3
            object DBGrid6: TDBGrid
              Left = 0
              Top = 0
              Width = 1008
              Height = 94
              Align = alClient
              DataSource = DsCNAB240
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'CNAB240L'
                  Title.Caption = 'Ret. ID'
                  Width = 40
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CNAB240L'
                  Title.Caption = 'Ctrl ID'
                  Width = 44
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Banco'
                  Title.Caption = 'Bco'
                  Width = 28
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Convenio'
                  Title.Caption = 'Conv'#234'nio'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Sequencia'
                  Title.Caption = 'Seq. ID'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Lote'
                  Width = 27
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Item'
                  Width = 25
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DataOcor'
                  Title.Caption = 'Data Ocor.'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMEENVIO'
                  Title.Caption = 'Envio'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Movimento'
                  Title.Caption = 'Mov.'
                  Width = 30
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMEMOVIMENTO'
                  Title.Caption = 'Descri'#231#227'o movimento'
                  Width = 193
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'IRTCLB'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Custas'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValPago'
                  Title.Caption = 'Pago'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValCred'
                  Title.Caption = 'Cr'#233'dito'
                  Visible = True
                end>
            end
          end
          object TabSheet7: TTabSheet
            Caption = ' [Tempos] '
            ImageIndex = 4
            object Memo1: TMemo
              Left = 0
              Top = 0
              Width = 1008
              Height = 94
              Align = alClient
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Courier New'
              Font.Style = []
              ParentFont = False
              ScrollBars = ssBoth
              TabOrder = 0
            end
          end
        end
      end
    end
    object PageControl2: TPageControl
      Left = 0
      Top = 0
      Width = 1016
      Height = 225
      ActivePage = TabSheet4
      Align = alTop
      TabOrder = 1
      object TabSheet4: TTabSheet
        Caption = 'Configura'
        object PainelPesq: TPanel
          Left = 0
          Top = 0
          Width = 1008
          Height = 197
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label75: TLabel
            Left = 4
            Top = 4
            Width = 35
            Height = 13
            Caption = 'Cliente:'
          end
          object Label1: TLabel
            Left = 4
            Top = 44
            Width = 45
            Height = 13
            Caption = 'Duplicata'
          end
          object Label2: TLabel
            Left = 236
            Top = 44
            Width = 40
            Height = 13
            Caption = 'Sacado:'
          end
          object Label4: TLabel
            Left = 104
            Top = 44
            Width = 61
            Height = 13
            Caption = 'CPF sacado:'
          end
          object Label9: TLabel
            Left = 160
            Top = 84
            Width = 44
            Height = 13
            Caption = 'Coligado:'
          end
          object Label55: TLabel
            Left = 160
            Top = 132
            Width = 38
            Height = 13
            Caption = 'Meu ID:'
          end
          object Label18: TLabel
            Left = 296
            Top = 4
            Width = 45
            Height = 13
            Caption = 'Telefone:'
            FocusControl = DBEdit5
          end
          object Label13: TLabel
            Left = 442
            Top = 169
            Width = 66
            Height = 13
            Caption = 'Lote remessa:'
          end
          object EdCliente: TdmkEditCB
            Left = 4
            Top = 20
            Width = 37
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdClienteChange
            OnEnter = EdClienteEnter
            DBLookupComboBox = CBCliente
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBCliente: TdmkDBLookupComboBox
            Left = 44
            Top = 20
            Width = 249
            Height = 21
            KeyField = 'Codigo'
            ListField = 'NOMECLIENTE'
            ListSource = DsClientes
            TabOrder = 1
            dmkEditCB = EdCliente
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdDuplicata: TdmkEdit
            Left = 4
            Top = 60
            Width = 97
            Height = 21
            Alignment = taCenter
            CharCase = ecUpperCase
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdDuplicataChange
          end
          object EdEmitente: TdmkEdit
            Left = 236
            Top = 60
            Width = 233
            Height = 21
            TabOrder = 5
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdEmitenteChange
          end
          object RGMascara: TRadioGroup
            Left = 6
            Top = 84
            Width = 87
            Height = 68
            Caption = ' M'#225'scara: '
            ItemIndex = 0
            Items.Strings = (
              'Pref. e suf.'
              'Prefixo'
              'Sufixo')
            TabOrder = 18
            OnClick = RGMascaraClick
          end
          object EdCPF: TdmkEdit
            Left = 104
            Top = 60
            Width = 129
            Height = 21
            TabOrder = 4
            FormatType = dmktfString
            MskType = fmtCPFJ
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnExit = EdCPFExit
          end
          object TPEIni: TDateTimePicker
            Left = 476
            Top = 20
            Width = 88
            Height = 21
            Date = 38675.000000000000000000
            Time = 0.714976851901155900
            TabOrder = 7
            OnChange = TPEIniChange
          end
          object TPEFim: TDateTimePicker
            Left = 476
            Top = 60
            Width = 88
            Height = 21
            Date = 38675.000000000000000000
            Time = 0.714976851901155900
            TabOrder = 9
            OnChange = TPEFimChange
          end
          object CkEIni: TCheckBox
            Left = 476
            Top = 3
            Width = 97
            Height = 17
            Caption = 'Emiss'#227'o inicial:'
            TabOrder = 6
            OnClick = CkEIniClick
          end
          object CkEFim: TCheckBox
            Left = 476
            Top = 43
            Width = 97
            Height = 17
            Caption = 'Emiss'#227'o final:'
            TabOrder = 8
            OnClick = CkEFimClick
          end
          object CkVIni: TCheckBox
            Left = 568
            Top = 3
            Width = 97
            Height = 17
            Caption = 'Vencto inicial:'
            TabOrder = 10
            OnClick = CkVIniClick
          end
          object TPVIni: TDateTimePicker
            Left = 568
            Top = 20
            Width = 88
            Height = 21
            Date = 38675.000000000000000000
            Time = 0.714976851901155900
            TabOrder = 11
            OnChange = TPVIniChange
          end
          object CkVFim: TCheckBox
            Left = 568
            Top = 43
            Width = 83
            Height = 17
            Caption = 'Vencto final:'
            TabOrder = 12
            OnClick = CkVFimClick
          end
          object TPVFim: TDateTimePicker
            Left = 568
            Top = 60
            Width = 88
            Height = 21
            Date = 38675.000000000000000000
            Time = 0.714976851901155900
            TabOrder = 13
            OnChange = TPVFimChange
          end
          object EdColigado: TdmkEditCB
            Left = 160
            Top = 100
            Width = 49
            Height = 21
            Alignment = taRightJustify
            TabOrder = 20
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdColigadoChange
            DBLookupComboBox = CBColigado
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBColigado: TdmkDBLookupComboBox
            Left = 212
            Top = 100
            Width = 257
            Height = 21
            KeyField = 'Codigo'
            ListField = 'NOMECLIENTE'
            ListSource = DsColigados
            TabOrder = 21
            dmkEditCB = EdColigado
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object GroupBox1: TGroupBox
            Left = 754
            Top = -1
            Width = 249
            Height = 152
            Caption = ' Totais: '
            Enabled = False
            TabOrder = 25
            object Label3: TLabel
              Left = 4
              Top = 16
              Width = 50
              Height = 13
              Caption = 'Valor total:'
              FocusControl = DBEdit1
            end
            object Label5: TLabel
              Left = 84
              Top = 16
              Width = 57
              Height = 13
              Caption = 'Aberto total:'
              FocusControl = DBEdit2
            end
            object Label7: TLabel
              Left = 164
              Top = 16
              Width = 43
              Height = 13
              Caption = 'Principal:'
              FocusControl = DBEdit4
            end
            object Label6: TLabel
              Left = 4
              Top = 56
              Width = 76
              Height = 13
              Caption = 'd. venc (m'#233'dia):'
              FocusControl = DBEdit3
            end
            object Label8: TLabel
              Left = 84
              Top = 56
              Width = 52
              Height = 13
              Caption = 'Atualizado:'
            end
            object Label10: TLabel
              Left = 164
              Top = 56
              Width = 53
              Height = 13
              Caption = 'Duplicatas:'
            end
            object Label11: TLabel
              Left = 4
              Top = 96
              Width = 47
              Height = 13
              Caption = 'Val.Ocorr:'
            end
            object Label12: TLabel
              Left = 84
              Top = 96
              Width = 66
              Height = 13
              Caption = 'Ocorr.Atualiz.:'
            end
            object Label17: TLabel
              Left = 164
              Top = 96
              Width = 78
              Height = 13
              Caption = 'Total atualizado:'
            end
            object DBEdit1: TDBEdit
              Left = 4
              Top = 32
              Width = 78
              Height = 21
              TabStop = False
              DataField = 'Valor'
              DataSource = DsSoma
              TabOrder = 0
            end
            object DBEdit2: TDBEdit
              Left = 84
              Top = 32
              Width = 78
              Height = 21
              TabStop = False
              DataField = 'SALDO_DESATUALIZADO'
              DataSource = DsSoma
              TabOrder = 1
            end
            object DBEdit4: TDBEdit
              Left = 60
              Top = -4
              Width = 78
              Height = 21
              TabStop = False
              DataField = 'SALDO_DESATUALIZADO'
              DataSource = DsVcto
              TabOrder = 2
              Visible = False
            end
            object DBEdit3: TDBEdit
              Left = 4
              Top = 72
              Width = 78
              Height = 21
              TabStop = False
              DataField = 'PRAZO_MEDIO'
              DataSource = DsVcto
              TabOrder = 3
            end
            object EdAtualizado: TdmkEdit
              Left = 84
              Top = 72
              Width = 78
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdITENS: TdmkEdit
              Left = 164
              Top = 72
              Width = 78
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              TabOrder = 5
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdOcorrencias: TdmkEdit
              Left = 4
              Top = 112
              Width = 78
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              TabOrder = 6
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdOcorAtualiz: TdmkEdit
              Left = 84
              Top = 112
              Width = 78
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              TabOrder = 7
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdTotalAtualiz: TdmkEdit
              Left = 164
              Top = 112
              Width = 78
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              TabOrder = 8
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object DBEdit6: TDBEdit
              Left = 164
              Top = 32
              Width = 78
              Height = 21
              TabStop = False
              DataField = 'Principal'
              DataSource = DsSoma
              TabOrder = 9
            end
          end
          object CkRepassado: TdmkCheckGroup
            Left = 96
            Top = 84
            Width = 61
            Height = 68
            Caption = ' Repas.: '
            ItemIndex = 0
            Items.Strings = (
              'Sim'
              'N'#227'o')
            TabOrder = 19
            UpdType = utYes
            Value = 1
            OldValor = 0
          end
          object CkStatus: TdmkCheckGroup
            Left = 476
            Top = 84
            Width = 129
            Height = 68
            Caption = ' Status: '
            Columns = 2
            ItemIndex = 1
            Items.Strings = (
              'Vencido'
              'Aberto'
              'Quitado'
              'Baixado'
              'Moroso')
            TabOrder = 22
            UpdType = utYes
            Value = 2
            OldValor = 0
          end
          object CkAtualiza: TCheckBox
            Left = 380
            Top = 132
            Width = 93
            Height = 17
            Caption = 'Atualiza saldos.'
            TabOrder = 24
          end
          object EdControle: TdmkEdit
            Left = 200
            Top = 128
            Width = 97
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInt64
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object CkQIni: TCheckBox
            Left = 660
            Top = 3
            Width = 93
            Height = 17
            Caption = 'Quita'#231#227'o inicial:'
            TabOrder = 14
            OnClick = CkVIniClick
          end
          object TPQIni: TDateTimePicker
            Left = 660
            Top = 20
            Width = 88
            Height = 21
            Date = 38675.000000000000000000
            Time = 0.714976851901155900
            TabOrder = 15
            OnChange = TPVIniChange
          end
          object CkQFim: TCheckBox
            Left = 660
            Top = 43
            Width = 91
            Height = 17
            Caption = 'Quita'#231#227'o final:'
            TabOrder = 16
            OnClick = CkVFimClick
          end
          object TPQFim: TDateTimePicker
            Left = 660
            Top = 60
            Width = 88
            Height = 21
            Date = 38675.000000000000000000
            Time = 0.714976851901155900
            TabOrder = 17
            OnChange = TPVFimChange
          end
          object DBEdit5: TDBEdit
            Left = 296
            Top = 20
            Width = 113
            Height = 21
            DataField = 'Te1_TXT'
            DataSource = DsClientes
            TabOrder = 26
          end
          object CkOcorrencias: TCheckBox
            Left = 300
            Top = 132
            Width = 77
            Height = 17
            Caption = 'Ocorr'#234'ncias.'
            Checked = True
            State = cbChecked
            TabOrder = 23
          end
          object RGFontes: TRadioGroup
            Left = 608
            Top = 84
            Width = 141
            Height = 68
            Caption = ' Fontes dos dados: '
            ItemIndex = 0
            Items.Strings = (
              'Apenas ativo'
              'Ativo + Env'#233's'
              'Ativo + Env'#233's + Morto')
            TabOrder = 27
          end
          object RGJuridico: TRadioGroup
            Left = 6
            Top = 151
            Width = 430
            Height = 42
            Caption = ' Status jur'#237'dico: '
            Columns = 3
            Items.Strings = (
              'Sem restri'#231#245'es'
              'Com a'#231#227'o de cobran'#231'a'
              'Ambos')
            TabOrder = 28
            OnClick = RGJuridicoClick
          end
          object EdCNABLot: TdmkEdit
            Left = 513
            Top = 166
            Width = 92
            Height = 21
            Alignment = taRightJustify
            TabOrder = 29
            FormatType = dmktfInt64
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object RGRemessa: TRadioGroup
            Left = 608
            Top = 151
            Width = 395
            Height = 42
            Caption = ' Remessa:'
            Columns = 3
            Items.Strings = (
              'Com remessa'
              'Sem remessa'
              'Ambos')
            TabOrder = 30
            OnClick = RGJuridicoClick
          end
        end
      end
      object TabSheet5: TTabSheet
        Caption = 'Imprime'
        ImageIndex = 1
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 1008
          Height = 156
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object RGOrdem1: TRadioGroup
            Left = 0
            Top = 0
            Width = 224
            Height = 156
            Align = alLeft
            Caption = ' Ordem 1: '
            Columns = 2
            ItemIndex = 0
            Items.Strings = (
              'Cliente'
              'Dia Resgate'
              'M'#234's Resgate'
              'Emitente'
              'CPF/CNPJ'
              'Dia Quita'#231#227'o'
              'M'#234's Quita'#231#227'o')
            TabOrder = 0
            OnClick = RGOrdem1Click
          end
          object RGOrdem2: TRadioGroup
            Left = 224
            Top = 0
            Width = 224
            Height = 156
            Align = alLeft
            Caption = ' Ordem 2: '
            Columns = 2
            ItemIndex = 1
            Items.Strings = (
              'Cliente'
              'Dia Resgate'
              'M'#234's Resgate'
              'Emitente'
              'CPF/CNPJ'
              'Dia Quita'#231#227'o'
              'M'#234's Quita'#231#227'o')
            TabOrder = 1
            OnClick = RGOrdem2Click
          end
          object RGOrdem3: TRadioGroup
            Left = 448
            Top = 0
            Width = 224
            Height = 156
            Align = alLeft
            Caption = ' Ordem 3: '
            Columns = 2
            ItemIndex = 2
            Items.Strings = (
              'Cliente'
              'Dia Resgate'
              'M'#234's Resgate'
              'Emitente'
              'CPF/CNPJ'
              'Dia Quita'#231#227'o'
              'M'#234's Quita'#231#227'o')
            TabOrder = 2
            OnClick = RGOrdem3Click
          end
          object RGOrdem4: TRadioGroup
            Left = 672
            Top = 0
            Width = 224
            Height = 156
            Align = alLeft
            Caption = ' Ordem 4: '
            Columns = 2
            ItemIndex = 3
            Items.Strings = (
              'Cliente'
              'Dia Resgate'
              'M'#234's Resgate'
              'Emitente'
              'CPF/CNPJ'
              'Dia Quita'#231#227'o'
              'M'#234's Quita'#231#227'o')
            TabOrder = 3
            OnClick = RGOrdem4Click
          end
          object BtImprime: TBitBtn
            Tag = 5
            Left = 908
            Top = 54
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Imprime'
            Enabled = False
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 4
            OnClick = BtImprimeClick
          end
        end
        object RGAgrupa: TRadioGroup
          Left = 0
          Top = 156
          Width = 1008
          Height = 41
          Align = alBottom
          Caption = ' Agrupamentos: '
          Columns = 4
          ItemIndex = 0
          Items.Strings = (
            '0'
            '1'
            '2'
            '3')
          TabOrder = 1
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1016
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 968
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 7
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbBordero: TBitBtn
        Tag = 116
        Left = 46
        Top = 7
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbBorderoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 7
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        Visible = False
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 7
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        Visible = False
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 7
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        Visible = False
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 752
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 277
        Height = 32
        Caption = 'Controle de Duplicatas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 277
        Height = 32
        Caption = 'Controle de Duplicatas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 277
        Height = 32
        Caption = 'Controle de Duplicatas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 570
    Width = 1016
    Height = 61
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel9: TPanel
      Left = 2
      Top = 15
      Width = 1012
      Height = 44
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 492
        Height = 16
        Caption = 
          'Obs.: Duplo clique na grade de pesquisa abre janela com dados ed' +
          'it'#225'veis do sacado.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 492
        Height = 16
        Caption = 
          'Obs.: Duplo clique na grade de pesquisa abre janela com dados ed' +
          'it'#225'veis do sacado.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object ProgressBar1: TProgressBar
        Left = 12
        Top = 21
        Width = 985
        Height = 17
        TabOrder = 0
        Visible = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 631
    Width = 1016
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 870
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Saida'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel10: TPanel
      Left = 2
      Top = 15
      Width = 868
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtCuidado: TBitBtn
        Tag = 151
        Left = 8
        Top = 3
        Width = 90
        Height = 40
        Caption = '&Hist'#243'rico'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtCuidadoClick
      end
      object BtPagtoDuvida: TBitBtn
        Tag = 152
        Left = 100
        Top = 3
        Width = 90
        Height = 40
        Caption = '&Pagto'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtPagtoDuvidaClick
      end
      object BtZAZ: TBitBtn
        Tag = 153
        Left = 192
        Top = 3
        Width = 90
        Height = 40
        Caption = 'H&+P'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtZAZClick
      end
      object BtStatusManual: TBitBtn
        Tag = 154
        Left = 284
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Status'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = BtStatusManualClick
      end
      object BtDuplicataOcorr: TBitBtn
        Tag = 148
        Left = 378
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Ocorr'#234'ncia'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        Visible = False
        OnClick = BtDuplicataOcorrClick
      end
      object BtQuitacao: TBitBtn
        Tag = 172
        Left = 472
        Top = 2
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Quita'#231#227'o'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
        OnClick = BtQuitacaoClick
      end
      object BtHistCNAB: TBitBtn
        Left = 566
        Top = 2
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Hist. CNAB'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 6
        Visible = False
        OnClick = BtHistCNABClick
      end
      object BtReabre: TBitBtn
        Tag = 18
        Left = 660
        Top = 2
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Reabre'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 7
        OnClick = BtReabreClick
      end
      object BtRefresh: TBitBtn
        Tag = 10086
        Left = 755
        Top = 2
        Width = 90
        Height = 40
        Caption = 'Re&fresh'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 8
        OnClick = BtRefreshClick
      end
    end
  end
  object QrClientes: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrClientesCalcFields
    SQL.Strings = (
      'SELECT '
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMECLIENTE, '
      'CASE WHEN Tipo=0 THEN ETe1'
      'ELSE PTe1 END Te1, Codigo'
      'FROM entidades'
      'WHERE Cliente1='#39'V'#39
      'ORDER BY NOMECLIENTE')
    Left = 176
    Top = 64
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrClientesTe1: TWideStringField
      FieldName = 'Te1'
    end
    object QrClientesTe1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Te1_TXT'
      Size = 40
      Calculated = True
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 204
    Top = 64
  end
  object QrPesq: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPesqAfterOpen
    BeforeClose = QrPesqBeforeClose
    AfterClose = QrPesqAfterClose
    AfterScroll = QrPesqAfterScroll
    OnCalcFields = QrPesqCalcFields
    SQL.Strings = (
      'SELECT MONTH(li.DDeposito)+YEAR(li.DDeposito)*100+0.00 PERIODO,'
      'MONTH(li.Data3)+YEAR(li.Data3)*100+0.00 PERIODO3, '
      'lo.NF, od.Nome STATUS, li.FatParcela, li.Duplicata, li.Data, '
      'li.DCompra, li.Credito, li.DDeposito, li.Emitente, li.CNPJCPF,'
      'lo.Cliente, li.Quitado, li.TotalJr, li.TotalDs, li.TotalPg,'
      'li.Repassado, IF(cl.Tipo=0, cl.RazaoSocial,  '
      'cl.Nome) NOMECLIENTE, li.Vencimento, li.Data3,'
      'li.TxaCompra, li.Devolucao, li.ProrrVz, li.ProrrDd, li.ValQuit, '
      'li.Banco, li.Agencia, lo.Tipo + 10 LOCALT, lo.Codigo, '
      
        '(li.Credito + li.TotalJr - li.TotalDs - li.TotalPg) SALDO_DESATU' +
        'ALIZ, '
      'sa.Tel1'
      'FROM lct0001a li'
      'LEFT JOIN lot0001a lo ON lo.Codigo=li.FatNum'
      'LEFT JOIN entidades cl ON cl.Codigo=lo.Cliente'
      'LEFT JOIN ocordupl  od ON od.Codigo=li.Devolucao'
      'LEFT JOIN repasits  ri ON ri.Origem=li.FatParcela'
      'LEFT JOIN repas     re ON re.Codigo=ri.Codigo'
      'LEFT JOIN sacados sa ON sa.CNPJ = li.CNPJCPF'
      'WHERE li.FatID=301 AND lo.Tipo=1'
      'AND lo.Cliente=267'
      ''
      ''
      ''
      'AND li.Quitado in (-1,0,1)'
      'ORDER BY SALDO_DESATUALIZ '
      '')
    Left = 612
    Top = 272
    object QrPesqDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Required = True
      Size = 12
    end
    object QrPesqDCompra: TDateField
      FieldName = 'DCompra'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPesqDDeposito: TDateField
      FieldName = 'DDeposito'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPesqEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrPesqCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrPesqNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrPesqSTATUS: TWideStringField
      FieldName = 'STATUS'
      Size = 50
    end
    object QrPesqQuitado: TIntegerField
      FieldName = 'Quitado'
      Required = True
    end
    object QrPesqTotalJr: TFloatField
      FieldName = 'TotalJr'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqTotalDs: TFloatField
      FieldName = 'TotalDs'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqTotalPg: TFloatField
      FieldName = 'TotalPg'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqSALDO_DESATUALIZ: TFloatField
      FieldName = 'SALDO_DESATUALIZ'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPesqSALDO_ATUALIZADO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO_ATUALIZADO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrPesqNOMESTATUS: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESTATUS'
      Size = 30
      Calculated = True
    end
    object QrPesqDDCALCJURO: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'DDCALCJURO'
      Calculated = True
    end
    object QrPesqData3: TDateField
      FieldName = 'Data3'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPesqCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 40
      Calculated = True
    end
    object QrPesqTAXA_COMPRA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TAXA_COMPRA'
      Calculated = True
    end
    object QrPesqTxaCompra: TFloatField
      FieldName = 'TxaCompra'
      Required = True
    end
    object QrPesqDevolucao: TIntegerField
      FieldName = 'Devolucao'
      Required = True
    end
    object QrPesqProrrVz: TIntegerField
      FieldName = 'ProrrVz'
      Required = True
    end
    object QrPesqProrrDd: TIntegerField
      FieldName = 'ProrrDd'
      Required = True
    end
    object QrPesqRepassado: TSmallintField
      FieldName = 'Repassado'
      Required = True
    end
    object QrPesqValQuit: TFloatField
      FieldName = 'ValQuit'
      Required = True
    end
    object QrPesqNF: TIntegerField
      FieldName = 'NF'
      DisplayFormat = '000000;-000000; '
    end
    object QrPesqBanco: TIntegerField
      FieldName = 'Banco'
      Required = True
    end
    object QrPesqPERIODO: TFloatField
      FieldName = 'PERIODO'
    end
    object QrPesqOCORRENCIAS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'OCORRENCIAS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrPesqOCORATUALIZ: TFloatField
      FieldKind = fkCalculated
      FieldName = 'OCORATUALIZ'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrPesqTOTALATUALIZ: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TOTALATUALIZ'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrPesqSALDO_TEXTO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SALDO_TEXTO'
      Size = 30
      Calculated = True
    end
    object QrPesqLOCALT: TLargeintField
      FieldName = 'LOCALT'
    end
    object QrPesqNOMELOCAL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMELOCAL'
      Size = 5
      Calculated = True
    end
    object QrPesqPERIODO3: TFloatField
      FieldName = 'PERIODO3'
    end
    object QrPesqDATA3_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATA3_TXT'
      Size = 10
      Calculated = True
    end
    object QrPesqFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrPesqData: TDateField
      FieldName = 'Data'
    end
    object QrPesqCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrPesqCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrPesqVencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrPesqAgencia: TIntegerField
      FieldName = 'Agencia'
      DisplayFormat = '0000'
    end
    object QrPesqCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPesqLctCtrl: TIntegerField
      FieldName = 'LctCtrl'
    end
    object QrPesqLctSub: TIntegerField
      FieldName = 'LctSub'
    end
    object QrPesqCartDep: TIntegerField
      FieldName = 'CartDep'
    end
    object QrPesqControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPesqTel1: TWideStringField
      FieldName = 'Tel1'
    end
    object QrPesqJuridico: TSmallintField
      FieldName = 'Juridico'
      MaxValue = 1
    end
    object QrPesqCNAB_Lot: TIntegerField
      FieldName = 'CNAB_Lot'
    end
  end
  object DsPesq: TDataSource
    DataSet = QrPesq
    Left = 640
    Top = 272
  end
  object QrSumPg: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(ad.Juros) Juros, SUM(ad.Desco) Desco, '
      'SUM(ad.Pago) Pago, li.Valor, MAX(ad.Data) MaxData'
      'FROM adup pgs ad'
      'LEFT JOIN lot esits li ON li.Controle=ad.Lot esIts'
      'WHERE ad.Lot esIts=:P0'
      'GROUP BY ad.Lot esIts')
    Left = 552
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumPgJuros: TFloatField
      FieldName = 'Juros'
    end
    object QrSumPgPago: TFloatField
      FieldName = 'Pago'
    end
    object QrSumPgDesco: TFloatField
      FieldName = 'Desco'
    end
    object QrSumPgValor: TFloatField
      FieldName = 'Valor'
    end
    object QrSumPgMaxData: TDateField
      FieldName = 'MaxData'
    end
  end
  object QrLocPg: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM adup pgs'
      'WHERE Data=('
      'SELECT Max(Data) FROM adup pgs'
      'WHERE Lot esIts=:P0)'
      'ORDER BY Controle DESC')
    Left = 584
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocPgData: TDateField
      FieldName = 'Data'
    end
    object QrLocPgFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
  end
  object QrADupIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT od.Nome NOMESTATUS, ad.* '
      'FROM adupits ad'
      'LEFT JOIN ocordupl od ON od.Codigo=ad.Alinea'
      'WHERE ad.Lot esIts=:P0'
      'ORDER BY ad.DataA')
    Left = 612
    Top = 300
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrADupItsAlinea: TIntegerField
      FieldName = 'Alinea'
    end
    object QrADupItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrADupItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrADupItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrADupItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrADupItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrADupItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrADupItsDataA: TDateField
      FieldName = 'DataA'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrADupItsNOMESTATUS: TWideStringField
      FieldName = 'NOMESTATUS'
      Size = 50
    end
  end
  object DsADupIts: TDataSource
    DataSet = QrADupIts
    Left = 640
    Top = 300
  end
  object DsDupPgs: TDataSource
    DataSet = QrDupPgs
    Left = 640
    Top = 328
  end
  object QrDupPgs: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      '/*'
      'SELECT adp.*, lot.Lote'
      'FROM adup pgs adp'
      'LEFT JOIN lot es lot ON lot.Codigo=adp.LotePg'
      'WHERE adp.Lot esIts=:P0'
      'ORDER BY adp.Data, adp.Controle'
      '*/'
      'SELECT lot.Lote, adp.FatParcela, adp.FatNum,'
      'adp.Data, adp.MoraVal, adp.Desco, adp.Credito,'
      'adp.Controle, adp.Ocorreu'
      'FROM lct0001a adp'
      'LEFT JOIN lot0001a lot ON lot.Codigo=adp.FatNum'
      'WHERE adp.FatParcRef=:P0'
      'ORDER BY adp.Data, adp.FatParcela'
      '')
    Left = 612
    Top = 328
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrDupPgsLote: TIntegerField
      FieldName = 'Lote'
    end
    object QrDupPgsFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrDupPgsData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDupPgsCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrDupPgsFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrDupPgsMoraVal: TFloatField
      FieldName = 'MoraVal'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrDupPgsDescoVal: TFloatField
      FieldName = 'DescoVal'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrDupPgsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrDupPgsOcorreu: TIntegerField
      FieldName = 'Ocorreu'
    end
    object QrDupPgsFatParcRef: TIntegerField
      FieldName = 'FatParcRef'
    end
    object QrDupPgsFatGrupo: TIntegerField
      FieldName = 'FatGrupo'
    end
    object QrDupPgsPago: TFloatField
      FieldName = 'Pago'
    end
  end
  object QrPos: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ad.Alinea '
      'FROM adupits ad'
      'WHERE ad.Lot esIts=:P0'
      'ORDER BY ad.DataA DESC, Controle DESC')
    Left = 268
    Top = 308
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPosAlinea: TIntegerField
      FieldName = 'Alinea'
      Required = True
    end
  end
  object PMStatus: TPopupMenu
    OnPopup = PMStatusPopup
    Left = 36
    Top = 611
    object Inclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = Inclui1Click
    end
    object Exclui1: TMenuItem
      Caption = '&Exclui'
      OnClick = Exclui1Click
    end
  end
  object PMPagto: TPopupMenu
    OnPopup = PMPagtoPopup
    Left = 136
    Top = 610
    object Inclui2: TMenuItem
      Caption = '&Inclui'
      OnClick = Inclui2Click
    end
    object Exclui2: TMenuItem
      Caption = '&Exclui'
      OnClick = Exclui2Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Recibo1: TMenuItem
      Caption = '&Recibo'
      OnClick = Recibo1Click
    end
  end
  object PMStatusManual: TPopupMenu
    OnPopup = PMStatusManualPopup
    Left = 312
    Top = 609
    object Incluiprorrogao1: TMenuItem
      Caption = '&Inclui prorroga'#231#227'o'
      OnClick = Incluiprorrogao1Click
    end
    object ExcluiProrrogao1: TMenuItem
      Caption = '&Exclui Prorroga'#231#227'o'
      OnClick = ExcluiProrrogao1Click
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object N0Statusautomtico1: TMenuItem
      Caption = '&0. For'#231'a status Autom'#225'tico'
      OnClick = N0Statusautomtico1Click
    end
    object N1Forastatusprorrogado1: TMenuItem
      Caption = '&1. For'#231'a status Prorrogado'
      OnClick = N1Forastatusprorrogado1Click
    end
    object N2ForastatusBaixado1: TMenuItem
      Caption = '&2. For'#231'a status Baixado'
      OnClick = N2ForastatusBaixado1Click
    end
    object N3ForastatusMoroso1: TMenuItem
      Caption = '&3. For'#231'a status Moroso'
      OnClick = N3ForastatusMoroso1Click
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object EditarstatusJurdicoSelecionados1: TMenuItem
      Caption = 'Editar status &Jur'#237'dico (Selecionados)'
      OnClick = EditarstatusJurdicoSelecionados1Click
    end
  end
  object PMOcorreu: TPopupMenu
    OnPopup = PMOcorreuPopup
    Left = 408
    Top = 608
    object Incluiocorrncia1: TMenuItem
      Caption = '&Inclui ocorr'#234'ncia'
      OnClick = Incluiocorrncia1Click
    end
    object Alteraocorrncia1: TMenuItem
      Caption = '&Altera ocorr'#234'ncia'
      OnClick = Alteraocorrncia1Click
    end
    object Excluiocorrncia1: TMenuItem
      Caption = '&Exclui ocorr'#234'ncia'
      OnClick = Excluiocorrncia1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object IncluiPagamento1: TMenuItem
      Caption = 'Inclui &Pagamento'
      OnClick = IncluiPagamento1Click
    end
    object Excluipagamento1: TMenuItem
      Caption = 'E&xclui pagamento'
      OnClick = Excluipagamento1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Recibodepagamento1: TMenuItem
      Caption = '&Recibo de pagamento'
      OnClick = Recibodepagamento1Click
    end
  end
  object QrOcorreu: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrOcorreuBeforeClose
    AfterScroll = QrOcorreuAfterScroll
    OnCalcFields = QrOcorreuCalcFields
    SQL.Strings = (
      '/*'
      'SELECT  ob.Nome NOMEOCORRENCIA, oc.*'
      'FROM ocorreu oc'
      'LEFT JOIN ocorbank ob ON ob.Codigo = oc.Ocorrencia'
      'WHERE oc.Lot esIts=:P0'
      '*/'
      'SELECT ob.Nome NOMEOCORRENCIA, ob.PlaGen,'
      'oc.Lot esIts LOIS, oc.*'
      'FROM ocorreu oc'
      'LEFT JOIN ocorbank ob ON ob.Codigo = oc.Ocorrencia'
      'WHERE oc.Lot esIts=:P0'
      '')
    Left = 408
    Top = 272
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOcorreuNOMEOCORRENCIA: TWideStringField
      FieldName = 'NOMEOCORRENCIA'
      Origin = 'ocorbank.Nome'
      Size = 50
    end
    object QrOcorreuCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'ocorreu.Codigo'
      Required = True
    end
    object QrOcorreuDataO: TDateField
      FieldName = 'DataO'
      Origin = 'ocorreu.DataO'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrOcorreuOcorrencia: TIntegerField
      FieldName = 'Ocorrencia'
      Origin = 'ocorreu.Ocorrencia'
      Required = True
    end
    object QrOcorreuValor: TFloatField
      FieldName = 'Valor'
      Origin = 'ocorreu.Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrOcorreuLoteQuit: TIntegerField
      FieldName = 'LoteQuit'
      Origin = 'ocorreu.LoteQuit'
      Required = True
    end
    object QrOcorreuLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'ocorreu.Lk'
    end
    object QrOcorreuDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'ocorreu.DataCad'
    end
    object QrOcorreuDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'ocorreu.DataAlt'
    end
    object QrOcorreuUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'ocorreu.UserCad'
    end
    object QrOcorreuUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'ocorreu.UserAlt'
    end
    object QrOcorreuTaxaP: TFloatField
      FieldName = 'TaxaP'
      Origin = 'ocorreu.TaxaP'
      Required = True
    end
    object QrOcorreuTaxaV: TFloatField
      FieldName = 'TaxaV'
      Origin = 'ocorreu.TaxaV'
      Required = True
    end
    object QrOcorreuPago: TFloatField
      FieldName = 'Pago'
      Origin = 'ocorreu.Pago'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrOcorreuDataP: TDateField
      FieldName = 'DataP'
      Origin = 'ocorreu.DataP'
      Required = True
    end
    object QrOcorreuTaxaB: TFloatField
      FieldName = 'TaxaB'
      Origin = 'ocorreu.TaxaB'
      Required = True
    end
    object QrOcorreuATUALIZADO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATUALIZADO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrOcorreuData3: TDateField
      FieldName = 'Data3'
      Origin = 'ocorreu.Data3'
      Required = True
    end
    object QrOcorreuStatus: TSmallintField
      FieldName = 'Status'
      Origin = 'ocorreu.Status'
      Required = True
    end
    object QrOcorreuCliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'ocorreu.Cliente'
      Required = True
    end
    object QrOcorreuSALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrOcorreuATZ_TEXTO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ATZ_TEXTO'
      Size = 30
      Calculated = True
    end
    object QrOcorreuSEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
    object QrOcorreuPlaGen: TIntegerField
      FieldName = 'PlaGen'
      Origin = 'ocorbank.PlaGen'
    end
    object QrOcorreuLOIS: TIntegerField
      FieldName = 'LOIS'
    end
  end
  object DsOcorreu: TDataSource
    DataSet = QrOcorreu
    Left = 436
    Top = 272
  end
  object QrLotPrr: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrLotPrrCalcFields
    SQL.Strings = (
      'SELECT * FROM lot esprr'
      'WHERE Codigo=:P0'
      'ORDER BY Controle')
    Left = 612
    Top = 356
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLotPrrCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLotPrrControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLotPrrData1: TDateField
      FieldName = 'Data1'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLotPrrData2: TDateField
      FieldName = 'Data2'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLotPrrData3: TDateField
      FieldName = 'Data3'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLotPrrOcorrencia: TIntegerField
      FieldName = 'Ocorrencia'
    end
    object QrLotPrrLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLotPrrDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLotPrrDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLotPrrUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrLotPrrUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrLotPrrDIAS_1_3: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'DIAS_1_3'
      Calculated = True
    end
    object QrLotPrrDIAS_2_3: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'DIAS_2_3'
      Calculated = True
    end
  end
  object DsLotPrr: TDataSource
    DataSet = QrLotPrr
    Left = 640
    Top = 356
  end
  object QrSoma: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Valor) Valor, SUM(Valor-TotalPg) Principal, '
      'SUM(Valor + TotalJr - TotalDs - TotalPg) SALDO_DESATUALIZADO'
      'FROM lot esits li'
      'LEFT JOIN lot es     lo ON lo.Codigo=li.Codigo'
      'LEFT JOIN entidades cl ON cl.Codigo=lo.Cliente'
      'LEFT JOIN ocordupl  od ON od.Codigo=li.Devolucao'
      'WHERE lo.Tipo=1 ')
    Left = 752
    Top = 356
    object QrSomaValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSomaSALDO_DESATUALIZADO: TFloatField
      FieldName = 'SALDO_DESATUALIZADO'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSomaPrincipal: TFloatField
      FieldName = 'Principal'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsSoma: TDataSource
    DataSet = QrSoma
    Left = 780
    Top = 356
  end
  object QrVcto: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrVctoCalcFields
    SQL.Strings = (
      'SELECT SUM(Valor) SOMA_VALOR, '
      'SUM(Valor + TotalJr - TotalDs - TotalPg) SALDO_DESATUALIZADO,'
      'SUM(CURDATE()-Vencto) SOMA_DIAS, '
      'SUM((CURDATE()-Vencto)*Valor) FATOR_VD'
      'FROM lot esits li'
      'LEFT JOIN lot es     lo ON lo.Codigo=li.Codigo'
      'LEFT JOIN entidades cl ON cl.Codigo=lo.Cliente'
      'LEFT JOIN ocordupl  od ON od.Codigo=li.Devolucao'
      'WHERE lo.Tipo=1 '
      'AND li.Vencto<CURDATE()')
    Left = 752
    Top = 328
    object QrVctoSOMA_VALOR: TFloatField
      FieldName = 'SOMA_VALOR'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVctoSALDO_DESATUALIZADO: TFloatField
      FieldName = 'SALDO_DESATUALIZADO'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVctoSOMA_DIAS: TFloatField
      FieldName = 'SOMA_DIAS'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVctoFATOR_VD: TFloatField
      FieldName = 'FATOR_VD'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVctoPRAZO_MEDIO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PRAZO_MEDIO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrVctoJURO_CLI: TFloatField
      FieldKind = fkCalculated
      FieldName = 'JURO_CLI'
      DisplayFormat = '#,###,##0.000000'
      Calculated = True
    end
  end
  object DsVcto: TDataSource
    DataSet = QrVcto
    Left = 780
    Top = 328
  end
  object QrSP: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MONTH(li.DDeposito)+YEAR(li.DDeposito)*100+0.00 PERIODO,'
      'MONTH(li.Data3)+YEAR(li.Data3)*100+0.00 PERIODO3, '
      'lo.NF, od.Nome STATUS, li.FatParcela, li.Duplicata, li.Data, '
      'li.DCompra, li.Credito, li.DDeposito, li.Emitente, li.CNPJCPF,'
      'lo.Cliente, li.Quitado, li.TotalJr, li.TotalDs, li.TotalPg,'
      'li.Repassado, IF(cl.Tipo=0, cl.RazaoSocial, '
      'cl.Nome) NOMECLIENTE, li.Vencimento, li.Data3,'
      'li.TxaCompra, li.Devolucao, li.ProrrVz, li.ProrrDd, li.ValQuit, '
      'li.Banco, li.Agencia, lo.Tipo + 10 LOCALT, lo.Codigo, '
      
        '(li.Credito + li.TotalJr - li.TotalDs - li.TotalPg) SALDO_DESATU' +
        'ALIZ'
      'FROM lct0001a li'
      'LEFT JOIN lot0001a lo ON lo.Codigo=li.FatNum'
      'LEFT JOIN entidades cl ON cl.Codigo=lo.Cliente'
      'LEFT JOIN ocordupl  od ON od.Codigo=li.Devolucao'
      'LEFT JOIN repasits  ri ON ri.Origem=li.FatParcela'
      'LEFT JOIN repas     re ON re.Codigo=ri.Codigo'
      'WHERE li.FatID=301'
      'AND lo.Tipo=1'
      '')
    Left = 696
    Top = 300
    object QrSPSALDO_DESATUALIZ: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO_DESATUALIZ'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrSPSALDO_ATUALIZADO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO_ATUALIZADO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrSPPERIODO: TFloatField
      FieldName = 'PERIODO'
    end
    object QrSPPERIODO3: TFloatField
      FieldName = 'PERIODO3'
    end
    object QrSPNF: TIntegerField
      FieldName = 'NF'
    end
    object QrSPSTATUS: TWideStringField
      FieldName = 'STATUS'
      Size = 100
    end
    object QrSPFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrSPDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 13
    end
    object QrSPData: TDateField
      FieldName = 'Data'
    end
    object QrSPDCompra: TDateField
      FieldName = 'DCompra'
    end
    object QrSPCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrSPDDeposito: TDateField
      FieldName = 'DDeposito'
    end
    object QrSPEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
    object QrSPCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrSPCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrSPQuitado: TIntegerField
      FieldName = 'Quitado'
    end
    object QrSPTotalJr: TFloatField
      FieldName = 'TotalJr'
    end
    object QrSPTotalDs: TFloatField
      FieldName = 'TotalDs'
    end
    object QrSPTotalPg: TFloatField
      FieldName = 'TotalPg'
    end
    object QrSPRepassado: TSmallintField
      FieldName = 'Repassado'
    end
    object QrSPNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrSPVencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrSPData3: TDateField
      FieldName = 'Data3'
    end
    object QrSPTxaCompra: TFloatField
      FieldName = 'TxaCompra'
    end
    object QrSPDevolucao: TIntegerField
      FieldName = 'Devolucao'
    end
    object QrSPProrrVz: TIntegerField
      FieldName = 'ProrrVz'
    end
    object QrSPProrrDd: TIntegerField
      FieldName = 'ProrrDd'
    end
    object QrSPValQuit: TFloatField
      FieldName = 'ValQuit'
    end
    object QrSPBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrSPAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrSPLOCALT: TLargeintField
      FieldName = 'LOCALT'
    end
    object QrSPCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object QrSumOc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      '/*'
      'SELECT SUM(Juros) Juros, SUM(Pago) Pago'
      'FROM ocor rpg'
      'WHERE Ocorreu=:P0'
      '*/'
      ''
      'SELECT SUM(MoraVal) Juros, SUM(Valor) Pago'
      'FROM lct0001a'
      'WHERE FatID=304'
      'AND Ocorreu=:P0')
    Left = 368
    Top = 372
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumOcJuros: TFloatField
      FieldName = 'Juros'
    end
    object QrSumOcPago: TFloatField
      FieldName = 'Pago'
    end
  end
  object QrLastOcor: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Data, Codigo'
      'FROM ocor rpg'
      'WHERE Ocorreu=:P0'
      'ORDER BY Data Desc, Codigo Desc')
    Left = 396
    Top = 372
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLastOcorData: TDateField
      FieldName = 'Data'
    end
    object QrLastOcorFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
  end
  object QrOcorP: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      '/*'
      'SELECT * FROM ocor rpg'
      'WHERE Ocorreu =:P0'
      '*/'
      ''
      'SELECT lct.Ocorreu, lct.Data, lct.FatParcela,'
      'lct.Controle, lct.FatNum, lct.MoraVal,'
      'lct.Credito - lct.Debito Pago'
      'FROM lct0001a lct'
      'WHERE lct.FatID=304 '
      'AND lct.Ocorreu>0'
      '')
    Left = 440
    Top = 376
    object QrOcorPOcorreu: TIntegerField
      FieldName = 'Ocorreu'
    end
    object QrOcorPData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrOcorPFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrOcorPPago: TFloatField
      FieldName = 'Pago'
      DisplayFormat = '#,###,##0.00'
    end
    object QrOcorPFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrOcorPMoraVal: TFloatField
      FieldName = 'MoraVal'
      DisplayFormat = '#,###,##0.00'
    end
    object QrOcorPControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object DsOcorP: TDataSource
    DataSet = QrOcorP
    Left = 468
    Top = 376
  end
  object PMQuitacao: TPopupMenu
    OnPopup = PMQuitacaoPopup
    Left = 500
    Top = 608
    object Quitadocumento1: TMenuItem
      Caption = '&Quita documento'
      OnClick = Quitadocumento1Click
    end
    object Imprimerecibodequitao1: TMenuItem
      Caption = '&Imprime recibo de quita'#231#227'o'
      OnClick = Imprimerecibodequitao1Click
    end
    object ImprimecartadeCancelamentodeProtesto1: TMenuItem
      Caption = 'Imprime carta de Cancelamento de &Protesto'
      OnClick = ImprimecartadeCancelamentodeProtesto1Click
    end
  end
  object QrPesq2: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrPesq2CalcFields
    SQL.Strings = (
      'SELECT MONTH(li.DDeposito)+YEAR(li.DDeposito)*100+0.00 PERIODO,'
      'MONTH(li.Data3)+YEAR(li.Data3)*100+0.00 PERIODO3,'
      ' lo.NF, od.Nome STATUS, li.Controle, li.Duplicata, li.Data, '
      
        'li.DCompra, li.Credito, li.DDeposito, li.Emitente, li.CNPJCPF,  ' +
        'li.Repassado,'
      
        'lo.Cliente, li.Quitado, li.TotalJr, li.TotalDs, li.TotalPg, li.V' +
        'alQuit,'
      'CASE WHEN cl.Tipo=0 THEN cl.RazaoSocial'
      'ELSE cl.Nome END NOMECLIENTE,'
      
        'li.Vencimento, li.Data3, li.TxaCompra, li.Devolucao, li.ProrrVz,' +
        ' li.ProrrDd,'
      'li.Banco, li.Agencia,  sa.Tel1'
      'FROM lct0001a li'
      'LEFT JOIN lot0001a lo ON lo.Codigo=li.FatNum'
      'LEFT JOIN entidades cl ON cl.Codigo=lo.Cliente'
      'LEFT JOIN ocordupl  od ON od.Codigo=li.Devolucao'
      'LEFT JOIN repasits  ri ON ri.Origem=li.Controle'
      'LEFT JOIN repas     re ON re.Codigo=ri.Codigo'
      'LEFT JOIN sacados sa ON sa.CNPJ = li.CNPJCPF'
      'WHERE lo.Tipo=1 ')
    Left = 188
    Top = 8
    object QrPesq2Controle: TIntegerField
      FieldName = 'Controle'
      Origin = 'lct0001a.Controle'
      Required = True
    end
    object QrPesq2Duplicata: TWideStringField
      FieldName = 'Duplicata'
      Origin = 'lct0001a.Duplicata'
      Required = True
      Size = 12
    end
    object QrPesq2Credito: TFloatField
      FieldName = 'Credito'
      Origin = 'lct0001a.Credito'
    end
    object QrPesq2DCompra: TDateField
      FieldName = 'DCompra'
      Origin = 'lct0001a.DCompra'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPesq2DDeposito: TDateField
      FieldName = 'DDeposito'
      Origin = 'lct0001a.DDeposito'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPesq2Emitente: TWideStringField
      FieldName = 'Emitente'
      Origin = 'lct0001a.Emitente'
      Size = 30
    end
    object QrPesq2CNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Origin = 'lct0001a.CNPJCPF'
      Size = 15
    end
    object QrPesq2Cliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'lot0001a.Cliente'
    end
    object QrPesq2NOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrPesq2STATUS: TWideStringField
      FieldName = 'STATUS'
      Origin = 'ocordupl.Nome'
      Size = 50
    end
    object QrPesq2Quitado: TIntegerField
      FieldName = 'Quitado'
      Origin = 'lct0001a.Quitado'
      Required = True
    end
    object QrPesq2TotalJr: TFloatField
      FieldName = 'TotalJr'
      Origin = 'lct0001a.TotalJr'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesq2TotalDs: TFloatField
      FieldName = 'TotalDs'
      Origin = 'lct0001a.TotalDs'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesq2TotalPg: TFloatField
      FieldName = 'TotalPg'
      Origin = 'lct0001a.TotalPg'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesq2SALDO_DESATUALIZ: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO_DESATUALIZ'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrPesq2SALDO_ATUALIZADO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO_ATUALIZADO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrPesq2NOMESTATUS: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESTATUS'
      Size = 30
      Calculated = True
    end
    object QrPesq2DDCALCJURO: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'DDCALCJURO'
      Calculated = True
    end
    object QrPesq2Data3: TDateField
      FieldName = 'Data3'
      Origin = 'lct0001a.Data3'
      Required = True
    end
    object QrPesq2CNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 40
      Calculated = True
    end
    object QrPesq2TAXA_COMPRA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TAXA_COMPRA'
      Calculated = True
    end
    object QrPesq2TxaCompra: TFloatField
      FieldName = 'TxaCompra'
      Origin = 'lct0001a.TxaCompra'
      Required = True
    end
    object QrPesq2Devolucao: TIntegerField
      FieldName = 'Devolucao'
      Origin = 'lct0001a.Devolucao'
      Required = True
    end
    object QrPesq2ProrrVz: TIntegerField
      FieldName = 'ProrrVz'
      Origin = 'lct0001a.ProrrVz'
      Required = True
    end
    object QrPesq2ProrrDd: TIntegerField
      FieldName = 'ProrrDd'
      Origin = 'lct0001a.ProrrDd'
      Required = True
    end
    object QrPesq2Repassado: TSmallintField
      FieldName = 'Repassado'
      Origin = 'lct0001a.Repassado'
      Required = True
    end
    object QrPesq2ValQuit: TFloatField
      FieldName = 'ValQuit'
      Origin = 'lct0001a.ValQuit'
      Required = True
    end
    object QrPesq2NF: TIntegerField
      FieldName = 'NF'
      Origin = 'lot0001a.NF'
      DisplayFormat = '000000;-000000; '
    end
    object QrPesq2Banco: TIntegerField
      FieldName = 'Banco'
      Origin = 'lct0001a.Banco'
      Required = True
    end
    object QrPesq2Agencia: TIntegerField
      FieldName = 'Agencia'
      Origin = 'lct0001a.Agencia'
      Required = True
    end
    object QrPesq2CONTA: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'CONTA'
      Calculated = True
    end
    object QrPesq2PERIODO: TFloatField
      FieldName = 'PERIODO'
    end
    object QrPesq2Valor_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Valor_TXT'
      Calculated = True
    end
    object QrPesq2DDeposito_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DDeposito_TXT'
      Calculated = True
    end
    object QrPesq2MES_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MES_TXT'
      Size = 30
      Calculated = True
    end
    object QrPesq2TOTALATUALIZ: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TOTALATUALIZ'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrPesq2OCORATUALIZ: TFloatField
      FieldKind = fkCalculated
      FieldName = 'OCORATUALIZ'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrPesq2OCORRENCIAS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'OCORRENCIAS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrPesq2ATZ_TEXTO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ATZ_TEXTO'
      Size = 30
      Calculated = True
    end
    object QrPesq2DATA3TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATA3TXT'
      Size = 12
      Calculated = True
    end
    object QrPesq2PERIODO3: TFloatField
      FieldName = 'PERIODO3'
    end
    object QrPesq2Tel1: TWideStringField
      FieldName = 'Tel1'
      Origin = 'sacados.Tel1'
    end
    object QrPesq2TEL1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEL1_TXT'
      Size = 30
      Calculated = True
    end
    object QrPesq2MEQ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MEQ_TXT'
      Size = 30
      Calculated = True
    end
    object QrPesq2Data: TDateField
      FieldName = 'Data'
      Origin = 'lct0001a.Data'
    end
    object QrPesq2Vencimento: TDateField
      FieldName = 'Vencimento'
      Origin = 'lct0001a.Vencimento'
    end
    object QrPesq2Juridico: TSmallintField
      FieldName = 'Juridico'
      MaxValue = 1
    end
    object QrPesq2CNAB_Lot: TIntegerField
      FieldName = 'CNAB_Lot'
    end
  end
  object QrOcu1: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrOcu1CalcFields
    SQL.Strings = (
      '/*'
      'SELECT  lo.Cliente CLIENTELOTE, ob.Nome NOMEOCORRENCIA, oc.*'
      'FROM ocorreu oc'
      'LEFT JOIN lot esits li ON oc.Lot esIts = li.Controle'
      'LEFT JOIN lot es lo ON lo.Codigo = li.Codigo'
      'LEFT JOIN ocorbank ob ON ob.Codigo = oc.Ocorrencia'
      'WHERE oc.Lot esIts=:P0'
      '*/'
      ''
      'SELECT  ob.Nome NOMEOCORRENCIA, oc.*'
      'FROM ocorreu oc'
      'LEFT JOIN ocorbank ob ON ob.Codigo = oc.Ocorrencia'
      'WHERE oc.Lot esIts=:P0'
      '')
    Left = 724
    Top = 300
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOcu1NOMEOCORRENCIA: TWideStringField
      FieldName = 'NOMEOCORRENCIA'
      Size = 50
    end
    object QrOcu1Codigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOcu1DataO: TDateField
      FieldName = 'DataO'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrOcu1Ocorrencia: TIntegerField
      FieldName = 'Ocorrencia'
      Required = True
    end
    object QrOcu1Valor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrOcu1LoteQuit: TIntegerField
      FieldName = 'LoteQuit'
      Required = True
    end
    object QrOcu1Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOcu1DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOcu1DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOcu1UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOcu1UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOcu1TaxaP: TFloatField
      FieldName = 'TaxaP'
      Required = True
    end
    object QrOcu1TaxaV: TFloatField
      FieldName = 'TaxaV'
      Required = True
    end
    object QrOcu1Pago: TFloatField
      FieldName = 'Pago'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrOcu1DataP: TDateField
      FieldName = 'DataP'
      Required = True
    end
    object QrOcu1TaxaB: TFloatField
      FieldName = 'TaxaB'
      Required = True
    end
    object QrOcu1ATUALIZADO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATUALIZADO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrOcu1Data3: TDateField
      FieldName = 'Data3'
      Required = True
    end
    object QrOcu1Status: TSmallintField
      FieldName = 'Status'
      Required = True
    end
    object QrOcu1Cliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrOcu1SALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
  end
  object QrOcu2: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrOcu2CalcFields
    SQL.Strings = (
      '/*'
      'SELECT  lo.Cliente CLIENTELOTE, ob.Nome NOMEOCORRENCIA, oc.*'
      'FROM ocorreu oc'
      'LEFT JOIN lot esits li ON oc.Lot esIts = li.Controle'
      'LEFT JOIN lot es lo ON lo.Codigo = li.Codigo'
      'LEFT JOIN ocorbank ob ON ob.Codigo = oc.Ocorrencia'
      'WHERE oc.Lot esIts=:P0'
      '*/'
      ''
      'SELECT  ob.Nome NOMEOCORRENCIA, oc.*'
      'FROM ocorreu oc'
      'LEFT JOIN ocorbank ob ON ob.Codigo = oc.Ocorrencia'
      'WHERE oc.Lot esIts=:P0'
      '')
    Left = 752
    Top = 300
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOcu2NOMEOCORRENCIA: TWideStringField
      FieldName = 'NOMEOCORRENCIA'
      Size = 50
    end
    object QrOcu2Codigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOcu2DataO: TDateField
      FieldName = 'DataO'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrOcu2Ocorrencia: TIntegerField
      FieldName = 'Ocorrencia'
      Required = True
    end
    object QrOcu2Valor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrOcu2LoteQuit: TIntegerField
      FieldName = 'LoteQuit'
      Required = True
    end
    object QrOcu2Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOcu2DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOcu2DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOcu2UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOcu2UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOcu2TaxaP: TFloatField
      FieldName = 'TaxaP'
      Required = True
    end
    object QrOcu2TaxaV: TFloatField
      FieldName = 'TaxaV'
      Required = True
    end
    object QrOcu2Pago: TFloatField
      FieldName = 'Pago'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrOcu2DataP: TDateField
      FieldName = 'DataP'
      Required = True
    end
    object QrOcu2TaxaB: TFloatField
      FieldName = 'TaxaB'
      Required = True
    end
    object QrOcu2ATUALIZADO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATUALIZADO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrOcu2Data3: TDateField
      FieldName = 'Data3'
      Required = True
    end
    object QrOcu2Status: TSmallintField
      FieldName = 'Status'
      Required = True
    end
    object QrOcu2Cliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrOcu2SALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
  end
  object QrColigados: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMECLIENTE, Codigo'
      'FROM entidades'
      'WHERE Fornece2='#39'V'#39
      'ORDER BY NOMECLIENTE')
    Left = 176
    Top = 96
    object QrColigadosNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrColigadosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsColigados: TDataSource
    DataSet = QrColigados
    Left = 204
    Top = 96
  end
  object QrCNAB240: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrCNAB240AfterOpen
    OnCalcFields = QrCNAB240CalcFields
    SQL.Strings = (
      'SELECT * FROM cnab240'
      'WHERE Controle=:P0'
      'ORDER BY DataOcor, CNAB240I')
    Left = 344
    Top = 472
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCNAB240CNAB240L: TIntegerField
      FieldName = 'CNAB240L'
    end
    object QrCNAB240CNAB240I: TIntegerField
      FieldName = 'CNAB240I'
    end
    object QrCNAB240Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCNAB240Banco: TIntegerField
      FieldName = 'Banco'
      DisplayFormat = '000'
    end
    object QrCNAB240Sequencia: TIntegerField
      FieldName = 'Sequencia'
    end
    object QrCNAB240Convenio: TWideStringField
      FieldName = 'Convenio'
    end
    object QrCNAB240ConfigBB: TIntegerField
      FieldName = 'ConfigBB'
    end
    object QrCNAB240Lote: TIntegerField
      FieldName = 'Lote'
    end
    object QrCNAB240Item: TIntegerField
      FieldName = 'Item'
    end
    object QrCNAB240DataOcor: TDateField
      FieldName = 'DataOcor'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCNAB240Envio: TSmallintField
      FieldName = 'Envio'
    end
    object QrCNAB240Movimento: TSmallintField
      FieldName = 'Movimento'
    end
    object QrCNAB240Custas: TFloatField
      FieldName = 'Custas'
      DisplayFormat = '###,###,##0.00'
    end
    object QrCNAB240ValPago: TFloatField
      FieldName = 'ValPago'
      DisplayFormat = '###,###,##0.00'
    end
    object QrCNAB240ValCred: TFloatField
      FieldName = 'ValCred'
      DisplayFormat = '###,###,##0.00'
    end
    object QrCNAB240Acao: TIntegerField
      FieldName = 'Acao'
    end
    object QrCNAB240Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCNAB240DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCNAB240DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCNAB240UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCNAB240UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCNAB240NOMEENVIO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEENVIO'
      Size = 10
      Calculated = True
    end
    object QrCNAB240NOMEMOVIMENTO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEMOVIMENTO'
      Size = 100
      Calculated = True
    end
    object QrCNAB240IRTCLB: TWideStringField
      FieldName = 'IRTCLB'
      Size = 10
    end
  end
  object DsCNAB240: TDataSource
    DataSet = QrCNAB240
    Left = 372
    Top = 472
  end
  object QrSacados: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT sac.Numero+0.000 Numero, sac.* '
      'FROM sacados sac'
      'WHERE CNPJ=:P0')
    Left = 56
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSacadosNumero: TFloatField
      FieldName = 'Numero'
    end
    object QrSacadosCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Required = True
      Size = 15
    end
    object QrSacadosIE: TWideStringField
      FieldName = 'IE'
      Required = True
      Size = 25
    end
    object QrSacadosNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrSacadosRua: TWideStringField
      FieldName = 'Rua'
      Size = 30
    end
    object QrSacadosCompl: TWideStringField
      FieldName = 'Compl'
      Size = 30
    end
    object QrSacadosBairro: TWideStringField
      FieldName = 'Bairro'
      Size = 30
    end
    object QrSacadosCidade: TWideStringField
      FieldName = 'Cidade'
      Size = 25
    end
    object QrSacadosUF: TWideStringField
      FieldName = 'UF'
      Required = True
      Size = 2
    end
    object QrSacadosCEP: TIntegerField
      FieldName = 'CEP'
    end
    object QrSacadosTel1: TWideStringField
      FieldName = 'Tel1'
    end
    object QrSacadosRisco: TFloatField
      FieldName = 'Risco'
      Required = True
    end
    object QrSacadosAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrSacadosEmail: TWideStringField
      FieldName = 'Email'
      Size = 255
    end
  end
  object PMHistCNAB: TPopupMenu
    OnPopup = PMHistCNABPopup
    Left = 600
    Top = 608
    object Excluihistriciatual1: TMenuItem
      Caption = 'Exclui item de hist'#243'rico atual'
      OnClick = Excluihistriciatual1Click
    end
    object Excluitodositensdehistricosdestaduplicata1: TMenuItem
      Caption = 'Exclui todo hist'#243'rico desta duplicata'
      OnClick = Excluitodositensdehistricosdestaduplicata1Click
    end
  end
  object frxPesq1: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39718.452973449100000000
    ReportOptions.LastChange = 39718.452973449100000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  if <VFR_ORD1> > 0 then'
      '  GH1.Visible := True else GH1.Visible := False;'
      '  if <VFR_ORD1> > 0 then'
      '  GF1.Visible := True else GF1.Visible := False;'
      '  //'
      
        '  if <VFR_ORD1> = 1 then GH1.Condition := '#39'frxDsPesq2."NOMECLIEN' +
        'TE"'#39';'
      
        '  if <VFR_ORD1> = 2 then GH1.Condition := '#39'frxDsPesq2."DDeposito' +
        '_TXT"'#39';'
      
        '  if <VFR_ORD1> = 3 then GH1.Condition := '#39'frxDsPesq2."PERIODO"'#39 +
        ';'
      
        '  if <VFR_ORD1> = 4 then GH1.Condition := '#39'frxDsPesq2."Emitente"' +
        #39';'
      
        '  if <VFR_ORD1> = 5 then GH1.Condition := '#39'frxDsPesq2."CNPJ_TXT"' +
        #39';'
      '  if <VFR_ORD1> = 6 then GH1.Condition := '#39'frxDsPesq2."Data3"'#39';'
      
        '  if <VFR_ORD1> = 7 then GH1.Condition := '#39'frxDsPesq2."PERIODO3"' +
        #39';'
      ''
      ''
      '  if <VFR_ORD2> > 0 then'
      '  GH2.Visible := True else GH2.Visible := False;'
      '  if <VFR_ORD2> > 0 then'
      '  GF2.Visible := True else GF2.Visible := False;'
      '  //'
      
        '  if <VFR_ORD2> = 1 then GH2.Condition := '#39'frxDsPesq2."NOMECLIEN' +
        'TE"'#39';'
      
        '  if <VFR_ORD2> = 2 then GH2.Condition := '#39'frxDsPesq2."DDeposito' +
        '_TXT"'#39';'
      
        '  if <VFR_ORD2> = 3 then GH2.Condition := '#39'frxDsPesq2."PERIODO"'#39 +
        ';'
      
        '  if <VFR_ORD2> = 4 then GH2.Condition := '#39'frxDsPesq2."Emitente"' +
        #39';'
      
        '  if <VFR_ORD2> = 5 then GH2.Condition := '#39'frxDsPesq2."CNPJ_TXT"' +
        #39';'
      '  if <VFR_ORD2> = 6 then GH2.Condition := '#39'frxDsPesq2."Data3"'#39';'
      
        '  if <VFR_ORD2> = 7 then GH2.Condition := '#39'frxDsPesq2."PERIODO3"' +
        #39';'
      ''
      ''
      '  if <VFR_ORD3> > 0 then'
      '  GH3.Visible := True else GH3.Visible := False;'
      '  if <VFR_ORD3> > 0 then'
      '  GF3.Visible := True else GF3.Visible := False;'
      '  //'
      
        '  if <VFR_ORD3> = 1 then GH3.Condition := '#39'frxDsPesq2."NOMECLIEN' +
        'TE"'#39';'
      
        '  if <VFR_ORD3> = 2 then GH3.Condition := '#39'frxDsPesq2."DDeposito' +
        '_TXT"'#39';'
      
        '  if <VFR_ORD3> = 3 then GH3.Condition := '#39'frxDsPesq2."PERIODO"'#39 +
        ';'
      
        '  if <VFR_ORD3> = 4 then GH3.Condition := '#39'frxDsPesq2."Emitente"' +
        #39';'
      
        '  if <VFR_ORD3> = 5 then GH3.Condition := '#39'frxDsPesq2."CNPJ_TXT"' +
        #39';'
      '  if <VFR_ORD3> = 6 then GH3.Condition := '#39'frxDsPesq2."Data3"'#39';'
      
        '  if <VFR_ORD3> = 7 then GH3.Condition := '#39'frxDsPesq2."PERIODO3"' +
        #39';'
      '  '
      'end.')
    OnGetValue = frxPesq1GetValue
    Left = 244
    Top = 8
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsPesq2
        DataSetName = 'frxDsPesq2'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 56.000000000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 554.000000000000000000
          Top = 4.661410000000000000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 2.000000000000000000
          Top = 20.661410000000000000
          Width = 716.000000000000000000
          Height = 26.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 2.000000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CLIENTE]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 36.000000000000000000
        Top = 661.417750000000000000
        Width = 718.110700000000000000
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 582.000000000000000000
          Top = 7.653059999999982000
          Width = 88.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[PAGE#] de ')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 670.000000000000000000
          Top = 7.653059999999982000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[TOTALPAGES]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 88.070810000000000000
        Top = 98.267780000000000000
        Width = 718.110700000000000000
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 2.000000000000000000
          Top = 1.070809999999995000
          Width = 700.000000000000000000
          Height = 22.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'RELAT'#211'RIO DE DUPLICATAS [VARF_SITUACOES]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 2.000000000000000000
          Top = 25.070809999999990000
          Width = 448.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_TODOS]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 514.000000000000000000
          Top = 25.070809999999990000
          Width = 188.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 466.000000000000000000
          Top = 25.070809999999990000
          Width = 48.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Per'#237'odo:')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Top = 71.070810000000010000
          Width = 26.456692910000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Bco.')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456692913385800000
          Top = 71.070810000000010000
          Width = 30.236220472440900000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ag.')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692913385826800000
          Top = 71.070810000000010000
          Width = 56.692913385826800000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Duplicata')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 154.960629920000000000
          Top = 71.070810000000010000
          Width = 185.196850393701000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sacado')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157480310000000000
          Top = 71.070810000000010000
          Width = 98.267716535433100000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'CPF / CNPJ')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 536.692913385827000000
          Top = 71.070810000000010000
          Width = 60.472440944881890000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor Face')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 438.425196850394000000
          Top = 71.070810000000010000
          Width = 49.133858267716500000
          Height = 17.000000000000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Vencto')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 487.559055118110000000
          Top = 71.070810000000010000
          Width = 49.133858267716500000
          Height = 17.000000000000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Quita'#231#227'o')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 2.000000000000000000
          Top = 47.070810000000010000
          Width = 692.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_FILTROS]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385826771654000000
          Top = 71.070810000000010000
          Width = 41.574803149606300000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '000000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'N.F.')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165354330709000000
          Top = 71.070810000000010000
          Width = 60.472440944881890000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor Pago')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 657.637795275591000000
          Top = 71.070810000000010000
          Width = 60.472440944881890000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo Atz')
          ParentFont = False
        end
      end
      object DadosMestre1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.000000000000000000
        Top = 370.393940000000000000
        Width = 718.110700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsPesq2
        DataSetName = 'frxDsPesq2'
        RowCount = 0
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Width = 26.456692910000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39', <frxDsPesq2."Banco">)]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456692910000000000
          Width = 30.236220470000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'0000'#39', <frxDsPesq2."Agencia">)]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692913385826800000
          Width = 56.692913385826800000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPesq2."Duplicata"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 154.960629920000000000
          Width = 185.196850393701000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsPesq2."Emitente"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157480310000000000
          Width = 98.267716540000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsPesq2."CNPJ_TXT"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 536.692913390000000000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq2."Credito"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 438.425196850000000000
          Width = 49.133858270000000000
          Height = 15.000000000000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPesq2."DDeposito"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 487.559055118110000000
          Width = 49.133858267716500000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPesq2."DATA3TXT"]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385826770000000000
          Width = 41.574803150000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000000;-000000; '#39', <frxDsPesq2."NF">)]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165354330709000000
          Width = 60.472440944881890000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq2."TotalPg"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 657.637795275591000000
          Width = 60.472440944881890000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq2."SALDO_ATUALIZADO"]')
          ParentFont = False
        end
      end
      object GH1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.000000000000000000
        Top = 245.669450000000000000
        Width = 718.110700000000000000
        OnBeforePrint = 'GH1OnBeforePrint'
        Condition = 'frxDsPesq2."NOMECLIENTE"'
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 18.000000000000000000
          Width = 692.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_LA1NOME]')
          ParentFont = False
        end
      end
      object GH2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.000000000000000000
        Top = 287.244280000000000000
        Width = 718.110700000000000000
        OnBeforePrint = 'GH2OnBeforePrint'
        Condition = 'frxDsPesq2."DDeposito"'
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 38.000000000000000000
          Width = 672.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_LA2NOME]')
          ParentFont = False
        end
      end
      object GH3: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 17.519480000000000000
        Top = 328.819110000000000000
        Width = 718.110700000000000000
        OnBeforePrint = 'GH3OnBeforePrint'
        Condition = 'frxDsPesq2."PERIODO"'
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 58.000000000000000000
          Width = 652.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_LA3NOME]')
          ParentFont = False
        end
      end
      object GF3: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.000000000000000000
        Top = 408.189240000000000000
        Width = 718.110700000000000000
        OnBeforePrint = 'GF3OnBeforePrint'
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Top = 4.724179999999990000
          Width = 536.692913390000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sub-total de [SUM(<frxDsPesq2."CONTA">)] duplicatas(s):')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 657.637795280000000000
          Top = 4.724179999999990000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."SALDO_ATUALIZADO">)]')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 536.692913390000000000
          Top = 4.724179999999990000
          Width = 60.000000000000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."Credito">)]')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165354330000000000
          Top = 4.724179999999990000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."TotalPg">)]')
          ParentFont = False
        end
      end
      object GF2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.000000000000000000
        Top = 453.543600000000000000
        Width = 718.110700000000000000
        OnBeforePrint = 'GF2OnBeforePrint'
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000022000
          Width = 536.692913390000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sub-total de [SUM(<frxDsPesq2."CONTA">)] duplicatas(s):')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 657.637795280000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."SALDO_ATUALIZADO">)]')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 536.692913390000000000
          Top = 3.779530000000022000
          Width = 60.000000000000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."Credito">)]')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165354330000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."TotalPg">)]')
          ParentFont = False
        end
      end
      object GF1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 43.440630000000000000
        Top = 498.897960000000000000
        Width = 718.110700000000000000
        OnBeforePrint = 'GF1OnBeforePrint'
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Top = 7.559059999999988000
          Width = 536.692913390000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sub-total de [SUM(<frxDsPesq2."CONTA">)] duplicatas(s):')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 657.637795280000000000
          Top = 7.559059999999988000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."SALDO_ATUALIZADO">)]')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 536.692913390000000000
          Top = 7.559059999999988000
          Width = 60.000000000000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."Credito">)]')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165354330000000000
          Top = 7.559059999999988000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."TotalPg">)]')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 32.000000000000000000
        Top = 604.724800000000000000
        Width = 718.110700000000000000
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Top = 7.559060000000045000
          Width = 536.692913390000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sub-total de [SUM(<frxDsPesq2."CONTA">)] duplicatas(s):')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 657.637795280000000000
          Top = 7.559060000000045000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."SALDO_ATUALIZADO">)]')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 536.692913390000000000
          Top = 7.559060000000045000
          Width = 60.000000000000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."Credito">)]')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165354330000000000
          Top = 7.559060000000045000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."TotalPg">)]')
          ParentFont = False
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 3.779530000000022000
          Width = 720.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 2.000000000000000000
        end
      end
    end
  end
  object frxDsPesq2: TfrxDBDataset
    UserName = 'frxDsPesq2'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Controle=Controle'
      'Duplicata=Duplicata'
      'Credito=Credito'
      'DCompra=DCompra'
      'DDeposito=DDeposito'
      'Emitente=Emitente'
      'CNPJCPF=CNPJCPF'
      'Cliente=Cliente'
      'NOMECLIENTE=NOMECLIENTE'
      'STATUS=STATUS'
      'Quitado=Quitado'
      'TotalJr=TotalJr'
      'TotalDs=TotalDs'
      'TotalPg=TotalPg'
      'SALDO_DESATUALIZ=SALDO_DESATUALIZ'
      'SALDO_ATUALIZADO=SALDO_ATUALIZADO'
      'NOMESTATUS=NOMESTATUS'
      'DDCALCJURO=DDCALCJURO'
      'Data3=Data3'
      'CNPJ_TXT=CNPJ_TXT'
      'TAXA_COMPRA=TAXA_COMPRA'
      'TxaCompra=TxaCompra'
      'Devolucao=Devolucao'
      'ProrrVz=ProrrVz'
      'ProrrDd=ProrrDd'
      'Repassado=Repassado'
      'ValQuit=ValQuit'
      'NF=NF'
      'Banco=Banco'
      'Agencia=Agencia'
      'CONTA=CONTA'
      'PERIODO=PERIODO'
      'Valor_TXT=Valor_TXT'
      'DDeposito_TXT=DDeposito_TXT'
      'MES_TXT=MES_TXT'
      'TOTALATUALIZ=TOTALATUALIZ'
      'OCORATUALIZ=OCORATUALIZ'
      'OCORRENCIAS=OCORRENCIAS'
      'ATZ_TEXTO=ATZ_TEXTO'
      'DATA3TXT=DATA3TXT'
      'PERIODO3=PERIODO3'
      'Tel1=Tel1'
      'TEL1_TXT=TEL1_TXT'
      'MEQ_TXT=MEQ_TXT'
      'Data=Data'
      'Vencimento=Vencimento'
      'Juridico=Juridico'
      'CNAB_Lot=CNAB_Lot')
    DataSet = QrPesq2
    BCDToCurrency = False
    
    Left = 216
    Top = 8
  end
  object frxPesq2: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39718.478022997700000000
    ReportOptions.LastChange = 39718.478022997700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  if <VFR_ORD1> > 0 then'
      '  GH1.Visible := True else GH1.Visible := False;'
      '  if <VFR_ORD1> > 0 then'
      '  GF1.Visible := True else GF1.Visible := False;'
      '  //'
      
        '  if <VFR_ORD1> = 1 then GH1.Condition := '#39'frxDsPesq2."NOMECLIEN' +
        'TE"'#39';'
      
        '  if <VFR_ORD1> = 2 then GH1.Condition := '#39'frxDsPesq2."DDeposito' +
        '_TXT"'#39';'
      
        '  if <VFR_ORD1> = 3 then GH1.Condition := '#39'frxDsPesq2."PERIODO"'#39 +
        ';'
      
        '  if <VFR_ORD1> = 4 then GH1.Condition := '#39'frxDsPesq2."Emitente"' +
        #39';'
      
        '  if <VFR_ORD1> = 5 then GH1.Condition := '#39'frxDsPesq2."CNPJ_TXT"' +
        #39';'
      '  if <VFR_ORD1> = 6 then GH1.Condition := '#39'frxDsPesq2."Data3"'#39';'
      
        '  if <VFR_ORD1> = 7 then GH1.Condition := '#39'frxDsPesq2."PERIODO3"' +
        #39';'
      ''
      ''
      '  if <VFR_ORD2> > 0 then'
      '  GH2.Visible := True else GH2.Visible := False;'
      '  if <VFR_ORD2> > 0 then'
      '  GF2.Visible := True else GF2.Visible := False;'
      '  //'
      
        '  if <VFR_ORD2> = 1 then GH2.Condition := '#39'frxDsPesq2."NOMECLIEN' +
        'TE"'#39';'
      
        '  if <VFR_ORD2> = 2 then GH2.Condition := '#39'frxDsPesq2."DDeposito' +
        '_TXT"'#39';'
      
        '  if <VFR_ORD2> = 3 then GH2.Condition := '#39'frxDsPesq2."PERIODO"'#39 +
        ';'
      
        '  if <VFR_ORD2> = 4 then GH2.Condition := '#39'frxDsPesq2."Emitente"' +
        #39';'
      
        '  if <VFR_ORD2> = 5 then GH2.Condition := '#39'frxDsPesq2."CNPJ_TXT"' +
        #39';'
      '  if <VFR_ORD2> = 6 then GH2.Condition := '#39'frxDsPesq2."Data3"'#39';'
      
        '  if <VFR_ORD2> = 7 then GH2.Condition := '#39'frxDsPesq2."PERIODO3"' +
        #39';'
      ''
      ''
      '  if <VFR_ORD3> > 0 then'
      '  GH3.Visible := True else GH3.Visible := False;'
      '  if <VFR_ORD3> > 0 then'
      '  GF3.Visible := True else GF3.Visible := False;'
      '  //'
      
        '  if <VFR_ORD3> = 1 then GH3.Condition := '#39'frxDsPesq2."NOMECLIEN' +
        'TE"'#39';'
      
        '  if <VFR_ORD3> = 2 then GH3.Condition := '#39'frxDsPesq2."DDeposito' +
        '_TXT"'#39';'
      
        '  if <VFR_ORD3> = 3 then GH3.Condition := '#39'frxDsPesq2."PERIODO"'#39 +
        ';'
      
        '  if <VFR_ORD3> = 4 then GH3.Condition := '#39'frxDsPesq2."Emitente"' +
        #39';'
      
        '  if <VFR_ORD3> = 5 then GH3.Condition := '#39'frxDsPesq2."CNPJ_TXT"' +
        #39';'
      '  if <VFR_ORD3> = 6 then GH3.Condition := '#39'frxDsPesq2."Data3"'#39';'
      
        '  if <VFR_ORD3> = 7 then GH3.Condition := '#39'frxDsPesq2."PERIODO3"' +
        #39';'
      '  '
      'end.')
    OnGetValue = frxPesq1GetValue
    Left = 272
    Top = 8
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsPesq2
        DataSetName = 'frxDsPesq2'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 56.000000000000000000
        Top = 18.897650000000000000
        Width = 1046.929810000000000000
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 554.000000000000000000
          Top = 4.661410000000000000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 2.000000000000000000
          Top = 20.661410000000000000
          Width = 716.000000000000000000
          Height = 26.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 2.000000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CLIENTE]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 36.000000000000000000
        Top = 638.740570000000000000
        Width = 1046.929810000000000000
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 876.803340000000000000
          Top = 3.873529999999960000
          Width = 88.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[PAGE#] de ')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 964.803340000000000000
          Top = 3.873529999999960000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[TOTALPAGES]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 88.070810000000000000
        Top = 98.267780000000000000
        Width = 1046.929810000000000000
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 10.000000000000000000
          Top = 1.070809999999995000
          Width = 700.000000000000000000
          Height = 22.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'RELAT'#211'RIO DE DUPLICATAS [VARF_SITUACOES]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 10.000000000000000000
          Top = 25.070809999999990000
          Width = 448.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_TODOS]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 522.000000000000000000
          Top = 25.070809999999990000
          Width = 188.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 474.000000000000000000
          Top = 25.070809999999990000
          Width = 48.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Per'#237'odo:')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = -0.000007320000000000
          Top = 71.070810000000010000
          Width = 26.456692910000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Bco.')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456685590000000000
          Top = 71.070810000000010000
          Width = 30.236220470000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ag.')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692906060000000000
          Top = 71.070810000000010000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Duplicata')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 158.740150160000000000
          Top = 71.070810000000010000
          Width = 234.330703780000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sacado')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 393.070829530000000000
          Top = 71.070810000000010000
          Width = 75.590551180000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'CPF / CNPJ')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 631.181090160000000000
          Top = 71.070810000000010000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Val.Face')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 532.913373620000000000
          Top = 71.070810000000010000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Vencto')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 582.047231890000000000
          Top = 71.070810000000010000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Quita'#231#227'o')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 10.000000000000000000
          Top = 47.070810000000010000
          Width = 692.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_FILTROS]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 117.165347010000000000
          Top = 71.070810000000010000
          Width = 41.574803150000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '000000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'N.F.')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 691.653531100000000000
          Top = 71.070810000000010000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pago')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 752.125972050000000000
          Top = 71.070810000000010000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Sal.Atz')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 812.598412990000000000
          Top = 71.070810000000010000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Ocorr'#234'nc.')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Left = 873.070853940000000000
          Top = 71.070810000000010000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Ocorr.Atz.')
          ParentFont = False
        end
        object Memo63: TfrxMemoView
          AllowVectorExport = True
          Left = 933.543294880000000000
          Top = 71.070810000000010000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total Atz.')
          ParentFont = False
        end
        object Memo69: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Top = 71.055118109999990000
          Width = 64.251961180000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Telefone')
          ParentFont = False
        end
        object Memo71: TfrxMemoView
          AllowVectorExport = True
          Left = 1031.811690000000000000
          Top = 71.055118109999990000
          Width = 15.118110240000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'J')
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          AllowVectorExport = True
          Left = 994.016390000000000000
          Top = 71.055118109999990000
          Width = 37.795270710000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Lote rem.')
          ParentFont = False
        end
      end
      object DadosMestre1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.000000000000000000
        Top = 370.393940000000000000
        Width = 1046.929810000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsPesq2
        DataSetName = 'frxDsPesq2'
        RowCount = 0
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = -0.000007320000000000
          Width = 26.456692910000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39', <frxDsPesq2."Banco">)]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456685590000000000
          Width = 30.236220470000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'0000'#39', <frxDsPesq2."Agencia">)]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692906060000000000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPesq2."Duplicata"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 158.740150160000000000
          Width = 234.330703780000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsPesq2."Emitente"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 393.070829530000000000
          Width = 75.590551180000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsPesq2."CNPJ_TXT"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 631.181090160000000000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DataField = 'Credito'
          DataSet = frxDsPesq2
          DataSetName = 'frxDsPesq2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq2."Credito"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 532.913373620000000000
          Width = 49.133858270000000000
          Height = 15.000000000000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPesq2."DDeposito"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 582.047231890000000000
          Width = 49.133858270000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPesq2."DATA3TXT"]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 117.165347010000000000
          Width = 41.574803150000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000000;-000000; '#39', <frxDsPesq2."NF">)]')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 691.653531100000000000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq2."TotalPg"]')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 752.125972050000000000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq2."SALDO_ATUALIZADO"]')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 812.598412990000000000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq2."OCORRENCIAS"]')
          ParentFont = False
        end
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Left = 873.070853940000000000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq2."OCORATUALIZ"]')
          ParentFont = False
        end
        object Memo68: TfrxMemoView
          AllowVectorExport = True
          Left = 933.543294880000000000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq2."TOTALATUALIZ"]')
          ParentFont = False
        end
        object Memo70: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Width = 64.251961180000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsPesq2."TEL1_TXT"]')
          ParentFont = False
        end
        object CheckBox1: TfrxCheckBoxView
          AllowVectorExport = True
          Left = 1031.811690000000000000
          Width = 15.118120000000000000
          Height = 15.118120000000000000
          CheckColor = clBlack
          CheckStyle = csCheck
          DataField = 'Juridico'
          DataSet = frxDsPesq2
          DataSetName = 'frxDsPesq2'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
        end
        object Memo73: TfrxMemoView
          AllowVectorExport = True
          Left = 994.016390000000000000
          Width = 37.795260940000000000
          Height = 15.000000000000000000
          DataField = 'CNAB_Lot'
          DataSet = frxDsPesq2
          DataSetName = 'frxDsPesq2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq2."CNAB_Lot"]')
          ParentFont = False
        end
      end
      object GH1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.000000000000000000
        Top = 245.669450000000000000
        Width = 1046.929810000000000000
        OnBeforePrint = 'GH1OnBeforePrint'
        Condition = 'frxDsPesq2."NOMECLIENTE"'
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 18.000000000000000000
          Width = 692.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_LA1NOME]')
          ParentFont = False
        end
      end
      object GH2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.000000000000000000
        Top = 287.244280000000000000
        Width = 1046.929810000000000000
        OnBeforePrint = 'GH2OnBeforePrint'
        Condition = 'frxDsPesq2."Emitente"'
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 38.000000000000000000
          Width = 672.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_LA2NOME]')
          ParentFont = False
        end
      end
      object GH3: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.000000000000000000
        Top = 328.819110000000000000
        Width = 1046.929810000000000000
        OnBeforePrint = 'GH3OnBeforePrint'
        Condition = 'frxDsPesq2."DDeposito_TXT"'
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 58.000000000000000000
          Width = 652.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_LA3NOME]')
          ParentFont = False
        end
      end
      object GF3: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.000000000000000000
        Top = 408.189240000000000000
        Width = 1046.929810000000000000
        OnBeforePrint = 'GF3OnBeforePrint'
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 11.338582680000000000
          Top = 3.779530000000022000
          Width = 619.842507480000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sub-total de [SUM(<frxDsPesq2."CONTA">)] duplicatas(s):')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 631.181090160000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."Credito">)]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 691.653531100000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."TotalPg">)]')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 752.125972050000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."SALDO_ATUALIZADO">)]')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 812.598412990000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."OCORRENCIAS">)]')
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          AllowVectorExport = True
          Left = 873.070853940000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."OCORATUALIZ">)]')
          ParentFont = False
        end
        object Memo64: TfrxMemoView
          AllowVectorExport = True
          Left = 933.543294880000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."TOTALATUALIZ">)]')
          ParentFont = False
        end
      end
      object GF2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.000000000000000000
        Top = 453.543600000000000000
        Width = 1046.929810000000000000
        OnBeforePrint = 'GF2OnBeforePrint'
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 11.338582680000000000
          Top = 3.779530000000022000
          Width = 619.842507480000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sub-total de [SUM(<frxDsPesq2."CONTA">)] duplicatas(s):')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 631.181090160000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."Credito">)]')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 691.653531100000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."TotalPg">)]')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 752.125972050000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."SALDO_ATUALIZADO">)]')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 812.598412990000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."OCORRENCIAS">)]')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 873.070853940000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."OCORATUALIZ">)]')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 933.543294880000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."TOTALATUALIZ">)]')
          ParentFont = False
        end
      end
      object GF1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.000000000000000000
        Top = 498.897960000000000000
        Width = 1046.929810000000000000
        OnBeforePrint = 'GF1OnBeforePrint'
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 11.338582680000000000
          Top = 3.779530000000022000
          Width = 619.842507480000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sub-total de [SUM(<frxDsPesq2."CONTA">)] duplicatas(s):')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 631.181090160000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."Credito">)]')
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 691.653531100000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."TotalPg">)]')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 752.125972050000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."SALDO_ATUALIZADO">)]')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 812.598412990000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."OCORRENCIAS">)]')
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Left = 873.070853940000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."OCORATUALIZ">)]')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Left = 933.543294880000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."TOTALATUALIZ">)]')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 32.000000000000000000
        Top = 582.047620000000000000
        Width = 1046.929810000000000000
        object Line1: TfrxLineView
          AllowVectorExport = True
          Left = 2.000000000000000000
          Top = 5.542979999999943000
          Width = 1032.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 2.000000000000000000
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 11.338582680000000000
          Top = 11.338589999999950000
          Width = 638.740157480315000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sub-total de [SUM(<frxDsPesq2."CONTA">)] duplicatas(s):')
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 650.078740160000000000
          Top = 11.338589999999950000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."Credito">)]')
          ParentFont = False
        end
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Left = 710.551181102362000000
          Top = 11.338589999999950000
          Width = 60.472440944881890000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."TotalPg">)]')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 771.023622047244000000
          Top = 11.338589999999950000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."SALDO_ATUALIZADO">)]')
          ParentFont = False
        end
        object Memo65: TfrxMemoView
          AllowVectorExport = True
          Left = 831.496062992126000000
          Top = 11.338589999999950000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."OCORRENCIAS">)]')
          ParentFont = False
        end
        object Memo66: TfrxMemoView
          AllowVectorExport = True
          Left = 891.968503937008000000
          Top = 11.338589999999950000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."OCORATUALIZ">)]')
          ParentFont = False
        end
        object Memo67: TfrxMemoView
          AllowVectorExport = True
          Left = 952.440944881890000000
          Top = 11.338589999999950000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."TOTALATUALIZ">)]')
          ParentFont = False
        end
      end
    end
  end
  object PMImprime: TPopupMenu
    Left = 20
    Top = 36
    object Extratodeduplicata1: TMenuItem
      Caption = '&Extrato de duplicata'
      OnClick = Extratodeduplicata1Click
    end
    object ImprimecartadeCancelamentodeProtesto2: TMenuItem
      Caption = 'Imprime carta de Cancelamento de &Protesto'
      OnClick = ImprimecartadeCancelamentodeProtesto2Click
    end
  end
  object frxExtratoDuplicata: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39949.647167257000000000
    ReportOptions.LastChange = 39949.647167257000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxExtratoDuplicataGetValue
    Left = 300
    Top = 8
    Datasets = <
      item
        DataSet = frxDsADupIts
        DataSetName = 'frxDsADupIts'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsDupPgs
        DataSetName = 'frxDsDupPgs'
      end
      item
        DataSet = frxDsLotPrr
        DataSetName = 'frxDsLotPrr'
      end
      item
        DataSet = frxDsOcorreu
        DataSetName = 'frxDsOcorreu'
      end
      item
        DataSet = frxDsPesq
        DataSetName = 'frxDsPesq'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 291.023810000000000000
        Width = 680.315400000000000000
        DataSet = frxDsOcorreu
        DataSetName = 'frxDsOcorreu'
        RowCount = 0
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          DataField = 'SEQ'
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsOcorreu."SEQ"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Width = 52.913420000000000000
          Height = 15.118110240000000000
          DataField = 'Codigo'
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOcorreu."Codigo"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 90.708720000000000000
          Width = 60.472480000000000000
          Height = 15.118110240000000000
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsOcorreu."DataO"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Width = 302.362400000000000000
          Height = 15.118110240000000000
          DataField = 'NOMEOCORRENCIA'
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOcorreu."NOMEOCORRENCIA"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DataField = 'Valor'
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOcorreu."Valor"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOcorreu."SALDO"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 71.811070000000000000
          Height = 15.118110240000000000
          DataField = 'ATZ_TEXTO'
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOcorreu."ATZ_TEXTO"]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 805.039890000000000000
        Width = 680.315400000000000000
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 64.252010000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'EXTRATO DE DUPLICATA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Top = 45.354360000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPesq."NOMECLIENTE"]')
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 41.574830000000000000
        Top = 226.771800000000000000
        Width = 680.315400000000000000
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 26.456710000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'OCORR'#202'NCIAS DO T'#205'TULO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Top = 26.456709999999990000
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Seq.')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 26.456709999999990000
          Width = 52.913420000000000000
          Height = 15.118110240000000000
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'ID')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 90.708720000000000000
          Top = 26.456709999999990000
          Width = 60.472480000000000000
          Height = 15.118110240000000000
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 26.456709999999990000
          Width = 302.362400000000000000
          Height = 15.118110240000000000
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o da ocorr'#234'ncia')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 26.456709999999990000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 26.456709999999990000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'SALDO')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 26.456709999999990000
          Width = 71.811070000000000000
          Height = 15.118110240000000000
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Atualizado')
          ParentFont = False
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.236240000000000000
        Top = 328.819110000000000000
        Width = 680.315400000000000000
      end
      object PageHeader3: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 60.472480000000000000
        Top = 105.826840000000000000
        Width = 680.315400000000000000
        object Shape2: TfrxShapeView
          AllowVectorExport = True
          Top = 3.779529999999994000
          Width = 680.315400000000000000
          Height = 56.692950000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Line2: TfrxLineView
          AllowVectorExport = True
          Top = 22.677179999999990000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 3.779529999999994000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Duplicata:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 60.472480000000000000
          Top = 3.779529999999994000
          Width = 128.504020000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsPesq."Duplicata"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976500000000000000
          Top = 3.779529999999994000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Valor original:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo64: TfrxMemoView
          AllowVectorExport = True
          Left = 264.567100000000000000
          Top = 3.779529999999994000
          Width = 113.385900000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsPesq."Credito"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo65: TfrxMemoView
          AllowVectorExport = True
          Left = 408.189240000000000000
          Top = 3.779529999999994000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Emiss'#227'o:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo66: TfrxMemoView
          AllowVectorExport = True
          Left = 457.323130000000000000
          Top = 3.779529999999994000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsPesq."Data"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo67: TfrxMemoView
          AllowVectorExport = True
          Left = 532.913730000000000000
          Top = 3.779529999999994000
          Width = 64.252010000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Vencimento:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo68: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165740000000000000
          Top = 3.779529999999994000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsPesq."DDeposito"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 22.677179999999990000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Sacado:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 60.472480000000000000
          Top = 22.677179999999990000
          Width = 430.866420000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsPesq."Emitente"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 41.574829999999990000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Baixado:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 60.472480000000000000
          Top = 41.574829999999990000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsPesq."DATA3_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 491.338900000000000000
          Top = 22.677179999999990000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_CNPJ_CPF]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 22.677179999999990000
          Width = 143.622022830000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsPesq."CNPJ_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Top = 41.574829999999990000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
      end
      object Header2: TfrxHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 41.574830000000000000
        Top = 381.732530000000000000
        Width = 680.315400000000000000
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 26.456710000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'HIST'#211'RICO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Top = 26.456709999999990000
          Width = 60.472440944881890000
          Height = 15.118110240000000000
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 60.472480000000000000
          Top = 26.456709999999990000
          Width = 619.842920000000000000
          Height = 15.118110240000000000
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Hist'#243'rico')
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 445.984540000000000000
        Width = 680.315400000000000000
        DataSet = frxDsADupIts
        DataSetName = 'frxDsADupIts'
        RowCount = 0
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'DataA'
          DataSet = frxDsADupIts
          DataSetName = 'frxDsADupIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsADupIts."DataA"]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 60.472480000000000000
          Width = 619.842880940000000000
          Height = 15.118110240000000000
          DataField = 'NOMESTATUS'
          DataSet = frxDsADupIts
          DataSetName = 'frxDsADupIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsADupIts."NOMESTATUS"]')
          ParentFont = False
        end
      end
      object Footer2: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 3.779530000000000000
        Top = 483.779840000000000000
        Width = 680.315400000000000000
      end
      object Header3: TfrxHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 41.574830000000000000
        Top = 510.236550000000000000
        Width = 680.315400000000000000
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 26.456710000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'PAGAMENTOS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Top = 26.456709999999990000
          Width = 113.385826771653500000
          Height = 15.118110240000000000
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385900000000000000
          Top = 26.456709999999990000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Juros')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 26.456709999999990000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Desconto')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 26.456709999999990000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pago')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 26.456709999999990000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Border'#244)
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Top = 26.456709999999990000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Lote do pagto')
          ParentFont = False
        end
      end
      object MasterData3: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 574.488560000000000000
        Width = 680.315400000000000000
        DataSet = frxDsDupPgs
        DataSetName = 'frxDsDupPgs'
        RowCount = 0
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DataField = 'Data'
          DataSet = frxDsDupPgs
          DataSetName = 'frxDsDupPgs'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDupPgs."Data"]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385900000000000000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DataField = 'MoraVal'
          DataSet = frxDsDupPgs
          DataSetName = 'frxDsDupPgs'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsDupPgs."MoraVal"]')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DataSet = frxDsDupPgs
          DataSetName = 'frxDsDupPgs'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsDupPgs."DescoVal"]')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DataField = 'Credito'
          DataSet = frxDsDupPgs
          DataSetName = 'frxDsDupPgs'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsDupPgs."Credito"]')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DataField = 'Lote'
          DataSet = frxDsDupPgs
          DataSetName = 'frxDsDupPgs'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDupPgs."Lote"]')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DataField = 'FatNum'
          DataSet = frxDsDupPgs
          DataSetName = 'frxDsDupPgs'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDupPgs."FatNum"]')
          ParentFont = False
        end
      end
      object Header4: TfrxHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 41.574830000000000000
        Top = 638.740570000000000000
        Width = 680.315400000000000000
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 26.456710000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'PRORROGA'#199#213'ES')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Top = 26.456710000000040000
          Width = 128.503937007874000000
          Height = 15.118110240000000000
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Vencimento original')
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Left = 385.512060000000000000
          Top = 26.456710000000040000
          Width = 136.063031180000000000
          Height = 15.118110240000000000
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Dias desta prorroga'#231#227'o')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 128.504020000000000000
          Top = 26.456710000000040000
          Width = 128.503937010000000000
          Height = 15.118110240000000000
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Vencimento anterior')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 257.008040000000000000
          Top = 26.456710000000040000
          Width = 128.503937010000000000
          Height = 15.118110240000000000
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Novo vencimento')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Left = 521.575140000000000000
          Top = 26.456710000000040000
          Width = 158.740211180000000000
          Height = 15.118110240000000000
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ac'#250'mulo dias prorrogados')
          ParentFont = False
        end
      end
      object MasterData4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 702.992580000000000000
        Width = 680.315400000000000000
        DataSet = frxDsLotPrr
        DataSetName = 'frxDsLotPrr'
        RowCount = 0
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Width = 128.503937010000000000
          Height = 15.118110240000000000
          DataField = 'Data1'
          DataSet = frxDsLotPrr
          DataSetName = 'frxDsLotPrr'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLotPrr."Data1"]')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 385.512060000000000000
          Width = 136.063031180000000000
          Height = 15.118110240000000000
          DataField = 'DIAS_2_3'
          DataSet = frxDsLotPrr
          DataSetName = 'frxDsLotPrr'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLotPrr."DIAS_2_3"]')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 128.504020000000000000
          Width = 128.503937010000000000
          Height = 15.118110240000000000
          DataField = 'Data2'
          DataSet = frxDsLotPrr
          DataSetName = 'frxDsLotPrr'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLotPrr."Data2"]')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 257.008040000000000000
          Width = 128.503937010000000000
          Height = 15.118110240000000000
          DataField = 'Data3'
          DataSet = frxDsLotPrr
          DataSetName = 'frxDsLotPrr'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLotPrr."Data3"]')
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          AllowVectorExport = True
          Left = 521.575140000000000000
          Width = 158.740211180000000000
          Height = 15.118110240000000000
          DataField = 'DIAS_1_3'
          DataSet = frxDsLotPrr
          DataSetName = 'frxDsLotPrr'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLotPrr."DIAS_1_3"]')
          ParentFont = False
        end
      end
      object Footer3: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 3.779530000000000000
        Top = 612.283860000000000000
        Width = 680.315400000000000000
      end
      object Footer4: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 3.779530000000000000
        Top = 740.787880000000000000
        Width = 680.315400000000000000
      end
    end
  end
  object frxDsOcorreu: TfrxDBDataset
    UserName = 'frxDsOcorreu'
    CloseDataSource = False
    DataSet = QrOcorreu
    BCDToCurrency = False
    
    Left = 464
    Top = 272
  end
  object frxDsADupIts: TfrxDBDataset
    UserName = 'frxDsADupIts'
    CloseDataSource = False
    DataSet = QrADupIts
    BCDToCurrency = False
    
    Left = 668
    Top = 300
  end
  object frxDsDupPgs: TfrxDBDataset
    UserName = 'frxDsDupPgs'
    CloseDataSource = False
    DataSet = QrDupPgs
    BCDToCurrency = False
    
    Left = 668
    Top = 328
  end
  object frxDsLotPrr: TfrxDBDataset
    UserName = 'frxDsLotPrr'
    CloseDataSource = False
    DataSet = QrLotPrr
    BCDToCurrency = False
    
    Left = 668
    Top = 356
  end
  object frxDsPesq: TfrxDBDataset
    UserName = 'frxDsPesq'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Duplicata=Duplicata'
      'DCompra=DCompra'
      'DDeposito=DDeposito'
      'Emitente=Emitente'
      'Cliente=Cliente'
      'NOMECLIENTE=NOMECLIENTE'
      'STATUS=STATUS'
      'Quitado=Quitado'
      'TotalJr=TotalJr'
      'TotalDs=TotalDs'
      'TotalPg=TotalPg'
      'SALDO_DESATUALIZ=SALDO_DESATUALIZ'
      'SALDO_ATUALIZADO=SALDO_ATUALIZADO'
      'NOMESTATUS=NOMESTATUS'
      'DDCALCJURO=DDCALCJURO'
      'Data3=Data3'
      'CNPJ_TXT=CNPJ_TXT'
      'TAXA_COMPRA=TAXA_COMPRA'
      'TxaCompra=TxaCompra'
      'Devolucao=Devolucao'
      'ProrrVz=ProrrVz'
      'ProrrDd=ProrrDd'
      'Repassado=Repassado'
      'ValQuit=ValQuit'
      'NF=NF'
      'Banco=Banco'
      'PERIODO=PERIODO'
      'OCORRENCIAS=OCORRENCIAS'
      'OCORATUALIZ=OCORATUALIZ'
      'TOTALATUALIZ=TOTALATUALIZ'
      'SALDO_TEXTO=SALDO_TEXTO'
      'LOCALT=LOCALT'
      'NOMELOCAL=NOMELOCAL'
      'PERIODO3=PERIODO3'
      'DATA3_TXT=DATA3_TXT'
      'FatParcela=FatParcela'
      'Data=Data'
      'Credito=Credito'
      'CNPJCPF=CNPJCPF'
      'Vencimento=Vencimento'
      'Agencia=Agencia'
      'Codigo=Codigo'
      'LctCtrl=LctCtrl'
      'LctSub=LctSub'
      'CartDep=CartDep'
      'Controle=Controle'
      'Tel1=Tel1'
      'Juridico=Juridico'
      'CNAB_Lot=CNAB_Lot')
    DataSet = QrPesq
    BCDToCurrency = False
    
    Left = 668
    Top = 272
  end
  object PMDiario: TPopupMenu
    OnPopup = PMDiarioPopup
    Left = 960
    Top = 368
    object Adicionareventoaodiario1: TMenuItem
      Caption = '&Adicionar evento ao di'#225'rio'
      OnClick = Adicionareventoaodiario1Click
    end
    object Gerenciardirio1: TMenuItem
      Caption = '&Gerenciar di'#225'rio'
      OnClick = Gerenciardirio1Click
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object Imprimirdiriodeitemselecionado1: TMenuItem
      Caption = '&Imprimir di'#225'rio de item selecionado'
      OnClick = Imprimirdiriodeitemselecionado1Click
    end
    object Imprimirdiriodeitenspesquisados1: TMenuItem
      Caption = 'Imprimir di'#225'rio de itens &pesquisados'
      OnClick = Imprimirdiriodeitenspesquisados1Click
    end
    object N7: TMenuItem
      Caption = '-'
    end
    object Localizarremessa1: TMenuItem
      Caption = '&Localizar remessa'
      OnClick = Localizarremessa1Click
    end
  end
  object frxGER_DIARI_001_01: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38101.979964398100000000
    ReportOptions.LastChange = 41187.375793703700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 800
    Top = 8
    Datasets = <
      item
        DataSet = frxDsDiarioAdd
        DataSetName = 'frxDsDiarioAdd'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Columns = 1
      ColumnWidth = 190.000000000000000000
      ColumnPositions.Strings = (
        '0')
      Frame.Typ = []
      MirrorMode = []
      object TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 117.165430000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsDiarioAdd."NOME_UH_ENT"'
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Width = 680.314960630000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsDiarioAdd."NOME_UH_ENT"]')
          ParentFont = False
        end
      end
      object TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 7.559060000000000000
        Top = 245.669450000000000000
        Width = 680.315400000000000000
        object frxMemoView1: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 3.779530000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
      end
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 37.795300000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Relat'#243'rio de Atividades Reportadas no Di'#225'rio')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 313.700990000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 162.519790000000000000
        Width = 680.315400000000000000
        Child = frxGER_DIARI_001_01.Child1
        DataSet = frxDsDiarioAdd
        DataSetName = 'frxDsDiarioAdd'
        RowCount = 0
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Width = 34.015721180000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data:')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 34.015672360000000000
          Width = 94.488188980000000000
          Height = 17.007874020000000000
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsDiarioAdd."Data"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 128.503873540000000000
          Width = 30.236178980000000000
          Height = 17.007874020000000000
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Hora:')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 158.740064720000000000
          Width = 94.488188980000000000
          Height = 17.007874020000000000
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = 'hh:mm:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsDiarioAdd."Hora"]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 253.228510000000000000
          Width = 90.708671180000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#250'mero duplicata:')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 343.937132360000000000
          Width = 94.488188980000000000
          Height = 17.007874020000000000
          DisplayFormat.DecimalSeparator = '.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsDiarioAdd."Duplicata"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 438.425480000000000000
          Width = 64.251961180000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Vencimento:')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677392360000000000
          Width = 94.488188980000000000
          Height = 17.007874020000000000
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsDiarioAdd."DDeposito"]')
          ParentFont = False
        end
      end
      object Child1: TfrxChild
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 204.094620000000000000
        Width = 680.315400000000000000
        Stretched = True
        ToNRows = 0
        ToNRowsMode = rmCount
        object Rich1: TfrxRichView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          StretchMode = smMaxHeight
          DataField = 'Nome'
          DataSet = frxDsDiarioAdd
          DataSetName = 'frxDsDiarioAdd'
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          GapX = 2.000000000000000000
          GapY = 1.000000000000000000
          RichEdit = {
            7B5C727466315C616E73695C616E7369637067313235325C64656666305C6E6F
            7569636F6D7061745C6465666C616E67313034367B5C666F6E7474626C7B5C66
            305C666E696C205461686F6D613B7D7D0D0A7B5C2A5C67656E657261746F7220
            52696368656432302031302E302E31393034317D5C766965776B696E64345C75
            6331200D0A5C706172645C66305C667331365C7061720D0A7D0D0A00}
        end
      end
    end
  end
  object frxDsDiarioAdd: TfrxDBDataset
    UserName = 'frxDsDiarioAdd'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NOME_CLI=NOME_CLI'
      'NOME_ENT=NOME_ENT'
      'NOME_DEPTO=NOME_DEPTO'
      'Codigo=Codigo'
      'Nome=Nome'
      'DiarioAss=DiarioAss'
      'Entidade=Entidade'
      'CliInt=CliInt'
      'Depto=Depto'
      'Data=Data'
      'Hora=Hora'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'NOME_UH_ENT=NOME_UH_ENT'
      'DDeposito=DDeposito'
      'Duplicata=Duplicata')
    DataSet = QrDiarioAdd
    BCDToCurrency = False
    
    Left = 828
    Top = 8
  end
  object QrDiarioAdd: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CONCAT(dpt.Unidade, " - ", '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)) NOME_UH_ENT,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NOME_CLI,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOME_ENT,'
      '"" NOME_DEPTO, lct.Duplicata, lct.DDeposito, dad.*'
      'FROM diarioadd dad'
      'LEFT JOIN entidades cli ON cli.Codigo=dad.CliInt'
      'LEFT JOIN entidades ent ON ent.Codigo=dad.Entidade'
      
        'LEFT JOIN lct0001a lct ON lct.Controle=dad.Depto AND lct.FatID=3' +
        '01')
    Left = 856
    Top = 8
    object QrDiarioAddNOME_CLI: TWideStringField
      FieldName = 'NOME_CLI'
      Size = 100
    end
    object QrDiarioAddNOME_ENT: TWideStringField
      FieldName = 'NOME_ENT'
      Size = 100
    end
    object QrDiarioAddNOME_DEPTO: TWideStringField
      FieldName = 'NOME_DEPTO'
      Size = 10
    end
    object QrDiarioAddCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDiarioAddNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrDiarioAddDiarioAss: TIntegerField
      FieldName = 'DiarioAss'
    end
    object QrDiarioAddEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrDiarioAddCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrDiarioAddDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrDiarioAddData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDiarioAddHora: TTimeField
      FieldName = 'Hora'
      DisplayFormat = 'hh:nn:ss'
    end
    object QrDiarioAddDataCad: TDateField
      FieldName = 'DataCad'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDiarioAddDataAlt: TDateField
      FieldName = 'DataAlt'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDiarioAddUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrDiarioAddUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrDiarioAddNOME_UH_ENT: TWideStringField
      FieldName = 'NOME_UH_ENT'
      Size = 113
    end
    object QrDiarioAddDDeposito: TDateField
      FieldName = 'DDeposito'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDiarioAddDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Required = True
      Size = 12
    end
  end
  object DsDiarioAdd: TDataSource
    DataSet = QrDiarioAdd
    Left = 884
    Top = 8
  end
end
