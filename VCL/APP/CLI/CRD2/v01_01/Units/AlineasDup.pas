unit AlineasDup;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkDBLookupComboBox,
  dmkEditCB, dmkRadioGroup, Variants, ComCtrls, dmkImage, UnDmkProcFunc,
  dmkCheckBox, UnDmkEnums;

type
  TFmAlineasDup = class(TForm)
    PainelDados: TPanel;
    DsOcorDupl: TDataSource;
    QrOcorDupl: TmySQLQuery;
    PainelEdita: TPanel;
    dmkPermissoes1: TdmkPermissoes;
    QrOcorDuplCodigo: TIntegerField;
    QrOcorDuplNome: TWideStringField;
    QrOcorDuplBase: TFloatField;
    QrOcorDuplLk: TIntegerField;
    QrOcorDuplDataCad: TDateField;
    QrOcorDuplDataAlt: TDateField;
    QrOcorDuplUserCad: TIntegerField;
    QrOcorDuplUserAlt: TIntegerField;
    QrOcorDuplDatapbase: TIntegerField;
    QrOcorDuplOcorrbase: TIntegerField;
    QrOcorDuplStatusbase: TIntegerField;
    QrOcorDuplEnvio: TIntegerField;
    QrOcorDuplMovimento: TIntegerField;
    QrOcorDuplAlterWeb: TSmallintField;
    QrOcorDuplAtivo: TSmallintField;
    QrOcorBank: TmySQLQuery;
    QrOcorBankCodigo: TIntegerField;
    QrOcorBankNome: TWideStringField;
    DsOcorBank: TDataSource;
    QrOcorDuplNOMEOCORBANK: TWideStringField;
    OpenDialog1: TOpenDialog;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBEdit: TGroupBox;
    Label7: TLabel;
    Label9: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    SpeedButton5: TSpeedButton;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    EdBase: TdmkEdit;
    RGDataPBase: TdmkRadioGroup;
    EdOcorrencia: TdmkEditCB;
    CBOcorrencia: TdmkDBLookupComboBox;
    RGStatusBase: TdmkRadioGroup;
    GBData: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdNome: TdmkDBEdit;
    dmkDBEdit1: TdmkDBEdit;
    dmkDBEdit2: TdmkDBEdit;
    DBRadioGroup1: TDBRadioGroup;
    DBRadioGroup2: TDBRadioGroup;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel6: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel7: TPanel;
    BitBtn1: TBitBtn;
    BtImportar: TBitBtn;
    Progress: TProgressBar;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrOcorDuplAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrOcorDuplBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure EdCodigoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedButton5Click(Sender: TObject);
    procedure BtImportarClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    //procedure IncluiRegistro;
    //procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
  end;

var
  FmAlineasDup: TFmAlineasDup;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, Principal;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmAlineasDup.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmAlineasDup.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrOcorDuplCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmAlineasDup.DefParams;
begin
  VAR_GOTOTABELA := 'ocordupl';
  VAR_GOTOMYSQLTABLE := QrOcorDupl;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT ok.Nome NOMEOCORBANK, od.*');
  VAR_SQLx.Add('FROM ocordupl od');
  VAR_SQLx.Add('LEFT JOIN ocorbank ok ON ok.Codigo=od.Ocorrbase');
  VAR_SQLx.Add('WHERE od.Codigo <> 0');
  //
  VAR_SQL1.Add('AND od.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND od.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND od.Nome Like :P0');
  //
end;

procedure TFmAlineasDup.EdCodigoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'ocordupl', 'Codigo', [], [],
    stIns, 0, siPositivo, EdCodigo);
end;

procedure TFmAlineasDup.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible    := True;
      PainelEdita.Visible    := False;
    end;
    1:
    begin
      PainelEdita.Visible    := True;
      PainelDados.Visible    := False;
      if SQLType = stIns then
      begin
        EdCodigo.ValueVariant     := FormatFloat(FFormatFloat, Codigo);
        EdNome.ValueVariant       := '';
        EdBase.ValueVariant       := 0;
        RGDataPBase.ItemIndex     := 0;
        EdOcorrencia.ValueVariant := 0;
        CBOcorrencia.KeyValue     := Null;
        RGStatusBase.ItemIndex    := 0;
        //
      end else begin
        EdCodigo.ValueVariant     := QrOcorDuplCodigo.Value;
        EdNome.ValueVariant       := QrOcorDuplNome.Value;
        EdBase.ValueVariant       := QrOcorDuplBase.Value;
        RGDataPBase.ItemIndex     := QrOcorDuplDatapbase.Value;
        EdOcorrencia.ValueVariant := QrOcorDuplOcorrbase.Value;
        CBOcorrencia.KeyValue     := QrOcorDuplOcorrbase.Value;
        RGStatusBase.ItemIndex    := QrOcorDuplStatusbase.Value;
        //
      end;
      EdNome.SetFocus;
    end;
    else Geral.MensagemBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmAlineasDup.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmAlineasDup.QueryPrincipalAfterOpen;
begin
end;

procedure TFmAlineasDup.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmAlineasDup.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmAlineasDup.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmAlineasDup.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmAlineasDup.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmAlineasDup.SpeedButton5Click(Sender: TObject);
var
  Codigo: Integer;
begin
  VAR_CADASTRO := 0;
  Codigo       := EdOcorrencia.ValueVariant;
  //
  FmPrincipal.CadastroOcorBank(Codigo);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.AbreQuery(QrOcorBank, Dmod.MyDB);
    //
    EdOcorrencia.ValueVariant := VAR_CADASTRO;
    CBOcorrencia.KeyValue     := VAR_CADASTRO;
    EdOcorrencia.SetFocus;
  end;
end;

procedure TFmAlineasDup.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrOcorDupl, [PainelDados],
  [PainelEdita], EdNome, ImgTipo, 'ocordupl');
end;

procedure TFmAlineasDup.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrOcorDuplCodigo.Value;
  Close;
end;

procedure TFmAlineasDup.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome   := EdNome.ValueVariant;
  //
  Codigo := EdCodigo.ValueVariant;
  if MyObjects.FIC(Codigo = 0, EdCodigo, 'Defina o ID!') then Exit;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, self, PainelEdita,
    'ocordupl', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmAlineasDup.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'ocordupl', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ocordupl', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ocordupl', 'Codigo');
end;

procedure TFmAlineasDup.BtImportarClick(Sender: TObject);
var
  Lista: TStringList;
  i, PosNum, Tam: Integer;
  Codigo, Nome: String;
begin
  if OpenDialog1.Execute then
  begin
    Screen.Cursor := crHourGlass;
    //
    PosNum := 0;
    Lista := TStringList.Create;
    Lista.LoadFromFile(OpenDialog1.FileName);
    Dmod.QrUpdM.SQL.Clear;
    Dmod.QrUpdM.SQL.Add('DELETE FROM ocordupl WHERE Codigo=:P0');
    //
    Dmod.QrUpdU.SQL.Clear;
    Dmod.QrUpdU.SQL.Add('INSERT INTO ocordupl SET');
    Dmod.QrUpdU.SQL.Add('Nome=:P0, Codigo=:P1');
    //
    Progress.Position := 0;
    Progress.Visible := True;
    Progress.Max := Lista.Count;

    for i := 0 to Lista.Count-1 do
    begin
      Progress.Position := Progress.Position + 1;
      Tam := Length(Lista[i]);
      if Tam > 4 then
      begin
        if (Lista[i][1] in (['0'..'9'])) then
        begin
          Codigo := Copy(Lista[i], 1, 3);
          if PosNum <> 0 then
            Nome := TrimRight(Copy(Lista[i], 5, Length(Lista[i])))
          else
            Nome := Copy(Lista[i], 5, Length(Lista[i]));
          //ShowMessage(PChar(Codigo+' - '+Nome+' @ '+Motivo));
          Dmod.QrUpdM.Params[00].AsString := Codigo;
          Dmod.QrUpdM.ExecSQL;
          //
          Dmod.QrUpdU.Params[00].AsString := Nome;
          Dmod.QrUpdU.Params[01].AsString := Codigo;
          Dmod.QrUpdU.ExecSQL;
        end;
      end;
    end;
    Progress.Visible := False;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmAlineasDup.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdita, QrOcorDupl, [PainelDados],
  [PainelEdita], EdCodigo, ImgTipo, 'ocordupl');
end;

procedure TFmAlineasDup.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  GBEdit.Align  := alClient;
  GBData.Align  := alClient;
  CriaOForm;
  //
  UMyMod.AbreQuery(QrOcorBank, Dmod.MyDB);
end;

procedure TFmAlineasDup.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrOcorDuplCodigo.Value, LaRegistro.Caption);
end;

procedure TFmAlineasDup.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmAlineasDup.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrOcorDuplCodigo.Value, LaRegistro.Caption);
end;

procedure TFmAlineasDup.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmAlineasDup.QrOcorDuplAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmAlineasDup.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmAlineasDup.SbQueryClick(Sender: TObject);
begin
  LocCod(QrOcorDuplCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'ocordupl', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmAlineasDup.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmAlineasDup.QrOcorDuplBeforeOpen(DataSet: TDataSet);
begin
  QrOcorDuplCodigo.DisplayFormat := FFormatFloat;
end;

end.

