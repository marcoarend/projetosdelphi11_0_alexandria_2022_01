unit Lot0Loc;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UnMLAGeral, UnGOTOy, Mask, UMySQLModule, mySQLDbTables, Grids, DBGrids,
  ComCtrls, dmkEdit, dmkEditCB, dmkDBLookupComboBox, dmkGeral, dmkImage,
  dmkEditDateTimePicker, UnDmkProcFunc, UnDmkEnums;

type
  TFmLot0Loc = class(TForm)
    PainelDados: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    CBCliente: TdmkDBLookupComboBox;
    DsClientes: TDataSource;
    EdCliente: TdmkEditCB;
    EdLote: TdmkEdit;
    Label3: TLabel;
    QrClientes: TmySQLQuery;
    QrClientesNOMEENTIDADE: TWideStringField;
    QrClientesCodigo: TIntegerField;
    QrLoc: TmySQLQuery;
    DBGrid1: TDBGrid;
    DsLoc: TDataSource;
    QrLocCodigo: TIntegerField;
    QrLocLote: TSmallintField;
    QrLocData: TDateField;
    QrLocTipo: TSmallintField;
    QrLocNOMETIPO: TWideStringField;
    QrLocTotal: TFloatField;
    QrLocNOMECLIENTE: TWideStringField;
    QrLocCNPJCPF: TWideStringField;
    QrLocSpread: TSmallintField;
    QrLocCliente: TIntegerField;
    QrLocDias: TFloatField;
    QrLocPeCompra: TFloatField;
    QrLocTxCompra: TFloatField;
    QrLocValValorem: TFloatField;
    QrLocAdValorem: TFloatField;
    QrLocIOC: TFloatField;
    QrLocIOC_VAL: TFloatField;
    QrLocTarifas: TFloatField;
    QrLocCPMF: TFloatField;
    QrLocCPMF_VAL: TFloatField;
    QrLocTipoAdV: TIntegerField;
    QrLocIRRF: TFloatField;
    QrLocIRRF_Val: TFloatField;
    QrLocISS: TFloatField;
    QrLocISS_Val: TFloatField;
    QrLocPIS: TFloatField;
    QrLocPIS_Val: TFloatField;
    QrLocPIS_R: TFloatField;
    QrLocPIS_R_Val: TFloatField;
    QrLocCOFINS: TFloatField;
    QrLocCOFINS_Val: TFloatField;
    QrLocCOFINS_R: TFloatField;
    QrLocCOFINS_R_Val: TFloatField;
    QrLocOcorP: TFloatField;
    QrLocMaxVencto: TDateField;
    QrLocCHDevPg: TFloatField;
    QrLocLk: TIntegerField;
    QrLocDataCad: TDateField;
    QrLocDataAlt: TDateField;
    QrLocUserCad: TIntegerField;
    QrLocUserAlt: TIntegerField;
    QrLocMINTC: TFloatField;
    QrLocMINAV: TFloatField;
    QrLocMINTC_AM: TFloatField;
    QrLocPIS_T_Val: TFloatField;
    QrLocCOFINS_T_Val: TFloatField;
    QrLocPgLiq: TFloatField;
    QrLocDUDevPg: TFloatField;
    QrLocNF: TIntegerField;
    TPIni: TdmkEditDateTimePicker;
    Label34: TLabel;
    TPFim: TdmkEditDateTimePicker;
    Label4: TLabel;
    QrLocItens: TIntegerField;
    QrLocConferido: TIntegerField;
    QrLocECartaSac: TSmallintField;
    EdControle: TdmkEdit;
    Label5: TLabel;
    QrCon: TmySQLQuery;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtConfirma: TBitBtn;
    QrConFatNum: TFloatField;
    Label6: TLabel;
    EdNF: TdmkEdit;
    procedure BtDesisteClick(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdLoteExit(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure QrLocAfterOpen(DataSet: TDataSet);
    procedure EdLoteChange(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure QrLocCalcFields(DataSet: TDataSet);
    procedure TPIniClick(Sender: TObject);
    procedure TPFimClick(Sender: TObject);
    procedure EdControleChange(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure EdNFChange(Sender: TObject);
    procedure EdNFExit(Sender: TObject);
  private
    { Private declarations }
    procedure LocalizaBordero(Lote: Integer);
    procedure LocalizaBorderoNF(NF: Integer);
  public
    { Public declarations }
    FNF, FFormCall: Integer;
    procedure ReopenLoc();
  end;

var
  FmLot0Loc: TFmLot0Loc;

implementation

uses UnMyObjects, Module, Entidades, Principal, MyListas, DmkDAC_PF;

{$R *.DFM}

procedure TFmLot0Loc.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLot0Loc.BtConfirmaClick(Sender: TObject);
begin
  case FFormCall of
    1:
    begin
      FmPrincipal.FLoteLoc := QrLocCodigo.Value;
      Close;
    end;
    2:
    begin
      FmPrincipal.AdicionaBorderoANF(FNF, QrLocCodigo.Value);
      ReopenLoc();
    end;
    3:
    begin
      FmPrincipal.FLoteLoc := QrLocNF.Value;
      Close;
    end;
    else FmPrincipal.FLoteLoc := 0;
  end;
end;

procedure TFmLot0Loc.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLot0Loc.EdClienteChange(Sender: TObject);
begin
  ReopenLoc();
end;

procedure TFmLot0Loc.ReopenLoc();
var
  Cliente, FatParcela: Integer;
  LinX1, LinX2, LinX3, LinX4, LinX5: String;
begin
  LinX1 := '';
  LinX2 := '';
  LinX3 := '';
  LinX4 := '';
  LinX5 := '';
  //
  Cliente := EdCliente.ValueVariant;
  if Cliente <> 0 then
    LinX1 := 'AND lo.Cliente='+IntToStr(Cliente);
  //
  FatParcela := EdControle.ValueVariant;
  if FatParcela <> 0 then
  begin
{
    QrCon.Close;
    QrCon.Params[0].AsInteger := FatParcela;
    QrCon. Open;
}
    UnDmkDAC_PF.AbreMySQLQuery0(QrCon, Dmod.MyDB, [
    'SELECT FatNum ',
    'FROM ' + CO_TabLctA,
    'WHERE FatID=' + FormatFloat('0', VAR_FATID_0301),
    'AND FatParcela=' + FormatFloat('0', FatParcela),
    '']);
    //
    if QrConFatNum.Value > 0 then
    begin
      LinX2 := 'AND lo.Codigo=' + FormatFloat('0', QrConFatNum.Value);
    end;
  end;
  //
  if FFormCall = 1 then
  begin
    LinX3 := 'AND lo.TxCompra + lo.ValValorem + ' + IntToStr(FmPrincipal.FConnections) + ' >= 0.01';
    LinX4 := 'ORDER BY lo.Lote DESC';
    LinX5 := '';
  end else if FFormCall = 2 then
  begin
    LinX3 := 'AND lo.TxCompra + lo.ValValorem >= 0.01';
    LinX4 := 'AND lo.NF = 0';
    LinX5 := 'ORDER BY lo.Data';
  end else if FFormCall = 3 then
  begin
    LinX3 := 'AND lo.TxCompra + lo.ValValorem >= 0.01';
    LinX4 := 'AND lo.NF <> 0';
    LinX5 := 'ORDER BY lo.Data';
  end;
  //


{
  QrLoc.SQL.Clear;
  QrLoc.SQL.Add('SELECT CASE WHEN en.Tipo=0 THEN en.RazaoSocial');
  QrLoc.SQL.Add('ELSE en.Nome END NOMECLIENTE,');
  QrLoc.SQL.Add('CASE WHEN en.Tipo=0 THEN en.CNPJ');
  QrLoc.SQL.Add('ELSE en.CPF END CNPJCPF, lo.*');
  QrLoc.SQL.Add('FROM lot es lo');
  QrLoc.SQL.Add('LEFT JOIN entidades en ON en.Codigo=lo.Cliente');
  QrLoc.SQL.Add(dmkPF.SQL_Periodo('WHERE lo.Data ', TPIni.Date, TPFim.Date, True, True));
  QrLoc.SQL.Add(LinX1);
  QrLoc.SQL.Add(LinX2);
  QrLoc.SQL.Add(LinX3);
  QrLoc.SQL.Add(LinX4);
  QrLoc.SQL.Add(LinX5);
  QrLoc. Open;
}
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
  'SELECT CASE WHEN en.Tipo=0 THEN en.RazaoSocial',
  'ELSE en.Nome END NOMECLIENTE,',
  'CASE WHEN en.Tipo=0 THEN en.CNPJ',
  'ELSE en.CPF END CNPJCPF, lo.*',
  'FROM ' + CO_TabLotA + ' lo',
  'LEFT JOIN entidades en ON en.Codigo=lo.Cliente',
  dmkPF.SQL_Periodo('WHERE lo.Data ', TPIni.Date, TPFim.Date, True, True),
  LinX1,
  LinX2,
  LinX3,
  LinX4,
  LinX5,
  '']);
  //
  LocalizaBordero(EdLote.ValueVariant);
  LocalizaBordero(EdNF.ValueVariant);
end;

procedure TFmLot0Loc.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  //
  UMyMod.AbreQuery(QrClientes, Dmod.MyDB);
  TPIni.Date := Date - 365;
  TPFim.Date := Date;
end;

procedure TFmLot0Loc.EdLoteExit(Sender: TObject);
begin
  LocalizaBordero(EdLote.ValueVariant);
end;

procedure TFmLot0Loc.EdNFChange(Sender: TObject);
begin
  LocalizaBorderoNF(EdNF.ValueVariant);
  ReopenLoc();
end;

procedure TFmLot0Loc.EdNFExit(Sender: TObject);
begin
  LocalizaBorderoNF(EdNF.ValueVariant);
end;

procedure TFmLot0Loc.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLot0Loc.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLot0Loc.QrLocAfterOpen(DataSet: TDataSet);
begin
  BtConfirma.Enabled := QrLoc.RecordCount > 0;
end;

procedure TFmLot0Loc.LocalizaBordero(Lote: Integer);
begin
  if QrLoc.State = dsBrowse then
    if QrLoc.RecordCount > 0 then
      if Lote > 0 then QrLoc.Locate('Lote', Lote, []);
end;

procedure TFmLot0Loc.LocalizaBorderoNF(NF: Integer);
begin
  if QrLoc.State = dsBrowse then
    if QrLoc.RecordCount > 0 then
      if NF > 0 then QrLoc.Locate('NF', NF, []);
end;

procedure TFmLot0Loc.EdLoteChange(Sender: TObject);
begin
  LocalizaBordero(EdLote.ValueVariant);
  ReopenLoc();
end;

procedure TFmLot0Loc.DBGrid1DblClick(Sender: TObject);
begin
  BtConfirmaClick(Self);
end;

procedure TFmLot0Loc.QrLocCalcFields(DataSet: TDataSet);
begin
  case QrLocTipo.Value of
    0: QrLocNOMETIPO.Value := 'Cheques';
    1: QrLocNOMETIPO.Value := 'Duplicatas';
    else QrLocNOMETIPO.Value := '? ? ?';
  end;
end;

procedure TFmLot0Loc.TPIniClick(Sender: TObject);
begin
  ReopenLoc();
end;

procedure TFmLot0Loc.TPFimClick(Sender: TObject);
begin
  ReopenLoc();
end;

procedure TFmLot0Loc.EdControleChange(Sender: TObject);
begin
  ReopenLoc();
end;

end.

