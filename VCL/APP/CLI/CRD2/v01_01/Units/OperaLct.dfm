object FmOperaLct: TFmOperaLct
  Left = 419
  Top = 217
  Caption = 'OPE-GEREN-005 :: Evolu'#231#227'o das Opera'#231#245'es'
  ClientHeight = 597
  ClientWidth = 597
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 597
    Height = 129
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Panel4: TPanel
      Left = 0
      Top = 0
      Width = 597
      Height = 125
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label3: TLabel
        Left = 8
        Top = 44
        Width = 35
        Height = 13
        Caption = 'Cliente:'
      end
      object Label34: TLabel
        Left = 8
        Top = 84
        Width = 55
        Height = 13
        Caption = 'Data inicial:'
      end
      object Label1: TLabel
        Left = 124
        Top = 84
        Width = 48
        Height = 13
        Caption = 'Data final:'
      end
      object Label4: TLabel
        Left = 8
        Top = 4
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object EdCliente: TdmkEditCB
        Left = 8
        Top = 60
        Width = 57
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnChange = EdClienteChange
        DBLookupComboBox = CBCliente
        IgnoraDBLookupComboBox = False
      end
      object CBCliente: TdmkDBLookupComboBox
        Left = 68
        Top = 60
        Width = 521
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMECLIENTE'
        ListSource = DsClientes
        TabOrder = 3
        dmkEditCB = EdCliente
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object TPIni: TdmkEditDateTimePicker
        Left = 8
        Top = 100
        Width = 112
        Height = 21
        Date = 38675.714976851900000000
        Time = 38675.714976851900000000
        TabOrder = 4
        OnClick = TPIniClick
        OnChange = TPIniChange
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
      end
      object TPFim: TdmkEditDateTimePicker
        Left = 124
        Top = 100
        Width = 112
        Height = 21
        Date = 38675.714976851900000000
        Time = 38675.714976851900000000
        TabOrder = 5
        OnClick = TPFimClick
        OnChange = TPFimChange
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
      end
      object EdEmpresa: TdmkEditCB
        Left = 8
        Top = 20
        Width = 57
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnChange = EdEmpresaChange
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 68
        Top = 20
        Width = 521
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 1
        dmkEditCB = EdEmpresa
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 597
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 549
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 501
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 304
        Height = 32
        Caption = 'Evolu'#231#227'o das Opera'#231#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 304
        Height = 32
        Caption = 'Evolu'#231#227'o das Opera'#231#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 304
        Height = 32
        Caption = 'Evolu'#231#227'o das Opera'#231#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 471
    Width = 597
    Height = 56
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 593
      Height = 39
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 22
        Width = 593
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 527
    Width = 597
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 451
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BitBtn1: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sair'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel7: TPanel
      Left = 2
      Top = 15
      Width = 449
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 177
    Width = 597
    Height = 294
    ActivePage = TabSheet4
    Align = alClient
    TabOrder = 4
    object TabSheet4: TTabSheet
      Caption = ' Confer'#234'ncias '
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object PageControl2: TPageControl
        Left = 0
        Top = 157
        Width = 589
        Height = 109
        ActivePage = TabSheet1
        Align = alBottom
        TabOrder = 0
        object TabSheet1: TTabSheet
          Caption = ' Opera'#231#245'es '
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel1: TPanel
            Left = 0
            Top = 0
            Width = 581
            Height = 81
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object GroupBox1: TGroupBox
              Left = 0
              Top = 0
              Width = 581
              Height = 65
              Align = alTop
              Caption = ' Configura'#231#227'o da impress'#227'o: '
              TabOrder = 0
              object Label5: TLabel
                Left = 12
                Top = 20
                Width = 64
                Height = 13
                Caption = 'Zoom (em %):'
              end
              object Label6: TLabel
                Left = 80
                Top = 20
                Width = 30
                Height = 13
                Caption = 'Fonte:'
              end
              object EdPercZoom: TdmkEdit
                Left = 12
                Top = 36
                Width = 65
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '10'
                ValMax = '1000'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '69,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 69.000000000000000000
                OnChange = EdPercZoomChange
              end
              object EdTamFonte: TdmkEdit
                Left = 80
                Top = 36
                Width = 33
                Height = 21
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '1'
                ValMax = '144'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '7'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 7
              end
              object BtImprime1: TBitBtn
                Tag = 5
                Left = 456
                Top = 16
                Width = 120
                Height = 40
                Caption = '&Imprime'
                Enabled = False
                NumGlyphs = 2
                TabOrder = 2
                OnClick = BtImprime1Click
              end
              object CkDesign: TCheckBox
                Left = 124
                Top = 40
                Width = 157
                Height = 17
                Caption = 'Modo de desenho (sint'#233'tico).'
                TabOrder = 3
              end
            end
          end
        end
        object TabSheet2: TTabSheet
          Caption = ' Confer'#234'ncia de N'#237'vel 2 '
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel2: TPanel
            Left = 0
            Top = 0
            Width = 581
            Height = 81
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object GroupBox2: TGroupBox
              Left = 0
              Top = 0
              Width = 581
              Height = 81
              Align = alClient
              Caption = ' Configura'#231#227'o de impress'#227'o: '
              TabOrder = 0
              object Label2: TLabel
                Left = 12
                Top = 16
                Width = 195
                Height = 13
                Caption = 'Configura'#231#227'o do relat'#243'rio de confer'#234'ncia:'
              end
              object EdCtaFat2Ger: TdmkEditCB
                Left = 12
                Top = 32
                Width = 57
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                DBLookupComboBox = CBCtaFat2Ger
                IgnoraDBLookupComboBox = False
              end
              object CBCtaFat2Ger: TdmkDBLookupComboBox
                Left = 70
                Top = 32
                Width = 383
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsCtaFat2Ger
                TabOrder = 1
                dmkEditCB = EdCtaFat2Ger
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object CkSoErrConf: TCheckBox
                Left = 10
                Top = 56
                Width = 349
                Height = 17
                Caption = 'Imprimir somente itens com erro na confer'#234'ncia.'
                TabOrder = 2
              end
              object BtImprime2: TBitBtn
                Tag = 5
                Left = 456
                Top = 16
                Width = 120
                Height = 40
                Caption = '&Imprime'
                Enabled = False
                NumGlyphs = 2
                TabOrder = 3
                OnClick = BtImprime2Click
              end
            end
          end
        end
      end
      object GradeA: TDBGrid
        Left = 0
        Top = 48
        Width = 589
        Height = 109
        Align = alClient
        DataSource = DsNiv1
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
      object Panel5: TPanel
        Left = 0
        Top = 0
        Width = 589
        Height = 48
        Align = alTop
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 2
        object BtGera: TBitBtn
          Tag = 163
          Left = 240
          Top = 4
          Width = 120
          Height = 40
          Caption = '&Gera'
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BtGeraClick
        end
        object BtBordero: TBitBtn
          Tag = 116
          Left = 369
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Border'#244
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtBorderoClick
        end
        object RGOrdem: TRadioGroup
          Left = 4
          Top = 7
          Width = 229
          Height = 37
          Caption = ' Ordem da pesquisa: '
          Columns = 2
          ItemIndex = 0
          Items.Strings = (
            'Cliente, Border'#244
            'Lote')
          TabOrder = 2
          OnClick = RGOrdemClick
        end
      end
    end
    object TabSheet5: TTabSheet
      Caption = ' Pesquisas '
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object PageControl3: TPageControl
        Left = 0
        Top = 0
        Width = 589
        Height = 266
        ActivePage = TabSheet3
        Align = alClient
        TabOrder = 0
        object TabSheet3: TTabSheet
          Caption = ' Arrecada'#231#245'es em border'#244' '
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel3: TPanel
            Left = 0
            Top = 0
            Width = 581
            Height = 85
            Align = alTop
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object GroupBox3: TGroupBox
              Left = 0
              Top = 0
              Width = 581
              Height = 85
              Align = alClient
              Caption = ' Configura'#231#227'o de impress'#227'o: '
              TabOrder = 0
              object Label7: TLabel
                Left = 12
                Top = 16
                Width = 195
                Height = 13
                Caption = 'Configura'#231#227'o do relat'#243'rio de confer'#234'ncia:'
              end
              object EdCtaFat0Ger: TdmkEditCB
                Left = 12
                Top = 32
                Width = 57
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                OnChange = EdCtaFat0GerChange
                DBLookupComboBox = CBCtaFat0Ger
                IgnoraDBLookupComboBox = False
              end
              object CBCtaFat0Ger: TdmkDBLookupComboBox
                Left = 70
                Top = 32
                Width = 383
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsCtaFat0Ger
                TabOrder = 1
                dmkEditCB = EdCtaFat0Ger
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object BtImprime0: TBitBtn
                Tag = 5
                Left = 456
                Top = 16
                Width = 120
                Height = 40
                Caption = '&Imprime'
                Enabled = False
                NumGlyphs = 2
                TabOrder = 2
                OnClick = BtImprime0Click
              end
              object CkBorderos: TCheckBox
                Left = 16
                Top = 60
                Width = 425
                Height = 17
                Caption = 
                  'Considerar apenas lan'#231'amentos em borer'#244' (ser'#225' considerada a data' +
                  ' do border'#244')'
                TabOrder = 3
              end
            end
          end
          object DBGrid1: TDBGrid
            Left = 0
            Top = 85
            Width = 581
            Height = 153
            Align = alClient
            DataSource = DsArrec
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'FatID'
                Title.Caption = 'ID Fat.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_FatID'
                Title.Caption = 'Descri'#231#227'o'
                Width = 401
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Valor'
                Width = 75
                Visible = True
              end>
          end
        end
      end
    end
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMECLIENTE, Codigo'
      'FROM entidades'
      'WHERE Cliente1='#39'V'#39
      'ORDER BY NOMECLIENTE')
    Left = 288
    Top = 64
    object QrClientesNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 316
    Top = 64
  end
  object QrCtaFat0: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT FatID'
      'FROM contasfat0'
      'ORDER BY FatID')
    Left = 368
    Top = 4
    object QrCtaFat0FatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCtaFat0ChkFator: TFloatField
      FieldName = 'ChkFator'
    end
  end
  object QrCtaFat1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Sigla, Ordem'
      'FROM contasfat1'
      'ORDER BY Ordem')
    Left = 368
    Top = 32
    object QrCtaFat1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCtaFat1Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 10
    end
    object QrCtaFat1Ordem: TIntegerField
      FieldName = 'Ordem'
    end
  end
  object QrFlds1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT FatID'
      'FROM contasfat0'
      'WHERE FatNiv1=301')
    Left = 396
    Top = 32
    object QrFlds1FatID: TIntegerField
      FieldName = 'FatID'
    end
  end
  object QrNiv1: TmySQLQuery
    Database = DModG.MyPID_DB
    AfterOpen = QrNiv1AfterOpen
    BeforeClose = QrNiv1BeforeClose
    Left = 424
    Top = 32
  end
  object DsNiv1: TDataSource
    DataSet = QrNiv1
    Left = 452
    Top = 32
  end
  object QrPsq1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Sigla '
      'FROM ctafat1'
      'WHERE Codigo=301')
    Left = 460
    Top = 176
  end
  object QrCtaFat2Ger: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM ctafat2ger'
      'ORDER BY Nome')
    Left = 160
    Top = 300
    object QrCtaFat2GerCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCtaFat2GerNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsCtaFat2Ger: TDataSource
    DataSet = QrCtaFat2Ger
    Left = 188
    Top = 300
  end
  object QrCtaFat2Its: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT cf0.FatID, cfi.DC'
      'FROM ctafat2its cfi'
      'LEFT JOIN ctafat0 cf0 ON cf0.FatNiv2=cfi.FatNiv2'
      'WHERE cfi.Codigo=1'
      'ORDER BY DC, FatNiv1')
    Left = 368
    Top = 60
    object QrCtaFat2ItsFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCtaFat2ItsDC: TWideStringField
      FieldName = 'DC'
      Size = 1
    end
  end
  object QrConfere: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT FatNum, Lote, Data, CodCli, NomCli,'
      'SUM(+ Fld0301) Entradas, '
      'SUM(+ Fld0302 + Fld0371) Saidas, '
      'SUM(+ Fld0301 + Fld0302 + Fld0371) Erro'
      'FROM operalctcab'
      'GROUP BY FatNum, Lote, Data, CodCli'
      'ORDER BY NomCli, Data, Lote, FatNum')
    Left = 396
    Top = 60
    object QrConfereFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrConfereLote: TIntegerField
      FieldName = 'Lote'
    end
    object QrConfereData: TDateField
      FieldName = 'Data'
    end
    object QrConfereCodCli: TIntegerField
      FieldName = 'CodCli'
    end
    object QrConfereNomCli: TWideStringField
      FieldName = 'NomCli'
      Size = 100
    end
    object QrConfereEntradas: TFloatField
      FieldName = 'Entradas'
    end
    object QrConfereSaidas: TFloatField
      FieldName = 'Saidas'
    end
    object QrConfereErro: TFloatField
      FieldName = 'Erro'
    end
  end
  object frxConfere0: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38101.979964398100000000
    ReportOptions.LastChange = 39722.438154294000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxConfere0GetValue
    Left = 452
    Top = 60
    Datasets = <
      item
        DataSet = frxDsConfere
        DataSetName = 'frxDsConfere'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Columns = 1
      ColumnWidth = 190.000000000000000000
      ColumnPositions.Strings = (
        '0')
      object DadosMestre1: TfrxMasterData
        Height = 17.007874020000000000
        Top = 226.771800000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsConfere
        DataSetName = 'frxDsConfere'
        RowCount = 0
        object Memo1: TfrxMemoView
          Left = -0.000048820000000000
          Width = 113.385851180000000000
          Height = 17.007874020000000000
          ShowHint = False
          DataField = 'Data'
          DataSet = frxDsConfere
          DataSetName = 'frxDsConfere'
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsConfere."Data"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 113.385802360000000000
          Width = 94.488188980000000000
          Height = 17.007874020000000000
          ShowHint = False
          DataField = 'FatNum'
          DataSet = frxDsConfere
          DataSetName = 'frxDsConfere'
          DisplayFormat.DecimalSeparator = '.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsConfere."FatNum"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 347.716535430000000000
          Width = 94.488188980000000000
          Height = 17.007874020000000000
          ShowHint = False
          DataSet = frxDsConfere
          DataSetName = 'frxDsConfere'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsConfere."Entradas"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 442.204724410000000000
          Width = 94.488188980000000000
          Height = 17.007874020000000000
          ShowHint = False
          DataField = 'Saidas'
          DataSet = frxDsConfere
          DataSetName = 'frxDsConfere'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsConfere."Saidas"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 585.826771650000000000
          Width = 94.488188980000000000
          Height = 17.007874020000000000
          ShowHint = False
          DataSet = frxDsConfere
          DataSetName = 'frxDsConfere'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsConfere."Erro"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 207.874150000000000000
          Width = 94.488188980000000000
          Height = 17.007874020000000000
          ShowHint = False
          DataField = 'Lote'
          DataSet = frxDsConfere
          DataSetName = 'frxDsConfere'
          DisplayFormat.DecimalSeparator = '.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsConfere."Lote"]')
          ParentFont = False
        end
      end
      object TfrxGroupHeader
        Height = 39.685039370000000000
        Top = 162.519790000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsConfere."CodCli"'
        object Memo10: TfrxMemoView
          Left = -0.000048820000000000
          Top = 22.677165350000000000
          Width = 113.385851180000000000
          Height = 17.007874020000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 113.385802360000000000
          Top = 22.677165350000000000
          Width = 94.488188980000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.000000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Lote')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 347.826840000000000000
          Top = 22.677165350000000000
          Width = 94.488188980000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Entradas')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 442.204717090000000000
          Top = 22.677165350000000000
          Width = 94.488188980000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Sa'#237'das')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 585.826808270000000000
          Top = 22.677165350000000000
          Width = 94.488188980000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Diferen'#231'a')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = -0.000048820000000000
          Width = 680.314960630000000000
          Height = 20.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsConfere."CodCli"] - [frxDsConfere."NomCli"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 207.874150000000000000
          Top = 22.677165350000000000
          Width = 94.488188980000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.000000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Border'#244)
          ParentFont = False
        end
      end
      object TfrxGroupFooter
        Height = 20.787401580000000000
        Top = 268.346630000000000000
        Width = 680.315400000000000000
        object Memo8: TfrxMemoView
          Left = 347.716535430000000000
          Top = 3.779527560000000000
          Width = 94.488188980000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsConfere."Entradas">)]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 442.204724410000000000
          Top = 3.779527560000000000
          Width = 94.488188980000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsConfere."Saidas">)]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 585.826771650000000000
          Top = 3.779527560000000000
          Width = 94.488188980000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsConfere."Erro">)]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Top = 3.779527560000000000
          Width = 339.685220000000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Total per'#237'odo: [frxDsConfere."NomCli"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageHeader4: TfrxPageHeader
        Height = 83.149660000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 120.944960000000000000
          Top = 18.897650000000000000
          Width = 438.425480000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Relat'#243'rio de Confer'#234'ncia de Opera'#231#245'es')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 113.385836540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 559.370440000000000000
          Top = 18.897650000000000000
          Width = 113.385836540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Cliente: [VARF_CLIENTE]')
          ParentFont = False
        end
        object Memo86: TfrxMemoView
          Top = 64.252010000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo: [VARF_PERIODO]')
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        Height = 15.118110240000000000
        Top = 396.850650000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        Height = 22.677180000000000000
        Top = 351.496290000000000000
        Width = 680.315400000000000000
        object Memo4: TfrxMemoView
          Left = 347.716535430000000000
          Top = 3.779527560000000000
          Width = 94.488188980000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsConfere."Entradas">)]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 442.204724410000000000
          Top = 3.779527560000000000
          Width = 94.488188980000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsConfere."Saidas">)]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 585.826771650000000000
          Top = 3.779527560000000000
          Width = 94.488188980000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsConfere."Erro">)]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Top = 3.779530000000000000
          Width = 339.685220000000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Total geral per'#237'odo:')
          ParentFont = False
          WordWrap = False
        end
      end
    end
  end
  object frxDsConfere: TfrxDBDataset
    UserName = 'frxDsConfere'
    CloseDataSource = False
    FieldAliases.Strings = (
      'FatNum=FatNum'
      'Lote=Lote'
      'Data=Data'
      'CodCli=CodCli'
      'NomCli=NomCli'
      'Entradas=Entradas'
      'Saidas=Saidas'
      'Erro=Erro')
    DataSet = QrConfere
    BCDToCurrency = False
    Left = 424
    Top = 60
  end
  object frxSintetico: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39873.799550636580000000
    ReportOptions.LastChange = 39873.799550636580000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxConfere0GetValue
    Left = 396
    Top = 88
    Datasets = <
      item
        DataSet = frxDsNiv1
        DataSetName = 'frxDsNiv1'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
    end
  end
  object frxDsNiv1: TfrxDBDataset
    UserName = 'frxDsNiv1'
    CloseDataSource = False
    DataSet = QrNiv1
    BCDToCurrency = False
    Left = 424
    Top = 88
  end
  object frxConfere1: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38101.979964398100000000
    ReportOptions.LastChange = 39722.438154294000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxConfere0GetValue
    Left = 480
    Top = 60
    Datasets = <
      item
        DataSet = frxDsConfere
        DataSetName = 'frxDsConfere'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Columns = 1
      ColumnWidth = 190.000000000000000000
      ColumnPositions.Strings = (
        '0')
      object DadosMestre1: TfrxMasterData
        Height = 17.007874020000000000
        Top = 188.976500000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsConfere
        DataSetName = 'frxDsConfere'
        RowCount = 0
        object Memo1: TfrxMemoView
          Left = -0.000048820000000000
          Width = 49.133858267716540000
          Height = 17.007874020000000000
          ShowHint = False
          DataField = 'Data'
          DataSet = frxDsConfere
          DataSetName = 'frxDsConfere'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsConfere."Data"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 49.133792360000000000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          ShowHint = False
          DataField = 'FatNum'
          DataSet = frxDsConfere
          DataSetName = 'frxDsConfere'
          DisplayFormat.DecimalSeparator = '.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsConfere."FatNum"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 408.189015430000000000
          Width = 90.708661420000000000
          Height = 17.007874020000000000
          ShowHint = False
          DataSet = frxDsConfere
          DataSetName = 'frxDsConfere'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsConfere."Entradas"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 498.897674410000000000
          Width = 90.708661420000000000
          Height = 17.007874020000000000
          ShowHint = False
          DataField = 'Saidas'
          DataSet = frxDsConfere
          DataSetName = 'frxDsConfere'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsConfere."Saidas"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 589.606301650000000000
          Width = 90.708661420000000000
          Height = 17.007874020000000000
          ShowHint = False
          DataSet = frxDsConfere
          DataSetName = 'frxDsConfere'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsConfere."Erro"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 109.606370000000000000
          Width = 49.133858267716540000
          Height = 17.007874020000000000
          ShowHint = False
          DataField = 'Lote'
          DataSet = frxDsConfere
          DataSetName = 'frxDsConfere'
          DisplayFormat.DecimalSeparator = '.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsConfere."Lote"]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 158.740260000000000000
          Width = 41.574768980000000000
          Height = 17.007874020000000000
          ShowHint = False
          DataField = 'CodCli'
          DataSet = frxDsConfere
          DataSetName = 'frxDsConfere'
          DisplayFormat.DecimalSeparator = '.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsConfere."CodCli"]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 200.315090000000000000
          Width = 207.874088980000000000
          Height = 17.007874020000000000
          ShowHint = False
          DataSet = frxDsConfere
          DataSetName = 'frxDsConfere'
          DisplayFormat.DecimalSeparator = '.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsConfere."NomCli"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageHeader4: TfrxPageHeader
        Height = 107.716608670000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 120.944960000000000000
          Top = 18.897650000000000000
          Width = 438.425480000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Relat'#243'rio de Confer'#234'ncia de Opera'#231#245'es')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 113.385836540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 559.370440000000000000
          Top = 18.897650000000000000
          Width = 113.385836540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Cliente: [VARF_CLIENTE]')
          ParentFont = False
        end
        object Memo86: TfrxMemoView
          Top = 64.252010000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo: [VARF_PERIODO]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Top = 90.708720000000000000
          Width = 49.133858267716540000
          Height = 17.007874020000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 49.133841180000000000
          Top = 90.708720000000000000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.000000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Lote')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 408.299368820000000000
          Top = 90.708720000000000000
          Width = 90.708661420000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Entradas')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 498.897715910000000000
          Top = 90.708720000000000000
          Width = 90.708661420000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Sa'#237'das')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 589.606387090000000000
          Top = 90.708720000000000000
          Width = 90.708661420000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Diferen'#231'a')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 109.606418820000000000
          Top = 90.708720000000000000
          Width = 49.133858267716540000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.000000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Border'#244)
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 158.740308820000000000
          Top = 90.708734650000000000
          Width = 249.448918980000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.000000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Cliente')
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        Height = 15.118110240000000000
        Top = 313.700990000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        Height = 22.677180000000000000
        Top = 268.346630000000000000
        Width = 680.315400000000000000
        object Memo4: TfrxMemoView
          Left = 408.189015430000000000
          Top = 3.779527560000000000
          Width = 90.708661420000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsConfere."Entradas">)]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 498.897674410000000000
          Top = 3.779527560000000000
          Width = 90.708661420000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsConfere."Saidas">)]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 589.606301650000000000
          Top = 3.779527560000000000
          Width = 90.708661420000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsConfere."Erro">)]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Top = 3.779530000000000000
          Width = 124.252010000000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Total geral per'#237'odo:')
          ParentFont = False
          WordWrap = False
        end
      end
    end
  end
  object QrCtaFat0Ger: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM ctafat0ger'
      'ORDER BY Nome')
    Left = 320
    Top = 300
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsCtaFat0Ger: TDataSource
    DataSet = QrCtaFat0Ger
    Left = 348
    Top = 300
  end
  object QrArrec: TmySQLQuery
    Database = Dmod.MyDB
    Left = 320
    Top = 328
    object QrArrecNO_FatID: TWideStringField
      FieldName = 'NO_FatID'
      Size = 60
    end
    object QrArrecFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrArrecValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsArrec: TDataSource
    DataSet = QrArrec
    Left = 348
    Top = 328
  end
  object frxDsArrec: TfrxDBDataset
    UserName = 'frxDsArrec'
    CloseDataSource = False
    DataSet = QrArrec
    BCDToCurrency = False
    Left = 376
    Top = 328
  end
  object frxArrec: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38101.979964398100000000
    ReportOptions.LastChange = 39722.438154294000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxConfere0GetValue
    Left = 404
    Top = 328
    Datasets = <
      item
        DataSet = frxDsArrec
        DataSetName = 'frxDsArrec'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Columns = 1
      ColumnWidth = 190.000000000000000000
      ColumnPositions.Strings = (
        '0')
      object DadosMestre1: TfrxMasterData
        Height = 17.007874020000000000
        Top = 181.417440000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsArrec
        DataSetName = 'frxDsArrec'
        RowCount = 0
        object Memo7: TfrxMemoView
          Left = 529.133821650000000000
          Width = 151.181138980000000000
          Height = 17.007874020000000000
          ShowHint = False
          DataSet = frxDsArrec
          DataSetName = 'frxDsArrec'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsArrec."Valor"]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Width = 56.692950000000000000
          Height = 17.007874020000000000
          ShowHint = False
          DataField = 'FatID'
          DataSet = frxDsArrec
          DataSetName = 'frxDsArrec'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsArrec."FatID"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 56.692950000000000000
          Width = 472.441250000000000000
          Height = 17.007874020000000000
          ShowHint = False
          DataField = 'NO_FatID'
          DataSet = frxDsArrec
          DataSetName = 'frxDsArrec'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsArrec."NO_FatID"]')
          ParentFont = False
        end
      end
      object PageHeader4: TfrxPageHeader
        Height = 100.157534020000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 120.944960000000000000
          Top = 18.897650000000000000
          Width = 438.425480000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Relat'#243'rio de Arrecada'#231#245'es em Border'#244)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 113.385836540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 559.370440000000000000
          Top = 18.897650000000000000
          Width = 113.385836540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Cliente: [VARF_CLIENTE]')
          ParentFont = False
        end
        object Memo86: TfrxMemoView
          Top = 64.252010000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo: [VARF_PERIODO]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 529.133821650000000000
          Top = 83.149660000000000000
          Width = 151.181138980000000000
          Height = 17.007874020000000000
          ShowHint = False
          DataSet = frxDsConfere
          DataSetName = 'frxDsConfere'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Top = 83.149660000000000000
          Width = 56.692950000000000000
          Height = 17.007874020000000000
          ShowHint = False
          DataSet = frxDsArrec
          DataSetName = 'frxDsArrec'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ID Fat.')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 56.692950000000000000
          Top = 83.149660000000000000
          Width = 472.441250000000000000
          Height = 17.007874020000000000
          ShowHint = False
          DataSet = frxDsArrec
          DataSetName = 'frxDsArrec'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        Height = 15.118110240000000000
        Top = 306.141930000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        Height = 22.677180000000000000
        Top = 260.787570000000000000
        Width = 680.315400000000000000
        object Memo20: TfrxMemoView
          Left = 529.133821650000000000
          Top = 3.779527560000000000
          Width = 151.181138980000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsArrec."Valor">)]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Top = 3.779530000000000000
          Width = 528.661720000000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Total geral per'#237'odo:')
          ParentFont = False
          WordWrap = False
        end
      end
    end
  end
end
