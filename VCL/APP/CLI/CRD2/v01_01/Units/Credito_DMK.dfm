object FmCredito_DMK: TFmCredito_DMK
  Left = 399
  Top = 261
  Caption = 'Credito2'
  ClientHeight = 267
  ClientWidth = 486
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnHide = FormHide
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 136
    Width = 486
    Height = 61
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 0
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 482
      Height = 44
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object ProgressBar1: TProgressBar
        Left = 12
        Top = 24
        Width = 457
        Height = 17
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 197
    Width = 486
    Height = 70
    Align = alBottom
    TabOrder = 1
    object PnSaiDesis: TPanel
      Left = 340
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtDesiste: TBitBtn
        Tag = 15
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtDesisteClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 338
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtEntra: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Confirma'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtEntraClick
      end
      object CkNaoTemInternet: TCheckBox
        Left = 148
        Top = 16
        Width = 181
        Height = 17
        Caption = 'N'#227'o estou conectado na internet.'
        TabOrder = 1
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 0
    Width = 486
    Height = 136
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 412
      Height = 136
      Align = alClient
      Caption = ' Defini'#231#227'o do perfil: '
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 32
        Width = 29
        Height = 13
        Caption = 'Login:'
      end
      object Label2: TLabel
        Left = 8
        Top = 64
        Width = 34
        Height = 13
        Caption = 'Senha:'
      end
      object Label4: TLabel
        Left = 8
        Top = 92
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object LaSenhas: TLabel
        Left = 60
        Top = 115
        Width = 80
        Height = 13
        Cursor = crHandPoint
        Caption = 'Gerenciar Senha'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        OnClick = LaSenhasClick
        OnMouseEnter = LaSenhasMouseEnter
        OnMouseLeave = LaSenhasMouseLeave
      end
      object LaConexao: TLabel
        Left = 304
        Top = 115
        Width = 96
        Height = 13
        Cursor = crHandPoint
        Caption = 'Gerenciar Conex'#245'es'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        OnClick = LaConexaoClick
        OnMouseEnter = LaConexaoMouseEnter
        OnMouseLeave = LaConexaoMouseLeave
      end
      object EdLogin: TEdit
        Left = 60
        Top = 28
        Width = 340
        Height = 20
        Font.Charset = SYMBOL_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Wingdings'
        Font.Style = []
        ParentFont = False
        PasswordChar = 'l'
        TabOrder = 0
        OnKeyDown = EdLoginKeyDown
      end
      object EdSenha: TEdit
        Left = 60
        Top = 60
        Width = 340
        Height = 20
        Font.Charset = SYMBOL_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Wingdings'
        Font.Style = []
        ParentFont = False
        PasswordChar = 'l'
        TabOrder = 1
        OnExit = EdSenhaExit
        OnKeyDown = EdSenhaKeyDown
      end
      object EdEmpresa: TEdit
        Left = 60
        Top = 92
        Width = 340
        Height = 20
        Font.Charset = SYMBOL_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Wingdings'
        Font.Style = []
        ParentFont = False
        PasswordChar = 'l'
        TabOrder = 2
        OnExit = EdSenhaExit
        OnKeyDown = EdSenhaKeyDown
      end
    end
    object GroupBox2: TGroupBox
      Left = 412
      Top = 0
      Width = 74
      Height = 136
      Align = alRight
      Caption = ' Coligadas: '
      TabOrder = 1
      object LaTitulo1A: TLabel
        Left = 10
        Top = 13
        Width = 53
        Height = 107
        Caption = '0'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = 5863167
        Font.Height = -96
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 10
        Top = 15
        Width = 53
        Height = 107
        Caption = '0'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -96
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 14
        Width = 53
        Height = 107
        Caption = '0'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -96
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 144
    Top = 152
    object Close1: TMenuItem
      Caption = '&Close'
      OnClick = Close1Click
    end
  end
end
