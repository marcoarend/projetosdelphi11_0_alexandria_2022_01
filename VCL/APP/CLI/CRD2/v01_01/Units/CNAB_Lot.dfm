object FmCNAB_Lot: TFmCNAB_Lot
  Left = 256
  Top = 161
  Caption = 'FBB-CNABC-005 :: Remessa CNAB'
  ClientHeight = 593
  ClientWidth = 1016
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 96
    Width = 1016
    Height = 497
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    Visible = False
    object GroupBox3: TGroupBox
      Left = 0
      Top = 434
      Width = 1016
      Height = 63
      Align = alBottom
      TabOrder = 0
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel4: TPanel
        Left = 876
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1016
      Height = 145
      Align = alTop
      TabOrder = 1
      object Label9: TLabel
        Left = 12
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label3: TLabel
        Left = 88
        Top = 16
        Width = 26
        Height = 13
        Caption = 'Data:'
      end
      object Label7: TLabel
        Left = 356
        Top = 16
        Width = 129
        Height = 13
        Caption = 'Configura'#231#227'o de cobran'#231'a:'
      end
      object Label11: TLabel
        Left = 208
        Top = 16
        Width = 26
        Height = 13
        Caption = 'Hora:'
      end
      object Label13: TLabel
        Left = 12
        Top = 56
        Width = 64
        Height = 13
        Caption = 'Mensagem 1:'
      end
      object Label14: TLabel
        Left = 336
        Top = 56
        Width = 64
        Height = 13
        Caption = 'Mensagem 2:'
      end
      object Label15: TLabel
        Left = 344
        Top = 116
        Width = 347
        Height = 13
        Caption = 
          'As mensagens n'#227'o podem conter caracteres especiais, acentuados, ' +
          'etc. '
      end
      object Label20: TLabel
        Left = 280
        Top = 16
        Width = 71
        Height = 13
        Caption = 'Lote Remessa:'
      end
      object Label23: TLabel
        Left = 12
        Top = 96
        Width = 215
        Height = 13
        Caption = 'CNAB 240 Registro 1 posi'#231#245'es de 208 a 240:'
      end
      object EdCodigo: TdmkEdit
        Left = 12
        Top = 32
        Width = 73
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object TPDataG: TdmkEditDateTimePicker
        Left = 88
        Top = 32
        Width = 115
        Height = 21
        Date = 39067.000000000000000000
        Time = 0.403862476901849700
        TabOrder = 1
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdCNAB_Cfg: TdmkEditCB
        Left = 356
        Top = 32
        Width = 65
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCNAB_Cfg
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCNAB_Cfg: TdmkDBLookupComboBox
        Left = 424
        Top = 32
        Width = 313
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsCNAB_Cfg
        TabOrder = 5
        dmkEditCB = EdCNAB_Cfg
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object TPHoraG: TDateTimePicker
        Left = 208
        Top = 32
        Width = 69
        Height = 21
        Date = 39198.000000000000000000
        Time = 39198.000000000000000000
        Kind = dtkTime
        TabOrder = 2
      end
      object EdMensagem1: TdmkEdit
        Left = 12
        Top = 72
        Width = 320
        Height = 21
        CharCase = ecUpperCase
        MaxLength = 40
        TabOrder = 6
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdMensagem2: TdmkEdit
        Left = 336
        Top = 72
        Width = 320
        Height = 21
        CharCase = ecUpperCase
        MaxLength = 40
        TabOrder = 7
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCodUsu: TdmkEdit
        Left = 280
        Top = 32
        Width = 72
        Height = 21
        Alignment = taRightJustify
        MaxLength = 8
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object Ed_2401_208: TdmkEdit
        Left = 12
        Top = 112
        Width = 280
        Height = 21
        CharCase = ecUpperCase
        MaxLength = 40
        TabOrder = 8
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 96
    Width = 1016
    Height = 497
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    object Splitter1: TSplitter
      Left = 0
      Top = 320
      Width = 1016
      Height = 10
      Cursor = crVSplit
      Align = alBottom
      ExplicitTop = 330
    end
    object Memo1: TMemo
      Left = 0
      Top = 330
      Width = 1016
      Height = 103
      Align = alBottom
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Courier New'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      WordWrap = False
      OnChange = Memo1Change
    end
    object GroupBox4: TGroupBox
      Left = 0
      Top = 0
      Width = 1016
      Height = 137
      Align = alTop
      TabOrder = 1
      object Panel1: TPanel
        Left = 2
        Top = 15
        Width = 667
        Height = 120
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 12
          Top = 0
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          FocusControl = DBEdCodigo
        end
        object Label8: TLabel
          Left = 116
          Top = 0
          Width = 129
          Height = 13
          Caption = 'Configura'#231#227'o de cobran'#231'a:'
          FocusControl = DBEdit3
        end
        object Label21: TLabel
          Left = 592
          Top = 0
          Width = 24
          Height = 13
          Caption = 'Lote:'
          FocusControl = DBEdit9
        end
        object Label16: TLabel
          Left = 12
          Top = 40
          Width = 64
          Height = 13
          Caption = 'Mensagem 1:'
          FocusControl = DBEdit5
        end
        object Label17: TLabel
          Left = 336
          Top = 40
          Width = 64
          Height = 13
          Caption = 'Mensagem 2:'
          FocusControl = DBEdit6
        end
        object Label22: TLabel
          Left = 12
          Top = 80
          Width = 215
          Height = 13
          Caption = 'CNAB 240 Registro 1 posi'#231#245'es de 208 a 240:'
          FocusControl = DBEdit10
        end
        object DBEdCodigo: TDBEdit
          Left = 12
          Top = 16
          Width = 100
          Height = 21
          Hint = 'N'#186' do banco'
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsCNAB_Lot
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8281908
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
        end
        object DBEdit3: TDBEdit
          Left = 116
          Top = 16
          Width = 473
          Height = 21
          DataField = 'NOMECONFIG'
          DataSource = DsCNAB_Lot
          TabOrder = 1
        end
        object DBEdit9: TDBEdit
          Left = 592
          Top = 16
          Width = 64
          Height = 21
          DataField = 'CodUsu'
          DataSource = DsCNAB_Lot
          TabOrder = 2
        end
        object DBEdit5: TDBEdit
          Left = 12
          Top = 56
          Width = 320
          Height = 21
          DataField = 'Mensagem1'
          DataSource = DsCNAB_Lot
          TabOrder = 3
        end
        object DBEdit6: TDBEdit
          Left = 336
          Top = 56
          Width = 320
          Height = 21
          DataField = 'Mensagem2'
          DataSource = DsCNAB_Lot
          TabOrder = 4
        end
        object DBEdit10: TDBEdit
          Left = 12
          Top = 95
          Width = 280
          Height = 21
          DataField = '_2401_208'
          DataSource = DsCNAB_Lot
          TabOrder = 5
        end
      end
      object Panel9: TPanel
        Left = 669
        Top = 15
        Width = 345
        Height = 120
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object GroupBox2: TGroupBox
          Left = 0
          Top = 0
          Width = 148
          Height = 120
          Align = alLeft
          Caption = ' Cria'#231#227'o: '
          TabOrder = 0
          object Label18: TLabel
            Left = 8
            Top = 16
            Width = 26
            Height = 13
            Caption = 'Data:'
            FocusControl = DBEdit7
          end
          object Label19: TLabel
            Left = 84
            Top = 16
            Width = 26
            Height = 13
            Caption = 'Hora:'
            FocusControl = DBEdit8
          end
          object DBEdit7: TDBEdit
            Left = 8
            Top = 32
            Width = 72
            Height = 21
            DataField = 'MyDATAG'
            DataSource = DsCNAB_Lot
            TabOrder = 0
          end
          object DBEdit8: TDBEdit
            Left = 84
            Top = 32
            Width = 56
            Height = 21
            DataField = 'HoraG'
            DataSource = DsCNAB_Lot
            TabOrder = 1
          end
        end
        object GroupBox1: TGroupBox
          Left = 148
          Top = 0
          Width = 148
          Height = 120
          Align = alLeft
          Caption = ' '#218'ltimo envio: '
          TabOrder = 1
          object Label2: TLabel
            Left = 8
            Top = 16
            Width = 26
            Height = 13
            Caption = 'Data:'
            FocusControl = DBEdit1
          end
          object Label12: TLabel
            Left = 84
            Top = 16
            Width = 26
            Height = 13
            Caption = 'Hora:'
            FocusControl = DBEdit4
          end
          object DBEdit1: TDBEdit
            Left = 8
            Top = 32
            Width = 72
            Height = 21
            DataField = 'MyDATAS'
            DataSource = DsCNAB_Lot
            TabOrder = 0
          end
          object DBEdit4: TDBEdit
            Left = 84
            Top = 32
            Width = 56
            Height = 21
            DataField = 'HoraS'
            DataSource = DsCNAB_Lot
            TabOrder = 1
          end
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 433
      Width = 1016
      Height = 64
      Align = alBottom
      TabOrder = 2
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 1
      end
      object Panel3: TPanel
        Left = 404
        Top = 15
        Width = 610
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 2
        object Label10: TLabel
          Left = 400
          Top = 4
          Width = 31
          Height = 13
          Caption = 'Linhas'
        end
        object BtGera: TBitBtn
          Tag = 266
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = 'CNAB'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtGeraClick
        end
        object BtTitulos: TBitBtn
          Tag = 11
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&T'#237'tulos'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtTitulosClick
        end
        object BtLot: TBitBtn
          Tag = 265
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Lote'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtLotClick
        end
        object Edit1: TEdit
          Left = 400
          Top = 20
          Width = 69
          Height = 21
          TabOrder = 3
          Text = '0'
        end
        object Panel6: TPanel
          Left = 477
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 4
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 137
      Width = 1016
      Height = 152
      ActivePage = TabSheet1
      Align = alTop
      TabOrder = 3
      object TabSheet1: TTabSheet
        Caption = 'Itens'
        object DBGrid1: TdmkDBGrid
          Left = 0
          Top = 0
          Width = 1008
          Height = 124
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'NOMECLI'
              Title.Caption = 'Cliente'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Lote'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Emitente'
              Title.Caption = 'Sacado'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CNPJCPF'
              Width = 113
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Bruto'
              Width = 66
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Desco'
              Title.Caption = 'Desconto'
              Width = 62
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Credito'
              Width = 66
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DCompra'
              Title.Caption = 'D. Compra'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Vencimento'
              Title.Caption = 'Vencto.'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DDeposito'
              Title.Caption = 'Deposito'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FatParcela'
              Visible = True
            end>
          Color = clWindow
          DataSource = DsCNABLotIts
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'NOMECLI'
              Title.Caption = 'Cliente'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Lote'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Emitente'
              Title.Caption = 'Sacado'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CNPJCPF'
              Width = 113
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Bruto'
              Width = 66
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Desco'
              Title.Caption = 'Desconto'
              Width = 62
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Credito'
              Width = 66
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DCompra'
              Title.Caption = 'D. Compra'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Vencimento'
              Title.Caption = 'Vencto.'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DDeposito'
              Title.Caption = 'Deposito'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FatParcela'
              Visible = True
            end>
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Lan'#231'amentos'
        ImageIndex = 1
        object DBGDespProf: TDBGrid
          Left = 0
          Top = 46
          Width = 1008
          Height = 78
          Align = alClient
          DataSource = DsLcts
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'SEQ'
              Title.Caption = 'N'#186
              Width = 22
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Data'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Vencimento'
              Title.Caption = 'Vencto'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Credito'
              Title.Alignment = taRightJustify
              Title.Caption = 'Cr'#233'dito'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Debito'
              Title.Alignment = taRightJustify
              Title.Caption = 'D'#233'bito'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Descri'#231#227'o'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECARTEIRA'
              Title.Caption = 'Carteira'
              Width = 175
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECONTA'
              Title.Caption = 'Conta (Plano de contas)'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'ID'
              Visible = True
            end>
        end
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 1008
          Height = 46
          Align = alTop
          BevelOuter = bvNone
          ParentColor = True
          TabOrder = 1
          object BtExclui: TBitBtn
            Tag = 12
            Left = 96
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Exclui'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BtExcluiClick
          end
          object BtInclui: TBitBtn
            Tag = 10
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Inclui'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtIncluiClick
          end
          object Panel8: TPanel
            Left = 899
            Top = 0
            Width = 109
            Height = 46
            Align = alRight
            Alignment = taRightJustify
            BevelOuter = bvNone
            TabOrder = 2
          end
        end
      end
    end
  end
  object PnTitulos: TPanel
    Left = 0
    Top = 96
    Width = 1016
    Height = 497
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 2
    Visible = False
    object DBGrid2: TdmkDBGrid
      Left = 0
      Top = 119
      Width = 1016
      Height = 120
      Align = alTop
      Columns = <
        item
          Expanded = False
          FieldName = 'Duplicata'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECLI'
          Title.Caption = 'Cliente'
          Width = 189
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Lote'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Emitente'
          Width = 181
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CNPJCPF'
          Width = 113
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Bruto'
          Width = 66
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Desco'
          Title.Caption = 'Desconto'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Credito'
          Width = 66
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DCompra'
          Title.Caption = 'D. Compra'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Vencimento'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DDeposito'
          Title.Caption = 'D. Pagto'
          Width = 56
          Visible = True
        end>
      Color = clWindow
      DataSource = DsTitulos
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Duplicata'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECLI'
          Title.Caption = 'Cliente'
          Width = 189
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Lote'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Emitente'
          Width = 181
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CNPJCPF'
          Width = 113
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Bruto'
          Width = 66
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Desco'
          Title.Caption = 'Desconto'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Credito'
          Width = 66
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DCompra'
          Title.Caption = 'D. Compra'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Vencimento'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DDeposito'
          Title.Caption = 'D. Pagto'
          Width = 56
          Visible = True
        end>
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 434
      Width = 1016
      Height = 63
      Align = alBottom
      TabOrder = 1
      object Panel2: TPanel
        Left = 876
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtDesiste3: TBitBtn
          Tag = 13
          Left = 8
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sair'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesiste3Click
        end
      end
      object BtAdiciona: TBitBtn
        Tag = 14
        Left = 8
        Top = 15
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Adiciona'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtAdicionaClick
      end
    end
    object GBTitulos: TGroupBox
      Left = 0
      Top = 0
      Width = 1016
      Height = 69
      Align = alTop
      TabOrder = 2
      object Label4: TLabel
        Left = 16
        Top = 20
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdit01
      end
      object Label5: TLabel
        Left = 120
        Top = 20
        Width = 26
        Height = 13
        Caption = 'Data:'
        FocusControl = DBEdit2
      end
      object Label6: TLabel
        Left = 224
        Top = 40
        Width = 229
        Height = 13
        Caption = 'Escolha um ou mais t'#237'tulos e clique em adiciona.'
      end
      object DBEdit01: TDBEdit
        Left = 16
        Top = 36
        Width = 100
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsCNAB_Lot
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdit2: TDBEdit
        Left = 120
        Top = 36
        Width = 97
        Height = 21
        DataField = 'MyDATAG'
        DataSource = DsCNAB_Lot
        TabOrder = 1
      end
    end
    object PnPesquisa: TPanel
      Left = 0
      Top = 69
      Width = 1016
      Height = 50
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 3
      object Label55: TLabel
        Left = 16
        Top = 21
        Width = 68
        Height = 13
        Caption = 'ID do border'#244':'
      end
      object Label24: TLabel
        Left = 290
        Top = 21
        Width = 187
        Height = 13
        Caption = 'Preencha o valor 0 (zero) para nenhum.'
      end
      object EdBordero: TdmkEdit
        Left = 92
        Top = 15
        Width = 97
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInt64
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object BtReabre: TBitBtn
        Tag = 18
        Left = 195
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Reabre'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtReabreClick
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1016
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 968
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 752
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 198
        Height = 32
        Caption = 'Remessa CNAB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 198
        Height = 32
        Caption = 'Remessa CNAB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 198
        Height = 32
        Caption = 'Remessa CNAB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1016
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel10: TPanel
      Left = 2
      Top = 15
      Width = 1012
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsCNAB_Lot: TDataSource
    DataSet = QrCNAB_Lot
    Left = 464
    Top = 12
  end
  object QrCNAB_Lot: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrCNAB_LotBeforeOpen
    AfterOpen = QrCNAB_LotAfterOpen
    BeforeClose = QrCNAB_LotBeforeClose
    AfterScroll = QrCNAB_LotAfterScroll
    OnCalcFields = QrCNAB_LotCalcFields
    SQL.Strings = (
      'SELECT con.Nome NOMECONFIG, cob.* '
      'FROM cnab_lot cob'
      'LEFT JOIN cnab_cfg con ON con.Codigo=cob.CNAB_Cfg'
      'WHERE cob.Codigo > 0')
    Left = 436
    Top = 12
    object QrCNAB_LotMyDATAG: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MyDATAG'
      Size = 10
      Calculated = True
    end
    object QrCNAB_LotMyDATAS: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MyDATAS'
      Size = 10
      Calculated = True
    end
    object QrCNAB_LotHoraS: TTimeField
      FieldName = 'HoraS'
      Required = True
    end
    object QrCNAB_LotHoraG: TTimeField
      FieldName = 'HoraG'
      Required = True
    end
    object QrCNAB_LotNOMECONFIG: TWideStringField
      FieldName = 'NOMECONFIG'
      Size = 50
    end
    object QrCNAB_LotCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCNAB_LotDataG: TDateField
      FieldName = 'DataG'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCNAB_LotCNAB_Cfg: TIntegerField
      FieldName = 'CNAB_Cfg'
    end
    object QrCNAB_LotMensagem1: TWideStringField
      FieldName = 'Mensagem1'
      Size = 40
    end
    object QrCNAB_LotMensagem2: TWideStringField
      FieldName = 'Mensagem2'
      Size = 40
    end
    object QrCNAB_LotDataS: TDateField
      FieldName = 'DataS'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCNAB_LotSeqArq: TIntegerField
      FieldName = 'SeqArq'
    end
    object QrCNAB_LotCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrCNAB_Lot_2401_208: TWideStringField
      FieldName = '_2401_208'
      Size = 33
    end
  end
  object QrCNAB_LotIts: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrCNAB_LotItsCalcFields
    SQL.Strings = (
      '/*'
      'SELECT lot.Cliente, lot.Lote, ent.MultaCodi, ent.MultaDias,'
      'ent.MultaValr, ent.MultaPerc, ent.MultaTiVe, ent.Protestar,'
      
        'ent.JuroSacado, ent.Tipo TipoCLI, PUF, EUF, uf0.Nome UFE, uf1.No' +
        'me UFP,'
      
        'CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE ent.Nome END NOME' +
        'CLI,'
      'CASE WHEN ent.Tipo=0 THEN ent.CNPJ ELSE ent.CPF END DOCUMCLI,'
      
        'CASE WHEN ent.Tipo=0 THEN ent.CNPJ    ELSE ent.CPF     END CNPJC' +
        'LI,'
      
        'CASE WHEN ent.Tipo=0 THEN ent.ERua    ELSE ent.PRua    END RuaCL' +
        'I,'
      
        'CASE WHEN ent.Tipo=0 THEN ent.ENumero+0.000 ELSE ent.PNumero+0.0' +
        '00 END NumCLI,'
      
        'CASE WHEN ent.Tipo=0 THEN ent.ECompl  ELSE ent.PCompl  END CplCL' +
        'I,'
      
        'CASE WHEN ent.Tipo=0 THEN ent.EBairro ELSE ent.PBairro END BrrCL' +
        'I,'
      
        'CASE WHEN ent.Tipo=0 THEN ent.ECEP    ELSE ent.PCEP    END CEPCL' +
        'I,'
      
        'CASE WHEN ent.Tipo=0 THEN ent.ECidade ELSE ent.PCidade END CidCL' +
        'I,'
      'ent.Corrido, sac.Numero+0.000 Numero,'
      'sac.CNPJ SACADO_CNPJ, sac.Nome SACADO_NOME, sac.Rua SACADO_RUA,'
      'sac.Numero SACADO_NUMERO, sac.Compl SACADO_COMPL,'
      'sac.Bairro SACADO_BAIRRO, sac.Bairro SACADO_BAIRRO,'
      'sac.Cidade SACADO_CIDADE, sac.UF SACADO_xUF,'
      'sac.CEP SACADO_CEP, IF(LENGTH(sac.CNPJ) >=14, 0, 1) SACADO_TIPO,'
      'sac.*, loi.*'
      'FROM lot esits loi'
      'LEFT JOIN lot es lot ON lot.Codigo=loi.Codigo'
      'LEFT JOIN entidades ent ON ent.Codigo=lot.Cliente'
      'LEFT JOIN sacados   sac ON sac.CNPJ=loi.CPF'
      'LEFT JOIN ufs       uf0 ON uf0.Codigo=ent.EUF'
      'LEFT JOIN ufs       uf1 ON uf1.Codigo=ent.PUF'
      'WHERE loi.CNAB_Lot=:P0'
      'ORDER BY Duplicata'
      '*/'
      ''
      'SELECT lot.Cliente, lot.Lote, ent.MultaCodi, ent.MultaDias,'
      'ent.MultaValr, ent.MultaPerc, ent.MultaTiVe, ent.Protestar,'
      
        'ent.JuroSacado, ent.Tipo TipoCLI, PUF, EUF, uf0.Nome UFE, uf1.No' +
        'me UFP,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMECLI,'
      'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) DOCUMCLI,'
      'IF(ent.Tipo=0, ent.CNPJ   , ent.CPF    ) CNPJCLI,'
      'IF(ent.Tipo=0, ent.ERua   , ent.PRua   ) RuaCLI,'
      'IF(ent.Tipo=0, ent.ENumero, ent.PNumero) + 0.000 NumCLI,'
      'IF(ent.Tipo=0, ent.ECompl , ent.PCompl ) CplCLI,'
      'IF(ent.Tipo=0, ent.EBairro, ent.PBairro) BrrCLI,'
      'IF(ent.Tipo=0, ent.ECEP   , ent.PCEP   ) + 0.000 CEPCLI,'
      'IF(ent.Tipo=0, ent.ECidade, ent.PCidade) CidCLI,'
      'ent.Corrido, sac.CNPJ SACADO_CNPJ, '
      'sac.Nome SACADO_NOME, sac.Rua SACADO_RUA,'
      'sac.Numero SACADO_NUMERO, sac.Compl SACADO_COMPL,'
      'sac.Bairro SACADO_BAIRRO, '
      'sac.Cidade SACADO_CIDADE, sac.UF SACADO_xUF,'
      'sac.CEP SACADO_CEP, IF(LENGTH(sac.CNPJ) >=14, 0, 1) SACADO_TIPO,'
      'loi.FatParcela, loi.Emitente, loi.CNPJCPF,'
      'loi.Bruto, loi.Desco, loi.Credito,'
      'loi.DCompra, loi.Vencimento, loi.DDeposito,'
      'loi.Duplicata, loi.Data,'
      'sac.*'
      'FROM lct0001a loi'
      'LEFT JOIN lot0001a lot ON lot.Codigo=loi.FatNum'
      'LEFT JOIN entidades ent ON ent.Codigo=lot.Cliente'
      'LEFT JOIN sacados   sac ON sac.CNPJ=loi.CNPJCPF'
      'LEFT JOIN ufs       uf0 ON uf0.Codigo=ent.EUF'
      'LEFT JOIN ufs       uf1 ON uf1.Codigo=ent.PUF'
      'WHERE loi.FatID=301'
      'AND loi.CNAB_Lot=:P0'
      'ORDER BY loi.Duplicata'
      ''
      '')
    Left = 496
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCNAB_LotItsENDERECO_EMI: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ENDERECO_EMI'
      Size = 255
      Calculated = True
    end
    object QrCNAB_LotItsSACADO_CEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SACADO_CEP_TXT'
      Size = 10
      Calculated = True
    end
    object QrCNAB_LotItsSACADO_NUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SACADO_NUMERO_TXT'
      Size = 15
      Calculated = True
    end
    object QrCNAB_LotItsCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrCNAB_LotItsLote: TIntegerField
      FieldName = 'Lote'
    end
    object QrCNAB_LotItsMultaCodi: TSmallintField
      FieldName = 'MultaCodi'
    end
    object QrCNAB_LotItsMultaDias: TSmallintField
      FieldName = 'MultaDias'
    end
    object QrCNAB_LotItsMultaValr: TFloatField
      FieldName = 'MultaValr'
    end
    object QrCNAB_LotItsMultaPerc: TFloatField
      FieldName = 'MultaPerc'
    end
    object QrCNAB_LotItsMultaTiVe: TSmallintField
      FieldName = 'MultaTiVe'
    end
    object QrCNAB_LotItsProtestar: TSmallintField
      FieldName = 'Protestar'
    end
    object QrCNAB_LotItsJuroSacado: TFloatField
      FieldName = 'JuroSacado'
    end
    object QrCNAB_LotItsTipoCLI: TSmallintField
      FieldName = 'TipoCLI'
    end
    object QrCNAB_LotItsPUF: TSmallintField
      FieldName = 'PUF'
    end
    object QrCNAB_LotItsEUF: TSmallintField
      FieldName = 'EUF'
    end
    object QrCNAB_LotItsUFE: TWideStringField
      FieldName = 'UFE'
      Required = True
      Size = 2
    end
    object QrCNAB_LotItsUFP: TWideStringField
      FieldName = 'UFP'
      Required = True
      Size = 2
    end
    object QrCNAB_LotItsNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrCNAB_LotItsDOCUMCLI: TWideStringField
      FieldName = 'DOCUMCLI'
      Size = 18
    end
    object QrCNAB_LotItsCNPJCLI: TWideStringField
      FieldName = 'CNPJCLI'
      Size = 18
    end
    object QrCNAB_LotItsRuaCLI: TWideStringField
      FieldName = 'RuaCLI'
      Size = 60
    end
    object QrCNAB_LotItsNumCLI: TFloatField
      FieldName = 'NumCLI'
    end
    object QrCNAB_LotItsCplCLI: TWideStringField
      FieldName = 'CplCLI'
      Size = 60
    end
    object QrCNAB_LotItsBrrCLI: TWideStringField
      FieldName = 'BrrCLI'
      Size = 60
    end
    object QrCNAB_LotItsCidCLI: TWideStringField
      FieldName = 'CidCLI'
      Size = 60
    end
    object QrCNAB_LotItsCorrido: TIntegerField
      FieldName = 'Corrido'
    end
    object QrCNAB_LotItsSACADO_CNPJ: TWideStringField
      FieldName = 'SACADO_CNPJ'
      Required = True
      Size = 15
    end
    object QrCNAB_LotItsSACADO_NOME: TWideStringField
      FieldName = 'SACADO_NOME'
      Size = 50
    end
    object QrCNAB_LotItsSACADO_RUA: TWideStringField
      FieldName = 'SACADO_RUA'
      Size = 30
    end
    object QrCNAB_LotItsSACADO_NUMERO: TLargeintField
      FieldName = 'SACADO_NUMERO'
    end
    object QrCNAB_LotItsSACADO_COMPL: TWideStringField
      FieldName = 'SACADO_COMPL'
      Size = 30
    end
    object QrCNAB_LotItsSACADO_BAIRRO: TWideStringField
      FieldName = 'SACADO_BAIRRO'
      Size = 30
    end
    object QrCNAB_LotItsSACADO_CIDADE: TWideStringField
      FieldName = 'SACADO_CIDADE'
      Size = 25
    end
    object QrCNAB_LotItsSACADO_xUF: TWideStringField
      FieldName = 'SACADO_xUF'
      Size = 2
    end
    object QrCNAB_LotItsSACADO_CEP: TIntegerField
      FieldName = 'SACADO_CEP'
    end
    object QrCNAB_LotItsSACADO_TIPO: TLargeintField
      FieldName = 'SACADO_TIPO'
      Required = True
    end
    object QrCNAB_LotItsCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Required = True
      Size = 15
    end
    object QrCNAB_LotItsIE: TWideStringField
      FieldName = 'IE'
      Size = 25
    end
    object QrCNAB_LotItsNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrCNAB_LotItsRua: TWideStringField
      FieldName = 'Rua'
      Size = 30
    end
    object QrCNAB_LotItsNumero: TLargeintField
      FieldName = 'Numero'
    end
    object QrCNAB_LotItsCompl: TWideStringField
      FieldName = 'Compl'
      Size = 30
    end
    object QrCNAB_LotItsBairro: TWideStringField
      FieldName = 'Bairro'
      Size = 30
    end
    object QrCNAB_LotItsCidade: TWideStringField
      FieldName = 'Cidade'
      Size = 25
    end
    object QrCNAB_LotItsUF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrCNAB_LotItsCEP: TIntegerField
      FieldName = 'CEP'
    end
    object QrCNAB_LotItsTel1: TWideStringField
      FieldName = 'Tel1'
    end
    object QrCNAB_LotItsRisco: TFloatField
      FieldName = 'Risco'
    end
    object QrCNAB_LotItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCNAB_LotItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrCNAB_LotItsEmail: TWideStringField
      FieldName = 'Email'
      Size = 255
    end
    object QrCNAB_LotItsCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrCNAB_LotItsEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
    object QrCNAB_LotItsCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrCNAB_LotItsBruto: TFloatField
      FieldName = 'Bruto'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrCNAB_LotItsDesco: TFloatField
      FieldName = 'Desco'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrCNAB_LotItsDCompra: TDateField
      FieldName = 'DCompra'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCNAB_LotItsVencimento: TDateField
      FieldName = 'Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCNAB_LotItsDDeposito: TDateField
      FieldName = 'DDeposito'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCNAB_LotItsFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrCNAB_LotItsDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 13
    end
    object QrCNAB_LotItsData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCNAB_LotItsCEPCLI: TFloatField
      FieldName = 'CEPCLI'
    end
  end
  object DsCNABLotIts: TDataSource
    DataSet = QrCNAB_LotIts
    Left = 524
    Top = 12
  end
  object PMLot: TPopupMenu
    OnPopup = PMLotPopup
    Left = 576
    Top = 424
    object Crianovolote1: TMenuItem
      Caption = '&Cria novo lote'
      OnClick = Crianovolote1Click
    end
    object Alteraloteatual1: TMenuItem
      Caption = '&Altera lote atual'
      OnClick = Alteraloteatual1Click
    end
    object Excluiloteatual1: TMenuItem
      Caption = '&Exclui lote atual'
      OnClick = Excluiloteatual1Click
    end
  end
  object PMTitulos: TPopupMenu
    Left = 660
    Top = 428
    object Inclui1: TMenuItem
      Caption = '&Entrada no Banco'
      OnClick = Inclui1Click
    end
    object Instruesparabanco1: TMenuItem
      Caption = '&Instru'#231#245'es para banco'
      Enabled = False
      OnClick = Instruesparabanco1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Retira1: TMenuItem
      Caption = '&Remove do lote'
      OnClick = Retira1Click
    end
  end
  object QrTitulos: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      '/*'
      
        'SELECT lot.Cliente, lot.Lote, CASE WHEN ent.Tipo=0 THEN ent.Raza' +
        'oSocial'
      'ELSE ent.Nome END NOMECLI, loi.*'
      'FROM lot esits loi'
      'LEFT JOIN lot es lot ON lot.Codigo=loi.Codigo'
      'LEFT JOIN entidades ent ON ent.Codigo=lot.Cliente'
      'WHERE lot.Tipo=1'
      'AND loi.Cobranca=0'
      'AND loi.CNAB_Lot=0'
      '*/'
      ''
      'SELECT lot.Cliente, lot.Lote, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMECLI, '
      'loi.FatParcela, loi.Emitente, loi.CNPJCPF,'
      'loi.Bruto, loi.Desco, loi.Credito,'
      'loi.Data, loi.DCompra, loi.Vencimento,'
      'loi.DDeposito, loi.Duplicata'
      'FROM lct0001a loi'
      'LEFT JOIN lot0001a lot ON lot.Codigo=loi.FatNum'
      'LEFT JOIN entidades ent ON ent.Codigo=lot.Cliente'
      'WHERE loi.FatID=301 '
      'AND lot.Tipo=1'
      'AND loi.Cobranca=0'
      'AND loi.CNAB_Lot=0'
      '')
    Left = 560
    Top = 12
    object QrTitulosCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrTitulosLote: TIntegerField
      FieldName = 'Lote'
    end
    object QrTitulosNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrTitulosFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrTitulosEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
    object QrTitulosCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrTitulosBruto: TFloatField
      FieldName = 'Bruto'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrTitulosDesco: TFloatField
      FieldName = 'Desco'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrTitulosCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrTitulosData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrTitulosDCompra: TDateField
      FieldName = 'DCompra'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrTitulosVencimento: TDateField
      FieldName = 'Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrTitulosDDeposito: TDateField
      FieldName = 'DDeposito'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrTitulosDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 13
    end
  end
  object DsTitulos: TDataSource
    DataSet = QrTitulos
    Left = 588
    Top = 12
  end
  object QrCNAB_Cfg: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cbb.Nome, cbb.Codigo, cbb.CartRetorno'
      'FROM cnab_cfg cbb'
      'ORDER BY Nome')
    Left = 500
    Top = 328
    object QrCNAB_CfgNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrCNAB_CfgCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCNAB_CfgCartRetorno: TIntegerField
      FieldName = 'CartRetorno'
    end
  end
  object DsCNAB_Cfg: TDataSource
    DataSet = QrCNAB_Cfg
    Left = 528
    Top = 328
  end
  object mySQLQuery1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ban.Nome NOMEBANCO, cbb.*'
      'FROM cnab_cfg cbb'
      'LEFT JOIN bancos ban ON ban.Codigo=cbb.Banco '
      'WHERE cbb.Codigo=:P0')
    Left = 616
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object IntegerField2: TIntegerField
      FieldName = 'Convenio'
    end
    object StringField2: TWideStringField
      FieldName = 'Carteira'
      Size = 2
    end
    object StringField3: TWideStringField
      FieldName = 'Variacao'
      Size = 3
    end
    object StringField4: TWideStringField
      FieldName = 'SiglaEspecie'
      Size = 5
    end
    object StringField5: TWideStringField
      FieldName = 'Moeda'
      Size = 5
    end
    object SmallintField1: TSmallintField
      FieldName = 'Aceite'
    end
    object SmallintField2: TSmallintField
      FieldName = 'Protestar'
    end
    object StringField6: TWideStringField
      FieldName = 'MsgLinha1'
      Size = 40
    end
    object SmallintField3: TSmallintField
      FieldName = 'PgAntes'
    end
    object SmallintField4: TSmallintField
      FieldName = 'MultaCodi'
    end
    object SmallintField5: TSmallintField
      FieldName = 'MultaDias'
    end
    object FloatField1: TFloatField
      FieldName = 'MultaValr'
    end
    object FloatField2: TFloatField
      FieldName = 'MultaPerc'
    end
    object SmallintField6: TSmallintField
      FieldName = 'MultaTiVe'
    end
    object SmallintField7: TSmallintField
      FieldName = 'ImpreLoc'
    end
    object IntegerField3: TIntegerField
      FieldName = 'Modalidade'
    end
    object StringField7: TWideStringField
      FieldName = 'clcAgencNr'
      Size = 4
    end
    object StringField8: TWideStringField
      FieldName = 'clcAgencDV'
      Size = 1
    end
    object StringField9: TWideStringField
      FieldName = 'clcContaNr'
      Size = 8
    end
    object StringField10: TWideStringField
      FieldName = 'clcContaDV'
      Size = 1
    end
    object StringField11: TWideStringField
      FieldName = 'cedAgencNr'
      Size = 4
    end
    object StringField12: TWideStringField
      FieldName = 'cedAgencDV'
      Size = 1
    end
    object StringField13: TWideStringField
      FieldName = 'cedContaNr'
      Size = 8
    end
    object StringField14: TWideStringField
      FieldName = 'cedContaDV'
      Size = 1
    end
    object SmallintField8: TSmallintField
      FieldName = 'Especie'
    end
    object SmallintField9: TSmallintField
      FieldName = 'Corrido'
    end
    object IntegerField4: TIntegerField
      FieldName = 'Banco'
    end
    object StringField15: TWideStringField
      FieldName = 'IDEmpresa'
    end
    object StringField16: TWideStringField
      FieldName = 'Produto'
      Size = 4
    end
    object StringField17: TWideStringField
      FieldName = 'NOMEBANCO'
      Size = 100
    end
    object SmallintField10: TSmallintField
      FieldName = 'InfoCovH'
      Required = True
    end
    object StringField18: TWideStringField
      FieldName = 'Carteira240'
      Size = 1
    end
    object StringField19: TWideStringField
      FieldName = 'Cadastramento'
      Size = 1
    end
    object StringField20: TWideStringField
      FieldName = 'TradiEscrit'
      Size = 1
    end
    object StringField21: TWideStringField
      FieldName = 'Distribuicao'
      Size = 1
    end
    object StringField22: TWideStringField
      FieldName = 'Aceite240'
      Size = 1
    end
    object StringField23: TWideStringField
      FieldName = 'Protesto'
      Size = 1
    end
    object IntegerField5: TIntegerField
      FieldName = 'Protestodd'
      Required = True
    end
    object StringField24: TWideStringField
      FieldName = 'BaixaDevol'
      Size = 1
    end
    object IntegerField6: TIntegerField
      FieldName = 'BaixaDevoldd'
      Required = True
    end
    object IntegerField7: TIntegerField
      FieldName = 'Lk'
    end
    object DateField1: TDateField
      FieldName = 'DataCad'
    end
    object DateField2: TDateField
      FieldName = 'DataAlt'
    end
    object IntegerField8: TIntegerField
      FieldName = 'UserCad'
    end
    object IntegerField9: TIntegerField
      FieldName = 'UserAlt'
    end
    object StringField25: TWideStringField
      FieldName = 'EmisBloqueto'
      Size = 1
    end
    object StringField26: TWideStringField
      FieldName = 'Especie240'
      Size = 2
    end
    object StringField27: TWideStringField
      FieldName = 'Juros240Cod'
      Size = 1
    end
    object FloatField3: TFloatField
      FieldName = 'Juros240Qtd'
      Required = True
    end
    object IntegerField10: TIntegerField
      FieldName = 'ContrOperCred'
      Required = True
    end
    object StringField28: TWideStringField
      FieldName = 'ReservBanco'
    end
    object StringField29: TWideStringField
      FieldName = 'ReservEmprs'
    end
    object StringField30: TWideStringField
      FieldName = 'LH_208_33'
      Size = 33
    end
    object StringField31: TWideStringField
      FieldName = 'SQ_233_008'
      Size = 8
    end
    object StringField32: TWideStringField
      FieldName = 'TL_124_117'
      Size = 117
    end
    object StringField33: TWideStringField
      FieldName = 'SR_208_033'
      Size = 33
    end
    object StringField34: TWideStringField
      FieldName = 'Diretorio'
      Size = 255
    end
  end
  object QrLot: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MAX(CodUsu) CodUsu'
      'FROM cnab_lot')
    Left = 396
    Top = 56
    object QrLotCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
  end
  object QrLcts: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrLctsCalcFields
    SQL.Strings = (
      '')
    Left = 280
    Top = 344
    object QrLctsNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Size = 100
    end
    object QrLctsNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 50
    end
    object QrLctsDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrLctsCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLctsVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctsData: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctsSEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
    object QrLctsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrLctsSub: TIntegerField
      FieldName = 'Sub'
    end
    object QrLctsGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrLctsCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrLctsSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrLctsTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrLctsID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
    end
    object QrLctsCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrLctsCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrLctsDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsLcts: TDataSource
    DataSet = QrLcts
    Left = 308
    Top = 344
  end
end
