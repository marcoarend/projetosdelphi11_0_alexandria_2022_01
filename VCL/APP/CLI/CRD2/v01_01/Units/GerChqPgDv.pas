unit GerChqPgDv;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  DmkDAC_PF, dmkImage, UnDmkEnums;

type
  TFmGerChqPgDv = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    PainelPgDv: TPanel;
    Panel9: TPanel;
    Label33: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    Label37: TLabel;
    Label12: TLabel;
    Label28: TLabel;
    Label30: TLabel;
    Label32: TLabel;
    Label31: TLabel;
    Label1: TLabel;
    TPDataBase2: TDateTimePicker;
    EdValorBase2: TdmkEdit;
    EdJurosBase2: TdmkEdit;
    EdJurosPeriodo2: TdmkEdit;
    TPData2: TDateTimePicker;
    TPPagto2: TDateTimePicker;
    EdJuros2: TdmkEdit;
    EdAPagar2: TdmkEdit;
    EdValPagar2: TdmkEdit;
    EdDesco2: TdmkEdit;
    Panel8: TPanel;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    Label47: TLabel;
    Label48: TLabel;
    Label49: TLabel;
    Label50: TLabel;
    EdAPCD: TdmkEditCB;
    CBAPCD: TdmkDBLookupComboBox;
    EdBanda2: TdmkEdit;
    EdRegiaoCompe2: TdmkEdit;
    EdBanco2: TdmkEdit;
    EdAgencia2: TdmkEdit;
    EdConta2: TdmkEdit;
    EdCheque2: TdmkEdit;
    EdCPF_2: TdmkEdit;
    EdEmitente2: TdmkEdit;
    EdCMC_7_2: TEdit;
    EdRealCC: TdmkEdit;
    GroupBox2: TGroupBox;
    Label51: TLabel;
    Label52: TLabel;
    Label53: TLabel;
    Label96: TLabel;
    Label98: TLabel;
    Label99: TLabel;
    Label91: TLabel;
    Label54: TLabel;
    EdDR: TdmkEdit;
    EdDT: TdmkEdit;
    EdRT: TdmkEdit;
    EdVT: TdmkEdit;
    EdST: TdmkEdit;
    EdCT: TdmkEdit;
    EdDV: TdmkEdit;
    EdCR: TdmkEdit;
    EdCV: TdmkEdit;
    EdOA: TdmkEdit;
    EdTT: TdmkEdit;
    QrAPCD: TmySQLQuery;
    QrAPCDCodigo: TIntegerField;
    QrAPCDNome: TWideStringField;
    QrAPCDDeposita: TSmallintField;
    QrAPCDExigeCMC7: TSmallintField;
    DsAPCD: TDataSource;
    QrBanco2: TmySQLQuery;
    QrBanco2Nome: TWideStringField;
    QrBanco2DVCC: TSmallintField;
    DsBanco2: TDataSource;
    QrSCB: TmySQLQuery;
    QrSCBSCB: TIntegerField;
    QrCli: TmySQLQuery;
    QrLocPg: TmySQLQuery;
    SpeedButton1: TSpeedButton;
    QrLocPgData: TDateField;
    QrLocPgFatParcela: TIntegerField;
    EdCorrigido2: TdmkEdit;
    Label2: TLabel;
    EdPendente2: TdmkEdit;
    Label9: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdValorBase2Change(Sender: TObject);
    procedure EdJurosBase2Change(Sender: TObject);
    procedure TPData2Change(Sender: TObject);
    procedure TPPagto2Change(Sender: TObject);
    procedure EdJuros2Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdBanda2Change(Sender: TObject);
    procedure EdCMC_7_2Change(Sender: TObject);
    procedure EdRegiaoCompe2Change(Sender: TObject);
    procedure EdRegiaoCompe2Exit(Sender: TObject);
    procedure EdAgencia2Change(Sender: TObject);
    procedure EdConta2Change(Sender: TObject);
    procedure EdBanco2Change(Sender: TObject);
    procedure EdCPF_2Exit(Sender: TObject);
    procedure EdCPF_2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtOKClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure EdValPagar2Change(Sender: TObject);
    procedure EdAPagar2Change(Sender: TObject);
    procedure EdDesco2Change(Sender: TObject);
    procedure EdValPagar2Exit(Sender: TObject);
  private
    { Private declarations }
    procedure CalculaAPagarDev();
    function  LocalizaEmitente2(MudaNome: Boolean): Boolean;
    procedure ReopenBanco2();
    procedure CalculaPendente();

  public
    { Public declarations }
    FPrimeiroPagto: Boolean;
    procedure CalculaJurosDev();
    procedure ConfiguraPgCHDev(Data: TDateTime);
  end;

  var
  FmGerChqPgDv: TFmGerChqPgDv;

implementation

uses UnMyObjects, Module, LotEmi, ModuleLot2, Principal, GerChqMain,
  UMySQLModule, MyListas, ModuleLot, APCD, MyDBCheck, ModuleFin;

{$R *.DFM}

procedure TFmGerChqPgDv.BtOKClick(Sender: TObject);
var
  Tipific, StatusSPC, Codigo, FatParcela, APCD, Cliente, DMais, Dias, Cheque,
  Comp, Banco, Agencia, Praca, TipoCart, Carteira, Controle, Sub, TpAlin,
  FatID_Sub, OcorAPCD, Genero, Ocorreu, FatParcRef,
  CartDep, AliIts, ChqPgs: Integer;
  //Valor, Desco,
  FatNum, TxaCompra, TxaAdValorem, VlrCompra,
  ValPagar, VlrAdValorem, TxaJuros, MoraVal: Double;
  Descricao, DataDoc, Data3, DDeposito, DCompra, Vencto, Conta, CPF, Emitente: String;
  DataCH, Dta: TDateTime;
  //
  Gen_0303, Gen_0304, Gen_0305, Gen_0307: Integer;
  ValPrinc, ValTaxas, ValMulta, ValJuros, ValDesco, ValPende: Double;
begin
  //Cliente := FmGerChqMain.QrPesqCliente.Value;
  APCD := EdAPCD.ValueVariant;
  if APCD = 0 then
  begin
    Geral.MensagemBox('Defina a al�nea de pagamento!', 'Erro',
      MB_OK+MB_ICONWARNING);
    EdAPCD.SetFocus;
    Exit;
  end;
  Dias    := Trunc(TPPagto2.Date - TPDataBase2.Date);
  //Valor   := Geral.DMV(EdPago2.Text);
  //Desco   := Geral.DMV(EdDesco2.Text);
  Vencto  := FormatDateTime(VAR_FORMATDATE, TPPagto2.Date);
  DCompra := FormatDateTime(VAR_FORMATDATE, TPData2.Date);
  Banco   := EdBanco2.ValueVariant;
  Agencia := EdAgencia2.ValueVariant;
  Conta   := EdConta2.Text;
  Cheque  := EdCheque2.ValueVariant;
  CPF     := Geral.SoNumero_TT(EdCPF_2.Text);
  Emitente:= EdEmitente2.Text;
  Praca   := EdRegiaoCompe2.ValueVariant;
  if QrAPCDExigeCMC7.Value = 1 then
  begin
    if (Conta = '') or (Cheque=0) then
    begin
      Geral.MensagemBox('A al�nea selecionada exige CMC7 (dados do cheque)!',
        'Aviso', MB_OK+MB_ICONWARNING);
      EdBanda2.SetFocus;
      Exit;
    end;
  end;
{
  QrSCB.Close;
  QrSCB.Params[0].AsInteger := FmGerChqMain.QrPesqCliente.Value;
  UMyMod.AbreQuery(QrSCB);
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrSCB, Dmod.MyDB, [
  'SELECT SCB ',
  'FROM entidades ',
  'WHERE Codigo=' + Geral.FF0(FmGerChqMain.QrPesqCliente.Value),
  '']);
  //
{
  Codigo := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
        'Alin Pgs', 'Alin Pgs', 'Codigo');
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO alin pgs SET AlterWeb=1, ');
  Dmod.QrUpd.SQL.Add('Data=:P0, Juros=:P1, Pago=:P2, DataCh=:P3, APCD=:P4, ');
  Dmod.QrUpd.SQL.Add('Desco=:P5, AlinIts=:Pa, Codigo=:Pb');
  Dmod.QrUpd.Params[00].AsString  := FormatDateTime(VAR_FORMATDATE, TPPagto2.Date);
  Dmod.QrUpd.Params[01].AsFloat   := Geral.DMV(EdJuros2.Text);
  Dmod.QrUpd.Params[02].AsFloat   := Geral.DMV(EdPago2.Text);
  Dmod.QrUpd.Params[03].AsString  := FormatDateTime(VAR_FORMATDATE, TPData2.Date);
  Dmod.QrUpd.Params[04].AsInteger := QrAPCDCodigo.Value;
  Dmod.QrUpd.Params[05].AsFloat   := Desco;
  //
  Dmod.QrUpd.Params[06].AsInteger := FmGerChqMain.QrAlinItsCodigo.Value;
  Dmod.QrUpd.Params[07].AsInteger := Codigo;
  Dmod.QrUpd.ExecSQL;
}
      //
  FatNum := 0; // n�o tem! � no cheque!
  FatParcela := 0;
  Controle := 0;
  FatID_Sub := FmGerChqMain.QrAlinItsAlinea1.Value;
  Cliente := FmGerChqMain.QrAlinItsCliente.Value;
  Ocorreu := APCD;
  FatParcRef := FmGerChqMain.QrAlinItsCodigo.Value;
  MoraVal := Geral.DMV(EdJuros2.Text);
  Dta := TPPagto2.Date;
  DataCH := TPData2.Date;
  //Desco := Geral.DMV(EdDesco2.Text);
  //
  if not DModFin.ObtemGeneroDeFatID(VAR_FATID_0303, Gen_0303, tpCred, True) then
    Exit;
  if not DModFin.ObtemGeneroDeFatID(VAR_FATID_0304, Gen_0304, tpCred, True) then
    Exit;
  if not DModFin.ObtemGeneroDeFatID(VAR_FATID_0305, Gen_0305, tpCred, True) then
    Exit;
  if not DModFin.ObtemGeneroDeFatID(VAR_FATID_0307, Gen_0307, tpDeb, True) then
    Exit;
  //
  ValPagar := EdValPagar2.ValueVariant;
  if FPrimeiroPagto then
  begin
    ValTaxas := FmGerChqMain.QrAlinItsTaxas.Value;
    ValMulta := FmGerChqMain.QrAlinItsMulta.Value;
  end else begin
    ValTaxas := 0;
    ValMulta := 0;
  end;
  ValDesco := EdDesco2.ValueVariant;
  ValJuros := EdJuros2.ValueVariant;
  ValPrinc := ValPagar - ValTaxas - ValMulta - ValJuros + ValDesco;
  ValPende := EdPendente2.ValueVariant;
  // N�o precisa, porque aqui est� quitando?:
  CartDep := 0;

{
  DmLot.InsUpd_ChqPg(Dmod.QrUpd, stIns, FatParcela, Controle, FatID_Sub, Genero,
  Cliente, Ocorreu, FatParcRef, FatNum, Pago, MoraVal, Desco, DataCH, Dta);
}
  DmLot.InsUpd_ChqPg(Dmod.QrUpd, stIns, FatParcela, Controle, FatID_Sub,
  Gen_0303, Gen_0304, Gen_0305, Gen_0307,
  Cliente, Ocorreu, FatParcRef, FatNum, ValPagar(*, MoraVal, Desco*), DataCH, Dta,
  ValPrinc, ValTaxas, ValMulta, ValJuros, ValDesco, ValPende, CartDep);
  Codigo := FatParcela;
  //
  if QrAPCDExigeCMC7.Value = 1 then
  begin
    //  Depositos, riscos, etc
{ TODO : CUIDADO!!! Quando arquivar cuidar lot es negativos >=< -Cliente!!! }
(*
    QrCli.Close;
    QrCli.Params[0].AsInteger := -Cliente;
    QrCli. Open;
*)
    // CMC7: 399003690099121555000362882904

    UnDmkDAC_PF.AbreMySQLQuery0(QrCli, Dmod.MyDB, [
    'SELECT Codigo ',
    'FROM ' + CO_TabLotA,
    'WHERE Codigo=' + FormatFloat('0', -Cliente),
    '']);
    if QrCli.RecordCount = 0 then
    begin
(*
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('INSERT INTO lot es');
      Dmod.QrUpd.SQL.Add('SET Codigo=:P0, Cliente=:P1');
      Dmod.QrUpd.Params[00].AsInteger := -Cliente;
      Dmod.QrUpd.Params[01].AsInteger := Cliente;
      Dmod.QrUpd.ExecSQL;
*)
      // InsUpd_IGNORE! n�o mexer!
      UMyMod.SQLInsUpd_IGNORE(Dmod.QrUpd, stIns, CO_TabLotA, False, [
      'Cliente'], ['Codigo'], [Cliente], [-Cliente], True);
    end;
    Comp := FmPrincipal.VerificaDiasCompensacao(Praca,
      FmGerChqMain.QrPesqCliente.Value, ValPagar, TPData2.Date, TPPagto2.Date,
      QrSCBSCB.Value);
    DDeposito := Geral.FDT(UMyMod.CalculaDataDeposito(TPPagto2.Date), 1);
(*
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO lot esits SET Quitado=0, ');
    Dmod.QrUpd.SQL.Add('Comp=:P0, Banco=:P1, Agencia=:P2, Conta=:P3, ');
    Dmod.QrUpd.SQL.Add('Cheque=:P4, CPF=:P5, Emitente=:P6, Valor=:P7, ');
    Dmod.QrUpd.SQL.Add('Vencto=:P8, TxaCompra=:P9, TxaAdValorem=:P10, ');
    Dmod.QrUpd.SQL.Add('VlrCompra=:P11, VlrAdValorem=:P12, DMais=:P13, ');
    Dmod.QrUpd.SQL.Add('Dias=:P14, TxaJuros=:P15, DCompra=:P16, ');
    Dmod.QrUpd.SQL.Add('DDeposito=:P17, Praca=:P18, Tipo=:P19, ');
    Dmod.QrUpd.SQL.Add('AliIts=:P20, Alin Pgs=:P21, Cliente=:P22, Data3=:P23 ');
    Dmod.QrUpd.SQL.Add(', Codigo=:Pa, Controle=:Pb');
    //
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
      'Lot esIts', 'Lot esIts', 'Controle');
    Dmod.QrUpd.Params[00].AsInteger := Comp;
    Dmod.QrUpd.Params[01].AsInteger := Banco;
    Dmod.QrUpd.Params[02].AsInteger := Agencia;
    Dmod.QrUpd.Params[03].AsString  := Conta;
    Dmod.QrUpd.Params[04].AsInteger := Cheque;
    Dmod.QrUpd.Params[05].AsString  := CPF;
    Dmod.QrUpd.Params[06].AsString  := Emitente;
    Dmod.QrUpd.Params[07].AsFloat   := Valor;
    Dmod.QrUpd.Params[08].AsString  := Vencto;
    //
    Dmod.QrUpd.Params[09].AsFloat   := 0;//FTaxa[0];
    Dmod.QrUpd.Params[10].AsFloat   := 0;
    Dmod.QrUpd.Params[11].AsFloat   := 0;//FValr[0];
    Dmod.QrUpd.Params[12].AsFloat   := 0;
    Dmod.QrUpd.Params[13].AsInteger := 0;//DMais;
    Dmod.QrUpd.Params[14].AsInteger := Dias;
    Dmod.QrUpd.Params[15].AsFloat   := 0;//FJuro[0];
    Dmod.QrUpd.Params[16].AsString  := DCompra;
    Dmod.QrUpd.Params[17].AsString  := DDeposito;
    Dmod.QrUpd.Params[18].AsInteger := Praca;
    Dmod.QrUpd.Params[19].AsInteger := QrAPCDDeposita.Value + 1;
    Dmod.QrUpd.Params[20].AsInteger := FmGerChqMain.QrAlinItsCodigo.Value;
    Dmod.QrUpd.Params[21].AsInteger := Codigo;
    Dmod.QrUpd.Params[22].AsInteger := Cliente;
    Dmod.QrUpd.Params[23].AsString  := DDeposito; // Quita autom�tico at� voltar
    //
    Dmod.QrUpd.Params[24].AsInteger := -Cliente;
    Dmod.QrUpd.Params[25].AsInteger := Controle;
    Dmod.QrUpd.ExecSQL;
*)
    FatParcela := 0;
    DMais := 0;
    FatNum := -Cliente;
    //
    Tipific := 5; //??
    StatusSPC := -1;
    //
    TxaCompra := 0;
    TxaAdValorem := 0;
    VlrCompra := 0;
    VlrAdValorem := 0;
    TxaJuros := 0;
    //
    DataDoc := Geral.FDT(TPData2.Date, 1);
    Data3 := Geral.FDT(0, 1);
    //
    TipoCart := 2;
    Carteira := CO_CARTEIRA_CHQ_BORDERO;
    Controle := 0;
    Sub := 0;
    //
    Descricao := 'PG CD';
    //
    TpAlin := QrAPCDDeposita.Value + 1;
    AliIts := FmGerChqMain.QrAlinItsCodigo.Value;
    ChqPgs := Codigo;
    //
    DmLot.SQL_CH(FatParcela, Comp, Banco, FormatFloat('0', Agencia), Cheque,
    DMais, Dias, FatNum, Praca, Tipific, StatusSPC, Conta, CPF, Emitente,
    DataDoc, Vencto, DCompra, DDeposito, ValPagar, stIns, Cliente, TxaCompra, TxaAdValorem,
    VlrCompra, VlrAdValorem, TxaJuros, Data3, TpAlin, AliIts, ChqPgs,
    Descricao, TipoCart, Carteira, Controle, Sub);
  end else
  begin
  {
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE ' + CO_TabLctA);
      Dmod.QrUpd.SQL.Add('SET AlterWeb=1,');
      Dmod.QrUpd.SQL.Add('ValQuit=' + FormatFloat('0.00', FmGerChqMain.QrAlinItsATUAL.Value));
      Dmod.QrUpd.SQL.Add('WHERE FatID=' + FormatFloat('0', VAR_FATID_0301));
      Dmod.QrUpd.SQL.Add('AND FatParcela=' + FormatFloat('0', FmGerChqMain.QrPesqFatParcela.Value));
      Dmod.QrUpd.ExecSQL;
  }

    DmLot.LOI_Upd_FPC(Dmod.QrUpd, FmGerChqMain.QrPesqFatParcela.Value,
    ['ValQuit'], [FmGerChqMain.QrAlinItsATUAL.Value]);
  end;
  //
  //FmGerChqMain.Calcula Pagamento CHDev(TPPagto2.Date);
  DmLot.CalculaPagtoAlinIts(TPPagto2.Date, FmGerChqMain.QrAlinItsCodigo.Value);

  FmGerChqMain.FChqPgs := Codigo;
  FmGerChqMain.ReopenChqPgs(Codigo);
  //
  DmLot2.AtualizaEmitente(FormatFloat('000', Banco),
    FormatFloat('0000', Agencia), Conta, CPF, Emitente, -1);
  QrSCB.Close;
  //
  FmGerChqMain.Pesquisa(True, False);
  //
  Close;
end;

procedure TFmGerChqPgDv.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGerChqPgDv.CalculaAPagarDev();
var
  Base, Juro, Desc: Double;
begin
  Base := EdValorBase2.ValueVariant;
  Juro := EdJuros2.ValueVariant;
  Desc := EdDesco2.ValueVariant;
  //
  EdCorrigido2.ValueVariant := Base + Juro;
  EdAPagar2.ValueVariant  := EdCorrigido2.ValueVariant - Desc;
  EdValPagar2.ValueVariant  := EdAPagar2.ValueVariant;
end;

procedure TFmGerChqPgDv.CalculaJurosDev();
var
  Prazo: Integer;
  Taxa, Juros, Valor: Double;
begin
  Juros := 0;
  Prazo := Trunc(Int(TPPagto2.Date) - Int(TPDataBase2.Date));
  if Prazo > 0 then
  begin
    Taxa  := Geral.DMV(EdJurosBase2.Text);
    Juros := MLAGeral.CalculaJuroComposto(Taxa, Prazo);
  end;
  Valor := Geral.DMV(EdValorBase2.Text);
  EdJurosPeriodo2.Text := Geral.FFT(Juros, 6, siPositivo);
  EdJuros2.Text := Geral.FFT(Juros * Valor / 100, 2, siPositivo);
end;

procedure TFmGerChqPgDv.CalculaPendente();
begin
  EdPendente2.ValueVariant := EdAPagar2.ValueVariant - EdValPagar2.ValueVariant;
end;

procedure TFmGerChqPgDv.ConfiguraPgCHDev(Data: TDateTime);
var
  ValorBase: Double;
begin
  TPPagto2.MinDate := 0;
  //ver se funciona !!!
{
  QrLocPg.Close;
  QrLocPg.Params[0].AsInteger := FmGerChqMain.QrAlinItsCodigo.Value;
  UMyMod.AbreQuery(QrLocPg);
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocPg, Dmod.MyDB, [
  'SELECT * FROM ' + CO_TabLctA,
  //'WHERE FatID=' + TXT_VAR_FATID_0305,
  'WHERE FatID=' + TXT_VAR_FATID_0311,
  'AND Data=( ',
  'SELECT Max(Data) FROM ' + CO_TabLctA,
  //'WHERE FatID=' + TXT_VAR_FATID_0305,
  'WHERE FatID=' + TXT_VAR_FATID_0311,
  'AND FatParcRef=' + Geral.FF0(FmGerChqMain.QrAlinItsCodigo.Value) +') ',
  'ORDER BY FatParcela DESC ',
  '']);
  //
  if QrLocPg.RecordCount > 0 then
  begin
    //if Int(QrLocPgData.Value) < int(Data) then
      TPDataBase2.Date := Int(QrLocPgData.Value)
    // Errado !!!!
    //else TPDataBase2.Date := date;
  end else TPDataBase2.Date := FmGerChqMain.QrAlinItsData1.Value;
  EdJurosBase2.Text := Geral.FFT(FmGerChqMain.QrAlinItsJurosP.Value, 6, siPositivo);
  ValorBase := FmGerChqMain.QrAlinItsValor.Value +
               FmGerChqMain.QrAlinItsTaxas.Value +
               FmGerChqMain.QrAlinItsMulta.Value +
               FmGerChqMain.QrAlinItsJurosV.Value -
               FmGerChqMain.QrAlinItsValPago.Value -
               FmGerChqMain.QrAlinItsDesconto.Value -
               FmGerChqMain.QrAlinItsPgDesc.Value;
  TPPagto2.Date := Date + 360;
  EdValorBase2.ValueVariant := ValorBase;
  if Data >= TPDataBase2.Date then
    TPPagto2.Date := Data
  else
    TPPagto2.Date := TPDataBase2.Date;
  TPPagto2.MinDate := TPDataBase2.Date;
  TPData2.Date := TPDataBase2.Date;
  //
end;

procedure TFmGerChqPgDv.EdAgencia2Change(Sender: TObject);
begin
  LocalizaEmitente2(True);
end;

procedure TFmGerChqPgDv.EdAPagar2Change(Sender: TObject);
begin
  CalculaPendente();
end;

procedure TFmGerChqPgDv.EdBanco2Change(Sender: TObject);
begin
  LocalizaEmitente2(True);
end;

procedure TFmGerChqPgDv.EdBanda2Change(Sender: TObject);
begin
  if MLAGeral.LeuTodoCMC7(EdBanda2.Text) then
    EdCMC_7_2.Text := Geral.SoNumero_TT(EdBanda2.Text);
end;

procedure TFmGerChqPgDv.EdCMC_7_2Change(Sender: TObject);
var
  Banda: TBandaMagnetica;
begin
  if MLAGeral.CalculaCMC7(EdCMC_7_2.Text) = 0 then
  begin
    Banda := TBandaMagnetica.Create;
    Banda.BandaMagnetica := EdCMC_7_2.Text;
    EdRegiaoCompe2.Text  := Banda.Compe;
    EdBanco2.Text        := Banda.Banco;
    EdAgencia2.Text      := Banda.Agencia;
    EdConta2.Text        := Banda.Conta;
    EdCheque2.Text       := Banda.Numero;
    //
  end;
end;

procedure TFmGerChqPgDv.EdConta2Change(Sender: TObject);
begin
  LocalizaEmitente2(True);
end;

procedure TFmGerChqPgDv.EdCPF_2Exit(Sender: TObject);
begin
  DmLot.PesquisaRiscoSacado(EdCPF_2.Text, EdCR, EdCV, EdCT,
  EdDR, EdDV, EdDt, EdRT, EdVT, EdST, EdOA, EdTT);
end;

procedure TFmGerChqPgDv.EdCPF_2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
  begin
    Dmod.QrLocCPF.Close;
    Dmod.QrLocCPF.Params[0].AsString := Geral.SoNumero_TT(EdCPF_2.Text);
    UMyMod.AbreQuery(Dmod.QrLocCPF, Dmod.MyDB);
    case Dmod.QrLocCPF.RecordCount of
      0: Application.MessageBox('N�o foi localizado emitente para este CPF!',
        'Aviso', MB_OK+MB_ICONWARNING);
      1: EdEmitente2.Text := Dmod.QrLocCPFNome.Value;
      else begin
        Application.CreateForm(TFmLotEmi, FmLotEmi);
        FmLotEmi.ShowModal;
        if FmLotEmi.FEmitenteNome <> '' then
          EdEmitente2.Text := FmLotEmi.FEmitenteNome;
        FmLotEmi.Destroy;
      end;
    end;
  end;
end;

procedure TFmGerChqPgDv.EdDesco2Change(Sender: TObject);
begin
  CalculaAPagarDev();
end;

procedure TFmGerChqPgDv.EdJuros2Change(Sender: TObject);
begin
  CalculaAPagarDev();
end;

procedure TFmGerChqPgDv.EdJurosBase2Change(Sender: TObject);
begin
  CalculaJurosDev();
end;

procedure TFmGerChqPgDv.EdRegiaoCompe2Change(Sender: TObject);
begin
  LocalizaEmitente2(True);
  if not EdBanco2.Focused then
    ReopenBanco2();
end;

procedure TFmGerChqPgDv.EdRegiaoCompe2Exit(Sender: TObject);
begin
  LocalizaEmitente2(True);
end;

procedure TFmGerChqPgDv.EdValorBase2Change(Sender: TObject);
begin
  CalculaAPagarDev;
end;

procedure TFmGerChqPgDv.EdValPagar2Change(Sender: TObject);
begin
  CalculaPendente();
end;

procedure TFmGerChqPgDv.EdValPagar2Exit(Sender: TObject);
begin
  //EdAPCD.SetFocus;
end;

procedure TFmGerChqPgDv.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGerChqPgDv.FormCreate(Sender: TObject);
begin
  FPrimeiroPagto := True;
  ImgTipo.SQLType := stNil;
  UMyMod.AbreQuery(QrAPCD, Dmod.MyDB);
  TPData2.Date := Date;
  TPPagto2.Date := Date;
end;

procedure TFmGerChqPgDv.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmGerChqPgDv.LocalizaEmitente2(MudaNome: Boolean): Boolean;
{
var
  B,A,C: String;
}
begin
{
  Result := False;
  if  (Geral.IMV(EdBanco2.Text) > 0)
  and (Geral.IMV(EdAgencia2.Text) > 0)
  and (Geral.DMV(EdConta2.Text) > 0.1) then
  begin
    Screen.Cursor := crHourGlass;
    try
      //
      if MudaNome then
      begin
        B := EdBanco2.Text;
        A := EdAgencia2.Text;
        C := EdConta2.Text;
        //
        Dmoc.QrLocEmiBAC.Close;
        Dmoc.QrLocEmiBAC.Params[0].AsInteger := QrBanco2DVCC.Value;
        Dmoc.QrLocEmiBAC.Params[1].AsString  := CriaBAC_Pesq(B,A,C,QrBanco2DVCC.Value);
        UMyMod.AbreQuery(Dmoc.QrLocEmiBAC);
        //
        if Dmoc.QrLocEmiBAC.RecordCount = 0 then
        begin
          EdEmitente2.Text := '';
          EdCPF_2.Text := '';
        end else begin
          Dmoc.QrLocEmiCPF.Close;
          Dmoc.QrLocEmiCPF.Params[0].AsString := Dmoc.QrLocEmiBACCPF.Value;
          UMyMod.AbreQuery(Dmoc.QrLocEmiCPF);
          //
          if Dmoc.QrLocEmiCPF.RecordCount = 0 then
          begin
            EdEmitente2.Text := '';
            EdCPF_2.Text := '';
          end else begin
            EdEmitente2.Text := Dmoc.QrLocEmiCPFNome.Value;
            EdCPF_2.Text      := Geral.FormataCNPJ_TT(Dmoc.QrLocEmiCPFCPF.Value);
            DmLot.PesquisaRiscoSacado(EdCPF_2.Text);
          end;
        end;
      end;
      Screen.Cursor := crDefault;
      Result := True;
    except
      Screen.Cursor := crDefault;
      raise;
    end;
  end;
}
  Result := DmLot2.LocalizaEmitente2(MudaNome, EdBanco2.Text, EdAgencia2.Text,
    EdConta2.Text, QrBanco2DVCC.Value, EdEmitente2, EdCPF_2, EdRealCC);
  if EdCPF_2.Text <> '' then
    DmLot.PesquisaRiscoSacado(EdCPF_2.Text, EdCR, EdCV, EdCT,
    EdDR, EdDV, EdDt, EdRT, EdVT, EdST, EdOA, EdTT);
end;

procedure TFmGerChqPgDv.ReopenBanco2();
begin
  QrBanco2.Close;
  QrBanco2.Params[0].AsString := EdBanco2.Text;
  UMyMod.AbreQuery(QrBanco2, Dmod.MyDB);
end;

procedure TFmGerChqPgDv.SpeedButton1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmAPCD, FmAPCD, afmoNegarComAviso) then
  begin
    FmAPCD.ShowModal;
    FmAPCD.Destroy;
    //
    UMyMod.SetaCodigoPesquisado(EdAPCD, CBAPCD, QrAPCD, VAR_CADASTRO);
  end;
end;

procedure TFmGerChqPgDv.TPData2Change(Sender: TObject);
begin
  CalculaJurosDev;
end;

procedure TFmGerChqPgDv.TPPagto2Change(Sender: TObject);
begin
  CalculaJurosDev;
end;

end.
