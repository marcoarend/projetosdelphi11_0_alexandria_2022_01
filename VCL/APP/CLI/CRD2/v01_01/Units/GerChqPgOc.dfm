object FmGerChqPgOc: TFmGerChqPgOc
  Left = 339
  Top = 185
  Caption = 'CHQ-CNTRL-005 :: Pagamento de Ocorr'#234'cia de Cheque'
  ClientHeight = 269
  ClientWidth = 559
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 559
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 511
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 463
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 442
        Height = 32
        Caption = 'Pagamento de Ocorr'#234'cia de Cheque'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 442
        Height = 32
        Caption = 'Pagamento de Ocorr'#234'cia de Cheque'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 442
        Height = 32
        Caption = 'Pagamento de Ocorr'#234'cia de Cheque'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 559
    Height = 107
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 559
      Height = 107
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 559
        Height = 107
        Align = alClient
        TabOrder = 0
        object PainelOcorPg: TPanel
          Left = 2
          Top = 15
          Width = 555
          Height = 90
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label86: TLabel
            Left = 80
            Top = 4
            Width = 52
            Height = 13
            Caption = 'Data base:'
          end
          object Label87: TLabel
            Left = 196
            Top = 4
            Width = 53
            Height = 13
            Caption = 'Valor base:'
          end
          object Label88: TLabel
            Left = 292
            Top = 4
            Width = 63
            Height = 13
            Caption = '%Tx jur.base:'
          end
          object Label89: TLabel
            Left = 388
            Top = 4
            Width = 68
            Height = 13
            Caption = '% Jur.per'#237'odo:'
          end
          object Label82: TLabel
            Left = 80
            Top = 44
            Width = 26
            Height = 13
            Caption = 'Data:'
          end
          object Label83: TLabel
            Left = 196
            Top = 44
            Width = 37
            Height = 13
            Caption = '$ Juros:'
          end
          object Label85: TLabel
            Left = 292
            Top = 44
            Width = 66
            Height = 13
            Caption = 'Total a pagar:'
          end
          object Label84: TLabel
            Left = 388
            Top = 44
            Width = 75
            Height = 13
            Caption = '$ Valor a pagar:'
          end
          object TPDataBase6: TDateTimePicker
            Left = 80
            Top = 20
            Width = 110
            Height = 21
            Date = 38698.785142685200000000
            Time = 38698.785142685200000000
            Color = clBtnFace
            TabOrder = 0
            TabStop = False
          end
          object EdValorBase6: TdmkEdit
            Left = 196
            Top = 20
            Width = 90
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            Color = clBtnFace
            ReadOnly = True
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            OnChange = EdValorBase6Change
          end
          object EdJurosBase6: TdmkEdit
            Left = 292
            Top = 20
            Width = 90
            Height = 21
            Alignment = taRightJustify
            Color = clWhite
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 6
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            OnChange = EdJurosBase6Change
          end
          object EdJurosPeriodo6: TdmkEdit
            Left = 388
            Top = 20
            Width = 90
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            Color = clBtnFace
            ReadOnly = True
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 6
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object TPPagto6: TDateTimePicker
            Left = 80
            Top = 60
            Width = 110
            Height = 21
            Date = 38698.785142685200000000
            Time = 38698.785142685200000000
            TabOrder = 4
            OnChange = TPPagto6Change
          end
          object EdJuros6: TdmkEdit
            Left = 196
            Top = 60
            Width = 90
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            Color = clBtnFace
            ReadOnly = True
            TabOrder = 5
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            OnChange = EdJuros6Change
          end
          object EdAPagar6: TdmkEdit
            Left = 292
            Top = 60
            Width = 90
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            Color = clBtnFace
            ReadOnly = True
            TabOrder = 6
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            OnChange = EdAPagar6Change
          end
          object EdPago6: TdmkEdit
            Left = 388
            Top = 60
            Width = 90
            Height = 21
            Alignment = taRightJustify
            TabOrder = 7
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 155
    Width = 559
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 555
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 199
    Width = 559
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 413
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 411
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrLocOc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      '/*'
      'SELECT * FROM ocor rpg'
      'WHERE Data=('
      'SELECT Max(Data) FROM ocor rpg'
      'WHERE Ocorreu=:P0)'
      'ORDER BY Codigo DESC'
      '*/'
      ''
      'SELECT Data'
      'FROM lct0001a'
      'WHERE FatID=304 '
      'AND Data=('
      '  SELECT Max(Data) '
      '  FROM lct0001a'
      '  WHERE FatID=304 '
      '  AND Ocorreu=:P0)'
      'ORDER BY FatNum DESC'
      '')
    Left = 5
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocOcData: TDateField
      FieldName = 'Data'
    end
  end
end
