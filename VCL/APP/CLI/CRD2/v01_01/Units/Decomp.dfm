object FmDecomp: TFmDecomp
  Left = 419
  Top = 217
  Caption = 'Juro Decomposto'
  ClientHeight = 227
  ClientWidth = 464
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 464
    Height = 65
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitHeight = 68
    object Label11: TLabel
      Left = 12
      Top = 8
      Width = 57
      Height = 13
      Caption = '% Juro total:'
    end
    object Label1: TLabel
      Left = 116
      Top = 8
      Width = 68
      Height = 13
      Caption = 'Dias de prazo:'
    end
    object Label2: TLabel
      Left = 188
      Top = 8
      Width = 32
      Height = 13
      Caption = 'Casas:'
    end
    object Label3: TLabel
      Left = 228
      Top = 8
      Width = 49
      Height = 13
      Caption = 'Taxa m'#234's:'
    end
    object EdJuros: TdmkEdit
      Left = 12
      Top = 24
      Width = 101
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 6
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
    end
    object EdPrazo: TdmkEdit
      Left = 116
      Top = 24
      Width = 69
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 4
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,0000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
    end
    object EdCasas: TdmkEdit
      Left = 188
      Top = 24
      Width = 37
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ValMin = '0'
      ValMax = '6'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '2'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 2
    end
    object EdTaxa: TdmkEdit
      Left = 228
      Top = 24
      Width = 101
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      ReadOnly = True
      TabOrder = 3
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
    end
  end
  object GB_M: TGroupBox
    Left = 0
    Top = 0
    Width = 464
    Height = 48
    Align = alTop
    TabOrder = 1
    ExplicitLeft = -112
    ExplicitWidth = 576
    object LaTitulo1A: TLabel
      Left = 7
      Top = 9
      Width = 213
      Height = 32
      Caption = 'Juro Decomposto'
      Color = clBtnFace
      Font.Charset = ANSI_CHARSET
      Font.Color = clGradientActiveCaption
      Font.Height = -27
      Font.Name = 'Arial'
      Font.Style = []
      ParentColor = False
      ParentFont = False
      Visible = False
    end
    object LaTitulo1B: TLabel
      Left = 9
      Top = 11
      Width = 213
      Height = 32
      Caption = 'Juro Decomposto'
      Color = clBtnFace
      Font.Charset = ANSI_CHARSET
      Font.Color = clSilver
      Font.Height = -27
      Font.Name = 'Arial'
      Font.Style = []
      ParentColor = False
      ParentFont = False
    end
    object LaTitulo1C: TLabel
      Left = 8
      Top = 10
      Width = 213
      Height = 32
      Caption = 'Juro Decomposto'
      Color = clBtnFace
      Font.Charset = ANSI_CHARSET
      Font.Color = clHotLight
      Font.Height = -27
      Font.Name = 'Arial'
      Font.Style = []
      ParentColor = False
      ParentFont = False
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 113
    Width = 464
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitLeft = -112
    ExplicitTop = 109
    ExplicitWidth = 576
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 460
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 572
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 157
    Width = 464
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitLeft = -112
    ExplicitTop = 153
    ExplicitWidth = 576
    object PnSaiDesis: TPanel
      Left = 318
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 430
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 316
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 428
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        TabOrder = 0
        OnClick = BtOKClick
        NumGlyphs = 2
      end
    end
  end
end
