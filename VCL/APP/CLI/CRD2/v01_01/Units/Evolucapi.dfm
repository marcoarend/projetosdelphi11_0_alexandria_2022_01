object FmEvolucapi: TFmEvolucapi
  Left = 335
  Top = 176
  Caption = 'CAP-EVOLU-001 :: Evolu'#231#227'o do Capital'
  ClientHeight = 538
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 92
    Width = 1008
    Height = 446
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Dados'
      object Panel1: TPanel
        Left = 0
        Top = 342
        Width = 1000
        Height = 76
        Align = alBottom
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object PnEditSdoCC: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 76
          Align = alClient
          TabOrder = 1
          Visible = False
          object Label3: TLabel
            Left = 4
            Top = 6
            Width = 69
            Height = 13
            Caption = '$ Saldo conta:'
          end
          object Label6: TLabel
            Left = 88
            Top = 6
            Width = 67
            Height = 13
            Caption = '$ Saldo caixa:'
          end
          object BitBtn1: TBitBtn
            Tag = 14
            Left = 186
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Sai da janela atual'
            Caption = '&Confirma'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BitBtn1Click
          end
          object EdSaldoConta: TdmkEdit
            Left = 4
            Top = 23
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object BitBtn2: TBitBtn
            Tag = 15
            Left = 282
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Sai da janela atual'
            Caption = '&Desiste'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = BitBtn2Click
          end
          object EdSaldoCaixa: TdmkEdit
            Left = 88
            Top = 23
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
        end
        object PnAtualiza: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 76
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object GroupBox2: TGroupBox
            Left = 0
            Top = 0
            Width = 1000
            Height = 64
            Align = alTop
            Caption = ' Atualizar o gr'#225'fico: '
            TabOrder = 0
            Visible = False
            object Label4: TLabel
              Left = 8
              Top = 20
              Width = 93
              Height = 13
              Caption = 'Data compra inicial:'
            end
            object Label5: TLabel
              Left = 112
              Top = 20
              Width = 86
              Height = 13
              Caption = 'Data compra final:'
            end
            object TPIniB: TDateTimePicker
              Left = 8
              Top = 36
              Width = 101
              Height = 21
              Date = 38675.714976851900000000
              Time = 38675.714976851900000000
              TabOrder = 0
            end
            object TPFimB: TDateTimePicker
              Left = 112
              Top = 36
              Width = 101
              Height = 21
              Date = 38675.714976851900000000
              Time = 38675.714976851900000000
              TabOrder = 1
            end
            object BtGrafico: TBitBtn
              Tag = 262
              Left = 300
              Top = 16
              Width = 90
              Height = 40
              Caption = 'Atuali&za'
              NumGlyphs = 2
              TabOrder = 2
              OnClick = BtGraficoClick
            end
            object RGIntervalo: TRadioGroup
              Left = 218
              Top = 8
              Width = 75
              Height = 54
              Caption = ' Intervalo: '
              ItemIndex = 0
              Items.Strings = (
                'Di'#225'rio'
                'Mensal')
              TabOrder = 3
            end
          end
        end
      end
      object PnDados: TPanel
        Left = 0
        Top = 0
        Width = 1000
        Height = 342
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 1
        object GroupBox1: TGroupBox
          Left = 0
          Top = 0
          Width = 1000
          Height = 64
          Align = alTop
          Caption = ' Atualizar os dados do per'#237'odo: '
          TabOrder = 0
          object Label34: TLabel
            Left = 8
            Top = 20
            Width = 93
            Height = 13
            Caption = 'Data compra inicial:'
          end
          object Label1: TLabel
            Left = 112
            Top = 20
            Width = 86
            Height = 13
            Caption = 'Data compra final:'
          end
          object Label2: TLabel
            Left = 216
            Top = 20
            Width = 86
            Height = 13
            Caption = 'Coligado especial:'
          end
          object TPIniA: TDateTimePicker
            Left = 8
            Top = 36
            Width = 101
            Height = 21
            Date = 38675.714976851900000000
            Time = 38675.714976851900000000
            TabOrder = 0
          end
          object TPFimA: TDateTimePicker
            Left = 112
            Top = 36
            Width = 101
            Height = 21
            Date = 38675.714976851900000000
            Time = 38675.714976851900000000
            TabOrder = 1
          end
          object EdColigado: TdmkEditCB
            Left = 216
            Top = 36
            Width = 52
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            DBLookupComboBox = CBColigado
            IgnoraDBLookupComboBox = False
          end
          object CBColigado: TdmkDBLookupComboBox
            Left = 270
            Top = 36
            Width = 407
            Height = 21
            KeyField = 'Codigo'
            ListField = 'NOMECOLIGADO'
            ListSource = DsColigado
            TabOrder = 3
            dmkEditCB = EdColigado
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object BtOK: TBitBtn
            Tag = 14
            Left = 684
            Top = 16
            Width = 90
            Height = 40
            Caption = '&Atualiza'
            NumGlyphs = 2
            TabOrder = 4
            OnClick = BtOKClick
          end
        end
        object ProgressBar1: TProgressBar
          Left = 0
          Top = 325
          Width = 1000
          Height = 17
          Align = alBottom
          TabOrder = 1
          Visible = False
        end
        object DBGrid1: TDBGrid
          Left = 0
          Top = 64
          Width = 1000
          Height = 261
          Align = alClient
          DataSource = DsEvolucapi
          TabOrder = 2
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDblClick = DBGrid1DblClick
          OnKeyDown = DBGrid1KeyDown
          Columns = <
            item
              Expanded = False
              FieldName = 'DataE'
              Title.Caption = 'Data'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CARTEIREAL'
              Title.Caption = 'Carteira'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'RepassEspe'
              Title.Caption = 'Especial'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'RepassOutr'
              Title.Caption = 'Outros'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CHDevolAbe'
              Title.Caption = 'CH Devolv.'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DUDevolAbe'
              Title.Caption = 'DU Abertas'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SaldoConta'
              Title.Caption = 'Saldo conta'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SaldoCaixa'
              Title.Caption = 'Saldo caixa'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VALOR_TOTAL'
              Title.Caption = 'Total'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VALOR_SUB_T'
              Title.Caption = 'Total - outros'
              Width = 100
              Visible = True
            end>
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Gr'#225'fico'
      ImageIndex = 1
    end
    object TabSheet3: TTabSheet
      Caption = 'Cheques devolvidos orf'#227'os'
      ImageIndex = 2
      object DBGrid2: TDBGrid
        Left = 0
        Top = 0
        Width = 1000
        Height = 370
        Align = alClient
        DataSource = DsCHNull
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
      object Panel2: TPanel
        Left = 0
        Top = 370
        Width = 1000
        Height = 48
        Align = alBottom
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 1
        object BtExclui1: TBitBtn
          Tag = 12
          Left = 11
          Top = 4
          Width = 96
          Height = 40
          Cursor = crHandPoint
          Hint = 'Exclui entidade atual'
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtExclui1Click
        end
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'Cheques devolvidos precocemente'
      ImageIndex = 3
      object DBGrid3: TDBGrid
        Left = 0
        Top = 0
        Width = 1000
        Height = 370
        Align = alClient
        DataSource = DsCHPrecoce
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
      object Panel3: TPanel
        Left = 0
        Top = 370
        Width = 1000
        Height = 48
        Align = alBottom
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 1
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 242
        Height = 32
        Caption = 'Evolu'#231#227'o do Capital'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 242
        Height = 32
        Caption = 'Evolu'#231#227'o do Capital'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 242
        Height = 32
        Caption = 'Evolu'#231#227'o do Capital'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 48
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrCarteiTudo: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(li.Valor) Valor'
      'FROM lotcesits li'
      'LEFT JOIN alinits ai ON ai.ChequeOrigem=li.Controle'
      'WHERE li.DCompra <= :P0'
      'AND li.DDeposito >= :P1'
      'AND ai.ChequeOrigem IS NULL')
    Left = 532
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrCarteiTudoValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrEvolucapi: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEvolucapiCalcFields
    SQL.Strings = (
      'SELECT * FROM evolucapi'
      'ORDER BY DataE')
    Left = 532
    Top = 32
    object QrEvolucapiDataE: TDateField
      FieldName = 'DataE'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEvolucapiCarteiTudo: TFloatField
      FieldName = 'CarteiTudo'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucapiRepassEspe: TFloatField
      FieldName = 'RepassEspe'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucapiRepassOutr: TFloatField
      FieldName = 'RepassOutr'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucapiCARTEIREAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CARTEIREAL'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrEvolucapiCHDevolAbe: TFloatField
      FieldName = 'CHDevolAbe'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucapiDUDevolAbe: TFloatField
      FieldName = 'DUDevolAbe'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucapiVALOR_SUB_T: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VALOR_SUB_T'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrEvolucapiSaldoConta: TFloatField
      FieldName = 'SaldoConta'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucapiSaldoCaixa: TFloatField
      FieldName = 'SaldoCaixa'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucapiVALOR_TOTAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VALOR_TOTAL'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
  end
  object DsEvolucapi: TDataSource
    DataSet = QrEvolucapi
    Left = 560
    Top = 32
  end
  object QrColigado: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMECOLIGADO'
      'FROM entidades'
      'WHERE Fornece2="V"')
    Left = 724
    Top = 5
    object QrColigadoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrColigadoNOMECOLIGADO: TWideStringField
      FieldName = 'NOMECOLIGADO'
      Size = 100
    end
  end
  object DsColigado: TDataSource
    DataSet = QrColigado
    Left = 752
    Top = 5
  end
  object QrRepassOutr: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(lit.Valor) Valor'
      'FROM lotcesits lit'
      'LEFT JOIN repasits rit ON rit.Origem=lit.Controle '
      'LEFT JOIN repas    rep ON rep.Codigo=rit.Codigo'
      'WHERE lit.DCompra <= :P0'
      'AND lit.DDeposito >= :P1'
      'AND lit.Repassado=1'
      'AND rep.Data <= :P2'
      'AND rep.Coligado <> :P3')
    Left = 560
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrRepassOutrValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrRepassEspe: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(lit.Valor) Valor'
      'FROM lot esits lit'
      'LEFT JOIN repasits rit ON rit.Origem=lit.Controle '
      'LEFT JOIN repas    rep ON rep.Codigo=rit.Codigo'
      'WHERE lit.DCompra <= :P0'
      'AND lit.DDeposito >= :P1'
      'AND lit.Repassado=1'
      'AND rep.Data <= :P2'
      'AND rep.Coligado = :P3'
      '')
    Left = 588
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrRepassEspeValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrCHDevolAbe: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Valor+JurosV-ValPago) Valor'
      'FROM alinits'
      'WHERE Data1 < :P0'
      'AND ((Data3=0) OR (Data3 >:P1)) ')
    Left = 616
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrCHDevolAbeValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrDUDevolAbe: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(lit.Valor + lit.TotalJr - lit.TotalDs - '
      'lit.TotalPg) Valor'
      'FROM lot esits lit'
      'LEFT JOIN lot es lot ON lot.Codigo=lit.Codigo'
      'WHERE lot.Tipo=1'
      'AND lit.Vencto < :P0'
      'AND ((lit.Quitado in (0,1)) OR (lit.Data3 > :P1))')
    Left = 644
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrDUDevolAbeValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrEvolper: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEvolperCalcFields
    SQL.Strings = (
      'SELECT * FROM evolucapi'
      'WHERE DataE BETWEEN :P0 AND :P1'
      'AND MONTH(DataE) <> MONTH(DATE_ADD(DataE, INTERVAL 1 DAY))'
      'ORDER BY DataE')
    Left = 588
    Top = 32
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrEvolperDataE: TDateField
      FieldName = 'DataE'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEvolperCarteiTudo: TFloatField
      FieldName = 'CarteiTudo'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolperRepassEspe: TFloatField
      FieldName = 'RepassEspe'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolperRepassOutr: TFloatField
      FieldName = 'RepassOutr'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolperCARTEIREAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CARTEIREAL'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrEvolperCHDevolAbe: TFloatField
      FieldName = 'CHDevolAbe'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolperDUDevolAbe: TFloatField
      FieldName = 'DUDevolAbe'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolperVALOR_SUB_T: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VALOR_SUB_T'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrEvolperSaldoConta: TFloatField
      FieldName = 'SaldoConta'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolperVALOR_TOTAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VALOR_TOTAL'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrEvolperSaldoCaixa: TFloatField
      FieldName = 'SaldoCaixa'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object QrMax: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MAX(CarteiTudo+CHDevolAbe+DUDevolAbe+SaldoConta) TOTAL'
      'FROM evolucapi'
      'WHERE DataE BETWEEN :P0 AND :P1')
    Left = 616
    Top = 32
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrMaxTOTAL: TFloatField
      FieldName = 'TOTAL'
    end
  end
  object QrCHNull: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrCHNullAfterOpen
    BeforeClose = QrCHNullBeforeClose
    SQL.Strings = (
      'SELECT ai.Cliente, ai.Emitente, ai.CPF, ai.Banco, ai.Agencia, '
      'ai.Conta, ai.Cheque, ai.Valor, ai.Taxas, ai.Multa, '
      'ai.JurosV Juros, ai.Desconto, ai.ValPago, ai.ChequeOrigem, '
      'ai.LoteOrigem, ai.Codigo'
      'FROM alinits ai'
      'LEFT JOIN lot esits li ON li.Controle=ai.ChequeOrigem'
      'WHERE ai.Data3=0'
      'AND li.Quitado IS NULL')
    Left = 140
    Top = 112
    object QrCHNullCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrCHNullEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrCHNullCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrCHNullBanco: TIntegerField
      FieldName = 'Banco'
      Required = True
    end
    object QrCHNullAgencia: TIntegerField
      FieldName = 'Agencia'
      Required = True
    end
    object QrCHNullConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrCHNullCheque: TIntegerField
      FieldName = 'Cheque'
      Required = True
    end
    object QrCHNullValor: TFloatField
      FieldName = 'Valor'
      Required = True
    end
    object QrCHNullTaxas: TFloatField
      FieldName = 'Taxas'
      Required = True
    end
    object QrCHNullMulta: TFloatField
      FieldName = 'Multa'
      Required = True
    end
    object QrCHNullJuros: TFloatField
      FieldName = 'Juros'
      Required = True
    end
    object QrCHNullDesconto: TFloatField
      FieldName = 'Desconto'
      Required = True
    end
    object QrCHNullValPago: TFloatField
      FieldName = 'ValPago'
      Required = True
    end
    object QrCHNullChequeOrigem: TIntegerField
      FieldName = 'ChequeOrigem'
      Required = True
    end
    object QrCHNullLoteOrigem: TIntegerField
      FieldName = 'LoteOrigem'
      Required = True
    end
    object QrCHNullCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object DsCHNull: TDataSource
    DataSet = QrCHNull
    Left = 168
    Top = 112
  end
  object QrCHPrecoce: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM alinits ai'
      'LEFT JOIN lot esits li ON li.Controle=ai.ChequeOrigem'
      'WHERE li.DDeposito >= SYSDATE()'
      'AND ai.Data3=0')
    Left = 140
    Top = 140
  end
  object DsCHPrecoce: TDataSource
    DataSet = QrCHPrecoce
    Left = 168
    Top = 140
  end
end
