object FmCompJuros: TFmCompJuros
  Left = 405
  Top = 175
  Caption = 'C'#225'lculos de juros'
  ClientHeight = 354
  ClientWidth = 584
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 584
    Height = 240
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 573
    ExplicitHeight = 358
    object PainelDecomp: TPanel
      Left = 0
      Top = 168
      Width = 584
      Height = 72
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 1
      ExplicitTop = 225
      ExplicitWidth = 571
      ExplicitHeight = 132
      object Label11: TLabel
        Left = 12
        Top = 8
        Width = 48
        Height = 13
        Caption = 'Juro Final:'
      end
      object Label1: TLabel
        Left = 116
        Top = 8
        Width = 54
        Height = 13
        Caption = 'Dias Prazo:'
      end
      object Label2: TLabel
        Left = 188
        Top = 8
        Width = 32
        Height = 13
        Caption = 'Casas:'
      end
      object Label3: TLabel
        Left = 228
        Top = 8
        Width = 49
        Height = 13
        Caption = 'Taxa m'#234's:'
      end
      object EdJuros: TdmkEdit
        Left = 12
        Top = 24
        Width = 101
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        OnChange = EdJurosChange
      end
      object EdPrazD: TdmkEdit
        Left = 116
        Top = 24
        Width = 69
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 4
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        OnChange = EdPrazDChange
      end
      object EdCasas: TdmkEdit
        Left = 188
        Top = 24
        Width = 37
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ValMin = '0'
        ValMax = '6'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnChange = EdCasasChange
      end
      object EdTaxa: TdmkEdit
        Left = 228
        Top = 24
        Width = 101
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 3
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
    end
    object PainelDados: TPanel
      Left = 0
      Top = 48
      Width = 584
      Height = 72
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitLeft = 1
      ExplicitTop = 49
      ExplicitWidth = 571
      object Label4: TLabel
        Left = 12
        Top = 8
        Width = 27
        Height = 13
        Caption = 'Valor:'
      end
      object Label5: TLabel
        Left = 116
        Top = 8
        Width = 54
        Height = 13
        Caption = 'Dias Prazo:'
      end
      object Label6: TLabel
        Left = 364
        Top = 8
        Width = 37
        Height = 13
        Caption = '$ Juros:'
      end
      object Label7: TLabel
        Left = 260
        Top = 8
        Width = 39
        Height = 13
        Caption = '% Juros:'
      end
      object Label8: TLabel
        Left = 468
        Top = 8
        Width = 39
        Height = 13
        Caption = 'L'#237'quido:'
      end
      object Label9: TLabel
        Left = 188
        Top = 8
        Width = 63
        Height = 13
        Caption = 'Taxa mensal:'
      end
      object EdBaseT: TdmkEdit
        Left = 12
        Top = 24
        Width = 101
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        OnChange = EdBaseTChange
      end
      object EdPrazC: TdmkEdit
        Left = 116
        Top = 24
        Width = 69
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 4
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        OnChange = EdPrazCChange
      end
      object EdJuroV: TdmkEdit
        Left = 364
        Top = 24
        Width = 101
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
      object EdJuroP: TdmkEdit
        Left = 260
        Top = 24
        Width = 101
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 3
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
      object EdLiqui: TdmkEdit
        Left = 468
        Top = 24
        Width = 101
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 4
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
      object EdTaxaM: TdmkEdit
        Left = 188
        Top = 24
        Width = 69
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        OnChange = EdTaxaMChange
      end
    end
    object GB_M1: TGroupBox
      Left = 0
      Top = 0
      Width = 584
      Height = 48
      Align = alTop
      TabOrder = 2
      ExplicitLeft = 5
      ExplicitTop = -19
      ExplicitWidth = 571
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 176
        Height = 32
        Caption = 'Comp'#245'e Juros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 176
        Height = 32
        Caption = 'Comp'#245'e Juros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 176
        Height = 32
        Caption = 'Comp'#245'e Juros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
    object GB_M2: TGroupBox
      Left = 0
      Top = 120
      Width = 584
      Height = 48
      Align = alTop
      TabOrder = 3
      ExplicitLeft = 1
      ExplicitTop = 173
      ExplicitWidth = 571
      object LaTitulo2A: TLabel
        Left = 7
        Top = 9
        Width = 205
        Height = 32
        Caption = 'Decomp'#245'e Juros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo2B: TLabel
        Left = 9
        Top = 11
        Width = 205
        Height = 32
        Caption = 'Decomp'#245'e Juros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo2C: TLabel
        Left = 8
        Top = 10
        Width = 205
        Height = 32
        Caption = 'Decomp'#245'e Juros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 240
    Width = 584
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    ExplicitLeft = -57
    ExplicitTop = 289
    ExplicitWidth = 630
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 580
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 626
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 284
    Width = 584
    Height = 70
    Align = alBottom
    TabOrder = 2
    ExplicitLeft = -57
    ExplicitTop = 333
    ExplicitWidth = 630
    object PnSaiDesis: TPanel
      Left = 438
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 484
      object BitBtn1: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 436
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 482
    end
  end
end
