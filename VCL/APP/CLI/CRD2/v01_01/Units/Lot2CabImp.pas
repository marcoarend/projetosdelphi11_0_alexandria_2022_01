unit Lot2CabImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage,
  //PlannerMonthView, DBPlannerMonthView, Planner, DBPlanner,
  frxClass,
  UnDmkEnums;

type
  TFmLot2CabImp = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    RGModelo: TRadioGroup;
    frxBorTotCliE2: TfrxReport;
    frxBorTotCliE3: TfrxReport;
    frxBorTotCliE0: TfrxReport;
    frxBorTotCliE1: TfrxReport;
    frxBorTotCliBP2: TfrxReport;
    frxBorTotCliBP3: TfrxReport;
    frxBorTotCliBP0: TfrxReport;
    frxBorTotCliBP1: TfrxReport;
    frxBorTotCli2S: TfrxReport;
    frxBorTotCli3S: TfrxReport;
    frxBorTotCli0S: TfrxReport;
    frxBorTotCli1S: TfrxReport;
    RGDias: TRadioGroup;
    RGDescontoJuros: TRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure frxBorTotCliE2GetValue(const VarName: string; var Value: Variant);
    function frxBorTotCliE2UserFunction(const MethodName: string;
      var Params: Variant): Variant;
    procedure BtOKClick(Sender: TObject);
    procedure RGModeloClick(Sender: TObject);
  private
    { Private declarations }
    FContaST: Integer;
    procedure ImprimeBorderoClienteE2();
    procedure ImprimeBorderoClienteBP();
    procedure ImprimeBorderoClienteSobra(Dias, DesJur: Boolean);
    procedure DefineDataSets(frxReport: TfrxReport);
    procedure ImprimeBordero;
  public
    { Public declarations }
  end;

  var
  FmLot2CabImp: TFmLot2CabImp;

implementation

uses UnMyObjects, Module, Lot2Cab;

{$R *.DFM}

procedure TFmLot2CabImp.BtOKClick(Sender: TObject);
begin
  ImprimeBordero;
end;

procedure TFmLot2CabImp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLot2CabImp.DefineDataSets(frxReport: TfrxReport);
begin
  MyObjects.frxDefineDatasets(frxReport, [
    FmLot2Cab.frxDsBordero,
    FmLot2Cab.frxDsCHDevP,
    FmLot2Cab.frxDsDPago,
    FmLot2Cab.frxDsLot,
    FmLot2Cab.frxDsLotTxs,
    FmLot2Cab.frxDsOcorOP,
    FmLot2Cab.frxDsOcorP,
    FmLot2Cab.frxDsDPago
  ]);
end;

procedure TFmLot2CabImp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLot2CabImp.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  RGDias.Visible          := False;
  RGDescontoJuros.Visible := False;
end;

procedure TFmLot2CabImp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLot2CabImp.frxBorTotCliE2GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_HOJE' then
  begin
    Value := FormatDateTime(VAR_FORMATDATE2, Date);
    FContaST := 0;
  end
  else if VarName = 'VARF_CHQS' then
    Value := FmLot2Cab.QrLotIts.RecordCount
  else if VarName = 'VARF_MOEDA' then
    Value := Dmod.QrControleMoeda.Value
  else if VarName = 'VAR_CONTAST' then
  begin
    FContaST := FContaST +1;
    Value := FContaST;// repasse
  end else if VarName = 'VARF_CHDEV' then
    Value := FmLot2Cab.QrCHDevP.RecordCount
  else if VarName = 'VARF_DUDEV' then
    Value := FmLot2Cab.QrDPago.RecordCount
  else if VarName = 'VARF_NOMEEMP' then
  begin
    if Dmod.QrControleCodCliRel.Value = 1 then
      Value := FormatFloat('0', FmLot2Cab.QrLotcliente.Value) + ' - '
    else
      Value := '';
    case Dmod.QrControleTipNomeEmp.Value of
      0: Value := Value + FmLot2Cab.QrLotNOMECLIENTE.Value;
      1: Value := Value + FmLot2Cab.QrLotNOMEFANCLIENTE.Value;
    end;
  end;
end;

function TFmLot2CabImp.frxBorTotCliE2UserFunction(const MethodName: string;
  var Params: Variant): Variant;
begin
  if MethodName = 'VARF_CHDEV' then
    Params := FmLot2Cab.QrCHDevP.RecordCount
  else if MethodName = 'VARF_DUDEV' then
    Params := FmLot2Cab.QrDPago.RecordCount;
end;

procedure TFmLot2CabImp.ImprimeBordero;
var
  Dias, DesJur: Boolean;
begin
  Dias   := RGDias.ItemIndex = 0;
  DesJur := RGDescontoJuros.ItemIndex = 0;
  //
  case RGModelo.ItemIndex of
    0: ImprimeBorderoClienteE2();
    1: ImprimeBorderoClienteSobra(Dias, DesJur);
    2: ImprimeBorderoClienteBP();
  end;
end;

procedure TFmLot2CabImp.ImprimeBorderoClienteBP;
begin
  FContaST := 0;
  //
  FmLot2Cab.ReopenSumOP(FmLot2Cab.QrLotCodigo.Value);
  //
  if FmLot2Cab.QrLotMINAV.Value + FmLot2Cab.QrLotMINTC.Value >= 0.01 then
  begin
    if FmLot2Cab.QrLotTipo.Value = 0 then
    begin
      DefineDataSets(frxBorTotCliBP2);
      //
      frxBorTotCliBP2.Variables['VARF_CHDEV'] := FmLot2Cab.QrCHDevP.RecordCount;
      frxBorTotCliBP2.Variables['VARF_DUDEV'] := FmLot2Cab.QrDPago.RecordCount;
      case FmLot2Cab.FImprime of
        1: frxBorTotCliBP2.Print; 
        2: MyObjects.frxMostra(frxBorTotCliBP2, 'Border� cliente n� ' +
             Geral.FF0(FmLot2Cab.QrLotLote.Value));
      end;
    end else begin
      DefineDataSets(frxBorTotCliBP3);
      //
      frxBorTotCliBP3.Variables['VARF_CHDEV'] := FmLot2Cab.QrCHDevP.RecordCount;
      frxBorTotCliBP3.Variables['VARF_DUDEV'] := FmLot2Cab.QrDPago.RecordCount;
      case FmLot2Cab.FImprime of
        1: frxBorTotCliBP3.Print;
        2: MyObjects.frxMostra(frxBorTotCliBP3, 'Border� cliente n� ' +
             Geral.FF0(FmLot2Cab.QrLotLote.Value));
      end;
    end;
  end else begin
    if FmLot2Cab.QrLotTipo.Value = 0 then
    begin
      DefineDataSets(frxBorTotCliBP0);
      //
      frxBorTotCliBP0.Variables['VARF_CHDEV'] := FmLot2Cab.QrCHDevP.RecordCount;
      frxBorTotCliBP0.Variables['VARF_DUDEV'] := FmLot2Cab.QrDPago.RecordCount;
      case FmLot2Cab.FImprime of
        1: frxBorTotCliBP0.Print;
        2: MyObjects.frxMostra(frxBorTotCliBP0, 'Border� cliente n� ' +
             Geral.FF0(FmLot2Cab.QrLotLote.Value));
      end;
    end else begin
      DefineDataSets(frxBorTotCliBP1);
      //
      frxBorTotCliBP1.Variables['VARF_CHDEV'] := FmLot2Cab.QrCHDevP.RecordCount;
      frxBorTotCliBP1.Variables['VARF_DUDEV'] := FmLot2Cab.QrDPago.RecordCount;
      case FmLot2Cab.FImprime of
        1: frxBorTotCliBP1.Print;
        2: MyObjects.frxMostra(frxBorTotCliBP1, 'Border� cliente n� ' +
             Geral.FF0(FmLot2Cab.QrLotLote.Value));
      end;
    end;
  end;
end;

procedure TFmLot2CabImp.ImprimeBorderoClienteE2;
begin
  FContaST := 0;
  //
  FmLot2Cab.ReopenSumOP(FmLot2Cab.QrLotCodigo.Value);
  //
  if FmLot2Cab.QrLotMINAV.Value + FmLot2Cab.QrLotMINTC.Value>=0.01 then
  begin
    if FmLot2Cab.QrLotTipo.Value = 0 then
    begin
      DefineDataSets(frxBorTotCliE2);
      //
      frxBorTotCliE2.Variables['VARF_CHDEV'] := FmLot2Cab.QrCHDevP.RecordCount;
      frxBorTotCliE2.Variables['VARF_DUDEV'] := FmLot2Cab.QrDPago.RecordCount;
      case FmLot2Cab.FImprime of
        1: frxBorTotCliE2.Print;
        2: MyObjects.frxMostra(frxBorTotCliE2, 'Border� cliente n� ' +
             Geral.FF0(FmLot2Cab.QrLotLote.Value));
      end;
    end else begin
      DefineDataSets(frxBorTotCliE3);
      //
      frxBorTotCliE3.Variables['VARF_CHDEV'] := FmLot2Cab.QrCHDevP.RecordCount;
      frxBorTotCliE3.Variables['VARF_DUDEV'] := FmLot2Cab.QrDPago.RecordCount;
      case FmLot2Cab.FImprime of
        1: frxBorTotCliE3.Print;
        2: MyObjects.frxMostra(frxBorTotCliE3, 'Border� cliente n� ' +
             Geral.FF0(FmLot2Cab.QrLotLote.Value));
      end;
    end;
  end else begin
    if FmLot2Cab.QrLotTipo.Value = 0 then
    begin
      DefineDataSets(frxBorTotCliE0);
      //
      frxBorTotCliE0.Variables['VARF_CHDEV'] := FmLot2Cab.QrCHDevP.RecordCount;
      frxBorTotCliE0.Variables['VARF_DUDEV'] := FmLot2Cab.QrDPago.RecordCount;
      case FmLot2Cab.FImprime of
        1: frxBorTotCliE0.Print;
        2: MyObjects.frxMostra(frxBorTotCliE0, 'Border� cliente n� ' +
             Geral.FF0(FmLot2Cab.QrLotLote.Value));
      end;
    end else begin
      DefineDataSets(frxBorTotCliE1);
      //
      frxBorTotCliE1.Variables['VARF_CHDEV'] := FmLot2Cab.QrCHDevP.RecordCount;
      frxBorTotCliE1.Variables['VARF_DUDEV'] := FmLot2Cab.QrDPago.RecordCount;
      case FmLot2Cab.FImprime of
        1: frxBorTotCliE1.Print;
        2: MyObjects.frxMostra(frxBorTotCliE1, 'Border� cliente n� ' +
             Geral.FF0(FmLot2Cab.QrLotLote.Value));
      end;
    end;
  end;
end;

procedure TFmLot2CabImp.ImprimeBorderoClienteSobra(Dias, DesJur: Boolean);
begin
  FContaST := 0;
  //
  FmLot2Cab.ReopenSumOP(FmLot2Cab.QrLotCodigo.Value);
  //
  if FmLot2Cab.QrLotMINAV.Value + FmLot2Cab.QrLotMINTC.Value >= 0.01 then
  begin
    if FmLot2Cab.QrLotTipo.Value = 0 then
    begin
      DefineDataSets(frxBorTotCli2S);
      //
      frxBorTotCli2S.Variables['VARF_DIAS']   := Geral.BoolToInt(Dias);
      frxBorTotCli2S.Variables['VARF_DESJUR'] := Geral.BoolToInt(DesJur);
      frxBorTotCli2S.Variables['VARF_CHDEV']  := FmLot2Cab.QrCHDevP.RecordCount;
      frxBorTotCli2S.Variables['VARF_DUDEV']  := FmLot2Cab.QrDPago.RecordCount;
      case FmLot2Cab.FImprime of
        1: frxBorTotCli2S.Print;
        2: MyObjects.frxMostra(frxBorTotCli2S, 'Border� cliente n� ' +
             Geral.FF0(FmLot2Cab.QrLotLote.Value));
      end;
    end else begin
      DefineDataSets(frxBorTotCli3S);
      //    
      frxBorTotCli3S.Variables['VARF_DIAS']   := MLAGeral.BoolToInt(Dias);
      frxBorTotCli3S.Variables['VARF_DESJUR'] := Geral.BoolToInt(DesJur);
      frxBorTotCli3S.Variables['VARF_CHDEV']  := FmLot2Cab.QrCHDevP.RecordCount;
      frxBorTotCli3S.Variables['VARF_DUDEV']  := FmLot2Cab.QrDPago.RecordCount;
      case FmLot2Cab.FImprime of
        1: frxBorTotCli3S.Print;
        2: MyObjects.frxMostra(frxBorTotCli3S, 'Border� cliente n� ' +
             Geral.FF0(FmLot2Cab.QrLotLote.Value));
      end;
    end;
  end else begin
    if FmLot2Cab.QrLotTipo.Value = 0 then
    begin
      DefineDataSets(frxBorTotCli0S);
      //
      frxBorTotCli0S.Variables['VARF_DIAS']   := MLAGeral.BoolToInt(Dias);
      frxBorTotCli0S.Variables['VARF_DESJUR'] := Geral.BoolToInt(DesJur);
      frxBorTotCli0S.Variables['VARF_CHDEV']  := FmLot2Cab.QrCHDevP.RecordCount;
      frxBorTotCli0S.Variables['VARF_DUDEV']  := FmLot2Cab.QrDPago.RecordCount;
      case FmLot2Cab.FImprime of
        1: frxBorTotCli0S.Print;
        2: MyObjects.frxMostra(frxBorTotCli0S, 'Border� cliente n� ' +
             Geral.FF0(FmLot2Cab.QrLotLote.Value));
      end;
    end else begin
      DefineDataSets(frxBorTotCli1S);
      //
      frxBorTotCli1S.Variables['VARF_DIAS']   := MLAGeral.BoolToInt(Dias);
      frxBorTotCli1S.Variables['VARF_DESJUR'] := Geral.BoolToInt(DesJur);
      frxBorTotCli1S.Variables['VARF_CHDEV']  := FmLot2Cab.QrCHDevP.RecordCount;
      frxBorTotCli1S.Variables['VARF_DUDEV']  := FmLot2Cab.QrDPago.RecordCount;
      case FmLot2Cab.FImprime of
        1: frxBorTotCli1S.Print;
        2: MyObjects.frxMostra(frxBorTotCli1S, 'Border� cliente n� ' +
             Geral.FF0(FmLot2Cab.QrLotLote.Value));
      end;
    end;
  end;
end;

procedure TFmLot2CabImp.RGModeloClick(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := RGModelo.ItemIndex = 1;
  //
  RGDias.Visible          := Enab;
  RGDescontoJuros.Visible := Enab;
end;

end.
