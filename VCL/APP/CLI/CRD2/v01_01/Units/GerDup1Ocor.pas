unit GerDup1Ocor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  DmkDAC_PF, dmkImage, UnDmkEnums;

type
  TFmGerDup1Ocor = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    PainelOcor: TPanel;
    QrOcorBank: TmySQLQuery;
    QrOcorBankCodigo: TIntegerField;
    QrOcorBankNome: TWideStringField;
    QrOcorBankLk: TIntegerField;
    QrOcorBankDataCad: TDateField;
    QrOcorBankDataAlt: TDateField;
    QrOcorBankUserCad: TIntegerField;
    QrOcorBankUserAlt: TIntegerField;
    QrOcorBankBase: TFloatField;
    DsOcorBank: TDataSource;
    QrLocPr: TmySQLQuery;
    QrLocPrCodigo: TIntegerField;
    QrLocPrControle: TIntegerField;
    QrLocPrData1: TDateField;
    QrLocPrData2: TDateField;
    QrLocPrData3: TDateField;
    QrLocPrOcorrencia: TIntegerField;
    QrLocPrLk: TIntegerField;
    QrLocPrDataCad: TDateField;
    QrLocPrDataAlt: TDateField;
    QrLocPrUserCad: TIntegerField;
    QrLocPrUserAlt: TIntegerField;
    GBPror: TGroupBox;
    Label13: TLabel;
    TPDataN: TDateTimePicker;
    Label14: TLabel;
    EdTaxaPror: TdmkEdit;
    Label29: TLabel;
    EdJurosPeriodoN: TdmkEdit;
    Label15: TLabel;
    EdValorBase: TdmkEdit;
    Label27: TLabel;
    EdJurosPr: TdmkEdit;
    Label16: TLabel;
    TPDataI: TDateTimePicker;
    GroupBox2: TGroupBox;
    Label20: TLabel;
    EdOcorrencia: TdmkEditCB;
    CBOcorrencia: TdmkDBLookupComboBox;
    Label21: TLabel;
    TPDataO: TDateTimePicker;
    Label22: TLabel;
    EdValor: TdmkEdit;
    SpeedButton1: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure TPDataNChange(Sender: TObject);
    procedure EdTaxaProrChange(Sender: TObject);
    procedure EdValorBaseChange(Sender: TObject);
    procedure EdJurosPrChange(Sender: TObject);
    procedure TPDataIChange(Sender: TObject);
    procedure EdOcorrenciaExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
    procedure CalculaAPagarPrr();
    procedure CalculaJurosPrr();
    procedure ConfirmaOcorrencia();
  public
    { Public declarations }
    FCliente: Integer;
    procedure ConfiguraPr(Data: TDateTime);
  end;

  var
  FmGerDup1Ocor: TFmGerDup1Ocor;

implementation

uses UnMyObjects, Module, GerDup1Main, UMySQLModule, OcorBank, MyListas,
MyDBCheck;

{$R *.DFM}

procedure TFmGerDup1Ocor.BtOKClick(Sender: TObject);
var
  Data: Integer;
begin
  if GBPror.Visible then
  begin
    Data := Trunc(TPDataN.Date);
    if UMyMod.DiaInutil(Data) <> 0 then
    begin
      Geral.MensagemBox('A data ' + Geral.FDT(Data, 3) +
      ' n�o � um dia �til.', 'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
  end;
  //
  ConfirmaOcorrencia();
end;

procedure TFmGerDup1Ocor.BtSaidaClick(Sender: TObject);
begin
  FmGerDup1Main.ForcaOcorBank := 0;
  Close;
end;

procedure TFmGerDup1Ocor.CalculaAPagarPrr();
begin
  EdValor.ValueVariant := EdJurosPr.ValueVariant;
end;

procedure TFmGerDup1Ocor.CalculaJurosPrr();
var
  Prazo, Praz2: Integer;
  Taxa, Juros, Valor: Double;
begin
  Prazo := Trunc(Int(TPDataN.Date) - Int(TPDataO.Date));
  if Prazo < 0 then Praz2 := -Prazo else Praz2 := Prazo;
  begin
    Taxa  := EdTaxaPror.ValueVariant;
    Juros := MLAGeral.CalculaJuroComposto(Taxa, Praz2);
  end;
  Valor := EdValorBase.ValueVariant;
  EdJurosPeriodoN.ValueVariant := Juros;
  Juros := Juros * Valor / 100;
  if Prazo < 0 then Juros := Juros * -1;
  EdJurosPr.ValueVariant := Juros;
end;

procedure TFmGerDup1Ocor.ConfiguraPr(Data: TDateTime);
begin
{
  QrLocPr.Close;
  QrLocPr.Params[0].AsInteger := FmGerDup1Main.QrPesqFatParcela.Value;
  QrLocPr. Open;
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrlocPr, Dmod.MyDB, [
  'SELECT * FROM ' + CO_TabLotPrr,
  'WHERE Data3=( ',
  'SELECT Max(Data3) FROM ' + CO_TabLotPrr,
  'WHERE Codigo=' + FormatFloat('0',
  FmGerDup1Main.QrPesqFatParcela.Value) +') ',
  'ORDER BY Controle DESC ',
  '']);
  //
  TPDataN.MinDate := 0;
  if QrLocPr.RecordCount > 0 then
  begin
    TPDataN.Date := Int(QrLocPrData3.Value);
    TPDataO.Date := Int(QrLocPrData3.Value);
    TPDataI.Date := Int(QrLocPrData1.Value);
  end else begin
    TPDataN.Date := Int(FmGerDup1Main.QrPesqVencimento.Value);
    TPDataO.Date := Int(FmGerDup1Main.QrPesqVencimento.Value);
    TPDataI.Date := Int(FmGerDup1Main.QrPesqVencimento.Value);
  end;
  EdTaxaPror.ValueVariant :=
    Dmod.ObtemTaxaDeCompraCliente(FmGerDup1Main.QrPesqCliente.Value);
  EdValorBase.ValueVariant := FmGerDup1Main.QrPesqCredito.Value;
  // Permitir adiantamento
  //TPDataN.MinDate := TPDataO.Date;
  if Int(Data) > TPDataN.Date then TPDataN.Date := Int(Data);
  //TPdataN.SetFocus;
  CalculaJurosPrr;
end;

procedure TFmGerDup1Ocor.ConfirmaOcorrencia();
var
  //Prorr,
  Codigo, LOIS, Ocorrencia, Date2, Controle, ProrrVz, ProrrDd, Quitado,
  FatParcela: Integer;
  Valor: Double;
  DataO, Data1, Data2, Data3, Vencimento, DDeposito: String;
begin
{
  Prorr := FmGerDup1Main.QrLotPrrControle.Value;
  Dmod.QrUpd.SQL.Clear;
  if ImgTipo.SQLType = stIns then
  begin
    Dmod.QrUpd.SQL.Add('INSERT INTO ocorreu SET AlterWeb=1, ');
    Ocorr := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
      'Ocorreu', 'Ocorreu', 'Codigo');
  end else begin
    Dmod.QrUpd.SQL.Add('UPDATE ocorreu SET AlterWeb=1, ');
    Ocorr := FmGerDup1Main.QrOcorreuCodigo.Value;
  end;
  Dmod.QrUpd.SQL.Add('Lot esIts=:P0, DataO=:P1, Ocorrencia=:P2, Valor=:P3');
  Dmod.QrUpd.SQL.Add('');
  if ImgTipo.SQLType = stIns then
  begin
    Dmod.QrUpd.SQL.Add(', Codigo=:Pa');
  end else begin
    Dmod.QrUpd.SQL.Add('WHERE Codigo=:Pa');
  end;
  Dmod.QrUpd.Params[00].AsInteger := FmGerDup1Main.QrPesqFatParcela.Value;
  Dmod.QrUpd.Params[01].AsString  := FormatDateTime(VAR_FORMATDATE, TPDataO.Date);
  Dmod.QrUpd.Params[02].AsInteger := EdOcorrencia.ValueVariant;
  Dmod.QrUpd.Params[03].AsFloat   := EdValor.ValueVariant;
  Dmod.QrUpd.Params[04].AsInteger := Ocorr;
  Dmod.QrUpd.ExecSQL;
  //
}
  LOIS := FmGerDup1Main.QrPesqFatParcela.Value;
  DataO := Geral.FDT(TPDataO.Date, 1);
  Ocorrencia := EdOcorrencia.ValueVariant;
  Valor := EdValor.ValueVariant;
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('ocorreu', 'Codigo', ImgTipo.SQLType,
    FmGerDup1Main.QrOcorreuCodigo.Value);
  FmGerDup1Main.FOcorreu := Codigo;
  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'ocorreu', False, [
  FLD_LOIS, 'DataO', 'Cliente',
  'Ocorrencia', 'Valor'], [
  'Codigo'], [
  LOIS, DataO, FCliente,
  Ocorrencia, Valor], [
  Codigo], True);
  //
  if GBPror.Visible then
  begin
{
    Dmod.QrUpd.SQL.Clear;
    if ImgTipo.SQLType = stIns then
    begin
      Dmod.QrUpd.SQL.Add('INSERT INTO lot esprr SET ');
      Prorr := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
        'Lot esPrr', 'Lot esPrr', 'Controle');
      if FmGerDup1Main.QrLotPrr.RecordCount = 0 then
        Data2 := Trunc(TPDataI.Date)
      else
        Data2 := Trunc(FmGerDup1Main.QrLotPrrData3.Value);
    end else begin
      Dmod.QrUpd.SQL.Add('UPDATE lot esprr SET ');
      Prorr := FmGerDup1Main.QrLotPrrControle.Value;
      Data2 := Trunc(FmGerDup1Main.QrLotPrrData2.Value);
    end;
    Dmod.QrUpd.SQL.Add('Codigo=:P0, Data1=:P1, Data2=:P2, Data3=:P3, ');
    Dmod.QrUpd.SQL.Add('Ocorrencia=:P4');
    if ImgTipo.SQLType = stIns then
    begin
      Dmod.QrUpd.SQL.Add(', Controle=:Pa');
    end else begin
      Dmod.QrUpd.SQL.Add('WHERE Controle=:Pa');
    end;
    Dmod.QrUpd.Params[00].AsInteger := FmGerDup1Main.QrPesqFatParcela.Value;
    Dmod.QrUpd.Params[01].AsString  := FormatDateTime(VAR_FORMATDATE, TPDataI.Date);
    Dmod.QrUpd.Params[02].AsString  := FormatDateTime(VAR_FORMATDATE, Data2);//TPDataO.Date);
    Dmod.QrUpd.Params[03].AsString  := DataN;
    Dmod.QrUpd.Params[04].AsInteger := Ocorr;
    //
    Dmod.QrUpd.Params[05].AsInteger := Prorr;
    Dmod.QrUpd.ExecSQL;
}
    if ImgTipo.SQLType = stIns then
    begin
      if FmGerDup1Main.QrLotPrr.RecordCount = 0 then
        Date2 := Trunc(TPDataI.Date)
      else
        Date2 := Trunc(FmGerDup1Main.QrLotPrrData3.Value);
    end else
      Date2 := Trunc(FmGerDup1Main.QrLotPrrData2.Value);
    //
    Ocorrencia     := Codigo;
    Codigo         := FmGerDup1Main.QrPesqFatParcela.Value;
    Data1          := Geral.FDT(TPDataI.Date, 1);
    Data2          := Geral.FDT(Date2, 1);
    Data3          := Geral.FDT(TPDataN.Date, 1);
    //
    Controle := UMyMod.BuscaEmLivreY_Def(CO_TabLotPrr, 'Controle',
      ImgTipo.SQLType, FmGerDup1Main.QrLotPrrControle.Value);
    FmGerDup1Main.FLOIS := Controle;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, CO_TabLotPrr, False, [
    'Codigo', 'Data1', 'Data2',
    'Data3', 'Ocorrencia'], [
    'Controle'], [
    Codigo, Data1, Data2,
    Data3, Ocorrencia], [
    Controle], True);
    //
    {
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE lot esits SET AlterWeb=1,   DDeposito=:P0, ProrrVz=:P1, ');
    Dmod.QrUpd.SQL.Add('ProrrDd=:P2, Quitado=:P3 WHERE Controle=:P4');
    Dmod.QrUpd.Params[0].AsString  := DataN;
    Dmod.QrUpd.Params[1].AsInteger := FmGerDup1Main.QrLotPrr.RecordCount+1;
    Dmod.QrUpd.Params[2].AsInteger := Trunc(Int(TPDataN.Date)-Int(TPDataI.Date));
    Dmod.QrUpd.Params[3].AsInteger := -1; //Prorrogado
    //
    Dmod.QrUpd.Params[4].AsInteger := FmGerDup1Main.QrPesqFatParcela.Value;
    Dmod.QrUpd.ExecSQL;
    }
    //
    DDeposito  := Data3;
    ProrrVz    := FmGerDup1Main.QrLotPrr.RecordCount + 1;
    ProrrDd    := Trunc(Int(TPDataN.Date) - Int(TPDataI.Date));
    Quitado    := -1; //Prorrogado
    FatParcela := FmGerDup1Main.QrPesqFatParcela.Value;
    Vencimento := DDeposito;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_TabLctA, False, [
    'Vencimento', 'DDeposito', 'ProrrVz', 'ProrrDd', 'Quitado'], [
    'FatID', 'FatParcela'], [
    Vencimento, DDeposito, ProrrVz, ProrrDd, Quitado], [
    VAR_FATID_0301, FatParcela], True);
    //
    Close;
  end;
end;

procedure TFmGerDup1Ocor.EdJurosPrChange(Sender: TObject);
begin
  CalculaAPagarPrr();
end;

procedure TFmGerDup1Ocor.EdOcorrenciaExit(Sender: TObject);
begin
  if (ImgTipo.SQLType = stIns) and (GBPror.Visible = False) then
  EdValor.ValueVariant := QrOcorBankBase.Value;
end;

procedure TFmGerDup1Ocor.EdTaxaProrChange(Sender: TObject);
begin
  CalculaJurosPrr();
end;

procedure TFmGerDup1Ocor.EdValorBaseChange(Sender: TObject);
begin
  CalculaAPagarPrr();
end;

procedure TFmGerDup1Ocor.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
{
  if not FmGerDup1Main.FCallFromLot then
    EdValor.SetFocus;
}
  EdOcorrencia.SetFocus;
end;

procedure TFmGerDup1Ocor.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stNil;
  //
  TPDataI.Date := Date;
  TPDataN.Date := Date;
  TPDataO.Date := Date;
  //
  UMyMod.AbreQuery(QrOcorBank, Dmod.MyDB);
end;

procedure TFmGerDup1Ocor.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGerDup1Ocor.SpeedButton1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmOcorBank, FmOcorBank, afmoNegarComAviso) then
  begin
    FmOcorBank.ShowModal;
    FmOcorBank.Destroy;
    //
    UMyMod.SetaCodigoPesquisado(EdOcorrencia, CBOcorrencia, QrOcorBank, VAR_CADASTRO);
  end;
end;

procedure TFmGerDup1Ocor.TPDataIChange(Sender: TObject);
begin
  CalculaJurosPrr();
end;

procedure TFmGerDup1Ocor.TPDataNChange(Sender: TObject);
begin
  CalculaJurosPrr();
end;

end.
