unit GerChqDevo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, UnDmkEnums;

type
  TFmGerChqDevo = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    QrAlinea1: TmySQLQuery;
    QrAlinea1Codigo: TIntegerField;
    QrAlinea1Nome: TWideStringField;
    QrAlinea1Lk: TIntegerField;
    QrAlinea1DataCad: TDateField;
    QrAlinea1DataAlt: TDateField;
    QrAlinea1UserCad: TIntegerField;
    QrAlinea1UserAlt: TIntegerField;
    DsAlinea1: TDataSource;
    DsAlinea2: TDataSource;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    EdAlinea1: TdmkEditCB;
    EdAlinea2: TdmkEditCB;
    CBAlinea2: TdmkDBLookupComboBox;
    CBAlinea1: TdmkDBLookupComboBox;
    TPData1_1: TdmkEditDateTimePicker;
    TPData1_2: TdmkEditDateTimePicker;
    EdMulta1: TdmkEdit;
    EdJuros1: TdmkEdit;
    EdTaxas1: TdmkEdit;
    SpeedButton1: TSpeedButton;
    QrAlinea2: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    IntegerField2: TIntegerField;
    DateField1: TDateField;
    DateField2: TDateField;
    IntegerField3: TIntegerField;
    IntegerField4: TIntegerField;
    SpeedButton2: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
  private
    { Private declarations }
    procedure ConfirmaInclusaoDev();
    procedure ConfirmaAlteracaoDev();
  public
    { Public declarations }
  end;

  var
  FmGerChqDevo: TFmGerChqDevo;

implementation

uses UnMyObjects, Module, GerChqMain, UMySQLModule, MyListas, MyDBCheck, Alineas,
  ModuleFin, ModuleLot;

{$R *.DFM}

procedure TFmGerChqDevo.BtOKClick(Sender: TObject);
begin
  case ImgTipo.SQLType of
    stIns: ConfirmaInclusaoDev();
    stUpd: ConfirmaAlteracaoDev();
    else
    Geral.MensagemBox('A��o SQL n�o definida!', 'Aviso', MB_OK+MB_ICONWARNING);
  end;
end;

procedure TFmGerChqDevo.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGerChqDevo.ConfirmaAlteracaoDev();
var
  Alinea1, Alinea2, Codigo: Integer;
  Data1, Data2: String;
  Taxas, Multa, Juros: Double;
begin
  Taxas   := EdTaxas1.ValueVariant;
  Multa   := EdMulta1.ValueVariant;
  Juros   := EdJuros1.ValueVariant;
  Alinea1 := EdAlinea1.ValueVariant;
  Alinea2 := EdAlinea2.ValueVariant;
  //
  if Alinea1 > 0 then
    Data1 := FormatDateTime(VAR_FORMATDATE, TPData1_1.Date) else Data1 := '0000-00-00';
  if Alinea2 > 0 then
    Data2 := FormatDateTime(VAR_FORMATDATE, TPData1_2.Date) else Data2 := '0000-00-00';
  //
  Dmod.QrUpd.SQL.Clear;
  Codigo := FmGerChqMain.QrAlinItsCodigo.Value;
  Dmod.QrUpd.SQL.Add('UPDATE alinits SET AlterWeb=1, ');
  Dmod.QrUpd.SQL.Add('Alinea1=:P0, Alinea2=:P1, Data1=:P2, Data2=:P3, ');
  Dmod.QrUpd.SQL.Add('Taxas=:P4, Multa=:P5, JurosP=:P6 WHERE Codigo=:Pa');
  //
  Dmod.QrUpd.Params[00].AsInteger := Alinea1;
  Dmod.QrUpd.Params[01].AsInteger := Alinea2;
  Dmod.QrUpd.Params[02].AsString  := Data1;
  Dmod.QrUpd.Params[03].AsString  := Data2;
  Dmod.QrUpd.Params[04].AsFloat   := Taxas;
  Dmod.QrUpd.Params[05].AsFloat   := Multa;
  Dmod.QrUpd.Params[06].AsFloat   := Juros;
  //
  Dmod.QrUpd.Params[07].AsInteger := Codigo;
  Dmod.QrUpd.ExecSQL;
  //
  FmGerChqMain.FAlinIts := Codigo;
  //FmGerChqMain.Calcula Pagamento CHDev(FmGerChqMain.QrAlinItsData3.Value);
  DmLot.CalculaPagtoAlinIts(FmGerChqMain.QrAlinItsData3.Value, FmGerChqMain.QrAlinItsCodigo.Value);

  //
  FmGerChqMain.Pesquisa(True, False);
  //
  Close;
end;

procedure TFmGerChqDevo.ConfirmaInclusaoDev();
var
  Cliente, Alinea1, Alinea2, Codigo, Banco, Agencia, Cheque, Origem: Integer;
  Data1, Data2, Emitente, Conta, CPF, Vencto, DDeposi: String;
  Valor, Multa, Juros, Taxas: Double;
begin
  Cheque  := Trunc(FmGerChqMain.QrPesqDocumento.Value);//Geral.IMV(EdCheque1.Text);
  Origem  := FmGerChqMain.QrPesqFatParcela.Value;
  Valor   := FmGerChqMain.QrPesqCredito.Value;//Geral.DMV(EdValor1.Text);
  Multa   := EdMulta1.ValueVariant;
  Juros   := EdJuros1.ValueVariant;
  Taxas   := EdTaxas1.ValueVariant;
  Banco   := FmGerChqMain.QrPesqBanco.Value;//Geral.IMV(EdBanco1.Text);
  Agencia := FmGerChqMain.QrPesqAgencia.Value;//Geral.IMV(EdAgencia1.Text);
  Cliente := FmGerChqMain.QrPesqCliente.Value;//Geral.IMV(EdCliente1.Text);
  Emitente:= FmGerChqMain.QrPesqEmitente.Value;
  Conta   := FmGerChqMain.QrPesqContaCorrente.Value;
  CPF     := FmGerChqMain.QrPesqCNPJCPF.Value;
  Alinea1 := EdAlinea1.ValueVariant;
  Alinea2 := EdAlinea2.ValueVariant;
  Vencto  := Geral.FDT(FmGerChqMain.QrPesqVencimento.Value, 1);
  DDeposi := Geral.FDT(FmGerChqMain.QrPesqDDeposito.Value, 1);
  //
  if Alinea1 > 0 then
    Data1 := FormatDateTime(VAR_FORMATDATE, TPData1_1.Date) else Data1 := '0000-00-00';
  if Alinea2 > 0 then
    Data2 := FormatDateTime(VAR_FORMATDATE, TPData1_2.Date) else Data2 := '0000-00-00';
  //
  Dmod.QrUpd.SQL.Clear;
  Codigo := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
  'AlinIts', 'AlinIts', 'Codigo');
  Dmod.QrUpd.SQL.Add('INSERT INTO alinits SET AlterWeb=1, ');
  Dmod.QrUpd.SQL.Add('Alinea1=:P0, Alinea2=:P1, Data1=:P2, Data2=:P3, ');
  Dmod.QrUpd.SQL.Add('Cliente=:P4, Banco=:P5, Agencia=:P6, Conta=:P7, ');
  Dmod.QrUpd.SQL.Add('Cheque=:P8, CPF=:P9, Valor=:P10, Multa=:P11, ');
  Dmod.QrUpd.SQL.Add('Emitente=:P12, ChequeOrigem=:P13, JurosP=:P14, ');
  Dmod.QrUpd.SQL.Add('Taxas=:P15, Vencto=:P16, DDeposito=:P17, ');
  Dmod.QrUpd.SQL.Add('LoteOrigem=:P18');  // , ObsGerais=:P19
  Dmod.QrUpd.SQL.Add(', Codigo=:Pa');
  //
  Dmod.QrUpd.Params[00].AsInteger := Alinea1;
  Dmod.QrUpd.Params[01].AsInteger := Alinea2;
  Dmod.QrUpd.Params[02].AsString  := Data1;
  Dmod.QrUpd.Params[03].AsString  := Data2;
  Dmod.QrUpd.Params[04].AsInteger := Cliente;
  Dmod.QrUpd.Params[05].AsInteger := Banco;
  Dmod.QrUpd.Params[06].AsInteger := Agencia;
  Dmod.QrUpd.Params[07].AsString  := Conta;
  Dmod.QrUpd.Params[08].AsInteger := Cheque;
  Dmod.QrUpd.Params[09].AsString  := CPF;
  Dmod.QrUpd.Params[10].AsFloat   := Valor;
  Dmod.QrUpd.Params[11].AsFloat   := Multa;
  Dmod.QrUpd.Params[12].AsString  := Emitente;
  Dmod.QrUpd.Params[13].AsInteger := Origem;
  Dmod.QrUpd.Params[14].AsFloat   := Juros;
  Dmod.QrUpd.Params[15].AsFloat   := Taxas;
  Dmod.QrUpd.Params[16].AsString  := Vencto;
  Dmod.QrUpd.Params[17].AsString  := DDeposi;
  Dmod.QrUpd.Params[18].AsInteger := FmGerChqMain.QrPesqCodigo.Value;
  //Dmod.QrUpd.Params[19].AsString  := QrPesqObsGerais.Value;
  //
  Dmod.QrUpd.Params[19].AsInteger  := Codigo;
  Dmod.QrUpd.ExecSQL;
  //
{
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE lot esits SET AlterWeb=1,   Devolucao=:P0, ');
  Dmod.QrUpd.SQL.Add('Data3=0, Data4=0 WHERE Controle=:P1');
  Dmod.QrUpd.Params[00].AsInteger := Codigo;
  Dmod.QrUpd.Params[01].AsInteger := Origem;
  Dmod.QrUpd.ExecSQL;
}
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_TabLctA, False, [
  'Sit', 'Devolucao', 'Data3', 'Data4'], [
  'FatID', 'FatParcela'], [
  6(*Devolvido*), Codigo, 0, 0], [
  VAR_FATID_0301, Origem], True);
  //
  //incluir na tabela de LctStep
  DmodFin.FolowStepLct(FmGerChqMain.QrPesqControle.Value, 0, CO_STEP_LCT_DEVOLU);
  //
  FmGerChqMain.FAlinIts := Codigo;
  //
  FmGerChqMain.Pesquisa(True, False);
  //
  Close;
end;

procedure TFmGerChqDevo.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGerChqDevo.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stNil;
  UMyMod.AbreQuery(QrAlinea1, Dmod.MyDB);
  UMyMod.AbreQuery(QrAlinea2, Dmod.MyDB);
end;

procedure TFmGerChqDevo.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGerChqDevo.SpeedButton1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmAlineas, FmAlineas, afmoNegarComAviso) then
  begin
    FmAlineas.ShowModal;
    FmAlineas.Destroy;
    //
    UMyMod.SetaCodigoPesquisado(EdAlinea1, CBAlinea1, QrAlinea1, VAR_CADASTRO);
  end;
end;

procedure TFmGerChqDevo.SpeedButton2Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmAlineas, FmAlineas, afmoNegarComAviso) then
  begin
    FmAlineas.ShowModal;
    FmAlineas.Destroy;
    //
    UMyMod.SetaCodigoPesquisado(EdAlinea2, CBAlinea2, QrAlinea2, VAR_CADASTRO);
  end;
end;

end.
