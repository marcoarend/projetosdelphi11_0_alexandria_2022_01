unit RepasLoc;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UnMLAGeral, UnGOTOy, Mask, UMySQLModule, mySQLDbTables, Grids, DBGrids,
  ComCtrls, dmkEdit, dmkEditCB, dmkDBLookupComboBox, dmkGeral, dmkImage,
  UnDmkProcFunc, UnDmkEnums;

type
  TFmRepasLoc = class(TForm)
    PainelDados: TPanel;
    Label2: TLabel;
    CBColigado: TdmkDBLookupComboBox;
    DsClientes: TDataSource;
    EdColigado: TdmkEditCB;
    QrColigados: TmySQLQuery;
    QrColigadosNOMEENTIDADE: TWideStringField;
    QrColigadosCodigo: TIntegerField;
    QrLoc: TmySQLQuery;
    DBGrid1: TDBGrid;
    DsLoc: TDataSource;
    TPIni: TDateTimePicker;
    Label34: TLabel;
    TPFim: TDateTimePicker;
    Label4: TLabel;
    QrLocNOMECOLIGADO: TWideStringField;
    QrLocCNPJCPF: TWideStringField;
    QrLocCodigo: TIntegerField;
    QrLocColigado: TIntegerField;
    QrLocData: TDateField;
    QrLocTotal: TFloatField;
    QrLocJurosV: TFloatField;
    QrLocLk: TIntegerField;
    QrLocDataCad: TDateField;
    QrLocDataAlt: TDateField;
    QrLocUserCad: TIntegerField;
    QrLocUserAlt: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtDesiste: TBitBtn;
    Panel1: TPanel;
    BtConfirma: TBitBtn;
    procedure BtDesisteClick(Sender: TObject);
    procedure EdColigadoChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure QrLocAfterOpen(DataSet: TDataSet);
    procedure BtConfirmaClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure TPIniChange(Sender: TObject);
    procedure TPFimChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ReopenLoc;
  end;

var
  FmRepasLoc: TFmRepasLoc;

implementation

uses UnMyObjects, Module, Entidades, Principal;

{$R *.DFM}

procedure TFmRepasLoc.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmRepasLoc.EdColigadoChange(Sender: TObject);
begin
  ReopenLoc;
end;

procedure TFmRepasLoc.ReopenLoc;
var
  Coligado: Integer;
begin
  Coligado := EdColigado.ValueVariant;
  QrLoc.SQL.Clear;
  QrLoc.SQL.Add('SELECT CASE WHEN en.Tipo=0 THEN en.RazaoSocial');
  QrLoc.SQL.Add('ELSE en.Nome END NOMECOLIGADO,');
  QrLoc.SQL.Add('CASE WHEN en.Tipo=0 THEN en.CNPJ');
  QrLoc.SQL.Add('ELSE en.CPF END CNPJCPF, re.*');
  QrLoc.SQL.Add('FROM repas re');
  QrLoc.SQL.Add('LEFT JOIN entidades en ON en.Codigo=re.Coligado');
  QrLoc.SQL.Add(dmkPF.SQL_Periodo('WHERE re.Data ',
    TPIni.Date, TPFim.Date, True, True));
  if Coligado <> 0 then
    QrLoc.SQL.Add('AND re.Coligado='+IntToStr(Coligado));
  QrLoc.SQL.Add('ORDER BY Codigo DESC');
  QrLoc.SQL.Add('');
  UMyMod.AbreQuery(QrLoc, Dmod.MyDB);
  //
end;

procedure TFmRepasLoc.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  //
  UMyMod.AbreQuery(QrColigados, Dmod.MyDB);
  TPIni.Date := Date-30;
  TPFim.Date := Date;
  //
  ReopenLoc;
end;

procedure TFmRepasLoc.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmRepasLoc.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmRepasLoc.QrLocAfterOpen(DataSet: TDataSet);
begin
  BtConfirma.Enabled := Geral.IntToBool_0(QrLoc.RecordCount);
end;

procedure TFmRepasLoc.BtConfirmaClick(Sender: TObject);
begin
  FmPrincipal.FLoteLoc := QrLocCodigo.Value;
  Close;
end;

procedure TFmRepasLoc.DBGrid1DblClick(Sender: TObject);
begin
  BtConfirmaClick(Self);
end;

procedure TFmRepasLoc.TPIniChange(Sender: TObject);
begin
  ReopenLoc;
end;

procedure TFmRepasLoc.TPFimChange(Sender: TObject);
begin
  ReopenLoc;
end;

end.

