object FmGerCliMain: TFmGerCliMain
  Left = 339
  Top = 185
  Caption = 'CLI-CNTRL-001 :: Gerenciamento de Ocorr'#234'ncias em Clientes'
  ClientHeight = 526
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 530
        Height = 32
        Caption = 'Gerenciamento de Ocorr'#234'ncias em Clientes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 530
        Height = 32
        Caption = 'Gerenciamento de Ocorr'#234'ncias em Clientes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 530
        Height = 32
        Caption = 'Gerenciamento de Ocorr'#234'ncias em Clientes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 52
    Width = 1008
    Height = 366
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 366
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 366
        Align = alClient
        TabOrder = 0
        object PainelData: TPanel
          Left = 2
          Top = 67
          Width = 1004
          Height = 297
          Align = alClient
          BevelOuter = bvLowered
          TabOrder = 0
          object DBGrid5: TDBGrid
            Left = 1
            Top = 1
            Width = 1002
            Height = 191
            Align = alClient
            DataSource = DsOcorreu
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'TIPODOC'
                Title.Caption = 'TD'
                Width = 20
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DOCUM_TXT'
                Title.Caption = 'Documento / Descri'#231#227'o'
                Width = 160
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DCompra'
                Title.Caption = 'Compra'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMEOCORRENCIA'
                Title.Caption = 'Ocorr'#234'ncia'
                Width = 180
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DataO'
                Title.Caption = 'Data'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Valor'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'TaxaV'
                Title.Caption = '$ Taxa'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pago'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SALDO'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Data3'
                Title.Caption = 'Ult.Pg'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ATUALIZADO'
                Title.Caption = 'Atualiz.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Emitente'
                Width = 180
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CPF'
                Title.Caption = 'CNPJ / CPF'
                Width = 113
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Emissao'
                Title.Caption = 'Emitido'
                Width = 56
                Visible = True
              end>
          end
          object Panel5: TPanel
            Left = 1
            Top = 192
            Width = 1002
            Height = 104
            Align = alBottom
            BevelOuter = bvLowered
            TabOrder = 1
            object DBGrid3_: TDBGrid
              Left = 1
              Top = 1
              Width = 333
              Height = 102
              Align = alLeft
              DataSource = DsOcorP
              TabOrder = 0
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Data'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'MoraVal'
                  Title.Caption = 'Juros'
                  Width = 88
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Pago'
                  Width = 88
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'FatNum'
                  Title.Caption = 'Lote pagto.'
                  Width = 60
                  Visible = True
                end>
            end
            object Panel6: TPanel
              Left = 334
              Top = 1
              Width = 667
              Height = 102
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 1
              object GroupBox2: TGroupBox
                Left = 0
                Top = 0
                Width = 667
                Height = 54
                Align = alTop
                Caption = ' Totais pesquisa: '
                TabOrder = 0
                object Label1: TLabel
                  Left = 5
                  Top = 14
                  Width = 27
                  Height = 13
                  Caption = 'Valor:'
                end
                object Label2: TLabel
                  Left = 91
                  Top = 14
                  Width = 41
                  Height = 13
                  Caption = '$ Taxas:'
                end
                object Label3: TLabel
                  Left = 177
                  Top = 14
                  Width = 28
                  Height = 13
                  Caption = 'Pago:'
                end
                object Label4: TLabel
                  Left = 263
                  Top = 14
                  Width = 39
                  Height = 13
                  Caption = 'SALDO:'
                end
                object Label5: TLabel
                  Left = 351
                  Top = 15
                  Width = 71
                  Height = 13
                  Caption = 'ATUALIZADO:'
                end
                object EdVal: TdmkEdit
                  Left = 5
                  Top = 30
                  Width = 85
                  Height = 21
                  Alignment = taRightJustify
                  ReadOnly = True
                  TabOrder = 0
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdTxV: TdmkEdit
                  Left = 91
                  Top = 30
                  Width = 85
                  Height = 21
                  Alignment = taRightJustify
                  ReadOnly = True
                  TabOrder = 1
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdPag: TdmkEdit
                  Left = 177
                  Top = 30
                  Width = 85
                  Height = 21
                  Alignment = taRightJustify
                  ReadOnly = True
                  TabOrder = 2
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdSal: TdmkEdit
                  Left = 263
                  Top = 30
                  Width = 85
                  Height = 21
                  Alignment = taRightJustify
                  ReadOnly = True
                  TabOrder = 3
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdAtz: TdmkEdit
                  Left = 351
                  Top = 30
                  Width = 85
                  Height = 21
                  Alignment = taRightJustify
                  ReadOnly = True
                  TabOrder = 4
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
              object BtBordero: TBitBtn
                Tag = 22
                Left = 4
                Top = 57
                Width = 90
                Height = 40
                Cursor = crHandPoint
                Caption = '&Border'#244
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                NumGlyphs = 2
                ParentFont = False
                ParentShowHint = False
                ShowHint = True
                TabOrder = 1
                OnClick = BtBorderoClick
              end
              object BitBtn1: TBitBtn
                Tag = 5
                Left = 96
                Top = 57
                Width = 90
                Height = 40
                Cursor = crHandPoint
                Caption = '&Recibo'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                NumGlyphs = 2
                ParentFont = False
                ParentShowHint = False
                ShowHint = True
                TabOrder = 2
                OnClick = BitBtn1Click
              end
            end
          end
        end
        object PainelPesq: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 52
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object Label75: TLabel
            Left = 8
            Top = 4
            Width = 35
            Height = 13
            Caption = 'Cliente:'
          end
          object EdCliente: TdmkEditCB
            Left = 8
            Top = 20
            Width = 52
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdClienteChange
            DBLookupComboBox = CBCliente
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBCliente: TdmkDBLookupComboBox
            Left = 64
            Top = 20
            Width = 377
            Height = 21
            KeyField = 'Codigo'
            ListField = 'NOMECLIENTE'
            ListSource = DsClientes
            TabOrder = 1
            dmkEditCB = EdCliente
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object CGTipo: TdmkCheckGroup
            Left = 444
            Top = 1
            Width = 227
            Height = 50
            Caption = ' Tipo de ocorr'#234'ncia: '
            Columns = 3
            Items.Strings = (
              'Direto'
              'Cheques'
              'Duplicatas')
            TabOrder = 2
            OnClick = CGTipoClick
            UpdType = utYes
            Value = 0
            OldValor = 0
          end
          object CkAbertas: TCheckBox
            Left = 676
            Top = 20
            Width = 105
            Height = 17
            Caption = 'Somente abertas.'
            Checked = True
            State = cbChecked
            TabOrder = 3
            OnClick = CkAbertasClick
          end
          object TPIni: TDateTimePicker
            Left = 780
            Top = 23
            Width = 109
            Height = 21
            Date = 38698.000000000000000000
            Time = 0.582684641201922200
            TabOrder = 4
          end
          object TPFim: TDateTimePicker
            Left = 892
            Top = 23
            Width = 109
            Height = 21
            Date = 38698.000000000000000000
            Time = 0.582684641201922200
            TabOrder = 5
          end
          object CkIni: TCheckBox
            Left = 780
            Top = 4
            Width = 113
            Height = 17
            Caption = 'Data Ocorr. inicial:'
            TabOrder = 6
            OnClick = CkAbertasClick
          end
          object CkFim: TCheckBox
            Left = 892
            Top = 4
            Width = 113
            Height = 17
            Caption = 'Data Ocorr. final:'
            TabOrder = 7
            OnClick = CkAbertasClick
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 418
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBCntrl: TGroupBox
    Left = 0
    Top = 462
    Width = 1008
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel8: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 0
      object BtExclui: TBitBtn
        Tag = 12
        Left = 188
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Exclui'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtExcluiClick
      end
      object BtAltera: TBitBtn
        Tag = 11
        Left = 96
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Altera'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtAlteraClick
      end
      object BtIncluiOcor: TBitBtn
        Tag = 10
        Left = 4
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Inclui'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtIncluiOcorClick
      end
      object Panel9: TPanel
        Left = 895
        Top = 0
        Width = 109
        Height = 47
        Align = alRight
        Alignment = taRightJustify
        BevelOuter = bvNone
        TabOrder = 3
        object BitBtn2: TBitBtn
          Tag = 13
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
    end
  end
  object PMImprime: TPopupMenu
    Left = 16
    Top = 8
    object Selecionados1: TMenuItem
      Caption = '&Selecionados'
      OnClick = Selecionados1Click
    end
    object odos1: TMenuItem
      Caption = '&Todos'
      OnClick = odos1Click
    end
  end
  object frxOcorreuC: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40420.448944745400000000
    ReportOptions.LastChange = 40420.695853043980000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 49
    Top = 9
    Datasets = <
      item
        DataSet = frxDsClientes
        DataSetName = 'frxDsClientes'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsPesq1
        DataSetName = 'frxDsPesq1'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 64.252010000000000000
        Top = 18.897650000000000000
        Width = 1028.032160000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 1028.032160000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 1012.914040000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 1028.032160000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 725.669760000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'OCORR'#202'NCIAS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 876.850960000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Top = 45.354360000000000000
          Width = 1020.473100000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsClientes."NOMECLIENTE"]')
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 17.000000000000000000
        Top = 143.622140000000000000
        Width = 1028.032160000000000000
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Width = 26.456692910000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'TD')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456607480000000000
          Width = 113.385826770000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Documen. / Descri.')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 321.259700940000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393488430000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
          WordWrap = False
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 491.338409370000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'SALDO')
          ParentFont = False
          WordWrap = False
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 551.810945510000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ult.Pg')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 139.842610000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Compra')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976500000000000000
          Width = 132.283464570000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Ocorr'#234'ncia')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 430.866420000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pago')
          ParentFont = False
          WordWrap = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 876.850676850000000000
          Width = 102.047244090000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'CPF / CNPJ')
          ParentFont = False
          WordWrap = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 672.755905510000000000
          Width = 204.094534570000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Emitente')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 978.898270000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Emitido')
          ParentFont = False
          WordWrap = False
        end
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Atualiz.')
          ParentFont = False
          WordWrap = False
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 28.338582680000000000
        Top = 226.771800000000000000
        Width = 1028.032160000000000000
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393940000000000000
          Top = 11.338582680000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq1."Valor">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 430.866420000000000000
          Top = 11.338582680000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq1."Pago">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 491.338900000000000000
          Top = 11.338582680000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq1."SALDO">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Top = 11.338582680000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq1."ATUALIZADO">)]')
          ParentFont = False
          WordWrap = False
        end
        object Line2: TfrxLineView
          AllowVectorExport = True
          Top = 6.000000000000000000
          Width = 1029.921460000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 2.000000000000000000
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Top = 11.338582680000000000
          Width = 370.393900940000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'TOTAL')
          ParentFont = False
          WordWrap = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 672.755866460000000000
          Top = 11.338582680000000000
          Width = 355.275780940000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 551.811380000000000000
          Top = 11.338590000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 17.000000000000000000
        Top = 185.196970000000000000
        Width = 1028.032160000000000000
        DataSet = frxDsPesq1
        DataSetName = 'frxDsPesq1'
        RowCount = 0
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Width = 26.456692910000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPesq1."TIPODOC"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456607480000000000
          Width = 113.385826770000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsPesq1."DOCUM_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 321.259700940000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPesq1."DataO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393488430000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq1."Valor"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 491.338409370000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq1."SALDO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 551.810945510000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPesq1."Data3"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 139.842610000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPesq1."DCompra"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976500000000000000
          Width = 132.283464570000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsPesq1."NOMEOCORRENCIA"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 430.866420000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq1."Pago"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 876.850676850000000000
          Width = 102.047244090000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsPesq1."CPF"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 672.755905510000000000
          Width = 204.094534570000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsPesq1."Emitente"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 978.898270000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPesq1."Emissao"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq1."ATUALIZADO"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 317.480520000000000000
        Width = 1028.032160000000000000
        OnAfterPrint = 'PageFooter1OnAfterPrint'
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Width = 1028.032160000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
    end
  end
  object QrClientes: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMECLIENTE, Codigo, FatorCompra'
      'FROM entidades'
      'WHERE Cliente1="V"'
      'ORDER BY NOMECLIENTE')
    Left = 161
    Top = 33
    object QrClientesNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
    end
    object QrClientesTAXA_COMPRA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TAXA_COMPRA'
      Calculated = True
    end
    object QrClientesFatorCompra: TFloatField
      FieldName = 'FatorCompra'
      Origin = 'entidades.FatorCompra'
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 189
    Top = 33
  end
  object frxDsClientes: TfrxDBDataset
    UserName = 'frxDsClientes'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NOMECLIENTE=NOMECLIENTE'
      'Codigo=Codigo'
      'TAXA_COMPRA=TAXA_COMPRA'
      'FatorCompra=FatorCompra')
    DataSource = DsClientes
    BCDToCurrency = False
    
    Left = 217
    Top = 33
  end
  object QrPesq1: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrPesq1CalcFields
    SQL.Strings = (
      'ELT(TpOcor, "CH", "DU", "CL", "??") TIPODOC'
      'ob.Nome NOMEOCORRENCIA, ocr.* '
      'FROM ocorreu ocr '
      'LEFT JOIN ocorbank ob ON ob.Codigo   = ocr.Ocorrencia '
      'WHERE ocr.Codigo > 0  '
      'AND ocr.Codigo IN(4979)'
      ''
      ''
      ''
      '')
    Left = 269
    Top = 9
    object QrPesq1TIPODOC: TWideStringField
      FieldName = 'TIPODOC'
      Required = True
      Size = 2
    end
    object QrPesq1NOMEOCORRENCIA: TWideStringField
      FieldName = 'NOMEOCORRENCIA'
      Size = 50
    end
    object QrPesq1Banco: TIntegerField
      FieldName = 'Banco'
    end
    object QrPesq1Agencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrPesq1Cheque: TIntegerField
      FieldName = 'Cheque'
    end
    object QrPesq1Duplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 12
    end
    object QrPesq1Conta: TWideStringField
      FieldName = 'Conta'
    end
    object QrPesq1SALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrPesq1Emissao: TDateField
      FieldName = 'Emissao'
    end
    object QrPesq1DCompra: TDateField
      FieldName = 'DCompra'
    end
    object QrPesq1Vencto: TDateField
      FieldName = 'Vencto'
    end
    object QrPesq1DDeposito: TDateField
      FieldName = 'DDeposito'
    end
    object QrPesq1Emitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrPesq1CPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrPesq1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPesq1Cliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrPesq1DataO: TDateField
      FieldName = 'DataO'
    end
    object QrPesq1Ocorrencia: TIntegerField
      FieldName = 'Ocorrencia'
    end
    object QrPesq1Valor: TFloatField
      FieldName = 'Valor'
    end
    object QrPesq1LoteQuit: TIntegerField
      FieldName = 'LoteQuit'
    end
    object QrPesq1TaxaB: TFloatField
      FieldName = 'TaxaB'
    end
    object QrPesq1TaxaP: TFloatField
      FieldName = 'TaxaP'
    end
    object QrPesq1TaxaV: TFloatField
      FieldName = 'TaxaV'
    end
    object QrPesq1Pago: TFloatField
      FieldName = 'Pago'
    end
    object QrPesq1DataP: TDateField
      FieldName = 'DataP'
    end
    object QrPesq1Data3: TDateField
      FieldName = 'Data3'
    end
    object QrPesq1Status: TSmallintField
      FieldName = 'Status'
    end
    object QrPesq1Descri: TWideStringField
      FieldName = 'Descri'
      Size = 30
    end
    object QrPesq1MoviBank: TIntegerField
      FieldName = 'MoviBank'
    end
    object QrPesq1Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPesq1DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPesq1DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPesq1UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPesq1UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPesq1AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrPesq1ATUALIZADO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATUALIZADO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrPesq1DOCUM_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DOCUM_TXT'
      Size = 100
      Calculated = True
    end
    object QrPesq1Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object frxDsPesq1: TfrxDBDataset
    UserName = 'frxDsPesq1'
    CloseDataSource = False
    FieldAliases.Strings = (
      'TIPODOC=TIPODOC'
      'NOMEOCORRENCIA=NOMEOCORRENCIA'
      'Banco=Banco'
      'Agencia=Agencia'
      'Cheque=Cheque'
      'Duplicata=Duplicata'
      'Conta=Conta'
      'Tipo=Tipo'
      'SALDO=SALDO'
      'Emissao=Emissao'
      'DCompra=DCompra'
      'Vencto=Vencto'
      'DDeposito=DDeposito'
      'Emitente=Emitente'
      'CPF=CPF'
      'CLIENTELOTE=CLIENTELOTE'
      'Codigo=Codigo'
      'Cliente=Cliente'
      'DataO=DataO'
      'Ocorrencia=Ocorrencia'
      'Valor=Valor'
      'LoteQuit=LoteQuit'
      'TaxaB=TaxaB'
      'TaxaP=TaxaP'
      'TaxaV=TaxaV'
      'Pago=Pago'
      'DataP=DataP'
      'Data3=Data3'
      'Status=Status'
      'Descri=Descri'
      'MoviBank=MoviBank'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'ATUALIZADO=ATUALIZADO'
      'DOCUM_TXT=DOCUM_TXT'
      'Ativo=Ativo')
    DataSet = QrPesq1
    BCDToCurrency = False
    
    Left = 297
    Top = 9
  end
  object QrOcorreu: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrOcorreuAfterOpen
    BeforeClose = QrOcorreuBeforeClose
    AfterScroll = QrOcorreuAfterScroll
    OnCalcFields = QrOcorreuCalcFields
    SQL.Strings = (
      'SELECT ELT(TpOcor, "CH", "DU", "CL", "??") TIPODOC, '
      'ob.Nome NOMEOCORRENCIA, ob.PlaGen,'
      'oc.Lot esIts LOIS, oc.*'
      'FROM ocorreu oc '
      'LEFT JOIN ocorbank ob ON ob.Codigo = oc.Ocorrencia '
      '')
    Left = 565
    Top = 9
    object QrOcorreuTIPODOC: TWideStringField
      FieldName = 'TIPODOC'
      Size = 2
    end
    object QrOcorreuNOMEOCORRENCIA: TWideStringField
      FieldName = 'NOMEOCORRENCIA'
      Origin = 'ocorbank.Nome'
      Size = 50
    end
    object QrOcorreuCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'ocorreu.Codigo'
      Required = True
    end
    object QrOcorreuDataO: TDateField
      FieldName = 'DataO'
      Origin = 'ocorreu.DataO'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrOcorreuOcorrencia: TIntegerField
      FieldName = 'Ocorrencia'
      Origin = 'ocorreu.Ocorrencia'
      Required = True
    end
    object QrOcorreuValor: TFloatField
      FieldName = 'Valor'
      Origin = 'ocorreu.Valor'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrOcorreuLoteQuit: TIntegerField
      FieldName = 'LoteQuit'
      Origin = 'ocorreu.LoteQuit'
      Required = True
    end
    object QrOcorreuLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'ocorreu.Lk'
    end
    object QrOcorreuDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'ocorreu.DataCad'
    end
    object QrOcorreuDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'ocorreu.DataAlt'
    end
    object QrOcorreuUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'ocorreu.UserCad'
    end
    object QrOcorreuUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'ocorreu.UserAlt'
    end
    object QrOcorreuTaxaP: TFloatField
      FieldName = 'TaxaP'
      Origin = 'ocorreu.TaxaP'
      Required = True
    end
    object QrOcorreuTaxaV: TFloatField
      FieldName = 'TaxaV'
      Origin = 'ocorreu.TaxaV'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrOcorreuPago: TFloatField
      FieldName = 'Pago'
      Origin = 'ocorreu.Pago'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrOcorreuDataP: TDateField
      FieldName = 'DataP'
      Origin = 'ocorreu.DataP'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrOcorreuTaxaB: TFloatField
      FieldName = 'TaxaB'
      Origin = 'ocorreu.TaxaB'
      Required = True
    end
    object QrOcorreuData3: TDateField
      FieldName = 'Data3'
      Origin = 'ocorreu.Data3'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrOcorreuStatus: TSmallintField
      FieldName = 'Status'
      Origin = 'ocorreu.Status'
      Required = True
    end
    object QrOcorreuSALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrOcorreuATUALIZADO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATUALIZADO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrOcorreuDOCUM_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DOCUM_TXT'
      Size = 100
      Calculated = True
    end
    object QrOcorreuBanco: TIntegerField
      FieldName = 'Banco'
      Origin = 'ocorreu.Banco'
    end
    object QrOcorreuAgencia: TIntegerField
      FieldName = 'Agencia'
      Origin = 'ocorreu.Agencia'
    end
    object QrOcorreuCheque: TIntegerField
      FieldName = 'Cheque'
      Origin = 'ocorreu.Cheque'
    end
    object QrOcorreuDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Origin = 'ocorreu.Duplicata'
      Size = 12
    end
    object QrOcorreuConta: TWideStringField
      FieldName = 'Conta'
      Origin = 'ocorreu.Conta'
    end
    object QrOcorreuEmissao: TDateField
      FieldName = 'Emissao'
      Origin = 'ocorreu.Emissao'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrOcorreuDCompra: TDateField
      FieldName = 'DCompra'
      Origin = 'ocorreu.DCompra'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrOcorreuVencto: TDateField
      FieldName = 'Vencto'
      Origin = 'ocorreu.Vencto'
    end
    object QrOcorreuDDeposito: TDateField
      FieldName = 'DDeposito'
      Origin = 'ocorreu.DDeposito'
    end
    object QrOcorreuEmitente: TWideStringField
      FieldName = 'Emitente'
      Origin = 'ocorreu.Emitente'
      Size = 50
    end
    object QrOcorreuCPF: TWideStringField
      FieldName = 'CPF'
      Origin = 'ocorreu.CPF'
      Size = 15
    end
    object QrOcorreuCliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'ocorreu.Cliente'
      Required = True
    end
    object QrOcorreuDescri: TWideStringField
      FieldName = 'Descri'
      Origin = 'ocorreu.Descri'
      Size = 30
    end
    object QrOcorreuMoviBank: TIntegerField
      FieldName = 'MoviBank'
    end
    object QrOcorreuTpOcor: TSmallintField
      FieldName = 'TpOcor'
    end
    object QrOcorreuLOIS: TIntegerField
      FieldName = 'LOIS'
    end
    object QrOcorreuPlaGen: TIntegerField
      FieldName = 'PlaGen'
    end
  end
  object DsOcorreu: TDataSource
    DataSet = QrOcorreu
    Left = 593
    Top = 9
  end
  object QrSumOc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Juros) Juros, SUM(Pago) Pago'
      'FROM ocor rpg'
      'WHERE Ocorreu=:P0')
    Left = 29
    Top = 169
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumOcJuros: TFloatField
      FieldName = 'Juros'
    end
    object QrSumOcPago: TFloatField
      FieldName = 'Pago'
    end
  end
  object QrLastOcor: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Data, Codigo'
      'FROM ocor rpg'
      'WHERE Ocorreu=:P0'
      'ORDER BY Data Desc, Codigo Desc')
    Left = 57
    Top = 169
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLastOcorData: TDateField
      FieldName = 'Data'
    end
    object QrLastOcorFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
  end
  object QrBco1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nome FROM bancos'
      'WHERE Codigo=:P0')
    Left = 165
    Top = 193
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrBco1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object QrPesq: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT oc.* '
      'FROM ocorreu oc'
      'WHERE Codigo=1')
    Left = 195
    Top = 194
    object QrPesqBanco: TIntegerField
      FieldName = 'Banco'
      Origin = 'ocorreu.Banco'
      Required = True
    end
    object QrPesqDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Origin = 'ocorreu.Duplicata'
      Required = True
      Size = 12
    end
    object QrPesqCliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'ocorreu.Cliente'
    end
    object QrPesqEmitente: TWideStringField
      FieldName = 'Emitente'
      Origin = 'ocorreu.Emitente'
      Size = 50
    end
    object QrPesqVencto: TDateField
      FieldName = 'Vencto'
      Origin = 'ocorreu.Vencto'
      Required = True
    end
    object QrPesqAgencia: TIntegerField
      FieldName = 'Agencia'
      Origin = 'ocorreu.Agencia'
    end
    object QrPesqConta: TWideStringField
      FieldName = 'Conta'
      Origin = 'ocorreu.Conta'
    end
    object QrPesqCheque: TIntegerField
      FieldName = 'Cheque'
      Origin = 'ocorreu.Cheque'
    end
  end
  object QrOcorP: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      '/*'
      'SELECT * FROM ocor rpg'
      'WHERE Ocorreu =:P0*/'
      ''
      'SELECT lct.Ocorreu, lct.Data, lct.FatParcela,'
      'lct.Controle, lct.FatNum, lct.MoraVal,'
      'lct.Credito - lct.Debito Pago'
      'FROM lct0001a lct'
      'WHERE lct.FatID=304 '
      'AND lct.Ocorreu>0'
      '')
    Left = 569
    Top = 382
    object QrOcorPOcorreu: TIntegerField
      FieldName = 'Ocorreu'
    end
    object QrOcorPData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrOcorPFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrOcorPControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOcorPFatNum: TFloatField
      FieldName = 'FatNum'
      DisplayFormat = '000000'
    end
    object QrOcorPMoraVal: TFloatField
      FieldName = 'MoraVal'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrOcorPPago: TFloatField
      FieldName = 'Pago'
      DisplayFormat = '#,###,###,##0.00'
    end
  end
  object DsOcorP: TDataSource
    DataSet = QrOcorP
    Left = 597
    Top = 382
  end
  object QrLocLote: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Tipo'
      'FROM lot es'
      'WHERE Codigo=:P0')
    Left = 789
    Top = 365
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocLoteCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLocLoteTipo: TSmallintField
      FieldName = 'Tipo'
    end
  end
  object PMIncluiOcor: TPopupMenu
    Left = 33
    Top = 432
    object Incluiocorrnciadecliente1: TMenuItem
      Caption = 'Inclui ocorr'#234'ncia de &Cliente'
      OnClick = Incluiocorrnciadecliente1Click
    end
    object IncluiocorrnciadeCheque1: TMenuItem
      Caption = 'Inclui ocorr'#234'ncia de C&heque'
      Enabled = False
      Visible = False
      OnClick = IncluiocorrnciadeCheque1Click
    end
    object IncluiocorrnciadeDuplicata1: TMenuItem
      Caption = '&Inclui ocorr'#234'ncia de &Duplicata'
      Enabled = False
      Visible = False
      OnClick = IncluiocorrnciadeDuplicata1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object IncluipagamentodeOcorrncia1: TMenuItem
      Caption = 'Inclui &Pagamento de Ocorr'#234'ncia'
      OnClick = IncluipagamentodeOcorrncia1Click
    end
  end
  object PMAlteraOcor: TPopupMenu
    Left = 129
    Top = 428
    object Alteraocorrnciaselecionada1: TMenuItem
      Caption = '&Altera ocorr'#234'ncia selecionada'
      OnClick = Alteraocorrnciaselecionada1Click
    end
  end
  object PMExcluiOcor: TPopupMenu
    OnPopup = PMExcluiOcorPopup
    Left = 205
    Top = 432
    object ExcluiOcorrnciaselecionada1: TMenuItem
      Caption = 'Exclui &Ocorr'#234'ncia selecionada'
      OnClick = ExcluiOcorrnciaselecionada1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Excluipagamentodeocorrnciaselecionado1: TMenuItem
      Caption = 'Exclui pagamento de ocorr'#234'ncia selecionado'
      OnClick = Excluipagamentodeocorrnciaselecionado1Click
    end
  end
end
