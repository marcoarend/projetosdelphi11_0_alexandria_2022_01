unit Lot0Sbr;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, frxClass, frxDBSet, Menus, DmkDAC_PF, UnDmkEnums;

type
  TFmLot0Sbr = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrLots: TmySQLQuery;
    PB1: TProgressBar;
    QrSobraCli: TmySQLQuery;
    QrSobraCliCliente: TIntegerField;
    QrSobraCliLote: TIntegerField;
    QrSobraCliCodigo: TIntegerField;
    QrSobraCliData: TDateField;
    QrSobraCliNomCli: TWideStringField;
    QrSobraCliSobraIni: TFloatField;
    QrSobraCliSobraNow: TFloatField;
    QrSobraCliSobraErr: TFloatField;
    QrSobraCliChecado: TIntegerField;
    QrSobraCliCodAnt: TIntegerField;
    QrSobraCliAtivo: TSmallintField;
    QrLotsCliente: TIntegerField;
    QrLotsLote: TIntegerField;
    QrLotsCodigo: TIntegerField;
    QrLotsData: TDateField;
    QrLotsNomCli: TWideStringField;
    QrLotsSobraIni: TFloatField;
    QrLotsSobraNow: TFloatField;
    QrLotsSobraErr: TFloatField;
    QrLotsChecado: TIntegerField;
    QrLotsCodAnt: TIntegerField;
    QrLotsAtivo: TSmallintField;
    DsSobraCli: TDataSource;
    DBGrid1: TDBGrid;
    QrSobraCliSobraAnt: TFloatField;
    QrLotsSobraAnt: TFloatField;
    frxBDR_GEREN_017_001: TfrxReport;
    frxDsSobraCli: TfrxDBDataset;
    BtImprime: TBitBtn;
    QrLotsLotAnt: TIntegerField;
    QrSobraCliLotAnt: TIntegerField;
    BtBordero: TBitBtn;
    PMBordero: TPopupMenu;
    Lotedasobraanterior1: TMenuItem;
    Lotedasobrainicial1: TMenuItem;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QrSobraCliBeforeClose(DataSet: TDataSet);
    procedure QrSobraCliAfterOpen(DataSet: TDataSet);
    procedure BtImprimeClick(Sender: TObject);
    procedure BtBorderoClick(Sender: TObject);
    procedure Lotedasobraanterior1Click(Sender: TObject);
    procedure Lotedasobrainicial1Click(Sender: TObject);
  private
    { Private declarations }
    F_Sobr_Cli_: String;
  public
    { Public declarations }
  end;

  var
  FmLot0Sbr: TFmLot0Sbr;

implementation

uses UnMyObjects, Module, UCreateCredito, ModuleGeral, UMySQLModule, MyListas,
  Principal;

{$R *.DFM}

procedure TFmLot0Sbr.BtBorderoClick(Sender: TObject);
begin
  if BtImprime.Enabled then
    MyObjects.MostraPopUpDeBotao(PMBordero, BtBordero)
  else
    FmPrincipal.CriaFormLot(2, 0, 0, 0)
end;

procedure TFmLot0Sbr.BtImprimeClick(Sender: TObject);
begin
  MyObjects.frxMostra(frxBDR_GEREN_017_001,
    'Relat�rio de Diferen�as de Saldo Anterior X Saldo Inicial');
end;

procedure TFmLot0Sbr.BtOKClick(Sender: TObject);
var
  CodAnt, LotAnt, Codigo, Cliente: Integer;
  SobraAnt, SobraErr: Double;
begin
  Screen.cursor := crHourGlass;
  try
    BtImprime.Enabled := False;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando lotes com sobras');
    //
    F_Sobr_Cli_ :=
      UCriarCredito.RecriaTempTableNovo(ntrtt_sobr_cli_, DModG.QrUpdPID1, False);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrLots, DModG.MyPID_DB, [
    'INSERT INTO ' + F_Sobr_Cli_,
    'SELECT lot.Cliente, lot.Lote, lot.Codigo, lot.Data, ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NomCli, ',
    '0 SobraAnt, lot.SobraIni, lot.SobraNow, 0 SobraErr, ',
    '0 Checado, 0 CodAnt, 0 LotAnt, 1 Ativo ',
    'FROM ' + TMeuDB + '.' + CO_TabLotA + ' lot ',
    'LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo=lot.Cliente ',
    'WHERE lot.SobraIni <> 0 ',
    'OR lot.SobraNow <> 0 ',
    'ORDER BY NomCli, Cliente, lot.Data, lot.Codigo; ',
    ' ',
    'SELECT * ',
    'FROM ' + F_Sobr_Cli_,
    'ORDER BY NomCli, Cliente, Data, Codigo; ',
    '']);
    //
    Cliente := -999999999;
    SobraAnt := 0;
    PB1.Position := 0;
    PB1.Max := QrLots.RecordCount;
    CodAnt := 0;
    LotAnt := 0;
    while not QrLots.Eof do
    begin
      PB1.Position := PB1.Position + 1;
      //
      if Cliente = QrLotsCliente.Value then
        SobraErr := QrLotsSobraIni.Value - SobraAnt
      else
        SobraErr := 0;
      if (SobraErr >= 0.01) or (SobraErr <= -0.01) then
      begin
        Codigo := QrLotsCodigo.Value;
        //
        UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, F_sobr_cli_, False, [
        'SobraAnt', 'SobraErr', 'CodAnt', 'LotAnt'], ['Codigo'
        ], [
        SobraAnt, SobraErr, CodAnt, LotAnt], [Codigo
        ], False);
      end;
      //
      Cliente := QrLotsCliente.Value;
      CodAnt := QrLotsCodigo.Value;
      LotAnt := QrLotsLote.Value;
      SobraAnt := QrLotsSobraNow.Value;
      //
      QrLots.Next;
    end;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrSobraCli, DModG.MyPID_DB, [
    'SELECT * ',
    'FROM ' + F_Sobr_Cli_,
    'WHERE SobraErr <> 0 ',
    'ORDER BY NomCli, Cliente, Data, Codigo; ',
    '']);
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    Screen.Cursor := crDefault;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmLot0Sbr.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLot0Sbr.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  ImgTipo.SQLType := stPsq;
  QrLots.Database := DModG.MyPID_DB;
end;

procedure TFmLot0Sbr.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLot0Sbr.Lotedasobraanterior1Click(Sender: TObject);
begin
  FmPrincipal.CriaFormLot(2, 0, QrSobraCliCodAnt.Value, 0)
end;

procedure TFmLot0Sbr.Lotedasobrainicial1Click(Sender: TObject);
begin
  FmPrincipal.CriaFormLot(2, 0, QrSobraCliCodigo.Value, 0)
end;

procedure TFmLot0Sbr.QrSobraCliAfterOpen(DataSet: TDataSet);
begin
   BtImprime.Enabled := QrSobraCli.RecordCount > 0;
end;

procedure TFmLot0Sbr.QrSobraCliBeforeClose(DataSet: TDataSet);
begin
  BtImprime.Enabled := False;
end;

end.
