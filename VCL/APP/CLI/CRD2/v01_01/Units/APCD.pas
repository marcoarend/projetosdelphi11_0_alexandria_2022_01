unit APCD;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  dmkDBLookupComboBox, dmkEditCB, UnDmkProcFunc, UnDmkEnums;

type
  TFmAPCD = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrAPCD: TmySQLQuery;
    QrAPCDCodigo: TIntegerField;
    QrAPCDNome: TWideStringField;
    QrAPCDDeposita: TSmallintField;
    QrAPCDExigeCMC7: TSmallintField;
    QrAPCDLk: TIntegerField;
    QrAPCDDataCad: TDateField;
    QrAPCDDataAlt: TDateField;
    QrAPCDUserCad: TIntegerField;
    QrAPCDUserAlt: TIntegerField;
    QrAPCDAlterWeb: TSmallintField;
    QrAPCDAtivo: TSmallintField;
    DsAPCD: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    RGDeposita: TdmkRadioGroup;
    RGExigeCMC7: TdmkRadioGroup;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    DBRadioGroup1: TDBRadioGroup;
    DBRadioGroup2: TDBRadioGroup;
    QrContas: TmySQLQuery;
    DsContas: TDataSource;
    Label8: TLabel;
    EdPla_Gen: TdmkEditCB;
    CBPla_Gen: TdmkDBLookupComboBox;
    SpeedButton5: TSpeedButton;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    Label10: TLabel;
    DBEdit3: TDBEdit;
    DBEdit1: TDBEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrAPCDAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrAPCDBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
  end;

var
  FmAPCD: TFmAPCD;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, Contas, MyDBCheck;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmAPCD.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmAPCD.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrAPCDCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmAPCD.DefParams;
begin
  VAR_GOTOTABELA := 'apcd';
  VAR_GOTOMYSQLTABLE := QrAPCD;
  VAR_GOTONEG := gotoNiP;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT apc.*');
  VAR_SQLx.Add('FROM apcd apc');
  VAR_SQLx.Add('WHERE apc.Codigo <> 0');
  //
  VAR_SQL1.Add('AND apc.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND apc.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND apc.Nome Like :P0');
  //

{
  VAR_SQLx.Add('SELECT cnt.Nome NO_PLA#GEN, apc.*');
  VAR_SQLx.Add('FROM apcd apc');
  VAR_SQLx.Add('LEFT JOIN contas cnt ON cnt.Codigo=apc.Pla#Gen');
  VAR_SQLx.Add('WHERE apc.Codigo <> 0');
  //
  VAR_SQL1.Add('AND apc.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND apc.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND apc.Nome Like :P0');
  //
}
end;

procedure TFmAPCD.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmAPCD.QueryPrincipalAfterOpen;
begin
end;

procedure TFmAPCD.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARAÇÕES///////////////////

procedure TFmAPCD.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmAPCD.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmAPCD.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmAPCD.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmAPCD.SpeedButton5Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmContas, FmContas, afmoNegarComAviso) then
  begin
    FmContas.ShowModal;
    FmContas.Destroy;
    //
    UMyMod.SetaCodigoPesquisado(EdPla_Gen, CBPla_Gen, QrContas, VAR_CADASTRO);
  end;
end;

procedure TFmAPCD.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmAPCD.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrAPCD, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'apcd');
end;

procedure TFmAPCD.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrAPCDCodigo.Value;
  Close;
end;

procedure TFmAPCD.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descrição!') then Exit;
{
  if MyObjects.FIC(EdPla#Gen.ValueVariant = 0, EdPla#Gen,
    'Defina uma conta (do plano de contas)!') then Exit;
}
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('apcd', 'Codigo', ImgTipo.SQLType,
    QrAPCDCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmAPCD, PnEdita,
    'apcd', Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmAPCD.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'apcd', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'apcd', 'Codigo');
end;

procedure TFmAPCD.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrAPCD, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'apcd');
end;

procedure TFmAPCD.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
  CriaOForm;
  UMyMod.AbreQuery(QrContas, Dmod.MyDB);
end;

procedure TFmAPCD.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrAPCDCodigo.Value, LaRegistro.Caption);
end;

procedure TFmAPCD.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmAPCD.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrAPCDCodigo.Value, LaRegistro.Caption);
end;

procedure TFmAPCD.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmAPCD.QrAPCDAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmAPCD.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmAPCD.SbQueryClick(Sender: TObject);
begin
  LocCod(QrAPCDCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'apcd', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmAPCD.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmAPCD.QrAPCDBeforeOpen(DataSet: TDataSet);
begin
  QrAPCDCodigo.DisplayFormat := FFormatFloat;
end;

end.

