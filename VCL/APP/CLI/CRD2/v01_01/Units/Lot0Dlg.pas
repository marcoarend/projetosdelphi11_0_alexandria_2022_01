unit Lot0Dlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, CheckLst, dmkGeral, dmkImage, UnDmkEnums;

type
  TFmLot0Dlg = class(TForm)
    CLImpressoes: TCheckListBox;
    Panel1: TPanel;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    BtSalvar: TBitBtn;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel2: TPanel;
    BtImprime: TBitBtn;
    BtPreview: TBitBtn;
    BitBtn1: TBitBtn;
    procedure BtImprimeClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtPreviewClick(Sender: TObject);
    procedure CLImpressoesClick(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure BtSalvarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure SetaRelatorios();
    procedure ClicaTodos(Checked: Boolean);
  public
    { Public declarations }
  end;

  var
  FmLot0Dlg: TFmLot0Dlg;

implementation

uses UnMyObjects, Principal, Lot2Cab;

{$R *.DFM}

procedure TFmLot0Dlg.BtImprimeClick(Sender: TObject);
begin
  case FmPrincipal.FFormLotShow of
    //0: FmLot0.FImprime := 1;
    //1: FmLot1.FImprime := 1;
    2: FmLot2Cab.FImprime := 1;
    else Application.MessageBox(PChar('Janela de gerenciamento de border�s ' +
    'n�o definida! (02)'), 'ERRO', MB_OK+MB_ICONERROR);
  end;
  Close;
end;

procedure TFmLot0Dlg.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLot0Dlg.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLot0Dlg.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLot0Dlg.BtPreviewClick(Sender: TObject);
begin
  case FmPrincipal.FFormLotShow of
    //0: FmLot0.FImprime := 2;
    //1: FmLot1.FImprime := 2;
    2: FmLot2Cab.FImprime := 2;
    else Application.MessageBox(PChar('Janela de gerenciamento de border�s ' +
    'n�o definida! (02)'), 'ERRO', MB_OK+MB_ICONERROR);
  end;
  Close;
end;

procedure TFmLot0Dlg.CLImpressoesClick(Sender: TObject);
begin
  SetaRelatorios();
end;

procedure TFmLot0Dlg.SetaRelatorios();
begin
  case FmPrincipal.FFormLotShow of
    {
    0:
    begin
      FmLot0.FRelatorios := MLAGeral.ValorDeChekListBox(CLImpressoes);
      BtImprime.Enabled := FmLot0.FRelatorios > 0;
      BtPreview.Enabled := FmLot0.FRelatorios > 0;
    end;
    1:
    begin
      FmLot1.FRelatorios := MLAGeral.ValorDeChekListBox(CLImpressoes);
      BtImprime.Enabled := FmLot1.FRelatorios > 0;
      BtPreview.Enabled := FmLot1.FRelatorios > 0;
    end;
    }
    2:
    begin
      FmLot2Cab.FRelatorios := MLAGeral.ValorDeChekListBox(CLImpressoes);
      BtImprime.Enabled := FmLot2Cab.FRelatorios > 0;
      BtPreview.Enabled := FmLot2Cab.FRelatorios > 0;
    end;
    else Application.MessageBox(PChar('Janela de gerenciamento de border�s ' +
    'n�o definida! (03)'), 'ERRO', MB_OK+MB_ICONERROR);
  end;
  //
end;

procedure TFmLot0Dlg.BtTodosClick(Sender: TObject);
begin
  ClicaTodos(True);
end;

procedure TFmLot0Dlg.BtNenhumClick(Sender: TObject);
begin
  ClicaTodos(False);
end;

procedure TFmLot0Dlg.ClicaTodos(Checked: Boolean);
var
  i: Integer;
begin
  for i := 0 to CLImpressoes.Items.Count -1 do
    CLImpressoes.Checked[i] := Checked;
  SetaRelatorios();
end;

procedure TFmLot0Dlg.BtSalvarClick(Sender: TObject);
var
  Tipo, Rel: Integer;
begin
  Tipo := 0;
  Rel  := 0;
  //
  case FmPrincipal.FFormLotShow of
    {
    0:
    begin
      Tipo := FmLot0.QrLotTipo.Value;
      Rel  := FmLot0.FRelatorios;
    end;
    1:
    begin
      Tipo := FmLot1.QrLotTipo.Value;
      Rel  := FmLot1.FRelatorios;
    end;
    }
    2:
    begin
      Tipo := FmLot2Cab.QrLotTipo.Value;
      Rel  := FmLot2Cab.FRelatorios;
    end;
    else Application.MessageBox(PChar('Janela de gerenciamento de border�s ' +
    'n�o definida! (05)'), 'ERRO', MB_OK+MB_ICONERROR);
  end;
  Geral.WriteAppKey('LotRelats'+IntToStr(Tipo),
    Application.Title, Rel, ktInteger, HKEY_LOCAL_MACHINE);
end;

procedure TFmLot0Dlg.FormCreate(Sender: TObject);
var
  Tipo, Relatorios, i, n: Integer;
begin
  ImgTipo.SQLType := stPsq;
  Tipo := 0;
  //
  case FmPrincipal.FFormLotShow of
    //0: FmLot0.QrLotTipo.Value;
    //1: FmLot1.QrLotTipo.Value;
    2: FmLot2Cab.QrLotTipo.Value;
    else Application.MessageBox(PChar('Janela de gerenciamento de border�s ' +
    'n�o definida! (06)'), 'ERRO', MB_OK+MB_ICONERROR);
  end;
  Relatorios := Geral.ReadAppKey('LotRelats'+IntToStr(Tipo),
  Application.Title, ktInteger, 0, HKEY_LOCAL_MACHINE);
  n := 2;
  for i := 0 to CLImpressoes.Items.Count -1 do
  begin
    CLImpressoes.Checked[i] := MLAGeral.IntInConjunto2(n, Relatorios);
    n := n * 2;
  end;
  SetaRelatorios();
end;

end.
