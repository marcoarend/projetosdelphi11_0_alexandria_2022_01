unit Principal;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  UnMLAGeral, UMySQLModule, ExtCtrls, Menus, Grids, DBGrids, Db, (*DBTables,*)
  TypInfo, StdCtrls, ZCF2, ToolWin, ComCtrls, UnInternalConsts, ExtDlgs, jpeg,
  mySQLDbTables, Buttons, WinSkinStore, WinSkinData, Mask, DBCtrls, Tabs,
  DockTabSet, AdvToolBar, AdvGlowButton, AdvMenus, dmkGeral, AdvToolBarStylers,
  AdvShapeButton, AdvPreviewMenu, AdvOfficeHint, dmkDBGrid, dmkEdit,
  UnMyObjects, dmkDBLookupComboBox, dmkEditCB, dmkEditDateTimePicker, ImgList,
  dmkRadioGroup, dmkDBGridDAC, Clipbrd, IniFiles, Sockets, CreditoConsts,
  ArquivosSCX, frxClass, MyListas, frxDBSet, dmkMemo, UnDmkProcFunc, DmkDAC_PF,
  TedC_Tabs, DMKpnfsConversao, UnDmkEnums, Vcl.Imaging.pngimage, dmkPageControl;

type
  TcpCalc = (cpJurosMes, cpMulta);
  TBandaMagnetica = class(TObject)
  private
    FBandaMagnetica: String;
    FCompe: String;
    FBanco: String;
    FAgencia: String;
    FConta: String;
    FNumero: String;
    FDv1: Char;
    FDv2: Char;
    FDv3: Char;
    FTipo: Char;
    procedure DestrincharBanda(Banda: String);
  public
    property BandaMagnetica: String read FBandaMagnetica write DestrincharBanda;
    property Compe: String read FCompe;
    property Banco: String read FBanco;
    property Agencia: String read FAgencia;
    property Conta: String read FConta;
    property Numero: String read FNumero;
    property Dv1: Char read FDv1;
    property Dv2: Char read FDv2;
    property Dv3: Char read FDv3;
    property Tipo: Char read FTipo;
  end;
  TFmPrincipal = class(TForm)
    OpenPictureDialog1: TOpenPictureDialog;
    Timer1: TTimer;
    PMProdImp: TPopupMenu;
    Estoque1: TMenuItem;
    Histrico1: TMenuItem;
    Vendas1: TMenuItem;
    sd1: TSkinData;
    SkinStore1: TSkinStore;
    DsCarteiras: TDataSource;
    TimerIdle: TTimer;
    OpenDialog1: TOpenDialog;
    AdvToolBarPager1: TAdvToolBarPager;
    AdvPage04: TAdvPage;
    AdvPage03: TAdvPage;
    AdvPage06: TAdvPage;
    AdvToolBar1: TAdvToolBar;
    AGBFinancas: TAdvGlowButton;
    AdvToolBar4: TAdvToolBar;
    AdvToolBar6: TAdvToolBar;
    AGBFinNiv: TAdvGlowButton;
    AGBFinCta: TAdvGlowButton;
    AGBSubGru: TAdvGlowButton;
    AGBFinGru: TAdvGlowButton;
    AGBFinCjt: TAdvGlowButton;
    AGBFinPlano: TAdvGlowButton;
    AdvToolBar8: TAdvToolBar;
    AdvToolBar9: TAdvToolBar;
    AdvPMSkin: TAdvPopupMenu;
    Escolher2: TMenuItem;
    Padro2: TMenuItem;
    AdvPMImagem: TAdvPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    Estilo2: TMenuItem;
    Centralizado1: TMenuItem;
    ManterProporo1: TMenuItem;
    ManterAltura1: TMenuItem;
    ManterLargura1: TMenuItem;
    Nenhum1: TMenuItem;
    Espichar1: TMenuItem;
    Repetido1: TMenuItem;
    AdvPMMenuCor: TAdvPopupMenu;
    Padro3: TMenuItem;
    Office20071: TMenuItem;
    Dermatek1: TMenuItem;
    Preto1: TMenuItem;
    Azul1: TMenuItem;
    Cinza1: TMenuItem;
    Verde1: TMenuItem;
    Prscia1: TMenuItem;
    WhidbeyStyle1: TMenuItem;
    WindowsXP1: TMenuItem;
    AdvToolBarOfficeStyler1: TAdvToolBarOfficeStyler;
    AdvShapeButton1: TAdvShapeButton;
    AdvQuickAccessToolBar1: TAdvQuickAccessToolBar;
    AdvPreviewMenu1: TAdvPreviewMenu;
    AdvToolBar11: TAdvToolBar;
    AdvGlowMenuButton3: TAdvGlowMenuButton;
    AdvGlowMenuButton4: TAdvGlowMenuButton;
    AdvGlowMenuButton1: TAdvGlowMenuButton;
    AdvOfficeHint1: TAdvOfficeHint;
    AGMBBackup: TAdvGlowButton;
    Automatico1: TMenuItem;
    AGBTextos: TAdvGlowMenuButton;
    AdvPMTextos: TAdvPopupMenu;
    AdvPage01: TAdvPage;
    ATBEmp1: TAdvToolBar;
    Desativar1: TMenuItem;
    AGBAcesRapid: TAdvGlowButton;
    Pagina: TdmkPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    PageControl3: TPageControl;
    TabSheet10: TTabSheet;
    Panel11: TPanel;
    Panel10: TPanel;
    Label6: TLabel;
    BtAtzListaMailProt: TBitBtn;
    DBEdEmeioProtTot: TDBEdit;
    BtReenvia: TBitBtn;
    DBGEmeioProt: TdmkDBGrid;
    DBGEmeioProtPak: TdmkDBGrid;
    DBGEmeioProtPakIts: TdmkDBGrid;
    TabSheet12: TTabSheet;
    Panel14: TPanel;
    GradePTK: TdmkDBGrid;
    Panel17: TPanel;
    Label11: TLabel;
    Panel15: TPanel;
    Panel16: TPanel;
    BitBtn17: TBitBtn;
    BitBtn18: TBitBtn;
    BitBtn19: TBitBtn;
    GradePPI: TDBGrid;
    TabSheet13: TTabSheet;
    GradePKA: TDBGrid;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    Panel4: TPanel;
    Label5: TLabel;
    MeTbInuteis: TMemo;
    Button4: TButton;
    DBEdit1: TDBEdit;
    TabSheet7: TTabSheet;
    Panel6: TPanel;
    Panel7: TPanel;
    Button6: TButton;
    RGCNAB: TRadioGroup;
    Panel8: TPanel;
    Button7: TButton;
    Memo4: TMemo;
    TabSheet11: TTabSheet;
    Panel13: TPanel;
    BtMenu: TBitBtn;
    DBGSemMez: TdmkDBGrid;
    TabSheet9: TTabSheet;
    Panel12: TPanel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdComando: TEdit;
    EdResposta: TEdit;
    MeResposta: TdmkEdit;
    GroupBox1: TGroupBox;
    Label10: TLabel;
    Ed1: TEdit;
    Ed2: TEdit;
    Button12: TButton;
    Edit1: TEdit;
    EdA: TEdit;
    EdB: TEdit;
    Edit2: TEdit;
    Button13: TButton;
    Button14: TButton;
    BtComando: TButton;
    BtRefrPrto: TButton;
    BtNegrito: TButton;
    BtNormal: TButton;
    dmkEdit1: TdmkEdit;
    dmkEdit2: TdmkEdit;
    Button15: TButton;
    dmkEdit3: TdmkEdit;
    dmkEdit4: TdmkEdit;
    Button16: TButton;
    TabSheet14: TTabSheet;
    QrDupl: TmySQLQuery;
    QrDuplITENS: TLargeintField;
    QrDuplConta: TIntegerField;
    QrDuplNOMECONTA: TWideStringField;
    QrDupi: TmySQLQuery;
    QrDupiCodigo: TIntegerField;
    QrDupiNome: TWideStringField;
    DsDupl: TDataSource;
    DsDupi: TDataSource;
    StatusBar: TStatusBar;
    LaAviso4: TLabel;
    ShapeFill: TShape;
    AGBGerCond: TAdvGlowButton;
    AGBDeposito: TAdvGlowButton;
    LaAviso3: TLabel;
    RgSitCobr: TRadioGroup;
    AdvPage05: TAdvPage;
    AdvToolBar15: TAdvToolBar;
    BtBalancete: TAdvGlowButton;
    BtReceDesp: TAdvGlowButton;
    AdvPage08: TAdvPage;
    AdvToolBar16: TAdvToolBar;
    AGBWClients: TAdvGlowButton;
    AGBWSincro: TAdvGlowButton;
    AGBBaixaLoteWeb: TAdvGlowButton;
    RGProtoFiltro: TRadioGroup;
    AdvToolBar23: TAdvToolBar;
    AdvGlowButton41: TAdvGlowButton;
    AdvGlowButton42: TAdvGlowButton;
    AGBFeriados: TAdvGlowButton;
    AGBEuroExport: TAdvGlowButton;
    AGBBAncoDados: TAdvGlowButton;
    AGBArquivoMorto: TAdvGlowButton;
    AGBAtzBordero: TAdvGlowButton;
    AGBAgrExtrCta: TAdvGlowButton;
    AGBLinkConcBco: TAdvGlowButton;
    AdvToolBar24: TAdvToolBar;
    AGBRemConfig: TAdvGlowButton;
    AdvToolBar27: TAdvToolBar;
    AGBLivre1_6_2: TAdvGlowButton;
    AdvGlowMenuButton5: TAdvGlowMenuButton;
    AdvPMVerificaBD: TAdvPopupMenu;
    VerificaBDServidor1: TMenuItem;
    VerificaBDWeb1: TMenuItem;
    AdvToolBar3: TAdvToolBar;
    AdvGlowButton82: TAdvGlowButton;
    AdvGlowButton83: TAdvGlowButton;
    AdvGlowButton84: TAdvGlowButton;
    AdvGlowButton90: TAdvGlowButton;
    AdvToolBar29: TAdvToolBar;
    AGBCheque_0: TAdvGlowButton;
    AdvGlowButton92: TAdvGlowButton;
    AdvGlowButton93: TAdvGlowButton;
    AdvGlowButton94: TAdvGlowButton;
    APMExtratos: TAdvPopupMenu;
    PagarReceber1: TMenuItem;
    Movimento1: TMenuItem;
    ResultadosMensais1: TMenuItem;
    AGBExtratos: TAdvGlowMenuButton;
    AGBPesquisas: TAdvGlowMenuButton;
    APMPesquisas: TAdvPopupMenu;
    PorNveldoPLanodeContas1: TMenuItem;
    ContasControladas1: TMenuItem;
    ContasSazonais1: TMenuItem;
    AGBSaldos: TAdvGlowMenuButton;
    APMSaldos: TAdvPopupMenu;
    Futuros1: TMenuItem;
    Em1: TMenuItem;
    AGBTheme: TAdvGlowButton;
    UnicaEmpresa1: TMenuItem;
    MultiplasEmpresas1: TMenuItem;
    MovimentoPlanodecontas1: TMenuItem;
    Timer2: TTimer;
    QrCond2: TmySQLQuery;
    QrCond2NOMECLI: TWideStringField;
    QrCond2Codigo: TIntegerField;
    QrCond2Cliente: TIntegerField;
    DsCond2: TDataSource;
    EdEmpresa: TdmkEdit;
    DBEdit2: TDBEdit;
    BitBtn4: TBitBtn;
    TabSheet8: TTabSheet;
    AGBResultados2: TAdvGlowButton;
    AGBInadimplencia2: TAdvGlowButton;
    AGBExpContab: TAdvGlowButton;
    AGBEvolucapi2: TAdvGlowButton;
    Grade: TStringGrid;
    PageControl2: TPageControl;
    TabSheet6: TTabSheet;
    DBGrid3: TDBGrid;
    TabSheet15: TTabSheet;
    DBGrid4: TDBGrid;
    TabSheet16: TTabSheet;
    Panel2: TPanel;
    DBGrid1: TDBGrid;
    DBGrid2: TDBGrid;
    Panel3: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    BtRefresh: TBitBtn;
    QrMalots: TmySQLQuery;
    QrMalotsLote: TSmallintField;
    QrMalotsData: TDateField;
    QrMalotsTotal: TFloatField;
    QrMalotsCodigo: TIntegerField;
    QrMalotsNOMECLIENTE: TWideStringField;
    DsMalote: TDataSource;
    QrECartaSac: TmySQLQuery;
    QrECartaSacCodigo: TIntegerField;
    QrECartaSacLote: TSmallintField;
    QrECartaSacData: TDateField;
    QrECartaSacTotal: TFloatField;
    QrECartaSacNOMECLIENTE: TWideStringField;
    DsECartaSac: TDataSource;
    Panel5: TPanel;
    Panel9: TPanel;
    Label4: TLabel;
    Label12: TLabel;
    Label15: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    EdQtdeCh: TdmkEdit;
    EdValorCH: TdmkEdit;
    BtImporta: TBitBtn;
    EdCodCli: TdmkEdit;
    EdNomCli: TdmkEdit;
    TPCheque: TDateTimePicker;
    Panel18: TPanel;
    Memo1: TMemo;
    Memo2: TMemo;
    GradeC: TStringGrid;
    ProgressBar4: TProgressBar;
    Panel19: TPanel;
    Label36: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    EdBanda: TdmkEdit;
    EdCPF: TdmkEdit;
    EdEmitente: TdmkEdit;
    Panel20: TPanel;
    BtConfAlt: TBitBtn;
    SP2: TStatusBar;
    ImgPrincipal: TImage;
    SockSPC: TTcpClient;
    TabSheet5: TTabSheet;
    ListBox1: TListBox;
    PMResultados: TPopupMenu;
    Bruto2: TMenuItem;
    LquidoNovo1: TMenuItem;
    N8: TMenuItem;
    GerenciaResultados1: TMenuItem;
    PMEvolucap: TPopupMenu;
    Antigo2: TMenuItem;
    NovoEmconstruo1: TMenuItem;
    PMRepasses: TPopupMenu;
    Cheques2: TMenuItem;
    Duplicatas1: TMenuItem;
    N3: TMenuItem;
    Relatorios1: TMenuItem;
    frxChequeSimples: TfrxReport;
    QrCreditado: TmySQLQuery;
    QrCreditadoBAC: TWideStringField;
    QrCreditadoNome: TWideStringField;
    QrCreditadoCPF: TWideStringField;
    QrCreditadoBanco: TIntegerField;
    QrCreditadoAgencia: TIntegerField;
    QrCreditadoConta: TWideStringField;
    QrSacado: TmySQLQuery;
    QrSacadoCNPJ: TWideStringField;
    QrSacadoNome: TWideStringField;
    QrSacadoRua: TWideStringField;
    QrSacadoCompl: TWideStringField;
    QrSacadoBairro: TWideStringField;
    QrSacadoCidade: TWideStringField;
    QrSacadoUF: TWideStringField;
    QrSacadoCEP: TIntegerField;
    QrSacadoTel1: TWideStringField;
    QrSacadoRisco: TFloatField;
    QrSacadoIE: TWideStringField;
    QrSacadoNUMERO: TFloatField;
    PmDeposito: TPopupMenu;
    MenuItem5: TMenuItem;
    MenuItem6: TMenuItem;
    AGBPesqPag: TAdvGlowButton;
    AGBBloqParc: TAdvGlowButton;
    AGBGerProp: TAdvGlowButton;
    AGBDeposit: TAdvGlowButton;
    AGBGrupUHs1: TAdvGlowButton;
    AGBRepaLoc: TAdvGlowButton;
    AGBChequesAvulsos2: TAdvGlowButton;
    AGBInadimplencia1: TAdvGlowButton;
    AGBEvolucapi1: TAdvGlowButton;
    BtEuroExport1: TAdvGlowButton;
    AdvGlowButton4: TAdvGlowButton;
    AGBResultados1: TAdvGlowButton;
    AGBOcorDupGer: TAdvGlowButton;
    AGBSimulaParcela: TAdvGlowButton;
    AGBLocPag: TAdvGlowButton;
    QrLot: TmySQLQuery;
    QrLotCodigo: TIntegerField;
    QrLotIts: TmySQLQuery;
    QrLotItsITENS: TLargeintField;
    AdvToolBar12: TAdvToolBar;
    AGBContrato: TAdvGlowButton;
    AdvGlowButton27: TAdvGlowButton;
    AdvGlowButton28: TAdvGlowButton;
    AGBRiscoAll2: TAdvGlowButton;
    AdvGlowButton9: TAdvGlowButton;
    AdvGlowButton10: TAdvGlowButton;
    AdvGlowButton11: TAdvGlowButton;
    AdvGlowButton13: TAdvGlowButton;
    AGBJanelaImpressao: TAdvGlowButton;
    frxReport1: TfrxReport;
    AdvPage02: TAdvPage;
    AdvToolBar14: TAdvToolBar;
    AGBCNABRetEnv: TAdvGlowButton;
    AdvToolBar20: TAdvToolBar;
    AGBOcorrencias: TAdvGlowButton;
    AdvToolBar22: TAdvToolBar;
    AGBAlinChDev: TAdvGlowButton;
    AGBAlinDuplicata: TAdvGlowButton;
    AGBAlinPagChDevolv: TAdvGlowButton;
    AGBTaxas: TAdvGlowButton;
    AGBBancos: TAdvGlowButton;
    AdvToolBar2: TAdvToolBar;
    AGBEntiCad: TAdvGlowButton;
    AGBClienteRapido: TAdvGlowButton;
    AGBSacados: TAdvGlowButton;
    AGBNFsCad: TAdvGlowButton;
    Contratos1: TMenuItem;
    extos2: TMenuItem;
    AGBGruSacEmi: TAdvGlowButton;
    VerificaBDsTerceiros1: TMenuItem;
    AdvToolBar10: TAdvToolBar;
    AdvGlowButton17: TAdvGlowButton;
    AdvGlowButton16: TAdvGlowButton;
    AdvPage07: TAdvPage;
    AGBServicoManager: TAdvGlowButton;
    AGBConexao: TAdvGlowButton;
    AGBMySQL: TAdvGlowButton;
    AdvToolBar21: TAdvToolBar;
    AGBComposto: TAdvGlowButton;
    AGBDecomp: TAdvGlowButton;
    AGBCompJuros: TAdvGlowButton;
    AdvToolBar30: TAdvToolBar;
    AGB_SPC_Config: TAdvGlowButton;
    AGB_SPC_Modalid: TAdvGlowButton;
    AGBSPC_Pesq: TAdvGlowButton;
    AGBAtzCliItsBordero: TAdvGlowButton;
    AGBExcluiItensLote: TAdvGlowButton;
    AGBCorrigeEmpresa: TAdvGlowButton;
    QrUpdCli1: TmySQLQuery;
    QrUpdCli2: TmySQLQuery;
    QrLoc: TmySQLQuery;
    QrLocITENS: TLargeintField;
    AGBFinSaldos: TAdvGlowButton;
    AGBFinLista: TAdvGlowButton;
    AGBExpContabExp: TAdvGlowButton;
    AGBFinCart: TAdvGlowButton;
    AdvGlowButton19: TAdvGlowButton;
    AGBPlanRelCab: TAdvGlowButton;
    AdvToolBar7: TAdvToolBar;
    AGB_CNAB_Remessa: TAdvGlowButton;
    AGB_CNAB_Retorno: TAdvGlowButton;
    AdvToolBar31: TAdvToolBar;
    AGBEmitentes: TAdvGlowButton;
    AGBEmiSacExport: TAdvGlowButton;
    AGBEmiSacImport: TAdvGlowButton;
    AGBLSincro: TAdvGlowButton;
    AGBAjusteWeb: TAdvGlowButton;
    Memo3: TMemo;
    TabSheet17: TTabSheet;
    PMMigracao: TPopupMenu;
    NovoFinanceiro1: TMenuItem;
    ItensdeLote1: TMenuItem;
    AdvGlowButton1: TAdvGlowButton;
    AdvToolBar13: TAdvToolBar;
    AGBNewFinMigra: TAdvGlowButton;
    AGBNewFinSdoIni: TAdvGlowButton;
    AGBCtaFat: TAdvGlowButton;
    AGBFinEncerMes: TAdvGlowButton;
    AGBEncerraBordero: TAdvGlowButton;
    AGBOperaLct: TAdvGlowButton;
    QrLotOpen: TmySQLQuery;
    QrLotOpenNOMECLIENTE: TWideStringField;
    QrLotOpenCNPJCPF: TWideStringField;
    QrLotOpenNOMEFANCLIENTE: TWideStringField;
    QrLotOpenCodigo: TIntegerField;
    QrLotOpenLote: TIntegerField;
    QrLotOpenData: TDateField;
    QrLotOpenNF: TIntegerField;
    QrLotOpenCliente: TIntegerField;
    QrLotOpenTipo: TSmallintField;
    QrLotOpenTotal: TFloatField;
    QrLotOpenLimiCred: TFloatField;
    QrLotOpenCBECli: TIntegerField;
    QrLotOpenSCBCli: TIntegerField;
    QrLotOpenQuantN1: TFloatField;
    QrLotOpenQuantI1: TIntegerField;
    frxLotOpen: TfrxReport;
    frxDsLotOpen: TfrxDBDataset;
    GBAvisos1: TGroupBox;
    Panel1: TPanel;
    LaAviso6: TLabel;
    LaAviso5: TLabel;
    PB1: TProgressBar;
    QrPsqLct: TmySQLQuery;
    QrPsqLctData: TDateField;
    QrPsqLctTipo: TSmallintField;
    QrPsqLctCarteira: TIntegerField;
    QrPsqLctControle: TIntegerField;
    QrPsqLctSub: TSmallintField;
    QrPsqLctFatID: TIntegerField;
    QrPsqLctFatNum: TFloatField;
    QrPsqLctFatParcela: TIntegerField;
    QrPsqLctCompensado: TDateField;
    QrPsqLctVencimento: TDateField;
    QrPsqLctSit: TIntegerField;
    Panel21: TPanel;
    Button1: TButton;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Qr301Chq: TmySQLQuery;
    PageControl1: TPageControl;
    TabSheet18: TTabSheet;
    Ds301Chq: TDataSource;
    Qr301ChqFatSit: TSmallintField;
    Qr301ChqFatSitSub: TSmallintField;
    Qr301ChqCarteira: TIntegerField;
    Qr301ChqITENS: TLargeintField;
    Qr301ChqCredito: TFloatField;
    Qr301ChqDebito: TFloatField;
    Qr301ChqSALDO: TFloatField;
    Qr301ChqNome: TWideStringField;
    Qr301ChqTipo: TIntegerField;
    Qr301ChqNO_FatSit: TWideStringField;
    Qr301ChqNO_FatSitSub: TWideStringField;
    Qr301Its: TmySQLQuery;
    Ds301Its: TDataSource;
    Qr301ItsVencimento: TDateField;
    Qr301ItsCredito: TFloatField;
    Panel22: TPanel;
    Panel23: TPanel;
    DBGrid5: TDBGrid;
    Label16: TLabel;
    Panel24: TPanel;
    DB_Sit_Cheques_Bordero: TDBGrid;
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    DB_Sit_Duplicatas_Bordero: TDBGrid;
    Qr301Dup: TmySQLQuery;
    Ds301Dup: TDataSource;
    Qr301DupFatSit: TSmallintField;
    Qr301DupFatSitSub: TSmallintField;
    Qr301DupCarteira: TIntegerField;
    Qr301DupITENS: TLargeintField;
    Qr301DupCredito: TFloatField;
    Qr301DupDebito: TFloatField;
    Qr301DupSALDO: TFloatField;
    Qr301DupNome: TWideStringField;
    Qr301DupTipo: TIntegerField;
    Qr301DupNO_FatSit: TWideStringField;
    Qr301DupNO_FatSitSub: TWideStringField;
    Panel25: TPanel;
    BtPesquisa: TBitBtn;
    Qr301Rel: TmySQLQuery;
    frxDs301Rel: TfrxDBDataset;
    Qr301RelCliente: TIntegerField;
    Qr301RelBanco: TIntegerField;
    Qr301RelAgencia: TIntegerField;
    Qr301RelContaCorrente: TWideStringField;
    Qr301RelDocumento: TFloatField;
    Qr301RelEmitente: TWideStringField;
    Qr301RelVencimento: TDateField;
    Qr301RelCredito: TFloatField;
    Qr301RelFatNum: TFloatField;
    Qr301RelNO_CLI: TWideStringField;
    frxPrincipal_301Rel: TfrxReport;
    Qr301RelFatParcela: TIntegerField;
    TabSheet19: TTabSheet;
    Button2: TButton;
    AdvToolBar5: TAdvToolBar;
    AGBTedCCfg: TAdvGlowButton;
    AGBTedCOcoSys: TAdvGlowButton;
    AdvToolBar17: TAdvToolBar;
    AGBTedRem: TAdvGlowButton;
    AGBTedRet: TAdvGlowButton;
    AGBTedRetGer: TAdvGlowButton;
    Ed16: TdmkEdit;
    Ed10: TdmkEdit;
    dmkEdit5: TdmkEdit;
    Button3: TButton;
    dmkEdit6: TdmkEdit;
    dmkEdit7: TdmkEdit;
    dmkEdit8: TdmkEdit;
    Button5: TButton;
    Qr301ItsANO: TFloatField;
    Qr301ItsMES: TFloatField;
    Button8: TButton;
    TmSuporte: TTimer;
    TySuporte: TTrayIcon;
    AdvPage1: TAdvPage;
    AdvToolBar32: TAdvToolBar;
    AdvGlowButton2: TAdvGlowButton;
    AdvPage7: TAdvPage;
    AdvToolBar33: TAdvToolBar;
    AGBNFSeSrvCad: TAdvGlowButton;
    AGBNFSeMenCab: TAdvGlowButton;
    AdvToolBar34: TAdvToolBar;
    AGBNFSe_Edit: TAdvGlowButton;
    AGBNFSeLRpsC: TAdvGlowButton;
    AGBNFSe_RPSPesq: TAdvGlowButton;
    AGBNFSe_NFSePesq: TAdvGlowButton;
    AGBNFSeFatCab: TAdvGlowButton;
    AdvToolBar35: TAdvToolBar;
    AGBListServ: TAdvGlowButton;
    AGBErrosAlertas: TAdvGlowButton;
    AdvGlowButton3: TAdvGlowButton;
    VerificaBDPblicas1: TMenuItem;
    AdvGlowButton5: TAdvGlowButton;
    AGBFiliais: TAdvGlowButton;
    AdvGlowButton23: TAdvGlowButton;
    AdvToolBarButton7: TAdvToolBarButton;
    AdvToolBarButton8: TAdvToolBarButton;
    AdvToolBarButton9: TAdvToolBarButton;
    AdvToolBarButton10: TAdvToolBarButton;
    AdvToolBarButton13: TAdvToolBarButton;
    AdvToolBarButton12: TAdvToolBarButton;
    PMGeral: TPopupMenu;
    Suporte1: TMenuItem;
    Reabrirtabelas1: TMenuItem;
    N1: TMenuItem;
    Minimizaraplicativo1: TMenuItem;
    BalloonHint1: TBalloonHint;
    TmVersao: TTimer;
    AdvToolBar18: TAdvToolBar;
    AGBVerificaVersoes: TAdvGlowButton;
    AdvGlowButton165: TAdvGlowButton;
    AdvGlowButton167: TAdvGlowButton;
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SBCadEntidadesClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Saldodecontas1Click(Sender: TObject);
    procedure TimerIdleTimer(Sender: TObject);
    procedure AdvGlowButton17Click(Sender: TObject);
    procedure Escolher2Click(Sender: TObject);
    procedure Padro2Click(Sender: TObject);
    procedure Padro3Click(Sender: TObject);
    procedure AGBFinancasClick(Sender: TObject);
    procedure AGBEntiCadClick(Sender: TObject);
    procedure AGBFinPlanoClick(Sender: TObject);
    procedure AGBFinCjtClick(Sender: TObject);
    procedure AGBFinGruClick(Sender: TObject);
    procedure AGBSubGruClick(Sender: TObject);
    procedure AGBFinCtaClick(Sender: TObject);
    procedure AGBFinNivClick(Sender: TObject);
    procedure AGBFinListaClick(Sender: TObject);
    procedure AGBFinSaldosClick(Sender: TObject);
    procedure AGBFinCartClick(Sender: TObject);
    procedure AdvGlowButton16Click(Sender: TObject);
    procedure AGMBBackupClick(Sender: TObject);
    procedure AGBBancosClick(Sender: TObject);
    procedure Desativar1Click(Sender: TObject);
    procedure CkPTK_AbertosClick(Sender: TObject);
    procedure GradePPICellClick(Column: TColumn);
    procedure GradePPIDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure BitBtn17Click(Sender: TObject);
    procedure BitBtn18Click(Sender: TObject);
    procedure BitBtn19Click(Sender: TObject);
    procedure GradePKACellClick(Column: TColumn);
    procedure GradePKADrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure BtRefreshClick(Sender: TObject);
    procedure BtComandoClick(Sender: TObject);
    procedure BtRefrPrtoClick(Sender: TObject);
    procedure BtNegritoClick(Sender: TObject);
    procedure BtNormalClick(Sender: TObject);
    procedure QrDuplAfterScroll(DataSet: TDataSet);
    procedure AGBAcesRapidClick(Sender: TObject);
    procedure AGBGerCondClick(Sender: TObject);
    procedure BtMenuClick(Sender: TObject);
    procedure AGBDepositoClick(Sender: TObject);
    procedure AGBNewFinSdoIniClick(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure AGBFinEncerMesClick(Sender: TObject);
    procedure BtBalanceteClick(Sender: TObject);
    procedure BtReceDespClick(Sender: TObject);
    procedure AGBPesqPagClick(Sender: TObject);
    procedure AGBBloqParcClick(Sender: TObject);
    procedure RGProtoFiltroClick(Sender: TObject);
    procedure AGBGerPropClick(Sender: TObject);
    procedure AGBChequesAvulsos2Click(Sender: TObject);
    procedure AGBInadimplencia1Click(Sender: TObject);
    procedure AGBBaixaLoteWebClick(Sender: TObject);
    procedure AGBRepaLocClick(Sender: TObject);
    procedure AdvGlowButton42Click(Sender: TObject);
    procedure AdvGlowButton41Click(Sender: TObject);
    procedure AGBAgrExtrCtaClick(Sender: TObject);
    procedure AGBLinkConcBcoClick(Sender: TObject);
    procedure AGBFeriadosClick(Sender: TObject);
    procedure AGBRemConfigClick(Sender: TObject);
    procedure VerificaBDServidor1Click(Sender: TObject);
    procedure VerificaBDWeb1Click(Sender: TObject);
    procedure AdvGlowButton82Click(Sender: TObject);
    procedure AdvGlowButton83Click(Sender: TObject);
    procedure AGBGrupUHs1Click(Sender: TObject);
    procedure AdvGlowButton90Click(Sender: TObject);
    procedure AdvGlowButton94Click(Sender: TObject);
    procedure AdvGlowButton93Click(Sender: TObject);
    procedure AdvGlowButton92Click(Sender: TObject);
    procedure ResultadosMensais1Click(Sender: TObject);
    procedure Movimento1Click(Sender: TObject);
    procedure PorNveldoPLanodeContas1Click(Sender: TObject);
    procedure ContasControladas1Click(Sender: TObject);
    procedure ContasSazonais1Click(Sender: TObject);
    procedure Futuros1Click(Sender: TObject);
    procedure Em1Click(Sender: TObject);
    procedure AGBThemeClick(Sender: TObject);
    procedure AdvGlowButton19Click(Sender: TObject);
    procedure UnicaEmpresa1Click(Sender: TObject);
    procedure MultiplasEmpresas1Click(Sender: TObject);
    procedure MovimentoPlanodecontas1Click(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure AGBPlanRelCabClick(Sender: TObject);
    procedure AGBEuroExportClick(Sender: TObject);
    procedure AGBResultados2Click(Sender: TObject);
    procedure AGBInadimplencia2Click(Sender: TObject);
    procedure AGBEvolucapi2Click(Sender: TObject);
    procedure BtImportaClick(Sender: TObject);
    procedure EdBandaChange(Sender: TObject);
    procedure EdCPFChange(Sender: TObject);
    procedure EdCPFExit(Sender: TObject);
    procedure EdCPFKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure EdEmitenteEnter(Sender: TObject);
    procedure BtConfAltClick(Sender: TObject);
    procedure Bruto2Click(Sender: TObject);
    procedure LquidoNovo1Click(Sender: TObject);
    procedure GerenciaResultados1Click(Sender: TObject);
    procedure Antigo2Click(Sender: TObject);
    procedure NovoEmconstruo1Click(Sender: TObject);
    procedure Cheques2Click(Sender: TObject);
    procedure Duplicatas1Click(Sender: TObject);
    procedure Relatorios1Click(Sender: TObject);
    procedure MenuItem5Click(Sender: TObject);
    procedure MenuItem6Click(Sender: TObject);
    procedure AGBEvolucapi1Click(Sender: TObject);
    procedure AGBResultados1Click(Sender: TObject);
    procedure AdvGlowButton4Click(Sender: TObject);
    procedure BtEuroExport1Click(Sender: TObject);
    procedure AdvGlowButton84Click(Sender: TObject);
    procedure AGBContratoClick(Sender: TObject);
    procedure AdvGlowButton27Click(Sender: TObject);
    procedure AdvGlowButton28Click(Sender: TObject);
    procedure AdvGlowButton9Click(Sender: TObject);
    procedure AGBCheque_0Click(Sender: TObject);
    procedure AdvGlowButton11Click(Sender: TObject);
    procedure AdvGlowButton13Click(Sender: TObject);
    procedure AdvGlowButton10Click(Sender: TObject);
    procedure AGBJanelaImpressaoClick(Sender: TObject);
    procedure AGBAlinChDevClick(Sender: TObject);
    procedure AGBAlinDuplicataClick(Sender: TObject);
    procedure AGBAlinPagChDevolvClick(Sender: TObject);
    procedure AGBOcorrenciasClick(Sender: TObject);
    procedure AGBTaxasClick(Sender: TObject);
    procedure AGBSacadosClick(Sender: TObject);
    procedure Contratos1Click(Sender: TObject);
    procedure extos2Click(Sender: TObject);
    procedure AGBClienteRapidoClick(Sender: TObject);
    procedure AGBCNABRetEnvClick(Sender: TObject);
    procedure AGBGruSacEmiClick(Sender: TObject);
    procedure AGB_SPC_ConfigClick(Sender: TObject);
    procedure AGB_SPC_ModalidClick(Sender: TObject);
    procedure AGBNFsCadClick(Sender: TObject);
    procedure AGBRiscoAll2Click(Sender: TObject);
    procedure VerificaBDsTerceiros1Click(Sender: TObject);
    procedure AGBArquivoMortoClick(Sender: TObject);
    procedure AGBLocPagClick(Sender: TObject);
    procedure AGBServicoManagerClick(Sender: TObject);
    procedure AGBVerificaVersoesClick(Sender: TObject);
    procedure AGBAtzBorderoClick(Sender: TObject);
    procedure AGBBAncoDadosClick(Sender: TObject);
    procedure AGBDecompClick(Sender: TObject);
    procedure AGBCompostoClick(Sender: TObject);
    procedure AGBCompJurosClick(Sender: TObject);
    procedure AGBSimulaParcelaClick(Sender: TObject);
    procedure AGBDepositClick(Sender: TObject);
    procedure AGBSPC_PesqClick(Sender: TObject);
    procedure AGBAtzCliItsBorderoClick(Sender: TObject);
    procedure AGBCorrigeEmpresaClick(Sender: TObject);
    procedure AGBExcluiItensLoteClick(Sender: TObject);
    procedure AGBMySQLClick(Sender: TObject);
    procedure AGBConexaoClick(Sender: TObject);
    procedure AGBExpContabClick(Sender: TObject);
    procedure AGBExpContabExpClick(Sender: TObject);
    procedure AGBEmitentesClick(Sender: TObject);
    procedure AGBEmiSacExportClick(Sender: TObject);
    procedure AGBEmiSacImportClick(Sender: TObject);
    procedure AGBWSincroClick(Sender: TObject);
    procedure AGBWClientsClick(Sender: TObject);
    procedure AGBLSincroClick(Sender: TObject);
    procedure AGBAjusteWebClick(Sender: TObject);
    procedure AGBOcorDupGerClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure NovoFinanceiro1Click(Sender: TObject);
    procedure AGBNewFinMigraClick(Sender: TObject);
    procedure PMMigracaoPopup(Sender: TObject);
    procedure ItensdeLote1Click(Sender: TObject);
    procedure AGB_CNAB_RemessaClick(Sender: TObject);
    procedure AGB_CNAB_RetornoClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure AGBCtaFatClick(Sender: TObject);
    procedure AGBEncerraBorderoClick(Sender: TObject);
    procedure AGBOperaLctClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure Qr301ChqCalcFields(DataSet: TDataSet);
    procedure Qr301DupCalcFields(DataSet: TDataSet);
    procedure BtPesquisaClick(Sender: TObject);
    procedure AGBLivre1_6_2Click(Sender: TObject);
    procedure AGBTedCCfgClick(Sender: TObject);
    procedure AGBTedRemClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure AGBTedRetClick(Sender: TObject);
    procedure AGBTedRetGerClick(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure AGBTedCOcoSysClick(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure TmSuporteTimer(Sender: TObject);
    procedure AdvGlowButton2Click(Sender: TObject);
    procedure AGBNFSe_EditClick(Sender: TObject);
    procedure AGBNFSe_NFSePesqClick(Sender: TObject);
    procedure AGBNFSe_RPSPesqClick(Sender: TObject);
    procedure AGBNFSeFatCabClick(Sender: TObject);
    procedure AGBNFSeLRpsCClick(Sender: TObject);
    procedure AGBNFSeSrvCadClick(Sender: TObject);
    procedure AGBNFSeMenCabClick(Sender: TObject);
    procedure AGBListServClick(Sender: TObject);
    procedure AdvGlowButton3Click(Sender: TObject);
    procedure AGBErrosAlertasClick(Sender: TObject);
    procedure VerificaBDPblicas1Click(Sender: TObject);
    procedure AdvGlowButton5Click(Sender: TObject);
    procedure AGBFiliaisClick(Sender: TObject);
    procedure AdvGlowButton23Click(Sender: TObject);
    procedure AdvToolBarButton7Click(Sender: TObject);
    procedure AdvToolBarButton8Click(Sender: TObject);
    procedure AdvToolBarButton9Click(Sender: TObject);
    procedure AdvToolBarButton10Click(Sender: TObject);
    procedure AdvToolBarButton13Click(Sender: TObject);
    procedure AdvToolBarButton12Click(Sender: TObject);
    procedure Suporte1Click(Sender: TObject);
    procedure Reabrirtabelas1Click(Sender: TObject);
    procedure Minimizaraplicativo1Click(Sender: TObject);
    procedure TmVersaoTimer(Sender: TObject);
    procedure AdvGlowButton167Click(Sender: TObject);
    procedure AdvGlowButton165Click(Sender: TObject);
  private
    { Private declarations }
    FWinsc: TIniFile;
    FOldVersion: Integer;
    FALiberar, FAtualizouFavoritos: Boolean;
{
    FTamLinha, FFoiExpand: Integer;
    FArqPrn: TextFile;
    FPrintedLine: Boolean;
}
    //
    procedure CriaCMC7s();
    procedure LeCMC7_Criados();
    procedure ProcuraBACs();
    function LocalizaEmitente(Linha: Integer; MudaNome: Boolean): Boolean;
    function CriaBAC_Pesq(Banco, Agencia, Conta: String; DVCC: Integer): String;
    //
    procedure MostraOpcoes;
    procedure MostraLogoff;
    procedure MostraMatriz();
    procedure SkinMenu(Index: integer);
    procedure VerificaOrelhas();
    procedure MostraFiliais();
  public
    { Public declarations }
    FCliInt_, FFormLotShow: Integer;
    FChangeData: Boolean;
    FCr3: String;
    FDirR: String;
    FVerArqSCX: Integer;
    FLoteLoc, FloteItsLoc: Integer;
    FConnections: Integer;
    FAplicaInfoRegEdit: Boolean;
    FReiniciarAppDB: Boolean;
    FForcaDesconexao: Boolean;
    FMyDBs: TMeusDBs;
    FMinhaGrade1: TStringGrid;
    FImportPath: String;
    // Compatibilidade
    FCheque, FDuplicata: Integer;
    FTipoNovoEnti: Integer;
    //
    FImpCHSimVal, FImpCHSimExt, FImpCHSimEnt, FImpCHSimCid,
    FImpCHSimDia, FImpCHSimMes, FImpCHSimAno: String;
    FFTPExiste: Boolean;
    FFTPDirArq: String;
    FTipoImport: TImportLote;
    FEntInt: Integer;
    FDmodCriado: Boolean;
    // vindos do FmLot2Cab
    FLoteWeb, FLoteWebDone, FControlIts, FGradeFocused: Integer;
    FCPFSacado: String;
    // fim FmLot2Cab
{   Syndic2
    FIncI: Integer;
    FBMPDescanso: TBitmap;
    FJaAbriuFmCondGer: Boolean;
    FCliInt_, FEntInt: Integer;
    FTipoNovoEnti: Integer;
    FDuplicata, FCheque, FVerArqSCX, FCliInt: Integer;
    FImagemDescanso: String;
    FImportPath: String;
    FContasMesEmiss: Integer;
}
    FDatabase1A, FDatabase1B, FDatabase2A, FDatabase2B: TmySQLDatabase;

    procedure VerificaBDsTerceiros();
    procedure MostraOpcoesCreditoX();
    procedure AcoesIniciaisDoAplicativo();
    //
    function  AdicionaMinutosSQL(HI, HF: TTime): Boolean;
    procedure AtualizaTextoBtAcessoRapido();
    procedure BalanceteConfiguravel(TabLctA: String);
    procedure CalculaCambioAlterado(MyProgress: TProgressBar);
    //procedure DefineVarsCliInt(CliInt: Integer);
    procedure DemonstrativoDeReceitasEDespesas(Empresa: Integer);
    function  PreparaMinutosSQL(): Boolean;
    procedure ShowHint(Sender: TObject);

    procedure AcoesExtrasDeCadastroDeEntidades(Grade1: TStringGrid;
              Entidade: Integer; AbrirEmAba: Boolean);
    procedure AcoesIniciaisDeCadastroDeEntidades(Reference: TComponent;
              Codigo: Integer; Grade1: TStringGrid);
    function RecriaTabelaLocal(Tabela: String; Acao: Integer): Boolean;
    procedure SalvaArquivo(EdNomCli, EdCPFCli: TEdit; Grade:
              TStringGrid; Data: TDateTime; FileName: String; ChangeData: Boolean);
    function AbreArquivoINI(Grade: TStringGrid; EdNomCli, EdCPFCli: TEdit;
             Data: TDateTime; Arquivo: String): Boolean;

    procedure CadastroDeFeriados();

    // Cashier
    function CartaoDeFatura: Integer;
    function CompensacaoDeFatura: String;
    function AcaoEspecificaDeApp(Servico: String): Boolean;
    //procedure CriaCalcPercent(Valor, Porcentagem: String; Calc: TcpCalc );
    function VerificaNovasVersoes(ApenasVerifica: Boolean = False): Boolean;
    procedure CadastroDeContasNiv();
    //
    function  LetraModelosBloq(Item: Integer): String;
    procedure PreencheModelosBloq(RG: TRadioGroup);
    function  MigraEspecificos(CliInt: Integer): Boolean;
    //
    procedure ReCaptionComponentesDeForm(Form: TForm);
    //
    procedure AtualizaCreditado(Banco, Agencia, Conta, CPF, Nome:
              String; Cliente, LastPaymt: Integer);
    procedure ImprimeChequeSimples(Valor, Creditado, Cidade: String;
              Data: TDateTime; Nominal: Boolean);
    procedure AdicionaBorderoANF(NF, Bordero: Integer);
    procedure CriaFormLot(FormShow, Tipo, Lote, FatParcela: Integer);
    procedure CadastroGruSacEmi(Grupo: Integer);
    function VerificaDiasCompensacao(Praca, CBE: Integer; Valor: Double;
             DtEmiss, DtVence: TDateTime; SBC: Integer): Integer;
    procedure AplicaInfoRegEdit(Form: TForm; Forca: Boolean);
    procedure SalvaInfoRegEdit(Form: TForm; Dest: Integer;
              MinhaGrade: TStringGrid);
    procedure AtualizaSacado(CNPJ, Nome, Rua: String; Numero: Integer;
              Compl, Bairro, Cidade, UF, CEP, Tel1, IE, Email: String; Risco: Double;
              Importando: Boolean = True);
    procedure TextosCartasGetValue(const ParName: String;
              var ParValue: Variant);
    procedure AtualizaSP2(Panel: Integer);
    procedure AtualizaLastEditLote(Data: String);
    procedure DepositoDeCheques(Config: Integer);
    procedure CadastroOcorBank(Codigo: Integer);
    procedure AtualizaClientesLotIts();
    procedure ObtemTipoEPercDeIOF2008(const TipoEnt, EhSimples: Integer;
              const ValOperacao: Double; var Perc: Double; var TipoIOF: Integer);
    procedure Importar(Tipo: TImportLote; CtrlIts: Integer; DarIni: String = '');
    procedure InsereImportLote(Pagina: TPageControl;
              GradeC, GradeD: TStringGrid; TPCheque: TdmkEditDateTimePicker);
    procedure AtualizaAcumulado(PB1: TProgressBar);

    procedure MostraDuplicatas2(Controle: Extended);
    procedure MostraRemessaCNABNovo(Codigo, FatParcela, Bordero: Integer);
    function  AtualizaDefinicaoDeFatID_0301(): Boolean;
    procedure ReopenFatID_0301();
    function  PreparaPesquisaFinanceira(TabLctA: String): Boolean;

    procedure MostraDiarioGer(Assunto, Contato: Integer; Depto, Texto: String);
    procedure MostraDiarioAdd(Assunto, Contato, Depto: Integer; Texto: String);
    procedure CadastroDeProtocolos(Lote: Integer);
    procedure MostraEntiJur1(Codigo: Integer);
    procedure RetornoCNAB();
    procedure MostraTaxas(Codigo: Integer);
    procedure MostraCartaG(Codigo: Integer);

    function  FixBugAtualizacoesApp(FixBug: Integer; FixBugStr: String): Boolean;
  end;

var
  FmPrincipal: TFmPrincipal;

const
  SExtraBDsCaminho = 'Software\Cred' + 'itor\Empresas';
  FVersaoSuitCash = '07.02.04.1226';
  FDermatekCamCli = 'C:\Dermatek\Clientes\';
//const FTPLot es
  FFTPCodeAtivo  = 226;
  FMaxTime = 30000;
  FTicTime =  1000;

implementation

uses
  Module, Entidades, VerifiDB, MyGlyfs, MyDBCheck, Credito_DMK, NFs,
  Opcoes, MyVCLSkin, UCreate, Backup3, FavoritosG, APCD, PertoChek, UnMyPrinters,
  UnFinanceiro, ContasNiv, NovaVersao, Bancos, Taxas, Cartas, LinkRankSkin,
  CartaG, ModuleGeral, Entidade2, Matriz, Anotacoes, ModuleLct2,
  ModuleFin, ReceDesp, CashBal, VerifiDBi, Maladireta, Feriados, OpcoesCreditoX,
  VerifiDBy, Deposimp, Lot0Dep, PesqDep, EntiJur1, GerDup1Main, MaloteConf,
  GerCliOcor, RiscoAll_1, HistCliEmi, SomaDiasCred, Inadimps, Promissoria,
  Duplicata1, SociosImp, ContratoImp, OperaCol, OperaImp,
  Sacados, SPC_Config, SPC_Modali, Emitentes, Alineas, UnEntities,
  AlineasDup, CNAB_Cfg, OcorBank, Evolucap2, RepasImp, Evolucapi, Result1,
  RepasDUCad, OperaAll, RepasCHCad, GruSacEmi, BancosDados, ServicoManager,
  SPC_Pesq, LocPag, SimulaParcela, Deposito,
  CompJuros, Composto, ChequesExtras, Decomp, Servidor, MyInis, ExpContab1,
  EmitSacExport, EmitSacImport, ExpContabExp, LSincro, wclients, WSincro,
  Lot2Cab, Lot2Inn, ModuleLot, Lot2Loa, ModuleLot2, GerChqMain, GerCliMain,
  Lot2MIF, UnGOTOy, CNAB_Ret2, CNAB_Lot, LeDataArq, CtaFat0,
  Lot2Enc, OperaLct, Lot0Sbr, TedC_Aux, DiarioAdd, DiarioGer, UnDmkWeb,
  Lot2DupMu1, CNAE21Cad, NFSe_PF_0000, NFSe_PF_0201, UnFixBugs,
  VerifiDBTerceiros, ParamsEmp, PreEmail, UnFinanceiroJan, About;

  // PreEmail, CNAB_Cfg, DiarioAdd, DiarioGer, DiarioAss, VerifiDBLocal,
  // Promissoria, Duplicata1, wclients, ConfigBol, ContasAgr, CNAB_Dir,
  // CNAB_Ret2a, ProtocoMot, CashPreCta, PlanRelCab, ContasConfEmisAll,
  // InfoSeq, CNAB_Fld, LeDataArq, CalcMod10e11,


 // ChConfigCab, UnMyPrinters, wusers, CalcPercent,

  //ModuleAtencoes, WSincro2, uploads, RelChAbertos,
const
  FAltLin = CO_POLEGADA / 2.16;

{$R *.DFM}

{ TBandaMagnetica }

procedure TBandaMagnetica.DestrincharBanda(Banda: String);
begin
  if FBandaMagnetica <> Banda then FBandaMagnetica := Banda;
  FDv3     := Copy(FBandaMagnetica,30,1)[1];
  FConta   := Copy(FBandaMagnetica,20,10);
  FDv1     := Copy(FBandaMagnetica,19,1)[1];
  FTipo    := Copy(FBandaMagnetica,18,1)[1];
  FNumero  := Copy(FBandaMagnetica,12,6);
  FCompe   := Copy(FBandaMagnetica,9,3);
  FDv2     := Copy(FBandaMagnetica,8,1)[1];
  FAgencia := Copy(FBandaMagnetica,4,4);
  FBanco   := Copy(FBandaMagnetica,1,3);
end;

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

{ TFmPrincipal }

function TFmPrincipal.FixBugAtualizacoesApp(FixBug: Integer;
  FixBugStr: String): Boolean;
begin
  Result := True;
  (* Marcar quando for criar uma fun��o de atualiza��o
  try
    if FixBug = 0 then
      Atualiza��o1()
    else if FixBug = 1 then
      Atualiza��o2()
    else if FixBug = 2 then
      Atualiza��o3()
    else
      Result := False;
  except
    Result := False;
  end;
  *)
end;

procedure TFmPrincipal.FormActivate(Sender: TObject);
begin
  APP_LIBERADO := True;
  MyObjects.CorIniComponente();
  VAR_ATUALIZANDO := False;
  VAR_APPNAME := Application.Title;
  if Geral.VersaoTxt2006(CO_VERSAO) <>
    Geral.FileVerInfo(Application.ExeName, 3 (*Versao*)) then
    ShowMessage('Vers�o difere do arquivo');
  if not FALiberar then Timer1.Enabled := True;
end;

procedure TFmPrincipal.FormCreate(Sender: TObject);
var
  MenuStyle: Integer;
  Imagem, ExtraTxt: String;
begin
  FAtualizouFavoritos := False;
  //
  VAR_CliIntUnico := -11;
  VAR_FilialUnica := 1;
  VAR_TYPE_LOG := ttlCliIntUni;
  FCr3 := '/z8niKfqbXvTw9xe';
  FDirR := 'C:\Dermatek\FTP_scx\';
  VAR_USA_TAG_BITBTN := True;
  // Servidor ou cliente (pen drive - rede)
  VAR_SERVIDOR3 := 3;
  //App version
  FWinsc := TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini'));
  try
    FOldVersion := FWinsc.ReadInteger('wsc', 'vernfo', 1);
  finally
    FWinsc.Free;
  end;
  //
  ExtraTxt := '';
  //VerAtual := Geral.VersaoInt2006(CO_VERSAO);
  //
  Pagina.ActivePageIndex       := 0;
  PageControl2.ActivePageIndex := 0;
  //
  TabSheet3.TabVisible  := False;
  //TabSheet7.TabVisible  := False;
  TabSheet9.TabVisible  := False;
  TabSheet12.TabVisible := False;
  TabSheet8.TabVisible  := False;
  //
  VAR_IP_LICENCA := Geral.ReadAppKey('IPClient', Application.Title,
    ktString, '', HKEY_LOCAL_MACHINE);
  FTipoNovoEnti := 1000;
  FMyDBs := TMeusDBs.Create;
  FMyDBS.RegistryKey := HKEY_LOCAL_MACHINE;
  FMyDBS.StringGrid  := Grade;
  FMyDBS.KeyPath     := SExtraBDsCaminho;
  FMyDBs.Reload;
  //
  VAR_BDsEXTRAS     := ' :: '+IntToStr(FMyDBs.MaxDBs);
  VAR_REPRES_ENTIJUR1 := 'Fornece5';
  VAR_ENTICREDS     := True;
  VAR_STLOGIN       := StatusBar.Panels[01];
  StatusBar.Panels[3].Text := Geral.VersaoTxt2006(CO_VERSAO);
  VAR_STTERMINAL    := StatusBar.Panels[05];
  VAR_STDATALICENCA := StatusBar.Panels[07];
  VAR_SKINUSANDO    := StatusBar.Panels[09];
  VAR_STDATABASES   := StatusBar.Panels[11];
  VAR_STIMPCHEQUE   := StatusBar.Panels[13];
  VAR_TIPOSPRODM_TXT := '0,1,2,3,4,5,6,7,8,9,10,11,12,13';
  VAR_APP := ExtractFilePath(Application.ExeName);
  VAR_VENDEOQUE := 1;
  //
  Application.OnHint      := ShowHint;
  Application.OnException := MyObjects.MostraErro;
  Application.OnMessage   := MyObjects.FormMsg;
  //2017-07-19 => N�o ativar por causa da c�mera Application.OnIdle := AppIdle;
  TimerIdle.Interval := VAR_TIMERIDLEITERVAL;
  TimerIdle.Enabled  := True;
  //Application.OnActionExecute := AppActionExecute;
  //
  Imagem := Geral.ReadAppKey(
    'ImagemFundo', Application.Title, ktString, VAR_APP+'Fundo.jpg', HKEY_LOCAL_MACHINE);
  if FileExists(Imagem) then ImgPrincipal.Picture.LoadFromFile(Imagem);
  VAR_CAD_POPUP := PMGeral;
  CRD_Consts.SetaVariaveisIniciais;
  Geral.DefineFormatacoes;
  //
  //VAR_TIMERIDLE := TimerIdle; 2017-05-24 => Foi criada um fun��o que executa no OnIdle para substituir
  TPCheque.Date := Date;
  {}
  ArqSCX.ConfiguraGrades(GradeC, nil, nil);
  GradeC.ColCount := GradeC.ColCount + 1;
  GradeC.Cells[GradeC.ColCount-1, 0] := 'CMC7';
  GradeC.ColWidths[GradeC.ColCount-1] := 200;
  {}





  //
  VAR_TIPO_TAB_LCT := 1;
  FDmodCriado := False;
  // Deixar invis�vel
  AlphaBlendValue := 0;
  AlphaBlend := True;
  //
  Pagina.ActivePageIndex := 0;
  AdvToolBarPager1.ActivePageIndex := 0;
  //
  VAR_TYPE_LOG := ttlCliIntUni;
  //
  VAR_USA_TAG_BITBTN := True;
  VAR_MULTIPLAS_TAB_LCT := True;
  VAR_SERVIDOR3 := 3;
  FTipoNovoEnti := 0;
  //
  MenuStyle := Geral.ReadAppKeyCU('MenuStyle', Application.Title,
    ktInteger, Integer(bsOffice2007Luna));
  SkinMenu(MenuStyle);
  //
  MyObjects.CopiaItensDeMenu(PMGeral, FmPrincipal);
  //
  //:://:://:://:://::

  FmPrincipal.FDatabase1A := nil;
  FmPrincipal.FDatabase1B := nil;
  FmPrincipal.FDatabase2A := nil;
  FmPrincipal.FDatabase2B := nil;

  // N�o tem!
  //VAR_NOME_TAB_CLIINT := '';
  //
  VAR_NaoReabrirLct   := False;
  //VAR_FP_EMPRESA := 'Cliente1="V"';
  //VAR_FP_FUNCION := '(Fornece2="V" OR Fornece4="V")';

  //////////////////////////////////////////////////////////////////////////////
  VAR_FL_DataIni := Date - Geral.ReadAppKeyCU('Dias', Application.Title,
    ktInteger, 60);
  VAR_FL_DataFim := Date;
  //////////////////////////////////////////////////////////////////////////////
  // Local
  VAR_IMPCHEQUE       := Geral.ReadAppKeyCU('ImpCheque', 'Dermatek', ktInteger, 0);
  VAR_IMPCHEQUE_PORTA := Geral.ReadAppKeyCU('ImpCheque_Porta', 'Dermatek', ktString, 'COM2');
  // Servidor
  VAR_IMPCH_IP        := Geral.ReadAppKeyCU('ImpreCh_IP', 'Dermatek', ktString, '127.0.0.1');
  VAR_IMPCHPORTA      := Geral.ReadAppKeyCU('ImpreChPorta', 'Dermatek', ktInteger, 9520);
  //
  TabSheet2.TabVisible  := False; //Protocolos
  TabSheet3.TabVisible  := False; //Pend�ncias
  TabSheet4.TabVisible  := False; //Outros
  TabSheet7.TabVisible  := False; //CNAB
  TabSheet8.TabVisible  := False; //Coligadas
  TabSheet11.TabVisible := False; //Lctos s/ m�s
  TabSheet9.TabVisible  := False; //Pertochek
  TabSheet14.TabVisible := False; //Importa Bco
  TabSheet5.TabVisible  := False; //Lote(s) FTP
  TabSheet17.TabVisible := False; //Novo creditor
  TabSheet19.TabVisible := False; //TabSheet19
  //
  MyObjects.MaximizaAdvToolBarPager(AdvToolBarPager1, deftfTrue);
end;

procedure TFmPrincipal.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if VAR_IMPRECHEQUE = 1 then
    PertoChekP.DesabilitaPertoCheck;
    (*DmDados.TbMaster. Open;
    DmDados.TbMaster.Edit;
    if Trunc(Date) > DmDados.TbMasterLA.Value then
      DmDados.TbMasterLD.Value := DmDados.TbMasterLD.Value +
      (Trunc(Date) - Trunc(DmDados.TbMasterLA.Value));
    if Trunc(Date) < DmDados.TbMasterLA.Value then
      DmDados.TbMasterLD.Value := DmDados.TbMasterLD.Value + 3;
    if (Trunc(Date) = DmDados.TbMasterLA.Value) and
       (Time < DmDados.TbMasterLH.Value)  then
      DmDados.TbMasterLD.Value := DmDados.TbMasterLD.Value + 1;
    DmDados.TbMasterLA.Value := Trunc(Date);
    DmDados.TbMasterLH.Value := Time;
    DmDados.TbMaster.Post;
    DmDados.TbMaster.Close;
    FmSkin.Close;
    FmSkin.Destroy;*)
  if DModG <> nil then
    DModG.MostraBackup3();
  //
  Application.Terminate;
end;

procedure TFmPrincipal.PreencheModelosBloq(RG: TRadioGroup);
begin
  RG.Items.Clear;
  {00} RG.Items.Add('? - Definir no momento da impress�o');
  {01} RG.Items.Add('A - 3 colunas fonte tamanho 7 sem rodap� (3 n�veis)');
  {02} RG.Items.Add('B - 3 colunas fonte tamanho 6 sem rodap� (3 n�veis)');
  {03} RG.Items.Add('C - 3 colunas fonte tamanho 7 com rodap� (3 n�veis)');
  {04} RG.Items.Add('D - 2 colunas fonte tamanho 6 com rodap� (3 n�veis)');
  {05} RG.Items.Add('E - Somente bloqueto (nenhum n�vel)');
  //{06} RG.Items.Add('F - 3 colunas fonte tamanho 7 com rodap� (4 n�veis)');
  {06} RG.Items.Add('F - Extinto (se selecionado ser� usado o modelo G)');
  {07} RG.Items.Add('G - 3 colunas fonte tamanho 7 com rodap� (4 n�veis)');
  {08} RG.Items.Add('H - Colunas, fonte e tamanho vari�vel (4 n�veis) - C�digo de barras');
  {09} RG.Items.Add('R - Colunas, fonte e tamanho vari�vel (4 n�veis) - Recibo');
  {10} RG.Items.Add('IB - 1 coluna, fonte e tamanho vari�vel (1 n�vel)');
  {11} RG.Items.Add('IR - 1 coluna, fonte e tamanho vari�vel (1 n�vel) - Recibo');
end;

function TFmPrincipal.PreparaMinutosSQL(): Boolean;
//var
//  i: Integer;
begin
  Result := True;
  try
    Dmod.QrUpdL.SQL.Clear;
    Dmod.QrUpdL.SQL.Add('DELETE FROM Ocupacao');
    Dmod.QrUpdL.ExecSQL;
    ///
(*    Dmod.QrUpdL.SQL.Clear;
    Dmod.QrUpdL.SQL.Add('INSERT INTO Ocupacao SET Min=:P0');
    for i := 1 to 1440 do
    begin
      Dmod.QrUpdL.Params[0].AsInteger := I;
      Dmod.QrUpdL.ExecSQL;
    end;*)
  except
    Result := False
  end;
end;

function TFmPrincipal.PreparaPesquisaFinanceira(TabLctA: String): Boolean;
var
  MyCursor: TCursor;
begin
  Result := True;
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  //
  UMyMod.AbreQuery(DmLot.QrLot, Dmod.MyDB);
  if DmLot.QrLot.RecordCount > 0 then
  begin
    if Geral.MensagemBox('Existem ' + Geral.FF0(DmLot.QrLot.RecordCount) +
    ' lotes de border�s n�o encerrados que poder�o distorcer valores financeiros!'
    + 'Deseja continuar assim mesmo?', 'Pergunta',
    MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then
    begin
      Result := False;
      Screen.Cursor := MyCursor;
      Exit;
    end;
  end;
  if AtualizaDefinicaoDeFatID_0301() then
    ReopenFatID_0301();
  //
  Screen.Cursor := MyCursor;
end;

procedure TFmPrincipal.ProcuraBACs();
var
  i: Integer;
begin
  ProgressBar4.Position := 0;
  ProgressBar4.Max := GradeC.RowCount - 1;
  for i := 1 to GradeC.RowCount - 1 do
  begin
    ProgressBar4.Position := ProgressBar4.Position + 1;
    Application.ProcessMessages;
    LocalizaEmitente(i, True);
  end;
  ProgressBar4.Position := 0;
end;

procedure TFmPrincipal.Qr301ChqCalcFields(DataSet: TDataSet);
begin
  case Qr301ChqFatSit.Value of
    -10: Qr301ChqNO_FatSit.Value := 'Vencido';
     10: Qr301ChqNO_FatSit.Value := 'Aberto';
     20: Qr301ChqNO_FatSit.Value := 'Compensado';
     30: Qr301ChqNO_FatSit.Value := 'Pago';
     else Qr301ChqNO_FatSit.Value := '??';
  end;
  case Qr301ChqFatSitSub.Value of
    -10: Qr301ChqNO_FatSitSub.Value := 'Devolvidos';
     10: Qr301ChqNO_FatSitSub.Value := 'Abertos';
     20: Qr301ChqNO_FatSitSub.Value := 'Depositado';
     30: Qr301ChqNO_FatSitSub.Value := 'Repassado';
     40: Qr301ChqNO_FatSitSub.Value := 'Dispon�vel';
     50: Qr301ChqNO_FatSitSub.Value := 'Quitado';
     else Qr301ChqNO_FatSitSub.Value := '??';
  end;
end;

procedure TFmPrincipal.Qr301DupCalcFields(DataSet: TDataSet);
begin
  case Qr301DupFatSit.Value of
    -10: Qr301DupNO_FatSit.Value := 'Vencido';
     10: Qr301DupNO_FatSit.Value := 'Aberto';
     20: Qr301DupNO_FatSit.Value := 'Compensado';
     30: Qr301DupNO_FatSit.Value := 'Pago';
     else Qr301DupNO_FatSit.Value := '??';
  end;
  case Qr301DupFatSitSub.Value of
    -10: Qr301DupNO_FatSitSub.Value := 'Devolvidos';
     10: Qr301DupNO_FatSitSub.Value := 'Abertos';
     20: Qr301DupNO_FatSitSub.Value := 'Depositado';
     30: Qr301DupNO_FatSitSub.Value := 'Repassado';
     40: Qr301DupNO_FatSitSub.Value := 'Dispon�vel';
     50: Qr301DupNO_FatSitSub.Value := 'Quitado';
     else Qr301DupNO_FatSitSub.Value := '??';
  end;
end;

procedure TFmPrincipal.QrDuplAfterScroll(DataSet: TDataSet);
begin
  QrDupi.Close;
  QrDupi.Params[0].AsInteger := QrDuplConta.Value;
  UMyMod.AbreQuery(QrDupi, Dmod.MyDB);
end;

procedure TFmPrincipal.AdicionaBorderoANF(NF, Bordero: Integer);
begin
{
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE lot es SET AlterWeb=1,   NF=:P0 WHERE Codigo=:P1');
  Dmod.QrUpd.Params[0].AsInteger := NF;
  Dmod.QrUpd.Params[1].AsInteger := Bordero;
  Dmod.QrUpd.ExecSQL;
}
  if DmLot.LOT_Ins_Upd(Dmod.QrUpd, stUpd, False, [
  'NF'], ['Codigo'], [NF], [Bordero], True) then
    DmLot2.AlterarNaWeb_Lote(Bordero);
end;

function TFmPrincipal.AdicionaMinutosSQL(HI, HF: TTime): Boolean;
var
  Hour, Min, Sec, MSec: Word;
  i, Ini, Fim: Integer;
begin
  Result := True;
  if (HI=0) and (HF=0) then Exit;
  DecodeTime(HI, Hour, Min, Sec, MSec);
  Ini := (Hour * 60) + Min;
  DecodeTime(HF, Hour, Min, Sec, MSec);
  Fim := (Hour * 60) + Min - 1;
  if Fim < Ini then Fim := Fim + 1440;
  Dmod.QrUpdL.SQL.Clear;
  Dmod.QrUpdL.SQL.Add('INSERT INTO Ocupacao SET Qtd=1, Min=:P0');
  for i := Ini to Fim do
  begin
    Dmod.QrUpdL.Params[0].AsInteger := I;
    Dmod.QrUpdL.ExecSQL;
  end;
end;

procedure TFmPrincipal.AGBFinNivClick(Sender: TObject);
begin
  FinanceiroJan.CadastroDeNiveisPlano();
end;

procedure TFmPrincipal.AGBFinCtaClick(Sender: TObject);
begin
  FinanceiroJan.CadastroDeContas(0);
end;

procedure TFmPrincipal.AGBSacadosClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmSacados, FmSacados, afmoNegarComAviso) then
  begin
    FmSacados.ShowModal;
    FmSacados.Destroy;
  end;
end;

procedure TFmPrincipal.AGBServicoManagerClick(Sender: TObject);
begin
  Application.CreateForm(TFmServicoManager, FmServicoManager);
  FmServicoManager.ShowModal;
  FmServicoManager.Destroy;
end;

procedure TFmPrincipal.AGBSimulaParcelaClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmSimulaParcela, FmSimulaParcela, afmoNegarComAviso) then
  begin
    FmSimulaParcela.ShowModal;
    FmSimulaParcela.Destroy;
  end;
end;

procedure TFmPrincipal.AGBSPC_PesqClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmSPC_Pesq, FmSPC_Pesq, afmoNegarComAviso) then
  begin
    FmSPC_Pesq.ShowModal;
    FmSPC_Pesq.Destroy;
  end;
end;

procedure TFmPrincipal.AGBSubGruClick(Sender: TObject);
begin
  FinanceiroJan.CadastroDeSubGrupos(0);
end;

procedure TFmPrincipal.AGBResultados2Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMResultados, AGBResultados2);
end;

procedure TFmPrincipal.AGBFinGruClick(Sender: TObject);
begin
  FinanceiroJan.CadastroDeGrupos(0);
end;

procedure TFmPrincipal.AGBFinCjtClick(Sender: TObject);
begin
  FinanceiroJan.CadastroDeConjutos(0);
end;

procedure TFmPrincipal.AGBFinPlanoClick(Sender: TObject);
begin
  FinanceiroJan.CadastroDePlano(0);
end;

procedure TFmPrincipal.AdvGlowButton10Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmInadimps, FmInadimps, afmoNegarComAviso) then
  begin
    FmInadimps.ShowModal;
    FmInadimps.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton11Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmOperaCol, FmOperaCol, afmoNegarComAviso) then
  begin
    FmOperaCol.ShowModal;
    FmOperaCol.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton13Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmSociosImp, FmSociosImp, afmoNegarComAviso) then
  begin
    FmSociosImp.ShowModal;
    FmSociosImp.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton165Click(Sender: TObject);
begin
  Application.CreateForm(TFmAbout, FmAbout);
  FmAbout.ShowModal;
  FmAbout.Destroy;
end;

procedure TFmPrincipal.AdvGlowButton167Click(Sender: TObject);
begin
  dmkWeb.AbrirAppAcessoRemoto;
end;

procedure TFmPrincipal.AdvGlowButton16Click(Sender: TObject);
begin
  MostraOpcoesCreditoX();
end;

procedure TFmPrincipal.AdvGlowButton17Click(Sender: TObject);
begin
  MostraOpcoes;
end;

procedure TFmPrincipal.AdvGlowButton19Click(Sender: TObject);
begin
  FinanceiroJan.CadastroIndiPag;
end;

procedure TFmPrincipal.AGBCorrigeEmpresaClick(Sender: TObject);
begin
  DMod.QrUpd.SQL.Clear;
  DMod.QrUpd.SQL.Add('UPDATE controle SET Dono=-11');
  DMod.QrUpd.ExecSQL;
  //
  DMod.QrUpd.SQL.Clear;
  DMod.QrUpd.SQL.Add('UPDATE entidades SET CliInt=1, Filial=1 WHERE Codigo=-11');
  DMod.QrUpd.ExecSQL;
  //
  DMod.QrUpd.SQL.Clear;
  DMod.QrUpd.SQL.Add('UPDATE enticliint SET CodCliInt=1, CodFilial=1 WHERE CodEnti=-11');
  DMod.QrUpd.ExecSQL;
  //
  DMod.QrUpd.SQL.Clear;
  DMod.QrUpd.SQL.Add('UPDATE entidades SET CliInt=-1, Filial=-1 WHERE Codigo=-1');
  DMod.QrUpd.ExecSQL;
end;

procedure TFmPrincipal.AGBEmiSacExportClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEmitSacExport, FmEmitSacExport, afmoNegarComAviso) then
  begin
    FmEmitSacExport.ShowModal;
    FmEmitSacExport.Destroy;
  end;
end;

procedure TFmPrincipal.AGBEmiSacImportClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEmitSacImport, FmEmitSacImport, afmoNegarComAviso) then
  begin
    FmEmitSacImport.ShowModal;
    FmEmitSacImport.Destroy;
  end;
end;

procedure TFmPrincipal.AGBEmitentesClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEmitentes, FmEmitentes, afmoNegarComAviso) then
  begin
    FmEmitentes.ShowModal;
    FmEmitentes.Destroy;
  end;
end;

procedure TFmPrincipal.AGBEncerraBorderoClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmLot2Enc, FmLot2Enc, afmoNegarComAviso) then
  begin
    FmLot2Enc.Show;
    FmLot2Enc.EncerraAbertos();
    FmLot2Enc.Destroy;
  end;
end;

procedure TFmPrincipal.AGBEntiCadClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
end;

procedure TFmPrincipal.AGBErrosAlertasClick(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormTsErrosAlertasCab();
end;

procedure TFmPrincipal.AGBBAncoDadosClick(Sender: TObject);
begin
  Application.CreateForm(TFmBancoDados, FmBancoDados);
  FmBancoDados.ShowModal;
  FmBancoDados.Destroy;
  //
  FMyDBs.Reload;
end;

procedure TFmPrincipal.AGBBancosClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmBancos, FmBancos, afmoNegarComAviso) then
  begin
    FmBancos.ShowModal;
    FmBancos.Destroy;
  end;
end;

procedure TFmPrincipal.AGBArquivoMortoClick(Sender: TObject);
begin
  Geral.MensagemBox('Em desenvolvimento!', 'DERMATEK', MB_OK+MB_ICONINFORMATION);
  //
{ TODO : Fazer arquivo morto!!! CUIDADO!!! Quando arquivar cuidar lot es negativos >=< -Cliente!!! }
{###
  if DBCheck.CriaFm(TFmLotMorto, FmLotMorto, afmoNegarComAviso) then
  begin
    FmLotMorto.ShowModal;
    FmLotMorto.Destroy;
  end;
}
end;

procedure TFmPrincipal.AGBAtzBorderoClick(Sender: TObject);
begin
  PB1.Position := 0;
  PB1.Visible := True;
{
  QrLot.Close;
  QrLot. Open;
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrLot, Dmod.MyDB, [
  'SELECT Codigo ',
  'FROM ' + CO_TabLotA,
  'ORDER BY Codigo ',
  '']);

  PB1.Max := QrLot.RecordCount;
  PB1.Update;
  Application.ProcessMessages;
{
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE lot es SET AlterWeb=1, Itens=:P0 WHERE Codigo=:P1');
}
  while not QrLot.Eof do
  begin
    PB1.Position := PB1.Position + 1;
{
    QrLotIts.Close;
    QrLotIts.Params[0].AsInteger := QrLotCodigo.Value;
    QrLotIts. Open;
}

    UnDmkDAC_PF.AbreMySQLQuery0(QrLotIts, Dmod.MyDB, [
    'SELECT COUNT(*) ITENS ',
    'FROM ' + CO_TabLctA,
    'WHERE FatID=' + Geral.FF0(VAR_FATID_0301),
    'AND FatNum=' + Geral.FF0(QrLotCodigo.Value),
    '']);
    //
{
    Dmod.QrUpd.Params[0].AsFloat   := QrLotItsITENS.Value;
    Dmod.QrUpd.Params[1].AsInteger := QrLotCodigo.Value;
    Dmod.QrUpd.ExecSQL;
}
    DmLot.LOT_Ins_Upd(Dmod.QrUpd, stUpd, False, [
    'Itens'], ['Codigo'], [
    QrLotItsITENS.Value], [QrLotCodigo.Value], True);
    //
    QrLot.Next;
  end;
  //PP1.Visible := False;
end;

procedure TFmPrincipal.AGBAtzCliItsBorderoClick(Sender: TObject);
begin
  AtualizaClientesLotIts();
end;

procedure TFmPrincipal.AGBBaixaLoteWebClick(Sender: TObject);
begin
  DmLot2.BaixaLotWeb();
end;

procedure TFmPrincipal.AdvGlowButton23Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPreEmail, FmPreEmail, afmoNegarComAviso) then
  begin
    FmPreEmail.ShowModal;
    FmPreEmail.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton27Click(Sender: TObject);
begin
  DepositoDeCheques(0);
end;

procedure TFmPrincipal.AdvGlowButton28Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmOperaImp, FmOperaImp, afmoNegarComAviso) then
  begin
    FmOperaImp.ShowModal;
    FmOperaImp.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton2Click(Sender: TObject);
begin
  {$IFDEF UsaWSuport}
  DmkWeb.MostraWSuporte(False, 2, BalloonHint1);
  {$ENDIF}
end;

procedure TFmPrincipal.AdvGlowButton3Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCNAE21Cad, FmCNAE21Cad, afmoNegarComAviso) then
  begin
    FmCNAE21Cad.ShowModal;
    FmCNAE21Cad.Destroy;
  end;
end;

procedure TFmPrincipal.AGBTedRemClick(Sender: TObject);
begin
  TedC_Ax.MostraTedCRemCab(0, 0);
end;

procedure TFmPrincipal.AGBTedRetClick(Sender: TObject);
begin
  TedC_Ax.MostraTedCRetArq;
end;

procedure TFmPrincipal.AGBTedRetGerClick(Sender: TObject);
begin
  TedC_Ax.MostraTedCRetCab;
end;

procedure TFmPrincipal.AGBResultados1Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMResultados, AGBResultados1);
end;

procedure TFmPrincipal.AGBBloqParcClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGerDup1Main, FmGerDup1Main, afmoNegarComAviso) then
  begin
    FmGerDup1Main.ShowModal;
    FmGerDup1Main.Destroy;
  end;
end;

procedure TFmPrincipal.AGBGerPropClick(Sender: TObject);
begin
  MostraEntiJur1(0);
end;

procedure TFmPrincipal.AGBFiliaisClick(Sender: TObject);
begin
  MostraFiliais();
end;

procedure TFmPrincipal.AGBFinancasClick(Sender: TObject);
begin
  FinanceiroJan.MostraFinancas(Pagina, AdvToolBarPager1);
end;

procedure TFmPrincipal.AGBEvolucapi2Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMEvolucap, AGBEvolucapi2);
end;

procedure TFmPrincipal.AGBDepositClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmDeposito, FmDeposito, afmoNegarComAviso) then
  begin
    FmDeposito.ShowModal;
    FmDeposito.Destroy;
  end;
end;

procedure TFmPrincipal.AGBRiscoAll2Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmRiscoAll_1, FmRiscoAll_1, afmoNegarComAviso) then
  begin
    FmRiscoAll_1.ShowModal;
    FmRiscoAll_1.Destroy;
  end;
end;

procedure TFmPrincipal.AGBChequesAvulsos2Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmChequesExtras, FmChequesExtras, afmoNegarComAviso) then
  begin
    FmChequesExtras.ShowModal;
    FmChequesExtras.Destroy;
  end;
end;

procedure TFmPrincipal.AGBInadimplencia1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmInadimps, FmInadimps, afmoNegarComAviso) then
  begin
    FmInadimps.ShowModal;
    FmInadimps.Destroy;
  end;
end;

procedure TFmPrincipal.AGBGrupUHs1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGerCliMain, FmGerCliMain, afmoNegarComAviso) then
  begin
    FmGerCliMain.ShowModal;
    FmGerCliMain.Destroy;
  end;
end;

procedure TFmPrincipal.AGBGruSacEmiClick(Sender: TObject);
begin
  CadastroGruSacEmi(-1000);
end;

procedure TFmPrincipal.AGBNewFinMigraClick(Sender: TObject);
begin
  DModG.CorrigeEmpresaDeMenosUmParaMenosOnze();
  DmLot.CorrigeDadosParaMigrarAoCredito2();
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  MyObjects.MostraPopUpDeBotao(PMMigracao, AGBNewFinMigra);
end;

procedure TFmPrincipal.AGBNewFinSdoIniClick(Sender: TObject);
begin
  Geral.MensagemBox(
  'Os saldos iniciais devem ser corrigidos na vers�o de produto 2.0.0.0',
  'Aviso', MB_OK+MB_ICONWARNING);
end;

procedure TFmPrincipal.AGBNFsCadClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmNFs, FmNFs, afmoNegarComAviso) then
  begin
    FmNFs.ShowModal;
    FmNFs.Destroy;
  end;
end;

procedure TFmPrincipal.AGBNFSeFatCabClick(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormNFSeFatCab(False, nil, nil);
end;

procedure TFmPrincipal.AGBNFSeLRpsCClick(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormNFSeLRpsC_0201(0);
end;

procedure TFmPrincipal.AGBNFSeMenCabClick(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormNFSeMenCab(False, nil, nil);
end;

procedure TFmPrincipal.AGBNFSeSrvCadClick(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormNFSeSrvCad();
end;

procedure TFmPrincipal.AGBNFSe_EditClick(Sender: TObject);
const
  DPS           = 0;
  NFSeFatCab    = 0;
  Tomador       = 0;
  Intermediario = 0;
  MeuServico    = 0;
  ItemListSrv   = 0;
  Valor         = 0;

  Discriminacao = '';
  GeraNFSe      = True;
  SQLType       = stIns;
  Servico       = fgnLoteRPS;
var
  Prestador, NumNF: Integer;
  SerieNF: String;
begin
  Prestador := DmodG.QrFiliLogFilial.Value;
  UnNFSe_PF_0201.MostraFormNFSe(SQLType,
  Prestador, Tomador, Intermediario, MeuServico, ItemListSrv,
  Discriminacao, GeraNFSe, NFSeFatCab, DPS, Servico, nil, Valor, SerieNF,
  NumNF, nil);
end;

procedure TFmPrincipal.AGBNFSe_NFSePesqClick(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormNFSe_NFSePesq(False, nil, nil);
end;

procedure TFmPrincipal.AGBNFSe_RPSPesqClick(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormNFSe_RPSPesq(False, nil, nil);
end;

procedure TFmPrincipal.AGBRepaLocClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMaloteConf, FmMaloteConf, afmoNegarComAviso) then
  begin
    FmMaloteConf.ShowModal;
    FmMaloteConf.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton41Click(Sender: TObject);
begin
 if DBCheck.CriaFm(TFmPromissoria, FmPromissoria, afmoNegarComAviso) then
  begin
    FmPromissoria.ShowModal;
    FmPromissoria.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton42Click(Sender: TObject);
begin
 if DBCheck.CriaFm(TFmDuplicata1, FmDuplicata1, afmoNegarComAviso) then
  begin
    FmDuplicata1.ShowModal;
    FmDuplicata1.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton4Click(Sender: TObject);
begin
  DmLot2.BaixaLotWeb();
end;

procedure TFmPrincipal.AdvGlowButton5Click(Sender: TObject);
begin
  MostraMatriz();
end;

procedure TFmPrincipal.AGBClienteRapidoClick(Sender: TObject);
begin
  MostraEntiJur1(0);
end;

procedure TFmPrincipal.AGBFeriadosClick(Sender: TObject);
begin
  CadastrodeFeriados();
end;

procedure TFmPrincipal.CadastrodeFeriados();
begin
  if DBCheck.CriaFm(TFmFeriados, FmFeriados, afmoNegarComAviso) then
  begin
    FmFeriados.ShowModal;
    FmFeriados.Destroy;
  end;
end;

procedure TFmPrincipal.AGBFinListaClick(Sender: TObject);
begin
  FinanceiroJan.ImpressaoDoPlanoDeContas();
end;

procedure TFmPrincipal.AGBCompJurosClick(Sender: TObject);
begin
  Application.CreateForm(TFmCompJuros, FmCompJuros);
  FmCompJuros.ShowModal;
  FmCompJuros.Destroy;
end;

procedure TFmPrincipal.AGBCompostoClick(Sender: TObject);
begin
  Application.CreateForm(TFmComposto, FmComposto);
  FmComposto.ShowModal;
  FmComposto.Destroy;
end;

procedure TFmPrincipal.AGBConexaoClick(Sender: TObject);
var
  Servidor: Integer;
begin
  Servidor := Geral.ReadAppKey('Server', Application.Title, ktInteger, 0,
    HKEY_LOCAL_MACHINE);
  Application.CreateForm(TFmServidor, FmServidor);
  FmServidor.RGTipo.ItemIndex := Servidor;
  FmServidor.ShowModal;
  FmServidor.Destroy;
  if (Servidor <> VAR_SERVIDOR) or (Servidor = 3) then
  begin
    Geral.MensagemBox('O aplicativo dever� ser reinicializado!', 'Aviso',
    MB_OK+MB_ICONWARNING);
    Application.Terminate;
  end;
end;

procedure TFmPrincipal.AGBCtaFatClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCtaFat0, FmCtaFat0, afmoNegarComAviso) then
  begin
    FmCtaFat0.ShowModal;
    FmCtaFat0.Destroy;
  end;
end;

procedure TFmPrincipal.AGBContratoClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmContratoImp, FmContratoImp, afmoNegarComAviso) then
  begin
    FmContratoImp.ShowModal;
    FmContratoImp.Destroy;
  end;
end;

procedure TFmPrincipal.AGBInadimplencia2Click(Sender: TObject);
begin
  AGBInadimplencia1Click(Self);
end;

procedure TFmPrincipal.AGBJanelaImpressaoClick(Sender: TObject);
begin
  MyObjects.frxMostra(frxReport1, '');
end;

procedure TFmPrincipal.BtEuroExport1Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMRepasses, BtEuroExport1);
end;

procedure TFmPrincipal.MostraMatriz();
begin
  // 2011-09-20 de afmoSoAdmin para afmoSoBoss
  if DBCheck.CriaFm(TFmMatriz, FmMatriz, afmoSoBoss) then
  begin
    FmMatriz.ShowModal;
    FmMatriz.Destroy;
  end;
end;

procedure TFmPrincipal.AGBFinSaldosClick(Sender: TObject);
begin
  FinanceiroJan.SaldoDeContas;
end;

procedure TFmPrincipal.AGBLinkConcBcoClick(Sender: TObject);
begin
  FinanceiroJan.CadastroContasLnk;
end;

procedure TFmPrincipal.AGBListServClick(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormListServ();
end;

procedure TFmPrincipal.AGBLivre1_6_2Click(Sender: TObject);
begin
  (*
  if DBCheck.CriaFm(TFmLot0Sbr, FmLot0Sbr, afmoNegarComAviso) then
  begin
    FmLot0Sbr.ShowModal;
    FmLot0Sbr.Destroy;
  end;
  *)
  Keybd_event(VK_LWIN, 0, 0, 0);
  Keybd_event(Byte('M'), 0, 0, 0);
  Keybd_event(Byte('M'), 0, KEYEVENTF_KEYUP, 0);
  Keybd_event(VK_LWIN, 0, KEYEVENTF_KEYUP, 0);
end;

procedure TFmPrincipal.AGBCheque_0Click(Sender: TObject);
begin
  MyPrinters.MostraEmiteCheque_0;
end;

procedure TFmPrincipal.AGBEvolucapi1Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMEvolucap, AGBEvolucapi1);
end;

procedure TFmPrincipal.AGBLocPagClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmLocPag, FmLocPag, afmoNegarComAviso) then
  begin
    FmLocPag.ShowModal;
    FmLocPag.Destroy;
  end;
end;

procedure TFmPrincipal.AGBLSincroClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmLSincro, FmLSincro, afmoNegarComAviso) then
  begin
    FmLSincro.ShowModal;
    FmLSincro.Destroy;
  end;
end;

procedure TFmPrincipal.AGBEuroExportClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMRepasses, AGBEuroExport);
end;

procedure TFmPrincipal.AGBAgrExtrCtaClick(Sender: TObject);
begin
  FinanceiroJan.CadastroContasAgr;
end;

procedure TFmPrincipal.AGBAjusteWebClick(Sender: TObject);
var
  Controle: Integer;
begin
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT MAX(Controle) Controle FROM lloteits');
  UMyMod.AbreQuery(Dmod.QrAux, Dmod.MyDB);
  Controle := Dmod.QrAux.FieldByName('Controle').AsInteger;
  //
  DmLot2.QrCtrl.Close;
  DmLot2.QrCtrl.Params[0].AsInteger := Controle;
  UMyMod.AbreQuery(DmLot2.QrCtrl, Dmod.MyDBn);
  if DmLot2.QrCtrl.RecordCount > 0 then
  begin
    if Geral.MensagemBox('Foram localizados ' +
    IntToStr(DmLot2.QrCtrl.RecordCount) + ' itens de lote na web com o ' +
    'n�mero de controle inv�lido. Deseja atualiz�-los?', 'Pergunta',
    MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
    begin
      Screen.Cursor := crHourGlass;
      Dmod.QrWeb.Close;
      Dmod.QrWeb.SQL.Clear;
      Dmod.QrWeb.SQL.Add('UPDATE wloteits ');
      Dmod.QrWeb.SQL.Add('SET Controle=:P0');
      Dmod.QrWeb.SQL.Add('WHERE Controle=:P1');
      Dmod.QrWeb.SQL.Add('AND Codigo=:P2');
      //
      DmLot2.QrCtrl.First;
      while not DmLot2.QrCtrl.Eof do
      begin
        Controle := Controle + 1;
        Dmod.QrWeb.Params[00].AsInteger := Controle;
        Dmod.QrWeb.Params[01].AsInteger := DmLot2.QrCtrlControle.Value;
        Dmod.QrWeb.Params[02].AsInteger := DmLot2.QrCtrlCodigo.Value;
        Dmod.QrWeb.ExecSQL;
        //
        DmLot2.QrCtrl.Next;
      end;
      Screen.Cursor := crDefault;
      Geral.MensagemBox('Atualiza��o finalizada!', 'Mensagem',
      MB_OK+MB_ICONINFORMATION);
    end;
  end;
end;

procedure TFmPrincipal.AGBAlinChDevClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmAlineas, FmAlineas, afmoNegarComAviso) then
  begin
    FmAlineas.ShowModal;
    FmAlineas.Destroy;
  end;
end;

procedure TFmPrincipal.AGBAlinDuplicataClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmAlineasDup, FmAlineasDup, afmoNegarComAviso) then
  begin
    FmAlineasDup.ShowModal;
    FmAlineasDup.Destroy;
  end;
end;

procedure TFmPrincipal.AGBAlinPagChDevolvClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmAPCD, FmAPCD, afmoNegarComAviso) then
  begin
    FmAPCD.ShowModal;
    FmAPCD.Destroy;
  end;
end;

procedure TFmPrincipal.AGBRemConfigClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCNAB_Cfg, FmCNAB_Cfg, afmoNegarComAviso) then
  begin
    FmCNAB_Cfg.ShowModal;
    FmCNAB_Cfg.Destroy;
  end;
end;

procedure TFmPrincipal.AGBCNABRetEnvClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCNAB_Cfg, FmCNAB_Cfg, afmoNegarComAviso) then
  begin
    FmCNAB_Cfg.ShowModal;
    FmCNAB_Cfg.Destroy;
  end;
end;

procedure TFmPrincipal.AGBMySQLClick(Sender: TObject);
begin
  Application.CreateForm(TFmMyInis, FmMyInis);
  FmMyInis.ShowModal;
  FmMyInis.Destroy;
end;

procedure TFmPrincipal.AGBOcorrenciasClick(Sender: TObject);
begin
  CadastroOcorBank(0);
end;

procedure TFmPrincipal.AGBOperaLctClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmOperaLct, FmOperaLct, afmoNegarComAviso) then
  begin
    FmOperaLct.ShowModal;
    FmOperaLct.Destroy;
  end;
end;

procedure TFmPrincipal.AGBOcorDupGerClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGerDup1Main, FmGerDup1Main, afmoNegarComAviso) then
  begin
    FmGerDup1Main.ShowModal;
    FmGerDup1Main.Destroy;
  end;
end;

procedure TFmPrincipal.AGBFinEncerMesClick(Sender: TObject);
begin
  FinanceiroJan.MostraLctEncerraMes;
end;

procedure TFmPrincipal.AdvGlowButton82Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmLeDataArq, FmLeDataArq, afmoNegarComAviso) then
  begin
    FmLeDataArq.ShowModal;
    FmLeDataArq.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton83Click(Sender: TObject);
begin
{ N�o usa!!!
  if DBCheck.CriaFm(TFmCalcMod10e11, FmCalcMod10e11, afmoNegarComAviso) then
  begin
    FmCalcMod10e11.ShowModal;
    FmCalcMod10e11.Destroy;
  end;
}
end;

procedure TFmPrincipal.AdvGlowButton84Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmSomaDiasCred, FmSomaDiasCred, afmoNegarComAviso) then
  begin
    FmSomaDiasCred.ShowModal;
    FmSomaDiasCred.Destroy;
  end;
end;

procedure TFmPrincipal.AGBTaxasClick(Sender: TObject);
begin
  MostraTaxas(0);
end;

procedure TFmPrincipal.AGBTedCCfgClick(Sender: TObject);
begin
  TedC_Ax.MostraTedCCfgCab(0);
end;

procedure TFmPrincipal.AGBTedCOcoSysClick(Sender: TObject);
begin
  TedC_Ax.MostraTedCOcoSys(0);
end;

procedure TFmPrincipal.AGBThemeClick(Sender: TObject);
begin
  FmLinkRankSkin.Show;
end;

procedure TFmPrincipal.AGBVerificaVersoesClick(Sender: TObject);
begin
  VerificaNovasVersoes();
end;

procedure TFmPrincipal.AGBWClientsClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmwclients, Fmwclients, afmoNegarComAviso) then
  begin
    Fmwclients.ShowModal;
    Fmwclients.Destroy;
  end;
end;

procedure TFmPrincipal.AGBWSincroClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmWSincro, FmWSincro, afmoNegarComAviso) then
  begin
    FmWSincro.ShowModal;
    FmWSincro.Destroy;
  end;
end;

procedure TFmPrincipal.AGB_CNAB_RemessaClick(Sender: TObject);
begin
  MostraRemessaCNABNovo(0, 0, 0);
end;

procedure TFmPrincipal.RetornoCNAB();
begin
  if DBCheck.CriaFm(TFmCNAB_Ret2, FmCNAB_Ret2, afmoNegarComAviso) then
  begin
    FmCNAB_Ret2.ShowModal;
    FmCNAB_Ret2.Destroy;
  end;
end;

procedure TFmPrincipal.AGB_CNAB_RetornoClick(Sender: TObject);
begin
  RetornoCNAB();
end;

procedure TFmPrincipal.AGB_SPC_ConfigClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmSPC_Config, FmSPC_Config, afmoNegarComAviso) then
  begin
    FmSPC_Config.ShowModal;
    FmSPC_Config.Destroy;
  end;
end;

procedure TFmPrincipal.AGB_SPC_ModalidClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmSPC_Modali, FmSPC_Modali, afmoNegarComAviso) then
  begin
    FmSPC_Modali.ShowModal;
    FmSPC_Modali.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton90Click(Sender: TObject);
begin
  FinanceiroJan.ImpressaoDoPlanoDeContas();
end;

procedure TFmPrincipal.AdvGlowButton92Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMalaDireta, FmMalaDireta, afmoNegarComAviso) then
  begin
    FmMalaDireta.ShowModal;
    FmMalaDireta.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton93Click(Sender: TObject);
begin
  MyPrinters.MostraCfgImpressao;
end;

procedure TFmPrincipal.AdvGlowButton94Click(Sender: TObject);
begin
  FinanceiroJan.MostraRelChAbertos(0);
end;

procedure TFmPrincipal.AdvGlowButton9Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmHistCliEmi, FmHistCliEmi, afmoNegarComAviso) then
  begin
    FmHistCliEmi.ShowModal;
    FmHistCliEmi.Destroy;
  end;
end;

procedure TFmPrincipal.Importar(Tipo: TImportLote; CtrlIts: Integer; DarIni: String);
var
  ver, Doc: Integer;
  Importou: Boolean;
  i, j, k: Integer;
begin
  Importou := False;
  if DBCheck.CriaFm(TFmLot2Inn, FmLot2Inn, afmoNegarComAviso) then
  begin
    with FmLot2Inn do
    begin
      if Tipo <> il0_Nil then
      begin
        Screen.Cursor := crHourGlass;
        try
        FmPrincipal.FTipoImport := TImportLote(Tipo);
        FControlIts := CtrlIts;
        case Tipo of
          il4_BCO:
          begin
            DmLot2.FDataTransacao := TPCheque.Date;
            ArqSCX.LimpaGrade(GradeC, 1, 1);
            ArqSCX.LimpaGrade(GradeD, 1, 1);
            j := FmPrincipal.GradeC.RowCount;
            GradeC.RowCount := j;
            for i := 1 to j -1 do
            begin
              for k := 1 to 10 do
                GradeC.Cells[k,i] := FmPrincipal.GradeC.Cells[k,i];
            end;
            if GradeC.RowCount > 2 then
              Importou := True
            else begin
              Geral.MensagemBox('"Importa BCO (txt)" deve ter mais de um documento!', 'Aviso', MB_OK+MB_ICONWARNING);

            end;
          end;
          il5_WEB:
          begin
            DmLot2.QrLLI.Close;
            DmLot2.QrLLI.Params[0].AsInteger := FLoteWeb;
            FLoteWebDone := FLoteWeb;
            FLoteWeb := 0;
            UMyMod.AbreQuery(DmLot2.QrLLI, Dmod.MyDB);
            DmLot2.QrLLI.First;

            TPCheque.Date := DmLot2.QrLLCData.Value;
            ArqSCX.LimpaGrade(GradeC, 1, 1);
            ArqSCX.LimpaGrade(GradeD, 1, 1);

            while not DmLot2.QrLLI.Eof do
            begin
              Doc := DmLot2.QrLLI.RecNo;
              if DmLot2.QrLLCTipo.Value = 0 then
              begin
                with GradeC do
                begin
                  if Doc > RowCount-1 then RowCount := Doc + 1;
                  Cells[00,Doc] := IntToStr(Doc);
                  Cells[01,Doc] := FormatFloat('000', DmLot2.QrLLIPraca.Value);
                  Cells[02,Doc] := FormatFloat('000', DmLot2.QrLLIBanco.Value);
                  Cells[03,Doc] := FormatFloat('0000', DmLot2.QrLLIAgencia.Value);
                  Cells[04,Doc] := DmLot2.QrLLIConta.Value;
                  Cells[05,Doc] := FormatFloat('000000', DmLot2.QrLLICheque.Value);
                  Cells[06,Doc] := Geral.FFT(DmLot2.QrLLIBruto.Value, 2, siPositivo);
                  Cells[07,Doc] := Geral.FDT(DmLot2.QrLLIVencto.Value, 2);
                  Cells[09,Doc] := Geral.FormataCNPJ_TT(DmLot2.QrLLICPF.Value);
                  Cells[10,Doc] := DmLot2.QrLLIEmitente.Value;
                end;
              end else begin
                // Parei Aqui
                with GradeD do
                begin
                  if Doc > RowCount-1 then RowCount := Doc + 1;
                  Cells[00,Doc] := IntToStr(Doc);
                  Cells[01,Doc] := Geral.FDT(DmLot2.QrLLIEmissao.Value, 2);
                  Cells[02,Doc] := DmLot2.QrLLIDuplicata.Value;
                  Cells[03,Doc] := Geral.FFT(DmLot2.QrLLIBruto.Value, 2, siPositivo);
                  Cells[04,Doc] := Geral.FFT(DmLot2.QrLLIDesco.Value, 2, siPositivo);
                  Cells[05,Doc] := Geral.FFT(DmLot2.QrLLIValor.Value, 2, siPositivo);
                  Cells[06,Doc] := Geral.FDT(DmLot2.QrLLIVencto.Value, 2);
                  Cells[07,Doc] := Geral.FormataCNPJ_TT(DmLot2.QrLLICPF.Value);
                  Cells[08,Doc] := DmLot2.QrLLIEmitente.Value;
                  Cells[09,Doc] := DmLot2.QrLLIRua.Value;
                  Cells[10,Doc] := IntToStr(DmLot2.QrLLINumero.Value);
                  Cells[11,Doc] := DmLot2.QrLLICompl.Value;
                  Cells[12,Doc] := DmLot2.QrLLIBairro.Value;
                  Cells[13,Doc] := DmLot2.QrLLICidade.Value;
                  Cells[14,Doc] :=Geral.FormataCEP_NT(DmLot2.QrLLICEP.Value);
                  Cells[15,Doc] := DmLot2.QrLLIUF.Value;
                  Cells[16,Doc] := MLAGeral.FormataTelefone_TT_Curto(DmLot2.QrLLITel1.Value);
                  Cells[17,Doc] := DmLot2.QrLLIIE.Value;
                end;
              end;
              DmLot2.QrLLI.Next;
            end;
            EdCodCli.Text := IntToStr(DmLot2.QrLLCCliente.Value);
            EdNomCli.Text := DmLot2.QrLLCNOMECLI.Value;
            TPCheque.Date := DmLot2.QrLLCData.Value;
            FmPrincipal.FVerArqSCX := 0;//Geral.IMV(DmLot2.QrLLCVersao.Value);
            Importou := True;
          end;
          else begin
            Importou := ArqSCX.AbreArquivoSCX(GradeC, GradeD, EdCodCli, EdNomCli,
            TPCheque, PB1, DarIni);
          end;
        end;
        if Importou then
        begin
          Application.ProcessMessages;
          // � mais r�pido para excluir dados ?
          UCriar.RecriaTabelaLocal('ImportLote', 1);
          Application.ProcessMessages;
          ArqSCX.CalculaGradeC(EdQtdeCh, EdValorCH, EdQtdeTo, EdValorTO,
            EdValorDU, GradeC, GradeD, TPCheque);
          Application.ProcessMessages;
          ArqSCX.CalculaGradeD(EdQtdeDU, EdValorDU, EdQtdeTo, EdValorTO,
            EdValorCH, GradeC, GradeD, TPCheque);
          Application.ProcessMessages;

          // Substituido pelo ShowMessage
          //MostraEdicao(3, CO_INCLUSAO, 0);
          ArqSCX.DefineContagens(GradeC, GradeD);
          if FmPrincipal.FCheque > 0 then
          begin
            BtConfirma1.Enabled    := True;
            BtPagtosExclui.Enabled := True;
            BtSPC_CH.Enabled       := True;
          end else begin
            BtConfirma1.Enabled    := False;
            BtPagtosExclui.Enabled := False;
            BtSPC_CH.Enabled       := False;
          end;
          if FmPrincipal.FDuplicata > 0 then
          begin
            BtConfirma3.Enabled     := True;
            BtPagtosExclui2.Enabled := True;
            BtSPC_DU.Enabled        := True;
          end else begin
            BtConfirma3.Enabled     := False;
            BtPagtosExclui2.Enabled := False;
            BtSPC_DU.Enabled        := False;
          end;
          //

          Application.ProcessMessages;
          FmLot2Cab.DefineRiscos(Geral.IMV(EdCodCli.Text));
          Application.ProcessMessages;
          //TimerSCX.Enabled := True;
          InsereImportLote(Pagina, GradeC, GradeD, TPCheque);
        end;
        LaVerSuit1.Caption := Geral.VersaoTxt2006(FmPrincipal.FVerArqSCX);
        LaVerSuit2.Caption := Geral.VersaoTxt2006(FmPrincipal.FVerArqSCX);
        Ver := Geral.VersaoInt2006(FVersaoSuitCash);
        if FmPrincipal.FVerArqSCX < Ver then
        begin
          LaVerSuit1.Font.Color := clRed;
          LaVerSuit2.Font.Color := clRed;
        end else begin
          LaVerSuit1.Font.Color := clNavy;
          LaVerSuit2.Font.Color := clNavy;
        end;
        finally
          Screen.Cursor := crDefault;
        end;
      end;
      //
      if Importou then
        ShowModal;
      Destroy;
    end;
  end;
end;

procedure TFmPrincipal.ImprimeChequeSimples(Valor, Creditado, Cidade: String;
  Data: TDateTime; Nominal: Boolean);
var
  Dia, Mes, Ano: Word;
begin
  DecodeDate(Data, Ano, Mes, Dia);
  FImpCHSimVal := Valor;
  FImpCHSimExt := dmkPF.ExtensoMoney(Valor);
  if Nominal then FImpCHSimEnt := Creditado else FImpCHSimEnt := '';
  FImpCHSimCid := Cidade;
  FImpCHSimDia := IntToStr(Dia);
  FImpCHSimMes := dmkPF.VerificaMes(Mes, True);
  FImpCHSimAno := IntToStr(Ano);
  //
  MyObjects.frxMostra(frxChequeSimples, 'Cheque simples');
end;

procedure TFmPrincipal.AGBFinCartClick(Sender: TObject);
begin
  FinanceiroJan.CadastroDeCarteiras(0);
end;

procedure TFmPrincipal.AdvToolBarButton10Click(Sender: TObject);
begin
  DModG.MostraBackup3();
end;

procedure TFmPrincipal.AdvToolBarButton12Click(Sender: TObject);
begin
  MyObjects.MaximizaAdvToolBarPager(AdvToolBarPager1, deftfInverse);
end;

procedure TFmPrincipal.AdvToolBarButton13Click(Sender: TObject);
begin
  {$IFDEF UsaWSuport}
  DmkWeb.MostraWSuporte(False, 2, BalloonHint1);
  {$ENDIF}
end;

procedure TFmPrincipal.AdvToolBarButton7Click(Sender: TObject);
begin
  MostraLogoff;
end;

procedure TFmPrincipal.AdvToolBarButton8Click(Sender: TObject);
begin
  VerificaNovasVersoes();
end;

procedure TFmPrincipal.AdvToolBarButton9Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmFavoritosG, FmFavoritosG, afmoLiberado) then
  begin
    FmFavoritosG.ShowModal;
    FmFavoritosG.Destroy;
    DModG.CriaFavoritos(AdvToolBarPager1, LaAviso3, LaAviso4, AGBAcesRapid, FmPrincipal);
  end;
end;

procedure TFmPrincipal.AGMBBackupClick(Sender: TObject);
begin
  DModG.MostraBackup3();
end;

procedure TFmPrincipal.SBCadEntidadesClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadSelecionar, fmcadSelecionar);
end;

procedure TFmPrincipal.Escolher2Click(Sender: TObject);
begin
(*
  MeuVCLSkin.VCLSkinEscolhe(FmMyGlyfs.Dialog1, FmPrincipal.sd1);
*)
end;

procedure TFmPrincipal.extos2Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCartas, FmCartas, afmoNegarComAviso) then
  begin
    FmCartas.FTipo := 4;
    FmCartas.ShowModal;
    FmCartas.Destroy;
    //
    Dmod.QrControle.Close;
    UMyMod.AbreQuery(Dmod.QrControle, Dmod.MyDB);
  end;
end;

procedure TFmPrincipal.TextosCartasGetValue(const ParName: String;
  var ParValue: Variant);
begin
       if ParName = 'LIMITE  ' then ParValue := 0
  else if ParName = 'NUM_CONT' then ParValue := 0
  else if ParName = 'ADITIVO ' then ParValue := 0
  else if ParName = 'DATA_CON' then ParValue := 0
  else if ParName = 'PLU1_TIT' then ParValue := '(S)'
  else if ParName = 'PLU2_TIT' then ParValue := 'i(ram)'
  else if ParName = 'PLU3_TIT' then ParValue := '�(�o)'
  else if ParName = 'MEMPRESA' then ParValue := DModG.QrDonoNomeDono.Value
  else if ParName = 'NOME_CLI' then ParValue := ''
  else if ParName = 'DUPLISAC' then ParValue := ''
  // cancelamento de protesto
  else if ParName = 'NOME_SAC' then ParValue := ''
  else if ParName = 'TIPD_SAC' then ParValue := ''
  else if ParName = 'DOCU_SAC' then ParValue := ''
  else if ParName = 'ENDE_SAC' then ParValue := ''
end;

procedure TFmPrincipal.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  FALiberar := True;
  FmCredito_DMK.Show;
  Enabled := False;
  FmCredito_DMK.Refresh;
  FmCredito_DMK.EdSenha.Text := FmCredito_DMK.EdSenha.Text+'*';
  FmCredito_DMK.EdSenha.Refresh;
  FmCredito_DMK.Refresh;
  try
    Application.CreateForm(TDmod, Dmod);
    FDmodCriado := True;
    ReCaptionComponentesDeForm(FmPrincipal);
    AdvToolBarPager1.Visible := True;
    // Tornar vis�vel
    Timer2.Enabled := True;
  except
    Geral.MensagemBox('Imposs�vel criar Modulo de dados', 'Erro', MB_OK+MB_ICONERROR);
    Application.Terminate;
    Exit;
  end;
  FmCredito_DMK.EdSenha.Text := FmCredito_DMK.EdSenha.Text+'*';
  FmCredito_DMK.EdSenha.Refresh;
  FmCredito_DMK.ReloadSkin;
  FmCredito_DMK.EdLogin.Text := '';
  FmCredito_DMK.EdLogin.PasswordChar := 'l';
  FmCredito_DMK.EdSenha.Text := '';
  FmCredito_DMK.EdSenha.Refresh;
  FmCredito_DMK.EdLogin.ReadOnly := False;
  FmCredito_DMK.EdSenha.ReadOnly := False;
  FmCredito_DMK.EdLogin.SetFocus;
  //FmCredito_DMK.ReloadSkin;
  FmCredito_DMK.Refresh;
  try
    (*
    // Parei Aqui quando atualizar tudo
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE status');
    Dmod.QrUpd.SQL.Add('SET Nome=Descri');
    Dmod.QrUpd.SQL.Add('WHERE Nome="?"');
    Dmod.QrUpd.ExecSQL;
    *)
  except

  end;
end;

procedure TFmPrincipal.Timer2Timer(Sender: TObject);
begin
  if AlphaBlendValue < 255 then
    AlphaBlendValue := AlphaBlendValue + 1
  else begin
    Timer2.Enabled := False;
    AlphaBlend := False;
  end;
end;

procedure TFmPrincipal.FormShow(Sender: TObject);
begin
  //N�o usa mais
  //MeuVCLSkin.VCLSkinDefineUso(FmPrincipal.sd1, FmPrincipal.skinstore1);
end;

procedure TFmPrincipal.Futuros1Click(Sender: TObject);
begin
{
  FEntInt := 0;
  DefineCondominio(0, siPositivo);
}
  if FEntInt <> 0 then
    DModFin.ImprimeSaldos(FEntInt);
end;

procedure TFmPrincipal.BalanceteConfiguravel(TabLctA: String);
begin
  //
  if not PreparaPesquisaFinanceira(TabLctA) then
    Exit;
  //
  //
  if DBCheck.CriaFm(TFmCashBal, FmCashBal, afmoNegarComAviso) then
  begin
    FmCashBal.ShowModal;
    FmCashBal.Destroy;
  end;
end;

procedure TFmPrincipal.MostraCartaG(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmCartaG, FmCartaG, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmCartaG.LocCod(Codigo, Codigo);
    FmCartaG.FTipo := 4;
    FmCartaG.ShowModal;
    FmCartaG.Destroy;
  end;
end;

procedure TFmPrincipal.MostraDiarioAdd(Assunto, Contato, Depto: Integer;
  Texto: String);
var
  CliInt: Integer;
begin
  CliInt := DModG.QrEmpresasFilial.Value;
  //
  if DBCheck.CriaFm(TFmDiarioAdd, FmDiarioAdd, afmoNegarComAviso) then
  begin
    FmDiarioAdd.PreencheDados(False, 0, 0, Assunto, CliInt, 0, Contato, 0, Texto);
    //
    FmDiarioAdd.FDepto          := Depto;
    FmDiarioAdd.FChamou         := -1;
    FmDiarioAdd.ImgTipo.SQLType := stIns;
    FmDiarioAdd.ShowModal;
    FmDiarioAdd.Destroy;
  end;
end;

procedure TFmPrincipal.MostraDiarioGer(Assunto, Contato: Integer; Depto,
  Texto: String);
var
  CliInt: Integer;
begin
  CliInt := DModG.QrEmpresasFilial.Value;
  //
  if DBCheck.CriaFm(TFmDiarioGer, FmDiarioGer, afmoNegarComAviso) then
  begin
    if Assunto <> 0 then
    begin
      FmDiarioGer.EdDiarioAss.ValueVariant := Assunto;
      FmDiarioGer.CBDiarioAss.KeyValue     := Assunto;
    end;
    if CliInt <> 0 then
    begin
      FmDiarioGer.EdCliInt.ValueVariant    := CliInt;
      FmDiarioGer.CBCliInt.KeyValue        := CliInt;
    end;
    if Contato <> 0 then
    begin
      FmDiarioGer.EdEntidade.ValueVariant  := Contato;
      FmDiarioGer.CBEntidade.KeyValue      := Contato;
    end;
    if Length(Depto) > 0 then
      FmDiarioGer.EdLctCtrl.Text := Depto;
    if Length(Texto) > 0 then
      FmDiarioGer.EdNome.Text := Texto;
    //
    FmDiarioGer.ShowModal;
    FmDiarioGer.Destroy;
  end;
end;

procedure TFmPrincipal.MostraDuplicatas2(Controle: Extended);
begin
  Application.CreateForm(TFmGerDup1Main, FmGerDup1Main);
  if Controle > 0 then
    FmGerDup1Main.EdControle.Texto := FormatFloat('0', Controle);
  FmGerDup1Main.ShowModal;
  FmGerDup1Main.Destroy;
end;

procedure TFmPrincipal.MostraEntiJur1(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmEntiJur1, FmEntiJur1, afmoLiberado) then
  begin
    if Codigo <> 0 then
      FmEntiJur1.LocCod(Codigo, Codigo);
    FmEntiJur1.ShowModal;
    FmEntiJur1.Destroy;
  end;
end;

procedure TFmPrincipal.MostraFiliais;
begin
  if DBCheck.CriaFm(TFmParamsEmp, FmParamsEmp, afmoSoBoss) then
  begin
    FmParamsEmp.ShowModal;
    FmParamsEmp.Destroy;
  end;
end;

procedure TFmPrincipal.MostraLogoff;
begin
  FmPrincipal.Enabled := False;
  //
  FmCredito_DMK.Show;
  FmCredito_DMK.EdLogin.Text   := '';
  FmCredito_DMK.EdSenha.Text   := '';
  FmCredito_DMK.EdLogin.SetFocus;
end;

procedure TFmPrincipal.MostraOpcoes;
begin
  if DBCheck.CriaFm(TFmOpcoes, FmOpcoes, afmoNegarComAviso) then
  begin
    FmOpcoes.ShowModal;
    FmOpcoes.Destroy;
  end;
end;

procedure TFmPrincipal.MostraOpcoesCreditoX;
begin
  if DBCheck.CriaFm(TFmOpcoesCreditoX, FmOpcoesCreditoX, afmoNegarComAviso) then
  begin
    FmOpcoesCreditoX.ShowModal;
    FmOpcoesCreditoX.Destroy;
  end;
end;

procedure TFmPrincipal.MostraRemessaCNABNovo(Codigo, FatParcela, Bordero: Integer);
begin
  DModG.EmpresaAtual_SetaCodigos(Dmod.QrControleDono.Value, tecEntidade, True);
  //
  if DBCheck.CriaFm(TFmCNAB_Lot, FmCNAB_Lot, afmoNegarComAviso) then
  begin
    if (Codigo > 0) and (FatParcela > 0) then
    begin
      FmCNAB_Lot.LocCod(Codigo, Codigo);
      FmCNAB_Lot.ReopenCNAB_LotIts(FatParcela);
    end;
    FmCNAB_Lot.FBordero := Bordero;
    FmCNAB_Lot.ShowModal;
    FmCNAB_Lot.Destroy;
  end;
end;

procedure TFmPrincipal.MostraTaxas(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmTaxas, FmTaxas, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmTaxas.LocCod(Codigo, Codigo);
    FmTaxas.ShowModal;
    FmTaxas.Destroy;
  end;
end;

procedure TFmPrincipal.Movimento1Click(Sender: TObject);
begin
  FinanceiroJan.MostraMovimento(0);
end;

procedure TFmPrincipal.MovimentoPlanodecontas1Click(Sender: TObject);
begin
  FinanceiroJan.MostraMovimentoPlanodeContas;
end;

procedure TFmPrincipal.MultiplasEmpresas1Click(Sender: TObject);
begin
  FinanceiroJan.ExtratosFinanceirosMultiplasEmpresas;
end;

procedure TFmPrincipal.NovoEmconstruo1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEvolucap2, FmEvolucap2, afmoNegarComAviso) then
  begin
    FmEvolucap2.ShowModal;
    FmEvolucap2.Destroy;
  end;
end;

procedure TFmPrincipal.NovoFinanceiro1Click(Sender: TObject);
begin
  MyObjects.Informa2EUpdPB(PB1, LaAviso5, LaAviso6, True,
  'Alterando lan�amentos de pagamentos j� existentes');
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE lan'+'ctos SET FatID=' + FormatFloat('0', VAR_FATID_0302));
  Dmod.QrUpd.SQL.Add('WHERE FatID=' + FormatFloat('0', VAR_FATID_0302 - 2)); // 3 0 0 !!!
  Dmod.QrUpd.ExecSQL;
  //
  MyObjects.Informa2EUpdPB(PB1, LaAviso5, LaAviso6, True,
  'Preparando para migra��o');
  DmLct2.MigraLctsParaTabLct();
  MyObjects.Informa2EUpdPB(PB1, LaAviso5, LaAviso6, False, '...');
end;

procedure TFmPrincipal.ObtemTipoEPercDeIOF2008(const TipoEnt,
  EhSimples: Integer; const ValOperacao: Double; var Perc: Double;
  var TipoIOF: Integer);
begin
  Perc := 0;
  TipoIOF := 0;
  //
  if TipoEnt = 0 then
  begin
    if EhSimples = 1 then
    begin
      if ValOperacao <= Dmod.QrControleIOF_Li.Value then
      begin
        Perc := Dmod.QrControleIOF_ME.Value;
        TipoIOF := 1;
      end else begin
        Perc := Dmod.QrControleIOF_PJ.Value;
        TipoIOF := 2;
      end;
    end else begin
      Perc := Dmod.QrControleIOF_PJ.Value;
      TipoIOF := 3;
    end;
  end else begin
    Perc := Dmod.QrControleIOF_PF.Value;
    TipoIOF := 4;
  end;
end;

procedure TFmPrincipal.EdBandaChange(Sender: TObject);
var
  CMC7: String;
  i: Integer;
begin
  BtConfAlt.Enabled := False;
  EdCPF.Text := '';
  EdEmitente.Text := '';
  if MLAGeral.LeuTodoCMC7(EdBanda.Text) then
  begin
    CMC7 := Geral.SoNumero_TT(EdBanda.Text);
    if MLAGeral.CalculaCMC7(CMC7) = 0 then
    begin
      for i := 1 to GradeC.RowCount - 1 do
      begin
        if GradeC.Cells[11, i] = CMC7 then
        begin
          GradeC.Row := i;
          EdCPF.Text := GradeC.Cells[09, i];
          EdEmitente.Text := GradeC.Cells[10, i];
          EdCPF.SetFocus;
          BtConfAlt.Enabled := True;
          Break;
        end;
      end;
      EdCPF.SetFocus;
    end;
  end;
end;

procedure TFmPrincipal.EdCPFChange(Sender: TObject);
begin
  EdEmitente.Text := '';
end;

procedure TFmPrincipal.EdCPFExit(Sender: TObject);
var
  CPF : String;
begin
  Screen.Cursor := crHourGlass;
  CPF := Geral.SoNumero_TT(EdCPF.Text);
  if (CPF <> '') and (CPF = Geral.CalculaCNPJCPF(CPF)) then
  begin
    if EdEmitente.Text = '' then
    begin
      DmLot2.QrLocEmiCPF.Close;
      DmLot2.QrLocEmiCPF.Params[0].AsString := CPF;
      UMyMod.AbreQuery(DmLot2.QrLocEmiCPF, Dmod.MyDB);
      //
      if DmLot2.QrLocEmiCPF.RecordCount > 0 then
        EdEmitente.Text := DmLot2.QrLocEmiCPFNome.Value;
    end;
  end;
  Screen.Cursor := crDefault;
end;

procedure TFmPrincipal.EdCPFKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
  begin
    EdBanda.Text := '';
    EdBanda.SetFocus;
  end;
end;

procedure TFmPrincipal.EdEmitenteEnter(Sender: TObject);
begin
  if Trim(EdCPF.Text) = '' then EdCPF.SetFocus;
end;

procedure TFmPrincipal.EdEmpresaChange(Sender: TObject);
begin
  QrCond2.Close;
  QrCond2.Params[0].AsInteger := EdEmpresa.ValueVariant;
  UMyMod.AbreQuery(QrCond2, Dmod.MyDB);
  if EdEmpresa.ValueVariant <> 0 then
    DModG.FPTK_Def_Client := QrCond2Cliente.Value
  else
    DModG.FPTK_Def_Client := 0;
  DModG.ReopenPTK();
end;

procedure TFmPrincipal.Em1Click(Sender: TObject);
begin
  FinanceiroJan.MostraSaldos(0);
end;

function TFmPrincipal.AcaoEspecificaDeApp(Servico: String): Boolean;
begin
  Result := True;
  //Compatibilidade
end;

procedure TFmPrincipal.AcoesExtrasDeCadastroDeEntidades(Grade1: TStringGrid;
  Entidade: Integer; AbrirEmAba: Boolean);
begin
 // Nada
end;

procedure TFmPrincipal.AcoesIniciaisDeCadastroDeEntidades(Reference: TComponent;
Codigo: Integer; Grade1: TStringGrid);
begin
   // Compatibilidade
end;

function TFmPrincipal.RecriaTabelaLocal(Tabela: String; Acao: Integer): Boolean;
begin
  Result := Ucriar.RecriaTabelaLocal(Tabela, Acao);
end;

procedure TFmPrincipal.Relatorios1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmRepasImp, FmRepasImp, afmoNegarComAviso) then
  begin
    FmRepasImp.ShowModal;
    FmRepasImp.Destroy;
  end;
end;

procedure TFmPrincipal.ReopenFatID_0301();
  procedure AbreSQL(Qry: TMySQLQuery; TpDoc: String);
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT lct.FatSit, lct.FatSitSub, ',
    'lct.Carteira, COUNT(*) ITENS, ',
    'SUM(Credito) Credito, ',
    'SUM(Debito) Debito, ',
    'SUM(Credito-Debito) SALDO, ',
    'car.Nome, car.Tipo ',
    'FROM ' + CO_TabLctA + ' lct ',
    'LEFT JOIN ' + CO_TabLotA + ' lot ON lot.Codigo=lct.FatNum ',
    'LEFT JOIN carteiras car ON car.Codigo=lct.Carteira ',
    'WHERE lct.FatID=' + TXT_VAR_FATID_0301,
    'AND lot.Tipo=' + TpDoc,
    'GROUP BY lct.FatSit, FatSitSub, lct.Carteira; ',
    '']);
  end;
begin
  Qr301Chq.Close;
  Qr301Dup.Close;
  Qr301Its.Close;
  //
  begin
    AbreSQL(Qr301Chq, '0');
    AbreSQL(Qr301Dup, '1');
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qr301Its, Dmod.MyDB, [
    'SELECT Vencimento, ',
    'YEAR(Vencimento) + 0.000 ANO, ',
    'MONTH(Vencimento) + 0.000 MES, ',
    'SUM(Credito) Credito ',
    'FROM ' + CO_TabLctA,
    'WHERE FatID=' + TXT_VAR_FATID_0301,
    'AND FatSit=20 ',
    'AND FatSitSub=40 ',
    'GROUP BY Vencimento ',
    'ORDER BY Vencimento ',
    '']);
  end;
end;

procedure TFmPrincipal.ResultadosMensais1Click(Sender: TObject);
begin
  FinanceiroJan.MostraResultadosMensais;
end;

procedure TFmPrincipal.Padro2Click(Sender: TObject);
(*
var
  Cam: String;
*)
begin
(*
  Cam := Application.Title+'\VCLSkins';
  MLAGeral.DelAppKey(Cam, HKEY_CURRENT_USER);
  MeuVCLSkin.VCLSkinDefineUso(FmPrincipal.sd1, FmPrincipal.skinstore1);
*)
end;

procedure TFmPrincipal.Padro3Click(Sender: TObject);
begin
  Geral.WriteAppKeyCU('MenuStyle', Application.Title,
    TMenuItem(Sender).Tag, ktInteger);
  SkinMenu(TMenuItem(Sender).Tag);
end;

procedure TFmPrincipal.PMMigracaoPopup(Sender: TObject);
var
  Lct1, Lct2: Integer;
begin
  Lct1 := 0;
  //Lct2 := 0;
  //
  try
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT COUNT(*) Itens FROM ' + LAN_CTOS + ' WHERE Controle <> 0');
    UMyMod.AbreQuery(Dmod.QrAux, Dmod.MyDB);
    Lct1 := Dmod.QrAux.FieldByName('Itens').AsInteger;
  except
    ;
  end;
  //
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT COUNT(*) Itens FROM ' + CO_TabLctA + ' WHERE Controle <> 0');
  UMyMod.AbreQuery(Dmod.QrAux, Dmod.MyDB);
  Lct2 := Dmod.QrAux.FieldByName('Itens').AsInteger;
  //
  NovoFinanceiro1.Enabled := (Lct1 > 0) and (Lct2 = 0);
  //
  ItensdeLote1.Enabled := Lct1 = 0;
end;

procedure TFmPrincipal.PorNveldoPLanodeContas1Click(Sender: TObject);
begin
  FinanceiroJan.MostraPesquisaPorNveldoPLanodeContas(0);
end;

procedure TFmPrincipal.Reabrirtabelas1Click(Sender: TObject);
begin
  UnDmkDAC_PF.ReabrirtabelasFormAtivo(Sender);
end;

procedure TFmPrincipal.ReCaptionComponentesDeForm(Form: TForm(*; Memo: TMemo*));
begin
  // N�o usa
end;

procedure TFmPrincipal.SalvaArquivo(EdNomCli, EdCPFCli: TEdit; Grade:
  TStringGrid; Data: TDateTime; FileName: String; ChangeData: Boolean);
begin
  //MyCBase.SalvaArquivo(EdNomCli, EdCPFCli, Grade, Data, VAR_DBPATH+FileName, ChangeData);
end;

procedure TFmPrincipal.SalvaInfoRegEdit(Form: TForm; Dest: Integer;
  MinhaGrade: TStringGrid);
var
  Cam, N: String;
  i, j, L, ARow: Integer;
  Painel: TPanel;
  Grade: TDBGrid;
begin
  ARow := 0;
  Cam := Application.Title+'\InitialConfig\'+Form.Name;
  if Dest = 1 then
  begin
    //if not Aplicar1.Checked then Exit;
    Geral.WriteAppKey('WindowState', Cam, Form.WindowState, ktInteger, HKey_LOCAL_MACHINE);
    Geral.WriteAppKey('WindowWidth', Cam, Form.Width, ktInteger, HKey_LOCAL_MACHINE);
    Geral.WriteAppKey('WindowHeigh', Cam, Form.Height, ktInteger, HKey_LOCAL_MACHINE);
    //Geral.WriteAppKey('WindowAplic', Cam, Aplicar1.Checked, ktBoolean, HKey_LOCAL_MACHINE);
  end else if (Dest = 0) and (MinhaGrade <> nil) then
  begin
    MinhaGrade.ColWidths[1] := 128;
    MinhaGrade.ColWidths[3] := 128;
    MinhaGrade.ColWidths[4] := 128;
    MinhaGrade.ColWidths[5] := 128;
    MinhaGrade.Cells[0, 0] := 'Componente';
    MinhaGrade.Cells[1, 0] := 'Nome do componente';
    MinhaGrade.Cells[2, 0] := 'Comprimento';
    MinhaGrade.Cells[3, 0] := 'Coluna';
    MinhaGrade.Cells[4, 0] := 'Campo';
    MinhaGrade.Cells[5, 0] := 'T�tulo';
  end;
  for i := 0 to Form.ComponentCount -1 do
  begin
    if Form.Components[i] is TPanel then
    begin
      Painel := TPanel(Form.Components[i]);
      N := Painel.Name;
      if Painel.Align <> alClient then
      begin
        L := Painel.Width;
        if Dest = 1 then Geral.WriteAppKey(N, Cam, L, ktInteger, HKey_LOCAL_MACHINE)
        else if (Dest = 0) and (MinhaGrade<>nil) then
        begin
          ARow := ARow + 1;
          MinhaGrade.RowCount := ARow + 1;
          MinhaGrade.Cells[0, ARow] := 'TPanel';
          MinhaGrade.Cells[1, ARow] := Painel.Name;
          MinhaGrade.Cells[2, ARow] := intToStr(Painel.Width);
        end;
      end;
    end;
    if Form.Components[i] is TDBGrid then
    begin
      Grade := TDBGrid(Form.Components[i]);
      for j := 0 to Grade.Columns.Count-1 do
      begin
        if Dest = 1 then
        begin
          Geral.WriteAppKey(IntToStr(j), Cam+'\Grades\'+Grade.Name+'\Pos',
          Grade.Columns[j].FieldName, ktString, HKey_LOCAL_MACHINE);
          Geral.WriteAppKey(IntToStr(j), Cam+'\Grades\'+Grade.Name+'\Title',
          Grade.Columns[j].Title.Caption, ktString, HKey_LOCAL_MACHINE);
          Geral.WriteAppKey(IntToStr(j), Cam+'\Grades\'+
          Grade.Name+'\Width', Grade.Columns[j].Width, ktInteger, HKey_LOCAL_MACHINE);
        end else if (Dest = 0) and (MinhaGrade<>nil) then
        begin
          ARow := ARow + 1;
          MinhaGrade.RowCount := ARow + 1;
          MinhaGrade.Cells[0, ARow] := 'TDBGrid';
          MinhaGrade.Cells[1, ARow] := Grade.Name;
          MinhaGrade.Cells[2, ARow] := IntToStr(Grade.Columns[j].Width);
          MinhaGrade.Cells[3, ARow] := IntToStr(j);
          MinhaGrade.Cells[4, ARow] := Grade.Columns[j].FieldName;
          MinhaGrade.Cells[5, ARow] := Grade.Columns[j].Title.Caption;
        end;
      end;
    end;
  end;
end;

function TFmPrincipal.AbreArquivoINI(Grade: TStringGrid; EdNomCli, EdCPFCli: TEdit;
  Data: TDateTime; Arquivo: String): Boolean;
begin
  Result := False;
  //MyCBase.AbreArquivoINI(Grade, EdNomCli, EdCPFCli, Data, Arquivo);
end;

procedure TFmPrincipal.Antigo2Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEvolucapi, FmEvolucapi, afmoNegarComAviso) then
  begin
    FmEvolucapi.ShowModal;
    FmEvolucapi.Destroy;
  end;
end;

procedure TFmPrincipal.AplicaInfoRegEdit(Form: TForm; Forca: Boolean);
var
  Cam, N: String;
  //W, H,
  i, L, j: Integer;
  Painel: TPanel;
  Grade: TDBGrid;
  Query: TmySQLQuery;
begin
  if Forca then FAplicaInfoRegEdit := False;
  if FAplicaInfoRegEdit then Exit;
  FAplicaInfoRegEdit := True;
  Cam := Application.Title+'\InitialConfig\'+Form.Name;
  if Forca or Geral.ReadAppKey('WindowAplic', Cam, ktBoolean, False,
  HKey_LOCAL_MACHINE) then
  begin
    //Aplicar1.Checked := True;
    Form.WindowState := Geral.ReadAppKey('WindowState', Cam, ktInteger, 0, HKEY_LOCAL_MACHINE);
    for i := 0 to Form.ComponentCount -1 do
    begin
      ////////////////////////////////////////////////////////////////////////
      if Form.Components[i] is TmySQLQuery then
      begin
        Query := TmySQLQuery(Form.Components[i]);
        for j := 0 to Query.FieldCount-1 do
        begin
          if Query.Fields[j] is TDateField then
          TDateField(Query.Fields[j]).DisplayFormat := VAR_FORMATDATEi;
        end;
      end;
      ////////////////////////////////////////////////////////////////////////
      if Form.Components[i] is TPanel then
      begin
        Painel := TPanel(Form.Components[i]);
        N := Painel.Name;
        if Painel.Align <> alClient then
        begin
          L := Geral.ReadAppKey(N, Cam, ktInteger, 0, HKEY_LOCAL_MACHINE);
          if L > 0 then
          Painel.Width := L;
        end;
      end;
      ////////////////////////////////////////////////////////////////////////
      if Form.Components[i] is TDBGrid then
      begin
        Grade := TDBGrid(Form.Components[i]);
        for j := 0 to Grade.Columns.Count-1 do
        begin
          N := Geral.ReadAppKey(IntToStr(j), Cam+'\Grades\'+
          Grade.Name+'\Pos', ktString, '', HKey_LOCAL_MACHINE);
          if N <> '' then Grade.Columns[j].FieldName := N;
          N := Geral.ReadAppKey(IntToStr(j), Cam+'\Grades\'+
          Grade.Name+'\Title', ktString, '', HKey_LOCAL_MACHINE);
          if N <> '' then Grade.Columns[j].Title.Caption := N;
          L := Geral.ReadAppKey(IntToStr(j), Cam+'\Grades\'+
          Grade.Name+'\Width', ktInteger, 0, HKey_LOCAL_MACHINE);
          if L > 0 then Grade.Columns[j].Width := L;
        end;
      end;
    end;
  end;
end;

procedure TFmPrincipal.ShowHint(Sender: TObject);
begin
  if Length(Application.Hint) > 0 then
  begin
    StatusBar.SimplePanel := True;
    StatusBar.SimpleText := Application.Hint;
  end
  else StatusBar.SimplePanel := False;
end;

procedure TFmPrincipal.MenuItem2Click(Sender: TObject);
begin
  Geral.WriteAppKeyCU('ImagemFundo', Application.Title, '', ktString);
  Geral.MensagemBox(
  '� necess�rio reexecutar o aplicativo para que a altera��o tenha efeito!',
  'Aviso', MB_OK+MB_ICONWARNING);
end;

procedure TFmPrincipal.MenuItem5Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmLot0Dep, FmLot0Dep, afmoNegarComAviso) then
  begin
    FmLot0Dep.ShowModal;
    FmLot0Dep.Destroy;
  end;
end;

procedure TFmPrincipal.MenuItem6Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPesqDep, FmPesqDep, afmoNegarComAviso) then
  begin
    FmPesqDep.ShowModal;
    FmPesqDep.Destroy;
  end;
end;

function TFmPrincipal.MigraEspecificos(CliInt: Integer): Boolean;
var
  A1, A2, B1, B2, A3, B3: Integer;
  CI_TXT, Campos, TabAriA, TabCnsA, TabPrvA, TabPriA: String;
begin
  CI_TXT  := FormatFloat('0', CliInt);
{
  TabAriA := 'ari' + FormatFloat('0000', CliInt) + ttA;
  TabCnsA := 'cns' + FormatFloat('0000', CliInt) + ttA;
  TabPriA := 'pri' + FormatFloat('0000', CliInt) + ttA;
  TabPrvA := 'prv' + FormatFloat('0000', CliInt) + ttA;
}
  TabAriA := DModG.NomeTab(TMeuDB, ntAri, False, ttA, CliInt);
  TabCnsA := DModG.NomeTab(TMeuDB, ntCns, False, ttA, CliInt);
  TabPriA := DModG.NomeTab(TMeuDB, ntPri, False, ttA, CliInt);
  TabPrvA := DModG.NomeTab(TMeuDB, ntPrv, False, ttA, CliInt);

  //////////////////////////////////////////////////////////////////////////////
  ///  P R E V I T S
  //////////////////////////////////////////////////////////////////////////////

  //
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT COUNT(pri.Codigo) Itens');
  Dmod.QrAux.SQL.Add('FROM ' + TAB_PRI + ' pri');
  Dmod.QrAux.SQL.Add('LEFT JOIN ' + TAB_PRV + ' prv ON prv.Codigo=pri.Codigo');
  Dmod.QrAux.SQL.Add('WHERE prv.Cond=' + CI_TXT);
  UMyMod.AbreQuery(Dmod.QrAux, Dmod.MyDB);
  A1 := Dmod.QrAux.FieldByName('Itens').AsInteger;
  //
  if A1 > 0 then
  begin
    Screen.Cursor := crHourGlass;
    //
    try
      Dmod.QrUpd.SQL.Clear;
      {
      Dmod.QrUpd.SQL.Add('LOCK TABLES ' +
        TabPrvA + ' WRITE, ' +
        TabPriA + ' WRITE, ' +
        TAB_PRI + ' WRITE, ' +
        TAB_PRV + ' WRITE, master WRITE');
      Dmod.QrUpd.ExecSQL;
      }
      //
      try
        Dmod.QrAux.Close;
        Dmod.QrAux.SQL.Clear;
        Dmod.QrAux.SQL.Add('SELECT COUNT(Codigo) Itens FROM ' + TabPriA);
        UMyMod.AbreQuery(Dmod.QrAux, Dmod.MyDB);
        B1 := Dmod.QrAux.FieldByName('Itens').AsInteger;
        //
        Campos := UMyMod.ObtemCamposDeTabelaIdentica(Dmod.MyDB, TabPriA, 'pri.');
        //
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('INSERT INTO ' + TabPriA);
        Dmod.QrUpd.SQL.Add('SELECT ' + Campos);
        Dmod.QrUpd.SQL.Add('FROM ' + TAB_PRI + ' pri');
        Dmod.QrUpd.SQL.Add('LEFT JOIN ' + TAB_PRV + ' prv ON prv.Codigo=pri.Codigo');
        Dmod.QrUpd.SQL.Add('WHERE prv.Cond=' + CI_TXT);

        Dmod.QrUpd.ExecSQL;
        //
        Dmod.QrAux.Close;
        Dmod.QrAux.SQL.Clear;
        Dmod.QrAux.SQL.Add('SELECT COUNT(Codigo) Itens FROM ' + TabPriA);
        UMyMod.AbreQuery(Dmod.QrAux, Dmod.MyDB);
        B2 := Dmod.QrAux.FieldByName('Itens').AsInteger;
        //
        B3 := B2 - B1;
        if B3 > 0 then
        begin
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('DELETE pri');
          Dmod.QrUpd.SQL.Add('FROM ' + TAB_PRI + ' pri, ' + TAB_PRV + ' prv');
          Dmod.QrUpd.SQL.Add('WHERE pri.Codigo=prv.Codigo');
          Dmod.QrUpd.SQL.Add('AND prv.Cond=' + CI_TXT);
          Dmod.QrUpd.ExecSQL;
          //
          // Parei aqui!
          //fazer update na tabela carteira (entidade?) que esta foi migrada
        end;
        //
        Dmod.QrAux.Close;
        Dmod.QrAux.SQL.Clear;
        Dmod.QrAux.SQL.Add('SELECT COUNT(pri.Codigo) Itens');
        Dmod.QrAux.SQL.Add('FROM ' + TAB_PRI + ' pri');
        Dmod.QrAux.SQL.Add('LEFT JOIN ' + TAB_PRV + ' prv ON prv.Codigo=pri.Codigo');
        Dmod.QrAux.SQL.Add('WHERE prv.Cond=' + CI_TXT);
        UMyMod.AbreQuery(Dmod.QrAux, Dmod.MyDB);
        A2 := Dmod.QrAux.FieldByName('Itens').AsInteger;
        //
      finally
      {
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('UNLOCK TABLES');
        Dmod.QrUpd.ExecSQL;
      }
      end;

      A3 := A1 - A2;
      if A3 <> B3 then
      begin
        Result := False;
      end else
      begin
        Result := True;
      end;
      Screen.Cursor := crDefault;
   finally
      Screen.Cursor := crDefault;
    end;
  end else Result := True;

  if Result then
  begin

    //////////////////////////////////////////////////////////////////////////////
    ///  C O N S I T S
    //////////////////////////////////////////////////////////////////////////////

    //
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT COUNT(cns.Codigo) Itens');
    Dmod.QrAux.SQL.Add('FROM ' + TAB_CNS + ' cns');
    Dmod.QrAux.SQL.Add('WHERE cns.Cond=' + CI_TXT);
    UMyMod.AbreQuery(Dmod.QrAux, Dmod.MyDB);
    A1 := Dmod.QrAux.FieldByName('Itens').AsInteger;
    //
    if A1 > 0 then
    begin
      Screen.Cursor := crHourGlass;
      //
      try
        Dmod.QrUpd.SQL.Clear;
        {
        Dmod.QrUpd.SQL.Add('LOCK TABLES ' +
          TabPrvA + ' WRITE, ' +
          TabPriA + ' WRITE, ' +
          TAB_CNS + ' WRITE, ' +
          TAB_PRV + ' WRITE, master WRITE');
        Dmod.QrUpd.ExecSQL;
        }
        //
        try
          Dmod.QrAux.Close;
          Dmod.QrAux.SQL.Clear;
          Dmod.QrAux.SQL.Add('SELECT COUNT(Codigo) Itens FROM ' + TabCnsA);
          UMyMod.AbreQuery(Dmod.QrAux, Dmod.MyDB);
          B1 := Dmod.QrAux.FieldByName('Itens').AsInteger;
          //
          Campos := UMyMod.ObtemCamposDeTabelaIdentica(Dmod.MyDB, TabCnsA, '');
          //
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('INSERT INTO ' + TabCnsA);
          Dmod.QrUpd.SQL.Add('SELECT ' + Campos);
          Dmod.QrUpd.SQL.Add('FROM ' + TAB_CNS + ' cns');
          Dmod.QrUpd.SQL.Add('WHERE cns.Cond=' + CI_TXT);

          Dmod.QrUpd.ExecSQL;
          //
          Dmod.QrAux.Close;
          Dmod.QrAux.SQL.Clear;
          Dmod.QrAux.SQL.Add('SELECT COUNT(Codigo) Itens FROM ' + TabCnsA);
          UMyMod.AbreQuery(Dmod.QrAux, Dmod.MyDB);
          B2 := Dmod.QrAux.FieldByName('Itens').AsInteger;
          //
          B3 := B2 - B1;
          if B3 > 0 then
          begin
            Dmod.QrUpd.SQL.Clear;
            Dmod.QrUpd.SQL.Add('DELETE cns');
            Dmod.QrUpd.SQL.Add('FROM ' + TAB_CNS + ' cns');
            Dmod.QrUpd.SQL.Add('WHERE cns.Cond=' + CI_TXT);
            Dmod.QrUpd.ExecSQL;
          end;
          //
          Dmod.QrAux.Close;
          Dmod.QrAux.SQL.Clear;
          Dmod.QrAux.SQL.Add('SELECT COUNT(cns.Codigo) Itens');
          Dmod.QrAux.SQL.Add('FROM ' + TAB_CNS + ' cns');
          Dmod.QrAux.SQL.Add('WHERE cns.Cond=' + CI_TXT);
          UMyMod.AbreQuery(Dmod.QrAux, Dmod.MyDB);
          A2 := Dmod.QrAux.FieldByName('Itens').AsInteger;
          //
        finally
        {
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('UNLOCK TABLES');
          Dmod.QrUpd.ExecSQL;
        }
        end;

        A3 := A1 - A2;
        if A3 <> B3 then
        begin
          Result := False;
        end else
        begin
          Result := True;
        end;
        Screen.Cursor := crDefault;
     finally
        Screen.Cursor := crDefault;
      end;
    end;
  end;

  if Result then
  begin

    //////////////////////////////////////////////////////////////////////////////
    ///  A R R E I T S
    //////////////////////////////////////////////////////////////////////////////

    //
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT COUNT(Ari.Codigo) Itens');
    Dmod.QrAux.SQL.Add('FROM ' + TAB_ARI + ' Ari');
    Dmod.QrAux.SQL.Add('LEFT JOIN ' + TAB_PRV + ' prv ON prv.Codigo=Ari.Codigo');
    Dmod.QrAux.SQL.Add('WHERE prv.Cond=' + CI_TXT);
    UMyMod.AbreQuery(Dmod.QrAux, Dmod.MyDB);
    A1 := Dmod.QrAux.FieldByName('Itens').AsInteger;
    //
    if A1 > 0 then
    begin
      Screen.Cursor := crHourGlass;
      //
      try
        Dmod.QrUpd.SQL.Clear;
        {
        Dmod.QrUpd.SQL.Add('LOCK TABLES ' +
          TabPrvA + ' WRITE, ' +
          TabAriA + ' WRITE, ' +
          TAB_ARI + ' WRITE, ' +
          TAB_PRV + ' WRITE, master WRITE');
        Dmod.QrUpd.ExecSQL;
        }
        //
        try
          Dmod.QrAux.Close;
          Dmod.QrAux.SQL.Clear;
          Dmod.QrAux.SQL.Add('SELECT COUNT(Codigo) Itens FROM ' + TabAriA);
          UMyMod.AbreQuery(Dmod.QrAux, Dmod.MyDB);
          B1 := Dmod.QrAux.FieldByName('Itens').AsInteger;
          //
          Campos := UMyMod.ObtemCamposDeTabelaIdentica(Dmod.MyDB, TabAriA, 'Ari.');
          //
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('INSERT INTO ' + TabAriA);
          Dmod.QrUpd.SQL.Add('SELECT ' + Campos);
          Dmod.QrUpd.SQL.Add('FROM ' + TAB_ARI + ' Ari');
          Dmod.QrUpd.SQL.Add('LEFT JOIN ' + TAB_PRV + ' prv ON prv.Codigo=Ari.Codigo');
          Dmod.QrUpd.SQL.Add('WHERE prv.Cond=' + CI_TXT);

          Dmod.QrUpd.ExecSQL;
          //
          Dmod.QrAux.Close;
          Dmod.QrAux.SQL.Clear;
          Dmod.QrAux.SQL.Add('SELECT COUNT(Codigo) Itens FROM ' + TabAriA);
          UMyMod.AbreQuery(Dmod.QrAux, Dmod.MyDB);
          B2 := Dmod.QrAux.FieldByName('Itens').AsInteger;
          //
          B3 := B2 - B1;
          if B3 > 0 then
          begin
            Dmod.QrUpd.SQL.Clear;
            Dmod.QrUpd.SQL.Add('DELETE Ari');
            Dmod.QrUpd.SQL.Add('FROM ' + TAB_ARI + ' Ari, ' + TAB_PRV + ' prv');
            Dmod.QrUpd.SQL.Add('WHERE Ari.Codigo=prv.Codigo');
            Dmod.QrUpd.SQL.Add('AND prv.Cond=' + CI_TXT);
            Dmod.QrUpd.ExecSQL;
            //
            // Parei aqui!
            //fazer update na tabela carteira (entidade?) que esta foi migrada
          end;
          //
          Dmod.QrAux.Close;
          Dmod.QrAux.SQL.Clear;
          Dmod.QrAux.SQL.Add('SELECT COUNT(Ari.Codigo) Itens');
          Dmod.QrAux.SQL.Add('FROM ' + TAB_ARI + ' Ari');
          Dmod.QrAux.SQL.Add('LEFT JOIN ' + TAB_PRV + ' prv ON prv.Codigo=Ari.Codigo');
          Dmod.QrAux.SQL.Add('WHERE prv.Cond=' + CI_TXT);
          UMyMod.AbreQuery(Dmod.QrAux, Dmod.MyDB);
          A2 := Dmod.QrAux.FieldByName('Itens').AsInteger;
          //
        finally
        {
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('UNLOCK TABLES');
          Dmod.QrUpd.ExecSQL;
        }
        end;

        A3 := A1 - A2;
        if A3 <> B3 then
        begin
          Result := False;
        end else
        begin
          Result := True;
        end;
        Screen.Cursor := crDefault;
     finally
        Screen.Cursor := crDefault;
      end;
    end;
  end;

  //

  if Result then
  begin

    //////////////////////////////////////////////////////////////////////////////
    ///  P R E V
    //////////////////////////////////////////////////////////////////////////////

    //
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT COUNT(prv.Codigo) Itens');
    Dmod.QrAux.SQL.Add('FROM ' + TAB_PRV + ' prv');
    Dmod.QrAux.SQL.Add('WHERE prv.Cond=' + CI_TXT);
    UMyMod.AbreQuery(Dmod.QrAux, Dmod.MyDB);
    A1 := Dmod.QrAux.FieldByName('Itens').AsInteger;
    //
    if A1 > 0 then
    begin
      Screen.Cursor := crHourGlass;
      //
      try
        Dmod.QrUpd.SQL.Clear;
        {
        Dmod.QrUpd.SQL.Add('LOCK TABLES ' +
          TabPrvA + ' WRITE, ' +
          TAB_PRV + ' WRITE, master WRITE');
        Dmod.QrUpd.ExecSQL;
        }
        //
        try
          Dmod.QrAux.Close;
          Dmod.QrAux.SQL.Clear;
          Dmod.QrAux.SQL.Add('SELECT COUNT(Codigo) Itens FROM ' + TabPrvA);
          UMyMod.AbreQuery(Dmod.QrAux, Dmod.MyDB);
          B1 := Dmod.QrAux.FieldByName('Itens').AsInteger;
          //
          Campos := UMyMod.ObtemCamposDeTabelaIdentica(Dmod.MyDB, TabPrvA, '');
          //
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('INSERT INTO ' + TabPrvA);
          Dmod.QrUpd.SQL.Add('SELECT ' + Campos);
          Dmod.QrUpd.SQL.Add('FROM ' + TAB_PRV + ' prv');
          Dmod.QrUpd.SQL.Add('WHERE prv.Cond=' + CI_TXT);
          
          Dmod.QrUpd.ExecSQL;
          //
          Dmod.QrAux.Close;
          Dmod.QrAux.SQL.Clear;
          Dmod.QrAux.SQL.Add('SELECT COUNT(Codigo) Itens FROM ' + TabPrvA);
          UMyMod.AbreQuery(Dmod.QrAux, Dmod.MyDB);
          B2 := Dmod.QrAux.FieldByName('Itens').AsInteger;
          //
          B3 := B2 - B1;
          if B3 > 0 then
          begin
            Dmod.QrUpd.SQL.Clear;
            Dmod.QrUpd.SQL.Add('DELETE Prv');
            Dmod.QrUpd.SQL.Add('FROM ' + TAB_PRV + ' prv');
            Dmod.QrUpd.SQL.Add('WHERE prv.Cond=' + CI_TXT);
            Dmod.QrUpd.ExecSQL;
          end;
          //
          Dmod.QrAux.Close;
          Dmod.QrAux.SQL.Clear;
          Dmod.QrAux.SQL.Add('SELECT COUNT(prv.Codigo) Itens');
          Dmod.QrAux.SQL.Add('FROM ' + TAB_PRV + ' prv');
          Dmod.QrAux.SQL.Add('WHERE prv.Cond=' + CI_TXT);
          UMyMod.AbreQuery(Dmod.QrAux, Dmod.MyDB);
          A2 := Dmod.QrAux.FieldByName('Itens').AsInteger;
          //
        finally
        {
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('UNLOCK TABLES');
          Dmod.QrUpd.ExecSQL;
        }
        end;

        A3 := A1 - A2;
        if A3 <> B3 then
        begin
          Result := False;
        end else
        begin
          Result := True;
        end;
        Screen.Cursor := crDefault;
     finally
        Screen.Cursor := crDefault;
      end;
    end;
  end;
end;

procedure TFmPrincipal.Minimizaraplicativo1Click(Sender: TObject);
begin
  Application.Minimize;
end;

procedure TFmPrincipal.LeCMC7_Criados();
var
  i: Integer;
  Banda: TBandaMagnetica;
  Comp, Banco, Agencia, Conta, Cheque, Valor, Vencto: String;
begin
  FCheque := 0;
  Banda := TBandaMagnetica.Create;
  for i := 0 to Memo2.Lines.Count - 1 do
  begin
    if MLAGeral.CalculaCMC7(Memo2.Lines[i]) = 0 then
    begin
      Banda.BandaMagnetica := Geral.SoNumero_TT(Memo2.Lines[i]);
      Comp    := Banda.Compe;
      Banco   := Banda.Banco;
      Agencia := Banda.Agencia;
      Conta   := Banda.Conta;
      Cheque  := Banda.Numero;
      //
      Valor   := Trim(Copy(Memo1.Lines[i], 072, 15));
      Vencto  :=      Copy(Memo1.Lines[i], 001, 10);
      //
      FCheque := FCheque + 1;
      with FmPrincipal.GradeC do
      begin
        if FCheque > RowCount-1 then RowCount := FCheque + 1;
        Cells[00,FCheque] := IntToStr(FCheque);
        Cells[01,FCheque] := Comp;
        Cells[02,FCheque] := Banco;
        Cells[03,FCheque] := Agencia;
        Cells[04,FCheque] := Conta;
        Cells[05,FCheque] := Cheque;
        Cells[06,FCheque] := Valor;
        Cells[07,FCheque] := Vencto;
        Cells[09,FCheque] := '';//CPF;
        Cells[10,FCheque] := '';//Emitente;
        Cells[11,FCheque] := Geral.SoNumero_TT(Memo2.Lines[i]);
      end;
      FChangeData := True;
      ArqSCX.CalculaGradeC(EdQtdeCh, EdValorCH, nil, nil, nil,
        GradeC, nil, TPCheque);
    end;
  end;
end;

function TFmPrincipal.LetraModelosBloq(Item: Integer): String;
begin
  case Item of
    1: Result := 'A';
    2: Result := 'B';
    3: Result := 'C';
    4: Result := 'D';
    5: Result := 'E';
    6: Result := 'G';
    7: Result := 'G';
    8: Result := 'H';
    9: Result := 'R';
    else Result := '?';
  end;
end;

function TFmPrincipal.LocalizaEmitente(Linha: Integer;
  MudaNome: Boolean): Boolean;
var
  B,A,C, CPF: String;
begin
  Result := False;
  if  (Geral.IMV(GradeC.Cells[2,Linha]) > 0)
  and (Geral.IMV(GradeC.Cells[3,Linha]) > 0)
  and (Geral.DMV(GradeC.Cells[4,Linha]) > 0.1) then
  begin
    Screen.Cursor := crHourGlass;
    try
      //
      if MudaNome then
      begin
        B := GradeC.Cells[2,Linha];
        A := GradeC.Cells[3,Linha];
        C := GradeC.Cells[4,Linha];
        CPF := Geral.SoNumero_TT(EdCPF.Text);
        DmLot2.ReopenBanco(Geral.IMV(B));
        //
        DmLot2.QrLocEmiBAC.Close;
        DmLot2.QrLocEmiBAC.Params[0].AsInteger := DmLot2.QrBancoDVCC.Value;
        DmLot2.QrLocEmiBAC.Params[1].AsString  := CriaBAC_Pesq(B,A,C,DmLot2.QrBancoDVCC.Value);
        UMyMod.AbreQuery(DmLot2.QrLocEmiBAC, Dmod.MyDB);
        //
        if DmLot2.QrLocEmiBAC.RecordCount = 0 then
        begin
          //EdEmitente.Text := '';
          //EdCPF.Text := '';
        end else begin
          DmLot2.QrLocEmiCPF.Close;
          DmLot2.QrLocEmiCPF.Params[0].AsString := DmLot2.QrLocEmiBACCPF.Value;
          UMyMod.AbreQuery(DmLot2.QrLocEmiCPF, Dmod.MyDB);
          //
          if DmLot2.QrLocEmiCPF.RecordCount = 0 then
          begin
            //EdEmitente.Text := '';
            //EdCPF.Text := '';
          end else begin
            GradeC.Cells[10, Linha] := DmLot2.QrLocEmiCPFNome.Value;
            GradeC.Cells[09, Linha] := Geral.FormataCNPJ_TT(DmLot2.QrLocEmiCPFCPF.Value);
            //PesquisaRiscoSacado(EdCPF.Text);
          end;
        end;
      end;
      Screen.Cursor := crDefault;
      Result := True;
    except
      Screen.Cursor := crDefault;
      raise;
    end;
  end;
end;

procedure TFmPrincipal.LquidoNovo1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmResult1, FmResult1, afmoNegarComAviso) then
  begin
    FmResult1.ShowModal;
    FmResult1.Destroy;
  end;
end;

procedure TFmPrincipal.SkinMenu(Index: integer);
{var
  Indice : Integer;}
begin
  {if Index >= 0 then
    Indice := Index
  else
    Indice := Geral.ReadAppKeyCU('MenuStyle', Application.Title,
      ktInteger, 1);}
  case Index of
    0: AdvToolBarOfficeStyler1.Style := bsOffice2003Blue;
    1: AdvToolBarOfficeStyler1.Style := bsOffice2003Classic;
    2: AdvToolBarOfficeStyler1.Style := bsOffice2003Olive;
    3: AdvToolBarOfficeStyler1.Style := bsOffice2003Silver;
    4: AdvToolBarOfficeStyler1.Style := bsOffice2007Luna;
    5: AdvToolBarOfficeStyler1.Style := bsOffice2007Obsidian;
    6: AdvToolBarOfficeStyler1.Style := bsOffice2007Silver;
    7: AdvToolBarOfficeStyler1.Style := bsOfficeXP;
    8: AdvToolBarOfficeStyler1.Style := bsWhidbeyStyle;
    9: AdvToolBarOfficeStyler1.Style := bsWindowsXP;
  end;
end;

procedure TFmPrincipal.Suporte1Click(Sender: TObject);
begin
  {$IFDEF UsaWSuport}
  DmkWeb.MostraWSuporte(False, 2, BalloonHint1);
  {$ENDIF}
end;

procedure TFmPrincipal.CadastroDeProtocolos(Lote: Integer);
begin
  //Compatibilidade
end;

procedure TFmPrincipal.CadastroGruSacEmi(Grupo: Integer);
begin
  if DBCheck.CriaFm(TFmGruSacEmi, FmGruSacEmi, afmoNegarComAviso) then
  begin
    FmGruSacEmi.ShowModal;
    FmGruSacEmi.Destroy;
  end;
end;

procedure TFmPrincipal.CadastroOcorBank(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmOcorBank, FmOcorBank, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmOcorBank.LocCod(Codigo, Codigo);
    FmOcorBank.ShowModal;
    FmOcorBank.Destroy;
  end;
end;

procedure TFmPrincipal.InsereImportLote(Pagina: TPageControl;
GradeC, GradeD: TStringGrid; TPCheque: TdmkEditDateTimePicker);
begin
  if DBCheck.CriaFm(TFmLot2Loa, FmLot2Loa, afmoLiberado) then
  begin
    FmLot2Loa.Show;
    FmLot2Loa.InsereItensImportLote(Pagina, GradeC, GradeD, TPCheque);
    FmLot2Loa.Destroy;
  end;
end;

procedure TFmPrincipal.ItensdeLote1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmLot2MIF, FmLot2MIF, afmoSoAdmin) then
  begin
    FmLot2MIF.ShowModal;
    FmLot2MIF.Destroy;
  end;
end;

{
procedure TFmPrincipal.DefineVarsCliInt(CliInt: Integer);
begin
(*
   ANTES:
    DmodG.QrCliIntLog.SQL.Strings = (
      'SELECT Codigo'
      'FROM entidades'
      'WHERE CliInt=:P0')
*)
  UnDmkDAC_PF.AbreMySQLQuery0(DmodG.QrCliIntLog, Dmod.MyDB, [
  'SELECT DISTINCT eci.CodEnti',
  'FROM enticliint eci',
  'LEFT JOIN senhas snh ON snh.Funcionario=eci.AccManager',
  'LEFT JOIN senhasits sei ON eci.CodEnti=sei.Empresa',
  'WHERE eci.CodCliInt=' + FormatFloat('0', CliInt),
  'AND ',
  '(',
  '  eci.AccManager=0',
  '  OR ',
  '  snh.Numero=' + FormatFloat('0', VAR_USUARIO),
  '  OR',
  '  sei.Numero=' + FormatFloat('0', VAR_USUARIO),
  '  OR',
     FormatFloat('0', VAR_USUARIO) + '<0',
  ')',
  '']);
  //
  FEntInt := DmodG.QrCliIntLogCodEnti.Value;
  if FEntInt = 0 then
    FCliInt_ := 0
  else
    FCliInt_ := CliInt;
  //
  VAR_LIB_EMPRESAS := FormatFloat('0', DmodG.QrCliIntLogCodEnti.Value);
  if VAR_LIB_EMPRESAS = '' then VAR_LIB_EMPRESAS := '-100000000';
  VAR_LIB_FILIAIS  := '';
  DModG.DefineDataMinima(FEntInt);
  //VAR LCT_ENCERRADO := DModG.OBtemDataUltimoEncerramentoFinanceiro(FEntInt);
  //
  DmLct2.QrCrt.Close;
  DmLct2.QrLct.Close;
  //
end;
}

procedure TFmPrincipal.DemonstrativoDeReceitasEDespesas(Empresa: Integer);
begin
  if DBCheck.CriaFm(TFmReceDesp, FmReceDesp, afmoNegarComAviso) then
  begin
    if Empresa <> 0 then
    begin
      FmReceDesp.EdEmpresa.ValueVariant := Empresa;
      FmReceDesp.CBEmpresa.KeyValue     := Empresa;
    end;
    FmReceDesp.ShowModal;
    FmReceDesp.Destroy;
  end;
end;

procedure TFmPrincipal.DepositoDeCheques(Config: Integer);
begin
  if DBCheck.CriaFm(TFmDeposimp, FmDeposimp, afmoNegarComAviso) then
  begin
    case Config of
      1:
      begin
        FmDeposimp.TPIni.Date := Date;
        FmDeposimp.TPFim.Date := Date;
        FmDeposimp.RGAgrupa.ItemIndex := 0;
        //FmDeposimp.CkQuitado.Checked := True;
        FmDeposimp.RGRepasse.ItemIndex := 0;
        FmDeposimp.RGOrdem1.ItemIndex := 0;
        FmDeposimp.FImprime := True;
      end;
    end;
    FmDeposimp.ShowModal;
    FmDeposimp.Destroy;
  end;
end;

procedure TFmPrincipal.Desativar1Click(Sender: TObject);
(*
var
  Cam: String;
*)
begin
(*
  Cam := Application.Title+'\VCLSkins';
  Geral.WriteAppKeyCU('VCLSkins', Application.Title, False, ktBoolean);
  MeuVCLSkin.VCLSkinDefineUso(FmPrincipal.sd1, FmPrincipal.skinstore1);
*)
end;

procedure TFmPrincipal.Duplicatas1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmRepasDUCad, FmRepasDUCad, afmoNegarComAviso) then
  begin
    FmRepasDUCad.ShowModal;
    FmRepasDUCad.Destroy;
  end;
end;

procedure TFmPrincipal.GerenciaResultados1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmOperaAll, FmOperaAll, afmoNegarComAviso) then
  begin
    FmOperaAll.ShowModal;
    FmOperaAll.Destroy;
  end;
end;

procedure TFmPrincipal.GradePKACellClick(Column: TColumn);
var
  DataHora, DataSai, DataRec, DataRet: TDateTime;
  Saiu, Recebeu, Retornou, Conta: Integer;
begin
  if (Column.FieldName = 'Saiu') and (DModG.QrPKASaiu.Value = 0) then
  begin
    DataHora := Now();
    if Geral.MensagemBox('Confirma a sa�da do protocolo ' + IntToStr(
    DModG.QrPKAConta.Value) + ' em ' + Geral.FDT(DataHora, 8) + #13#10 +
    ' no login de "' + VAR_LOGIN + '"?', 'Pergunta',
    MB_YESNOCANCEL+MB_ICONQUESTION) =
    ID_YES then
    begin
      Saiu := VAR_USUARIO;
      DataSai := DataHora;
      Conta := DModG.QrPKAConta.Value;
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'protpakits', False, [
      'Saiu', 'DataSai'], ['Conta'], [Saiu, DataSai], [Conta], True) then
      DModG.ReopenPTK();
    end;
  end;
  //
  if (Column.FieldName = 'Recebeu') and (DModG.QrPKARecebeu.Value = 0) then
  begin
    if DModG.QrPKASaiu.Value = 0 then
    begin
      Geral.MensagemBox('� necess�rio informar a sa�da antes do recebimento!',
      'Aviso', MB_OK+MB_ICONWARNING);
    end else
    begin
      DataHora := Now();
      if Geral.MensagemBox('Confirma o recebimento do protocolo ' + IntToStr(
      DModG.QrPKAConta.Value) + ' em ' + Geral.FDT(DataHora, 8) + #13#10 +
      ' no login de "' + VAR_LOGIN + '"?', 'Pergunta',
      MB_YESNOCANCEL+MB_ICONQUESTION) =
      ID_YES then
      begin
        Recebeu := VAR_USUARIO;
        DataRec := DataHora;
        Conta := DModG.QrPKAConta.Value;
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'protpakits', False, [
        'Recebeu', 'DataRec'], ['Conta'], [Recebeu, DataRec], [Conta], True) then
        DModG.ReopenPTK();
      end;
    end;
  end;
  //
  if (Column.FieldName = 'Retornou') and (DModG.QrPKARetornou.Value = 0) then
  begin
    if DModG.QrPKARecebeu.Value = 0 then
    begin
      Geral.MensagemBox('� necess�rio informar o recebimento antes do retorno!',
      'Aviso', MB_OK+MB_ICONWARNING);
    end else
    begin
      DataHora := Now();
      if Geral.MensagemBox('Confirma o retorno do protocolo ' + IntToStr(
      DModG.QrPKAConta.Value) + ' em ' + Geral.FDT(DataHora, 8) + #13#10 +
      ' no login de "' + VAR_LOGIN + '"?', 'Pergunta',
      MB_YESNOCANCEL+MB_ICONQUESTION) =
      ID_YES then
      begin
        Retornou := VAR_USUARIO;
        DataRet := DataHora;
        Conta := DModG.QrPKAConta.Value;
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'protpakits', False, [
        'Retornou', 'DataRet'], ['Conta'], [Retornou, DataRet], [Conta], True) then
        begin
          DModG.FPKA_Next := UMyMod.ProximoRegistro(DModG.QrPKA, 'Conta',
            DModG.QrPKAConta.Value);
          DModG.ReopenPTK();
        end;  
      end;
    end;
  end;
  //
end;

procedure TFmPrincipal.GradePKADrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Txt, Bak: TColor;
begin
  if (Column.FieldName = 'NO_RETORNA') then
  begin
    if DModG.QrPKARetorna.Value = 1 then
    begin
      Txt := clWindow;
      Bak := clRed;
    end else begin
      Txt := clWindow;
      Bak := clBlue;
    end;
    MyObjects.DesenhaTextoEmDBGrid(GradePKA, Rect,
      Txt, Bak, Column.Alignment, Column.Field.DisplayText);
  end;
  if Column.FieldName = 'Saiu' then
    MeuVCLSkin.DrawGrid(TDBGrid(GradePKA), Rect, 1, DModG.QrPKASaiu.Value);
  if Column.FieldName = 'Recebeu' then
    MeuVCLSkin.DrawGrid(TDBGrid(GradePKA), Rect, 1, DModG.QrPKARecebeu.Value);
  if Column.FieldName = 'Retornou' then
    MeuVCLSkin.DrawGrid(TDBGrid(GradePKA), Rect, 1, DModG.QrPKARetornou.Value);
end;

procedure TFmPrincipal.GradePPICellClick(Column: TColumn);
var
  DataHora, DataSai, DataRec, DataRet: TDateTime;
  Saiu, Recebeu, Retornou, Conta: Integer;
begin
  if (Column.FieldName = 'Saiu') and (DModG.QrPPISaiu.Value = 0) then
  begin
    DataHora := Now();
    if Geral.MensagemBox('Confirma a sa�da do protocolo ' + IntToStr(
    DModG.QrPPIConta.Value) + ' em ' + Geral.FDT(DataHora, 8) + #13#10 +
    ' no login de "' + VAR_LOGIN + '"?', 'Pergunta',
    MB_YESNOCANCEL+MB_ICONQUESTION) =
    ID_YES then
    begin
      Saiu := VAR_USUARIO;
      DataSai := DataHora;
      Conta := DModG.QrPPIConta.Value;
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'protpakits', False, [
      'Saiu', 'DataSai'], ['Conta'], [Saiu, DataSai], [Conta], True) then
      DModG.ReopenPTK();
    end;
  end;
  //
  if (Column.FieldName = 'Recebeu') and (DModG.QrPPIRecebeu.Value = 0) then
  begin
    if DModG.QrPPISaiu.Value = 0 then
    begin
      Geral.MensagemBox('� necess�rio informar a sa�da antes do recebimento!',
      'Aviso', MB_OK+MB_ICONWARNING);
    end else
    begin
      DataHora := Now();
      if Geral.MensagemBox('Confirma o recebimento do protocolo ' + IntToStr(
      DModG.QrPPIConta.Value) + ' em ' + Geral.FDT(DataHora, 8) + #13#10 +
      ' no login de "' + VAR_LOGIN + '"?', 'Pergunta',
      MB_YESNOCANCEL+MB_ICONQUESTION) =
      ID_YES then
      begin
        Recebeu := VAR_USUARIO;
        DataRec := DataHora;
        Conta := DModG.QrPPIConta.Value;
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'protpakits', False, [
        'Recebeu', 'DataRec'], ['Conta'], [Recebeu, DataRec], [Conta], True) then
        DModG.ReopenPTK();
      end;
    end;
  end;
  //
  if (Column.FieldName = 'Retornou') and (DModG.QrPPIRetornou.Value = 0) then
  begin
    if DModG.QrPPIRecebeu.Value = 0 then
    begin
      Geral.MensagemBox('� necess�rio informar o recebimento antes do retorno!',
      'Aviso', MB_OK+MB_ICONWARNING);
    end else
    begin
      DataHora := Now();
      if Geral.MensagemBox('Confirma o retorno do protocolo ' + IntToStr(
      DModG.QrPPIConta.Value) + ' em ' + Geral.FDT(DataHora, 8) + #13#10 +
      ' no login de "' + VAR_LOGIN + '"?', 'Pergunta',
      MB_YESNOCANCEL+MB_ICONQUESTION) =
      ID_YES then
      begin
        Retornou := VAR_USUARIO;
        DataRet := DataHora;
        Conta := DModG.QrPPIConta.Value;
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'protpakits', False, [
        'Retornou', 'DataRet'], ['Conta'], [Retornou, DataRet], [Conta], True) then
        DModG.ReopenPTK();
      end;
    end;
  end;
  //
end;

procedure TFmPrincipal.GradePPIDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Txt, Bak: TColor;
begin
  if (Column.FieldName = 'NO_RETORNA') then
  begin
    if DModG.QrPPIRetorna.Value = 1 then
    begin
      Txt := clWindow;
      Bak := clRed;
    end else begin
      Txt := clWindow;
      Bak := clBlue;
    end;
    MyObjects.DesenhaTextoEmDBGrid(GradePPI, Rect,
      Txt, Bak, Column.Alignment, Column.Field.DisplayText);
  end;
  if Column.FieldName = 'Saiu' then
    MeuVCLSkin.DrawGrid(TDBGrid(GradePPI), Rect, 1, DModG.QrPPISaiu.Value);
  if Column.FieldName = 'Recebeu' then
    MeuVCLSkin.DrawGrid(TDBGrid(GradePPI), Rect, 1, DModG.QrPPIRecebeu.Value);
  if Column.FieldName = 'Retornou' then
    MeuVCLSkin.DrawGrid(TDBGrid(GradePPI), Rect, 1, DModG.QrPPIRetornou.Value);
end;

procedure TFmPrincipal.AcoesIniciaisDoAplicativo();
begin
  try
    if ZZTerminate then
      Exit;
    //
    Application.ProcessMessages;
    Screen.Cursor := crHourGlass;
    CRD_Consts.AvisaSetarOpcoes;
    AtualizaSP2(0);
    //PertoChekP.VerificaPertochek;
    UMyMod.VerificaFeriadosFuturos(TFmFeriados, FmFeriados);
    if DModG <> nil then
    begin
      DModG.MyPID_DB_Cria();
      DModG.ReopenEmpresas(VAR_USUARIO, 0);
      //
      DMod.MyDB.Execute('UPDATE entidades set cliint=1 WHERE codigo=-11');
      DMod.MyDB.Execute('UPDATE enticliint set codcliint=1 WHERE codenti=-11');
      //
      if not FAtualizouFavoritos then
      begin
        MyObjects.Informa2(LaAviso3, LaAviso4, True, 'Criando favoritos');
        DModG.CriaFavoritos(AdvToolBarPager1, LaAviso3, LaAviso4, AGBAcesRapid, FmPrincipal);
        //
        FAtualizouFavoritos := True;
      end;
      VerificaOrelhas();
      (*
      if DModG.QrCtrlGeralAtualizouEntidades.AsInteger = 0 then
        Entities.AtualizaEntidadesParaEntidade2(PB1, Dmod.MyDB, DModG.AllID_DB);
      *)
      DmkWeb.ConfiguraAlertaWOrdSerApp(TmSuporte, TySuporte, BalloonHint1);
      //
      TmVersao.Enabled := True;
      //
      DmkWeb.HistoricoDeAlteracoes(CO_DMKID_APP, CO_VERSAO, dtMostra);
      //
      UFixBugs.MostraFixBugs(['']);
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TFmPrincipal.CartaoDeFatura: Integer;
begin
  Result := 0;//FmFaturas.QrFaturasCodigo.Value;
end;

procedure TFmPrincipal.Cheques2Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmRepasCHCad, FmRepasCHCad, afmoNegarComAviso) then
  begin
    FmRepasCHCad.ShowModal;
    FmRepasCHCad.Destroy;
  end;
end;

procedure TFmPrincipal.CkPTK_AbertosClick(Sender: TObject);
begin
  DModG.FFiltroPPI := RGProtoFiltro.ItemIndex;
  DModG.ReopenPTK();
end;

function TFmPrincipal.CompensacaoDeFatura: String;
begin
  Result := '';//FormatDateTime(VAR_FORMATDATE, FmFaturas.QrFaturasData.Value);
end;

procedure TFmPrincipal.ContasControladas1Click(Sender: TObject);
begin
  FinanceiroJan.MostraPesquisaContasControladas(0);
end;

procedure TFmPrincipal.ContasSazonais1Click(Sender: TObject);
begin
  FinanceiroJan.MostraContasSazonais;
end;

procedure TFmPrincipal.Contratos1Click(Sender: TObject);
begin
  MostraCartaG(0);
end;

function TFmPrincipal.CriaBAC_Pesq(Banco, Agencia, Conta: String;
  DVCC: Integer): String;
var
  RealCC: String;
begin
  RealCC := Copy(Conta, 10 - DVCC + 1, DVCC);
  Result := Banco + Agencia + RealCC;
end;

procedure TFmPrincipal.CriaCMC7s();
var
  i: Integer;
  Txt: String;
  Dv3, Conta, Dv1, Tipo, Numero, Compe, Dv2, Agencia, Banco, Raz: String;
begin
  Memo2.Lines.Clear;
  for i := 0 to Memo1.Lines.Count - 1 do
  begin
    Txt := Memo1.Lines[i];
    if Length(Txt) > 30 then
    begin
      Compe   := Copy(Txt,014,03);
      Numero  := Copy(Txt,020,06);
      Tipo    := Copy(Txt,029,01);
      Dv1     := Copy(Txt,033,01);
      Banco   := Copy(Txt,037,03);
      Agencia := Copy(Txt,043,04);
      Dv2     := Copy(Txt,050,01);
      Raz     := Copy(Txt,054,03);
      Conta   := Copy(Txt,060,08);
      Dv3     := Copy(Txt,063,01);
      //
      Conta := Raz + Geral.SoNumero_TT(Conta);
      Memo2.Lines.Add(MLAGeral.RetornaCMC7(Banco, Agencia, Conta, Numero,
        Compe, Tipo));
    end;
  end;
end;

procedure TFmPrincipal.CriaFormLot(FormShow, Tipo, Lote, FatParcela: Integer);
begin
  if UnDmkDAC_PF.TabelaExiste(TAB_LOT + 'its', Dmod.MyDB) then
  begin
    Geral.MB_Aviso('A��o cancelada!' + sLineBreak +
    'A migra��o dos itens de border�s n�o foi realizada ou finalizada!');
    //
    Exit;
  end;
  //
  FFormLotShow := FormShow;
  case FormShow of
    {
    0:
    begin
      try
        Application.CreateForm(TFmLot0, FmLot0);
        FmLot0.FTipoLote := Tipo;
        if Codigo > 0 then
        begin
          FmLot0.LocCod(Codigo, Codigo);
          if Controle > 0 then FmLot0.QrLotIts.Locate('Controle', Controle, []);
        end;
        FmLot0.ShowModal;
        FmLot0.Destroy;
        Screen.Cursor := crDefault;
      except
        Screen.Cursor := crDefault;
        raise;
      end;
    end;
    1:
    begin
      try
        Application.CreateForm(TFmLot1, FmLot1);
        FmLot1.FTipoLote := Tipo;
        if Codigo > 0 then
        begin
          FmLot1.LocCod(Codigo, Codigo);
          if Controle > 0 then FmLot1.QrLotIts.Locate('Controle', Controle, []);
        end;
        FmLot1.ShowModal;
        FmLot1.Destroy;
        Screen.Cursor := crDefault;
      except
        Screen.Cursor := crDefault;
        raise;
      end;
    end;
    }
    2:
    begin
      try
        if DBCheck.CriaFm(TFmLot2Cab, FmLot2Cab, afmoNegarComAviso) then
        begin
          FmLot2Cab.FTipoLote := Tipo;
          if Lote > 0 then
          begin
            FmLot2Cab.LocCod(Lote, Lote);
            if FatParcela > 0 then
              FmLot2Cab.QrLotIts.Locate('FatParcela', FatParcela, []);
          end;
          FmLot2Cab.ShowModal;
          FmLot2Cab.Destroy;
        end;
      except
        raise;
      end;
    end;
    else Geral.MensagemBox('Janela de gerenciamento de border�s ' +
      'n�o definida! (01)', 'ERRO', MB_OK+MB_ICONERROR);
  end;
end;

{
procedure TFmPrincipal.CriaCalcPercent(Valor, Porcentagem: String; Calc: TcpCalc );
begin
  Application.CreateForm(TFmCalcPercent, FmCalcPercent);
  with FmCalcPercent do
  begin
    if Calc = cpMulta    then LaPorcent.Caption := '% Multa';
    if Calc = cpJurosMes then LaPorcent.Caption := '% Juros/m�s';
    EdValor.Text := Valor;
    EdPercent.Text := Porcentagem;
    ShowModal;
    Destroy;
  end;
end;
}

procedure TFmPrincipal.Saldodecontas1Click(Sender: TObject);
begin
  FinanceiroJan.SaldoDeContas;
end;

procedure TFmPrincipal.VerificaBDPblicas1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmVerifiDBTerceiros, FmVerifiDBTerceiros, afmoNegarComAviso) then
  begin
    FmVerifiDBTerceiros.ShowModal;
    FmVerifiDBTerceiros.Destroy;
  end;
end;

procedure TFmPrincipal.VerificaBDServidor1Click(Sender: TObject);
begin
  if not VAR_VERIFI_DB_CANCEL then
  begin
    Application.CreateForm(TFmVerifiDB, FmVerifiDB);
    FmVerifiDB.ShowModal;
    FmVerifiDB.Destroy;
  end;
end;

procedure TFmPrincipal.VerificaBDsTerceiros();
var
  i: Integer;
begin
  FConnections := 0;
  for i := 0 to FMyDBs.MaxDBs-1 do
  begin
    //t := IntToStr(i+1);
    if (FMyDBs.BDName[i] <> '') and (FMyDBs.FolderExist[i] = '1') then
    begin
      //MyDBx := TmySQLDatabase(FindComponent('MyDB'+t));
      //FArrayMySQLBD[i] := MyDB1;

      Dmod.FArrayMySQLBD[i].DatabaseName := FMyDBs.BDName[i];
      Dmod.FArrayMySQLBD[i].Host         := Dmod.MyDB.Host;
      Dmod.FArrayMySQLBD[i].UserPassword := Dmod.MyDB.UserPassword;
      Dmod.FArrayMySQLBD[i].Connected    := False;
      try
        Dmod.FArrayMySQLBD[i].Connected := True;
        FMyDBs.Connected[i] := '1';
        FConnections := FConnections +1;
      except
        ;
      end;
      if FMyDBs.Connected[i] = '1' then
      begin
        Application.CreateForm(TFmVerifiDBy, FmVerifiDBy);
        with FmVerifiDBy do
        begin
          FMySQLDB := Dmod.FArrayMySQLBD[i];
          BtSair.Enabled := False;
          FVerifi := True;
          ShowModal;
          FVerifi := False;
          Destroy;
        end;
      end;
    end;
  end;
end;

procedure TFmPrincipal.VerificaBDsTerceiros1Click(Sender: TObject);
begin
  VerificaBDsTerceiros();
end;

procedure TFmPrincipal.VerificaBDWeb1Click(Sender: TObject);
begin
  if Dmod.QrControleWeb_MySQL.Value > 0 then
  begin
    if not VAR_VERIFI_DB_CANCEL then
    begin
      Application.CreateForm(TFmVerifiDBi, FmVerifiDBi);
      FmVerifiDBi.ShowModal;
      FmVerifiDBi.Destroy;
    end;
  end;
end;

function TFmPrincipal.VerificaDiasCompensacao(Praca, CBE: Integer;
  Valor: Double; DtEmiss, DtVence: TDateTime; SBC: Integer): Integer;
var
  V, E, S: Integer;
begin
  // TESTE
  // CBE Ser� calculado nos dias em TUModule.CalculaDias(...
  CBE := 0;
  // FIM TESTE
  V := Trunc(Int(DtVence));
  E := Trunc(Int(DtEmiss));
  if (V <> 0) and (E <> 0) then S := E-V else S := 0;
  if ((CBE > 0) and (S < CBE)) then
  begin
    Result := CBE + S;
    if Result < 0 then Result := 0;
  end else begin
    if SBC = 1 then Result := 0 else
    begin
      if Praca = Dmod.QrControleRegiaoCompe.Value then
        Result := Dmod.QrControleMinhaCompe.Value
      else
        Result := Dmod.QrControleOutraCompe.Value;
      if Valor < DMod.QrControleChequeMaior.Value then Result := Result + 1;
    end;
  end;
end;

function TFmPrincipal.VerificaNovasVersoes(ApenasVerifica: Boolean = False): Boolean;
var
  VersaoCredScan, Versao: Integer;
  ArqNome, IP, ID, PW, BD, Arq: String;
begin
  VersaoCredScan := 0;
  //
  Result := DmkWeb.VerificaAtualizacaoVersao2(True, True, 'Credito2', 'Credito2',
    Geral.SoNumero_TT(DModG.QrMasterCNPJ.Value), CO_VERSAO, CO_DMKID_APP,
    DModG.ObtemAgora(), Memo3, dtExec, Versao, ArqNome, False, ApenasVerifica, BalloonHint1);
  //
  if Dmod.QrControleWeb_MySQL.Value > 0 then
  begin
    IP := Dmod.QrControleWeb_Host.Value;
    ID := Dmod.QrControleWeb_User.Value;
    PW := Dmod.QrControleWeb_Pwd.Value;
    BD := Dmod.QrControleWeb_DB.Value;
    //
    if MyObjects.FIC(IP = '', nil, 'Host n�o definido!') then Exit;
    if MyObjects.FIC(ID = '', nil, 'Usu�rio n�o definido!') then Exit;
    if MyObjects.FIC(PW = '', nil, 'Senha n�o defindia!') then Exit;
    if MyObjects.FIC(BD = '', nil, 'Banco de dados n�o definido!') then Exit;
    //
    Arq := ExtractFilePath(Application.ExeName)+'CredScan2.Exe';
    //
    if FileExists(Arq) then
    begin
      VersaoCredScan := Geral.VersaoInt2006(Geral.FileVerInfo(Arq, 3 (*Versao*)));
      //
      DmkWeb.VerificaAtualizacaoVersao2(True, True, 'CredScan2', 'CredScan2',
        Geral.SoNumero_TT(DModG.QrMasterCNPJ.Value), VersaoCredScan, 25,
        DModG.ObtemAgora(), Memo3, dtExec, VersaoCredScan, ArqNome, False,
        ApenasVerifica, BalloonHint1);
    end;
  end;
end;

procedure TFmPrincipal.VerificaOrelhas;
var
{
  Mostra: Boolean;
  Select: Integer;
}
  MyCursor: TCursor;
begin
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    if DModG.QrCtrlGeralVctAutDefU.Value = 0 then
    begin
      if Int(DModG.ObtemAgora()) > DModG.QrCtrlGeralVctAutLast.Value  then
      begin
        if AtualizaDefinicaoDeFatID_0301() then
          ReopenFatID_0301();
      end else ReopenFatID_0301();
    end else ReopenFatID_0301();
{
    Select := 0;
    //
    MyObjects.Informa2(LaAviso3, LaAviso2, True, 'Verificando provis�es base');
    Mostra := False;
    QrDupl.Close;
    QrDupl. Open;
    if QrDupl.RecordCount > 0 then
    begin
      if QrDuplItens.Value > 1 then
      begin
        Mostra := True;
        PageControl1.ActivePageIndex := 2;
        PageControl2.ActivePageIndex := 0;
        Select := 2;
      end;
    end;
    if Select = 0 then
    begin
      PageControl1.ActivePageIndex := 0;
      PageControl2.ActivePageIndex := 0;
      Select := 3;
    end;
    Panel2.Visible := Mostra;
    //
    MyObjects.Informa2(LaAviso3, LaAviso2, True, 'Verificando protocolos');
    // em caso de logoff
    EdEmpresa.ValueVariant := 0;
    //
    GradePTK.DataSource := DModG.DsPTK;
    GradePPI.DataSource := DModG.DsPPI;
    DModG.ReopenPTK();
    if (DModG.QrPTK.State <> dsInactive)
    and (DModG.QrPTK.RecordCount > 0) then
    begin
      if Select = 0 then
      begin
        PageControl1.ActivePageIndex := 1;
        PageControl3.ActivePageIndex := 1;
        //Select := -1;
      end;
    end;
}
    MyObjects.Informa2(LaAviso3, LaAviso4, False, '');
  finally
    Screen.Cursor := MyCursor;
  end;
end;

procedure TFmPrincipal.TimerIdleTimer(Sender: TObject);
(*
var
  Dia: Integer;
*)
begin
  if DModG <> nil then
    DmodG.ExecutaPing(FmCredito_DMK, [Dmod.MyDB, Dmod.MyDBn, DModG.MyPID_DB, DModG.AllID_DB]);
  (* 2017-05-24 => Foi criada um fun��o que executa no OnIdle para substituir
  TimerIdle.Enabled := False;
  Dia := Geral.ReadAppKeyCU('VeriNetVersao', Application.Title, ktInteger, 0);
  if (Dia > 2) and (Dia < Int(Date)) then
  begin
    if not VerificaNovasVersoes then
      Application.Terminate;
  end else
    Application.Terminate;
  *)
end;

procedure TFmPrincipal.TmSuporteTimer(Sender: TObject);
begin
  {$IFDEF UsaWSuport}
  DmkWeb.AtualizaSolicitApl2(Dmod.QrUpd, Dmod.MyDB, TmSuporte, TySuporte,
    AdvToolBarButton7, BalloonHint1);
  {$ENDIF}
end;

procedure TFmPrincipal.TmVersaoTimer(Sender: TObject);
begin
  TmVersao.Enabled := False;
  //
  if DmkWeb.RemoteConnection then
  begin
    if VerificaNovasVersoes(True) then
      DmkWeb.MostraBalloonHintMenuTopo(AdvToolBarButton8, BalloonHint1,
        'H� uma nova vers�o!', 'Clique aqui para atualizar.');
  end;
end;

procedure TFmPrincipal.UnicaEmpresa1Click(Sender: TObject);
begin
  if VAR_LIB_EMPRESAS = '' then
    VAR_LIB_EMPRESAS := '-100000000';
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  //
  if not PreparaPesquisaFinanceira(CO_TabLctA) then
    Exit;
  //
  FinanceiroJan.ExtratosFinanceirosEmpresaUnica;
end;

procedure TFmPrincipal.BtMenuClick(Sender: TObject);
begin
{### buscar do Cred itor PMMenu}
end;

procedure TFmPrincipal.BitBtn17Click(Sender: TObject);
var
  DataHora, DataSai: TDateTime;
  Saiu, Conta: Integer;
begin
  DataHora := Now();
  if Geral.MensagemBox('Confirma a sa�da de todos protocolos do lote ' + IntToStr(
  DModG.QrPTKLote.Value) + ' em ' + Geral.FDT(DataHora, 8) + #13#10 +
  ' no login de "' + VAR_LOGIN + '"?', 'Pergunta',
  MB_YESNOCANCEL+MB_ICONQUESTION) =
  ID_YES then
  begin
    Saiu := VAR_USUARIO;
    DataSai := DataHora;
    DModG.QrPPI.First;
    while not DModG.QrPPI.Eof do
    begin
      if DModG.QrPPISaiu.Value = 0 then
      begin
        Conta := DModG.QrPPIConta.Value;
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'protpakits', False, [
        'Saiu', 'DataSai'], ['Conta'], [Saiu, DataSai], [Conta], True);
      end;
      DModG.QrPPI.Next;
      //
    end;
    DModG.ReopenPTK();
  end;
end;

procedure TFmPrincipal.BitBtn18Click(Sender: TObject);
var
  DataHora,DataRec: TDateTime;
  Recebeu, Conta: Integer;
begin
  DataHora := Now();
  if Geral.MensagemBox('Confirma o recebimento de todos protocolos do lote ' + IntToStr(
  DModG.QrPTKLote.Value) + ' em ' + Geral.FDT(DataHora, 8) + #13#10 +
  ' no login de "' + VAR_LOGIN + '"?', 'Pergunta',
  MB_YESNOCANCEL+MB_ICONQUESTION) =
  ID_YES then
  begin
    Recebeu := VAR_USUARIO;
    DataRec := DataHora;
    DModG.QrPPI.First;
    while not DModG.QrPPI.Eof do
    begin
      if (DModG.QrPPISaiu.Value <> 0) and (DModG.QrPPIRecebeu.Value = 0) then
      begin
        Conta := DModG.QrPPIConta.Value;
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'protpakits', False, [
        'Recebeu', 'DataRec'], ['Conta'], [Recebeu, DataRec], [Conta], True);
      end;
      DModG.QrPPI.Next;
      //
    end;
    DModG.ReopenPTK();
  end;
end;

procedure TFmPrincipal.BitBtn19Click(Sender: TObject);
var
  DataHora,DataRet: TDateTime;
  Retornou, Conta: Integer;
begin
  DataHora := Now();
  if Geral.MensagemBox('Confirma o recebimento de todos protocolos do lote ' + IntToStr(
  DModG.QrPTKLote.Value) + ' em ' + Geral.FDT(DataHora, 8) + #13#10 +
  ' no login de "' + VAR_LOGIN + '"?', 'Pergunta',
  MB_YESNOCANCEL+MB_ICONQUESTION) =
  ID_YES then
  begin
    Retornou := VAR_USUARIO;
    DataRet := DataHora;
    DModG.QrPPI.First;
    while not DModG.QrPPI.Eof do
    begin
      if (DModG.QrPPIRecebeu.Value <> 0) and (DModG.QrPPIRetorna.Value = 1)
      and (DModG.QrPPIRetornou.Value = 0) then
      begin
        Conta := DModG.QrPPIConta.Value;
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'protpakits', False, [
        'Retornou', 'DataRet'], ['Conta'], [Retornou, DataRet], [Conta], True);
      end;
      DModG.QrPPI.Next;
      //
    end;
    DModG.ReopenPTK();
  end;
end;

procedure TFmPrincipal.BitBtn1Click(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
  'UPDATE ' + CO_TabLctA,
  'SET CliInt = -11 ',
  '']);
  //
  Screen.Cursor := crHourGlass;
  UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
  'UPDATE Carteiras ',
  'SET ForneceI = -11 ',
  '']);
  //
  Screen.Cursor := crHourGlass;
  UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
  'DELETE FROM Carteiras ',
  'WHERE Codigo < -1 ',
  '']);
  //
  Geral.MensagemBox('A��o realizada com sucesso!' + #13#10 +
  'Execute a verifica��o do banco de dados do servidor!',
  'Aviso', MB_OK+MB_ICONWARNING);
  //
  Screen.Cursor := crDefault;
end;

procedure TFmPrincipal.BitBtn2Click(Sender: TObject);
begin
  if AtualizaDefinicaoDeFatID_0301() then
    ReopenFatID_0301();
end;

procedure TFmPrincipal.Bruto2Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmOperaCol, FmOperaCol, afmoNegarComAviso) then
  begin
    FmOperaCol.ShowModal;
    FmOperaCol.Destroy;
  end;
end;

procedure TFmPrincipal.BtReceDespClick(Sender: TObject);
begin
  DemonstrativoDeReceitasEDespesas(0);
end;

procedure TFmPrincipal.AGBAcesRapidClick(Sender: TObject);
begin
  CriaFormLot(2, 0, 0, 0);
end;

procedure TFmPrincipal.BtBalanceteClick(Sender: TObject);
begin
  BalanceteConfiguravel(CO_TabLctA);
end;

procedure TFmPrincipal.AGBDecompClick(Sender: TObject);
begin
  Application.CreateForm(TFmDecomp, FmDecomp);
  FmDecomp.ShowModal;
  FmDecomp.Destroy;
end;

procedure TFmPrincipal.AGBDepositoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMDeposito, AGBDeposito);
end;

procedure TFmPrincipal.BtComandoClick(Sender: TObject);
var
  Retorno: String;
begin
  EdResposta.Text := '';
  MeResposta.Text := '';
  EdResposta.Text := PertoChekP.EnviaPertoChek2(EdComando.Text, Retorno);
  MeResposta.Text := Retorno;
end;

procedure TFmPrincipal.BtConfAltClick(Sender: TObject);
var
  i, x: Integer;
  CMC7: String;
begin
  //if AvisaErroCMC7 then Exit;
  CMC7 := Geral.SoNumero_TT(EdBanda.Text);
  x := 0;
  for i := 1 to GradeC.RowCount - 1 do
  begin
    if GradeC.Cells[11, i] = CMC7 then
    begin
      x := i;
      Break;
    end;
  end;
  if GradeC.Cells[11, x] = CMC7 then
  begin
    GradeC.Cells[09, x] := EdCPF.Text;
    GradeC.Cells[10, x] := EdEmitente.Text;
    DmLot2.AtualizaEmitente(GradeC.Cells[2, i], GradeC.Cells[3, i],
      GradeC.Cells[4, i], Geral.SoNumero_TT(EdCPF.Text), EdEmitente.Text, -1);
    //
    EdBanda.Text := '';
    EdCPF.Text := '';
    EdEmitente.Text := '';
  end else Geral.MensagemBox('CMC7 n�o localizado!', 'Aviso',
    MB_OK+MB_ICONWARNING);
  EdBanda.SetFocus;
end;

procedure TFmPrincipal.BtImportaClick(Sender: TObject);
var
  Arquivo: String;
begin
  if MyObjects.FileOpenDialog(self, '', '', 'Importa��o de cheques e duplicatas',
  'Arquivo texto (*.TXT)|*.txt', [], Arquivo) then
  begin
    Screen.Cursor := crHourGlass;
    Application.ProcessMessages;
    MyObjects.LimpaGrade(GradeC, 1, 1, True);
    Application.ProcessMessages;
    Geral.LeArquivoToMemo(Arquivo, Memo1);
    CriaCMC7s;
    LeCMC7_Criados;
    ProcuraBACs;
    TPCheque.Date := Geral.ValidaDataSimples(GradeC.Cells[07, 1], True) +
      Geral.IMV( GradeC.Cells[08, 1]);
    if TPCheque.Date < 1000 then TPCheque.Date := Date;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPrincipal.AGBExcluiItensLoteClick(Sender: TObject);
var
  Num: String;
  Cod, ITS: Integer;
begin
  Num := InputBox('Exclus�o for�ada de itens de lote.','Digite o n�mero do lote.', '' );
  QrLoc.SQL.Add('AND la.Cliente=en.Codigo');
  if Num = '' then Exit
  else begin
    Cod := Geral.IMV(Num);
    QrLoc.Close;
    QrLoc.SQL.Clear;
    QrLoc.SQL.Add('SELECT COUNT(*) ITENS');
    QrLoc.SQL.Add('FROM ' + CO_TabLctA);
    QrLoc.SQL.Add('WHERE FatID=' + Geral.FF0(VAR_FATID_0301));
    QrLoc.SQL.Add('AND FatNum=' + Geral.FF0(Cod));
    UMyMod.AbreQuery(QrLoc, Dmod.MyDB);
    ITS := QrLocITENS.Value;
    //
    QrLoc.Close;
    QrLoc.SQL.Clear;
    QrLoc.SQL.Add('SELECT COUNT(Codigo) ITENS');
    QrLoc.SQL.Add('FROM ' + CO_TabLotA);
    QrLoc.SQL.Add('WHERE Codigo=' + Geral.FF0(Cod));
    UMyMod.AbreQuery(QrLoc, Dmod.MyDB);
    //
    if Geral.MensagemBox('Confirma a exclus�o dos ' +
    IntToStr(ITS) + ' itens do lote ' + IntToStr(Cod) + '?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES
    then begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM ' + CO_TabLctA);
      Dmod.QrUpd.SQL.Add('WHERE FatID=' + Geral.FF0(VAR_FATID_0301));
      Dmod.QrUpd.SQL.Add('AND FatNum=' + Geral.FF0(Cod));
      //Dmod.QrUpd.Params[0].AsInteger := Cod;
      //
      Dmod.QrUpd.ExecSQL;
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM ' + CO_TabLotA + ' WHERE Codigo=:P0');
      Dmod.QrUpd.Params[0].AsInteger := Cod;
      Dmod.QrUpd.ExecSQL;
    end;
  end;
end;

procedure TFmPrincipal.AGBExpContabClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmExpContab1, FmExpContab1, afmoNegarComAviso) then
  begin
    FmExpContab1.ShowModal;
    FmExpContab1.Destroy;
  end;
end;

procedure TFmPrincipal.AGBExpContabExpClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmExpContabExp, FmExpContabExp, afmoNegarComAviso) then
  begin
    FmExpContabExp.ShowModal;
    FmExpContabExp.Destroy;
  end;
end;

procedure TFmPrincipal.AGBGerCondClick(Sender: TObject);
begin
  DepositoDeCheques(1);
end;

procedure TFmPrincipal.BtNegritoClick(Sender: TObject);
var
  Retorno: String;
begin
  EdResposta.Text := PertoChekP.EnviaPertoChek2(',1000', Retorno);
  MeResposta.Text := Retorno;
end;

procedure TFmPrincipal.BtNormalClick(Sender: TObject);
var
  Retorno: String;
begin
  EdResposta.Text := PertoChekP.EnviaPertoChek2(',0000', Retorno);
  MeResposta.Text := Retorno;
end;

procedure TFmPrincipal.BtPesquisaClick(Sender: TObject);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qr301Rel, Dmod.MyDB, [
  'SELECT lct.Cliente, lct.Banco, lct.Agencia, ',
  'lct.ContaCorrente, lct.Documento, lct.Emitente, ',
  'lct.Vencimento, lct.Credito, lct.FatNum, lct.FatParcela, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_CLI ',
  'FROM ' + CO_TabLctA + ' lct ',
  'LEFT JOIN entidades ent ON ent.Codigo=lct.Cliente ',
  'WHERE lct.FatID=' + TXT_VAR_FATID_0301,
  'AND lct.FatSit=20 ',
  'AND lct.FatSitSub=40 ',
  'ORDER BY lct.Vencimento, NO_CLI, Banco, Agencia, ',
  'ContaCorrente, Documento ',
  '']);
  //
  MyObjects.frxMostra(frxPrincipal_301Rel, 'Cheques dispon�veis');
end;

procedure TFmPrincipal.AGBPesqPagClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGerChqMain, FmGerChqMain, afmoNegarComAviso) then
  begin
    FmGerChqMain.ShowModal;
    FmGerChqMain.Destroy;
  end;
end;

procedure TFmPrincipal.AGBPlanRelCabClick(Sender: TObject);
begin
  FinanceiroJan.MostraPlanRelCab;
end;

procedure TFmPrincipal.BtRefreshClick(Sender: TObject);
begin
  VerificaOrelhas();
end;

procedure TFmPrincipal.BtRefrPrtoClick(Sender: TObject);
var
  Retorno: String;
begin
  PertoChekP.DesabilitaPertoCheck;
  PertoChekP.HabilitaPertoChek(Retorno);
  if Retorno <> '' then
    Geral.MensagemBox(Retorno, 'Aviso', MB_OK+MB_ICONWARNING);
end;

procedure TFmPrincipal.Button1Click(Sender: TObject);
begin
  DmLot.ReopenSacCHDevA();
  DmLot.ReopenSacDOpen();
  DmLot.ReopenSacRiscoC();
  DmLot.RiscoSacado('08311918000163');
  DmLot.RiscoSacado('17274516949');
end;

procedure TFmPrincipal.Button2Click(Sender: TObject);
var
  Casas: Integer;
  Valor: Double;
begin
  Casas := Ed10.ValueVariant;
  dmkEdit8.DecimalSize := Casas;
  Geral.MontaValDblPorFmt(Ed16.Text,  Casas, Valor);
  dmkEdit8.ValueVariant := Valor;
end;

procedure TFmPrincipal.Button3Click(Sender: TObject);
begin
  dmkEdit6.Text := Geral.ReduzNomeDeCaminhoDeArquivo(dmkEdit5.Text,
    dmkEdit7.ValueVariant);
end;

procedure TFmPrincipal.Button5Click(Sender: TObject);
begin
  ShowMessage(Format('%3.3d', [1]));
end;

procedure TFmPrincipal.Button8Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmLot2DupMu1, FmLot2DupMu1, afmoNegarComAviso) then
  begin
    FmLot2DupMu1.ShowModal;
    FmLot2DupMu1.Destroy;
  end;
end;

procedure TFmPrincipal.AtualizaAcumulado(PB1: TProgressBar);
var
  Disponiv: Double;
  Visivel: Boolean;
begin
  Visivel := True;
  DmLot2.QrValAcum.Close;
  UMyMod.AbreQuery(DmLot2.QrValAcum, Dmod.MyLocDatabase);
  Dmod.QrUpdL.SQL.Clear;
  Dmod.QrUpdL.SQL.Add('UPDATE importlote SET ValAcum=:P0, Disponiv=:P1');
  Dmod.QrUpdL.SQL.Add('WHERE CPF=:Px');
  Dmod.QrUpdL.SQL.Add('');
  Dmod.QrUpdL.SQL.Add('');
  //
  if PB1 <> nil then
  begin
    Visivel := PB1.Visible;
    PB1.Visible := True;
    PB1.Position := 0;
    PB1.Max := DmLot2.QrValAcum.RecordCount;
    PB1.Refresh;
    PB1.Update;
  end;
  Application.ProcessMessages;
  while not DmLot2.QrValAcum.Eof do
  begin
    if PB1 <> nil then
    begin
      PB1.Position := PB1.Position + 1;
      PB1.Update;
    end;
    Application.ProcessMessages;
    Disponiv := (DmLot2.QrValAcumRISCOSA.Value + DmLot2.QrValAcumRISCOEM.Value) -
      DmLot2.QrValAcumAberto.Value - DmLot2.QrValAcumValor.Value;
    Dmod.QrUpdL.Params[0].AsFloat   := DmLot2.QrValAcumValor.Value;
    Dmod.QrUpdL.Params[1].AsFloat   := Disponiv;
    Dmod.QrUpdL.Params[2].AsString  := DmLot2.QrValAcumCPF.Value;
    Dmod.QrUpdL.ExecSQL;
    DmLot2.QrValAcum.Next;
  end;
  if PB1 <> nil then
    PB1.Visible := Visivel;
end;

procedure TFmPrincipal.AtualizaClientesLotIts();
begin
begin
  Screen.Cursor := crHourGlass;
  UMyMod.TextMySQLQuery1(QrUpdCli1, [
  'UPDATE ' + CO_TabLctA + ' loi, ' + CO_TabLotA + ' lot',
  'SET loi.Cliente=lot.Cliente',
  'WHERE loi.FatID=' + Geral.FF0(VAR_FATID_0301),
  'AND lot.Codigo=loi.FatNum',
  'AND loi.TpAlin <> -9',
  'AND loi.FatNum > 0']);
  QrUpdCli1.ExecSQL;
  //
  UMyMod.TextMySQLQuery1(QrUpdCli2, [
  'UPDATE ' + CO_TabLctA + ' loi, ' + CO_TabLotA + ' lot',
  'SET loi.Cliente=-lot.Cliente',
  'WHERE loi.FatID=' + Geral.FF0(VAR_FATID_0301),
  'AND lot.Codigo=loi.FatNum',
  'AND loi.TpAlin <> -9',
  'AND loi.FatNum < 0']);
  QrUpdCli2.ExecSQL;
  //
  Screen.Cursor := crDefault;
  Geral.MensagemBox('Atualiza��o concluida!', 'Aviso', MB_OK+MB_ICONINFORMATION);
end;

end;

procedure TFmPrincipal.AtualizaCreditado(Banco, Agencia, Conta, CPF,
  Nome: String; Cliente, LastPaymt: Integer);
var
  Registros: Integer;
begin
  case FmPrincipal.FFormLotShow of
    //0: FmLot0.ReopenPagtos;
    //1: FmLot1.ReopenPagtos;
    2: FmLot2Cab.ReopenPagtos;
    else Geral.MensagemBox('Janela de gerenciamento de border�s ' +
    'n�o definida! (17)', 'ERRO', MB_OK+MB_ICONERROR);
  end;
  //
  if (Trim(Banco) = '')
  or (Trim(Agencia) = '')
  or (Trim(Conta) = '') then Exit;
  //
  QrCreditado.Close;
  QrCreditado.Params[0].AsString := Banco+Agencia+Conta;
  UMyMod.AbreQuery(QrCreditado, Dmod.MyDB);
  //
  CPF := Geral.SoNumero_TT(CPF);
  if QrCreditado.RecordCount > 1 then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM creditados WHERE BAC=:P0');
    Dmod.QrUpd.Params[0].AsString := Banco+Agencia+Conta;
    Dmod.QrUpd.ExecSQL;
    //
    Registros := 0;
  end else begin
    Registros := QrCreditado.RecordCount;
  end;
  Dmod.QrUpd.SQL.Clear;
  if Registros = 0 then
    Dmod.QrUpd.SQL.Add('INSERT INTO creditados SET ')
  else
    Dmod.QrUpd.SQL.Add('UPDATE creditados SET ');
  Dmod.QrUpd.SQL.Add('CPF=:P0, Nome=:P1, Banco=:P2, Agencia=:P3, ');
  Dmod.QrUpd.SQL.Add('Conta=:P4');
  if Registros = 0 then
    Dmod.QrUpd.SQL.Add(', BAC=:Pa')
  else
    Dmod.QrUpd.SQL.Add('WHERE BAC=:Pa');
  Dmod.QrUpd.Params[0].AsString  := CPF;
  Dmod.QrUpd.Params[1].AsString  := Nome;
  Dmod.QrUpd.Params[2].AsInteger := Geral.IMV(Banco);
  Dmod.QrUpd.Params[3].AsInteger := Geral.IMV(Agencia);
  Dmod.QrUpd.Params[4].AsString  := Conta;
  //
  Dmod.QrUpd.Params[5].AsString  := Banco+Agencia+Conta;
  Dmod.QrUpd.ExecSQL;

  //

  if LastPaymt > 0 then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO creditadosits SET ');
    Dmod.QrUpd.SQL.Add('BAC=:P0, Cliente=:P1, LastPaymt=:P2');
    Dmod.QrUpd.Params[0].AsString  := Banco+Agencia+Conta;
    Dmod.QrUpd.Params[1].AsInteger := Cliente;
    Dmod.QrUpd.Params[2].AsInteger := LastPaymt;
    Dmod.QrUpd.ExecSQL;
  end;
end;

function TFmPrincipal.AtualizaDefinicaoDeFatID_0301(): Boolean;
begin
  //Result := False;
  Screen.Cursor := crHourGlass;
  try
{
    // Cheques aptos!
    // Deve ser primeiro?
    MyObjects.Informa2(LaAviso5, LaAviso6, True,
      'Movendo cheques de terceiros repassados a clientes');
    UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
    'UPDATE ' + CO_TabLctA,
    'SET Carteira = -7, ',
    'Tipo = 0, Sit = 3, ',
    'Compensado = Vencimento ',
    'WHERE RepCli > 0 ',
    '']);
    //
    // Cheques devolvidos!
    MyObjects.Informa2(LaAviso5, LaAviso6, True,
      'Movendo cheques Devolvidos');
    UnDmkDAC_PF.AbreMySQLQuery0(QrPsqLct, Dmod.MyDB, [
    'SELECT lct.Data, lct.Tipo, lct.Carteira, lct.Controle, ',
    'lct.Sub, lct.FatID, lct.FatNum, lct.FatParcela, ',
    'lct.Compensado, lct.Vencimento, lct.Sit' ,
    'FROM ' + CO_TabLctA + ' lct ',
    'LEFT JOIN ' + CO_TabLotA + ' lot ON lot.Codigo=lct.FatNum ',
    'LEFT JOIN alinits   ai ON ai.ChequeOrigem=lct.FatParcela ',
    'WHERE lct.FatID=' + TXT_VAR_FATID_0301,
    'AND lot.Tipo=0 ',
    'AND lct.Carteira<>-6 ',
    'AND ( ',
    ' (lct.Devolucao<>0 AND ai.Status in (0,1)) AND lct.Quitado>-2 ',
    ') ',
    'ORDER BY lct.Vencimento, lct.DDeposito ',
    '']);
    PB1.Visible := True;
    PB1.Position := 0;
    PB1.Max := QrPsqLct.RecordCount;
    QrPsqLct.First;
    while not QrPsqLct.Eof do
    begin
      PB1.Position := PB1.Position + 1;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_TabLctA, False, [
      'Carteira', 'Tipo', 'Sit', 'Compensado', 'Teste'], [
      'Data', 'Tipo', 'Carteira',
      'Controle', 'Sub', 'FatID',
      'FatNum', 'FatParcela'], [
      -6, 2, 0, '0000-00-00', 1], [
      Geral.FDT(QrPsqLctData.Value, 1), QrPsqLctTipo.Value, QrPsqLctCarteira.Value,
      QrPsqLctControle.Value, QrPsqLctSub.Value, QrPsqLctFatID.Value,
      QrPsqLctFatNum.Value, QrPsqLctFatParcela.Value], False);
      //
      QrPsqLct.Next;
    end;
    PB1.Position := 0;
    //
}
    MyObjects.Informa2(LaAviso3, LaAviso4, True,
    'Preparando defini��o autom�tica de itens de border�s');
    // Limpar campo FatSit e FatSitSub
    UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
    'UPDATE ' + CO_TabLctA,
    'SET FatSit=0, FatSitSub=0 ',
    'WHERE FatID=' + TXT_VAR_FATID_0301,
    '']);
    //

    ////             C H E Q U E S

    // Cheques Devolvidos
    MyObjects.Informa2(LaAviso3, LaAviso4, True,
    'Definindo cheques devolvidos de border�s');
    UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
    'UPDATE ' + CO_TabLctA + ' lct, ' + CO_TabLotA + ' lot, alinits ali ',
    'SET lct.FatSit=-10, lct.FatSitSub=-10, lct.Carteira=-6, lct.Tipo=2',
    // perigoso???
    ', lct.Sit=6, lct.Compensado="0000-00-00" ',
    'WHERE lot.Codigo=lct.FatNum ',
    'AND ali.Codigo=lct.Devolucao ',
    'AND lct.FatID=' + TXT_VAR_FATID_0301,
    'AND lot.Tipo=0 ',
    'AND ( ',
    ' (lct.Devolucao<>0 AND ali.Status in (0,1)) AND lct.Quitado>-2 ',
    ') ',
    '']);
    //

    // Cheques A expirar (Abertos)
    MyObjects.Informa2(LaAviso3, LaAviso4, True,
    'Definindo cheques abertos (a vencer) de border�s');
    UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
    'UPDATE ' + CO_TabLctA + ' lct, ' + CO_TabLotA + ' lot ',
    'SET lct.FatSit=10, lct.FatSitSub=10, lct.Carteira=-5, lct.Tipo=2',
    // perigoso???
    ', lct.Sit=0, lct.Compensado="0000-00-00" ',
    'WHERE lot.Codigo=lct.FatNum ',
    'AND lct.FatID=' + TXT_VAR_FATID_0301,
    'AND lot.Tipo=0 ',
    'AND ( ',
    ' (lct.Devolucao=0 AND lct.DDeposito>=SYSDATE()) AND lct.Quitado>-2 ',
    ')',
    '']);
    //

    // Cheques liquidados...
    //
    // ... com liquidez
    MyObjects.Informa2(LaAviso3, LaAviso4, True,
    'Definindo cheques l�quidos de border�s');
    UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
    'UPDATE ' + CO_TabLctA + ' lct, ' + CO_TabLotA + ' lot ',
    'SET lct.FatSit=20, lct.FatSitSub=40, lct.Carteira=-7, lct.Tipo=0',
    ', lct.Sit=3, lct.Compensado=lct.DDeposito ',
    'WHERE lot.Codigo=lct.FatNum ',
    'AND lct.FatID=' + TXT_VAR_FATID_0301,
    'AND lot.Tipo=0 ',
    'AND lct.RepCli=0 ',
    'AND lct.Depositado=' + Geral.FF0(CO_CH_DEPOSITADO_NAO),
    'AND ( ',
    ' (lct.Devolucao=0 AND lct.DDeposito<SYSDATE()) AND lct.Quitado>-2 ',
    ')' ,
    '']);
    // ... repassados a clientes
    MyObjects.Informa2(LaAviso3, LaAviso4, True,
    'Definindo cheques de border�s repassados a clientes');
    UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
    'UPDATE ' + CO_TabLctA + ' lct, ' + CO_TabLotA + ' lot ',
    'SET lct.FatSit=20, lct.FatSitSub=30, lct.Carteira=-9, lct.Tipo=0', // n�o pode ser Tipo=2 se n�o d� erro nos saldos de contas!!
    ', lct.Sit=2, lct.Compensado=lct.DDeposito ',
    'WHERE lot.Codigo=lct.FatNum ',
    'AND lct.FatID=' + TXT_VAR_FATID_0301,
    'AND lot.Tipo=0 ',
    'AND lct.RepCli<>0 ',
    'AND ( ',
    ' (lct.Devolucao=0 AND lct.DDeposito<SYSDATE()) AND lct.Quitado>-2 ',
    ')' ,
    '']);
    // ... depositados em conta corrente pr�pria
    MyObjects.Informa2(LaAviso3, LaAviso4, True,
    'Definindo cheques de border�s depositados em banco');
    UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
    'UPDATE ' + CO_TabLctA + ' lct, ' + CO_TabLotA + ' lot ',
    'SET lct.FatSit=20, lct.FatSitSub=20, lct.Carteira=-10, lct.Tipo=2',
    ', lct.Sit=2, lct.Compensado=lct.DDeposito ',
    'WHERE lot.Codigo=lct.FatNum ',
    'AND lct.FatID=' + TXT_VAR_FATID_0301,
    'AND lot.Tipo=0 ',
    'AND lct.Depositado<>' + Geral.FF0(CO_CH_DEPOSITADO_NAO),
    'AND ( ',
    ' (lct.Devolucao=0 AND lct.DDeposito<SYSDATE()) AND lct.Quitado>-2 ',
    ')' ,
    '']);
    //
    // Fim cheques liquidados
    //
    // Cheques Pagos / quitados
    MyObjects.Informa2(LaAviso3, LaAviso4, True,
    'Definindo quita��o de cheques de border�s');
    UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
    'UPDATE ' + CO_TabLctA + ' lct, ' + CO_TabLotA + ' lot, alinits ali ',
    // pode se Tipo=0 porque h� pagamento atrelado!!!
    'SET lct.FatSit=30, lct.FatSitSub=50, lct.Carteira=-8, lct.Tipo=2, lct.Sit=2 ',
    'WHERE lot.Codigo=lct.FatNum ',
    'AND ali.Codigo=lct.Devolucao ',
    'AND lct.FatID=' + TXT_VAR_FATID_0301,
    'AND lot.Tipo=0 ',
    'AND lct.FatSit=0 ',
    'AND ali.Data4 > 2 ',
    '']);
    //

    ////             D U P L I C A T A S

    // Duplicatas em aberto
    MyObjects.Informa2(LaAviso3, LaAviso4, True,
    'Definindo duplicatas em aberto de border�s');
    UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
    'UPDATE ' + CO_TabLctA + ' lct, ' + CO_TabLotA + ' lot',
    'SET lct.FatSit=10, lct.FatSitSub=10, lct.Carteira=-12, ',
    'lct.Tipo=2, lct.Sit=0 ',
    'WHERE lot.Codigo=lct.FatNum ',
    'AND lct.FatID=' + TXT_VAR_FATID_0301,
    'AND lot.Tipo=1 ',
    'AND ((lct.Quitado<2) AND (lct.DDeposito >= SYSDATE())) ',
    '']);
    //
    // Duplicatas Vencidas
    MyObjects.Informa2(LaAviso3, LaAviso4, True,
    'Definindo duplicatas vencidas de border�s');
    UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
    'UPDATE ' + CO_TabLctA + ' lct, ' + CO_TabLotA + ' lot',
    'SET lct.FatSit=-10, lct.FatSitSub=-10, lct.Carteira=-11, ',
                           // Perigoso?
    'lct.Tipo=2, lct.Sit=0, lct.Compensado="0000-00-00" ',
    'WHERE lot.Codigo=lct.FatNum ',
    'AND lct.FatID=' + TXT_VAR_FATID_0301,
    'AND lot.Tipo=1 ',
    'AND ((lct.Quitado< 2)AND (lct.DDeposito< SYSDATE())) ',
    '']);
    //
    // Duplicatas pagas em Dia
    MyObjects.Informa2(LaAviso3, LaAviso4, True,
    'Definindo duplicatas pagas em dia de border�s');
    UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
    'UPDATE ' + CO_TabLctA + ' lct, ' + CO_TabLotA + ' lot',
    'SET lct.FatSit=30, lct.FatSitSub=50, lct.Carteira=-13, ',
    'lct.Tipo=2, lct.Sit=2',
    'WHERE lot.Codigo=lct.FatNum ',
    'AND lct.FatID=' + TXT_VAR_FATID_0301,
    'AND lot.Tipo=1 ',
    'AND ((lct.Quitado>1) AND (lct.DDeposito>=lct.Data3)) ',
    '']);
    //
    // Duplicatas pagas com atraso
    MyObjects.Informa2(LaAviso3, LaAviso4, True,
    'Definindo duplicatas pagas com atraso de border�s');
    UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
    'UPDATE ' + CO_TabLctA + ' lct, ' + CO_TabLotA + ' lot',
    'SET lct.FatSit=30, lct.FatSitSub=50, lct.Carteira=-14, ',
    'lct.Tipo=2, lct.Sit=2',
    'WHERE lot.Codigo=lct.FatNum ',
    'AND lct.FatID=' + TXT_VAR_FATID_0301,
    'AND lot.Tipo=1 ',
    'AND (/*(lct.Quitado> 1)AND*/ (lct.DDeposito< lct.Data3)) ',
    '']);
    //

{

UNION

SELECT -1 Sit, -1 Sub,
SUM(IF((lct.Quitado< 2)AND (lct.DDeposito< SYSDATE()),
  lct.Credito - (lct.TotalPg - lct.TotalJr) + lct.TotalDs, 0)) Valor
FROM lct 0001a lct
LEFT JOIN lot 0001a lot ON lot.Codigo=lct.FatNum
WHERE lct.FatID=301
AND lot.Tipo=1
}

    //
    UMyMod.ExecutaMySQLQuery1(Dmod.QrUpdM, [
    'UPDATE ctrlgeral SET VctAutLast="' + Geral.FDT(DmodG.ObtemAgora(), 109) + '"',
    '']);
    //
    MyObjects.Informa2(LaAviso3, LaAviso4, False, '');
    //
    Result := True;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPrincipal.AtualizaLastEditLote(Data: String);
begin
// Atualiza LastEditLote em controle para orientar usuario na atualiza��o
// do Evolucapi
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE controle SET LastEditLote=:P0 ');
  Dmod.QrUpd.SQL.Add('WHERE LastEditLote > :P0 ');
  Dmod.QrUpd.Params[0].AsString := Data;
  Dmod.QrUpd.Params[1].AsString := Data;
  Dmod.QrUpd.ExecSQL;
end;

procedure TFmPrincipal.AtualizaSacado(CNPJ, Nome, Rua: String; Numero: Integer;
  Compl, Bairro, Cidade, UF, CEP, Tel1, IE, Email: String; Risco: Double;
  Importando: Boolean);
var
  Registros: Integer;
begin
  CNPJ := Geral.SoNumero_TT(CNPJ);
  QrSacado.Close;
  QrSacado.Params[0].AsString := CNPJ;
  UMyMod.AbreQuery(QrSacado, Dmod.MyDB);
  //
  if Risco = -1 then Risco := QrSacadoRisco.Value;
  if QrSacado.RecordCount > 1 then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM sacados WHERE CNPJ=:P0');
    Dmod.QrUpd.Params[0].AsString := CNPJ;
    Dmod.QrUpd.ExecSQL;
    //
    Registros := 0;
  end else begin
    Registros := QrSacado.RecordCount;
  end;
  if (Risco < 0.01) and (Registros = 0 ) then
    Risco := Dmod.QrControleDURisco.Value;
  //
  Dmod.QrUpd.SQL.Clear;
  if Registros = 0 then
    Dmod.QrUpd.SQL.Add('INSERT INTO sacados SET ')
  else
    Dmod.QrUpd.SQL.Add('UPDATE sacados SET ');
  Dmod.QrUpd.SQL.Add('Nome=:P0, Rua=:P1, Numero=:P2, Compl=:P3, Bairro=:P4, ');
  Dmod.QrUpd.SQL.Add('Cidade=:P5, UF=:P6, CEP=:P7, Tel1=:P8, Risco=:P9, ');
  Dmod.QrUpd.SQL.Add('IE=:P10, Email=:P11 ');
  if Registros = 0 then
    Dmod.QrUpd.SQL.Add(', CNPJ=:Pa')
  else
    Dmod.QrUpd.SQL.Add('WHERE CNPJ=:Pa');
  //
  Dmod.QrUpd.Params[00].AsString  := Nome;
  Dmod.QrUpd.Params[01].AsString  := Rua;
  Dmod.QrUpd.Params[02].AsInteger := Numero;
  Dmod.QrUpd.Params[03].AsString  := Compl;
  Dmod.QrUpd.Params[04].AsString  := Bairro;
  Dmod.QrUpd.Params[05].AsString  := Cidade;
  Dmod.QrUpd.Params[06].AsString  := UF;
  Dmod.QrUpd.Params[07].AsString  := CEP;
  Dmod.QrUpd.Params[08].AsString  := Tel1;
  Dmod.QrUpd.Params[09].AsFloat   := Risco;
  Dmod.QrUpd.Params[10].AsString  := IE;
  Dmod.QrUpd.Params[11].AsString  := Email;
  //
  Dmod.QrUpd.Params[12].AsString  := CNPJ;
  Dmod.QrUpd.ExecSQL;
  //

  //  ATUALIZAR ITENS SENDO IMPORTADOS
  if Importando then
  begin
    try
      Dmod.QrUpdL.SQL.Clear;
      Dmod.QrUpdL.SQL.Add('UPDATE importlote SET');
      Dmod.QrUpdL.SQL.Add('Nome_2=:P0, Rua_2=:P1, Numero_2=:P2, Compl_2=:P3, ');
      Dmod.QrUpdL.SQL.Add('Bairro_2=:P4, Cidade_2=:P5, UF_2=:P6, CEP_2=:P7, ');
      Dmod.QrUpdL.SQL.Add('Tel1_2=:P8, RiscoSA=:P9, IE_2=:P10, Email_2=:P11, ');
      Dmod.QrUpdL.SQL.Add('CPF_2=:Pa WHERE CPF=:Pb');
      //
      Dmod.QrUpdL.Params[00].AsString  := Nome;
      Dmod.QrUpdL.Params[01].AsString  := Rua;
      Dmod.QrUpdL.Params[02].AsInteger := Numero;
      Dmod.QrUpdL.Params[03].AsString  := Compl;
      Dmod.QrUpdL.Params[04].AsString  := Bairro;
      Dmod.QrUpdL.Params[05].AsString  := Cidade;
      Dmod.QrUpdL.Params[06].AsString  := UF;
      Dmod.QrUpdL.Params[07].AsString  := CEP;
      Dmod.QrUpdL.Params[08].AsString  := Tel1;
      Dmod.QrUpdL.Params[09].AsFloat   := Risco;
      Dmod.QrUpdL.Params[10].AsString  := IE;
      Dmod.QrUpdL.Params[11].AsString  := Email;
      //
      Dmod.QrUpdL.Params[12].AsString  := CNPJ;
      Dmod.QrUpdL.Params[13].AsString  := CNPJ;
      Dmod.QrUpdL.ExecSQL;
    except
      ;
    end;
  end;
end;

procedure TFmPrincipal.AtualizaSP2(Panel: Integer);
begin
  try
    if Panel in ([0,1]) then
    begin
{
      QrMalots.Close;
      QrMalots.Params[0].AsInteger := FConnections;
      QrMalots. Open;
}
      UnDmkDAC_PF.AbreMySQLQuery0(QrMalots, Dmod.MyDB, [
      'SELECT lo.Lote, lo.Data, lo.Total, lo.Codigo, ',
      'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ',
      'ELSE en.Nome END NOMECLIENTE ',
      'FROM ' + CO_TabLotA + ' lo ',
      'LEFT JOIN entidades en ON en.Codigo=lo.Cliente ',
      'WHERE lo.Tipo=0 AND lo.Conferido=0 ',
      'AND lo.TxCompra + lo.ValValorem + ' +
      Geral.FF0(FConnections) + ' >=0.01 ',
      'AND lo.Codigo>0 ',
      'ORDER BY NOMECLIENTE, Lote ',
      '']);
      //
      SP2.Panels[1].Text := ' ' + FloatToStr(QrMalots.RecordCount);
    end;
  except
    SP2.Panels[1].Text := ' ???';
  end;
  try
    if Panel in ([0,2]) then
    begin
{
      QrECartaSac.Close;
      UMyMod.AbreQuery(QrECartaSac);
}
      UnDmkDAC_PF.AbreMySQLQuery0(QrECartaSac, Dmod.MyDB, [
      'SELECT lo.Lote, lo.Data, lo.Total, lo.Codigo, ',
      'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ',
      'ELSE en.Nome END NOMECLIENTE',
      'FROM ' + CO_TabLotA + ' lo',
      'LEFT JOIN entidades en ON en.Codigo=lo.Cliente',
      'WHERE lo.ECartaSac=0 ',
      'AND lo.Tipo=1',
      'AND lo.Codigo>0',
      'ORDER BY NOMECLIENTE, Lote']);
      //
      SP2.Panels[3].Text := ' '+ FloatToStr(QrECartaSac.RecordCount);
    end;
  except
    SP2.Panels[3].Text := ' ???';
  end;

  // 2011-12-24
  try
    if Panel in ([0,3]) then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrLotOpen, Dmod.MyDB, [
      'SELECT IF(en.Tipo=0 , en.RazaoSocial, en.Nome) NOMECLIENTE, ',
      'IF(en.Tipo=0, en.CNPJ, en.CPF) CNPJCPF, ',
      'IF(en.Tipo=0, en.Fantasia, en.Apelido) NOMEFANCLIENTE, ',
      'lo.Codigo, lo.Lote, lo.Data, lo.NF, ',
      'lo.Cliente, lo.Tipo, lo.Total, en.LimiCred, ',
      'en.CBE CBECli, en.SCB SCBCli, en.QuantN1, en.QuantI1 ',
      'FROM ' + CO_TabLotA + ' lo ',
      'LEFT JOIN entidades en ON en.Codigo=lo.Cliente ',
      'WHERE lo.TxCompra + lo.ValValorem + lo.NF + ' +
      Geral.FF0(FConnections) + ' <> 0',
      'AND lo.Codigo <> 0 ',
      'AND lo.LctsAuto=0 ',
      'ORDER BY Data, Codigo ',
      '']);
      //
      SP2.Panels[9].Text := ' '+ FloatToStr(QrLotOpen.RecordCount);
      if (QrLotOpen.RecordCount > 0) and (date - QrLotOpenData.Value >= 1) then
        MyObjects.frxMostra(frxLotOpen, 'Lotes em Aberto');
    end;
  except
    SP2.Panels[9].Text := ' ???';
  end;
end;

procedure TFmPrincipal.AtualizaTextoBtAcessoRapido();
var
  Texto, Cond, MesAno: String;
  CliInt, Periodo: Integer;
begin
  Texto := 'Acesso'#13#10'R�pido';
  CliInt := Geral.ReadAppKeyCU('Condominio2', Application.Title,
      ktInteger, 0);
  if CliInt > 0 then
  begin
    Periodo := Geral.ReadAppKeyCU('Periodo', Application.Title,
      ktInteger, 0);
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT Sigla FROM cond ');
    Dmod.QrAux.SQL.Add('WHERE Codigo=:P0');
    Dmod.QrAux.Params[0].AsInteger := CliInt;
    UMyMod.AbreQuery(Dmod.QrAux, Dmod.MyDB);
    //
    Cond := Trim(Dmod.QrAux.FieldByName('Sigla').AsString);
//    if (Cond = '') or (Cond = '?') then Cond := 'Cond=' + IntToStr(CliInt);
    if (Cond = '') or (Cond = '?') then Cond := 'N� ' + IntToStr(CliInt);
    if Periodo > 0 then
      MesAno := Geral.FDT(Geral.PeriodoToDate(Periodo, 1, False), 5)
    else
      MesAno := '--/--';
    Texto := Cond + #13#10 + Uppercase(MesAno);
  end;
  AGBAcesRapid.Caption := Texto;
end;

procedure TFmPrincipal.CadastroDeContasNiv();
begin
  if DBCheck.CriaFm(TFmContasNiv, FmContasNiv, afmoNegarComAviso) then
  begin
    FmContasNiv.ShowModal;
    FmContasNiv.Destroy;
  end;
end;

procedure TFmPrincipal.RGProtoFiltroClick(Sender: TObject);
begin
  DModG.FFiltroPPI := RGProtoFiltro.ItemIndex;
  DModG.ReopenPTK();
end;

procedure TFmPrincipal.CalculaCambioAlterado(MyProgress: TProgressBar);
//var
  //Valor: Double;
begin
  // Compatibilidade. Ver adiante
  (*QrPQx.Close;
  UMyMod.AbreQuery(QrPQx);
  if QrPQx.RecordCount > 0 then
  begin
    if MyProgress <> nil then
    begin
      MyProgress.Position := 0;
      MyProgress.Visible := True;
      MyProgress.Max := QrPQx.RecordCount;
    end;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE PQ SET Valor=:P0 WHERE Codigo=:P1');
    while not QrPQx.Eof do
    begin
      if MyProgress <> nil then
      begin
        MyProgress.Position := MyProgress.Position + 1;
        MyProgress.Update;
      end;
      Valor := MLAGeral.CalculaValorPQ(QrPQxReais.Value, QrPQxDolar.Value,
      QrPQxEuro.Value, QrPQxPreco.Value, QrPQxICMS.Value, QrPQxRICMS.VAlue,
      QrPQxRICMSF.Value, QrPQxIPI.VAlue, QrPQxRIPI.VAlue, QrPQxFrete.VAlue,
      QrPQxCFin.VAlue, Dmod.QrCambiosDolar.Value, Dmod.QrCambiosEuro.Value,
      QrPQxMoeda.Value, 0);
      //
      Dmod.QrUpd.Params[0].AsFloat   := Valor;
      Dmod.QrUpd.Params[1].AsInteger := QrPQxCodigo.Value;
      Dmod.QrUpd.ExecSQL;
      //
      QrPQx.Next;
    end;
  end;
  QrPQx.Close;*)
end;

{ DADOS WEB TESTE

200.140.196.106
dermatek_dmk
8wwmqk1u7x38
dermatek_webcred

}



{ TODO :   ZZZ :: URGENTE! Despesas. Cuidar Sit=1 (11 - 2 = 9)!! }
{
SELECT Data, Controle, Credito, Debito,
Tipo, Sit

FROM lct 0001a
WHERE FatID=0
AND Tipo < 2
OR (Tipo=2 AND Sit <> 2 AND Sit <> 3)

}


{ TODO :   ZZZ :: URGENTE! Como est� a carteira de recebimento de duplicatas? }

{ TODO :   ZZZ :: URGENTE! Ver a demora na corre��o dos lan�amentos (Aguarde... Verificando compensados... }
{
18. Ver o que fazer com as emiss�es FatID=302 que est�o
    com sit > 1 e Compensado<2. Segue SQL:
UPDATE lct 0001a
SET Compensado=Vencimento
WHERE Sit > 1
AND Compensado < 2
AND Tipo=2
AND FatID=302
}

{ TODO :   >-< :: Tabelas Web para o aplicativo CustoRM ou GeRC}
{
C:\Projetos\Delphi 2007\Outros\Web\Users
}

end.
