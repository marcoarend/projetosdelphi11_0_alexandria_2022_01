object FmGerDup1Baxa: TFmGerDup1Baxa
  Left = 317
  Top = 252
  Caption = 'DUP-CNTRL-002 :: Baixa de Duplicata'
  ClientHeight = 528
  ClientWidth = 632
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 632
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 584
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 536
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 226
        Height = 32
        Caption = 'Baixa de Duplicata'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 226
        Height = 32
        Caption = 'Baixa de Duplicata'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 226
        Height = 32
        Caption = 'Baixa de Duplicata'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 632
    Height = 101
    Align = alTop
    TabOrder = 1
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 628
      Height = 84
      Align = alClient
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      object Label2: TLabel
        Left = 8
        Top = 4
        Width = 59
        Height = 13
        Caption = 'Vencimento:'
      end
      object Label3: TLabel
        Left = 124
        Top = 4
        Width = 35
        Height = 13
        Caption = 'Cliente:'
      end
      object Label4: TLabel
        Left = 8
        Top = 44
        Width = 48
        Height = 13
        Caption = 'Duplicata:'
      end
      object Label5: TLabel
        Left = 504
        Top = 44
        Width = 20
        Height = 13
        Caption = 'CPF'
      end
      object Label6: TLabel
        Left = 168
        Top = 44
        Width = 44
        Height = 13
        Caption = 'Emitente:'
      end
      object Label7: TLabel
        Left = 92
        Top = 44
        Width = 27
        Height = 13
        Caption = 'Valor:'
      end
      object TPVenc: TdmkEditDateTimePicker
        Left = 8
        Top = 20
        Width = 112
        Height = 21
        Date = 38698.785142685200000000
        Time = 38698.785142685200000000
        TabOrder = 0
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
      end
      object EdCliCod: TdmkEdit
        Left = 124
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdCliNome: TdmkEdit
        Left = 180
        Top = 20
        Width = 437
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdDuplicata: TdmkEdit
        Left = 8
        Top = 60
        Width = 81
        Height = 21
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdCPF: TdmkEdit
        Left = 504
        Top = 60
        Width = 113
        Height = 21
        TabOrder = 4
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdEmitente: TdmkEdit
        Left = 168
        Top = 60
        Width = 333
        Height = 21
        TabOrder = 5
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdValor: TdmkEdit
        Left = 92
        Top = 60
        Width = 73
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 149
    Width = 632
    Height = 64
    Align = alTop
    TabOrder = 2
    object PainelStatus: TPanel
      Left = 2
      Top = 15
      Width = 628
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      Visible = False
      object Label15: TLabel
        Left = 8
        Top = 4
        Width = 121
        Height = 13
        Caption = 'Novo status da duplicata:'
      end
      object Label17: TLabel
        Left = 500
        Top = 4
        Width = 26
        Height = 13
        Caption = 'Data:'
      end
      object SpeedButton1: TSpeedButton
        Left = 476
        Top = 20
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton1Click
      end
      object EdAlinea: TdmkEditCB
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnChange = EdAlineaChange
        DBLookupComboBox = CBAlinea
        IgnoraDBLookupComboBox = False
      end
      object CBAlinea: TdmkDBLookupComboBox
        Left = 64
        Top = 20
        Width = 409
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsOcorDupl
        TabOrder = 1
        dmkEditCB = EdAlinea
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object TPData: TdmkEditDateTimePicker
        Left = 500
        Top = 20
        Width = 112
        Height = 21
        Date = 38698.785142685200000000
        Time = 38698.785142685200000000
        TabOrder = 2
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
      end
    end
  end
  object GBPagto: TGroupBox
    Left = 0
    Top = 213
    Width = 632
    Height = 201
    Align = alClient
    TabOrder = 3
    Visible = False
    object PainelPagto: TPanel
      Left = 2
      Top = 65
      Width = 628
      Height = 134
      Align = alClient
      BevelOuter = bvLowered
      TabOrder = 1
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 626
        Height = 48
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label23: TLabel
          Left = 8
          Top = 4
          Width = 52
          Height = 13
          Caption = 'Data base:'
        end
        object Label24: TLabel
          Left = 124
          Top = 4
          Width = 53
          Height = 13
          Caption = 'Valor base:'
        end
        object Label29: TLabel
          Left = 220
          Top = 4
          Width = 79
          Height = 13
          Caption = '% Juros per'#237'odo:'
        end
        object Label27: TLabel
          Left = 316
          Top = 4
          Width = 37
          Height = 13
          Caption = '$ Juros:'
        end
        object Label30: TLabel
          Left = 412
          Top = 4
          Width = 62
          Height = 13
          Caption = 'Total devido:'
        end
        object Label28: TLabel
          Left = 508
          Top = 4
          Width = 75
          Height = 13
          Caption = '$ Valor a pagar:'
        end
        object TPDataBase2: TdmkEditDateTimePicker
          Left = 8
          Top = 20
          Width = 112
          Height = 21
          Date = 38698.785142685200000000
          Time = 38698.785142685200000000
          Color = clBtnFace
          Enabled = False
          TabOrder = 0
          TabStop = False
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
        object EdValorBase2: TdmkEdit
          Left = 124
          Top = 20
          Width = 93
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          Color = clBtnFace
          ReadOnly = True
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          OnChange = EdValorBase2Change
        end
        object EdJurosPeriodo2: TdmkEdit
          Left = 220
          Top = 20
          Width = 93
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          Color = clBtnFace
          ReadOnly = True
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 6
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
        end
        object EdJuros2: TdmkEdit
          Left = 316
          Top = 20
          Width = 93
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          Color = clBtnFace
          ReadOnly = True
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          OnChange = EdJuros2Change
        end
        object EdCorrigido2: TdmkEdit
          Left = 412
          Top = 20
          Width = 93
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          Color = clBtnFace
          ReadOnly = True
          TabOrder = 4
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
        end
        object EdAPagar2: TdmkEdit
          Left = 508
          Top = 20
          Width = 93
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          Color = clBtnFace
          ReadOnly = True
          TabOrder = 5
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          OnChange = EdAPagar2Change
        end
      end
      object Panel4: TPanel
        Left = 1
        Top = 49
        Width = 626
        Height = 84
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        ExplicitLeft = -2
        ExplicitTop = 46
        object Label26: TLabel
          Left = 8
          Top = 4
          Width = 26
          Height = 13
          Caption = 'Data:'
        end
        object Label1: TLabel
          Left = 316
          Top = 4
          Width = 58
          Height = 13
          Caption = '$ Desconto:'
        end
        object Label8: TLabel
          Left = 412
          Top = 4
          Width = 81
          Height = 13
          Caption = '$ Valor pagando:'
        end
        object Label25: TLabel
          Left = 124
          Top = 4
          Width = 89
          Height = 13
          Caption = '% Taxa juros base:'
        end
        object Label9: TLabel
          Left = 508
          Top = 4
          Width = 84
          Height = 13
          Caption = '$ Valor pendente:'
        end
        object Label10: TLabel
          Left = 220
          Top = 4
          Width = 60
          Height = 13
          Caption = '% Desconto:'
        end
        object TPPagto2: TdmkEditDateTimePicker
          Left = 8
          Top = 20
          Width = 112
          Height = 21
          Date = 38698.785142685200000000
          Time = 38698.785142685200000000
          TabOrder = 0
          OnChange = TPPagto2Change
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
        object EdDesco2: TdmkEdit
          Left = 316
          Top = 20
          Width = 93
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          OnExit = EdDesco2Exit
        end
        object EdValPagar2: TdmkEdit
          Left = 412
          Top = 20
          Width = 93
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          OnChange = EdValPagar2Change
        end
        object EdJurosBase2: TdmkEdit
          Left = 124
          Top = 20
          Width = 93
          Height = 21
          Alignment = taRightJustify
          Color = clWhite
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 6
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          OnChange = EdJurosBase2Change
        end
        object EdPendente2: TdmkEdit
          Left = 508
          Top = 20
          Width = 93
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          Color = clBtnFace
          ReadOnly = True
          TabOrder = 5
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
        end
        object EdDesco3: TdmkEdit
          Left = 220
          Top = 20
          Width = 93
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          OnExit = EdDesco3Exit
        end
        object CkMovimento: TdmkCheckBox
          Left = 8
          Top = 47
          Width = 250
          Height = 17
          Caption = 'N'#227'o gerar movimenta'#231#227'o no financeiro'
          TabOrder = 6
          OnClick = CkMovimentoClick
          UpdType = utYes
          ValCheck = #0
          ValUncheck = #0
          OldValor = #0
        end
      end
    end
    object PnCarteira: TPanel
      Left = 2
      Top = 15
      Width = 628
      Height = 50
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label11: TLabel
        Left = 8
        Top = 4
        Width = 39
        Height = 13
        Caption = 'Carteira:'
      end
      object EdCarteira: TdmkEditCB
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnChange = EdAlineaChange
        DBLookupComboBox = CBCarteira
        IgnoraDBLookupComboBox = False
      end
      object CBCarteira: TdmkDBLookupComboBox
        Left = 64
        Top = 20
        Width = 409
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsCarteiras
        TabOrder = 1
        dmkEditCB = EdCarteira
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 414
    Width = 632
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 628
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 458
    Width = 632
    Height = 70
    Align = alBottom
    TabOrder = 5
    object PnSaiDesis: TPanel
      Left = 486
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 484
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrOcorDupl: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM ocordupl'
      'ORDER BY Nome')
    Left = 164
    Top = 12
    object QrOcorDuplCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOcorDuplNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrOcorDuplBase: TFloatField
      FieldName = 'Base'
    end
    object QrOcorDuplDatapbase: TIntegerField
      FieldName = 'Datapbase'
    end
    object QrOcorDuplOcorrbase: TIntegerField
      FieldName = 'Ocorrbase'
    end
    object QrOcorDuplStatusbase: TIntegerField
      FieldName = 'Statusbase'
    end
  end
  object DsOcorDupl: TDataSource
    DataSet = QrOcorDupl
    Left = 192
    Top = 12
  end
  object QrCarteiras: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM carteiras'
      'WHERE Tipo IN (0, 1)'
      'AND Ativo = 1'
      'AND Codigo <> -1'
      'ORDER BY Nome')
    Left = 348
    Top = 12
    object QrCarteirasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCarteirasNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsCarteiras: TDataSource
    DataSet = QrCarteiras
    Left = 376
    Top = 12
  end
end
