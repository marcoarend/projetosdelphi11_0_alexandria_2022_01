unit GerCliOcor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, UnDmkEnums;

type
  TFmGerCliOcor = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    QrOcorBank: TmySQLQuery;
    QrOcorBankCodigo: TIntegerField;
    QrOcorBankNome: TWideStringField;
    QrOcorBankLk: TIntegerField;
    QrOcorBankDataCad: TDateField;
    QrOcorBankDataAlt: TDateField;
    QrOcorBankUserCad: TIntegerField;
    QrOcorBankUserAlt: TIntegerField;
    QrOcorBankBase: TFloatField;
    DsOcorBank: TDataSource;
    GBOcor: TGroupBox;
    Label20: TLabel;
    EdOcorrencia: TdmkEditCB;
    CBOcorrencia: TdmkDBLookupComboBox;
    Label21: TLabel;
    TPDataO: TDateTimePicker;
    Label22: TLabel;
    EdValor: TdmkEdit;
    Label7: TLabel;
    EdTaxaP: TdmkEdit;
    Label6: TLabel;
    EdDescri: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdOcorrenciaChange(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmGerCliOcor: TFmGerCliOcor;

implementation

uses UnMyObjects, Module, GerCliMain, UMySQLModule;

{$R *.DFM}

procedure TFmGerCliOcor.BtOKClick(Sender: TObject);
var
  DataO, Descri: String;
  Codigo, Cliente, Ocorrencia, TpOcor: Integer;
  Valor, TaxaP: Double;
begin
  DataO          := Geral.FDT(TPDataO.Date, 1);
  Ocorrencia     := EdOcorrencia.ValueVariant;
  Valor          := EdValor.ValueVariant;
  TaxaP          := EdTaxaP.ValueVariant;
  Descri         := EdDescri.Text;
  if ImgTipo.SQLType = stIns then
  begin
    TpOcor := 3;
    Cliente := FmGerCliMain.EdCliente.ValueVariant;
  end else
  begin
    TpOcor := FmGerCliMain.QrOcorreuTpOcor.Value;
    Cliente := FmGerCliMain.QrOcorreuCliente.Value;
  end;
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('ocorreu', 'Codigo', ImgTipo.SQLType, FmGerCliMain.QrOcorreuCodigo.Value);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'ocorreu', False, [
  'Cliente', 'DataO', 'Ocorrencia', 'Valor',
  'TaxaP', 'Descri', 'TpOcor'], ['Codigo'], [
  Cliente, DataO, Ocorrencia, Valor,
  TaxaP, Descri, TpOcor], [Codigo], True) then
  begin
    FmGerCliMain.ReopenOcorreu(Codigo);
    Close;
  end;
end;

procedure TFmGerCliOcor.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGerCliOcor.EdOcorrenciaChange(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
  EdValor.Text := Geral.FFT(QrOcorBankBase.Value, 2, siNegativo);
end;

procedure TFmGerCliOcor.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGerCliOcor.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stNil;
  TPDataO.Date := Date;
  UMyMod.AbreQuery(QrOcorBank, Dmod.MyDB);
end;

procedure TFmGerCliOcor.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
