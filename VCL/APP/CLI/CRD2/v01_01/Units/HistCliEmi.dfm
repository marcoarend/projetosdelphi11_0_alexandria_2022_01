object FmHistCliEmi: TFmHistCliEmi
  Left = 334
  Top = 171
  Caption = 'CSE-PESQU-001 :: Hist'#243'rico'
  ClientHeight = 654
  ClientWidth = 792
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 492
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PageControl1: TPageControl
      Left = 0
      Top = 353
      Width = 792
      Height = 139
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 0
      OnChange = PageControl1Change
      object TabSheet1: TTabSheet
        Caption = 'Dados da pesquisa'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGrid1: TDBGrid
          Left = 0
          Top = 0
          Width = 784
          Height = 111
          Align = alClient
          DataSource = DsItens
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnTitleClick = DBGrid1TitleClick
          Columns = <
            item
              Expanded = False
              FieldName = 'DDeposito'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Valor'
              Width = 62
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Tipo'
              Title.Caption = '**'
              Width = 15
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMESTATUS'
              Title.Caption = 'Status'
              Width = 85
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AEXPIRAR'
              Title.Caption = 'A Expirar'
              Width = 62
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PGDDEPOSITO'
              Title.Caption = 'Quitado'
              Width = 62
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PGATRAZO'
              Title.Caption = 'Pg c/ atraso'
              Width = 62
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PGABERTO'
              Title.Caption = 'Pendente'
              Width = 62
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEEMITENTE'
              Title.Caption = 'Emitente'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECLI'
              Title.Caption = 'Cliente'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Documento'
              Visible = True
            end>
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Sacados / Emitentes do mesmo grupo'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 95
        object DBGrid2: TDBGrid
          Left = 0
          Top = 0
          Width = 784
          Height = 111
          Align = alClient
          DataSource = DsGruSacEmiIts
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'NOMEGRUPO'
              Title.Caption = 'Grupo de Sacados / Emitentes'
              Width = 300
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Sacado / Emitente'
              Width = 300
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CNPJ_CPF_TXT'
              Title.Caption = 'CNPJ / CPF'
              Width = 113
              Visible = True
            end>
        end
      end
    end
    object GroupBox1: TGroupBox
      Left = 0
      Top = 313
      Width = 792
      Height = 40
      Align = alTop
      Caption = ' Configura'#231#227'o da pesquisa de itens: '
      TabOrder = 1
      object CkA: TCheckBox
        Left = 16
        Top = 16
        Width = 97
        Height = 17
        Caption = 'A vencer'
        Checked = True
        State = cbChecked
        TabOrder = 0
        OnClick = CkAClick
      end
      object CkQ: TCheckBox
        Left = 220
        Top = 12
        Width = 97
        Height = 17
        Caption = 'Quitado'
        Checked = True
        State = cbChecked
        TabOrder = 1
        OnClick = CkQClick
      end
      object CkR: TCheckBox
        Left = 416
        Top = 16
        Width = 109
        Height = 17
        Caption = 'Pago com atraso'
        Checked = True
        State = cbChecked
        TabOrder = 2
        OnClick = CkRClick
      end
      object CkP: TCheckBox
        Left = 616
        Top = 16
        Width = 97
        Height = 17
        Caption = 'Pendente'
        Checked = True
        State = cbChecked
        TabOrder = 3
        OnClick = CkPClick
      end
    end
    object GroupBox2: TGroupBox
      Left = 0
      Top = 0
      Width = 792
      Height = 117
      Align = alTop
      TabOrder = 2
      object Label2: TLabel
        Left = 8
        Top = 8
        Width = 164
        Height = 13
        Caption = 'CPF / CNPJ Emitente [F8 localiza]:'
      end
      object Label75: TLabel
        Left = 8
        Top = 48
        Width = 35
        Height = 13
        Caption = 'Cliente:'
      end
      object EdCPF1: TdmkEdit
        Left = 8
        Top = 24
        Width = 121
        Height = 21
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        OnChange = EdCPF1Change
        OnExit = EdCPF1Exit
      end
      object EdCliente: TdmkEditCB
        Left = 8
        Top = 64
        Width = 45
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnChange = EdClienteChange
        DBLookupComboBox = CBCliente
        IgnoraDBLookupComboBox = False
      end
      object CBCliente: TdmkDBLookupComboBox
        Left = 56
        Top = 64
        Width = 309
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMECLIENTE'
        ListSource = DsClientes
        TabOrder = 2
        dmkEditCB = EdCliente
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object CkPeriodo: TCheckBox
        Left = 368
        Top = 46
        Width = 181
        Height = 17
        Caption = 'Per'#237'odo dep'#243'sito (in'#237'cio - fim):'
        TabOrder = 3
        OnClick = CkPeriodoClick
      end
      object TPIni: TDateTimePicker
        Left = 368
        Top = 64
        Width = 90
        Height = 21
        Date = 38855.863521944400000000
        Time = 38855.863521944400000000
        TabOrder = 4
        Visible = False
        OnClick = TPIniClick
        OnChange = TPIniChange
      end
      object TPFim: TDateTimePicker
        Left = 460
        Top = 64
        Width = 90
        Height = 21
        Date = 38855.863521944400000000
        Time = 38855.863521944400000000
        TabOrder = 5
        Visible = False
        OnClick = TPFimClick
        OnChange = TPFimChange
      end
      object CkLotNeg: TCheckBox
        Left = 668
        Top = 8
        Width = 117
        Height = 17
        Caption = 'Border'#244's negativos.'
        Checked = True
        State = cbChecked
        TabOrder = 6
        OnClick = CkLotNegClick
      end
      object CkGrupos: TCheckBox
        Left = 668
        Top = 28
        Width = 117
        Height = 17
        Caption = 'Grupo de sac/emi.'
        Enabled = False
        TabOrder = 7
        OnClick = CkGruposClick
      end
      object CkMediaDias: TCheckBox
        Left = 8
        Top = 93
        Width = 201
        Height = 17
        Caption = 'M'#233'dia de dias dos itens pesquisados:'
        TabOrder = 8
        OnClick = CkMediaDiasClick
      end
      object dmkEdMediaDias: TdmkDBEdit
        Left = 207
        Top = 91
        Width = 100
        Height = 21
        BiDiMode = bdRightToLeft
        DataField = 'PRAZO_MEDIO'
        DataSource = DsMediaDias
        ParentBiDiMode = False
        TabOrder = 9
        Visible = False
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object RGTipo: TRadioGroup
        Left = 552
        Top = 48
        Width = 233
        Height = 38
        Caption = ' Tipo de documento: '
        Columns = 3
        ItemIndex = 2
        Items.Strings = (
          'Cheques'
          'Duplicatas'
          'Ambos')
        TabOrder = 10
        OnClick = RGTipoClick
      end
    end
    object GroupBox3: TGroupBox
      Left = 0
      Top = 117
      Width = 792
      Height = 196
      Align = alTop
      TabOrder = 3
      object Panel2: TPanel
        Left = 2
        Top = 15
        Width = 788
        Height = 179
        Align = alClient
        BevelOuter = bvNone
        Enabled = False
        TabOrder = 0
        object Bevel31: TBevel
          Left = 28
          Top = 109
          Width = 116
          Height = 21
          Shape = bsSpacer
        end
        object Bevel1: TBevel
          Left = 396
          Top = 88
          Width = 116
          Height = 21
          Shape = bsSpacer
          Style = bsRaised
        end
        object Bevel24: TBevel
          Left = 640
          Top = 151
          Width = 144
          Height = 21
          Shape = bsSpacer
          Style = bsRaised
        end
        object Bevel23: TBevel
          Left = 640
          Top = 130
          Width = 144
          Height = 21
          Shape = bsSpacer
          Style = bsRaised
        end
        object Bevel25: TBevel
          Left = 640
          Top = 109
          Width = 144
          Height = 21
          Shape = bsSpacer
        end
        object Bevel26: TBevel
          Left = 640
          Top = 67
          Width = 144
          Height = 21
          Shape = bsSpacer
          Style = bsRaised
        end
        object Bevel27: TBevel
          Left = 640
          Top = 46
          Width = 144
          Height = 21
          Shape = bsSpacer
          Style = bsRaised
        end
        object Bevel28: TBevel
          Left = 640
          Top = 25
          Width = 144
          Height = 21
          Shape = bsSpacer
          Style = bsRaised
        end
        object Bevel29: TBevel
          Left = 640
          Top = 4
          Width = 144
          Height = 21
          Shape = bsSpacer
          Style = bsRaised
        end
        object Bevel22: TBevel
          Left = 396
          Top = 151
          Width = 116
          Height = 21
          Shape = bsSpacer
          Style = bsRaised
        end
        object Bevel21: TBevel
          Left = 396
          Top = 130
          Width = 116
          Height = 21
          Shape = bsSpacer
          Style = bsRaised
        end
        object Bevel20: TBevel
          Left = 396
          Top = 109
          Width = 116
          Height = 21
          Shape = bsSpacer
        end
        object Bevel19: TBevel
          Left = 396
          Top = 67
          Width = 116
          Height = 21
          Shape = bsSpacer
          Style = bsRaised
        end
        object Bevel18: TBevel
          Left = 396
          Top = 46
          Width = 116
          Height = 21
          Shape = bsSpacer
          Style = bsRaised
        end
        object Bevel17: TBevel
          Left = 396
          Top = 25
          Width = 116
          Height = 21
          Shape = bsSpacer
          Style = bsRaised
        end
        object Bevel16: TBevel
          Left = 396
          Top = 4
          Width = 116
          Height = 21
          Shape = bsSpacer
          Style = bsRaised
        end
        object Bevel8: TBevel
          Left = 28
          Top = 130
          Width = 116
          Height = 21
          Shape = bsSpacer
          Style = bsRaised
        end
        object Bevel7: TBevel
          Left = 28
          Top = 151
          Width = 116
          Height = 21
          Shape = bsSpacer
          Style = bsRaised
        end
        object Bevel5: TBevel
          Left = 28
          Top = 67
          Width = 116
          Height = 21
          Shape = bsSpacer
          Style = bsRaised
        end
        object Bevel6: TBevel
          Left = 28
          Top = 88
          Width = 116
          Height = 21
          Shape = bsSpacer
          Style = bsRaised
        end
        object Bevel4: TBevel
          Left = 28
          Top = 46
          Width = 116
          Height = 21
          Shape = bsSpacer
          Style = bsRaised
        end
        object Bevel3: TBevel
          Left = 28
          Top = 25
          Width = 116
          Height = 21
          Shape = bsSpacer
          Style = bsRaised
        end
        object Bevel2: TBevel
          Left = 28
          Top = 4
          Width = 116
          Height = 21
          Shape = bsSpacer
          Style = bsRaised
        end
        object Label1: TLabel
          Left = 32
          Top = 155
          Width = 100
          Height = 13
          Caption = 'TOTAL pesquisados:'
          FocusControl = DBEdit1
        end
        object Label3: TLabel
          Left = 32
          Top = 8
          Width = 44
          Height = 13
          Caption = 'A expirar:'
          FocusControl = DBEdit2
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label4: TLabel
          Left = 32
          Top = 29
          Width = 49
          Height = 13
          Caption = 'Expirados:'
          FocusControl = DBEdit3
        end
        object Label5: TLabel
          Left = 32
          Top = 50
          Width = 67
          Height = 13
          Caption = 'Pagos em dia:'
          FocusControl = DBEdit4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label6: TLabel
          Left = 32
          Top = 71
          Width = 103
          Height = 13
          Caption = 'Vencidos/devolvidos:'
          FocusControl = DBEdit5
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label7: TLabel
          Left = 32
          Top = 92
          Width = 88
          Height = 13
          Caption = 'Pagos com atraso:'
          FocusControl = DBEdit6
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clPurple
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label8: TLabel
          Left = 32
          Top = 134
          Width = 54
          Height = 13
          Caption = 'Pendentes:'
          FocusControl = DBEdit7
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Bevel9: TBevel
          Left = 236
          Top = 4
          Width = 128
          Height = 21
          Shape = bsSpacer
          Style = bsRaised
        end
        object Bevel10: TBevel
          Left = 236
          Top = 25
          Width = 128
          Height = 21
          Shape = bsSpacer
          Style = bsRaised
        end
        object Bevel11: TBevel
          Left = 236
          Top = 46
          Width = 128
          Height = 21
          Shape = bsSpacer
          Style = bsRaised
        end
        object Bevel12: TBevel
          Left = 236
          Top = 67
          Width = 128
          Height = 21
          Shape = bsSpacer
          Style = bsRaised
        end
        object Bevel13: TBevel
          Left = 236
          Top = 88
          Width = 128
          Height = 21
          Shape = bsSpacer
          Style = bsRaised
        end
        object Bevel14: TBevel
          Left = 236
          Top = 130
          Width = 128
          Height = 21
          Shape = bsSpacer
          Style = bsRaised
        end
        object Bevel15: TBevel
          Left = 236
          Top = 151
          Width = 128
          Height = 21
          Shape = bsSpacer
          Style = bsRaised
        end
        object Label9: TLabel
          Left = 240
          Top = 155
          Width = 80
          Height = 13
          Caption = '% de todos itens.'
          FocusControl = DBEdit1
        end
        object Label10: TLabel
          Left = 240
          Top = 134
          Width = 102
          Height = 13
          Caption = '% dos itens vencidos.'
          FocusControl = DBEdit7
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label11: TLabel
          Left = 240
          Top = 92
          Width = 102
          Height = 13
          Caption = '% dos itens vencidos.'
          FocusControl = DBEdit6
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clPurple
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label12: TLabel
          Left = 240
          Top = 71
          Width = 104
          Height = 13
          Caption = '% dos itens expirados.'
          FocusControl = DBEdit5
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label13: TLabel
          Left = 240
          Top = 50
          Width = 104
          Height = 13
          Caption = '% dos itens expirados.'
          FocusControl = DBEdit4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label14: TLabel
          Left = 240
          Top = 29
          Width = 118
          Height = 13
          Caption = '% dos itens pesquisados.'
          FocusControl = DBEdit3
        end
        object Label15: TLabel
          Left = 240
          Top = 8
          Width = 118
          Height = 13
          Caption = '% dos itens pesquisados.'
          FocusControl = DBEdit2
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label23: TLabel
          Left = 644
          Top = 8
          Width = 130
          Height = 13
          Caption = '% dos valores pesquisados.'
          FocusControl = DBEdit27
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label24: TLabel
          Left = 644
          Top = 29
          Width = 130
          Height = 13
          Caption = '% dos valores pesquisados.'
          FocusControl = DBEdit26
        end
        object Label25: TLabel
          Left = 644
          Top = 50
          Width = 116
          Height = 13
          Caption = '% dos valores expirados.'
          FocusControl = DBEdit23
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label26: TLabel
          Left = 644
          Top = 71
          Width = 116
          Height = 13
          Caption = '% dos valores expirados.'
          FocusControl = DBEdit22
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label27: TLabel
          Left = 644
          Top = 113
          Width = 118
          Height = 13
          Caption = '% dos pagos com atraso.'
          FocusControl = DBEdit19
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8388863
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label28: TLabel
          Left = 644
          Top = 155
          Width = 92
          Height = 13
          Caption = '% de todos valores.'
          FocusControl = DBEdit15
        end
        object Label29: TLabel
          Left = 644
          Top = 134
          Width = 114
          Height = 13
          Caption = '% dos valores vencidos.'
          FocusControl = DBEdit18
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label16: TLabel
          Left = 400
          Top = 8
          Width = 44
          Height = 13
          Caption = 'A expirar:'
          FocusControl = DBEdit2
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label17: TLabel
          Left = 400
          Top = 29
          Width = 49
          Height = 13
          Caption = 'Expirados:'
          FocusControl = DBEdit3
        end
        object Label18: TLabel
          Left = 400
          Top = 50
          Width = 67
          Height = 13
          Caption = 'Pagos em dia:'
          FocusControl = DBEdit4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label19: TLabel
          Left = 400
          Top = 71
          Width = 103
          Height = 13
          Caption = 'Vencidos/devolvidos:'
          FocusControl = DBEdit5
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label20: TLabel
          Left = 400
          Top = 113
          Width = 60
          Height = 13
          Caption = 'Revendidos:'
          FocusControl = DBEdit6
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8388863
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label21: TLabel
          Left = 400
          Top = 134
          Width = 54
          Height = 13
          Caption = 'Pendentes:'
          FocusControl = DBEdit7
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label22: TLabel
          Left = 400
          Top = 155
          Width = 100
          Height = 13
          Caption = 'TOTAL pesquisados:'
          FocusControl = DBEdit1
        end
        object LaValores: TdmkLabelRotate
          Left = 374
          Top = 4
          Width = 22
          Height = 168
          Text3D = True
          Angle = ag90
          Caption = 'VALORES'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
        end
        object LaItens: TdmkLabelRotate
          Left = 6
          Top = 4
          Width = 22
          Height = 168
          Text3D = True
          Angle = ag90
          Caption = 'ITENS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
        end
        object Label31: TLabel
          Left = 400
          Top = 92
          Width = 88
          Height = 13
          Caption = 'Pagos com atraso:'
          FocusControl = DBEdit6
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clPurple
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label32: TLabel
          Left = 644
          Top = 92
          Width = 114
          Height = 13
          Caption = '% dos valores vencidos.'
          FocusControl = DBEdit29
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clPurple
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Bevel30: TBevel
          Left = 640
          Top = 88
          Width = 144
          Height = 21
          Shape = bsSpacer
          Style = bsRaised
        end
        object Label33: TLabel
          Left = 32
          Top = 113
          Width = 60
          Height = 13
          Caption = 'Revendidos:'
          FocusControl = DBEdit31
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8388863
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Bevel32: TBevel
          Left = 236
          Top = 109
          Width = 128
          Height = 21
          Shape = bsSpacer
        end
        object Label34: TLabel
          Left = 240
          Top = 111
          Width = 118
          Height = 13
          Caption = '% dos pagos com atraso.'
          FocusControl = DBEdit31
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8388863
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object DBEdit1: TDBEdit
          Left = 144
          Top = 151
          Width = 52
          Height = 21
          DataField = 'SOMA_I_ITENS'
          DataSource = DsSomaA
          TabOrder = 0
        end
        object DBEdit2: TDBEdit
          Left = 144
          Top = 4
          Width = 52
          Height = 21
          DataField = 'SOMA_I_AVencer'
          DataSource = DsSomaA
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
        end
        object DBEdit3: TDBEdit
          Left = 144
          Top = 25
          Width = 52
          Height = 21
          DataField = 'SOMA_I_Vencidos'
          DataSource = DsSomaA
          TabOrder = 2
        end
        object DBEdit4: TDBEdit
          Left = 144
          Top = 46
          Width = 52
          Height = 21
          DataField = 'SOMA_I_PgVencto'
          DataSource = DsSomaA
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
        end
        object DBEdit5: TDBEdit
          Left = 144
          Top = 67
          Width = 52
          Height = 21
          DataField = 'SOMA_I_Devolvid'
          DataSource = DsSomaA
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 4
        end
        object DBEdit6: TDBEdit
          Left = 144
          Top = 88
          Width = 52
          Height = 21
          DataField = 'SOMA_I_DevPgTot'
          DataSource = DsSomaA
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clPurple
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 5
        end
        object DBEdit7: TDBEdit
          Left = 144
          Top = 130
          Width = 52
          Height = 21
          DataField = 'SOMA_I_DevNaoQt'
          DataSource = DsSomaA
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 6
        end
        object DBEdit8: TDBEdit
          Left = 196
          Top = 4
          Width = 40
          Height = 21
          DataField = 'PERC_AVencer'
          DataSource = DsSomaA
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 7
        end
        object DBEdit9: TDBEdit
          Left = 196
          Top = 25
          Width = 40
          Height = 21
          DataField = 'PERC_Vencidos'
          DataSource = DsSomaA
          TabOrder = 8
        end
        object DBEdit10: TDBEdit
          Left = 196
          Top = 46
          Width = 40
          Height = 21
          DataField = 'PERC_PgVencto'
          DataSource = DsSomaA
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 9
        end
        object DBEdit11: TDBEdit
          Left = 196
          Top = 67
          Width = 40
          Height = 21
          DataField = 'PERC_Devolvid'
          DataSource = DsSomaA
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 10
        end
        object DBEdit12: TDBEdit
          Left = 196
          Top = 88
          Width = 40
          Height = 21
          DataField = 'PERC_DevPgTot'
          DataSource = DsSomaA
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clPurple
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 11
        end
        object DBEdit13: TDBEdit
          Left = 196
          Top = 130
          Width = 40
          Height = 21
          DataField = 'PERC_DevNaoQt'
          DataSource = DsSomaA
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 12
        end
        object DBEdit14: TDBEdit
          Left = 196
          Top = 151
          Width = 40
          Height = 21
          DataField = 'PERC_ITENS'
          DataSource = DsSomaA
          TabOrder = 13
        end
        object DBEdit15: TDBEdit
          Left = 512
          Top = 151
          Width = 88
          Height = 21
          DataField = 'SOMA_V_VALOR'
          DataSource = DsSomaA
          TabOrder = 14
        end
        object DBEdit16: TDBEdit
          Left = 600
          Top = 151
          Width = 40
          Height = 21
          DataField = 'PERC_VALOR'
          DataSource = DsSomaA
          TabOrder = 15
        end
        object DBEdit17: TDBEdit
          Left = 600
          Top = 130
          Width = 40
          Height = 21
          DataField = 'PERC_Aberto'
          DataSource = DsSomaA
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 16
        end
        object DBEdit18: TDBEdit
          Left = 512
          Top = 130
          Width = 88
          Height = 21
          DataField = 'SOMA_V_PGABERTO'
          DataSource = DsSomaA
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 17
        end
        object DBEdit19: TDBEdit
          Left = 512
          Top = 109
          Width = 88
          Height = 21
          DataField = 'Pago'
          DataSource = DsRevenda
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8388863
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 18
        end
        object DBEdit20: TDBEdit
          Left = 600
          Top = 109
          Width = 40
          Height = 21
          DataField = 'Pago_Perc'
          DataSource = DsRevenda
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8388863
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 19
        end
        object DBEdit21: TDBEdit
          Left = 600
          Top = 67
          Width = 40
          Height = 21
          DataField = 'PERC_Vencido'
          DataSource = DsSomaA
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 20
        end
        object DBEdit22: TDBEdit
          Left = 512
          Top = 67
          Width = 88
          Height = 21
          DataField = 'SOMA_V_VENCIDO'
          DataSource = DsSomaA
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 21
        end
        object DBEdit23: TDBEdit
          Left = 512
          Top = 46
          Width = 88
          Height = 21
          DataField = 'SOMA_V_PGDDEPOSITO'
          DataSource = DsSomaA
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 22
        end
        object DBEdit24: TDBEdit
          Left = 600
          Top = 46
          Width = 40
          Height = 21
          DataField = 'PERC_PgDDeposito'
          DataSource = DsSomaA
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 23
        end
        object DBEdit25: TDBEdit
          Left = 600
          Top = 25
          Width = 40
          Height = 21
          DataField = 'PERC_Expirado'
          DataSource = DsSomaA
          TabOrder = 24
        end
        object DBEdit26: TDBEdit
          Left = 512
          Top = 25
          Width = 88
          Height = 21
          DataField = 'SOMA_V_EXPIRADO'
          DataSource = DsSomaA
          TabOrder = 25
        end
        object DBEdit27: TDBEdit
          Left = 512
          Top = 4
          Width = 88
          Height = 21
          DataField = 'SOMA_V_AEXPIRAR'
          DataSource = DsSomaA
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 26
        end
        object DBEdit28: TDBEdit
          Left = 600
          Top = 4
          Width = 40
          Height = 21
          DataField = 'PERC_AExpirar'
          DataSource = DsSomaA
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 27
        end
        object DBEdit29: TDBEdit
          Left = 512
          Top = 88
          Width = 88
          Height = 21
          DataField = 'SOMA_V_PGATRAZO'
          DataSource = DsSomaA
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clPurple
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 28
        end
        object DBEdit30: TDBEdit
          Left = 600
          Top = 88
          Width = 40
          Height = 21
          DataField = 'PERC_Atrazo'
          DataSource = DsSomaA
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clPurple
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 29
        end
        object DBEdit31: TDBEdit
          Left = 144
          Top = 109
          Width = 52
          Height = 21
          DataField = 'Itens'
          DataSource = DsRevenda
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8388863
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 30
        end
        object DBEdit32: TDBEdit
          Left = 196
          Top = 109
          Width = 40
          Height = 21
          DataField = 'Itens_Perc'
          DataSource = DsRevenda
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8388863
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 31
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 744
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 696
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 106
        Height = 32
        Caption = 'Hist'#243'rico'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 106
        Height = 32
        Caption = 'Hist'#243'rico'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 106
        Height = 32
        Caption = 'Hist'#243'rico'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 540
    Width = 792
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 788
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 288
        Height = 16
        Caption = '** Tipo de documento: C = Cheque, D= Duplicata.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 288
        Height = 16
        Caption = '** Tipo de documento: C = Cheque, D= Duplicata.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 584
    Width = 792
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 646
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 42
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 644
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 22
        Left = 20
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Pesquisa'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtImprime: TBitBtn
        Tag = 5
        Left = 120
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Imprime'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtImprimeClick
      end
      object BtGrupo: TBitBtn
        Tag = 243
        Left = 216
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Inclui novo banco'
        Caption = '&Grupo'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        Visible = False
        OnClick = BtGrupoClick
      end
    end
  end
  object QrEmitCPF: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nome'
      'FROM emitcpf'
      'WHERE CPF=:P0')
    Left = 636
    Top = 320
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmitCPFNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsEmitCPF: TDataSource
    DataSet = QrEmitCPF
    Left = 664
    Top = 320
  end
  object QrSomaA: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrSomaACalcFields
    SQL.Strings = (
      '/*'
      'SELECT SUM(loi.Valor) Valor, COUNT(loi.Controle) ITENS,'
      'SUM(IF(loi.DDeposito > SYSDATE(), 1, 0)) ITENS_AVencer,'
      'SUM(IF((loi.DDeposito < SYSDATE()), 1, 0)) ITENS_Vencidos,'
      'SUM(IF((loi.DDeposito < SYSDATE()), 1, 0)) -'
      'SUM(IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) > 0),'
      '1, 0)) ITENS_PgVencto,'
      'SUM(IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) > 0),'
      '1, 0)) ITENS_Devolvid,'
      'SUM(IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) = 12),'
      '1, 0)) ITENS_DevPgTot,'
      
        'SUM(IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) in (10, ' +
        '11)),'
      '1, 0)) ITENS_DevNaoQt,'
      'SUM((chd.ValPago - chd.Taxas - chd.JurosV + chd.Desconto)) Pago,'
      'SUM(IF(loi.DDeposito < SYSDATE(), loi.Valor, 0)) EXPIRADO,'
      'SUM(IF(loi.DDeposito < SYSDATE(), 0, loi.Valor)) AEXPIRAR,'
      'SUM(IF(loi.DDeposito < SYSDATE() AND (chd.Status+10) > 0,'
      
        'loi.Valor - IF(loi.DDeposito < SYSDATE(), 0, loi.Valor), 0)) VEN' +
        'CIDO,'
      'SUM(IF(loi.DDeposito < SYSDATE() AND (chd.Status+10) > 0, 0,'
      
        'loi.Valor - IF(loi.DDeposito < SYSDATE(), 0, loi.Valor))) PGDDEP' +
        'OSITO,'
      'SUM(IF(loi.DDeposito < SYSDATE() AND (chd.Status+10) > 0,'
      
        'chd.ValPago - chd.Taxas - chd.JurosV + chd.Desconto,0)) PGATRAZO' +
        ','
      'SUM(IF(loi.DDeposito < SYSDATE() AND (chd.Status+10) > 0,'
      'loi.Valor - (chd.ValPago - chd.Taxas - chd.JurosV) +'
      'chd.Desconto, 0)) PGABERTO'
      'FROM lot esits loi'
      'LEFT JOIN lot es     lot ON lot.Codigo=loi.Codigo'
      'LEFT JOIN entidades cli ON cli.Codigo=lot.Cliente'
      'LEFT JOIN alinits   chd ON chd.ChequeOrigem=loi.Controle'
      'LEFT JOIN bancos    ban ON ban.Codigo=loi.Banco'
      'WHERE lot.Tipo=0'
      '*/'
      'SELECT SUM(lct.Credito) Valor, COUNT(lct.FatParcela) ITENS,'
      'SUM(IF(lct.DDeposito > SYSDATE(), 1, 0)) ITENS_AVencer,'
      'SUM(IF((lct.DDeposito < SYSDATE()), 1, 0)) ITENS_Vencidos,'
      'SUM(IF((lct.DDeposito < SYSDATE()), 1, 0)) -'
      'SUM(IF((lct.DDeposito < SYSDATE()) AND ((chd.Status+10) > 0),'
      '1, 0)) ITENS_PgVencto,'
      'SUM(IF((lct.DDeposito < SYSDATE()) AND ((chd.Status+10) > 0),'
      '1, 0)) ITENS_Devolvid,'
      'SUM(IF((lct.DDeposito < SYSDATE()) AND ((chd.Status+10) = 12),'
      '1, 0)) ITENS_DevPgTot,'
      
        'SUM(IF((lct.DDeposito < SYSDATE()) AND ((chd.Status+10) in (10, ' +
        '11)),'
      '1, 0)) ITENS_DevNaoQt,'
      'SUM((chd.ValPago - chd.Taxas - chd.JurosV + chd.Desconto)) Pago,'
      'SUM(IF(lct.DDeposito < SYSDATE(), lct.Credito, 0)) EXPIRADO,'
      'SUM(IF(lct.DDeposito < SYSDATE(), 0, lct.Credito)) AEXPIRAR,'
      'SUM(IF(lct.DDeposito < SYSDATE() AND (chd.Status+10) > 0,'
      
        'lct.Credito - IF(lct.DDeposito < SYSDATE(), 0, lct.Credito), 0))' +
        ' VENCIDO,'
      'SUM(IF(lct.DDeposito < SYSDATE() AND (chd.Status+10) > 0, 0,'
      
        'lct.Credito - IF(lct.DDeposito < SYSDATE(), 0, lct.Credito))) PG' +
        'DDEPOSITO,'
      'SUM(IF(lct.DDeposito < SYSDATE() AND (chd.Status+10) > 0,'
      
        'chd.ValPago - chd.Taxas - chd.JurosV + chd.Desconto,0)) PGATRAZO' +
        ','
      'SUM(IF(lct.DDeposito < SYSDATE() AND (chd.Status+10) > 0,'
      'lct.Credito - (chd.ValPago - chd.Taxas - chd.JurosV) +'
      'chd.Desconto, 0)) PGABERTO'
      'FROM lct0001a lct'
      'LEFT JOIN lot es     lot ON lot.Codigo=lct.FatNum'
      'LEFT JOIN entidades cli ON cli.Codigo=lot.Cliente'
      'LEFT JOIN alinits   chd ON chd.ChequeOrigem=lct.FatParcela'
      'LEFT JOIN bancos    ban ON ban.Codigo=lct.Banco'
      'WHERE lct.FatID=301'
      'AND lot.Tipo <> 1'
      'AND lct.Cliente=267'
      '')
    Left = 568
    Top = 468
    object QrSomaAITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
      DisplayFormat = '0;-0; '
    end
    object QrSomaAITENS_AVencer: TFloatField
      FieldName = 'ITENS_AVencer'
      DisplayFormat = '0;-0; '
    end
    object QrSomaAITENS_Vencidos: TFloatField
      FieldName = 'ITENS_Vencidos'
      DisplayFormat = '0;-0; '
    end
    object QrSomaAITENS_PgVencto: TFloatField
      FieldName = 'ITENS_PgVencto'
      DisplayFormat = '0;-0; '
    end
    object QrSomaAITENS_Devolvid: TFloatField
      FieldName = 'ITENS_Devolvid'
      DisplayFormat = '0;-0; '
    end
    object QrSomaAITENS_DevPgTot: TFloatField
      FieldName = 'ITENS_DevPgTot'
      DisplayFormat = '0;-0; '
    end
    object QrSomaAITENS_DevNaoQt: TFloatField
      FieldName = 'ITENS_DevNaoQt'
      DisplayFormat = '0;-0; '
    end
    object QrSomaAPago: TFloatField
      FieldName = 'Pago'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrSomaAValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrSomaAEXPIRADO: TFloatField
      FieldName = 'EXPIRADO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrSomaAAEXPIRAR: TFloatField
      FieldName = 'AEXPIRAR'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrSomaAVENCIDO: TFloatField
      FieldName = 'VENCIDO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrSomaAPGDDEPOSITO: TFloatField
      FieldName = 'PGDDEPOSITO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrSomaAPGATRAZO: TFloatField
      FieldName = 'PGATRAZO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrSomaAPGABERTO: TFloatField
      FieldName = 'PGABERTO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrSomaAPERC_AVencer: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PERC_AVencer'
      DisplayFormat = '0.00;-0.00; '
      Calculated = True
    end
    object QrSomaAPERC_Vencidos: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PERC_Vencidos'
      DisplayFormat = '0.00;-0.00; '
      Calculated = True
    end
    object QrSomaAPERC_PgVencto: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PERC_PgVencto'
      DisplayFormat = '0.00;-0.00; '
      Calculated = True
    end
    object QrSomaAPERC_Devolvid: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PERC_Devolvid'
      DisplayFormat = '0.00;-0.00; '
      Calculated = True
    end
    object QrSomaAPERC_DevPgTot: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PERC_DevPgTot'
      DisplayFormat = '0.00;-0.00; '
      Calculated = True
    end
    object QrSomaAPERC_DevNaoQt: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PERC_DevNaoQt'
      DisplayFormat = '0.00;-0.00; '
      Calculated = True
    end
    object QrSomaAPERC_ITENS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PERC_ITENS'
      DisplayFormat = '0.00;-0.00; '
      Calculated = True
    end
    object QrSomaAPERC_Expirado: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PERC_Expirado'
      DisplayFormat = '0.00;-0.00; '
      Calculated = True
    end
    object QrSomaAPERC_AExpirar: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PERC_AExpirar'
      DisplayFormat = '0.00;-0.00; '
      Calculated = True
    end
    object QrSomaAPERC_Vencido: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PERC_Vencido'
      DisplayFormat = '0.00;-0.00; '
      Calculated = True
    end
    object QrSomaAPERC_PgDDeposito: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PERC_PgDDeposito'
      DisplayFormat = '0.00;-0.00; '
      Calculated = True
    end
    object QrSomaAPERC_Atrazo: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PERC_Atrazo'
      DisplayFormat = '0.00;-0.00; '
      Calculated = True
    end
    object QrSomaAPERC_Aberto: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PERC_Aberto'
      DisplayFormat = '0.00;-0.00; '
      Calculated = True
    end
    object QrSomaAPERC_VALOR: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PERC_VALOR'
      DisplayFormat = '0.00;-0.00; '
      Calculated = True
    end
    object QrSomaASOMA_I_AVencer: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SOMA_I_AVencer'
      DisplayFormat = '0;-0; '
      Calculated = True
    end
    object QrSomaASOMA_I_Vencidos: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SOMA_I_Vencidos'
      DisplayFormat = '0;-0; '
      Calculated = True
    end
    object QrSomaASOMA_I_PgVencto: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SOMA_I_PgVencto'
      DisplayFormat = '0;-0; '
      Calculated = True
    end
    object QrSomaASOMA_I_ITENS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SOMA_I_ITENS'
      DisplayFormat = '0;-0; '
      Calculated = True
    end
    object QrSomaASOMA_I_Devolvid: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SOMA_I_Devolvid'
      DisplayFormat = '0;-0; '
      Calculated = True
    end
    object QrSomaASOMA_I_DevPgTot: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SOMA_I_DevPgTot'
      DisplayFormat = '0;-0; '
      Calculated = True
    end
    object QrSomaASOMA_I_DevNaoQt: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SOMA_I_DevNaoQt'
      DisplayFormat = '0;-0; '
      Calculated = True
    end
    object QrSomaASOMA_V_AEXPIRAR: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SOMA_V_AEXPIRAR'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrSomaASOMA_V_EXPIRADO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SOMA_V_EXPIRADO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrSomaASOMA_V_VENCIDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SOMA_V_VENCIDO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrSomaASOMA_V_PGDDEPOSITO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SOMA_V_PGDDEPOSITO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrSomaASOMA_V_PGATRAZO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SOMA_V_PGATRAZO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrSomaASOMA_V_PGABERTO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SOMA_V_PGABERTO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrSomaASOMA_V_VALOR: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SOMA_V_VALOR'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
  end
  object DsSomaA: TDataSource
    DataSet = QrSomaA
    Left = 596
    Top = 468
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMECLIENTE, Codigo'
      'FROM entidades'
      'WHERE Cliente1="V"'
      'ORDER BY NOMECLIENTE')
    Left = 249
    Top = 101
    object QrClientesNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 277
    Top = 101
  end
  object QrItens: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrItensAfterOpen
    BeforeClose = QrItensBeforeClose
    OnCalcFields = QrItensCalcFields
    SQL.Strings = (
      '/*'
      'SELECT loi.Controle, loi.Valor, 1 ITENS,'
      ''
      '(IF(loi.DDeposito > SYSDATE(), 1, 0) +'
      'IF((loi.DDeposito < SYSDATE()), 2, 0) -'
      
        'IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) > 0), 2, 0) ' +
        '+'
      
        'IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) = 12), 4, 0)' +
        ')'
      'ITENS_XXX,'
      ''
      'IF(loi.DDeposito > SYSDATE(), 1, 0) ITENS_AVencer,'
      'IF((loi.DDeposito < SYSDATE()), 1, 0) ITENS_Vencidos,'
      'IF((loi.DDeposito < SYSDATE()), 1, 0) -'
      'IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) > 0),'
      '1, 0) ITENS_PgVencto,'
      'IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) > 0),'
      '1, 0) ITENS_Devolvid,'
      'IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) = 12),'
      '1, 0) ITENS_DevPgTot,'
      
        'IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) in (10, 11))' +
        ','
      '1, 0) ITENS_DevNaoQt,'
      '(chd.ValPago - chd.Taxas - chd.JurosV + chd.Desconto) Pago,'
      'IF(loi.DDeposito < SYSDATE(), loi.Valor, 0) EXPIRADO,'
      'IF(loi.DDeposito < SYSDATE(), 0, loi.Valor) AEXPIRAR,'
      'IF(loi.DDeposito < SYSDATE() AND (chd.Status+10) > 0,'
      
        'loi.Valor - IF(loi.DDeposito < SYSDATE(), 0, loi.Valor), 0) VENC' +
        'IDO,'
      'IF(loi.DDeposito < SYSDATE() AND (chd.Status+10) > 0, 0,'
      
        'loi.Valor - IF(loi.DDeposito < SYSDATE(), 0, loi.Valor)) PGDDEPO' +
        'SITO,'
      'IF(loi.DDeposito < SYSDATE() AND (chd.Status+10) > 0,'
      'chd.ValPago - chd.Taxas - chd.JurosV + chd.Desconto,0) PGATRAZO,'
      'IF(loi.DDeposito < SYSDATE() AND (chd.Status+10) > 0,'
      'loi.Valor - (chd.ValPago - chd.Taxas - chd.JurosV) +'
      'chd.Desconto, 0) PGABERTO, "C" Tipo,'
      'MONTH(loi.DDeposito)+YEAR(loi.DDeposito)*100+0.00 PERIODO,'
      
        'CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial ELSE cli.Nome END NOME' +
        'CLI,'
      'ban.Nome NOMEBANCO, loi.DDeposito, loi.Emitente NOMEEMITENTE,'
      'loi.Banco, loi.Agencia, loi.Conta, loi.Cheque, loi.Duplicata'
      'FROM lot esits loi'
      'LEFT JOIN lot es     lot ON lot.Codigo=loi.Codigo'
      'LEFT JOIN entidades cli ON cli.Codigo=lot.Cliente'
      'LEFT JOIN alinits   chd ON chd.ChequeOrigem=loi.Controle'
      'LEFT JOIN bancos    ban ON ban.Codigo=loi.Banco'
      'WHERE lot.Tipo=0'
      'AND loi.DDeposito  BETWEEN "2006/05/18" AND "2007/05/18"'
      ''
      'AND (IF(loi.DDeposito > SYSDATE(), 1, 0) +'
      'IF((loi.DDeposito < SYSDATE()), 2, 0) -'
      
        'IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) > 0), 2, 0) ' +
        '+'
      
        'IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) = 12), 4, 0)' +
        ') in (0,1,2,4)'
      ''
      ''
      'UNION'
      ''
      ''
      'SELECT loi.Controle, loi.Valor, 1 ITENS,'
      ''
      '(IF(loi.DDeposito > SYSDATE(), 1, 0) +'
      'IF((loi.Quitado>1) AND (loi.DDeposito>=loi.Data3), 2, 0) +'
      'IF((loi.Quitado>1) AND (loi.DDeposito< loi.Data3), 4, 0) )'
      'ITENS_XXX,'
      ''
      'IF(loi.DDeposito > SYSDATE(), 1, 0) ITENS_AVencer,'
      'IF((loi.DDeposito < SYSDATE()), 1, 0) ITENS_Vencidos,'
      
        'IF((loi.Quitado>1) AND (loi.DDeposito>=loi.Data3), 1, 0) ITENS_P' +
        'gVencto,'
      'IF((loi.Quitado<2) AND (loi.DDeposito< SYSDATE()), 1, 0) +'
      
        'IF((loi.Quitado>1) AND (loi.DDeposito< loi.Data3), 1, 0) ITENS_D' +
        'evolvid,'
      
        'IF((loi.Quitado>1) AND (loi.DDeposito< loi.Data3), 1, 0) ITENS_D' +
        'evPgTot,'
      
        'IF((loi.Quitado<2) AND (loi.DDeposito< SYSDATE()), 1, 0) ITENS_D' +
        'evNaoQt,'
      '(loi.TotalPg - loi.TotalJr + loi.TotalDs) Pago,'
      'IF(loi.DDeposito < SYSDATE(), loi.Valor, 0) EXPIRADO,'
      'IF(loi.DDeposito < SYSDATE(), 0, loi.Valor) AEXPIRAR,'
      'IF((loi.DDeposito<SYSDATE()) AND ((loi.Data3<2)'
      '  OR (loi.DDeposito<loi.Data3)), loi.Valor, 0) VENCIDO,'
      'IF((loi.Quitado>1) AND (loi.DDeposito>=loi.Data3),'
      '  loi.TotalPg - loi.TotalJr + loi.TotalDs, 0) PGDDEPOSITO,'
      'IF((loi.DDeposito< loi.Data3),'
      '  loi.TotalPg - loi.TotalJr + loi.TotalDs, 0) PGATRAZO,'
      'IF((loi.Quitado< 2)AND (loi.DDeposito< SYSDATE()),'
      
        '  loi.Valor - (loi.TotalPg - loi.TotalJr) + loi.TotalDs, 0) PGAB' +
        'ERTO,'
      '"D" Tipo,'
      'MONTH(loi.DDeposito)+YEAR(loi.DDeposito)*100+0.00 PERIODO,'
      
        'CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial ELSE cli.Nome END NOME' +
        'CLI,'
      'ban.Nome NOMEBANCO, loi.DDeposito, loi.Emitente NOMEEMITENTE,'
      'loi.Banco, loi.Agencia, loi.Conta, loi.Cheque, loi.Duplicata'
      'FROM lot esits loi'
      'LEFT JOIN lot es     lot ON lot.Codigo=loi.Codigo'
      'LEFT JOIN entidades cli ON cli.Codigo=lot.Cliente'
      'LEFT JOIN bancos    ban ON ban.Codigo=loi.Banco'
      'WHERE lot.Tipo=1'
      'AND loi.DDeposito  BETWEEN "2006/05/18" AND "2007/05/18"'
      ''
      'AND (IF(loi.DDeposito > SYSDATE(), 1, 0) +'
      'IF((loi.Quitado>1) AND (loi.DDeposito>=loi.Data3), 2, 0) +'
      
        'IF((loi.Quitado>1) AND (loi.DDeposito< loi.Data3), 4, 0) ) in (0' +
        ',1,2,4)'
      ''
      'ORDER BY ITENS_XXX'
      '*/'
      'SELECT lct.FatParcela, lct.Credito, 1 ITENS,'
      ''
      '(IF(lct.DDeposito > SYSDATE(), 1, 0) +'
      'IF((lct.DDeposito < SYSDATE()), 2, 0) -'
      
        'IF((lct.DDeposito < SYSDATE()) AND ((chd.Status+10) > 0), 2, 0) ' +
        '+'
      
        'IF((lct.DDeposito < SYSDATE()) AND ((chd.Status+10) = 12), 4, 0)' +
        ')'
      'ITENS_XXX,'
      ''
      'IF(lct.DDeposito > SYSDATE(), 1, 0) ITENS_AVencer,'
      'IF((lct.DDeposito < SYSDATE()), 1, 0) ITENS_Vencidos,'
      'IF((lct.DDeposito < SYSDATE()), 1, 0) -'
      'IF((lct.DDeposito < SYSDATE()) AND ((chd.Status+10) > 0),'
      '1, 0) ITENS_PgVencto,'
      'IF((lct.DDeposito < SYSDATE()) AND ((chd.Status+10) > 0),'
      '1, 0) ITENS_Devolvid,'
      'IF((lct.DDeposito < SYSDATE()) AND ((chd.Status+10) = 12),'
      '1, 0) ITENS_DevPgTot,'
      
        'IF((lct.DDeposito < SYSDATE()) AND ((chd.Status+10) in (10, 11))' +
        ','
      '1, 0) ITENS_DevNaoQt,'
      '(chd.ValPago - chd.Taxas - chd.JurosV + chd.Desconto) Pago,'
      'IF(lct.DDeposito < SYSDATE(), lct.Credito, 0) EXPIRADO,'
      'IF(lct.DDeposito < SYSDATE(), 0, lct.Credito) AEXPIRAR,'
      'IF(lct.DDeposito < SYSDATE() AND (chd.Status+10) > 0,'
      
        'lct.Credito - IF(lct.DDeposito < SYSDATE(), 0, lct.Credito), 0) ' +
        'VENCIDO,'
      'IF(lct.DDeposito < SYSDATE() AND (chd.Status+10) > 0, 0,'
      
        'lct.Credito - IF(lct.DDeposito < SYSDATE(), 0, lct.Credito)) PGD' +
        'DEPOSITO,'
      'IF(lct.DDeposito < SYSDATE() AND (chd.Status+10) > 0,'
      'chd.ValPago - chd.Taxas - chd.JurosV + chd.Desconto,0) PGATRAZO,'
      'IF(lct.DDeposito < SYSDATE() AND (chd.Status+10) > 0,'
      'lct.Credito - (chd.ValPago - chd.Taxas - chd.JurosV) +'
      'chd.Desconto, 0) PGABERTO, "C" Tipo,'
      'MONTH(lct.DDeposito)+YEAR(lct.DDeposito)*100+0.00 PERIODO,'
      
        'CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial ELSE cli.Nome END NOME' +
        'CLI,'
      'ban.Nome NOMEBANCO, lct.DDeposito, lct.Emitente NOMEEMITENTE, '
      
        'lct.Banco, lct.Agencia, lct.ContaCorrente, lct.Documento, lct.Du' +
        'plicata'
      'FROM lct0001a lct'
      'LEFT JOIN lot es     lot ON lot.Codigo=lct.FatNum'
      'LEFT JOIN entidades cli ON cli.Codigo=lot.Cliente'
      'LEFT JOIN alinits   chd ON chd.ChequeOrigem=lct.FatParcela'
      'LEFT JOIN bancos    ban ON ban.Codigo=lct.Banco'
      'WHERE lct.FatID=301'
      'AND lot.Tipo <> 1'
      'AND lot.Codigo > 0'
      'AND lot.TxCompra + lot.ValValorem+ 1 >= 0.01'
      
        'AND lct.DDeposito  BETWEEN "2011-05-23" AND "2012-11-18 23:59:59' +
        '"'
      'AND lot.Cliente=267'
      'AND lct.CNPJCPF="58171754015"'
      ''
      'AND (IF(lct.DDeposito > SYSDATE(), 1, 0) +'
      'IF((lct.DDeposito < SYSDATE()), 2, 0) -'
      
        'IF((lct.DDeposito < SYSDATE()) AND ((chd.Status+10) > 0), 2, 0) ' +
        '+'
      
        'IF((lct.DDeposito < SYSDATE()) AND ((chd.Status+10) = 12), 4, 0)' +
        ')'
      ' IN (1, -1000, 4, 0)'
      ''
      'UNION'
      'SELECT lct.FatParcela, lct.Credito, 1 ITENS,'
      ''
      '(IF(lct.DDeposito > SYSDATE(), 1, 0) +'
      'IF((lct.Quitado>1) AND (lct.DDeposito>=lct.Data3), 2, 0) +'
      'IF((lct.Quitado>1) AND (lct.DDeposito< lct.Data3), 4, 0) )'
      'ITENS_XXX,'
      ''
      'IF(lct.DDeposito > SYSDATE(), 1, 0) ITENS_AVencer,'
      'IF((lct.DDeposito < SYSDATE()), 1, 0) ITENS_Vencidos,'
      
        'IF((lct.Quitado>1) AND (lct.DDeposito>=lct.Data3), 1, 0) ITENS_P' +
        'gVencto,'
      'IF((lct.Quitado<2) AND (lct.DDeposito< SYSDATE()), 1, 0) +'
      
        'IF((lct.Quitado>1) AND (lct.DDeposito< lct.Data3), 1, 0) ITENS_D' +
        'evolvid,'
      
        'IF((lct.Quitado>1) AND (lct.DDeposito< lct.Data3), 1, 0) ITENS_D' +
        'evPgTot,'
      
        'IF((lct.Quitado<2) AND (lct.DDeposito< SYSDATE()), 1, 0) ITENS_D' +
        'evNaoQt,'
      '(lct.TotalPg - lct.TotalJr + lct.TotalDs) Pago,'
      'IF(lct.DDeposito < SYSDATE(), lct.Credito, 0) EXPIRADO,'
      'IF(lct.DDeposito < SYSDATE(), 0, lct.Credito) AEXPIRAR,'
      'IF((lct.DDeposito<SYSDATE()) AND ((lct.Data3<2)'
      '  OR (lct.DDeposito<lct.Data3)), lct.Credito, 0) VENCIDO,'
      'IF((lct.Quitado>1) AND (lct.DDeposito>=lct.Data3),'
      '  lct.TotalPg - lct.TotalJr + lct.TotalDs, 0) PGDDEPOSITO,'
      'IF(/*(lct.Quitado> 1)AND*/ (lct.DDeposito< lct.Data3),'
      '  lct.TotalPg - lct.TotalJr + lct.TotalDs, 0) PGATRAZO,'
      'IF((lct.Quitado< 2)AND (lct.DDeposito< SYSDATE()),'
      
        '  lct.Credito - (lct.TotalPg - lct.TotalJr) + lct.TotalDs, 0) PG' +
        'ABERTO, '
      
        '"D" Tipo, MONTH(lct.DDeposito)+YEAR(lct.DDeposito)*100+0.00 PERI' +
        'ODO,'
      
        'CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial ELSE cli.Nome END NOME' +
        'CLI,'
      'ban.Nome NOMEBANCO, lct.DDeposito, lct.Emitente NOMEEMITENTE,'
      
        'lct.Banco, lct.Agencia, lct.ContaCorrente, lct.Documento, lct.Du' +
        'plicata'
      'FROM lct0001a lct'
      'LEFT JOIN lot es     lot ON lot.Codigo=lct.FatNum'
      'LEFT JOIN entidades cli ON cli.Codigo=lot.Cliente'
      'LEFT JOIN bancos    ban ON ban.Codigo=lct.Banco'
      'WHERE lct.FatID=301'
      'AND lot.Tipo=1'
      'AND lot.TxCompra + lot.ValValorem+ 1 >= 0.01'
      'AND lot.Codigo > 0'
      
        'AND lct.DDeposito  BETWEEN "2011-05-23" AND "2012-11-18 23:59:59' +
        '"'
      'AND lot.Cliente=267'
      'AND lct.CNPJCPF="58171754015"'
      ''
      'AND (IF(lct.DDeposito > SYSDATE(), 1, 0) +'
      'IF((lct.Quitado>1) AND (lct.DDeposito>=lct.Data3), 2, 0) +'
      'IF((lct.Quitado>1) AND (lct.DDeposito< lct.Data3), 4, 0) )'
      ' IN (1, -1000, 4, 0)'
      ''
      'ORDER BY DDeposito '
      ''
      '')
    Left = 568
    Top = 440
    object QrItensITENS: TLargeintField
      FieldName = 'ITENS'
      Origin = 'ITENS'
      Required = True
    end
    object QrItensITENS_AVencer: TLargeintField
      FieldName = 'ITENS_AVencer'
      Origin = 'ITENS_AVencer'
      Required = True
    end
    object QrItensITENS_Vencidos: TLargeintField
      FieldName = 'ITENS_Vencidos'
      Origin = 'ITENS_Vencidos'
      Required = True
    end
    object QrItensITENS_PgVencto: TLargeintField
      FieldName = 'ITENS_PgVencto'
      Origin = 'ITENS_PgVencto'
      Required = True
    end
    object QrItensITENS_Devolvid: TLargeintField
      FieldName = 'ITENS_Devolvid'
      Origin = 'ITENS_Devolvid'
      Required = True
    end
    object QrItensITENS_DevPgTot: TLargeintField
      FieldName = 'ITENS_DevPgTot'
      Origin = 'ITENS_DevPgTot'
      Required = True
    end
    object QrItensITENS_DevNaoQt: TLargeintField
      FieldName = 'ITENS_DevNaoQt'
      Origin = 'ITENS_DevNaoQt'
      Required = True
    end
    object QrItensPago: TFloatField
      FieldName = 'Pago'
      Origin = 'Pago'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrItensEXPIRADO: TFloatField
      FieldName = 'EXPIRADO'
      Origin = 'EXPIRADO'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrItensAEXPIRAR: TFloatField
      FieldName = 'AEXPIRAR'
      Origin = 'AEXPIRAR'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrItensVENCIDO: TFloatField
      FieldName = 'VENCIDO'
      Origin = 'VENCIDO'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrItensPGDDEPOSITO: TFloatField
      FieldName = 'PGDDEPOSITO'
      Origin = 'PGDDEPOSITO'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrItensPGATRAZO: TFloatField
      FieldName = 'PGATRAZO'
      Origin = 'PGATRAZO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrItensPGABERTO: TFloatField
      FieldName = 'PGABERTO'
      Origin = 'PGABERTO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrItensPERIODO: TFloatField
      FieldName = 'PERIODO'
      Origin = 'PERIODO'
    end
    object QrItensNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Origin = 'NOMECLI'
      Size = 100
    end
    object QrItensNOMEBANCO: TWideStringField
      FieldName = 'NOMEBANCO'
      Origin = 'NOMEBANCO'
      Size = 100
    end
    object QrItensDDeposito: TDateField
      FieldName = 'DDeposito'
      Origin = 'DDeposito'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrItensNOMEEMITENTE: TWideStringField
      FieldName = 'NOMEEMITENTE'
      Origin = 'NOMEEMITENTE'
      Size = 30
    end
    object QrItensNOMESTATUS: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESTATUS'
      Size = 50
      Calculated = True
    end
    object QrItensBanco: TIntegerField
      FieldName = 'Banco'
      Origin = 'Banco'
      Required = True
    end
    object QrItensDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Origin = 'Duplicata'
      Required = True
      Size = 12
    end
    object QrItensITENS_XXX: TLargeintField
      FieldName = 'ITENS_XXX'
      Origin = 'ITENS_XXX'
      Required = True
    end
    object QrItensFatParcela: TIntegerField
      FieldName = 'FatParcela'
      Origin = 'FatParcela'
    end
    object QrItensCredito: TFloatField
      FieldName = 'Credito'
      Origin = 'Credito'
    end
    object QrItensAgencia: TIntegerField
      FieldName = 'Agencia'
      DisplayFormat = '0000'
    end
    object QrItensContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Origin = 'ContaCorrente'
      Size = 15
    end
    object QrItensTipo: TWideStringField
      FieldName = 'Tipo'
      Origin = 'Tipo'
      Required = True
      Size = 1
    end
    object QrItensDOCUM_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DOCUM_TXT'
      Calculated = True
    end
    object QrItensDocumento: TFloatField
      FieldName = 'Documento'
      Origin = 'Documento'
    end
  end
  object DsItens: TDataSource
    DataSet = QrItens
    Left = 596
    Top = 440
  end
  object QrTudo: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COUNT(FatParcela) ITENS,'
      'SUM(Credito) VALOR'
      'FROM lct0001a'
      'WHERE FatID=301')
    Left = 340
    Top = 420
    object QrTudoITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
    object QrTudoVALOR: TFloatField
      FieldName = 'VALOR'
    end
  end
  object QrSomaB: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      '/*'
      'SELECT SUM(loi.Valor) Valor, COUNT(loi.Controle) ITENS,'
      'SUM(IF(loi.DDeposito > SYSDATE(), 1, 0)) ITENS_AVencer,'
      'SUM(IF((loi.DDeposito < SYSDATE()), 1, 0)) ITENS_Vencidos,'
      
        'SUM(IF((loi.Quitado>1) AND (loi.DDeposito>=loi.Data3), 1, 0)) IT' +
        'ENS_PgVencto,'
      'SUM(IF((loi.Quitado<2) AND (loi.DDeposito< SYSDATE()), 1, 0)) +'
      
        'SUM(IF((loi.Quitado>1) AND (loi.DDeposito< loi.Data3), 1, 0)) IT' +
        'ENS_Devolvid,'
      
        'SUM(IF((loi.Quitado>1) AND (loi.DDeposito< loi.Data3), 1, 0)) IT' +
        'ENS_DevPgTot,'
      
        'SUM(IF((loi.Quitado<2) AND (loi.DDeposito< SYSDATE()), 1, 0)) IT' +
        'ENS_DevNaoQt,'
      'SUM((loi.TotalPg - loi.TotalJr + loi.TotalDs)) Pago,'
      'SUM(IF(loi.DDeposito < SYSDATE(), loi.Valor, 0)) EXPIRADO,'
      'SUM(IF(loi.DDeposito < SYSDATE(), 0, loi.Valor)) AEXPIRAR,'
      'SUM(IF((loi.DDeposito<SYSDATE()) AND ((loi.Data3<2)'
      '  OR (loi.DDeposito<loi.Data3)), loi.Valor, 0)) VENCIDO,'
      'SUM(IF((loi.Quitado>1) AND (loi.DDeposito>=loi.Data3),'
      '  loi.TotalPg - loi.TotalJr + loi.TotalDs, 0)) PGDDEPOSITO,'
      'SUM(IF((loi.DDeposito< loi.Data3),'
      '  loi.TotalPg - loi.TotalJr + loi.TotalDs, 0)) PGATRAZO,'
      'SUM(IF((loi.Quitado< 2)AND (loi.DDeposito< SYSDATE()),'
      
        '  loi.Valor - (loi.TotalPg - loi.TotalJr) + loi.TotalDs, 0)) PGA' +
        'BERTO'
      'FROM lot esits loi'
      'LEFT JOIN lot es     lot ON lot.Codigo=loi.Codigo'
      'LEFT JOIN entidades cli ON cli.Codigo=lot.Cliente'
      'LEFT JOIN bancos    ban ON ban.Codigo=loi.Banco'
      'WHERE lot.Tipo=1'
      '*/'
      ''
      'SELECT SUM(lct.Credito) Valor, COUNT(lct.FatParcela) ITENS,'
      'SUM(IF(lct.DDeposito > SYSDATE(), 1, 0)) ITENS_AVencer,'
      'SUM(IF((lct.DDeposito < SYSDATE()), 1, 0)) ITENS_Vencidos,'
      
        'SUM(IF((lct.Quitado>1) AND (lct.DDeposito>=lct.Data3), 1, 0)) IT' +
        'ENS_PgVencto,'
      'SUM(IF((lct.Quitado<2) AND (lct.DDeposito< SYSDATE()), 1, 0)) +'
      
        'SUM(IF((lct.Quitado>1) AND (lct.DDeposito< lct.Data3), 1, 0)) IT' +
        'ENS_Devolvid,'
      
        'SUM(IF((lct.Quitado>1) AND (lct.DDeposito< lct.Data3), 1, 0)) IT' +
        'ENS_DevPgTot,'
      
        'SUM(IF((lct.Quitado<2) AND (lct.DDeposito< SYSDATE()), 1, 0)) IT' +
        'ENS_DevNaoQt,'
      'SUM((lct.TotalPg - lct.TotalJr + lct.TotalDs)) Pago,'
      'SUM(IF(lct.DDeposito < SYSDATE(), lct.Credito, 0)) EXPIRADO,'
      'SUM(IF(lct.DDeposito < SYSDATE(), 0, lct.Credito)) AEXPIRAR,'
      'SUM(IF((lct.DDeposito<SYSDATE()) AND ((lct.Data3<2)'
      '  OR (lct.DDeposito<lct.Data3)), lct.Credito, 0)) VENCIDO,'
      'SUM(IF((lct.Quitado>1) AND (lct.DDeposito>=lct.Data3),'
      '  lct.TotalPg - lct.TotalJr + lct.TotalDs, 0)) PGDDEPOSITO,'
      'SUM(IF(/*(lct.Quitado> 1)AND*/ (lct.DDeposito< lct.Data3),'
      '  lct.TotalPg - lct.TotalJr + lct.TotalDs, 0)) PGATRAZO,'
      'SUM(IF((lct.Quitado< 2)AND (lct.DDeposito< SYSDATE()),'
      
        '  lct.Credito - (lct.TotalPg - lct.TotalJr) + lct.TotalDs, 0)) P' +
        'GABERTO'
      'FROM lct0001a lct'
      'LEFT JOIN lot es     lot ON lot.Codigo=lct.FatNum'
      'LEFT JOIN entidades cli ON cli.Codigo=lot.Cliente'
      'LEFT JOIN bancos    ban ON ban.Codigo=lct.Banco'
      'WHERE lct.FatID=301'
      'AND lot.Tipo=1'
      'AND lot.TxCompra + lot.ValValorem + 1 >= 0.01'
      'AND lot.Codigo > 0'
      'AND lct.DDeposito'
      'AND lot.Cliente=267'
      ''
      '')
    Left = 312
    Top = 420
    object QrSomaBValor: TFloatField
      FieldName = 'Valor'
    end
    object QrSomaBITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
    object QrSomaBITENS_AVencer: TFloatField
      FieldName = 'ITENS_AVencer'
    end
    object QrSomaBITENS_Vencidos: TFloatField
      FieldName = 'ITENS_Vencidos'
    end
    object QrSomaBITENS_PgVencto: TFloatField
      FieldName = 'ITENS_PgVencto'
    end
    object QrSomaBITENS_Devolvid: TFloatField
      FieldName = 'ITENS_Devolvid'
    end
    object QrSomaBITENS_DevPgTot: TFloatField
      FieldName = 'ITENS_DevPgTot'
    end
    object QrSomaBITENS_DevNaoQt: TFloatField
      FieldName = 'ITENS_DevNaoQt'
    end
    object QrSomaBPago: TFloatField
      FieldName = 'Pago'
    end
    object QrSomaBEXPIRADO: TFloatField
      FieldName = 'EXPIRADO'
    end
    object QrSomaBAEXPIRAR: TFloatField
      FieldName = 'AEXPIRAR'
    end
    object QrSomaBVENCIDO: TFloatField
      FieldName = 'VENCIDO'
    end
    object QrSomaBPGDDEPOSITO: TFloatField
      FieldName = 'PGDDEPOSITO'
    end
    object QrSomaBPGATRAZO: TFloatField
      FieldName = 'PGATRAZO'
    end
    object QrSomaBPGABERTO: TFloatField
      FieldName = 'PGABERTO'
    end
  end
  object QrGruSacEmiIts: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrGruSacEmiItsAfterOpen
    BeforeClose = QrGruSacEmiItsBeforeClose
    OnCalcFields = QrGruSacEmiItsCalcFields
    SQL.Strings = (
      'SELECT gse.Nome NOMEGRUPO, gei.* '
      'FROM grusacemiits gei'
      'LEFT JOIN grusacemi gse ON gse.Codigo=gei.Codigo'
      'WHERE gse.Codigo =:P0'
      'ORDER BY NOMEGRUPO, gei.Nome')
    Left = 348
    Top = 449
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGruSacEmiItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGruSacEmiItsCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 15
    end
    object QrGruSacEmiItsNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrGruSacEmiItsCNPJ_CPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_CPF_TXT'
      Size = 40
      Calculated = True
    end
    object QrGruSacEmiItsNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Size = 100
    end
  end
  object DsGruSacEmiIts: TDataSource
    DataSet = QrGruSacEmiIts
    Left = 376
    Top = 449
  end
  object QrPesqGru: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM grusacemiits'
      'WHERE CNPJ_CPF=:P0')
    Left = 404
    Top = 449
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesqGruCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrRevenda: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrRevendaCalcFields
    SQL.Strings = (
      
        'SELECT SUM(adp.Pago-adp.Juros) Pago, SUM((adp.Pago-adp.Juros)/ l' +
        'oi.Valor) Itens'
      'FROM adup pgs adp'
      'LEFT JOIN lot es its loi ON loi.Controle=adp.Lot esIts'
      'WHERE adp.LotePg>0')
    Left = 376
    Top = 420
    object QrRevendaPago: TFloatField
      FieldName = 'Pago'
      DisplayFormat = '#,###,##0.00'
    end
    object QrRevendaItens: TFloatField
      FieldName = 'Itens'
      DisplayFormat = '#,###,##0.0'
    end
    object QrRevendaPago_Perc: TFloatField
      FieldKind = fkCalculated
      FieldName = 'Pago_Perc'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrRevendaItens_Perc: TFloatField
      FieldKind = fkCalculated
      FieldName = 'Itens_Perc'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
  end
  object DsRevenda: TDataSource
    DataSet = QrRevenda
    Left = 404
    Top = 420
  end
  object frxHistorico: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39719.731659965300000000
    ReportOptions.LastChange = 39751.865779803240000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  Memo48.Visible := <VAR_MEDIADIAS>;'
      
        '  Memo50.Visible := <VAR_MEDIADIAS>;                            ' +
        '                    '
      'end.')
    OnGetValue = frxHistoricoGetValue
    Left = 624
    Top = 412
    Datasets = <
      item
        DataSet = frxDsItens
        DataSetName = 'frxDsItens'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsMediaDias
        DataSetName = 'frxDsMediaDias'
      end
      item
        DataSet = frxDsSomaA
        DataSetName = 'frxDsSomaA'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 5.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        Height = 196.000000000000000000
        Top = 18.897650000000000000
        Width = 737.008350000000000000
        object Memo32: TfrxMemoView
          Left = 610.000000000000000000
          Width = 108.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Picture1: TfrxPictureView
          Left = 2.000000000000000000
          Top = 0.661410000000000000
          Width = 168.000000000000000000
          Height = 64.000000000000000000
          ShowHint = False
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo49: TfrxMemoView
          Left = 174.000000000000000000
          Top = 0.661410000000000000
          Width = 432.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 43.000000000000000000
          Top = 76.661410000000000000
          Width = 20.000000000000000000
          Height = 112.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'ITENS')
          ParentFont = False
          Rotation = 90
        end
        object Memo7: TfrxMemoView
          Left = 63.000000000000000000
          Top = 76.661410000000000000
          Width = 100.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'A Expirar')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 163.000000000000000000
          Top = 76.661410000000000000
          Width = 52.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0;-0; '#39', <frxDsSomaA."SOMA_I_AVencer">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 215.000000000000000000
          Top = 76.661410000000000000
          Width = 40.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSomaA."PERC_AVencer"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 255.000000000000000000
          Top = 76.661410000000000000
          Width = 108.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '% dos itens pesquisados.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo68: TfrxMemoView
          Left = 63.000000000000000000
          Top = 92.661410000000000000
          Width = 100.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Expirados:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo69: TfrxMemoView
          Left = 63.000000000000000000
          Top = 124.661410000000000000
          Width = 100.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Vencidos/devolvidos:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo70: TfrxMemoView
          Left = 63.000000000000000000
          Top = 108.661410000000000000
          Width = 100.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Pagos em dia:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo71: TfrxMemoView
          Left = 63.000000000000000000
          Top = 140.661410000000000000
          Width = 100.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Revendidos')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo72: TfrxMemoView
          Left = 63.000000000000000000
          Top = 156.661410000000000000
          Width = 100.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Pendentes:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo74: TfrxMemoView
          Left = 63.000000000000000000
          Top = 172.661410000000000000
          Width = 100.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'TOTAL pesquisados')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo73: TfrxMemoView
          Left = 255.000000000000000000
          Top = 92.661410000000000000
          Width = 108.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '% dos itens pesquisados.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo75: TfrxMemoView
          Left = 255.000000000000000000
          Top = 108.661410000000000000
          Width = 108.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '% dos itens expirados.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo76: TfrxMemoView
          Left = 255.000000000000000000
          Top = 124.661410000000000000
          Width = 108.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '% dos itens expirados.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo77: TfrxMemoView
          Left = 255.000000000000000000
          Top = 140.661410000000000000
          Width = 108.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '% dos itens vencidos.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo78: TfrxMemoView
          Left = 255.000000000000000000
          Top = 156.661410000000000000
          Width = 108.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '% dos itens vencidos.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo79: TfrxMemoView
          Left = 255.000000000000000000
          Top = 172.661410000000000000
          Width = 108.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '% dos TODOS itens.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo80: TfrxMemoView
          Left = 163.000000000000000000
          Top = 92.661410000000000000
          Width = 52.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0;-0; '#39', <frxDsSomaA."SOMA_I_Vencidos">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo81: TfrxMemoView
          Left = 215.000000000000000000
          Top = 92.661410000000000000
          Width = 40.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSomaA."PERC_Vencidos"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo82: TfrxMemoView
          Left = 163.000000000000000000
          Top = 108.661410000000000000
          Width = 52.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0;-0; '#39', <frxDsSomaA."SOMA_I_PgVencto">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo83: TfrxMemoView
          Left = 215.000000000000000000
          Top = 108.661410000000000000
          Width = 40.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSomaA."PERC_PgVencto"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo84: TfrxMemoView
          Left = 163.000000000000000000
          Top = 124.661410000000000000
          Width = 52.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0;-0; '#39', <frxDsSomaA."SOMA_I_Devolvid">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo85: TfrxMemoView
          Left = 215.000000000000000000
          Top = 124.661410000000000000
          Width = 40.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSomaA."PERC_Devolvid"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo86: TfrxMemoView
          Left = 163.000000000000000000
          Top = 140.661410000000000000
          Width = 52.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0;-0; '#39', <frxDsSomaA."SOMA_I_DevPgTot">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo87: TfrxMemoView
          Left = 215.000000000000000000
          Top = 140.661410000000000000
          Width = 40.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSomaA."PERC_DevPgTot"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo88: TfrxMemoView
          Left = 163.000000000000000000
          Top = 156.661410000000000000
          Width = 52.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0;-0; '#39', <frxDsSomaA."SOMA_I_DevNaoQt">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo89: TfrxMemoView
          Left = 215.000000000000000000
          Top = 156.661410000000000000
          Width = 40.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSomaA."PERC_DevNaoQt"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo90: TfrxMemoView
          Left = 163.000000000000000000
          Top = 172.661410000000000000
          Width = 52.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0;-0; '#39', <frxDsSomaA."SOMA_I_ITENS">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo91: TfrxMemoView
          Left = 215.000000000000000000
          Top = 172.661410000000000000
          Width = 40.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSomaA."PERC_ITENS"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 374.000000000000000000
          Top = 76.661410000000000000
          Width = 20.000000000000000000
          Height = 112.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'VALORES')
          ParentFont = False
          Rotation = 90
        end
        object Memo17: TfrxMemoView
          Left = 394.000000000000000000
          Top = 76.661410000000000000
          Width = 100.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'A Expirar')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Left = 494.000000000000000000
          Top = 76.661410000000000000
          Width = 76.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSomaA."SOMA_V_AEXPIRAR"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo54: TfrxMemoView
          Left = 570.000000000000000000
          Top = 76.661410000000000000
          Width = 40.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSomaA."PERC_AExpirar"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo55: TfrxMemoView
          Left = 610.000000000000000000
          Top = 76.661410000000000000
          Width = 112.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '% dos valores pesquisados.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo92: TfrxMemoView
          Left = 394.000000000000000000
          Top = 92.661410000000000000
          Width = 100.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Expirados:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo93: TfrxMemoView
          Left = 394.000000000000000000
          Top = 124.661410000000000000
          Width = 100.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Vencidos/devolvidos:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo94: TfrxMemoView
          Left = 394.000000000000000000
          Top = 108.661410000000000000
          Width = 100.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Pagos em dia:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo95: TfrxMemoView
          Left = 394.000000000000000000
          Top = 140.661410000000000000
          Width = 100.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Revendidos')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo96: TfrxMemoView
          Left = 394.000000000000000000
          Top = 156.661410000000000000
          Width = 100.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Pendentes:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo97: TfrxMemoView
          Left = 394.000000000000000000
          Top = 172.661410000000000000
          Width = 100.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'TOTAL pesquisados')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo98: TfrxMemoView
          Left = 610.000000000000000000
          Top = 92.661410000000000000
          Width = 112.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '% dos valores pesquisados.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo99: TfrxMemoView
          Left = 610.000000000000000000
          Top = 108.661410000000000000
          Width = 112.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '% dos valores expirados.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo100: TfrxMemoView
          Left = 610.000000000000000000
          Top = 124.661410000000000000
          Width = 112.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '% dos valores expirados.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo101: TfrxMemoView
          Left = 610.000000000000000000
          Top = 140.661410000000000000
          Width = 112.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '% dos valores vencidos.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo102: TfrxMemoView
          Left = 610.000000000000000000
          Top = 156.661410000000000000
          Width = 112.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '% dos valores vencidos.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo103: TfrxMemoView
          Left = 610.000000000000000000
          Top = 172.661410000000000000
          Width = 112.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '% dos TODOS valores.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo104: TfrxMemoView
          Left = 494.000000000000000000
          Top = 92.661410000000000000
          Width = 76.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSomaA."SOMA_V_EXPIRADO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo105: TfrxMemoView
          Left = 570.000000000000000000
          Top = 92.661410000000000000
          Width = 40.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSomaA."PERC_Expirado"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo106: TfrxMemoView
          Left = 494.000000000000000000
          Top = 108.661410000000000000
          Width = 76.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSomaA."SOMA_V_PGDDEPOSITO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo107: TfrxMemoView
          Left = 570.000000000000000000
          Top = 108.661410000000000000
          Width = 40.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSomaA."PERC_PgDDeposito"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo108: TfrxMemoView
          Left = 494.000000000000000000
          Top = 124.661410000000000000
          Width = 76.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSomaA."SOMA_V_VENCIDO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo109: TfrxMemoView
          Left = 570.000000000000000000
          Top = 124.661410000000000000
          Width = 40.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSomaA."PERC_Vencido"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo110: TfrxMemoView
          Left = 494.000000000000000000
          Top = 140.661410000000000000
          Width = 76.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSomaA."SOMA_V_PGATRAZO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo111: TfrxMemoView
          Left = 570.000000000000000000
          Top = 140.661410000000000000
          Width = 40.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSomaA."PERC_Atrazo"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo112: TfrxMemoView
          Left = 494.000000000000000000
          Top = 156.661410000000000000
          Width = 76.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSomaA."SOMA_V_PGABERTO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo113: TfrxMemoView
          Left = 570.000000000000000000
          Top = 156.661410000000000000
          Width = 40.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSomaA."PERC_Aberto"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo114: TfrxMemoView
          Left = 494.000000000000000000
          Top = 172.661410000000000000
          Width = 76.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSomaA."SOMA_V_VALOR"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo115: TfrxMemoView
          Left = 570.000000000000000000
          Top = 172.661410000000000000
          Width = 40.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSomaA."PERC_VALOR"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Left = 174.000000000000000000
          Top = 20.661410000000000000
          Width = 40.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cliente:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Left = 174.000000000000000000
          Top = 38.661409999999990000
          Width = 40.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Emi/Sac:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 174.000000000000000000
          Top = 56.661409999999990000
          Width = 40.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Grupo:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Left = 214.000000000000000000
          Top = 20.661410000000000000
          Width = 496.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VARF_CLIENTE]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Left = 214.000000000000000000
          Top = 38.661409999999990000
          Width = 496.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VARF_EMISAC]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Left = 214.000000000000000000
          Top = 56.661410000000010000
          Width = 496.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VARF_GRUPO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 3.023624000000000000
          Top = 76.724459000000000000
          Width = 15.118110240000000000
          Height = 112.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'M'#233'dia de dias dos '#237'tens pesquisados')
          ParentFont = False
          Rotation = 90
        end
        object Memo50: TfrxMemoView
          Left = 18.433086000000000000
          Top = 76.724459000000000000
          Width = 15.118110240000000000
          Height = 112.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMediaDias."PRAZO_MEDIO"]')
          ParentFont = False
          Rotation = 90
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 25.763760000000000000
        Top = 483.779840000000000000
        Width = 737.008350000000000000
        object Memo53: TfrxMemoView
          Left = 474.000000000000000000
          Top = 1.763449999999978000
          Width = 236.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 14.000000000000000000
          Top = 1.763449999999978000
          Width = 276.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '** Tipo de documento: "C" = Cheque; "D"  = Duplicata')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Band4: TfrxMasterData
        Height = 16.000000000000000000
        Top = 362.834880000000000000
        Width = 737.008350000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsItens
        DataSetName = 'frxDsItens'
        RowCount = 0
        object Memo2: TfrxMemoView
          Left = 2.000000000000000000
          Width = 40.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsItens."DDeposito"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 42.000000000000000000
          Width = 64.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsItens."Credito"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 106.000000000000000000
          Width = 128.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsItens."DOCUM_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 234.000000000000000000
          Width = 40.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsItens."NOMESTATUS"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 274.000000000000000000
          Width = 64.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsItens."AEXPIRAR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 338.000000000000000000
          Width = 64.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsItens."PGDDEPOSITO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Left = 402.000000000000000000
          Width = 64.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsItens."PGATRAZO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 466.000000000000000000
          Width = 56.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsItens."PGABERTO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 522.000000000000000000
          Width = 100.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsItens."NOMEEMITENTE"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Left = 622.000000000000000000
          Width = 100.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsItens."NOMECLI"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 65.637600000000000000
        Top = 238.110390000000000000
        Width = 737.008350000000000000
        object Memo1: TfrxMemoView
          Left = 2.000000000000000000
          Top = 1.858069999999998000
          Width = 720.000000000000000000
          Height = 40.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'DETALHES DOS ITENS')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 2.000000000000000000
          Top = 49.637599999999990000
          Width = 40.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Dep'#243'sito')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Left = 42.000000000000000000
          Top = 49.637599999999990000
          Width = 64.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor doc.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 106.000000000000000000
          Top = 49.637599999999990000
          Width = 128.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Documento**')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Left = 234.000000000000000000
          Top = 49.637599999999990000
          Width = 40.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Status')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Left = 274.000000000000000000
          Top = 49.637599999999990000
          Width = 64.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'A Expirar')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Left = 338.000000000000000000
          Top = 49.637599999999990000
          Width = 64.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quitado')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 402.000000000000000000
          Top = 49.637599999999990000
          Width = 64.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Revendido')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 466.000000000000000000
          Top = 49.637599999999990000
          Width = 56.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pendente')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 522.000000000000000000
          Top = 49.637599999999990000
          Width = 100.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Emitente')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Left = 622.000000000000000000
          Top = 49.637599999999990000
          Width = 100.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cliente')
          ParentFont = False
          VAlign = vaCenter
        end
        object Check1: TfrxCheckBoxView
          Left = 10.000000000000000000
          Top = 21.858070000000030000
          Width = 16.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          CheckColor = clBlack
          CheckStyle = csCross
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
        end
        object Memo44: TfrxMemoView
          Left = 30.000000000000000000
          Top = 21.858070000000030000
          Width = 48.000000000000000000
          Height = 20.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'A Vencer')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 218.000000000000000000
          Top = 21.858070000000030000
          Width = 48.000000000000000000
          Height = 20.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Quitado')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Left = 458.000000000000000000
          Top = 21.858070000000030000
          Width = 48.000000000000000000
          Height = 20.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Revendido')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Left = 626.000000000000000000
          Top = 21.858070000000030000
          Width = 48.000000000000000000
          Height = 20.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Pendente')
          ParentFont = False
        end
        object Check2: TfrxCheckBoxView
          Left = 198.000000000000000000
          Top = 21.858070000000030000
          Width = 16.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          CheckColor = clBlack
          CheckStyle = csCross
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
        end
        object Check3: TfrxCheckBoxView
          Left = 438.000000000000000000
          Top = 21.858070000000030000
          Width = 16.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          CheckColor = clBlack
          CheckStyle = csCross
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
        end
        object Check4: TfrxCheckBoxView
          Left = 606.000000000000000000
          Top = 21.858070000000030000
          Width = 16.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          CheckColor = clBlack
          CheckStyle = csCross
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
        end
      end
      object Footer1: TfrxFooter
        Height = 24.000000000000000000
        Top = 400.630180000000000000
        Width = 737.008350000000000000
        object Memo31: TfrxMemoView
          Left = 42.000000000000000000
          Top = 3.779527560000000000
          Width = 64.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsItens."Credito">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          Left = 274.000000000000000000
          Top = 3.779527560000000000
          Width = 64.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsItens."AEXPIRAR">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          Left = 338.000000000000000000
          Top = 3.779527560000000000
          Width = 64.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsItens."PGDDEPOSITO">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Left = 402.000000000000000000
          Top = 3.779527560000000000
          Width = 64.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsItens."PGATRAZO">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          Left = 466.000000000000000000
          Top = 3.779527560000000000
          Width = 56.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsItens."PGABERTO">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Left = 2.000000000000000000
          Top = 3.779527559054998000
          Width = 40.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'TOTAL:')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxCheckBoxObject1: TfrxCheckBoxObject
    Left = 624
    Top = 440
  end
  object frxDsItens: TfrxDBDataset
    UserName = 'frxDsItens'
    CloseDataSource = False
    DataSet = QrItens
    BCDToCurrency = False
    Left = 540
    Top = 440
  end
  object frxDsSomaA: TfrxDBDataset
    UserName = 'frxDsSomaA'
    CloseDataSource = False
    DataSet = QrSomaA
    BCDToCurrency = False
    Left = 540
    Top = 468
  end
  object QrMediaDias: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(loi.Valor*loi.Dias)/SUM(loi.Valor) PRAZO_MEDIO'
      'FROM lot esits loi'
      'WHERE loi.Codigo=2236')
    Left = 568
    Top = 412
    object QrMediaDiasPRAZO_MEDIO: TFloatField
      FieldName = 'PRAZO_MEDIO'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsMediaDias: TDataSource
    DataSet = QrMediaDias
    Left = 596
    Top = 412
  end
  object frxDsMediaDias: TfrxDBDataset
    UserName = 'frxDsMediaDias'
    CloseDataSource = False
    DataSet = QrMediaDias
    BCDToCurrency = False
    Left = 540
    Top = 412
  end
end
