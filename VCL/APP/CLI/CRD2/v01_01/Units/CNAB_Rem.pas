unit CNAB_Rem;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, ComCtrls, dmkEdit, dmkLabel, Db,
  mySQLDbTables, Grids, RichEdit, dmkMemoBar, CheckLst, Mask, UnDmkProcFunc,
  DBCtrls, dmkEditDateTimePicker, dmkGeral, dmkImage, UnDmkEnums;

const
  MinColsArr = -2; MaxColsArr = 1000;

type
  dceAlinha = (posEsquerda, posCentro, posDireita);
  TArrayCols = array[MinColsArr..MaxColsArr] of Integer;
  TFmCNAB_Rem = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    TabSheet6: TTabSheet;
    TabSheet9: TTabSheet;
    Grade_5: TStringGrid;
    Panel9: TPanel;
    Grade_1: TStringGrid;
    Panel5: TPanel;
    Label10: TLabel;
    LaCodDmk_1: TLabel;
    Panel4: TPanel;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label7: TLabel;
    Label9: TLabel;
    dmkEd_0_000: TdmkEdit;
    dmkEd_0_005: TdmkEdit;
    dmkEd_0_006: TdmkEdit;
    dmkEd_0_003: TdmkEdit;
    dmkEd_0_004: TdmkEdit;
    dmkEd_0_402: TdmkEdit;
    dmkEd_0_990: TdmkEdit;
    dmkEd_0_410: TdmkEdit;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    dmkEd_0_020: TdmkEdit;
    dmkEd_0_021: TdmkEdit;
    dmkEd_0_022: TdmkEdit;
    dmkEd_0_023: TdmkEdit;
    dmkEd_0_024: TdmkEdit;
    dmkEdNomeBanco: TdmkEdit;
    dmkEd_0_001: TdmkEdit;
    Panel6: TPanel;
    Panel10: TPanel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Edit2: TEdit;
    Edit3: TEdit;
    Edit4: TEdit;
    Edit6: TEdit;
    PageControl2: TPageControl;
    TabSheet10: TTabSheet;
    Panel11: TPanel;
    Panel8: TPanel;
    Label15: TLabel;
    EdArqGerado: TEdit;
    MeGerado: TMemo;
    dmkMBGravado: TdmkMemoBar;
    TabSheet11: TTabSheet;
    Panel7: TPanel;
    Panel12: TPanel;
    Label28: TLabel;
    SpeedButton1: TSpeedButton;
    EdArqCompar: TEdit;
    MeCompar: TMemo;
    dmkMBCompar: TdmkMemoBar;
    OpenDialog1: TOpenDialog;
    TabSheet7: TTabSheet;
    Panel13: TPanel;
    Panel14: TPanel;
    Panel15: TPanel;
    Panel16: TPanel;
    GradeG: TStringGrid;
    GradeO: TStringGrid;
    GradeC: TStringGrid;
    Panel17: TPanel;
    Panel18: TPanel;
    BitBtn1: TBitBtn;
    Panel19: TPanel;
    dmkEdVerifLin: TdmkEdit;
    Label16: TLabel;
    dmkEdVerifCol: TdmkEdit;
    Label17: TLabel;
    GradeI: TStringGrid;
    GradeF: TStringGrid;
    Label18: TLabel;
    Label19: TLabel;
    dmkEdVerifValCompar: TdmkEdit;
    dmkEdVerifCampoCod: TdmkEdit;
    Label20: TLabel;
    dmkEdVerifValGerado: TdmkEdit;
    dmkEdVerifCampoTxt: TdmkEdit;
    Label21: TLabel;
    dmkEd_0_699: TdmkEdit;
    dmkEdVerifPos: TdmkEdit;
    Label22: TLabel;
    Label23: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBRadioGroup1: TDBRadioGroup;
    PB1: TProgressBar;
    Label24: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel20: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel21: TPanel;
    BtOK: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure dmkEd_0_001Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure Grade_1SelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure Edit2Change(Sender: TObject);
    procedure Edit3Change(Sender: TObject);
    procedure Edit4Change(Sender: TObject);
    procedure MeGeradoEnter(Sender: TObject);
    procedure MeGeradoExit(Sender: TObject);
    procedure MeComparEnter(Sender: TObject);
    procedure MeComparExit(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure GradeGDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure GradeGSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure PageControl2Change(Sender: TObject);
  private
    { Private declarations }
    FCols1, FCols5: TArrayCols;
    FLinArq: Integer;
    FEditor: TMemo;
    procedure PesquisaTexto;
    function NovaLinhaArq(): String;
    function Incrementa(const Grade: TStringGrid; var Contador: Integer;
             const Incremento: Integer; var ListaCols: TArrayCols;
             const MyCodField: Integer): Integer;
    procedure ConfGr(Grade: TStringGrid; Coluna, Tam: Integer;
              Titulo: String);
    function CriaListaCNAB(const CNAB, Banco, Registro, Posicoes: Integer; var Lista:
             MyArrayLista): Boolean;
    procedure VerificaCelula(Col, Row: Integer);
    function DAC_NossoNumero(const Bloqueto: Double; var Res: String): Boolean;
    function GetCNAB(): Integer;
  public
    { Public declarations }
    //LCCod: array[0..1000] of byte;
    //LCNome: array[0..1000] of String;
    //
    FLote, FSeqArqRem, FBanco: Integer;
    FColG1, FColG5,
    FLinG1, FLinG5,
    F1_000, F1_001, F1_011, F1_020, F1_021, F1_022, F1_023, F1_024,
    F1_400, F1_401,
    F1_501, F1_502, F1_504, F1_506, F1_507, F1_508, F1_509, F1_520, F1_550,
    F1_551, F1_552, F1_558, F1_569, F1_572, F1_573, F1_574, F1_575, F1_576,
    F1_577, F1_580, F1_583, F1_584, F1_586,
    F1_621, F1_639, F1_640,
    F1_701, F1_702, F1_711, F1_712,
    F1_801, F1_802, F1_803, F1_804, F1_805, F1_806, F1_807, F1_808, F1_811,
    F1_812, F1_813, F1_814, F1_853,
    F1_950,
    //
    F5_000, F5_011,
    F5_815, F5_850, F5_851, F5_852, F5_854, F5_855, F5_856, F5_857, F5_858,
    F5_861, F5_862, F5_863, F5_864: Integer;
    //
    function AddLinha(var Contador: Integer; const Grade: TStringGrid): Integer;
    procedure AddCampo(Grade: TStringGrid; Linha, Coluna: Integer;
              Formato: String; Valor: Variant);
    function AjustaString(const Texto, Compl: String; const Tamanho:
             Integer; const Ajusta: Boolean; var Res: String): Boolean;
    function ObtemNumeroDoArquivo(var Res: String): Boolean;
  end;

const
   _Ini = 0; _Fim = 1; _Tam=2; _Fil = 3; _Typ = 4; _Fmt = 5;
   _Fld = 6; _Def = 7; _Nom = 8; _Des = 9; _AjT = 10;

  var
  FmCNAB_Rem: TFmCNAB_Rem;

implementation

uses UnMyObjects, ModuleBco, UnBancos, UnBco_rem, Module, UMySQLModule;

{$R *.DFM}

procedure TFmCNAB_Rem.PesquisaTexto;
var
  Lin, Col, Tam: Integer;
begin
  case PageControl2.ActivePageIndex of
    0: FEditor := MeGerado;
    1: FEditor := MeCompar;
    else FEditor := nil;
  end;
  if FEditor = nil then Exit;
  Lin := Geral.IMV(Edit2.Text);
  Col := Geral.IMV(Edit3.Text);
  Tam := Geral.IMV(Edit4.Text);
  //
  if (Lin > 0) and (Col > 0) and (Tam > 0) then
  begin
    Edit6.Text := Copy(FEditor.Lines[Lin-1], Col, Tam);
  end else Edit6.Text := '';
end;

function TFmCNAB_Rem.AjustaString(const Texto, Compl: String;
const Tamanho: Integer; const Ajusta: Boolean; var Res: String): Boolean;
  function CompletaString(Texto, Compl: String; Tamanho: Integer;
    Alinhamento: dceAlinha): String;
  var
    Txt: String;
    Direita: Boolean;
  begin
    Direita := True;
    Txt := Texto;
    while Length(Txt) < Tamanho do
    begin
      case Alinhamento of
        posEsquerda: Txt := Txt + Compl;
        posCentro  :
        begin
          if Direita then
            Txt := Txt + Compl
          else
            Txt := Compl + Txt;
          Direita := not Direita;
        end;
        posDireita:  Txt := Compl + Txt;
      end;
    end;
    Result := Txt;
  end;
var
  Txt: String;
  Direita: Boolean;
  Alinhamento: dceAlinha;
  //Data: TDateTime;
begin
  //Result := False;
  Direita := True;
  if Compl = '0' then
    Alinhamento := posDireita
  else
    Alinhamento := posEsquerda;
  Txt := CompletaString(Texto, Compl, Tamanho, Alinhamento);
  if Ajusta then
  begin
    while Length(Txt) > Tamanho do
    begin
      case Alinhamento of
        posEsquerda: Txt := Copy(Txt, 1, Length(Txt)-1);
        posCentro  :
        begin
          if Direita then
            Txt := Copy(Txt, 2, Length(Txt)-1)
          else
            Txt := Copy(Txt, 1, Length(Txt)-1);
          Direita := not Direita;
        end;
        posDireita: Txt := Copy(Txt, 2, Length(Txt)-1);
      end;
    end;
  end;
  Res := Txt;
  Result := Length(Txt) = Tamanho;
end;

procedure TFmCNAB_Rem.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCNAB_Rem.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCNAB_Rem.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCNAB_Rem.dmkEd_0_001Change(Sender: TObject);
begin
  DmBco.QrBco.Close;
  DmBco.QrBco.Params[0].AsInteger := dmkEd_0_001.ValueVariant;
  UMyMod.AbreQuery(DmBco.QrBco, Dmod.MyDB);
  //
  dmkEdNomeBanco.Text := DmBco.QrBcoNome.Value;
end;

procedure TFmCNAB_Rem.FormCreate(Sender: TObject);
  function OrdenaColunas(var Colunas: array of Integer): Integer;
  var
    i, k: integer;
  begin
    k := 0;
    for i := Low(Colunas) to High(Colunas) do
    begin
      Colunas[i] := k;
      inc(k, 1);
    end;
    Result := k;
  end;
  var
    i: Integer;
begin
  // N�o aparecer nada preto
  GradeG.ColCount := 0;
  GradeG.RowCount := 0;
  //
  PageControl1.ActivePageIndex := 1;
  for i := 0 to MaxColsArr do
  begin
    FCols1[i] := 0; // Coluna 0
    FCols5[i] := 0; // Coluna 0
  end;
  FLinG1 := 0;
  FColG1 := 0;
  F1_011 := Incrementa(Grade_1, FColG1, 1, FCols1, 011);
  F1_400 := Incrementa(Grade_1, FColG1, 1, FCols1, 400);
  F1_401 := Incrementa(Grade_1, FColG1, 1, FCols1, 401);
  F1_020 := Incrementa(Grade_1, FColG1, 1, FCols1, 020);
  F1_022 := Incrementa(Grade_1, FColG1, 1, FCols1, 022);
  F1_021 := Incrementa(Grade_1, FColG1, 1, FCols1, 021);
  F1_023 := Incrementa(Grade_1, FColG1, 1, FCols1, 023);
  F1_024 := Incrementa(Grade_1, FColG1, 1, FCols1, 024);
  F1_506 := Incrementa(Grade_1, FColG1, 1, FCols1, 506);
  F1_501 := Incrementa(Grade_1, FColG1, 1, FCols1, 501);
  F1_509 := Incrementa(Grade_1, FColG1, 1, FCols1, 509);
  F1_508 := Incrementa(Grade_1, FColG1, 1, FCols1, 508);
  F1_504 := Incrementa(Grade_1, FColG1, 1, FCols1, 504);
  F1_502 := Incrementa(Grade_1, FColG1, 1, FCols1, 502);
  F1_580 := Incrementa(Grade_1, FColG1, 1, FCols1, 580);
  F1_550 := Incrementa(Grade_1, FColG1, 1, FCols1, 550);
  F1_001 := Incrementa(Grade_1, FColG1, 1, FCols1, 001);
  F1_507 := Incrementa(Grade_1, FColG1, 1, FCols1, 507);
  F1_520 := Incrementa(Grade_1, FColG1, 1, FCols1, 520);
  F1_583 := Incrementa(Grade_1, FColG1, 1, FCols1, 583);
  F1_701 := Incrementa(Grade_1, FColG1, 1, FCols1, 701);
  F1_702 := Incrementa(Grade_1, FColG1, 1, FCols1, 702);
  F1_711 := Incrementa(Grade_1, FColG1, 1, FCols1, 711);
  F1_712 := Incrementa(Grade_1, FColG1, 1, FCols1, 712);
  F1_950 := Incrementa(Grade_1, FColG1, 1, FCols1, 950);
  F1_572 := Incrementa(Grade_1, FColG1, 1, FCols1, 572);
  F1_574 := Incrementa(Grade_1, FColG1, 1, FCols1, 574);
  F1_576 := Incrementa(Grade_1, FColG1, 1, FCols1, 576);
  F1_573 := Incrementa(Grade_1, FColG1, 1, FCols1, 573);
  F1_575 := Incrementa(Grade_1, FColG1, 1, FCols1, 575);
  F1_577 := Incrementa(Grade_1, FColG1, 1, FCols1, 577);
  F1_801 := Incrementa(Grade_1, FColG1, 1, FCols1, 801);
  F1_802 := Incrementa(Grade_1, FColG1, 1, FCols1, 802);
  F1_803 := Incrementa(Grade_1, FColG1, 1, FCols1, 803);
  F1_811 := Incrementa(Grade_1, FColG1, 1, FCols1, 811);
  F1_812 := Incrementa(Grade_1, FColG1, 1, FCols1, 812);
  F1_813 := Incrementa(Grade_1, FColG1, 1, FCols1, 813);
  F1_814 := Incrementa(Grade_1, FColG1, 1, FCols1, 814);
  F1_805 := Incrementa(Grade_1, FColG1, 1, FCols1, 805);
  F1_806 := Incrementa(Grade_1, FColG1, 1, FCols1, 806);
  F1_807 := Incrementa(Grade_1, FColG1, 1, FCols1, 807);
  F1_808 := Incrementa(Grade_1, FColG1, 1, FCols1, 808);
  F1_853 := Incrementa(Grade_1, FColG1, 1, FCols1, 853);
  F1_584 := Incrementa(Grade_1, FColG1, 1, FCols1, 584);
  F1_586 := Incrementa(Grade_1, FColG1, 1, FCols1, 586);
  F1_552 := Incrementa(Grade_1, FColG1, 1, FCols1, 552);
  F1_569 := Incrementa(Grade_1, FColG1, 1, FCols1, 569);
  F1_551 := Incrementa(Grade_1, FColG1, 1, FCols1, 551);
  F1_558 := Incrementa(Grade_1, FColG1, 1, FCols1, 558);
  F1_621 := Incrementa(Grade_1, FColG1, 1, FCols1, 621);
  F1_639 := Incrementa(Grade_1, FColG1, 1, FCols1, 639);
  F1_640 := Incrementa(Grade_1, FColG1, 1, FCols1, 640);
  F1_804 := Incrementa(Grade_1, FColG1, 1, FCols1, 804);
  //
  ConfGr(Grade_1, F1_000, 032, 'Item');
  ConfGr(Grade_1, F1_011, 018, 'TR');
  ConfGr(Grade_1, F1_400, 018, 'TIE - Tipo inscr. empresa');
  ConfGr(Grade_1, F1_401, 112, 'Inscr. Empresa');
  ConfGr(Grade_1, F1_020, 032, 'Ag�n.');
  ConfGr(Grade_1, F1_022, 016, 'DV');
  ConfGr(Grade_1, F1_021, 072, 'Conta corrente');
  ConfGr(Grade_1, F1_023, 016, 'DV');
  ConfGr(Grade_1, F1_024, 020, 'DAC');
  ConfGr(Grade_1, F1_506, 072, 'Seu n�mero');
  ConfGr(Grade_1, F1_501, 056, 'Nosso n�mero');
  ConfGr(Grade_1, F1_509, 024, 'Carteira');
  ConfGr(Grade_1, F1_508, 024, 'C�d.Cart');
  ConfGr(Grade_1, F1_504, 024, 'Ocorrencia');
  ConfGr(Grade_1, F1_502, 072, 'Duplicata');
  ConfGr(Grade_1, F1_580, 056, 'Vencto');
  ConfGr(Grade_1, F1_550, 080, 'Valor doc.');
  ConfGr(Grade_1, F1_001, 024, 'Bco');
  ConfGr(Grade_1, F1_507, 020, 'Esp.T�t');
  ConfGr(Grade_1, F1_520, 012, 'Aceite');
  ConfGr(Grade_1, F1_583, 056, 'Emiss�o');
  ConfGr(Grade_1, F1_701, 020, 'I1 - instru��o 1');
  ConfGr(Grade_1, F1_711, 020, 'D1 - dias instru��o 1');
  ConfGr(Grade_1, F1_702, 020, 'I2 - instru��o 2');
  ConfGr(Grade_1, F1_712, 020, 'D2 - dias instru��o 2');
  ConfGr(Grade_1, F1_950, 020, 'Dias �nico');
  ConfGr(Grade_1, F1_572, 080, '$ mora dd');
  ConfGr(Grade_1, F1_573, 080, '$ multa');
  ConfGr(Grade_1, F1_574, 080, '% mora/m�s');
  ConfGr(Grade_1, F1_575, 080, '% multa');
  ConfGr(Grade_1, F1_576, 024, 'T.Mora');
  ConfGr(Grade_1, F1_577, 024, 'T.Multa');
  ConfGr(Grade_1, F1_801, 020, 'TIS - Tipo inscr. sacado');
  ConfGr(Grade_1, F1_802, 112, 'Inscr. Sacado');
  ConfGr(Grade_1, F1_803, 210, 'Nome do Sacado');
  ConfGr(Grade_1, F1_811, 048, 'T.logr.');
  ConfGr(Grade_1, F1_812, 140, 'Logradouro');
  ConfGr(Grade_1, F1_813, 035, 'N�');
  ConfGr(Grade_1, F1_814, 072, 'Complem.');
  ConfGr(Grade_1, F1_805, 084, 'Bairro');
  ConfGr(Grade_1, F1_806, 064, 'CEP');
  ConfGr(Grade_1, F1_807, 108, 'Cidade');
  ConfGr(Grade_1, F1_808, 020, 'UF');
  ConfGr(Grade_1, F1_853, 210, 'Nome do Sacador / avalista');
  ConfGr(Grade_1, F1_584, 056, 'Dt mora');
  ConfGr(Grade_1, F1_586, 056, 'Dt.lim.desconto');
  ConfGr(Grade_1, F1_552, 064, 'Val.Desconto');
  ConfGr(Grade_1, F1_569, 064, 'Val. IOF');
  ConfGr(Grade_1, F1_551, 064, 'Val.Abatimento');
  ConfGr(Grade_1, F1_804, 280, 'Logradouro completo');
  ConfGr(Grade_1, F1_621, 018, 'Quem imprime?');
  ConfGr(Grade_1, F1_639, 077, 'Msg 1 (237) 12 posi��es');
  ConfGr(Grade_1, F1_640, 200, 'Msg 2 (237) 60 posi��es');
  ConfGr(Grade_1, F1_558, 064, 'Val.Bonifica��o');
  //
  FLinG5 := 0;
  FColG5 := 0;
  F5_000 := Incrementa(Grade_5, FColG5, 1, FCols5, 000);
  F5_011 := Incrementa(Grade_5, FColG5, 1, FCols5, 011);
  F5_815 := Incrementa(Grade_5, FColG5, 1, FCols5, 815);
  F5_850 := Incrementa(Grade_5, FColG5, 1, FCols5, 850);
  F5_851 := Incrementa(Grade_5, FColG5, 1, FCols5, 851);
  F5_852 := Incrementa(Grade_5, FColG5, 1, FCols5, 852);
  F5_861 := Incrementa(Grade_5, FColG5, 1, FCols5, 861);
  F5_862 := Incrementa(Grade_5, FColG5, 1, FCols5, 862);
  F5_863 := Incrementa(Grade_5, FColG5, 1, FCols5, 863);
  F5_864 := Incrementa(Grade_5, FColG5, 1, FCols5, 864);
  F5_855 := Incrementa(Grade_5, FColG5, 1, FCols5, 855);
  F5_856 := Incrementa(Grade_5, FColG5, 1, FCols5, 856);
  F5_857 := Incrementa(Grade_5, FColG5, 1, FCols5, 857);
  F5_858 := Incrementa(Grade_5, FColG5, 1, FCols5, 858);
  F5_854 := Incrementa(Grade_5, FColG5, 1, FCols5, 854);
  //
  ConfGr(Grade_5, 000000, 032, 'Seq.');
  ConfGr(Grade_5, F5_000, 032, 'Item');
  ConfGr(Grade_5, F5_011, 018, 'TR');
  ConfGr(Grade_5, F5_815, 200, 'Emeio do sacado');
  ConfGr(Grade_5, F5_851, 020, 'TIS - Tipo inscr. sacador / avalista');
  ConfGr(Grade_5, F5_852, 112, 'Inscr. Sacador / avalista');
  ConfGr(Grade_5, F5_861, 048, 'T.logr. do Sacador / avalista');
  ConfGr(Grade_5, F5_862, 140, 'Logradouro do Sacador / avalista');
  ConfGr(Grade_5, F5_863, 035, 'N�do logradouro do Sacador / avalista');
  ConfGr(Grade_5, F5_864, 072, 'Complem. do endere�o do Sacador / avalista');
  ConfGr(Grade_5, F5_855, 084, 'Bairro do Sacador / avalista');
  ConfGr(Grade_5, F5_856, 064, 'CEP do Sacador / avalista');
  ConfGr(Grade_5, F5_857, 108, 'Cidade do Sacador / avalista');
  ConfGr(Grade_5, F5_858, 020, 'UF do Sacador / avalista');
  ConfGr(Grade_5, F5_854, 210, 'Logradouro completo do Sacador / avalista');
end;

function TFmCNAB_Rem.AddLinha(var Contador: Integer; const Grade: TStringGrid): Integer;
begin
  Contador       := Contador + 1;
  Grade.RowCount := Contador + 1;
  Grade.Cells[0,Contador] := FormatFloat('0000', Contador);
  //
  Result := Contador;
end;

procedure TFmCNAB_Rem.AddCampo(Grade: TStringGrid; Linha, Coluna: Integer;
              Formato: String; Valor: Variant);
var
  Texto: String;
begin
  Texto := MLAGeral.VariavelToTexto(Formato, Valor);
  Grade.Cells[Coluna, Linha] := Texto;
end;

function TFmCNAB_Rem.Incrementa(const Grade: TStringGrid; var Contador: Integer;
             const Incremento: Integer; var ListaCols: TArrayCols;
             const MyCodField: Integer): Integer;
begin
  inc(Contador, Incremento);
  Result := Contador;
  ListaCols[MyCodField] := Contador;
  if Grade.ColCount < Result + 1 then
    Grade.ColCount := Result + 1;
end;

procedure TFmCNAB_Rem.ConfGr(Grade: TStringGrid; Coluna, Tam: Integer;
Titulo: String);
begin
  Grade.ColWidths[Coluna] := Tam;
  Grade.Cells[Coluna,  0] := Titulo;
end;

procedure TFmCNAB_Rem.Grade_1SelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
var
  i: Integer;
begin
  for i := 1 to 1000 do
    if FCols1[i] = ACol then
    begin
      LaCodDmk_1.Caption := FormatFloat('000', i);
      Break;
    end;
end;

function TFmCNAB_Rem.NovaLinhaArq(): String;
begin
  inc(FLinArq, 1);
  Result := FormatFloat('0', FLinArq);
end;

procedure TFmCNAB_Rem.Edit2Change(Sender: TObject);
begin
  PesquisaTexto;
end;

procedure TFmCNAB_Rem.Edit3Change(Sender: TObject);
begin
  PesquisaTexto;
end;

procedure TFmCNAB_Rem.Edit4Change(Sender: TObject);
begin
  PesquisaTexto;
end;

procedure TFmCNAB_Rem.PageControl2Change(Sender: TObject);
begin
  PesquisaTexto;
end;

function TFmCNAB_Rem.CriaListaCNAB(const CNAB, Banco, Registro, Posicoes: Integer; var Lista:
MyArrayLista): Boolean;
var
  CNABResult: TCNABResult;
begin
  case CNAB of
    240: CNABResult := cresNoCNAB;
    400: CNABResult := UBco_Rem.LayoutCNAB400(Banco, Registro, Lista, Posicoes, '');
    else CNABResult := cresNoCNAB;
  end;
  if not (CNABResult in ([cresNoField, cresOKField])) then
  begin
    UBancos.CNABResult_Msg(CNABResult, CNAB, Banco, Registro, ecnabRemessa);
    Result := False;
  end else Result := True;
end;

procedure TFmCNAB_Rem.BtOKClick(Sender: TObject);
  function ValorDeColunaDeGrade(const Grade: TStringGrid; const Lista:
  MyArrayLista; const ItemLista: Integer; const FCols: TArrayCols;
  const LinhaGrade: Integer; var Res: String): Boolean;
  var
    CodiFld, NomeFld: String;
    Campo, Coluna: Integer;
  begin
    CodiFld := Lista[ItemLista][_Fld];
    Campo   := Geral.IMV(CodiFld);
    // Se o campo for DAC do "Nosso n�mero"  separado do seu DAC...
    if Campo = 511 then
    // obtem o valor do "Nosso n�mero"
      Coluna := FCols[501]
    //sen�o obtem o valor do campo normal
    else
      Coluna  := FCols[Campo];
    Res := '';
    //if Campo = 20 then
      //ShowMessage(IntToStr(Coluna));
    Result := True;
    case Campo of
       -02: Res := ' ';
       -01: Res := '0';
       000: Res := '';
       999: Res := NovaLinhaArq();
      else
      begin
        if Coluna > 0 then
        begin
          Res := Grade.Cells[Coluna, LinhaGrade];
          case Campo of
            // 400: Tipo de inscri��o cedente
            // 801: Tipo de inscricao sacado
            // 851: Tipo de inscricao sacador / avalista
            400, 801, 851: Result := UBco_Rem.TipoDeInscricao(Geral.IMV(Res), FBanco, GetCNAB(), Campo, '', Res);
            // Ocorr�ncia ( motivo de enviar o arquivo)
            504: Result := UBco_Rem.CodigoDeOcorrenciaRemessa(FBanco, 01, Res);
            // DAC do Nosso n�mero
            511: Result := DAC_NossoNumero(Geral.DMV(Res), Res);
            // Aceite do t�tulo
            520: Result := UBco_Rem.CodigoDeAceiteRemessa(FBanco, Geral.IMV(Res), '', Res);
            // Quem Imprime
            621: Result := UBco_Rem.QuemImprimeOBloqueto(FBanco, Geral.IMV(Res), '', Res);
          end;
          //Result := True;
        end else begin
          Result := False;
          case Campo of

            // CAMPOS N�O IMPEMENTADOS !!!!!

            // Ag�ncia cobradora
            025: Result := True;
            // Banco cobrador
            027: Result := True;
            // D�bito autom�tico no bradesco
            610..615,622,624: Result := True;
            // Rateio de cr�dito Bradesco
            623: Result := True;
            // Quantidade de moeda vari�vel
            749: Result := True;
            //C�digo de instru��o / alega��o (a ser cancelada)
            //   Parei aqui!!!! Fazer quando responder a  arquivos de retorno!!!!
            750: Result := True;
          end;
          if Result = False then
          begin
            // descobrir todos n�o implementados
            Result := True;
            //
            NomeFld := ' - "' + UBancos.DescricaoDoMeuCodigoCNAB(Campo) + '" ';
            Geral.MensagemBox('O c�digo de campo ' + CodiFld + NomeFld
            + ' n�o est� implementado! (Coluna ' + IntToStr(Coluna) + ')', 'Erro',
            MB_OK+MB_ICONERROR);
          end;  
        end;
      end;
    end;
  end;

  function ValorDeCampo(const Lista: MyArrayLista; Campo: Integer;
  const Pre_Def: String; var Res: String): Boolean;
  var
    Def, Fil, Typ, Formato, Texto: String;
    Len, Fld, i, Cas: Integer;
    Ajusta: Boolean;
    Data: TDateTime;
  begin
    Result := False;
    Def := Trim(Lista[Campo][_Def]);
    Fil := Lista[Campo][_Fil];
    Len := Geral.IMV(Lista[Campo][_Tam]);
    Fld := Geral.IMV(Lista[Campo][_Fld]);
    Ajusta := Uppercase(Lista[Campo][_AjT]) = 'S';
    //if Fld = 504 then
      //ShowMessage('504');
    Typ := Lista[Campo][_Typ];
    if Def <> '' then Texto := Def else
    begin
      if Pre_Def <> '' then Texto := Pre_def else
      begin
        // Parei Aqui buscar dados de grades
        Texto := '';
      end;

      //  Formata Data
      if Typ = 'D' then
      begin
        Data    := Geral.ValidaDataSimples(Texto, True);
        if Data = 0 then
        begin
          Texto := '';
          while Length(Texto) < Len do
            Texto := Texto + Fil;
        end else begin
          Formato := Uppercase(Trim(Lista[Campo][_Fmt]));
          if Length(Formato) = 0 then
          begin
            Result := False;
            Geral.MensagemBox('Data sem formata��o para o campo "' +
            Lista[Campo][_Nom] + '" em "[CNAB_Rem]ValorDeCampo"!', 'Erro',
            MB_OK+MB_ICONERROR);
            Exit;
          end;
          for i := 1 to Length(Formato) do
            if Formato[i] = 'A' then Formato[i] := 'Y';
          Texto := FormatDateTime(Formato, Data);
        end;
      end else
      //         Texto           Zeros          Brancos
      if (Typ = 'X') or  (Typ = 'Z') or (Typ = 'B') then
      begin
        Texto := Geral.SemAcento(Texto);
        Texto := Geral.Maiusculas(Texto, False);
      end else
      // Formata Inteiro
      if Typ = 'I' then
      begin
        Texto := Geral.SoNumero_TT(Texto);
      end else
      // Formata Inteiro
      if Typ = 'F' then
      begin
        Cas := Geral.IMV(Lista[Campo][_Fmt]);

        Texto := Trim(Texto);
        Texto := Geral.TFT(Texto, Cas, siPositivo);
        Texto := Geral.SoNumero_TT(Texto);
      end else
      begin
        Geral.MensagemBox('Tipo de dado n�o implementado: "' +
        Typ + '" em "CNAB_Rem.BtOKClick"!', 'Erro', MB_OK+MB_ICONERROR);
        Exit;
      end;
    end;

    {case Fld of
      402, 803, 853: Corrige := True;
      else Corrige := False;
    end;}
    //
    Result := AjustaString(Texto, Fil, Len, Ajusta, Res);
    if not Result then
      Geral.MensagemBox('O tamanho do texto "' + Res + '" ficou ' +
      'com tamanho = ' + IntToStr(Length(Res)) + ' que � diferente do ' +
      'estipulado, que � de ' + IntToStr(Len) + ' posi��es para o campo "' +
      FormatFloat('000', fld) + ' - ' + Trim(Lista[Campo][_Nom]) + '"!',
      'Erro', MB_OK+MB_ICONERROR);
  end;
  procedure AvisaCancelamento(Texto: String; TamCorreto, TamAtual: Integer);
  begin
    MeGerado.Lines.Add(Texto);
    Geral.MensagemBox('Gera��o de arquivo remessa cancelado!' +
    #13+#10 + 'A linha ' + IntToStr(MeGerado.Lines.Count) + ' deveria ter ' +
    IntToStr(TamCorreto) + ' caracteres, mas est� com ' + IntToStr(TamAtual) +
    '!', 'Erro', MB_OK+MB_ICONERROR);
  end;
  function RemoveQuebraDeLinha(Txt: String):String;
  var
    Texto: String;
    T, K: Integer;
  begin
    //Tirar o #13#10
    Texto := '';
    T := Length(Txt);
    for K := 1 to T do
    begin
      if K < T - DmBco.QrCNAB_CfgCNAB.Value + 2 then
        Texto := Texto + MeGerado.Text[K]
      else begin
        if not (Txt[K] in ([#10, #13])) then
        Texto := Texto + Txt[K];
      end;
    end;
    Result := Texto;
  end;
const
  // Compatibilidade (por causa do CNAB_Rem2)
  Posicoes = 0;
var
  i, j, T, K, Item, Lin1, Banco, CNAB, Campo: Integer;
  Envio: TEnvioCNAB;
  ListaA, ListaB: MyArrayLista;
  SeqArq, LinReg, ValCampo, Pre_Def, Arquivo, Texto: String;
  Continua: Boolean;
begin
  Screen.Cursor := crHourGlass;
  MeGerado.Clear;
  EdArqGerado.Text := '';
  FLinArq := 0;
  Banco := dmkEd_0_001.ValueVariant;
  CNAB := DmBco.QrCNAB_CfgCNAB.Value;
  case dmkEd_0_005.ValueVariant of
     1: Envio := ecnabRemessa;
     2: Envio := ecnabRetorno;
    else Envio := ecnabIndefinido;
  end;
  if not UBancos.BancoImplementado(Banco, CNAB, Envio) then begin Screen.Cursor := crDefault; Exit; end;
  //
  ////////////////////// 0 - REGISTRO HEADER ///////////////////////////////////
  if not CriaListaCNAB(CNAB, Banco, 0, Posicoes, ListaA) then
  begin Screen.Cursor := crDefault; Exit; end;
  LinReg := '';
  for i := Low(ListaA) to High(ListaA) do
  begin
    Campo := Geral.IMV(ListaA[i][_Fld]);
    case Campo of
      -02: Pre_Def := ' ';
      -01: Pre_def := '0';
      020: Pre_Def := dmkEd_0_020.Text;
      021: Pre_Def := dmkEd_0_021.Text;
      022: Pre_Def := dmkEd_0_022.Text;
      023: Pre_Def := dmkEd_0_023.Text;
      024: Pre_Def := dmkEd_0_024.Text;
      402: Pre_Def := dmkEd_0_402.Text;
      410: Pre_Def := dmkEd_0_410.Text;
      699: Pre_Def := dmkEd_0_699.Text;
      990: Pre_Def := dmkEd_0_990.Text;
      998: ObtemNumeroDoArquivo(Pre_Def);
      999: Pre_Def := NovaLinhaArq();
      else Pre_def := '';
    end;
    if not ValorDeCampo(ListaA, i, Pre_Def, ValCampo) then begin Screen.Cursor := crDefault; Exit; end;
    //
    LinReg := LinReg + ValCampo;
    if Length(LinReg) <> Geral.IMV(ListaA[i][_Fim]) then
    begin
      AvisaCancelamento(LinReg, Geral.IMV(ListaA[i][_Fim]), Length(LinReg));
      Screen.Cursor := crDefault;
      Exit;
    end;
  end;
  MeGerado.Lines.Add(LinReg);
  MeGerado.Text := RemoveQuebraDeLinha(MeGerado.Text);
  LinReg := '';
  ////////////////////// 1 - REGISTRO DETALHE OBRIGAT�RIO  /////////////////////
  if not CriaListaCNAB(CNAB, Banco, 1, Posicoes, ListaA) then
    begin Screen.Cursor := crDefault; Exit; end;
  ////////////////////// 5 - REGISTRO DETALHE OPCIONAL /////////////////////////
  if not CriaListaCNAB(CNAB, Banco, 5, Posicoes, ListaB) then
    begin Screen.Cursor := crDefault; Exit; end;
  for Item := 1 to Grade_1.RowCount - 1 do
  begin
    Lin1 := Geral.IMV(Grade_1.Cells[F1_000, Item]);
    if Lin1 <> Item then
    begin
      Geral.MensagemBox('A linha ' + intToStr(Item) + ' da grade 1 ' +
      ' n�o � "Item" = ' + intToStr(Item) + '!' + #13#10 + 'Gera��o de arquivo ' +
      'cancelada!', 'Erro', MB_OK+MB_ICONERROR);
      Screen.Cursor := crDefault;
      Exit;
    end;

    //  Grade 1 (Detalhe obrigat�rio)
    for i := Low(ListaA) to High(ListaA) do
    begin
      if not ValorDeColunaDeGrade(Grade_1, ListaA, i, FCols1, Item, Pre_Def) then
        begin Screen.Cursor := crDefault; Exit; end;
      if not ValorDeCampo(ListaA, i, Pre_Def, ValCampo) then
        begin Screen.Cursor := crDefault; Exit; end;
      //
      LinReg := LinReg + ValCampo;
      if Length(LinReg) <> Geral.IMV(ListaA[i][_Fim]) then
      begin
        AvisaCancelamento(LinReg, Geral.IMV(ListaA[i][_Fim]), Length(LinReg));
        Screen.Cursor := crDefault;
        Exit;
      end;
    end;
    MeGerado.Lines.Add(LinReg);
    LinReg := '';

    // Grade 5 (detalhe opcional) (Banco 341 tem mais algum?)
    // primeiro verifica se existe um detalhe 5 referente ao detalhe 1
    for j := 1 to Grade_5.RowCount -1 do
    begin
      if Geral.IMV(Grade_5.Cells[F5_000, j]) = Lin1 then
      begin
        for i := Low(ListaB) to High(ListaB) do
        begin
          if not ValorDeColunaDeGrade(Grade_5, ListaB, i, FCols5, Item, Pre_Def) then
            begin Screen.Cursor := crDefault; Exit; end;
          if not ValorDeCampo(ListaB, i, Pre_Def, ValCampo) then
            begin Screen.Cursor := crDefault; Exit; end;
          //
          LinReg := LinReg + ValCampo;
          if Length(LinReg) <> Geral.IMV(ListaB[i][_Fim]) then
          begin
            AvisaCancelamento(LinReg, Geral.IMV(ListaB[i][_Fim]), Length(LinReg));
            Screen.Cursor := crDefault;
            Exit;
          end;
        end;
        MeGerado.Lines.Add(LinReg);
        //
        MeGerado.Text := RemoveQuebraDeLinha(MeGerado.Text);
        LinReg := '';
      end;
    end;
  end;
  ////////////////////// 9 - REGISTRO TRAILLER  ////////////////////////////////
  if not CriaListaCNAB(CNAB, Banco, 9, Posicoes, ListaA) then
    begin Screen.Cursor := crDefault; Exit; end;
  for i := Low(ListaA) to High(ListaA) do
  begin
    Campo := Geral.IMV(ListaA[i][_Fld]);
    case Campo of
      -02: Pre_Def := ' ';
      -01: Pre_def := '0';
      999: Pre_Def := NovaLinhaArq();
      else Pre_def := '';
    end;
    if not ValorDeCampo(ListaA, i, Pre_Def, ValCampo) then begin Screen.Cursor := crDefault; Exit; end;
    //
    LinReg := LinReg + ValCampo;
    if Length(LinReg) <> Geral.IMV(ListaA[i][_Fim]) then
    begin
      AvisaCancelamento(LinReg, Geral.IMV(ListaA[i][_Fim]), Length(LinReg));
      Screen.Cursor := crDefault;
      Exit;
    end;
  end;
  MeGerado.Lines.Add(LinReg);
  LinReg := '';

  //
  ObtemNumeroDoArquivo(SeqArq);
  Arquivo := dmkPF.CaminhoArquivo(DmBco.QrCNAB_CfgDiretorio.Value,
    FormatFloat('0000000', Geral.IMV(SeqArq)), 'Rem');
  if FileExists(Arquivo) then
    Continua := Geral.MensagemBox('O arquivo "' + Arquivo +
    ' j� existe. Deseja sobrescrev�-lo?', 'Pergunta', MB_YESNOCANCEL+
    MB_ICONQUESTION) = ID_YES
  else
    Continua := True;
  if Continua then
  begin
    MeGerado.Text := RemoveQuebraDeLinha(MeGerado.Text);
    if Geral.SalvaTextoEmArquivo(Arquivo, MeGerado.Text, True) then
    begin
      EdArqGerado.Text := Arquivo;
      PageControl1.ActivePageIndex := 0;
      PageControl2.ActivePageIndex := 0;
      //
      Geral.MensagemBox('O arquivo remessa foi salvo como "' +
      Arquivo + '". Voc� pode copiar o caminho da caixa de edi��o na orelha "' +
      'Arquivo Gravado"!', 'Mensagem', MB_OK+MB_ICONINFORMATION);
      //
    end;
  end;
  Screen.Cursor := crDefault;
end;

procedure TFmCNAB_Rem.MeGeradoEnter(Sender: TObject);
begin
  FEditor := MeGerado;
end;

procedure TFmCNAB_Rem.MeGeradoExit(Sender: TObject);
begin
  FEditor := nil;
end;

procedure TFmCNAB_Rem.MeComparEnter(Sender: TObject);
begin
  FEditor := MeCompar;
end;

procedure TFmCNAB_Rem.MeComparExit(Sender: TObject);
begin
  FEditor := nil;
end;

procedure TFmCNAB_Rem.SpeedButton1Click(Sender: TObject);
begin
  if OpenDialog1.Execute then
  begin
    MeCompar.Lines.LoadFromFile(OpenDialog1.FileName);
    EdArqCompar.Text := OpenDialog1.FileName;
  end;
end;

function TFmCNAB_Rem.GetCNAB: Integer;
begin
  Result := DmBco.QrCNAB_CfgCNAB.Value;
end;

procedure TFmCNAB_Rem.GradeGDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  OldAlign: Integer;
  CorFundo, CorTexto: TColor;
begin
  if GradeG.Cells[ACol,ARow] <> GradeO.Cells[ACol,ARow] then
  begin
    CorFundo := clBlack;
    CorTexto := clWhite;
  end else begin
    CorFundo := clWhite;
    CorTexto := clBlack;
  end;
  GradeG.Canvas.Brush.Color := CorFundo;
  //FillRect(Rect);
  GradeG.Canvas.Font.Color := CorTexto;
  OldAlign := SetTextAlign(GradeG.Canvas.Handle, TA_LEFT);
  GradeG.Canvas.TextRect(Rect, Rect.Left, Rect.Top,
    GradeG.Cells[Acol, ARow]);
  SetTextAlign(GradeG.Canvas.Handle, OldAlign);
end;

procedure TFmCNAB_Rem.GradeGSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
  VerificaCelula(Acol, ARow);
end;

procedure TFmCNAB_Rem.VerificaCelula(Col, Row: Integer);
var
  Ini, Fim, Campo: Integer;
  Pos, ValGera, ValComp: String;
begin
  Campo := Geral.IMV(GradeC.Cells[Col, Row]);
  Ini   := Geral.IMV(GradeI.Cells[Col, Row]);
  Fim   := Geral.IMV(GradeF.Cells[Col, Row]);
  Pos   := FormatFloat('000', Ini) + ' a ' +
           FormatFloat('000', Fim) + ' = ' +
           FormatFloat('000', Fim - Ini + 1);
  ValGera := Copy(MeGerado.Lines[Row], Ini, Fim - Ini + 1);
  ValComp := Copy(MeCompar.Lines[Row], Ini, Fim - Ini + 1);
  dmkEdVerifPos.Text                := Pos;
  dmkEdVerifCol.ValueVariant        := Col + 1;
  dmkEdVerifLin.ValueVariant        := Row + 1;
  dmkEdVerifCampoCod.ValueVariant   := Campo;
  dmkEdVerifCampoTxt.Text           := UBancos.DescricaoDoMeuCodigoCNAB(Campo);
  dmkEdVerifValGerado.Text          := Trim(ValGera);
  dmkEdVerifValCompar.Text          := Trim(ValComp);
end;

procedure TFmCNAB_Rem.BitBtn1Click(Sender: TObject);
var
  i, n, Col, Lin, NewReg, OldReg, Ini, Fim, Este: Integer;
  Campo: String;
  Lista: MyArrayLista;
  CNAB: Integer;
const
  Posicoes = 0;  
begin
  Screen.Cursor := crHourGlass;
  Ini := 0;
  Fim := 0;
  CNAB := DmBco.QrCNAB_CfgCNAB.Value;
  MyObjects.LimpaGrade(GradeG, 0, 0, True);
  MyObjects.LimpaGrade(GradeO, 0, 0, True);
  MyObjects.LimpaGrade(GradeC, 0, 0, True);
  //
  n := CNAB;
  if n <= 0 then
  begin
    Geral.MensagemBox('Quantidade de carateres n�o definida no arquivo!',
    'Erro', MB_OK+MB_ICONERROR);
    Screen.Cursor := crDefault;
    Exit;
  end;
  GradeG.ColCount := n; // Arquivo gerado
  GradeO.ColCount := n; // Arquivo a comparar
  GradeC.ColCount := n; // Campos
  GradeI.ColCount := n; // In�cio do campo
  GradeF.ColCount := n; // Fim do campo
  //
  if MeGerado.Lines.Count <> MeCompar.Lines.Count then
  begin
    Geral.MensagemBox('Quantidade de linhas n�o conferem!', 'Aviso',
    MB_OK+MB_ICONWARNING);
  end;
  n := MeGerado.Lines.Count;
  if n < MeCompar.Lines.Count then n := MeCompar.Lines.Count;
  if n = 0 then begin Screen.Cursor := crDefault; Exit; end;
  GradeG.RowCount := n;
  GradeO.RowCount := n;
  GradeC.RowCount := n;
  GradeI.RowCount := n;
  GradeF.RowCount := n;
  OldReg := -1;
  PB1.Position := 0;
  PB1.Max := n;
  for Lin := 0 to n-1 do
  begin
    if MeGerado.Lines.Count <= Lin then
    begin
      // Evitar erro
      Screen.Cursor := crDefault;
      exit;
    end;
    PB1.Position := PB1.Position + 1;
    NewReg := Geral.IMV(MeGerado.Lines[Lin][1]);
    if NewReg <> OldReg then
    begin
      OldReg := NewReg;
      CriaListaCNAB(CNAB, FBanco, NewReg, Posicoes, Lista);
    end;
    for Col := 0 to Length(MeGerado.Lines[Lin])-1 do
    begin
      Campo := '';
      for i := Low(Lista) to High(Lista) do
      begin
        Este := Col + 1;
        Ini := Geral.IMV(Lista[i][_Ini]);
        Fim := Geral.IMV(Lista[i][_Fim]);
        if (Este >= Ini) and (Este <= Fim) then
        begin
          Campo := Lista[i][_Fld];
          Break;
        end;
      end;
      GradeC.Cells[Col, Lin] := Campo;
      GradeI.Cells[Col, Lin] := FormatFloat('000', Ini);
      GradeF.Cells[Col, Lin] := FormatFloat('000', Fim);
      GradeO.Cells[Col, Lin] := MeCompar.Lines[Lin][Col + 1];
      GradeG.Cells[Col, Lin] := MeGerado.Lines[Lin][Col + 1];
    end;
  end;
  Screen.Cursor := crDefault;
end;

function TFmCNAB_Rem.DAC_NossoNumero(const Bloqueto: Double; var Res: String): Boolean;
begin
  Result := UBancos.DACNossoNumero(DmBco.QrCNAB_CfgModalCobr.Value,
    DmBco.QrCNAB_CfgCedBanco.Value,
    DmBco.QrCNAB_CfgCedAgencia.Value, DmBco.QrCNAB_CfgCedPosto.Value, Bloqueto,
    DmBco.QrCNAB_CfgCedConta.Value, DmBco.QrCNAB_CfgCartCod.Value,
    DmBco.QrCNAB_CfgCartNum.Value,
    Geral.SoNumero_TT(DmBco.QrCNAB_CfgCodEmprBco.Value),
    DmBco.QrCNAB_CfgTipoCobranca.Value, DmBco.QrCNAB_CfgEspecieDoc.Value,
    DmBco.QrCNAB_CfgCNAB.Value, DmBco.QrCNAB_CfgCtaCooper.Value,
    DmBco.QrCNAB_CfgLayoutRem.Value, Res);
end;

function TFmCNAB_Rem.ObtemNumeroDoArquivo(var Res: String): Boolean;
begin
  //Result := False;
  if FSeqArqRem = 0 then
  begin
    FSeqArqRem := DmBco.QrCNAB_CfgSeqArq.Value + 1;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE cnab_cfg SET SeqArq=:P0');
    Dmod.QrUpd.SQL.Add('WHERE Codigo=:P1');
    Dmod.QrUpd.Params[00].AsInteger := FSeqArqRem;
    Dmod.QrUpd.Params[01].AsInteger := DmBco.QrCNAB_CfgCodigo.Value;
    Dmod.QrUpd.ExecSQL;
  end;
  if UpperCase(Application.Title) <> 'CREDITOR' then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE protocopak SET SeqArq=:P0');
    Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
    Dmod.QrUpd.Params[00].AsInteger := FSeqArqRem;
    Dmod.QrUpd.Params[01].AsInteger := FLote;
    Dmod.QrUpd.ExecSQL;
  end;
  //
  Res := FormatFloat('0', FSeqArqRem);
  Result := True;
end;

end.

