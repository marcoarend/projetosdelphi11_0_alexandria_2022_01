object FmLot0Loc: TFmLot0Loc
  Left = 333
  Top = 182
  Caption = 'BDR-GEREN-014 :: Border'#244's'
  ClientHeight = 477
  ClientWidth = 914
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 0
    Top = 93
    Width = 914
    Height = 270
    Align = alClient
    DataSource = DsLoc
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDblClick = DBGrid1DblClick
    Columns = <
      item
        Expanded = False
        FieldName = 'Lote'
        Title.Caption = 'Border'#244
        Width = 48
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Data'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOMETIPO'
        Title.Caption = 'Tipo de documento'
        Width = 98
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Codigo'
        Title.Caption = 'Lote'
        Width = 48
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Total'
        Title.Caption = 'Valor'
        Width = 77
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOMECLIENTE'
        Title.Caption = 'Cliente'
        Width = 360
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'NF'
        Width = 54
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Itens'
        Width = 49
        Visible = True
      end>
  end
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 914
    Height = 45
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 845
    object Label1: TLabel
      Left = 12
      Top = 4
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
    end
    object Label2: TLabel
      Left = 80
      Top = 4
      Width = 101
      Height = 13
      Caption = 'Descri'#231#227'o do Cliente:'
    end
    object Label3: TLabel
      Left = 435
      Top = 4
      Width = 70
      Height = 13
      Caption = 'N'#186' do Border'#244':'
    end
    object Label34: TLabel
      Left = 600
      Top = 4
      Width = 55
      Height = 13
      Caption = 'Data inicial:'
    end
    object Label4: TLabel
      Left = 716
      Top = 4
      Width = 48
      Height = 13
      Caption = 'Data final:'
    end
    object Label5: TLabel
      Left = 832
      Top = 4
      Width = 46
      Height = 13
      Caption = 'Controle*:'
    end
    object Label6: TLabel
      Left = 518
      Top = 4
      Width = 47
      Height = 13
      Caption = 'N'#186' da NF:'
    end
    object CBCliente: TdmkDBLookupComboBox
      Left = 80
      Top = 20
      Width = 350
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NOMEENTIDADE'
      ListSource = DsClientes
      TabOrder = 1
      dmkEditCB = EdCliente
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdCliente: TdmkEditCB
      Left = 12
      Top = 20
      Width = 65
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      OnChange = EdClienteChange
      DBLookupComboBox = CBCliente
      IgnoraDBLookupComboBox = False
    end
    object EdLote: TdmkEdit
      Left = 435
      Top = 20
      Width = 77
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      OnChange = EdLoteChange
      OnExit = EdLoteExit
    end
    object TPIni: TdmkEditDateTimePicker
      Left = 600
      Top = 20
      Width = 112
      Height = 21
      Date = 38675.714976851900000000
      Time = 38675.714976851900000000
      TabOrder = 4
      OnClick = TPIniClick
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
    end
    object TPFim: TdmkEditDateTimePicker
      Left = 716
      Top = 20
      Width = 112
      Height = 21
      Date = 38675.714976851900000000
      Time = 38675.714976851900000000
      TabOrder = 5
      OnClick = TPFimClick
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
    end
    object EdControle: TdmkEdit
      Left = 832
      Top = 20
      Width = 72
      Height = 21
      Alignment = taRightJustify
      TabOrder = 6
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      OnChange = EdControleChange
    end
    object EdNF: TdmkEdit
      Left = 518
      Top = 20
      Width = 77
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      OnChange = EdNFChange
      OnExit = EdNFExit
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 914
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitWidth = 845
    object GB_R: TGroupBox
      Left = 866
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 797
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 818
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 749
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 110
        Height = 32
        Caption = 'Border'#244's'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 110
        Height = 32
        Caption = 'Border'#244's'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 110
        Height = 32
        Caption = 'Border'#244's'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 363
    Width = 914
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    ExplicitWidth = 845
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 910
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 841
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 480
        Height = 16
        Caption = 
          '* N'#250'mero do controle do cheque ou duplicata ao qual se deseja lo' +
          'calizar o border'#244'.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 480
        Height = 16
        Caption = 
          '* N'#250'mero do controle do cheque ou duplicata ao qual se deseja lo' +
          'calizar o border'#244'.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 407
    Width = 914
    Height = 70
    Align = alBottom
    TabOrder = 4
    ExplicitWidth = 845
    object PnSaiDesis: TPanel
      Left = 768
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 699
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 766
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 697
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 320
    Top = 30
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      'CASE 1 WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE, Codigo'
      'FROM entidades'
      'WHERE Cliente1="V" '
      'ORDER BY NOMEENTIDADE')
    Left = 292
    Top = 30
    object QrClientesNOMEENTIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrLocAfterOpen
    OnCalcFields = QrLocCalcFields
    SQL.Strings = (
      'SELECT CASE WHEN en.Tipo=0 THEN en.RazaoSocial'
      'ELSE en.Nome END NOMECLIENTE,'
      'CASE WHEN en.Tipo=0 THEN en.CNPJ'
      'ELSE en.CPF END CNPJCPF, lo.*'
      'FROM lct0001a lo'
      'LEFT JOIN entidades en ON en.Codigo=lo.Cliente')
    Left = 40
    Top = 156
    object QrLocCodigo: TIntegerField
      FieldName = 'Codigo'
      DisplayFormat = '000000'
    end
    object QrLocLote: TSmallintField
      FieldName = 'Lote'
      DisplayFormat = '0000'
    end
    object QrLocData: TDateField
      FieldName = 'Data'
    end
    object QrLocTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrLocNOMETIPO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMETIPO'
      Size = 50
      Calculated = True
    end
    object QrLocTotal: TFloatField
      FieldName = 'Total'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLocNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrLocCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 18
    end
    object QrLocSpread: TSmallintField
      FieldName = 'Spread'
      Required = True
    end
    object QrLocCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrLocDias: TFloatField
      FieldName = 'Dias'
      Required = True
    end
    object QrLocPeCompra: TFloatField
      FieldName = 'PeCompra'
      Required = True
    end
    object QrLocTxCompra: TFloatField
      FieldName = 'TxCompra'
      Required = True
    end
    object QrLocValValorem: TFloatField
      FieldName = 'ValValorem'
      Required = True
    end
    object QrLocAdValorem: TFloatField
      FieldName = 'AdValorem'
      Required = True
    end
    object QrLocIOC: TFloatField
      FieldName = 'IOC'
      Required = True
    end
    object QrLocIOC_VAL: TFloatField
      FieldName = 'IOC_VAL'
      Required = True
    end
    object QrLocTarifas: TFloatField
      FieldName = 'Tarifas'
      Required = True
    end
    object QrLocCPMF: TFloatField
      FieldName = 'CPMF'
      Required = True
    end
    object QrLocCPMF_VAL: TFloatField
      FieldName = 'CPMF_VAL'
      Required = True
    end
    object QrLocTipoAdV: TIntegerField
      FieldName = 'TipoAdV'
      Required = True
    end
    object QrLocIRRF: TFloatField
      FieldName = 'IRRF'
      Required = True
    end
    object QrLocIRRF_Val: TFloatField
      FieldName = 'IRRF_Val'
      Required = True
    end
    object QrLocISS: TFloatField
      FieldName = 'ISS'
      Required = True
    end
    object QrLocISS_Val: TFloatField
      FieldName = 'ISS_Val'
      Required = True
    end
    object QrLocPIS: TFloatField
      FieldName = 'PIS'
      Required = True
    end
    object QrLocPIS_Val: TFloatField
      FieldName = 'PIS_Val'
      Required = True
    end
    object QrLocPIS_R: TFloatField
      FieldName = 'PIS_R'
      Required = True
    end
    object QrLocPIS_R_Val: TFloatField
      FieldName = 'PIS_R_Val'
      Required = True
    end
    object QrLocCOFINS: TFloatField
      FieldName = 'COFINS'
      Required = True
    end
    object QrLocCOFINS_Val: TFloatField
      FieldName = 'COFINS_Val'
      Required = True
    end
    object QrLocCOFINS_R: TFloatField
      FieldName = 'COFINS_R'
      Required = True
    end
    object QrLocCOFINS_R_Val: TFloatField
      FieldName = 'COFINS_R_Val'
      Required = True
    end
    object QrLocOcorP: TFloatField
      FieldName = 'OcorP'
      Required = True
    end
    object QrLocMaxVencto: TDateField
      FieldName = 'MaxVencto'
      Required = True
    end
    object QrLocCHDevPg: TFloatField
      FieldName = 'CHDevPg'
      Required = True
    end
    object QrLocLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLocDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLocDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLocUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrLocUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrLocMINTC: TFloatField
      FieldName = 'MINTC'
      Required = True
    end
    object QrLocMINAV: TFloatField
      FieldName = 'MINAV'
      Required = True
    end
    object QrLocMINTC_AM: TFloatField
      FieldName = 'MINTC_AM'
      Required = True
    end
    object QrLocPIS_T_Val: TFloatField
      FieldName = 'PIS_T_Val'
      Required = True
    end
    object QrLocCOFINS_T_Val: TFloatField
      FieldName = 'COFINS_T_Val'
      Required = True
    end
    object QrLocPgLiq: TFloatField
      FieldName = 'PgLiq'
      Required = True
    end
    object QrLocDUDevPg: TFloatField
      FieldName = 'DUDevPg'
      Required = True
    end
    object QrLocNF: TIntegerField
      FieldName = 'NF'
      Required = True
      DisplayFormat = '000000;-000000; '
    end
    object QrLocItens: TIntegerField
      FieldName = 'Itens'
      Required = True
      DisplayFormat = '00;-00; '
    end
    object QrLocConferido: TIntegerField
      FieldName = 'Conferido'
      Required = True
    end
    object QrLocECartaSac: TSmallintField
      FieldName = 'ECartaSac'
      Required = True
    end
  end
  object DsLoc: TDataSource
    DataSet = QrLoc
    Left = 68
    Top = 156
  end
  object QrCon: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT FatNum'
      'FROM lct???'
      'WHERE FatID=VAR_FATID_0301'
      'AND Controle=:P0')
    Left = 344
    Top = 180
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrConFatNum: TFloatField
      FieldName = 'FatNum'
    end
  end
end
