object FmCredito2_Ini: TFmCredito2_Ini
  Left = 339
  Top = 185
  Caption = '???-?????-999 :: Adequa'#231#245'es Credito2'
  ClientHeight = 285
  ClientWidth = 582
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 582
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 534
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 486
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 262
        Height = 32
        Caption = 'Adequa'#231#245'es Credito2'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 262
        Height = 32
        Caption = 'Adequa'#231#245'es Credito2'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 262
        Height = 32
        Caption = 'Adequa'#231#245'es Credito2'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 151
    Width = 582
    Height = 64
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 578
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 30
        Width = 578
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 215
    Width = 582
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 436
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        Visible = False
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 434
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 582
    Height = 103
    Align = alClient
    TabOrder = 3
  end
  object QrOcorreu: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Lot esIts LOIS'
      'FROM ocorreu'
      'WHERE Cliente=0'
      'AND Lot esIts<>0')
    Left = 212
    Top = 64
    object QrOcorreuCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOcorreuLOIS: TIntegerField
      FieldName = 'LOIS'
    end
  end
  object QrLOI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lot.Cliente, lot.Tipo, loi.Banco, loi.Agencia, loi.Conta,'
      'loi.Cheque, loi.Duplicata, loi.CPF, loi.Emitente, '
      'loi.Emissao, loi.DDeposito, loi.Vencto, loi.DCompra'
      'FROM lot esits loi'
      'LEFT JOIN lot es lot ON lot.Codigo=loi.Codigo'
      'WHERE loi.Controle=:P0')
    Left = 240
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLOICliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrLOITipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrLOIBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrLOIAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrLOIConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrLOICheque: TIntegerField
      FieldName = 'Cheque'
    end
    object QrLOIDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 12
    end
    object QrLOICPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrLOIEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrLOIEmissao: TDateField
      FieldName = 'Emissao'
    end
    object QrLOIDDeposito: TDateField
      FieldName = 'DDeposito'
    end
    object QrLOIVencto: TDateField
      FieldName = 'Vencto'
    end
    object QrLOIDCompra: TDateField
      FieldName = 'DCompra'
    end
  end
end
