unit GerDup1Main;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, ComCtrls, DBCtrls, Db, mySQLDbTables,
  Grids, DBGrids, Menus, Mask, frxClass, frxDBSet, Variants, dmkGeral,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkCheckGroup, dmkImage,
  UnDmkProcFunc, DmkDAC_PF, UnDmkEnums, dmkDBGrid;

type
  TTipoFormDup = (tmfDupOcor, tmfDupPror, tmfDupPgOc);
  TFmGerDup1Main = class(TForm)
    Panel1: TPanel;
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesNOMECLIENTE: TWideStringField;
    DsClientes: TDataSource;
    QrPesq: TmySQLQuery;
    DsPesq: TDataSource;
    QrPesqDuplicata: TWideStringField;
    QrPesqDCompra: TDateField;
    QrPesqDDeposito: TDateField;
    QrPesqEmitente: TWideStringField;
    QrPesqCliente: TIntegerField;
    QrPesqNOMECLIENTE: TWideStringField;
    QrPesqSTATUS: TWideStringField;
    QrPesqQuitado: TIntegerField;
    QrSumPg: TmySQLQuery;
    QrSumPgJuros: TFloatField;
    QrSumPgPago: TFloatField;
    QrSumPgDesco: TFloatField;
    QrSumPgValor: TFloatField;
    QrSumPgMaxData: TDateField;
    QrLocPg: TmySQLQuery;
    QrLocPgData: TDateField;
    QrLocPgFatParcela: TIntegerField;
    QrPesqTotalJr: TFloatField;
    QrPesqTotalDs: TFloatField;
    QrPesqTotalPg: TFloatField;
    QrADupIts: TmySQLQuery;
    QrADupItsAlinea: TIntegerField;
    QrADupItsLk: TIntegerField;
    QrADupItsDataCad: TDateField;
    QrADupItsDataAlt: TDateField;
    QrADupItsUserCad: TIntegerField;
    QrADupItsUserAlt: TIntegerField;
    QrADupItsControle: TIntegerField;
    QrADupItsDataA: TDateField;
    DsADupIts: TDataSource;
    DsDupPgs: TDataSource;
    QrDupPgs: TmySQLQuery;
    Panel3: TPanel;
    QrADupItsNOMESTATUS: TWideStringField;
    QrPos: TmySQLQuery;
    QrPosAlinea: TIntegerField;
    PMStatus: TPopupMenu;
    Inclui1: TMenuItem;
    Exclui1: TMenuItem;
    QrPesqSALDO_ATUALIZADO: TFloatField;
    QrPesqNOMESTATUS: TWideStringField;
    PMPagto: TPopupMenu;
    Inclui2: TMenuItem;
    Exclui2: TMenuItem;
    QrPesqDDCALCJURO: TIntegerField;
    QrPesqData3: TDateField;
    Panel8: TPanel;
    GradeItens: TdmkDBGrid;
    Panel7: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    DBGrid4: TDBGrid;
    DBGrid3: TDBGrid;
    TabSheet2: TTabSheet;
    PMStatusManual: TPopupMenu;
    Incluiprorrogao1: TMenuItem;
    ExcluiProrrogao1: TMenuItem;
    PMOcorreu: TPopupMenu;
    Incluiocorrncia1: TMenuItem;
    Alteraocorrncia1: TMenuItem;
    Excluiocorrncia1: TMenuItem;
    QrOcorreu: TmySQLQuery;
    QrOcorreuNOMEOCORRENCIA: TWideStringField;
    QrOcorreuCodigo: TIntegerField;
    QrOcorreuDataO: TDateField;
    QrOcorreuOcorrencia: TIntegerField;
    QrOcorreuValor: TFloatField;
    QrOcorreuLoteQuit: TIntegerField;
    QrOcorreuLk: TIntegerField;
    QrOcorreuDataCad: TDateField;
    QrOcorreuDataAlt: TDateField;
    QrOcorreuUserCad: TIntegerField;
    QrOcorreuUserAlt: TIntegerField;
    QrOcorreuTaxaP: TFloatField;
    QrOcorreuTaxaV: TFloatField;
    QrOcorreuPago: TFloatField;
    QrOcorreuDataP: TDateField;
    QrOcorreuTaxaB: TFloatField;
    DsOcorreu: TDataSource;
    QrLotPrr: TmySQLQuery;
    QrLotPrrCodigo: TIntegerField;
    QrLotPrrControle: TIntegerField;
    QrLotPrrData1: TDateField;
    QrLotPrrData2: TDateField;
    QrLotPrrData3: TDateField;
    QrLotPrrOcorrencia: TIntegerField;
    QrLotPrrLk: TIntegerField;
    QrLotPrrDataCad: TDateField;
    QrLotPrrDataAlt: TDateField;
    QrLotPrrUserCad: TIntegerField;
    QrLotPrrUserAlt: TIntegerField;
    QrLotPrrDIAS_1_3: TIntegerField;
    QrLotPrrDIAS_2_3: TIntegerField;
    DsLotPrr: TDataSource;
    QrPesqCNPJ_TXT: TWideStringField;
    QrPesqTAXA_COMPRA: TFloatField;
    QrPesqTxaCompra: TFloatField;
    QrPesqDevolucao: TIntegerField;
    QrPesqProrrVz: TIntegerField;
    QrPesqProrrDd: TIntegerField;
    N1: TMenuItem;
    Recibo1: TMenuItem;
    QrSoma: TmySQLQuery;
    DsSoma: TDataSource;
    QrVcto: TmySQLQuery;
    QrSomaValor: TFloatField;
    QrSomaSALDO_DESATUALIZADO: TFloatField;
    QrVctoSOMA_VALOR: TFloatField;
    QrVctoSALDO_DESATUALIZADO: TFloatField;
    QrVctoSOMA_DIAS: TFloatField;
    QrVctoFATOR_VD: TFloatField;
    QrVctoPRAZO_MEDIO: TFloatField;
    DsVcto: TDataSource;
    QrVctoJURO_CLI: TFloatField;
    QrSP: TmySQLQuery;
    QrSPSALDO_DESATUALIZ: TFloatField;
    QrSPSALDO_ATUALIZADO: TFloatField;
    QrPesqRepassado: TSmallintField;
    N2: TMenuItem;
    IncluiPagamento1: TMenuItem;
    Excluipagamento1: TMenuItem;
    N3: TMenuItem;
    Recibodepagamento1: TMenuItem;
    QrSumOc: TmySQLQuery;
    QrSumOcJuros: TFloatField;
    QrSumOcPago: TFloatField;
    QrLastOcor: TmySQLQuery;
    QrLastOcorData: TDateField;
    QrLastOcorFatParcela: TIntegerField;
    QrOcorP: TmySQLQuery;
    DsOcorP: TDataSource;
    TabSheet3: TTabSheet;
    DBGrid2: TDBGrid;
    DBGrid5: TDBGrid;
    DBGrid1: TDBGrid;
    QrOcorreuATUALIZADO: TFloatField;
    QrOcorreuData3: TDateField;
    QrOcorreuStatus: TSmallintField;
    QrOcorreuCliente: TIntegerField;
    QrOcorreuSALDO: TFloatField;
    PMQuitacao: TPopupMenu;
    Quitadocumento1: TMenuItem;
    Imprimerecibodequitao1: TMenuItem;
    QrPesqValQuit: TFloatField;
    QrPesqNF: TIntegerField;
    QrPesqBanco: TIntegerField;
    PageControl2: TPageControl;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    PainelPesq: TPanel;
    Label75: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label9: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    EdDuplicata: TdmkEdit;
    EdEmitente: TdmkEdit;
    RGMascara: TRadioGroup;
    EdCPF: TdmkEdit;
    TPEIni: TDateTimePicker;
    TPEFim: TDateTimePicker;
    CkEIni: TCheckBox;
    CkEFim: TCheckBox;
    CkVIni: TCheckBox;
    TPVIni: TDateTimePicker;
    CkVFim: TCheckBox;
    TPVFim: TDateTimePicker;
    EdColigado: TdmkEditCB;
    CBColigado: TdmkDBLookupComboBox;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label5: TLabel;
    Label7: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    Label10: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit3: TDBEdit;
    EdAtualizado: TdmkEdit;
    EdITENS: TdmkEdit;
    CkRepassado: TdmkCheckGroup;
    CkStatus: TdmkCheckGroup;
    Panel6: TPanel;
    RGOrdem1: TRadioGroup;
    RGOrdem2: TRadioGroup;
    RGOrdem3: TRadioGroup;
    RGOrdem4: TRadioGroup;
    QrPesqPERIODO: TFloatField;
    RGAgrupa: TRadioGroup;
    BtImprime: TBitBtn;
    QrPesq2: TmySQLQuery;
    QrPesq2Controle: TIntegerField;
    QrPesq2Duplicata: TWideStringField;
    QrPesq2DCompra: TDateField;
    QrPesq2DDeposito: TDateField;
    QrPesq2Emitente: TWideStringField;
    QrPesq2Cliente: TIntegerField;
    QrPesq2NOMECLIENTE: TWideStringField;
    QrPesq2STATUS: TWideStringField;
    QrPesq2Quitado: TIntegerField;
    QrPesq2TotalJr: TFloatField;
    QrPesq2TotalDs: TFloatField;
    QrPesq2TotalPg: TFloatField;
    QrPesq2SALDO_DESATUALIZ: TFloatField;
    QrPesq2SALDO_ATUALIZADO: TFloatField;
    QrPesq2NOMESTATUS: TWideStringField;
    QrPesq2DDCALCJURO: TIntegerField;
    QrPesq2Data3: TDateField;
    QrPesq2CNPJ_TXT: TWideStringField;
    QrPesq2TAXA_COMPRA: TFloatField;
    QrPesq2TxaCompra: TFloatField;
    QrPesq2Devolucao: TIntegerField;
    QrPesq2ProrrVz: TIntegerField;
    QrPesq2ProrrDd: TIntegerField;
    QrPesq2Repassado: TSmallintField;
    QrPesq2ValQuit: TFloatField;
    QrPesq2NF: TIntegerField;
    QrPesq2Banco: TIntegerField;
    QrPesq2Agencia: TIntegerField;
    QrPesq2CONTA: TIntegerField;
    QrPesq2PERIODO: TFloatField;
    QrPesq2Valor_TXT: TWideStringField;
    QrPesq2DDeposito_TXT: TWideStringField;
    QrPesq2MES_TXT: TWideStringField;
    QrPesqSALDO_DESATUALIZ: TFloatField;
    QrOcu1: TmySQLQuery;
    QrOcu1NOMEOCORRENCIA: TWideStringField;
    QrOcu1Codigo: TIntegerField;
    QrOcu1DataO: TDateField;
    QrOcu1Ocorrencia: TIntegerField;
    QrOcu1Valor: TFloatField;
    QrOcu1LoteQuit: TIntegerField;
    QrOcu1Lk: TIntegerField;
    QrOcu1DataCad: TDateField;
    QrOcu1DataAlt: TDateField;
    QrOcu1UserCad: TIntegerField;
    QrOcu1UserAlt: TIntegerField;
    QrOcu1TaxaP: TFloatField;
    QrOcu1TaxaV: TFloatField;
    QrOcu1Pago: TFloatField;
    QrOcu1DataP: TDateField;
    QrOcu1TaxaB: TFloatField;
    QrOcu1ATUALIZADO: TFloatField;
    QrOcu1Data3: TDateField;
    QrOcu1Status: TSmallintField;
    QrOcu1Cliente: TIntegerField;
    QrOcu1SALDO: TFloatField;
    Label11: TLabel;
    EdOcorrencias: TdmkEdit;
    EdOcorAtualiz: TdmkEdit;
    Label12: TLabel;
    Label17: TLabel;
    EdTotalAtualiz: TdmkEdit;
    QrPesqOCORRENCIAS: TFloatField;
    QrPesqOCORATUALIZ: TFloatField;
    QrPesqTOTALATUALIZ: TFloatField;
    QrOcu2: TmySQLQuery;
    QrOcu2NOMEOCORRENCIA: TWideStringField;
    QrOcu2Codigo: TIntegerField;
    QrOcu2DataO: TDateField;
    QrOcu2Ocorrencia: TIntegerField;
    QrOcu2Valor: TFloatField;
    QrOcu2LoteQuit: TIntegerField;
    QrOcu2Lk: TIntegerField;
    QrOcu2DataCad: TDateField;
    QrOcu2DataAlt: TDateField;
    QrOcu2UserCad: TIntegerField;
    QrOcu2UserAlt: TIntegerField;
    QrOcu2TaxaP: TFloatField;
    QrOcu2TaxaV: TFloatField;
    QrOcu2Pago: TFloatField;
    QrOcu2DataP: TDateField;
    QrOcu2TaxaB: TFloatField;
    QrOcu2ATUALIZADO: TFloatField;
    QrOcu2Data3: TDateField;
    QrOcu2Status: TSmallintField;
    QrOcu2Cliente: TIntegerField;
    QrOcu2SALDO: TFloatField;
    QrPesq2OCORRENCIAS: TFloatField;
    QrPesq2OCORATUALIZ: TFloatField;
    QrPesq2TOTALATUALIZ: TFloatField;
    QrColigados: TmySQLQuery;
    DsColigados: TDataSource;
    QrColigadosNOMECLIENTE: TWideStringField;
    QrColigadosCodigo: TIntegerField;
    TabSheet6: TTabSheet;
    QrPesqSALDO_TEXTO: TWideStringField;
    QrOcorreuATZ_TEXTO: TWideStringField;
    QrPesq2ATZ_TEXTO: TWideStringField;
    CkAtualiza: TCheckBox;
    QrPesqLOCALT: TLargeintField;
    QrPesqNOMELOCAL: TWideStringField;
    QrPesq2DATA3TXT: TWideStringField;
    QrPesqPERIODO3: TFloatField;
    QrPesq2PERIODO3: TFloatField;
    QrPesq2MEQ_TXT: TWideStringField;
    Label55: TLabel;
    EdControle: TdmkEdit;
    TabSheet7: TTabSheet;
    Memo1: TMemo;
    QrCNAB240: TmySQLQuery;
    DsCNAB240: TDataSource;
    DBGrid6: TDBGrid;
    QrCNAB240CNAB240L: TIntegerField;
    QrCNAB240CNAB240I: TIntegerField;
    QrCNAB240Controle: TIntegerField;
    QrCNAB240Banco: TIntegerField;
    QrCNAB240Sequencia: TIntegerField;
    QrCNAB240Convenio: TWideStringField;
    QrCNAB240ConfigBB: TIntegerField;
    QrCNAB240Lote: TIntegerField;
    QrCNAB240Item: TIntegerField;
    QrCNAB240DataOcor: TDateField;
    QrCNAB240Envio: TSmallintField;
    QrCNAB240Movimento: TSmallintField;
    QrCNAB240Custas: TFloatField;
    QrCNAB240ValPago: TFloatField;
    QrCNAB240ValCred: TFloatField;
    QrCNAB240Acao: TIntegerField;
    QrCNAB240Lk: TIntegerField;
    QrCNAB240DataCad: TDateField;
    QrCNAB240DataAlt: TDateField;
    QrCNAB240UserCad: TIntegerField;
    QrCNAB240UserAlt: TIntegerField;
    QrCNAB240NOMEENVIO: TWideStringField;
    QrCNAB240NOMEMOVIMENTO: TWideStringField;
    QrCNAB240IRTCLB: TWideStringField;
    Splitter1: TSplitter;
    CkQIni: TCheckBox;
    TPQIni: TDateTimePicker;
    CkQFim: TCheckBox;
    TPQFim: TDateTimePicker;
    QrClientesTe1: TWideStringField;
    Label18: TLabel;
    DBEdit5: TDBEdit;
    QrClientesTe1_TXT: TWideStringField;
    QrSacados: TmySQLQuery;
    QrSacadosNumero: TFloatField;
    QrSacadosCNPJ: TWideStringField;
    QrSacadosIE: TWideStringField;
    QrSacadosNome: TWideStringField;
    QrSacadosRua: TWideStringField;
    QrSacadosCompl: TWideStringField;
    QrSacadosBairro: TWideStringField;
    QrSacadosCidade: TWideStringField;
    QrSacadosUF: TWideStringField;
    QrSacadosCEP: TIntegerField;
    QrSacadosTel1: TWideStringField;
    QrSacadosRisco: TFloatField;
    QrSacadosAlterWeb: TSmallintField;
    N4: TMenuItem;
    N1Forastatusprorrogado1: TMenuItem;
    N0Statusautomtico1: TMenuItem;
    N2ForastatusBaixado1: TMenuItem;
    N3ForastatusMoroso1: TMenuItem;
    DBEdit6: TDBEdit;
    QrSomaPrincipal: TFloatField;
    CkOcorrencias: TCheckBox;
    PMHistCNAB: TPopupMenu;
    Excluihistriciatual1: TMenuItem;
    Excluitodositensdehistricosdestaduplicata1: TMenuItem;
    frxPesq1: TfrxReport;
    frxDsPesq2: TfrxDBDataset;
    frxPesq2: TfrxReport;
    PMImprime: TPopupMenu;
    Extratodeduplicata1: TMenuItem;
    frxExtratoDuplicata: TfrxReport;
    frxDsOcorreu: TfrxDBDataset;
    QrOcorreuSEQ: TIntegerField;
    QrPesqDATA3_TXT: TWideStringField;
    frxDsADupIts: TfrxDBDataset;
    frxDsDupPgs: TfrxDBDataset;
    frxDsLotPrr: TfrxDBDataset;
    ImprimecartadeCancelamentodeProtesto1: TMenuItem;
    ImprimecartadeCancelamentodeProtesto2: TMenuItem;
    frxDsPesq: TfrxDBDataset;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbBordero: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel9: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel10: TPanel;
    BtCuidado: TBitBtn;
    BtPagtoDuvida: TBitBtn;
    BtZAZ: TBitBtn;
    BtStatusManual: TBitBtn;
    BtDuplicataOcorr: TBitBtn;
    BtQuitacao: TBitBtn;
    BtHistCNAB: TBitBtn;
    BtReabre: TBitBtn;
    BtRefresh: TBitBtn;
    ProgressBar1: TProgressBar;
    RGFontes: TRadioGroup;
    QrPesqFatParcela: TIntegerField;
    QrPesqData: TDateField;
    QrPesqCredito: TFloatField;
    QrPesqCNPJCPF: TWideStringField;
    QrPesqVencimento: TDateField;
    QrPesqCodigo: TIntegerField;
    QrPesqAgencia: TIntegerField;
    QrOcorPOcorreu: TIntegerField;
    QrOcorPData: TDateField;
    QrOcorPFatParcela: TIntegerField;
    QrOcorPPago: TFloatField;
    QrOcorPFatNum: TFloatField;
    QrOcorPMoraVal: TFloatField;
    QrOcorPControle: TIntegerField;
    QrOcorreuPlaGen: TIntegerField;
    QrOcorreuLOIS: TIntegerField;
    QrDupPgsLote: TIntegerField;
    QrDupPgsFatParcela: TIntegerField;
    QrDupPgsData: TDateField;
    QrDupPgsCredito: TFloatField;
    QrDupPgsFatNum: TFloatField;
    QrDupPgsMoraVal: TFloatField;
    QrDupPgsDescoVal: TFloatField;
    QrDupPgsControle: TIntegerField;
    QrDupPgsOcorreu: TIntegerField;
    QrSPPERIODO: TFloatField;
    QrSPPERIODO3: TFloatField;
    QrSPNF: TIntegerField;
    QrSPSTATUS: TWideStringField;
    QrSPFatParcela: TIntegerField;
    QrSPDuplicata: TWideStringField;
    QrSPData: TDateField;
    QrSPDCompra: TDateField;
    QrSPCredito: TFloatField;
    QrSPDDeposito: TDateField;
    QrSPEmitente: TWideStringField;
    QrSPCNPJCPF: TWideStringField;
    QrSPCliente: TIntegerField;
    QrSPQuitado: TIntegerField;
    QrSPTotalJr: TFloatField;
    QrSPTotalDs: TFloatField;
    QrSPTotalPg: TFloatField;
    QrSPRepassado: TSmallintField;
    QrSPNOMECLIENTE: TWideStringField;
    QrSPVencimento: TDateField;
    QrSPData3: TDateField;
    QrSPTxaCompra: TFloatField;
    QrSPDevolucao: TIntegerField;
    QrSPProrrVz: TIntegerField;
    QrSPProrrDd: TIntegerField;
    QrSPValQuit: TFloatField;
    QrSPBanco: TIntegerField;
    QrSPAgencia: TIntegerField;
    QrSPLOCALT: TLargeintField;
    QrSPCodigo: TIntegerField;
    QrPesqLctCtrl: TIntegerField;
    QrPesqLctSub: TIntegerField;
    QrDupPgsFatParcRef: TIntegerField;
    QrDupPgsFatGrupo: TIntegerField;
    QrPesqCartDep: TIntegerField;
    QrDupPgsPago: TFloatField;
    QrPesqControle: TIntegerField;
    PMDiario: TPopupMenu;
    Adicionareventoaodiario1: TMenuItem;
    Gerenciardirio1: TMenuItem;
    QrPesq2Tel1: TWideStringField;
    QrPesq2TEL1_TXT: TWideStringField;
    QrPesq2Credito: TFloatField;
    QrPesq2CNPJCPF: TWideStringField;
    QrPesqTel1: TWideStringField;
    QrPesq2Data: TDateField;
    QrPesq2Vencimento: TDateField;
    N5: TMenuItem;
    frxGER_DIARI_001_01: TfrxReport;
    frxDsDiarioAdd: TfrxDBDataset;
    QrDiarioAdd: TmySQLQuery;
    QrDiarioAddNOME_CLI: TWideStringField;
    QrDiarioAddNOME_ENT: TWideStringField;
    QrDiarioAddNOME_DEPTO: TWideStringField;
    QrDiarioAddCodigo: TIntegerField;
    QrDiarioAddNome: TWideStringField;
    QrDiarioAddDiarioAss: TIntegerField;
    QrDiarioAddEntidade: TIntegerField;
    QrDiarioAddCliInt: TIntegerField;
    QrDiarioAddDepto: TIntegerField;
    QrDiarioAddData: TDateField;
    QrDiarioAddHora: TTimeField;
    QrDiarioAddDataCad: TDateField;
    QrDiarioAddDataAlt: TDateField;
    QrDiarioAddUserCad: TIntegerField;
    QrDiarioAddUserAlt: TIntegerField;
    QrDiarioAddNOME_UH_ENT: TWideStringField;
    DsDiarioAdd: TDataSource;
    Imprimirdiriodeitemselecionado1: TMenuItem;
    Imprimirdiriodeitenspesquisados1: TMenuItem;
    QrDiarioAddDuplicata: TWideStringField;
    QrDiarioAddDDeposito: TDateField;
    QrSacadosEmail: TWideStringField;
    RGJuridico: TRadioGroup;
    QrPesqJuridico: TSmallintField;
    N6: TMenuItem;
    EditarstatusJurdicoSelecionados1: TMenuItem;
    QrPesq2Juridico: TSmallintField;
    QrPesqCNAB_Lot: TIntegerField;
    QrPesq2CNAB_Lot: TIntegerField;
    Label13: TLabel;
    EdCNABLot: TdmkEdit;
    N7: TMenuItem;
    Localizarremessa1: TMenuItem;
    RGRemessa: TRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure EdCPFExit(Sender: TObject);
    procedure RGMascaraClick(Sender: TObject);
    procedure QrPesqCalcFields(DataSet: TDataSet);
    procedure QrPesqAfterOpen(DataSet: TDataSet);
    procedure BtZAZClick(Sender: TObject);
    procedure QrPesqAfterScroll(DataSet: TDataSet);
    procedure BtCuidadoClick(Sender: TObject);
    procedure BtPagtoDuvidaClick(Sender: TObject);
    procedure Inclui1Click(Sender: TObject);
    procedure Exclui1Click(Sender: TObject);
    procedure Inclui2Click(Sender: TObject);
    procedure Exclui2Click(Sender: TObject);
    procedure GradeItensDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure QrPesqAfterClose(DataSet: TDataSet);
    procedure PMPagtoPopup(Sender: TObject);
    procedure BtStatusManualClick(Sender: TObject);
    procedure BtDuplicataOcorrClick(Sender: TObject);
    procedure Incluiocorrncia1Click(Sender: TObject);
    procedure Alteraocorrncia1Click(Sender: TObject);
    procedure Excluiocorrncia1Click(Sender: TObject);
    procedure QrLotPrrCalcFields(DataSet: TDataSet);
    procedure Incluiprorrogao1Click(Sender: TObject);
    procedure ExcluiProrrogao1Click(Sender: TObject);
    procedure CkEIniClick(Sender: TObject);
    procedure TPEIniChange(Sender: TObject);
    procedure CkEFimClick(Sender: TObject);
    procedure TPEFimChange(Sender: TObject);
    procedure CkVIniClick(Sender: TObject);
    procedure TPVIniChange(Sender: TObject);
    procedure CkVFimClick(Sender: TObject);
    procedure TPVFimChange(Sender: TObject);
    procedure Recibo1Click(Sender: TObject);
    procedure QrVctoCalcFields(DataSet: TDataSet);
    procedure BtReabreClick(Sender: TObject);
    procedure EdColigadoChange(Sender: TObject);
    procedure IncluiPagamento1Click(Sender: TObject);
    procedure Excluipagamento1Click(Sender: TObject);
    procedure Recibodepagamento1Click(Sender: TObject);
    procedure QrOcorreuCalcFields(DataSet: TDataSet);
    procedure QrOcorreuAfterScroll(DataSet: TDataSet);
    procedure BtQuitacaoClick(Sender: TObject);
    procedure Quitadocumento1Click(Sender: TObject);
    procedure Imprimerecibodequitao1Click(Sender: TObject);
    procedure RGOrdem1Click(Sender: TObject);
    procedure RGOrdem2Click(Sender: TObject);
    procedure RGOrdem3Click(Sender: TObject);
    procedure RGOrdem4Click(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure QrPesq2CalcFields(DataSet: TDataSet);
    procedure GradeItensTitleClick(Column: TColumn);
    procedure BtRefreshClick(Sender: TObject);
    procedure QrOcu1CalcFields(DataSet: TDataSet);
    procedure QrOcu2CalcFields(DataSet: TDataSet);
    procedure QrPesqBeforeClose(DataSet: TDataSet);
    procedure PageControl1Change(Sender: TObject);
    procedure EdDuplicataChange(Sender: TObject);
    procedure EdEmitenteChange(Sender: TObject);
    procedure QrCNAB240CalcFields(DataSet: TDataSet);
    procedure QrClientesCalcFields(DataSet: TDataSet);
    procedure GradeItensDblClick(Sender: TObject);
    procedure N0Statusautomtico1Click(Sender: TObject);
    procedure N1Forastatusprorrogado1Click(Sender: TObject);
    procedure N2ForastatusBaixado1Click(Sender: TObject);
    procedure N3ForastatusMoroso1Click(Sender: TObject);
    procedure BtHistCNABClick(Sender: TObject);
    procedure Excluihistriciatual1Click(Sender: TObject);
    procedure Excluitodositensdehistricosdestaduplicata1Click(
      Sender: TObject);
    procedure QrCNAB240AfterOpen(DataSet: TDataSet);
    procedure frxPesq1GetValue(const VarName: String; var Value: Variant);
    procedure Extratodeduplicata1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure frxExtratoDuplicataGetValue(const VarName: string;
      var Value: Variant);
    procedure ImprimecartadeCancelamentodeProtesto1Click(Sender: TObject);
    procedure ImprimecartadeCancelamentodeProtesto2Click(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure SbBorderoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure QrOcorreuBeforeClose(DataSet: TDataSet);
    procedure EdClienteEnter(Sender: TObject);
    procedure PMDiarioPopup(Sender: TObject);
    procedure Adicionareventoaodiario1Click(Sender: TObject);
    procedure Gerenciardirio1Click(Sender: TObject);
    procedure Imprimirdiriodeitemselecionado1Click(Sender: TObject);
    procedure Imprimirdiriodeitenspesquisados1Click(Sender: TObject);
    procedure PMStatusPopup(Sender: TObject);
    procedure PMStatusManualPopup(Sender: TObject);
    procedure PMOcorreuPopup(Sender: TObject);
    procedure PMQuitacaoPopup(Sender: TObject);
    procedure PMHistCNABPopup(Sender: TObject);
    procedure RGJuridicoClick(Sender: TObject);
    procedure EditarstatusJurdicoSelecionados1Click(Sender: TObject);
    procedure Localizarremessa1Click(Sender: TObject);
  private
    { Private declarations }
    FOrdC1, FOrdC2, FOrdC3, FOrdS1, FOrdS2, FOrdS3: String;
    FEmitente, FCPF, FDuplicata: String;
    FCliente, FCNAB240I: Integer;
    FControle: Extended;
    FSomando: Boolean;
    FTempo, FUltim: TDateTime;

    function TextoSQLPesq(TabLote, TabLoteIts, Local: String): Integer;

    procedure ForcaStatus(NovoStatus: Integer);
    procedure InfoTempo(Tempo: TDateTime; Texto: String; Inicial: Boolean);
    procedure ConfiguraPg(Data: TDateTime);
    procedure AtualizaDevolucao(LOIS: Integer);
    procedure ReopenSubItens();
    procedure MostraEdicao(TipoForm: TTipoFormDup; SQLType: TSQLType; Cliente: Integer);
    procedure MostraGerDupOcor(SQLType: TSQLType; Prorroga: Boolean; Cliente: Integer);
    procedure MostraGerDupPgOc(SQLType: TSQLType);
    procedure ReopenLastOcor(Ocorreu: Integer);
    procedure ReopenLotPrr(Controle: Integer);
    procedure ReopenCNAB240(CNAB240I: Integer);
    procedure Pesquisa(Forca: Boolean);
    procedure CondicoesExtras(Query: TmySQLQuery; Coligado: Integer);
    procedure ReopenOcorP();
    procedure FecharTabelas;
    //Di�rio
    procedure ImprimeDiarioAdd(ID: Integer);
  public
    { Public declarations }
    FCallFromLot: Boolean;
    ForcaOBData: TDateTime;
    FOcorP, ForcaOcorBank, FLOIS, FAdupIts, FDupPgs: Integer;
    FOcorreu, FLotPrr, FLotePgOrigem: Integer;
    procedure PreparaDuplicataPg(Acao, LOIS: Integer);
    procedure ReabrirTabelas;
    procedure CalculaPagtoOcorP(Quitacao: TDateTime; OcorP: Integer);
    procedure ReopenOcorreu(Codigo: Integer);
  end;
var
  FmGerDup1Main: TFmGerDup1Main;

implementation

{$R *.DFM}

uses UnMyObjects, Module, GerDup1Baxa, UnInternalConsts, UMySQLModule, UnGOTOy, MyDBCheck,
  MyListas, SacadosEdit, GerDup1CCPr, Principal, GerDup1Ocor,
  ModuleLot, GerDup1PgOc, ModuleFin, UnBancos;

procedure TFmGerDup1Main.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGerDup1Main.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  EdCliente.SetFocus;
end;

procedure TFmGerDup1Main.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGerDup1Main.FormShow(Sender: TObject);
begin
  if Length(EdControle.Text) > 0 then
  begin
    CkStatus.SetMaxValue;
    ReabrirTabelas;
  end;
end;

procedure TFmGerDup1Main.FormCreate(Sender: TObject);
begin
  FCliente := 0;
  ImgTipo.SQLType := stPsq;
  //
  FOrdC1 := 'SALDO_DESATUALIZ';
  FOrdS1 := '';
  FOrdC2 := '';
  FOrdS2 := '';
  FOrdC3 := '';
  FOrdS3 := '';
  if Screen.Width > 800 then
  begin
    Width := Width + (Screen.Width - 800) div 2;
    Left  := (Screen.Width - Width) div 2;
  end;
  UMyMod.AbreQuery(QrClientes, Dmod.MyDB);
  UMyMod.AbreQuery(QrColigados, Dmod.MyDB);
  PageControl1.ActivePageIndex := 0;
  PageControl2.ActivePageIndex := 0;
  TPEIni.Date := Date - 180;
  TPEFim.Date := Date;
  TPVFim.Date := Date + 180;
  TPVIni.Date := Date;
  TPQFim.Date := Date;
  TPQIni.Date := Date;
  //
  CkRepassado.Value    := 3;
  CkStatus.Value       := 3;
  RGJuridico.ItemIndex := 2;
  RGRemessa.ItemIndex  := 2;
end;

procedure TFmGerDup1Main.EdClienteChange(Sender: TObject);
begin
  if EdCliente.ValueVariant <> FCliente then
    FecharTabelas;
end;

procedure TFmGerDup1Main.EdClienteEnter(Sender: TObject);
begin
  FCliente := EdCliente.ValueVariant;
end;

procedure TFmGerDup1Main.EdCPFExit(Sender: TObject);
begin
  FecharTabelas;
end;

procedure TFmGerDup1Main.RGJuridicoClick(Sender: TObject);
begin
  FecharTabelas;
end;

procedure TFmGerDup1Main.RGMascaraClick(Sender: TObject);
begin
  FecharTabelas;
end;

procedure TFmGerDup1Main.Pesquisa;
begin
  FecharTabelas;
end;

procedure TFmGerDup1Main.ReabrirTabelas;
const
  ItensOrdem: array[0..6] of String = ('NOMECLIENTE', 'li.DDeposito', 'PERIODO',
  'Emitente', 'CNPJ_TXT', 'li.Data3', 'PERIODO3');
var
  Atualizado, Ocorrencias, OcorAtualiz, TotalAtualiz, CalcAtualiz: Double;
  Orelha: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
  Orelha := PageControl1.ActivePageIndex;
  PageControl1.ActivePageIndex := 4;
  Application.ProcessMessages;
  InfoTempo(Now, 'Reabertura de tabelas', True);
  if Trim(EdEmitente.Text) <> '' then
  begin
    FEmitente := EdEmitente.Text;
    if RGMascara.ItemIndex in ([0,1]) then FEmitente := '%'+FEmitente;
    if RGMascara.ItemIndex in ([0,2]) then FEmitente := FEmitente+'%';
  end else FEmitente := '';
  if Trim(EdCPF.Text) <> '' then FCPF := Geral.SoNumero_TT(EdCPF.Text)
  else FCPF := '';
  if Trim(EdDuplicata.Text) <> '' then FDuplicata := EdDuplicata.Text
  else FDuplicata := '';
  FControle := EdControle.ValueVariant;
  FCliente := EdCliente.ValueVariant;
  //////////////////////////////////////////////////////////////////////////////
  QrPesq.Close;
  QrPesq.SQL.Clear;

  QrSoma.Close;
  QrSoma.SQL.Clear;

  QrVcto.Close;
  QrVcto.SQL.Clear;

  //Ok := Ok +
  //TextoSQLPesq('lot es', 'lot esits', '10');
  TextoSQLPesq(CO_TabLotA, CO_TablctA, '10');
{
  if CkMorto.Checked then
  begin
    QrPesq.SQL.Add('');
    QrPesq.SQL.Add('UNION');
    QrPesq.SQL.Add('');
    //
    QrSoma.SQL.Add('');
    QrSoma.SQL.Add('UNION');
    QrSoma.SQL.Add('');
    //
    QrVcto.SQL.Add('');
    QrVcto.SQL.Add('UNION');
    QrVcto.SQL.Add('');
    //
    //Ok := Ok +
    TextoSQLPesq('lotez', 'lotezits', '20');
  end;
}
  if RGFontes.ItemIndex > 0 then
  begin
    QrPesq.SQL.Add('');
    QrPesq.SQL.Add('UNION');
    QrPesq.SQL.Add('');
    //
    QrSoma.SQL.Add('');
    QrSoma.SQL.Add('UNION');
    QrSoma.SQL.Add('');
    //
    QrVcto.SQL.Add('');
    QrVcto.SQL.Add('UNION');
    QrVcto.SQL.Add('');
    //
    //Ok := Ok +
    TextoSQLPesq('lot0001b', 'lct0001b', '20');
  end;
  if RGFontes.ItemIndex > 1 then
  begin
    QrPesq.SQL.Add('');
    QrPesq.SQL.Add('UNION');
    QrPesq.SQL.Add('');
    //
    QrSoma.SQL.Add('');
    QrSoma.SQL.Add('UNION');
    QrSoma.SQL.Add('');
    //
    QrVcto.SQL.Add('');
    QrVcto.SQL.Add('UNION');
    QrVcto.SQL.Add('');
    //
    //Ok := Ok +
    TextoSQLPesq('lot0001d', 'lct0001d', '40');
  end;
  //

  case PageControl2.ActivePageIndex of
    0: QrPesq.SQL.Add(MLAGeral.OrdemSQL3(FOrdC1, FOrdS1, FOrdC2, FOrdS2, FOrdC3, FOrdS3));
    1: QrPesq.SQL.Add('ORDER BY '+
    ItensOrdem[RGOrdem1.ItemIndex]+', '+
    ItensOrdem[RGOrdem2.ItemIndex]+', '+
    ItensOrdem[RGOrdem3.ItemIndex]+', '+
    ItensOrdem[RGOrdem4.ItemIndex]);
    else QrPesq.SQL.Add('ORDER BY ?');
  end;
  UMyMod.AbreQuery(QrPesq, Dmod.MyDB);
  InfoTempo(Now, 'Duplicatas', False);
  UMyMod.AbreQuery(QrSoma, Dmod.MyDB);
  InfoTempo(Now, 'Saldo desatualizado', False);
  UMyMod.AbreQuery(QrVcto, Dmod.MyDB);
  InfoTempo(Now, 'Saldo Vencido', False);
  //////////////////////////////////////////////////////////////////////////////
  if FLOIS > 0 then QrPesq.Locate('FatParcela', FLOIS, []);
  //
  if CkAtualiza.Checked then
  begin
    QrSP.Close;
    QrSP.SQL := QrPesq.SQL;
    UMyMod.AbreQuery(QrSP, Dmod.MyDB);
    Atualizado   := 0;
    Ocorrencias  := 0;
    OcorAtualiz  := 0;
    TotalAtualiz := 0;
    EdAtualizado.Visible := False;
    ProgressBar1.Max := QrSP.RecordCount;
    ProgressBar1.Position := 0;
    ProgressBar1.Visible := True;
    while not QrSP.Eof do
    begin
      ProgressBar1.Position := ProgressBar1.Position + 1;
      CalcAtualiz := DMod.ObtemValorAtualizado(
        QrSPCliente.Value, QrSPQuitado.Value, QrSPVencimento.Value, Date,
        QrSPData3.Value, QrSPCredito.Value, QrSPTotalJr.Value,
        QrSPTotalDs.Value, QrSPTotalPg.Value, 0, True);
      Atualizado := Atualizado + CalcAtualiz;
      if CkOcorrencias.Checked then
      begin
{
        QrOcu2.Close;
        QrOcu2.Params[00].AsInteger := QrSPControle.Value;
        UMyMod.AbreQuery(QrOcu2);
}
        UnDmkDAC_PF.AbreMySQLQuery0(QrOcu2, Dmod.MyDB, [
        'SELECT  ob.Nome NOMEOCORRENCIA, oc.* ',
        'FROM ocorreu oc ',
        'LEFT JOIN ocorbank ob ON ob.Codigo = oc.Ocorrencia ',
        'WHERE oc.' + FLD_LOIS + '=' + Geral.FF0(QrSPFatParcela.Value),
        '']);
        //
        while not QrOcu2.Eof do
        begin
          Ocorrencias  := Ocorrencias  + QrOcu2SALDO.Value;
          OcorAtualiz  := OcorAtualiz  + QrOcu2ATUALIZADO.Value;
          TotalAtualiz := TotalAtualiz + QrOcu2ATUALIZADO.Value;
          QrOcu2.Next;
        end;
        TotalAtualiz := TotalAtualiz + CalcAtualiz;
      end;
      QrSP.Next;
    end;
    EdAtualizado.Text   := Geral.FFT(Atualizado,   2, siNegativo);
    EdOcorrencias.Text  := Geral.FFT(Ocorrencias,  2, siNegativo);
    EdOcorAtualiz.Text  := Geral.FFT(OcorAtualiz,  2, siNegativo);
    EdTotalAtualiz.Text := Geral.FFT(TotalAtualiz, 2, siNegativo);
    InfoTempo(Now, 'Atualiza��es', False);
  end else begin
    EdAtualizado.Text   := 'n�o calculado';
    EdOcorrencias.Text  := 'n�o calculado';
    EdOcorAtualiz.Text  := 'n�o calculado';
    EdTotalAtualiz.Text := 'n�o calculado';
  end;
  //
  ProgressBar1.Visible := False;
  EdAtualizado.Visible := True;
  Memo1.Lines.Add('========================================== Fim reaberturas');
  PageControl1.ActivePageIndex := Orelha;
  Screen.Cursor := crDefault;
  except
    Screen.Cursor := crDefault;
    raise;
  end;
end;

procedure TFmGerDup1Main.QrPesqCalcFields(DataSet: TDataSet);
var
  Taxas: MyArrayR07;
  Ocorrencias, OcorAtualiz, TotalAtualiz: Double;
begin
  if QrPesqLOCALT.Value < 20 then QrPesqNOMELOCAL.Value := 'Ativo' else
  if QrPesqLOCALT.Value < 30 then QrPesqNOMELOCAL.Value := 'Env�s' else
  QrPesqNOMELOCAL.Value := 'Morto';
  //////////////////////////////////////////////////////////////////////////////
  QrPesqNOMESTATUS.Value := MLAGeral.NomeStatusPgto2(QrPesqQuitado.Value,
    QrPesqDDeposito.Value, Date, QrPesqData3.Value, QrPesqRepassado.Value);
  //////////////////////////////////////////////////////////////////////////////
  if CkAtualiza.Checked then
  begin
    QrPesqSALDO_ATUALIZADO.Value := DMod.ObtemValorAtualizado(
      QrPesqCliente.Value, QrPesqQuitado.Value, QrPesqVencimento.Value, Date,
      QrPesqData3.Value, QrPesqCredito.Value, QrPesqTotalJr.Value, QrPesqTotalDs.Value,
      QrPesqTotalPg.Value, 0, True);
    QrPesqSALDO_TEXTO.Value := Geral.FFT(QrPesqSALDO_ATUALIZADO.Value, 2, siNegativo);
    if CkOcorrencias.Checked then
    begin
      Ocorrencias  := 0;
      OcorAtualiz  := 0;
      TotalAtualiz := 0;
{
      QrOcu1.Close;
      QrOcu1.Params[00].AsInteger := QrPesqFatParcela.Value;
      UMyMod.AbreQuery(QrOcu1);
}
      UnDmkDAC_PF.AbreMySQLQuery0(QrOcu1, Dmod.MyDB, [
      'SELECT ob.Nome NOMEOCORRENCIA, oc.* ',
      'FROM ocorreu oc ',
      'LEFT JOIN ocorbank ob ON ob.Codigo = oc.Ocorrencia ',
      'WHERE oc.' + FLD_LOIS + '=' + Geral.FF0(QrPesqFatParcela.Value),
      '']);
      //
      while not QrOcu1.Eof do
      begin
        Ocorrencias  := Ocorrencias  + QrOcu1SALDO.Value;
        OcorAtualiz  := OcorAtualiz  + QrOcu1ATUALIZADO.Value;
        TotalAtualiz := TotalAtualiz + QrOcu1ATUALIZADO.Value;
        QrOcu1.Next;
      end;
      QrPesqOCORRENCIAS.Value  := Ocorrencias;
      QrPesqOCORATUALIZ.Value  := OcorAtualiz;
      QrPesqTOTALATUALIZ.Value := TotalAtualiz + QrPesqSALDO_ATUALIZADO.Value;
    end else begin
      QrPesqOCORRENCIAS.Value  := 0;
      QrPesqOCORATUALIZ.Value  := 0;
      QrPesqTOTALATUALIZ.Value := 0;
    end;
  end else begin
    QrPesqSALDO_ATUALIZADO.Value := QrPesqSALDO_DESATUALIZ.Value;
    QrPesqSALDO_TEXTO.Value := 'n�o calculado';
    //
    QrPesqOCORRENCIAS.Value  := 0;
    QrPesqOCORATUALIZ.Value  := 0;
    QrPesqTOTALATUALIZ.Value := 0;
  end;
  //////////////////////////////////////////////////////////////////////////////
  QrPesqCNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrPesqCNPJCPF.Value);
  //
  Taxas := DMod.ObtemTaxaDeCompraRealizada(QrPesqFatParcela.Value);
  QrPesqTAXA_COMPRA.Value := QrPesqTxaCompra.Value + Taxas[0];
  //
  if (QrPesqQuitado.Value = -1) or (QrPesqQuitado.Value = 0) then
    QrPesqDATA3_TXT.Value := ''
  else
    QrPesqDATA3_TXT.Value := dmkPF.FDT_NULO(QrPesqDATA3.Value, 2);
end;

procedure TFmGerDup1Main.QrPesqAfterOpen(DataSet: TDataSet);
begin
  BtPagtoDuvida.Enabled      := Geral.IntToBool_0(QrPesq.RecordCount);
  BtCuidado.Enabled          := Geral.IntToBool_0(QrPesq.RecordCount);
  BtZAZ.Enabled              := Geral.IntToBool_0(QrPesq.RecordCount);
  BtDuplicataOcorr.Enabled   := Geral.IntToBool_0(QrPesq.RecordCount);
  BtStatusManual.Enabled     := Geral.IntToBool_0(QrPesq.RecordCount);
  BtQuitacao.Enabled         := Geral.IntToBool_0(QrPesq.RecordCount);
  Btimprime.Enabled          := Geral.IntToBool_0(QrPesq.RecordCount);
  BtRefresh.Enabled          := Geral.IntToBool_0(QrPesq.RecordCount);
  //
  EdITENS.ValueVariant       := QrPesq.RecordCount;
end;

procedure TFmGerDup1Main.ConfiguraPg(Data: TDateTime);
var
  ValorBase: Double;
begin
{
  QrLocPg.Close;
  QrLocPg.Params[0].AsInteger := QrPesqFatParcela.Value;
  QrLocPg. Open;
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocPg, Dmod.MyDB, [
  'SELECT * FROM adup pgs ',
  'WHERE Data=( ',
  '  SELECT MAX(Data) ',
  '  FROM adup pgs ',
  '  WHERE ' + FLD_LOIS + '=' + FormatFloat('0', QrPesqFatParcela.Value),
  ')',
  'ORDER BY Controle DESC ',
  '']);
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocPg, Dmod.MyDB, [
  'SELECT Data, FatParcela ',
  'FROM ' + CO_TabLctA,
  'WHERE FatID=' + TXT_VAR_FATID_0312,
  'AND Data=( ',
  '  SELECT MAX(Data) ',
  '  FROM ' + CO_TabLctA,
  '  WHERE FatID=' + TXT_VAR_FATID_0312,
  '  AND FatParcRef=' + Geral.FF0(QrPesqFatParcela.Value),
  ') ',
  'ORDER BY FatParcela DESC ',
  '']);
  //
  if QrLocPg.RecordCount > 0 then
  begin
      FmGerDup1Baxa.TPDataBase2.Date := Int(QrLocPgData.Value)
  end else FmGerDup1Baxa.TPDataBase2.Date := Int(QrPesqDDeposito.Value);
  FmGerDup1Baxa.EdJurosBase2.ValueVariant :=
    Dmod.ObtemTaxaDeCompraCliente(QrPesqCliente.Value);
  ValorBase := QrPesqCredito.Value  + QrPesqTotalJr.Value - QrPesqTotalDs.Value - QrPesqTotalPg.Value;
  FmGerDup1Baxa.TPPagto2.Date := Int(Date + 360);
  FmGerDup1Baxa.EdValorBase2.ValueVariant := ValorBase;
  if Data >= FmGerDup1Baxa.TPDataBase2.Date then
    FmGerDup1Baxa.TPPagto2.Date := Int(Data)
  else FmGerDup1Baxa.TPPagto2.Date := Int(FmGerDup1Baxa.TPDataBase2.Date);
  //FmGerDup1Baxa.TPPagto2.MinDate := Int(FmGerDup1Baxa.TPDataBase2.Date);
  FmGerDup1Baxa.TPVenc.Date := QrPesqVencimento.Value;
  // Erro
  //FmGerDup1Baxa.TPPagto2.SetFocus;
end;

procedure TFmGerDup1Main.PreparaDuplicataPg(Acao, LOIS: Integer);
begin
  FLOIS := LOIS;
  FAdupIts  := QrADupItsControle.Value;
  FDupPgs  := QrDupPgsFatParcela.Value;
  //
  Application.CreateForm(TFmGerDup1Baxa, FmGerDup1Baxa);
  FmGerDup1Baxa.ImgTipo.SQLType         := stIns;
  FmGerDup1Baxa.FLotePagto              := 0;
  FmGerDup1Baxa.EdCarteira.ValueVariant := QrPesqCartDep.Value;
  FmGerDup1Baxa.CBCarteira.KeyValue     := QrPesqCartDep.Value;
  FmGerDup1Baxa.FDuplicataOrigem        := QrPesqFatParcela.Value;
  FmGerDup1Baxa.EdCliCod.ValueVariant   := QrPesqCliente.Value;
  FmGerDup1Baxa.EdCliNome.Text          := QrPesqNOMECLIENTE.Value;
  FmGerDup1Baxa.EdDuplicata.Text        := QrPesqDuplicata.Value;
  FmGerDup1Baxa.EdCPF.Text              := Geral.FormataCNPJ_TT(QrPesqCNPJCPF.Value);
  FmGerDup1Baxa.EdEmitente.Text         := QrPesqEmitente.Value;
  FmGerDup1Baxa.EdValor.ValueVariant    := QrPesqCredito.Value;
  FmGerDup1Baxa.CkMovimento.Checked     := QrPesqCNAB_Lot.Value <> 0;
  ConfiguraPg(Date);
  //
  case Acao of
    1:
    begin
      FmGerDup1Baxa.GBPagto.Visible      := True;
      FmGerDup1Baxa.PainelStatus.Visible := False;
    end;
    2:
    begin
      FmGerDup1Baxa.GBPagto.Visible      := False;
      FmGerDup1Baxa.PainelStatus.Align   := alClient;
      FmGerDup1Baxa.PainelStatus.Visible := True;
      //
      FmGerDup1Baxa.CalculaJuros;
      FmGerDup1Baxa.CalculaAPagar(False);
    end;
    3:
    begin
      FmGerDup1Baxa.GBPagto.Visible      := True;
      FmGerDup1Baxa.PainelStatus.Visible := True;
      //
      FmGerDup1Baxa.CalculaJuros;
      FmGerDup1Baxa.CalculaAPagar(False);
    end;
  end;
  //
  FmGerDup1Baxa.ShowModal;
  FmGerDup1Baxa.Destroy;
  //
  AtualizaDevolucao(LOIS);
  ReabrirTabelas;
  //
  if ForcaOcorBank > 0 then
    MostraEdicao(tmfDupOcor, stIns, QrPesqCliente.Value);
end;

procedure TFmGerDup1Main.QrPesqAfterScroll(DataSet: TDataSet);
begin
  if FSomando then Exit;
  ReopenSubItens();
  ReopenOcorreu(0);
  ReopenLotPrr(0);
  ReopenCNAB240(0);
end;

procedure TFmGerDup1Main.ReopenSubItens();
var
  FatParcela: String;
begin
  FatParcela := FormatFloat('0', QrPesqFatParcela.Value);
{
  QrADupIts.Close;
  QrADupIts.Params[0].AsInteger := QrPesqFatParcela.Value;
  QrADupIts. Open;
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrADupIts, Dmod.MyDB, [
  'SELECT od.Nome NOMESTATUS, ad.* ',
  'FROM adupits ad ',
  'LEFT JOIN ocordupl od ON od.Codigo=ad.Alinea ',
  'WHERE ad.' + FLD_LOIS + '=' + FatParcela,
  'ORDER BY ad.DataA ',
  '']);
  if FAdupIts <> 0 then QrADupIts.Locate('Controle', FADupIts, []);
  //
{
  QrDupPgs.Close;
  QrDupPgs.Params[0].AsInteger := QrPesqFatParcela.Value;
  QrDupPgs. Open;
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrDupPgs, Dmod.MyDB, [
  'SELECT lot.Lote, adp.FatParcela, adp.FatNum, ',
  'adp.Data, adp.MoraVal, adp.DescoVal, adp.Credito, ',
  'adp.Controle, adp.Ocorreu, adp.Pago, ',
  'adp.FatParcRef, adp.FatGrupo ',
  'FROM ' + CO_TabLctA + ' adp ',
  'LEFT JOIN ' + CO_TabLotA + ' lot ON lot.Codigo=adp.FatNum ',
  'WHERE adp.FatID=' + TXT_VAR_FATID_0312,
  'AND adp.FatParcRef=' + FatParcela,
  'ORDER BY adp.Data, adp.FatParcela' ,
  '']);
  if FDupPgs <> 0 then
    QrDupPgs.Locate('FatParcela', FDupPgs, []);
end;

procedure TFmGerDup1Main.AtualizaDevolucao(LOIS: Integer);
var
  Devolucao: Integer;
begin
{
  QrPos.Close;
  QrPos.Params[0].AsInteger := LOIS;
  QrPos. Open;
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrPos, Dmod.MyDB, [
  'SELECT ad.Alinea ',
  'FROM adupits ad ',
  'WHERE ad.' + FLD_LOIS + '=' + FormatFloat('0', LOIS),
  'ORDER BY ad.DataA DESC, Controle DESC ',
  '']);
  //
{
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE lot esits SET AlterWeb=1, Devolucao=:P0 ');
  Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
  Dmod.QrUpd.Params[0].AsInteger := QrPosAlinea.Value;
  Dmod.QrUpd.Params[1].AsInteger := LOIS;
  Dmod.QrUpd.ExecSQL;
}
  Devolucao := QrPosAlinea.Value;
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_TabLctA, False, [
  'Devolucao'], ['FatID', 'FatParcela'], [
  Devolucao], [VAR_FATID_0301, LOIS], True);
  //
  ReabrirTabelas;
end;

procedure TFmGerDup1Main.BtCuidadoClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //
  MyObjects.MostraPopUpDeBotao(PMStatus, BtCuidado);
end;

procedure TFmGerDup1Main.BtPagtoDuvidaClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //
  MyObjects.MostraPopUpDeBotao(PMPagto, BtPagtoDuvida);
end;

procedure TFmGerDup1Main.BtZAZClick(Sender: TObject);
begin
  if (QrPesq.State = dsInactive) or (QrPesq.RecordCount = 0) then Exit;
  //
  if MyObjects.FIC(QrPesqJuridico.Value <> 0, nil,
    'A��o abortada! Item atual possui status jur�dico!') then Exit;
  //
  PreparaDuplicataPg(3, QrPesqFatParcela.Value);
end;

procedure TFmGerDup1Main.Inclui1Click(Sender: TObject);
begin
  PreparaDuplicataPg(2, QrPesqFatParcela.Value);
end;

procedure TFmGerDup1Main.Exclui1Click(Sender: TObject);
begin
  if QrADupIts.RecordCount > 0 then
  begin
    if Geral.MensagemBox('Confirma a exclus�o do item do hist�rico?',
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM adupits WHERE Controle=:P0');
      Dmod.QrUpd.Params[0].AsInteger := QrADupItsControle.Value;
      Dmod.QrUpd.ExecSQL;
      //
      FLOIS     := QrPesqFatParcela.Value;
      FADupIts := UmyMod.ProximoRegistro(QrADupIts, 'Controle',
        QrADupItsControle.Value);
      AtualizaDevolucao(QrPesqFatParcela.Value);
    end;
  end;
end;

procedure TFmGerDup1Main.Inclui2Click(Sender: TObject);
begin
  PreparaDuplicataPg(1, QrPesqFatParcela.Value);
end;

procedure TFmGerDup1Main.Exclui2Click(Sender: TObject);
var
  ValDup: Double;
begin
  if QrDupPgs.RecNo = QrDupPgs.RecordCount then
  begin
    if Geral.MensagemBox('Confirma a exclus�o do item de pagamento?',
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
    begin
{
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM adup pgs WHERE Controle=:P0');
      Dmod.QrUpd.Params[0].AsInteger := QrDupPgsFatParcela.Value;
      Dmod.QrUpd.ExecSQL;
}
{
      DmLot.Exclui_DupPg(QrDupPgsControle.Value,
        QrDupPgsFatParcela.Value, QrDupPgsOcorreu.Value);
}
      DmLot.Exclui_DupPg(QrDupPgsControle.Value,
        QrDupPgsFatParcela.Value, QrDupPgsOcorreu.Value,
        QrDupPgsFatParcRef.Value, QrDupPgsFatGrupo.Value,
        False, QrDupPgsFatNum.Value);
      //
      FLOIS     := QrPesqFatParcela.Value;
      FDupPgs := UmyMod.ProximoRegistro(QrDupPgs, 'Controle',
        QrDupPgsFatParcela.Value);
      // N�o precisa
      //AtualizaDevolucao(QrPesqFatParcela.Value);
      ValDup := QrPesqCredito.Value;// - QrPesqDebito.Value;
      DmLot.CalculaPagtoDuplicata(QrPesqFatParcela.Value, ValDup);
      ReabrirTabelas;
    end;
  end else Geral.MensagemBox('Somente o �ltimo pagamento pode ser exclu�do!',
      'Aviso', MB_OK+MB_ICONWARNING);
end;

procedure TFmGerDup1Main.GradeItensDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
  OldAlign: Integer;
begin
  with GradeItens.Canvas do
  begin
    if (Column.FieldName = 'NOMESTATUS') then
    begin
      Font.Color := MLAGeral.CorStatusPgto2(QrPesqQuitado.Value,
        QrPesqDDeposito.Value, Date);
      Font.Style := [fsBold];
      FillRect(Rect);
      OldAlign := SetTextAlign(Handle, TA_LEFT);
      TextOut(Rect.Left + 2,rect.Top + 2,Column.Field.DisplayText);
      SetTextAlign(Handle, OldAlign);
    end;
  end;
end;

procedure TFmGerDup1Main.QrPesqAfterClose(DataSet: TDataSet);
begin
  BtDuplicataOcorr.Enabled   := False;
  BtStatusManual.Enabled     := False;
  BtQuitacao.Enabled         := False;
  BtImprime.Enabled          := False;
  //
  EdITENS.ValueVariant       := 0;
  //
  QrADupIts.Close;
  QrDupPgs.Close;
end;

procedure TFmGerDup1Main.PMDiarioPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrPesq.State <> dsInactive) and (QrPesq.RecordCount > 0);
  //
  Adicionareventoaodiario1.Enabled := Enab;
  Gerenciardirio1.Enabled          := Enab;
  Localizarremessa1.Enabled        := Enab;
end;

procedure TFmGerDup1Main.PMHistCNABPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrPesq.State <> dsInactive) and (QrPesq.RecordCount > 0) and (QrPesqJuridico.Value = 0);
  Enab2 := (QrCNAB240.State <> dsInactive) and (QrCNAB240.RecordCount > 0);
  //
  Excluihistriciatual1.Enabled                       := Enab and Enab2;
  Excluitodositensdehistricosdestaduplicata1.Enabled := Enab and Enab2;
end;

procedure TFmGerDup1Main.PMOcorreuPopup(Sender: TObject);
var
  Enab, Enab2, Enab3, Enab4: Boolean;
begin
  Enab  := (QrPesq.State <> dsInactive) and (QrPesq.RecordCount > 0);
  Enab2 := (QrOcorreu.State <> dsInactive) and (QrOcorreu.RecordCount > 0);
  Enab3 := (QrOcorP.State <> dsInactive) and (QrOcorP.RecordCount > 0);
  Enab4 := (Enab) and (QrPesqJuridico.Value = 0);
  //
  Incluiocorrncia1.Enabled := Enab and Enab4;
  Alteraocorrncia1.Enabled := Enab and Enab2 and Enab4;
  Excluiocorrncia1.Enabled := Enab and Enab2 and Enab4;
  //
  IncluiPagamento1.Enabled := Enab and Enab2 and Enab4;
  Excluipagamento1.Enabled := Enab and Enab2 and Enab3 and Enab4;
  //
  Recibodepagamento1.Enabled := Enab and Enab2 and Enab3;  
end;

procedure TFmGerDup1Main.PMPagtoPopup(Sender: TObject);
var
  Enab, Enab2, Enab3: Boolean;
begin
  Enab  := (QrPesq.State <> dsInactive) and (QrPesq.RecordCount > 0);
  Enab2 := (QrDupPgs.State <> dsInactive) and (QrDupPgs.RecordCount > 0);
  Enab3 := (Enab) and (QrPesqJuridico.Value = 0);
  //
  Inclui2.Enabled := Enab and Enab3;
  Exclui2.Enabled := Enab and Enab2 and Enab3;
  Recibo1.Enabled := Enab and Enab2;
end;

procedure TFmGerDup1Main.PMQuitacaoPopup(Sender: TObject);
var
  Enab, Enab2, Enab3: Boolean;
begin
  Enab  := (QrPesq.State <> dsInactive) and (QrPesq.RecordCount > 0);
  Enab2 := (QrDupPgs.State <> dsInactive) and (QrDupPgs.RecordCount > 0);
  Enab3 := (Enab) and (QrPesqJuridico.Value = 0);
  //
  Quitadocumento1.Enabled                       := Enab and Enab3;
  Imprimerecibodequitao1.Enabled                := Enab and Enab2;
  ImprimecartadeCancelamentodeProtesto1.Enabled := Enab and Enab2;
end;

procedure TFmGerDup1Main.PMStatusManualPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrPesq.State <> dsInactive) and (QrPesq.RecordCount > 0) and (QrPesqJuridico.Value = 0);
  Enab2 := (QrLotPrr.State <> dsInactive) and (QrLotPrr.RecordCount > 0);
  //
  Incluiprorrogao1.Enabled := Enab;
  ExcluiProrrogao1.Enabled := Enab and Enab2;
  //
  N0Statusautomtico1.Enabled      := Enab;
  N1Forastatusprorrogado1.Enabled := Enab;
  N2ForastatusBaixado1.Enabled    := Enab;
  N3ForastatusMoroso1.Enabled     := Enab;
end;

procedure TFmGerDup1Main.PMStatusPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrPesq.State <> dsInactive) and (QrPesq.RecordCount > 0) and (QrPesqJuridico.Value = 0);
  Enab2 := (QrADupIts.State <> dsInactive) and (QrADupIts.RecordCount > 0);
  //
  Inclui1.Enabled := Enab;
  Exclui1.Enabled := Enab and Enab2; 
end;

procedure TFmGerDup1Main.MostraEdicao(TipoForm: TTipoFormDup; SQLType: TSQLType; Cliente: Integer);
begin
  ForcaOcorBank := 0;
  if QrPesq.State     = dsBrowse then FLOIS := QrPesqFatParcela.Value;
  if QrOcorreu.State  = dsBrowse then FOcorreu  := QrOcorreuCodigo.Value;
  if QrLotPrr.State = dsBrowse then FLotPrr := QrLotPrrControle.Value;
  if QrADupIts.State  = dsBrowse then FADupIts  := QrADupItsControle.Value;
  if QrDupPgs.State  = dsBrowse then FDupPgs  := QrDupPgsFatParcela.Value;
  case TipoForm of
    tmfDupOcor: MostraGerDupOcor(SQLType, False, Cliente);
    tmfDupPror: MostraGerDupOcor(SQLType, True, Cliente);
    tmfDupPgOc: MostraGerDupPgOc(SQLType);
  end;
  //
  if FCallFromLot then
    Close
  else
  begin
    Pesquisa(True);
    ReabrirTabelas;
  end;
end;

procedure TFmGerDup1Main.MostraGerDupOcor(SQLType: TSQLType; Prorroga: Boolean; Cliente: Integer);
begin
  if DBCheck.CriaFm(TFmGerDup1Ocor, FmGerDup1Ocor, afmoNegarComAviso) then
  begin
    FmGerDup1Ocor.FCliente        := Cliente;
    FmGerDup1Ocor.ImgTipo.SQLType := SQLType;
    if SQLType = stIns then
    begin
      if (ForcaOcorBank > 0) and (Prorroga = False) then
      begin
        FmGerDup1Ocor.EdOcorrencia.ValueVariant := ForcaOcorBank;
        FmGerDup1Ocor.CBOcorrencia.KeyValue := ForcaOcorBank;
        FmGerDup1Ocor.TPDataO.Date := ForcaOBData;
      end;
    end else
    begin
      FmGerDup1Ocor.EdOcorrencia.ValueVariant := QrOcorreuOcorrencia.Value;
      FmGerDup1Ocor.CBOcorrencia.KeyValue := QrOcorreuOcorrencia.Value;
      FmGerDup1Ocor.TPDataO.Date := QrOcorreuDataO.Value;
      FmGerDup1Ocor.EdValor.ValueVariant := QrOcorreuValor.Value;
    end;

    //

    if Prorroga then
    begin
      //ERRO: Controle Duplicatas: Liberar data da ocorr�ncia na prorroga��o.
      FmGerDup1Ocor.GBPror.Visible := True;
      //
      // Permitir Retroceder data
      //TPDataO.Enabled    := False;
      if SQLType = stIns then
        FmGerDup1Ocor.ConfiguraPr(Date);
    end;

    //

    FmGerDup1Ocor.ShowModal;
    FmGerDup1Ocor.Destroy;
  end;
end;

procedure TFmGerDup1Main.MostraGerDupPgOc(SQLType: TSQLType);
begin
  if (SQLType <> stIns) and DmLot.AvisoDeNaoUpd(2) then Exit;
  //
  if DBCheck.CriaFm(TFmGerDup1PgOc, FmGerDup1PgOc, afmoNegarComAviso) then
  begin
    FmGerDup1PgOc.ImgTipo.SQLType := SQLType;
    if SQLType = stIns then
    begin
      FmGerDup1PgOc.ConfiguraPgOC();
      FmGerDup1PgOc.CalculaJurosOcor();
      FmGerDup1PgOc.CalculaAPagarOcor();
    end;
    FmGerDup1PgOc.ShowModal;
    FmGerDup1PgOc.Destroy;
  end;
end;

procedure TFmGerDup1Main.BtStatusManualClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 2;
  //  
  MyObjects.MostraPopUpDeBotao(PMStatusManual, BtStatusManual);
end;

procedure TFmGerDup1Main.BtDuplicataOcorrClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 1;
  //
  MyObjects.MostraPopUpDeBotao(PMOcorreu, BtDuplicataOcorr);
end;

procedure TFmGerDup1Main.Incluiocorrncia1Click(Sender: TObject);
begin
  MostraEdicao(tmfDupOcor, stIns, QrPesqCliente.Value);
end;

procedure TFmGerDup1Main.Adicionareventoaodiario1Click(Sender: TObject);
var
  Texto: String;
begin
  Texto := QrPesqCNPJCPF.Value + ' - ' + QrPesqEmitente.Value + #13#10;
  //
  FmPrincipal.MostraDiarioAdd(0, QrPesqCliente.Value, QrPesqControle.Value, Texto);
end;

procedure TFmGerDup1Main.Alteraocorrncia1Click(Sender: TObject);
begin
  MostraEdicao(tmfDupOcor, stUpd, QrPesqCliente.Value);
end;

procedure TFmGerDup1Main.Excluiocorrncia1Click(Sender: TObject);
begin
  if Geral.MensagemBox('Confirma a exclus�o da ocorr�ncia selecionada?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM ocorreu WHERE Codigo=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrOcorreuCodigo.Value;
    Dmod.QrUpd.ExecSQL;
    QrOcorreu.Next;
    //
    ReopenOcorreu(QrOcorreuCodigo.Value);
  end;
end;

procedure TFmGerDup1Main.ReopenOcorreu(Codigo: Integer);
begin
{
  QrOcorreu.Close;
  QrOcorreu.Params[00].AsInteger := QrPesqFatParcela.Value;
  QrOcorreu. Open;
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrOcorreu, Dmod.MyDB, [
  'SELECT  ob.Nome NOMEOCORRENCIA, ob.PlaGen, ',
  'oc.' + FLD_LOIS + ' LOIS, oc.* ',
  'FROM ocorreu oc ',
  'LEFT JOIN ocorbank ob ON ob.Codigo = oc.Ocorrencia ',
  'WHERE oc.' + FLD_LOIS + '=' + FormatFloat('0', QrPesqFatParcela.Value),
  '']);
  //
  if Codigo <> 0 then
    QrOcorreu.Locate('Codigo', Codigo, [])
  else
    QrOcorreu.Locate('Codigo', FOcorreu, []);
end;

procedure TFmGerDup1Main.ReopenLastOcor(Ocorreu: Integer);
begin
{
  QrLastOcor.Close;
  QrLastOcor.Params[0].AsInteger := QrOcorPOcorreu.Value;
  UMyMod.AbreQuery(QrLastOcor);
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrLastOcor, Dmod.MyDB, [
  'SELECT Data, FatParcela ',
  'FROM ' + CO_TabLctA,
  'WHERE FatID=' + TXT_VAR_FATID_0304,
  'AND Ocorreu=' + Geral.FF0(Ocorreu),
  'ORDER BY Data Desc, FatID Desc ',
  '']);
  //
end;

procedure TFmGerDup1Main.ReopenLotPrr(Controle: Integer);
begin
{
  QrLotPrr.Close;
  QrLotPrr.Params[00].AsInteger := QrPesqFatParcela.Value;
  QrLotPrr. Open;
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrLotPrr, Dmod.MyDB, [
  'SELECT * FROM ' + CO_TabLotPrr,
  'WHERE Codigo=' + FormatFloat('0', QrPesqFatParcela.Value),
  'ORDER BY Controle ',
  '']);
  //
  if Controle <> 0 then QrLotPrr.Locate('Controle', Controle, [])
  else QrLotPrr.Locate('Controle', FLotPrr, []);
end;

procedure TFmGerDup1Main.ReopenCNAB240(CNAB240I: Integer);
begin
  QrCNAB240.Close;
  QrCNAB240.Params[00].AsInteger := QrPesqFatParcela.Value;
  UMyMod.AbreQuery(QrCNAB240, Dmod.MyDB);
  //
  if CNAB240I <> 0 then QrCNAB240.Locate('CNAB240I', CNAB240I, [])
  else QrCNAB240.Locate('CNAB240I', FCNAB240I, []);
end;

procedure TFmGerDup1Main.ImprimeDiarioAdd(ID: Integer);
var
  Depto: String;
begin
  Screen.Cursor := crHourGlass;
  try
    if ID <> 0 then
      Depto := Geral.FF0(ID)
    else if Length(Depto) = 0 then
    begin
      if QrPesq.RecordCount > 40 then
      begin
        if Geral.MensagemBox('Pesquisar muitos itens pode demorar v�rios minutos.' +
          #13#10 + 'Deseja continuar assim mesmo?', 'Pergunta',
          MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then Exit;
      end;
      QrPesq.First;
      while not QrPesq.Eof do
      begin
        if QrPesq.RecNo = QrPesq.RecordCount then
          Depto := Depto + Geral.FF0(QrPesqControle.Value)
        else
          Depto := Depto + Geral.FF0(QrPesqControle.Value) + ', ';
        QrPesq.Next;
      end;
    end;
    QrDiarioAdd.Close;
    QrDiarioAdd.SQL.Clear;
    QrDiarioAdd.SQL.Add('SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOME_UH_ENT,');
    QrDiarioAdd.SQL.Add('IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NOME_CLI,');
    QrDiarioAdd.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOME_ENT,');
    QrDiarioAdd.SQL.Add('"" NOME_DEPTO, lct.Duplicata, lct.DDeposito, dad.*');
    QrDiarioAdd.SQL.Add('FROM diarioadd dad');
    QrDiarioAdd.SQL.Add('LEFT JOIN entidades cli ON cli.Codigo=dad.CliInt');
    QrDiarioAdd.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=dad.Entidade');
    QrDiarioAdd.SQL.Add('LEFT JOIN lct0001a lct ON lct.Controle=dad.Depto AND lct.FatID=' + Geral.FF0(VAR_FATID_0301));
    QrDiarioAdd.SQL.Add('WHERE dad.Depto IN(' + Depto + ')');
    QrDiarioAdd.SQL.Add('ORDER BY dad.Data, dad.Hora, NOME_UH_ENT');
    QrDiarioAdd.Open;
    //
    MyObjects.frxMostra(frxGER_DIARI_001_01, 'Pesquisa de Di�rio');
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmGerDup1Main.QrLotPrrCalcFields(DataSet: TDataSet);
begin
  QrLotPrrDIAS_1_3.Value := Trunc(Int(QrLotPrrData3.Value)-Int(QrLotPrrData1.Value));
  QrLotPrrDIAS_2_3.Value := Trunc(Int(QrLotPrrData3.Value)-Int(QrLotPrrData2.Value));
end;

procedure TFmGerDup1Main.Incluiprorrogao1Click(Sender: TObject);
begin
  MostraEdicao(tmfDupPror, stIns, QrPesqCliente.Value);
end;

procedure TFmGerDup1Main.ExcluiProrrogao1Click(Sender: TObject);
var
  Ocor: Integer;
  Orig: Integer;
  //
  Vencimento, DDeposito: String;
  ProrrVz, ProrrDd, Quitado, FatParcela: Integer;
begin
  if QrLotPrr.RecNo <> QrLotPrr.RecordCount then
  Geral.MensagemBox('Somente a �ltima prorroga��o pode ser exclu�da!',
  'Aviso', MB_OK+MB_ICONWARNING) else
  begin
    if Geral.MensagemBox('Confirma a exclus�o da prorroga��o?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
    begin
      Ocor := QrLotPrrOcorrencia.Value;
      Orig := Trunc(QrLotPrrData1.Value);
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM ' + CO_TabLotPrr); //lot esprr
      Dmod.QrUpd.SQL.Add('WHERE Controle=:P0');
      Dmod.QrUpd.Params[0].AsInteger := QrLotPrrControle.Value;
      Dmod.QrUpd.ExecSQL;
      //
      ReopenLotPrr(0);
      QrLotPrr.Last;
      ProrrDd := Trunc(Int(QrLotPrrData3.Value)-Int(QrLotPrrData1.Value));
      if QrLotPrrData3.Value < 2 then
      begin
        DDeposito := Geral.FDT(Orig, 1);
        Quitado := 0;
      end else
      begin
        DDeposito := Geral.FDT(QrLotPrrData3.Value, 1);
        Quitado := QrPesqQuitado.Value;
      end;
      Vencimento := DDeposito;
{
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE lot esits SET AlterWeb=1,   DDeposito=:P0, ProrrVz=:P1, ');
      Dmod.QrUpd.SQL.Add('ProrrDd=:P2 WHERE Controle=:P3');
      Dmod.QrUpd.Params[0].AsString  := DDep;
      Dmod.QrUpd.Params[1].AsInteger := QrLotPrr.RecordCount;
      Dmod.QrUpd.Params[2].AsInteger := Dias;
      //
      Dmod.QrUpd.Params[3].AsInteger := QrPesqFatParcela.Value;
      Dmod.QrUpd.ExecSQL;
}
      ProrrVz := QrLotPrr.RecordCount;
      FatParcela := QrPesqFatParcela.Value;
      //
      // 2011-11-28: Acrescentado a altera�ao do campo "Quitado" (volta a ser zero quando exclui a primeira prorroga��o!)
      // 2012-01-02: Acrescido a altera��o do campo "Vencimento" (volta a ser a data antiga!);
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_TabLctA, False, [
      'Vencimento', 'DDeposito', 'ProrrVz',
      'ProrrDd', 'Quitado'], [
      'FatID', 'FatParcela'], [
      Vencimento, DDeposito, ProrrVz,
      ProrrDd, Quitado], [
      VAR_FATID_0301, FatParcela], True);
      //
      FLOIS := QrPesqFatParcela.Value;
      FOcorreu  := QrOcorreuCodigo.Value;
      FLotPrr := QrLotPrrControle.Value;
      //
      if Geral.MensagemBox(
      'Deseja excluir tamb�m a ocorr�ncia da prorroga��o exclu�da?',
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
      begin
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('DELETE FROM ocorreu WHERE Codigo=:P0');
        Dmod.QrUpd.Params[0].AsInteger := Ocor;
        Dmod.QrUpd.ExecSQL;
        QrOcorreu.Next;
        //
        ReopenOcorreu(QrOcorreuCodigo.Value);
      end;
      Pesquisa(True);
      ReabrirTabelas();
    end;
  end;
end;

procedure TFmGerDup1Main.CkEIniClick(Sender: TObject);
begin
  FecharTabelas;
end;

procedure TFmGerDup1Main.TPEIniChange(Sender: TObject);
begin
  if CkEIni.Checked then FecharTabelas;
end;

procedure TFmGerDup1Main.CkEFimClick(Sender: TObject);
begin
  FecharTabelas;
end;

procedure TFmGerDup1Main.TPEFimChange(Sender: TObject);
begin
  if CkEFim.Checked then FecharTabelas;
end;

procedure TFmGerDup1Main.CkVIniClick(Sender: TObject);
begin
  FecharTabelas;
end;

procedure TFmGerDup1Main.TPVIniChange(Sender: TObject);
begin
  if CkVIni.Checked then FecharTabelas;
end;

procedure TFmGerDup1Main.CkVFimClick(Sender: TObject);
begin
  FecharTabelas;
end;

procedure TFmGerDup1Main.TPVFimChange(Sender: TObject);
begin
  if CkVFim.Checked then FecharTabelas;
end;

procedure TFmGerDup1Main.Recibo1Click(Sender: TObject);
var
  Pagamento, Texto: String;
begin
  Pagamento := MLAGeral.NomeStatusPgto5(QrPesqQuitado.Value,
    QrPesqDDeposito.Value, Date, '', False, QrPesqDevolucao.Value,
    QrPesqQuitado.Value, QrPesqProrrVz.Value, QrPesqProrrDd.Value, True, False);
  Texto := Pagamento + ' da duplicata ' + QrPesqDuplicata.Value +
    ' emitida por '+ QrPesqEmitente.Value + ' em '+FormatDateTime(
    'dd" de "mmmm" de "yyyy', QrPesqData.Value);
  //
  //if QrLctDebito.Value > 0 then
  GOTOy.EmiteRecibo(QrPesqCliente.Value, Dmod.QrMasterDono.Value, CO_BENEFICIARIO_0,
    QrDupPgsCredito.Value, 0, 0, 'DUP-'+FormatFloat('000',
    QrDupPgsFatParcela.Value), Texto, '', '', QrDupPgsData.Value, 0);
end;

procedure TFmGerDup1Main.QrVctoCalcFields(DataSet: TDataSet);
(*var
  Cliente: Integer;
  Juro: Double;*)
begin
  if QrVctoSALDO_DESATUALIZADO.Value <> 0 then
    QrVctoPRAZO_MEDIO.Value := QrVctoFATOR_VD.Value /
    QrVctoSALDO_DESATUALIZADO.Value
  else QrVctoPRAZO_MEDIO.Value := 0;
  //
  (*Cliente := EdCliente.ValueVariant;
  if Cliente = 0 then
  begin
    QrVctoJURO_CLI.Value   := 0;
    DBEdTaxa.Visible := False;
  end else begin
    QrVctoJURO_CLI.Value   := Dmod.ObtemTaxaDeCompraCliente(Cliente);
    DBEdTaxa.Visible       := True;
  end;*)
end;

procedure TFmGerDup1Main.BtReabreClick(Sender: TObject);
begin
  ReabrirTabelas;
end;

procedure TFmGerDup1Main.EdColigadoChange(Sender: TObject);
begin
  FecharTabelas;
end;

procedure TFmGerDup1Main.CondicoesExtras(Query: TmySQLQuery; Coligado: Integer);
begin
  case CkRepassado.Value of
    0:
    begin
      Geral.MensagemBox('Defina o status de repassado!', 'Aviso',
        MB_OK+MB_ICONWARNING);
      Exit;
    end;
    1:
    begin
      if Coligado = 0 then Query.SQL.Add('AND li.Repassado = 1')
      else begin
        Query.SQL.Add('AND (re.Coligado = ' + FormatFloat('0', Coligado)+
        ' AND li.Repassado = 1)')
      end;
    end;
    2: Query.SQL.Add('AND li.Repassado = 0');
    3:
    begin
      if Coligado <> 0 then
      begin
        Query.SQL.Add('AND (li.Repassado = 0 OR (');
        Query.SQL.Add('li.Repassado=1 AND re.Coligado = '+
        IntToStr(Coligado)+'))');
      end;
    end;
  end;
  case CkStatus.Value of
    // Nada
    0: Query.SQL.Add('AND li.Quitado=-1000');
    // Vencido
    1: Query.SQL.Add('AND li.Quitado in (-1,0,1) AND li.DDeposito < CURRENT_DATE');
    // Aberto
    2: Query.SQL.Add('AND li.Quitado in (-1,0,1) AND li.DDeposito >= CURRENT_DATE');
    // Vencido + Aberto
    3: Query.SQL.Add('AND li.Quitado in (-1,0,1)');
    // Quitado
    4: Query.SQL.Add('AND li.Quitado in (2,3)');
    5: // Vencido + Quitado
    begin
      Query.SQL.Add('AND ((li.Quitado in (-1,0,1)) ');
      Query.SQL.Add('  AND (li.DDeposito < CURRENT_DATE) ');
      Query.SQL.Add('OR (li.Quitado in (2,3)))');
    end;
    6: // Aberto + Quitado
    begin
      Query.SQL.Add('AND ((li.Quitado in (-1,0,1)) ');
      Query.SQL.Add('  AND (li.DDeposito >= CURRENT_DATE) ');
      Query.SQL.Add('OR (li.Quitado in (2,3)))');
    end;
    // Vencido + Aberto + Quitado
    7: Query.SQL.Add('AND li.Quitado in (-1,0,1,2,3)');
    // Baixado
    8: Query.SQL.Add('AND li.Quitado =-2');
    9: // Vencido + Baixado
    begin
      Query.SQL.Add('AND ((li.Quitado in (-1,0,1)) ');
      Query.SQL.Add('  AND (li.DDeposito < CURRENT_DATE) ');
      Query.SQL.Add('OR (li.Quitado =-2))');
    end;
    10: // Aberto + Baixado
    begin
      Query.SQL.Add('AND ((li.Quitado in (-1,0,1)) ');
      Query.SQL.Add('  AND (li.DDeposito >= CURRENT_DATE) ');
      Query.SQL.Add('OR (li.Quitado =-2))');
    end;
    // Vencido + Aberto + Baixado
    11: Query.SQL.Add('AND li.Quitado in (-2,-1,0,1)');
    // Quitado
    12: Query.SQL.Add('AND li.Quitado in (-2,2,3)');
    13: // Vencido + Quitado + Baixado
    begin
      Query.SQL.Add('AND ((li.Quitado in (-1,0,1)) ');
      Query.SQL.Add('  AND (li.DDeposito < CURRENT_DATE) ');
      Query.SQL.Add('OR (li.Quitado in (-2,2,3)))');
    end;
    14: // Aberto + Quitado + Baixado
    begin
      Query.SQL.Add('AND ((li.Quitado in (-1,0,1)) ');
      Query.SQL.Add('  AND (li.DDeposito >= CURRENT_DATE) ');
      Query.SQL.Add('OR (li.Quitado in (-2,2,3)))');
    end;
    // Vencido + Aberto + Quitado
    15: Query.SQL.Add('AND li.Quitado in (-2,-1,0,1,2,3)');
    16: Query.SQL.Add('AND li.Quitado =-3');
    17: // Vencido + Moroso
    begin
      Query.SQL.Add('AND ((li.Quitado in (-1,0,1)) ');
      Query.SQL.Add('  AND (li.DDeposito < CURRENT_DATE) ');
      Query.SQL.Add('OR (li.Quitado =-3))');
    end;
    18: // Aberto + Moroso
    begin
      Query.SQL.Add('AND ((li.Quitado in (-1,0,1)) ');
      Query.SQL.Add('  AND (li.DDeposito >= CURRENT_DATE) ');
      Query.SQL.Add('OR (li.Quitado =-3))');
    end;
    // Vencido + Aberto + Moroso
    19: Query.SQL.Add('AND li.Quitado in (-3,-1,0,1)');
    // Quitado
    20: Query.SQL.Add('AND li.Quitado in (-3,2,3)');
    21: // Vencido + Quitado + Moroso
    begin
      Query.SQL.Add('AND ((li.Quitado in (-1,0,1)) ');
      Query.SQL.Add('  AND (li.DDeposito < CURRENT_DATE) ');
      Query.SQL.Add('OR (li.Quitado in (-3,2,3)))');
    end;
    22: // Aberto + Quitado + Moroso
    begin
      Query.SQL.Add('AND ((li.Quitado in (-1,0,1)) ');
      Query.SQL.Add('  AND (li.DDeposito >= CURRENT_DATE) ');
      Query.SQL.Add('OR (li.Quitado in (-3,2,3)))');
    end;
    // Vencido + Aberto + Quitado
    23: Query.SQL.Add('AND li.Quitado in (-3,-1,0,1,2,3)');
    24: Query.SQL.Add('AND li.Quitado in (-3,-2)');
    25: // Vencido + Moroso
    begin
      Query.SQL.Add('AND ((li.Quitado in (-1,0,1)) ');
      Query.SQL.Add('  AND (li.DDeposito < CURRENT_DATE) ');
      Query.SQL.Add('OR (li.Quitado in (-3,-2)))');
    end;
    26: // Aberto + Moroso
    begin
      Query.SQL.Add('AND ((li.Quitado in (-1,0,1)) ');
      Query.SQL.Add('  AND (li.DDeposito >= CURRENT_DATE) ');
      Query.SQL.Add('OR (li.Quitado in (-3,-2)))');
    end;
    // Vencido + Aberto + Baixado + Moroso
    27: Query.SQL.Add('AND li.Quitado in (-3,-2,-1,0,1)');
    // Quitado
    28: Query.SQL.Add('AND li.Quitado in (-3,-2,2,3)');
    29: // Vencido + Quitado + Baixado + Moroso
    begin
      Query.SQL.Add('AND ((li.Quitado in (-1,0,1)) ');
      Query.SQL.Add('  AND (li.DDeposito < CURRENT_DATE) ');
      Query.SQL.Add('OR (li.Quitado in (-3,-2,2,3)))');
    end;
    30: // Aberto + Quitado + Baixado + Moroso
    begin
      Query.SQL.Add('AND ((li.Quitado in (-1,0,1)) ');
      Query.SQL.Add('  AND (li.DDeposito >= CURRENT_DATE) ');
      Query.SQL.Add('OR (li.Quitado in (-3,-2,2,3)))');
    end;
    // Vencido + Aberto + Quitado + Baixado + Moroso
    31: Query.SQL.Add('AND li.Quitado in (-3,-2,-1,0,1,2,3)');
  end;
  // Parei aqui
end;

procedure TFmGerDup1Main.CalculaPagtoOcorP(Quitacao: TDateTime; OcorP: Integer);
var
  Sit: Integer;
begin
{
  QrSumOc.Close;
  QrSumOc.Params[0].AsInteger := OcorP;
  UMyMod.AbreQuery(QrSumOc);
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumOC, Dmod.MyDB, [
  'SELECT SUM(MoraVal) Juros, SUM(Credito-Debito) Pago ',
  'FROM ' + CO_TabLctA,
  'WHERE FatID=' + TXT_VAR_FATID_0304,
  'AND Ocorreu=' + Geral.FF0(OcorP),
  '']);
  if QrSumOcPago.Value < 0.01 then
    Sit := 0
  //Est� com erro - Atualizado em 09/08/2012
  //else if QrSumOcPago.Value < (QrOcorreuValor.Value + ==>QrOcorreuTaxaV.Value<==)-0.009 then
  else if QrSumOcPago.Value < (QrOcorreuValor.Value + QrSumOcJuros.Value)-0.009 then
    Sit := 1
  else
    Sit := 2;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE ocorreu SET AlterWeb=1, ');
  Dmod.QrUpd.SQL.Add('TaxaV=:P0, Pago=:P1, Status=:P2, ');
  Dmod.QrUpd.SQL.Add('Data3=:P3 WHERE Codigo=:Pa');
  //
  Dmod.QrUpd.Params[00].AsFloat   := QrSumOcJuros.Value;
  Dmod.QrUpd.Params[01].AsFloat   := QrSumOcPago.Value;
  Dmod.QrUpd.Params[02].AsInteger := Sit;
  Dmod.QrUpd.Params[03].AsString  := FormatDateTime(VAR_FORMATDATE, Quitacao);
  Dmod.QrUpd.Params[04].AsInteger := OcorP;
  Dmod.QrUpd.ExecSQL;
  //
end;

procedure TFmGerDup1Main.IncluiPagamento1Click(Sender: TObject);
begin
  MostraEdicao(tmfDupPgOc, stIns, QrPesqCliente.Value);
end;

procedure TFmGerDup1Main.Excluipagamento1Click(Sender: TObject);
var
  Ocorreu: Integer;
begin
  ReopenLastOcor(QrOcorPOcorreu.Value);
  //
  UMyMod.AbreQuery(QrLastOcor, Dmod.MyDB);
  if (QrLastOcorData.Value > QrOcorPData.Value)
  or ((QrLastOcorData.Value = QrOcorPData.Value)
  and (QrLastOcorFatParcela.Value > QrOcorPFatParcela.Value))
  then
     Geral.MensagemBox('Somente o �ltimo pagamento pode ser exclu�do!',
    'Aviso', MB_OK+MB_ICONWARNING)
  else begin
    if Geral.MensagemBox('Confirma a exclus�o deste pagamento?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
    begin
      Screen.Cursor := crHourGlass;
      try
        Ocorreu := QrOcorPOcorreu.Value;
{
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('DELETE FROM ocor rpg WHERE Codigo=:P0');
        Dmod.QrUpd.Params[0].AsInteger := QrOcorPFatParcela.Value;
        Dmod.QrUpd.ExecSQL;
}
        DmLot.Exclui_OcorP(QrOcorPControle.Value, QrOcorPFatParcela.Value,
          QrOcorPOcorreu.Value);
        //
        ReopenLastOcor(Ocorreu);
        //
        CalculaPagtoOcorP(QrLastOcorData.Value, Ocorreu);
        //
        QrOcorP.Next;
        FOcorP := QrOcorPFatParcela.Value;
        ReopenOcorreu(QrOcorreuCodigo.Value);
        Screen.Cursor := crDefault;
      except
        Screen.Cursor := crDefault;
      end;
    end;
  end;
end;

procedure TFmGerDup1Main.ReopenOcorP();
begin
{
  QrOcorP.Close;
  QrOcorP.Params[0].AsInteger := QrOcorreuCodigo.Value;
  UMyMod.AbreQuery(QrOcorP);
  //
  QrOcorP.Locate('FatParcela', FOcorP, []);
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrOcorP, Dmod.MyDB, [
  'SELECT lct.Ocorreu, lct.Data, lct.FatParcela, ',
  'lct.Controle, lct.FatNum, lct.MoraVal, ',
  'lct.Credito - lct.Debito Pago ',
  'FROM ' + CO_TabLctA + ' lct ',
  'WHERE lct.FatID=' + TXT_VAR_FATID_0304,
  'AND lct.Ocorreu=' + Geral.FF0(QrOcorreuCodigo.Value),
  '']);
  //
  QrOcorP.Locate('FatParcela', FOcorP, []);
end;

procedure TFmGerDup1Main.Recibodepagamento1Click(Sender: TObject);
var
  Pagamento, Texto: String;
begin
  Pagamento := MLAGeral.NomeStatusPgto6(QrOcorreuATUALIZADO.Value,
    QrOcorP.RecNo)+' da ocorr�ncia "'+ QrOcorreuNOMEOCORRENCIA.Value+
    '" cobrada sobre';
  Texto := Pagamento + ' a duplicata n� ' + QrPesqDuplicata.Value +
    ' emitida por '+ QrPesqEmitente.Value + ' para o dia '+ FormatDateTime(
    'dd" de "mmmm" de "yyyy', QrPesqVencimento.Value)+'. A quita��o desta '+
    'ocorr�ncia n�o quita a duplicata';
  GOTOy.EmiteRecibo(QrPesqCliente.Value, Dmod.QrMasterDono.Value, CO_BENEFICIARIO_0,
    QrOcorPPago.Value, 0, 0, 'DUO-'+FormatFloat('000',
    QrOcorPFatParcela.Value), Texto, '', '', QrOcorPData.Value, 0);
end;

procedure TFmGerDup1Main.QrOcorreuCalcFields(DataSet: TDataSet);
begin
  QrOcorreuSALDO.Value := QrOcorreuValor.Value + QrOcorreuTaxaV.Value - QrOcorreuPago.Value;
  //
  if CkAtualiza.Checked then
  begin
    QrOcorreuATUALIZADO.Value := DMod.ObtemValorAtualizado(
      QrOcorreuCliente.Value, 1, QrOcorreuDataO.Value, Date, QrOcorreuData3.Value,
      QrOcorreuValor.Value, QrOcorreuTaxaV.Value, 0 (*Desco*),
      QrOcorreuPago.Value, (*QrOcorreuTaxaP.Value*)0, (*False*)True);
    QrOcorreuATZ_TEXTO.Value := Geral.FFT(QrOcorreuATUALIZADO.Value, 2, siNegativo);
  end else begin
    QrOcorreuATUALIZADO.Value := QrOcorreuSALDO.Value;
    QrOcorreuATZ_TEXTO.Value := 'n�o calculado';
  end;
  //
  QrOcorreuSEQ.Value := QrOcorreu.RecNo;
  //
end;

procedure TFmGerDup1Main.QrOcorreuAfterScroll(DataSet: TDataSet);
begin
  ReopenOcorP();
end;

procedure TFmGerDup1Main.QrOcorreuBeforeClose(DataSet: TDataSet);
begin
  QrOcorP.Close;
end;

procedure TFmGerDup1Main.BtQuitacaoClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //
  MyObjects.MostraPopUpDeBotao(PMQuitacao, BtQuitacao);
end;

procedure TFmGerDup1Main.Quitadocumento1Click(Sender: TObject);
var
  Controle, Localiz, Ocorreu, FatParcela(*, LOIS, LotePg, Codigo*): Integer;
  Data: String;
  ValQuit, (*Juros, Desco,*) Pago: Double;
  //
  (*FatParcela, Controle,*) FatID_Sub, Genero, Cliente, FatParcRef: Integer;
  FatNum, Valor, MoraVal: Double;
  Dta, DataDU: TDateTime;
  //
  Gen_0303, Gen_0304, Gen_0306, Gen_0308, LctCtrl, LctSub: Integer;
  ValPagar, ValTaxas, ValMulta, ValJuros, ValPrinc, ValDesco, ValPende: Double;
  Duplicata: String;
  Incluiu: Boolean;
begin
  Screen.Cursor := crHourGlass;
  try
    Incluiu := False;
    Ocorreu := QrOcorreuCodigo.Value;
    QrOcorreu.First;
    while not QrOcorreu.Eof do
    begin
      //Genero := QrOcorreuPlaGen.Value;
      if MyObjects.FIC(QrOcorreuPlaGen.Value = 0, nil,
      'A ocorr�ncia cadastro n� ' + Geral.FF0(QrOcorreuOcorrencia.Value) +
      ' n�o possui conta (do plano de contas) atrelada!') then Exit;
      //
      QrOcorreu.Next;
    end;
    //PesqCtrl := QrPesqFatParcela.Value;
    ValQuit := 0;
    if QrPesqSALDO_ATUALIZADO.Value > 0 then
    begin
      ValQuit := QrPesqSALDO_ATUALIZADO.Value;
{
      Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
            'ADup Pgs', 'ADup Pgs', 'Controle');
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('INSERT INTO adup pgs SET AlterWeb=1, ');
      Dmod.QrUpd.SQL.Add('Data=:P0, Juros=:P1, Desco=:P2, Pago=:P3, ');
      Dmod.QrUpd.SQL.Add('LotePg=:P4, ' + FLD_LOIS + '=:Pa, Controle=:Pb');
      Dmod.QrUpd.Params[00].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
      Dmod.QrUpd.Params[01].AsFloat   := QrPesqSALDO_ATUALIZADO.Value-QrPesqSALDO_DESATUALIZ.Value;
      Dmod.QrUpd.Params[02].AsFloat   := 0;
      Dmod.QrUpd.Params[03].AsFloat   := QrPesqSALDO_ATUALIZADO.Value;
      Dmod.QrUpd.Params[04].AsInteger := 0;
      //
      Dmod.QrUpd.Params[05].AsInteger := QrPesqFatParcela.Value;
      Dmod.QrUpd.Params[06].AsInteger := Controle;
      Dmod.QrUpd.ExecSQL;
      Desco          := 0;
      LotePg         := 0;
      //
      Controle := UMyMod.BuscaEmLivreY_Def('adup pgs', 'Controle', stIns, 0);
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'adup pgs', False, [
      FLD_LOIS, 'Data', 'Juros',
      'Desco', 'Pago', 'LotePg'], [
      'Controle'], [
      LOIS, Data, Juros,
      Desco, Pago, LotePg], [
      Controle], True);
}
      FatParcRef := QrPesqFatParcela.Value;
      DmLot.ReopenLOI(FatParcRef);
      DmLot.ReopenAdupIts(FatParcRef);
      FatNum := FLotePgOrigem; 
      FatParcela := 0;
      Controle := 0;
      FatID_Sub := 0; // as al�neas s�o em separado!
      Cliente := DmLot.QrLOICliente.Value;
      Ocorreu := DmLot.QrAdupItsControle.Value;
      MoraVal := QrPesqSALDO_ATUALIZADO.Value-QrPesqSALDO_DESATUALIZ.Value;
      Pago    := QrPesqSALDO_ATUALIZADO.Value;
      Dta     := Date;
      DataDU := DmLot.QrLOIVencimento.Value;
      //
      if not DModFin.ObtemGeneroDeFatID(VAR_FATID_0303, Gen_0303, tpCred, True) then
        Exit;
      if not DModFin.ObtemGeneroDeFatID(VAR_FATID_0304, Gen_0304, tpCred, True) then
        Exit;
      if not DModFin.ObtemGeneroDeFatID(VAR_FATID_0306, Gen_0306, tpCred, True) then
        Exit;
      if not DModFin.ObtemGeneroDeFatID(VAR_FATID_0308, Gen_0308, tpDeb, True) then
        Exit;
      //
      ValPagar := Pago;
      ValTaxas := 0; // N�o tem aqui!
      ValMulta := 0; // N�o tem aqui!
      ValDesco := 0; // N�o tem aqui!
      ValJuros := MoraVal;
      ValPrinc := ValPagar - ValTaxas - ValMulta - ValJuros;// - ValDesco;
      ValPende := 0; // Vai pagar tudo!
  {
      DmLot.SQL_DupPg(Dmod.QrUpd, stIns, FatParcela, Controle, FatID_Sub, Genero,
       Cliente, Ocorreu, FatParcRef, FatNum, Pago, MoraVal, Desco, DataDU, Dta);
  }
      //
      LctCtrl := QrPesqLctCtrl.Value;
      LctSub := QrPesqLctSub.Value;
      Duplicata := QrPesqDuplicata.Value;

      if DmLot.InsUpd_DupPg(Dmod.QrUpd, stIns, LctCtrl, LctSub, FatParcela, Controle,
      FatID_Sub, Gen_0303, Gen_0304, Gen_0306, Gen_0308, Cliente, Ocorreu,
      FatParcRef, FatNum, Pago(*, MoraVal, Desco*), DataDU, Dta, Duplicata,
      ValPrinc, ValTaxas, ValMulta, ValJuros, ValDesco, ValPende,
      QrPesqCartDep.Value) then
      begin
        //
        Incluiu := True;
        FDupPgs := FatParcela;
        DmLot.CalculaPagtoDuplicata(QrPesqFatParcela.Value, QrPesqCredito.Value);
        //
        ReabrirTabelas;
      end;
    end;
    QrOcorreu.First;
    while not QrOcorreu.Eof do
    begin
      if QrOcorreuATUALIZADO.Value > 0 then
      begin
        ValQuit := ValQuit + QrOcorreuATUALIZADO.Value;
        //Ocorreu        := QrOcorreuCodigo.Value;
        Data           := Geral.FDT(Date, 1);
        //Juros          := QrOcorreuATUALIZADO.Value-QrOcorreuSALDO.Value;
        //Pago           := QrOcorreuATUALIZADO.Value;
        //LotePg         := 0;
        //
{
        Codigo := UMyMod.BuscaEmLivreY_Def('ocor rpg', 'Codigo', stIns, 0);
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'ocor rpg', False, [
        'Ocorreu', 'Data', 'Juros',
        'Pago', 'LotePg'], [
        'Codigo'], [
        Ocorreu, Data, Juros,
        Pago, LotePg], [
        Codigo], True);
        //
}
        FatID_Sub := QrOcorreuOcorrencia.Value;
        // genero verificado se = 0 em loop acima!
        Genero := QrOcorreuPlaGen.Value;
        Cliente := QrOcorreuCliente.Value;
        Ocorreu := QrOcorreuCodigo.Value;
        FatParcRef := QrOcorreuLOIS.Value;
        //
        FatNum := 0; // n�o tem! � na duplicata!
        FatParcela := 0;
        Controle := 0;
        Valor := QrOcorreuATUALIZADO.Value;
        MoraVal := QrOcorreuATUALIZADO.Value-QrOcorreuSALDO.Value;
        Dta := Date;
        //
        if DmLot.SQL_OcorP(Dmod.QrUpd, stIns, FatParcela, Controle,
        FatID_Sub, Genero, Cliente, Ocorreu, FatParcRef,
        FatNum, Valor, MoraVal, Dta, 0) then
        begin
          CalculaPagtoOcorP(Date, QrOcorreuCodigo.Value);
        end;
      end;
      QrOcorreu.Next;
    end;
{
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE lot esits SET AlterWeb=1,   ValQuit=:P0');
    Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
    Dmod.QrUpd.Params[0].AsFloat   := ValQuit;
    Dmod.QrUpd.Params[1].AsInteger := QrPesqFatParcela.Value;
    Dmod.QrUpd.ExecSQL;
}
    FatParcela := QrPesqFatParcela.Value;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_TabLctA, False, [
    'ValQuit'], ['FatID', 'FatParcela'], [
    ValQuit], [VAR_FATID_0301, FatParcela], True);
    //
    Localiz := QrPesqFatParcela.Value;
    QrPesq.Close;
    UMyMod.AbreQuery(QrPesq, Dmod.MyDB);
    QrPesq.Locate('FatParcela', Localiz, []);
    ReopenOcorreu(Ocorreu);
    if Incluiu then
      if Geral.MensagemBox(
      'Deseja imprimir um recibo com a soma dos recebimentos efetuados?',
      'Emiss�o de Recibo', MB_YESNO+MB_ICONQUESTION) = ID_YES then
        Imprimerecibodequitao1Click(Self);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmGerDup1Main.ImprimecartadeCancelamentodeProtesto1Click(
  Sender: TObject);
begin
  Application.CreateForm(TFmGerDup1CCPr, FmGerDup1CCPr);
  FmGerDup1CCPr.ReopenLotIProt(QrPesqFatParcela.Value);
  FmGerDup1CCPr.EdComarca.Text := FmGerDup1CCPr.QrLotIProtComarca.Value;
  FmGerDup1CCPr.EdNumDistrib.Text := FmGerDup1CCPr.QrLotIProtNumDistrib.Value;
  FmGerDup1CCPr.EdLivro.Text := FmGerDup1CCPr.QrLotIProtLivro.Value;
  FmGerDup1CCPr.EdFolhas.Text := FmGerDup1CCPr.QrLotIProtFolhas.Value;
  FmGerDup1CCPr.ShowModal;
  FmGerDup1CCPr.Destroy;
end;

procedure TFmGerDup1Main.ImprimecartadeCancelamentodeProtesto2Click(
  Sender: TObject);
begin
  ImprimecartadeCancelamentodeProtesto1Click(Self);
end;

procedure TFmGerDup1Main.Imprimerecibodequitao1Click(Sender: TObject);
var
  Pagamento, Texto, Liga: String;
  Valor: Double;
begin
  Pagamento := 'Quita��o de d�bitos gerados at� esta data relativos a';
  Texto := Pagamento + ' duplicata n� ' + QrPesqDuplicata.Value+
    ' emitida por '+ QrPesqEmitente.Value + ' para o dia '+ FormatDateTime(
    'dd" de "mmmm" de "yyyy', QrPesqVencimento.Value);
  if QrOcorreu.RecordCount > 0 then
  begin
    if QrOcorreu.RecordCount = 1 then Texto := Texto + ' e a ocorr�ncia'
    else Texto := Texto + ' e as ocorr�ncias';
    QrOcorreu.First;
    Liga := ' ';
    while not QrOcorreu.Eof do
    begin
      Texto := Texto + Liga + '"'+QrOcorreuNOMEOCORRENCIA.Value + '"';
      if QrOcorreu.RecNo+1= QrOcorreu.RecordCount then Liga := ' e '
      else liga := ', ';
      QrOcorreu.Next;
    end;
  end;
  //
  if QrPesqValQuit.Value > QrPesqTotalPg.Value then
    Valor := QrPesqValQuit.Value
  else
    Valor := QrPesqTotalPg.Value;
  //
  GOTOy.EmiteRecibo(QrPesqCliente.Value, Dmod.QrMasterDono.Value, CO_BENEFICIARIO_0,
    Valor, 0, 0, 'DUX-'+FormatFloat('000000',
    QrPesqFatParcela.Value), Texto, '', '', Date, 0);
end;

procedure TFmGerDup1Main.Imprimirdiriodeitemselecionado1Click(Sender: TObject);
begin
  ImprimeDiarioAdd(QrPesqControle.Value);
end;

procedure TFmGerDup1Main.Imprimirdiriodeitenspesquisados1Click(Sender: TObject);
begin
  ImprimeDiarioAdd(0);
end;

procedure TFmGerDup1Main.RGOrdem1Click(Sender: TObject);
begin
  FecharTabelas;
end;

procedure TFmGerDup1Main.RGOrdem2Click(Sender: TObject);
begin
  FecharTabelas;
end;

procedure TFmGerDup1Main.RGOrdem3Click(Sender: TObject);
begin
  FecharTabelas;
end;

procedure TFmGerDup1Main.RGOrdem4Click(Sender: TObject);
begin
  FecharTabelas;
end;

procedure TFmGerDup1Main.SbBorderoClick(Sender: TObject);
begin
  FmPrincipal.CriaFormLot(2, 0, QrPesqCodigo.Value, QrPesqFatParcela.Value);
end;

procedure TFmGerDup1Main.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmGerDup1Main.SbNomeClick(Sender: TObject);
begin
//
end;

procedure TFmGerDup1Main.SbNumeroClick(Sender: TObject);
begin
//
end;

procedure TFmGerDup1Main.SbQueryClick(Sender: TObject);
begin
//
end;

procedure TFmGerDup1Main.BtImprimeClick(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  ReabrirTabelas;
  QrPesq2.Close;
  QrPesq2.SQL := QrPesq.SQL;
  UMyMod.AbreQuery(QrPesq2, Dmod.MyDB);
  if QrPesq.State = dsBrowse  then
  begin
    if CkOcorrencias.Checked = True then
      MyObjects.frxMostra(frxPesq2, 'Relat�rio de duplicatas com ocorr�ncias')
    else
      MyObjects.frxMostra(frxPesq1, 'Relat�rio de duplicatas');
  end;
end;

procedure TFmGerDup1Main.QrPesq2CalcFields(DataSet: TDataSet);
var
  Taxas: MyArrayR07;
  Ano, Mes: Integer;
  Ocorrencias, OcorAtualiz, TotalAtualiz: Double;
begin
  QrPesq2CONTA.Value := 1;
  QrPesq2DDeposito_TXT.Value :=
    FormatDateTime(VAR_FORMATDATE3, QrPesq2DDeposito.Value);
  QrPesq2Valor_TXT.Value :=
    FormatFloat('#,###,##0.00', QrPesq2Credito.Value);
  //
  QrPesq2SALDO_DESATUALIZ.Value := QrPesq2Credito.Value + QrPesq2TotalJr.Value -
  QrPesq2TotalDs.Value - QrPesq2TotalPg.Value;
  //
  QrPesq2NOMESTATUS.Value := MLAGeral.NomeStatusPgto2(QrPesq2Quitado.Value,
    QrPesq2DDeposito.Value, Date, QrPesq2Data3.Value, QrPesq2Repassado.Value);
  //
  QrPesq2SALDO_ATUALIZADO.Value := DMod.ObtemValorAtualizado(
    QrPesq2Cliente.Value, QrPesq2Quitado.Value, QrPesq2Vencimento.Value, Date,
    QrPesq2Data3.Value, QrPesq2Credito.Value, QrPesq2TotalJr.Value, QrPesq2TotalDs.Value,
    QrPesq2TotalPg.Value, 0, True);
  //////////////////////////////////////////////////////////////////////////////
  QrPesq2CNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrPesq2CNPJCPF.Value);
  //
  Taxas := DMod.ObtemTaxaDeCompraRealizada(QrPesq2Controle.Value);
  QrPesq2TAXA_COMPRA.Value := QrPesq2TxaCompra.Value + Taxas[0];
  //
  //
  Ano := Trunc(QrPesq2PERIODO.Value) div 100;
  Mes := Trunc(QrPesq2PERIODO.Value) mod 100;
  QrPesq2MES_TXT.Value := FormatDateTime(VAR_FORMATDATE7,
    EncodeDate(Ano, Mes, 1));
    //
  Ano := Trunc(QrPesq2PERIODO3.Value) div 100;
  Mes := Trunc(QrPesq2PERIODO3.Value) mod 100;
  if (Ano = 0) and (Mes = 0) then QrPesq2MEQ_TXT.Value := 'N�O QUITADO' else
  QrPesq2MEQ_TXT.Value := FormatDateTime(VAR_FORMATDATE7,
    EncodeDate(Ano, Mes, 1));
////////////////////////////////////////////////////////////////////////////////
  if CkOcorrencias.Checked = True then
  begin
    Ocorrencias  := 0;
    OcorAtualiz  := 0;
    TotalAtualiz := 0;
    {
    QrOcu1.Close;
    QrOcu1.Params[00].AsInteger := QrPesq2Controle.Value;
    UMyMod.AbreQuery(QrOcu1);
    }
    UnDmkDAC_PF.AbreMySQLQuery0(QrOcu1, Dmod.MyDB, [
    'SELECT ob.Nome NOMEOCORRENCIA, oc.* ',
    'FROM ocorreu oc ',
    'LEFT JOIN ocorbank ob ON ob.Codigo = oc.Ocorrencia ',
    'WHERE oc.' + FLD_LOIS + '=' + Geral.FF0(QrPesqFatParcela.Value),
    '']);
    //
    while not QrOcu1.Eof do
    begin
      Ocorrencias  := Ocorrencias  + QrOcu1SALDO.Value;
      OcorAtualiz  := OcorAtualiz  + QrOcu1ATUALIZADO.Value;
      TotalAtualiz := TotalAtualiz + QrOcu1ATUALIZADO.Value;
      QrOcu1.Next;
    end;
    QrPesq2OCORRENCIAS.Value  := Ocorrencias;
    QrPesq2OCORATUALIZ.Value  := OcorAtualiz;
    QrPesq2TOTALATUALIZ.Value := TotalAtualiz + QrPesq2SALDO_ATUALIZADO.Value;
  end else begin
    QrPesq2OCORRENCIAS.Value  := 0;
    QrPesq2OCORATUALIZ.Value  := 0;
    QrPesq2TOTALATUALIZ.Value := 0;
  end;
  //
  if (QrPesq2Quitado.Value = -1) or (QrPesq2Quitado.Value = 0) then
    QrPesq2DATA3TXT.Value := ''
  else
    QrPesq2DATA3TXT.Value := Geral.FDT(QrPesq2Data3.Value, 3);
  //
  QrPesq2TEL1_TXT.Value := Geral.FormataTelefone_TT_Curto(QrPesq2Tel1.Value);
end;

procedure TFmGerDup1Main.GradeItensTitleClick(Column: TColumn);
var
  Antigo: String;
begin
  Antigo := FOrdC1;
  FOrdC1 := Column.FieldName;
  FOrdC2 := '';
  FOrdS2 := '';
  FOrdC3 := '';
  FOrdS3 := '';
  if FOrdC1 = 'SALDO_ATUALIZADO' then
  begin
    Geral.MensagemBox(
    'N�o � poss�vel ordenar pelo saldo atualizado pois a atualiza��o � feita ap�s a ordena��o!',
    'Aviso', MB_OK+MB_ICONWARNING);
    FOrdC1 := 'SALDO_DESATUALIZ';
  end else if FOrdC1 = 'NOMESTATUS' then
  begin
    FOrdC1 := 'Quitado';
    FOrdC2 := 'Vencimento';
    FOrdC3 := 'Repassado';
  end;
  if Antigo = FOrdC1 then
  FOrdS1 := MLAGeral.InverteOrdemAsc(FOrdS1);
  ReabrirTabelas;
end;

procedure TFmGerDup1Main.BtRefreshClick(Sender: TObject);
begin
  QrPesq.First;
  while not QrPesq.Eof do
  begin
    DmLot.CalculaPagtoDuplicata(QrPesqFatParcela.Value, QrPesqCredito.Value);
    QrPesq.Next;
  end;
  ReabrirTabelas;
end;

procedure TFmGerDup1Main.QrOcu1CalcFields(DataSet: TDataSet);
begin
  QrOcu1SALDO.Value := QrOcu1Valor.Value + QrOcu1TaxaV.Value - QrOcu1Pago.Value;
  //
  QrOcu1ATUALIZADO.Value := DMod.ObtemValorAtualizado(
    QrOcu1Cliente.Value, 1, QrOcu1DataO.Value, Date, QrOcu1Data3.Value,
    QrOcu1Valor.Value, QrOcu1TaxaV.Value, 0 (*Desco*),
    QrOcu1Pago.Value, (*QrOcu1TaxaP.Value*)0, (*False*)True);
end;

procedure TFmGerDup1Main.QrOcu2CalcFields(DataSet: TDataSet);
begin
  QrOcu2SALDO.Value := QrOcu2Valor.Value + QrOcu2TaxaV.Value - QrOcu2Pago.Value;
  //
  QrOcu2ATUALIZADO.Value := DMod.ObtemValorAtualizado(
    QrOcu2Cliente.Value, 1, QrOcu2DataO.Value, Date, QrOcu2Data3.Value,
    QrOcu2Valor.Value, QrOcu2TaxaV.Value, 0 (*Desco*),
    QrOcu2Pago.Value, (*QrOcu2TaxaP.Value*)0, (*False*)True);
end;

procedure TFmGerDup1Main.QrPesqBeforeClose(DataSet: TDataSet);
begin
  QrOcorreu.Close;
  QrADupIts.Close;
  QrLotPrr.Close;
  BtRefresh.Enabled := False;
end;

procedure TFmGerDup1Main.PageControl1Change(Sender: TObject);
begin
  BtDuplicataOcorr.Visible := False;
  BtCuidado.Visible        := False;
  BtPagtoDuvida.Visible    := False;
  BtZAZ.Visible            := False;
  BtHistCNAB.Visible       := False;
  case PageControl1.ActivePageIndex of
    0:
    begin
      BtCuidado.Visible := True;
      BtPagtoDuvida.Visible := True;
      BtZAZ.Visible := True;
    end;
    1:
    begin
      BtDuplicataOcorr.Visible := True;
    end;
    3:
    begin
      BtHistCNAB.Visible := true;
    end;
  end;
end;

procedure TFmGerDup1Main.InfoTempo(Tempo: TDateTime; Texto: String; Inicial: Boolean);
begin
  if Inicial then
  begin
    FTempo := Tempo;
    FUltim := Tempo;
    Memo1.Lines.Add('');
    Memo1.Lines.Add('==============================================================================');
    Memo1.Lines.Add('');
    Memo1.Lines.Add(FormatDateTime('dd/mm/yyyy  hh:nn:ss ', Tempo) +
      '- [ Total ] [Unit�ri] '+ Texto);
  end else begin
    Memo1.Lines.Add(FormatDateTime('dd/mm/yyyy  hh:nn:ss - ', Tempo)+
    FormatDateTime('nn:ss:zzz ', Tempo-FTempo) +
    FormatDateTime('nn:ss:zzz ', Tempo-FUltim) + Texto);
    FUltim := Tempo;
  end;
end;

procedure TFmGerDup1Main.Localizarremessa1Click(Sender: TObject);
var
  FatParcela, CNAB_Lot: Integer;
begin
  FatParcela := QrPesqFatParcela.Value;
  CNAB_Lot   := QrPesqCNAB_Lot.Value;
  //
  FmPrincipal.MostraRemessaCNABNovo(CNAB_Lot, FatParcela, 0);
  ReabrirTabelas;
end;

procedure TFmGerDup1Main.EdDuplicataChange(Sender: TObject);
begin
  FecharTabelas;
end;

procedure TFmGerDup1Main.EdEmitenteChange(Sender: TObject);
begin
  FecharTabelas;
end;

procedure TFmGerDup1Main.EditarstatusJurdicoSelecionados1Click(Sender: TObject);
var
  Status, i, Controle: Integer;
begin
  if (QrPesq.State <> dsInactive) and (QrPesq.RecordCount > 0) then
  begin
    Status := MyObjects.SelRadioGroup('Selecione o status jur�dico', '',
                ['Sem restri��es', 'Com a��o de cobran�a'], 1, 0);
    //
    if Status > -1 then
    begin
      Screen.Cursor := crHourGlass;
      try
        if GradeItens.SelectedRows.Count > 1 then
        begin
          with GradeItens.DataSource.DataSet do
          begin
            for i:= 0 to GradeItens.SelectedRows.Count -1 do
            begin
              //GotoBookmark(pointer(GradeItens.SelectedRows.Items[i]));
              GotoBookmark(GradeItens.SelectedRows.Items[i]);
              //
              Controle := QrPesqControle.Value;
              //
              Dmod.AtualizaStatusJuridico(Status, Controle);
            end;
          end;
        end else
        begin
          Controle := QrPesqControle.Value;
          //
          Dmod.AtualizaStatusJuridico(Status, Controle);
        end;
      finally
        Screen.Cursor := crDefault;
        //
        ReabrirTabelas;
      end;
    end;
  end;
end;

procedure TFmGerDup1Main.FecharTabelas;
begin
  QrPesq.Close;
  QrSoma.Close;
  QrVcto.Close;
end;

function TFmGerDup1Main.TextoSQLPesq(TabLote, TabLoteIts, Local: String): Integer;
var
  Remessa, Juridico, CNAB_Lot, Coligado: Integer;
begin
  Result   := 0;
  Juridico := RGJuridico.ItemIndex;
  Remessa  := RGRemessa.ItemIndex;
  CNAB_Lot := EdCNABLot.ValueVariant;
  //
  QrPesq.SQL.Add('SELECT MONTH(li.DDeposito)+YEAR(li.DDeposito)*100+0.00 PERIODO,');
  QrPesq.SQL.Add('MONTH(li.Data3)+YEAR(li.Data3)*100+0.00 PERIODO3, ');
  QrPesq.SQL.Add('lo.NF, od.Nome STATUS, li.FatParcela, li.Duplicata, li.Data, ');
  QrPesq.SQL.Add('li.DCompra, li.Credito, li.DDeposito, li.Emitente, li.CNPJCPF,');
  QrPesq.SQL.Add('lo.Cliente, li.Quitado, li.TotalJr, li.TotalDs, li.TotalPg,');
  QrPesq.SQL.Add('li.Repassado, IF(cl.Tipo=0, cl.RazaoSocial, ');
  QrPesq.SQL.Add('cl.Nome) NOMECLIENTE, li.Vencimento, li.Data3, li.Juridico, li.CNAB_Lot, ');
  QrPesq.SQL.Add('li.TxaCompra, li.Devolucao, li.ProrrVz, li.ProrrDd, li.ValQuit, ');
  QrPesq.SQL.Add('li.Banco, li.Agencia, lo.Tipo + ' + Local + ' LOCALT, lo.Codigo, ');
  QrPesq.SQL.Add('(li.Credito + li.TotalJr - li.TotalDs - li.TotalPg) SALDO_DESATUALIZ, ');
  QrPesq.SQL.Add('li.Controle LctCtrl, li.Sub LctSub, li.CartDep, li.Controle, sa.Tel1');
  //
  QrPesq.SQL.Add('FROM ' + TabLoteIts + ' li');
  QrPesq.SQL.Add('LEFT JOIN ' + TabLote + ' lo ON lo.Codigo=li.FatNum');
  QrPesq.SQL.Add('LEFT JOIN entidades cl ON cl.Codigo=lo.Cliente');
  QrPesq.SQL.Add('LEFT JOIN ocordupl  od ON od.Codigo=li.Devolucao');
  QrPesq.SQL.Add('LEFT JOIN repasits  ri ON ri.Origem=li.FatParcela');
  QrPesq.SQL.Add('LEFT JOIN repas     re ON re.Codigo=ri.Codigo');
  QrPesq.SQL.Add('LEFT JOIN sacados sa ON sa.CNPJ=li.CNPJCPF');
  QrPesq.SQL.Add('WHERE li.FatID=' +FormatFloat('0', VAR_FATID_0301));
  QrPesq.SQL.Add('AND lo.Tipo=1');
  //////////////////////////////////////////////////////////////////////////////
  QrSoma.SQL.Add('SELECT SUM(li.Credito) Valor, ');
    //CASE WHEN li.Credito-li.TotalPg > 0 THEN ' +
    //'SUM(li.Credito-li.TotalPg) ELSE 0 END Principal, ');
  QrSoma.SQL.Add('SUM(IF(li.Credito-li.TotalPg > 0, li.Credito-li.TotalPg, 0)) Principal,');
  QrSoma.SQL.Add('SUM(li.Credito + li.TotalJr - li.TotalDs - li.TotalPg) SALDO_DESATUALIZADO');
  QrSoma.SQL.Add('FROM ' + TabLoteIts + ' li');
  QrSoma.SQL.Add('LEFT JOIN ' + TabLote + ' lo ON lo.Codigo=li.FatNum');
  QrSoma.SQL.Add('LEFT JOIN entidades cl ON cl.Codigo=lo.Cliente');
  QrSoma.SQL.Add('LEFT JOIN ocordupl  od ON od.Codigo=li.Devolucao');
  QrSoma.SQL.Add('LEFT JOIN repasits  ri ON ri.Origem=li.FatParcela');
  QrSoma.SQL.Add('LEFT JOIN repas     re ON re.Codigo=ri.Codigo');
  QrSoma.SQL.Add('WHERE li.FatID=' +FormatFloat('0', VAR_FATID_0301));
  QrSoma.SQL.Add('AND lo.Tipo=1');
  //////////////////////////////////////////////////////////////////////////////
  QrVcto.SQL.Add('SELECT SUM(li.Credito) SOMA_VALOR,');
  QrVcto.SQL.Add('SUM(li.Credito + li.TotalJr - li.TotalDs - li.TotalPg) SALDO_DESATUALIZADO,');
  QrVcto.SQL.Add('SUM(CURDATE()-li.Vencimento) SOMA_DIAS,');
  QrVcto.SQL.Add('SUM((CURDATE()-li.Vencimento)*li.Credito) FATOR_VD');
  QrVcto.SQL.Add('FROM ' + TabLoteIts + ' li');
  QrVcto.SQL.Add('LEFT JOIN ' + TabLote + ' lo ON lo.Codigo=li.FatNum');
  QrVcto.SQL.Add('LEFT JOIN entidades cl ON cl.Codigo=lo.Cliente');
  QrVcto.SQL.Add('LEFT JOIN ocordupl  od ON od.Codigo=li.Devolucao');
  QrVcto.SQL.Add('LEFT JOIN repasits  ri ON ri.Origem=li.FatParcela');
  QrVcto.SQL.Add('LEFT JOIN repas     re ON re.Codigo=ri.Codigo');
  QrVcto.SQL.Add('WHERE li.FatID=' +FormatFloat('0', VAR_FATID_0301));
  QrVcto.SQL.Add('AND lo.Tipo=1');
  QrVcto.SQL.Add('AND li.Vencimento<CURDATE()');
  //////////////////////////////////////////////////////////////////////////////
  if FCliente <> 0 then
  begin
    QrPesq.SQL.Add('AND lo.Cliente=' + FormatFloat('0', FCliente));
    QrSoma.SQL.Add('AND lo.Cliente=' + FormatFloat('0', FCliente));
    QrVcto.SQL.Add('AND lo.Cliente=' + FormatFloat('0', FCliente));
  end;
  if FEmitente <> '' then
  begin
    QrPesq.SQL.Add('AND li.Emitente LIKE "' + FEmitente + '"');
    QrSoma.SQL.Add('AND li.Emitente LIKE "' + FEmitente + '"');
    QrVcto.SQL.Add('AND li.Emitente LIKE "' + FEmitente + '"');
  end;
  if FCPF <> '' then
  begin
    QrPesq.SQL.Add('AND li.CNPJCPF = "' + FCPF + '"');
    QrSoma.SQL.Add('AND li.CNPJCPF = "' + FCPF + '"');
    QrVcto.SQL.Add('AND li.CNPJCPF = "' + FCPF + '"');
  end;
  if FDuplicata <> '' then
  begin
    QrPesq.SQL.Add('AND li.Duplicata = "' + FDuplicata + '"');
    QrSoma.SQL.Add('AND li.Duplicata = "' + FDuplicata + '"');
    QrVcto.SQL.Add('AND li.Duplicata = "' + FDuplicata + '"');
  end;
  if FControle <> 0 then
  begin
    QrPesq.SQL.Add('AND li.FatParcela = ' + FormatFloat('0', FControle));
    QrSoma.SQL.Add('AND li.FatParcela = ' + FormatFloat('0', FControle));
    QrVcto.SQL.Add('AND li.FatParcela = ' + FormatFloat('0', FControle));
  end;
  case Juridico of
    0: QrPesq.SQL.Add('AND li.Juridico = 0');
    1: QrPesq.SQL.Add('AND li.Juridico = 1');
  end;
  if CNAB_Lot <> 0 then
    QrPesq.SQL.Add('AND li.CNAB_Lot=' + Geral.FF0(CNAB_Lot));
  case Remessa of
    0: QrPesq.SQL.Add('AND li.CNAB_Lot <> 0');
    1: QrPesq.SQL.Add('AND li.CNAB_Lot = 0');
  end;
  //
  //////////////////////////////////////////////////////////////////////////////
  QrPesq.SQL.Add(dmkPF.SQL_Periodo('AND li.Data ',
    TPEIni.Date, TPEFim.Date, CkEIni.Checked, CkEFim.Checked));
  QrPesq.SQL.Add(dmkPF.SQL_Periodo('AND li.DDeposito ',
    TPVIni.Date, TPVFim.Date, CkVIni.Checked, CkVFim.Checked));
  QrPesq.SQL.Add(dmkPF.SQL_Periodo('AND li.Data3 ',
    TPQIni.Date, TPQFim.Date, CkQIni.Checked, CkQFim.Checked));
  Coligado := EdColigado.ValueVariant;
  CondicoesExtras(QrPesq, Coligado);
  //////////////////////////////////////////////////////////////////////////////
  QrSoma.SQL.Add(dmkPF.SQL_Periodo('AND li.Data ',
    TPEIni.Date, TPEFim.Date, CkEIni.Checked, CkEFim.Checked));
  QrSoma.SQL.Add(dmkPF.SQL_Periodo('AND li.DDeposito ',
    TPVIni.Date, TPVFim.Date, CkVIni.Checked, CkVFim.Checked));
  QrSoma.SQL.Add(dmkPF.SQL_Periodo('AND li.Data3 ',
    TPQIni.Date, TPQFim.Date, CkQIni.Checked, CkQFim.Checked));
  CondicoesExtras(QrSoma, Coligado);
  //////////////////////////////////////////////////////////////////////////////
  QrVcto.SQL.Add(dmkPF.SQL_Periodo('AND li.Data ',
    TPEIni.Date, TPEFim.Date, CkEIni.Checked, CkEFim.Checked));
  QrVcto.SQL.Add(dmkPF.SQL_Periodo('AND li.DDeposito ',
    TPVIni.Date, TPVFim.Date, CkVIni.Checked, CkVFim.Checked));
  QrVcto.SQL.Add(dmkPF.SQL_Periodo('AND li.Data3 ',
    TPQIni.Date, TPQFim.Date, CkQIni.Checked, CkQFim.Checked));
  CondicoesExtras(QrVcto, Coligado);
end;

procedure TFmGerDup1Main.QrCNAB240CalcFields(DataSet: TDataSet);
var
  Envio: TEnvioCNAB;
begin
  case QrCNAB240Envio.Value of
    1: Envio := ecnabRemessa;
    2: Envio := ecnabRetorno;
    else Envio := ecnabIndefinido;
  end;
  QrCNAB240NOMEENVIO.Value := UBancos.CNAB240Envio(QrCNAB240Envio.Value);
  QrCNAB240NOMEMOVIMENTO.Value := UBancos.CNABTipoDeMovimento(1,
    Envio, QrCNAB240Movimento.Value, 0, False, '');
end;

procedure TFmGerDup1Main.QrClientesCalcFields(DataSet: TDataSet);
begin
  QrClientesTe1_TXT.Value := MLAGeral.FormataTelefone_TT_Curto(QrClientesTe1.Value);
end;

procedure TFmGerDup1Main.Gerenciardirio1Click(Sender: TObject);
var
  Texto: String;
begin
  Texto := QrPesqCNPJCPF.Value + ' - ' + QrPesqEmitente.Value;
  //
  FmPrincipal.MostraDiarioGer(0, QrPesqCliente.Value, Geral.FF0(QrPesqControle.Value), Texto);
end;

procedure TFmGerDup1Main.GradeItensDblClick(Sender: TObject);
var
  i: Integer;
begin
  if QrPesq.State = dsInactive then Exit;
  if QrPesq.RecordCount = 0 then Exit;
  QrSacados.Close;
  QrSacados.Params[0].AsString := Geral.SoNumero_TT(QrPesqCNPJCPF.Value);
  UMyMod.AbreQuery(QrSacados, Dmod.MyDB);
  //
  Application.CreateForm(TFmSacadosEdit, FmSacadosEdit);
  with FmSacadosEdit do
  begin
    ImgTipo.SQLType := stUpd;
    FOrigem         := 'FmDuplicatas2';
    EdCNPJ.Text     := Geral.FormataCNPJ_TT(QrSacadosCNPJ.Value);
    EdSacado.Text   := QrSacadosNome.Value;
    EdRua.Text      := QrSacadosRua.Value;
    EdNumero.Text   := IntToStr(Trunc(QrSacadosNumero.Value));
    EdCompl.Text    := QrSacadosCompl.Value;
    EdBairro.Text   := QrSacadosBairro.Value;
    EdCidade.Text   := QrSacadosCidade.Value;
    EdCEP.Text      := Geral.FormataCEP_NT(QrSacadosCEP.Value);
    EdTel1.Text     := Geral.FormataTelefone_TT(QrSacadosTel1.Value);
    EdEmail.Text    := QrSacadosEmail.Value;
    EdRisco.Text    := Geral.FFT(QrSacadosRisco.Value, 2, siPositivo);
    //
    for i := 0 to CBUF.Items.Count-1 do
      if CBUF.Items[i] = QrSacadosUF.Value then CBUF.ItemIndex := i;
  end;
  FmSacadosEdit.ShowModal;
  FmSacadosEdit.Destroy;
end;

procedure TFmGerDup1Main.N0Statusautomtico1Click(Sender: TObject);
begin
  ForcaStatus(0);
end;

procedure TFmGerDup1Main.N1Forastatusprorrogado1Click(Sender: TObject);
begin
  ForcaStatus(-1);
end;

procedure TFmGerDup1Main.N2ForastatusBaixado1Click(Sender: TObject);
begin
  ForcaStatus(-2);
end;

procedure TFmGerDup1Main.N3ForastatusMoroso1Click(Sender: TObject);
begin
  ForcaStatus(-3);
end;

procedure TFmGerDup1Main.ForcaStatus(NovoStatus: Integer);
var
  FatParcela, Quitado: Integer;
begin
  if QrPesq.State = dsBrowse then
  begin
    if QrPesq.RecordCount > 0 then
    begin
      FatParcela := QrPesqFatParcela.Value;
{
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE lot esits SET Quitado=:P0');
      Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
      Dmod.QrUpd.Params[0].AsInteger := NovoStatus;
      Dmod.QrUpd.Params[1].AsInteger := Controle;
      Dmod.QrUpd.ExecSQL;
}
      //
      Quitado := NovoStatus;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_TabLctA, False, [
      'Quitado'], ['FatID', 'FatParcela'], [
      Quitado], [VAR_FATID_0301, FatParcela], True);
      //
      QrPesq.Close;
      UMyMod.AbreQuery(QrPesq, Dmod.MyDB);
      QrPesq.Locate('FatParcela', FatParcela, []);
    end;
  end;
end;

procedure TFmGerDup1Main.BtHistCNABClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 3;
  //
  MyObjects.MostraPopUpDeBotao(PMHistCNAB, BtHistCNAB);
end;

procedure TFmGerDup1Main.Excluihistriciatual1Click(Sender: TObject);
begin
  UMyMod.SQLDel1(Dmod.QrUpd, QrCNAB240, 'cnab240', 'CNAB240I',
  QrCNAB240CNAB240I.Value, True, 'Confirma a exclus�o do item de hist�rico ' +
  ' selecionado?', False);
end;

procedure TFmGerDup1Main.Excluitodositensdehistricosdestaduplicata1Click(
  Sender: TObject);
begin
  UMyMod.SQLDel1(Dmod.QrUpd, QrCNAB240, 'cnab240', 'Controle',
  QrCNAB240Controle.Value, True, 'Confirma a exclus�o de todo hist�rico ' +
  ' da duplicata selecionada?', False);
end;

procedure TFmGerDup1Main.Extratodeduplicata1Click(Sender: TObject);
begin
  MyObjects.frxMostra(frxExtratoDuplicata, 'Extrato de Duplicata');
end;

procedure TFmGerDup1Main.QrCNAB240AfterOpen(DataSet: TDataSet);
begin
  BtHistCNAB.Enabled := Geral.IntToBool_0(QrCNAB240.RecordCount);
end;

procedure TFmGerDup1Main.frxExtratoDuplicataGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_CNPJ_CPF' then
  begin
    if Length(QrPesqCNPJCPF.Value) = 11 then
      Value := 'CPF:'
    else
      Value := 'CNPJ:';
  end
end;

procedure TFmGerDup1Main.frxPesq1GetValue(const VarName: String;
  var Value: Variant);
begin
  if VarName = 'VARF_CLIENTE' then
  begin
    if CBCliente.KeyValue = NULL then Value := ' ' else
    Value := CBCliente.Text;
  end else if VarName = 'VARF_TODOS' then
  begin
    if CBCliente.KeyValue = NULL then Value := 'TODOS CLIENTES' else
    Value := ' ';
  end else if VarName = 'VARF_PERIODO' then Value :=
    dmkPF.PeriodoImp(TPEIni.Date, TPEFim.Date, TPVIni.Date, TPVFim.Date,
      CkEIni.Checked, CkEFim.Checked, CkVIni.Checked, CkVFim.Checked,
      'Emiss�o:', 'Vencimento:')
  else if VarName = 'VARF_QTD_CHEQUES' then Value := QrPesq2.RecordCount
  else if VarName = 'VFR_LA1NOME' then
  begin
    case RGOrdem1.ItemIndex of
      0: Value := 'Cliente: '+QrPesq2NOMECLIENTE.Value;
      1: Value := 'Dep�sito no dia  '+QrPesq2DDeposito_TXT.Value;
      2: Value := 'Dep�sito no m�s de '+QrPesq2MES_TXT.Value;
      3: Value := 'Emitido por: '+QrPesq2Emitente.Value;
      4: Value := 'CPF/CNPJ: '+QrPesq2CNPJ_TXT.Value;
      5: Value := 'Dia da quita��o: '+QrPesq2DATA3TXT.Value;
      6: Value := 'M�s da quita��o: '+QrPesq2MEQ_TXT.Value;
    end;
  end
  else if VarName = 'VFR_LA2NOME' then
  begin
    case RGOrdem2.ItemIndex of
      0: Value := 'Cliente: '+QrPesq2NOMECLIENTE.Value;
      1: Value := 'Dep�sito no dia  '+QrPesq2DDeposito_TXT.Value;
      2: Value := 'Dep�sito no m�s de '+QrPesq2MES_TXT.Value;
      3: Value := 'Emitido por: '+QrPesq2Emitente.Value;
      4: Value := 'CPF/CNPJ: '+QrPesq2CNPJ_TXT.Value;
      5: Value := 'Dia da quita��o: '+QrPesq2DATA3TXT.Value;
      6: Value := 'M�s da quita��o: '+QrPesq2MEQ_TXT.Value;
    end;
  end
  else if VarName = 'VFR_LA3NOME' then
  begin
    case RGOrdem3.ItemIndex of
      0: Value := 'Cliente: '+QrPesq2NOMECLIENTE.Value;
      1: Value := 'Dep�sito no dia  '+QrPesq2DDeposito_TXT.Value;
      2: Value := 'Dep�sito no m�s de '+QrPesq2MES_TXT.Value;
      3: Value := 'Emitido por: '+QrPesq2Emitente.Value;
      4: Value := 'CPF/CNPJ: '+QrPesq2CNPJ_TXT.Value;
      5: Value := 'Dia da quita��o: '+QrPesq2DATA3TXT.Value;
      6: Value := 'M�s da quita��o: '+QrPesq2MEQ_TXT.Value;
    end;
  end
  else if VarName = 'VARF_SITUACOES' then
  begin
    Value := ' ';
    //if CkQuitado.Checked then Value := Value + CkQuitado.Caption;
    //if CkRepassado.Checked then Value := Value + '(incluindo repassados)';
    //if Trim(Value) = '' then Value := '(??????)';
  end
  else if VarName = 'VARF_FILTROS' then
  begin
    Value := '';
    if EdColigado.ValueVariant <> 0 then Value := Value +
      'Coligado: '+CBColigado.Text+ '  ';
    if FEmitente <> '' then Value := Value + '  {Emitente: '+ FEmitente+'}';
    if FCPF <> '' then Value := Value + '  {CPF/CNPJ: '+
    Geral.FormataCNPJ_TT(FCPF)+'}';
    if Value <> '' then Value := 'FILTROS : '+Value;
  end

  // User function

  else if VarName = 'VFR_ORD1' then
  begin
    if RGAgrupa.ItemIndex < 1 then Value := 0 else
    Value := RGOrdem1.ItemIndex + 1;
  end else
  if VarName = 'VFR_ORD2' then
  begin
    if RGAgrupa.ItemIndex < 2 then Value := 0 else
    Value := RGOrdem2.ItemIndex + 1;
  end else
  if VarName = 'VFR_ORD3' then
  begin
    if RGAgrupa.ItemIndex < 3 then Value := 0 else
    Value := RGOrdem3.ItemIndex + 1;
  end

end;

end.

