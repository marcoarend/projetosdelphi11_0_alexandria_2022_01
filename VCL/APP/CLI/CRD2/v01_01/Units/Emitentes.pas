unit Emitentes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Db, mySQLDbTables, Grids, DBGrids,
  dmkGeral, dmkImage, UnDmkEnums;

type
  TFmEmitentes = class(TForm)
    PainelDados: TPanel;
    DsEmitPF: TDataSource;
    GrEmitentes: TDBGrid;
    Panel1: TPanel;
    RGOrdem1: TRadioGroup;
    RGOrdem2: TRadioGroup;
    QrEmitCPF: TmySQLQuery;
    QrEmitCPFCPF_TXT: TWideStringField;
    StringGrid1: TStringGrid;
    QrEmitCPFCPF: TWideStringField;
    QrEmitCPFNome: TWideStringField;
    QrEmitCPFLimite: TFloatField;
    QrEmitCPFLastAtz: TDateField;
    QrEmitCPFAcumCHComV: TFloatField;
    QrEmitCPFAcumCHComQ: TIntegerField;
    QrEmitCPFAcumCHDevV: TFloatField;
    QrEmitCPFAcumCHDevQ: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure RGOrdem1Click(Sender: TObject);
    procedure RGOrdem2Click(Sender: TObject);
    procedure QrEmitCPFCalcFields(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ReopenEmitentes(BAC: String);
  end;

  var
  FmEmitentes: TFmEmitentes;

implementation

uses UnMyObjects, Module, EmitentesEdit, UnInternalConsts, Principal, 
  UMySQLModule;

{$R *.DFM}

procedure TFmEmitentes.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEmitentes.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //FmPrincipal.AplicaInfoRegEdit(FmEmitentes, True);
end;

procedure TFmEmitentes.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEmitentes.RGOrdem1Click(Sender: TObject);
begin
  ReopenEmitentes(''{QrEmitCPFBAC.Value});
end;

procedure TFmEmitentes.RGOrdem2Click(Sender: TObject);
begin
  ReopenEmitentes(''{QrEmitCPFBAC.Value});
end;

procedure TFmEmitentes.ReopenEmitentes(BAC: String);
var
  Ordem: String;
begin
  case RGOrdem1.ItemIndex of
    //0: Ordem := 'BAC';
    0: Ordem := 'Nome';
    1: Ordem := 'CPF';
    2: Ordem := 'Limite';
    //4: Ordem := 'Banco';
    //5: Ordem := 'Agencia';
    //6: Ordem := 'Conta';
    else Ordem := '';
  end;
  if Ordem <> '' then Ordem := 'ORDER BY '+Ordem;
  if Ordem <> '' then if RGOrdem2.ItemIndex = 1 then Ordem := Ordem + ' DESC';
  //
  QrEmitCPF.Close;
  QrEmitCPF.SQL.Clear;
  QrEmitCPF.SQL.Add('SELECT * FROM emitcpf');
  QrEmitCPF.SQL.Add(ORDEM);
  UMyMod.AbreQuery(QrEmitCPF, Dmod.MyDB);
  //
  //if BAC <> '' then QrEmitCPF.Locate('BAC', BAC, []);
end;

procedure TFmEmitentes.QrEmitCPFCalcFields(DataSet: TDataSet);
begin
  QrEmitCPFCPF_TXT.Value := Geral.FormataCNPJ_TT(QrEmitCPFCPF.Value);
end;

procedure TFmEmitentes.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  ReopenEmitentes('');
end;

procedure TFmEmitentes.BtExcluiClick(Sender: TObject);
begin
  (*if Application.MessageBox(PChar('Confirma a exclus�o do CP "'+
  QrEmitCPFBAC.Value+'"?'), 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION)=
  ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM emitentes WHERE BAC=:P0');
    Dmod.QrUpd.Params[0].AsString := QrEmitCPFBAC.Value;
    Dmod.QrUpd.ExecSQl;
    //
    QrEmitCPF.Next;
    ReopenEmitentes(''{QrEmitCPFBAC.Value});
  end;*)
end;

procedure TFmEmitentes.BtIncluiClick(Sender: TObject);
begin
  Application.CreateForm(TFmEmitentesEdit, FmEmitentesEdit);
  FmEmitentesEdit.ImgTipo.SQLType := stIns;
  FmEmitentesEdit.ShowModal;
  FmEmitentesEdit.Destroy;
end;

procedure TFmEmitentes.BtAlteraClick(Sender: TObject);
begin
  (*Application.CreateForm(TFmEmitentesEdit, FmEmitentesEdit);
  with FmEmitentesEdit do
  begin
    ImgTipo.SQLType := stUpd;
    EdBanco.Text     := MLAGeral.FFD(QrEmitCPFBanco.Value, 3, siPositivo);
    EdAgencia.Text   := MLAGeral.FFD(QrEmitCPFAgencia.Value, 4, siPositivo);
    EdConta.Text     := QrEmitCPFConta.Value;
    EdCPF.Text       := MLAGeral.FormataCNPJ_TFT(QrEmitCPFCPF.Value);
    EdEmitente.Text  := QrEmitCPFNome.Value;
    EdRisco.Text     := Geral.FFT(QrEmitCPFRisco.Value, 2, siPositivo);
  end;
  FmEmitentesEdit.ShowModal;
  FmEmitentesEdit.Destroy;*)
end;

procedure TFmEmitentes.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  FmPrincipal.SalvaInfoRegEdit(FmEmitentes, 1, nil);
end;

end.

