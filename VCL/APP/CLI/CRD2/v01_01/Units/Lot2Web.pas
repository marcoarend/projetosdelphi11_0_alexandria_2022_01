unit Lot2Web;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, Db, mySQLDbTables,
  dmkCheckGroup, dmkGeral, dmkImage, UnDmkEnums;

type
  TFmLot2Web = class(TForm)
    QrLLC: TmySQLQuery;
    QrLLCCodigo: TIntegerField;
    QrLLCCliente: TIntegerField;
    QrLLCLote: TIntegerField;
    QrLLCTipo: TSmallintField;
    QrLLCData: TDateField;
    QrLLCTotal: TFloatField;
    QrLLCDias: TFloatField;
    QrLLCItens: TIntegerField;
    QrLLCBaixado: TIntegerField;
    QrLLCCPF: TWideStringField;
    QrLLCNOMECLI: TWideStringField;
    QrLLCNOMETIPO: TWideStringField;
    QrLLI: TmySQLQuery;
    QrLLICodigo: TIntegerField;
    QrLLIControle: TIntegerField;
    QrLLIComp: TIntegerField;
    QrLLIPraca: TIntegerField;
    QrLLIBanco: TIntegerField;
    QrLLIAgencia: TIntegerField;
    QrLLIConta: TWideStringField;
    QrLLICheque: TIntegerField;
    QrLLICPF: TWideStringField;
    QrLLIEmitente: TWideStringField;
    QrLLIBruto: TFloatField;
    QrLLIDesco: TFloatField;
    QrLLIValor: TFloatField;
    QrLLIEmissao: TDateField;
    QrLLIDCompra: TDateField;
    QrLLIDDeposito: TDateField;
    QrLLIVencto: TDateField;
    QrLLIDias: TIntegerField;
    QrLLIDuplicata: TWideStringField;
    QrLLIIE: TWideStringField;
    QrLLIRua: TWideStringField;
    QrLLINumero: TLargeintField;
    QrLLICompl: TWideStringField;
    QrLLIBairro: TWideStringField;
    QrLLICidade: TWideStringField;
    QrLLIUF: TWideStringField;
    QrLLICEP: TIntegerField;
    QrLLITel1: TWideStringField;
    QrLLIUser_ID: TIntegerField;
    QrLLIPasso: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    DBGrid1: TDBGrid;
    Panel3: TPanel;
    CgStatus: TdmkCheckGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure DefineLoteWeb(LoteWeb: Integer; CloseForm: Boolean);
  public
    { Public declarations }
  end;

  var
  FmLot2Web: TFmLot2Web;

implementation

{$R *.DFM}

uses UnMyObjects, Module, Principal, Lot2Cab, ModuleLot2;

procedure TFmLot2Web.DefineLoteWeb(LoteWeb: Integer; CloseForm: Boolean);
begin
  case FmPrincipal.FFormLotShow of
    2: FmPrincipal.FLoteWeb := LoteWeb;
    else Application.MessageBox(PChar('Janela de gerenciamento de border�s ' +
    'n�o definida! (12)'), 'ERRO', MB_OK+MB_ICONERROR);
  end;
  if CloseForm then
    FmLot2Web.Close;
end;

procedure TFmLot2Web.BtSaidaClick(Sender: TObject);
begin
  DefineLoteWeb(0, True);
end;

procedure TFmLot2Web.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLot2Web.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
end;

procedure TFmLot2Web.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLot2Web.BtOKClick(Sender: TObject);
begin
  DefineLoteWeb(DmLot2.QrLLCCodigo.Value, True);
end;

procedure TFmLot2Web.DBGrid1DblClick(Sender: TObject);
begin
  DefineLoteWeb(DmLot2.QrLLCCodigo.Value, True);
end;

procedure TFmLot2Web.BitBtn1Click(Sender: TObject);
begin
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE LLoteCab SET Baixado=4 WHERE Codigo=:P0');
  Dmod.QrUpd.Params[0].AsInteger := DmLot2.QrLLCCodigo.Value;
  Dmod.QrUpd.ExecSQL;
  //
  DmLot2.ReopenLLoteCab(CgStatus.Value);
end;

end.
