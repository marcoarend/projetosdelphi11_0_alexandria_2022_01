unit Result1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DBCtrls, Db, mySQLDbTables, ComCtrls,
  Grids, DBGrids, frxClass, frxDBSet, Variants, dmkGeral, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, dmkImage, UnDmkProcFunc, DmkDAC_PF, UnDmkEnums;

type
  TFmResult1 = class(TForm)
    PainelDados: TPanel;
    Label3: TLabel;
    EdCliente: TdmkEditCB;
    CbCliente: TdmkDBLookupComboBox;
    QrClientes: TmySQLQuery;
    DsClientes: TDataSource;
    QrClientesNOMECLIENTE: TWideStringField;
    QrClientesCodigo: TIntegerField;
    Label34: TLabel;
    TPIni: TDateTimePicker;
    Label1: TLabel;
    TPFim: TDateTimePicker;
    QrOpera: TmySQLQuery;
    QrRepassados: TmySQLQuery;
    QrRepassadosJurosV: TFloatField;
    QrEmLot1: TmySQLQuery;
    QrEmLot1IRRF: TFloatField;
    QrEmLot1IRRF_Val: TFloatField;
    QrEmLot1ISS: TFloatField;
    QrEmLot1ISS_Val: TFloatField;
    QrEmLot1PIS_R: TFloatField;
    QrEmLot1PIS_R_Val: TFloatField;
    QrEmLot1TaxaVal: TFloatField;
    QrEmLot1ValValorem: TFloatField;
    QrEmLot1Codigo: TIntegerField;
    QrEmLot1AdValorem: TFloatField;
    QrEmLot2: TmySQLQuery;
    QrEmLot2IRRF: TFloatField;
    QrEmLot2IRRF_Val: TFloatField;
    QrEmLot2ISS: TFloatField;
    QrEmLot2ISS_Val: TFloatField;
    QrEmLot2PIS_R: TFloatField;
    QrEmLot2PIS_R_Val: TFloatField;
    QrEmLot2TaxaVal: TFloatField;
    QrEmLot2ValValorem: TFloatField;
    QrEmLot2Codigo: TIntegerField;
    QrEmLot2AdValorem: TFloatField;
    QrEmLot3: TmySQLQuery;
    QrEmLot3IRRF: TFloatField;
    QrEmLot3IRRF_Val: TFloatField;
    QrEmLot3ISS: TFloatField;
    QrEmLot3ISS_Val: TFloatField;
    QrEmLot3PIS_R: TFloatField;
    QrEmLot3PIS_R_Val: TFloatField;
    QrEmLot3TaxaVal: TFloatField;
    QrEmLot3ValValorem: TFloatField;
    QrEmLot3Codigo: TIntegerField;
    QrEmLot3AdValorem: TFloatField;
    QrEmLot4: TmySQLQuery;
    QrEmLot4IRRF: TFloatField;
    QrEmLot4IRRF_Val: TFloatField;
    QrEmLot4ISS: TFloatField;
    QrEmLot4ISS_Val: TFloatField;
    QrEmLot4PIS_R: TFloatField;
    QrEmLot4PIS_R_Val: TFloatField;
    QrEmLot4TaxaVal: TFloatField;
    QrEmLot4ValValorem: TFloatField;
    QrEmLot4Codigo: TIntegerField;
    QrEmLot4AdValorem: TFloatField;
    QrEmLot5: TmySQLQuery;
    QrEmLot5IRRF: TFloatField;
    QrEmLot5IRRF_Val: TFloatField;
    QrEmLot5ISS: TFloatField;
    QrEmLot5ISS_Val: TFloatField;
    QrEmLot5PIS_R: TFloatField;
    QrEmLot5PIS_R_Val: TFloatField;
    QrEmLot5TaxaVal: TFloatField;
    QrEmLot5ValValorem: TFloatField;
    QrEmLot5Codigo: TIntegerField;
    QrEmLot5AdValorem: TFloatField;
    QrEmLot6: TmySQLQuery;
    QrEmLot6IRRF: TFloatField;
    QrEmLot6IRRF_Val: TFloatField;
    QrEmLot6ISS: TFloatField;
    QrEmLot6ISS_Val: TFloatField;
    QrEmLot6PIS_R: TFloatField;
    QrEmLot6PIS_R_Val: TFloatField;
    QrEmLot6TaxaVal: TFloatField;
    QrEmLot6ValValorem: TFloatField;
    QrEmLot6Codigo: TIntegerField;
    QrEmLot6AdValorem: TFloatField;
    QrRepassadosValor: TFloatField;
    EdColigado: TdmkEditCB;
    Label2: TLabel;
    CBColigado: TdmkDBLookupComboBox;
    QrColigado: TmySQLQuery;
    DsColigado: TDataSource;
    QrNRep: TmySQLQuery;
    QrNRepValor: TFloatField;
    QrNRepJurosV: TFloatField;
    QrColigadoCodigo: TIntegerField;
    QrColigadoNOMECOLIGADO: TWideStringField;
    DBGrid1: TDBGrid;
    Panel1: TPanel;
    RGOrdem1: TRadioGroup;
    RGOrdem2: TRadioGroup;
    RGOrdem3: TRadioGroup;
    RGGrupos: TRadioGroup;
    RGSintetico: TRadioGroup;
    DsOpera: TDataSource;
    QrPesq: TmySQLQuery;
    QrPesqNOMECLIENTE: TWideStringField;
    QrPesqCodigo: TIntegerField;
    QrPesqTipo: TSmallintField;
    QrPesqSpread: TSmallintField;
    QrPesqCliente: TIntegerField;
    QrPesqLote: TSmallintField;
    QrPesqData: TDateField;
    QrPesqTotal: TFloatField;
    QrPesqDias: TFloatField;
    QrPesqPeCompra: TFloatField;
    QrPesqTxCompra: TFloatField;
    QrPesqValValorem: TFloatField;
    QrPesqAdValorem: TFloatField;
    QrPesqIOC: TFloatField;
    QrPesqCPMF: TFloatField;
    QrPesqTipoAdV: TIntegerField;
    QrPesqIRRF: TFloatField;
    QrPesqIRRF_Val: TFloatField;
    QrPesqISS: TFloatField;
    QrPesqISS_Val: TFloatField;
    QrPesqPIS_R: TFloatField;
    QrPesqPIS_R_Val: TFloatField;
    QrPesqLk: TIntegerField;
    QrPesqDataCad: TDateField;
    QrPesqDataAlt: TDateField;
    QrPesqUserCad: TIntegerField;
    QrPesqUserAlt: TIntegerField;
    QrPesqTarifas: TFloatField;
    QrPesqOcorP: TFloatField;
    QrPesqCOFINS_R: TFloatField;
    QrPesqCOFINS_R_Val: TFloatField;
    QrPesqPIS: TFloatField;
    QrPesqPIS_Val: TFloatField;
    QrPesqCOFINS: TFloatField;
    QrPesqCOFINS_Val: TFloatField;
    QrPesqPIS_TOTAL: TFloatField;
    QrPesqCOFINS_TOTAL: TFloatField;
    QrPesqIOC_VAL: TFloatField;
    QrPesqCPMF_VAL: TFloatField;
    QrPesqMaxVencto: TDateField;
    QrPesqCHDevPg: TFloatField;
    QrPesqMINTC: TFloatField;
    QrPesqMINAV: TFloatField;
    QrPesqMINTC_AM: TFloatField;
    QrPesqPIS_T_Val: TFloatField;
    QrPesqCOFINS_T_Val: TFloatField;
    QrPesqPgLiq: TFloatField;
    QrPesqDUDevPg: TFloatField;
    QrPesqNF: TIntegerField;
    QrPesqItens: TIntegerField;
    QrPesqREPAS_V: TFloatField;
    QrPesqREPAS_T: TFloatField;
    QrPesqTXCOMPRAT: TFloatField;
    QrPesqVALVALOREMT: TFloatField;
    QrPesqMARGEM_BRUTA: TFloatField;
    QrPesqMARGEM_TODOS: TFloatField;
    QrPesqPROPRIO: TFloatField;
    QrPesqNREP_V: TFloatField;
    QrPesqNREP_T: TFloatField;
    QrPesqPRO_COL: TFloatField;
    QrPesqMes: TFloatField;
    QrPesqNOMEMES: TWideStringField;
    QrPesqMARGEM_COL: TFloatField;
    QrOperaNOMECLIENTE: TWideStringField;
    QrOperaData: TDateField;
    QrOperaLote: TIntegerField;
    QrOperaCodigo: TIntegerField;
    QrOperaPROPRIO: TFloatField;
    QrOperaTXCOMPRAT: TFloatField;
    QrOperaVALVALOREMT: TFloatField;
    QrOperaMARGEM_TODOS: TFloatField;
    QrOperaREPAS_V: TFloatField;
    QrOperaMARGEM_BRUTA: TFloatField;
    QrOperaNREP_T: TFloatField;
    QrOperaPRO_COL: TFloatField;
    QrOperaMARGEM_COL: TFloatField;
    QrOperaPIS_T_VAL: TFloatField;
    QrOperaISS_VAL: TFloatField;
    QrOperaCOFINS_T_VAL: TFloatField;
    QrOperaTOTAL: TFloatField;
    QrOperaNOMEMES: TWideStringField;
    RGAgrup3: TRadioGroup;
    RGAgrup2: TRadioGroup;
    RGAgrup1: TRadioGroup;
    CkDescA: TCheckBox;
    CkDescB: TCheckBox;
    CkDescC: TCheckBox;
    CkDescG: TCheckBox;
    DBGrid2: TDBGrid;
    CkRepasse: TCheckBox;
    frxOperaCli: TfrxReport;
    frxDsOpera: TfrxDBDataset;
    frxOperaCol: TfrxReport;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel2: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    BtSaida: TBitBtn;
    BtPesquisa: TBitBtn;
    BtReabre: TBitBtn;
    BtImprime: TBitBtn;
    BitBtn1: TBitBtn;
    ProgressBar1: TProgressBar;
    GroupBox1: TGroupBox;
    LaNO1: TLabel;
    LaOrd1: TLabel;
    LaNO2: TLabel;
    LaOrd2: TLabel;
    LaNO3: TLabel;
    LaOrd3: TLabel;
    GroupBox2: TGroupBox;
    LaNA1: TLabel;
    LaAgr1: TLabel;
    LaNA2: TLabel;
    LaAgr2: TLabel;
    LaNA3: TLabel;
    LaAgr3: TLabel;
    QrRepassadosFatNum: TFloatField;
    QrNRepFatNum: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure EdColigadoChange(Sender: TObject);
    procedure DBGrid1TitleClick(Column: TColumn);
    procedure BtPesquisaClick(Sender: TObject);
    procedure TPIniChange(Sender: TObject);
    procedure TPFimChange(Sender: TObject);
    procedure QrPesqCalcFields(DataSet: TDataSet);
    procedure RGGruposClick(Sender: TObject);
    procedure CkNivel4Click(Sender: TObject);
    procedure RGOrdem1Click(Sender: TObject);
    procedure RGOrdem2Click(Sender: TObject);
    procedure RGOrdem3Click(Sender: TObject);
    procedure RGSinteticoClick(Sender: TObject);
    procedure RGAgrup1Click(Sender: TObject);
    procedure RGAgrup2Click(Sender: TObject);
    procedure RGAgrup3Click(Sender: TObject);
    procedure QrOperaAfterClose(DataSet: TDataSet);
    procedure QrOperaAfterOpen(DataSet: TDataSet);
    procedure BtReabreClick(Sender: TObject);
    procedure CkDescAClick(Sender: TObject);
    procedure CkDescBClick(Sender: TObject);
    procedure CkDescCClick(Sender: TObject);
    procedure CkDescGClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure frxOperaCliGetValue(const VarName: String;
      var Value: Variant);
  private
    { Private declarations }
    FAgrC1, FAgrC2, FAgrC3, FTitle,
    FOrdC1, FOrdC2, FOrdC3, FOrdC4,
    FOrdS1, FOrdS2, FOrdS3, FOrdS4,
    FTmpTabResLiqFac, FCampo: String;
    function NomeDeExibicao(Nome: String): String;
    //function CampoDaExibicao(Nome: String): String;
    procedure RedefineOrdem(Titulo: String; Novo: Boolean);
    procedure CalculaOpera;
    procedure ReopenOpera(Redefine: Boolean);
    procedure EnabledBotoes(Habilita: Boolean);

  public
    { Public declarations }
  end;

  var
    FmResult1: TFmResult1;

implementation

{$R *.DFM}

uses UnMyObjects, Module, UnInternalConsts, OperaImp, UCreate, UMySQLModule, 
  MyListas, ModuleGeral;

const
  FORDEM_RG: array[0..3] of string = ('NOMECLIENTE', 'Data', 'Mes', 'Total');
  FAGRUP_RG: array[0..3] of string = ('NOMECLIENTE', 'Data', 'Mes', ''); // N�o pode ser agrupado pelo total

procedure TFmResult1.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmResult1.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmResult1.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmResult1.EdClienteChange(Sender: TObject);
begin
  EnabledBotoes(False);
end;

procedure TFmResult1.FormCreate(Sender: TObject);
var
  Cam: String;
  i, f, k, t: Integer;
  Data: TDateTime;
begin
  ImgTipo.SQLType := stPsq;
  //
  TPIni.Date := Date -30;
  TPFim.Date := Date;
  FCampo := DBGrid2.Columns[0].FieldName;
  UCriar.GerenciaTabelaLocal('ResLiqFac', acCreate);
  FOrdC1 := 'Total';
  FTitle := 'Total';
  FOrdS1 := '';
  FOrdC2 := '';
  FOrdS2 := '';
  FOrdC3 := '';
  FOrdS3 := '';
  //
  UMyMod.AbreQuery(QrClientes, Dmod.MyDB);
  UMyMod.AbreQuery(QrColigado, Dmod.MyDB);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrRepassados, Dmod.MyDB, [
  'SELECT loi.FatNum, SUM(rit.Valor) Valor, ',
  'SUM(rit.JurosV) JurosV ',
  'FROM repasits rit ',
  'LEFT JOIN ' + CO_TabLctA + ' loi ON loi.FatParcela=rit.Origem ',
  'WHERE loi.FatID=' + Geral.FF0(VAR_FATID_0301),
  'GROUP BY loi.FatNum ',
  '']);
  //
  Cam := Application.Title+'\InitialConfig\'+'OperaImp';
  t := Geral.ReadAppKey('TipPeriod', Cam, ktInteger, 0, HKey_LOCAL_MACHINE);
  i := Geral.ReadAppKey('DiaSemIni', Cam, ktInteger, 0, HKEY_LOCAL_MACHINE);
  f := Geral.ReadAppKey('DiaSemFim', Cam, ktInteger, 6, HKEY_LOCAL_MACHINE);
  //
  if t = 1 then
  begin
    k := -1;
    Data := Date+1;
    while k <> f-1 do
    begin
      Data := Data -1;
      k := DayOfWeek(Data);
    end;
    TPFim.Date := Data+1;
    TPIni.Date := Data-f+i+1;
  end;
  RedefineOrdem(FTitle, False);
  DBGrid1.Align := alClient;
  DBGrid2.Align := alClient;
end;

procedure TFmResult1.BtImprimeClick(Sender: TObject);
begin
  // caso a pesquisa foi rejeitada ...
  if QrOpera.State = dsInactive then
  begin
    Application.MessageBox('Pesquisa n�o definida corretamente!', 'Aviso',
    MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if Geral.IMV(EdColigado.Text) = 0 then
    MyObjects.frxMostra(frxOperaCli, 'Resultado das Opera��oes Realizadas')
  else
    MyObjects.frxMostra(frxOperaCol,
    'Resultado das Opera��oes Realizadas com coligadas');
end;

procedure TFmResult1.CalculaOpera;
var
  Coligado, Cliente: Integer;
begin
  Screen.Cursor := crHourGlass;
  Coligado := EdColigado.ValueVariant;
  //
{
  QrNRep.Close;
  QrNRep.Params[0].AsInteger := Coligado;
  QrNRep. Open;
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrNRep, Dmod.MyDB, [
  'SELECT loi.FatNum, SUM(rit.Valor) Valor, ',
  'SUM(rit.JurosV) JurosV ',
  'FROM repasits rit ',
  'LEFT JOIN repas    rep ON rep.Codigo=rit.Codigo ',
  'LEFT JOIN ' + CO_TabLctA + ' loi ON loi.FatParcela=rit.Origem ',
  'WHERE rep.Coligado=' + Geral.FF0(Coligado),
  'GROUP BY loi.FatNum ',
  '']);
  //
  Cliente  := EdCliente.ValueVariant;
  QrPesq.Close;
  QrPesq.SQL.Clear;
  QrPesq.SQL.Add('SELECT MONTH(lo.Data)+YEAR(lo.Data)*100+0.00  Mes,');
  QrPesq.SQL.Add('lo.*, CASE WHEN en.Tipo=0 THEN en.RazaoSocial');
  QrPesq.SQL.Add('ELSE en.Nome END NOMECLIENTE');
  QrPesq.SQL.Add('FROM ' + CO_TabLotA + ' lo');
  QrPesq.SQL.Add('LEFT JOIN entidades en ON en.Codigo=lo.Cliente');
  QrPesq.SQL.Add('WHERE lo.data BETWEEN :P0 AND :P1');
  if Cliente <> 0 then
    QrPesq.SQL.Add('AND lo.Cliente='+IntToStr(Cliente));
  //
  QrPesq.Params[00].AsString := FormatDateTime(VAR_FORMATDATE, TPIni.Date);
  QrPesq.Params[01].AsString := FormatDateTime(VAR_FORMATDATE, TPFim.Date);
  UMyMod.AbreQuery(QrPesq, Dmod.MyDB);
  ProgressBar1.Position := 0;
  ProgressBar1.Visible := True;
  ProgressBar1.Max := QrPesq.RecordCount;
  ProgressBar1.Update;
  //
  FTmpTabResLiqFac := UCriar.RecriaTempTableNovo(ntrttresliqfac, DmodG.QrUpdPID1, False);
  //
  DmodG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('INSERT INTO ' + FTmpTabResLiqFac + ' SET ');
  DmodG.QrUpdPID1.SQL.Add('  NOMECLIENTE  =:P00');
  DmodG.QrUpdPID1.SQL.Add(', Data         =:P01');
  DmodG.QrUpdPID1.SQL.Add(', Lote         =:P02');
  DmodG.QrUpdPID1.SQL.Add(', Codigo       =:P03');
  DmodG.QrUpdPID1.SQL.Add(', PROPRIO      =:P04');
  DmodG.QrUpdPID1.SQL.Add(', TXCOMPRAT    =:P05');
  DmodG.QrUpdPID1.SQL.Add(', VALVALOREMT  =:P06');
  DmodG.QrUpdPID1.SQL.Add(', MARGEM_TODOS =:P07');
  DmodG.QrUpdPID1.SQL.Add(', REPAS_V      =:P08');
  DmodG.QrUpdPID1.SQL.Add(', MARGEM_BRUTA =:P09');
  DmodG.QrUpdPID1.SQL.Add(', NREP_T       =:P10');
  DmodG.QrUpdPID1.SQL.Add(', PRO_COL      =:P11');
  DmodG.QrUpdPID1.SQL.Add(', MARGEM_COL   =:P12');
  DmodG.QrUpdPID1.SQL.Add(', PIS_T_VAL    =:P13');
  DmodG.QrUpdPID1.SQL.Add(', ISS_VAL      =:P14');
  DmodG.QrUpdPID1.SQL.Add(', COFINS_T_VAL =:P15');
  DmodG.QrUpdPID1.SQL.Add(', TOTAL        =:P16');
  DmodG.QrUpdPID1.SQL.Add(', NOMEMES      =:P17');
  DmodG.QrUpdPID1.SQL.Add(', Mes          =:P18');
  DmodG.QrUpdPID1.SQL.Add('');
  //
  QrPesq.First;
  //
  while not QrPesq.Eof do
  begin
    ProgressBar1.Position := ProgressBar1.Position + 1;
    ProgressBar1.Update;
    //
    DmodG.QrUpdPID1.Params[00].AsString  := QrPesqNOMECLIENTE.Value  ;
    DmodG.QrUpdPID1.Params[01].AsString  := Geral.FDT(QrPesqData.Value, 1);
    DmodG.QrUpdPID1.Params[02].AsInteger := QrPesqLote.Value         ;
    DmodG.QrUpdPID1.Params[03].AsInteger := QrPesqCodigo.Value       ;
    DmodG.QrUpdPID1.Params[04].AsFloat   := QrPesqPROPRIO.Value      ;
    DmodG.QrUpdPID1.Params[05].AsFloat   := QrPesqTXCOMPRAT.Value    ;
    DmodG.QrUpdPID1.Params[06].AsFloat   := QrPesqVALVALOREMT.Value  ;
    DmodG.QrUpdPID1.Params[07].AsFloat   := QrPesqMARGEM_TODOS.Value ;
    DmodG.QrUpdPID1.Params[08].AsFloat   := QrPesqREPAS_V.Value      ;
    DmodG.QrUpdPID1.Params[09].AsFloat   := QrPesqMARGEM_BRUTA.Value ;
    DmodG.QrUpdPID1.Params[10].AsFloat   := QrPesqNREP_T.Value       ;
    DmodG.QrUpdPID1.Params[11].AsFloat   := QrPesqPRO_COL.Value      ;
    DmodG.QrUpdPID1.Params[12].AsFloat   := QrPesqMARGEM_COL.Value   ;
    DmodG.QrUpdPID1.Params[13].AsFloat   := QrPesqPIS_T_VAL.Value    ;
    DmodG.QrUpdPID1.Params[14].AsFloat   := QrPesqISS_VAL.Value      ;
    DmodG.QrUpdPID1.Params[15].AsFloat   := QrPesqCOFINS_T_VAL.Value ;
    DmodG.QrUpdPID1.Params[16].AsFloat   := QrPesqTOTAL.Value        ;
    DmodG.QrUpdPID1.Params[17].AsString  := QrPesqNOMEMES.Value      ;
    DmodG.QrUpdPID1.Params[18].AsInteger := Trunc(QrPesqMes.Value)   ;
    DmodG.QrUpdPID1.ExecSQL;
    //
    QrPesq.Next;
  end;
  //
  EnabledBotoes(True);
  //
  QrOpera.Close;
  Screen.Cursor := crDefault;
  ProgressBar1.Visible := False;
end;

procedure TFmResult1.EdColigadoChange(Sender: TObject);
begin
  EnabledBotoes(False);
end;

procedure TFmResult1.RedefineOrdem(Titulo: String; Novo: Boolean);
var
  i: Integer;
  y: TRadioGroup;
begin
  y := nil;
  FOrdC1 := FORDEM_RG[RGOrdem1.ItemIndex];
  FOrdS1 := MLAGeral.EscolhaDe2Str(CkDescA.Checked, 'DESC', '');
  FOrdC2 := FORDEM_RG[RGOrdem2.ItemIndex];
  FOrdS2 := MLAGeral.EscolhaDe2Str(CkDescB.Checked, 'DESC', '');;
  FOrdC3 := FORDEM_RG[RGOrdem3.ItemIndex];
  FOrdS3 := MLAGeral.EscolhaDe2Str(CkDescC.Checked, 'DESC', '');;
  FOrdC4 := Titulo;
  FOrdS4 := MLAGeral.EscolhaDe2Str(CkDescG.Checked, 'DESC', '');
  //
  LaOrd1.Caption := NomeDeExibicao(FOrdC1)+' '+FOrdS1;
  LaOrd2.Caption := NomeDeExibicao(FOrdC2)+' '+FOrdS2;
  LaOrd3.Caption := NomeDeExibicao(FOrdC3)+' '+FOrdS3;
  //LaOrd4.Caption := NomeDeExibicao(FOrdC4)+' '+FOrdS4;
  FTitle := Titulo;
  //
  LaOrd1.Font.Color := clInactiveCaption;
  LaOrd2.Font.Color := clInactiveCaption;
  LaOrd3.Font.Color := clInactiveCaption;
  //LaOrd4.Font.Color := clActiveCaption;
  LaNO1.Font.Color := clInactiveCaption;
  LaNO2.Font.Color := clInactiveCaption;
  LaNO3.Font.Color := clInactiveCaption;
  //LaNO4.Font.Color := clActiveCaption;
  //
  FAgrC1 := FAGRUP_RG[RGAgrup1.ItemIndex];
  FAgrC2 := FAGRUP_RG[RGAgrup2.ItemIndex];
  FAgrC3 := FAGRUP_RG[RGAgrup3.ItemIndex];
  //
  LaAgr1.Caption := NomeDeExibicao(FAgrC1);
  LaAgr2.Caption := NomeDeExibicao(FAgrC2);
  LaAgr3.Caption := NomeDeExibicao(FAgrC3);
  FTitle := Titulo;
  //
  LaAgr1.Font.Color := clInactiveCaption;
  LaAgr2.Font.Color := clInactiveCaption;
  LaAgr3.Font.Color := clInactiveCaption;
  LaNA1.Font.Color := clInactiveCaption;
  LaNA2.Font.Color := clInactiveCaption;
  LaNA3.Font.Color := clInactiveCaption;
  QrOpera.Close;
  if RGSintetico.ItemIndex = 0 then
  begin
    DBGrid1.Visible := True;
    DBGrid2.Visible := False;
  end else begin
    for i := 0 to DBGrid2.Columns.Count -1 do
    begin
      if DBGrid2.Columns[i].FieldName = FCampo then
      begin
        case RGGrupos.ItemIndex of
          0: y := RGAgrup1;
          1: y := RGAgrup2;
          2: y := RGAgrup3;
        end;
        case y.ItemIndex of
          0: DBGrid2.Columns[i].FieldName := 'NOMECLIENTE';
          1: DBGrid2.Columns[i].FieldName := 'Data';
          2: DBGrid2.Columns[i].FieldName := 'NOMEMES';
          3: DBGrid2.Columns[i].FieldName := '%%ERRO%%';
        end;
        if y <> nil then
          DBGrid2.Columns[i].Title.Caption := y.Items[y.ItemIndex];
        FCampo := DBGrid2.Columns[i].FieldName;
      end;
    end;
    DBGrid2.Visible := True;
    DBGrid1.Visible := False;
  end;
end;

procedure TFmResult1.DBGrid1TitleClick(Column: TColumn);
var
  NomeCol: String;
begin
  NomeCol := Column.FieldName;
  if NomeDeExibicao(NomeCol) = RGOrdem1.Items[3] then
    CkDescG.Checked := not CkDescG.Checked;
  RGOrdem1.Items[3] := NomeDeExibicao(NomeCol);
  RGOrdem2.Items[3] := NomeDeExibicao(NomeCol);
  RGOrdem3.Items[3] := NomeDeExibicao(NomeCol);
  RedefineOrdem(NomeCol, True);
  if QrOpera.State = dsBrowse then ReopenOpera(True);
end;

procedure TFmResult1.BtPesquisaClick(Sender: TObject);
begin
  CalculaOpera;
end;

procedure TFmResult1.TPIniChange(Sender: TObject);
begin
  EnabledBotoes(False);
end;

procedure TFmResult1.TPFimChange(Sender: TObject);
begin
  EnabledBotoes(False);
end;

procedure TFmResult1.EnabledBotoes(Habilita: Boolean);
begin
  BtPesquisa.Enabled := not Habilita;
  BtReabre.Enabled   :=     Habilita;
  if BtPesquisa.Enabled = True then QrOpera.Close;
end;

procedure TFmResult1.QrPesqCalcFields(DataSet: TDataSet);
var
  Valores: MyArrayD02;
begin
  QrPesqNOMEMES.Value := MLAGeral.FormatAno4Mes2(Trunc(QrPesqMes.Value));

  Valores := DMod.ObtemValoresDeLote(QrPesqCodigo.Value);
  QrPesqTXCOMPRAT.Value   := QrPesqTxCompra.Value   + Valores[1];
  QrPesqVALVALOREMT.Value := QrPesqValValorem.Value + Valores[2];
  //
  QrPesqPROPRIO.Value := QrPesqTotal.Value - QrPesqREPAS_T.Value;
  QrPesqPRO_COL.Value := QrPesqPROPRIO.Value + QrPesqNREP_T.Value;
  //
  QrPesqMARGEM_TODOS.Value := QrPesqTXCOMPRAT.Value + QrPesqVALVALOREMT.Value -
  QrPesqPIS_T_VAL.Value - QrPesqISS_Val.Value - QrPesqCOFINS_T_VAL.Value; //- QrPesqIOC_VAL.Value;
  QrPesqMARGEM_BRUTA.Value := QrPesqMARGEM_TODOS.Value - QrPesqREPAS_V.Value;
  QrPesqMARGEM_COL.Value := QrPesqNREP_V.Value+QrPesqMARGEM_BRUTA.Value;
end;

procedure TFmResult1.ReopenOpera(Redefine: Boolean);
var
  Agrup: String;
begin
  if Redefine then RedefineOrdem(FTitle, False);
  //
  QrOpera.Close;
  QrOpera.DataBase := DmodG.MyPID_DB;
  QrOpera.SQL.Clear;
  if RGSintetico.ItemIndex = 0 then
  begin
    QrOpera.SQL.Add('SELECT * FROM ' + FTmpTabResLiqFac);
    QrOpera.SQL.Add(MLAGeral.OrdemSQL4(
      FOrdC1, FOrdS1, FOrdC2, FOrdS2, FOrdC3, FOrdS3, FOrdC4, FOrdS4));
  end else begin
    Agrup := MLAGeral.GroupSQL4(RGGrupos.ItemIndex+1,FAgrC1, FAgrC2, FAgrC3, '');
    if Pos('TOTAL', Uppercase(Agrup)) > 0 then
    (*if (RGOrdem1.ItemIndex = 3) or
       (RGOrdem2.ItemIndex = 3) or
       (RGOrdem3.ItemIndex = 3) then*)
    begin
      Application.MessageBox(PChar('"'+RGOrdem1.Items[3]+
      '" n�o pode fazer parte do "Estilo" no relat�rio sint�tico!'), 'Aviso',
      MB_OK+MB_ICONWARNING);
      Exit;
    end;
    QrOpera.SQL.Add('SELECT NOMECLIENTE, NOMEMES, Mes, Data, Lote, Codigo,');
    QrOpera.SQL.Add('SUM(PROPRIO) PROPRIO, SUM(TXCOMPRAT) TXCOMPRAT,');
    QrOpera.SQL.Add('SUM(VALVALOREMT) VALVALOREMT, SUM(MARGEM_TODOS) MARGEM_TODOS,');
    QrOpera.SQL.Add('SUM(REPAS_V) REPAS_V, SUM(MARGEM_BRUTA) MARGEM_BRUTA,');
    QrOpera.SQL.Add('SUM(NREP_T) NREP_T, SUM(PRO_COL) PRO_COL,');
    QrOpera.SQL.Add('SUM(MARGEM_COL) MARGEM_COL, SUM(PIS_T_VAL) PIS_T_VAL,');
    QrOpera.SQL.Add('SUM(ISS_VAL) ISS_VAL, SUM(COFINS_T_VAL) COFINS_T_VAL,');
    QrOpera.SQL.Add('SUM(TOTAL) TOTAL');
    QrOpera.SQL.Add('FROM ' + FTmpTabResLiqFac);
    QrOpera.SQL.Add(Agrup);
    QrOpera.SQL.Add(MLAGeral.OrdemSQL4(
      FOrdC1, FOrdS1, FOrdC2, FOrdS2, FOrdC3, FOrdS3, FOrdC4, FOrdS4));
  end;
  UMyMod.AbreQuery(QrOpera, Dmod.MyLocDatabase);
end;

procedure TFmResult1.RGGruposClick(Sender: TObject);
begin
  RedefineOrdem(FTitle, False);
end;

procedure TFmResult1.CkNivel4Click(Sender: TObject);
begin
  RedefineOrdem(FTitle, False);
end;

procedure TFmResult1.RGOrdem1Click(Sender: TObject);
begin
  RGAgrup1.ItemIndex := RGOrdem1.ItemIndex;
  RedefineOrdem(FTitle, False);
end;

procedure TFmResult1.RGOrdem2Click(Sender: TObject);
begin
  RGAgrup2.ItemIndex := RGOrdem2.ItemIndex;
  RedefineOrdem(FTitle, False);
end;

procedure TFmResult1.RGOrdem3Click(Sender: TObject);
begin
  RGAgrup3.ItemIndex := RGOrdem3.ItemIndex;
  RedefineOrdem(FTitle, False);
end;

procedure TFmResult1.RGSinteticoClick(Sender: TObject);
begin
  RedefineOrdem(FTitle, False);
end;

procedure TFmResult1.RGAgrup1Click(Sender: TObject);
begin
  RedefineOrdem(FTitle, False);
end;

procedure TFmResult1.RGAgrup2Click(Sender: TObject);
begin
  RedefineOrdem(FTitle, False);
end;

procedure TFmResult1.RGAgrup3Click(Sender: TObject);
begin
  RedefineOrdem(FTitle, False);
end;

procedure TFmResult1.QrOperaAfterClose(DataSet: TDataSet);
begin
  BtImprime.Enabled := False;
end;

procedure TFmResult1.QrOperaAfterOpen(DataSet: TDataSet);
begin
  BtImprime.Enabled := True;
end;

procedure TFmResult1.BtReabreClick(Sender: TObject);
begin
  ReopenOpera(False);
end;

procedure TFmResult1.CkDescAClick(Sender: TObject);
begin
  RedefineOrdem(FTitle, False);
end;

procedure TFmResult1.CkDescBClick(Sender: TObject);
begin
  RedefineOrdem(FTitle, False);
end;

procedure TFmResult1.CkDescCClick(Sender: TObject);
begin
  RedefineOrdem(FTitle, False);
end;

procedure TFmResult1.CkDescGClick(Sender: TObject);
begin
  RedefineOrdem(FTitle, True);
end;

function TFmResult1.NomeDeExibicao(Nome: String): String;
begin
       if Uppercase(Nome) = 'DATA'         then Result := 'Data da opera��o'
  else if Uppercase(Nome) = 'LOTE'         then Result := 'N� do Border�'
  else if Uppercase(Nome) = 'NOMECLIENTE'  then Result := 'Nome do cliente'
  else if Uppercase(Nome) = 'NOMEMES'      then Result := 'M�s'
  else if Uppercase(Nome) = 'MES'          then Result := 'M�s'
  else if Uppercase(Nome) = 'TOTAL'        then Result := 'Valor negociado'
  else if Uppercase(Nome) = 'PROPRIO'      then Result := 'Valor pr�prio'
  else if Uppercase(Nome) = 'TXCOMPRAT'    then Result := 'Taxa de compra'
  else if Uppercase(Nome) = 'VALVALOREMT'  then Result := 'Ad Valorem'
  else if Uppercase(Nome) = 'MARGEM_TODOS' then Result := 'Margem todos'
  else if Uppercase(Nome) = 'REPAS_V'      then Result := 'Valor repasse'
  else if Uppercase(Nome) = 'MARGEM_BRUTA' then Result := 'Margem l�quida'
  else if Uppercase(Nome) = 'PIS_T_VAL'    then Result := 'Valor PIS'
  else if Uppercase(Nome) = 'ISS_VAL'      then Result := 'Valor ISS'
  else if Uppercase(Nome) = 'COFINS_T_VAL' then Result := 'Valor COFINS'
  else
  Result := Nome;
end;

(*function TFmResult1.CampoDaExibicao(Nome: String): String;
begin
       if Nome = 'Data da opera��o'    then Result := 'DATA'
  else if Nome = 'N� do Border�'       then Result := 'LOTE'
  else if Nome = 'Nome do cliente'     then Result := 'NOMECLIENTE'
  else if Nome = 'M�s'                 then Result := 'NOMEMES'
  else if Nome = 'M�s'                 then Result := 'MES'
  else if Nome = 'Valor negociado'     then Result := 'TOTAL'
  else if Nome = 'Valor pr�prio'       then Result := 'PROPRIO'
  else if Nome = 'Taxa de compra'      then Result := 'TXCOMPRAT'
  else if Nome = 'Ad Valorem'          then Result := 'VALVALOREMT'
  else if Nome = 'Margem todos'        then Result := 'MARGEM_TODOS'
  else if Nome = 'Valor repasse'       then Result := 'REPAS_V'
  else if Nome = 'Margem l�quida'      then Result := 'MARGEM_BRUTA'
  else if Nome = 'Valor PIS'           then Result := 'PIS_T_VAL'
  else if Nome = 'Valor ISS'           then Result := 'ISS_VAL'
  else if Nome = 'Valor COFINS'        then Result := 'COFINS_T_VAL'
  else
  Result := '##ERRO##';
end;*)

procedure TFmResult1.BitBtn1Click(Sender: TObject);
begin
  UMyMod.ConfigJanela20(Name, EdCliente, CBCliente, EdColigado, CBColigado,
    TPIni, TPFim, RGOrdem1, RGOrdem2, RGOrdem3, RGAgrup1, RGAgrup2, RGAgrup3,
    CkDescA, CkDescB, CkDescC, CkDescG, RGGrupos, RGSintetico, CkRepasse, nil);
end;

procedure TFmResult1.frxOperaCliGetValue(const VarName: String;
  var Value: Variant);
var
  y: TRadioGroup;
begin
  y := nil;
  if VarName = 'VARF_CLIENTE' then
  begin
    if CbCliente.KeyValue = NULL then Value := 'TODOS' else
    Value := CBCliente.Text;
  end else if VarName = 'VARF_COLIGADO' then
  begin
    if CbColigado.KeyValue = NULL then Value := ' ' else
    Value := CBColigado.Text;
  end else if VarName = 'VARF_PERIODO' then Value :=
    FormatDateTime(VAR_FORMATDATE3, TPIni.Date) + CO_ATE +
    FormatDateTime(VAR_FORMATDATE3, TPFim.Date)
  else if VarName = 'VFR_LA1NOME' then
  begin
    if RGSintetico.ItemIndex = 0 then
    begin
      case RGOrdem1.ItemIndex of
        0: Value := 'Cliente: '+QrOperaNOMECLIENTE.Value;
        1: Value := 'Emitido no dia  '+Geral.FDT(QrOperaData.Value, 2);
        2: Value := 'Emitido no m�s de '+QrOperaNOMEMES.Value;
        3: Value := 'Valor negociado:'+Geral.FFT(QrOperaTotal.Value, 2, siNegativo);
      end;
    end else begin
      case RGAgrup1.ItemIndex of
        0: Value := QrOperaNOMECLIENTE.Value;
        1: Value := Geral.FDT(QrOperaData.Value, 2);
        2: Value := QrOperaNOMEMES.Value;
        3: Value := Geral.FFT(QrOperaTotal.Value, 2, siNegativo);
      end;
    end;
  end
  else if VarName = 'VFR_LA2NOME' then
  begin
    if RGSintetico.ItemIndex = 0 then
    begin
      case RGOrdem2.ItemIndex of
        0: Value := 'Cliente: '+QrOperaNOMECLIENTE.Value;
        1: Value := 'Emitido no dia  '+Geral.FDT(QrOperaData.Value, 2);
        2: Value := 'Emitido no m�s de '+QrOperaNOMEMES.Value;
        3: Value := 'Valor negociado:'+Geral.FFT(QrOperaTotal.Value, 2, siNegativo);
      end;
    end else begin
      case RGAgrup2.ItemIndex of
        0: Value := QrOperaNOMECLIENTE.Value;
        1: Value := Geral.FDT(QrOperaData.Value, 2);
        2: Value := QrOperaNOMEMES.Value;
        // Parei Aqui
        //3: Value := Geral.FFT(QrOperaTotal.Value, 2, siNegativo);
      end;
    end;
  end
  else if VarName = 'VFR_LA3NOME' then
  begin
    case RGGrupos.ItemIndex of
      0: y := RGAgrup1;
      1: y := RGAgrup2;
      2: y := RGAgrup3;
    end;
    case y.ItemIndex of
      0: Value := QrOperaNOMECLIENTE.Value;
      1: Value := Geral.FDT(QrOperaData.Value, 2);
      2: Value := QrOperaNOMEMES.Value;
      3:
      begin
        Value := '### ??? ###';
      end;
    end;
  end
  else if VarName = 'VFR_LA3TITU' then
  begin
    case RGGrupos.ItemIndex of
      0: y := RGAgrup1;
      1: y := RGAgrup2;
      2: y := RGAgrup3;
    end;
    if y <> nil then Value := y.Items[y.ItemIndex]
    else Value := '';
  end



  // User function

  else if VarName = 'VFR_ORD1' then
  begin
    if RGGrupos.ItemIndex < 1 then Value := 0 else
    Value := RGOrdem1.ItemIndex + 1;
  end else
  if VarName = 'VFR_ORD2' then
  begin
    if RGGrupos.ItemIndex < 2 then Value := 0 else
    Value := RGOrdem2.ItemIndex + 1;
  end else if VarName = 'VARF_VISIBLE' then
    Value := not Geral.IntToBool_0(RGSintetico.ItemIndex)
  else if VarName = 'VFR_SINTETICO' then
    Value := RGSintetico.ItemIndex = 1;


end;

end.

