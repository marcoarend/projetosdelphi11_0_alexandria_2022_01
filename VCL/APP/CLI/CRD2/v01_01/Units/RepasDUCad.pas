unit RepasDUCad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, ComCtrls,
  Grids, DBGrids, Menus, frxClass, frxDBSet, Variants, dmkGeral, DmkDAC_PF,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, UnDmkProcFunc, dmkImage, UnDmkEnums;

type
  TFmRepasDUCad = class(TForm)
    PainelDados: TPanel;
    DsRepas: TDataSource;
    QrRepas: TmySQLQuery;
    PainelEdita: TPanel;
    QrColigado: TmySQLQuery;
    DsColigado: TDataSource;
    QrColigadoCodigo: TIntegerField;
    QrColigadoNOMECOLIGADO: TWideStringField;
    QrRepasCodigo: TIntegerField;
    QrRepasData: TDateField;
    QrRepasTotal: TFloatField;
    QrRepasJurosV: TFloatField;
    QrRepasColigado: TIntegerField;
    QrRepasNOMECOLIGADO: TWideStringField;
    QrRepasSALDO: TFloatField;
    QrRepasIts: TmySQLQuery;
    DsRepasIts: TDataSource;
    QrBanco: TmySQLQuery;
    QrBancoCodigo: TIntegerField;
    QrBancoNome: TWideStringField;
    QrBancoLk: TIntegerField;
    QrBancoDataCad: TDateField;
    QrBancoDataAlt: TDateField;
    QrBancoUserCad: TIntegerField;
    QrBancoUserAlt: TIntegerField;
    QrBancoSite: TWideStringField;
    DsBanco: TDataSource;
    PMLote: TPopupMenu;
    Incluinovolote1: TMenuItem;
    Alteraloteatual1: TMenuItem;
    Excluiloteatual1: TMenuItem;
    PMCheque: TPopupMenu;
    Adicionachequeaoloteatual1: TMenuItem;
    Alterarepassedochequeatual1: TMenuItem;
    Retirachequedoloteatual1: TMenuItem;
    QrSum: TmySQLQuery;
    QrSumValor: TFloatField;
    QrSumJurosV: TFloatField;
    QrRepasFatorCompra: TFloatField;
    QrRepasItsBanco: TIntegerField;
    QrRepasItsAgencia: TIntegerField;
    QrRepasItsEmitente: TWideStringField;
    QrRepasItsCodigo: TIntegerField;
    QrRepasItsControle: TIntegerField;
    QrRepasItsOrigem: TIntegerField;
    QrRepasItsDias: TIntegerField;
    QrRepasItsTaxa: TFloatField;
    QrRepasItsJurosP: TFloatField;
    QrRepasItsJurosV: TFloatField;
    QrRepasItsCPF_TXT: TWideStringField;
    QrRepasItsDDeposito: TDateField;
    QrRepasItsLIQUIDO: TFloatField;
    QrLI: TmySQLQuery;
    QrRepasItsDuplicata: TWideStringField;
    frxRepasIts: TfrxReport;
    frxDsRepasIts: TfrxDBDataset;
    frxDsRepas: TfrxDBDataset;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    BtImpCalculado: TBitBtn;
    BtImpPesquisa: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtReceitas: TBitBtn;
    BtCheque: TBitBtn;
    BtImportar: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Progress: TProgressBar;
    GBEdita: TGroupBox;
    Label9: TLabel;
    Label75: TLabel;
    Label3: TLabel;
    EdCodigo: TdmkEdit;
    EdColigado: TdmkEditCB;
    CBColigado: TdmkDBLookupComboBox;
    TPData: TDateTimePicker;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel6: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    PainelData: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    GradeRepas: TDBGrid;
    QrRepasItsCNPJCPF: TWideStringField;
    QrRepasItsVencimento: TDateField;
    QrRepasItsValor: TFloatField;
    QrLITipo: TSmallintField;
    QrLIRepassado: TSmallintField;
    QrLIFatNum: TFloatField;
    QrLIFatParcela: TIntegerField;
    QrLIDDeposito: TDateField;
    QrLIDCompra: TDateField;
    QrLICredito: TFloatField;
    QrLIVencimento: TDateField;
    QrLIBanco: TIntegerField;
    QrLIAgencia: TIntegerField;
    QrLIDuplicata: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtReceitasClick(Sender: TObject);
    procedure BtChequeClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrRepasAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrRepasAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrRepasBeforeOpen(DataSet: TDataSet);
    procedure Incluinovolote1Click(Sender: TObject);
    procedure Alteraloteatual1Click(Sender: TObject);
    procedure Adicionachequeaoloteatual1Click(Sender: TObject);
    procedure Retirachequedoloteatual1Click(Sender: TObject);
    procedure QrRepasItsCalcFields(DataSet: TDataSet);
    procedure BtImpCalculadoClick(Sender: TObject);
    procedure BtImpPesquisaClick(Sender: TObject);
    procedure QrRepasItsAfterOpen(DataSet: TDataSet);
    procedure BtImportarClick(Sender: TObject);
    procedure frxRepasItsGetValue(const VarName: String;
      var Value: Variant);
  private
    FItensBloqueados, FItensImportados: Integer;
    FItens: Boolean;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure ReopenRepasIts;
    procedure ImportaItemDeLote(Taxa: Double);
    procedure RecalculaCheque;
  public
    { Public declarations }
    FRepasIts: Integer;
    procedure CalculaLote(Lote: Integer);
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmRepasDUCad: TFmRepasDUCad;
  
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, Principal, RepasImp, Lot0Loc, GetPercent, RepasLoc, 
  MyDBCheck, RepasDUIts, MyListas;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmRepasDUCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmRepasDUCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrRepasCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmRepasDUCad.DefParams;
begin
  VAR_GOTOTABELA := 'Repas';
  VAR_GOTOMYSQLTABLE := QrRepas;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 1;
  VAR_GOTOVAR1 := ' Tipo=1';

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT CASE WHEN en.Tipo=0 THEN en.RazaoSocial');
  VAR_SQLx.Add('ELSE en.Nome END NOMECOLIGADO, en.FatorCompra, ');
  VAR_SQLx.Add('re.*, (re.Total-re.JurosV) SALDO');
  VAR_SQLx.Add('FROM repas re');
  VAR_SQLx.Add('LEFT JOIN entidades en ON en.Codigo=re.Coligado');
  VAR_SQLx.Add('WHERE re.Tipo=1');
  //
  VAR_SQL1.Add('AND re.Codigo=:P0');
  //
  VAR_SQLa.Add('AND (CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome END) LIKE :P0');
  //
end;

procedure TFmRepasDUCad.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible    := True;
      PainelEdita.Visible    := False;
      //
      CalculaLote(Codigo);
    end;
    1:
    begin
      PainelEdita.Visible    := True;
      PainelDados.Visible    := False;
      //
      if SQLType = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdColigado.Text     := '';
        CBColigado.KeyValue := NULL;
        TPData.Date         := Date;
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdColigado.Text     := IntToStr(QrRepasColigado.Value);
        CBColigado.KeyValue := QrRepasColigado.Value;
        TPData.Date         := QrRepasData.Value;
      end;
      EdColigado.SetFocus;
    end;
    2:
    begin
      FItens := True;
      if DBCheck.CriaFm(TFmRepasDUIts, FmRepasDUIts, afmoNegarComAviso) then
      begin
        FmRepasDUIts.EdTaxa.ValueVariant := QrRepasFatorCompra.Value +
          Dmod.ObtemExEntiMaior(QrRepasColigado.Value);
        //
        FmRepasDUIts.ShowModal;
        FmRepasDUIts.Destroy;
      end;
      FItens := False;
    end;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmRepasDUCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmRepasDUCad.AlteraRegistro;
var
  Repas : Integer;
begin
  Repas := QrRepasCodigo.Value;
  if QrRepasCodigo.Value = 0 then
  begin
    Application.MessageBox('N�o dados selecionados para editar', 'Erro',
      MB_OK+MB_ICONERROR);
    Exit;
  end;
  if not UMyMod.SelLockY(Repas, Dmod.MyDB, 'Repas', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(Repas, Dmod.MyDB, 'Repas', 'Codigo');
      MostraEdicao(1, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmRepasDUCad.IncluiRegistro;
var
  Cursor : TCursor;
  Repas : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    Repas := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'Repas', 'Repas', 'Codigo');
    if Length(FormatFloat(FFormatFloat, Repas))>Length(FFormatFloat) then
    begin
      Application.MessageBox(
      'Inclus�o cancelada. Limite de cadastros extrapolado', 'Erro',
      MB_OK+MB_ICONERROR);
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(1, stIns, Repas);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmRepasDUCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmRepasDUCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmRepasDUCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmRepasDUCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmRepasDUCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmRepasDUCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmRepasDUCad.BtReceitasClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMLote, BtReceitas);
end;

procedure TFmRepasDUCad.BtChequeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCheque, BtCheque);
end;

procedure TFmRepasDUCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrRepasCodigo.Value;
  Close;
end;

procedure TFmRepasDUCad.BtConfirmaClick(Sender: TObject);
var
  Coligado, Codigo : Integer;
begin
  Coligado := EdColigado.ValueVariant;
  if Coligado = 0 then
  begin
    Application.MessageBox('Defina o coligado!', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Codigo := EdCodigo.ValueVariant;
  Dmod.QrUpdU.SQL.Clear;
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('INSERT INTO repas SET Tipo=1, ')
  else Dmod.QrUpdU.SQL.Add('UPDATE repas SET ');
  Dmod.QrUpdU.SQL.Add('Coligado=:P0, Data=:P1, ');
  //
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else Dmod.QrUpdU.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  Dmod.QrUpdU.Params[00].AsInteger := Coligado;
  Dmod.QrUpdU.Params[01].AsString  := FormatDateTime(VAR_FORMATDATE, TPData.Date);
  //
  Dmod.QrUpdU.Params[02].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[03].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[04].AsInteger := Codigo;
  Dmod.QrUpdU.ExecSQL;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Repas', 'Codigo');
  FRepasIts := QrRepasItsControle.Value;
  MostraEdicao(0, stLok, 0);
  LocCod(Codigo,Codigo);
  //
  Progress.Max := QrRepasIts.RecordCount;
  Progress.Position := 0;
  Progress.Visible := True;
  QrRepasIts.First;
  while not QrRepasIts.Eof do
  begin
    Progress.Position := Progress.Position + 1;
    RecalculaCheque;
    QrRepasIts.Next;
  end;
  CalculaLote(Codigo);
  LocCod(Codigo,Codigo);
  Progress.Position := 0;
  Progress.Visible := False;
end;

procedure TFmRepasDUCad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'Repas', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Repas', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Repas', 'Codigo');
end;

procedure TFmRepasDUCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
  GradeRepas.Align  := alClient;
  LaRegistro.Align  := alClient;
  CriaOForm;
  //
  UMyMod.AbreQuery(QrColigado, Dmod.MyDB);
end;

procedure TFmRepasDUCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrRepasCodigo.Value,LaRegistro.Caption);
end;

procedure TFmRepasDUCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmRepasDUCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmRepasDUCad.QrRepasAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmRepasDUCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'Repas', 'Livres', 99) then
  //BtInclui.Enabled := False;
end;

procedure TFmRepasDUCad.QrRepasAfterScroll(DataSet: TDataSet);
begin
  ReopenRepasIts;
end;

procedure TFmRepasDUCad.SbQueryClick(Sender: TObject);
begin
  Application.CreateForm(TFmRepasLoc, FmRepasLoc);
  FmRepasLoc.ShowModal;
  FmRepasLoc.Destroy;
  if FmPrincipal.FLoteLoc <> 0 then
    LocCod(FmPrincipal.FLoteLoc, FmPrincipal.FLoteLoc);
end;

procedure TFmRepasDUCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmRepasDUCad.QrRepasBeforeOpen(DataSet: TDataSet);
begin
  QrRepasCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmRepasDUCad.ReopenRepasIts;
begin
{
  QrRepasIts.Close;
  QrRepasIts.Params[0].AsInteger := QrRepasCodigo.Value;
  QrRepasIts. Open;
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrRepasIts, Dmod.MyDB, [
  'SELECT li.Duplicata, li.Banco, li.Agencia, ',
  'li.Emitente, li.CNPJCPF, li.DDeposito, li.Vencimento, ',
  '(ri.Valor - ri.JurosV) LIQUIDO, ri.* ',
  'FROM repasits ri ',
  'LEFT JOIN ' + CO_TabLctA + ' li ON li.FatParcela=ri.Origem ',
  'WHERE li.FatID=' + Geral.FF0(VAR_FATID_0301),
  'AND ri.Codigo=' + Geral.FF0(QrRepasCodigo.Value),
  '']);
  //
  if FRepasIts <> 0 then QrRepasIts.Locate('Controle', FRepasIts, []);
end;

procedure TFmRepasDUCad.Incluinovolote1Click(Sender: TObject);
begin
  IncluiRegistro;
end;

procedure TFmRepasDUCad.Alteraloteatual1Click(Sender: TObject);
begin
  AlteraRegistro;
end;

procedure TFmRepasDUCad.Adicionachequeaoloteatual1Click(Sender: TObject);
begin
  MostraEdicao(2, stIns, 0);
end;

procedure TFmRepasDUCad.RecalculaCheque;
var
  Controle, Codigo, Origem, Dias: Integer;
  Valor, Taxa, JurosP, JurosV: Double;
begin
  Codigo := QrRepasCodigo.Value;
  Origem := QrRepasItsOrigem.Value;
  Taxa   := QrRepasItsTaxa.Value;
  Dias   := Trunc(int(QrRepasItsDDeposito.Value) - int(QrRepasData.Value));
  JurosP := MLAGeral.CalculaJuroComposto(Taxa, Dias);
  Valor  := QrRepasItsValor.Value;
  JurosV := (Trunc(Valor * JurosP))/100;
  //
  Controle := QrRepasItsControle.Value;
  Dmod.QrUpdU.SQL.Clear;
  Dmod.QrUpdU.SQL.Add('UPDATE repasits SET ');
  Dmod.QrUpdU.SQL.Add('Origem=:P0, Taxa=:P1, JurosP=:P2, JurosV=:P3, ');
  Dmod.QrUpdU.SQL.Add('Dias=:P4, Valor=:P5, ');
  //
  Dmod.QrUpdU.SQL.Add('DataAlt=:Pa, UserAlt=:Pb WHERE Codigo=:Pc AND Controle=:Pd');
  Dmod.QrUpdU.Params[00].AsInteger := Origem;
  Dmod.QrUpdU.Params[01].AsFloat   := Taxa;
  Dmod.QrUpdU.Params[02].AsFloat   := JurosP;
  Dmod.QrUpdU.Params[03].AsFloat   := JurosV;
  Dmod.QrUpdU.Params[04].AsInteger := Dias;
  Dmod.QrUpdU.Params[05].AsFloat   := Valor;
  //
  Dmod.QrUpdU.Params[06].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[07].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[08].AsInteger := Codigo;
  Dmod.QrUpdU.Params[09].AsInteger := Controle;
  Dmod.QrUpdU.ExecSQL;
  //
end;

procedure TFmRepasDUCad.ImportaItemDeLote(Taxa: Double);
var
  Controle, Codigo, Origem, Dias: Integer;
  Valor, JurosP, JurosV: Double;
begin
  if QrLIRepassado.Value > 0 then
  begin
    FItensBloqueados := FItensBloqueados + 1;
    Exit;
  end;
  FItensImportados := FItensImportados + 1;
  Codigo := QrRepasCodigo.Value;
  Origem := QrLIFatParcela.Value;
  Dias   := Trunc(int(QrLIDDeposito.Value) - int(QrRepasData.Value));
  JurosP := MLAGeral.CalculaJuroComposto(Taxa, Dias);
  Valor  := QrLICredito.Value;
  JurosV := (Trunc(Valor * JurosP))/100;
  //
  Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
  'RepasIts', 'RepasIts', 'Codigo');
  //
  Dmod.QrUpdU.SQL.Clear;
  Dmod.QrUpdU.SQL.Add('INSERT INTO repasits SET ');
  Dmod.QrUpdU.SQL.Add('Origem=:P0, Taxa=:P1, JurosP=:P2, JurosV=:P3, ');
  Dmod.QrUpdU.SQL.Add('Dias=:P4, Valor=:P5, ');
  Dmod.QrUpdU.SQL.Add('DataCad=:Pa, UserCad=:Pb, Codigo=:Pc, Controle=:Pd');
  //
  Dmod.QrUpdU.Params[00].AsInteger := Origem;
  Dmod.QrUpdU.Params[01].AsFloat   := Taxa;
  Dmod.QrUpdU.Params[02].AsFloat   := JurosP;
  Dmod.QrUpdU.Params[03].AsFloat   := JurosV;
  Dmod.QrUpdU.Params[04].AsInteger := Dias;
  Dmod.QrUpdU.Params[05].AsFloat   := Valor;
  //
  Dmod.QrUpdU.Params[06].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[07].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[08].AsInteger := Codigo;
  Dmod.QrUpdU.Params[09].AsInteger := Controle;
  Dmod.QrUpdU.ExecSQL;
  //
{
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE lot esits SET AlterWeb=1,   Repassado=1');
  Dmod.QrUpd.SQL.Add('WHERE Controle=:P0');
  Dmod.QrUpd.Params[00].AsInteger := Origem;
  Dmod.QrUpd.ExecSQL;
}
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_TabLctA, False,[
  'Repassado'], ['FatID', 'FatParcela'], [
  1], [VAR_FATID_0301, Origem], True);
  //
end;

procedure TFmRepasDUCad.CalculaLote(Lote: Integer);
begin
 QrSum.Close;
 QrSum.Params[0].AsInteger := lote;
 UMyMod.AbreQuery(QrSum, Dmod.MyDB);
 //
 Dmod.QrUpd.SQL.Clear;
 Dmod.QrUpd.SQL.Add('UPDATE repas SET Total=:P0, JurosV=:P1');
 Dmod.QrUpd.SQL.Add('WHERE Codigo=:Pa');
 Dmod.QrUpd.SQL.Add('');
 Dmod.QrUpd.Params[00].AsFloat   := QrSumValor.Value;
 Dmod.QrUpd.Params[01].AsFloat   := QrSumJurosV.Value;
 //
 Dmod.QrUpd.Params[02].AsInteger := Lote;
 Dmod.QrUpd.ExecSQL;
end;

procedure TFmRepasDUCad.Retirachequedoloteatual1Click(Sender: TObject);
begin
  if Application.MessageBox(PChar('Confirma a retirada da duplicata selecionada '+
  'do lote atual?'), 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
{
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE lot esits SET AlterWeb=1,   Repassado=0');
    Dmod.QrUpd.SQL.Add('WHERE Controle=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrRepasItsOrigem.Value;
    Dmod.QrUpd.ExecSQL;
}
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_TabLctA, False,[
    'Repassado'], ['FatID', 'FatParcela'], [
    0], [VAR_FATID_0301, QrRepasItsOrigem.Value], True);
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM repasits WHERE Controle=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrRepasItsControle.Value;
    Dmod.QrUpd.ExecSQL;
    //
    QrRepasIts.Next;
    FRepasIts := QrRepasItsControle.Value;
    CalculaLote(QrRepasCodigo.Value);
    LocCod(QrRepasCodigo.Value, QrRepasCodigo.Value);
  end;
end;

procedure TFmRepasDUCad.QrRepasItsCalcFields(DataSet: TDataSet);
begin
  QrRepasItsCPF_TXT.Value := Geral.FormataCNPJ_TT(QrRepasItsCNPJCPF.Value);
end;

procedure TFmRepasDUCad.BtImpCalculadoClick(Sender: TObject);
begin
  MyObjects.frxMostra(frxRepasIts, 'Repasse de duplicatas');
end;

procedure TFmRepasDUCad.BtImpPesquisaClick(Sender: TObject);
begin
  Application.CreateForm(TFmRepasImp, FmRepasImp);
  FmRepasImp.ShowModal;
  FmRepasImp.Destroy;
end;

procedure TFmRepasDUCad.QrRepasItsAfterOpen(DataSet: TDataSet);
begin
  if FItens then
    QrRepasIts.Last;
end;

procedure TFmRepasDUCad.BtImportarClick(Sender: TObject);
var
  Taxa: Double;
  Texto1, Texto2: String;
begin
  if Application.MessageBox(PChar('Todas duplicatas do border� a ser selecionado '+
  'ser�o incorporadas ao lote de repasse atual. Para incorporar o border� a ser '+
  'selecionado em um lote novo, desista desta opera��o e crie um lote antes '+
  'da importa��o. Deseja continuar assim mesmo?'), 'Aviso', MB_YESNOCANCEL+MB_ICONWARNING) = ID_YES then
  begin
    FmPrincipal.FLoteLoc := 0;
    Application.CreateForm(TFmLot0Loc, FmLot0Loc);
    FmLot0Loc.FFormCall := 1;
    FmLot0Loc.ShowModal;
    FmLot0Loc.Destroy;
    if FmPrincipal.FLoteLoc <> 0 then
    begin
{
      QrLI.Close;
      QrLI.Params[0].AsInteger := FmPrincipal.FLoteLoc;
      QrLI. Open;
}
      UnDmkDAC_PF.AbreMySQLQuery0(QrLI, Dmod.MyDB, [
      'SELECT lo.Tipo, li.Repassado, li.FatNum, li.FatParcela, ',
      'li.DDeposito, li.DCompra, li.Credito, li.Vencimento, ',
      'li.Banco, li.Agencia, li.Duplicata ',
      'FROM ' + CO_TabLctA + ' li ',
      'LEFT JOIN ' + CO_TabLotA + ' lo ON lo.Codigo=li.FatNum ',
      'WHERE li.FatID=' + Geral.FF0(VAR_FATID_0301),
      'AND lo.Codigo=' + Geral.FF0(FmPrincipal.FLoteLoc),
      '']);
      //
      if QrLI.RecordCount = 0 then
      begin
        Application.MessageBox('O border� selecionado n�o cont�m nenhuma duplicata!',
        'Erro', MB_OK+MB_ICONERROR);
        Exit;
      end else begin
        FItensBloqueados := 0;
        FItensImportados := 0;
        Taxa := MLAGeral.GetPercent(TfmGetPercent, FmGetPercent, 'Taxa de Repasse',
          'Taxa de repasse:', QrRepasFatorCompra.Value+
          Dmod.ObtemExEntiMaior(QrRepasColigado.Value), 6, siPositivo);
        while not QrLI.Eof do
        begin
          ImportaItemDeLote(Taxa);
          QrLI.Next;
        end;
        CalculaLote(QrRepasCodigo.Value);
        LocCod(QrRepasCodigo.Value, QrRepasCodigo.Value);
        FRepasIts := QrRepasItsControle.Value;
        case FItensImportados of
          0: Texto1 := 'Nenhuma duplicata foi importado!';
          1: Texto1 := 'Uma duplicata foi importada!';
          else Texto1 := IntToStr(FItensImportados)+ ' duplicatas foram importadas!';
        end;
        case FItensBloqueados of
          0: Texto2 := 'Nenhum duplicata foi descartada!';
          1: Texto2 := 'Uma duplicata foi descartada!';
          else Texto2 := IntToStr(FItensBloqueados)+ ' duplicatas foram '+
          'descartadas por j� estarem repassados!';
        end;
        Application.MessageBox(PChar(Texto1+Chr(13)+Chr(10)+Chr(13)+Chr(10)+
          Texto2), 'Aviso', MB_OK+MB_ICONWARNING);
      end;
    end;
  end;
end;

procedure TFmRepasDUCad.frxRepasItsGetValue(const VarName: String;
  var Value: Variant);
begin
  if VarName = 'VARF_QTD_CHEQUES' then Value := QrRepasIts.RecordCount;
end;

end.

