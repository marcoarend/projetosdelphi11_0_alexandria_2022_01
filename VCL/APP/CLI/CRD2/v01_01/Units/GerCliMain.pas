unit GerCliMain;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, frxClass, frxDBSet, Menus, UnDmkProcFunc, DmkDAC_PF, UnDmkEnums;

type
  TFmGerCliMain = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PMImprime: TPopupMenu;
    Selecionados1: TMenuItem;
    odos1: TMenuItem;
    frxOcorreuC: TfrxReport;
    QrClientes: TmySQLQuery;
    QrClientesNOMECLIENTE: TWideStringField;
    QrClientesCodigo: TIntegerField;
    QrClientesTAXA_COMPRA: TFloatField;
    QrClientesFatorCompra: TFloatField;
    DsClientes: TDataSource;
    frxDsClientes: TfrxDBDataset;
    QrPesq1: TmySQLQuery;
    QrPesq1TIPODOC: TWideStringField;
    QrPesq1NOMEOCORRENCIA: TWideStringField;
    QrPesq1Banco: TIntegerField;
    QrPesq1Agencia: TIntegerField;
    QrPesq1Cheque: TIntegerField;
    QrPesq1Duplicata: TWideStringField;
    QrPesq1Conta: TWideStringField;
    QrPesq1SALDO: TFloatField;
    QrPesq1Emissao: TDateField;
    QrPesq1DCompra: TDateField;
    QrPesq1Vencto: TDateField;
    QrPesq1DDeposito: TDateField;
    QrPesq1Emitente: TWideStringField;
    QrPesq1CPF: TWideStringField;
    QrPesq1Codigo: TIntegerField;
    QrPesq1Cliente: TIntegerField;
    QrPesq1DataO: TDateField;
    QrPesq1Ocorrencia: TIntegerField;
    QrPesq1Valor: TFloatField;
    QrPesq1LoteQuit: TIntegerField;
    QrPesq1TaxaB: TFloatField;
    QrPesq1TaxaP: TFloatField;
    QrPesq1TaxaV: TFloatField;
    QrPesq1Pago: TFloatField;
    QrPesq1DataP: TDateField;
    QrPesq1Data3: TDateField;
    QrPesq1Status: TSmallintField;
    QrPesq1Descri: TWideStringField;
    QrPesq1MoviBank: TIntegerField;
    QrPesq1Lk: TIntegerField;
    QrPesq1DataCad: TDateField;
    QrPesq1DataAlt: TDateField;
    QrPesq1UserCad: TIntegerField;
    QrPesq1UserAlt: TIntegerField;
    QrPesq1AlterWeb: TSmallintField;
    QrPesq1ATUALIZADO: TFloatField;
    QrPesq1DOCUM_TXT: TWideStringField;
    QrPesq1Ativo: TSmallintField;
    frxDsPesq1: TfrxDBDataset;
    QrOcorreu: TmySQLQuery;
    QrOcorreuTIPODOC: TWideStringField;
    QrOcorreuNOMEOCORRENCIA: TWideStringField;
    QrOcorreuCodigo: TIntegerField;
    QrOcorreuDataO: TDateField;
    QrOcorreuOcorrencia: TIntegerField;
    QrOcorreuValor: TFloatField;
    QrOcorreuLoteQuit: TIntegerField;
    QrOcorreuLk: TIntegerField;
    QrOcorreuDataCad: TDateField;
    QrOcorreuDataAlt: TDateField;
    QrOcorreuUserCad: TIntegerField;
    QrOcorreuUserAlt: TIntegerField;
    QrOcorreuTaxaP: TFloatField;
    QrOcorreuTaxaV: TFloatField;
    QrOcorreuPago: TFloatField;
    QrOcorreuDataP: TDateField;
    QrOcorreuTaxaB: TFloatField;
    QrOcorreuData3: TDateField;
    QrOcorreuStatus: TSmallintField;
    QrOcorreuSALDO: TFloatField;
    QrOcorreuATUALIZADO: TFloatField;
    QrOcorreuDOCUM_TXT: TWideStringField;
    QrOcorreuBanco: TIntegerField;
    QrOcorreuAgencia: TIntegerField;
    QrOcorreuCheque: TIntegerField;
    QrOcorreuDuplicata: TWideStringField;
    QrOcorreuConta: TWideStringField;
    QrOcorreuEmissao: TDateField;
    QrOcorreuDCompra: TDateField;
    QrOcorreuVencto: TDateField;
    QrOcorreuDDeposito: TDateField;
    QrOcorreuEmitente: TWideStringField;
    QrOcorreuCPF: TWideStringField;
    QrOcorreuCliente: TIntegerField;
    QrOcorreuDescri: TWideStringField;
    DsOcorreu: TDataSource;
    QrSumOc: TmySQLQuery;
    QrSumOcJuros: TFloatField;
    QrSumOcPago: TFloatField;
    QrLastOcor: TmySQLQuery;
    QrLastOcorData: TDateField;
    QrLastOcorFatParcela: TIntegerField;
    QrBco1: TmySQLQuery;
    QrBco1Nome: TWideStringField;
    QrPesq: TmySQLQuery;
    QrPesqBanco: TIntegerField;
    QrPesqDuplicata: TWideStringField;
    QrPesqCliente: TIntegerField;
    QrPesqEmitente: TWideStringField;
    QrPesqVencto: TDateField;
    QrOcorP: TmySQLQuery;
    DsOcorP: TDataSource;
    QrLocLote: TmySQLQuery;
    QrLocLoteCodigo: TIntegerField;
    QrLocLoteTipo: TSmallintField;
    PMIncluiOcor: TPopupMenu;
    Incluiocorrnciadecliente1: TMenuItem;
    IncluiocorrnciadeCheque1: TMenuItem;
    IncluiocorrnciadeDuplicata1: TMenuItem;
    N1: TMenuItem;
    IncluipagamentodeOcorrncia1: TMenuItem;
    PMAlteraOcor: TPopupMenu;
    Alteraocorrnciaselecionada1: TMenuItem;
    PMExcluiOcor: TPopupMenu;
    ExcluiOcorrnciaselecionada1: TMenuItem;
    N2: TMenuItem;
    Excluipagamentodeocorrnciaselecionado1: TMenuItem;
    PainelData: TPanel;
    DBGrid5: TDBGrid;
    Panel5: TPanel;
    DBGrid3_: TDBGrid;
    Panel6: TPanel;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EdVal: TdmkEdit;
    EdTxV: TdmkEdit;
    EdPag: TdmkEdit;
    EdSal: TdmkEdit;
    EdAtz: TdmkEdit;
    BtBordero: TBitBtn;
    BitBtn1: TBitBtn;
    PainelPesq: TPanel;
    Label75: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    CGTipo: TdmkCheckGroup;
    CkAbertas: TCheckBox;
    TPIni: TDateTimePicker;
    TPFim: TDateTimePicker;
    CkIni: TCheckBox;
    CkFim: TCheckBox;
    SbImprime: TBitBtn;
    GBCntrl: TGroupBox;
    Panel8: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtIncluiOcor: TBitBtn;
    Panel9: TPanel;
    BitBtn2: TBitBtn;
    QrOcorreuMoviBank: TIntegerField;
    QrOcorreuTpOcor: TSmallintField;
    QrPesqAgencia: TIntegerField;
    QrPesqConta: TWideStringField;
    QrPesqCheque: TIntegerField;
    QrOcorPOcorreu: TIntegerField;
    QrOcorPData: TDateField;
    QrOcorPFatParcela: TIntegerField;
    QrOcorPControle: TIntegerField;
    QrOcorPFatNum: TFloatField;
    QrOcorPMoraVal: TFloatField;
    QrOcorPPago: TFloatField;
    QrOcorreuLOIS: TIntegerField;
    QrOcorreuPlaGen: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure Selecionados1Click(Sender: TObject);
    procedure odos1Click(Sender: TObject);
    procedure QrPesq1CalcFields(DataSet: TDataSet);
    procedure QrOcorreuAfterOpen(DataSet: TDataSet);
    procedure QrOcorreuAfterScroll(DataSet: TDataSet);
    procedure QrOcorreuBeforeClose(DataSet: TDataSet);
    procedure QrOcorreuCalcFields(DataSet: TDataSet);
    procedure PMExcluiOcorPopup(Sender: TObject);
    procedure Incluiocorrnciadecliente1Click(Sender: TObject);
    procedure IncluiocorrnciadeCheque1Click(Sender: TObject);
    procedure IncluiocorrnciadeDuplicata1Click(Sender: TObject);
    procedure IncluipagamentodeOcorrncia1Click(Sender: TObject);
    procedure Alteraocorrnciaselecionada1Click(Sender: TObject);
    procedure ExcluiOcorrnciaselecionada1Click(Sender: TObject);
    procedure Excluipagamentodeocorrnciaselecionado1Click(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure CkAbertasClick(Sender: TObject);
    procedure BtBorderoClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure BtIncluiOcorClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CGTipoClick(Sender: TObject);
  private
    { Private declarations }
    procedure MostraGerCliOcor(SQLType: TSQLType);
    procedure MostraGerCliPgOc(SQLType: TSQLType);
    procedure ReciboDePagamentoDeOcorCheque();
    procedure ReciboDePagamentoDeOcorDuplic();
    procedure ReciboDePagamentoDeOcorClient();
    procedure ReopenLastOcor(Ocorreu: Integer);
    procedure ReopenOcorP();
    procedure ReopenPesq1(Comp: String);
  public
    { Public declarations }
    FDataLot: TDateTime;
    FCliente, FOcorP, FOcorreu: Integer;
    //
    procedure CalculaPagtoOcorP(Quitacao: TDateTime; OcorP: Integer);
    procedure ReopenOcorreu(Codigo: Integer);
  end;

  var
  FmGerCliMain: TFmGerCliMain;

implementation

uses UnMyObjects, Module, MyDBCheck, Principal, GerCliOcor, GerCliPgOc, UnGOTOy,
  MyListas, UMySQLModule, ModuleLot;

{$R *.DFM}

procedure TFmGerCliMain.Alteraocorrnciaselecionada1Click(Sender: TObject);
begin
  MostraGerCliOcor(stUpd);
end;

procedure TFmGerCliMain.BitBtn1Click(Sender: TObject);
begin
  if (QrOcorreu.State = dsBrowse) and (QrOcorP.State = dsBrowse)
  and (QrOcorP.RecordCount > 0) then
  begin
    if QrOcorreuTIPODOC.Value = 'CH' then ReciboDePagamentoDeOcorCheque() else
    if QrOcorreuTIPODOC.Value = 'DU' then ReciboDePagamentoDeOcorDuplic() else
                                          ReciboDePagamentoDeOcorClient();
  end else
  GOTOy.EmiteRecibo(0, Dmod.QrMasterDono.Value, 0, 0, 0, 0, '----000', '', '', '', Int(Date), 0);
end;

procedure TFmGerCliMain.BtAlteraClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAlteraOcor, BtAltera);
end;

procedure TFmGerCliMain.BtBorderoClick(Sender: TObject);
var
  Ocorreu: Integer;
begin
  Ocorreu := QrOcorreuCodigo.Value;
  FOcorP := QrOcorPFatParcela.Value;
  //
{
  QrLocLote.Close;
  QrLocLote.Params[0].AsInteger := QrOcorPFatNum.Value;
  QrLocLote. Open;
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocLote, Dmod.MyDB, [
  'SELECT Codigo, Tipo ',
  'FROM ' + CO_TabLotA,
  'WHERE Codigo=' + FormatFloat('0', QrOcorPFatNum.Value),
  '']);
  FmPrincipal.CriaFormLot(2, QrLocLoteTipo.Value, Trunc(QrOcorPFatNum.Value), 0);
  ReopenOcorreu(Ocorreu);
end;

procedure TFmGerCliMain.BtExcluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMExcluiOcor, BtExclui);
end;

procedure TFmGerCliMain.BtIncluiOcorClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIncluiOcor, BtIncluiOcor);
end;

procedure TFmGerCliMain.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGerCliMain.CalculaPagtoOcorP(Quitacao: TDateTime;
  OcorP: Integer);
var
  Sit: Integer;
begin
{
  QrSumOc.Close;
  QrSumOc.Params[0].AsInteger := Ocor rPg;
  UMyMod.AbreQuery(QrSumOc);
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumOC, Dmod.MyDB, [
  'SELECT SUM(MoraVal) Juros, SUM(Credito-Debito) Pago ',
  'FROM ' + CO_TabLctA,
  'WHERE FatID=' + TXT_VAR_FATID_0304,
  'AND Ocorreu=' + Geral.FF0(OcorP),
  '']);
  //
  Sit := 0;
  if QrSumOcPago.Value <> 0 then
  begin
    //Est� com erro - Atualizado em 09/08/2012
    //if (QrSumOcPago.Value < (QrOcorreuValor.Value + ==>QrOcorreuTaxaV.Value<==)-0.009)
    if (QrSumOcPago.Value < (QrOcorreuValor.Value + QrSumOcJuros.Value)-0.009) then
      Sit := 1
    else
      Sit := 2;
  end;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE ocorreu SET AlterWeb=1, ');
  Dmod.QrUpd.SQL.Add('TaxaV=:P0, Pago=:P1, Status=:P2, ');
  Dmod.QrUpd.SQL.Add('Data3=:P3 WHERE Codigo=:Pa');
  //
  Dmod.QrUpd.Params[00].AsFloat   := QrSumOcJuros.Value;
  Dmod.QrUpd.Params[01].AsFloat   := QrSumOcPago.Value;
  Dmod.QrUpd.Params[02].AsInteger := Sit;
  Dmod.QrUpd.Params[03].AsString  := FormatDateTime(VAR_FORMATDATE, Quitacao);
  Dmod.QrUpd.Params[04].AsInteger := OcorP;
  Dmod.QrUpd.ExecSQL;
  //
end;

procedure TFmGerCliMain.CGTipoClick(Sender: TObject);
begin
  if CGTipo.Value = 0 then
    CGTipo.Value := 7;
  ReopenOcorreu(0);
  PainelData.Visible := True;
end;

procedure TFmGerCliMain.CkAbertasClick(Sender: TObject);
begin
  ReopenOcorreu(0);
  PainelData.Visible := True;
end;

procedure TFmGerCliMain.EdClienteChange(Sender: TObject);
begin
  BtIncluiOcor.Enabled := EdCliente.ValueVariant <> 0;
  ReopenOcorreu(0);
  PainelData.Visible := True;
end;

procedure TFmGerCliMain.ExcluiOcorrnciaselecionada1Click(Sender: TObject);
begin
  if QrOcorP.RecordCount > 0 then
  begin
    Application.MessageBox('A ocorr�ncia selecionada n�o pode ser exclu�da '+
    'pois possui pagamento(s)!', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if Application.MessageBox('Confirma a exclus�o da ocorr�ncia selecionada?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM ocorreu WHERE Codigo=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrOcorreuCodigo.Value;
    Dmod.QrUpd.ExecSQL;
    QrOcorreu.Next;
    //
    ReopenOcorreu(QrOcorreuCodigo.Value);
  end;
end;

procedure TFmGerCliMain.Excluipagamentodeocorrnciaselecionado1Click(
  Sender: TObject);
var
  Ocorreu: Integer;
begin
  if QrOcorPFatNum.Value > 0 then
  begin
    Application.MessageBox(PChar('O pagamento da ocorr�ncia selecionada n�o '+
    'pode ser exclu�do pois foi paga em border�!. Para exclu�-lo acesse o '+
    'Border� correspondente.'), 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
{
  QrLastOcor.Close;
  QrLastOcor.Params[0].AsInteger := QrOcorPOcorreu.Value;
  UMyMod.AbreQuery(QrLastOcor);
}
  ReopenLastOcor(QrOcorPOcorreu.Value);
  //
  if (QrLastOcorData.Value > QrOcorPData.Value)
  or ((QrLastOcorData.Value = QrOcorPData.Value)
  and (QrLastOcorFatParcela.Value > QrOcorPFatParcela.Value))
  then
     Application.MessageBox('Somente o �ltimo pagamento pode ser exclu�do!',
    'Erro', MB_OK+MB_ICONERROR)
  else begin
    if Application.MessageBox('Confirma a exclus�o deste pagamento?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
    begin
      Screen.Cursor := crHourGlass;
      try
        Ocorreu := QrOcorPOcorreu.Value;
        DmLot.Exclui_OcorP(QrOcorPControle.Value, QrOcorPFatParcela.Value,
          QrOcorPOcorreu.Value);
{
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('DELETE FROM ocor rpg WHERE Codigo=:P0');
        Dmod.QrUpd.Params[0].AsInteger := QrOcorPFatParcela.Value;
        Dmod.QrUpd.ExecSQL;
}
        //
{
        QrLastOcor.Close;
        QrLastOcor.Params[0].AsInteger := Ocorreu;
        UMyMod.AbreQuery(QrLastOcor);
}
        ReopenLastOcor(Ocorreu);
        //
        CalculaPagtoOcorP(QrLastOcorData.Value, Ocorreu);
        //
        QrOcorP.Next;
        FOcorP := QrOcorPFatParcela.Value;
        ReopenOcorreu(QrOcorreuCodigo.Value);
        Screen.Cursor := crDefault;
      except
        Screen.Cursor := crDefault;
      end;
    end;
  end;
end;

procedure TFmGerCliMain.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FCliente > 0 then
  begin
    EdCliente.Text := IntToStr(FCliente);
    CBCliente.KeyValue := FCliente;
    //
    MostraGerCliOcor(stIns);
  end;
end;

procedure TFmGerCliMain.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  //
  FDataLot := Date;
  TPIni.Date := IncMonth(Int(Date), -1);
  TPFim.Date := Int(Date);
  UMyMod.AbreQuery(QrClientes, Dmod.MyDB);
end;

procedure TFmGerCliMain.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGerCliMain.IncluiocorrnciadeCheque1Click(Sender: TObject);
begin
// # # 2
end;

procedure TFmGerCliMain.Incluiocorrnciadecliente1Click(Sender: TObject);
begin
  MostraGerCliOcor(stIns);
end;

procedure TFmGerCliMain.IncluiocorrnciadeDuplicata1Click(Sender: TObject);
begin
// # # 3
end;

procedure TFmGerCliMain.IncluipagamentodeOcorrncia1Click(Sender: TObject);
begin
  MostraGerCliPgOc(stIns);
end;

procedure TFmGerCliMain.MostraGerCliOcor(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmGerCliOcor, FmGerCliOcor, afmoNegarComAviso) then
  begin
    FmGerCliOcor.ImgTipo.SQLType := SQLType;
    if SQLType = stIns then
    begin
      FmGerCliOcor.EdTaxaP.ValueVariant :=
        Dmod.ObtemTaxaDeCompraCliente(EdCliente.ValueVariant);
        FmGerCliOcor.TPDataO.Date := FDataLot;
    end else begin
      FmGerCliOcor.EdOcorrencia.ValueVariant := QrOcorreuOcorrencia.Value;
      FmGerCliOcor.CBOcorrencia.KeyValue := QrOcorreuOcorrencia.Value;
      FmGerCliOcor.TPDataO.Date := QrOcorreuDataO.Value;
      FmGerCliOcor.EdValor.ValueVariant := QrOcorreuValor.Value;
      FmGerCliOcor.EdTaxaP.ValueVariant := QrOcorreuTaxaP.Value;
      FmGerCliOcor.EdDescri.Text := QrOcorreuDescri.Value;
    end;
    //
    FmGerCliOcor.ShowModal;
    FmGerCliOcor.Destroy;
    if FCliente > 0 then Close;
  end;
end;

procedure TFmGerCliMain.MostraGerCliPgOc(SQLType: TSQLType);
begin
  if (SQLType <> stIns) and DmLot.AvisoDeNaoUpd(1) then Exit;
  //
  if DBCheck.CriaFm(TFmGerCliPgOc, FmGerCliPgOc, afmoNegarComAviso) then
  begin
    FOcorreu := QrOcorreuCodigo.Value;
    FmGerCliPgOc.ImgTipo.SQLType := SQLType;
    if SQLType = stIns then
    begin
      FmGerCliPgOc.ConfiguraPgOc(Int(Date));
      FmGerCliPgOc.CalculaJurosOcor();
      FmGerCliPgOc.CalculaAPagarOcor();
    end;
    FmGerCliPgOc.ShowModal;
    FmGerCliPgOc.Destroy;
    if FCliente > 0 then Close;
  end;
end;

procedure TFmGerCliMain.odos1Click(Sender: TObject);
begin
  frxDsPesq1.DataSet := QrOcorreu;
  MyObjects.frxMostra(frxOcorreuC, 'Relat�rio de Ocorr�ncias')
end;

procedure TFmGerCliMain.PMExcluiOcorPopup(Sender: TObject);
begin
  ExcluiOcorrnciaselecionada1.Enabled := Geral.IntToBool_0(QrOcorreu.RecordCount);
  Excluipagamentodeocorrnciaselecionado1.Enabled := Geral.IntToBool_0(QrOcorP.RecordCount);
end;

procedure TFmGerCliMain.QrOcorreuAfterOpen(DataSet: TDataSet);
var
  Val, TxV, Pag, Sal, Atz: Double;
begin
  if QrOcorreu.RecordCount = 0 then
  begin
    Alteraocorrnciaselecionada1.Enabled := False;
    BtExclui.Enabled := False;
  end else begin
    Alteraocorrnciaselecionada1.Enabled := True;
    BtExclui.Enabled := True;
  end;
  Val := 0;
  TxV := 0;
  Pag := 0;
  Sal := 0;
  Atz := 0;
  QrOcorreu.DisableControls;
  while not QrOcorreu.Eof do
  begin
    Val := Val + QrOcorreuValor.Value;
    TxV := TxV + QrOcorreuTaxaV.Value;
    Pag := Pag + QrOcorreuPago.Value;
    Sal := Sal + QrOcorreuSALDO.Value;
    Atz := Atz + QrOcorreuATUALIZADO.Value;
    QrOcorreu.Next;
  end;
  EdVal.Text := Geral.FFT(Val, 2, siNegativo);
  EdTxV.Text := Geral.FFT(TxV, 2, siNegativo);
  EdPag.Text := Geral.FFT(Pag, 2, siNegativo);
  EdSal.Text := Geral.FFT(Sal, 2, siNegativo);
  EdAtz.Text := Geral.FFT(Atz, 2, siNegativo);
  QrOcorreu.EnableControls;
end;

procedure TFmGerCliMain.QrOcorreuAfterScroll(DataSet: TDataSet);
begin
  ReopenOcorP();
end;

procedure TFmGerCliMain.QrOcorreuBeforeClose(DataSet: TDataSet);
begin
  EdVal.Text := '0,00';
  EdTxV.Text := '0,00';
  EdPag.Text := '0,00';
  EdSal.Text := '0,00';
  EdAtz.Text := '0,00';
  QrOcorP.Close;
end;

procedure TFmGerCliMain.QrOcorreuCalcFields(DataSet: TDataSet);
begin
  QrOcorreuSALDO.Value := QrOcorreuValor.Value + QrOcorreuTaxaV.Value - QrOcorreuPago.Value;
  //
  QrOcorreuATUALIZADO.Value := DMod.ObtemValorAtualizado(
    QrOcorreuCliente.Value, 1, QrOcorreuDataO.Value, Date, QrOcorreuData3.Value,
    QrOcorreuValor.Value, QrOcorreuTaxaV.Value, 0 (*Desco*),
    QrOcorreuPago.Value, QrOcorreuTaxaP.Value, False);
  //
  if QrOcorreuDescri.Value <> '' then
    QrOcorreuDOCUM_TXT.Value := QrOcorreuDescri.Value
  else if QrOcorreuTIPODOC.Value = 'CH' then
    QrOcorreuDOCUM_TXT.Value := FormatFloat('000', QrOcorreuBanco.Value)+'/'+
    FormatFloat('0000', QrOcorreuAgencia.Value)+'/'+ QrOcorreuConta.Value+'-'+
    FormatFloat('000000', QrOcorreuCheque.Value)
  else if QrOcorreuTIPODOC.Value = 'DU' then
    QrOcorreuDOCUM_TXT.Value := QrOcorreuDuplicata.Value
  else QrOcorreuDOCUM_TXT.Value := '';
end;

procedure TFmGerCliMain.QrPesq1CalcFields(DataSet: TDataSet);
begin
  QrPesq1SALDO.Value := QrPesq1Valor.Value + QrPesq1TaxaV.Value - QrPesq1Pago.Value;
  //
  QrPesq1ATUALIZADO.Value := DMod.ObtemValorAtualizado(
    QrPesq1Cliente.Value, 1, QrPesq1DataO.Value, Date, QrPesq1Data3.Value,
    QrPesq1Valor.Value, QrPesq1TaxaV.Value, 0 (*Desco*),
    QrPesq1Pago.Value, QrPesq1TaxaP.Value, False);
  //
  if QrPesq1Descri.Value <> '' then
    QrPesq1DOCUM_TXT.Value := QrPesq1Descri.Value
  else if QrPesq1TIPODOC.Value = 'CH' then
    QrPesq1DOCUM_TXT.Value := FormatFloat('000', QrPesq1Banco.Value)+'/'+
    FormatFloat('0000', QrPesq1Agencia.Value)+'/'+ QrPesq1Conta.Value+'-'+
    FormatFloat('000000', QrPesq1Cheque.Value)
  else if QrPesq1TIPODOC.Value = 'DU' then
    QrPesq1DOCUM_TXT.Value := QrPesq1Duplicata.Value
  else QrPesq1DOCUM_TXT.Value := '';
end;

procedure TFmGerCliMain.ReciboDePagamentoDeOcorCheque();
var
  Pagamento, Texto: String;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq, Dmod.MyDB, [
  'SELECT oc.* ',
  'FROM ocorreu oc ',
  'WHERE Codigo=' + FormatFloat('0', QrOcorreuCodigo.Value),
  '']);
  //
  QrBco1.Close;
  QrBco1.Params[0].AsInteger := QrPesqBanco.Value;
  UMyMod.AbreQuery(QrBco1, Dmod.MyDB);
  //
  Pagamento := MLAGeral.NomeStatusPgto6(QrOcorreuATUALIZADO.Value,
    QrOcorP.RecNo)+' da ocorr�ncia "'+ QrOcorreuNOMEOCORRENCIA.Value+
    '" cobrada sobre';
  Texto := Pagamento + ' o cheque n� ' + FormatFloat('000000', QrPesqCheque.Value)
    +' do banco n� '+FormatFloat('000', QrPesqBanco.Value) + ' ' +
    QrBco1Nome.Value+ ' ag�ncia n� '+ FormatFloat('0000', QrPesqAgencia.Value) +
    ' emitida por '+ QrPesqEmitente.Value + ' para o dia '+ FormatDateTime(
    'dd" de "mmmm" de "yyyy', QrPesqVencto.Value)+'. A quita��o desta '+
    'ocorr�ncia n�o quita o cheque';
  GOTOy.EmiteRecibo(QrPesqCliente.Value, Dmod.QrMasterDono.Value, CO_BENEFICIARIO_0,
    QrOcorPPago.Value, 0, 0, 'CHO-'+FormatFloat('000',
    QrOcorPFatParcela.Value), Texto, '', '', QrOcorPData.Value, 0);
end;

procedure TFmGerCliMain.ReciboDePagamentoDeOcorClient();
var
  Texto: String;
begin
  Texto := MLAGeral.NomeStatusPgto6(QrOcorreuATUALIZADO.Value,
    QrOcorP.RecNo)+' referente a ocorr�ncia "'+ QrOcorreuNOMEOCORRENCIA.Value;
  GOTOy.EmiteRecibo(QrOcorreuCliente.Value, Dmod.QrMasterDono.Value, CO_BENEFICIARIO_0,
    QrOcorPPago.Value, 0, 0, 'CLO-'+FormatFloat('000',
    QrOcorPFatParcela.Value), Texto, '', '', QrOcorPData.Value, 0);
end;

procedure TFmGerCliMain.ReciboDePagamentoDeOcorDuplic();
var
  Pagamento, Texto: String;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq, Dmod.MyDB, [
  'SELECT oc.* ',
  'FROM ocorreu oc ',
  'WHERE Codigo=' + FormatFloat('0', QrOcorreuCodigo.Value),
  '']);
  //
  Pagamento := MLAGeral.NomeStatusPgto6(QrOcorreuATUALIZADO.Value,
    QrOcorP.RecNo)+' da ocorr�ncia "'+ QrOcorreuNOMEOCORRENCIA.Value+
    '" cobrada sobre';
  Texto := Pagamento + ' a duplicata n� ' + QrPesqDuplicata.Value +
    ' emitida por '+ QrPesqEmitente.Value + ' para o dia '+ FormatDateTime(
    'dd" de "mmmm" de "yyyy', QrPesqVencto.Value)+'. A quita��o desta '+
    'ocorr�ncia n�o quita a duplicata';
  GOTOy.EmiteRecibo(QrPesqCliente.Value, Dmod.QrMasterDono.Value, CO_BENEFICIARIO_0,
    QrOcorPPago.Value, 0, 0, 'DUO-'+FormatFloat('000',
    QrOcorPFatParcela.Value), Texto, '', '', QrOcorPData.Value, 0);
end;

procedure TFmGerCliMain.ReopenOcorreu(Codigo: Integer);
var
  SQL_Xtra1, SQL_Xtra2, SQL_Xtra3: String;
begin
{
  QrOcorreu.Close;
  case CGTipo.Value of
    0: QrOcorreu.SQL[10] := '';
    1: QrOcorreu.SQL[10] := 'AND oc.Cliente > 0';
    2: QrOcorreu.SQL[10] := 'AND ((oc.Cliente = 0) AND (lo.Tipo=0))';
    3: QrOcorreu.SQL[10] := 'AND ((oc.Cliente > 0) OR  ((oc.Cliente = 0) AND (lo.Tipo=0)))';
    4: QrOcorreu.SQL[10] := 'AND ((oc.Cliente = 0) AND (lo.Tipo=1))';
    5: QrOcorreu.SQL[10] := 'AND ((oc.Cliente > 0) OR  ((oc.Cliente = 0) AND (lo.Tipo=1)))';
    6: QrOcorreu.SQL[10] := 'AND oc.Cliente = 0';
    7: QrOcorreu.SQL[10] := '';
    else QrOcorreu.SQL[10] := '';
  end;
  //
  if CkAbertas.Checked then QrOcorreu.SQL[11] := 'AND oc.Status<2'
  else QrOcorreu.SQL[11] := '';
  //
  QrOcorreu.SQL[12] := dmkPF.SQL_Periodo('AND oc.DataO',
    TPIni.Date, TPFim.Date, CkIni.Checked, CkFim.Checked);

  QrOcorreu.Params[00].AsInteger := QrClientesCodigo.Value;
  QrOcorreu.Params[01].AsInteger := QrClientesCodigo.Value;
  QrOcorreu. Open;
}
  case CGTipo.Value of
    0: SQL_Xtra1 := '';
    1: SQL_Xtra1 := 'AND oc.TpOcor = 3';
    2: SQL_Xtra1 := 'AND oc.TpOcor = 1';
    3: SQL_Xtra1 := 'AND oc.TpOcor IN (1,3)';
    4: SQL_Xtra1 := 'AND oc.TpOcor = 2';
    5: SQL_Xtra1 := 'AND oc.TpOcor IN (1,2)';
    6: SQL_Xtra1 := 'AND oc.TpOcor IN (2,3)';
    7: SQL_Xtra1 := '';
    else SQL_Xtra1 := '';
  end;
  if CkAbertas.Checked then SQL_Xtra2 := 'AND oc.Status<2'
  else SQL_Xtra2 := '';
  //
  SQL_Xtra3 := dmkPF.SQL_Periodo('AND oc.DataO',
    TPIni.Date, TPFim.Date, CkIni.Checked, CkFim.Checked);
  UnDmkDAC_PF.AbreMySQLQuery0(QrOcorreu, Dmod.MyDB, [
  'SELECT ELT(TpOcor, "CH", "DU", "CL", "??") TIPODOC, ',
  'ob.Nome NOMEOCORRENCIA, ob.PlaGen, ',
  'oc.' + FLD_LOIS + ' LOIS, oc.* ',
  'FROM ocorreu oc ',
  'LEFT JOIN ocorbank ob ON ob.Codigo = oc.Ocorrencia ',
  'WHERE oc.' + FLD_LOIS + '=0 AND oc.Cliente=' + FormatFloat('0', QrClientesCodigo.Value),
  SQL_Xtra1,
  SQL_Xtra2,
  SQL_Xtra3,
  '']);
  //
  if Codigo <> 0 then QrOcorreu.Locate('Codigo', Codigo, []);
end;

procedure TFmGerCliMain.ReopenLastOcor(Ocorreu: Integer);
begin
{
  QrLastOcor.Close;
  QrLastOcor.Params[0].AsInteger := QrOcorPOcorreu.Value;
  UMyMod.AbreQuery(QrLastOcor);
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrLastOcor, Dmod.MyDB, [
  'SELECT Data, FatParcela ',
  'FROM ' + CO_TabLctA,
  'WHERE FatID=' + TXT_VAR_FATID_0304,
  'AND Ocorreu=' + Geral.FF0(Ocorreu),
  'ORDER BY Data Desc, FatID Desc ',
  '']);
  //
end;

procedure TFmGerCliMain.ReopenOcorP();
begin
{
  QrOcor rPg.Close;
  QrOcor rPg.Params[0].AsInteger := QrOcorreuCodigo.Value;
  UMyMod.AbreQuery(QrOcor rPg);
  //
  if FOcor rPg <> 0 then QrOcor rPg.Locate('FatParcela', FOcor rPg, []);
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrOcorP, Dmod.MyDB, [
  'SELECT lct.Ocorreu, lct.Data, lct.FatParcela, ',
  'lct.Controle, lct.FatNum, lct.MoraVal, ',
  'lct.Credito - lct.Debito Pago ',
  'FROM ' + CO_TabLctA + ' lct ',
  'WHERE lct.FatID=' + TXT_VAR_FATID_0304,
  'AND lct.Ocorreu=' + Geral.FF0(QrOcorreuCodigo.Value),
  '']);
  //
  QrOcorP.Locate('FatParcela', FOcorP, []);
end;

procedure TFmGerCliMain.ReopenPesq1(Comp: String);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq1, Dmod.MyDB, [
  'SELECT ELT(TpOcor, "CH", "DU", "CL", "??") TIPODOC, ',
  'ob.Nome NOMEOCORRENCIA, ocr.* ',
  'FROM ocorreu ocr ',
  'LEFT JOIN ocorbank ob ON ob.Codigo   = ocr.Ocorrencia ',
  'WHERE ocr.Codigo > 0  ',
   Geral.ATS_if(Length(Comp) > 0, [
   'AND ocr.Codigo IN(' + Comp + ')']),
  '']);
end;

procedure TFmGerCliMain.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmGerCliMain.Selecionados1Click(Sender: TObject);
var
  Txt: String;
  i: Integer;
begin
  Txt := '';
  frxDsPesq1.DataSet := QrPesq1;
  //
  if DBGrid5.SelectedRows.Count > 1 then
  begin
    with DBGrid5.DataSource.DataSet do
    for i:= 0 to DBGrid5.SelectedRows.Count-1 do
    begin
      //GotoBookmark(pointer(DBGrid5.SelectedRows.Items[i]));
      GotoBookmark(DBGrid5.SelectedRows.Items[i]);
      if DBGrid5.SelectedRows.Count - 1 = i then
        Txt := Txt + IntToStr(QrOcorreuCodigo.Value)
      else
        Txt := Txt + IntToStr(QrOcorreuCodigo.Value) + ', '; 
    end;
  end else
      Txt := Txt + IntToStr(QrOcorreuCodigo.Value);
  ReopenPesq1(Txt);
  MyObjects.frxMostra(frxOcorreuC, 'Relat�rio de Ocorr�ncias');
end;

end.
