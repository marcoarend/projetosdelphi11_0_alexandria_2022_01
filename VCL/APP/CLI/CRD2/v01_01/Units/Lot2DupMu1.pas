unit Lot2DupMu1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Mask, UnDmkEnums;

type
  TFmLot2DupMu1 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtConfirma4: TBitBtn;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    Label1: TLabel;
    SpeedButton1: TSpeedButton;
    EdArquivo: TEdit;
    OpenDialog1: TOpenDialog;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Grade2: TStringGrid;
    TabSheet2: TTabSheet;
    Grade1: TStringGrid;
    BitBtn1: TBitBtn;
    PB1: TProgressBar;
    EdSeparador: TdmkEdit;
    Label2: TLabel;
    Label70: TLabel;
    dmkEdTPDescAte: TdmkEditDateTimePicker;
    Label67: TLabel;
    EdDMaisD: TdmkEdit;
    Label35: TLabel;
    EdTxaCompra4: TdmkEdit;
    Label204: TLabel;
    EdCartDep4: TdmkEditCB;
    CBCartDep4: TdmkDBLookupComboBox;
    Label92: TLabel;
    DBEdBanco4: TDBEdit;
    Label93: TLabel;
    DBEdAgencia4: TDBEdit;
    DBEdConta4: TDBEdit;
    Label95: TLabel;
    DBEdit23: TDBEdit;
    TPEmiss4: TdmkEditDateTimePicker;
    Label3: TLabel;
    TPData4: TdmkEditDateTimePicker;
    Label4: TLabel;
    TabSheet3: TTabSheet;
    GradeDU: TStringGrid;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BtConfirma4Click(Sender: TObject);
    procedure GradeDUKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure GradeDUDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
  private
    { Private declarations }
    FTotalCompraPer4: Double;
    //
    procedure CarregaItens();
    function  VerificaLinha(Linha: Integer; Parcela, Duplicata: String;
              Dia, Mes, Ano: Integer; Valor: Double; Doc: String): Integer;
    function  LocalizaSacado(Linha: Integer; Doc: String): Boolean;
    procedure CalculaDias(const Vcto: TDateTime;
              var Dias, Prazo: Integer);
    procedure CalculaValoresDuplicataAlterado(
              Valor: Double; Dias: Integer);
  public
    { Public declarations }
  end;

  var
  FmLot2DupMu1: TFmLot2DupMu1;

implementation

uses UnMyObjects, Module, ModuleLot, UMySQLModule, ModuleLot2, Lot2Cab,
  Principal, MyListas;

{$R *.DFM}

const
  FColIni = 5;

procedure TFmLot2DupMu1.BitBtn1Click(Sender: TObject);
{
var
  Erros: String;
}
begin
  MyObjects.Xls_To_StringGrid(Grade1, EdArquivo.Text, PB1, LaAviso1, LaAviso2);
  Grade2.ColCount := 20;
  CarregaItens();
end;

procedure TFmLot2DupMu1.BtConfirma4Click(Sender: TObject);
var
  Vence, Data_, Emiss_: TDateTime;
  Lote, i, z, Erros, DMais, Dias, Prazo, FatParcela, FatNum, Numero, Banco, Agencia,
  CartDep, Cliente, TipoCart, Carteira, Controle, Sub: Integer;
  Credito, Desco, Bruto: Double;
  Duplic, DDeposito, DCompra, Emissao, Vencimento, CNPJCPF, Sacado, Rua,
  CEP, Compl, Bairro, Cidade, UF, IE, DescAte: String;
  //
  J: integer;
begin
  if MyObjects.FIC( EdCartDep4.ValueVariant = 0, EdCartDep4,
  'Informe a carteira de dep�sito!') then
    Exit;
  //
  Screen.Cursor := crHourGlass;
  try
    PB1.Max := Grade2.RowCount -1;
    PB1.Position := 0;
    Update;
    Application.ProcessMessages;
    for J := 1 to Grade2.RowCount - 1 do
    begin
      PB1.Position := PB1.Position + 1;
      //CalculaValoresDuplicataAlterado(Valor);
      //Erros     := 0;
      Bruto     := Geral.DMV(Grade2.Cells[3, J]);
      Desco     := 0;
      DescAte   := Geral.FDT(dmkEdTPDescAte.Date, 1);
      Credito   := Bruto-Desco;
      Vence     := Geral.ValidaDataSimples(Grade2.Cells[2, J], True);
      Vencimento := FormatDateTime(VAR_FORMATDATE, Vence);
      Data_     := TPData4.Date;
      DCompra   := FormatDateTime(VAR_FORMATDATE, Data_);
      Emiss_    := TPEmiss4.Date;
      Emissao   := FormatDateTime(VAR_FORMATDATE, Emiss_);
      Duplic    := Grade2.Cells[1, J];
      CNPJCPF   := Geral.SoNumero_TT(Grade2.Cells[4, J]);
      IE        := Geral.SoNumero_TT(Grade2.Cells[5, J]);
      Sacado    := Grade2.Cells[FColIni + 1, J];
      Rua       := Grade2.Cells[FColIni + 2, J];
      Numero    := Geral.IMV(Grade2.Cells[FColIni + 3, J]);
      Compl     := Grade2.Cells[FColIni + 4, J];;
      Bairro    := Grade2.Cells[FColIni + 5, J];
      Cidade    := Grade2.Cells[FColIni + 6, J];
      CEP       := Geral.SoNumero_TT(Grade2.Cells[FColIni + 7, J]);
      UF        := Grade2.Cells[FColIni + 8, J];
      CartDep   := EdCartDep4.ValueVariant;
      // Manter informa��es de antes de 01/04/2007 no Lot esIts
      Banco     := Geral.IMV(DBEdBanco4.Text);
      Agencia   := Geral.IMV(DBEdAgencia4.Text);
      // F I M   Manter informa��es
      //
      Cliente   := FmLot2Cab.QrLotCliente.Value;
      Carteira  := FmLot2Cab.QrLotItsCarteira.Value;
      Controle  := FmLot2Cab.QrLotItsControle.Value;
      Sub       := FmLot2Cab.QrLotItsSub.Value;
      ///
      Geral.Valida_IE(IE, MLAGeral.GetCodigoUF_da_SiglaUF(UF), '??');
      //
      {
      z := 10;
      for i := 1 to z do
      begin
        case i of
          1: if Credito <= 0 then Erros := Erros +
            MLAGeral.MostraErroControle('Informe um valor v�lido!', EdValor4);
          2: if Bruto <= 0 then Erros := Erros +
            MLAGeral.MostraErroControle('Informe o valor!', EdValor4);
          3: if Vence < Data_ then Erros := Erros +
            MLAGeral.MostraErroControle('Prazo inv�lido!', EdVence4);
          4: if Duplic = '' then Erros := Erros +
            MLAGeral.MostraErroControle('Informe a duplicata!', EdDuplicata);
          5: if CNPJCPF = '' then Erros := Erros +
            MLAGeral.MostraErroControle('Informe o CNPJ!', EdCNPJ);
          6: if Sacado = '' then Erros := Erros +
            MLAGeral.MostraErroControle('Informe o Sacado!', EdSacado);
          7: if Rua = '' then Erros := Erros +
            MLAGeral.MostraErroControle('Informe o logradouro!', EdRua);
          8: if Cidade = '' then Erros := Erros +
            MLAGeral.MostraErroControle('Informe a Cidade!', EdCidade);
          9: if CEP = '' then Erros := Erros +
            MLAGeral.MostraErroControle('Informe o CEP!', EdCEP);
         10: if CartDep = 0 then Erros := Erros +
            MLAGeral.MostraErroControle('Informe a carteira de recebimento!', EdCartDep4);
        end;
        if Erros > 0 then
        begin
          Screen.Cursor := crDefault;
          Exit;
        end;
      end;
      }
      DMais := EdDMaisD.ValueVariant;
      CalculaDias(Vence, Dias, Prazo);
      CalculaValoresDuplicataAlterado(Bruto, Dias);
      DDeposito := FormatDateTime(VAR_FORMATDATE, UMyMod.CalculaDataDeposito(Vence));
      //
      FatNum := FmLot2Cab.QrLotCodigo.Value;
      if ImgTipo.SQLType = stUpd then
      begin
        FatParcela := FmLot2Cab.QrLotItsFatParcela.Value;
        TipoCart  := FmLot2Cab.QrLotItsTipo.Value;
        Carteira := FmLot2Cab.QrLotItsCarteira.Value;
        Controle := FmLot2Cab.QrLotItsControle.Value;
        Sub := FmLot2Cab.QrLotItsSub.Value;
      end else begin
        FatParcela := 0;
        TipoCart := 2;
        Carteira := CO_CARTEIRA_DUP_BORDERO;
        Controle := 0;
        Sub := 0;
      end;
      //
      FatParcela := FmLot2Cab.SQL_DU(FatNum, FatParcela, DMais, Dias, Banco,
                  Agencia, Credito, Desco, Bruto, Duplic, CNPJCPF, Sacado,
                  Vencimento, DCompra, DDeposito, Emissao, DescAte, ImgTipo.SQLType,
                  Cliente, CartDep, TipoCart, Carteira, Controle, Sub);
      //
      FmPrincipal.FControlIts := FatParcela;
      FmPrincipal.AtualizaSacado(CNPJCPF, Sacado, Rua, Numero, Compl, Bairro,
        Cidade, UF, CEP, Geral.SoNumero_TT(Grade2.Cells[FColIni + 9, J]), IE,
        '', -1, False);
      //
    end;
    if ImgTipo.SQLType <> stIns then
      Geral.MensagemBox('Tipo de Update de SQL n�o esperado!', 'ERRO', MB_OK+MB_ICONERROR)
    else begin
      Lote := FmLot2Cab.QrLotCodigo.Value;
      FmLot2Cab.CalculaLote(Lote, True);
      FmLot2Cab.LocCod(Lote, Lote);
      if Lote <> FmLot2Cab.QrLotCodigo.Value then
      begin
        Geral.MensagemBox('Erro no refresh do lote. O aplicativo ' +
        'dever� ser encerrado!', 'Erro', MB_OK+MB_ICONERROR);
        Application.Terminate;
      end;
    end;
    Screen.Cursor := crDefault;
    Close;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmLot2DupMu1.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLot2DupMu1.CalculaDias(const Vcto: TDateTime;
  var Dias, Prazo: Integer);
var
  Comp, DMai: Integer;
  Data: TDateTime;
begin
  Data := TPData4.Date;
  DMai := EdDMaisD.ValueVariant;
  Comp := 0;  // S� para cheques
  Dias := UMyMod.CalculaDias(Int(Data), Int(Vcto), DMai, Comp,
    Dmod.QrControleTipoPrazoDesc.Value, 0);
  Prazo := Trunc(Vcto-Int(Data));
  //
  //CalculaValoresDuplicataAlterado();
end;

procedure TFmLot2DupMu1.CalculaValoresDuplicataAlterado(
  Valor: Double; Dias: Integer);
var
  TaxaT, JuroT: Double;
  i: Integer;
begin
  FmLot2Cab.FTaxa[0] := Geral.DMV(EdTxaCompra4.Text);
  for i := 1 to GradeDU.RowCount -1 do FmLot2Cab.FTaxa[i] :=
    Geral.DMV(GradeDU.Cells[1, i]);
  // serar n�o setados
  for i := GradeDU.RowCount to 6 do FmLot2Cab.FTaxa[i] := 0;
  TaxaT := 0;
  for i := 0 to 6 do TaxaT := TaxaT + FmLot2Cab.FTaxa[i];
  //Valor := EdValor4.ValueVariant;
  //Dias := EdDias4.ValueVariant;
  //juros compostos ?
  JuroT   := MLAGeral.CalculaJuroComposto(TaxaT, Dias);
  //
  FTotalCompraPer4 := TaxaT;
  //
  (*for i := 0 to 6 do
  begin
    if TaxaT = 0 then FJuro[i] := 0 else
      FJuro[i] := FTaxa[i] / TaxaT * JuroT;
  end;
  for i := 0 to 6 do
      FValr[i] := FJuro[i] * Base / 100;*)
  //////////////////////////////////////////////////////////////////////////////
  if TaxaT > FmLot2Cab.FTaxa[0] then
  begin
    if Dmod.QrControleCalcMyJuro.Value = 1 then
    begin
      for i := 0 to 6 do
      begin
        if TaxaT = 0 then FmLot2Cab.FJuro[i] := 0 else
          FmLot2Cab.FJuro[i] := FmLot2Cab.FTaxa[i] / TaxaT * JuroT;
      end;
      for i := 0 to 6 do
          FmLot2Cab.FValr[i] := FmLot2Cab.FJuro[i] * Valor / 100;
    end else begin
      FmLot2Cab.FJuro[0] := MLAGeral.CalculaJuroComposto(FmLot2Cab.FTaxa[0], Dias);
      FmLot2Cab.FValr[0] := FmLot2Cab.FJuro[0] * Valor / 100;
      JuroT := JuroT - FmLot2Cab.FJuro[0];
      TaxaT := TaxaT - FmLot2Cab.FTaxa[0];
      for i := 1 to 6 do
      begin
        if TaxaT = 0 then FmLot2Cab.FJuro[i] := 0 else
          FmLot2Cab.FJuro[i] := FmLot2Cab.FTaxa[i] / TaxaT * JuroT;
      end;
      for i := 1 to 6 do
          FmLot2Cab.FValr[i] := FmLot2Cab.FJuro[i] * Valor / 100;
    end;
  end else begin
    FmLot2Cab.FJuro[0] := JuroT;
    FmLot2Cab.FValr[0] := JuroT * Valor / 100;
  end;
end;

procedure TFmLot2DupMu1.CarregaItens();
var
  I, K: Integer;
  Parcela, Duplicata, Doc: String;
  Dia, Mes, Ano: Integer;
  Valor: Double;
begin
  for I := 1 to Grade1.RowCount - 1 do
  begin
    //parcela	duplicata	dia	m�s	ano       	valor	 cpf
    //      1	       10 	5	  9	2012	   R$ 300,00 	027.431.909-81
    //
    Parcela := Grade1.Cells[1, I];
    Duplicata := Grade1.Cells[2, I];
    Dia:= Geral.IMV(Grade1.Cells[3, I]);
    Mes:= Geral.IMV(Grade1.Cells[4, I]);
    Ano:= Geral.IMV(Grade1.Cells[5, I]);
    Valor := Geral.DMV(Geral.SoNumeroEVirgula_TT(Grade1.Cells[6, I]));
    Doc := Grade1.Cells[7, I];
    K := VerificaLinha(I, Parcela, Duplicata, Dia, Mes, Ano, Valor, Doc);
    if K = 0 then
      Exit;
    //
    if K = 2 then
    begin
      Grade2.Cells[1, I] := Duplicata + EdSeparador.Text + Parcela;
      try
        Grade2.Cells[2, I] := Geral.FDT(EncodeDate(Ano, Mes, Dia), 3);
      except
        Geral.MensagemBox(
        'N�o foi poss�vel determinar a data de vencimento da linha: ' +
        Geral.FF0(I), 'Aviso', MB_OK+MB_ICONERROR);
        Exit;
      end;
      Grade2.Cells[3, I] := Geral.FFT(Valor, 2, siPositivo);
      Grade2.Cells[4, I] := Doc;
      //
      if not LocalizaSacado(I, Doc) then
        Exit;
    end;
  end;
  //

  PageControl1.ActivePageIndex := 2;
  BtConfirma4.Enabled := True;

  //
  MyObjects.LarguraAutomaticaGrade(Grade2);
end;

function TFmLot2DupMu1.VerificaLinha(Linha: Integer; Parcela, Duplicata: String;
  Dia, Mes, Ano: Integer; Valor: Double; Doc: String): Integer;
  function AvisaErro(Item: String): String;
  begin
    Result := 'A linha ' + Geral.FF0(Linha) + ' est� com informa��o inv�lida no campo: "' + Item;
  end;
begin
  if (Parcela <> '')
  or (Duplicata <> '')
  or (Dia <> 0)
  or (Mes <> 0)
  or (Ano <> 0)
  or (Valor <> 0)
  or (Doc <> '') then
  begin
    Result := 0;
    if MyObjects.FIC(Parcela = '', nil, AvisaErro('Parcela')) then Exit;
    if MyObjects.FIC(Duplicata = '', nil, AvisaErro('Duplicata')) then Exit;
    if MyObjects.FIC((Dia < 1) or (Dia > 31), nil, AvisaErro('Dia')) then Exit;
    if MyObjects.FIC((Mes < 1) or (Mes > 12), nil, AvisaErro('Mes')) then Exit;
    if MyObjects.FIC(Ano = 0, nil, AvisaErro('Ano')) then Exit;
    if MyObjects.FIC(Valor = 0, nil, AvisaErro('Valor')) then Exit;
    if MyObjects.FIC(Doc = '', nil, AvisaErro('Doc')) then Exit;
    //
    Result := 2;
  end else
    Result := 1;
end;

procedure TFmLot2DupMu1.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLot2DupMu1.FormCreate(Sender: TObject);
{
var
  I: Integer;
}
begin
  ImgTipo.SQLType := stIns;
  //
  Grade1.Cells[0, 0] := 'Lin';
  Grade1.Cells[1, 0] := 'Parcela';
  Grade1.Cells[2, 0] := 'Duplicata';
  Grade1.Cells[3, 0] := 'Dia';
  Grade1.Cells[4, 0] := 'Mes';
  Grade1.Cells[5, 0] := 'Ano';
  Grade1.Cells[6, 0] := 'Valor';
  Grade1.Cells[7, 0] := 'CNPJ / CPF';
  //
  //
  //
  Grade2.Cells[0, 0] := 'Lin';
  Grade2.Cells[1, 0] := 'Duplicata';
  Grade2.Cells[2, 0] := 'Vencto';
  Grade2.Cells[3, 0] := 'Valor';
  Grade2.Cells[4, 0] := 'CNPJ / CPF';
  Grade2.Cells[5, 0] := 'IE / RG';
  //
  Grade2.Cells[FColIni + 1, 0] := 'Nome';
  Grade2.Cells[FColIni + 2, 0] := 'Rua';
  Grade2.Cells[FColIni + 3, 0] := 'Numero';
  Grade2.Cells[FColIni + 4, 0] := 'Complemento';
  Grade2.Cells[FColIni + 5, 0] := 'Bairro';
  Grade2.Cells[FColIni + 6, 0] := 'Cidade';
  Grade2.Cells[FColIni + 7, 0] := 'CEP';
  Grade2.Cells[FColIni + 8, 0] := 'UF';
  Grade2.Cells[FColIni + 9, 0] := 'Tel1';
  //
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmLot2DupMu1.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLot2DupMu1.GradeDUDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  OldAlign, Color: Integer;
begin
  if GradeDU = nil then Exit;
  Color := clBlack;
  SetTextColor(GradeDU.Canvas.Handle, Color);
  if ARow = 0 then
  begin
    //
  end else if ACol = 0 then begin
    if ARow <> 0 then begin
      OldAlign := SetTextAlign(GradeDU.Canvas.Handle, TA_LEFT);
      GradeDU.Canvas.TextRect(Rect, Rect.Left+2, Rect.Top + 2,
        GradeDU.Cells[Acol, ARow]);
      SetTextAlign(GradeDU.Canvas.Handle, OldAlign);
    end;
  end else if ACol = 01 then begin
      OldAlign := SetTextAlign(GradeDU.Canvas.Handle, TA_RIGHT);
      GradeDU.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Geral.TFT(GradeDU.Cells[Acol, ARow], 6, siPositivo));
      SetTextAlign(GradeDU.Canvas.Handle, OldAlign);
  end //else if ACol = 02 then begin
end;

procedure TFmLot2DupMu1.GradeDUKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  //CalculaValoresDuplicataAlterado();
  if key=VK_RETURN then
  begin
    if GradeDU.EditorMode = True then
    begin
      if (GradeDU.Row < GradeDU.RowCount-1) then
        GradeDU.Row := GradeDU.Row +1
      else
        BtConfirma4.SetFocus;
    end;
  end
end;

function TFmLot2DupMu1.LocalizaSacado(Linha: Integer; Doc: String): Boolean;
{
var
  CIni, I: Integer;
}
begin
  //Result := False;
  Screen.Cursor := crHourGlass;
  try
    Grade2.RowCount := Linha + 1;
    //
    DmLot.QrLocSaca.Close;
    DmLot.QrLocSaca.Params[0].AsString := Geral.SoNumero_TT(Doc);
    UMyMod.AbreQuery(DmLot.QrLocSaca, Dmod.MyDB);
    if DmLot.QrLocSaca.RecordCount = 0 then
    begin
      Result := False;
    end else
    begin
      Grade2.Cells[FColIni + 1, Linha] := DmLot.QrLocSacaNome.Value;
      Grade2.Cells[FColIni + 2, Linha] := DmLot.QrLocSacaRua.Value;
      Grade2.Cells[FColIni + 3, Linha] := IntToStr(Trunc(DmLot.QrLocSacaNumero.Value));
      Grade2.Cells[FColIni + 4, Linha] := DmLot.QrLocSacaCompl.Value;
      Grade2.Cells[FColIni + 5, Linha] := DmLot.QrLocSacaBairro.Value;
      Grade2.Cells[FColIni + 6, Linha] := DmLot.QrLocSacaCidade.Value;
      Grade2.Cells[FColIni + 7, Linha] := Geral.FormataCEP_NT(DmLot.QrLocSacaCEP.Value);
      Grade2.Cells[FColIni + 8, Linha] := DmLot.QrLocSacaUF.Value;
      Grade2.Cells[FColIni + 9, Linha] := Geral.FormataTelefone_TT(DmLot.QrLocSacaTel1.Value);
      //
      Result := True;
    end;
    Screen.Cursor := crDefault;
  except
    Screen.Cursor := crDefault;
    raise;
  end;
  if Result = False then Geral.MensagemBox(
  'Importa��o cancelada! Sacado n�o localizado! ' +
  #13#10 + 'Cadastre o sacado "' + Doc + '" na janela especifica!' + #13#10 +
  'Janela Principal > Aba "Cadastros1" > Painel "Entidades" > Bot�o "Sacados',
  'Aviso', MB_OK+MB_ICONWARNING);
end;

procedure TFmLot2DupMu1.SpeedButton1Click(Sender: TObject);
begin
  if OpenDialog1.Execute then
    EdArquivo.Text := OpenDialog1.FileName;
end;

end.
