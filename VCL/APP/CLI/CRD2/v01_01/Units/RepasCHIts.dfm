object FmRepasCHIts: TFmRepasCHIts
  Left = 339
  Top = 185
  Caption = 'REP-GEREN-005 :: Adi'#231#227'o de Cheque a Repasse'
  ClientHeight = 630
  ClientWidth = 796
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 796
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 748
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 700
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 363
        Height = 32
        Caption = 'Adi'#231#227'o de Cheque a Repasse'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 363
        Height = 32
        Caption = 'Adi'#231#227'o de Cheque a Repasse'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 363
        Height = 32
        Caption = 'Adi'#231#227'o de Cheque a Repasse'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 796
    Height = 475
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 796
      Height = 475
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 796
        Height = 475
        Align = alClient
        TabOrder = 0
        object PainelItens: TPanel
          Left = 2
          Top = 15
          Width = 792
          Height = 458
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object GradeCHs: TDBGrid
            Left = 0
            Top = 88
            Width = 792
            Height = 370
            Align = alClient
            DataSource = DsCheques
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
            TabOrder = 1
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Lote'
                Title.Caption = 'Border'#244
                Width = 40
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Banco'
                Title.Caption = 'Bco.'
                Width = 27
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Agencia'
                Title.Caption = 'Ag.'
                Width = 31
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ContaCorrente'
                Title.Caption = 'Conta corrente'
                Width = 76
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Documento'
                Title.Caption = 'Cheque'
                Width = 49
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Credito'
                Title.Caption = 'Valor'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DCompra'
                Title.Caption = 'D. Compra'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DDeposito'
                Title.Caption = 'D. Dep'#243'sito'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CPF_TXT'
                Title.Caption = 'CPF / CNPJ'
                Width = 120
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Emitente'
                Width = 208
                Visible = True
              end>
          end
          object Panel5: TPanel
            Left = 0
            Top = 0
            Width = 792
            Height = 88
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object Label36: TLabel
              Left = 8
              Top = 4
              Width = 143
              Height = 13
              Caption = 'Leitura pela banda magn'#233'tica:'
            end
            object Label11: TLabel
              Left = 8
              Top = 44
              Width = 35
              Height = 13
              Caption = 'Cliente:'
            end
            object Label10: TLabel
              Left = 296
              Top = 4
              Width = 34
              Height = 13
              Caption = 'Banco:'
            end
            object Label12: TLabel
              Left = 340
              Top = 4
              Width = 42
              Height = 13
              Caption = 'Ag'#234'ncia:'
            end
            object Label13: TLabel
              Left = 392
              Top = 4
              Width = 31
              Height = 13
              Caption = 'Conta:'
            end
            object Label32: TLabel
              Left = 508
              Top = 4
              Width = 40
              Height = 13
              Caption = 'Cheque:'
            end
            object Label8: TLabel
              Left = 576
              Top = 4
              Width = 67
              Height = 13
              Caption = 'CPF Emitente:'
            end
            object Label31: TLabel
              Left = 592
              Top = 44
              Width = 79
              Height = 13
              Caption = 'Nome do banco:'
              FocusControl = DBEdit5
            end
            object Label14: TLabel
              Left = 368
              Top = 44
              Width = 54
              Height = 13
              Caption = 'Taxa [F12]:'
            end
            object Label15: TLabel
              Left = 320
              Top = 44
              Width = 40
              Height = 13
              Caption = 'Border'#244':'
            end
            object EdBanda: TdmkEdit
              Left = 8
              Top = 20
              Width = 285
              Height = 21
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              MaxLength = 34
              ParentFont = False
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdBandaChange
              OnKeyDown = EdBandaKeyDown
            end
            object EdCliente: TdmkEditCB
              Left = 8
              Top = 60
              Width = 45
              Height = 21
              Alignment = taRightJustify
              TabOrder = 6
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdClienteChange
              DBLookupComboBox = CBCliente
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBCliente: TdmkDBLookupComboBox
              Left = 56
              Top = 60
              Width = 261
              Height = 21
              Color = clWhite
              KeyField = 'Codigo'
              ListField = 'NOMECLIENTE'
              ListSource = DsClientes
              TabOrder = 7
              dmkEditCB = EdCliente
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdBanco: TdmkEdit
              Left = 296
              Top = 20
              Width = 41
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 3
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdBancoChange
              OnExit = EdBancoExit
            end
            object EdAgencia: TdmkEdit
              Left = 340
              Top = 20
              Width = 49
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 4
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnExit = EdAgenciaExit
            end
            object EdConta: TdmkEdit
              Left = 392
              Top = 20
              Width = 113
              Height = 21
              TabOrder = 3
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnExit = EdContaExit
            end
            object EdCheque: TdmkEdit
              Left = 508
              Top = 20
              Width = 65
              Height = 21
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 6
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '000000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnExit = EdChequeExit
            end
            object EdCPF: TdmkEdit
              Left = 576
              Top = 20
              Width = 113
              Height = 21
              TabOrder = 5
              FormatType = dmktfString
              MskType = fmtCPFJ
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnExit = EdCPFExit
            end
            object DBEdit5: TDBEdit
              Left = 588
              Top = 60
              Width = 193
              Height = 21
              TabStop = False
              DataField = 'Nome'
              DataSource = DsBanco
              TabOrder = 10
            end
            object EdTaxa: TdmkEdit
              Left = 368
              Top = 60
              Width = 65
              Height = 21
              Alignment = taRightJustify
              TabOrder = 9
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 6
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,000000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnKeyDown = EdTaxaKeyDown
            end
            object CkInap: TCheckBox
              Left = 436
              Top = 60
              Width = 149
              Height = 17
              Caption = 'Mostrar cheques vencidos.'
              TabOrder = 11
              Visible = False
              OnClick = CkInapClick
            end
            object EdBordero: TdmkEdit
              Left = 320
              Top = 60
              Width = 45
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 8
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnExit = EdBorderoExit
            end
            object MeAviso: TMemo
              Left = 692
              Top = 4
              Width = 89
              Height = 49
              Enabled = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Lines.Strings = (
                'Ctrl + Mouse '
                'seleciona mais'
                'de 1 cheque.')
              ParentFont = False
              ReadOnly = True
              TabOrder = 12
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 523
    Width = 796
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 792
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBConfirma: TGroupBox
    Left = 0
    Top = 567
    Width = 796
    Height = 63
    Align = alBottom
    TabOrder = 3
    object Panel1: TPanel
      Left = 686
      Top = 15
      Width = 108
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object BtDesiste: TBitBtn
        Tag = 15
        Left = 7
        Top = 2
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtDesisteClick
      end
    end
    object BtConfirma2: TBitBtn
      Tag = 14
      Left = 12
      Top = 15
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
      Caption = '&Confirma'
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtConfirma2Click
    end
    object BtExclui2: TBitBtn
      Tag = 12
      Left = 276
      Top = 15
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
      Caption = '&Exclui'
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
    end
  end
  object QrClientes: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMECLIENTE'
      'FROM entidades'
      'WHERE Cliente1="V"'
      'ORDER BY NOMECLIENTE')
    Left = 368
    Top = 205
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 396
    Top = 205
  end
  object QrCheques: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrChequesAfterOpen
    AfterClose = QrChequesAfterClose
    OnCalcFields = QrChequesCalcFields
    SQL.Strings = (
      'SELECT lo.Lote, li.* '
      'FROM lct0001a li '
      'LEFT JOIN lot0001a lo ON lo.Codigo=li.FatNum '
      'WHERE li.FatID=301'
      'AND lo.Tipo=0 '
      'AND li.DDeposito > "2009-08-25" '
      'AND li.Quitado = 0 '
      'AND li.Repassado = 0 '
      'AND lo.Cliente=2'
      ''
      'AND li.Banco=399'
      ''
      'AND li.Agencia=36'
      ''
      'AND li.ContaCorrente=0036288290'
      ''
      'AND li.Documento=912157'
      ''
      ''
      'AND lo.Cliente=2'
      ''
      'AND lo.Lote=1'
      '')
    Top = 8
    object QrChequesLote: TIntegerField
      FieldName = 'Lote'
    end
    object QrChequesCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrChequesFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrChequesFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrChequesFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrChequesFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrChequesEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
    object QrChequesBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrChequesAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrChequesContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrChequesCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrChequesDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrChequesDCompra: TDateField
      FieldName = 'DCompra'
    end
    object QrChequesDDeposito: TDateField
      FieldName = 'DDeposito'
    end
    object QrChequesCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Size = 40
      Calculated = True
    end
  end
  object DsCheques: TDataSource
    DataSet = QrCheques
    Left = 28
    Top = 8
  end
  object QrBanco: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM bancos'
      'WHERE Codigo=:P0')
    Left = 425
    Top = 205
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrBancoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrBancoNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrBancoLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrBancoDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrBancoDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrBancoUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrBancoUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrBancoSite: TWideStringField
      FieldName = 'Site'
      Size = 255
    end
  end
  object DsBanco: TDataSource
    DataSet = QrBanco
    Left = 453
    Top = 205
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 300
    OnTimer = Timer1Timer
    Left = 274
    Top = 313
  end
end
