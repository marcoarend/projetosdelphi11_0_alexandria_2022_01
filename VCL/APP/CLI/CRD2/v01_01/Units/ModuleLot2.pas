unit ModuleLot2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, Db,
  mySQLDbTables, frxClass, frxDBSet, UnMLAGeral, dmkGeral, dmkEdit, StdCtrls,
  DmkDAC_PF, UnDmkEnums;

type
  TDmLot2 = class(TDataModule)
    QrLocEmiBAC: TmySQLQuery;
    QrLocEmiBACBAC: TWideStringField;
    QrLocEmiBACCPF: TWideStringField;
    QrLocEmiCPF: TmySQLQuery;
    QrLCHs: TmySQLQuery;
    QrLCHsTipo: TIntegerField;
    QrLCHsOrdem: TIntegerField;
    QrLCHsDEmiss: TDateField;
    QrLCHsDCompra: TDateField;
    QrLCHsDVence: TDateField;
    QrLCHsDeposito: TDateField;
    QrLCHsComp: TIntegerField;
    QrLCHsBanco: TIntegerField;
    QrLCHsAgencia: TIntegerField;
    QrLCHsConta: TWideStringField;
    QrLCHsCPF: TWideStringField;
    QrLCHsEmitente: TWideStringField;
    QrLCHsCheque: TIntegerField;
    QrLCHsDuplicata: TWideStringField;
    QrLCHsValor: TFloatField;
    QrLCHsRua: TWideStringField;
    QrLCHsNumero: TIntegerField;
    QrLCHsCompl: TWideStringField;
    QrLCHsBirro: TWideStringField;
    QrLCHsCidade: TWideStringField;
    QrLCHsUF: TWideStringField;
    QrLCHsCEP: TIntegerField;
    QrLCHsTel1: TWideStringField;
    QrLCHsBAC: TWideStringField;
    QrLCHsDIAS: TFloatField;
    QrLCHsCPF_TXT: TWideStringField;
    QrLCHsSit: TIntegerField;
    QrLCHsMEU_SIT: TIntegerField;
    QrLCHsAberto: TFloatField;
    QrLCHsOcorrA: TFloatField;
    QrLCHsOcorrT: TFloatField;
    QrLCHsQtdeCH: TIntegerField;
    QrLCHsAberCH: TIntegerField;
    QrLCHsValAcum: TFloatField;
    QrLCHsIE: TWideStringField;
    QrLCHsDesco: TFloatField;
    QrLCHsBruto: TFloatField;
    QrLCHsDuplicado: TIntegerField;
    QrLCHsNOMEDUPLICADO: TWideStringField;
    QrLCHsPraca: TIntegerField;
    QrLCHsDisponiv: TFloatField;
    QrLCHsRISCO: TFloatField;
    DsLCHs: TDataSource;
    QrLDUs: TmySQLQuery;
    QrLDUsTipo: TIntegerField;
    QrLDUsOrdem: TIntegerField;
    QrLDUsDEmiss: TDateField;
    QrLDUsDCompra: TDateField;
    QrLDUsDVence: TDateField;
    QrLDUsDeposito: TDateField;
    QrLDUsComp: TIntegerField;
    QrLDUsBanco: TIntegerField;
    QrLDUsAgencia: TIntegerField;
    QrLDUsConta: TWideStringField;
    QrLDUsCPF: TWideStringField;
    QrLDUsEmitente: TWideStringField;
    QrLDUsCheque: TIntegerField;
    QrLDUsDuplicata: TWideStringField;
    QrLDUsValor: TFloatField;
    QrLDUsRua: TWideStringField;
    QrLDUsNumero: TIntegerField;
    QrLDUsCompl: TWideStringField;
    QrLDUsBirro: TWideStringField;
    QrLDUsCidade: TWideStringField;
    QrLDUsUF: TWideStringField;
    QrLDUsCEP: TIntegerField;
    QrLDUsTel1: TWideStringField;
    QrLDUsCPF_2: TWideStringField;
    QrLDUsNOME_2: TWideStringField;
    QrLDUsDIAS: TFloatField;
    QrLDUsCPF_TXT: TWideStringField;
    QrLDUsSit: TIntegerField;
    QrLDUsMEU_SIT: TIntegerField;
    QrLDUsAberto: TFloatField;
    QrLDUsOcorrA: TFloatField;
    QrLDUsOcorrT: TFloatField;
    QrLDUsAberCH: TIntegerField;
    QrLDUsQtdeCH: TIntegerField;
    QrLDUsRUA_2: TWideStringField;
    QrLDUsCOMPL_2: TWideStringField;
    QrLDUsBAIRRO_2: TWideStringField;
    QrLDUsCIDADE_2: TWideStringField;
    QrLDUsUF_2: TWideStringField;
    QrLDUsCEP_2: TIntegerField;
    QrLDUsTEL1_2: TWideStringField;
    QrLDUsTEL1_TXT: TWideStringField;
    QrLDUsTEL1_2_TXT: TWideStringField;
    QrLDUsCEP_TXT: TWideStringField;
    QrLDUsCEP_2_TXT: TWideStringField;
    QrLDUsNUMERO_2_TXT: TWideStringField;
    QrLDUsNUMERO_TXT: TWideStringField;
    QrLDUsIE: TWideStringField;
    QrLDUsIE_2: TWideStringField;
    QrLDUsDesco: TFloatField;
    QrLDUsBruto: TFloatField;
    QrLDUsValAcum: TFloatField;
    QrLDUsDuplicado: TIntegerField;
    QrLDUsNOMEDUPLICADO: TWideStringField;
    QrLDUsPraca: TIntegerField;
    QrLDUsDisponiv: TFloatField;
    QrLDUsRISCOEM: TFloatField;
    QrLDUsRISCOSA: TFloatField;
    QrLDUsRISCO: TFloatField;
    DsLDUs: TDataSource;
    QrEmitBAC: TmySQLQuery;
    QrEmitBACBAC: TWideStringField;
    QrEmitBACCPF: TWideStringField;
    QrValAcum: TmySQLQuery;
    QrValAcumValor: TFloatField;
    QrValAcumCPF: TWideStringField;
    QrValAcumAberto: TFloatField;
    QrValAcumRISCOEM: TFloatField;
    QrValAcumRISCOSA: TFloatField;
    QrLocEmiCPFCPF: TWideStringField;
    QrLocEmiCPFNome: TWideStringField;
    QrLocEmiCPFLimite: TFloatField;
    QrLocEmiCPFLastAtz: TDateField;
    QrLocEmiCPFAcumCHComV: TFloatField;
    QrLocEmiCPFAcumCHComQ: TIntegerField;
    QrLocEmiCPFAcumCHDevV: TFloatField;
    QrLocEmiCPFAcumCHDevQ: TIntegerField;
    QrLDUsBAC: TWideStringField;
    QrBanco: TmySQLQuery;
    QrBancoNome: TWideStringField;
    QrBancoDVCC: TSmallintField;
    DsBanco: TDataSource;
    QrSobraIni: TmySQLQuery;
    QrSobraIniData: TDateField;
    QrSobraIniCodigo: TIntegerField;
    QrOutros: TmySQLQuery;
    QrOutrosData: TDateField;
    QrOutrosCodigo: TIntegerField;
    QrOutrosLote: TIntegerField;
    frxOutros: TfrxReport;
    frxDsOutros: TfrxDBDataset;
    QrSobraIniSobraNow: TFloatField;
    QrOutrosSobraNow: TFloatField;
    Qrwlc: TmySQLQuery;
    QrwlcCodigo: TAutoIncField;
    QrwlcCliente: TIntegerField;
    QrwlcLote: TIntegerField;
    QrwlcTipo: TSmallintField;
    QrwlcData: TDateField;
    QrwlcTotal: TFloatField;
    QrwlcDias: TFloatField;
    QrwlcItens: TIntegerField;
    QrwlcBaixado: TIntegerField;
    QrwlcCPF: TWideStringField;
    Qrwli: TmySQLQuery;
    QrwliCodigo: TIntegerField;
    QrwliControle: TAutoIncField;
    QrwliComp: TIntegerField;
    QrwliPraca: TIntegerField;
    QrwliBanco: TIntegerField;
    QrwliAgencia: TIntegerField;
    QrwliConta: TWideStringField;
    QrwliCheque: TIntegerField;
    QrwliCPF: TWideStringField;
    QrwliEmitente: TWideStringField;
    QrwliBruto: TFloatField;
    QrwliDesco: TFloatField;
    QrwliValor: TFloatField;
    QrwliEmissao: TDateField;
    QrwliDCompra: TDateField;
    QrwliDDeposito: TDateField;
    QrwliVencto: TDateField;
    QrwliDias: TIntegerField;
    QrwliDuplicata: TWideStringField;
    QrwliUser_ID: TIntegerField;
    QrwliPasso: TIntegerField;
    QrwliRua: TWideStringField;
    QrwliNumero: TLargeintField;
    QrwliCompl: TWideStringField;
    QrwliBairro: TWideStringField;
    QrwliCidade: TWideStringField;
    QrwliUF: TWideStringField;
    QrwliCEP: TIntegerField;
    QrwliTel1: TWideStringField;
    QrwliIE: TWideStringField;
    QrSumLLC: TmySQLQuery;
    QrSumLLCTotal: TFloatField;
    QrLLC: TmySQLQuery;
    QrLLCCodigo: TIntegerField;
    QrLLCCliente: TIntegerField;
    QrLLCLote: TIntegerField;
    QrLLCTipo: TSmallintField;
    QrLLCData: TDateField;
    QrLLCTotal: TFloatField;
    QrLLCDias: TFloatField;
    QrLLCItens: TIntegerField;
    QrLLCBaixado: TIntegerField;
    QrLLCCPF: TWideStringField;
    QrLLCNOMECLI: TWideStringField;
    QrLLCNOMETIPO: TWideStringField;
    QrLLI: TmySQLQuery;
    QrLLICodigo: TIntegerField;
    QrLLIControle: TIntegerField;
    QrLLIComp: TIntegerField;
    QrLLIPraca: TIntegerField;
    QrLLIBanco: TIntegerField;
    QrLLIAgencia: TIntegerField;
    QrLLIConta: TWideStringField;
    QrLLICheque: TIntegerField;
    QrLLICPF: TWideStringField;
    QrLLIEmitente: TWideStringField;
    QrLLIBruto: TFloatField;
    QrLLIDesco: TFloatField;
    QrLLIValor: TFloatField;
    QrLLIEmissao: TDateField;
    QrLLIDCompra: TDateField;
    QrLLIDDeposito: TDateField;
    QrLLIVencto: TDateField;
    QrLLIDias: TIntegerField;
    QrLLIDuplicata: TWideStringField;
    QrLLIIE: TWideStringField;
    QrLLIRua: TWideStringField;
    QrLLINumero: TLargeintField;
    QrLLICompl: TWideStringField;
    QrLLIBairro: TWideStringField;
    QrLLICidade: TWideStringField;
    QrLLIUF: TWideStringField;
    QrLLICEP: TIntegerField;
    QrLLITel1: TWideStringField;
    QrLLIUser_ID: TIntegerField;
    QrLLIPasso: TIntegerField;
    DsLLC: TDataSource;
    QrSumLLCItens: TFloatField;
    QrUDLC: TmySQLQuery;
    QrVeri: TmySQLQuery;
    QrVeriControle: TIntegerField;
    QrCtrl: TmySQLQuery;
    QrCtrlCodigo: TIntegerField;
    QrCtrlControle: TAutoIncField;
    QrLocSacados: TmySQLQuery;
    QrLocSacadosCNPJ: TWideStringField;
    QrLocSacadosIE: TWideStringField;
    QrLocSacadosNome: TWideStringField;
    QrLocSacadosRua: TWideStringField;
    QrLocSacadosNumero: TLargeintField;
    QrLocSacadosCompl: TWideStringField;
    QrLocSacadosBairro: TWideStringField;
    QrLocSacadosCidade: TWideStringField;
    QrLocSacadosUF: TWideStringField;
    QrLocSacadosCEP: TIntegerField;
    QrLocSacadosTel1: TWideStringField;
    QrLocSacadosRisco: TFloatField;
    QrLocSacadosAlterWeb: TSmallintField;
    QrLocSacadosAtivo: TSmallintField;
    QrLCHsCPF_2: TWideStringField;
    QrLCHsNome_2: TWideStringField;
    QrLCHsRISCOEM: TFloatField;
    QrLCHsRISCOSA: TFloatField;
    QrLCHsRua_2: TWideStringField;
    QrLCHsNumero_2: TIntegerField;
    QrLCHsCompl_2: TWideStringField;
    QrLCHsIE_2: TWideStringField;
    QrLCHsBairro_2: TWideStringField;
    QrLCHsCidade_2: TWideStringField;
    QrLCHsUF_2: TWideStringField;
    QrLCHsCEP_2: TIntegerField;
    QrLCHsTEL1_2: TWideStringField;
    QrLDUsNumero_2: TIntegerField;
    QrLCHsStatusSPC: TSmallintField;
    QrLDUsStatusSPC: TSmallintField;
    QrLDUsTEXTO_SPC: TWideStringField;
    QrLCHsTEXTO_SPC: TWideStringField;
    QrSPC_Cfg: TmySQLQuery;
    QrSPC_CfgSPC_ValMin: TFloatField;
    QrSPC_CfgSPC_ValMax: TFloatField;
    QrSPC_CfgServidor: TWideStringField;
    QrSPC_CfgSPC_Porta: TSmallintField;
    QrSPC_CfgPedinte: TWideStringField;
    QrSPC_CfgCodigSocio: TIntegerField;
    QrSPC_CfgModalidade: TIntegerField;
    QrSPC_CfgInfoExtra: TIntegerField;
    QrSPC_CfgBAC_CMC7: TSmallintField;
    QrSPC_CfgTipoCred: TSmallintField;
    QrSPC_CfgSenhaSocio: TWideStringField;
    QrSPC_CfgCodigo: TIntegerField;
    QrSPC_CfgValAviso: TFloatField;
    QrSPC_CfgVALCONSULTA: TFloatField;
    QrSCHs: TmySQLQuery;
    QrSCHsValor: TFloatField;
    QrSCHsItens: TLargeintField;
    DsSCHs: TDataSource;
    DsSDUs: TDataSource;
    QrSDUs: TmySQLQuery;
    QrSDUsValor: TFloatField;
    QrSDUsItens: TLargeintField;
    QrSDUsTOTAL_V: TFloatField;
    QrSDUsTOTAL_Q: TFloatField;
    QrILd: TmySQLQuery;
    QrILdCPF: TWideStringField;
    QrUpdLocDB: TmySQLQuery;
    QrLot: TmySQLQuery;
    QrLotCodigo: TIntegerField;
    QrLotTipo: TSmallintField;
    QrLotCliente: TIntegerField;
    QrLotLote: TSmallintField;
    QrLotData: TDateField;
    QrLotTotal: TFloatField;
    QrLotDias: TFloatField;
    QrLotPeCompra: TFloatField;
    QrLotTxCompra: TFloatField;
    QrLotAdValorem: TFloatField;
    QrLotIOC: TFloatField;
    QrLotCPMF: TFloatField;
    QrLotLk: TIntegerField;
    QrLotDataCad: TDateField;
    QrLotDataAlt: TDateField;
    QrLotUserCad: TIntegerField;
    QrLotUserAlt: TIntegerField;
    QrLotNOMECLIENTE: TWideStringField;
    QrLotTipoAdV: TIntegerField;
    QrLotADVALOREM_TOTAL: TFloatField;
    QrLotVALVALOREM_TOTAL: TFloatField;
    QrLotValValorem: TFloatField;
    QrLotSpread: TSmallintField;
    QrLotTAXAVALOR_TOTAL: TFloatField;
    QrLotTAXAPERCE_TOTAL: TFloatField;
    QrLotTAXAPERCE_PROPR: TFloatField;
    QrLotIOC_VAL: TFloatField;
    QrLotCPMF_VAL: TFloatField;
    QrLotSUB_TOTAL: TFloatField;
    QrLotVAL_LIQUIDO: TFloatField;
    QrLotTOT_ISS: TFloatField;
    QrLotTOT_IRR: TFloatField;
    QrLotISS: TFloatField;
    QrLotISS_Val: TFloatField;
    QrLotPIS_R: TFloatField;
    QrLotPIS_R_Val: TFloatField;
    QrLotIRRF: TFloatField;
    QrLotIRRF_Val: TFloatField;
    QrLotVAL_LIQUIDO_MEU: TFloatField;
    QrLotSUB_TOTAL_MEU: TFloatField;
    QrLotCNPJCPF: TWideStringField;
    QrLotCNPJ_CPF_TXT: TWideStringField;
    QrLotTAXA_AM_TXT: TWideStringField;
    QrLotADVAL_TXT: TWideStringField;
    QrLotTarifas: TFloatField;
    QrLotOcorP: TFloatField;
    QrLotSUB_TOTAL_2: TFloatField;
    QrLotCOFINS_R: TFloatField;
    QrLotCOFINS_R_Val: TFloatField;
    QrLotPIS: TFloatField;
    QrLotCOFINS: TFloatField;
    QrLotMEU_PISCOFINS_VAL: TFloatField;
    QrLotPIS_Val: TFloatField;
    QrLotCOFINS_Val: TFloatField;
    QrLotMEU_PISCOFINS: TFloatField;
    QrLotMaxVencto: TDateField;
    QrLotSUB_TOTAL_3: TFloatField;
    QrLotCHDevPg: TFloatField;
    QrLotMINTC: TFloatField;
    QrLotMINAV: TFloatField;
    QrLotMINTC_AM: TFloatField;
    QrLotPIS_T_Val: TFloatField;
    QrLotCOFINS_T_Val: TFloatField;
    QrLotPgLiq: TFloatField;
    QrLotA_PG_LIQ: TFloatField;
    QrLotDUDevPg: TFloatField;
    QrLotNOMETIPOLOTE: TWideStringField;
    QrLotNF: TIntegerField;
    QrLotTAXAPERCEMENSAL: TFloatField;
    QrLotItens: TIntegerField;
    QrLotConferido: TIntegerField;
    QrLotECartaSac: TSmallintField;
    QrLotNOMECOFECARTA: TWideStringField;
    QrLotCAPTIONCONFCARTA: TWideStringField;
    QrLotNOMESPREAD: TWideStringField;
    QrLotLimiCred: TFloatField;
    QrLotCBECli: TIntegerField;
    QrLotSCBCli: TIntegerField;
    QrLotCBE: TIntegerField;
    QrLotSCB: TIntegerField;
    QrLotSALDOAPAGAR: TFloatField;
    QrLotQuantN1: TFloatField;
    QrLotQuantI1: TIntegerField;
    QrLotAllQuit: TSmallintField;
    QrLotSobraIni: TFloatField;
    QrLotSobraNow: TFloatField;
    QrLotIOC_VALNEG: TFloatField;
    QrLotCPMF_VALNEG: TFloatField;
    QrLotTAXAVALOR_TOTALNEG: TFloatField;
    QrLotVAVALOREM_TOTALNEG: TFloatField;
    QrLotCHDevPgNEG: TFloatField;
    QrLotDUDevPgNEG: TFloatField;
    QrLotIOFd_VAL: TFloatField;
    QrLotIOFv_VAL: TFloatField;
    QrLotNOMEFANCLIENTE: TWideStringField;
    QrLotWebCod: TIntegerField;
    QrLotAlterWeb: TSmallintField;
    QrLotIOFd: TFloatField;
    QrLotIOFv: TFloatField;
    QrLotTipoIOF: TSmallintField;
    QrLotAtivo: TSmallintField;
    QrEmLot1: TmySQLQuery;
    QrEmLot1IRRF: TFloatField;
    QrEmLot1IRRF_Val: TFloatField;
    QrEmLot1ISS: TFloatField;
    QrEmLot1ISS_Val: TFloatField;
    QrEmLot1PIS_R: TFloatField;
    QrEmLot1PIS_R_Val: TFloatField;
    QrEmLot1TaxaVal: TFloatField;
    QrEmLot1ValValorem: TFloatField;
    QrEmLot1Codigo: TIntegerField;
    QrEmLot1AdValorem: TFloatField;
    QrEmLot2: TmySQLQuery;
    QrEmLot2IRRF: TFloatField;
    QrEmLot2IRRF_Val: TFloatField;
    QrEmLot2ISS: TFloatField;
    QrEmLot2ISS_Val: TFloatField;
    QrEmLot2PIS_R: TFloatField;
    QrEmLot2PIS_R_Val: TFloatField;
    QrEmLot2TaxaVal: TFloatField;
    QrEmLot2ValValorem: TFloatField;
    QrEmLot2Codigo: TIntegerField;
    QrEmLot2AdValorem: TFloatField;
    QrEmLot3: TmySQLQuery;
    QrEmLot3IRRF: TFloatField;
    QrEmLot3IRRF_Val: TFloatField;
    QrEmLot3ISS: TFloatField;
    QrEmLot3ISS_Val: TFloatField;
    QrEmLot3PIS_R: TFloatField;
    QrEmLot3PIS_R_Val: TFloatField;
    QrEmLot3TaxaVal: TFloatField;
    QrEmLot3ValValorem: TFloatField;
    QrEmLot3Codigo: TIntegerField;
    QrEmLot3AdValorem: TFloatField;
    QrEmLot4: TmySQLQuery;
    QrEmLot4IRRF: TFloatField;
    QrEmLot4IRRF_Val: TFloatField;
    QrEmLot4ISS: TFloatField;
    QrEmLot4ISS_Val: TFloatField;
    QrEmLot4PIS_R: TFloatField;
    QrEmLot4PIS_R_Val: TFloatField;
    QrEmLot4TaxaVal: TFloatField;
    QrEmLot4ValValorem: TFloatField;
    QrEmLot4Codigo: TIntegerField;
    QrEmLot4AdValorem: TFloatField;
    QrEmLot5: TmySQLQuery;
    QrEmLot5IRRF: TFloatField;
    QrEmLot5IRRF_Val: TFloatField;
    QrEmLot5ISS: TFloatField;
    QrEmLot5ISS_Val: TFloatField;
    QrEmLot5PIS_R: TFloatField;
    QrEmLot5PIS_R_Val: TFloatField;
    QrEmLot5TaxaVal: TFloatField;
    QrEmLot5ValValorem: TFloatField;
    QrEmLot5Codigo: TIntegerField;
    QrEmLot5AdValorem: TFloatField;
    QrEmLot6: TmySQLQuery;
    QrEmLot6IRRF: TFloatField;
    QrEmLot6IRRF_Val: TFloatField;
    QrEmLot6ISS: TFloatField;
    QrEmLot6ISS_Val: TFloatField;
    QrEmLot6PIS_R: TFloatField;
    QrEmLot6PIS_R_Val: TFloatField;
    QrEmLot6TaxaVal: TFloatField;
    QrEmLot6ValValorem: TFloatField;
    QrEmLot6Codigo: TIntegerField;
    QrEmLot6AdValorem: TFloatField;
    QrIDLct: TmySQLQuery;
    QrIDLctCredito: TFloatField;
    QrIDLctControle: TIntegerField;
    QrIDLctFatID: TIntegerField;
    QrIDLctFatNum: TFloatField;
    QrIDLctFatParcela: TIntegerField;
    QrLotLctsAuto: TIntegerField;
    procedure QrLCHsCalcFields(DataSet: TDataSet);
    procedure QrLCHsAfterScroll(DataSet: TDataSet);
    procedure QrLDUsCalcFields(DataSet: TDataSet);
    procedure QrLDUsAfterScroll(DataSet: TDataSet);
    procedure QrSCHsBeforeClose(DataSet: TDataSet);
    procedure QrSCHsAfterOpen(DataSet: TDataSet);
    procedure QrSDUsAfterOpen(DataSet: TDataSet);
    procedure QrSDUsBeforeClose(DataSet: TDataSet);
    procedure QrLotCalcFields(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure QrLotBeforeClose(DataSet: TDataSet);
  private
    { Private declarations }
    FArMySQLLO: array[0..5] of TmySQLQuery;
    procedure ReopenLot(Lote: String);
  public
    { Public declarations }
    FDataTransacao: TDateTime;
    FORDA_LCHs, FORDA_LDUs, FORDB_LCHs,FORDB_LDUs: String;
    procedure ReopenBanco(Banco: Integer);
    procedure AtualizaEmitente(Banco, Agencia, Conta, CPF, Nome:
              String; Risco: Double);
    function BaixaLotWeb(): Integer;
    function ConectaBDn(Pergunta: Boolean): Boolean;
    procedure ReopenLLoteCab(Status: Integer);
    procedure AlterarNaWeb_Lote(Lote: Integer);
    //procedure VerificaControle(Codigo, Controle: Integer);
    procedure VerificaCPFCNP_XX_Importacao(NomeEnt: String; Entidade, Pagina:
              Integer; SelType: TSelType);
    procedure VerificaCPFCNP_CH_Importacao(SelType: TSelType);
    procedure VerificaCPFCNP_DU_Importacao(SelType: TSelType);
    procedure ReopenImportLote(CH, DU: Integer);
    procedure ReopenOutros(Cliente: Integer; Data: String; Lote: Double);
    procedure ReopenSobraIni(Cliente: Integer; Data: String; Lote: Double);
    function  DataTPCheque(): TDateTime;
    procedure HabilitaControle(Controle0, Controle1: TControl; Habilita: Boolean);
    procedure HabilitaBotoes(Meu_Sit, TipoDoc: Integer);
    function  LocalizaEmitente2(MudaNome: Boolean; B, A, C: String;
              DVCC: Integer; EdNome, EdCPF, EdRealCC: TdmkEdit): Boolean;
    function  CriaBAC_Pesq(Banco, Agencia, Conta: String; DVCC: Integer; EdRealCC: TdmkEdit): String;
    function  LocalizaSacado(MudaNome: Boolean; EdCNPJ, EdSacado, EdRua,
              EdNumero, EdCompl, EdBairro, EdCidade, EdCEP, EdTel1, EdEmail: TdmkEdit;
              CBUF: TComboBox): Boolean;
    function  ObtemEmitente(Banco, Agencia, Conta: String; var Emitente, CPF:
              String; AvisaNaoEncontrado: Boolean; EdRealCC: TdmkEdit): Boolean;
    procedure LancamentosAutomaticosBordero(Lote: Integer);
  end;

var
  DmLot2: TDmLot2;

implementation

{$R *.DFM}

uses UnMyObjects, Module, UnitSPC, UnInternalConsts, ModuleLot, Principal, UCreate,
  Lot2Cab, Lot2Inn, UMySQLModule, MyListas, ModuleFin;

procedure TDmLot2.HabilitaBotoes(Meu_Sit, TipoDoc: Integer);
begin
  case FmPrincipal.FFormLotShow of
    2: FmLot2Inn.HabilitaBotoes(Meu_Sit, TipoDoc);
    else Application.MessageBox(PChar('Janela de gerenciamento de border�s ' +
    'n�o definida! (08)'), 'ERRO', MB_OK+MB_ICONERROR);
  end;
end;

procedure TDmLot2.DataModuleCreate(Sender: TObject);
begin
  FArMySQLLO[00] := QrEmLot1;
  FArMySQLLO[01] := QrEmLot2;
  FArMySQLLO[02] := QrEmLot3;
  FArMySQLLO[03] := QrEmLot4;
  FArMySQLLO[04] := QrEmLot5;
  FArMySQLLO[05] := QrEmLot6;
end;

function TDmLot2.DataTPCheque(): TDateTime;
begin
  Result := 0;
  case FmPrincipal.FFormLotShow of
    2: Result := FDataTransacao;
    else Application.MessageBox(PChar('Janela de gerenciamento de border�s ' +
    'n�o definida! (07)'), 'ERRO', MB_OK+MB_ICONERROR);
  end;
end;

procedure TDmLot2.QrLCHsCalcFields(DataSet: TDataSet);
begin
  QrLCHsBAC.Value :=
    FormatFloat('000', QrLCHsBanco.Value) +
    FormatFloat('0000', QrLCHsAgencia.Value) +
    QrLCHsConta.Value ;
  QrLCHsDIAS.Value := QrLCHsDVence.Value - Int(DataTPCheque);
  QrLCHsCPF_TXT.Value := Geral.FormataCNPJ_TT(QrLCHsCPF.Value);
  //
  if QrLCHsSit.Value = 2 then QrLCHsMEU_SIT.Value := 2 else
  if QrLCHsCPF_2.Value = '' then QrLCHsMEU_SIT.Value := 0
  else if QrLCHsCPF.Value = QrLCHsCPF_2.Value then
  begin
    if AnsiCompareText(QrLCHsEmitente.Value, QrLCHsNOME_2.Value) = 0 then
      QrLCHsMEU_SIT.Value := 2 else QrLCHsMEU_SIT.Value := 1;
  end else
  QrLCHsMEU_SIT.Value := -1;
  //
  if QrLCHsDuplicado.Value = 0 then QrLCHsNOMEDUPLICADO.Value := 'N'
  else QrLCHsNOMEDUPLICADO.Value := 'S';
  //
  QrLCHsRISCO.Value := QrLCHsRISCOEM.Value + QrLCHsRISCOSA.Value;
  //
  QrLCHsTEXTO_SPC.Value := MLAGeral.StatusSPC_TXT(QrLCHsStatusSPC.Value);
end;

procedure TDmLot2.QrLCHsAfterScroll(DataSet: TDataSet);
var
  MyCursor: TCursor;
begin
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
  HabilitaBotoes(QrLCHsMEU_SIT.Value, 0);
  //
{
  DmLot.QrCHOpen.Close;
  DmLot.QrCHOpen.Params[0].AsString  := QrLCHsCPF.Value;
  DmLot.QrCHOpen. Open;
}
  UnDmkDAC_PF.AbreMySQLQuery0(DmLot.QrCHOpen, Dmod.MyDB, [
  'SELECT li.Banco, li.Agencia, li.ContaCorrente, ',
  'li.Documento, li.Credito, li.DCompra, li.DDeposito ',
  'FROM ' + CO_TabLctA + ' li ',
  'WHERE li.CNPJCPF ="' + QrLCHsCPF.Value + '"',
  'AND (li.Devolucao=0) AND(DDeposito>=SYSDATE()) ',
  'ORDER BY DDeposito ',
  '']);
  //
  DmLot.QrAlinIts.Close;
  DmLot.QrAlinIts.Params[0].AsString := QrLCHsCPF.Value;
  UMyMod.AbreQuery(DmLot.QrAlinIts, Dmod.MyDB);
  //
  DmLot.ReopenSPC_Result(QrLCHsCPF.Value, 0);
  //
  Screen.Cursor := MyCursor;
  except
    Screen.Cursor := MyCursor;
    raise;
  end;
end;

procedure TDmLot2.QrLDUsCalcFields(DataSet: TDataSet);
begin
  QrLDUsDIAS.Value := QrLDUsDVence.Value - Int(DataTPCheque);
  QrLDUsCPF_TXT.Value := Geral.FormataCNPJ_TT(QrLDUsCPF.Value);
  //
  if QrLDUsSit.Value = 2 then QrLDUsMEU_SIT.Value := 2 else
  if QrLDUsCPF_2.Value = '' then QrLDUsMEU_SIT.Value := 0
  else if QrLDUsCPF.Value = QrLDUsCPF_2.Value then
  begin
    if (AnsiCompareText(QrLDUsEmitente.Value, QrLDUsNOME_2.Value) = 0)
    and (AnsiCompareText(QrLDUsIE.Value, QrLDUsIE_2.Value) = 0)
    and (AnsiCompareText(QrLDUsEmitente.Value, QrLDUsNOME_2.Value) = 0)
    and (AnsiCompareText(QrLDUsRua.Value, QrLDUsRUA_2.Value) = 0)
    and (AnsiCompareText(QrLDUsCompl.Value, QrLDUsCOMPL_2.Value) = 0)
    and (AnsiCompareText(QrLDUsBirro.Value, QrLDUsBAIRRO_2.Value) = 0)
    and (AnsiCompareText(QrLDUsCidade.Value, QrLDUsCIDADE_2.Value) = 0)
    and (AnsiCompareText(QrLDUsUF.Value, QrLDUsUF_2.Value) = 0)
    and (AnsiCompareText(Geral.SoNumero_TT(QrLDUsTel1.Value), QrLDUsTEL1_2.Value) = 0)
    and (QrLDUsNumero.Value = QrLDUsNUMERO_2.Value)
    and (Geral.FormataCEP_NT(QrLDUsCEP.Value) =Geral.FormataCEP_NT(QrLDUsCEP_2.Value))
    then QrLDUsMEU_SIT.Value := 2 else QrLDUsMEU_SIT.Value := 1;
  end else QrLDUsMEU_SIT.Value := -1;

  //////////////////////////////////////////////////////////////////////////////

  QrLDUsCEP_TXT.Value :=Geral.FormataCEP_NT(QrLDUsCEP.Value);
  QrLDUsCEP_2_TXT.Value :=Geral.FormataCEP_NT(QrLDUsCEP_2.Value);
  //
  QrLDUsTel1_TXT.Value := MLAGeral.FormataTelefone_TT_Curto(QrLDUsTel1.Value);
  QrLDUsTel1_2_TXT.Value := MLAGeral.FormataTelefone_TT_Curto(QrLDUsTel1_2.Value);
  //
  QrLDUsNumero_TXT.Value := MLAGeral.FormataNumeroDeRua(QrLDUsRua.Value, QrLDUsNumero.Value, False);
  QrLDUsNumero_2_TXT.Value := MLAGeral.FormataNumeroDeRua(QrLDUsRUA_2.Value, Trunc(QrLDUsNUMERO_2.Value), False);
  //
  if QrLDUsDuplicado.Value = 0 then QrLDUsNOMEDUPLICADO.Value := 'N'
  else QrLDUsNOMEDUPLICADO.Value := 'S';
  //
  QrLDUsRISCO.Value := QrLDUsRISCOEM.Value + QrLDUsRISCOSA.Value;
  //
  QrLDUsTEXTO_SPC.Value := MLAGeral.StatusSPC_TXT(QrLDUsStatusSPC.Value);
end;

procedure TDmLot2.QrLotBeforeClose(DataSet: TDataSet);
var
  i: Integer;
begin
  for i := 0 to FmPrincipal.FMyDBs.MaxDBs -1 do
    FArMySQLLO[i].Close;
end;

procedure TDmLot2.QrLotCalcFields(DataSet: TDataSet);
var
 VaValT, AdValT, VaTaxT, TotISS, TotIRR, TotPIS: Double;
 i: Integer;
begin
{!!!
  QrLotNOMETIPOLOTE.Value := GetTitle(QrLotTipo.Value);
}
  //
  QrLotCNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrLotCNPJCPF.Value);
  AdValT := QrLotAdValorem.Value ;
  VaValT := QrLotValValorem.Value;
  VaTaxT := QrLotTxCompra.Value;
  TotISS := QrLotISS_Val.Value;
  TotIRR := QrLotIRRF_Val.Value;
  TotPIS := QrLotPIS_R_Val.Value;
  for i := 0 to FmPrincipal.FMyDBs.MaxDBs -1 do
  begin
    if FmPrincipal.FMyDBs.Connected[i] = '1' then
    begin
      AdValT := AdValT + FArMySQLLO[i].FieldByName('AdValorem').AsFloat;
      VaValT := VaValT + FArMySQLLO[i].FieldByName('ValValorem').AsFloat;
      VaTaxT := VaTaxT + FArMySQLLO[i].FieldByName('TaxaVal').AsFloat;
      TotISS := TotISS + FArMySQLLO[i].FieldByName('ISS_Val').AsFloat;
      TotIRR := TotIRR + FArMySQLLO[i].FieldByName('IRRF_Val').AsFloat;
      TotPIS := TotPIS + FArMySQLLO[i].FieldByName('PIS_R_Val').AsFloat;
    end;
  end;
  //
  VaValT := Geral.RoundC(VaValT, 2);
  VaTaxT := Geral.RoundC(VaTaxT, 2);
  // IMPOSTOS
  QrLotTOT_ISS.Value  := TotISS;
  QrLotTOT_IRR.Value := TotIRR;
  //
  QrLotADVALOREM_TOTAL.Value := AdValT;
  QrLotVALVALOREM_TOTAL.Value := VaValT;
  QrLotTAXAVALOR_TOTAL.Value := VaTaxT;
  //
  if QrLotTotal.Value = 0 then
  begin
    QrLotTAXAPERCE_TOTAL.Value := 0;
    QrLotTAXAPERCE_PROPR.Value := 0;
    QrLotTAXAPERCEMENSAL.Value := 0;
  end else begin
    QrLotTAXAPERCE_TOTAL.Value := VaTaxT / QrLotTotal.Value * 100;
    QrLotTAXAPERCE_PROPR.Value := QrLotTxCompra.Value / QrLotTotal.Value * 100;
    QrLotTAXAPERCEMENSAL.Value := MLAGeral.DescobreJuroComposto(
      QrLotTAXAPERCE_TOTAL.Value, QrLotDias.Value, 2);
  end;
  //////////////////////////////////////////////////////////////////////////////
  QrLotSUB_TOTAL.Value := Round((QrLotTotal.Value - QrLotVALVALOREM_TOTAL.Value -
    QrLotTAXAVALOR_TOTAL.Value - QrLotIRRF_VAL.Value - QrLotIOC_VAL.Value -
    QrLotIOFd_VAL.Value - QrLotIOFv_VAL.Value - QrLotCPMF_VAL.Value) * 100)/100;
  //////////////////////////////////////////////////////////////////////////////
  QrLotSUB_TOTAL_2.Value := QrLotSUB_TOTAL.Value - QrLotTarifas.Value;
  //////////////////////////////////////////////////////////////////////////////
  QrLotSUB_TOTAL_3.Value := QrLotSUB_TOTAL_2.Value - QrLotOcorP.Value;
  //////////////////////////////////////////////////////////////////////////////
  QrLotVAL_LIQUIDO.Value := Round((QrLotSUB_TOTAL_3.Value -
    QrLotCHDevPG.Value - QrLotDUDevPG.Value)*100)/100;
  //
  QrLotSALDOAPAGAR.Value := QrLotVAL_LIQUIDO.Value - QrLotPgLiq.Value
   + QrLotSobraIni.Value;
  //
  QrLotMEU_PISCOFINS_VAL.Value := QrLotPIS_Val.Value + QrLotPIS_R_Val.Value +
    QrLotCOFINS_Val.Value + QrLotCOFINS_R_Val.Value;
  QrLotMEU_PISCOFINS.Value := QrLotPIS.Value + QrLotPIS_R.Value +
    QrLotCOFINS.Value + QrLotCOFINS_R.Value;

  ///////// M�nimos ///////////////////
  if QrLotTotal.Value > 0 then
  begin
    QrLotADVAL_TXT.Value := Geral.FFT(QrLotMINAV.Value /
      QrLotTotal.Value * 100, 2, siPositivo);
  end else begin
    QrLotADVAL_TXT.Value := '0,00';
  end;
  QrLotTAXA_AM_TXT.Value := Geral.FFT(QrLotMINTC_AM.Value, 2, siPositivo);
  //
  QrLotSUB_TOTAL_MEU.Value := QrLotTotal.Value -
    QrLotMINAV.Value - QrLotMINTC.Value;
  QrLotVAL_LIQUIDO_MEU.Value := QrLotSUB_TOTAL_MEU.Value -
    QrLotIOC_VAL.Value - QrLotIOFd_VAL.Value - QrLotIOFv_VAL.Value;
  //
  QrLotA_PG_LIQ.Value := QrLotVAL_LIQUIDO_MEU.Value - QrLotPgLiq.Value;
  ///////// Fim M�nimos ///////////////////
  if QrLotTipo.Value = 0 then
  begin
    QrLotCAPTIONCONFCARTA.Value := 'Malote conferido:';
    if QrLotConferido.Value > 0 then
      QrLotNOMECOFECARTA.Value := 'SIM'
    else QrLotNOMECOFECARTA.Value := 'N�O';
  end else begin
    QrLotCAPTIONCONFCARTA.Value := 'Carta sacado impressa:';
    if QrLotECartaSac.Value > 0 then
      QrLotNOMECOFECARTA.Value := 'SIM'
    else QrLotNOMECOFECARTA.Value := 'N�O';
  end;
  if QrLotSpread.Value = 0 then QrLotNOMESPREAD.Value := '+'
  else QrLotNOMESPREAD.Value := '-';
  if QrLotSCB.Value = 0 then
    QrLotNOMESPREAD.Value := 'CCB' + QrLotNOMESPREAD.Value
  else
    QrLotNOMESPREAD.Value := 'SCB' + QrLotNOMESPREAD.Value;
  // Valores Negativos - Ponto de vista cliente da factoring
  QrLotIOC_VALNEG.Value         := - QrLotIOC_VAL.Value;
  QrLotCPMF_VALNEG.Value        := - QrLotCPMF_VAL.Value;
  QrLotTAXAVALOR_TOTALNEG.Value := - QrLotTAXAVALOR_TOTAL.Value;
  QrLotVAVALOREM_TOTALNEG.Value := - QrLotVALVALOREM_TOTAL.Value;
  QrLotCHDevPgNEG.Value         := - QrLotCHDevPg.Value;
  QrLotDUDevPgNEG.Value         := - QrLotDUDevPg.Value;
end;

procedure TDmLot2.HabilitaControle(Controle0, Controle1: TControl; Habilita: Boolean);
begin
  case FmPrincipal.FFormLotShow of
    2: if Controle1 <> nil then Controle1.Enabled := Habilita;
    else Application.MessageBox(PChar('Janela de gerenciamento de border�s ' +
    'n�o definida! (09)'), 'ERRO', MB_OK+MB_ICONERROR);
  end;

end;

procedure TDmLot2.LancamentosAutomaticosBordero(Lote: Integer);
var
  TXT_FatNum: String;
  //
  function AtualizaValor(FatID: Integer; Valor: Double; tpPagto: TTipoPagto): Boolean;
  var
    Credito, Debito, FatNum: Double;
    FatParcela, Controle, Genero: Integer;
  begin
    Result := False;

    if not DmodFin.ObtemGeneroDeFatID(FatID, Genero, tpPagto, True) then Exit;
    //
    if tpPagto = tpDeb then
    begin
      Credito := 0;
      Debito := Valor;
    end else
    begin
      Credito := Valor;
      Debito := 0;
    end;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrIDLct, Dmod.MyDB, [
    'SELECT Credito, Controle, FatID, FatNum, FatParcela ',
    'FROM ' + CO_TabLctA,
    'WHERE FatID=' + Geral.FF0(FatID),
    'AND FatNum=' + TXT_FatNum,
    '']);
    // Excluir?
    if (QrIDLct.RecordCount > 0) and (Valor < 0.01) and (Valor > -0.01) then
    begin
      // N�o excluir!! Mas sim deixar como zero!
      DmLot.LOI_Upd_NUM(Dmod.QrUpd, QrIDLctFatID.Value, Trunc(QrIDLctFatNum.Value),
        QrIDLctFatParcela.Value, QrIDLctControle.Value, [
        'Credito', 'Debito', 'Genero'], [Credito, Debito, Genero]);
    end else
    // Alterar
    if (QrIDLct.RecordCount > 0) and ((Valor >= 0.01) or (Valor <= -0.01)) then
    begin
      // N�o excluir!! Mas sim deixar como zero!
      DmLot.LOI_Upd_NUM(Dmod.QrUpd, QrIDLctFatID.Value, Trunc(QrIDLctFatNum.Value),
        QrIDLctFatParcela.Value, QrIDLctControle.Value, [
        'Credito', 'Debito', 'Genero'], [Credito, Debito, genero]);
    end else
    // Incluir!
    if (QrIDLct.RecordCount = 0) and ((Valor >= 0.01) or (Valor <= -0.01)) then
    begin
      FatParcela := 0;
      Controle := 0;
      FatNum := QrLotCodigo.Value;
      //
      DmLot.SQL_Autom(Dmod.QrUpd, stIns, FatParcela, Controle, FatID, Genero,
        QrLotCliente.Value, FatNum, Credito, Debito, QrLotData.Value);
    end else
    // N�o Incluir!
    if (QrIDLct.RecordCount = 0) and (Valor < 0.01) and (Valor > -0.01) then
    begin
      // N�o Fazer nada!
    end else
    begin
      // ERRO!!
      Geral.MensagemBox(
      'A��o n�o implementada na fun��o "LancamentosAutomaticosBordero"!' +
      'Situa��o:' + #13#10 + 'Registros localizados = ' + Geral.FF0(
      QrIDLct.RecordCount) + #13#10 + 'Valor = ' + FormatFloat(
      '0.000000000000000', Valor) + #13#10 + #13#10 +
      'Avise a DERMATEK!', 'ERRO', MB_OK+MB_ICONWARNING);
      Exit;
    end;
    Result := True;
  end;
var
  Vlr_0371, Sobra: Double;
begin
  if Lote = 0 then
  begin
    Geral.MensagemBox('O lote zero n�o pode ter lan�amentos autom�ticos!',
    'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  TXT_FatNum := Geral.FF0(Lote);
  ReopenLot(TXT_FatNum);
  //
  // Ad Valorem
  if not AtualizaValor(VAR_FATID_0322, QrLotVALVALOREM_TOTAL.Value, tpCred) then Exit;
  // Fator de compra
  if not AtualizaValor(VAR_FATID_0333, QrLotTAXAVALOR_TOTAL.Value, tpCred) then Exit;
  // IOC
  if not AtualizaValor(VAR_FATID_0341, QrLotIOC_VAL.Value, tpCred) then Exit;
  // IOFd
  if not AtualizaValor(VAR_FATID_0342, QrLotIOFd_VAL.Value, tpCred) then Exit;
  // IOFc
  if not AtualizaValor(VAR_FATID_0343, QrLotIOFv_VAL.Value, tpCred) then Exit;
  // CPMF
  if not AtualizaValor(VAR_FATID_0344, QrLotCPMF_VAL.Value, tpCred) then Exit;
  // IRRF
  if not AtualizaValor(VAR_FATID_0345, QrLotIRRF_VAL.Value, tpCred) then Exit;
  // Sobras
  Sobra := QrLotSobraNow.Value - QrLotSobraIni.Value;
  if Sobra <= -0.01 then
  begin
    if not AtualizaValor(VAR_FATID_0362, -Sobra , tpDeb) then Exit;
  end else
  begin
    if not AtualizaValor(VAR_FATID_0361, Sobra, tpCred) then Exit;
  end;
  //
  // Repasse de cheques de terceiros a clientes!
  DmLot.ReopenSumRepCli(Lote);
  if not AtualizaValor(VAR_FATID_0365, DmLot.QrSumRepCliValor.Value, tpDeb) then Exit;
  //
  Vlr_0371 :=
    QrLotVALVALOREM_TOTAL.Value +
    QrLotTAXAVALOR_TOTAL.Value +
    QrLotIOC_VAL.Value +
    QrLotIOFd_VAL.Value +
    QrLotIOFv_VAL.Value +
    QrLotCPMF_VAL.Value +
    QrLotIRRF_VAL.Value +
    QrLotTarifas.Value +
    QrLotOcorP.Value +
    QrLotCHDevPg.Value +
    QrLotDUDevPg.Value +

    QrLotSobraNow.Value -
    QrLotSobraIni.Value;

  //
  if not AtualizaValor(VAR_FATID_0371, Vlr_0371, tpDeb) then Exit;
  //
  // Definir como encerrado
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_TabLotA, False, [
    'LctsAuto'], ['Codigo'], [1], [QrLotCodigo.Value], True);
end;

function TDmLot2.LocalizaEmitente2(MudaNome: Boolean; B, A, C: String;
DVCC: Integer; EdNome, EdCPF, EdRealCC: TdmkEdit): Boolean;
begin
  Result := False;
{
  if  (Geral.IMV(EdBanco2.Text) > 0)
  and (Geral.IMV(EdAgencia2.Text) > 0)
  and (Geral.DMV(EdConta2.Text) > 0.1) then
}
  if  (Geral.IMV(B) > 0)
  and (Geral.IMV(A) > 0)
  and (Geral.DMV(C) > 0.1) then
  begin
    Screen.Cursor := crHourGlass;
    try
      if MudaNome then
      begin
{
        B := EdBanco2.Text;
        A := EdAgencia2.Text;
        C := EdConta2.Text;
}
        //
        QrLocEmiBAC.Close;
        QrLocEmiBAC.Params[0].AsInteger := DVCC;//QrBanco2DVCC.Value;
        QrLocEmiBAC.Params[1].AsString  := CriaBAC_Pesq(B,A,C, DVCC(*QrBanco2DVCC.Value*), EdRealCC);
        UMyMod.AbreQuery(QrLocEmiBAC, Dmod.MyDB);
        //
        if QrLocEmiBAC.RecordCount = 0 then
        begin
{
            EdEmitente2.Text := '';
            EdCPF_2.Text := '';
}
          EdNome.Text := '';
          EdCPF.Text := '';
        end else begin
          QrLocEmiCPF.Close;
          QrLocEmiCPF.Params[0].AsString := QrLocEmiBACCPF.Value;
          UMyMod.AbreQuery(QrLocEmiCPF, Dmod.MyDB);
          //
          if QrLocEmiCPF.RecordCount = 0 then
          begin
            EdNome.Text := '';
            EdCPF.Text := '';
          end else begin
            EdNome.Text := QrLocEmiCPFNome.Value;
            EdCPF.Text      := Geral.FormataCNPJ_TT(QrLocEmiCPFCPF.Value);
            //DmLot.PesquisaRiscoSacado(EdCPF_2.Text);
          end;
        end;
      end;
      Screen.Cursor := crDefault;
      Result := True;
    except
      Screen.Cursor := crDefault;
      raise;
    end;
  end;
end;

function TDmLot2.LocalizaSacado(MudaNome: Boolean; EdCNPJ, EdSacado, EdRua,
EdNumero, EdCompl, EdBairro, EdCidade, EdCEP, EdTel1, EdEmail: TdmkEdit; CBUF:
TComboBox): Boolean;
var
  i: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    DmLot.QrLocSaca.Close;
    DmLot.QrLocSaca.Params[0].AsString := Geral.SoNumero_TT(EdCNPJ.Text);
    UMyMod.AbreQuery(DmLot.QrLocSaca, Dmod.MyDB);
    if MudaNome then
    begin
      if DmLot.QrLocSaca.RecordCount = 0 then
      begin
        EdSacado.Text := '';
        QrLocEmiCPF.Close;
        QrLocEmiCPF.Params[0].AsString := Geral.SoNumero_TT(EdCNPJ.Text);
        UMyMod.AbreQuery(QrLocEmiCPF, Dmod.MyDB);
        if QrLocEmiCPF.RecordCount > 0 then
          EdSacado.Text   := QrLocEmiCPFNome.Value;
      end else begin
        EdSacado.Text := DmLot.QrLocSacaNome.Value;
        EdRua.Text    := DmLot.QrLocSacaRua.Value;
        EdNumero.Text := IntToStr(Trunc(DmLot.QrLocSacaNumero.Value));
        EdCompl.Text  := DmLot.QrLocSacaCompl.Value;
        EdBairro.Text := DmLot.QrLocSacaBairro.Value;
        EdCidade.Text := DmLot.QrLocSacaCidade.Value;
        EdCEP.Text    :=Geral.FormataCEP_NT(DmLot.QrLocSacaCEP.Value);
        for i := 0 to CBUF.Items.Count do
        begin
          if CBUF.Items[i] = DmLot.QrLocSacaUF.Value then
          begin
            CBUF.ItemIndex := i;
            Break;
          end;
        end;
        EdTel1.Text  := Geral.FormataTelefone_TT(DmLot.QrLocSacaTel1.Value);
        EdEmail.Text := DmLot.QrLocSacaEmail.Value;
      end;
    end;
    Screen.Cursor := crDefault;
    Result := True;
  except
    Screen.Cursor := crDefault;
    raise;
  end;
end;

function TDmLot2.ObtemEmitente(Banco, Agencia, Conta: String; var Emitente,
  CPF: String; AvisaNaoEncontrado: Boolean; EdRealCC: TdmkEdit): Boolean;
var
  B,A,C: String;
begin
  B := Banco;
  A := Agencia;
  C := Conta;
  //
  QrBanco.Close;
  QrBanco.Params[0].AsInteger := Geral.IMV(Banco);
  UMyMod.AbreQuery(QrBanco, Dmod.MyDB);
  //
  QrLocEmiBAC.Close;
  QrLocEmiBAC.Params[0].AsInteger := QrBancoDVCC.Value;
  QrLocEmiBAC.Params[1].AsString  := CriaBAC_Pesq(B,A,C, QrBancoDVCC.Value, EdRealCC);
  UMyMod.AbreQuery(QrLocEmiBAC, Dmod.MyDB);
  //
  if QrLocEmiBAC.RecordCount = 0 then
  begin
    Emitente   := '';
    CPF        := '';
    Result     := False;
  end else begin
    QrLocEmiCPF.Close;
    QrLocEmiCPF.Params[0].AsString := QrLocEmiBACCPF.Value;
    UMyMod.AbreQuery(QrLocEmiCPF, Dmod.MyDB);
    //
    if QrLocEmiCPF.RecordCount = 0 then
    begin
      Emitente := '';
      CPF      := '';
      Result   := False;
    end else begin
      Emitente := QrLocEmiCPFNome.Value;
      CPF      := QrLocEmiCPFCPF.Value;
      Result   := True;
    end;
  end;
  if AvisaNaoEncontrado then
    Geral.MensagemBox('Emitente n�o encontrado', 'Aviso', MB_OK+MB_ICONWARNING);
end;

procedure TDmLot2.QrSCHsAfterOpen(DataSet: TDataSet);
begin
  case FmPrincipal.FFormLotShow of
    2:
    try
      HabilitaControle(nil, FmLot2Inn.BtSPC_CH, QrSCHs.RecordCount > 0);
    except
      // nada
    end;
  end;
end;

procedure TDmLot2.QrSCHsBeforeClose(DataSet: TDataSet);
begin
  case FmPrincipal.FFormLotShow of
    2:
    try
      HabilitaControle(nil, FmLot2Inn.BtSPC_CH, False);
    except
      ; // nada
    end;
  end;
end;

procedure TDmLot2.QrSDUsAfterOpen(DataSet: TDataSet);
begin
  case FmPrincipal.FFormLotShow of
    2:
    try
      HabilitaControle(nil, FmLot2Inn.BtSPC_DU, QrSDUs.RecordCount > 0);
    except
      ;// Nada
    end;
  end;
end;

procedure TDmLot2.QrSDUsBeforeClose(DataSet: TDataSet);
begin
  case FmPrincipal.FFormLotShow of
    2:
    try
      HabilitaControle(nil, FmLot2Inn.BtSPC_DU, False);
    except
      ; // nada
    end;
  end;
end;

procedure TDmLot2.QrLDUsAfterScroll(DataSet: TDataSet);
var
  MyCursor: TCursor;
begin
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
  HabilitaBotoes(QrLDUsMEU_SIT.Value, 1);
  //
{
  DmLot.QrDUOpen.Close;
  DmLot.QrDUOpen.Params[0].AsString  := QrLDUsCPF.Value;
  DmLot.QrDUOpen. Open;
}
  DmLot.ReopenDUOpen(QrLDUsCPF.Value);
  //
{
  DmLot.QrDUVenc.Close;
  DmLot.QrDUVenc.Params[0].AsString  := QrLDUsCPF.Value;
  DmLot.QrDUVenc. Open;
}
  DmLot.ReopenDUVenc(QrLDUsCPF.Value);
  //
  DmLot.ReopenSPC_Result(QrLDUsCPF.Value, 0);
  //
  Screen.Cursor := MyCursor;
  except
    Screen.Cursor := MyCursor;
    raise;
  end;
end;

procedure TDmLot2.AtualizaEmitente(Banco, Agencia, Conta, CPF, Nome:
String; Risco: Double);
var
  Registros: Integer;
  MyCursor: TCursor;
begin
  if Trim(CPF) = '' then Exit;
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
  CPF := Geral.SoNumero_TT(CPF);
  //
  QrEmitBAC.Close;
  QrEmitBAC.Params[0].AsString := Banco+Agencia+Conta;
  QrEmitBAC.Params[1].AsString := CPF;
  UMyMod.AbreQuery(QrEmitBAC, Dmod.MyDB);
  //
  QrLocEmiCPF.Close;
  QrLocEmiCPF.Params[0].AsString := CPF;
  UMyMod.AbreQuery(QrLocEmiCPF, Dmod.MyDB);
  //
  if Risco = -1 then Risco := QrLocEmiCPFLimite.Value;
  if (Risco < 0.01) then
    Risco := Dmod.QrControleCHRisco.Value;
  if QrEmitBAC.RecordCount = 0 then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO emitbac SET BAC=:P0, CPF=:P1 ');
    Dmod.QrUpd.Params[0].AsString := Banco+Agencia+Conta;
    Dmod.QrUpd.Params[1].AsString := CPF;
    Dmod.QrUpd.ExecSQL;
  end;
  //
  Registros := QrLocEmiCPF.RecordCount;
  Dmod.QrUpd.SQL.Clear;
  if Registros = 0 then
    Dmod.QrUpd.SQL.Add('INSERT INTO emitcpf SET ')
  else
    Dmod.QrUpd.SQL.Add('UPDATE emitcpf SET ');
  Dmod.QrUpd.SQL.Add('Nome=:P0, Limite=:P1, LastAtz=:P2');
  if Registros = 0 then
    Dmod.QrUpd.SQL.Add(', CPF=:Pa')
  else
    Dmod.QrUpd.SQL.Add('WHERE CPF=:Pa');
  Dmod.QrUpd.Params[0].AsString  := Nome;
  Dmod.QrUpd.Params[1].AsFloat   := Risco;
  Dmod.QrUpd.Params[2].AsString  := Geral.FDT(Now, 1);
  //
  Dmod.QrUpd.Params[3].AsString  := CPF;
  Dmod.QrUpd.ExecSQL;
  //

  //  ATUALIZAR ITENS SENDO IMPORTADOS
  //Marcelo Ini 07/10/2011
  //Cria a tabela caso n�o exista!

  QrUpdLocDB.Close;
  QrUpdLocDB.SQL.Clear;
  QrUpdLocDB.SQL.Add('SHOW TABLES ');
  QrUpdLocDB.SQL.Add('LIKE "importlote"');
  UMyMod.AbreQuery(QrUpdLocDB, Dmod.MyLocDatabase);
  if QrUpdLocDB.RecordCount = 0 then
    UCriar.RecriaTabelaLocal('ImportLote', 1);
  //
  //Marcelo Fim 07/10/2011
  Dmod.QrUpdL.SQL.Clear;
  Dmod.QrUpdL.SQL.Add('UPDATE importlote SET ');
  Dmod.QrUpdL.SQL.Add('Nome_2=:P0, RISCOEM=:P1, ');
  Dmod.QrUpdL.SQL.Add('CPF_2=:Pa WHERE CPF=:Pb');
  Dmod.QrUpdL.Params[00].AsString  := Nome;
  Dmod.QrUpdL.Params[01].AsFloat   := Risco;
  Dmod.QrUpdL.Params[02].AsString  := CPF;
  //
  Dmod.QrUpdL.Params[03].AsString  := CPF;
  Dmod.QrUpdL.ExecSQL;
  //

  Screen.Cursor := MyCursor;
  except
    Screen.Cursor := MyCursor;
    raise;
  end;
end;

procedure TDmLot2.ReopenBanco(Banco: Integer);
begin
  QrBanco.Close;
  QrBanco.Params[0].AsInteger := Banco;
  UMyMod.AbreQuery(QrBanco, Dmod.MyDB);
end;

function TDmLot2.BaixaLotWeb(): Integer;
var
  Baixar: Integer;
begin
  // Parei Aqui Fazer conex�o do BD, se este estiver desconectado
  if not ConectaBDn(True) then
  begin
    Result := 0;
    Exit;
  end;
  Qrwlc.Close;
  UMyMod.AbreQuery(Qrwlc, Dmod.MyDBn);
  Baixar := Qrwlc.RecordCount;
  case Baixar of
    0: Application.MessageBox('N�o h� lote novo na web!', 'Informa��o',
      MB_OK+MB_ICONINFORMATION);
    1: if Application.MessageBox(PChar('Foi localizado um lote novo na web! '+
       'Deseja baix�-lo?'), 'Pergunta', MB_YESNO+MB_ICONQUESTION) <> ID_YES then
       Baixar := 0;
    else if Application.MessageBox(PChar('Foram localizados ' +
       IntToStr(Baixar) + ' lot' + 'es novos na web! Deseja baix�-los?'), 'Pergunta',
       MB_YESNO+MB_ICONQUESTION) <> ID_YES then Baixar := 0;
  end;
  if Baixar > 0 then
  begin
    Screen.Cursor := crHourGlass;
    try
      Qrwlc.First;
      while not Qrwlc.Eof do
      begin
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('DELETE FROM llotecab WHERE Codigo=:P0');
        Dmod.QrUpd.Params[0].AsInteger := QrwlcCodigo.Value;
        Dmod.QrUpd.ExecSQL;
        //
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('DELETE FROM lloteits WHERE Codigo=:P0');
        Dmod.QrUpd.Params[0].AsInteger := QrwlcCodigo.Value;
        Dmod.QrUpd.ExecSQL;
        //
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('INSERT INTO llotecab SET ');
        Dmod.QrUpd.SQL.Add('Codigo=:P0, Cliente=:P1, Lote=:P2, Tipo=:P3, ');
        Dmod.QrUpd.SQL.Add('Data=:P4, Total=:P5, Itens=:P6');

        Dmod.QrUpd.Params[00].AsInteger := QrwlcCodigo.Value;
        Dmod.QrUpd.Params[01].AsInteger := QrwlcCliente.Value;
        Dmod.QrUpd.Params[02].AsInteger := QrwlcLote.Value;
        Dmod.QrUpd.Params[03].AsInteger := QrwlcTipo.Value;
        Dmod.QrUpd.Params[04].AsString  := Geral.FDT(QrwlcData.Value, 1);
        Dmod.QrUpd.Params[05].AsFloat   := QrwlcTotal.Value;
        Dmod.QrUpd.Params[06].AsInteger := QrwlcItens.Value;
        Dmod.QrUpd.ExecSQL;
        //
        Qrwli.Close;
        Qrwli.Params[0].AsInteger := QrwlcCodigo.Value;
        UMyMod.AbreQuery(Qrwli, Dmod.MyDBn);
        //
        Qrwli.First;
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('INSERT INTO lloteits SET ');
        Dmod.QrUpd.SQL.Add('Codigo=:P0, Controle=:P1, Praca=:P2, Banco=:P3, ');
        Dmod.QrUpd.SQL.Add('Agencia=:P4, Conta=:P5, Cheque=:P6, CPF=:P7, ');
        Dmod.QrUpd.SQL.Add('Emitente=:P8, Bruto=:P9, Valor=:P10, ');
        Dmod.QrUpd.SQL.Add('Vencto=:P11, Duplicata=:P12, Rua=:P13, ');
        Dmod.QrUpd.SQL.Add('Numero=:P14, Compl=:P15, Bairro=:P16, ');
        Dmod.QrUpd.SQL.Add('Cidade=:P17, UF=:P18, CEP=:P19, Tel1=:P20, ');
        Dmod.QrUpd.SQL.Add('IE=:P21, Emissao=:P22');
        Dmod.QrUpd.SQL.Add('');
        //Controle = VerificaControle(QrwliCodigo.Value, QrwliControle.Value);
        while not Qrwli.Eof do
        begin
          Dmod.QrUpd.Params[00].AsInteger := QrwliCodigo.Value;
          Dmod.QrUpd.Params[01].AsInteger := QrwliControle.Value;
          Dmod.QrUpd.Params[02].AsInteger := QrwliPraca.Value;
          Dmod.QrUpd.Params[03].AsInteger := QrwliBanco.Value;
          Dmod.QrUpd.Params[04].AsInteger := QrwliAgencia.Value;
          Dmod.QrUpd.Params[05].AsString  := QrwliConta.Value;
          Dmod.QrUpd.Params[06].AsInteger := QrwliCheque.Value;
          Dmod.QrUpd.Params[07].AsString  := QrwliCPF.Value;
          Dmod.QrUpd.Params[08].AsString  := QrwliEmitente.Value;
          Dmod.QrUpd.Params[09].AsFloat   := QrwliBruto.Value;
          Dmod.QrUpd.Params[10].AsFloat   := QrwliValor.Value;
          Dmod.QrUpd.Params[11].AsString  := Geral.FDT(QrwliVencto.Value, 1);
          Dmod.QrUpd.Params[12].AsString  := QrwliDuplicata.Value;
          Dmod.QrUpd.Params[13].AsString  := QrwliRua.Value;
          Dmod.QrUpd.Params[14].AsInteger := QrwliNumero.Value;
          Dmod.QrUpd.Params[15].AsString  := QrwliCompl.Value;
          Dmod.QrUpd.Params[16].AsString  := QrwliBairro.Value;
          Dmod.QrUpd.Params[17].AsString  := QrwliCidade.Value;
          Dmod.QrUpd.Params[18].AsString  := QrwliUF.Value;
          Dmod.QrUpd.Params[19].AsInteger := QrwliCEP.Value;
          Dmod.QrUpd.Params[20].AsString  := QrwliTel1.Value;
          Dmod.QrUpd.Params[21].AsString  := QrwliIE.Value;
          Dmod.QrUpd.Params[22].AsString  := Geral.FDT(QrwliEmissao.Value, 1);
          Dmod.QrUpd.ExecSQL;
          Qrwli.Next;
        end;
        QrSumLLC.Close;
        QrSumLLC.Params[0].AsInteger := QrwlcCodigo.Value;
        UMyMod.AbreQuery(QrSumLLC, Dmod.MyDBn);
        //
        Dmod.QrWeb.SQL.Clear;
        Dmod.QrWeb.SQL.Add('UPDATE wlotecab SET Baixado=2, Total=:P0, Itens=:P1 ');
        Dmod.QrWeb.SQL.Add('WHERE Codigo=:P2');
        Dmod.QrWeb.Params[00].AsFloat   := QrSumLLCTotal.Value;
        Dmod.QrWeb.Params[01].AsInteger := Trunc(QrSumLLCItens.Value);
        Dmod.QrWeb.Params[02].AsInteger := QrwlcCodigo.Value;
        Dmod.QrWeb.ExecSQL;
        //
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('UPDATE llotecab SET Baixado=2, Total=:P0, Itens=:P1 ');
        Dmod.QrUpd.SQL.Add('WHERE Codigo=:P2');
        Dmod.QrUpd.Params[00].AsFloat   := QrSumLLCTotal.Value;
        Dmod.QrUpd.Params[01].AsInteger := Trunc(QrSumLLCItens.Value);
        Dmod.QrUpd.Params[02].AsInteger := QrwlcCodigo.Value;
        Dmod.QrUpd.ExecSQL;
        //
        Qrwlc.Next;
      end;
      Application.MessageBox('Download conclu�do com sucesso!', 'Informa��o',
        MB_OK+MB_ICONINFORMATION);
      Screen.Cursor := crDefault;
    finally
      Screen.Cursor := crDefault;
    end;  
  end;
  Result := Baixar;
end;

function TDmLot2.ConectaBDn(Pergunta: Boolean): Boolean;
var
  Conectar: Integer;
  BD, IP, PW, ID: String;
begin
  Result := False;
  if not Dmod.MyDBn.Connected then
  begin
    if Pergunta then Conectar := Application.MessageBox(PChar('A base de dados'+
    ' web est� desconectada. Deseja se conectar agora?'), 'Pergunta', MB_YESNO+
    MB_ICONQUESTION) else Conectar := ID_YES;
    if Conectar = ID_YES then
    begin
      try
        IP := Dmod.QrControleWeb_Host.Value;
        ID := Dmod.QrControleWeb_User.Value;
        PW := Dmod.QrControleWeb_Pwd.Value;
        BD := Dmod.QrControleWeb_DB.Value;
        //
        if IP = '' then
        begin
          Result := False;
          Exit;
        end;
        if ID = '' then
        begin
          Result := False;
          Exit;
        end;
        if PW = '' then
        begin
          Result := False;
          Exit;
        end;
        if BD = '' then
        begin
          Result := False;
          Exit;
        end;
        (*
        if Geral.FaltaInfo(4,
        [IP,'Servidor',ID,'Usu�rio',PW,'Senha',BD,'Banco de dados'],
        'Conex�o ao MySQL no meu site') then
        begin
          Result := False;
          Exit;
        end;
        //
        Dmod.MyDBn.Connected    := False;
        Dmod.MyDBn.UserPassword := PW;
        Dmod.MyDBn.UserName     := ID;
        Dmod.MyDBn.Host         := IP;
        Dmod.MyDBn.DatabaseName := BD; // J� deve existir (configurado pelo hospedeiro)
        Dmod.MyDBn.Connected := True;
        Result := True;
        *)
        UnDmkDAC_PF.ConectaMyDB_DAC(Dmod.MyDBn, BD, IP, 3306,  ID,
          PW, (*Desconecta*)True, (*Configura*)True, (*Conecta*)False);
        Result := True;
      except
        Application.MessageBox(PChar('N�o foi poss�vel a conex�o � base de '+
        'dados web!'), 'Aviso', MB_OK+ MB_ICONWARNING);
        raise;
      end;
    end;
  end else Result := True;
end;

function TDmLot2.CriaBAC_Pesq(Banco, Agencia, Conta: String;
  DVCC: Integer; EdRealCC: TdmkEdit): String;
var
  RealCC: String;
begin
  RealCC := Copy(Conta, 10 - DVCC + 1, DVCC);
  if EdRealCC <> nil then
    EdRealCC.Text := RealCC;
  Result := Banco + Agencia + RealCC;
end;

procedure TDmLot2.ReopenLLoteCab(Status: Integer);
begin
  //Dmoc.ReopenLLoteCab(CgStatus.Value);
  // Parei Aqui
end;

procedure TDmLot2.ReopenLot(Lote: String);
var
  I: Integer;
begin
  // Deve ser primeiro!
  QrLot.Close;
  //
  QrEmLot1.Database := Dmod.MyDB1;
  QrEmLot2.Database := Dmod.MyDB2;
  QrEmLot3.Database := Dmod.MyDB3;
  QrEmLot4.Database := Dmod.MyDB4;
  QrEmLot5.Database := Dmod.MyDB5;
  QrLotCodigo.DisplayFormat := FFormatFloat;
  for i := 0 to FmPrincipal.FMyDBs.MaxDBs -1 do
  begin
    FArMySQLLO[i].Close;
    if FmPrincipal.FMyDBs.Connected[i] = '1' then
    begin
{
      FArMySQLLO[i].Params[0].AsInteger := //;;Erro! abrea antes! QrLotCodigo.Value;
      FArMySQLLO[i]. Open;
}
      UnDmkDAC_PF.AbreMySQLQuery0(FArMySQLLO[i], FArMySQLLO[i].Database, [
      'SELECT * ',
      'FROM emlot ',
      'WHERE Codigo=' + Lote,
      '']);
    end;
  end;
  // Deve ser no final?
  UnDmkDAC_PF.AbreMySQLQuery0(QrLot, Dmod.MyDB, [
  'SELECT IF(en.Tipo=0, en.RazaoSocial, en.Nome) NOMECLIENTE, ',
  'IF(en.Tipo=0, en.Fantasia, en.Apelido) NOMEFANCLIENTE, ',
  'IF(en.Tipo=0, en.CNPJ, en.CPF) CNPJCPF, lo.*, en.LimiCred, ',
  'en.CBE CBECli, en.SCB SCBCli, QuantN1, QuantI1 ',
  'FROM ' + CO_TabLotA + ' lo ',
  'LEFT JOIN entidades en ON en.Codigo=lo.Cliente ',
  'WHERE lo.Codigo=' + Lote,
  '']);
  //
end;

procedure TDmLot2.ReopenOutros(Cliente: Integer; Data: String; Lote: Double);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOutros, Dmod.MyDB, [
  'SELECT Data, Codigo, Lote, SobraNow ',
  'FROM ' + CO_TabLotA,
  'WHERE Cliente=' + FormatFloat('0', Cliente),
  'AND (Data > "' + Data + '" ',
  'OR (Data = "' + Data + '" AND Codigo>' + FormatFloat('0', Lote) + ')) ',
  'ORDER BY Data, Codigo ',
  '']);
end;

procedure TDmLot2.ReopenSobraIni(Cliente: Integer; Data: String; Lote: Double);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSobraIni, Dmod.MyDB, [
  'SELECT Data, Codigo, SobraNow ',
  'FROM ' + CO_TabLotA,
  'WHERE Cliente=' + FormatFloat('0', Cliente),
  'AND (Data < "' + Data + '" ',
  'OR (Data = "' + Data + '" AND Codigo<' + FormatFloat('0', Lote) + ')) ',
  'ORDER BY Data DESC, Codigo DESC ',
  '']);
end;

procedure TDmLot2.AlterarNaWeb_Lote(Lote: Integer);
begin
{
  QrUDLC.Params[0].AsInteger := Lote;
  QrUDLC.ExecSQL;
}
  // Ainda precisa????
  UMyMod.SQLInsUpd(QrUDLC, stUpd, CO_TabLotA, False, [
  'AlterWeb'], ['Codigo'], [1], [Lote], False);
end;

{function TDmLot2.VerificaControle(Codigo, Controle: Integer): Integer;
begin
  Result := Controle;
  QrVeri.Close;
  UMyMod.AbreQuery(QrVeri);
  if QrVeriControle.Value > Controle then
  begin
    Result := QrVeriControle.Value + 1;
    Dmod.QrWeb.Close;
    Dmod.QrWeb.SQL.Clear;
    Dmod.QrWeb.SQL.Add('UPDATE wloteits SET ');
    Dmod.QrWeb.SQL.Add('Controle=:P0');
    Dmod.QrWeb.SQL.Add('WHERE Controle=:P1 AND Codigo=:P2');
    //
    Dmod.QrWeb.Params[00].AsInteger = Result;
    Dmod.QrWeb.Params[01].AsInteger = Controle;
    Dmod.QrWeb.Params[02].AsInteger = Codigo;
    Dmod.QrWeb.ExecSQL;
  end;
end;}

procedure TDmLot2.VerificaCPFCNP_XX_Importacao(NomeEnt: String; Entidade, Pagina: Integer;
SelType: TSelType);
begin
  QrSPC_Cfg.Close;
  QrSPC_Cfg.Params[0].AsInteger := Entidade;
  UMyMod.AbreQuery(QrSPC_Cfg, Dmod.MyDB);
  //
  if QrSPC_CfgCodigo.Value = 0 then
  begin
    Application.MessageBox(PChar('O cliente ' + IntToStr(Entidade) + ' - ' +
    NomeEnt + ' n�o possui configura��o de pesquisa ao SPC em seu cadastro!'),
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  case Pagina of
    0: VerificaCPFCNP_CH_Importacao(SelType);
    1: VerificaCPFCNP_DU_Importacao(SelType);
    else Application.MessageBox(PChar('Aba n�o definida para consulta de CPF ' +
    '/CNPJ na importa��o de lote!'), 'Aviso', MB_OK+MB_ICONWARNING);
  end;
end;

procedure TDmLot2.VerificaCPFCNP_CH_Importacao(SelType: TSelType);
  procedure VerificaCPF_Atual(const MinMax: Boolean; var Contagem: Integer;
  AvisaValor: Boolean);
  var
    TipoDoc, Solicitante, RG, UF, Endereco, Telefone, CEP, Banda: String;
    Nascimento: TDateTime;
    DigitoCC, DigitoCH, Qtde: Integer;
    //
    StatusSPC: Integer;
    ValMax, ValMin: Double;
  begin
    if Length(QrLCHsCPF.Value) = 11 then
      TipoDoc := 'CPF'
    else
      TipoDoc := 'CNPJ';
    FmLot2Inn.StCPF_CH.Caption := 'Aguarde... consultando ' + TipoDoc +
      Geral.FormataCNPJ_TT(QrLCHsCPF.Value);
    FmLot2Inn.StCPF_CH.Update;
    //
    Application.ProcessMessages;
    //
    Solicitante := VAR_LOGIN + '#' + FormatFloat('0', VAR_USUARIO);
    RG          := '';
    UF          := '';
    Nascimento  := 0;
    //
    //MLAGeral.GeraCMC7_30(Comp, Banco, Agencia, Conta, Cheque, Tipif, Banda);
    Banda       := '';
    //
    DigitoCC    := 0;
    DigitoCH    := 0;
    Qtde        := 1;
    //
    Endereco    := '';
    Telefone    := '';
    CEP         := '';
    //
    if MinMax then
    begin
      ValMin := QrSPC_CfgSPC_ValMin.Value;
      ValMax := QrSPC_CfgSPC_ValMax.Value;
    end else begin
      ValMin := 0;
      ValMax := 0;
    end;
    StatusSPC :=
    UnSPC.ConsultaSPC_Rapida(QrSPC_CfgCodigo.Value, True, QrLCHsCPF.Value,
      QrLCHsEmitente.Value, QrSPC_CfgServidor.Value,
      QrSPC_CfgSPC_Porta.Value, QrSPC_CfgCodigSocio.Value,
      QrSPC_CfgSenhaSocio.Value, QrSPC_CfgModalidade.Value,
      Solicitante, QrLCHsDCompra.Value, QrSPC_CfgBAC_CMC7.Value,
      Banda, RG, UF, Nascimento, QrLCHsBanco.Value, FormatFloat('0', QrLCHsAgencia.Value),
      QrLCHsConta.Value, DigitoCC, QrLCHsCheque.Value, DigitoCH, Qtde,
      QrSPC_CfgTipoCred.Value, QrLCHsValor.Value,
      QrSPC_CfgValAviso.Value, QrSPC_CfgVALCONSULTA.Value,
      Endereco, Telefone, CEP, ValMax, ValMin, nil, nil, AvisaValor);
    Dmod.QrUpdL.Params[00].AsInteger := StatusSPC;
    Dmod.QrUpdL.Params[01].AsString  := QrLCHsCPF.Value;
    Dmod.QrUpdL.ExecSQL;
    //
    if StatusSPC = 2 then Contagem := Contagem + 1;
  end;
  var
  CH, DU, i, Conta: Integer;
begin
  FmLot2Inn.StCPF_CH.Visible := True;
  CH := QrLCHsOrdem.Value;
  DU := QrLDUsOrdem.Value;
  //
  Dmod.QrUpdL.SQL.Clear;
  Dmod.QrUpdL.SQL.Add('UPDATE importlote SET StatusSPC=:P0');
  Dmod.QrUpdL.SQL.Add('WHERE CPF=:P1');
  //
  Conta := 0;
  case SelType of
    istAtual: VerificaCPF_Atual(False, Conta, True);
    istExtra1, istExtra2:
    begin
      QrILd.Close;
      QrILd.Params[0].AsInteger := 0; // CH
      UMyMod.AbreQuery(QrILd, Dmod.MyLocDatabase);
      //
      if UnSPC.AceitaValorSPC(QrSPC_CfgCodigo.Value,
      QrSPC_CfgVALCONSULTA.Value, QrILd.RecordCount, QrSPC_CfgValAviso.Value) =
      ID_YES then
      begin
        FmLot2Inn.PB1.Position := 0;
        FmLot2Inn.PB1.Max := QrILd.RecordCount;
        //
        while not QrILd.Eof do
        begin
          FmLot2Inn.PB1.Position := FmLot2Inn.PB1.Position + 1;
          FmLot2Inn.PB1.Update;
          Application.ProcessMessages;
          //
          if QrLCHs.Locate('CPF', QrILdCPF.Value, [])  then
          begin
            if SelType = istExtra1 then
              VerificaCPF_Atual(False, Conta, False)
            else
              VerificaCPF_Atual(True, Conta, False);
          end;
          //
          QrILd.Next;
        end;
      end;
    end;
    istSelecionados:
    begin
      if FmLot2Inn.GradeC_L.SelectedRows.Count > 0 then
      begin
        //
        //
        if UnSPC.AceitaValorSPC(QrSPC_CfgCodigo.Value,
        QrSPC_CfgVALCONSULTA.Value, FmLot2Inn.GradeC_L.SelectedRows.Count,
        QrSPC_CfgValAviso.Value) =
        ID_YES then
        begin
          FmLot2Inn.PB1.Position := 0;
          FmLot2Inn.PB1.Max := FmLot2Inn.GradeC_L.SelectedRows.Count;
          //
          with FmLot2Inn.GradeC_L.DataSource.DataSet do
          for i:= 0 to FmLot2Inn.GradeC_L.SelectedRows.Count-1 do
          begin
            FmLot2Inn.PB1.Position := FmLot2Inn.PB1.Position + 1;
            FmLot2Inn.PB1.Update;
            Application.ProcessMessages;
            //
            //GotoBookmark(pointer(FmLot2Inn.GradeC_L.SelectedRows.Items[i]));
            GotoBookmark(FmLot2Inn.GradeC_L.SelectedRows.Items[i]);
            VerificaCPF_Atual(False, Conta, False);
          end;
        end;
      end else VerificaCPF_Atual(False, Conta, True);
    end;
  end;
  //
  ReopenImportLote(CH, DU);
  FmLot2Inn.StCPF_CH.Visible := False;
  case Conta of
    0: ; // Nada
    1: Application.MessageBox('Foi localizado um documento com informa��es!',
    'Aviso', MB_OK+MB_ICONWARNING);
    else Application.MessageBox(PChar('Foram localizados ' + IntToStr(Conta) +
    ' documentos com informa��es!'), 'Aviso', MB_OK+MB_ICONWARNING);
  end;
end;

procedure TDmLot2.VerificaCPFCNP_DU_Importacao(SelType: TSelType);
  procedure VerificaCPF_Atual(const MinMax: Boolean; var Contagem: Integer;
  AvisaValor: Boolean);
  var
    TipoDoc, Solicitante, RG, UF, Endereco, Telefone, CEP, Banda: String;
    Nascimento: TDateTime;
    DigitoCC, DigitoCH, Qtde: Integer;
    //
    StatusSPC: Integer;
    ValMax, ValMin: Double;
  begin
    if Length(QrLDUsCPF.Value) = 11 then
      TipoDoc := 'CPF'
    else
      TipoDoc := 'CNPJ';
    FmLot2Inn.StCPF_DU.Caption := 'Aguarde... consultando ' + TipoDoc +
      Geral.FormataCNPJ_TT(QrLDUsCPF.Value);
    FmLot2Inn.StCPF_DU.Update;
    //
    Solicitante := VAR_LOGIN + '#' + FormatFloat('0', VAR_USUARIO);
    RG          := '';
    UF          := '';
    Nascimento  := 0;
    //
    //MLAGeral.GeraCMC7_30(Comp, Banco, Agencia, Conta, Cheque, Tipif, Banda);
    Banda       := '';
    //
    DigitoCC    := 0;
    DigitoCH    := 0;
    Qtde        := 1;
    //
    Endereco    := '';
    Telefone    := '';
    CEP         := '';
    //
    if MinMax then
    begin
      ValMin := QrSPC_CfgSPC_ValMin.Value;
      ValMax := QrSPC_CfgSPC_ValMax.Value;
    end else begin
      ValMin := 0;
      ValMax := 0;
    end;
    StatusSPC :=
    UnSPC.ConsultaSPC_Rapida(QrSPC_CfgCodigo.Value, True, QrLDUsCPF.Value,
      QrLDUsEmitente.Value, QrSPC_CfgServidor.Value,
      QrSPC_CfgSPC_Porta.Value, QrSPC_CfgCodigSocio.Value,
      QrSPC_CfgSenhaSocio.Value, QrSPC_CfgModalidade.Value,
      Solicitante, QrLDUsDCompra.Value, QrSPC_CfgBAC_CMC7.Value,
      Banda, RG, UF, Nascimento, QrLDUsBanco.Value, FormatFloat('0', QrLDUsAgencia.Value),
      QrLDUsConta.Value, DigitoCC, QrLDUsCheque.Value, DigitoCH, Qtde,
      QrSPC_CfgTipoCred.Value, QrLDUsValor.Value,
      QrSPC_CfgValAviso.Value, QrSPC_CfgVALCONSULTA.Value,
      Endereco, Telefone, CEP, ValMax, ValMin, nil, nil, AvisaValor);
    Dmod.QrUpdL.Params[00].AsInteger := StatusSPC;
    Dmod.QrUpdL.Params[01].AsString  := QrLDUsCPF.Value;
    Dmod.QrUpdL.ExecSQL;
    //
    if StatusSPC = 2 then Contagem := Contagem + 1;
  end;
  var
  CH, DU, i, Conta: Integer;
begin
  FmLot2Inn.StCPF_DU.Visible := True;
  CH := QrLCHsOrdem.Value;
  DU := QrLDUsOrdem.Value;
  //
  Dmod.QrUpdL.SQL.Clear;
  Dmod.QrUpdL.SQL.Add('UPDATE importlote SET StatusSPC=:P0');
  Dmod.QrUpdL.SQL.Add('WHERE CPF=:P1');
  //
  Conta := 0;
  case SelType of
    istAtual: VerificaCPF_Atual(False, Conta, True);
    istExtra1, istExtra2:
    begin
      QrILd.Close;
      QrILd.Params[0].AsInteger := 1; // DU
      UMyMod.AbreQuery(QrILd, Dmod.MyLocDatabase);
      //
      if UnSPC.AceitaValorSPC(QrSPC_CfgCodigo.Value,
      QrSPC_CfgVALCONSULTA.Value, QrILd.RecordCount, QrSPC_CfgValAviso.Value) =
      ID_YES then
      begin
        FmLot2Inn.PB1.Position := 0;
        FmLot2Inn.PB1.Max := QrILd.RecordCount;
        //
        while not QrILd.Eof do
        begin
          FmLot2Inn.PB1.Position := FmLot2Inn.PB1.Position + 1;
          FmLot2Inn.PB1.Update;
          Application.ProcessMessages;
          //
          if QrLDUs.Locate('CPF', QrILdCPF.Value, [])  then
          begin
            if SelType = istExtra1 then
              VerificaCPF_Atual(False, Conta, False)
            else
              VerificaCPF_Atual(True, Conta, False);
          end;
          //
          QrILd.Next;
        end;
      end;
    end;
    istSelecionados:
    begin
      if FmLot2Inn.GradeD_L.SelectedRows.Count > 0 then
      begin
        //
        if UnSPC.AceitaValorSPC(QrSPC_CfgCodigo.Value,
        QrSPC_CfgVALCONSULTA.Value, FmLot2Inn.GradeD_L.SelectedRows.Count,
        QrSPC_CfgValAviso.Value) =
        ID_YES then
        begin
          FmLot2Inn.PB1.Position := 0;
          FmLot2Inn.PB1.Max := FmLot2Inn.GradeD_L.SelectedRows.Count;
          //
          with FmLot2Inn.GradeD_L.DataSource.DataSet do
          for i:= 0 to FmLot2Inn.GradeD_L.SelectedRows.Count-1 do
          begin
            FmLot2Inn.PB1.Position := FmLot2Inn.PB1.Position + 1;
            FmLot2Inn.PB1.Update;
            Application.ProcessMessages;
            //
            //GotoBookmark(pointer(FmLot2Inn.GradeD_L.SelectedRows.Items[i]));
            GotoBookmark(FmLot2Inn.GradeD_L.SelectedRows.Items[i]);
            VerificaCPF_Atual(False, Conta, False);
          end;
        end;
      end else VerificaCPF_Atual(False, Conta, True);
    end;
  end;
  //
  ReopenImportLote(CH, DU);
  FmLot2Inn.StCPF_DU.Visible := False;
  case Conta of
    0: ; // Nada
    1: Application.MessageBox('Foi localizado um documento com informa��es!',
    'Aviso', MB_OK+MB_ICONWARNING);
    else Application.MessageBox(PChar('Foram localizados ' + IntToStr(Conta) +
    ' documentos com informa��es!'), 'Aviso', MB_OK+MB_ICONWARNING);
  end;
end;

procedure TDmLot2.ReopenImportLote(CH, DU: Integer);
begin
  QrLCHs.Close;
  QrLCHs.SQL.Clear;
  QrLCHs.SQL.Add('SELECT  il.*');
  QrLCHs.SQL.Add('FROM importlote il');
  QrLCHs.SQL.Add('WHERE il.Tipo=0');
  QrLCHs.SQL.Add(MLAGeral.OrdemSQL1(FORDA_LCHs, FORDB_LCHs));
  UMyMod.AbreQuery(QrLCHs, Dmod.MyLocDatabase);
  QrLCHs.Locate('Ordem', CH, []);

  QrLDUs.Close;
  QrLDUs.SQL.Clear;
  QrLDUs.SQL.Add('SELECT  il.*');
  QrLDUs.SQL.Add('FROM importlote il');
  QrLDUs.SQL.Add('WHERE il.Tipo=1');
  QrLDUs.SQL.Add(MLAGeral.OrdemSQL1(FORDA_LDUs, FORDB_LDUs));
  UMyMod.AbreQuery(QrLDUs, Dmod.MyLocDatabase);
  QrLDUs.Locate('Ordem', DU, []);
  //
  QrSCHs.Close;
  UMyMod.AbreQuery(QrSCHs, Dmod.MyLocDatabase);
  //
  QrSDUs.Close;
  UMyMod.AbreQuery(QrSDUs, Dmod.MyLocDatabase);
  //
end;

end.

