unit RiscoAll_1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DBCtrls, Grids, DBGrids, Db,
  mySQLDbTables, ComCtrls, Menus, Mask, frxClass, frxDBSet, Variants, ABSMain,
  DateUtils, dmkDBGrid, dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkImage,
  dmkGeral, DmkDAC_PF, UnDmkEnums;

type
  TFmRiscoAll_1 = class(TForm)
    PainelDados: TPanel;
    Panel1: TPanel;
    Label75: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    QrOcorA: TmySQLQuery;
    QrOcorATIPODOC: TWideStringField;
    QrOcorANOMEOCORRENCIA: TWideStringField;
    QrOcorADataO: TDateField;
    QrOcorAOcorrencia: TIntegerField;
    QrOcorALoteQuit: TIntegerField;
    QrOcorATaxaP: TFloatField;
    QrOcorATaxaV: TFloatField;
    QrOcorAPago: TFloatField;
    QrOcorADataP: TDateField;
    QrOcorATaxaB: TFloatField;
    QrOcorAData3: TDateField;
    QrOcorAStatus: TSmallintField;
    QrOcorASALDO: TFloatField;
    QrOcorAATUALIZADO: TFloatField;
    DsOcorA: TDataSource;
    QrOcorACliente: TIntegerField;
    QrCHDevA: TmySQLQuery;
    QrCHDevADATA1_TXT: TWideStringField;
    QrCHDevADATA2_TXT: TWideStringField;
    QrCHDevADATA3_TXT: TWideStringField;
    QrCHDevACPF_TXT: TWideStringField;
    QrCHDevANOMECLIENTE: TWideStringField;
    QrCHDevACodigo: TIntegerField;
    QrCHDevAAlinea1: TIntegerField;
    QrCHDevAAlinea2: TIntegerField;
    QrCHDevAData1: TDateField;
    QrCHDevAData2: TDateField;
    QrCHDevAData3: TDateField;
    QrCHDevACliente: TIntegerField;
    QrCHDevABanco: TIntegerField;
    QrCHDevAAgencia: TIntegerField;
    QrCHDevAConta: TWideStringField;
    QrCHDevACheque: TIntegerField;
    QrCHDevACPF: TWideStringField;
    QrCHDevAValor: TFloatField;
    QrCHDevATaxas: TFloatField;
    QrCHDevALk: TIntegerField;
    QrCHDevADataCad: TDateField;
    QrCHDevADataAlt: TDateField;
    QrCHDevAUserCad: TIntegerField;
    QrCHDevAUserAlt: TIntegerField;
    QrCHDevAEmitente: TWideStringField;
    QrCHDevAChequeOrigem: TIntegerField;
    QrCHDevAStatus: TSmallintField;
    QrCHDevAValPago: TFloatField;
    QrCHDevAMulta: TFloatField;
    QrCHDevAJurosP: TFloatField;
    QrCHDevAJurosV: TFloatField;
    QrCHDevADesconto: TFloatField;
    QrCHDevASALDO: TFloatField;
    QrCHDevAATUAL: TFloatField;
    DsCHDevA: TDataSource;
    QrDOpen: TmySQLQuery;
    QrDOpenDuplicata: TWideStringField;
    QrDOpenDCompra: TDateField;
    QrDOpenDDeposito: TDateField;
    QrDOpenEmitente: TWideStringField;
    QrDOpenCliente: TIntegerField;
    QrDOpenSTATUS: TWideStringField;
    QrDOpenQuitado: TIntegerField;
    QrDOpenTotalJr: TFloatField;
    QrDOpenTotalDs: TFloatField;
    QrDOpenTotalPg: TFloatField;
    QrDOpenSALDO_DESATUALIZ: TFloatField;
    QrDOpenSALDO_ATUALIZADO: TFloatField;
    QrDOpenNOMESTATUS: TWideStringField;
    QrDOpenDDCALCJURO: TIntegerField;
    QrDOpenData3: TDateField;
    DsDOpen: TDataSource;
    QrRiscoC: TmySQLQuery;
    QrRiscoCBanco: TIntegerField;
    QrRiscoCDCompra: TDateField;
    QrRiscoCDDeposito: TDateField;
    QrRiscoCEmitente: TWideStringField;
    QrRiscoCCPF_TXT: TWideStringField;
    DsRiscoC: TDataSource;
    DsRiscoTC: TDataSource;
    QrRiscoTC: TmySQLQuery;
    QrRiscoTCValor: TFloatField;
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesNOMECLIENTE: TWideStringField;
    QrClientesAdValorem: TFloatField;
    QrClientesDMaisC: TIntegerField;
    QrClientesFatorCompra: TFloatField;
    QrClientesMAIOR_T: TFloatField;
    QrClientesADVAL_T: TFloatField;
    QrClientesDMaisD: TIntegerField;
    DsClientes: TDataSource;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    DBGrid10: TDBGrid;
    DBGrid14: TDBGrid;
    DBGrid12: TDBGrid;
    Label1: TLabel;
    EdColigado: TdmkEditCB;
    CBColigado: TdmkDBLookupComboBox;
    QrColigados: TmySQLQuery;
    DsColigados: TDataSource;
    QrColigadosCodigo: TIntegerField;
    QrColigadosNOMECOLIGADO: TWideStringField;
    QrDOpenBanco: TIntegerField;
    QrOcorABanco: TIntegerField;
    QrOcorADuplicata: TWideStringField;
    QrOcorADCompra: TDateField;
    QrOcorADDeposito: TDateField;
    QrOcorAEmitente: TWideStringField;
    QrRisco1: TmySQLQuery;
    QrRisco1Tipo: TIntegerField;
    QrRisco1Banco: TIntegerField;
    QrRisco1Agencia: TIntegerField;
    QrRisco1Conta: TWideStringField;
    QrRisco1Duplicata: TWideStringField;
    QrRisco1Cheque: TIntegerField;
    QrRisco1DEmissao: TDateField;
    QrRisco1DCompra: TDateField;
    QrRisco1DVence: TDateField;
    QrRisco1DDeposito: TDateField;
    QrRisco1DDevol1: TDateField;
    QrRisco1DDevol2: TDateField;
    QrRisco1UltPagto: TDateField;
    QrRisco1Valor: TFloatField;
    QrRisco1Taxas: TFloatField;
    QrRisco1Saldo: TFloatField;
    QrRisco1Juros: TFloatField;
    QrRisco1Desco: TFloatField;
    QrRisco1Pago: TFloatField;
    QrRisco1Atual: TFloatField;
    QrRisco1Emitente: TWideStringField;
    QrRisco1CPF: TWideStringField;
    QrRisco1Alinea1: TIntegerField;
    QrRisco1Alinea2: TIntegerField;
    QrRisco1Status: TWideStringField;
    QrRisco1Historico: TWideStringField;
    QrRisco1CONTA_DUPLICATA: TWideStringField;
    QrRisco1DEMISSAO_TXT: TWideStringField;
    QrRisco1DCOMPRA_TXT: TWideStringField;
    QrRisco1DVENCE_TXT: TWideStringField;
    QrRisco1DDEPOSITO_TXT: TWideStringField;
    QrRisco1DDEVOL1_TXT: TWideStringField;
    QrRisco1DDEVOL2_TXT: TWideStringField;
    QrRisco1ULTPAGTO_TXT: TWideStringField;
    TabSheet5: TTabSheet;
    GroupBox2: TGroupBox;
    Ck10: TCheckBox;
    Ck30: TCheckBox;
    Ck20: TCheckBox;
    Ck50: TCheckBox;
    Panel2: TPanel;
    BtImprime: TBitBtn;
    BtCancela: TBitBtn;
    QrRisco1TIPODOC: TWideStringField;
    Panel3: TPanel;
    RGOrdem3: TRadioGroup;
    RGOrdem2: TRadioGroup;
    RGOrdem1: TRadioGroup;
    QrRisco1CONTAGEM: TIntegerField;
    QrRiscoCCliente: TIntegerField;
    QrRisco1NOMECLIENTE: TWideStringField;
    QrRisco1Cliente: TIntegerField;
    BtGera: TBitBtn;
    QrRisco1Controle: TIntegerField;
    Ck40: TCheckBox;
    Panel4: TPanel;
    Panel5: TPanel;
    RGAgrupa: TRadioGroup;
    Panel6: TPanel;
    Progress1: TProgressBar;
    Ck60: TCheckBox;
    Ck70: TCheckBox;
    QrRisco1STATUS_HISTORICO: TWideStringField;
    QrSintetico: TmySQLQuery;
    QrAnalitico: TmySQLQuery;
    QrAnaliticoCliente: TIntegerField;
    QrAnaliticoTipo: TIntegerField;
    QrAnaliticoValor: TFloatField;
    QrAnaliticoTaxas: TFloatField;
    QrAnaliticoSaldo: TFloatField;
    QrAnaliticoJuros: TFloatField;
    QrAnaliticoDesco: TFloatField;
    QrAnaliticoPago: TFloatField;
    QrAnaliticoAtual: TFloatField;
    QrAnaliticoITENS: TLargeintField;
    QrAnaliticoDESCRITIPO: TWideStringField;
    QrAnaliticoNOMECLI: TWideStringField;
    PMImprime: TPopupMenu;
    Descritivo1: TMenuItem;
    Analtico1: TMenuItem;
    Sinttico1: TMenuItem;
    QrSinteticoCliente: TIntegerField;
    QrSinteticoTipo: TIntegerField;
    QrSinteticoValor: TFloatField;
    QrSinteticoTaxas: TFloatField;
    QrSinteticoSaldo: TFloatField;
    QrSinteticoJuros: TFloatField;
    QrSinteticoDesco: TFloatField;
    QrSinteticoPago: TFloatField;
    QrSinteticoAtual: TFloatField;
    QrSinteticoITENS: TLargeintField;
    QrSinteticoNOMECLI: TWideStringField;
    DBGrid1: TDBGrid;
    QrOcorADOCUM_TXT: TWideStringField;
    QrDOpenRepassado: TSmallintField;
    TabSheet6: TTabSheet;
    QrSacEmi: TmySQLQuery;
    QrSacEmiCPF_TXT: TWideStringField;
    DsSacEmi: TDataSource;
    DBGrid2: TDBGrid;
    Label2: TLabel;
    EdEmitente: TdmkEdit;
    Label4: TLabel;
    EdCPF: TdmkEdit;
    RGMascara: TRadioGroup;
    QrSinteticoDevolvido: TFloatField;
    QrAnaliticoDevolvido: TFloatField;
    QrRisco1Devolvido: TFloatField;
    QrSinteticoLimiCred: TFloatField;
    QrSinteticoULO: TFloatField;
    QrDOpenNOMEBANCO: TWideStringField;
    QrClientesLimiCred: TFloatField;
    Panel7: TPanel;
    Memo1: TMemo;
    PMHistorico: TPopupMenu;
    Clientepesquisado1: TMenuItem;
    Emitentesacadoselecionado1: TMenuItem;
    Ambospesquisados1: TMenuItem;
    Emitentesacadopesquisado1: TMenuItem;
    N1: TMenuItem;
    QrDOpenCPF_TXT: TWideStringField;
    QrOcorACPF_TXT: TWideStringField;
    EmitentesacadoselecionadoClientepesquisado1: TMenuItem;
    QrOcorADescri: TWideStringField;
    frxRisco1: TfrxReport;
    frxDsRisco1: TfrxDBDataset;
    frxAnalitico: TfrxReport;
    frxDsAnalitico: TfrxDBDataset;
    frxSintetico: TfrxReport;
    frxDsSintetico: TfrxDBDataset;
    QmRisco: TABSQuery;
    QrRiscoCNOMECLIENTE: TWideStringField;
    QrDOpenNOMECLIENTE: TWideStringField;
    QrOcorANOMECLIENTE: TWideStringField;
    QmRisco1: TABSQuery;
    QmRisco1Tipo: TIntegerField;
    QmRisco1Banco: TIntegerField;
    QmRisco1Agencia: TIntegerField;
    QmRisco1Conta: TWideStringField;
    QmRisco1Duplicata: TWideStringField;
    QmRisco1Cheque: TIntegerField;
    QmRisco1DEmissao: TDateField;
    QmRisco1DCompra: TDateField;
    QmRisco1DVence: TDateField;
    QmRisco1DDeposito: TDateField;
    QmRisco1DDevol1: TDateField;
    QmRisco1DDevol2: TDateField;
    QmRisco1UltPagto: TDateField;
    QmRisco1Valor: TFloatField;
    QmRisco1Taxas: TFloatField;
    QmRisco1Saldo: TFloatField;
    QmRisco1Juros: TFloatField;
    QmRisco1Desco: TFloatField;
    QmRisco1Pago: TFloatField;
    QmRisco1Atual: TFloatField;
    QmRisco1Emitente: TWideStringField;
    QmRisco1CPF: TWideStringField;
    QmRisco1Alinea1: TIntegerField;
    QmRisco1Alinea2: TIntegerField;
    QmRisco1Status: TWideStringField;
    QmRisco1Historico: TWideStringField;
    QmRisco1CONTA_DUPLICATA: TWideStringField;
    QmRisco1DEMISSAO_TXT: TWideStringField;
    QmRisco1DCOMPRA_TXT: TWideStringField;
    QmRisco1DVENCE_TXT: TWideStringField;
    QmRisco1DDEPOSITO_TXT: TWideStringField;
    QmRisco1DDEVOL1_TXT: TWideStringField;
    QmRisco1DDEVOL2_TXT: TWideStringField;
    QmRisco1ULTPAGTO_TXT: TWideStringField;
    QmRisco1TIPODOC: TWideStringField;
    QmRisco1CONTAGEM: TIntegerField;
    QmRisco1NOMECLIENTE: TWideStringField;
    QmRisco1Cliente: TIntegerField;
    QmRisco1Controle: TIntegerField;
    QmRisco1STATUS_HISTORICO: TWideStringField;
    QmRisco1Devolvido: TFloatField;
    TabSheet7: TTabSheet;
    DBGrid3: TDBGrid;
    DataSource1: TDataSource;
    QmAnalitico: TABSQuery;
    QmAnaliticoCliente: TIntegerField;
    QmAnaliticoTipo: TIntegerField;
    QmAnaliticoValor: TFloatField;
    QmAnaliticoTaxas: TFloatField;
    QmAnaliticoSaldo: TFloatField;
    QmAnaliticoJuros: TFloatField;
    QmAnaliticoDesco: TFloatField;
    QmAnaliticoPago: TFloatField;
    QmAnaliticoAtual: TFloatField;
    QmAnaliticoNOMECLIENTE: TWideStringField;
    QmAnaliticoDESCRITIPO: TWideStringField;
    QmAnaliticoDevolvido: TFloatField;
    QmSintetico: TABSQuery;
    QmSinteticoValor: TFloatField;
    QmSinteticoTaxas: TFloatField;
    QmSinteticoSaldo: TFloatField;
    QmSinteticoJuros: TFloatField;
    QmSinteticoDesco: TFloatField;
    QmSinteticoPago: TFloatField;
    QmSinteticoAtual: TFloatField;
    QmSinteticoNOMECLIENTE: TWideStringField;
    QmSinteticoDevolvido: TFloatField;
    QmSinteticoLimiCred: TFloatField;
    QmSinteticoULO: TFloatField;
    QrRiscoCLimiCred: TFloatField;
    QrCHDevALimiCred: TFloatField;
    QrDOpenLimiCred: TFloatField;
    QrOcorALimiCred: TFloatField;
    QmSinteticoITENS: TIntegerField;
    QmAnaliticoITENS: TIntegerField;
    TabSheet8: TTabSheet;
    dmkDBGrid1: TdmkDBGrid;
    DsSintetico: TDataSource;
    GBAvisos1: TGroupBox;
    Panel8: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel9: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GroupBox1: TGroupBox;
    Label19: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label96: TLabel;
    Label98: TLabel;
    Label99: TLabel;
    Label91: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    EdDR: TdmkEdit;
    EdDT: TdmkEdit;
    EdRT: TdmkEdit;
    EdVT: TdmkEdit;
    EdST: TdmkEdit;
    EdCT: TdmkEdit;
    EdDV: TdmkEdit;
    EdCR: TdmkEdit;
    EdCV: TdmkEdit;
    EdOA: TdmkEdit;
    EdTT: TdmkEdit;
    EdLimiCredCli: TdmkEdit;
    BtSaida: TBitBtn;
    BtPesquisa: TBitBtn;
    BtHistorico: TBitBtn;
    QrOcorACodigo: TIntegerField;
    QrOcorAValor: TFloatField;
    QrOcorAMoviBank: TIntegerField;
    QrOcorAAgencia: TIntegerField;
    QrOcorAConta: TWideStringField;
    QrOcorACheque: TIntegerField;
    QrOcorACPF: TWideStringField;
    QrOcorAEmissao: TDateField;
    QrOcorAVencto: TDateField;
    QrRiscoCTipo: TSmallintField;
    QrRiscoCContaCorrente: TWideStringField;
    QrRiscoCDocumento: TFloatField;
    QrRiscoCCredito: TFloatField;
    QrRiscoCCNPJCPF: TWideStringField;
    QrRiscoCData: TDateField;
    QrRiscoCVencimento: TDateField;
    QrRiscoCFatParcela: TIntegerField;
    QrDOpenFatParcela: TIntegerField;
    QrDOpenCredito: TFloatField;
    QrDOpenCNPJCPF: TWideStringField;
    QrDOpenVencimento: TDateField;
    QrDOpenData: TDateField;
    QrSacEmiEmitente: TWideStringField;
    QrSacEmiCNPJCPF: TWideStringField;
    QrOcorATpOcor: TSmallintField;
    QrRiscoCAgencia: TIntegerField;
    QrDOpenAgencia: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure QrOcorAAfterOpen(DataSet: TDataSet);
    procedure QrOcorABeforeClose(DataSet: TDataSet);
    procedure QrOcorACalcFields(DataSet: TDataSet);
    procedure QrCHDevAAfterOpen(DataSet: TDataSet);
    procedure QrCHDevABeforeClose(DataSet: TDataSet);
    procedure QrCHDevACalcFields(DataSet: TDataSet);
    procedure QrDOpenAfterOpen(DataSet: TDataSet);
    procedure QrDOpenBeforeClose(DataSet: TDataSet);
    procedure QrDOpenCalcFields(DataSet: TDataSet);
    procedure QrRiscoCCalcFields(DataSet: TDataSet);
    procedure EdClienteChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdColigadoChange(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure BtCancelaClick(Sender: TObject);
    procedure QrRisco1CalcFields(DataSet: TDataSet);
    procedure frRisco1GetValue(const ParName: String;
      var ParValue: Variant);
    procedure frRisco1UserFunction(const Name: String; p1, p2, p3: Variant;
      var Val: Variant);
    procedure BtGeraClick(Sender: TObject);
    procedure Descritivo1Click(Sender: TObject);
    procedure Analtico1Click(Sender: TObject);
    procedure QrAnaliticoCalcFields(DataSet: TDataSet);
    procedure Sinttico1Click(Sender: TObject);
    procedure DBGrid2TitleClick(Column: TColumn);
    procedure EdEmitenteChange(Sender: TObject);
    procedure EdCPFChange(Sender: TObject);
    procedure RGMascaraClick(Sender: TObject);
    procedure DBGrid2DblClick(Sender: TObject);
    procedure QrSinteticoCalcFields(DataSet: TDataSet);
    procedure QrSacEmiCalcFields(DataSet: TDataSet);
    procedure BtHistoricoClick(Sender: TObject);
    procedure Clientepesquisado1Click(Sender: TObject);
    procedure Ambospesquisados1Click(Sender: TObject);
    procedure PMHistoricoPopup(Sender: TObject);
    procedure Emitentesacadopesquisado1Click(Sender: TObject);
    procedure Emitentesacadoselecionado1Click(Sender: TObject);
    procedure EmitentesacadoselecionadoClientepesquisado1Click(
      Sender: TObject);
    procedure frxRisco1GetValue(const VarName: String; var Value: Variant);
    procedure QmRisco1CalcFields(DataSet: TDataSet);
    procedure QmAnaliticoCalcFields(DataSet: TDataSet);
    procedure QmSinteticoCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    FCHDevOpen_Total, FOcorA_Total: Double;
    FCliente, FColigado: Integer;
    FEmitente, FCPF: String;
    FORDA_Emisac, FORDB_Emisac: String;
    FParar: Boolean;
    FR10, FR20, FR30, FR40, FR50, FR60, FR70: Integer;
    procedure ReopenOcorA;
    procedure ReopenCHDevA;
    procedure ReopenDOpen;
    procedure ReopenRiscoC;
    procedure ReopenSacEmiSac;
    procedure DesfazPesquisa;
    function ParaImpressao: Boolean;
    function ImpressaoDeCabecalho(Tipo: Integer): String;
    function DescricaoDeDocumento(Tipo: Integer): String;
    function CondicaoDeImpressao(Tipo: Integer): String;
    function Ordem(Tipo: Integer): String;
    procedure DefineTipos;
    procedure MostraHistorico(Quem: Integer);
 public
    { Public declarations }
  end;

  var
  FmRiscoAll_1: TFmRiscoAll_1;

implementation

{$R *.DFM}

uses UnMyObjects, Module, UCreate, HistCliEmi, UMySQLModule, Principal,
  MyListas, UnInternalConsts, UnDmkABS_PF;

procedure TFmRiscoAll_1.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmRiscoAll_1.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmRiscoAll_1.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmRiscoAll_1.BtPesquisaClick(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  if Trim(EdEmitente.Text) <> '' then
  begin
    FEmitente := EdEmitente.Text;
    if RGMascara.ItemIndex in ([0,1]) then FEmitente := '%'+FEmitente;
    if RGMascara.ItemIndex in ([0,2]) then FEmitente := FEmitente+'%';
  end else FEmitente := '';
  if Trim(EdCPF.Text) <> '' then FCPF := Geral.SoNumero_TT(EdCPF.Text)
  else FCPF := '';
  FCliente  := EdCliente.ValueVariant;
  FColigado := EdColigado.ValueVariant;
  // soma antes do �ltimo !!!!
  ReopenCHDevA;
  ReopenOcorA;
  // Normais
  ReopenRiscoC;
  // precisa ser o �ltimo !!!! para Ed?.Text
  ReopenDOpen;
  BtImprime.Enabled := False;
  BtGera.Enabled := True;
  ReopenSacEmiSac;
  Screen.Cursor := crDefault;
end;

procedure TFmRiscoAll_1.QrOcorAAfterOpen(DataSet: TDataSet);
begin
  FOcorA_Total := 0;
  while not QrOcorA.Eof do
  begin
    //if Int(Date) > QrOcorADataO.Value then
      FOcorA_Total := FOcorA_Total + QrOcorAATUALIZADO.Value;
    QrOcorA.Next;
  end;
end;

procedure TFmRiscoAll_1.QrOcorABeforeClose(DataSet: TDataSet);
begin
  FOcorA_Total := 0;
end;

procedure TFmRiscoAll_1.QrOcorACalcFields(DataSet: TDataSet);
begin
  QrOcorASALDO.Value := QrOcorAValor.Value + QrOcorATaxaV.Value - QrOcorAPago.Value;
  //
  QrOcorAATUALIZADO.Value := DMod.ObtemValorAtualizado(
    QrOcorACliente.Value, 1, QrOcorADataO.Value, Date, QrOcorAData3.Value,
    QrOcorAValor.Value, QrOcorATaxaV.Value, 0 (*Desco*),
    QrOcorAPago.Value, QrOcorATaxaP.Value, False);
  //
  if QrOcorADescri.Value <> '' then
    QrOcorADOCUM_TXT.Value := QrOcorADescri.Value
  else if QrOcorATIPODOC.Value = 'CH' then
    QrOcorADOCUM_TXT.Value := FormatFloat('000', QrOcorABanco.Value)+'/'+
    FormatFloat('0000', QrOcorAAgencia.Value)+'/'+ QrOcorAConta.Value+'-'+
    FormatFloat('000000', QrOcorACheque.Value)
  else if QrOcorATIPODOC.Value = 'DU' then
    QrOcorADOCUM_TXT.Value := QrOcorADuplicata.Value
  else QrOcorADOCUM_TXT.Value := '';
  //
  QrOcorACPF_TXT.Value := Geral.FormataCNPJ_TT(QrOcorACPF.Value);
end;

procedure TFmRiscoAll_1.ReopenOcorA();
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True,
    'Pesquisando em tabelas. Ocorr�ncias');
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrOcorA, Dmod.MyDB, [
  'SELECT ELT(TpOcor, "CH", "DU", "CL", "??") TIPODOC, ',
  'ob.Nome NOMEOCORRENCIA, ocr.*, ent.LimiCred, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMECLIENTE ',
  'FROM ocorreu ocr ',
  'LEFT JOIN ocorbank ob ON ob.Codigo   = ocr.Ocorrencia ',
  'LEFT JOIN entidades ent ON ent.Codigo=ocr.Cliente',
  //
  Geral.ATS_if(FColigado <> 0, [
  'LEFT JOIN repasits ri ON ri.Origem   = ocr.' + FLD_LOIS,
  'LEFT JOIN repas    re ON re.Codigo   = ri.Codigo']),
  //
  'WHERE ocr.Status<2',
  //
  Geral.ATS_if(FCliente <> 0, [
  'AND ocr.Cliente=' + FormatFloat('0', FCliente) + ' AND ocr.' + FLD_LOIS + '=0']),
  //
  Geral.ATS_if(FColigado <> 0, [
  'AND re.Coligado=' + FormatFloat('0', FColigado)]),
  //
  Geral.ATS_if(FCPF <> '', [
  'AND ocr.CPF = "' + FCPF + '"']),
  '']);
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '');
end;

procedure TFmRiscoAll_1.ReopenCHDevA;
begin
  QrCHDevA.Close;
  QrCHDevA.SQL.Clear;
  QrCHDevA.SQL.Add('SELECT CASE WHEN en.Tipo=0 THEN en.RazaoSocial');
  QrCHDevA.SQL.Add('ELSE en.Nome END NOMECLIENTE, en.LimiCred, ai.*');
  QrCHDevA.SQL.Add('FROM alinits ai');
  QrCHDevA.SQL.Add('LEFT JOIN entidades en ON en.Codigo=ai.Cliente');
  if FColigado <> 0 then
  begin
    QrCHDevA.SQL.Add('LEFT JOIN repasits ri ON ri.Origem   = ai.ChequeOrigem');
    QrCHDevA.SQL.Add('LEFT JOIN repas    re ON re.Codigo   = ri.Codigo');
  end;
  QrCHDevA.SQL.Add('WHERE ai.Status<2');
  if FCliente <> 0 then
  begin
    QrCHDevA.SQL.Add('AND ai.Cliente=:P0');
    QrCHDevA.Params[0].AsInteger := FCliente;
  end;
  if FColigado <> 0 then
    QrCHDevA.SQL.Add('AND re.Coligado='+IntToStr(FColigado));
  if FEmitente <> '' then
    QrCHDevA.SQL.Add('AND ai.Emitente LIKE "'+FEmitente+'"');
  if FCPF <> '' then
    QrCHDevA.SQL.Add('AND ai.CPF = "'+FCPF+'"');
  QrCHDevA.SQL.Add('ORDER BY ai.Data1, ai.Data2');
  QrCHDevA.SQL.Add('');
  UMyMod.AbreQuery(QrCHDevA, Dmod.MyDB);
end;

procedure TFmRiscoAll_1.ReopenDOpen;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True,
    'Pesquisando em tabelas. Duplicatas abertas');
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrDOpen, Dmod.MyDB, [
  'SELECT ba.Nome NOMEBANCO, od.Nome STATUS, ',
  'lct.FatParcela, lct.Duplicata, lct.Repassado, ',
  'lct.DCompra, lct.Credito, lct.DDeposito, lct.Emitente, lct.CNPJCPF, ',
  'lo.Cliente, lct.Quitado, lct.TotalJr, lct.TotalDs, lct.TotalPg, ',
  'lct.Vencimento, lct.Data3, lct.Banco, lct.Agencia, lct.Data, ',
  'IF(en.Tipo=0, en.RazaoSocial,en.Nome) NOMECLIENTE, en.LimiCred ',
  'FROM ' + CO_TabLctA + ' lct ',
  'LEFT JOIN ' + CO_TabLotA + ' lo ON lo.Codigo=lct.FatNum ',
  'LEFT JOIN ocordupl  od ON od.Codigo=lct.Devolucao ',
  'LEFT JOIN bancos    ba ON ba.Codigo=lct.Banco ',
  'LEFT JOIN entidades en ON en.Codigo=lo.Cliente ',
  //
  Geral.ATS_if(FColigado <> 0, [
  'LEFT JOIN repasits  ri ON ri.Origem=lct.FatParcela ',
  'LEFT JOIN repas     re ON re.Codigo=ri.Codigo ']),
  //
  'WHERE lct.FatID=' + FormatFloat('0', VAR_FATID_0301),
  'AND lo.Tipo=1 ',
  'AND lo.TxCompra + lo.ValValorem + ' +
        IntToStr(FmPrincipal.FConnections) + ' >= 0.01',
  'AND lct.Quitado <> 2 ',
  //
  Geral.ATS_if(FCliente <> 0, [
  'AND lo.Cliente=' + FormatFloat('0', FCliente)]),
  //
  Geral.ATS_if(FColigado <> 0, [
  'AND re.Coligado=' + FormatFloat('0', FColigado)]),
  //
  Geral.ATS_if(FEmitente <> '', [
    'AND lct.Emitente LIKE "' + FEmitente + '" ']),
  //
  Geral.ATS_if(FCPF <> '', [
    'AND lct.CNPJCPF = "' + FCPF + '" ']),
  //
  'ORDER BY lct.Vencimento ',
  '']);
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '');
end;

procedure TFmRiscoAll_1.QrCHDevAAfterOpen(DataSet: TDataSet);
begin
  FCHDevOpen_Total := 0;
  while not QrCHDevA.Eof do
  begin
    //if Int(Date) > QrCHDevAData1.Value then
      FCHDevOpen_Total := FCHDevOpen_Total + QrCHDevAATUAL.Value;
    QrCHDevA.Next;
  end;
end;

procedure TFmRiscoAll_1.QrCHDevABeforeClose(DataSet: TDataSet);
begin
  FCHDevOpen_Total := 0;
end;

procedure TFmRiscoAll_1.QrCHDevACalcFields(DataSet: TDataSet);
begin
  QrCHDevADATA1_TXT.Value := MLAGeral.DataNula('dd/mm/yy', QrCHDevAData1.Value);
  QrCHDevADATA2_TXT.Value := MLAGeral.DataNula('dd/mm/yy', QrCHDevAData2.Value);
  QrCHDevADATA3_TXT.Value := MLAGeral.DataNula('dd/mm/yy', QrCHDevAData3.Value);
  QrCHDevACPF_TXT.Value := Geral.FormataCNPJ_TT(QrCHDevACPF.Value);
  QrCHDevASALDO.Value := QrCHDevAValor.Value +QrCHDevATaxas.Value +
    QrCHDevAMulta.Value + QrCHDevAJurosV.Value - QrCHDevAValPago.Value -
    QrCHDevADesconto.Value;
  //
  QrCHDevAATUAL.Value := DMod.ObtemValorAtualizado(
    QrCHDevACliente.Value, QrCHDevAStatus.Value, QrCHDevAData1.Value, Date,
    QrCHDevAData3.Value, QrCHDevAValor.Value, QrCHDevAJurosV.Value,
    QrCHDevADesconto.Value, QrCHDevAValPago.Value, QrCHDevAJurosP.Value, False);
end;

procedure TFmRiscoAll_1.QrDOpenAfterOpen(DataSet: TDataSet);
var
  DR, DV, DT, CR, CV, CT, RT, VT, ST, OA, TT: Double;
begin
  DR := 0; DV := 0;
  while not QrDOpen.Eof do
  begin
    if Int(Date) > QrDOpenVencimento.Value then
      DV := DV + QrDOpenSALDO_ATUALIZADO.Value
    else
      DR := DR + QrDOpenSALDO_ATUALIZADO.Value;
    QrDOpen.Next;
  end;
  DT := DR+DV;
  QrDOpen.First;
  CR := QrRiscoTCValor.Value;
  CV := FCHDevOpen_Total;
  CT := CR+CV;
  RT := DR+CR;
  VT := DV+CV;
  ST := RT+VT;
  OA := FOcorA_Total;
  TT := ST + OA;
  EdDV.ValueVariant := DV;
  EdDR.ValueVariant := DR;
  EdDT.ValueVariant := DT;
  EdCV.ValueVariant := CV;
  EdCR.ValueVariant := CR;
  EdCT.ValueVariant := CT;
  EdRT.ValueVariant := RT;
  EdVT.ValueVariant := VT;
  EdST.ValueVariant := ST;
  EdOA.ValueVariant := OA;
  EdTT.ValueVariant := TT;
  //
end;

procedure TFmRiscoAll_1.QrDOpenBeforeClose(DataSet: TDataSet);
begin
  EdDV.ValueVariant := 0;
  EdDR.ValueVariant := 0;
  EdDT.ValueVariant := 0;
  EdCV.ValueVariant := 0;
  EdCR.ValueVariant := 0;
  EdCT.ValueVariant := 0;
  EdRT.ValueVariant := 0;
  EdVT.ValueVariant := 0;
  EdST.ValueVariant := 0;
  EdOA.ValueVariant := 0;
  EdTT.ValueVariant := 0;
end;

procedure TFmRiscoAll_1.QrDOpenCalcFields(DataSet: TDataSet);
begin
  QrDOpenSALDO_DESATUALIZ.Value := QrDOpenCredito.Value + QrDOpenTotalJr.Value -
  QrDOpenTotalDs.Value - QrDOpenTotalPg.Value;
  //
  QrDOpenNomeStatus.Value := MLAGeral.NomeStatusPgto2(QrDOpenQuitado.Value,
    QrDOpenDDeposito.Value, Date, QrDOpenData3.Value, QrDOpenRepassado.Value);
  //
  QrDOpenSALDO_ATUALIZADO.Value := DMod.ObtemValorAtualizado(
    QrDOpenCliente.Value, QrDOpenQuitado.Value, QrDOpenVencimento.Value, Date,
    QrDOpenData3.Value, QrDOpenCredito.Value, QrDOpenTotalJr.Value, QrDOpenTotalDs.Value,
    QrDOpenTotalPg.Value, 0, True);
  //
  QrDOpenCPF_TXT.Value := Geral.FormataCNPJ_TT(QrDOpenCNPJCPF.Value);
end;

procedure TFmRiscoAll_1.QrRiscoCCalcFields(DataSet: TDataSet);
begin
  QrRiscoCCPF_TXT.Value := Geral.FormataCNPJ_TT(QrRiscoCCNPJCPF.Value);
end;

procedure TFmRiscoAll_1.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  //
  FORDA_Emisac := 'Emitente';
  FORDB_Emisac := '';
  //UCriar.GerenciaTabelaLocal('Risco', acCreate);
  UMyMod.AbreQuery(QrClientes, Dmod.MyDB);
  UMyMod.AbreQuery(QrColigados, Dmod.MyDB);
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmRiscoAll_1.EdClienteChange(Sender: TObject);
var
  Cliente: Integer;
begin
  DesfazPesquisa;
  Cliente := EdCliente.ValueVariant;
  if Cliente = 0 then EdLimiCredCli.ValueVariant := 0 else
    EdLimiCredCli.ValueVariant := QrClientesLimiCred.Value;
end;

procedure TFmRiscoAll_1.DesfazPesquisa;
begin
  BtImprime.Enabled := False;
  QrOcorA.Close;
  QrCHDevA.Close;
  QrRiscoC.Close;
  QrRiscoTC.Close;
  QrDOpen.Close;
end;

procedure TFmRiscoAll_1.EdColigadoChange(Sender: TObject);
begin
  DesfazPesquisa;
end;

procedure TFmRiscoAll_1.ReopenRiscoC;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True,
    'Pesquisando em tabelas. Risco cheques: Itens');
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrRiscoC, Dmod.MyDB, [
  'SELECT lo.Cliente, lo.Tipo, lct.Banco, lct.Agencia, lct.ContaCorrente, ',
  'lct.Documento, lct.Credito, lct.DCompra, lct.DDeposito, ',
  'lct.Emitente, lct.CNPJCPF, lct.Data, lct.Vencimento, lct.FatParcela, ',
  'IF(en.Tipo=0, en.RazaoSocial,en.Nome) NOMECLIENTE, en.LimiCred ',
  'FROM ' + CO_TabLctA + ' lct ',
  'LEFT JOIN entidades en ON en.Codigo=lo.Cliente ',
  'LEFT JOIN ' + CO_TabLotA + ' lo ON lo.Codigo=lct.FatNum ',
  //
   Geral.ATS_if(FColigado <> 0, [
  'LEFT JOIN repasits ri ON ri.Origem   = lct.FatParcela  ',
  'LEFT JOIN repas    re ON re.Codigo   = ri.Codigo  ']),
   //
  'WHERE lct.FatID=' + FormatFloat('0', VAR_FATID_0301),
  'AND lo.Tipo = 0 ',
  'AND lo.TxCompra + lo.ValValorem + ' +
        IntToStr(FmPrincipal.FConnections) + ' >= 0.01 ',
  'AND (lct.Devolucao=0) AND(DDeposito>=SYSDATE()) ',
   //
  Geral.ATS_if(FCliente <> 0, [
  'AND lo.Cliente=' + FormatFloat('0', FCliente)]),
  //
  Geral.ATS_if(FColigado <> 0, [
  'AND re.Coligado=' + FormatFloat('0', FColigado)]),
  //
  Geral.ATS_if(FEmitente <> '', [
    'AND lct.Emitente LIKE "' + FEmitente + '" ']),
  //
  Geral.ATS_if(FCPF <> '', [
    'AND lct.CNPJCPF = "' + FCPF + '" ']),
  //
  'ORDER BY DDeposito ',
  '']);
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True,
    'Pesquisando em tabelas. Risco cheques: Totais');
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrRiscoTC, Dmod.MyDB, [
  'SELECT SUM(lct.Credito) Valor',
  'FROM ' + CO_TabLctA + ' lct ',
  'LEFT JOIN ' + CO_TabLotA + ' lo ON lo.Codigo=lct.FatNum ',
  //
   Geral.ATS_if(FColigado <> 0, [
  'LEFT JOIN repasits ri ON ri.Origem   = lct.FatParcela  ',
  'LEFT JOIN repas    re ON re.Codigo   = ri.Codigo  ']),
   //
  'WHERE lct.FatID=' + FormatFloat('0', VAR_FATID_0301),
  'AND lo.Tipo = 0 ',
  'AND lo.TxCompra + lo.ValValorem + ' +
        IntToStr(FmPrincipal.FConnections) + ' >= 0.01 ',
  'AND (lct.Devolucao=0) AND(DDeposito>=SYSDATE()) ',
   //
  Geral.ATS_if(FCliente <> 0, [
  'AND lo.Cliente=' + FormatFloat('0', FCliente)]),
  //
  Geral.ATS_if(FColigado <> 0, [
  'AND re.Coligado=' + FormatFloat('0', FColigado)]),
  //
  Geral.ATS_if(FEmitente <> '', [
    'AND lct.Emitente LIKE "' + FEmitente + '" ']),
  Geral.ATS_if(FCPF <> '', [
    'AND lct.CNPJCPF = "' + FCPF + '" ']),
  //
  '']);
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '');

end;

procedure TFmRiscoAll_1.BtImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, BtImprime);
end;

procedure TFmRiscoAll_1.BtCancelaClick(Sender: TObject);
begin
  FParar := True;
end;

function TFmRiscoAll_1.ParaImpressao: Boolean;
begin
  BtCancela.Enabled := False;
  Progress1.Visible := False;
  Result := True;
end;

procedure TFmRiscoAll_1.QrRisco1CalcFields(DataSet: TDataSet);
var
  Liga: String;
begin
  QrRisco1CONTA_DUPLICATA.Value :=
    QrRisco1Conta.Value + QrRisco1Duplicata.Value;
  QrRisco1DEMISSAO_TXT.Value   := Geral.FDT(QrRisco1DEmissao.Value, 3);
  QrRisco1DCOMPRA_TXT.Value    := Geral.FDT(QrRisco1DCompra.Value, 3);
  QrRisco1DVENCE_TXT.Value     := Geral.FDT(QrRisco1DVence.Value, 3);
  QrRisco1DDEPOSITO_TXT.Value  := Geral.FDT(QrRisco1DDeposito.Value, 3);
  QrRisco1DDEVOL1_TXT.Value    := Geral.FDT(QrRisco1DDevol1.Value, 3);
  QrRisco1DDEVOL2_TXT.Value    := Geral.FDT(QrRisco1DDevol2.Value, 3);
  QrRisco1ULTPAGTO_TXT.Value   := Geral.FDT(QrRisco1UltPagto.Value, 3);
////////////////////////////////////////////////////////////////////////////////
  case QrRisco1Tipo.Value of
    10: QrRisco1TIPODOC.Value := 'CH CART';
    20: QrRisco1TIPODOC.Value := 'CH DEVO';
    30: QrRisco1TIPODOC.Value := 'DU CART';
    40: QrRisco1TIPODOC.Value := 'DU DEVO';
    50: QrRisco1TIPODOC.Value := 'CL OCOR';
    60: QrRisco1TIPODOC.Value := 'CH OCOR';
    70: QrRisco1TIPODOC.Value := 'DU OCOR';
  end;
////////////////////////////////////////////////////////////////////////////////
  QrRisco1CONTAGEM.Value := 1;
////////////////////////////////////////////////////////////////////////////////
  if (QrRisco1Status.Value <> '') and (QrRisco1Historico.Value <> '') then
    Liga := ' - ' else Liga := '';
  QrRisco1STATUS_HISTORICO.Value := QrRisco1Status.Value + Liga +
    QrRisco1Historico.Value;
end;

procedure TFmRiscoAll_1.frRisco1GetValue(const ParName: String;
  var ParValue: Variant);
begin
  if ParName = 'VARF_CLIENTE' then
  begin
    if CBCliente.KeyValue = NULL then ParValue := ' ' else
    ParValue := CBCliente.Text;
  end else if ParName = 'VARF_TODOS' then
  begin
    if CBCliente.KeyValue = NULL then ParValue := 'TODOS CLIENTES' else
    ParValue := ' ';
  end else if ParName = 'VARF_QTD_CHEQUES' then ParValue := Progress1.Max
  else if ParName = 'VFR_LA1NOME' then
    ParValue := ImpressaoDeCabecalho(RGOrdem1.ItemIndex)
  else if ParName = 'VFR_LA2NOME' then
    ParValue := ImpressaoDeCabecalho(RGOrdem2.ItemIndex)
  else if ParName = 'VFR_LA3NOME' then
    ParValue := ImpressaoDeCabecalho(RGOrdem3.ItemIndex)
  else if ParName = 'VARF_SITUACOES' then
  begin
    Parvalue := ' ';
    if Ck10.Checked then ParValue := ParValue + '[' + Ck10.Caption + ']';
    if Ck20.Checked then ParValue := ParValue + '[' + Ck20.Caption + ']';
    if Ck30.Checked then ParValue := ParValue + '[' + Ck30.Caption + ']';
    if Ck40.Checked then ParValue := ParValue + '[' + Ck40.Caption + ']';
    if Ck50.Checked then ParValue := ParValue + '[' + Ck50.Caption + ']';
    if Ck60.Checked then ParValue := ParValue + '[' + Ck60.Caption + ']';
    if Ck70.Checked then ParValue := ParValue + '[' + Ck70.Caption + ']';
    //if Ck00.Checked then ParValue := ParValue + ' (Resumido)';
    if Trim(ParValue) = '' then ParValue := '(??????)';
  end
  else if ParName = 'VARF_FILTROS' then
  begin
    ParValue := '';
    if EdColigado.ValueVariant <> 0 then ParValue := ParValue +
      'Coligado: '+CBColigado.Text+ '  ';
    if FEmitente <> '' then ParValue := ParValue + '  {Emitente: '+ FEmitente+'}';
    if FCPF <> '' then ParValue := ParValue + '  {CPF/CNPJ: '+
    Geral.FormataCNPJ_TT(FCPF)+'}';
    if ParValue <> '' then ParValue := 'FILTROS : '+ParValue;
  end;
end;

procedure TFmRiscoAll_1.frRisco1UserFunction(const Name: String; p1, p2,
  p3: Variant; var Val: Variant);
begin
       if Name = 'VFR_ORD1' then Val := RGAgrupa.ItemIndex >= 1
  else if Name = 'VFR_ORD2' then Val := RGAgrupa.ItemIndex >= 2
  else if Name = 'VFR_ORD3' then Val := RGAgrupa.ItemIndex >= 3
  else if Name = 'VFR_IDX1' then Val := RGOrdem1.ItemIndex
  else if Name = 'VFR_IDX2' then Val := RGOrdem2.ItemIndex
  else if Name = 'VFR_IDX3' then Val := RGOrdem3.ItemIndex
  else if Name = 'VFR_CON1' then Val := CondicaoDeImpressao(RGOrdem1.ItemIndex)
  else if Name = 'VFR_CON2' then Val := CondicaoDeImpressao(RGOrdem2.ItemIndex)
  else if Name = 'VFR_CON3' then Val := CondicaoDeImpressao(RGOrdem3.ItemIndex)

end;

function TFmRiscoAll_1.ImpressaoDeCabecalho(Tipo: Integer): String;
begin
  case Tipo of
    0: Result := 'Cliente: '+QrRisco1NOMECLIENTE.Value;
    1: Result := DescricaoDeDocumento(QrRisco1Tipo.Value);
    2: Result := 'Vencimento: '+QrRisco1DVENCE_TXT.Value;
    3: Result := 'Status: '+QrRisco1Status.Value;
  end;
end;

function TFmRiscoAll_1.CondicaoDeImpressao(Tipo: Integer): String;
begin     
  case Tipo of
    0: Result := QuotedStr('[QrRisco1."NOMECLIENTE"]');
    1: Result := QuotedStr('[QrRisco1."TIPODOC"]');
    2: Result := QuotedStr('[QrRisco1."DVENCE_TXT"]');
    3: Result := QuotedStr('[QrRisco1."Status"]');
  end;
end;

function TFmRiscoAll_1.Ordem(Tipo: Integer): String;
begin
  case Tipo of
    0: Result := 'NOMECLIENTE';
    1: Result := 'Tipo';
    2: Result := 'DVence';
    3: Result := 'Status';
  end;
end;

function TFmRiscoAll_1.DescricaoDeDocumento(Tipo: Integer): String;
begin
  case Tipo of
    10: Result := 'Cheques em carteira';
    20: Result := 'Cheques devolvidos';
    30: Result := 'Duplicatas em carteira';
    40: Result := 'Duplicatas vencidas';
    50: Result := 'Ocorr�ncias em clientes';
    60: Result := 'Ocorr�ncias de cheques';
    70: Result := 'Ocorr�ncias de duplicatas';
    else Result := '??????????????????????????????????????????';
  end;
end;

procedure TFmRiscoAll_1.BtGeraClick(Sender: TObject);
const
  Campos = 'INSERT INTO risco (' +
'Tipo,Banco,Agencia,Conta,Duplicata,Cheque,DEmissao,DCompra,DVence,' +
'DDeposito,DDevol1,DDevol2,UltPagto,Valor,Taxas,Saldo,Pago,Atual,Emitente,' +
'CPF,Alinea1,Alinea2,Status,Historico,Desco,Juros,Cliente,Controle,' +
'Devolvido,LimiCred,NOMECLIENTE) VALUES (';

var
  Cliente, Tipo: Integer;
  ValDev: Double;
begin
  Screen.Cursor := crHourGlass;
  try
    MyObjects.Informa2(LaAviso1, LaAviso2, True,
      'Gerando registros: Risco cheques');
    Tipo := 0;
    FParar := False;
    BtCancela.Enabled := True;
    Progress1.Position := 0;
    Progress1.Visible := True;
    Progress1.Max := QrRiscoC.RecordCount + QrCHDevA.RecordCount +
      QrDOpen.RecordCount + QrOcorA.RecordCount;
    QmRisco.Close;
    QmRisco.SQL.Clear;
    QmRisco.SQL.Add('DROP TABLE Risco;                                ');
    QmRisco.SQL.Add('CREATE TABLE Risco (                             ');
    QmRisco.SQL.Add('  Cliente      integer  NOT NULL DEFAULT "0",    ');
    QmRisco.SQL.Add('  Controle     integer  NOT NULL DEFAULT "0",    ');
    QmRisco.SQL.Add('  Tipo         integer  NOT NULL DEFAULT "0",    ');
    QmRisco.SQL.Add('  Banco        integer  NOT NULL DEFAULT "0",    ');
    QmRisco.SQL.Add('  Agencia      integer  NOT NULL DEFAULT "0",    ');
    QmRisco.SQL.Add('  Conta        varchar(30)  ,                    ');
    QmRisco.SQL.Add('  Duplicata    varchar(50)  ,                    ');
    QmRisco.SQL.Add('  Cheque       integer  NOT NULL DEFAULT "0",    ');
    QmRisco.SQL.Add('  DEmissao     date,                             ');
    QmRisco.SQL.Add('  DCompra      date,                             ');
    QmRisco.SQL.Add('  DVence       date,                             ');
    QmRisco.SQL.Add('  DDeposito    date,                             ');
    QmRisco.SQL.Add('  DDevol1      date,                             ');
    QmRisco.SQL.Add('  DDevol2      date,                             ');
    QmRisco.SQL.Add('  UltPagto     date,                             ');
    QmRisco.SQL.Add('  Valor        float NOT NULL DEFAULT "0.00",    ');
    QmRisco.SQL.Add('  Taxas        float NOT NULL DEFAULT "0.00",    ');
    QmRisco.SQL.Add('  Saldo        float NOT NULL DEFAULT "0.00",    ');
    QmRisco.SQL.Add('  Juros        float NOT NULL DEFAULT "0.00",    ');
    QmRisco.SQL.Add('  Desco        float NOT NULL DEFAULT "0.00",    ');
    QmRisco.SQL.Add('  Pago         float NOT NULL DEFAULT "0.00",    ');
    QmRisco.SQL.Add('  Atual        float NOT NULL DEFAULT "0.00",    ');
    QmRisco.SQL.Add('  Devolvido    float NOT NULL DEFAULT "0.00",    ');
    QmRisco.SQL.Add('  Emitente     varchar(100),                     ');
    QmRisco.SQL.Add('  CPF          varchar(30)  ,                    ');
    QmRisco.SQL.Add('  Alinea1      integer  NOT NULL DEFAULT "0",    ');
    QmRisco.SQL.Add('  Alinea2      integer  NOT NULL DEFAULT "0",    ');
    QmRisco.SQL.Add('  Status       varchar(100),                     ');
    QmRisco.SQL.Add('  Historico    varchar(100),                     ');
    QmRisco.SQL.Add('  LimiCred     float NOT NULL DEFAULT "0.00",    ');
    QmRisco.SQL.Add('  NOMECLIENTE  varchar(100)                      ');
    QmRisco.SQL.Add(');                                               ');
    //
    QrRiscoC.First;
    while not QrRiscoC.Eof do
    begin
      Progress1.Position := Progress1.Position + 1;
      Application.ProcessMessages;
      if FParar then if ParaImpressao then Exit;

      QmRisco.SQL.Add(Campos +
      {00}MLAGeral.FormatABSLn(10) +
      {01}MLAGeral.FormatABSLn(QrRiscoCBanco.Value) +
      {02}MLAGeral.FormatABSLn(QrRiscoCAgencia.Value) +
      {03}MLAGeral.FormatABSLn(QrRiscoCContaCorrente.Value) +
      {04}MLAGeral.FormatABSLn('') +
      {05}MLAGeral.FormatABSLn(QrRiscoCDocumento.Value) +
      {06}MLAGeral.FormatABSLn(QrRiscoCData.Value) +
      {07}MLAGeral.FormatABSLn(QrRiscoCDCompra.Value) +
      {08}MLAGeral.FormatABSLn(QrRiscoCVencimento.Value) +
      {09}MLAGeral.FormatABSLn(QrRiscoCDDeposito.Value) +
      {10}MLAGeral.FormatABSLn(IncDay(0,0)) +
      {11}MLAGeral.FormatABSLn(IncDay(0,0)) +
      {12}MLAGeral.FormatABSLn(IncDay(0,0)) +
      {13}MLAGeral.FormatABSLn(QrRiscoCCredito.Value) +
      {14}MLAGeral.FormatABSLn(0) + //Taxas.Value
      {15}MLAGeral.FormatABSLn(QrRiscoCCredito.Value) + //Saldo.Value
      {16}MLAGeral.FormatABSLn(0) + //Pago.Value
      {17}MLAGeral.FormatABSLn(QrRiscoCCredito.Value) + //Atual.Value
      {18}MLAGeral.FormatABSLn(QrRiscoCEmitente.Value) +
      {19}MLAGeral.FormatABSLn(QrRiscoCCPF_TXT.Value) +
      {20}MLAGeral.FormatABSLn(0) +
      {21}MLAGeral.FormatABSLn(0) +
      {22}MLAGeral.FormatABSLn('') +
      {23}MLAGeral.FormatABSLn('') +
      {24}MLAGeral.FormatABSLn(0) +
      {25}MLAGeral.FormatABSLn(0) +
      {26}MLAGeral.FormatABSLn(QrRiscoCCliente.Value) +
      {27}MLAGeral.FormatABSLn(QrRiscoCFatParcela.Value) +
      {28}MLAGeral.FormatABSLn(0) +
      {29}MLAGeral.FormatABSLn(QrRiscoCLimiCred.Value) +
      {30}MLAGeral.FormatABS(QrRiscoCNOMECLIENTE.Value) +
      ');');
      QrRiscoC.Next;
    end;

    MyObjects.Informa2(LaAviso1, LaAviso2, True,
      'Gerando registros: Cheques devolvidos');
    QrCHDevA.First;
    while not QrCHDevA.Eof do
    begin
      Progress1.Position := Progress1.Position + 1;
      Application.ProcessMessages;
      if FParar then if ParaImpressao then Exit;
      //
      QmRisco.SQL.Add(Campos +
      {00}MLAGeral.FormatABSLn(20) +
      {01}MLAGeral.FormatABSLn(QrCHDevABanco.Value) +
      {02}MLAGeral.FormatABSLn(QrCHDevAAgencia.Value) +
      {03}MLAGeral.FormatABSLn(QrCHDevAConta.Value) +
      {04}MLAGeral.FormatABSLn('') +
      {05}MLAGeral.FormatABSLn(QrCHDevACheque.Value) +
      {06}MLAGeral.FormatABSLn(IncDay(0,0)) +                                                 //Geral.FDT(QrCHDevAEmissao.Value, 1);
      {07}MLAGeral.FormatABSLn(IncDay(0,0)) +                                                 //Geral.FDT(QrCHDevADCompra.Value, 1);
      {08}MLAGeral.FormatABSLn(IncDay(0,0)) +                                                 //Geral.FDT(QrCHDevAVencto.Value, 1);
      {09}MLAGeral.FormatABSLn(IncDay(0,0)) +                                                 //Geral.FDT(QrCHDevADDeposito.Value, 1);
      {10}MLAGeral.FormatABSLn(QrCHDevAData1.Value) +
      {11}MLAGeral.FormatABSLn(QrCHDevAData2.Value) +
      {12}MLAGeral.FormatABSLn(QrCHDevAData3.Value) +
      {13}MLAGeral.FormatABSLn(QrCHDevAValor.Value) +
      {14}MLAGeral.FormatABSLn(QrCHDevATaxas.Value) +
      {15}MLAGeral.FormatABSLn(QrCHDevASaldo.Value) +
      {16}MLAGeral.FormatABSLn(QrCHDevAValPago.Value) +
      {17}MLAGeral.FormatABSLn(QrCHDevAAtual.Value) +
      {18}MLAGeral.FormatABSLn(QrCHDevAEmitente.Value) +
      {19}MLAGeral.FormatABSLn(QrCHDevACPF_TXT.Value) +
      {20}MLAGeral.FormatABSLn(QrCHDevAAlinea1.Value) +
      {21}MLAGeral.FormatABSLn(QrCHDevAAlinea2.Value) +
      {22}MLAGeral.FormatABSLn('') +
      {23}MLAGeral.FormatABSLn('') +
      {24}MLAGeral.FormatABSLn(0) +
      {25}MLAGeral.FormatABSLn(0) +
      {26}MLAGeral.FormatABSLn(QrCHDevACliente.Value) +
      {27}MLAGeral.FormatABSLn(QrCHDevACodigo.Value) +
      {28}MLAGeral.FormatABSLn(QrCHDevAValor.Value) +
      {29}MLAGeral.FormatABSLn(QrCHDevALimiCred.Value) +
      {30}MLAGeral.FormatABS(QrCHDevANOMECLIENTE.Value) +
      ');');

      //
      QrCHDevA.Next;
    end;

    MyObjects.Informa2(LaAviso1, LaAviso2, True,
      'Gerando registros: Duplicatas abertas');
    QrDOpen.First;
    while not QrDOpen.Eof do
    begin
      Progress1.Position := Progress1.Position + 1;
      Application.ProcessMessages;
      if FParar then if ParaImpressao then Exit;
      if Int(Date) > QrDOpenVencimento.Value then
      begin
        ValDev := QrDOpenCredito.Value;
        Tipo := 40;
      end else begin
        Tipo := 30;
        ValDev := 0;
      end;
      QmRisco.SQL.Add(Campos +
      {00}MLAGeral.FormatABSLn(Tipo) +
      {01}MLAGeral.FormatABSLn(QrDOpenBanco.Value) +
      {02}MLAGeral.FormatABSLn(QrDOpenAgencia.Value) +
      {03}MLAGeral.FormatABSLn('') + //QrDOpenConta.Value) +
      {04}MLAGeral.FormatABSLn(QrDOpenDuplicata.Value) +
      {05}MLAGeral.FormatABSLn(0) + //Cheque.Value) +
      {06}MLAGeral.FormatABSLn(QrDOpenData.Value) +
      {07}MLAGeral.FormatABSLn(QrDOpenDCompra.Value) +
      {08}MLAGeral.FormatABSLn(QrDOpenVencimento.Value) +
      {09}MLAGeral.FormatABSLn(QrDOpenDDeposito.Value) +
      {10}MLAGeral.FormatABSLn(QrDOpenVencimento.Value) +
      {11}MLAGeral.FormatABSLn(IncDay(0,0)) + //QrDOpenData2.Value) +
      {12}MLAGeral.FormatABSLn(QrDOpenData3.Value) +
      {13}MLAGeral.FormatABSLn(QrDOpenCredito.Value) +
      {14}MLAGeral.FormatABSLn(0) +
      {15}MLAGeral.FormatABSLn(QrDOpenSALDO_DESATUALIZ.Value) +
      {16}MLAGeral.FormatABSLn(QrDOpenTotalPg.Value) +
      {17}MLAGeral.FormatABSLn(QrDOpenSALDO_ATUALIZADO.Value) +
      {18}MLAGeral.FormatABSLn(QrDOpenEmitente.Value) +
      {19}MLAGeral.FormatABSLn(Geral.FormataCNPJ_TT(QrDOpenCNPJCPF.Value)) +
      {20}MLAGeral.FormatABSLn(0) + //QrDOpenAlinea1.Value) +
      {21}MLAGeral.FormatABSLn(0) + //QrDOpenAlinea2.Value) +
      {22}MLAGeral.FormatABSLn(QrDOpenNOMESTATUS.Value) +
      {23}MLAGeral.FormatABSLn(QrDOpenSTATUS.Value) +
      {24}MLAGeral.FormatABSLn(QrDOpenTotalDs.Value) +
      {25}MLAGeral.FormatABSLn(QrDOpenTotalJr.Value) +
      {26}MLAGeral.FormatABSLn(QrDOpenCliente.Value) +
      {27}MLAGeral.FormatABSLn(QrDOpenFatParcela.Value) +
      {28}MLAGeral.FormatABSLn(ValDev) +
      {29}MLAGeral.FormatABSLn(QrDOpenLimiCred.Value) +
      {30}MLAGeral.FormatABS(QrDOpenNOMECLIENTE.Value) +
      ');');
      QrDOpen.Next;
    end;

    MyObjects.Informa2(LaAviso1, LaAviso2, True,
      'Gerando registros: Ocorr�ncias');
    QrOcorA.First;
    while not QrOcorA.Eof do
    begin
      Progress1.Position := Progress1.Position + 1;
      Application.ProcessMessages;
      if FParar then if ParaImpressao then Exit;
      if QrOcorACliente.Value <> 0 then Cliente := QrOcorACliente.Value
      else Cliente := QrOcorACliente.Value;
      if QrOcorATIPODOC.Value = 'CL' then Tipo := 50 else
      if QrOcorATIPODOC.Value = 'CH' then Tipo := 60 else
      if QrOcorATIPODOC.Value = 'DU' then Tipo := 70;

      QmRisco.SQL.Add(Campos +
      {00}MLAGeral.FormatABSLn(Tipo) +
      {01}MLAGeral.FormatABSLn(QrOcorABanco.Value) +
      {02}MLAGeral.FormatABSLn(QrOcorAAgencia.Value) +
      {03}MLAGeral.FormatABSLn(QrOcorAConta.Value) +
      {04}MLAGeral.FormatABSLn(QrOcorADuplicata.Value) +
      {05}MLAGeral.FormatABSLn(QrOcorACheque.Value) +
      {06}MLAGeral.FormatABSLn(QrOcorAEmissao.Value) +
      {07}MLAGeral.FormatABSLn(QrOcorADCompra.Value) +
      {08}MLAGeral.FormatABSLn(QrOcorAVencto.Value) +
      {09}MLAGeral.FormatABSLn(QrOcorADDeposito.Value) +
      {10}MLAGeral.FormatABSLn(QrOcorADataO.Value) +
      {11}MLAGeral.FormatABSLn(IncDay(0,0)) + //QrOcorAData2.Value) +
      {12}MLAGeral.FormatABSLn(QrOcorAData3.Value) +
      {13}MLAGeral.FormatABSLn(QrOcorAValor.Value) +
      {14}MLAGeral.FormatABSLn(QrOcorATaxaV.Value) +
      {15}MLAGeral.FormatABSLn(QrOcorASALDO.Value) +
      {16}MLAGeral.FormatABSLn(QrOcorAPago.Value) +
      {17}MLAGeral.FormatABSLn(QrOcorAATUALIZADO.Value) +
      {18}MLAGeral.FormatABSLn(QrOcorAEmitente.Value) +
      {19}MLAGeral.FormatABSLn(Geral.FormataCNPJ_TT(QrOcorACPF.Value)) +
      {20}MLAGeral.FormatABSLn(0) + //QrOcorAAlinea1.Value) +
      {21}MLAGeral.FormatABSLn(0) + //QrOcorAAlinea2.Value) +
      {22}MLAGeral.FormatABSLn('') + //QrOcorANOMESTATUS.Value) +
      {23}MLAGeral.FormatABSLn(QrOcorANOMEOCORRENCIA.Value) + //QrOcorASTAT
      {24}MLAGeral.FormatABSLn(0) + //QrOcorATotalDs.Value) +
      {25}MLAGeral.FormatABSLn(0) + //QrOcorATotalJr.Value) +
      {26}MLAGeral.FormatABSLn(Cliente) +
      {27}MLAGeral.FormatABSLn(QrOcorACodigo.Value) +
      {28}MLAGeral.FormatABSLn(QrOcorAValor.Value) +
      {29}MLAGeral.FormatABSLn(QrOcorALimiCred.Value) +
      {30}MLAGeral.FormatABS(QrOcorANOMECLIENTE.Value) +
      ');');
      QrOcorA.Next;
    end;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True,
      'Abrindo tabela');
    QmRisco.SQL.Add('SELECT * FROM risco;');
    DmkABS_PF.AbreQuery(QmRisco);
    //
    Progress1.Visible := False;
    BtImprime.Enabled := True;
    BtGera.Enabled    := False;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '');
  finally
    BtCancela.Enabled := False;
    Screen.Cursor     := crDefault;
  end;
end;

procedure TFmRiscoAll_1.Descritivo1Click(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
    DefineTipos;
    QmRisco1.Close;
    QmRisco1.SQL.Clear;
    QmRisco1.SQL.Add('SELECT ri.*');
    QmRisco1.SQL.Add('FROM risco ri');
    QmRisco1.SQL.Add('WHERE ri.Tipo in (:P10, :P20, :P30, :P40, :P50, :P60, :P70)');
    QmRisco1.SQL.Add('ORDER BY '+Ordem(RGOrdem1.ItemIndex)+', '+
    Ordem(RGOrdem2.ItemIndex)+', '+Ordem(RGOrdem3.ItemIndex));
    QmRisco1.Params[00].AsInteger := FR10;
    QmRisco1.Params[01].AsInteger := FR20;
    QmRisco1.Params[02].AsInteger := FR30;
    QmRisco1.Params[03].AsInteger := FR40;
    QmRisco1.Params[04].AsInteger := FR50;
    QmRisco1.Params[05].AsInteger := FR60;
    QmRisco1.Params[06].AsInteger := FR70;
    DmkABS_PF.AbreQuery(QmRisco1);
    //
    Screen.Cursor := crDefault;
    MyObjects.frxMostra(frxRisco1, 'Risco Descritivo');
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmRiscoAll_1.Analtico1Click(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
    DefineTipos;
    QmAnalitico.Close;
    QmAnalitico.SQL.Clear;
    QmAnalitico.SQL.Add('SELECT ri.Cliente, ri.Tipo, SUM(ri.Valor) Valor,');
    QmAnalitico.SQL.Add('SUM(ri.Taxas) Taxas, SUM(ri.Saldo) Saldo, ');
    QmAnalitico.SQL.Add('SUM(ri.Juros) Juros, SUM(ri.Desco) Desco, ');
    QmAnalitico.SQL.Add('SUM(ri.Pago) Pago, SUM(ri.Atual) Atual, ');
    QmAnalitico.SQL.Add('SUM(ri.Devolvido) Devolvido, COUNT(ri.Controle) ITENS,');
    QmAnalitico.SQL.Add('ri.NOMECLIENTE');
    QmAnalitico.SQL.Add('FROM risco ri');
    QmAnalitico.SQL.Add('WHERE ri.Tipo in (:P10, :P20, :P30, :P40, :P50, :P60, :P70)');
    QmAnalitico.SQL.Add('GROUP BY NOMECLIENTE, ri.Tipo, ri.Cliente');
    QmAnalitico.Params[00].AsInteger := FR10;
    QmAnalitico.Params[01].AsInteger := FR20;
    QmAnalitico.Params[02].AsInteger := FR30;
    QmAnalitico.Params[03].AsInteger := FR40;
    QmAnalitico.Params[04].AsInteger := FR50;
    QmAnalitico.Params[05].AsInteger := FR60;
    QmAnalitico.Params[06].AsInteger := FR70;
    DmkABS_PF.AbreQuery(QmAnalitico);
    //
    Screen.Cursor := crDefault;
    MyObjects.frxMostra(frxAnalitico, 'Risco Anal�tico');
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmRiscoAll_1.QmAnaliticoCalcFields(DataSet: TDataSet);
begin
  QmAnaliticoDESCRITIPO.Value := DescricaoDeDocumento(QmAnaliticoTipo.Value);
end;

procedure TFmRiscoAll_1.QmRisco1CalcFields(DataSet: TDataSet);
var
  Liga: String;
begin
  QmRisco1CONTA_DUPLICATA.Value :=
    QmRisco1Conta.Value + QmRisco1Duplicata.Value;
  QmRisco1DEMISSAO_TXT.Value   := Geral.FDT(QmRisco1DEmissao.Value, 3);
  QmRisco1DCOMPRA_TXT.Value    := Geral.FDT(QmRisco1DCompra.Value, 3);
  QmRisco1DVENCE_TXT.Value     := Geral.FDT(QmRisco1DVence.Value, 3);
  QmRisco1DDEPOSITO_TXT.Value  := Geral.FDT(QmRisco1DDeposito.Value, 3);
  QmRisco1DDEVOL1_TXT.Value    := Geral.FDT(QmRisco1DDevol1.Value, 3);
  QmRisco1DDEVOL2_TXT.Value    := Geral.FDT(QmRisco1DDevol2.Value, 3);
  QmRisco1ULTPAGTO_TXT.Value   := Geral.FDT(QmRisco1UltPagto.Value, 3);
////////////////////////////////////////////////////////////////////////////////
  case QmRisco1Tipo.Value of
    10: QmRisco1TIPODOC.Value := 'CH CART';
    20: QmRisco1TIPODOC.Value := 'CH DEVO';
    30: QmRisco1TIPODOC.Value := 'DU CART';
    40: QmRisco1TIPODOC.Value := 'DU DEVO';
    50: QmRisco1TIPODOC.Value := 'CL OCOR';
    60: QmRisco1TIPODOC.Value := 'CH OCOR';
    70: QmRisco1TIPODOC.Value := 'DU OCOR';
  end;
////////////////////////////////////////////////////////////////////////////////
  QmRisco1CONTAGEM.Value := 1;
////////////////////////////////////////////////////////////////////////////////
  if (QmRisco1Status.Value <> '') and (QmRisco1Historico.Value <> '') then
    Liga := ' - ' else Liga := '';
  QmRisco1STATUS_HISTORICO.Value := QmRisco1Status.Value + Liga +
    QmRisco1Historico.Value;
end;

procedure TFmRiscoAll_1.QmSinteticoCalcFields(DataSet: TDataSet);
begin
  if QmSinteticoLimiCred.Value = 0 then QmSinteticoULO.Value := 0 else
  QmSinteticoULO.Value := QmSinteticoAtual.Value / QmSinteticoLimiCred.Value * 100;
end;

procedure TFmRiscoAll_1.QrAnaliticoCalcFields(DataSet: TDataSet);
begin
  QrAnaliticoDESCRITIPO.Value := DescricaoDeDocumento(QrAnaliticoTipo.Value);
end;

procedure TFmRiscoAll_1.Sinttico1Click(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
    DefineTipos;
    //
    QmSintetico.Close;
    QmSintetico.SQL.Clear;
    QmSintetico.SQL.Add('SELECT ri.LimiCred, ');
    QmSintetico.SQL.Add('SUM(ri.Valor) Valor, SUM(ri.Taxas) Taxas, ');
    QmSintetico.SQL.Add('SUM(ri.Saldo) Saldo, SUM(ri.Juros) Juros,');
    QmSintetico.SQL.Add('SUM(ri.Desco) Desco, SUM(ri.Pago) Pago, ');
    QmSintetico.SQL.Add('SUM(ri.Atual) Atual, SUM(ri.Devolvido) Devolvido, ');
    QmSintetico.SQL.Add('COUNT(ri.Controle) ITENS, ri.NOMECLIENTE');
    QmSintetico.SQL.Add('FROM risco ri');
    QmSintetico.SQL.Add('WHERE ri.Tipo in (:P10, :P20, :P30, :P40, :P50, :P60, :P70)');
    QmSintetico.SQL.Add('GROUP BY ri.NOMECLIENTE, LimiCred');
    QmSintetico.Params[00].AsInteger := FR10;
    QmSintetico.Params[01].AsInteger := FR20;
    QmSintetico.Params[02].AsInteger := FR30;
    QmSintetico.Params[03].AsInteger := FR40;
    QmSintetico.Params[04].AsInteger := FR50;
    QmSintetico.Params[05].AsInteger := FR60;
    QmSintetico.Params[06].AsInteger := FR70;
    DmkABS_PF.AbreQuery(QmSintetico);
    //
    Screen.Cursor := crDefault;
    MyObjects.frxMostra(frxSintetico, 'Risco Sint�tico');
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmRiscoAll_1.DefineTipos;
begin
  FR10 := MLAGeral.BoolToInt2(Ck10.Checked, 10, -1000);
  FR20 := MLAGeral.BoolToInt2(Ck20.Checked, 20, -1000);
  FR30 := MLAGeral.BoolToInt2(Ck30.Checked, 30, -1000);
  FR40 := MLAGeral.BoolToInt2(Ck40.Checked, 40, -1000);
  FR50 := MLAGeral.BoolToInt2(Ck50.Checked, 50, -1000);
  FR60 := MLAGeral.BoolToInt2(Ck60.Checked, 60, -1000);
  FR70 := MLAGeral.BoolToInt2(Ck70.Checked, 70, -1000);
end;

procedure TFmRiscoAll_1.ReopenSacEmiSac;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSacEmi, Dmod.MyDB, [
  'SELECT DISTINCT lct.Emitente, lct.CNPJCPF ',
  'FROM ' + CO_TabLctA + ' lct ',
  'WHERE lct.FatID=' + FormatFloat('0', VAR_FATID_0301),
  'AND lct.FatParcela > -1000 ',
  'AND lct.Emitente LIKE "' + FEmitente + '" ',
  'AND lct.CNPJCPF = "' + FCPF + '" ',
  '']);
end;

procedure TFmRiscoAll_1.DBGrid2TitleClick(Column: TColumn);
begin
  FORDA_EmiSac := Column.FieldName;
  if FORDA_EmiSac = 'CPF_TXT' then FORDA_EmiSac := 'CNPJCPF';
  FORDB_Emisac := MLAGeral.InverteOrdemAsc(FORDB_Emisac);
  ReopenSacEmiSac;
end;

procedure TFmRiscoAll_1.EdEmitenteChange(Sender: TObject);
begin
  DesfazPesquisa;
end;

procedure TFmRiscoAll_1.EdCPFChange(Sender: TObject);
begin
  DesfazPesquisa;
end;

procedure TFmRiscoAll_1.RGMascaraClick(Sender: TObject);
begin
  DesfazPesquisa;
end;

procedure TFmRiscoAll_1.DBGrid2DblClick(Sender: TObject);
var
  Emitente, CPF: String;
begin
  Emitente := QrSacEmiEmitente.Value;
  CPF      := QrSacEmiCPF_TXT.Value;
  //
  EdEmitente.Text := '';
  EdCPF.Text := CPF;
  //
  DesfazPesquisa;
  BtPesquisaClick(Self);
end;

procedure TFmRiscoAll_1.QrSinteticoCalcFields(DataSet: TDataSet);
begin
  if QrSinteticoLimiCred.Value = 0 then QrSinteticoULO.Value := 0 else
  QrSinteticoULO.Value := QrSinteticoAtual.Value / QrSinteticoLimiCred.Value * 100;
end;

procedure TFmRiscoAll_1.QrSacEmiCalcFields(DataSet: TDataSet);
begin
  QrSacEmiCPF_TXT.Value := Geral.FormataCNPJ_TT(QrSacEmiCNPJCPF.Value);
end;

procedure TFmRiscoAll_1.BtHistoricoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMHistorico, BtHistorico);
end;

procedure TFmRiscoAll_1.PMHistoricoPopup(Sender: TObject);
var
  Sim: Boolean;
begin
  if EdCliente.ValueVariant <> 0 then Clientepesquisado1.Enabled := True
  else Clientepesquisado1.Enabled := False;
  //
  if EdCPF.Text <> '' then Emitentesacadopesquisado1.Enabled := True
  else Emitentesacadopesquisado1.Enabled := False;
  //
  if (Clientepesquisado1.Enabled = False)
  or (Emitentesacadopesquisado1.Enabled = False) then
    Ambospesquisados1.Enabled := False else Ambospesquisados1.Enabled := True;
  //
  case PageControl1.ActivePageIndex of
    0: Sim := (QrRiscoC.State = dsBrowse) and (QrRiscoC.RecordCount > 0);
    1: Sim := (QrCHDevA.State = dsBrowse) and (QrCHDevA.RecordCount > 0);
    2: Sim := (QrDOpen.State  = dsBrowse) and (QrDOpen.RecordCount  > 0);
    3: Sim := (QrOcorA.State  = dsBrowse) and (QrOcorA.RecordCount  > 0);
    5: Sim := (QrSacEmi.State = dsBrowse) and (QrSacEmi.RecordCount > 0);
    else Sim := False;
  end;
  Emitentesacadoselecionado1.Enabled := Sim;
  EmitentesacadoselecionadoClientepesquisado1.Enabled :=
    (Sim and (EdCliente.ValueVariant <> 0));
end;

procedure TFmRiscoAll_1.MostraHistorico(Quem: Integer);
begin
  Application.CreateForm(TFmHistCliEmi, FmHistCliEmi);
  case Quem of
    1:
    begin
      FmHistCliEmi.EdCliente.ValueVariant      := EdCliente.ValueVariant;
      FmHistCliEmi.CBCliente.KeyValue  := EdCliente.ValueVariant;
    end;
    2: FmHistCliEmi.EdCPF1.Text        := EdCPF.Text;
    3:
    begin
      FmHistCliEmi.EdCliente.ValueVariant      := EdCliente.ValueVariant;
      FmHistCliEmi.CBCliente.KeyValue  := EdCliente.ValueVariant;
      FmHistCliEmi.EdCPF1.Text         := EdCPF.Text;
    end;
    4:
    begin
      case PageControl1.ActivePageIndex of
        0: FmHistCliEmi.EdCPF1.Text    := QrRiscoCCPF_TXT.Value;
        1: FmHistCliEmi.EdCPF1.Text    := QrCHDevACPF_TXT.Value;
        2: FmHistCliEmi.EdCPF1.Text    := QrDOpenCPF_TXT.Value;
        3: FmHistCliEmi.EdCPF1.Text    := QrOcorACPF_TXT.Value;
        5: FmHistCliEmi.EdCPF1.Text    := QrSacEmiCPF_TXT.Value;
        else FmHistCliEmi.EdCPF1.Text  := '';
      end;
    end;
    5:
    begin
      case PageControl1.ActivePageIndex of
        0: FmHistCliEmi.EdCPF1.Text    := QrRiscoCCPF_TXT.Value;
        1: FmHistCliEmi.EdCPF1.Text    := QrCHDevACPF_TXT.Value;
        2: FmHistCliEmi.EdCPF1.Text    := QrDOpenCPF_TXT.Value;
        3: FmHistCliEmi.EdCPF1.Text    := QrOcorACPF_TXT.Value;
        5: FmHistCliEmi.EdCPF1.Text    := QrSacEmiCPF_TXT.Value;
        else FmHistCliEmi.EdCPF1.Text  := '';
      end;
      FmHistCliEmi.EdCliente.ValueVariant      := EdCliente.ValueVariant;
      FmHistCliEmi.CBCliente.KeyValue  := EdCliente.ValueVariant;
    end;
  end;
  if FmHistCliEmi.EdCPF1.Text <> '' then
    FmHistCliEmi.ReopenEmiSac(FmHistCliEmi.EdCPF1.Text);
  FmHistCliEmi.Pesquisa;
  FmHistCliEmi.ShowModal;
  FmHistCliEmi.Destroy;
end;

procedure TFmRiscoAll_1.Clientepesquisado1Click(Sender: TObject);
begin
  MostraHistorico(1);
end;

procedure TFmRiscoAll_1.Emitentesacadopesquisado1Click(Sender: TObject);
begin
  MostraHistorico(2);
end;

procedure TFmRiscoAll_1.Ambospesquisados1Click(Sender: TObject);
begin
  MostraHistorico(3);
end;

procedure TFmRiscoAll_1.Emitentesacadoselecionado1Click(Sender: TObject);
begin
  MostraHistorico(4);
end;

procedure TFmRiscoAll_1.EmitentesacadoselecionadoClientepesquisado1Click(
  Sender: TObject);
begin
  MostraHistorico(5);
end;

procedure TFmRiscoAll_1.frxRisco1GetValue(const VarName: String;
  var Value: Variant);
begin
  if VarName = 'VARF_CLIENTE' then
  begin
    if CBCliente.KeyValue = NULL then Value := ' ' else
    Value := CBCliente.Text;
  end else if VarName = 'VARF_TODOS' then
  begin
    if CBCliente.KeyValue = NULL then Value := 'TODOS CLIENTES' else
    Value := ' ';
  end else if VarName = 'VARF_QTD_CHEQUES' then Value := Progress1.Max
  else if VarName = 'VFR_LA1NOME' then
    Value := ImpressaoDeCabecalho(RGOrdem1.ItemIndex)
  else if VarName = 'VFR_LA2NOME' then
    Value := ImpressaoDeCabecalho(RGOrdem2.ItemIndex)
  else if VarName = 'VFR_LA3NOME' then
    Value := ImpressaoDeCabecalho(RGOrdem3.ItemIndex)
  else if VarName = 'VARF_SITUACOES' then
  begin
    Value := ' ';
    if Ck10.Checked then Value := Value + '[' + Ck10.Caption + ']';
    if Ck20.Checked then Value := Value + '[' + Ck20.Caption + ']';
    if Ck30.Checked then Value := Value + '[' + Ck30.Caption + ']';
    if Ck40.Checked then Value := Value + '[' + Ck40.Caption + ']';
    if Ck50.Checked then Value := Value + '[' + Ck50.Caption + ']';
    if Ck60.Checked then Value := Value + '[' + Ck60.Caption + ']';
    if Ck70.Checked then Value := Value + '[' + Ck70.Caption + ']';
    //if Ck00.Checked then Value := Value + ' (Resumido)';
    if Trim(Value) = '' then Value := '(??????)';
  end
  else if VarName = 'VARF_FILTROS' then
  begin
    Value := '';
    if EdColigado.ValueVariant <> 0 then Value := Value +
      'Coligado: '+CBColigado.Text+ '  ';
    if FEmitente <> '' then Value := Value + '  {Emitente: '+ FEmitente+'}';
    if FCPF <> '' then Value := Value + '  {CPF/CNPJ: '+
    Geral.FormataCNPJ_TT(FCPF)+'}';
    if Value <> '' then Value := 'FILTROS : '+Value;
  end

  // user function


  else if VarName = 'VFR_ORD1' then Value := RGAgrupa.ItemIndex >= 1
  else if VarName = 'VFR_ORD2' then Value := RGAgrupa.ItemIndex >= 2
  else if VarName = 'VFR_ORD3' then Value := RGAgrupa.ItemIndex >= 3
  else if VarName = 'VFR_IDX1' then Value := RGOrdem1.ItemIndex
  else if VarName = 'VFR_IDX2' then Value := RGOrdem2.ItemIndex
  else if VarName = 'VFR_IDX3' then Value := RGOrdem3.ItemIndex
  else if VarName = 'VFR_CON1' then Value := CondicaoDeImpressao(RGOrdem1.ItemIndex)
  else if VarName = 'VFR_CON2' then Value := CondicaoDeImpressao(RGOrdem2.ItemIndex)
  else if VarName = 'VFR_CON3' then Value := CondicaoDeImpressao(RGOrdem3.ItemIndex)

end;

end.

