object FmLot2Txa: TFmLot2Txa
  Left = 339
  Top = 185
  Caption = 'BDR-GEREN-009 :: Lote - Taxa'
  ClientHeight = 322
  ClientWidth = 509
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 509
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 461
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 413
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 133
        Height = 32
        Caption = 'Lote - Taxa'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 133
        Height = 32
        Caption = 'Lote - Taxa'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 133
        Height = 32
        Caption = 'Lote - Taxa'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 252
    Width = 509
    Height = 70
    Align = alBottom
    TabOrder = 1
    object PnSaiDesis: TPanel
      Left = 363
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 361
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 509
    Height = 160
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 509
      Height = 160
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 509
        Height = 160
        Align = alClient
        TabOrder = 0
        object Panel15: TPanel
          Left = 2
          Top = 15
          Width = 505
          Height = 143
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label75: TLabel
            Left = 12
            Top = 0
            Width = 61
            Height = 13
            Caption = 'Taxa / tarifa:'
          end
          object Label77: TLabel
            Left = 296
            Top = 88
            Width = 46
            Height = 13
            Caption = '% / Valor:'
          end
          object Label76: TLabel
            Left = 200
            Top = 88
            Width = 58
            Height = 13
            Caption = 'Quantidade:'
          end
          object Label78: TLabel
            Left = 396
            Top = 88
            Width = 73
            Height = 13
            Caption = 'Total % / Valor:'
          end
          object SpeedButton1: TSpeedButton
            Left = 472
            Top = 16
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SpeedButton1Click
          end
          object Label1: TLabel
            Left = 12
            Top = 88
            Width = 29
            Height = 13
            Caption = 'Data: '
          end
          object EdTaxaCod5: TdmkEditCB
            Left = 12
            Top = 16
            Width = 65
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            OnChange = EdTaxaCod5Change
            DBLookupComboBox = CBTaxaCod5
            IgnoraDBLookupComboBox = False
          end
          object CBTaxaCod5: TdmkDBLookupComboBox
            Left = 80
            Top = 16
            Width = 389
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DmLot.DsTaxas
            TabOrder = 1
            dmkEditCB = EdTaxaCod5
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdTaxaTxa5: TdmkEdit
            Left = 296
            Top = 104
            Width = 93
            Height = 21
            Alignment = taRightJustify
            TabOrder = 5
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 6
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            OnChange = EdTaxaTxa5Change
          end
          object RGForma: TRadioGroup
            Left = 12
            Top = 40
            Width = 481
            Height = 41
            Caption = ' Forma de cobran'#231'a: '
            Columns = 2
            ItemIndex = 0
            Items.Strings = (
              'Valor'
              'Porcentagem')
            TabOrder = 2
            OnClick = RGFormaClick
          end
          object EdTaxaQtd5: TdmkEdit
            Left = 200
            Top = 104
            Width = 93
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            OnChange = EdTaxaQtd5Change
          end
          object EdTaxaTot5: TdmkEdit
            Left = 396
            Top = 104
            Width = 93
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 6
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 6
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object TPData: TdmkEditDateTimePicker
            Left = 12
            Top = 104
            Width = 180
            Height = 21
            Date = 0.439571469905786200
            Time = 0.439571469905786200
            TabOrder = 3
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 208
    Width = 509
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 505
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
end
