unit GerDup1Baxa;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, ComCtrls, DBCtrls, Db, mySQLDbTables, dmkGeral,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkImage, dmkEditDateTimePicker,
  UnDmkEnums, dmkCheckBox;

type
  TFmGerDup1Baxa = class(TForm)
    QrOcorDupl: TmySQLQuery;
    DsOcorDupl: TDataSource;
    QrOcorDuplCodigo: TIntegerField;
    QrOcorDuplNome: TWideStringField;
    QrOcorDuplBase: TFloatField;
    QrOcorDuplDatapbase: TIntegerField;
    QrOcorDuplOcorrbase: TIntegerField;
    QrOcorDuplStatusbase: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    TPVenc: TdmkEditDateTimePicker;
    EdCliCod: TdmkEdit;
    EdCliNome: TdmkEdit;
    EdDuplicata: TdmkEdit;
    EdCPF: TdmkEdit;
    EdEmitente: TdmkEdit;
    EdValor: TdmkEdit;
    GroupBox2: TGroupBox;
    PainelStatus: TPanel;
    Label15: TLabel;
    Label17: TLabel;
    EdAlinea: TdmkEditCB;
    CBAlinea: TdmkDBLookupComboBox;
    TPData: TdmkEditDateTimePicker;
    GBPagto: TGroupBox;
    PainelPagto: TPanel;
    Panel5: TPanel;
    Label23: TLabel;
    Label24: TLabel;
    Label29: TLabel;
    TPDataBase2: TdmkEditDateTimePicker;
    EdValorBase2: TdmkEdit;
    EdJurosPeriodo2: TdmkEdit;
    Panel4: TPanel;
    Label26: TLabel;
    Label1: TLabel;
    TPPagto2: TdmkEditDateTimePicker;
    EdDesco2: TdmkEdit;
    GBAvisos1: TGroupBox;
    Panel2: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel3: TPanel;
    BtOK: TBitBtn;
    SpeedButton1: TSpeedButton;
    EdValPagar2: TdmkEdit;
    Label8: TLabel;
    EdJuros2: TdmkEdit;
    Label27: TLabel;
    EdJurosBase2: TdmkEdit;
    Label25: TLabel;
    EdCorrigido2: TdmkEdit;
    Label30: TLabel;
    EdAPagar2: TdmkEdit;
    Label28: TLabel;
    EdPendente2: TdmkEdit;
    Label9: TLabel;
    EdDesco3: TdmkEdit;
    Label10: TLabel;
    QrCarteiras: TmySQLQuery;
    DsCarteiras: TDataSource;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    PnCarteira: TPanel;
    Label11: TLabel;
    EdCarteira: TdmkEditCB;
    CBCarteira: TdmkDBLookupComboBox;
    CkMovimento: TdmkCheckBox;
    procedure BtOKClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdAlineaChange(Sender: TObject);
    procedure EdValorBase2Change(Sender: TObject);
    procedure EdJurosBase2Change(Sender: TObject);
    procedure TPPagto2Change(Sender: TObject);
    procedure EdJuros2Change(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure EdAPagar2Change(Sender: TObject);
    procedure EdValPagar2Change(Sender: TObject);
    procedure EdDesco3Exit(Sender: TObject);
    procedure EdDesco2Exit(Sender: TObject);
    procedure CkMovimentoClick(Sender: TObject);
  private
    { Private declarations }
    FPainelStatusHeight: Integer;
    procedure CalculaPendente();

  public
    { Public declarations }
    FDuplicataOrigem, FLotePagto: Integer;
    procedure CalculaAPagar(DesconPercent: Boolean);
    procedure CalculaJuros;
  end;

  var
  FmGerDup1Baxa: TFmGerDup1Baxa;

implementation

{$R *.DFM}

uses UnMyObjects, Module, UnMLAGeral, UnInternalConsts, UMySQLModule, GerDup1Main,
  MyListas, MyDBCheck, AlineasDup, ModuleLot, ModuleFin;

procedure TFmGerDup1Baxa.BtOKClick(Sender: TObject);
var
  //LotePg,
  LOI, Alinea, Quitado, FatParcela, ADupIts, Controle: Integer;
  //Data,
  DataA: String;
  //Juros, Desco,
  Pago: Double;
  //
  Gen_0303, Gen_0304, Gen_0306, Gen_0308, LctCtrl, LctSub: Integer;
  ValPagar, ValTaxas, ValMulta, ValJuros, ValPrinc, ValDesco, ValPende: Double;
  Duplicata: String;
var
{
  ValQuit, ValJr, Difer: Double;
  LocFP, GenAPCD, DupPgs, MeuID, Ocorr, MsgValor: Integer;
  //
}
  //MoraVal,
  Ocorreu, FatID_Sub, Cliente, FatParcRef, CartDep: Integer;
  FatNum, ValDupl: Double;
  DataDU, Dta: TDateTime;
begin
  if MyObjects.FIC(ImgTipo.SQLType <> stIns, nil, 'A��o de SQL inv�lida!') then
    Exit;
  //
  if PainelStatus.Visible then
  begin
    ADupIts := EdAlinea.ValueVariant;
    if MyObjects.FIC(ADupIts = 0, EdAlinea, 'Defina o status da duplicata!') then
      Exit;
  end else ADupIts := 0;
  if ADupIts <> 0 then
  begin
{
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', 'ADupIts',
    'ADupIts', 'Controle');
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO adupits SET Alinea=:P0, DataA=:P1, ');
    Dmod.QrUpd.SQL.Add('Controle=:Pa, Lot esIts=:Pb');
    //
    Dmod.QrUpd.Params[00].AsInteger := ADupIts;
    Dmod.QrUpd.Params[01].AsString  := Data;
    Dmod.QrUpd.Params[02].AsInteger := Controle;
    Dmod.QrUpd.Params[03].AsInteger := FmGerDup1Main.QrPesqFatParcela.Value;
    Dmod.QrUpd.ExecSQL;
}
    LOI    := FmGerDup1Main.QrPesqFatParcela.Value;
    Alinea := ADupIts;
    DataA  := Geral.FDT(TPData.Date, 1);
    //
    Controle := UMyMod.BuscaEmLivreY_Def('adupits', 'Controle', stIns, 0);
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'adupits', False, [
    FLD_LOIS, 'Alinea', 'DataA'], ['Controle'], [
    LOI, Alinea, DataA], [Controle], True);
    //
    FmGerDup1Main.FAdupIts := Controle;
    FmGerDup1Main.ForcaOcorBank := QrOcorDuplOcorrbase.Value;
    FmGerDup1Main.ForcaOBData   := TPData.Date;
    //
    if QrOcorDuplStatusbase.Value < 0 then
    begin
{
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE Lot esIts SET Quitado=:P0 WHERE Controle=:P1');
      //
      Dmod.QrUpd.Params[00].AsInteger := QrOcorDuplStatusbase.Value;
      Dmod.QrUpd.Params[01].AsInteger := FmGerDup1Main.QrPesqFatParcela.Value;
      Dmod.QrUpd.ExecSQL;
}
      Quitado    := QrOcorDuplStatusbase.Value;
      FatParcela := FmGerDup1Main.QrPesqFatParcela.Value;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_TabLctA, False, [
      'Quitado'], ['FatID', 'FatParcela'], [
      Quitado], [VAR_FATID_0301, FatParcela], True);
    end;
  end;
  if GBPagto.Visible then
  begin
    if CkMovimento.Checked then
    begin
      if TPVenc.Date >= TPData.Date then
        CartDep := -13 //Border�s - duplicatas pagas em dia
      else
        CartDep := -14; //Border�s - duplicatas pagas com atraso
    end else
      CartDep := EdCarteira.ValueVariant;
    //
    if MyObjects.FIC(CartDep = 0, EdCarteira, 'Defina a carteira!') then Exit;
    //
{
    ADup Pgs := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
          'ADup Pgs', 'ADup Pgs', 'Controle');
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO adup pgs SET AlterWeb=1, ');
    Dmod.QrUpd.SQL.Add('Data=:P0, Juros=:P1, Desco=:P2, Pago=:P3, LotePg=:P4');
    Dmod.QrUpd.SQL.Add(', Lot esIts=:Pa, Controle=:Pb');
    Dmod.QrUpd.Params[00].AsString  := FormatDateTime(VAR_FORMATDATE, TPPagto2.Date);
    Dmod.QrUpd.Params[01].AsFloat   := EdJuros2.ValueVariant;
    Dmod.QrUpd.Params[02].AsFloat   := EdDesco2.ValueVariant;
    Dmod.QrUpd.Params[03].AsFloat   := EdPago2.ValueVariant;
    Dmod.QrUpd.Params[04].AsInteger := FmGerDup1Main.FLotePgOrigem;
    //
    Dmod.QrUpd.Params[05].AsInteger := FmGerDup1Main.QrPesqFatParcela.Value;
    Dmod.QrUpd.Params[06].AsInteger := ADup Pgs;
    Dmod.QrUpd.ExecSQL;
}
    //LOI            := FmGerDup1Main.QrPesqFatParcela.Value;
    //Data           := Geral.FDT(TPPagto2.Date, 1);
    //Juros          := EdJuros2.ValueVariant;
    //Desco          := EdDesco2.ValueVariant;
    //Pago           := EdPago2.ValueVariant;
    //LotePg         := FmGerDup1Main.FLotePgOrigem;
    //
{
    Controle := UMyMod.BuscaEmLivreY_Def('adup pgs', 'Controle', stIns, 0);
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'adup pgs', False, [
    FLD_LOIS, 'Data', 'Juros', 'Desco', 'Pago', 'LotePg'], ['Controle'], [
    LOI, Data, Juros, Desco, Pago, LotePg], [Controle], True);
}
    FatParcRef := FmGerDup1Main.QrPesqFatParcela.Value;
    DmLot.ReopenLOI(FatParcRef);
    ValDupl := DmLot.QrLOICredito.Value - DmLot.QrLOIDebito.Value;
    DmLot.ReopenAdupIts(FatParcRef);
    FatNum := FmGerDup1Main.FLotePgOrigem;
    FatParcela := 0;
    Controle := 0;
    FatID_Sub := 0; // as al�neas s�o em separado!
    Cliente := DmLot.QrLOICliente.Value;
    Ocorreu := DmLot.QrAdupItsControle.Value;
    //MoraVal := EdJuros2.ValueVariant;
    Pago := EdCorrigido2.ValueVariant;
    //Desco := EdDesco2.ValueVariant;
    Dta := TPPagto2.Date;
    DataDU := DmLot.QrLOIVencimento.Value;
    //
    if not DModFin.ObtemGeneroDeFatID(VAR_FATID_0303, Gen_0303, tpCred, True) then
      Exit;
    if not DModFin.ObtemGeneroDeFatID(VAR_FATID_0304, Gen_0304, tpCred, True) then
      Exit;
    if not DModFin.ObtemGeneroDeFatID(VAR_FATID_0306, Gen_0306, tpCred, True) then
      Exit;
    if not DModFin.ObtemGeneroDeFatID(VAR_FATID_0308, Gen_0308, tpDeb, True) then
      Exit;
    //
    ValPagar := EdValPagar2.ValueVariant;
    ValTaxas := 0; // N�o tem aqui! � separado!
    ValMulta := 0; // N�o tem aqui! � separado!
    ValDesco := EdDesco2.ValueVariant;
    ValJuros := EdJuros2.ValueVariant;
    ValPrinc := ValPagar - ValTaxas - ValMulta - ValJuros + ValDesco;
    ValPende := EdPendente2.ValueVariant;
{
    DmLot.SQL_DupPg(Dmod.QrUpd, stIns, FatParcela, Controle, FatID_Sub, Genero,
     Cliente, Ocorreu, FatParcRef, FatNum, Pago, MoraVal, Desco, DataDU, Dta);
}
    //
    LctCtrl   := FmGerDup1Main.QrPesqLctCtrl.Value;
    LctSub    := FmGerDup1Main.QrPesqLctSub.Value;
    Duplicata := FmGerDup1Main.QrPesqDuplicata.Value;

    if DmLot.InsUpd_DupPg(Dmod.QrUpd, stIns, LctCtrl, LctSub, FatParcela,
      Controle, FatID_Sub, Gen_0303, Gen_0304, Gen_0306, Gen_0308, Cliente,
      Ocorreu, FatParcRef, FatNum, Pago(*, MoraVal, Desco*), DataDU, Dta,
      Duplicata, ValPrinc, ValTaxas, ValMulta, ValJuros, ValDesco, ValPende,
      CartDep) then
    begin
      FmGerDup1Main.FDupPgs := FatParcela;
      DmLot.CalculaPagtoDuplicata(FmGerDup1Main.QrPesqFatParcela.Value, ValDupl);
    end;
  end;
  FmGerDup1Main.FLOIS := FmGerDup1Main.QrPesqFatParcela.Value;
  Close;
end;

procedure TFmGerDup1Baxa.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGerDup1Baxa.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGerDup1Baxa.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  if GBPagto.Visible = False then PainelStatus.Align := alClient else
  begin
    PainelStatus.Align := alTop;
    PainelStatus.Height := FPainelStatusHeight;
  end;
  if GBPagto.Visible then
  begin
    CalculaJuros;
    CalculaAPagar(False);
  end;
  if PainelStatus.Visible then
    EdAlinea.SetFocus
  else if Panel4.Visible then
    EdJurosBase2.SetFocus;
end;

procedure TFmGerDup1Baxa.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGerDup1Baxa.SpeedButton1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmAlineasDup, FmAlineasDup, afmoNegarComAviso) then
  begin
    FmAlineasDup.ShowModal;
    FmAlineasDup.Destroy;
    //
    UMyMod.SetaCodigoPesquisado(EdAlinea, CBAlinea, QrOcorDupl, VAR_CADASTRO);
  end;
end;

procedure TFmGerDup1Baxa.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stIns;
  //
  FPainelStatusHeight := PainelStatus.Height;
  FLotePagto := 0;
  //
  UMyMod.AbreQuery(QrOcorDupl, Dmod.MyDB);
  UMyMod.AbreQuery(QrCarteiras, Dmod.MyDB);
  //
  TPData.Date := Date;
  TPPagto2.Date := Date;
end;

procedure TFmGerDup1Baxa.EdAlineaChange(Sender: TObject);
begin
  case QrOcorDuplDatapbase.Value of
    0:
    begin
      if Date > TPData.MinDate   then TPData.Date   := Date;
      if Date > TPPagto2.MinDate then TPPagto2.Date := Date;
    end;
    1:
    begin
      TPData.Date   := TPVenc.Date;
      TPPagto2.Date := TPVenc.Date;
    end;
  end;
  CalculaJuros;
end;

procedure TFmGerDup1Baxa.EdAPagar2Change(Sender: TObject);
begin
  CalculaPendente();
end;

procedure TFmGerDup1Baxa.EdValorBase2Change(Sender: TObject);
begin
  CalculaAPagar(False);
end;

procedure TFmGerDup1Baxa.EdValPagar2Change(Sender: TObject);
begin
  CalculaPendente();
end;

procedure TFmGerDup1Baxa.CalculaAPagar(DesconPercent: Boolean);
var
  Base, Juro, DescPer, Desc: Double;
begin
  Base    := EdValorBase2.ValueVariant;
  Juro    := EdJuros2.ValueVariant;
  DescPer := EdDesco3.ValueVariant;
  Desc    := EdDesco2.ValueVariant;
  //
  if DesconPercent then
  begin
    Desc := ((Base + Juro) * DescPer) / 100;
    //
    EdDesco2.ValueVariant := Desc;
  end else
  begin
    DescPer := (((((Base + Juro) - Desc) - (Base + Juro)) * 100) / (Base + Juro)) * -1;
  end;
  EdDesco3.ValueVariant     := DescPer;
  EdCorrigido2.ValueVariant := Base + Juro;
  EdAPagar2.ValueVariant    := EdCorrigido2.ValueVariant - Desc;
  EdValPagar2.ValueVariant  := EdAPagar2.ValueVariant;
end;


procedure TFmGerDup1Baxa.EdJurosBase2Change(Sender: TObject);
begin
  CalculaJuros;
end;

procedure TFmGerDup1Baxa.TPPagto2Change(Sender: TObject);
begin
  CalculaJuros;
  CalculaAPagar(True);
end;

procedure TFmGerDup1Baxa.EdJuros2Change(Sender: TObject);
begin
  CalculaAPagar(False);
end;

procedure TFmGerDup1Baxa.EdDesco2Exit(Sender: TObject);
begin
  CalculaAPagar(False);
end;

procedure TFmGerDup1Baxa.EdDesco3Exit(Sender: TObject);
begin
  CalculaAPagar(True);
end;

procedure TFmGerDup1Baxa.CalculaJuros;
var
  Prazo: Integer;
  Taxa, Juros, Desconto, Valor: Double;
begin
  Juros    := 0;
  Desconto := 0;
  Prazo    := Trunc(Int(TPPagto2.Date) - Int(TPDataBase2.Date));
  if Prazo > 0 then
  begin
    Taxa  := EdJurosBase2.ValueVariant;
    Juros := MLAGeral.CalculaJuroComposto(Taxa, Prazo);
  end else
  if Prazo < 0 then
  begin
    Prazo    := Prazo * -1;
    Taxa     := EdJurosBase2.ValueVariant;
    Desconto := MLAGeral.CalculaJuroComposto(Taxa, Prazo);
  end;
  Valor := EdValorBase2.ValueVariant;
  EdJurosPeriodo2.ValueVariant := Juros;
  EdJuros2.ValueVariant := Juros * Valor / 100;
  //
  EdDesco3.ValueVariant := Desconto;
  EdDesco2.ValueVariant := Desconto * Valor / 100;
end;

procedure TFmGerDup1Baxa.CalculaPendente();
begin
  EdPendente2.ValueVariant := EdAPagar2.ValueVariant - EdValPagar2.ValueVariant;
end;

procedure TFmGerDup1Baxa.CkMovimentoClick(Sender: TObject);
begin
  PnCarteira.Visible := not CkMovimento.Checked;
end;

end.
