object FmPesqCPF: TFmPesqCPF
  Left = 419
  Top = 217
  Caption = 'CSE-PESQU-003 :: Pesquisa de Emitentes de Cheques'
  ClientHeight = 374
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 61
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Label1: TLabel
      Left = 188
      Top = 12
      Width = 31
      Height = 13
      Caption = 'Nome:'
    end
    object RGMascara: TRadioGroup
      Left = 8
      Top = 8
      Width = 177
      Height = 41
      Caption = ' M'#225'scara: '
      Columns = 3
      ItemIndex = 0
      Items.Strings = (
        'Ambos'
        'Prefixo'
        'Sufixo')
      TabOrder = 0
      OnClick = RGMascaraClick
    end
    object EdEmitente: TdmkEdit
      Left = 188
      Top = 28
      Width = 593
      Height = 21
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      OnChange = EdEmitenteChange
    end
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 109
    Width = 784
    Height = 151
    Align = alClient
    DataSource = DsEmitCPF
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDblClick = DBGrid1DblClick
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 433
        Height = 32
        Caption = 'Pesquisa de Emitentes de Cheques'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 433
        Height = 32
        Caption = 'Pesquisa de Emitentes de Cheques'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 433
        Height = 32
        Caption = 'Pesquisa de Emitentes de Cheques'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 260
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 304
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 4
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 10
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtPesquisa: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Pesquisa'
        TabOrder = 0
        OnClick = BtPesquisaClick
        NumGlyphs = 2
      end
      object BtConfirma: TBitBtn
        Tag = 15
        Left = 254
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Confirma'
        Enabled = False
        TabOrder = 1
        OnClick = BtConfirmaClick
        NumGlyphs = 2
      end
    end
  end
  object QrEmitCPF: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM emitcpf'
      'WHERE Nome LIKE :P0'
      'ORDER BY Nome')
    Left = 296
    Top = 164
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmitCPFCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrEmitCPFNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrEmitCPFLimite: TFloatField
      FieldName = 'Limite'
    end
    object QrEmitCPFLastAtz: TDateField
      FieldName = 'LastAtz'
    end
    object QrEmitCPFAcumCHComV: TFloatField
      FieldName = 'AcumCHComV'
    end
    object QrEmitCPFAcumCHComQ: TIntegerField
      FieldName = 'AcumCHComQ'
    end
    object QrEmitCPFAcumCHDevV: TFloatField
      FieldName = 'AcumCHDevV'
    end
    object QrEmitCPFAcumCHDevQ: TIntegerField
      FieldName = 'AcumCHDevQ'
    end
  end
  object DsEmitCPF: TDataSource
    DataSet = QrEmitCPF
    Left = 324
    Top = 164
  end
end
