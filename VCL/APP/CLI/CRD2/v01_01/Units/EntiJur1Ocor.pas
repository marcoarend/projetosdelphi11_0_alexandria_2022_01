unit EntiJur1Ocor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Db, mySQLDbTables, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkLabel, dmkGeral, dmkImage,
  UnInternalConsts, UnDmkEnums;

type
  TFmEntiJur1Ocor = class(TForm)
    QrOcorBank: TmySQLQuery;
    DsOcorBank: TDataSource;
    QrOcorBankCodigo: TIntegerField;
    QrOcorBankNome: TWideStringField;
    QrOcorBankBase: TFloatField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BitBtn1: TBitBtn;
    Panel3: TPanel;
    BitBtn2: TBitBtn;
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    Label1: TLabel;
    Label4: TLabel;
    EdOcor: TdmkEditCB;
    CBOcor: TdmkDBLookupComboBox;
    RGFormaCNAB: TRadioGroup;
    EdBase: TdmkEdit;
    SpeedButton5: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdOcorChange(Sender: TObject);
    procedure CBOcorClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCliente, FControle: Integer;
    FResult: Boolean;
  end;

  var
  FmEntiJur1Ocor: TFmEntiJur1Ocor;

implementation

uses UnMyObjects, UnFinanceiro, Module, UMySQLModule, Principal;

{$R *.DFM}

procedure TFmEntiJur1Ocor.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEntiJur1Ocor.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEntiJur1Ocor.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEntiJur1Ocor.SpeedButton5Click(Sender: TObject);
var
  Codigo: Integer;
begin
  VAR_CADASTRO := 0;
  Codigo       := EdOcor.ValueVariant;
  //
  FmPrincipal.CadastroOcorBank(Codigo);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.AbreQuery(QrOcorBank, Dmod.MyDB);
    //
    EdOcor.ValueVariant := VAR_CADASTRO;
    CBOcor.KeyValue     := VAR_CADASTRO;
    EdOcor.SetFocus;
  end;
end;

procedure TFmEntiJur1Ocor.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UMyMod.AbreQuery(QrOcorBank, Dmod.MyDB);
  FResult := False;
end;

procedure TFmEntiJur1Ocor.EdOcorChange(Sender: TObject);
begin
  if QrOcorBank.Locate('Codigo', EdOcor.ValueVariant, []) then
    EdBase.ValueVariant := QrOcorBankBase.value;
end;

procedure TFmEntiJur1Ocor.BitBtn2Click(Sender: TObject);
var
  Ocorrencia, FormaCNAB: Integer;
  Base: Double;
begin
  Ocorrencia := EdOcor.ValueVariant;
  Base       := EdBase.ValueVariant;
  FormaCNAB  := RGFormaCNAB.ItemIndex;
  if Ocorrencia = 0 then
  begin
    Application.MessageBox('Defina uma ocorrência.', 'Aviso',
      MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  FControle := UMyMod.BuscaEmLivreY_Def('ocorcli', 'Controle',
  ImgTipo.SQLType, FControle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'ocorcli', False, [
    'Cliente', 'Ocorrencia', 'Base', 'FormaCNAB'
  ], ['Controle'], [
    FCliente, Ocorrencia, Base, FormaCNAB],
  [FControle], True) then
  begin
    FResult := True;
    Close;
  end;
end;

procedure TFmEntiJur1Ocor.CBOcorClick(Sender: TObject);
begin
  EdBase.ValueVariant := QrOcorBankBase.value;
end;

end.
