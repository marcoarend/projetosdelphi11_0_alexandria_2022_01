object FmLot2PCD: TFmLot2PCD
  Left = 339
  Top = 185
  Caption = 'BDR-GEREN-011 :: Lote - Pagamento de Cheque Devolvido'
  ClientHeight = 306
  ClientWidth = 925
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 925
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 877
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 829
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 482
        Height = 32
        Caption = 'Lote - Pagamento de Cheque Devolvido'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 482
        Height = 32
        Caption = 'Lote - Pagamento de Cheque Devolvido'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 482
        Height = 32
        Caption = 'Lote - Pagamento de Cheque Devolvido'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 236
    Width = 925
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 779
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 777
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        TabOrder = 0
        OnClick = BtOKClick
        NumGlyphs = 2
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 925
    Height = 144
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 925
      Height = 144
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 925
        Height = 144
        Align = alClient
        TabOrder = 0
        object DBGrid9: TDBGrid
          Left = 2
          Top = 15
          Width = 921
          Height = 40
          Align = alTop
          DataSource = DmLot.DsCHDevA
          Enabled = False
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'DATA1_TXT'
              Title.Caption = '1'#170' Devol.'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Alinea1'
              Title.Caption = 'Ali 1'
              Width = 24
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Emitente'
              Title.Caption = 'Nome emitente'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CPF_TXT'
              Title.Caption = 'CPF / CNPJ emitente'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Banco'
              Width = 35
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Agencia'
              Title.Caption = 'Ag'#234'ncia'
              Width = 42
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Conta'
              Width = 98
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Cheque'
              Width = 45
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DATA2_TXT'
              Title.Caption = '2'#170' Devol.'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Alinea2'
              Title.Caption = 'Ali 2'
              Width = 24
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Valor'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Taxas'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DATA3_TXT'
              Title.Caption = #218'lt.Pgto'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValPago'
              Title.Caption = 'Pago'
              Visible = True
            end>
        end
        object Panel28: TPanel
          Left = 2
          Top = 55
          Width = 921
          Height = 87
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label102: TLabel
            Left = 4
            Top = 4
            Width = 52
            Height = 13
            Caption = 'Data base:'
          end
          object Label103: TLabel
            Left = 120
            Top = 4
            Width = 53
            Height = 13
            Caption = 'Valor base:'
          end
          object Label104: TLabel
            Left = 4
            Top = 44
            Width = 89
            Height = 13
            Caption = '% Taxa juros base:'
          end
          object Label105: TLabel
            Left = 216
            Top = 4
            Width = 79
            Height = 13
            Caption = '% Juros per'#237'odo:'
          end
          object Label106: TLabel
            Left = 96
            Top = 44
            Width = 26
            Height = 13
            Caption = 'Data:'
          end
          object Label107: TLabel
            Left = 308
            Top = 4
            Width = 37
            Height = 13
            Caption = '$ Juros:'
          end
          object Label109: TLabel
            Left = 500
            Top = 4
            Width = 66
            Height = 13
            Caption = 'Total a pagar:'
          end
          object Label108: TLabel
            Left = 308
            Top = 44
            Width = 75
            Height = 13
            Caption = '$ Valor a pagar:'
          end
          object Label202: TLabel
            Left = 212
            Top = 44
            Width = 58
            Height = 13
            Caption = '$ Desconto:'
          end
          object Label2: TLabel
            Left = 404
            Top = 4
            Width = 62
            Height = 13
            Caption = 'Total devido:'
          end
          object Label9: TLabel
            Left = 404
            Top = 44
            Width = 84
            Height = 13
            Caption = '$ Valor pendente:'
          end
          object TPDataBase7: TdmkEditDateTimePicker
            Left = 4
            Top = 20
            Width = 112
            Height = 21
            Date = 38698.785142685200000000
            Time = 38698.785142685200000000
            Color = clBtnFace
            Enabled = False
            TabOrder = 0
            TabStop = False
            ReadOnly = True
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
          end
          object EdValorBase7: TdmkEdit
            Left = 120
            Top = 20
            Width = 92
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            Color = clBtnFace
            ReadOnly = True
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            OnChange = EdValorBase7Change
          end
          object EdJurosBase7: TdmkEdit
            Left = 4
            Top = 60
            Width = 88
            Height = 21
            Alignment = taRightJustify
            Color = clWhite
            TabOrder = 6
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 6
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            OnChange = EdJurosBase7Change
          end
          object EdJurosPeriodo7: TdmkEdit
            Left = 216
            Top = 20
            Width = 88
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            Color = clBtnFace
            ReadOnly = True
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 6
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object TPPagto7: TdmkEditDateTimePicker
            Left = 96
            Top = 60
            Width = 112
            Height = 21
            Date = 38698.785142685200000000
            Time = 38698.785142685200000000
            TabOrder = 7
            OnChange = TPPagto7Change
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
          end
          object EdJuros7: TdmkEdit
            Left = 308
            Top = 20
            Width = 93
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            Color = clBtnFace
            ReadOnly = True
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            OnChange = EdJuros7Change
          end
          object EdAPagar7: TdmkEdit
            Left = 500
            Top = 20
            Width = 93
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            Color = clBtnFace
            ReadOnly = True
            TabOrder = 5
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            OnChange = EdAPagar7Change
          end
          object EdValPagar7: TdmkEdit
            Left = 308
            Top = 60
            Width = 93
            Height = 21
            Alignment = taRightJustify
            TabOrder = 9
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            OnChange = EdValPagar7Change
          end
          object EdDesco7: TdmkEdit
            Left = 212
            Top = 60
            Width = 93
            Height = 21
            Alignment = taRightJustify
            TabOrder = 8
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            OnExit = EdDesco7Exit
          end
          object EdCorrigido7: TdmkEdit
            Left = 404
            Top = 20
            Width = 92
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            Color = clBtnFace
            ReadOnly = True
            TabOrder = 4
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object EdPendente7: TdmkEdit
            Left = 404
            Top = 60
            Width = 92
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            Color = clBtnFace
            ReadOnly = True
            TabOrder = 10
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 192
    Width = 925
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 921
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
end
