object FmSociosImp: TFmSociosImp
  Left = 396
  Top = 181
  Caption = 'CLI-JURID-005 :: Impress'#227'o de S'#243'cios'
  ClientHeight = 232
  ClientWidth = 478
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 478
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 430
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 382
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 255
        Height = 32
        Caption = 'Impress'#227'o de S'#243'cios'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 255
        Height = 32
        Caption = 'Impress'#227'o de S'#243'cios'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 255
        Height = 32
        Caption = 'Impress'#227'o de S'#243'cios'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 478
    Height = 70
    Align = alClient
    TabOrder = 1
    object PainelDados: TPanel
      Left = 2
      Top = 15
      Width = 474
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 12
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label2: TLabel
        Left = 80
        Top = 4
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 80
        Top = 20
        Width = 385
        Height = 21
        KeyField = 'Codigo'
        ListField = 'RazaoSocial'
        ListSource = DsEmpresas
        TabOrder = 1
        dmkEditCB = EdEmpresa
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdEmpresa: TdmkEditCB
        Left = 12
        Top = 20
        Width = 65
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 118
    Width = 478
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 474
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 162
    Width = 478
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 332
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 330
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtImprime: TBitBtn
        Tag = 5
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Imprime'
        TabOrder = 0
        OnClick = BtImprimeClick
        NumGlyphs = 2
      end
    end
  end
  object DsEmpresas: TDataSource
    DataSet = QrEmpresas
    Left = 400
    Top = 60
  end
  object QrEmpresas: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrEmpresasAfterScroll
    SQL.Strings = (
      'SELECT Codigo, RazaoSocial'
      'FROM entidades '
      'WHERE Tipo=0'
      'ORDER BY RazaoSocial'
      ' ')
    Left = 372
    Top = 60
    object QrEmpresasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmpresasRazaoSocial: TWideStringField
      FieldName = 'RazaoSocial'
      Size = 100
    end
  end
  object QrSocios: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrSociosCalcFields
    SQL.Strings = (
      'SELECT CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome '
      'END NOMESOCIO, en.Codigo, en.Sexo, en.Pai, en.Mae, en.PNatal,'
      'en.CidadeNatal, en.Nacionalid, en.ConjugeNome, en.CPF, en.RG, '
      'en.SSP, en.DataRG, en.PRua, en.PNumero, en.PCompl, en.PBairro, '
      'en.PCidade, en.PCEP, en.PTe1, en.Profissao, en.Cargo, '
      'uf.Nome NOMEPUF, ll.Nome NOMEPLOGRAD, le.Nome NOMEECIVIL'
      'FROM socios so'
      'LEFT JOIN entidades   en ON en.Codigo=so.Socio'
      'LEFT JOIN ufs uf         ON uf.Codigo=en.PUF'
      'LEFT JOIN listalograd ll ON ll.Codigo=en.PLograd'
      'LEFT JOIN listaecivil le ON le.Codigo=en.EstCivil'
      'WHERE so.Empresa=:P0'
      'ORDER BY so.Ordem, NOMESOCIO')
    Left = 284
    Top = 36
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSociosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSociosSexo: TWideStringField
      FieldName = 'Sexo'
      Required = True
      Size = 1
    end
    object QrSociosPai: TWideStringField
      FieldName = 'Pai'
      Required = True
      Size = 60
    end
    object QrSociosMae: TWideStringField
      FieldName = 'Mae'
      Required = True
      Size = 60
    end
    object QrSociosPNatal: TDateField
      FieldName = 'PNatal'
    end
    object QrSociosNOMEPUF: TWideStringField
      FieldName = 'NOMEPUF'
      Size = 2
    end
    object QrSociosNOMEPLOGRAD: TWideStringField
      FieldName = 'NOMEPLOGRAD'
      Size = 10
    end
    object QrSociosNOMEECIVIL: TWideStringField
      FieldName = 'NOMEECIVIL'
      Size = 10
    end
    object QrSociosITEM: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'ITEM'
      Calculated = True
    end
    object QrSociosNOMESEXO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESEXO'
      Calculated = True
    end
    object QrSociosCidadeNatal: TWideStringField
      FieldName = 'CidadeNatal'
      Size = 30
    end
    object QrSociosNacionalid: TWideStringField
      FieldName = 'Nacionalid'
      Size = 15
    end
    object QrSociosConjugeNome: TWideStringField
      FieldName = 'ConjugeNome'
      Size = 35
    end
    object QrSociosCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrSociosRG: TWideStringField
      FieldName = 'RG'
      Size = 15
    end
    object QrSociosSSP: TWideStringField
      FieldName = 'SSP'
      Size = 10
    end
    object QrSociosDataRG: TDateField
      FieldName = 'DataRG'
    end
    object QrSociosPRua: TWideStringField
      FieldName = 'PRua'
      Size = 30
    end
    object QrSociosPCompl: TWideStringField
      FieldName = 'PCompl'
      Size = 30
    end
    object QrSociosPBairro: TWideStringField
      FieldName = 'PBairro'
      Size = 30
    end
    object QrSociosPCidade: TWideStringField
      FieldName = 'PCidade'
      Size = 25
    end
    object QrSociosPCEP: TIntegerField
      FieldName = 'PCEP'
    end
    object QrSociosPTe1: TWideStringField
      FieldName = 'PTe1'
    end
    object QrSociosProfissao: TWideStringField
      FieldName = 'Profissao'
      Size = 60
    end
    object QrSociosCargo: TWideStringField
      FieldName = 'Cargo'
      Size = 60
    end
    object QrSociosECEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ECEP_TXT'
      Calculated = True
    end
    object QrSociosNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 10
      Calculated = True
    end
    object QrSociosE_LNR: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_LNR'
      Size = 255
      Calculated = True
    end
    object QrSociosCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrSociosETE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ETE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrSociosDATARG_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATARG_TXT'
      Calculated = True
    end
    object QrSociosPNATAL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PNATAL_TXT'
      Calculated = True
    end
    object QrSociosNOMESOCIO: TWideStringField
      FieldName = 'NOMESOCIO'
      Size = 100
    end
    object QrSociosPNumero: TIntegerField
      FieldName = 'PNumero'
    end
  end
  object DsSocios: TDataSource
    DataSet = QrSocios
    Left = 312
    Top = 36
  end
  object QrEmpresa: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEmpresaCalcFields
    SQL.Strings = (
      
        'SELECT en.Codigo, en.RazaoSocial, en.Cadastro, en.ENatal, en.CNP' +
        'J, '
      
        'en.IE, en.NIRE, en.Simples, en.ELograd, en.ERua, en.ENumero, en.' +
        'ECompl, '
      
        'en.EBairro, en.ECidade, en.ECEP, en.ETe1, en.EFax, en.FormaSocie' +
        't,'
      'en.Atividade, uf.Nome NOMEUF, ll.Nome NOMELOGRAD'
      'FROM entidades en'
      'LEFT JOIN ufs uf ON uf.Codigo=en.EUF'
      'LEFT JOIN listalograd ll ON ll.Codigo=en.ELograd'
      'WHERE en.Codigo=:P0')
    Left = 284
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmpresaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmpresaRazaoSocial: TWideStringField
      FieldName = 'RazaoSocial'
      Size = 100
    end
    object QrEmpresaCadastro: TDateField
      FieldName = 'Cadastro'
    end
    object QrEmpresaENatal: TDateField
      FieldName = 'ENatal'
    end
    object QrEmpresaCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrEmpresaIE: TWideStringField
      FieldName = 'IE'
    end
    object QrEmpresaSimples: TSmallintField
      FieldName = 'Simples'
    end
    object QrEmpresaELograd: TSmallintField
      FieldName = 'ELograd'
    end
    object QrEmpresaERua: TWideStringField
      FieldName = 'ERua'
      Size = 30
    end
    object QrEmpresaECompl: TWideStringField
      FieldName = 'ECompl'
      Size = 30
    end
    object QrEmpresaEBairro: TWideStringField
      FieldName = 'EBairro'
      Size = 30
    end
    object QrEmpresaECidade: TWideStringField
      FieldName = 'ECidade'
      Size = 25
    end
    object QrEmpresaECEP: TIntegerField
      FieldName = 'ECEP'
    end
    object QrEmpresaETe1: TWideStringField
      FieldName = 'ETe1'
    end
    object QrEmpresaEFax: TWideStringField
      FieldName = 'EFax'
    end
    object QrEmpresaETE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ETE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrEmpresaFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Size = 40
      Calculated = True
    end
    object QrEmpresaFormaSociet: TWideStringField
      FieldName = 'FormaSociet'
    end
    object QrEmpresaCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrEmpresaE_LNR: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_LNR'
      Size = 255
      Calculated = True
    end
    object QrEmpresaNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Required = True
      Size = 2
    end
    object QrEmpresaNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 10
      Calculated = True
    end
    object QrEmpresaNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrEmpresaECEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ECEP_TXT'
      Calculated = True
    end
    object QrEmpresaAtividade: TWideStringField
      FieldName = 'Atividade'
      Size = 50
    end
    object QrEmpresaENATAL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ENATAL_TXT'
      Calculated = True
    end
    object QrEmpresaENumero: TIntegerField
      FieldName = 'ENumero'
    end
    object QrEmpresaNIRE: TWideStringField
      FieldName = 'NIRE'
      Size = 15
    end
  end
  object DsEmpresa: TDataSource
    DataSet = QrEmpresa
    Left = 312
    Top = 8
  end
  object QrRepr0: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrSociosCalcFields
    SQL.Strings = (
      'SELECT en.Codigo, en.Nome, en.Cargo'
      'FROM entidades en'
      'WHERE Empresa=:P0'
      'ORDER BY en.Nome')
    Left = 284
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrRepr0Codigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrRepr0Nome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrRepr0Cargo: TWideStringField
      FieldName = 'Cargo'
      Size = 60
    end
  end
  object DsRepr0: TDataSource
    DataSet = QrRepr0
    Left = 312
    Top = 64
  end
  object QrRepr1: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrSociosCalcFields
    SQL.Strings = (
      'SELECT CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome '
      'END NOMESOCIO, en.Codigo, en.Sexo, en.Pai, en.Mae, en.PNatal,'
      'en.CidadeNatal, en.Nacionalid, en.ConjugeNome, en.CPF, en.RG, '
      'en.SSP, en.DataRG, en.PRua, en.PNumero, en.PCompl, en.PBairro, '
      'en.PCidade, en.PCEP, en.PTe1, en.Profissao, en.Cargo, '
      'uf.Nome NOMEPUF, ll.Nome NOMEPLOGRAD, le.Nome NOMEECIVIL'
      'FROM socios so'
      'LEFT JOIN entidades   en ON en.Codigo=so.Socio'
      'LEFT JOIN ufs uf         ON uf.Codigo=en.PUF'
      'LEFT JOIN listalograd ll ON ll.Codigo=en.PLograd'
      'LEFT JOIN listaecivil le ON le.Codigo=en.EstCivil'
      'WHERE so.Empresa=:P0'
      'ORDER BY so.Ordem, NOMESOCIO')
    Left = 284
    Top = 92
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrRepr1NOMESOCIO: TWideStringField
      FieldName = 'NOMESOCIO'
      Size = 100
    end
    object QrRepr1Codigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrRepr1Sexo: TWideStringField
      FieldName = 'Sexo'
      Size = 1
    end
    object QrRepr1Pai: TWideStringField
      FieldName = 'Pai'
      Size = 60
    end
    object QrRepr1Mae: TWideStringField
      FieldName = 'Mae'
      Size = 60
    end
    object QrRepr1PNatal: TDateField
      FieldName = 'PNatal'
    end
    object QrRepr1CidadeNatal: TWideStringField
      FieldName = 'CidadeNatal'
      Size = 30
    end
    object QrRepr1Nacionalid: TWideStringField
      FieldName = 'Nacionalid'
      Size = 15
    end
    object QrRepr1ConjugeNome: TWideStringField
      FieldName = 'ConjugeNome'
      Size = 35
    end
    object QrRepr1CPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrRepr1RG: TWideStringField
      FieldName = 'RG'
      Size = 15
    end
    object QrRepr1SSP: TWideStringField
      FieldName = 'SSP'
      Size = 10
    end
    object QrRepr1DataRG: TDateField
      FieldName = 'DataRG'
    end
    object QrRepr1PRua: TWideStringField
      FieldName = 'PRua'
      Size = 30
    end
    object QrRepr1PCompl: TWideStringField
      FieldName = 'PCompl'
      Size = 30
    end
    object QrRepr1PBairro: TWideStringField
      FieldName = 'PBairro'
      Size = 30
    end
    object QrRepr1PCidade: TWideStringField
      FieldName = 'PCidade'
      Size = 25
    end
    object QrRepr1PCEP: TIntegerField
      FieldName = 'PCEP'
    end
    object QrRepr1PTe1: TWideStringField
      FieldName = 'PTe1'
    end
    object QrRepr1Profissao: TWideStringField
      FieldName = 'Profissao'
      Size = 60
    end
    object QrRepr1Cargo: TWideStringField
      FieldName = 'Cargo'
      Size = 60
    end
    object QrRepr1NOMEPUF: TWideStringField
      FieldName = 'NOMEPUF'
      Required = True
      Size = 2
    end
    object QrRepr1NOMEPLOGRAD: TWideStringField
      FieldName = 'NOMEPLOGRAD'
      Size = 10
    end
    object QrRepr1NOMEECIVIL: TWideStringField
      FieldName = 'NOMEECIVIL'
      Required = True
      Size = 10
    end
    object QrRepr1PNumero: TIntegerField
      FieldName = 'PNumero'
    end
  end
  object DsRepr1: TDataSource
    DataSet = QrRepr1
    Left = 312
    Top = 92
  end
  object frxSocios: TfrxReport
    Version = '4.10.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.935345590300000000
    ReportOptions.LastChange = 39717.935345590300000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxSociosGetValue
    Left = 188
    Top = 88
    Datasets = <
      item
        DataSet = frxDsEmpresa
        DataSetName = 'frxDsEmpresa'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsRepr0
        DataSetName = 'frxDsRepr0'
      end
      item
        DataSet = frxDsRepr1
        DataSetName = 'frxDsRepr1'
      end
      item
        DataSet = frxDsSocios
        DataSetName = 'frxDsSocios'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        Height = 36.000000000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo19: TfrxMemoView
          Left = 2.000000000000000000
          Top = 5.763760000000000000
          Width = 608.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsMaster."Em"]')
          ParentFont = False
        end
        object Line3: TfrxLineView
          Left = 2.000000000000000000
          Top = 29.763760000000000000
          Width = 720.000000000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
        end
        object Memo3: TfrxMemoView
          Left = 542.000000000000000000
          Top = 5.763760000000000000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            'Folha: [PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object DetailData1: TfrxDetailData
        Height = 292.000000000000000000
        Top = 351.496290000000000000
        Width = 718.110700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsSocios
        DataSetName = 'frxDsSocios'
        RowCount = 0
        object Memo5: TfrxMemoView
          Left = 18.000000000000000000
          Top = 2.519479999999990000
          Width = 684.000000000000000000
          Height = 152.000000000000000000
          ShowHint = False
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        end
        object Memo38: TfrxMemoView
          Left = 34.000000000000000000
          Top = 6.519479999999990000
          Width = 64.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            '2.[frxDsSocios."ITEM"] - Nome: ')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Left = 98.000000000000000000
          Top = 6.519479999999990000
          Width = 600.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[frxDsSocios."NOMESOCIO"]')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Left = 34.000000000000000000
          Top = 22.519480000000000000
          Width = 64.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            'Sexo:')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 98.000000000000000000
          Top = 22.519480000000000000
          Width = 160.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[frxDsSocios."NOMESEXO"]')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Left = 258.000000000000000000
          Top = 22.519480000000000000
          Width = 88.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            'Data de nascimento:')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 346.000000000000000000
          Top = 22.519480000000000000
          Width = 352.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[frxDsSocios."PNatal"]')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 34.000000000000000000
          Top = 38.519480000000000000
          Width = 64.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            'Filia'#195#167#195#163'o:')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 98.000000000000000000
          Top = 38.519480000000000000
          Width = 600.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[frxDsSocios."Pai"]')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Left = 98.000000000000000000
          Top = 54.519480000000000000
          Width = 600.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[frxDsSocios."Mae"]')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Left = 34.000000000000000000
          Top = 70.519480000000000000
          Width = 64.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            'Naturalidade:')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          Left = 98.000000000000000000
          Top = 70.519480000000000000
          Width = 264.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[frxDsSocios."CidadeNatal"]')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Left = 362.000000000000000000
          Top = 70.519480000000000000
          Width = 68.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            'Nacionalidade:')
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          Left = 430.000000000000000000
          Top = 70.519480000000000000
          Width = 268.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[frxDsSocios."Nacionalid"]')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Left = 34.000000000000000000
          Top = 86.519480000000000000
          Width = 64.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            'Estado civil:')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Left = 98.000000000000000000
          Top = 86.519480000000000000
          Width = 600.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[frxDsSocios."NOMEECIVIL"]')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 34.000000000000000000
          Top = 102.519480000000000000
          Width = 160.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            'Nome do c'#195#180'njuge ou companheiro(a):')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Left = 194.000000000000000000
          Top = 102.519480000000000000
          Width = 504.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[frxDsSocios."ConjugeNome"]')
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          Left = 34.000000000000000000
          Top = 118.519480000000000000
          Width = 64.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            'CPF:')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Left = 94.000000000000000000
          Top = 118.519480000000000000
          Width = 604.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[frxDsSocios."CNPJ_TXT"]')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Left = 34.000000000000000000
          Top = 134.519480000000000000
          Width = 64.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            'CI')
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          Left = 98.000000000000000000
          Top = 134.519480000000000000
          Width = 156.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[frxDsSocios."RG"]')
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          Left = 254.000000000000000000
          Top = 134.519480000000000000
          Width = 40.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            'Emissor:')
          ParentFont = False
        end
        object Memo60: TfrxMemoView
          Left = 294.000000000000000000
          Top = 134.519480000000000000
          Width = 172.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[frxDsSocios."SSP"]')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          Left = 466.000000000000000000
          Top = 134.519480000000000000
          Width = 92.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            'Data da expedi'#195#167#195#163'o:')
          ParentFont = False
        end
        object Memo63: TfrxMemoView
          Left = 558.000000000000000000
          Top = 134.519480000000000000
          Width = 140.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[frxDsSocios."DATARG_TXT"]')
          ParentFont = False
        end
        object Memo62: TfrxMemoView
          Left = 18.000000000000000000
          Top = 154.519480000000000000
          Width = 684.000000000000000000
          Height = 92.000000000000000000
          ShowHint = False
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        end
        object Memo64: TfrxMemoView
          Left = 34.000000000000000000
          Top = 158.519480000000000000
          Width = 176.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            '2.[frxDsSocios."ITEM"].1 - Endere'#195#167'o:')
          ParentFont = False
        end
        object Memo65: TfrxMemoView
          Left = 98.000000000000000000
          Top = 176.519480000000000000
          Width = 600.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[frxDsSocios."E_LNR"]')
          ParentFont = False
        end
        object Memo66: TfrxMemoView
          Left = 34.000000000000000000
          Top = 176.519480000000000000
          Width = 64.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            'Logradouro: ')
          ParentFont = False
        end
        object Memo67: TfrxMemoView
          Left = 98.000000000000000000
          Top = 192.519480000000000000
          Width = 272.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[frxDsSocios."PCompl"]')
          ParentFont = False
        end
        object Memo68: TfrxMemoView
          Left = 34.000000000000000000
          Top = 192.519480000000000000
          Width = 64.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            'Complemento: ')
          ParentFont = False
        end
        object Memo69: TfrxMemoView
          Left = 434.000000000000000000
          Top = 192.519480000000000000
          Width = 264.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[frxDsSocios."PBairro"]')
          ParentFont = False
        end
        object Memo70: TfrxMemoView
          Left = 370.000000000000000000
          Top = 192.519480000000000000
          Width = 64.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            'Bairro: ')
          ParentFont = False
        end
        object Memo71: TfrxMemoView
          Left = 98.000000000000000000
          Top = 208.519480000000000000
          Width = 272.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[frxDsSocios."PCidade"]')
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          Left = 34.000000000000000000
          Top = 208.519480000000000000
          Width = 64.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            'Cidade: ')
          ParentFont = False
        end
        object Memo73: TfrxMemoView
          Left = 434.000000000000000000
          Top = 208.519480000000000000
          Width = 264.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[frxDsSocios."NOMEPUF"]')
          ParentFont = False
        end
        object Memo74: TfrxMemoView
          Left = 370.000000000000000000
          Top = 208.519480000000000000
          Width = 64.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            'Estado: ')
          ParentFont = False
        end
        object Memo75: TfrxMemoView
          Left = 98.000000000000000000
          Top = 224.519480000000000000
          Width = 120.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[frxDsSocios."ECEP_TXT"]')
          ParentFont = False
        end
        object Memo76: TfrxMemoView
          Left = 34.000000000000000000
          Top = 224.519480000000000000
          Width = 64.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            'CEP:')
          ParentFont = False
        end
        object Memo77: TfrxMemoView
          Left = 282.000000000000000000
          Top = 224.519480000000000000
          Width = 416.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[frxDsSocios."ETE1_TXT"]')
          ParentFont = False
        end
        object Memo78: TfrxMemoView
          Left = 218.000000000000000000
          Top = 224.519480000000000000
          Width = 64.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            'Telefone:')
          ParentFont = False
        end
        object Memo79: TfrxMemoView
          Left = 18.000000000000000000
          Top = 246.519480000000000000
          Width = 340.000000000000000000
          Height = 40.000000000000000000
          ShowHint = False
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        end
        object Memo80: TfrxMemoView
          Left = 34.000000000000000000
          Top = 250.519480000000000000
          Width = 312.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            '2.[frxDsSocios."ITEM"].2 - Atividade principal desenvolvida:')
          ParentFont = False
        end
        object Memo81: TfrxMemoView
          Left = 34.000000000000000000
          Top = 266.519480000000000000
          Width = 272.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[frxDsSocios."Profissao"]')
          ParentFont = False
        end
        object Memo82: TfrxMemoView
          Left = 358.000000000000000000
          Top = 246.519480000000000000
          Width = 344.000000000000000000
          Height = 40.000000000000000000
          ShowHint = False
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        end
        object Memo83: TfrxMemoView
          Left = 366.000000000000000000
          Top = 250.519480000000000000
          Width = 176.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            '2.[frxDsSocios."ITEM"].3 - Cargo')
          ParentFont = False
        end
        object Memo84: TfrxMemoView
          Left = 366.000000000000000000
          Top = 266.519480000000000000
          Width = 272.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[frxDsSocios."Cargo"]')
          ParentFont = False
        end
      end
      object DadosMestre1: TfrxMasterData
        Height = 209.779530000000000000
        Top = 117.165430000000000000
        Width = 718.110700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsEmpresa
        DataSetName = 'frxDsEmpresa'
        RowCount = 0
        object Memo1: TfrxMemoView
          Left = 18.000000000000000000
          Top = 0.850340000000003000
          Width = 176.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            '1 - Qualifica'#195#167#195#160'o da empresa contratante:')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 362.000000000000000000
          Top = 0.850340000000003000
          Width = 340.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = 'dd " de "mmmm" de "yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            'Cadastrada em: [frxDsEmpresa."Cadastro"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 14.000000000000000000
          Top = 187.850340000000000000
          Width = 688.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            
              '2 - Qualifica'#195#167#195#163'o do[VAR_S] Propriet'#195#161'rio[VAR_S], Controlador[V' +
              'AR_ES], Representante[VAR_S], Mandat'#195#161'rio[VAR_S] e Preposto[VAR_' +
              'S] da Contratante:')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 18.000000000000000000
          Top = 20.850340000000000000
          Width = 684.000000000000000000
          Height = 160.000000000000000000
          ShowHint = False
          Frame.Style = fsDot
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 2.000000000000000000
        end
        object Memo7: TfrxMemoView
          Left = 98.000000000000000000
          Top = 24.850340000000000000
          Width = 600.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[frxDsEmpresa."RazaoSocial"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 34.000000000000000000
          Top = 24.850340000000000000
          Width = 64.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            'Raz'#195#163'o Social: ')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 98.000000000000000000
          Top = 40.850340000000000000
          Width = 264.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[frxDsEmpresa."FormaSociet"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 34.000000000000000000
          Top = 40.850340000000000000
          Width = 64.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            'Forma: ')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 466.000000000000000000
          Top = 40.850340000000000000
          Width = 232.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HideZeros = True
          Memo.UTF8 = (
            '[frxDsEmpresa."ENATAL_TXT"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 362.000000000000000000
          Top = 40.850340000000000000
          Width = 104.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HideZeros = True
          Memo.UTF8 = (
            'Data funda'#195#167#195#163'o empresa: ')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 98.000000000000000000
          Top = 56.850340000000000000
          Width = 168.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[frxDsEmpresa."CNPJ_TXT"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 34.000000000000000000
          Top = 56.850340000000000000
          Width = 64.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            'CNPJ:')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 330.000000000000000000
          Top = 56.850340000000000000
          Width = 168.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[frxDsEmpresa."IE"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 266.000000000000000000
          Top = 56.850340000000000000
          Width = 64.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            'I.E.:')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 562.000000000000000000
          Top = 56.850340000000000000
          Width = 136.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[frxDsEmpresa."NIRE"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 498.000000000000000000
          Top = 56.850340000000000000
          Width = 64.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            'NIRE:')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 34.000000000000000000
          Top = 76.850340000000000000
          Width = 176.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            '1 - Endere'#195#167'o:')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 98.000000000000000000
          Top = 94.850340000000000000
          Width = 600.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[frxDsEmpresa."E_LNR"]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 34.000000000000000000
          Top = 94.850340000000000000
          Width = 64.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            'Logradouro: ')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 98.000000000000000000
          Top = 110.850340000000000000
          Width = 272.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[frxDsEmpresa."ECompl"]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 34.000000000000000000
          Top = 110.850340000000000000
          Width = 64.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            'Complemento: ')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 434.000000000000000000
          Top = 110.850340000000000000
          Width = 264.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[frxDsEmpresa."EBairro"]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 370.000000000000000000
          Top = 110.850340000000000000
          Width = 64.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            'Bairro: ')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 98.000000000000000000
          Top = 126.850340000000000000
          Width = 272.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[frxDsEmpresa."ECidade"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 34.000000000000000000
          Top = 126.850340000000000000
          Width = 64.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            'Cidade: ')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 434.000000000000000000
          Top = 126.850340000000000000
          Width = 264.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[frxDsEmpresa."NOMEUF"]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 370.000000000000000000
          Top = 126.850340000000000000
          Width = 64.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            'Estado: ')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 98.000000000000000000
          Top = 142.850340000000000000
          Width = 120.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[frxDsEmpresa."ECEP_TXT"]')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 34.000000000000000000
          Top = 142.850340000000000000
          Width = 64.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            'CEP:')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 282.000000000000000000
          Top = 142.850340000000000000
          Width = 196.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[frxDsEmpresa."ETE1_TXT"]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 218.000000000000000000
          Top = 142.850340000000000000
          Width = 64.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            'Telefone:')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 34.000000000000000000
          Top = 162.850340000000000000
          Width = 176.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            '1.2 - Atividade principal desenvolvida:')
          ParentFont = False
        end
        object Memo85: TfrxMemoView
          Left = 214.000000000000000000
          Top = 162.850340000000000000
          Width = 484.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[frxDsEmpresa."Atividade"]')
          ParentFont = False
        end
      end
      object DetailData2: TfrxDetailData
        Height = 43.330240000000000000
        Top = 714.331170000000000000
        Width = 718.110700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsRepr0
        DataSetName = 'frxDsRepr0'
        RowCount = 0
        object Memo87: TfrxMemoView
          Left = 161.559060000000000000
          Top = 2.464129999999950000
          Width = 420.000000000000000000
          Height = 20.000000000000000000
          ShowHint = False
          Frame.Typ = [ftBottom]
        end
        object Memo88: TfrxMemoView
          Left = 161.559060000000000000
          Top = 24.464130000000000000
          Width = 420.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsRepr0."Nome"]')
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        Height = 26.346010000000000000
        Top = 665.197280000000000000
        Width = 718.110700000000000000
        object Memo86: TfrxMemoView
          Left = 18.000000000000000000
          Top = 8.377550000000040000
          Width = 688.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            'Nome[VAR_S2] do[VAR_S2] respons'#195#161've[VAR_IS2]:')
          ParentFont = False
        end
      end
      object Band1: TfrxHeader
        Height = 24.000000000000000000
        Top = 782.362710000000000000
        Width = 718.110700000000000000
        object Memo89: TfrxMemoView
          Left = 18.220470000000000000
          Top = 3.653059999999980000
          Width = 688.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            
              'Assinaturas do[VAR_S3] Representante[VAR_S3] Lega[VAR_IS3] da Co' +
              'ntratante:')
          ParentFont = False
        end
      end
      object Band2: TfrxDetailData
        Height = 64.000000000000000000
        Top = 827.717070000000000000
        Width = 718.110700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsRepr1
        DataSetName = 'frxDsRepr1'
        RowCount = 0
        object Memo90: TfrxMemoView
          Left = 150.000000000000000000
          Top = 1.739640000000010000
          Width = 420.000000000000000000
          Height = 20.000000000000000000
          ShowHint = False
          Frame.Typ = [ftBottom]
        end
        object Memo91: TfrxMemoView
          Left = 150.000000000000000000
          Top = 23.739640000000000000
          Width = 420.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            'Nome: [frxDsRepr1."NOMESOCIO"]')
          ParentFont = False
        end
        object Memo92: TfrxMemoView
          Left = 150.000000000000000000
          Top = 42.739640000000000000
          Width = 420.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            'Cargo: [frxDsRepr1."Cargo"]')
          ParentFont = False
        end
      end
    end
  end
  object frxDsEmpresa: TfrxDBDataset
    UserName = 'frxDsEmpresa'
    CloseDataSource = False
    DataSet = QrEmpresa
    BCDToCurrency = False
    Left = 256
    Top = 8
  end
  object frxDsSocios: TfrxDBDataset
    UserName = 'frxDsSocios'
    CloseDataSource = False
    DataSet = QrSocios
    BCDToCurrency = False
    Left = 256
    Top = 36
  end
  object frxDsRepr0: TfrxDBDataset
    UserName = 'frxDsRepr0'
    CloseDataSource = False
    DataSet = QrRepr0
    BCDToCurrency = False
    Left = 256
    Top = 64
  end
  object frxDsRepr1: TfrxDBDataset
    UserName = 'frxDsRepr1'
    CloseDataSource = False
    DataSet = QrRepr1
    BCDToCurrency = False
    Left = 256
    Top = 92
  end
end
