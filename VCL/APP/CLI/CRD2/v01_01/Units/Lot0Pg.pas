unit Lot0Pg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DBCtrls, Db, mySQLDbTables, ComCtrls,
  Menus, Mask, frxClass, frxDBSet, dmkGeral, dmkDBLookupComboBox, dmkEdit,
  dmkEditCB, UnDmkProcFunc, dmkImage, UnDmkEnums;

type
  TFmLot0PG = class(TForm)
    QrCarteiras: TmySQLQuery;
    DsCarteiras: TDataSource;
    QrLocCC: TmySQLQuery;
    QrLocCCCodigo: TIntegerField;
    QrLocCCTipo: TIntegerField;
    QrLocCCNome: TWideStringField;
    QrLocCCInicial: TFloatField;
    QrLocCCBanco: TIntegerField;
    QrLocCCID: TWideStringField;
    QrLocCCFatura: TWideStringField;
    QrLocCCID_Fat: TWideStringField;
    QrLocCCSaldo: TFloatField;
    QrLocCCEmCaixa: TFloatField;
    QrLocCCFechamento: TIntegerField;
    QrLocCCPrazo: TSmallintField;
    QrLocCCPagRec: TIntegerField;
    QrLocCCDiaMesVence: TSmallintField;
    QrLocCCExigeNumCheque: TSmallintField;
    QrLocCCForneceI: TIntegerField;
    QrLocCCLk: TIntegerField;
    QrLocCCDataCad: TDateField;
    QrLocCCDataAlt: TDateField;
    QrLocCCUserCad: TIntegerField;
    QrLocCCUserAlt: TIntegerField;
    QrLocCCNome2: TWideStringField;
    QrLocCCTipoDoc: TSmallintField;
    QrLocCCBanco1: TIntegerField;
    QrLocCCAgencia1: TIntegerField;
    QrLocCCConta1: TWideStringField;
    QrLocCCCheque1: TIntegerField;
    QrCreditados: TmySQLQuery;
    DsCreditados: TDataSource;
    QrCreditadosLastPaymt: TIntegerField;
    QrCreditadosBAC: TWideStringField;
    QrCreditadosNome: TWideStringField;
    QrCreditadosCPF: TWideStringField;
    QrCreditadosBanco: TIntegerField;
    QrCreditadosAgencia: TIntegerField;
    QrCreditadosConta: TWideStringField;
    QrLocCr: TmySQLQuery;
    QrLocCrLastPaymt: TIntegerField;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasTipo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    QrCarteirasInicial: TFloatField;
    QrCarteirasBanco: TIntegerField;
    QrCarteirasID: TWideStringField;
    QrCarteirasFatura: TWideStringField;
    QrCarteirasID_Fat: TWideStringField;
    QrCarteirasSaldo: TFloatField;
    QrCarteirasEmCaixa: TFloatField;
    QrCarteirasFechamento: TIntegerField;
    QrCarteirasPrazo: TSmallintField;
    QrCarteirasPagRec: TIntegerField;
    QrCarteirasDiaMesVence: TSmallintField;
    QrCarteirasExigeNumCheque: TSmallintField;
    QrCarteirasForneceI: TIntegerField;
    QrCarteirasLk: TIntegerField;
    QrCarteirasDataCad: TDateField;
    QrCarteirasDataAlt: TDateField;
    QrCarteirasUserCad: TIntegerField;
    QrCarteirasUserAlt: TIntegerField;
    QrCarteirasNome2: TWideStringField;
    QrCarteirasTipoDoc: TSmallintField;
    QrCarteirasBanco1: TIntegerField;
    QrCarteirasAgencia1: TIntegerField;
    QrCarteirasConta1: TWideStringField;
    QrCarteirasCheque1: TIntegerField;
    QrCarteirasContato1: TWideStringField;
    QrCarteirasNOMEFORNECEI: TWideStringField;
    QrCarteirasNOME2DOBANCO: TWideStringField;
    QrCarteirasNOMETIPODOC: TWideStringField;
    QrBanco1: TmySQLQuery;
    QrBanco1Nome: TWideStringField;
    frxDOC_TED: TfrxReport;
    frxDsCarteiras: TfrxDBDataset;
    PMCheque: TPopupMenu;
    Pertochek1: TMenuItem;
    Texto1: TMenuItem;
    Portador1: TMenuItem;
    Nominal1: TMenuItem;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    BitBtn2: TBitBtn;
    BtOK: TBitBtn;
    BtCheque: TBitBtn;
    BtEnvelope: TBitBtn;
    BitBtn1: TBitBtn;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label10: TLabel;
    PnCreditado: TPanel;
    Label14: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label11: TLabel;
    Label13: TLabel;
    DBEdit5: TDBEdit;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    CBCreditado: TDBLookupComboBox;
    EdCreditado: TdmkEdit;
    EdBanco2: TdmkEdit;
    EdAgencia2: TdmkEdit;
    EdConta2: TdmkEdit;
    EdCPF2: TdmkEdit;
    GroupBox2: TGroupBox;
    Label75: TLabel;
    Label1: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    Label37: TLabel;
    Label38: TLabel;
    EdCarteira: TdmkEditCB;
    CBCarteira: TdmkDBLookupComboBox;
    EdValor: TdmkEdit;
    TPData: TDateTimePicker;
    EdTxt: TdmkEdit;
    EdBanda1: TdmkEdit;
    EdBanco1: TdmkEdit;
    EdAgencia1: TdmkEdit;
    EdConta1: TdmkEdit;
    EdCheque1: TdmkEdit;
    EdCMC_7_1: TEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdBanda1Change(Sender: TObject);
    procedure EdCMC_7_1Change(Sender: TObject);
    procedure QrCreditadosAfterScroll(DataSet: TDataSet);
    procedure CBCreditadoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCreditadoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtChequeClick(Sender: TObject);
    procedure Nominal1Click(Sender: TObject);
    procedure Portador1Click(Sender: TObject);
    procedure Pertochek1Click(Sender: TObject);
    procedure PMChequePopup(Sender: TObject);
    procedure BtEnvelopeClick(Sender: TObject);
    procedure QrCarteirasCalcFields(DataSet: TDataSet);
    procedure BitBtn1Click(Sender: TObject);
    procedure frxDOC_TEDGetValue(const VarName: String;
      var Value: Variant);
  private
    { Private declarations }
    procedure ConfiguraCBCreditado;
  public
    { Public declarations }
    FCliente, FFatNum, FFatParcela: Integer;
    FNomeCreditado, FCNPJCreditado: String;
    FValor: Double;
    procedure ReopenCreditados;
  end;

  var
  FmLot0PG: TFmLot0PG;

implementation

{$R *.DFM}

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, Principal, MyListas,
  EmiteCheque_0, Lot2Cab, UnFinanceiro, ModuleFin, ModuleGeral;

procedure TFmLot0PG.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLot0PG.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  if EdCreditado.Visible then
    EdCreditado.SetFocus;
end;

procedure TFmLot0PG.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLot0PG.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  //
  EdCreditado.Top := CBCreditado.Top;
  UMyMod.AbreQuery(QrCarteiras, Dmod.MyDB);
end;

procedure TFmLot0PG.BtOKClick(Sender: TObject);
var
  Carteira(*, Controle, Banco*): Integer;
  Nome, DataStr: String;
  Doc, Debito: Double;
begin
  Carteira := EdCarteira.ValueVariant;
  if Carteira = 0 then
  begin
    Application.MessageBox('Informe a carteira!', 'Erro', MB_OK+MB_ICONERROR);
    EdCarteira.SetFocus;
    Exit;
  end;
  Debito := Geral.DMV(EdValor.Text);
  if Debito = 0 then
  begin
    Application.MessageBox('Informe o valor!', 'Erro', MB_OK+MB_ICONERROR);
    EdValor.SetFocus;
    Exit;
  end;
  //Banco   := EdBanco2.ValueVariant;
  Doc := EdCheque1.ValueVariant;
  //
  DataStr := FormatDateTime(VAR_FORMATDATE, TPData.Date);
  UFinanceiro.LancamentoDefaultVARS;
  //
  FLAN_Documento    := Doc;
  FLAN_Data         := DataStr;
  FLAN_Tipo         := QrCarteirasTipo.Value;
  FLAN_Carteira     := Carteira;
  FLAN_Debito       := Debito;
  //  FLAN_Genero       := - 3 9 7;
  if not DModFin.ObtemGeneroDeFatID(VAR_FATID_0302, FLAN_Genero, tpDeb, True) then
    Exit;
  FLAN_Vencimento   := DataStr;
  FLAN_Descricao    := EdTxt.Text;
  FLAN_Sit          := 2;
  FLAN_Cliente      := FCliente;
  FLAN_DataDoc      := DataStr;
  FLAN_FatID        := VAR_FATID_0302;
  FLAN_FatNum       := FFatNum;
  FLAN_FatParcela   := FFatParcela;
  //
  //
  FLAN_Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres',
        'Controle', CO_TabLctA, LAN_CTOS, 'Controle');
  VAR_LANCTO2 := FLAN_Controle;
  //
  if UFinanceiro.InsereLancamento(CO_TabLctA) then
  begin
    if CBCreditado.Visible then Nome := CBCreditado.Text
    else Nome := EdCreditado.Text;
    FmPrincipal.AtualizaCreditado(EdBanco2.Text, EdAgencia2.Text, EdConta2.Text,
      Geral.SoNumero_TT(EdCPF2.Text), Nome, FCliente, FLAN_Controle);

    if Doc > 0.1 then EdCheque1.Text := Geral.FFT(Doc+1, 0, siPositivo);
    CBCreditado.Visible := True;
    PnCreditado.Visible := True;
    EdCreditado.Visible := False;
    FValor := FValor - Debito;
    if FValor < 0 then FValor := 0;
    EdValor.Text := Geral.FFT(FValor, 2, siPositivo);
    EdValor.SetFocus;
    ReopenCreditados;
    case FmPrincipal.FFormLotShow of
      {
      0:
      begin
        FmLot0.CalculaLote(FFatNum, False);
        FmLot0.LocCod(FFatNum, FFatNum);
        FmLot0.FMudouSaldo := True;
      end;
      1:
      begin
        FmLot1.CalculaLote(FFatNum, False);
        FmLot1.LocCod(FFatNum, FFatNum);
        FmLot1.FMudouSaldo := True;
      end;
      }
      2:
      begin
        FmLot2Cab.CalculaLote(FFatNum, False);
        FmLot2Cab.LocCod(FFatNum, FFatNum);
        FmLot2Cab.FMudouSaldo := True;
      end;
      else Application.MessageBox(PChar('Janela de gerenciamento de border�s ' +
      'n�o definida! (15)'), 'ERRO', MB_OK+MB_ICONERROR);
    end;
  end;
end;

procedure TFmLot0PG.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_ESCAPE then BtSaidaClick(Self);
end;

procedure TFmLot0PG.EdBanda1Change(Sender: TObject);
begin
  if MLAGeral.LeuTodoCMC7(EdBanda1.Text) then
    EdCMC_7_1.Text := Geral.SoNumero_TT(EdBanda1.Text);
end;

procedure TFmLot0PG.EdCMC_7_1Change(Sender: TObject);
var
  Banda1: TBandaMagnetica;
begin
  if MLAGeral.CalculaCMC7(EdCMC_7_1.Text) = 0 then
  begin
    Banda1 := TBandaMagnetica.Create;
    Banda1.BandaMagnetica := EdCMC_7_1.Text;
    //EdComp1.Text    := Banda.Compe;
    EdBanco1.Text   := Banda1.Banco;
    EdAgencia1.Text := Banda1.Agencia;
    EdConta1.Text   := Banda1.Conta;
    EdCheque1.Text  := Banda1.Numero;
    //
    //LocalizaContaCorrente;
    QrLocCC.Close;
    QrLocCC.Params[00].AsString := Banda1.Banco;
    QrLocCC.Params[01].AsString := Banda1.Agencia;
    QrLocCC.Params[02].AsString := Banda1.Conta;
    UMyMod.AbreQuery(QrLocCC, Dmod.MyDB);
    if QrLocCC.RecordCount > 0 then
    begin
      EdCarteira.Text := IntToStr(QrLocCCCodigo.Value);
      CBCarteira.KeyValue := QrLocCCCodigo.Value;
      CBCreditado.SetFocus;
    end else EdCarteira.SetFocus;
  end;
end;

procedure TFmLot0PG.QrCreditadosAfterScroll(DataSet: TDataSet);
begin
  if CBCreditado.Visible then ConfiguraCBCreditado;
end;

procedure TFmLot0PG.ConfiguraCBCreditado;
begin
  if CBCreditado.Visible then
  begin
    EdCreditado.Text:= QrCreditadosNome.Value;
    EdCPF2.Text     := Geral.FormataCNPJ_TT(QrCreditadosCPF.Value);
    EdBanco2.Text   := MLAGeral.FFD(QrCreditadosBanco.Value, 3, siPositivo);
    EdAgencia2.Text := MLAGeral.FFD(QrCreditadosAgencia.Value, 4, siPositivo);
    EdConta2.Text   := QrCreditadosConta.Value;
  end;
end;

procedure TFmLot0PG.CBCreditadoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (not CBCreditado.ListVisible) then
  begin
    EdCreditado.Visible := True;
    CBCreditado.Visible := False;
    PnCreditado.Visible := False;
    EdCreditado.Text    := FNomeCreditado;
    EdCPF2.Text         := Geral.FormataCNPJ_TT(FCNPJCreditado);
    EdBanco2.Text       := '';
    EdAgencia2.Text     := '';
    EdConta2.Text       := '';
    EdCreditado.SetFocus;
  end;
end;

procedure TFmLot0PG.ReopenCreditados;
begin
  QrCreditados.Close;
  QrCreditados.Params[0].AsInteger := FCliente;
  UMyMod.AbreQuery(QrCreditados, Dmod.MyDB);
  //
  if QrCreditados.RecordCount > 0 then
  begin
    QrLocCr.Close;
    QrLocCr.Params[0].AsInteger := FCliente;
    //QrLocCr.Params[1].AsInteger := FCliente;
    UMyMod.AbreQuery(QrLocCr, Dmod.MyDB);
    //
    if QrCreditados.Locate('LastPaymt', QrLocCrLastPaymt.Value, []) then
    CBCreditado.KeyValue := QrLocCrLastPaymt.Value;
  end;
end;

procedure TFmLot0PG.EdCreditadoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
  begin
    CBCreditado.Visible := True;
    PnCreditado.Visible := True;
    EdCreditado.Visible := False;
    //
    CBCreditado.SetFocus;
    ConfiguraCBCreditado;
  end;
end;

procedure TFmLot0PG.BtChequeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCheque, BtCheque);
end;

procedure TFmLot0PG.Nominal1Click(Sender: TObject);
var
  Creditado: String;
begin
  if CBCreditado.Visible then Creditado := CBCreditado.Text
  else Creditado := EdCreditado.Text;
  FmPrincipal.ImprimeChequeSimples(EdValor.Text, Creditado,
    DModG.QrDonoCIDADE.Value, TPData.Date, True);
end;

procedure TFmLot0PG.Portador1Click(Sender: TObject);
var
  Creditado: String;
begin
  if CBCreditado.Visible then Creditado := CBCreditado.Text
  else Creditado := EdCreditado.Text;
  FmPrincipal.ImprimeChequeSimples(EdValor.Text, Creditado,
    DModG.QrDonoCIDADE.Value, TPData.Date, False);
end;

procedure TFmLot0PG.Pertochek1Click(Sender: TObject);
var
  Valor, Benef, Cidade: String;
begin
  // MUDADO EM 2010-05-05
  // N�o foi testado!!!!
{
  Application.CreateForm(TFmEmiteCheque, FmEmiteCheque);
  FmEmiteCheque.TPData.Date  := TPData.Date;
  FmEmiteCheque.EdValor.Text := EdValor.Text;
  if CBCreditado.Visible then FmEmiteCheque.EdBenef.Text := CBCreditado.Text
  else FmEmiteCheque.EdBenef.Text := EdCreditado.Text;
  FmEmiteCheque.EdCidade.Text := Geral.SemAcento(Geral.Maiusculas(
  DModG.QrDonoCIDADE.Value, False));
  //
  FmEmiteCheque.ShowModal;
  if FmEmiteCheque.ST_CMC7.Caption <> '' then
    EdBanda1.Text := MLAGeral.SoNumeroECMC7(FmEmiteCheque.ST_CMC7.Caption);
  FmEmiteCheque.Destroy;
}
  {
  if FmPrincipal.QrLctDebito.Value <> 0 then
  begin
    Valor := Geral.FFT(FmPrincipal.QrLctDebito.Value, 2, siPositivo);
    Benef := FmPrincipal.QrLctNOMEFORNECEDOR.Value;
  end else begin
    Valor := Geral.FFT(FmPrincipal.QrLctCredito.Value, 2, siPositivo);
    Benef := FmPrincipal.QrLctNOMECLIENTE.Value;
  end;
  }
  Valor := EdValor.Text;
  Benef := QrCreditadosNome.Value;
  //
  Benef := Geral.SemAcento(Geral.Maiusculas(Benef, False));
  Cidade := Geral.SemAcento(Geral.Maiusculas(DModG.QrDonoCIDADE.Value, False));
  //
  Application.CreateForm(TFmEmiteCheque_0, FmEmiteCheque_0);
  FmEmiteCheque_0.TPData.Date   := TPData.Date;//FmPrincipal.QrLctData.Value;
  FmEmiteCheque_0.EdValor.Text  := Valor;
  FmEmiteCheque_0.EdBenef.Text  := Benef;
  FmEmiteCheque_0.EdCidade.Text := Cidade;
  FmEmiteCheque_0.EdBanco.Text  := EdBanco1.Text;//FormatFloat('0', FmPrincipal.QrCarteirasBanco1.Value);
  //
  FmEmiteCheque_0.ShowModal;
  FmEmiteCheque_0.Destroy;
end;

procedure TFmLot0PG.PMChequePopup(Sender: TObject);
begin
  if VAR_IMPRECHEQUE = 1 then
    Pertochek1.Enabled := True
  else
    Pertochek1.Enabled := False;
end;

procedure TFmLot0PG.BtEnvelopeClick(Sender: TObject);
begin
 MyObjects.frxMostra(frxDOC_TED, 'Carta a banco (DOC - TED)');
end;

procedure TFmLot0PG.QrCarteirasCalcFields(DataSet: TDataSet);
begin
  case QrCarteirasTipoDoc.Value of
    0: QrCarteirasNOMETIPODOC.Value := 'N/I';
    1: QrCarteirasNOMETIPODOC.Value := 'Cheque';
    2: QrCarteirasNOMETIPODOC.Value := 'DOC';
    3: QrCarteirasNOMETIPODOC.Value := 'TED';
    4: QrCarteirasNOMETIPODOC.Value := 'Esp�cie';
    else QrCarteirasNOMETIPODOC.Value := '???';
  end;
end;

procedure TFmLot0PG.BitBtn1Click(Sender: TObject);
begin
  case FmPrincipal.FFormLotShow of
    //0: FmLot0.Rpido1Click(Self);
    //1: FmLot1.Rpido1Click(Self);
    2: FmLot2Cab.AtualizaPagamento(FmLot2Cab.QrLotCodigo.Value,
       FmLot2Cab.QrLotCliente.Value, FmLot2Cab.QrLotData.Value,
       FmLot2Cab.QrLotVAL_LIQUIDO.Value);
    else Application.MessageBox(PChar('Janela de gerenciamento de border�s ' +
    'n�o definida! (16)'), 'ERRO', MB_OK+MB_ICONERROR);
  end;
  Close;
end;

procedure TFmLot0PG.frxDOC_TEDGetValue(const VarName: String;
  var Value: Variant);
begin
  if VarName = 'VARF_VALOR' then Value := EdValor.Text + ' ('+
    dmkPF.ExtensoMoney(EdValor.Text)+')'
  else if VarName = 'VARF_BANCO' then
  begin
    QrBanco1.Close;
    QrBanco1.Params[0].AsString := EdBanco2.Text;
    UMyMod.AbreQuery(QrBanco1, Dmod.MyDB);
    if QrBanco1.RecordCount > 0 then Value := EdBanco2.Text + ' - ' +
      QrBanco1Nome.Value else Value := EdBanco2.Text;
  end else if VarName = 'VARF_AGENCIA' then Value := EdAgencia2.Text
  else if VarName = 'VARF_CONTA' then Value := EdConta2.Text
  else if VarName = 'VARF_CREDITADO' then
  begin
    if CBCreditado.Visible then Value := CBCreditado.Text
    else Value := EdCreditado.Text;
  end else if VarName = 'VARF_CNPJ' then Value := EdCPF2.Text
  else if VarName = 'VARF_CIDADE_E_DATA' then Value := DModG.QrDonoCIDADE.Value
    + ', '+FormatDateTime('dd" de "mmmm" de "yyyy"."', TPData.Date)
end;

end.
