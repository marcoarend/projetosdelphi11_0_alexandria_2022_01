unit OperaCol;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DBCtrls, Db, mySQLDbTables, ComCtrls,
  Grids, DBGrids, frxClass, frxDBSet, Variants, dmkGeral, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, dmkImage, UnDmkProcFunc, DmkDAC_PF, UnDmkEnums;

type
  TFmOperaCol = class(TForm)
    PainelDados: TPanel;
    Label3: TLabel;
    EdCliente: TdmkEditCB;
    CbCliente: TdmkDBLookupComboBox;
    QrClientes: TmySQLQuery;
    DsClientes: TDataSource;
    QrClientesNOMECLIENTE: TWideStringField;
    QrClientesCodigo: TIntegerField;
    Label34: TLabel;
    TPIni: TDateTimePicker;
    Label1: TLabel;
    TPFim: TDateTimePicker;
    QrEmLot: TmySQLQuery;
    QrEmLotCodigo: TIntegerField;
    QrEmLotAdValorem: TFloatField;
    QrEmLotValValorem: TFloatField;
    QrEmLotTaxaVal: TFloatField;
    QrEmLotIRRF: TFloatField;
    QrEmLotIRRF_Val: TFloatField;
    QrEmLotISS: TFloatField;
    QrEmLotISS_Val: TFloatField;
    QrEmLotPIS: TFloatField;
    QrEmLotPIS_Val: TFloatField;
    QrEmLotPIS_R: TFloatField;
    QrEmLotPIS_R_Val: TFloatField;
    QrEmLotCOFINS: TFloatField;
    QrEmLotCOFINS_Val: TFloatField;
    QrEmLotCOFINS_R: TFloatField;
    QrEmLotCOFINS_R_Val: TFloatField;
    QrOper1: TmySQLQuery;
    QrOper1Codigo: TIntegerField;
    QrOper1Tipo: TSmallintField;
    QrOper1Spread: TSmallintField;
    QrOper1Cliente: TIntegerField;
    QrOper1Lote: TIntegerField;
    QrOper1Data: TDateField;
    QrOper1Total: TFloatField;
    QrOper1Dias: TFloatField;
    QrOper1PeCompra: TFloatField;
    QrOper1TxCompra: TFloatField;
    QrOper1ValValorem: TFloatField;
    QrOper1AdValorem: TFloatField;
    QrOper1IOC: TFloatField;
    QrOper1IOC_VAL: TFloatField;
    QrOper1Tarifas: TFloatField;
    QrOper1CPMF: TFloatField;
    QrOper1CPMF_VAL: TFloatField;
    QrOper1TipoAdV: TIntegerField;
    QrOper1IRRF: TFloatField;
    QrOper1IRRF_Val: TFloatField;
    QrOper1ISS: TFloatField;
    QrOper1ISS_Val: TFloatField;
    QrOper1PIS: TFloatField;
    QrOper1PIS_Val: TFloatField;
    QrOper1PIS_R: TFloatField;
    QrOper1PIS_R_Val: TFloatField;
    QrOper1COFINS: TFloatField;
    QrOper1COFINS_Val: TFloatField;
    QrOper1COFINS_R: TFloatField;
    QrOper1COFINS_R_Val: TFloatField;
    QrOper1OcorP: TFloatField;
    QrOper1MaxVencto: TDateField;
    QrOper1CHDevPg: TFloatField;
    QrOper1Lk: TIntegerField;
    QrOper1DataCad: TDateField;
    QrOper1DataAlt: TDateField;
    QrOper1UserCad: TIntegerField;
    QrOper1UserAlt: TIntegerField;
    QrOper1MINTC: TFloatField;
    QrOper1MINAV: TFloatField;
    QrOper1MINTC_AM: TFloatField;
    QrOper1PIS_T_Val: TFloatField;
    QrOper1COFINS_T_Val: TFloatField;
    QrOper1PgLiq: TFloatField;
    QrOper1DUDevPg: TFloatField;
    QrOper1NF: TIntegerField;
    QrOper1Itens: TIntegerField;
    QrOper1Conferido: TIntegerField;
    QrOper1ECartaSac: TSmallintField;
    QrOper1CBE: TIntegerField;
    QrOper1NOMECLIENTE: TWideStringField;
    QrEmLo1: TmySQLQuery;
    QrEmLo1Codigo: TIntegerField;
    QrEmLo1ValValorem: TFloatField;
    QrEmLo1TaxaVal: TFloatField;
    QrLReA: TmySQLQuery;
    QrLReACodigo: TIntegerField;
    QrLReAData: TDateField;
    QrLReANomeCli: TWideStringField;
    QrLReALote: TIntegerField;
    QrLReANF: TIntegerField;
    QrLReATotal: TFloatField;
    QrLReATC0: TFloatField;
    QrLReAVV0: TFloatField;
    QrLReATTC: TFloatField;
    QrLReATVV: TFloatField;
    QrLReAETC: TFloatField;
    QrLReAEVV: TFloatField;
    QrLReADias: TFloatField;
    QrLReATTC_T: TFloatField;
    QrLReAALL_T: TFloatField;
    QrLReAALL_J: TFloatField;
    QrLReATTC_J: TFloatField;
    QrLReAddVal: TFloatField;
    RGOrdem1: TRadioGroup;
    RGOrdem2: TRadioGroup;
    RGOrdem3: TRadioGroup;
    RGGrupos: TRadioGroup;
    QrLReANomeMes: TWideStringField;
    QrLReAMes: TIntegerField;
    RGSintetico: TRadioGroup;
    QrLReAQtdeDocs: TIntegerField;
    QrLReAMediaDoc: TFloatField;
    QrLReAITEM: TIntegerField;
    DBGrid1: TDBGrid;
    DsLReA: TDataSource;
    QrLReATC1: TFloatField;
    QrLReATC2: TFloatField;
    QrLReATC3: TFloatField;
    QrLReATC4: TFloatField;
    QrLReATC5: TFloatField;
    QrLReATC6: TFloatField;
    QrLReAVV1: TFloatField;
    QrLReAVV2: TFloatField;
    QrLReAVV3: TFloatField;
    QrLReAVV4: TFloatField;
    QrLReAVV5: TFloatField;
    QrLReAVV6: TFloatField;
    QrLReAISS_Val: TFloatField;
    QrLReAPIS_Val: TFloatField;
    QrLReACOFINS_Val: TFloatField;
    QrLReAImpostos: TFloatField;
    QrLReATRC_T: TFloatField;
    QrLReATRC_J: TFloatField;
    QrLReAddLiq: TFloatField;
    frxOper2: TfrxReport;
    frxDsLReA: TfrxDBDataset;
    frxOper3: TfrxReport;
    frxOper4: TfrxReport;
    GroupBox1: TGroupBox;
    LaN1: TLabel;
    La1: TLabel;
    LaN2: TLabel;
    La2: TLabel;
    La3: TLabel;
    LaN3: TLabel;
    La4: TLabel;
    LaN4: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    ProgressBar1: TProgressBar;
    BtCalcula: TBitBtn;
    BtResultado: TBitBtn;
    BitBtn1: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtCalculaClick(Sender: TObject);
    procedure BtResultadoClick(Sender: TObject);
    procedure TPIniChange(Sender: TObject);
    procedure TPFimChange(Sender: TObject);
    procedure QrLReACalcFields(DataSet: TDataSet);
    procedure DBGrid1TitleClick(Column: TColumn);
    procedure RGOrdem1Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure frxOper2GetValue(const VarName: String;
      var Value: Variant);
    function frxOper2UserFunction(const MethodName: String;
      var Params: Variant): Variant;
  private
    { Private declarations }
    FContaGF1: Integer;
    FDesc, FTitle, FOrdC1, FOrdC2, FOrdC3, FOrdC4, FOrdS1, FOrdS2, FOrdS3, FOrdS4: String;
    procedure EnabledBotoes(Habilita: Boolean);
    procedure ReopenQrLReA(Redefine: Boolean);
    function CalculaJuroMes_frxReport(d, v, t: Extended): String;
    procedure RedefineOrdem(Titulo: String; Novo: Boolean);
    function NomeDeExibicao(Nome: String): String;

  public
    { Public declarations }
  end;

  var
  FmOperaCol: TFmOperaCol;

implementation

{$R *.DFM}

uses UnMyObjects, Module, UnInternalConsts, Principal, UCreate, UMySQLModule, 
  MyListas;

const
  FORDEM_RG: array[0..2] of string = ('NomeCli', 'Data', 'Mes');

procedure TFmOperaCol.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOperaCol.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOperaCol.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOperaCol.EdClienteChange(Sender: TObject);
begin
  EnabledBotoes(False);
end;

procedure TFmOperaCol.FormCreate(Sender: TObject);
begin
  FOrdC1 := 'Total';
  FTitle := 'Total';
  FOrdS1 := '';
  FOrdC2 := '';
  FOrdS2 := '';
  FOrdC3 := '';
  FOrdS3 := '';
  //
  UMyMod.AbreQuery(QrClientes, Dmod.MyDB);
  TPIni.Date := Date - 30;
  TPFim.Date := Date;
end;

procedure TFmOperaCol.BtCalculaClick(Sender: TObject);
var
  Cliente, Periodo: Integer;
  i, j: Integer;
  TTC_T, TRC_T, TTC_J, TRC_J, MediaDoc: Double;
begin
  Screen.Cursor := crHourGlass;
  //Tempo := Now;
  UCriar.RecriaTabelaLocal('LReA', 1);
  Cliente := EdCliente.ValueVariant;
{
  QrOper1.Close;
  QrOper1.SQL.Clear;
  QrOper1.SQL.Add('SELECT lo.*, CASE WHEN en.Tipo=0 THEN en.RazaoSocial');
  QrOper1.SQL.Add('ELSE en.Nome END NOMECLIENTE');
  QrOper1.SQL.Add('FROM lot es lo');
  QrOper1.SQL.Add('LEFT JOIN entidades en ON en.Codigo=lo.Cliente');
  QrOper1.SQL.Add('WHERE lo.data BETWEEN :P0 AND :P1');
  if Cliente <> 0 then
    QrOper1.SQL.Add('AND lo.Cliente='+IntToStr(Cliente));
  //
  QrOper1.Params[00].AsString := FormatDateTime(VAR_FORMATDATE, TPIni.Date);
  QrOper1.Params[01].AsString := FormatDateTime(VAR_FORMATDATE, TPFim.Date);
  QrOper1. Open;
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrOper1, Dmod.MyDB, [
  'SELECT lo.*, CASE WHEN en.Tipo=0 THEN en.RazaoSocial ',
  'ELSE en.Nome END NOMECLIENTE ',
  'FROM ' + CO_TabLotA + ' lo ',
  'LEFT JOIN entidades en ON en.Codigo=lo.Cliente ',
  dmkPF.SQL_Periodo('WHERE lo.data ', TPIni.Date, TPFim.Date, True, True),
  //'WHERE lo.data BETWEEN :P0 AND :P1');
  Geral.ATS_if(Cliente <> 0, ['AND lo.Cliente=' + Geral.FF0(Cliente)]),
  '']);

  //
  Dmod.QrUpdL.SQL.Clear;
  Dmod.QrUpdL.SQL.Add('INSERT INTO lrea SET Codigo=:P0, Data=:P1, ');
  Dmod.QrUpdL.SQL.Add('NomeCli=:P2, Lote=:P3, NF=:P4, Total=:P5, ');
  Dmod.QrUpdL.SQL.Add('TC0=:P6, VV0=:P7, TTC=:P8, TVV=:P9, Dias=:P10, ');
  Dmod.QrUpdL.SQL.Add('Mes=:P11, NomeMes=:P12, QtdeDocs=:P13, MediaDoc=:P14, ');
  Dmod.QrUpdL.SQL.Add('ISS_Val=:P15, PIS_Val=:P16, COFINS_Val=:P17, ');
  Dmod.QrUpdL.SQL.Add('Impostos=:P18 ');
  ProgressBar1.Position := 0;
  ProgressBar1.Max := QrOper1.RecordCount;
  while not QrOper1.Eof do
  begin
    if QrOper1Itens.Value = 0 then MediaDoc := 0 else
    MediaDoc := QrOper1Total.Value / QrOper1Itens.Value;
    Periodo := Geral.Periodo2000(QrOper1Data.Value);
    ProgressBar1.Position := ProgressBar1.Position +1;
    Dmod.QrUpdL.Params[00].AsInteger := QrOper1Codigo.Value;
    Dmod.QrUpdL.Params[01].AsString  := Geral.FDT(QrOper1Data.Value, 1);
    Dmod.QrUpdL.Params[02].AsString  := QrOper1NOMECLIENTE.Value;
    Dmod.QrUpdL.Params[03].AsInteger := QrOper1Lote.Value;
    Dmod.QrUpdL.Params[04].AsInteger := QrOper1NF.Value;
    Dmod.QrUpdL.Params[05].AsFloat   := QrOper1Total.Value;
    Dmod.QrUpdL.Params[06].AsFloat   := QrOper1TxCompra.Value;
    Dmod.QrUpdL.Params[07].AsFloat   := QrOper1ValValorem.Value;
    Dmod.QrUpdL.Params[08].AsFloat   := QrOper1TxCompra.Value;
    Dmod.QrUpdL.Params[09].AsFloat   := QrOper1ValValorem.Value;
    Dmod.QrUpdL.Params[10].AsFloat   := QrOper1Dias.Value;
    Dmod.QrUpdL.Params[11].AsInteger := Periodo;
    Dmod.QrUpdL.Params[12].AsString  := MLAGeral.MesEAnoDoPeriodo(Periodo);
    Dmod.QrUpdL.Params[13].AsFloat   := QrOper1Itens.Value;
    Dmod.QrUpdL.Params[14].AsFloat   := MediaDoc;
    Dmod.QrUpdL.Params[15].AsFloat   := QrOper1ISS_Val.Value;
    Dmod.QrUpdL.Params[16].AsFloat   := QrOper1PIS_T_Val.Value;
    Dmod.QrUpdL.Params[17].AsFloat   := QrOper1COFINS_T_Val.Value;
    Dmod.QrUpdL.Params[18].AsFloat   := QrOper1ISS_Val.Value +
                                        QrOper1PIS_T_Val.Value +
                                        QrOper1COFINS_T_Val.Value;
    Dmod.QrUpdL.ExecSQL;
    QrOper1.Next;
  end;
  ProgressBar1.Position := 0;
{
  QrEmLo1.Close;
  QrEmLo1.SQL.Clear;
  QrEmLo1.SQL.Add('SELECT el.Codigo, el.ValValorem, el.TaxaVal');
  QrEmLo1.SQL.Add('FROM emlot el');
  QrEmLo1.SQL.Add('LEFT JOIN ' + TMeuDB + '.lot es lo');
  QrEmLo1.SQL.Add('ON lo.Codigo=el.Codigo');
  QrEmLo1.SQL.Add('WHERE lo.data BETWEEN :P0 AND :P1');
  if Cliente <> 0 then
    QrEmLo1.SQL.Add('AND lo.Cliente='+IntToStr(Cliente));
  //
  QrEmLo1.Params[00].AsString := FormatDateTime(VAR_FORMATDATE, TPIni.Date);
  QrEmLo1.Params[01].AsString := FormatDateTime(VAR_FORMATDATE, TPFim.Date);
  //
}
  //
  j := FmPrincipal.FMyDBs.MaxDBs -1;
  for i := 0 to j do
  begin
    if FmPrincipal.FMyDBs.Connected[i] = '1' then
    begin
      QrEmLo1.Close;
      QrEmLo1.Database := Dmod.FArrayMySQLBD[i];
{
      QrEmLo1. Open;
}
      UnDmkDAC_PF.AbreMySQLQuery0(QrEmLo1, Dmod.FArrayMySQLBD[i], [
      'SELECT el.Codigo, el.ValValorem, el.TaxaVal ',
      'FROM emlot el ',
      'LEFT JOIN ' + TMeuDB + '.' + CO_TabLotA + ' lo ',
      'ON lo.Codigo=el.Codigo ',
      dmkPF.SQL_Periodo('WHERE lo.data ', TPIni.Date, TPFim.Date, True, True),
      Geral.ATS_if(Cliente <> 0, ['AND lo.Cliente=' + Geral.FF0(Cliente)]),
      '']);
      //
      Dmod.QrUpdL.SQL.Clear;
      Dmod.QrUpdL.SQL.Add('UPDATE lrea SET TC'+IntToStr(i+1)+'=:P0, ');
      Dmod.QrUpdL.SQL.Add('VV'+IntToStr(i+1)+'=:P1, ETC=ETC+ :P2, ');
      Dmod.QrUpdL.SQL.Add('EVV=EVV+ :P3, TTC=TTC+ :P4, TVV=TVV+ :P5');
      Dmod.QrUpdL.SQL.Add('WHERE Codigo=:Pa');
      ProgressBar1.Max := QrEmLo1.RecordCount;
      while not QrEmLo1.Eof do
      begin
        ProgressBar1.Position := ProgressBar1.Position +1;
        Dmod.QrUpdL.Params[00].AsFloat   := QrEmLo1TaxaVal.Value;
        Dmod.QrUpdL.Params[01].AsFloat   := QrEmLo1ValValorem.Value;
        Dmod.QrUpdL.Params[02].AsFloat   := QrEmLo1TaxaVal.Value;
        Dmod.QrUpdL.Params[03].AsFloat   := QrEmLo1ValValorem.Value;
        Dmod.QrUpdL.Params[04].AsFloat   := QrEmLo1TaxaVal.Value;
        Dmod.QrUpdL.Params[05].AsFloat   := QrEmLo1ValValorem.Value;
        //
        Dmod.QrUpdL.Params[06].AsInteger := QrEmLo1Codigo.Value;
        Dmod.QrUpdL.ExecSQL;
        QrEmLo1.Next;
      end;
      ProgressBar1.Position := 0;
    end;
  end;
  QrLReA.Close;
  UMyMod.AbreQuery(QrLReA, Dmod.MyLocDatabase);
  ProgressBar1.Max := QrLReA.RecordCount;
  Dmod.QrUpdL.SQL.Clear;
  Dmod.QrUpdL.SQL.Add('UPDATE lrea SET TTC_T=:P0, TRC_T=:P1, TTC_J=:P2, ');
  Dmod.QrUpdL.SQL.Add('TRC_J=:P3, ddVal=:P4, ddLiq=:P5 WHERE Codigo=:Pa');
  QrLReA.DisableControls;
  QrLReA.First;
  while not QrLReA.Eof do
  begin
    ProgressBar1.Position := ProgressBar1.Position +1;
    if QrLReADias.Value = 0 then
    begin
      TTC_J := 0;
      TRC_J := 0;
    end else begin
      TTC_J := QrLReATTC.Value / QrLReATotal.Value * 100;
      TRC_J := (QrLReATTC.Value - QrLReaImpostos.Value) / QrLReATotal.Value * 100;
    end;
     //
    TTC_T := MLAGeral.DescobreJuroComposto(TTC_J, QrLReADias.Value, 2);
    TRC_T := MLAGeral.DescobreJuroComposto(TRC_J, QrLReADias.Value, 2);
    //
    Dmod.QrUpdL.Params[00].AsFloat   := TTC_T;
    Dmod.QrUpdL.Params[01].AsFloat   := TRC_T;
    Dmod.QrUpdL.Params[02].AsFloat   := TTC_J;
    Dmod.QrUpdL.Params[03].AsFloat   := TRC_J;
    Dmod.QrUpdL.Params[04].AsFloat   := QrLReATotal.Value * QrLReADias.Value;
    Dmod.QrUpdL.Params[05].AsFloat   := (QrLReATotal.Value - QrLReAImpostos.Value) * QrLReADias.Value;
    //
    Dmod.QrUpdL.Params[06].AsInteger := QrLReACodigo.Value;
    Dmod.QrUpdL.ExecSQL;
    QrLReA.Next;
  end;
  QrLReA.First;
  QrLReA.EnableControls;
  ProgressBar1.Position := 0;
  EnabledBotoes(True);
  ReopenQrLReA(False);
  Screen.Cursor := crDefault;
end;

procedure TFmOperaCol.BtResultadoClick(Sender: TObject);
begin
  //ReopenQrLReA; N�o pode para n�o mudar ordem
  case RGSintetico.ItemIndex of
    0:
    begin
      frxOper2.AddFunction(
        'function CalculaJuroMes_frxReport(d, v, t: Extended): String');
      MyObjects.frxMostra(frxOper2,
        'Relat�rio das Opera��es Realizadas com as Coligadas');
    end;
    1:
    begin
      case RGGrupos.ItemIndex of
        0: Application.MessageBox('Relat�rio sint�tico requer pelo menos um '+
           'agrupamento!', 'Erro', MB_OK+MB_ICONERROR);
        1:
        begin
          frxOper4.AddFunction(
            'function CalculaJuroMes_frxReport(d, v, t: Extended): String');
          MyObjects.frxMostra(frxOper4,
            'Relat�rio das Opera��es Realizadas com as Coligadas');
        end;
        2:
        begin
          frxOper3.AddFunction(
            'function CalculaJuroMes_frxReport(d, v, t: Extended): String');
          MyObjects.frxMostra(frxOper3,
            'Relat�rio das Opera��es Realizadas com as Coligadas');
        end
        else Application.MessageBox('Relat�rio n�o definido!', 'Erro',
             MB_OK+MB_ICONERROR);
      end;
    end;
    else Application.MessageBox('Relat�rio n�o definido!', 'Erro',
         MB_OK+MB_ICONERROR);
  end;
end;

function TFmOperaCol.CalculaJuroMes_frxReport(d, v, t: Extended): String;
var
  Taxa, Dias: Double;
  Res: String;
begin
  if v > 0 then
  begin
    Taxa := t / v * 100;
    Dias := d / v;
    Res := Geral.FFT(MLAGeral.DescobreJuroComposto(Taxa, Dias, 2), 2, siNegativo);
    Result := Res;
  end else Result := '0.00';
end;

procedure TFmOperaCol.TPIniChange(Sender: TObject);
begin
  EnabledBotoes(False);
end;

procedure TFmOperaCol.TPFimChange(Sender: TObject);
begin
  EnabledBotoes(False);
end;

procedure TFmOperaCol.EnabledBotoes(Habilita: Boolean);
begin
  BtCalcula.Enabled   := not Habilita;
  BtResultado.Enabled :=     Habilita;
  if BtResultado.Enabled = False then QrLReA.Close;
end;

procedure TFmOperaCol.ReopenQrLReA(Redefine: Boolean);
begin
  if Redefine then RedefineOrdem(FTitle, False);
  if BtResultado.Enabled = False then Exit;
  QrLReA.Close;
  QrLReA.SQL.Clear;
  QrLReA.SQL.Add('SELECT * FROM lrea');
  QrLReA.SQL.Add(MLAGeral.OrdemSQL4(
    FOrdC1, FOrdS1, FOrdC2, FOrdS2, FOrdC3, FOrdS3, FOrdC4, FOrdS4));
  UMyMod.AbreQuery(QrLReA, Dmod.MyLocDatabase);
end;

procedure TFmOperaCol.QrLReACalcFields(DataSet: TDataSet);
begin
  QrLReAITEM.Value := 1;
end;

procedure TFmOperaCol.DBGrid1TitleClick(Column: TColumn);
begin
  FDesc := MLAGeral.InverteOrdemAsc(FDesc);
  RedefineOrdem(Column.FieldName, True);
  if QrLReA.State = dsBrowse then ReopenQrLReA(True);
end;

procedure TFmOperaCol.RedefineOrdem(Titulo: String; Novo: Boolean);
begin
  case RGGrupos.ItemIndex of
    0:
    begin
      FOrdC1 := Titulo;
      FOrdS1 := FDesc;
      FOrdC2 := FORDEM_RG[RGOrdem1.ItemIndex];
      FOrdS2 := '';
      FOrdC3 := FORDEM_RG[RGOrdem2.ItemIndex];
      FOrdS3 := '';
      FOrdC4 := FORDEM_RG[RGOrdem3.ItemIndex];
      FOrdS4 := '';
    end;
    1:
    begin
      FOrdC1 := FORDEM_RG[RGOrdem1.ItemIndex];
      FOrdS1 := '';
      FOrdC2 := Titulo;
      FOrdS2 := FDesc;
      FOrdC3 := FORDEM_RG[RGOrdem2.ItemIndex];
      FOrdS3 := '';
      FOrdC4 := FORDEM_RG[RGOrdem3.ItemIndex];
      FOrdS4 := '';
    end;
    2:
    begin
      FOrdC1 := FORDEM_RG[RGOrdem1.ItemIndex];
      FOrdS1 := '';
      FOrdC2 := FORDEM_RG[RGOrdem2.ItemIndex];
      FOrdS2 := '';
      FOrdC3 := Titulo;
      FOrdS3 := FDesc;
      FOrdC4 := FORDEM_RG[RGOrdem3.ItemIndex];
      FOrdS4 := '';
    end;
  end;
  La1.Caption := NomeDeExibicao(FOrdC1);
  La2.Caption := NomeDeExibicao(FOrdC2);
  La3.Caption := NomeDeExibicao(FOrdC3);
  La4.Caption := NomeDeExibicao(FOrdC4);
  FTitle := Titulo;
  case RGGrupos.ItemIndex of
    0:
    begin
      La1.Font.Color := clActiveCaption;
      La2.Font.Color := clInactiveCaption;
      La3.Font.Color := clInactiveCaption;
      La4.Font.Color := clInactiveCaption;
      LaN1.Font.Color := clActiveCaption;
      LaN2.Font.Color := clInactiveCaption;
      LaN3.Font.Color := clInactiveCaption;
      LaN4.Font.Color := clInactiveCaption;
      La1.Caption := La1.Caption + FDesc;
    end;
    1:
    begin
      La1.Font.Color := clInactiveCaption;
      La2.Font.Color := clActiveCaption;
      La3.Font.Color := clInactiveCaption;
      La4.Font.Color := clInactiveCaption;
      LaN1.Font.Color := clInactiveCaption;
      LaN2.Font.Color := clActiveCaption;
      LaN3.Font.Color := clInactiveCaption;
      LaN4.Font.Color := clInactiveCaption;
      La2.Caption := La2.Caption + FDesc;
    end;
    2:
    begin
      La1.Font.Color := clInactiveCaption;
      La2.Font.Color := clInactiveCaption;
      La3.Font.Color := clActiveCaption;
      La4.Font.Color := clInactiveCaption;
      LaN1.Font.Color := clInactiveCaption;
      LaN2.Font.Color := clInactiveCaption;
      LaN3.Font.Color := clActiveCaption;
      LaN4.Font.Color := clInactiveCaption;
      La3.Caption := La3.Caption + FDesc;
    end;
  end;
end;

procedure TFmOperaCol.RGOrdem1Click(Sender: TObject);
begin
  ReopenQrLReA(True);
end;

function TFmOperaCol.NomeDeExibicao(Nome: String): String;
begin
  if Nome = 'NomeCli'       then Result := 'Cliente'
  else if Nome = 'NomeMes'  then Result := 'M�s'
  else if Nome = 'NF'       then Result := 'Nota fiscal'
  else if Nome = 'QtdeDocs' then Result := 'Quant. de documentos'
  else if Nome = 'MediaDoc' then Result := 'Valor m�dia/documento'
  else if Nome = 'Total'    then Result := 'Valor negociado'
  else if Nome = 'TTC'      then Result := 'Fator de compra'
  else if Nome = 'TVV'      then Result := 'Ad Valorem'
  else if Nome = 'TTC_J'    then Result := '% Taxa per�odo'
  else if Nome = 'Dias'     then Result := 'Prazo m�dio (dias)'
  else if Nome = 'TTC_T'    then Result := '% Taxa mensal m�dia'
  else if Nome = 'Lote'     then Result := 'N� do Border�'
  else Result := Nome;
end;

procedure TFmOperaCol.BitBtn1Click(Sender: TObject);
begin
  UMyMod.ConfigJanela10(Name, EdCliente, CBCliente, TPIni, TPFim,
    RGOrdem1, RGOrdem2, RGOrdem3, RGGrupos, RGSintetico, nil);
end;

procedure TFmOperaCol.frxOper2GetValue(const VarName: String;
  var Value: Variant);
const
  ITEM: array[0..2] of string = ('Cliente', 'Dia', 'M�s');
  ITEMs: array[0..2] of string = ('Clientes', 'Dias com transa��o', 'Meses');
var
  MeuItem: String;
begin
  if VarName = 'VARF_TITULO1' then Value := ITEM[RGOrdem1.ItemIndex]
  else if VarName = 'VARF_CONTA_GF1' then
  begin
    if FContaGF1 < 2 then MeuItem := ITEM[RGOrdem1.ItemIndex]
    else MeuItem := ITEMs[RGOrdem1.ItemIndex];
    Value := 'Total de '+Geral.FFT(FContaGF1, 0, siPositivo)+' '+MeuItem;
  end else if VarName = 'VARF_CLIENTE' then
  begin
    if CbCliente.KeyValue = NULL then Value := 'TODOS' else
    Value := CBCliente.Text;
  end else if VarName = 'VARF_PERIODO' then Value :=
    FormatDateTime(VAR_FORMATDATE3, TPIni.Date) + CO_ATE +
    FormatDateTime(VAR_FORMATDATE3, TPFim.Date);
  if VarName = 'VAR_TESTE' then
  begin
    Value := 1;
  end
  else if VarName = 'VFR_LA1NOME' then
  begin
    if RGSintetico.ItemIndex = 0 then
    begin
      case RGOrdem1.ItemIndex of
        0: Value := 'Cliente: '+QrLReANomeCli.Value;
        1: Value := 'Emitido no dia  '+Geral.FDT(QrLReAData.Value, 2);
        2: Value := 'Emitido no m�s de '+QrLReANomeMes.Value;
      end;
    end else begin
      case RGOrdem1.ItemIndex of
        0: Value := QrLReANomeCli.Value;
        1: Value := Geral.FDT(QrLReAData.Value, 2);
        2: Value := QrLReANomeMes.Value;
      end;
    end;
  end
  else if VarName = 'VFR_LA2NOME' then
  begin
    if RGSintetico.ItemIndex = 0 then
    begin
      case RGOrdem2.ItemIndex of
        0: Value := 'Cliente: '+QrLReANomeCli.Value;
        1: Value := 'Emitido no dia  '+Geral.FDT(QrLReAData.Value, 2);
        2: Value := 'Emitido no m�s de '+QrLReANomeMes.Value;
      end;
    end else begin
      case RGOrdem2.ItemIndex of
        0: Value := QrLReANomeCli.Value;
        1: Value := Geral.FDT(QrLReAData.Value, 2);
        2: Value := QrLReANomeMes.Value;
      end;
    end;
  end

  // user function


  else if VarName = 'VARF_CONTA_GF1' then
  begin
    {if Geral.IMV(frParser.Calc(p1)) = 0 then FContaGF1 := 0
    else FContaGF1 := FContaGF1 + 1;}
    Value := ' ';
  end;
  if VarName = 'VFR_ORD1' then
  begin
    if RGGrupos.ItemIndex < 1 then Value := 0 else
    Value := RGOrdem1.ItemIndex + 1;
  end else
  if VarName = 'VFR_ORD2' then
  begin
    if RGGrupos.ItemIndex < 2 then Value := 0 else
    Value := RGOrdem2.ItemIndex + 1;
  end else if VarName = 'VARF_VISIBLE' then
    Value := not Geral.IntToBool_0(RGSintetico.ItemIndex);
end;

function TFmOperaCol.frxOper2UserFunction(const MethodName: String;
  var Params: Variant): Variant;
begin
  if MethodName = Uppercase('CalculaJuroMes_frxReport') then
    Result := CalculaJuroMes_frxReport(Params[0], Params[1], Params[2])
end;

{
object QrOpera: TmySQLQuery
  Database = Dmod.MyDB
  OnCalcFields = QrOperaCalcFields
  SQL.Strings = (
    'SELECT lo.*, CASE WHEN en.Tipo=0 THEN en.RazaoSocial'
    'ELSE en.Nome END NOMECLIENTE'
    'FROM lot es lo'
    'LEFT JOIN entidades en ON en.Codigo=lo.Cliente'
    'WHERE lo.data BETWEEN :P0 AND :P1'
    'AND (TxCompra+ValValorem)>=0.01'
    'ORDER BY lo.Data, NOMECLIENTE, lo.Lote')
  Left = 468
  Top = 252
  ParamData = <
    item
      DataType = ftUnknown
      Name = 'P0'
      ParamType = ptUnknown
    end
    item
      DataType = ftUnknown
      Name = 'P1'
      ParamType = ptUnknown
    end>
  object QrOperaNOMECLIENTE: TWideStringField
    FieldName = 'NOMECLIENTE'
    Size = 100
  end
  object QrOperaCodigo: TIntegerField
    FieldName = 'Codigo'
    Required = True
  end
  object QrOperaTipo: TSmallintField
    FieldName = 'Tipo'
    Required = True
  end
  object QrOperaSpread: TSmallintField
    FieldName = 'Spread'
    Required = True
  end
  object QrOperaCliente: TIntegerField
    FieldName = 'Cliente'
    Required = True
  end
  object QrOperaLote: TSmallintField
    FieldName = 'Lote'
    Required = True
  end
  object QrOperaData: TDateField
    FieldName = 'Data'
    Required = True
  end
  object QrOperaTotal: TFloatField
    FieldName = 'Total'
    Required = True
  end
  object QrOperaDias: TFloatField
    FieldName = 'Dias'
    Required = True
  end
  object QrOperaPeCompra: TFloatField
    FieldName = 'PeCompra'
    Required = True
  end
  object QrOperaTxCompra: TFloatField
    FieldName = 'TxCompra'
    Required = True
  end
  object QrOperaValValorem: TFloatField
    FieldName = 'ValValorem'
    Required = True
  end
  object QrOperaAdValorem: TFloatField
    FieldName = 'AdValorem'
    Required = True
  end
  object QrOperaIOC: TFloatField
    FieldName = 'IOC'
    Required = True
  end
  object QrOperaCPMF: TFloatField
    FieldName = 'CPMF'
    Required = True
  end
  object QrOperaTipoAdV: TIntegerField
    FieldName = 'TipoAdV'
    Required = True
  end
  object QrOperaIRRF: TFloatField
    FieldName = 'IRRF'
    Required = True
  end
  object QrOperaIRRF_Val: TFloatField
    FieldName = 'IRRF_Val'
    Required = True
  end
  object QrOperaISS: TFloatField
    FieldName = 'ISS'
    Required = True
  end
  object QrOperaISS_Val: TFloatField
    FieldName = 'ISS_Val'
    Required = True
  end
  object QrOperaPIS_R: TFloatField
    FieldName = 'PIS_R'
    Required = True
  end
  object QrOperaPIS_R_Val: TFloatField
    FieldName = 'PIS_R_Val'
    Required = True
  end
  object QrOperaLk: TIntegerField
    FieldName = 'Lk'
  end
  object QrOperaDataCad: TDateField
    FieldName = 'DataCad'
  end
  object QrOperaDataAlt: TDateField
    FieldName = 'DataAlt'
  end
  object QrOperaUserCad: TIntegerField
    FieldName = 'UserCad'
  end
  object QrOperaUserAlt: TIntegerField
    FieldName = 'UserAlt'
  end
  object QrOperaTarifas: TFloatField
    FieldName = 'Tarifas'
    Required = True
  end
  object QrOperaOcorP: TFloatField
    FieldName = 'OcorP'
    Required = True
  end
  object QrOperaCOFINS_R: TFloatField
    FieldName = 'COFINS_R'
    Required = True
  end
  object QrOperaCOFINS_R_Val: TFloatField
    FieldName = 'COFINS_R_Val'
    Required = True
  end
  object QrOperaPIS: TFloatField
    FieldName = 'PIS'
    Required = True
  end
  object QrOperaPIS_Val: TFloatField
    FieldName = 'PIS_Val'
    Required = True
  end
  object QrOperaCOFINS: TFloatField
    FieldName = 'COFINS'
    Required = True
  end
  object QrOperaCOFINS_Val: TFloatField
    FieldName = 'COFINS_Val'
    Required = True
  end
  object QrOperaPIS_TOTAL: TFloatField
    FieldKind = fkCalculated
    FieldName = 'PIS_TOTAL'
    Calculated = True
  end
  object QrOperaCOFINS_TOTAL: TFloatField
    FieldKind = fkCalculated
    FieldName = 'COFINS_TOTAL'
    Calculated = True
  end
  object QrOperaIOC_VAL: TFloatField
    FieldName = 'IOC_VAL'
    Required = True
  end
  object QrOperaCPMF_VAL: TFloatField
    FieldName = 'CPMF_VAL'
    Required = True
  end
  object QrOperaMaxVencto: TDateField
    FieldName = 'MaxVencto'
    Required = True
  end
  object QrOperaCHDevPg: TFloatField
    FieldName = 'CHDevPg'
    Required = True
  end
  object QrOperaMINTC: TFloatField
    FieldName = 'MINTC'
    Required = True
  end
  object QrOperaMINAV: TFloatField
    FieldName = 'MINAV'
    Required = True
  end
  object QrOperaMINTC_AM: TFloatField
    FieldName = 'MINTC_AM'
    Required = True
  end
  object QrOperaPIS_T_Val: TFloatField
    FieldName = 'PIS_T_Val'
    Required = True
  end
  object QrOperaCOFINS_T_Val: TFloatField
    FieldName = 'COFINS_T_Val'
    Required = True
  end
  object QrOperaPgLiq: TFloatField
    FieldName = 'PgLiq'
    Required = True
  end
  object QrOperaDUDevPg: TFloatField
    FieldName = 'DUDevPg'
    Required = True
  end
  object QrOperaNF: TIntegerField
    FieldName = 'NF'
    Required = True
  end
  object QrOperaItens: TIntegerField
    FieldName = 'Itens'
    Required = True
  end
  object QrOperaCOL_VALVALOREM: TFloatField
    FieldKind = fkCalculated
    FieldName = 'COL_VALVALOREM'
    Calculated = True
  end
  object QrOperaCOL_TAXAVAL: TFloatField
    FieldKind = fkCalculated
    FieldName = 'COL_TAXAVAL'
    Calculated = True
  end
end
object DsOpera: TDataSource
  DataSet = QrOpera
  Left = 496
  Top = 252
end

///////////////////////////////////////////////////
    procedure QrOperaCalcFields(DataSet: TDataSet);
///////////////////////////////////////////////////

procedure TFmOperaCol.QrOperaCalcFields(DataSet: TDataSet);
var
  i, j: Integer;
  vv, tv: Double;
begin
  Screen.Cursor := crHourGlass;
  j := FmPrincipal.FMyDBs.MaxDBs -1;
  vv := 0;
  tv := 0;
  for i := 0 to j do
  begin
    if FmPrincipal.FMyDBs.Connected[i] = '1' then
    begin
      QrEmLot.Close;
      QrEmLot.Database := Dmod.FArrayMySQLBD[i];
      QrEmLot.Params[0].AsInteger := QrOperaCodigo.Value;
      UMyMod.AbreQuery(QrEmLot);
      vv := vv + QrEmLotValValorem.Value;
      tv := tv + QrEmLotTaxaVal.Value;
    end;
  end;
  QrOperaCOL_VALVALOREM.Value := vv;
  QrOperaCOL_TAXAVAL.Value := tv;
  Screen.Cursor := crDefault;
end;

}
end.

