unit ModuleAtencoes;

interface

uses
  Windows, Forms, SysUtils, Classes, DB, mySQLDbTables, Controls, ComCtrls,
  StdCtrls, ExtCtrls, dmkGeral, ABSMain, UnDmkProcFunc, DmkDAC_PF, UnDmkEnums;

type
  TDmAtencoes = class(TDataModule)
    QrErrRepa: TmySQLQuery;
    QrErrRepaData: TDateField;
    QrErrRepaTipo: TSmallintField;
    QrErrRepaControle: TIntegerField;
    QrErrRepaSub: TSmallintField;
    QrErrRepaDescricao: TWideStringField;
    QrErrRepaCredito: TFloatField;
    QrErrRepaCompensado: TDateField;
    QrErrRepaDocumento: TFloatField;
    QrErrRepaVencimento: TDateField;
    QrErrRepaID_Pgto: TIntegerField;
    QrErrRepaCliente: TIntegerField;
    QrErrRepaCliInt: TIntegerField;
    QrErrRepaDepto: TIntegerField;
    QrErrRepaMES: TWideStringField;
    QrErrRepaNOMECOND: TWideStringField;
    QrErrRepaNOMEPROP: TWideStringField;
    QrErrRepaCarteira: TIntegerField;
    QrErrRepaGenero: TIntegerField;
    QrErrRepaAutorizacao: TIntegerField;
    QrErrRepaQtde: TFloatField;
    QrErrRepaNotaFiscal: TIntegerField;
    QrErrRepaDebito: TFloatField;
    QrErrRepaSit: TIntegerField;
    QrErrRepaFatID: TIntegerField;
    QrErrRepaFatID_Sub: TIntegerField;
    QrErrRepaFatParcela: TIntegerField;
    QrErrRepaID_Sub: TSmallintField;
    QrErrRepaFatura: TWideStringField;
    QrErrRepaEmitente: TWideStringField;
    QrErrRepaBanco: TIntegerField;
    QrErrRepaContaCorrente: TWideStringField;
    QrErrRepaCNPJCPF: TWideStringField;
    QrErrRepaLocal: TIntegerField;
    QrErrRepaCartao: TIntegerField;
    QrErrRepaLinha: TIntegerField;
    QrErrRepaOperCount: TIntegerField;
    QrErrRepaLancto: TIntegerField;
    QrErrRepaPago: TFloatField;
    QrErrRepaMez: TIntegerField;
    QrErrRepaFornecedor: TIntegerField;
    QrErrRepaForneceI: TIntegerField;
    QrErrRepaMoraDia: TFloatField;
    QrErrRepaMulta: TFloatField;
    QrErrRepaProtesto: TDateField;
    QrErrRepaDataDoc: TDateField;
    QrErrRepaCtrlIni: TIntegerField;
    QrErrRepaNivel: TIntegerField;
    QrErrRepaVendedor: TIntegerField;
    QrErrRepaAccount: TIntegerField;
    QrErrRepaICMS_P: TFloatField;
    QrErrRepaICMS_V: TFloatField;
    QrErrRepaDuplicata: TWideStringField;
    QrErrRepaDescoPor: TIntegerField;
    QrErrRepaDescoVal: TFloatField;
    QrErrRepaDescoControle: TIntegerField;
    QrErrRepaNFVal: TFloatField;
    QrErrRepaAntigo: TWideStringField;
    QrErrRepaExcelGru: TIntegerField;
    QrErrRepaDataCad: TDateField;
    QrErrRepaDataAlt: TDateField;
    QrErrRepaUserCad: TIntegerField;
    QrErrRepaUserAlt: TIntegerField;
    QrErrRepaSerieCH: TWideStringField;
    QrErrRepaDoc2: TWideStringField;
    QrErrRepaMoraVal: TFloatField;
    QrErrRepaMultaVal: TFloatField;
    QrErrRepaCNAB_Sit: TSmallintField;
    QrErrRepaTipoCH: TSmallintField;
    QrErrRepaReparcel: TIntegerField;
    QrErrRepaID_Quit: TIntegerField;
    QrErrRepaAtrelado: TIntegerField;
    QrErrRepaPagMul: TFloatField;
    QrErrRepaPagJur: TFloatField;
    QrErrRepaUH: TWideStringField;
    QrErrRepaFatNum: TFloatField;
    DsErrRepa: TDataSource;
    DsErrRepaIts: TDataSource;
    QrErrRepaIts: TmySQLQuery;
    QrErrRepaItsData: TDateField;
    QrErrRepaItsDescricao: TWideStringField;
    QrErrRepaItsGenero: TIntegerField;
    QrErrRepaItsVencimento: TDateField;
    QrErrRepaItsCredito: TFloatField;
    QrErrRepaItsMez: TIntegerField;
    QrErrRepaItsFatID: TIntegerField;
    QrErrRepaItsCliInt: TIntegerField;
    QrErrRepaItsCliente: TIntegerField;
    QrErrRepaItsForneceI: TIntegerField;
    QrErrRepaItsMulta: TFloatField;
    QrErrRepaItsDepto: TIntegerField;
    QrErrRepaItsVlrOrigi: TFloatField;
    QrErrRepaItsVlrMulta: TFloatField;
    QrErrRepaItsVlrJuros: TFloatField;
    QrErrRepaItsVlrTotal: TFloatField;
    QrErrRepaItsMultaVal: TFloatField;
    QrErrRepaItsMoraVal: TFloatField;
    QrErrRepaItsVlrAjust: TFloatField;
    QrErrRepaItsCTRL_LAN: TIntegerField;
    QrErrRepaItsCTRL_REP: TIntegerField;
    QrErrRepaItsCONTA_REP: TAutoIncField;
    QrErrRepaItsFatNum: TFloatField;
    QrErrRepaOri: TmySQLQuery;
    QrErrRepaOriAtrelado: TIntegerField;
    QrErrRepaIt2: TmySQLQuery;
    QrErrRepaIt2Data: TDateField;
    QrErrRepaIt2Descricao: TWideStringField;
    QrErrRepaIt2Genero: TIntegerField;
    QrErrRepaIt2Vencimento: TDateField;
    QrErrRepaIt2Credito: TFloatField;
    QrErrRepaIt2Mez: TIntegerField;
    QrErrRepaIt2FatID: TIntegerField;
    QrErrRepaIt2CliInt: TIntegerField;
    QrErrRepaIt2Cliente: TIntegerField;
    QrErrRepaIt2ForneceI: TIntegerField;
    QrErrRepaIt2Multa: TFloatField;
    QrErrRepaIt2Depto: TIntegerField;
    QrErrRepaIt2MultaVal: TFloatField;
    QrErrRepaIt2MoraVal: TFloatField;
    QrErrRepaIt2VlrOrigi: TFloatField;
    QrErrRepaIt2VlrMulta: TFloatField;
    QrErrRepaIt2VlrJuros: TFloatField;
    QrErrRepaIt2VlrTotal: TFloatField;
    QrErrRepaIt2VlrAjust: TFloatField;
    QrErrRepaIt2CTRL_LAN: TIntegerField;
    QrErrRepaIt2CTRL_REP: TIntegerField;
    QrErrRepaIt2CONTA_REP: TIntegerField;
    QrErrRepaIt2FatNum: TFloatField;
    DsErrRepaSum: TDataSource;
    QrErrRepaSum: TmySQLQuery;
    QrErrRepaSumCredito: TFloatField;
    QrErrRepaSumMultaVal: TFloatField;
    QrErrRepaSumMoraVal: TFloatField;
    QrErrRepaSumVlrOrigi: TFloatField;
    QrErrRepaSumVlrMulta: TFloatField;
    QrErrRepaSumVlrJuros: TFloatField;
    QrErrRepaSumVlrTotal: TFloatField;
    QrErrRepaSumVlrAjust: TFloatField;
    QrIncorr: TmySQLQuery;
    QrIncorrControle: TIntegerField;
    QrIncorrData: TDateField;
    QrIncorrDescricao: TWideStringField;
    QrIncorrCredito: TFloatField;
    QrIncorrTipo: TSmallintField;
    QrIncorrCarteira: TIntegerField;
    QrIncorrSit: TIntegerField;
    QrIncorrVencimento: TDateField;
    QrIncorrCompensado: TDateField;
    QrBPI2: TmySQLQuery;
    QrBPI2CTRL_LAN: TIntegerField;
    QrBPI2SUB_LAN: TSmallintField;
    QrBPI2Descricao: TWideStringField;
    QrBPI2Genero: TIntegerField;
    QrBPI2VlrTotal: TFloatField;
    QrBPI2MultaVal: TFloatField;
    QrBPI2CTRL_REP: TIntegerField;
    QrBPI2Dias: TIntegerField;
    QrBPI2VlrOrigi: TFloatField;
    QrBPI2Fator: TFloatField;
    QrIncorr2_: TmySQLQuery;
    QrIncorr2_Controle: TIntegerField;
    QrIncorr2_Data: TDateField;
    QrIncorr2_Descricao: TWideStringField;
    QrIncorr2_Credito: TFloatField;
    QrIncorr2_Tipo: TSmallintField;
    QrIncorr2_Carteira: TIntegerField;
    QrIncorr2_Sit: TIntegerField;
    QrIncorr2_Vencimento: TDateField;
    QrIncorr2_Compensado: TDateField;
    Query: TABSQuery;
    QueryOrdem: TIntegerField;
    QueryOrigi: TFloatField;
    QueryDias: TFloatField;
    QueryFator: TFloatField;
    QueryValDif: TFloatField;
    QueryValItem: TFloatField;
    QueryAtivo: TIntegerField;
    QueryRestaFat: TFloatField;
    QueryRestaDif: TFloatField;
    DataSource1: TDataSource;
    DataSource2: TDataSource;
    QrSum: TABSQuery;
    QrSemMez: TmySQLQuery;
    QrSemMezData: TDateField;
    QrSemMezTipo: TSmallintField;
    QrSemMezCarteira: TIntegerField;
    QrSemMezAutorizacao: TIntegerField;
    QrSemMezGenero: TIntegerField;
    QrSemMezDescricao: TWideStringField;
    QrSemMezNotaFiscal: TIntegerField;
    QrSemMezDebito: TFloatField;
    QrSemMezCredito: TFloatField;
    QrSemMezCompensado: TDateField;
    QrSemMezDocumento: TFloatField;
    QrSemMezSit: TIntegerField;
    QrSemMezVencimento: TDateField;
    QrSemMezLk: TIntegerField;
    QrSemMezFatID: TIntegerField;
    QrSemMezFatParcela: TIntegerField;
    QrSemMezCONTA: TIntegerField;
    QrSemMezNOMECONTA: TWideStringField;
    QrSemMezNOMEEMPRESA: TWideStringField;
    QrSemMezNOMESUBGRUPO: TWideStringField;
    QrSemMezNOMEGRUPO: TWideStringField;
    QrSemMezNOMECONJUNTO: TWideStringField;
    QrSemMezNOMESIT: TWideStringField;
    QrSemMezAno: TFloatField;
    QrSemMezMENSAL: TWideStringField;
    QrSemMezMENSAL2: TWideStringField;
    QrSemMezBanco: TIntegerField;
    QrSemMezLocal: TIntegerField;
    QrSemMezFatura: TWideStringField;
    QrSemMezSub: TSmallintField;
    QrSemMezCartao: TIntegerField;
    QrSemMezLinha: TIntegerField;
    QrSemMezPago: TFloatField;
    QrSemMezSALDO: TFloatField;
    QrSemMezID_Sub: TSmallintField;
    QrSemMezMez: TIntegerField;
    QrSemMezFornecedor: TIntegerField;
    QrSemMezcliente: TIntegerField;
    QrSemMezMoraDia: TFloatField;
    QrSemMezNOMECLIENTE: TWideStringField;
    QrSemMezNOMEFORNECEDOR: TWideStringField;
    QrSemMezTIPOEM: TWideStringField;
    QrSemMezNOMERELACIONADO: TWideStringField;
    QrSemMezOperCount: TIntegerField;
    QrSemMezLancto: TIntegerField;
    QrSemMezMulta: TFloatField;
    QrSemMezATRASO: TFloatField;
    QrSemMezJUROS: TFloatField;
    QrSemMezDataDoc: TDateField;
    QrSemMezNivel: TIntegerField;
    QrSemMezVendedor: TIntegerField;
    QrSemMezAccount: TIntegerField;
    QrSemMezMes2: TLargeintField;
    QrSemMezProtesto: TDateField;
    QrSemMezDataCad: TDateField;
    QrSemMezDataAlt: TDateField;
    QrSemMezUserCad: TSmallintField;
    QrSemMezUserAlt: TSmallintField;
    QrSemMezControle: TIntegerField;
    QrSemMezID_Pgto: TIntegerField;
    QrSemMezCtrlIni: TIntegerField;
    QrSemMezFatID_Sub: TIntegerField;
    QrSemMezICMS_P: TFloatField;
    QrSemMezICMS_V: TFloatField;
    QrSemMezDuplicata: TWideStringField;
    QrSemMezCOMPENSADO_TXT: TWideStringField;
    QrSemMezCliInt: TIntegerField;
    QrSemMezDepto: TIntegerField;
    QrSemMezDescoPor: TIntegerField;
    QrSemMezPrazo: TSmallintField;
    QrSemMezForneceI: TIntegerField;
    QrSemMezQtde: TFloatField;
    QrSemMezEmitente: TWideStringField;
    QrSemMezContaCorrente: TWideStringField;
    QrSemMezCNPJCPF: TWideStringField;
    QrSemMezDescoVal: TFloatField;
    QrSemMezDescoControle: TIntegerField;
    QrSemMezUnidade: TIntegerField;
    QrSemMezNFVal: TFloatField;
    QrSemMezAntigo: TWideStringField;
    QrSemMezExcelGru: TIntegerField;
    QrSemMezSerieCH: TWideStringField;
    QrSemMezSERIE_CHEQUE: TWideStringField;
    QrSemMezUH: TWideStringField;
    QrSemMezDoc2: TWideStringField;
    QrSemMezNOMEFORNECEI: TWideStringField;
    QrSemMezMoraVal: TFloatField;
    QrSemMezMultaVal: TFloatField;
    QrSemMezCNAB_Sit: TSmallintField;
    QrSemMezBanco1: TIntegerField;
    QrSemMezAgencia1: TIntegerField;
    QrSemMezConta1: TWideStringField;
    QrSemMezTipoCH: TSmallintField;
    QrSemMezAlterWeb: TSmallintField;
    QrSemMezReparcel: TIntegerField;
    QrSemMezID_Quit: TIntegerField;
    QrSemMezAtrelado: TIntegerField;
    QrSemMezAtivo: TSmallintField;
    QrSemMezFatNum: TFloatField;
    QrSemMezProtocolo: TIntegerField;
    QrSemMezPagMul: TFloatField;
    QrSemMezPagJur: TFloatField;
    DsSemMez: TDataSource;
    QrErrRepaAgencia: TIntegerField;
    QrSemMezAgencia: TIntegerField;
    procedure QrErrRepaAfterScroll(DataSet: TDataSet);
    procedure QrErrRepaBeforeClose(DataSet: TDataSet);
    procedure QrErrRepaItsBeforeClose(DataSet: TDataSet);
  private
    { Private declarations }
    FOrdem: Integer;
    procedure ABSQueryCria();
    procedure ABSQueryIns(Origi, Dias, Fator, ValDif, ValItem, RestaFat,
              RestaDif: Double);
    procedure ABSQueryReabre();
    procedure ReopenErrRepaIts(Loc: Integer);
  public
    { Public declarations }
    procedure ReopenErrRepa();
    procedure ExecutaPesquisas();
    procedure AtualizaParcelaReparcNovo(Reparcel, BPPControle, ErrControle, BPPLancto:
              Integer; PBAtz1: TProgressBar; LaAtz1: TLabel; PnAtz1: TPanel;
              PageControl: TPageControl; ActivePageIndex: Integer;
              QrBPP_Pgt: TmySQLQuery; TabLctA: String);
  end;

var
  DmAtencoes: TDmAtencoes;

implementation

uses Principal, UnMyObjects, Module, MyDBCheck, Atencoes, ModuleCond,
  UnFinanceiro, UnMLAGeral, UMySQLModule, UnInternalConsts, ModuleFin,
  ModuleGeral;

{$R *.dfm}

procedure TDmAtencoes.ABSQueryCria();
begin
  Query.Close;
  Query.SQL.Clear;
  Query.SQL.Add('DROP TABLE Teste001; ');
  Query.SQL.Add('CREATE TABLE Teste001 (');
  Query.SQL.Add('  Ordem     Integer     ,');
  Query.SQL.Add('  Origi     float       ,');
  Query.SQL.Add('  Dias      float       ,');
  Query.SQL.Add('  Fator     float       ,');
  Query.SQL.Add('  ValDif    float       ,');
  Query.SQL.Add('  ValItem   float       ,');
  Query.SQL.Add('  RestaFat  float       ,');
  Query.SQL.Add('  RestaDif  float       ,');
  Query.SQL.Add('  Ativo     Integer      ');
  Query.SQL.Add(');');
  //
end;

procedure TDmAtencoes.ABSQueryIns(Origi, Dias, Fator, ValDif, ValItem, RestaFat,
RestaDif: Double);
begin
  FOrdem := FOrdem + 1;
  Query.SQL.Add('INSERT INTO teste001 (' +
  'Ordem,Origi,Dias,Fator,ValDif,RestaFat,RestaDif,ValItem) VALUES (' +
    dmkPF.FFP(FOrdem,  1) + ', ' +
    dmkPF.FFP(Origi,   2) + ', ' +
    dmkPF.FFP(Dias,    0) + ', ' +
    dmkPF.FFP(Fator,   6) + ', ' +
    dmkPF.FFP(ValDif,  2) + ', ' +
    dmkPF.FFP(ValItem, 2) + ', ' +
    dmkPF.FFP(RestaFat,6) + ', ' +
    dmkPF.FFP(RestaDif,2) +
  ');');
end;

procedure TDmAtencoes.ABSQueryReabre();
begin
  Query.Close;
  Query.SQL.Add('SELECT * FROM teste001 ORDER BY Ordem;');
  Query.Open;
  //
  QrSum.Close;
  QrSum.SQL.Clear;
  QrSum.SQL.Add('SELECT SUM(Origi) Origi, SUM(Dias) Dias, SUM(Fator) Fator, ');
  QrSum.SQL.Add('SUM(ValDif) ValDif, SUM(ValItem) ValItem FROM teste001;');
  QrSum.SQL.Add('');
  QrSum.Open;
end;

procedure TDmAtencoes.AtualizaParcelaReparcNovo(Reparcel, BPPControle,
  ErrControle, BPPLancto: Integer; PBAtz1: TProgressBar; LaAtz1: TLabel;
  PnAtz1: TPanel; PageControl: TPageControl; ActivePageIndex: Integer;
  QrBPP_Pgt: TmySQLQuery; TabLctA: String);
var
  TotalI, TotalO, ValItem, TotalF, Fator2,
  RestaFat, RestaDif, AtuFat, ValDif2: Double;
  SubPgto1, P: Integer;
  ValTxt, DataP_Txt: String;
  wTabLctA, TLA: String;
  //BaseDadosB: TmySQLDatabase;
begin
  if Geral.MB_Pergunta('ATEN��O! Este procedimento n�o poder� ser revertido!' +
    sLineBreak + 'Deseja continuar?') <> ID_YES then
    Exit;
  // 2011-10-10
  //QrIncorr.Close;
  if FmPrincipal.FDatabase1A <> nil then
  begin
    //BasedadosB := FmPrincipal.FDatabase1B;
    QrIncorr.Database := FmPrincipal.FDatabase1A;
  end else
  begin
    //BaseDadosB := FmPrincipal.FDatabase2B;
    QrIncorr.Database := FmPrincipal.FDatabase1B;
  end;
  //
  //QrIncorr.Close;
  //QrIncorr.Database := BasedadosB;
  //////////////////////////////////////////////////////////////////////////////
  ///  FIM 2011-10-10
  //
  //wTabLctA := TabLctA;
  wTabLctA := DmodG.ObtemLctA_Para_Web(TabLctA);
  //
  PBAtz1.Position := 0;
  //CtrlLoc := QrBPP_PgtControle.Value;

  if LaAtz1 <> nil then
  begin
    LaAtz1.Caption := 'Aguarde...   Atualizando....';
    LaAtz1.Update;
  end;
  Application.ProcessMessages;
  FOrdem := 0;
  //
  //SubPgto1 := QrbloqparcparControle.Value;
  SubPgto1 := BPPControle;
  if SubPgto1 = 0 then
  begin
    Geral.MensagemBox('Ratifica��o cancelada! "SubPgto1" n�o definido!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT SUM(lan.Credito-lan.Debito) Valor',
  'FROM ' + TabLctA + ' lan',
  'WHERE lan.SubPgto1=' + FormatFloat('0', SubPgto1),
  '']);
  (*
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT SUM(lan.Credito-lan.Debito) Valor');
  Dmod.QrAux.SQL.Add('FROM ' + TabLctA + ' lan');
  Dmod.QrAux.SQL.Add('WHERE lan.SubPgto1=:P0');
  Dmod.QrAux.Params[0].AsInteger := SubPgto1;
  Dmod.QrAux.Open;
  *)
  //
  P := Pos('.', TabLctA);
  TLA := Copy(TabLctA, P + 1);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrIncorr, Dmod.MyDB, [
  'SELECT Controle, Data, Descricao, Credito, Tipo,',
  'Carteira, Sit, Vencimento, Compensado',
  'FROM ' + TLA,
  'WHERE Genero=-10',
  'AND ID_Pgto=' + FormatFloat('0', BPPLancto),
  '']);
  if QrIncorr.RecordCount = 0 then
  begin
    Geral.MensagemBox('ID_Pgto ' + Geral.FF0(BPPLancto) + ' n�o localizado!', 'Aviso', MB_OK+MB_ICONWARNING); 
    Exit;
  end;
  {
  QrIncorr.Close;
  QrIncorr.SQL.Clear;
  QrIncorr.SQL.Add('SELECT Controle, Data, Descricao, Credito, Tipo,');
  QrIncorr.SQL.Add('Carteira, Sit, Vencimento, Compensado');
  QrIncorr.SQL.Add('FROM ' + TabLctA);
  QrIncorr.SQL.Add('WHERE Genero=-10');
  QrIncorr.SQL.Add('AND ID_Pgto=:P0');
  QrIncorr.SQL.Add('');
  QrIncorr.Params[00].AsInteger := BPPLancto;
  QrIncorr.Open;
  }
  //
  DataP_Txt := Geral.FDT(QrIncorrData.Value, 1);
  TotalI := QrIncorrCredito.Value;//Geral.DMV(ValTxt);
  if TotalI <= 0 then
  begin
    Geral.MensagemBox('Valor informado inv�lido: ' + ValTxt,
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  Screen.Cursor := crHourGlass;
  if LaAtz1 <> nil then
  begin
    LaAtz1.Caption := 'Quitando lan�amentos originais...';
    LaAtz1.Update;
  end;
  Application.ProcessMessages;
  if Reparcel <> 0 then
  begin
    UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
    'Compensado', 'Sit'], ['Reparcel'], [DataP_Txt, 3], [Reparcel], True, '',
    TabLctA);
  end;
  if LaAtz1 <> nil then
  begin
    LaAtz1.Caption := 'Excluindo Lan�amentos atuais...';
    LaAtz1.Update;
  end;
  Application.ProcessMessages;
  if PnAtz1 <> nil then
  begin
    PnAtz1.Visible := True;
    PnAtz1.Invalidate;
    PnAtz1.Update;
  end;
  Application.processMessages;
  if QrBPP_Pgt <> nil then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmodFin.QrLcts, DmodFin.QrLcts.DataBase, [
    'SELECT Data, Tipo, Carteira, Controle, Sub, FatID',
    'FROM ' + TabLctA,
    'WHERE SubPgto1 > 0 AND SubPgto1=' + FormatFloat('0', SubPgto1),
    '']);
(*
    DmodFin.QrLcts.Close;
    DmodFin.QrLcts.SQL.Clear;
    DmodFin.QrLcts.SQL.Add('SELECT Data, Tipo, Carteira, Controle, Sub, FatID');
    DmodFin.QrLcts.SQL.Add('FROM ' + TabLctA);
    DmodFin.QrLcts.SQL.Add('WHERE SubPgto1 > 0 AND SubPgto1=:P0');
    DmodFin.QrLcts.Params[0].AsFloat := SubPgto1;
    DmodFin.QrLcts.Open;
*)
    while not DmodFin.QrLcts.Eof do
    begin
      UFinanceiro.ExcluiLct_Unico(TabLctA, Dmod.MyDB, DmodFin.QrLctsData.Value,
        DmodFin.QrLctsTipo.Value, DmodFin.QrLctsCarteira.Value,
        DmodFin.QrLctsControle.Value, DmodFin.QrLctsSub.Value,
        dmkPF.MotivDel_ValidaCodigo(309), True);
      //
      DmodFin.QrLcts.Next;
    end;
    //
    //
    QrBPP_Pgt.First;
    while not QrBPP_Pgt.Eof do
    begin
      Dmod.MyDB.Execute('INSERT INTO livres SET Tabela="' + TabLctA + '", Codigo=' +
      FormatFloat('0', QrBPP_Pgt.FieldByName('Controle').AsInteger));
      QrBPP_Pgt.Next;
    end;
  end;
  if LaAtz1 <> nil then
  begin
    LaAtz1.Caption := 'Abrindo tabelas com os dados da parcela atual...';
    LaAtz1.Update;
  end;
  Application.ProcessMessages;
  //
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT SUM(VlrOrigi* Dias) Fator, ');
  Dmod.QrAux.SQL.Add('SUM(VlrOrigi) VlrOrigi, SUM(Dias) Dias');
  Dmod.QrAux.SQL.Add('FROM bloqparcits');
  Dmod.QrAux.SQL.Add('WHERE Controle=:P0');
  Dmod.QrAux.Params[0].AsInteger := BPPControle;
  Dmod.QrAux.Open;
  TotalF := Dmod.QrAux.FieldByName('Fator').AsFloat;
  TotalO := Dmod.QrAux.FieldByName('VlrOrigi').AsFloat;
  //TotalD := Trunc(Dmod.QrAux.FieldByName('Dias').AsFloat);
  //RestaD := TotalD;
  //
  //RestaDif := TotalI - TotalO;
  RestaFat := TotalF;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrBPI2, Dmod.MyDB, [
  'SELECT lan.Data, lan.Controle CTRL_LAN, lan.Descricao, lan.Genero,',
  'lan.Vencimento, lan.Credito, lan.Mez, lan.FatNum, lan.FatID,',
  'lan.CliInt, lan.Cliente, lan.ForneceI, lan.Multa, lan.Depto,',
  'lan.MultaVal, lan.MoraVal, lan.Sub SUB_LAN,',
  'bpi.VlrAjust, bpi.Controle CTRL_REP,',
  'bpi.VlrOrigi, bpi.VlrMulta, bpi.VlrJuros, bpi.VlrTotal,',
  'bpi.Conta CONTA_REP, bpi.Dias, bpi.Dias * bpi.VlrOrigi Fator',
  'FROM bloqparcits bpi',
  'LEFT JOIN ' + wTabLctA + ' lan ON lan.Controle=bpi.CtrlOrigi',
  'WHERE bpi.Controle=' + FormatFloat('0', BPPControle),
  'ORDER BY Fator',
  '']);
(*
  QrBPI2.Close;
  QrBPI2.SQL.Clear;
  QrBPI2.SQL.Add('SELECT lan.Data, lan.Controle CTRL_LAN, lan.Descricao, lan.Genero,');
  QrBPI2.SQL.Add('lan.Vencimento, lan.Credito, lan.Mez, lan.FatNum, lan.FatID,');
  QrBPI2.SQL.Add('lan.CliInt, lan.Cliente, lan.ForneceI, lan.Multa, lan.Depto,');
  QrBPI2.SQL.Add('lan.MultaVal, lan.MoraVal, lan.Sub SUB_LAN,');
  QrBPI2.SQL.Add('bpi.VlrAjust, bpi.Controle CTRL_REP,');
  QrBPI2.SQL.Add('bpi.VlrOrigi, bpi.VlrMulta, bpi.VlrJuros, bpi.VlrTotal,');
  QrBPI2.SQL.Add('bpi.Conta CONTA_REP, bpi.Dias, bpi.Dias * bpi.VlrOrigi Fator');
  QrBPI2.SQL.Add('FROM bloqparcits bpi');
  QrBPI2.SQL.Add('LEFT JOIN ' + wTabLctA + ' lan ON lan.Controle=bpi.CtrlOrigi');
  QrBPI2.SQL.Add('WHERE bpi.Controle=:P0');
  QrBPI2.SQL.Add('ORDER BY Fator');
  QrBPI2.SQL.Add('');
  QrBPI2.Params[0].AsInteger := BPPControle;
  QrBPI2.Open;
*)
  PBAtz1.Max := QrBPI2.RecordCount;
  //
  if LaAtz1 <> nil then
  begin
    LaAtz1.Caption := 'Criando lan�amentos novos ...';
    LaAtz1.Update;
  end;
  Application.ProcessMessages;
  //
  RestaDif := TotalI - TotalO;
  ABSQueryCria;
  QrBPI2.First;
  while not QrBPI2.Eof do
  begin
    PBAtz1.Position := PBAtz1.Position + 1;
    PBAtz1.Update;
    Application.ProcessMessages;
    //
    //ValDif2 := 0;
    AtuFat   := QrBPI2Dias.Value * QrBPI2VlrOrigi.Value;
    if RestaFat = 0 then Fator2 := 0 else
      Fator2 := AtuFat / RestaFat;
    ValDif2  := Round(Fator2 * RestaDif * 100) / 100;
    //
    RestaFat := RestaFat - AtuFat;
    RestaDif := RestaDif - ValDif2;
    //
    ValItem := QrBPI2VlrOrigi.Value + ValDif2;
    ABSQueryIns(QrBPI2VlrOrigi.Value, QrBPI2Dias.Value, ValItem,
                RestaFat, RestaDif, Fator2, ValDif2);
    UFinanceiro.LancamentoDuplica(Dmod.QrAux, QrBPI2CTRL_LAN.Value, QrBPI2Sub_LAN.Value, TabLctA);
{
    QrIncorr2 - Onde abre ???
    Resposta: Lugar nenhum! 2011-08-17
    SQL:
SELECT Controle, Data, Descricao, Credito, Tipo,
Carteira, Sit, Vencimento, Compensado
FROM lct
WHERE Genero=-10
AND ID_Pgto=:P0
}

    FLAN_Tipo        := QrIncorrTipo.Value;
    FLAN_Carteira    := QrIncorrCarteira.Value;
    FLAN_Sit         := QrIncorrSit.Value;
    FLAN_Data        := DataP_Txt;
    FLAN_Vencimento  := Geral.FDT(QrIncorrVencimento.Value, 1);
    FLAN_Compensado  := Geral.FDT(QrIncorrCompensado.Value, 1);

    FLAN_Genero      := QrBPI2Genero.Value;
    FLAN_Descricao   := '[Reparc] ' + QrBPI2Descricao.Value;
    FLAN_Credito     := ValItem;
    FLAN_MultaVal    := QrBPI2MultaVal.Value;
    FLAN_MoraVal     := ValDif2 - QrBPI2MultaVal.Value;
    FLAN_SubPgto1    := QrBPI2CTRL_REP.Value;
    //
    FLAN_Controle   := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
      TabLctA, LAN_CTOS, 'Controle');
    if UFinanceiro.InsereLancamento(TabLctA) then ;
    //
    QrBPI2.Next;
  end;
  ABSQueryReabre;
  //ReopenBPP_Pgt(CtrlLoc);
  if PageControl <> nil then
    PageControl.ActivePageIndex := ActivePageIndex;
  //
  if LaAtz1 <> nil then
  begin
    LaAtz1.Caption := 'Ratifica��o finalizada!!!';
    LaAtz1.Update;
  end;
  PBAtz1.Position := 0;
  //
  UMyMod.SQLDel1(Dmod.QrUpd, nil, TabLctA, 'Controle', ErrControle, False, '', False);
  Application.ProcessMessages;
  //
  Screen.Cursor := crDefault;
end;

procedure TDmAtencoes.ExecutaPesquisas();
var
  PageIndex: Integer;
  MyCursor: TCursor;
begin
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    //
    MyObjects.Informa2(FmPrincipal.LaAviso3, FmPrincipal.LaAviso2, True, 'Verificando reparcelamentos');
    PageIndex := -1;
    ReopenErrRepa();
    if QrErrRepa.RecordCount > 0 then
      PageIndex := 0;

    //

    MyObjects.Informa2(FmPrincipal.LaAviso3, FmPrincipal.LaAviso2, True, 'Verificando compet�ncia de lan�amentos');
    QrSemMez.Close;
    QrSemMez.SQL.Clear;
    QrSemMez.SQL.Add('SELECT MOD(la.Mez, 100) Mes2,');
    QrSemMez.SQL.Add('((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano,');
    QrSemMez.SQL.Add('ci.Unidade UH, la.*, ct.Codigo CONTA, ca.Prazo,');
    QrSemMez.SQL.Add('ca.Banco1, ca.Agencia1, ca.Conta1, ');
    QrSemMez.SQL.Add('ct.Nome NOMECONTA, sg.Nome NOMESUBGRUPO,');
    QrSemMez.SQL.Add('gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,');
    QrSemMez.SQL.Add('IF(em.Tipo=0, em.RazaoSocial, em.Nome) NOMEEMPRESA,');
    QrSemMez.SQL.Add('IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMECLIENTE,');
    QrSemMez.SQL.Add('IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome) NOMEFORNECEDOR, ');
    QrSemMez.SQL.Add('IF(la.ForneceI=0, "", IF(fi.Tipo=0, fi.RazaoSocial, fi.Nome)) NOMEFORNECEI,');
    QrSemMez.SQL.Add('IF(la.Sit<2, la.Credito-la.Debito-(la.Pago*la.Sit), 0) SALDO,');
    QrSemMez.SQL.Add('IF(la.Cliente>0, ');
    QrSemMez.SQL.Add('  IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome),');
    QrSemMez.SQL.Add('  IF (la.Fornecedor>0,');
    QrSemMez.SQL.Add('    IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome), "")) NOMERELACIONADO');
    QrSemMez.SQL.Add('FROM ' + DmCond.FTabLctA + ' la');
    QrSemMez.SQL.Add('LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira');
    QrSemMez.SQL.Add('LEFT JOIN contas ct    ON ct.Codigo=la.Genero AND ct.Terceiro = 0');
    QrSemMez.SQL.Add('LEFT JOIN subgrupos sg ON sg.Codigo=ct.Subgrupo');
    QrSemMez.SQL.Add('LEFT JOIN grupos gr    ON gr.Codigo=sg.Grupo');
    QrSemMez.SQL.Add('LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto');
    QrSemMez.SQL.Add('LEFT JOIN entidades em ON em.Codigo=ct.Empresa');
    QrSemMez.SQL.Add('LEFT JOIN entidades cl ON cl.Codigo=la.Cliente');
    QrSemMez.SQL.Add('LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor');
    QrSemMez.SQL.Add('LEFT JOIN entidades fi ON fi.Codigo=la.ForneceI');
    QrSemMez.SQL.Add('LEFT JOIN condimov  ci ON ci.Conta=la.Depto');
    QrSemMez.SQL.Add('');
    QrSemMez.SQL.Add('WHERE la.FatID > 0');
    QrSemMez.SQL.Add('AND la.Mez=0');
    QrSemMez.SQL.Add('AND la.Genero > 0');
    QrSemMez.SQL.Add('');
    QrSemMez.SQL.Add('ORDER BY la.Data, la.Controle');
    QrSemMez.SQL.Add('');
    QrSemMez.Open;
    if QrSemMez.RecordCount > 0 then
    begin
      if PageIndex = -1 then
        PageIndex := 1;
    end;

    //

    if PageIndex > -1 then
    begin
      if DBCheck.CriaFm(TFmAtencoes, FmAtencoes, afmoLiberado) then
      begin
        FmAtencoes.FTabLctA := DmCond.FTabLctA;
        FmAtencoes.PageControl1.ActivePageIndex := PageIndex;
        FmAtencoes.ShowModal;
        FmAtencoes.Destroy;
      end;
    end;
    //
    MyObjects.Informa2(FmPrincipal.LaAviso3, FmPrincipal.LaAviso2, False, '');
  finally
    Screen.Cursor := MyCursor;
  end;
end;

procedure TDmAtencoes.QrErrRepaAfterScroll(DataSet: TDataSet);
begin
  ReopenErrRepaIts(0);
end;

procedure TDmAtencoes.QrErrRepaBeforeClose(DataSet: TDataSet);
begin
  QrErrRepaIts.Close;
end;

procedure TDmAtencoes.QrErrRepaItsBeforeClose(DataSet: TDataSet);
begin
  QrErrRepaSum.Close;
end;

procedure TDmAtencoes.ReopenErrRepa();
begin
  QrErrRepa.Close;
  QrErrRepa.SQL.Clear;
  QrErrRepa.SQL.Add('SELECT lct.*, CONCAT(RIGHT(lct.Mez, 2), "/",');
  QrErrRepa.SQL.Add('LEFT(lct.Mez + 200000, 4)) MES,');
  QrErrRepa.SQL.Add('IF(cond.Tipo=0, cond.RazaoSocial, cond.Nome) NOMECOND,');
  QrErrRepa.SQL.Add('IF(prop.Tipo=0, prop.RazaoSocial, prop.Nome) NOMEPROP,');
  QrErrRepa.SQL.Add('imov.Unidade UH');
  QrErrRepa.SQL.Add('FROM ' + DmCond.FTabLctA + ' lct, entidades cond, entidades prop, condimov  imov');
  QrErrRepa.SQL.Add('WHERE cond.Codigo=lct.CliInt');
  QrErrRepa.SQL.Add('AND prop.Codigo=lct.Cliente');
  QrErrRepa.SQL.Add('AND imov.Conta=lct.Depto');
  QrErrRepa.SQL.Add('AND lct.Genero=-10');
  QrErrRepa.SQL.Add('AND lct.Tipo <> 2');
  QrErrRepa.SQL.Add('ORDER BY NOMECOND, NOMEPROP, Data');
  QrErrRepa.Open;
end;

procedure TDmAtencoes.ReopenErrRepaIts(Loc: Integer);
var
  MyCursor: TCursor;
begin
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  //
  MyObjects.Informa2(FmPrincipal.LaAviso3, FmPrincipal.LaAviso2, True, 'Verificando reparcelamentos. Lan�amentos originais');
  QrErrRepaOri.Close;
  QrErrRepaOri.SQL.Clear;
  QrErrRepaOri.SQL.Add('SELECT Atrelado');
  QrErrRepaOri.SQL.Add('FROM ' + DmCond.FTabLctA);
  QrErrRepaOri.SQL.Add('WHERE Controle=:P0');
  QrErrRepaOri.SQL.Add('AND Sub=:P1');
  QrErrRepaOri.SQL.Add('');
  QrErrRepaOri.Params[00].AsInteger := QrErrRepaID_Pgto.Value;
  QrErrRepaOri.Params[01].AsInteger := 0;
  QrErrRepaOri.Open;
  //
  MyObjects.Informa2(FmPrincipal.LaAviso3, FmPrincipal.LaAviso2, True, 'Verificando reparcelamentos. Itens do reparcelamento');
  QrErrRepaIts.Close;
  QrErrRepaIts.SQL.Clear;
  QrErrRepaIts.SQL.Add('SELECT lct.Data, lct.Controle CTRL_LAN, lct.Descricao, lct.Genero,');
  QrErrRepaIts.SQL.Add('lct.Vencimento, lct.Credito, lct.Mez, lct.FatNum, lct.FatID,');
  QrErrRepaIts.SQL.Add('lct.CliInt, lct.Cliente, lct.ForneceI, lct.Multa, lct.Depto,');
  QrErrRepaIts.SQL.Add('lct.MultaVal, lct.MoraVal, bpi.VlrAjust, bpi.Controle CTRL_REP,');
  QrErrRepaIts.SQL.Add('bpi.VlrOrigi, bpi.VlrMulta, bpi.VlrJuros, bpi.VlrTotal,');
  QrErrRepaIts.SQL.Add('bpi.Conta CONTA_REP');
  QrErrRepaIts.SQL.Add('FROM ' + DmCond.FTabLctA + ' lct, bloqparcits bpi');
  QrErrRepaIts.SQL.Add('WHERE lct.Controle=bpi.CtrlOrigi');
  QrErrRepaIts.SQL.Add('AND bpi.Controle=:P0');
  QrErrRepaIts.SQL.Add('');
  QrErrRepaIts.Params[0].AsInteger := QrErrRepaOriAtrelado.Value;
  QrErrRepaIts.Open;
  //
  MyObjects.Informa2(FmPrincipal.LaAviso3, FmPrincipal.LaAviso2, True, 'Verificando reparcelamentos. Valor Total');
  QrErrRepaSum.Close;
  QrErrRepaSum.SQL.Clear;
  QrErrRepaSum.SQL.Add('SELECT SUM(lct.Credito) Credito, SUM(lct.MultaVal) MultaVal,');
  QrErrRepaSum.SQL.Add('SUM(lct.MoraVal) MoraVal, SUM(bpi.VlrOrigi) VlrOrigi, ');
  QrErrRepaSum.SQL.Add('SUM(bpi.VlrMulta) VlrMulta, SUM(bpi.VlrJuros) VlrJuros, ');
  QrErrRepaSum.SQL.Add('SUM(bpi.VlrTotal) VlrTotal, SUM(bpi.VlrAjust) VlrAjust');
  QrErrRepaSum.SQL.Add('FROM bloqparcits bpi, ' + DmCond.FTabLctA + ' lct ');
  QrErrRepaSum.SQL.Add('WHERE lct.Controle=bpi.CtrlOrigi');
  QrErrRepaSum.SQL.Add('AND bpi.Controle=:P0');
  QrErrRepaSum.SQL.Add('');
  QrErrRepaSum.Params := QrErrRepaIts.Params;
  QrErrRepaSum.Open;
  //
  MyObjects.Informa2(FmPrincipal.LaAviso3, FmPrincipal.LaAviso2, False, '');
  Screen.Cursor := MyCursor;
end;

end.
