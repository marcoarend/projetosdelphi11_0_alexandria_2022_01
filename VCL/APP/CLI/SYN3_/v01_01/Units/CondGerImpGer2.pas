unit CondGerImpGer2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, frxClass, frxDBSet, Grids, StdCtrls, Buttons, ExtCtrls, DB, frxCross,
  mySQLDbTables, dmkEdit, DBGrids, Menus, dmkGeral, printers, frxRich,
  frxChBox, UnDmkProcFunc, UnDmkEnums;

type
  TFmCondGerImpGer2 = class(TForm)
    PainelTitulo: TPanel;
    Image1: TImage;
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    GradeA: TStringGrid;
    frxGrade: TfrxReport;
    frxCrossObject1: TfrxCrossObject;
    BtTitulo: TBitBtn;
    //Query_: TABSQuery;
    GradeB: TStringGrid;
    BitBtn2: TBitBtn;
    frxGrade2: TfrxReport;
    CkDesign: TCheckBox;
    frxDsQuery: TfrxDBDataset;
    PMGradeA: TPopupMenu;
    Alterasiglanocadastrodacontaplanodecontas1: TMenuItem;
    Alterasiglanocadastrodaarrecadaobase1: TMenuItem;
    Alteratextodottuloselecionado1: TMenuItem;
    Larguradacolunaatual1: TMenuItem;
    AlterasiglanocadastrodaLeitura1: TMenuItem;
    Panel3: TPanel;
    Label3: TLabel;
    EdLarguraFolha: TdmkEdit;
    Label4: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    EdLinhaAlt: TdmkEdit;
    EdTamFonte: TdmkEdit;
    CkRecalcN: TCheckBox;
    CkRecalcT: TCheckBox;
    CkLargTit: TCheckBox;
    dmkEdit5: TdmkEdit;
    dmkEdit6: TdmkEdit;
    QrPgBloq: TmySQLQuery;
    QrPgBloqApto: TIntegerField;
    QrPgBloqFatNum: TFloatField;
    QrPgBloqData: TDateField;
    QrPgBloqDATA_TXT: TWideStringField;
    QrAptos: TmySQLQuery;
    QrAptosAndar: TIntegerField;
    QrAptosUnidade: TWideStringField;
    QrAptosNOMEPROP: TWideStringField;
    QrAptosNOMEUSUARIO: TWideStringField;
    QrAptosSigla: TWideStringField;
    QrAptosConta: TIntegerField;
    QrAptosDATAPG_TXT: TWideStringField;
    QrBol3: TmySQLQuery;
    QrBol3Ordem: TIntegerField;
    QrBol3FracaoIdeal: TFloatField;
    QrBol3Andar: TIntegerField;
    QrBol3Boleto: TFloatField;
    QrBol3Apto: TIntegerField;
    QrBol3Propriet: TIntegerField;
    QrBol3Vencto: TDateField;
    QrBol3Unidade: TWideStringField;
    QrBol3NOMEPROPRIET: TWideStringField;
    QrBol3BOLAPTO: TWideStringField;
    QrBol3KGT: TLargeintField;
    Query: TmySQLQuery;
    procedure BtSaidaClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure frxGradeBeforePrint(Sender: TfrxReportComponent);
    procedure FormCreate(Sender: TObject);
    procedure frxGradeGetValue(const VarName: string; var Value: Variant);
    function frxGradeUserFunction(const MethodName: string;
      var Params: Variant): Variant;
    procedure BtTituloClick(Sender: TObject);
    procedure EdLinhaAltChange(Sender: TObject);
    procedure GradeADrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure BitBtn2Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure PMGradeAPopup(Sender: TObject);
    procedure EdTamFonteChange(Sender: TObject);
    procedure Alterasiglanocadastrodacontaplanodecontas1Click(Sender: TObject);
    procedure GradeASelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure GradeAMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure EdLarguraFolhaChange(Sender: TObject);
    procedure Larguradacolunaatual1Click(Sender: TObject);
    procedure Alterasiglanocadastrodaarrecadaobase1Click(Sender: TObject);
    procedure Alteratextodottuloselecionado1Click(Sender: TObject);
    procedure AlterasiglanocadastrodaLeitura1Click(Sender: TObject);
    procedure EdTamFonteExit(Sender: TObject);
    procedure GradeAMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure CkLargTitClick(Sender: TObject);
    procedure CkRecalcNClick(Sender: TObject);
    procedure CkRecalcTClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    FColClicked, FRowClicked, FXClicked, FYClicked, FCodCad, FTipoGrade: Integer;
    FTitulo: String;
    FSigla: String;
    //FTextoCopia: String;
    procedure RecalculaLarguraColunaTextos;
    procedure RecalculaAlturaLinhaTitulos;

    function ObtemFrxDataField(const Coluna: Integer;
             var Campo, Somas: String; var HAlinha: TfrxHAlign;
             var Somar: Boolean): Boolean;
    function ObtemSigla(EhLeitura: Boolean): Boolean;
    procedure DefineTamanhoTexto(const Texto: String; var Tam: Integer);
    procedure MostraPMTitulo(Como: Integer);
    procedure ReacertaFrxDataSets();

   public
    { Public declarations }
    FImprimePage1, FImprimePage2: Boolean;
    procedure RelatorioDeArrecadacoes_ListaDeCondominos(Mostra: Boolean);
    procedure ImprimeGrade2();
    procedure CalculaLarguraFolha();
    function Define_frxCond(frx: TfrxReport): TfrxReport;
  end;

var
  FmCondGerImpGer2: TFmCondGerImpGer2;

const
  FColIniVal = 6;
  FDotImp    = 0.0377953;

implementation

uses Module, ModuleCond,
//CondGer,
UnMLAGeral, UMySQLModule, UnInternalConsts,
MyDBCheck, CondGerImpGerLar, UnBancos, MyGlyfs, ModuleGeral, UnAuxCondGer,
Principal, UnMyObjects, ModuleBloq, ModuleLct2;

{$R *.dfm}

procedure TFmCondGerImpGer2.Alterasiglanocadastrodaarrecadaobase1Click(
  Sender: TObject);
begin
  if ObtemSigla(False) then
  begin
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'arrebac', False,
      ['Sigla'], ['Codigo'], [FSigla], [FCodCad], True);
    if CkRecalcN.Checked then
      RelatorioDeArrecadacoes_ListaDeCondominos(False)
    else
      GradeA.Cells[GradeA.Col, 0] := FSigla;
  end;
end;

procedure TFmCondGerImpGer2.Alterasiglanocadastrodacontaplanodecontas1Click(
  Sender: TObject);
begin
  if ObtemSigla(False) then
  begin
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'contas', False,
      ['Sigla'], ['Codigo'], [FSigla], [FCodCad], True);
    if CkRecalcN.Checked then
      RelatorioDeArrecadacoes_ListaDeCondominos(False)
    else  
      GradeA.Cells[GradeA.Col, 0] := FSigla;
  end;
end;

procedure TFmCondGerImpGer2.AlterasiglanocadastrodaLeitura1Click(
  Sender: TObject);
var
  i, t, c: Integer;
begin
  if ObtemSigla(True) then
  begin
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'cons', False,
      ['Sigla'], ['Codigo'], [FSigla], [FCodCad], True);
    if CkRecalcN.Checked then
      RelatorioDeArrecadacoes_ListaDeCondominos(False)
    else begin
      DmBloq.QrListaL.Close;
      DmBloq.QrListaL.Open;
      for i := FColIniVal to GradeA.ColCount - 1 do
      begin
        t := Geral.IMV(GradeB.Cells[i,0]);
        //
        if t in ([1,2]) then
        begin
          c := Geral.IMV(GradeB.Cells[i,1]);
          if DmBloq.QrListaL.Locate('Codigo', c, []) then
            GradeA.Cells[i,0] := DmBloq.QrListaLUnidLei.Value + ' ' + DmBloq.QrListaLSIGLA_IMP.Value;
        end;
      end;
    end;
  end;
end;

procedure TFmCondGerImpGer2.Alteratextodottuloselecionado1Click(Sender: TObject);
begin
  if ObtemSigla(False) then
  begin
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'contas', False,
      ['Sigla'], ['Codigo'], [FSigla], [FCodCad], True);
    if CkRecalcN.Checked then
      RelatorioDeArrecadacoes_ListaDeCondominos(False)
    else
      GradeA.Cells[GradeA.Col, 0] := FSigla;
  end;
end;

procedure TFmCondGerImpGer2.BtTituloClick(Sender: TObject);
begin
  MostraPMTitulo(1);
end;

procedure TFmCondGerImpGer2.MostraPMTitulo(Como: Integer);
begin
  if (FColClicked > 0) and (FColClicked < GradeA.ColCount) then
  begin
    FSigla := GradeA.Cells[FColClicked, 0];
    case Como of
      1: MyObjects.MostraPopUpDeBotao(PMGradeA, BtTitulo);
      //2: MLAGeral.MostraPopUpDeBotaoObject(PMGradeA, GradeA, FXClicked, FYClicked);
      2: PMGradeA.Popup(FXClicked + GradeA.Left + Panel1.Left + Left,
      FYClicked + GradeA.Top + Panel1.Top + Top);
    end;
  end else Geral.MensagemBox('Selecione uma coluna v�lida!',
  'Aviso', MB_OK+MB_ICONWARNING);
end;

procedure TFmCondGerImpGer2.CalculaLarguraFolha;
var
  i, n: Integer;
begin
  n := 0;
  for i := 1 to GradeA.ColCount - 1 do
  n := n + GradeA.ColWidths[i];
  EdLarguraFolha.ValueVariant := n;
end;

procedure TFmCondGerImpGer2.CkLargTitClick(Sender: TObject);
begin
  RelatorioDeArrecadacoes_ListaDeCondominos(False);
end;

procedure TFmCondGerImpGer2.CkRecalcNClick(Sender: TObject);
begin
  RelatorioDeArrecadacoes_ListaDeCondominos(False);
end;

procedure TFmCondGerImpGer2.CkRecalcTClick(Sender: TObject);
begin
  RelatorioDeArrecadacoes_ListaDeCondominos(False);
end;

procedure TFmCondGerImpGer2.DefineTamanhoTexto(const Texto: String;
  var Tam: Integer);
var
  k: Integer;
begin
  k := Canvas.TextWidth(Texto) + 8;
  if k > Tam then
    Tam := k;
end;

procedure TFmCondGerImpGer2.BitBtn2Click(Sender: TObject);
begin
  FTitulo := 'Lista de Arrecada��es';
  ReacertaFrxDataSets();
  MyObjects.frxMostra(frxGrade, 'Lista de Arrecada��es por unidade');
end;

{
procedure TFmCondGerImpGer2.BtOKClick(Sender: TObject);
var
  Lin, Dad: String;
  i, l, c: Integer;
  j: Integer;
begin
  // Parei aqui
  //incluir data na sql!!
  Screen.Cursor := crHourGlass;
  try
    DmBloq.ReopenListaA;
    Query.Close;
    Query.SQL.Clear;
    Query.SQL.Add('DROP TABLE StringGrid1; ');
    Query.SQL.Add('CREATE TABLE StringGrid1 (');
    Query.SQL.Add('  imvConta    integer      ,');
    Query.SQL.Add('  Andar       integer      ,');
    Query.SQL.Add('  UH          varchar(10)  ,');
    Query.SQL.Add('  Propriet    varchar(100) ,');
    Query.SQL.Add('  Quitado     varchar(10)  ,');
    //Query.SQL.Add('  Status      char(1)      ,');
    Query.SQL.Add('  Observacao  varchar(255) ,');
    for i := FColIniVal to GradeA.ColCount - 1 do
      Query.SQL.Add('  ValCol' +
        MLAGeral.FTX(i - FColIniVal+1, 3, 0, siPositivo) + ' float ,');
    Query.SQL.Add('  Ativo       smallint      ');
    Query.SQL.Add(');');
    //
    //Lin := 'INSERT INTO stringgrid1 (imvConta,Andar,UH,Propriet,Status,Observacao,';
    Lin := 'INSERT INTO stringgrid1 (imvConta,Andar,UH,Propriet,Quitado,Observacao,';
    for i := FColIniVal to GradeA.ColCount - 1 do
      Lin := Lin + 'ValCol' + MLAGeral.FTX(i - FColIniVal+1, 3, 0, siPositivo) + ',';

    Lin := Lin + 'Ativo) Values (';
    //
    for l := 1 to GradeA.RowCount - 2 do
    begin
      Dad := '';
      for c := 0 to GradeA.ColCount - 1 do
      begin
        for j := 0 to GradeA.ColCount - 1 do
        begin
          if Geral.IMV(GradeA.Cells[j, GradeA.RowCount - 1]) = c then
          begin
            Dad := Dad + '"' + GradeA.Cells[j,l] + '",';
            Break;
          end;
        end;
      end;
      Query.SQL.Add(Lin + Dad + '0);');
    end;
    //
    Query.SQL.Add('SELECT * FROM stringgrid1;');
    UMyMod.AbreABSQuery(Query);
    //Query.Open;
    //
    ImprimeGrade2();
    Screen.Cursor := crDefault;
  finally
    Screen.Cursor := crDefault;
  end;
end;
}

procedure TFmCondGerImpGer2.BtOKClick(Sender: TObject);
var
  Lin, Dad, Val: String;
  i, l, c: Integer;
  j: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    DmBloq.ReopenListaA;
    Query.Database := DModG.MyPID_DB;
    //
    Query.Close;
    Query.SQL.Clear;
    Query.SQL.Add('DROP TABLE IF EXISTS StringGrid1; ');
    Query.SQL.Add('CREATE TABLE StringGrid1 (');
    Query.SQL.Add('  imvConta    int(11)      ,');
    Query.SQL.Add('  Andar       int(11)      ,');
    Query.SQL.Add('  UH          varchar(10)  ,');
    Query.SQL.Add('  Propriet    varchar(100) ,');
    Query.SQL.Add('  Quitado     varchar(10)  ,');
    //Query.SQL.Add('  Status      char(1)      ,');
    Query.SQL.Add('  Observacao  varchar(255) ,');
    for i := FColIniVal to GradeA.ColCount - 1 do
      Query.SQL.Add('  ValCol' +
        MLAGeral.FTX(i - FColIniVal+1, 3, 0, siPositivo) + ' float ,');
    Query.SQL.Add('  Ativo       smallint      ');
    Query.SQL.Add(');');
    //
    //Lin := 'INSERT INTO stringgrid1 (imvConta,Andar,UH,Propriet,Status,Observacao,';
    Lin := 'INSERT INTO stringgrid1 (imvConta,Andar,UH,Propriet,Quitado,Observacao,';
    for i := FColIniVal to GradeA.ColCount - 1 do
      Lin := Lin + 'ValCol' + MLAGeral.FTX(i - FColIniVal+1, 3, 0, siPositivo) + ',';

    Lin := Lin + 'Ativo) Values (';
    //
    for l := 1 to GradeA.RowCount - 2 do
    begin
      Dad := '';
      for c := 0 to GradeA.ColCount - 1 do
      begin
        for j := 0 to GradeA.ColCount - 1 do
        begin
          if Geral.IMV(GradeA.Cells[j, GradeA.RowCount - 1]) = c then
          begin
            Val := GradeA.Cells[j,l];
            if MLAGeral.EhIntOuFloat(Val) then
              Val := MLAGeral.TTS(Val);
            Dad := Dad + '"' + Val + '",';
            Break;
          end;
        end;
      end;
      Query.SQL.Add(Lin + Dad + '0);');
    end;
    //
    Query.SQL.Add('SELECT * FROM stringgrid1;');
    UMyMod.AbreQuery(Query, DModG.MyPID_DB, 'StringGrid1');
    //Query.Open;
    //
    ImprimeGrade2();
    Screen.Cursor := crDefault;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCondGerImpGer2.BtSaidaClick(Sender: TObject);
begin
  Hide;
end;

procedure TFmCondGerImpGer2.EdLarguraFolhaChange(Sender: TObject);
begin
  if EdLarguraFolha.ValueVariant > 1009 then
    EdLarguraFolha.Font.Color := clRed
  else
  if EdLarguraFolha.ValueVariant > 680 then
    EdLarguraFolha.Font.Color := $004080FF  // Laranja
  else
    EdLarguraFolha.Font.Color := clBlue;
end;

procedure TFmCondGerImpGer2.EdLinhaAltChange(Sender: TObject);
begin
  GradeA.DefaultRowHeight := EdLinhaAlt.ValueVariant;
end;

procedure TFmCondGerImpGer2.EdTamFonteChange(Sender: TObject);
begin
  GradeA.Font.Size := EdTamFonte.ValueVariant;
end;

procedure TFmCondGerImpGer2.EdTamFonteExit(Sender: TObject);
begin
  RelatorioDeArrecadacoes_ListaDeCondominos(False)
end;

procedure TFmCondGerImpGer2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  ReacertaFrxDataSets();
end;

procedure TFmCondGerImpGer2.FormClose(Sender: TObject; var Action: TCloseAction);
var
  Fechar: TBasicAction;
begin
  if FmCondGerImpGer2.Visible then
  begin
    FmCondGerImpGer2.Hide;
    Fechar := TBasicAction(caNone);
    FmCondGerImpGer2.Action := Fechar;
  end;  
end;

procedure TFmCondGerImpGer2.FormCreate(Sender: TObject);
begin
  FColClicked := -1;
  FRowClicked := -1;
  FXClicked   := 0;
  FYClicked   := 0;
  FTipoGrade  := 0;
  frxGrade.AddFunction(
        'function ObtemHALign(Col, Lin: Extended): Integer');
  frxGrade.AddFunction(
        'function ObtemWidth(Col: Extended): Integer');
  frxGrade.AddFunction(
        'function ObtemRowHeight(Row: Extended): Integer');
  GradeA.ColWidths[00] := 056;
  GradeA.ColWidths[01] := 070;
  GradeA.ColWidths[02] := 024;
  GradeA.ColWidths[03] := 160;
  //GradeA.ColWidths[04] := 014;
  GradeA.ColWidths[04] := 078;
  //
  //
end;

procedure TFmCondGerImpGer2.frxGradeBeforePrint(Sender: TfrxReportComponent);
var
  Cross: TfrxCrossView;
  i, j: Integer;
begin
  if Sender is TfrxCrossView then
  begin
    Cross := TfrxCrossView(Sender);
    if FTipoGrade = 0 then
    begin
      for i := 1 to GradeA.RowCount do
        for j := 1 to GradeA.ColCount do
          Cross.AddValue([i], [j], [GradeA.Cells[j - 1, i - 1]]);
    end else begin
      for i := 1 to GradeA.RowCount do
        for j := 1 to GradeA.ColCount do
          Cross.AddValue([i], [j], [GradeA.Cells[i - 1, j - 1]]);
    end;
   // Cross.Align :=
  end;
end;

procedure TFmCondGerImpGer2.frxGradeGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_TITULO' then Value := FTitulo
  else
  if VarName = 'VARF_MEMO_HEIGHT' then
    Value := GradeA.RowHeights[0];
end;

function TFmCondGerImpGer2.frxGradeUserFunction(const MethodName: string;
  var Params: Variant): Variant;
begin
  if MethodName = Uppercase('ObtemHALign') then
  begin
    if (Params[1] = 0) or (Params[0] = 2) then
      Result := 0
    else
      Result := 2;
  end else
  if MethodName = Uppercase('ObtemWidth') then
    Result := GradeA.ColWidths[Params[0]]
  else
  if MethodName = Uppercase('ObtemRowHeight') then
    Result := GradeA.DefaultRowHeight
end;

procedure TFmCondGerImpGer2.GradeADrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  Cor, c, r, Col, Row: Integer;
  Texto: String;
begin
  c := GradeA.FixedCols;
  r := GradeA.FixedRows;
  Col := Geral.IMV(GradeA.Cells[ACol, GradeA.RowCount -1]);
  Row := ARow;
  //
  //if GradeA.ColWidths[Col] < 5 then
    //Texto := ''
  //else
    Texto := GradeA.Cells[ACol, ARow];
  //
  if (Col = 0) or (Row = 0) then
    Cor := PainelConfirma.Color
  else if GradeA.Cells[Col, Row] <> '' then
    Cor := clWindow
  else
    Cor := clMenu;
  //
  if Row = 0 then
    MyObjects.StringGridRotateTextOut(GradeA, Row, Col, Rect, 'Univers Light Condensed',
      10, clWindowText, taLeftJustify)
  else
  if (Row = GradeA.RowCount -1) then
  begin
    MyObjects.DesenhaTextoEmStringGridEx(GradeA, Rect, clWhite,
      clBtnFace, taCenter, Texto, c, r, False,
      'Arial', 10, [fsBold], Char(0))
  end else
  //if (Col in ([0,1,2,4])) then
  if (Col in ([0,1,2])) then
    MyObjects.DesenhaTextoEmStringGrid(GradeA, Rect, clBlack,
      Cor(*FmPrincipal.sd1.Colors[csButtonFace]*), taCenter,
      Texto, c, r, False)
  else
  if (Col in ([3])) or (Row = 0) then
    MyObjects.DesenhaTextoEmStringGrid(GradeA, Rect, clBlack,
      Cor(*FmPrincipal.sd1.Colors[csButtonFace]*), taLeftJustify,
      Texto, c, r, False)
  else
    MyObjects.DesenhaTextoEmStringGrid(GradeA, Rect, clBlack,
      Cor(*FmPrincipal.sd1.Colors[csButtonFace]*), taRightJustify,
      Texto, c, r, False);
end;

procedure TFmCondGerImpGer2.GradeAMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  GradeA.MouseToCell(X, Y, FColClicked, FRowClicked);
  dmkEdit5.ValueVariant := FColClicked;
  dmkEdit6.ValueVariant := FRowClicked;
  FXClicked := X;
  FYClicked := Y;

{
  if (r = 0) and (c > 0) and (c < GradeA.ColCount) then
  begin
    GradeA.Col := c;
    GradeA.Row := r;
    FSigla := GradeA.Cells[GradeA.Col, 0];
    MLAGeral.MostraPopUpDeBotaoObject(PMGradeA, GradeA, 0, 0);
  end;
}
end;

procedure TFmCondGerImpGer2.GradeAMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  CalculaLarguraFolha();
end;

procedure TFmCondGerImpGer2.GradeASelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
  if ARow = GradeA.RowCount -1 then
    CanSelect := False;
end;

procedure TFmCondGerImpGer2.Larguradacolunaatual1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCondGerImpGerLar, FmCondGerImpGerLar, afmoNegarComAviso) then
  begin
    FmCondGerImpGerLar.STLarg.Caption := IntToStr(GradeA.ColWidths[FColClicked]);
    FmCondGerImpGerLar.STCodi.Caption := GradeA.Cells[FColClicked, GradeA.RowCount - 1];
    FmCondGerImpGerLar.STNome.Caption := GradeA.Cells[FColClicked,0];
    FmCondGerImpGerLar.ShowModal;
    FmCondGerImpGerLar.Destroy;
  end;
end;

function TFmCondGerImpGer2.ObtemFrxDataField(const Coluna: Integer;
var Campo, Somas: String; var HAlinha: TfrxHAlign; var Somar: Boolean): Boolean;
var
  Fmt, Texto: String;
  i, n: integer;
begin
  n     := 0;
  Fmt   := '';
  Somar := False;
  Somas := '';
  case Coluna of
    1:
    begin
      Texto := 'Andar';
      HAlinha := haCenter;
    end;
    2:
    begin
      Texto := 'UH';
      HAlinha := haCenter;
    end;
    3:
    begin
      Texto := 'Propriet';
      HAlinha := haLeft;
    end;
    4:
    begin
      Texto := 'Quitado';
      HAlinha := haCenter;
    end;
    5:
    begin
      Texto := 'Observacao';
      HAlinha := haLeft;
    end
    else
    begin
      Texto := 'ValCol' +
        MLAGeral.FTX(Coluna - FColIniVal + 1, 3, 0, siPositivo);
      HAlinha := haRight;
      if GradeB.Cells[Coluna, 2] = '' then Fmt := '' else
      begin
        n := Geral.IMV(GradeB.Cells[Coluna, 2]);
        for i := 1 to n do
          Fmt := Fmt + '0';
      end;
      Somar := n > 0;
    end;
  end;
  if Somar = True then
  begin
    if Fmt <> '' then
    begin
      Somar := True;
      Campo := '[FormatFloat(''#,###,##0.' + Fmt + ';-#,###,##0.' + Fmt + '; '',<frxDsQuery."' + Texto + '">)]';
      Somas := '[FormatFloat(''#,###,##0.' + Fmt + ';-#,###,##0.' + Fmt + '; '',SUM(<frxDsQuery."' + Texto + '">))]';
    end else begin
      Campo := '[frxDsQuery."' + Texto + '"]';
      Somas := '[SUM(<frxDsQuery."' + Texto + '">)]';
    end;
  end else
    Campo := '[frxDsQuery."' + Texto + '"]';
  Result := True;  
end;

function TFmCondGerImpGer2.ObtemSigla(EhLeitura: Boolean): Boolean;
var
  c: Integer;
begin
  if EhLeitura then
  begin
    DmBloq.QrListaL.Close;
    DmBloq.QrListaL.Open;
    c := Geral.IMV(GradeB.Cells[GradeA.Col,1]);
    if DmBloq.QrListaL.Locate('Codigo', c, []) then
      FSigla := DmBloq.QrListaLSIGLA_IMP.Value
    else FSigla := '';
  end else FSigla := GradeA.Cells[GradeA.Col,0];
  Result := InputQuery('Nova Sigla', 'Informe a nova sigla:', FSigla);
end;

procedure TFmCondGerImpGer2.PMGradeAPopup(Sender: TObject);
var
  Coluna, TipoTab, TipoCad: Integer;
begin
{ Alexandria
  Coluna  := Geral.IMV(GradeA.Cells[GradeA.Col, GradeA.RowCount -1]);
  TipoTab := Geral.IMV(GradeB.Cells[Coluna,0]);
  TipoCad := FmCondGer.RGAgrupListaA.ItemIndex;
  FCodCad := Geral.IMV(GradeB.Cells[Coluna,1]);
  //
  Alterasiglanocadastrodacontaplanodecontas1.Enabled := False;
  Alterasiglanocadastrodaarrecadaobase1.Enabled      := False;
  AlterasiglanocadastrodaLeitura1.Enabled            := False;
  if FCodCad > 0 then
  begin
    case TipoTab of
      0:
      begin
        case TipoCad of
          1: Alterasiglanocadastrodaarrecadaobase1.Enabled      := True;
          2: Alterasiglanocadastrodacontaplanodecontas1.Enabled := True;
        end;
      end;
      1..2: AlterasiglanocadastrodaLeitura1.Enabled := True;
    end;
  end;
}
end;

procedure TFmCondGerImpGer2.RecalculaAlturaLinhaTitulos;
var
  i, Tam: Integer;
begin
  Tam := GradeA.DefaultRowHeight;
  for i := 1 to GradeA.ColCount - 1 do
    DefineTamanhoTexto(GradeA.Cells[i, 0], Tam);
  GradeA.RowHeights[0] := Tam;
end;

procedure TFmCondGerImpGer2.RecalculaLarguraColunaTextos;
var
  i, j, k, n, a, Tam: Integer;
begin
  if not CkRecalcT.Checked then Exit;
  //
  if CkLargTit.Checked then n := 0 else n := 1;
  a := GradeA.RowCount -2;
  //
  for i := 1 to GradeB.ColCount - 1 do
  begin
    Tam := 0;
    k := Geral.IMV(GradeB.Cells[i,0]);
    if k = -2 then
    begin
      for j := n to a do
        DefineTamanhoTexto(GradeA.Cells[i, j], Tam);
      GradeA.ColWidths[i] := Tam;
    end;
  end;
end;

procedure TFmCondGerImpGer2.RelatorioDeArrecadacoes_ListaDeCondominos(Mostra: Boolean);
  procedure GeraParteSQL(TabLct, EntiTXT, MezTxt: String);
  begin
    QrPgBloq.SQL.Add('SELECT DISTINCT imv.Conta Apto, lan.FatNum, ');
    QrPgBloq.SQL.Add('lan.Compensado Data, lan.Carteira, lan.Controle, ');
    QrPgBloq.SQL.Add('IF(lan.Compensado<2, "", DATE_FORMAT(lan.Compensado, ');
    QrPgBloq.SQL.Add('"%d/%m/%Y")) DATA_TXT');
    QrPgBloq.SQL.Add('FROM ' + TabLct + ' lan');
    QrPgBloq.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
    QrPgBloq.SQL.Add('LEFT JOIN condimov imv ON imv.Conta=lan.Depto');
    QrPgBloq.SQL.Add('LEFT JOIN contas con ON con.Codigo=lan.Genero');
    QrPgBloq.SQL.Add('WHERE car.Tipo in (0,2)');
    QrPgBloq.SQL.Add('AND lan.FatID in (600,601)');
    QrPgBloq.SQL.Add('AND car.ForneceI=' + EntiTXT);
    QrPgBloq.SQL.Add('AND lan.Mez=' + MezTXT);
  end;
var
  Lin, i, l, j, Tam02, Tam01, c1, c2(*, tf*): Integer;
  Valor, Somas, Leitu: Double;
  TxtCell, EntiTxt, MezTxt: String;
  Achou: Boolean;
begin
{ Alexandria
  Achou := False;
  Screen.Cursor := crHourGlass;
  try
    //tf := Round(EdTamFonte.ValueVariant * 0.6);
    //
    Canvas.Font.Name := 'Univers Light Condensed';
    Canvas.Font.Size := EdTamFonte.ValueVariant;
    //
    // deve ser antes para n�o acumular
    // deve ser todas colunas pois aptos podem ser inativados
    MyObjects.LimpaGrade(GradeA, 0, 0, True);
    GradeA.ColCount := FColIniVal;
    for i := 0 to FColIniVal - 1 do
    begin
      case i of
        0: TxtCell := 'ID ' + DModG.ReCaptionTexto(VAR_U_H);
        1: TxtCell := 'Bloqueto';
        2: TxtCell := DModG.ReCaptionTexto(VAR_U_H);
        3: TxtCell := DModG.ReCaptionTexto(VAR_P_R_O_P_R_I_E_T_A_R_I_O);
        4: TxtCell := 'Quita��o';
        5: TxtCell := 'Observa��o';
      end;
      GradeA.Cells[i,0] := TxtCell;
      if i = 0 then TxtCell := '-3' else TxtCell := '-2';
      GradeB.Cells[i,0] := TxtCell;
    end;

    // L I N H A S

    // UHs
    EntiTXT := FormatFloat('0', DmCond.QrCondCliente.Value);
    MezTXT  := FormatFloat('0', MLAGeral.PeriodoToMez(FmCondGer.QrPrevPeriodo.Value));

    QrPgBloq.Close;
    QrPgBloq.SQL.Clear;
    //
    QrPgBloq.Close;
    QrPgBloq.SQL.Clear;
    QrPgBloq.SQL.Add('DROP TABLE IF EXISTS _COND_GER_IMP_GER_2_;');
    QrPgBloq.SQL.Add('CREATE TABLE _COND_GER_IMP_GER_2_');
    QrPgBloq.SQL.Add('');
    GeraParteSQL(DmCond.FTabLctA, EntiTXT, MezTxt);
    QrPgBloq.SQL.Add('UNION');
    GeraParteSQL(DmCond.FTabLctB, EntiTXT, MezTxt);
    QrPgBloq.SQL.Add('UNION');
    GeraParteSQL(DmCond.FTabLctD, EntiTXT, MezTxt);
    QrPgBloq.SQL.Add(';');
    QrPgBloq.SQL.Add('');
    QrPgBloq.SQL.Add('SELECT * FROM _COND_GER_IMP_GER_2_');
    QrPgBloq.SQL.Add('ORDER BY Data, Carteira, Controle;');
    QrPgBloq.SQL.Add('');
    QrPgBloq.SQL.Add('DROP TABLE IF EXISTS _COND_GER_IMP_GER_2_;');
    QrPgBloq.SQL.Add('');
    QrPgBloq.Open;
    //  QrPgBloq > QrAptos  (ligados pelo apto por lookup)
    QrAptos.Close;
    QrAptos.Params[0].AsInteger := DmCond.QrCondCodigo.Value;
    QrAptos.Open;
    (*
    GradeA.RowCount := QrAptos.RecordCount + 1;
    QrAptos.First;
    while not QrAptos.Eof do
    *)
    QrBol3.Close;
(*
    QrBol3.SQL.Clear;
    QrBol3.SQL.Add('SELECT DISTINCT cdb.Ordem, cdi.FracaoIdeal, cdi.Andar,');
    QrBol3.SQL.Add('ari.Boleto, ari.Apto,');
    QrBol3.SQL.Add('ari.Propriet, ari.Vencto, cdi.Unidade,');
    QrBol3.SQL.Add('CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
    QrBol3.SQL.Add('ELSE ent.Nome END NOMEPROPRIET,');
    QrBol3.SQL.Add('CONCAT_WS("-", ari.Boleto, ari.Apto) BOLAPTO, 0 KGT');
    QrBol3.SQL.Add('FROM ' + DmCond.FTabAriA + ' ari');
    QrBol3.SQL.Add('LEFT JOIN condimov cdi ON cdi.Conta=ari.Apto');
    QrBol3.SQL.Add('LEFT JOIN condbloco cdb ON cdb.Controle=cdi.Controle');
    QrBol3.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=ari.Propriet');
    QrBol3.SQL.Add('WHERE ari.Codigo=:P0');
    QrBol3.SQL.Add('AND ari.Boleto <> 0');
    QrBol3.SQL.Add('');
    QrBol3.SQL.Add('UNION');
    QrBol3.SQL.Add('');
    QrBol3.SQL.Add('SELECT DISTINCT cdb.Ordem, cdi.FracaoIdeal, cdi.Andar, cni.Boleto, cni.Apto,');
    QrBol3.SQL.Add('cni.Propriet, cni.Vencto, cdi.Unidade,');
    QrBol3.SQL.Add('CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
    QrBol3.SQL.Add('ELSE ent.Nome END NOMEPROPRIET,');
    QrBol3.SQL.Add('CONCAT_WS("-", cni.Boleto, cni.Apto) BOLAPTO, 0 KGT');
    QrBol3.SQL.Add('FROM ' + DmCond.FTabCnsA + ' cni');
    QrBol3.SQL.Add('LEFT JOIN condimov  cdi ON cdi.Conta=cni.Apto');
    QrBol3.SQL.Add('LEFT JOIN condbloco cdb ON cdb.Controle=cdi.Controle');
    QrBol3.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=cni.Propriet');
    QrBol3.SQL.Add('WHERE cni.Cond=:P1');
    QrBol3.SQL.Add('AND cni.Periodo=:P2');
    QrBol3.SQL.Add('AND cni.Boleto <> 0');
    QrBol3.SQL.Add('');
    QrBol3.SQL.Add('ORDER BY Ordem, Andar, Unidade, Boleto');
*)
    QrBol3.SQL.Clear;
    QrBol3.SQL.Text := DmBloq.QrBoletos.SQL.Text;
    QrBol3.Params := DmBloq.QrBoletos.Params;
    QrBol3.Open;

    GradeA.RowCount := QrBol3.RecordCount + 1;
    QrBol3.First;
    while not QrBol3.Eof do
    begin
      Lin := QrBol3.RecNo;
      if QrAptos.Locate('Conta', QrBol3Apto.Value, []) then
      begin
        GradeA.Cells[00,Lin] := MLAGeral.FTX(QrAptosConta.Value, 6,0, siNegativo);
        GradeA.Cells[01,Lin] := FormatFloat('0', QrBol3Boleto.Value);
        GradeA.Cells[02,Lin] := QrAptosUnidade.Value;
        GradeA.Cells[03,Lin] := QrAptosNOMEPROP.Value;
        //GradeA.Cells[04,Lin] := QrAptosSigla.Value;
        GradeA.Cells[04,Lin] := QrAptosDATAPG_TXT.Value;
        GradeA.Cells[05,Lin] := '';
        //
        //QrAptos.Next;
        QrBol3.Next;
      end else begin
        Geral.MensagemBox('Unidade n�o localizada: C�digo = ' +
        FormatFloat('0', DmBloq.QrListaAConta.Value) + '. AVISE A DERMATEK',
        'Erro', MB_OK+MB_ICONERROR);
        Exit;
      end;
    end;
    //


    // C O L U N A S

    // 1/2 colunas: Arrecada��es
    DmBloq.ReopenListaA(FmCondGer.RGGerado.ItemIndex = 1);
    DmBloq.QrListaA.First;
    while not DmBloq.QrListaA.Eof do
    begin
      //GradeA.Cells[01,Lin] := MLAGeral.FTX(QrAptosAndar.Value, 3,0, siNegativo);
      Tam01 := 16;
      if DmBloq.QrListaAInfoRelArr.Value = 1 then
      begin
        GradeA.ColCount := GradeA.ColCount + 2;
        c1 := GradeA.ColCount -2;
        c2 := GradeA.ColCount -1;
        GradeB.ColCount := c2;
        // Informar que � cota de arrecada��o
        GradeB.Cells[c2,0] := '3';
        // Informar qual � o codigo do cadastro cfe selecionado em FmCondger.RGAgrupListaA
        case FmCondger.RGAgrupListaA.ItemIndex of
          0: GradeB.Cells[c2,1] := '0';                                         // ArreaBaI
          1: GradeB.Cells[c2,1] := dmkPF.FFP(DmBloq.QrListaACodigo.Value, 0); // ArreBaC
          2: GradeB.Cells[c2,1] := dmkPF.FFP(DmBloq.QrListaAConta.Value, 0);  // Conta (plano de contas)
          else GradeB.Cells[c2,1] := '-1';
        end;
        //T�tulo
        GradeA.Cells[c2,0] := DmBloq.QrListaATITULO_FATOR.Value;
        // Informar a quantidade de casas decimais em n�meros flutuantes ('' = sem formata��o)
        GradeB.Cells[c2,2] := '';
      end else begin
        GradeA.ColCount := GradeA.ColCount + 1;
        c1 := GradeA.ColCount -1;
        c2 := -1;
        GradeB.ColCount := c1;
      end;
      GradeA.Cells[c1,0] := DmBloq.QrListaASIGLA_IMP.Value;
      //
      // Informar que � arrecada��o
      GradeB.Cells[c1,0] := '0';
      // Informar qual � o codigo do cadastro cfe selecionado em FmCondger.RGAgrupListaA
      case FmCondger.RGAgrupListaA.ItemIndex of
        0: GradeB.Cells[c1,1] := '0';                                           // ArreaBaI
        1: GradeB.Cells[c1,1] := dmkPF.FFP(DmBloq.QrListaACodigo.Value, 0);   // ArreBaC
        2: GradeB.Cells[c1,1] := dmkPF.FFP(DmBloq.QrListaAConta.Value, 0);    // Conta (plano de contas)
        else GradeB.Cells[c1,1] := '-1';
      end;
      //
      // Informar a quantidade de casas decimais em n�meros flutuantes
      GradeB.Cells[c1,2] := '2';

      DmBloq.QrListaAIts.First;
      Somas := 0;
      Leitu := 0;
      while not DmBloq.QrListaAIts.Eof do
      begin
        Achou := False;
        for i := 1 to GradeA.RowCount - 1 do
        begin
          if (Geral.IMV(GradeA.Cells[0,i]) = DmBloq.QrListaAItsApto.Value) and
          (Geral.IMV(GradeA.Cells[1,i]) = DmBloq.QrListaAItsBoleto.Value) then
          begin
            Achou := True;
            Valor := Geral.DMV(GradeA.Cells[c1, i]);
            Somas := Somas + DmBloq.QrListaAItsValor.Value;
            Valor := Valor + DmBloq.QrListaAItsValor.Value;
            TxtCell := Geral.FFT(Valor, 2, siNegativo);
            GradeA.Cells[c1, i] := TxtCell;
            DefineTamanhoTexto(TxtCell, Tam01);
            //
            if c2 > -1 then
            begin
              if DmBloq.QrListaAItsFATOR_COBRADO.Value > 0 then
              begin
                Leitu := Leitu + DmBloq.QrListaAItsFATOR_COBRADO.Value;
                TxtCell := FloatToStr(DmBloq.QrListaAItsFATOR_COBRADO.Value);
                GradeA.Cells[c2, i] := TxtCell;
                DefineTamanhoTexto(TxtCell, Tam02);
              end;
            end;
            //
            Break;
          end;
        end;
        //
        DmBloq.QrListaAIts.Next;
      end;
      if not Achou then
      begin
        Geral.MensagemBox('O bloqueto ' + FormatFloat('0',
        DmBloq.QrListaAItsBoleto.Value) + ' n�o foi localizado!'#13#10 +
        DmBloq.QrListaATEXTO_IMP.Value + #13#10 + #13#10 + 'AVISE A DERMATEK',
        'Erro', MB_OK+MB_ICONERROR);
        Screen.Cursor := crDefault;
        Exit;
      end;
      TxtCell := Geral.FFT(Somas, 2, siNegativo);
      DefineTamanhoTexto(TxtCell, Tam01);
      GradeA.ColWidths[c1] := Tam01;
      //
      if c2 > -1 then
      begin
        TxtCell := Geral.FFT(Leitu, 2, siNegativo);
        DefineTamanhoTexto(TxtCell, Tam02);
        if Tam02 > 160 then
          Tam02 := 160;
        GradeA.ColWidths[c2] := Tam02;
      end;  
      //
      DmBloq.QrListaA.Next;
    end;
    //  Fim arrecada��es

    // 2/2 colunas: Leituras
    DmBloq.ReopenListaL(FmCondGer.RGGerado.ItemIndex = 1);
    DmBloq.QrListaL.First;
    while not DmBloq.QrListaL.Eof do
    begin
      Tam02 := 16;
      Tam01 := 16;
      GradeA.ColCount := GradeA.ColCount + 2;
      c1 := GradeA.ColCount - 1;
      c2 := GradeA.ColCount - 2;
      GradeA.Cells[c2,0] := DmBloq.QrListaLUnidLei.Value + ' ' + DmBloq.QrListaLSIGLA_IMP.Value;
      GradeA.Cells[c1,0] := Dmod.QrControle.FieldByName('Moeda').AsString + ' ' + DmBloq.QrListaLSIGLA_IMP.Value;
      GradeA.ColWidths[c1] := 72;
      GradeA.ColWidths[c2] := 72;
      //

      //
      GradeB.ColCount := GradeA.ColCount;
      // Informar que � a medi��o de uma leitura
      GradeB.Cells[c2,0] := '1';
      // Informar qual � o codigo de cadastro da leitura
      GradeB.Cells[c2,1] := dmkPF.FFP(DmBloq.QrListaLCodigo.Value, 0);
      // Informar a quantidade de casas decimais em n�meros flutuantes
      GradeB.Cells[c2,2] := dmkPF.FFP(DmBloq.QrListaLCasas.Value, 0);
      //

      // Informar que � o valor (R$) de uma leitura
      GradeB.Cells[c1,0] := '2';
      // Informar qual � o codigo de cadastro da leitura
      GradeB.Cells[c1,1] := dmkPF.FFP(DmBloq.QrListaLCodigo.Value, 0);
      // Informar a quantidade de casas decimais em n�meros flutuantes
      GradeB.Cells[c1,2] := '2';
      //

      Somas := 0;
      Leitu := 0;
      DmBloq.QrListaLIts.First;
      while not DmBloq.QrListaLIts.Eof do
      begin
        Achou := False;
        for i := 1 to GradeA.RowCount - 1 do
        begin
          if (Geral.IMV(GradeA.Cells[0,i]) = DmBloq.QrListaLItsApto.Value) and
          (Geral.IMV(GradeA.Cells[1,i]) = DmBloq.QrListaLItsBoleto.Value) then
          begin
            Achou := True;
            Valor := Geral.DMV(GradeA.Cells[c2, i]);
            Leitu := Leitu + DmBloq.QrListaLItsConsumo.Value;
            Valor := Valor + DmBloq.QrListaLItsConsumo.Value;
            TxtCell := Geral.FFT(Valor, DmBloq.QrListaLCasas.Value, siNegativo);
            GradeA.Cells[c2, i] := TxtCell;
            DefineTamanhoTexto(TxtCell, Tam02);
            //
            Valor := Geral.DMV(GradeA.Cells[c1, i]);
            Valor := Valor + DmBloq.QrListaLItsValor.Value;
            Somas := Somas + DmBloq.QrListaLItsValor.Value;
            TxtCell := Geral.FFT(Valor, 2, siNegativo);
            GradeA.Cells[c1, i] := TxtCell;
            DefineTamanhoTexto(TxtCell, Tam01);
            //
            Break;
          end;
        end;
        if not Achou then
        begin
          Geral.MensagemBox('O bloqueto ' + FormatFloat('0',
          DmBloq.QrListaAItsBoleto.Value) + ' n�o foi localizado!'#13#10 +
          DmBloq.QrListaATEXTO_IMP.Value + #13#10 + #13#10 + 'AVISE A DERMATEK',
          'Erro', MB_OK+MB_ICONERROR);
          Screen.Cursor := crDefault;
          Exit;
        end;
        //
        DmBloq.QrListaLIts.Next;
      end;
      //
      TxtCell := Geral.FFT(Leitu, 2, siNegativo);
      DefineTamanhoTexto(TxtCell, Tam02);
      if Tam02 > 160 then
        Tam02 := 160;
      GradeA.ColWidths[c2] := Tam02;
      //
      TxtCell := Geral.FFT(Somas, 2, siNegativo);
      DefineTamanhoTexto(TxtCell, Tam01);
      if Tam01 > 160 then
        Tam01 := 160;
      GradeA.ColWidths[c1] := Tam01;
      //
      DmBloq.QrListaL.Next;
    end;
    // Fim leituras


    // Totais de arrecada��es + leituras
    GradeA.ColCount := GradeA.ColCount + 1;
    c1 := GradeA.ColCount -1;
    GradeA.Cells[c1,0] := 'TOTAL';
    GradeB.ColCount := GradeA.ColCount;
    GradeB.Cells[c1,0] := '-1';
    GradeB.Cells[c1,1] := '';
    GradeB.Cells[c1,2] := '2';
    Tam01 := 16;
    c1    := GradeA.ColCount - 1;
    Somas := 0;
    for i := 1 to GradeA.RowCount - 1 do
    begin
      Valor := 0;
      for j := FColIniVal to GradeA.ColCount - 2 do
        if Geral.IMV(GradeB.Cells[j, 0]) in ([0,2]) then
        begin
          Valor := Valor + Geral.DMV(GradeA.Cells[j,i]);
        end;
      Somas := Somas + Valor;
      TxtCell := Geral.FFT(Valor, 2, siNegativo);
      GradeA.Cells[c1, i] := TxtCell;
      DefineTamanhoTexto(TxtCell, Tam01);
    end;
    TxtCell := Geral.FFT(Somas, 2, siNegativo);
    DefineTamanhoTexto(TxtCell, Tam01);
    GradeA.ColWidths[c1] := Tam01;
    //
    // Numerar colunas na �ltima linha para saber qual coluna � ao mov�-la.
    //Valor := 0;
    GradeA.RowCount := GradeA.RowCount + 1;
    for i := 0 to GradeA.ColCount - 1 do
      GradeA.Cells[i,GradeA.RowCount - 1] := dmkPF.FFP(i, 0);
    //
    CalculaLarguraFolha();
    //
    for i := 0 to GradeB.ColCount - 1 do
      GradeB.ColWidths[i] := GradeA.ColWidths[i];
    //
    RecalculaLarguraColunaTextos;
    RecalculaAlturaLinhaTitulos;
    //
    Screen.Cursor := crDefault;
    if Mostra then Show;
  finally
    Screen.Cursor := crDefault;
  end;
}
end;

procedure TFmCondGerImpGer2.ImprimeGrade2();
var
  Page: TfrxReportPage;
  Head: TfrxPageHeader;
  Shape: TfrxShapeView;
  Memo: TfrxMemoView;
  Line01: TfrxLineView;
  Band: TfrxMasterData;
  Summ: TfrxReportSummary;
  L, T, W, H, Z, M, X, Y, U: Integer;
  i, c, n: Integer;
  Campo, Somas: String;
  AlinhaH: TfrxHAlign;
  Somar: Boolean;
begin
{
  // Configura frxReport
  frxGrade2.DataSets.Add(frxDsQuery);
  //frxGrade2.DataSets.Items[0] := frxDsQuery;
  //
  // Configura p�gina
  while frxGrade2.PagesCount > 0 do
    frxGrade2.Pages[0].Free;
  Page := TfrxReportPage.Create(frxGrade2);
  Page.CreateUniqueName;
  Page.Height       := 210;
  if EdLarguraFolha.ValueVariant > 680 then
  begin
    Page.Orientation  := poLandscape;
    X := 7700;                 // 2010-07-02 -> Original = 8700
    Page.LeftMargin   :=  20;  // 2010-07-02 -> Original = 10
    Page.RightMargin  :=  10;
    Page.TopMargin    :=  15;
    Page.BottomMargin :=  15;
  end else begin
    Page.Orientation  := poPortrait;
    X := 0;
    Page.LeftMargin   :=  15;
    Page.RightMargin  :=  15;
    Page.TopMargin    :=  10;
    Page.BottomMargin :=  10;
  end;
  //
  //////////////////////////////////////////////////////////////////////////////
  // Configura Page Header
  //
  L :=  Round(     0      / VAR_frCM);
  T :=  Round(   500      / VAR_frCM);
  W :=  Round((18000 + X) / VAR_frCM);
  H :=  Round(  5000      / VAR_frCM);  // 2010-07-02 -> Original = 4500
  Head := TfrxPageHeader.Create(Page);
  Head.CreateUniqueName;
  Head.SetBounds(L, T, W, H);
  //////////////////////////////////////////////////////////////////////////////
  ///  Shape
  L :=  Round(     0      / VAR_frCM);
  T :=  Round(     0      / VAR_frCM);
  W :=  Round((18000 + X) / VAR_frCM);
  H :=  Round(  1000      / VAR_frCM);
  Shape := TfrxShapeView.Create(Head);
  Shape.CreateUniqueName;
  Shape.Visible := True;
  Shape.SetBounds(L, T, W, H);
  Shape.Frame.Typ := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
  Shape.Frame.Width := 0.1;
  Shape.Shape := skRoundRectangle;
  //
  // Nome administradora
  L :=  Round(   200      / VAR_frCM);
  T :=  Round(     0      / VAR_frCM);
  W :=  Round((17600 + X) / VAR_frCM);
  H :=  Round(   500      / VAR_frCM);
  Memo := TfrxMemoView.Create(Head);
  Memo.CreateUniqueName;
  Memo.SetBounds(L, T, W, H);
  Memo.Visible     := True;
  Memo.Font.Name   := 'Arial';
  Memo.Font.Size   := 8;
  Memo.Memo.Text   := Dmod.QrDonoNOMEDONO.Value;
  Memo.Font.Style  := [fsBold];
  Memo.HAlign      := haCenter;
  Memo.VAlign      := vaCenter;
  //
  // Data, hora impress�o
  L :=  Round(   200      / VAR_frCM);
  T :=  Round(   500      / VAR_frCM);
  W :=  Round(  3800      / VAR_frCM);
  H :=  Round(   500      / VAR_frCM);
  Memo := TfrxMemoView.Create(Head);
  Memo.CreateUniqueName;
  Memo.SetBounds(L, T, W, H);
  Memo.Visible     := True;
  Memo.Font.Name   := 'Univers Light Condensed';
  Memo.Font.Size   := 8;
  Memo.Memo.Text   := 'Impresso em [Date], [Time]';
  Memo.VAlign      := vaCenter;
  //
  // Nome Relat�rio
  L :=  Round(  4000      / VAR_frCM);
  T :=  Round(   500      / VAR_frCM);
  W :=  Round((10000 + X) / VAR_frCM);
  H :=  Round(   500      / VAR_frCM);
  Memo := TfrxMemoView.Create(Head);
  Memo.CreateUniqueName;
  Memo.SetBounds(L, T, W, H);
  Memo.Visible     := True;
  Memo.Font.Name   := 'Arial';
  Memo.Font.Size   := 8;
  Memo.Memo.Text   := 'RELAT�RIO DE ARRECADA��ES';
  Memo.Font.Style  := [fsBold];
  Memo.HAlign      := haCenter;
  Memo.VAlign      := vaCenter;
  //
  // P�gina ? de ?
  L :=  Round((14000 + X) / VAR_frCM);
  T :=  Round(   500      / VAR_frCM);
  W :=  Round(  3800      / VAR_frCM);
  H :=  Round(   500      / VAR_frCM);
  Memo := TfrxMemoView.Create(Head);
  Memo.CreateUniqueName;
  Memo.SetBounds(L, T, W, H);
  Memo.Visible     := True;
  Memo.Font.Name   := 'Univers Light Condensed';
  Memo.Font.Size   := 8;
  Memo.Memo.Text   := 'P�gina [Page] de [TotalPages]';
  Memo.HAlign      := haRight;
  Memo.VAlign      := vaCenter;
  //
  // Linha divis�ria shape
  L :=  Round(     0      / VAR_frCM);
  T :=  Round(   500      / VAR_frCM);
  W :=  Round((18000 + X) / VAR_frCM);
  H :=  Round(     0      / VAR_frCM);
  Line01 := TfrxLineView.Create(Head);
  Line01.CreateUniqueName;
  Line01.SetBounds(L, T, W, H);
  Line01.Visible     := True;
  Line01.Frame.Width := 0.1;
  //
  // Nome Condom�nio
  L :=  Round(    0       / VAR_frCM);
  T :=  Round( 1200       / VAR_frCM);
  W :=  Round((18000 + X) / VAR_frCM);
  H :=  Round(  500       / VAR_frCM);
  Memo := TfrxMemoView.Create(Head);
  Memo.CreateUniqueName;
  Memo.SetBounds(L, T, W, H);
  Memo.Visible     := True;
  Memo.Font.Name   := 'Arial';
  Memo.Font.Size   := 8;
  Memo.Memo.Text   := DmCond.QrCondNOMECLI.Value;
  Memo.Font.Style  := [fsBold];
  Memo.HAlign      := haCenter;
  Memo.VAlign      := vaCenter;
  //
  // Per�odo do relat�rio (M�s e ano)
  L :=  Round(     0       / VAR_frCM);
  T :=  Round(  1800       / VAR_frCM);
  W :=  Round((18000 + X)  / VAR_frCM);
  H :=  Round(   500       / VAR_frCM);
  Memo := TfrxMemoView.Create(Head);
  Memo.CreateUniqueName;
  Memo.SetBounds(L, T, W, H);
  Memo.Visible     := True;
  Memo.Font.Name   := 'Arial';
  Memo.Font.Size   := 10;
  Memo.Memo.Text   := FmCondGer.QrPrevPERIODO_TXT.Value;
  Memo.Font.Style  := [fsBold];
  Memo.HAlign      := haCenter;
  Memo.VAlign      := vaCenter;
  Memo.Frame.Typ   := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
  Memo.Frame.Width := 0.1;
  //
  // Master data para linhas de dados
  H :=  GradeA.DefaultRowHeight;
  Band := TfrxMasterData.Create(Page);
  Band.CreateUniqueName;
  Band.SetBounds(0, 0, 0, H);
  Band.DataSet     := frxDsQuery;
  // Linha de totais dos valores
  Y := H + Round(200/VAR_frCM);
  Summ := TfrxReportSummary.Create(Page);
  Summ.CreateUniqueName;
  Summ.SetBounds(0, 0, 0, Y);
  //
  // Dados do relat�rio
  //H := H;
  L := 0;
  T := 0;
  W := 0;
  for i := 1 to GradeA.ColCount -1 do
  begin
    c := Geral.IMV(GradeA.Cells[i, GradeA.RowCount -1]);

    // Dados das colunas
    ObtemFrxDataField(c, Campo, Somas, AlinhaH, Somar);
    if GradeA.ColWidths[i] < 5 then Campo := '';
    L :=  L + W;
    W :=  GradeA.ColWidths[i];
    Memo := TfrxMemoView.Create(Band);
    Memo.CreateUniqueName;
    Memo.SetBounds(L, T, W, H);
    Memo.Visible     := True;
    Memo.Font.Name   := 'Univers Light Condensed';
    Memo.Font.Size   := EdTamFonte.ValueVariant;
    Memo.Memo.Text   := Campo;
    Memo.HAlign      := AlinhaH;
    Memo.VAlign      := vaCenter;
    Memo.Frame.Typ   := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
    Memo.Frame.Width := 0.1;
    Memo.WordWrap    := False;
    Memo.Rotation    := 0;

    // T�tulos das colunas
    Z :=  Round( 2500 / VAR_frCM);
    M :=  Round( 2500 / VAR_frCM);  // 2010-07-02 -> Original = 2000
    Memo := TfrxMemoView.Create(Head);
    Memo.CreateUniqueName;
    Memo.SetBounds(L, Z, W, M);
    Memo.Visible     := True;
    Memo.Font.Name   := 'Univers Light Condensed';
    Memo.Font.Size   := EdTamFonte.ValueVariant;
    if GradeA.ColWidths[i] < 5 then
      Memo.Memo.Text   := ''
    else
      Memo.Memo.Text   := GradeA.Cells[i, 0];

    // 2010-07-02 - Exporta��o para PDF incorreta
    (* original:
    Memo.HAlign      := haLeft;
    Memo.VAlign      := vaCenter;
    *) // Novo
    Memo.HAlign      := haCenter;
    Memo.VAlign      := vaCenter;
    // fim 2010-07-02

    Memo.Frame.Typ   := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
    Memo.Frame.Width := 0.1;
    Memo.WordWrap    := False;
    Memo.Rotation    := 90;

    // somas das colunas
    Y :=  (*Z + M +*) Round(150 / VAR_frCM);
    Memo := TfrxMemoView.Create(Summ);
    Memo.CreateUniqueName;
    if c < FColIniVal then
    begin
      if c = 1 then
      begin
        U := 0;
        for n := 1 to FColIniVal - 1 do
          U := U + GradeA.ColWidths[n];
        Memo.SetBounds(L, Y, U, H);
        if GradeA.ColWidths[i] < 5 then
          Memo.Memo.Text   := ''
        else
          Memo.Memo.Text   := 'TOTAIS: ';
        //
      end;
    end else begin
      Memo.SetBounds(L, Y, W, H);
      if GradeA.ColWidths[i] < 5 then
        Memo.Memo.Text   := ''
      else
        Memo.Memo.Text   := Somas;
    end;
    Memo.Visible     := True;
    Memo.Font.Name   := 'Univers Light Condensed';
    Memo.Font.Size   := EdTamFonte.ValueVariant;
    Memo.HAlign      := AlinhaH;
    Memo.VAlign      := vaCenter;
    Memo.Frame.Typ   := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
    Memo.Frame.Width := 0.1;
    Memo.WordWrap    := False;
    Memo.Font.Style  := [fsBold];
  end;

  //

  ReacertaFrxDataSets();
  if CkDesign.Checked then
    frxGrade2.DesignReport
  else
    MyObjects.frxMostra(frxGrade2, 'Relat�rio de arrecada��es');
}
end;

function TFmCondGerImpGer2.Define_frxCond(frx: TfrxReport): TfrxReport;
begin
{ Alexandria
  ReacertaFrxDataSets();
  //frx.EnabledDataSets.Add(                 frxDsBoletos);
  //frx.EnabledDataSets.Add(                 frxDsBoletosIts);
  frx.EnabledDataSets.Add(FmCondGer.       frxDsCarts);
  //frx.EnabledDataSets.Add(                 frxDsCond);
  //frx.EnabledDataSets.Add(                 frxDsConfigBol);
  frx.EnabledDataSets.Add(DMod.            frxDsDono);
  frx.EnabledDataSets.Add(DMod.            frxDsEndereco);
  //frx.EnabledDataSets.Add(                 frxDsInquilino);
  frx.EnabledDataSets.Add(DMod.            frxDsMaster);
  //frx.EnabledDataSets.Add(                 frxDsMov);
  //frx.EnabledDataSets.Add(                 frxDsMov2);
  //frx.EnabledDataSets.Add(FmCondGer.       frxDsPrev Its);
  //
  Result := frx;
}
end;

procedure TFmCondGerImpGer2.ReacertaFrxDataSets();
begin
  //frxDsCons.DataSet := DCond.QrCons;
  //frxDsCons Its.DataSet := DCond.QrCons Its;
end;

end.

