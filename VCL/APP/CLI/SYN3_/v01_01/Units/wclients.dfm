object Fmwclients: TFmwclients
  Left = 345
  Top = 165
  Caption = 'WEB-CLIEN-001 :: Clientes WEB'
  ClientHeight = 544
  ClientWidth = 916
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelWeb: TPanel
    Left = 0
    Top = 96
    Width = 916
    Height = 448
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 2
    Visible = False
    object DBGClientes: TDBGrid
      Left = 0
      Top = 48
      Width = 916
      Height = 156
      Align = alTop
      DataSource = DsClientes
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
    end
    object Panel7: TPanel
      Left = 0
      Top = 0
      Width = 916
      Height = 48
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label3: TLabel
        Left = 8
        Top = 4
        Width = 98
        Height = 13
        Caption = 'Pesquisa pelo nome:'
      end
      object SpeedButton5: TSpeedButton
        Left = 380
        Top = 20
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton5Click
      end
      object Label25: TLabel
        Left = 596
        Top = 4
        Width = 63
        Height = 13
        Caption = 'Senha inicial:'
      end
      object Label26: TLabel
        Left = 408
        Top = 4
        Width = 58
        Height = 13
        Caption = 'Login inicial:'
      end
      object EdPesq: TdmkEdit
        Left = 8
        Top = 20
        Width = 369
        Height = 21
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        OnChange = EdPesqChange
        OnKeyDown = EdPesqKeyDown
      end
      object EdPassword: TdmkEdit
        Left = 596
        Top = 20
        Width = 185
        Height = 21
        MaxLength = 32
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdUsername: TdmkEdit
        Left = 408
        Top = 20
        Width = 185
        Height = 21
        MaxLength = 32
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
    end
    object GroupBox5: TGroupBox
      Left = 0
      Top = 378
      Width = 916
      Height = 70
      Align = alBottom
      TabOrder = 2
      object Panel1: TPanel
        Left = 2
        Top = 15
        Width = 912
        Height = 53
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Panel16: TPanel
          Left = 768
          Top = 0
          Width = 144
          Height = 53
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BitBtn2: TBitBtn
            Tag = 15
            Left = 8
            Top = 3
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BitBtn2Click
          end
        end
        object BtConf2: TBitBtn
          Tag = 14
          Left = 4
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
          Caption = '&Confirma'
          Default = True
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtConf2Click
        end
      end
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 96
    Width = 916
    Height = 448
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    Visible = False
    object PainelEdit: TPanel
      Left = 0
      Top = 0
      Width = 916
      Height = 264
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label8: TLabel
        Left = 8
        Top = 48
        Width = 249
        Height = 13
        Caption = 'Descri'#231#227'o do condom'#237'nio no site (Meu Condom'#237'nio):'
        FocusControl = DBEdit2
      end
      object Label29: TLabel
        Left = 488
        Top = 48
        Width = 29
        Height = 13
        Caption = 'Login:'
      end
      object Label30: TLabel
        Left = 676
        Top = 48
        Width = 34
        Height = 13
        Caption = 'Senha:'
      end
      object Label10: TLabel
        Left = 76
        Top = 4
        Width = 60
        Height = 13
        Caption = 'Condom'#237'nio:'
        FocusControl = DBEdit1
      end
      object Label12: TLabel
        Left = 144
        Top = 4
        Width = 45
        Height = 13
        Caption = 'Entidade:'
        FocusControl = DBEdit3
      end
      object Label13: TLabel
        Left = 212
        Top = 4
        Width = 90
        Height = 13
        Caption = 'Nome da entidade:'
        FocusControl = DBEdit4
      end
      object Label14: TLabel
        Left = 8
        Top = 4
        Width = 40
        Height = 13
        Caption = 'ID Web:'
        FocusControl = DBEdit5
      end
      object Label7: TLabel
        Left = 276
        Top = 92
        Width = 118
        Height = 13
        Caption = 'Nome do s'#237'ndico no site:'
      end
      object EdNomeWeb: TdmkEdit
        Left = 8
        Top = 64
        Width = 475
        Height = 21
        TabOrder = 4
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdUser2: TdmkEdit
        Left = 488
        Top = 64
        Width = 182
        Height = 21
        MaxLength = 32
        TabOrder = 5
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdPass2: TdmkEdit
        Left = 676
        Top = 64
        Width = 185
        Height = 21
        MaxLength = 32
        TabOrder = 6
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object DBEdit1: TDBEdit
        Left = 76
        Top = 20
        Width = 64
        Height = 21
        DataField = 'Codigo'
        DataSource = Dswclients
        TabOrder = 1
      end
      object DBEdit3: TDBEdit
        Left = 144
        Top = 20
        Width = 64
        Height = 21
        DataField = 'Cliente'
        DataSource = Dswclients
        TabOrder = 2
      end
      object DBEdit4: TDBEdit
        Left = 212
        Top = 20
        Width = 649
        Height = 21
        DataField = 'NCONDOM'
        DataSource = Dswclients
        TabOrder = 3
      end
      object DBEdit5: TDBEdit
        Left = 8
        Top = 20
        Width = 64
        Height = 21
        DataField = 'User_ID'
        DataSource = Dswclients
        TabOrder = 0
      end
      object GroupBox1: TGroupBox
        Left = 8
        Top = 88
        Width = 265
        Height = 149
        Caption = ' Parcelamento de bloquetos em atrazo: '
        TabOrder = 7
        object Label17: TLabel
          Left = 16
          Top = 24
          Width = 120
          Height = 13
          Caption = 'Valor m'#237'nimo da parcela: '
        end
        object Label18: TLabel
          Left = 16
          Top = 48
          Width = 157
          Height = 13
          Caption = 'Quantidade m'#225'xima de parcelas: '
        end
        object Label21: TLabel
          Left = 16
          Top = 96
          Width = 132
          Height = 13
          Caption = 'Taxa em % de juros ao m'#234's:'
        end
        object Label22: TLabel
          Left = 16
          Top = 72
          Width = 57
          Height = 13
          Caption = 'Multa em %:'
        end
        object dmkEdParc_ValMin: TdmkEdit
          Left = 176
          Top = 20
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
        end
        object dmkEdParc_QtdMax: TdmkEdit
          Left = 176
          Top = 44
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
        end
        object dmkEdMulta: TdmkEdit
          Left = 176
          Top = 68
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
        end
        object dmkEdJuros: TdmkEdit
          Left = 176
          Top = 92
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
        end
      end
      object EdNomeSindico: TdmkEdit
        Left = 276
        Top = 108
        Width = 585
        Height = 21
        MaxLength = 32
        TabOrder = 8
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object RGPwdSite: TRadioGroup
        Left = 276
        Top = 140
        Width = 260
        Height = 35
        Caption = 'Mostra senha de acesso restrito ao site no boleto: '
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'N'#227'o'
          'Sim')
        TabOrder = 9
      end
      object CkOcultaBloq: TCheckBox
        Left = 276
        Top = 180
        Width = 330
        Height = 17
        Caption = 'Oculta boletos vencidos, avencer e reparcelamentos no site.'
        TabOrder = 10
      end
    end
    object GroupBox4: TGroupBox
      Left = 0
      Top = 378
      Width = 916
      Height = 70
      Align = alBottom
      TabOrder = 1
      object Panel8: TPanel
        Left = 2
        Top = 15
        Width = 912
        Height = 53
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Panel9: TPanel
          Left = 768
          Top = 0
          Width = 144
          Height = 53
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 8
            Top = 3
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 8
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtConfirmaClick
        end
      end
    end
  end
  object PainelUser: TPanel
    Left = 0
    Top = 96
    Width = 916
    Height = 448
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 3
    Visible = False
    object Panel10: TPanel
      Left = 0
      Top = 0
      Width = 916
      Height = 52
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label27: TLabel
        Left = 8
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label33: TLabel
        Left = 84
        Top = 4
        Width = 29
        Height = 13
        Caption = 'Login:'
        FocusControl = DBEdit2
      end
      object Label34: TLabel
        Left = 432
        Top = 4
        Width = 34
        Height = 13
        Caption = 'Senha:'
        FocusControl = DBEdit6
      end
      object Edit001: TdmkEdit
        Left = 8
        Top = 20
        Width = 73
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 2
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdULogin: TdmkEdit
        Left = 84
        Top = 20
        Width = 344
        Height = 21
        MaxLength = 32
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdUSenha: TdmkEdit
        Left = 432
        Top = 20
        Width = 344
        Height = 21
        MaxLength = 32
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
    end
    object GroupBox3: TGroupBox
      Left = 0
      Top = 378
      Width = 916
      Height = 70
      Align = alBottom
      TabOrder = 1
      object Panel11: TPanel
        Left = 2
        Top = 15
        Width = 912
        Height = 53
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Panel12: TPanel
          Left = 768
          Top = 0
          Width = 144
          Height = 53
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BitBtn3: TBitBtn
            Tag = 15
            Left = 8
            Top = 3
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
        object BitBtn1: TBitBtn
          Tag = 14
          Left = 8
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BitBtn1Click
        end
      end
    end
  end
  object PainelDono: TPanel
    Left = 0
    Top = 96
    Width = 916
    Height = 448
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 4
    Visible = False
    object Panel13: TPanel
      Left = 0
      Top = 0
      Width = 916
      Height = 56
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label4: TLabel
        Left = 8
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label5: TLabel
        Left = 84
        Top = 4
        Width = 29
        Height = 13
        Caption = 'Login:'
        FocusControl = DBEdit2
      end
      object Label11: TLabel
        Left = 432
        Top = 4
        Width = 34
        Height = 13
        Caption = 'Senha:'
        FocusControl = DBEdit6
      end
      object Edit002: TdmkEdit
        Left = 8
        Top = 20
        Width = 73
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 2
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdDLogin: TdmkEdit
        Left = 84
        Top = 20
        Width = 344
        Height = 21
        MaxLength = 32
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdDSenha: TdmkEdit
        Left = 432
        Top = 20
        Width = 344
        Height = 21
        MaxLength = 32
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 378
      Width = 916
      Height = 70
      Align = alBottom
      TabOrder = 1
      object Panel15: TPanel
        Left = 2
        Top = 15
        Width = 912
        Height = 53
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 768
          Top = 0
          Width = 144
          Height = 53
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BitBtn5: TBitBtn
            Tag = 15
            Left = 8
            Top = 3
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
        object BitBtn4: TBitBtn
          Tag = 14
          Left = 8
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BitBtn4Click
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 96
    Width = 916
    Height = 448
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    object PainelData: TPanel
      Left = 0
      Top = 0
      Width = 916
      Height = 125
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      object Label2: TLabel
        Left = 116
        Top = 4
        Width = 110
        Height = 13
        Caption = 'Razao social ou Nome:'
        FocusControl = DBEdit2
      end
      object Label6: TLabel
        Left = 476
        Top = 4
        Width = 30
        Height = 13
        Caption = 'CNPJ:'
        FocusControl = DBEdit6
      end
      object Label80: TLabel
        Left = 8
        Top = 4
        Width = 106
        Height = 13
        Caption = 'Entidade/condom'#237'nio:'
        FocusControl = DBEdit81
      end
      object Label1: TLabel
        Left = 592
        Top = 4
        Width = 45
        Height = 13
        Caption = 'Telefone:'
        FocusControl = DBEdit002
      end
      object Label9: TLabel
        Left = 8
        Top = 44
        Width = 89
        Height = 13
        Caption = 'Descri'#231#227'o na web:'
        FocusControl = DBEdit7
      end
      object Label15: TLabel
        Left = 476
        Top = 44
        Width = 92
        Height = 13
        Caption = 'Usu'#225'rio do s'#237'ndico:'
        FocusControl = DBEdit8
      end
      object Label16: TLabel
        Left = 592
        Top = 44
        Width = 87
        Height = 13
        Caption = 'Senha do s'#237'ndico:'
        FocusControl = DBEdit9
      end
      object Label23: TLabel
        Left = 8
        Top = 85
        Width = 118
        Height = 13
        Caption = 'Nome do s'#237'ndico no site:'
        FocusControl = DBEdit12
      end
      object DBEdit2: TDBEdit
        Left = 116
        Top = 20
        Width = 357
        Height = 21
        DataField = 'NCONDOM'
        DataSource = Dswclients
        TabOrder = 2
      end
      object DBEdit6: TDBEdit
        Left = 476
        Top = 20
        Width = 113
        Height = 21
        DataField = 'CNPJ_TXT'
        DataSource = Dswclients
        TabOrder = 3
      end
      object DBEdit81: TDBEdit
        Left = 8
        Top = 20
        Width = 52
        Height = 21
        TabStop = False
        DataField = 'Cliente'
        DataSource = Dswclients
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
      end
      object DBEdit001: TDBEdit
        Left = 60
        Top = 20
        Width = 52
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = Dswclients
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 1
      end
      object DBEdit002: TDBEdit
        Left = 592
        Top = 20
        Width = 113
        Height = 21
        DataField = 'CONDTEL'
        DataSource = Dswclients
        TabOrder = 4
      end
      object DBEdit7: TDBEdit
        Left = 8
        Top = 60
        Width = 465
        Height = 21
        DataField = 'NomeWeb'
        DataSource = Dswclients
        TabOrder = 5
      end
      object DBEdit8: TDBEdit
        Left = 476
        Top = 60
        Width = 113
        Height = 21
        DataField = 'Username'
        DataSource = Dswclients
        TabOrder = 6
      end
      object DBEdit9: TDBEdit
        Left = 592
        Top = 60
        Width = 113
        Height = 21
        DataField = 'Password'
        DataSource = Dswclients
        PasswordChar = '*'
        TabOrder = 7
      end
      object GroupBox2: TGroupBox
        Left = 712
        Top = 9
        Width = 197
        Height = 73
        Caption = ' Parcelamento de bloquetos em atrazo: '
        TabOrder = 10
        object Label19: TLabel
          Left = 4
          Top = 24
          Width = 92
          Height = 13
          Caption = 'Valor m'#237'n. parcela: '
        end
        object Label20: TLabel
          Left = 4
          Top = 48
          Width = 97
          Height = 13
          Caption = 'Qtde m'#225'x. parcelas: '
        end
        object DBEdit10: TDBEdit
          Left = 104
          Top = 20
          Width = 85
          Height = 21
          DataField = 'Parc_ValMin'
          DataSource = Dswclients
          TabOrder = 0
        end
        object DBEdit11: TDBEdit
          Left = 104
          Top = 44
          Width = 85
          Height = 21
          DataField = 'Parc_QtdMax'
          DataSource = Dswclients
          TabOrder = 1
        end
      end
      object DBRadioGroup1: TDBRadioGroup
        Left = 340
        Top = 87
        Width = 255
        Height = 35
        Caption = 'Mostra senha de acesso restrito ao site no boleto: '
        Columns = 2
        DataField = 'PwdSite'
        DataSource = Dswclients
        Items.Strings = (
          'N'#227'o'
          'Sim')
        ParentBackground = True
        TabOrder = 9
        Values.Strings = (
          '0'
          '1')
      end
      object DBCheckBox1: TDBCheckBox
        Left = 604
        Top = 88
        Width = 305
        Height = 17
        Caption = 'Oculta boletos vencidos, avencer e reparcelamentos no site.'
        DataField = 'OcultaBloq'
        DataSource = Dswclients
        TabOrder = 11
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object DBEdit12: TDBEdit
        Left = 8
        Top = 101
        Width = 325
        Height = 21
        DataField = 'NomeSindico'
        DataSource = Dswclients
        TabOrder = 8
      end
    end
    object PageControl1: TPageControl
      Left = 28
      Top = 148
      Width = 765
      Height = 229
      ActivePage = TabSheet2
      TabHeight = 25
      TabOrder = 1
      OnChange = PageControl1Change
      OnChanging = PageControl1Changing
      object TabSheet1: TTabSheet
        Caption = 'Dados dos Usu'#225'rios (Morador, se cadastrado, ou propriet'#225'rio)'
        object dmkDBGUsers: TdmkDBGrid
          Left = 0
          Top = 0
          Width = 757
          Height = 194
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'User_ID'
              Title.Caption = 'User ID'
              Width = 42
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Unidade'
              Title.Caption = 'UH'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'USUARIO'
              Title.Caption = 'C'#243'digo'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'USER_NOME'
              Title.Caption = 'Usu'#225'rio ou dono do im'#243'vel'
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NIVEL'
              Title.Caption = 'N'#237'vel'
              Width = 76
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Username'
              Title.Caption = 'Login'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Password'
              Title.Caption = 'Senha'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'USER_DOCU'
              Title.Caption = 'CNPJ / CPF'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'USER_TEL1'
              Title.Caption = 'Telefone 1'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LastAcess'
              Title.Caption = #218'ltimo acesso'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LoginID'
              Title.Caption = 'Identifica'#231#227'o do '#250'ltimo acesso'
              Visible = True
            end>
          Color = clWindow
          DataSource = DsUsers
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDblClick = dmkDBGUsersDblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'User_ID'
              Title.Caption = 'User ID'
              Width = 42
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Unidade'
              Title.Caption = 'UH'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'USUARIO'
              Title.Caption = 'C'#243'digo'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'USER_NOME'
              Title.Caption = 'Usu'#225'rio ou dono do im'#243'vel'
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NIVEL'
              Title.Caption = 'N'#237'vel'
              Width = 76
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Username'
              Title.Caption = 'Login'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Password'
              Title.Caption = 'Senha'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'USER_DOCU'
              Title.Caption = 'CNPJ / CPF'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'USER_TEL1'
              Title.Caption = 'Telefone 1'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LastAcess'
              Title.Caption = #218'ltimo acesso'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LoginID'
              Title.Caption = 'Identifica'#231#227'o do '#250'ltimo acesso'
              Visible = True
            end>
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Dados do dono do im'#243'vel'
        ImageIndex = 1
        object dmkDBGDonos: TdmkDBGrid
          Left = 0
          Top = 0
          Width = 757
          Height = 194
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'User_ID'
              Title.Caption = 'User ID'
              Width = 42
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Propriet'
              Title.Caption = 'C'#243'digo'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DONO_NOME'
              Title.Caption = 'Usu'#225'rio ou dono do im'#243'vel'
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NIVEL'
              Title.Caption = 'N'#237'vel'
              Width = 76
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Username'
              Title.Caption = 'Login'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Password'
              Title.Caption = 'Senha'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DONO_DOCU'
              Title.Caption = 'CNPJ / CPF'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DONO_TEL1'
              Title.Caption = 'Telefone 1'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LastAcess'
              Title.Caption = #218'ltimo acesso'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LoginID'
              Title.Caption = 'Identifica'#231#227'o do '#250'ltimo acesso'
              Visible = True
            end>
          Color = clWindow
          DataSource = DsDonos
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'User_ID'
              Title.Caption = 'User ID'
              Width = 42
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Propriet'
              Title.Caption = 'C'#243'digo'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DONO_NOME'
              Title.Caption = 'Usu'#225'rio ou dono do im'#243'vel'
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NIVEL'
              Title.Caption = 'N'#237'vel'
              Width = 76
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Username'
              Title.Caption = 'Login'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Password'
              Title.Caption = 'Senha'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DONO_DOCU'
              Title.Caption = 'CNPJ / CPF'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DONO_TEL1'
              Title.Caption = 'Telefone 1'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LastAcess'
              Title.Caption = #218'ltimo acesso'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LoginID'
              Title.Caption = 'Identifica'#231#227'o do '#250'ltimo acesso'
              Visible = True
            end>
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'Acessos do usu'#225'rio selecionado a '#225'rea de clientes'
        ImageIndex = 2
        object DBGrid1: TDBGrid
          Left = 0
          Top = 0
          Width = 757
          Height = 194
          Align = alClient
          DataSource = DsVisitascli
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'Seq.geral'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataHora'
              Title.Caption = 'Data e hora'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IP'
              Visible = True
            end>
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 384
      Width = 916
      Height = 64
      Align = alBottom
      TabOrder = 2
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 219
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 393
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel14: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCliente: TBitBtn
          Tag = 10005
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Cliente'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtClienteClick
        end
        object BtUsuario: TBitBtn
          Tag = 10021
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Usu'#225'rio'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtUsuarioClick
        end
        object BtDono: TBitBtn
          Left = 192
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Grupo'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtDonoClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 916
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 5
    object GB_R: TGroupBox
      Left = 868
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 652
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 171
        Height = 32
        Caption = 'Clientes WEB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 171
        Height = 32
        Caption = 'Clientes WEB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 171
        Height = 32
        Caption = 'Clientes WEB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 916
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 6
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 912
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object Dswclients: TDataSource
    DataSet = Qrwclients
    Left = 460
    Top = 17
  end
  object Qrwclients: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrwclientsAfterOpen
    BeforeClose = QrwclientsBeforeClose
    AfterScroll = QrwclientsAfterScroll
    OnCalcFields = QrwclientsCalcFields
    SQL.Strings = (
      'SELECT con.*, IF(rec.Tipo=0, rec.RazaoSocial, rec.Nome) NCONDOM,'
      'IF(rec.Tipo=0, rec.CNPJ, rec.CPF) CNPJ_CPF,'
      'IF(rec.Tipo=0, rec.ETe1, rec.PTe2) CONDTEL, '
      'wcl.NomeWeb, wcl.Username, wcl.Password, wcl.User_ID,'
      'wcl.Parc_ValMin, wcl.Parc_QtdMax, wcl.NomeSindico,'
      'wcl.Juros, wcl.Multa'
      'FROM cond con'
      'LEFT JOIN entidades rec ON rec.Codigo=con.Cliente'
      'LEFT JOIN wclients  wcl ON wcl.CodCliEsp=con.Codigo'
      'WHERE con.Codigo > 0')
    Left = 432
    Top = 17
    object QrwclientsNCONDOM: TWideStringField
      FieldName = 'NCONDOM'
      Size = 100
    end
    object QrwclientsCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrwclientsCONDTEL: TWideStringField
      FieldName = 'CONDTEL'
    end
    object QrwclientsNomeWeb: TWideStringField
      FieldName = 'NomeWeb'
      Origin = 'wclients.NomeWeb'
      Size = 150
    end
    object QrwclientsUsername: TWideStringField
      FieldName = 'Username'
      Origin = 'wclients.Username'
      Size = 32
    end
    object QrwclientsPassword: TWideStringField
      FieldName = 'Password'
      Origin = 'wclients.Password'
      Size = 32
    end
    object QrwclientsUser_ID: TAutoIncField
      FieldName = 'User_ID'
      Origin = 'wclients.User_ID'
    end
    object QrwclientsParc_ValMin: TFloatField
      FieldName = 'Parc_ValMin'
      DisplayFormat = '#,###,##0.00'
    end
    object QrwclientsParc_QtdMax: TIntegerField
      FieldName = 'Parc_QtdMax'
    end
    object QrwclientsNomeSindico: TWideStringField
      FieldName = 'NomeSindico'
      Size = 100
    end
    object QrwclientsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'cond.Codigo'
      Required = True
    end
    object QrwclientsJuros: TFloatField
      FieldName = 'Juros'
    end
    object QrwclientsMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrwclientsPwdSite: TSmallintField
      FieldName = 'PwdSite'
      Origin = 'cond.PwdSite'
      Required = True
    end
    object QrwclientsCliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'cond.Cliente'
      Required = True
    end
    object QrwclientsCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 40
      Calculated = True
    end
    object QrwclientsOcultaBloq: TIntegerField
      FieldName = 'OcultaBloq'
    end
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrClientesAfterOpen
    BeforeClose = QrClientesBeforeClose
    SQL.Strings = (
      'SELECT Codigo, LimiCred,'
      'CASE WHEN Tipo=0 THEN RazaoSocial ELSE Nome END NOMECLI,'
      'CASE WHEN Tipo=0 THEN CNPJ    ELSE CPF     END CNPJCPF,'
      'CASE WHEN Tipo=0 THEN IE      ELSE RG      END IERG,'
      'CASE WHEN Tipo=0 THEN ERua    ELSE PRua    END Rua,'
      'CASE WHEN Tipo=0 THEN ENumero ELSE PNumero END Numero,'
      'CASE WHEN Tipo=0 THEN ECompl  ELSE PCompl  END Compl,'
      'CASE WHEN Tipo=0 THEN EBairro ELSE PBairro END Bairro,'
      'CASE WHEN Tipo=0 THEN ECidade ELSE PCidade END Cidade,'
      'CASE WHEN Tipo=0 THEN ECEP    ELSE PCEP    END CEP,'
      'CASE WHEN Tipo=0 THEN EUF     ELSE PUF     END UF,'
      'CASE WHEN Tipo=0 THEN ETe1    ELSE PTe1    END Tel1'
      'FROM entidades'
      'WHERE Cliente1 = "V"'
      'AND CASE WHEN Tipo=0 THEN RazaoSocial ELSE Nome END LIKE :P0'
      'ORDER BY NOMECLI')
    Left = 656
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
    end
    object QrClientesNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrClientesCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 18
    end
    object QrClientesIERG: TWideStringField
      FieldName = 'IERG'
    end
    object QrClientesRua: TWideStringField
      FieldName = 'Rua'
      Size = 30
    end
    object QrClientesNumero: TLargeintField
      FieldName = 'Numero'
    end
    object QrClientesCompl: TWideStringField
      FieldName = 'Compl'
      Size = 30
    end
    object QrClientesBairro: TWideStringField
      FieldName = 'Bairro'
      Size = 30
    end
    object QrClientesCidade: TWideStringField
      FieldName = 'Cidade'
      Size = 25
    end
    object QrClientesCEP: TLargeintField
      FieldName = 'CEP'
    end
    object QrClientesUF: TLargeintField
      FieldName = 'UF'
    end
    object QrClientesLimiCred: TFloatField
      FieldName = 'LimiCred'
      Origin = 'entidades.LimiCred'
    end
    object QrClientesTel1: TWideStringField
      FieldName = 'Tel1'
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 684
    Top = 12
  end
  object QrWPesq: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM wclients'
      'WHERE Codigo = :P0')
    Left = 717
    Top = 13
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrWPesqCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object PMUsuario: TPopupMenu
    Left = 552
    Top = 464
    object Alterausurioatual1: TMenuItem
      Caption = '&Altera usu'#225'rio atual'
      OnClick = Alterausurioatual1Click
    end
    object RecriaLoginsesenhas1: TMenuItem
      Caption = 'Recria Logins e senhas'
      object Atual1: TMenuItem
        Caption = '&Atual'
        OnClick = Atual1Click
      end
      object Selecionados1: TMenuItem
        Caption = '&Selecionados'
        OnClick = Selecionados1Click
      end
      object Todos1: TMenuItem
        Caption = '&Todos'
        OnClick = Todos1Click
      end
    end
  end
  object QrUsers: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrUsersCalcFields
    SQL.Strings = (
      'SELECT DISTINCT ent.Codigo,  '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) USER_NOME, '
      'IF(ent.Tipo=1, ent.CNPJ, ent.CPF) USER_DOCU,'
      'IF(ent.Tipo=0, ent.ETe1, ent.PTe2) USER_TEL1, '
      'IF(imv.Usuario>0, imv.Usuario, imv.Propriet) USUARIO,'
      'imv.Propriet, imv.Unidade, imv.Conta APTO, usu.*'
      'FROM condimov imv'
      
        'LEFT JOIN entidades ent ON IF(imv.Usuario>0, imv.Usuario, imv.Pr' +
        'opriet)=ent.Codigo'
      'LEFT JOIN users usu ON usu.CodigoEnt=imv.Propriet '
      '  AND usu.CodigoEsp=imv.Conta '
      '  AND (usu.Tipo=1 OR usu.Tipo IS NULL)'
      'WHERE imv.Codigo=:P0'
      ''
      '')
    Left = 492
    Top = 16
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrUsersCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrUsersUSER_NOME: TWideStringField
      FieldName = 'USER_NOME'
      Size = 100
    end
    object QrUsersUSER_DOCU: TWideStringField
      FieldName = 'USER_DOCU'
      Size = 18
    end
    object QrUsersUSER_TEL1: TWideStringField
      FieldName = 'USER_TEL1'
    end
    object QrUsersUSUARIO: TLargeintField
      FieldName = 'USUARIO'
      Required = True
    end
    object QrUsersUnidade: TWideStringField
      FieldName = 'Unidade'
      Required = True
      Size = 10
    end
    object QrUsersAPTO: TIntegerField
      FieldName = 'APTO'
      Required = True
    end
    object QrUsersUser_ID: TIntegerField
      FieldName = 'User_ID'
    end
    object QrUsersCodCliEsp: TIntegerField
      FieldName = 'CodCliEsp'
    end
    object QrUsersCodCliEnt: TIntegerField
      FieldName = 'CodCliEnt'
    end
    object QrUsersCodigoEnt: TIntegerField
      FieldName = 'CodigoEnt'
    end
    object QrUsersCodigoEsp: TIntegerField
      FieldName = 'CodigoEsp'
    end
    object QrUsersUsername: TWideStringField
      FieldName = 'Username'
      Size = 32
    end
    object QrUsersPassword: TWideStringField
      FieldName = 'Password'
      Size = 32
    end
    object QrUsersLoginID: TWideStringField
      FieldName = 'LoginID'
      Size = 32
    end
    object QrUsersLastAcess: TDateTimeField
      FieldName = 'LastAcess'
    end
    object QrUsersTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrUsersLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrUsersDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrUsersDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrUsersUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrUsersUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrUsersNIVEL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NIVEL'
      Size = 15
      Calculated = True
    end
    object QrUsersPropriet: TIntegerField
      FieldName = 'Propriet'
      Required = True
    end
  end
  object DsUsers: TDataSource
    DataSet = QrUsers
    Left = 520
    Top = 16
  end
  object QrVisitascli: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM visitascli'
      'WHERE Usuario=:P0'
      'ORDER BY DataHora DESC')
    Left = 464
    Top = 316
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrVisitascliControle: TAutoIncField
      FieldName = 'Controle'
      Origin = 'visitascli.Controle'
    end
    object QrVisitascliIP: TWideStringField
      FieldName = 'IP'
      Origin = 'visitascli.IP'
      Size = 64
    end
    object QrVisitascliDataHora: TDateTimeField
      FieldName = 'DataHora'
      Origin = 'visitascli.DataHora'
    end
    object QrVisitascliUsuario: TIntegerField
      FieldName = 'Usuario'
      Origin = 'visitascli.Usuario'
      DisplayFormat = '0000000'
    end
  end
  object DsVisitascli: TDataSource
    DataSet = QrVisitascli
    Left = 492
    Top = 316
  end
  object QrPropriet: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, LimiCred,'
      'CASE WHEN Tipo=0 THEN RazaoSocial ELSE Nome END NOMECLI,'
      'CASE WHEN Tipo=0 THEN CNPJ    ELSE CPF     END CNPJCPF,'
      'CASE WHEN Tipo=0 THEN IE      ELSE RG      END IERG,'
      'CASE WHEN Tipo=0 THEN ERua    ELSE PRua    END Rua,'
      'CASE WHEN Tipo=0 THEN ENumero ELSE PNumero END Numero,'
      'CASE WHEN Tipo=0 THEN ECompl  ELSE PCompl  END Compl,'
      'CASE WHEN Tipo=0 THEN EBairro ELSE PBairro END Bairro,'
      'CASE WHEN Tipo=0 THEN ECidade ELSE PCidade END Cidade,'
      'CASE WHEN Tipo=0 THEN ECEP    ELSE PCEP    END CEP,'
      'CASE WHEN Tipo=0 THEN EUF     ELSE PUF     END UF,'
      'CASE WHEN Tipo=0 THEN ETe1    ELSE PTe1    END Tel1'
      'FROM entidades'
      'WHERE Cliente2 = "V"')
    Left = 656
    Top = 40
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object StringField2: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 18
    end
    object StringField3: TWideStringField
      FieldName = 'IERG'
    end
    object StringField4: TWideStringField
      FieldName = 'Rua'
      Size = 30
    end
    object LargeintField1: TLargeintField
      FieldName = 'Numero'
    end
    object StringField5: TWideStringField
      FieldName = 'Compl'
      Size = 30
    end
    object StringField6: TWideStringField
      FieldName = 'Bairro'
      Size = 30
    end
    object StringField7: TWideStringField
      FieldName = 'Cidade'
      Size = 25
    end
    object LargeintField2: TLargeintField
      FieldName = 'CEP'
    end
    object LargeintField3: TLargeintField
      FieldName = 'UF'
    end
    object FloatField1: TFloatField
      FieldName = 'LimiCred'
      Origin = 'entidades.LimiCred'
    end
    object StringField8: TWideStringField
      FieldName = 'Tel1'
    end
  end
  object QrExiste: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Password '
      'FROM users'
      'WHERE Password=:P0')
    Left = 436
    Top = 316
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrExistePassword: TWideStringField
      FieldName = 'Password'
      Size = 32
    end
  end
  object QrDonos: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrDonosCalcFields
    SQL.Strings = (
      'SELECT COUNT(imv.Codigo) Imoveis,   '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) DONO_NOME, '
      'IF(ent.Tipo=1, ent.CNPJ, ent.CPF) DONO_DOCU,'
      'IF(ent.Tipo=0, ent.ETe1, ent.PTe2) DONO_TEL1, '
      'imv.Propriet, ent.Codigo, usu.*'
      'FROM condimov imv'
      'LEFT JOIN entidades ent ON imv.Propriet=ent.Codigo'
      'LEFT JOIN users usu ON usu.CodigoEnt=imv.Propriet '
      '  AND usu.CodigoEsp=0'
      '  AND (usu.Tipo=3 OR usu.Tipo IS NULL)'
      'WHERE imv.Codigo=:P0'
      'GROUP BY imv.Propriet')
    Left = 552
    Top = 16
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrDonosImoveis: TLargeintField
      FieldName = 'Imoveis'
      Required = True
    end
    object QrDonosDONO_NOME: TWideStringField
      FieldName = 'DONO_NOME'
      Size = 100
    end
    object QrDonosDONO_DOCU: TWideStringField
      FieldName = 'DONO_DOCU'
      Size = 18
    end
    object QrDonosDONO_TEL1: TWideStringField
      FieldName = 'DONO_TEL1'
    end
    object QrDonosPropriet: TIntegerField
      FieldName = 'Propriet'
      Required = True
    end
    object QrDonosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDonosUser_ID: TIntegerField
      FieldName = 'User_ID'
    end
    object QrDonosCodCliEsp: TIntegerField
      FieldName = 'CodCliEsp'
    end
    object QrDonosCodCliEnt: TIntegerField
      FieldName = 'CodCliEnt'
    end
    object QrDonosCodigoEnt: TIntegerField
      FieldName = 'CodigoEnt'
    end
    object QrDonosCodigoEsp: TIntegerField
      FieldName = 'CodigoEsp'
    end
    object QrDonosUsername: TWideStringField
      FieldName = 'Username'
      Size = 32
    end
    object QrDonosPassword: TWideStringField
      FieldName = 'Password'
      Size = 32
    end
    object QrDonosLoginID: TWideStringField
      FieldName = 'LoginID'
      Size = 32
    end
    object QrDonosLastAcess: TDateTimeField
      FieldName = 'LastAcess'
    end
    object QrDonosTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrDonosLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrDonosDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrDonosDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrDonosUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrDonosUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrDonosNIVEL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NIVEL'
      Size = 15
      Calculated = True
    end
  end
  object DsDonos: TDataSource
    DataSet = QrDonos
    Left = 580
    Top = 16
  end
  object PMDono: TPopupMenu
    Left = 644
    Top = 464
    object Alteraloginesenhadodonoatual1: TMenuItem
      Caption = '&Altera login e senha do dono atual'
      OnClick = Alteraloginesenhadodonoatual1Click
    end
    object Recrialoginsesenhas2: TMenuItem
      Caption = '&Recria logins e senhas'
      object Atual2: TMenuItem
        Caption = '&Atual'
        OnClick = Atual2Click
      end
      object Selecionados2: TMenuItem
        Caption = '&Selecionados'
        OnClick = Selecionados2Click
      end
      object Todos2: TMenuItem
        Caption = '&Todos'
        OnClick = Todos2Click
      end
    end
  end
end
