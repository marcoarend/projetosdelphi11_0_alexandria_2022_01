unit FlxMensBlqPer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Variants, dmkPermissoes, UnDmkEnums;

type
  TFmFlxMensBlqPer = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    LBPeriodos: TDBLookupListBox;
    StaticText2: TStaticText;
    QrAnoMes: TmySQLQuery;
    QrAnoMesAnoMes: TIntegerField;
    QrAnoMesAnoEMes: TWideStringField;
    DsAnoMes: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure LBPeriodosClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure LBPeriodosDblClick(Sender: TObject);
  private
    { Private declarations }
    procedure Localizar;
  public
    { Public declarations }
    FLocalizar: Boolean;
    FAnoMes   : Integer;
  end;

  var
  FmFlxMensBlqPer: TFmFlxMensBlqPer;

implementation

uses Module, UnMyObjects;

{$R *.DFM}

procedure TFmFlxMensBlqPer.BtOKClick(Sender: TObject);
begin
  Localizar;
end;

procedure TFmFlxMensBlqPer.BtSaidaClick(Sender: TObject);
begin
  FLocalizar := False;
  Close;
end;

procedure TFmFlxMensBlqPer.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  ImgTipo.SQLType := stPsq;
end;

procedure TFmFlxMensBlqPer.FormCreate(Sender: TObject);
begin
  FLocalizar := False;
  QrAnoMes.Open;
end;

procedure TFmFlxMensBlqPer.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, False, taCenter, 2, 10, 20);
end;

procedure TFmFlxMensBlqPer.LBPeriodosClick(Sender: TObject);
begin
  BtOK.Enabled := LBPeriodos.KeyValue <> NULL;
end;

procedure TFmFlxMensBlqPer.LBPeriodosDblClick(Sender: TObject);
begin
  Localizar;
end;

procedure TFmFlxMensBlqPer.Localizar;
begin
  FLocalizar := True;
  FAnoMes    := QrAnoMesAnoMes.Value;
  //
  Close;
end;

end.
