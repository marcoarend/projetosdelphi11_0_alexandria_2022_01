unit PesqCond;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, ComCtrls, Db, mySQLDbTables, Grids,
  DBGrids, DBCtrls, dmkDBGrid, dmkEdit, dmkDBLookupComboBox, dmkEditCB, Menus,
  Variants, UnInternalConsts, UMySQLModule, dmkGeral, frxClass, frxDBSet,
  UnDmkProcFunc, dmkImage, UnDmkEnums, DmkDAC_PF;

type
  TFmPesqCond = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    QrPropriet: TmySQLQuery;
    QrProprietCodigo: TIntegerField;
    QrProprietNOMEPROP: TWideStringField;
    DsPropriet: TDataSource;
    QrCondImov: TmySQLQuery;
    QrCondImovConta: TIntegerField;
    QrCondImovUnidade: TWideStringField;
    DsCondImov: TDataSource;
    QrCondGri: TmySQLQuery;
    QrCondGriCodigo: TIntegerField;
    QrCondGriNome: TWideStringField;
    DsCondGri: TDataSource;
    QrCondGriImv: TmySQLQuery;
    QrCondGriImvNOMEPROP: TWideStringField;
    QrCondGriImvNOMECOND: TWideStringField;
    QrCondGriImvUNIDADE: TWideStringField;
    QrCondGriImvCodigo: TIntegerField;
    QrCondGriImvControle: TIntegerField;
    QrCondGriImvCond: TIntegerField;
    QrCondGriImvApto: TIntegerField;
    QrCondGriImvPropr: TIntegerField;
    QrCondGriImvLk: TIntegerField;
    QrCondGriImvDataCad: TDateField;
    QrCondGriImvDataAlt: TDateField;
    QrCondGriImvUserCad: TIntegerField;
    QrCondGriImvUserAlt: TIntegerField;
    QrCondGriImvAlterWeb: TSmallintField;
    QrCondGriImvAtivo: TSmallintField;
    DsCondGriImv: TDataSource;
    PageControl2: TPageControl;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    DBGrid1: TDBGrid;
    Panel8: TPanel;
    SpeedButton1: TSpeedButton;
    EdCondGri: TdmkEditCB;
    CBCondGri: TdmkDBLookupComboBox;
    QrPesq1: TmySQLQuery;
    DsPesq1: TDataSource;
    QrCondominos: TmySQLQuery;
    DsCondominos: TDataSource;
    QrCondominosCodigo: TIntegerField;
    QrCondominosNOMECONDOMINO: TWideStringField;
    QrLct: TmySQLQuery;
    DsLct: TDataSource;
    QrPesq1APTO: TIntegerField;
    QrPesq1Unidade: TWideStringField;
    QrPesq1COND: TIntegerField;
    QrPesq1Propriet: TIntegerField;
    QrPesq1NOMEPROP: TWideStringField;
    QrPesq1NOMECOND: TWideStringField;
    QrLctData: TDateField;
    QrLctVencimento: TDateField;
    QrLctDescricao: TWideStringField;
    QrLctCredito: TFloatField;
    QrLctMez: TIntegerField;
    QrLctCliente: TIntegerField;
    QrLctForneceI: TIntegerField;
    QrLctCliInt: TIntegerField;
    QrLctGenero: TIntegerField;
    QrLctControle: TIntegerField;
    QrLctCompensado: TDateField;
    QrBoletos: TmySQLQuery;
    QrBloqs: TmySQLQuery;
    Panel9: TPanel;
    GBEmissao: TGroupBox;
    CkIniDta: TCheckBox;
    TPIniDta: TDateTimePicker;
    CkFimDta: TCheckBox;
    TPFimDta: TDateTimePicker;
    GBVencto: TGroupBox;
    CkIniVct: TCheckBox;
    TPIniVct: TDateTimePicker;
    CkFimVct: TCheckBox;
    TPFimVct: TDateTimePicker;
    GroupBox2: TGroupBox;
    Label6: TLabel;
    CkNaoMensais: TCheckBox;
    dmkEdMezIni: TdmkEdit;
    dmkEdMezFim: TdmkEdit;
    Panel4: TPanel;
    dmkDBGrid2: TdmkDBGrid;
    dmkDBGrid3: TdmkDBGrid;
    DsBloqs: TDataSource;
    QrBloqsPeriodo: TIntegerField;
    QrBloqsPERIODO_TXT: TWideStringField;
    PMTransfere: TPopupMenu;
    TransfereLanamentos1: TMenuItem;
    TransfereBloquetos1: TMenuItem;
    QrLctFatNum: TFloatField;
    QrBloqsBoleto: TFloatField;
    Panel6: TPanel;
    Label2: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    Label1: TLabel;
    EdPropriet: TdmkEditCB;
    CBPropriet: TdmkDBLookupComboBox;
    Label3: TLabel;
    EdCondImov: TdmkEditCB;
    CBCondImov: TdmkDBLookupComboBox;
    EdPesqProp: TEdit;
    Label4: TLabel;
    EdEntDono: TdmkEditCB;
    CBEntDono: TdmkDBLookupComboBox;
    EdPesqApto: TEdit;
    QrPgBloq: TmySQLQuery;
    QrPgBloqCredito: TFloatField;
    QrPgBloqMultaVal: TFloatField;
    QrPgBloqMoraVal: TFloatField;
    QrPgBloqNOMEPROPRIET: TWideStringField;
    QrPgBloqUH: TWideStringField;
    QrPgBloqNOMECONTA: TWideStringField;
    QrPgBloqMez: TIntegerField;
    QrPgBloqVencimento: TDateField;
    QrPgBloqDocumento: TFloatField;
    QrPgBloqORIGINAL: TFloatField;
    QrPgBloqData: TDateField;
    QrPgBloqMES: TWideStringField;
    QrPgBloqDATA_TXT: TWideStringField;
    frxDsPgBloq: TfrxDBDataset;
    QrSuBloq: TmySQLQuery;
    QrSuBloqMultaVal: TFloatField;
    QrSuBloqMoraVal: TFloatField;
    QrSuBloqNOMECONTA: TWideStringField;
    QrSuBloqORIGINAL: TFloatField;
    QrSuBloqKGT: TLargeintField;
    QrSuBloqPAGO: TFloatField;
    frxDsSuBloq: TfrxDBDataset;
    frxBal_A_12A: TfrxReport;
    frxBal_A_12B: TfrxReport;
    Panel5: TPanel;
    StaticText1: TStaticText;
    dmkDBGrid1: TdmkDBGrid;
    PMImprime: TPopupMenu;
    Histricodepagamentos1: TMenuItem;
    Fontetamanho61: TMenuItem;
    Fontetamanho81: TMenuItem;
    GBAvisos1: TGroupBox;
    Panel7: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel10: TPanel;
    PnSaiDesis: TPanel;
    BtPesquisa: TBitBtn;
    BtReabre: TBitBtn;
    BtTransfere: TBitBtn;
    Label5: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    BtSaida: TBitBtn;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    QrBloqsLancto: TIntegerField;
    SpeedButton5: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure EdProprietChange(Sender: TObject);
    procedure EdCondImovChange(Sender: TObject);
    procedure EdProprietKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCondGriChange(Sender: TObject);
    procedure EdPesqPropChange(Sender: TObject);
    procedure EdPesqAptoChange(Sender: TObject);
    procedure CkIniDtaClick(Sender: TObject);
    procedure TPIniDtaChange(Sender: TObject);
    procedure EdCondImovKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBCondImovKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBProprietKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QrLctBeforeClose(DataSet: TDataSet);
    procedure BtTransfereClick(Sender: TObject);
    procedure TPIniDtaExit(Sender: TObject);
    procedure dmkEdMezIniExit(Sender: TObject);
    procedure dmkEdMezFimExit(Sender: TObject);
    procedure QrBloqsCalcFields(DataSet: TDataSet);
    procedure PMTransferePopup(Sender: TObject);
    procedure TransfereLanamentos1Click(Sender: TObject);
    procedure TransfereBloquetos1Click(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure frxBal_A_12AGetValue(const VarName: string; var Value: Variant);
    procedure BtPesquisaClick(Sender: TObject);
    procedure Fontetamanho61Click(Sender: TObject);
    procedure Fontetamanho81Click(Sender: TObject);
    procedure QrPgBloqCalcFields(DataSet: TDataSet);
    procedure SpeedButton5Click(Sender: TObject);
  private
    { Private declarations }
    FTabLctA, FTabAriA, FTabCnsA, FTabPrvA: String;
    procedure ReopenPropriet(Todos: Boolean);
    procedure ReopenCondImov(Todos: Boolean);
    procedure ReopenCondGriImv(Controle: Integer);
    procedure ReopenPesq(Reabrir: Boolean);
    procedure ReopenLct();
    //
    function ParaNaoDefinido(Para: Integer): Boolean;
    procedure ImprimeHistoricoDePagamentos(TamFonte: Integer);
  public
    { Public declarations }
  end;

  var
  FmPesqCond: TFmPesqCond;

implementation

uses Module, UnFinanceiro, UnMyObjects, ModuleGeral;

{$R *.DFM}

procedure TFmPesqCond.BtReabreClick(Sender: TObject);
begin
  ReopenLct;
end;

procedure TFmPesqCond.BtPesquisaClick(Sender: TObject);
begin
  ReopenPesq(True);
end;

procedure TFmPesqCond.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPesqCond.Fontetamanho61Click(Sender: TObject);
begin
  ImprimeHistoricoDePagamentos(6);
end;

procedure TFmPesqCond.Fontetamanho81Click(Sender: TObject);
begin
  ImprimeHistoricoDePagamentos(8);
end;

procedure TFmPesqCond.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmPesqCond.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPesqCond.frxBal_A_12AGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_PERIODO' then
  begin
    Value := dmkPF.PeriodoImp(
      TPIniDta.Date, TPFimDta.Date, TPIniVct.Date, TPFimVct.Date,
      CkIniDta.Checked, CkFimDta.Checked, CkIniVct.Checked, CkFimVct.Checked,
      'Emiss�o: ', 'Vencimento: ') +
      dmkPF.PeriodoImpMezAno(dmkEdMezIni.Text, dmkEdMezFim.Text,
      'Compet�ncia: ', '', '');
  end
  else if VarName = 'VARF_EMPRESA' then
    Value := CBEmpresa.Text
  else if VarName = 'VARF_PAGINAR' then
    Value := True // CkPaginar.Checked
  else if VarName ='VARF_DATA' then Value := Date // TPData.Date
{
  else if AnsiCompareText(VarName, 'VARF_ULTIMODIA') = 0 then Value :=
    FormatDateTime(VAR_FORMATDATE3,
    MLAGeral.UltimoDiaDoPeriodo_Date(FPeriodoAtu))
  else if AnsiCompareText(VarName, 'VARF_PERIODO') = 0 then Value :=
    FormatDateTime(VAR_FORMATDATE6,
    MLAGeral.PrimeiroDiaDoPeriodo_Date(FPeriodoAtu)) + ' at� ' +
    FormatDateTime(VAR_FORMATDATE6,
    MLAGeral.UltimoDiaDoPeriodo_Date(FPeriodoAtu))
  else if AnsiCompareText(VarName, 'MES_ANO ') = 0 then Value :=
    dmkPF.MesEAnoDoPeriodoLongo(FPeriodoAtu)
  else if AnsiCompareText(VarName, 'CIDADE  ') = 0 then Value :=
    QrEmpCIDADE.Value
  else if AnsiCompareText(VarName, 'SIGLA_UF') = 0 then Value :=
    QrEmpNO_UF.Value
  else if AnsiCompareText(VarName, 'DATA_ATU') = 0 then Value :=
    Geral.FDT(Date, 6)
  else if AnsiCompareText(VarName, 'DATA_FIM') = 0 then Value :=
    FormatDateTime(VAR_FORMATDATE6,
    MLAGeral.UltimoDiaDoPeriodo_Date(FPeriodoAtu))
  //
  else if AnsiCompareText(VarName, 'VAR_DATA_PER_ANT') = 0 then Value :=
    FormatDateTime(VAR_FORMATDATE2,
    MLAGeral.UltimoDiaDoPeriodo_Date(FPeriodoAtu-1))
  else if AnsiCompareText(VarName, 'VAR_DATA_PER_FIM') = 0 then Value :=
    FormatDateTime(VAR_FORMATDATE2,
    MLAGeral.UltimoDiaDoPeriodo_Date(FPeriodoAtu))
  else if Copy(VarName, 1, 5) = 'VMES_' then
    Value := Copy(VarName, 6) + ' / ' + CBAno.Text
  else if VarName = 'VARF_ANO1' then
    Value := 'Total ' + CBAno.Text
  else if VarName = 'VARF_ANO0' then
    Value := 'Total ' + FormatFloat('0', Geral.IMV(CBAno.Text) -1)
  else if VarName = 'VARF_MID1' then
    Value := 'M�dia ' + CBAno.Text
  else if VarName = 'VARF_MID0' then
    Value := 'M�dia ' + FormatFloat('0', Geral.IMV(CBAno.Text) -1)
  else if VarName = 'LogoBalanceteExiste' then
    Value := FileExists(QrEntiCfgRel_01LogoPath.Value)
  else if VarName = 'LogoBalancetePath' then
    Value := QrEntiCfgRel_01LogoPath.Value
  else if VarName = 'VARF_RESUMIDO_01' then Value := FResumido_01
  else if VarName = 'VARF_RESUMIDO_02' then Value := FResumido_02
  else if VarName = 'VARF_RESUMIDO_03' then Value := FResumido_03
  else if VarName = 'VARF_RESUMIDO_04' then Value := FResumido_04
  else if VarName = 'VARF_RESUMIDO_05' then Value := FResumido_05
  else if VarName = 'VARF_RESUMIDO_06' then Value := FResumido_06
  else
}
end;

procedure TFmPesqCond.ImprimeHistoricoDePagamentos(TamFonte: Integer);
const
  Titulo = 'Hist�rico de pagamentos de bloquetos de cobran�a';
var
  Empresa, Entidade, Propriet, Apto: Integer;
begin
  Empresa  := EdEmpresa.ValueVariant;
  if Empresa <> 0 then
    Entidade := DModG.QrEmpresasCodigo.Value//QrEntiCondCodEnti.Value
  else begin
    Geral.MensagemBox('Informe o condom�nio!', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  Propriet := EdPropriet.ValueVariant;
  Apto     := EdCondImov.ValueVariant;
  //
  QrPgBloq.Close;
(*
SELECT IF(lan.Compensado<2, 0, IF(car.Tipo=2, 
lan.Credito + lan.PagMul + lan.PagJur, lan.Credito)) Credito, 
IF(lan.Compensado<2, 0, IF(car.Tipo=2, lan.PagMul, lan.MultaVal)) MultaVal,
IF(lan.Compensado<2, 0, IF(car.Tipo=2, lan.PagJur, lan.MoraVal)) MoraVal, 
CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE 
ent.Nome END NOMEPROPRIET, imv.Unidade UH, 
con.Nome NOMECONTA, lan.Mez, lan.Vencimento, 
lan.Documento, lan.Credito ORIGINAL, lan.Compensado Data,
IF(lan.Compensado<2, "", DATE_FORMAT(lan.Compensado, "%d/%m/%Y")) DATA_TXT
FROM VAR LCT lan
LEFT JOIN carteiras car ON car.Codigo=lan.Carteira
LEFT JOIN condimov imv ON imv.Conta=lan.Depto
LEFT JOIN entidades ent ON ent.Codigo=lan.ForneceI
LEFT JOIN contas con ON con.Codigo=lan.Genero
WHERE car.Tipo in (0,2)
AND lan.FatID in (600,601)
AND car.ForneceI=:P0
AND lan.Mez=:P1

ORDER BY UH, lan.Documento, con.OrdemLista, NOMECONTA
*)
  QrPgBloq.SQL.Clear;
  QrPgBloq.SQL.Add('SELECT IF(lan.Compensado<2, 0, IF(car.Tipo=2,');
  QrPgBloq.SQL.Add('lan.Credito + lan.PagMul + lan.PagJur, lan.Credito)) Credito,');
  QrPgBloq.SQL.Add('IF(lan.Compensado<2, 0, IF(car.Tipo=2, lan.PagMul, lan.MultaVal)) MultaVal,');
  QrPgBloq.SQL.Add('IF(lan.Compensado<2, 0, IF(car.Tipo=2, lan.PagJur, lan.MoraVal)) MoraVal,');
  QrPgBloq.SQL.Add('CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE');
  QrPgBloq.SQL.Add('ent.Nome END NOMEPROPRIET, imv.Unidade UH,');
  QrPgBloq.SQL.Add('con.Nome NOMECONTA, lan.Mez, lan.Vencimento,');
  QrPgBloq.SQL.Add('lan.Documento, lan.Credito ORIGINAL, lan.Compensado Data,');
  QrPgBloq.SQL.Add('IF(lan.Compensado<2, "", DATE_FORMAT(lan.Compensado, "%d/%m/%Y")) DATA_TXT');
  QrPgBloq.SQL.Add('FROM ' + FTabLctA + ' lan');
  QrPgBloq.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
  QrPgBloq.SQL.Add('LEFT JOIN condimov imv ON imv.Conta=lan.Depto');
  QrPgBloq.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=lan.ForneceI');
  QrPgBloq.SQL.Add('LEFT JOIN contas con ON con.Codigo=lan.Genero');
  QrPgBloq.SQL.Add('WHERE car.Tipo in (0,2)');
  QrPgBloq.SQL.Add('AND lan.FatID in (600,601)');
  QrPgBloq.SQL.Add('AND car.ForneceI=' + FormatFloat('0', Entidade));

  if Propriet <> 0 then
    QrPgBloq.SQL.Add('AND lan.ForneceI=' + FormatFloat('0', Propriet));

  if Apto <> 0 then
    QrPgBloq.SQL.Add('AND lan.Depto=' + FormatFloat('0', Apto));


  QrPgBloq.SQL.Add(dmkPF.SQL_Mensal_2('AND lan.Mez', dmkEdMezIni.Text,
    dmkEdMezFim.Text, not CkNaoMensais.Checked));

  QrPgBloq.SQL.Add(dmkPF.SQL_Periodo('AND lan.Data ', TPIniDta.Date,
    TPFimDta.Date, CkIniDta.Checked, CkFimDta.Checked));

  QrPgBloq.SQL.Add(dmkPF.SQL_Periodo('AND lan.Vencimento ', TPIniVct.Date,
    TPFimVct.Date, CkIniVct.Checked, CkFimVct.Checked));


  QrPgBloq.SQL.Add('');
  QrPgBloq.SQL.Add('ORDER BY UH, lan.Vencimento, lan.Mez, lan.Documento, con.OrdemLista, NOMECONTA');
  //dmkPF.LeMeuTexto(QrPgBloq.SQL.Text);
  QrPgBloq.Open;

  //

  QrSuBloq.Close;
  QrSuBloq.SQL.Clear;
  QrSuBloq.SQL.Add('SELECT SUM(IF(lan.Compensado<2, 0, IF(car.Tipo=2,');
  QrSuBloq.SQL.Add('lan.Credito + lan.PagMul + lan.PagJur, lan.Credito))) PAGO,');
  QrSuBloq.SQL.Add('SUM(IF(lan.Compensado<2, 0, IF(car.Tipo=2, lan.PagMul, lan.MultaVal))) MultaVal,');
  QrSuBloq.SQL.Add('SUM(IF(lan.Compensado<2, 0, IF(car.Tipo=2, lan.PagJur, lan.MoraVal))) MoraVal,');
  QrSuBloq.SQL.Add('con.Nome NOMECONTA, SUM(lan.Credito) ORIGINAL, 0 KGT');
  QrSuBloq.SQL.Add('FROM ' + FTabLctA + ' lan');
  QrSuBloq.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
  QrSuBloq.SQL.Add('LEFT JOIN condimov imv ON imv.Conta=lan.Depto');
  QrSuBloq.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=lan.ForneceI');
  QrSuBloq.SQL.Add('LEFT JOIN contas con ON con.Codigo=lan.Genero');
  QrSuBloq.SQL.Add('WHERE car.Tipo in (0,2)');
  QrSuBloq.SQL.Add('AND lan.FatID in (600,601)');
  QrSuBloq.SQL.Add('AND car.ForneceI=' + FormatFloat('0', Entidade));

  if Propriet <> 0 then
    QrSuBloq.SQL.Add('AND lan.ForneceI=' + FormatFloat('0', Propriet));

  if Apto <> 0 then
    QrSuBloq.SQL.Add('AND lan.Depto=' + FormatFloat('0', Apto));


  QrSuBloq.SQL.Add(dmkPF.SQL_Mensal_2('AND lan.Mez', dmkEdMezIni.Text,
    dmkEdMezFim.Text, not CkNaoMensais.Checked));

  QrSuBloq.SQL.Add(dmkPF.SQL_Periodo('AND lan.Data ', TPIniDta.Date,
    TPFimDta.Date, CkIniDta.Checked, CkFimDta.Checked));

  QrSuBloq.SQL.Add(dmkPF.SQL_Periodo('AND lan.Vencimento ', TPIniVct.Date,
    TPFimVct.Date, CkIniVct.Checked, CkFimVct.Checked));


  QrSuBloq.SQL.Add('');
  QrSuBloq.SQL.Add('GROUP BY NOMECONTA');
  //dmkPF.LeMeuTexto(QrSuBloq.SQL.Text);
  QrSuBloq.Open;

  //
  case TamFonte of
    6: MyObjects.frxMostra(frxBal_A_12A, Titulo);
    8: MyObjects.frxMostra(frxBal_A_12B, Titulo);
    else Geral.MensagemBox(
    'Tamanho de fonte n�o definida na gera��o do relat�rio:' + Titulo,
    'Aviso', MB_OK+MB_ICONWARNING);
  end;

end;

procedure TFmPesqCond.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  PageControl2.ActivePageIndex := 0;
  QrPropriet.Open;
  QrCondImov.Open;
  QrCondGri.Open;
  QrCondominos.Open;
  //
  TPIniDta.Date := Date;
  TPFimDta.Date := Date;
  //
  TPIniVct.Date := Date;
  TPFimVct.Date := Date;
  //
end;

procedure TFmPesqCond.EdEmpresaChange(Sender: TObject);
var
  CliInt: Integer;
begin
  CliInt := DModG.QrEmpresasFilial.Value; //QrEntiCondCodCond.Value;
  //
  FTabLctA := DModG.NomeTab(TMeuDB, ntLct, False, ttA, CliInt);
  //
  FTabAriA := DModG.NomeTab(TMeuDB, ntAri, False, ttA, CliInt);
  FTabCnsA := DModG.NomeTab(TMeuDB, ntCns, False, ttA, CliInt);
  //FTabPriA := DModG.NomeTab(TMeuDB, ntPri, False, ttA, CliInt);
  FTabPrvA := DModG.NomeTab(TMeuDB, ntPrv, False, ttA, CliInt);
  //
  ReopenPropriet(False);
  ReopenCondImov(False);
  ReopenPesq(False);
end;

procedure TFmPesqCond.EdProprietChange(Sender: TObject);
var
  Codigo: Integer;
begin
  ReopenCondImov(False);
  ReopenPesq(False);
  Codigo := Geral.IMV(EdPropriet.Text);
  Label8.Caption :=
    MLAGeral.EscolhaDe2Str(Codigo > 0, IntToStr(Codigo), 'N�O DEFINIDO');
  ReopenLct();
end;

procedure TFmPesqCond.EdCondImovChange(Sender: TObject);
var
  Codigo: Integer;
begin
  ReopenPesq(False);
  Codigo := Geral.IMV(EdCondImov.Text);
  Label9.Caption :=
    MLAGeral.EscolhaDe2Str(Codigo > 0, IntToStr(Codigo), 'N�O DEFINIDA');
  ReopenLct();
  // Evitar erro
  Screen.Cursor := crDefault;
end;

procedure TFmPesqCond.EdProprietKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then ReopenPropriet(True);
end;

procedure TFmPesqCond.EdCondGriChange(Sender: TObject);
begin
  ReopenCondGriImv(0);
end;

procedure TFmPesqCond.ReopenPropriet(Todos: Boolean);
var
  Cond: Integer;
begin
  EdPropriet.Text := '';
  CBPropriet.KeyValue := NULL;
  //
  Cond := Geral.IMV(EdEmpresa.Text);
  QrPropriet.Close;
  QrPropriet.SQL.Clear;
  QrPropriet.SQL.Add('SELECT DISTINCT ent.Codigo,');
  QrPropriet.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEPROP');
  QrPropriet.SQL.Add('FROM entidades ent');
  QrPropriet.SQL.Add('LEFT JOIN condimov cim ON ent.Codigo=cim.Propriet');
  QrPropriet.SQL.Add('WHERE ent.Cliente2="V"');
  QrPropriet.SQL.Add('');
  if (Cond <> 0) and not Todos then
    QrPropriet.SQL.Add('AND cim.Codigo=' + IntToStr(Cond));
  QrPropriet.SQL.Add('');
  QrPropriet.SQL.Add('ORDER BY NOMEPROP');
  //dmkPF.LeMeuTexto(QrPropriet.SQL.Text);
  QrPropriet.Open;
end;

procedure TFmPesqCond.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmPesqCond.SpeedButton5Click(Sender: TObject);
begin
  DModG.CadastroDeEntidade(EdEntDono.ValueVariant, fmcadEntidade2, fmcadEntidade2);
  //
  if VAR_CADASTRO > 0 then
  begin
    UnDmkDAC_PF.AbreQuery(QrCondominos, Dmod.MyDB);
    //
    UMyMod.SetaCodigoPesquisado(EdEntDono, CBEntDono, QrCondominos, VAR_CADASTRO);
    EdPropriet.SetFocus;
  end;
end;

procedure TFmPesqCond.ReopenCondImov(Todos: Boolean);
var
  Cond, Propriet: Integer;
begin
  EdCondImov.Text := '';
  CBCondImov.KeyValue := NULL;
  //
  Cond := Geral.IMV(EdEmpresa.Text);
  Propriet := Geral.IMV(EdPropriet.Text);
  QrCondImov.Close;
  QrCondImov.SQL.Clear;
  QrCondImov.SQL.Add('SELECT cim.Conta, cim.Unidade');
  QrCondImov.SQL.Add('FROM condimov cim');
  QrCondImov.SQL.Add('WHERE cim.Codigo<>0');
  QrCondImov.SQL.Add('AND cim.Propriet<>0');
  QrCondImov.SQL.Add('');
  if Cond <> 0 then
    QrCondImov.SQL.Add('AND cim.Codigo=' + IntToStr(Cond));
  if Todos = False then
    if Propriet <> 0 then
      QrCondImov.SQL.Add('AND cim.Propriet=' + IntToStr(Propriet));
  QrCondImov.SQL.Add('');
  QrCondImov.SQL.Add('ORDER BY cim.Unidade');
  QrCondImov.Open;
end;

procedure TFmPesqCond.ReopenCondGriImv(Controle: Integer);
begin
  QrCondGriImv.Close;
  QrCondGriImv.Params[0].AsInteger := QrCondGriCodigo.Value;
  QrCondGriImv.Open;
  //
end;

procedure TFmPesqCond.ReopenPesq(Reabrir: Boolean);
var
  CondCod, PropCod, AptoCod: Integer;
  PropTxt, AptoTxt: String;
begin
  QrPesq1.Close;
  if Reabrir then
  begin
    Screen.Cursor := crHourGlass;
    CondCod := Geral.IMV(EdEmpresa.Text);
    PropCod := Geral.IMV(EdPropriet.Text);
    AptoCod := Geral.IMV(EdCondImov.Text);
    //
    PropTxt := EdPesqProp.Text;
    AptoTxt := EdPesqApto.Text;
    //
    QrPesq1.SQL.Clear;
    //
    QrPesq1.SQL.Add('SELECT imv.Conta APTO, imv.Unidade, imv.Codigo COND, imv.Propriet,');
    QrPesq1.SQL.Add('IF(prp.Tipo=0, prp.RazaoSocial, prp.Nome) NOMEPROP,');
    QrPesq1.SQL.Add('IF(con.Tipo=0, con.RazaoSocial, con.Nome) NOMECOND');
    QrPesq1.SQL.Add('FROM entidades prp');
    QrPesq1.SQL.Add('LEFT JOIN condimov  imv ON imv.Propriet=prp.Codigo');
    QrPesq1.SQL.Add('LEFT JOIN cond      cnd ON imv.Codigo=cnd.Codigo');
    QrPesq1.SQL.Add('LEFT JOIN entidades con ON cnd.Cliente=con.Codigo');
    QrPesq1.SQL.Add('WHERE prp.Cliente2="V"');
    //
    if CondCod <> 0 then
      QrPesq1.SQL.Add('AND imv.Codigo=' + IntToStr(CondCod));
    if PropCod <> 0 then
      QrPesq1.SQL.Add('AND imv.Propriet=' + IntToStr(PropCod));
    if AptoCod <> 0 then
      QrPesq1.SQL.Add('AND imv.Conta=' + IntToStr(AptoCod));
    //
    if PropTxt <> '' then
      QrPesq1.SQL.Add('AND (IF(prp.Tipo=0,prp.RazaoSocial,prp.Nome) LIKE "%' + PropTxt+'%")');
    if AptoTxt <> '' then
      QrPesq1.SQL.Add('AND (imv.Unidade LIKE "%' + AptoTxt+'%")');
    QrPesq1.SQL.Add('');
    //dmkPF.LeMeuTexto(QrPesq1.SQL.Text);
    QrPesq1.Open;
    //
    Panel5.Visible   := True;
    Panel4.Visible   := True;
    BtReabre.Enabled := True;
    Screen.Cursor := crDefault;
  end else begin
    Panel5.Visible   := False;
    Panel4.Visible   := False;
    BtReabre.Enabled := False;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPesqCond.EdPesqPropChange(Sender: TObject);
begin
  ReopenPesq(False);
end;

procedure TFmPesqCond.EdPesqAptoChange(Sender: TObject);
begin
  ReopenPesq(False);
end;

procedure TFmPesqCond.ReopenLct();
var
  Apto, Prop, PeriodoI, PeriodoF: Integer;
begin
  QrLct.Close;
  QrBloqs.Close;
  {
  if QrPesq1.State <> dsBrowse then Exit;
  if QrPesq1.RecordCount = 0 then Exit;
  }
  Apto := Geral.IMV(EdCondImov.Text);
  Prop := Geral.IMV(EdPropriet.Text);
  if (Apto = 0) or (Prop=0) then
    Exit;
  Screen.Cursor := crHourGlass;
  //
  if dmkEdMezIni.Text = '' then PeriodoI := 0 else
    PeriodoI := dmkPF.MensalToPeriodo(dmkEdMezIni.Text);
  if dmkEdMezFim.Text = '' then PeriodoF := 0 else
    PeriodoF := dmkPF.MensalToPeriodo(dmkEdMezFim.Text);
  //
(*
SELECT Data, Vencimento, Descricao, Credito, Mez, FatNum,
Cliente, ForneceI, CliInt, Genero, Controle, Compensado
FROM lan ctos lan
WHERE FatID in (600,6001,610)
AND Depto=134
AND ForneceI=277
AND lan.Mez > 0
*)
  QrLct.SQL.Clear;
  QrLct.SQL.Add('SELECT Data, Vencimento, Descricao, Credito, Mez, FatNum,');
  QrLct.SQL.Add('Cliente, ForneceI, CliInt, Genero, Controle, Compensado');
  QrLct.SQL.Add('FROM ' + FTabLctA + ' lan');
  QrLct.SQL.Add('WHERE FatID in (600,601,610)');
  QrLct.SQL.Add('AND Depto=' + IntToStr(Apto));
  QrLct.SQL.Add('AND ForneceI=' + IntToStr(Prop));
  QrLct.SQL.Add('');

  QrLct.SQL.Add(dmkPF.SQL_Mensal_2('AND lan.Mez', dmkEdMezIni.Text,
    dmkEdMezFim.Text, not CkNaoMensais.Checked));

  QrLct.SQL.Add(dmkPF.SQL_Periodo('AND lan.Data ', TPIniDta.Date,
    TPFimDta.Date, CkIniDta.Checked, CkFimDta.Checked));

  QrLct.SQL.Add(dmkPF.SQL_Periodo('AND lan.Vencimento ', TPIniVct.Date,
    TPFimVct.Date, CkIniVct.Checked, CkFimVct.Checked));
  QrLct.Open;

  //

  QrBloqs.Close;
(*
SELECT DISTINCT prv.Periodo, ari.Boleto
FROM arre its ari
LEFT JOIN ? prv ON prv.Codigo=ari.Codigo
WHERE prv.Cond=5
AND ari.Apto=235
AND ari.Propriet=280
AND prv.Periodo BETWEEN 0 AND 2000

UNION

SELECT DISTINCT cni.Periodo, cni.Boleto
FROM cons its  cni
LEFT JOIN condimov  cdi ON cdi.Conta=cni.Apto
LEFT JOIN entidades ent ON ent.Codigo=cni.Propriet
WHERE cni.Cond=5
AND cni.Apto=235
AND cni.Propriet=180
AND cni.Periodo BETWEEN 0 AND 2000

ORDER BY Periodo,  Boleto
*)
  QrBloqs.SQL.Clear;
  QrBloqs.SQL.Add('SELECT DISTINCT prv.Periodo, ari.Boleto, ari.Lancto ');
  QrBloqs.SQL.Add('FROM ' + FTabAriA + ' ari');
  QrBloqs.SQL.Add('LEFT JOIN ' + FTabPrvA + ' prv ON prv.Codigo=ari.Codigo');
  QrBloqs.SQL.Add('WHERE ari.Apto=' + IntToStr(Apto));
  QrBloqs.SQL.Add('AND ari.Propriet=' + IntToStr(Prop));
  //QrBloqs.SQL.Add('AND prv.Periodo BETWEEN 0 AND 2000');
  QrBloqs.SQL.Add(MLAGeral.SQL_Competencia_Periodo('AND prv.Periodo', PeriodoI,
    PeriodoF, dmkEdMezIni.Text <> '', dmkEdMezFim.Text <> ''));

  QrBloqs.SQL.Add('');
  QrBloqs.SQL.Add('UNION');
  QrBloqs.SQL.Add('');
  QrBloqs.SQL.Add('SELECT DISTINCT cni.Periodo, cni.Boleto, cni.Lancto ');
  QrBloqs.SQL.Add('FROM ' + FTabCnsA + ' cni');
  QrBloqs.SQL.Add('LEFT JOIN condimov  cdi ON cdi.Conta=cni.Apto');
  QrBloqs.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=cni.Propriet');
  QrBloqs.SQL.Add('WHERE cni.Apto=' + IntToStr(Apto));
  QrBloqs.SQL.Add('AND cni.Propriet=' + IntToStr(Prop));
  //QrBloqs.SQL.Add('AND cni.Periodo BETWEEN 0 AND 2000');
  QrBloqs.SQL.Add(MLAGeral.SQL_Competencia_Periodo('AND cni.Periodo', PeriodoI,
    PeriodoF, dmkEdMezIni.Text <> '', dmkEdMezFim.Text <> ''));
  QrBloqs.SQL.Add('');
  QrBloqs.SQL.Add('ORDER BY Periodo,  Boleto');
  QrBloqs.Open;
  //
  BtTransfere.Enabled := (QrLct.RecordCount > 0) or
                         (QrBloqs.RecordCount > 0);
  //                       
  Screen.Cursor := crDefault;
end;

procedure TFmPesqCond.CkIniDtaClick(Sender: TObject);
begin
  ReopenLct();
end;

procedure TFmPesqCond.TPIniDtaChange(Sender: TObject);
begin
  ReopenLct();
end;

procedure TFmPesqCond.EdCondImovKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
  ReopenCondImov(True);
end;

procedure TFmPesqCond.CBCondImovKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
   ReopenCondImov(True);
end;

procedure TFmPesqCond.CBProprietKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then ReopenPropriet(True);
end;

procedure TFmPesqCond.QrLctBeforeClose(DataSet: TDataSet);
begin
  BtTransfere.Enabled := False;
end;

procedure TFmPesqCond.QrPgBloqCalcFields(DataSet: TDataSet);
begin
  QrPgBloqMES.Value := dmkPF.MezToMesEAno(QrPgBloqMez.Value);
end;

procedure TFmPesqCond.BtTransfereClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMTransfere, BtTransfere);
end;

procedure TFmPesqCond.TPIniDtaExit(Sender: TObject);
begin
  ReopenLct();
end;

procedure TFmPesqCond.dmkEdMezIniExit(Sender: TObject);
begin
  ReopenLct();
end;

procedure TFmPesqCond.dmkEdMezFimExit(Sender: TObject);
begin
  ReopenLct();
end;

procedure TFmPesqCond.QrBloqsCalcFields(DataSet: TDataSet);
begin
  QrBloqsPERIODO_TXT.Value := dmkPF.PeriodoToMensal(QrBloqsPeriodo.Value);
end;

procedure TFmPesqCond.PMTransferePopup(Sender: TObject);
begin
  TransfereLanamentos1.Enabled := QrLct.RecordCount > 0;
  TransfereBloquetos1.Enabled := QrBloqs.RecordCount > 0;
end;

procedure TFmPesqCond.TransfereLanamentos1Click(Sender: TObject);
var
  Para: Integer;
begin
  Para := Geral.IMV(EdEntDono.Text);
  if ParaNaoDefinido(Para) then Exit;
  if Geral.MensagemBox('Confirma a transfer�ncia dos ' + IntToStr(
  QrLct.RecordCount) + ' lan�amentos pesquisados pertencentes a:' + Chr(13) +
  Chr(10) + CBPropriet.Text + Chr(13) +Chr(10) + 'Para a entidade:' + Chr(13) +
  Chr(10) + CBEntDono.Text + '?', 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION)=
  ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    QrLct.First;
    while not QrLct.Eof do
    begin
      {
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE lan ctos SET ForneceI=:P0, Cliente=:P1');
      Dmod.QrUpd.SQL.Add('WHERE Controle=:P2');
      Dmod.QrUpd.Params[00].AsInteger := Para;
      Dmod.QrUpd.Params[01].AsInteger := Para;
      Dmod.QrUpd.Params[02].AsInteger := QrLctControle.Value;
      Dmod.QrUpd.ExecSQL;
      }
      UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, ['ForneceI', 'Cliente'
      ], ['Controle'], [Para, Para], [QrLctControle.Value], True, '', FTabLctA);
      QrLct.Next;
    end;
    ReopenLct;
  end;
  Screen.Cursor := crDefault;
end;

function TFmPesqCond.ParaNaoDefinido(Para: Integer): Boolean;
begin
  Result := Para = 0;
  if Result then
    Geral.MensagemBox('A entidade receptora dos lan�amentos n�o foi definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
end;

procedure TFmPesqCond.TransfereBloquetos1Click(Sender: TObject);
  procedure TransfereBloquetoAtual(Cond, NovoDono, DonoAntigo, Apto: Integer);
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE ' + FTabAriA + ' ari, ' + FTabPrvA + ' prv');
    Dmod.QrUpd.SQL.Add('SET ari.Propriet=:P0');
    Dmod.QrUpd.SQL.Add('WHERE prv.Codigo=ari.Codigo');
    Dmod.QrUpd.SQL.Add('AND prv.Cond=:P1');
    Dmod.QrUpd.SQL.Add('AND ari.Apto=:P2');
    Dmod.QrUpd.SQL.Add('AND ari.Propriet=:P3');
    Dmod.QrUpd.SQL.Add('AND prv.Periodo=:P4');
    Dmod.QrUpd.SQL.Add('AND ari.Boleto=:P5');
    Dmod.QrUpd.Params[00].AsInteger := NovoDono;
    Dmod.QrUpd.Params[01].AsInteger := Cond;
    Dmod.QrUpd.Params[02].AsInteger := Apto;
    Dmod.QrUpd.Params[03].AsInteger := DonoAntigo;
    Dmod.QrUpd.Params[04].AsInteger := QrBloqsPeriodo.Value;
    Dmod.QrUpd.Params[05].AsFloat   := QrBloqsBoleto.Value;
    Dmod.QrUpd.ExecSQL;
    //

    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE ' + FTabCnsA + ' SET Propriet=:P0 WHERE Periodo=:P1 ');
    Dmod.QrUpd.SQL.Add('AND Boleto=:P2 AND Apto=:P3 AND Propriet=:P4 ');
    Dmod.QrUpd.Params[00].AsInteger := NovoDono;
    Dmod.QrUpd.Params[01].AsInteger := QrBloqsPeriodo.Value;
    Dmod.QrUpd.Params[02].AsFloat   := QrBloqsBoleto.Value;
    Dmod.QrUpd.Params[03].AsInteger := Apto;
    Dmod.QrUpd.Params[04].AsInteger := DonoAntigo;
    Dmod.QrUpd.ExecSQL;
    //
    if QrBloqsLancto.Value <> 0 then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE ' + FTabLctA + ' SET ForneceI=:P0, Cliente=:P1 ');
      Dmod.QrUpd.SQL.Add('WHERE Controle=:P2 ');
      Dmod.QrUpd.Params[0].AsInteger := NovoDono;
      Dmod.QrUpd.Params[1].AsInteger := NovoDono;
      Dmod.QrUpd.Params[2].AsInteger := QrBloqsLancto.Value;
      Dmod.QrUpd.ExecSQL;
    end;
  end;
var
  Para, De, Apto, Cond: Integer;
begin
  Para := Geral.IMV(EdEntDono.Text);
  De   := Geral.IMV(EdPropriet.Text);
  Apto := Geral.IMV(EdCondImov.Text);
  Cond := Geral.IMV(EdEmpresa.Text);
  if ParaNaoDefinido(Para) then Exit;
  //
  QrBloqs.First;
  //
  while not QrBloqs.Eof do
  begin
    TransfereBloquetoAtual(Cond, Para, De, Apto);
    QrBloqs.Next;
  end;
end;

end.

