object FmVisitasCli: TFmVisitasCli
  Left = 339
  Top = 185
  Caption = 'WEB-VISIT-001 :: Controle de usu'#225'rios web'
  ClientHeight = 496
  ClientWidth = 792
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 334
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 792
      Height = 128
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object TCCont: TTabControl
        Left = 0
        Top = 0
        Width = 792
        Height = 128
        Align = alClient
        TabOrder = 0
        Tabs.Strings = (
          'Moradores'
          'S'#237'ndico'
          'Grupo de UHs / Cond'#244'minos')
        TabIndex = 0
        OnChange = TCContChange
        object dmkCkCond: TdmkLabel
          Left = 7
          Top = 28
          Width = 144
          Height = 13
          Caption = 'Condom'#237'nio [F4 mostra todos]:'
          UpdType = utYes
          SQLType = stNil
        end
        object dmkCkPropriet: TdmkLabel
          Left = 7
          Top = 68
          Width = 126
          Height = 13
          Caption = 'Morador [F4 mostra todos]:'
          UpdType = utYes
          SQLType = stNil
        end
        object dmkCKUH: TdmkLabel
          Left = 457
          Top = 68
          Width = 103
          Height = 13
          Caption = 'Unidade habitacional:'
          UpdType = utYes
          SQLType = stNil
        end
        object dmkCBCond: TdmkDBLookupComboBox
          Left = 67
          Top = 44
          Width = 550
          Height = 21
          KeyField = 'Codigo'
          ListField = 'COND'
          ListSource = DsCond
          TabOrder = 0
          OnKeyDown = dmkCBCondKeyDown
          dmkEditCB = dmkEdCBCond
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object dmkEdCBPropriet: TdmkEditCB
          Left = 7
          Top = 84
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          OnChange = dmkEdCBProprietChange
          OnKeyDown = dmkEdCBProprietKeyDown
          DBLookupComboBox = dmkCBPropriet
          IgnoraDBLookupComboBox = False
        end
        object dmkCBPropriet: TdmkDBLookupComboBox
          Left = 67
          Top = 84
          Width = 381
          Height = 21
          KeyField = 'Propriet'
          ListField = 'PROP'
          ListSource = DsPropriet
          TabOrder = 2
          OnKeyDown = dmkCBProprietKeyDown
          dmkEditCB = dmkEdCBPropriet
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object dmkEdCBUH: TdmkEditCB
          Left = 457
          Top = 84
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          OnKeyDown = dmkEdCBUHKeyDown
          DBLookupComboBox = dmkCBUH
          IgnoraDBLookupComboBox = False
        end
        object dmkCBUH: TdmkDBLookupComboBox
          Left = 517
          Top = 84
          Width = 100
          Height = 21
          KeyField = 'Conta'
          ListField = 'Unidade'
          ListSource = DsUHs
          TabOrder = 4
          OnKeyDown = dmkCBUHKeyDown
          dmkEditCB = dmkEdCBUH
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object dmkEdCBCond: TdmkEditCB
          Left = 7
          Top = 44
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          OnChange = dmkEdCBCondChange
          OnKeyDown = dmkEdCBCondKeyDown
          DBLookupComboBox = dmkCBCond
          IgnoraDBLookupComboBox = False
        end
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 128
      Width = 792
      Height = 206
      ActivePage = TabSheet1
      Align = alClient
      MultiLine = True
      TabOrder = 1
      object TabSheet1: TTabSheet
        Caption = #218'ltimo acesso'
        object dmkDBGrid2: TdmkDBGrid
          Left = 0
          Top = 0
          Width = 784
          Height = 178
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'IP'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataHora'
              Title.Caption = 'Data/Hora'
              Width = 130
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ACESSOS'
              Title.Caption = 'Total de acessos'
              Width = 90
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOME'
              Title.Caption = 'Condom'#237'nio'
              Width = 300
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Unidade'
              Title.Caption = 'UH'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOME2'
              Title.Caption = 'Morador'
              Width = 300
              Visible = True
            end>
          Color = clWindow
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'IP'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataHora'
              Title.Caption = 'Data/Hora'
              Width = 130
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ACESSOS'
              Title.Caption = 'Total de acessos'
              Width = 90
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOME'
              Title.Caption = 'Condom'#237'nio'
              Width = 300
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Unidade'
              Title.Caption = 'UH'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOME2'
              Title.Caption = 'Morador'
              Width = 300
              Visible = True
            end>
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Hist'#243'rico'
        ImageIndex = 1
        object dmkDBGrid1: TdmkDBGrid
          Left = 0
          Top = 0
          Width = 784
          Height = 178
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'IP'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataHora'
              Title.Caption = 'Data/Hora'
              Width = 130
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOME'
              Title.Caption = 'Cond'
              Width = 300
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Unidade'
              Title.Caption = 'UH'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOME2'
              Title.Caption = 'Morador'
              Width = 300
              Visible = True
            end>
          Color = clWindow
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'IP'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataHora'
              Title.Caption = 'Data/Hora'
              Width = 130
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOME'
              Title.Caption = 'Cond'
              Width = 300
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Unidade'
              Title.Caption = 'UH'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOME2'
              Title.Caption = 'Morador'
              Width = 300
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 382
    Width = 792
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 788
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 426
    Width = 792
    Height = 70
    Align = alBottom
    TabOrder = 2
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 788
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 644
        Top = 0
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 10
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
          NumGlyphs = 2
        end
      end
      object BtPesq: TBitBtn
        Tag = 22
        Left = 9
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Pesquisa'
        TabOrder = 1
        OnClick = BtPesqClick
        NumGlyphs = 2
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 704
      Top = 0
      Width = 88
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 48
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
      object ImgWEB: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 656
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 309
        Height = 32
        Caption = 'Controle de usu'#225'rios web'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 309
        Height = 32
        Caption = 'Controle de usu'#225'rios web'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 309
        Height = 32
        Caption = 'Controle de usu'#225'rios web'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object QrVisitasCliSin: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT cli.Controle, cli.IP, cli.DataHora,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOME,'
      'cli.Tipo'
      'FROM visitascli cli'
      'LEFT JOIN wclients wcl ON wcl.User_ID = cli.Usuario'
      'LEFT JOIN entidades ent ON ent.Codigo = wcl.CodCliEnt'
      'WHERE cli.Tipo=:P0'
      'AND cli.Usuario=:P1'
      'ORDER BY NOME')
    Left = 76
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrVisitasCliSinControle: TAutoIncField
      FieldName = 'Controle'
    end
    object QrVisitasCliSinIP: TWideStringField
      FieldName = 'IP'
      Size = 64
    end
    object QrVisitasCliSinDataHora: TDateTimeField
      FieldName = 'DataHora'
      Required = True
    end
    object QrVisitasCliSinNOME: TWideStringField
      FieldName = 'NOME'
      Size = 100
    end
    object QrVisitasCliSinTipo: TIntegerField
      FieldName = 'Tipo'
      Required = True
    end
  end
  object DsVisitasCliSin: TDataSource
    DataSet = QrVisitasCliSin
    Left = 104
    Top = 9
  end
  object QrUHs: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT imv.Conta, imv.Unidade, usu.User_ID'
      'FROM condimov imv'
      'LEFT JOIN users usu ON usu.CodigoEnt=imv.Propriet '
      '  AND usu.CodigoEsp=imv.Conta '
      '  AND (usu.Tipo=1 OR usu.Tipo IS NULL)'
      'WHERE imv.Codigo=:P0')
    Left = 668
    Top = 11
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrUHsUnidade: TWideStringField
      FieldName = 'Unidade'
      Required = True
      Size = 10
    end
    object QrUHsConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrUHsUser_ID: TAutoIncField
      FieldName = 'User_ID'
    end
  end
  object DsUHs: TDataSource
    DataSet = QrUHs
    Left = 696
    Top = 11
  end
  object QrPropriet: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT imv.Propriet, usu.User_ID,  '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) PROP'
      'FROM condimov imv'
      'LEFT JOIN entidades ent ON ent.Codigo = imv.Propriet'
      'LEFT JOIN users usu ON usu.CodigoEnt=imv.Propriet '
      '  AND usu.CodigoEsp=imv.Conta '
      '  AND (usu.Tipo=1 OR usu.Tipo IS NULL)'
      'WHERE imv.Codigo=:P0'
      'ORDER BY PROP')
    Left = 612
    Top = 11
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object a: TWideStringField
      FieldName = 'PROP'
      Origin = 'PROP'
      Size = 100
    end
    object QrProprietPropriet: TIntegerField
      FieldName = 'Propriet'
      Origin = 'condimov.Propriet'
      Required = True
    end
    object QrProprietUser_ID: TIntegerField
      FieldName = 'User_ID'
    end
  end
  object DsPropriet: TDataSource
    DataSet = QrPropriet
    Left = 640
    Top = 11
  end
  object QrCond: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT con.Codigo, wcl.User_ID, '
      'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) COND'
      'FROM cond con'
      'LEFT JOIN wclients wcl ON con.Codigo = wcl.CodCliEsp'
      'LEFT JOIN entidades ent ON ent.Codigo = con.Cliente'
      'WHERE con.Ativo=1'
      'AND con.Codigo = wcl.CodCliEsp')
    Left = 556
    Top = 11
    object QrCondCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'cond.Codigo'
    end
    object QrCondCOND: TWideStringField
      FieldName = 'COND'
      Size = 100
    end
    object QrCondUser_ID: TAutoIncField
      FieldName = 'User_ID'
    end
  end
  object DsCond: TDataSource
    DataSet = QrCond
    Left = 584
    Top = 11
  end
  object QrVisitasCliUsu: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT cli.Controle, cli.IP, cli.DataHora,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOME,'
      'IF(eni.Tipo=0, eni.RazaoSocial, eni.Nome) NOME2,'
      'cli.Tipo, cli.Usuario'
      'FROM visitascli cli'
      'LEFT JOIN users usu ON usu.User_ID = cli.Usuario'
      'LEFT JOIN entidades ent ON ent.Codigo = usu.CodCliEnt'
      'LEFT JOIN users usr ON usr.User_ID = cli.Usuario'
      'LEFT JOIN entidades eni ON eni.Codigo = usu.CodigoEnt'
      'WHERE cli.Controle > 0'
      'AND cli.Tipo=1'
      'ORDER BY NOME, NOME2, DataHora DESC')
    Left = 20
    Top = 9
    object QrVisitasCliUsuControle: TAutoIncField
      FieldName = 'Controle'
    end
    object QrVisitasCliUsuIP: TWideStringField
      FieldName = 'IP'
      Size = 64
    end
    object QrVisitasCliUsuDataHora: TDateTimeField
      FieldName = 'DataHora'
      Required = True
    end
    object QrVisitasCliUsuNOME: TWideStringField
      FieldName = 'NOME'
      Size = 100
    end
    object QrVisitasCliUsuNOME2: TWideStringField
      FieldName = 'NOME2'
      Size = 100
    end
    object QrVisitasCliUsuTipo: TIntegerField
      FieldName = 'Tipo'
      Required = True
    end
    object QrVisitasCliUsuUsuario: TIntegerField
      FieldName = 'Usuario'
      Required = True
    end
    object QrVisitasCliUsuUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
  end
  object DsVisitasCliUsu: TDataSource
    DataSet = QrVisitasCliUsu
    Left = 48
    Top = 9
  end
  object QrVisitasCliDono: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT cli.Controle, cli.IP, cli.DataHora,'
      'cli.Tipo, gri.Nome NOME'
      'FROM visitascli cli'
      'LEFT JOIN condgri gri ON gri.Codigo = cli.Usuario'
      'WHERE cli.Tipo=:P0'
      'AND cli.Usuario=:P1'
      'ORDER BY cli.Controle, DataHora DESC')
    Left = 132
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrVisitasCliDonoControle: TAutoIncField
      FieldName = 'Controle'
    end
    object QrVisitasCliDonoIP: TWideStringField
      FieldName = 'IP'
      Size = 64
    end
    object QrVisitasCliDonoDataHora: TDateTimeField
      FieldName = 'DataHora'
      Required = True
    end
    object QrVisitasCliDonoTipo: TIntegerField
      FieldName = 'Tipo'
      Required = True
    end
    object QrVisitasCliDonoNOME: TWideStringField
      FieldName = 'NOME'
      Required = True
      Size = 100
    end
  end
  object DsVisitasCliDono: TDataSource
    DataSet = QrVisitasCliDono
    Left = 160
    Top = 9
  end
  object QrVisitasTotUsu: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT cli.Controle, cli.IP, cli.DataHora,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOME,'
      'IF(eni.Tipo=0, eni.RazaoSocial, eni.Nome) NOME2,'
      'COUNT(cli.Controle) ACESSOS'
      'FROM visitascli cli'
      'LEFT JOIN users usu ON usu.User_ID = cli.Usuario'
      'LEFT JOIN entidades ent ON ent.Codigo = usu.CodCliEnt'
      'LEFT JOIN entidades eni ON eni.Codigo = usu.CodigoEnt'
      'AND cli.Tipo=1'
      'GROUP BY NOME2, NOME'
      'ORDER BY NOME, NOME2, DataHora DESC')
    Left = 236
    Top = 9
    object QrVisitasTotUsuControle: TAutoIncField
      FieldName = 'Controle'
    end
    object QrVisitasTotUsuIP: TWideStringField
      FieldName = 'IP'
      Size = 64
    end
    object QrVisitasTotUsuDataHora: TDateTimeField
      FieldName = 'DataHora'
      Required = True
    end
    object QrVisitasTotUsuNOME: TWideStringField
      FieldName = 'NOME'
      Size = 100
    end
    object QrVisitasTotUsuNOME2: TWideStringField
      FieldName = 'NOME2'
      Size = 100
    end
    object QrVisitasTotUsuACESSOS: TLargeintField
      FieldName = 'ACESSOS'
      Required = True
    end
    object QrVisitasTotUsuUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
  end
  object DsVisitasTotUsu: TDataSource
    DataSet = QrVisitasTotUsu
    Left = 264
    Top = 9
  end
  object QrVisitasTotSin: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT cli.Controle, cli.IP, cli.DataHora,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOME,'
      'COUNT(cli.Controle) ACESSOS'
      'FROM visitascli cli'
      'LEFT JOIN wclients wcl ON wcl.User_ID = cli.Usuario'
      'LEFT JOIN entidades ent ON ent.Codigo = wcl.CodCliEnt'
      'WHERE cli.Controle > 0'
      'AND cli.Tipo=7'
      'GROUP BY NOME'
      'ORDER BY NOME, DataHora DESC')
    Left = 292
    Top = 9
    object QrVisitasTotSinControle: TAutoIncField
      FieldName = 'Controle'
    end
    object QrVisitasTotSinIP: TWideStringField
      FieldName = 'IP'
      Size = 64
    end
    object QrVisitasTotSinDataHora: TDateTimeField
      FieldName = 'DataHora'
      Required = True
    end
    object QrVisitasTotSinNOME: TWideStringField
      FieldName = 'NOME'
      Size = 100
    end
    object QrVisitasTotSinACESSOS: TLargeintField
      FieldName = 'ACESSOS'
      Required = True
    end
  end
  object DsVisitasTotSin: TDataSource
    DataSet = QrVisitasTotSin
    Left = 320
    Top = 9
  end
  object QrVisitasTotDono: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT cli.Controle, cli.IP,'
      'cli.DataHora, gri.Nome NOME, COUNT(cli.Controle) ACESSOS'
      'FROM visitascli cli'
      'LEFT JOIN condgri gri ON gri.Codigo = cli.Usuario'
      'WHERE cli.Controle > 0'
      'AND cli.Tipo=3'
      'GROUP BY gri.Nome'
      'ORDER BY gri.Nome')
    Left = 348
    Top = 9
    object QrVisitasTotDonoControle: TAutoIncField
      FieldName = 'Controle'
    end
    object QrVisitasTotDonoIP: TWideStringField
      FieldName = 'IP'
      Size = 64
    end
    object QrVisitasTotDonoDataHora: TDateTimeField
      FieldName = 'DataHora'
      Required = True
    end
    object QrVisitasTotDonoNOME: TWideStringField
      FieldName = 'NOME'
      Required = True
      Size = 100
    end
    object QrVisitasTotDonoACESSOS: TLargeintField
      FieldName = 'ACESSOS'
      Required = True
    end
  end
  object DsVisitasTotDono: TDataSource
    DataSet = QrVisitasTotDono
    Left = 376
    Top = 9
  end
end
