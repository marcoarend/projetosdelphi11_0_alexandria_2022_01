unit CertidaoNeg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Variants, DB, mySQLDbTables, Grids,
  DBGrids, dmkDBGrid, DBCtrls, dmkEdit, dmkEditCB, dmkDBLookupComboBox, ComCtrls,
  dmkEditDateTimePicker, frxClass, frxDBSet, dmkGeral, UnInternalConsts,
  UnDmkProcFunc, dmkImage, UnDmkEnums;

type
  TFmCertidaoNeg = class(TForm)
    Panel1: TPanel;
    QrInad: TmySQLQuery;
    DsInad: TDataSource;
    QrCondImov: TmySQLQuery;
    QrCondImovConta: TIntegerField;
    QrCondImovUnidade: TWideStringField;
    DsCondImov: TDataSource;
    DsPropriet: TDataSource;
    QrPropriet: TmySQLQuery;
    QrProprietCodigo: TIntegerField;
    QrProprietNOMEPROP: TWideStringField;
    DsEntiCond: TDataSource;
    QrEntiCond: TmySQLQuery;
    QrEntiCondCodCond: TIntegerField;
    QrEntiCondCodEnti: TIntegerField;
    QrEntiCondNOMECOND: TWideStringField;
    Panel3: TPanel;
    Label2: TLabel;
    EdEmpresa: TdmkEditCB;
    Label1: TLabel;
    EdPropriet: TdmkEditCB;
    Label3: TLabel;
    EdCondImov: TdmkEditCB;
    dmkDBGrid1: TdmkDBGrid;
    CBEmpresa: TdmkDBLookupComboBox;
    CBPropriet: TdmkDBLookupComboBox;
    CBCondImov: TdmkDBLookupComboBox;
    TPDataPesq: TdmkEditDateTimePicker;
    Label4: TLabel;
    QrInadMES: TWideStringField;
    QrInadData: TDateField;
    QrInadFatNum: TFloatField;
    QrInadVencimento: TDateField;
    QrInadMez: TIntegerField;
    QrInadDescricao: TWideStringField;
    frxDsCartas: TfrxDBDataset;
    frxCNDC: TfrxReport;
    Label5: TLabel;
    EdCarta: TdmkEditCB;
    CBCarta: TdmkDBLookupComboBox;
    QrCondImovNOMEUSU: TWideStringField;
    QrCondImovDOC_USU: TWideStringField;
    QrCondImovNOMEPRP: TWideStringField;
    QrCondImovDOC_PRP: TWideStringField;
    QrCondImovCodigo: TIntegerField;
    QrCondImovCliente: TIntegerField;
    QrCondImovNOMECND: TWideStringField;
    frxCDCU: TfrxReport;
    QrCartas: TmySQLQuery;
    DsCartas: TDataSource;
    QrCartasCodigo: TIntegerField;
    QrCartasTitulo: TWideStringField;
    QrCartasTexto: TWideMemoField;
    QrCartasTipo: TIntegerField;
    frxDsInad: TfrxDBDataset;
    dmkDBGrid2: TdmkDBGrid;
    Splitter1: TSplitter;
    QrUnidades: TmySQLQuery;
    QrUnidadesVALOR: TFloatField;
    QrUnidadesNOME_CON: TWideStringField;
    QrUnidadesNOME_PRP: TWideStringField;
    QrUnidadesUnidade: TWideStringField;
    DsUnidades: TDataSource;
    QrUnidadesDepto: TIntegerField;
    RGModo: TRadioGroup;
    frxDsUnidades: TfrxDBDataset;
    QrTexto: TmySQLQuery;
    QrTextoCodigo: TIntegerField;
    QrTextoTitulo: TWideStringField;
    QrTextoTexto: TWideMemoField;
    QrTextoTipo: TIntegerField;
    QrUnidadesDOC_PRP: TWideStringField;
    QrUnidadesNOMEUSU: TWideStringField;
    QrUnidadesDOC_USU: TWideStringField;
    QrInadCREDITO: TFloatField;
    QrInadPAGO: TFloatField;
    QrInadJuros: TFloatField;
    QrInadMulta: TFloatField;
    QrInadTOTAL: TFloatField;
    QrInadSALDO: TFloatField;
    QrInadPEND_VAL: TFloatField;
    QrInadVALOR: TFloatField;
    QrInadATZ_VAL: TFloatField;
    QrUsers: TmySQLQuery;
    QrUsersCodigoEnt: TIntegerField;
    QrUsersCodigoEsp: TIntegerField;
    QrUsersUsername: TWideStringField;
    QrUsersPassword: TWideStringField;
    QrUnidadesConta: TIntegerField;
    QrUnidadesCOD_PRP: TIntegerField;
    QrUnidadesCOD_CON: TIntegerField;
    QrUnidadesCEP_PRP: TFloatField;
    QrUnidadesUF_PRP: TFloatField;
    QrUnidadesMUNI_PRP: TWideStringField;
    QrUnidadesCliInt: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtPesquisa: TBitBtn;
    Label6: TLabel;
    EdUnidades: TdmkEdit;
    Panel5: TPanel;
    RGPesquisa: TRadioGroup;
    RGImpressao: TRadioGroup;
    BtImprime: TBitBtn;
    CkDesign: TCheckBox;
    CkMultaJur: TCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure EdProprietChange(Sender: TObject);
    procedure EdProprietKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBProprietKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBCondImovKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCondImovChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure frxCNDCGetValue(const VarName: string; var Value: Variant);
    procedure QrUnidadesAfterScroll(DataSet: TDataSet);
    procedure BtPesquisaClick(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure QrUnidadesBeforeClose(DataSet: TDataSet);
    procedure TPDataPesqChange(Sender: TObject);
    procedure RGModoClick(Sender: TObject);
    procedure QrUnidadesAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
    procedure ReopenPropriet(Todos: Boolean);
    procedure ReopenCondImov();
    procedure DesfazPesquisa();
    procedure ImprimeDaqui();
    procedure ImprimeCartasImp();
  public
    { Public declarations }
    FTabLctA: String;
    //FEmImpressao: Boolean;
  end;

  var
  FmCertidaoNeg: TFmCertidaoNeg;

implementation

uses Module, ModuleGeral, UnMyObjects, UnGOTOy, DmkDAC_PF, ModuleCond,
UnFinanceiro, CartasImp;

{$R *.DFM}

procedure TFmCertidaoNeg.BtImprimeClick(Sender: TObject);
begin
  case RGImpressao.ItemIndex of
    0: ImprimeDaqui();
    1: ImprimeCartasImp();
  end;
end;

procedure TFmCertidaoNeg.BtPesquisaClick(Sender: TObject);
var
  Entidade, EntiCond, Propriet, Apto: Integer;
  Vencto: String;
begin
  //Carta    := Geral.IMV(EdCarta.Text);
  EntiCond := Geral.IMV(EdEmpresa.Text);
  Entidade := QrEntiCondCodEnti.Value;
  Propriet := Geral.IMV(EdPropriet.Text);
  Apto     := Geral.IMV(EdCondImov.Text);
  Vencto   := Geral.FDT(TPDataPesq.Date, 1);
  //
  QrUnidades.Close;
  QrUnidades.SQL.Clear;
  QrUnidades.SQL.Add('SELECT SUM(IF(lan.Sit<2,lan.Credito-lan.Pago,0)) VALOR,');
  QrUnidades.SQL.Add('IF(con.Tipo=0,con.RazaoSocial, con.Nome) NOME_CON,');
  QrUnidades.SQL.Add('con.Codigo COD_CON,');
  QrUnidades.SQL.Add('IF(prp.Tipo=0, prp.RazaoSocial, prp.Nome) NOME_PRP,');
  QrUnidades.SQL.Add('prp.Codigo COD_PRP, IF(prp.Tipo=0, prp.ECEP, prp.PCEP) + 0.000 CEP_PRP,');
  QrUnidades.SQL.Add('IF(prp.Tipo=0, prp.CNPJ, prp.CPF) DOC_PRP,');
  QrUnidades.SQL.Add('IF(prp.Tipo=0, prp.EUF, prp.PUF) + 0.000 UF_PRP,');
  QrUnidades.SQL.Add('IF(LENGTH(IF(prp.Tipo=0, prp.ECidade, prp.PCidade))=0,');
  QrUnidades.SQL.Add('IF(prp.Tipo=0, dme.Nome, dmp.Nome),');
  QrUnidades.SQL.Add('IF(prp.Tipo=0, prp.ECidade, prp.PCidade)) MUNI_PRP,');
  QrUnidades.SQL.Add('IF(usu.Tipo=0, usu.RazaoSocial, usu.Nome) NOMEUSU,');
  QrUnidades.SQL.Add('IF(usu.Tipo=0, usu.CNPJ, usu.CPF) DOC_USU,');
  QrUnidades.SQL.Add('imv.Unidade, imv.Conta, lan.Depto, lan.CliInt');
  QrUnidades.SQL.Add('');
  QrUnidades.SQL.Add('FROM ' + FTabLctA + ' lan');
  QrUnidades.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
  QrUnidades.SQL.Add('LEFT JOIN condimov  imv ON imv.Conta=lan.Depto');
  QrUnidades.SQL.Add('LEFT JOIN entidades con ON con.Codigo=lan.CliInt');
  QrUnidades.SQL.Add('LEFT JOIN entidades prp ON prp.Codigo=lan.ForneceI');
  QrUnidades.SQL.Add('LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici dme ON dme.Codigo=prp.ECodMunici');
  QrUnidades.SQL.Add('LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici dmp ON dmp.Codigo=prp.PCodMunici');
  QrUnidades.SQL.Add('LEFT JOIN entidades usu ON usu.Codigo=imv.Usuario');
  QrUnidades.SQL.Add('LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mue ON mue.Codigo=prp.ECodMunici');
  QrUnidades.SQL.Add('LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mup ON mup.Codigo=prp.PCodMunici');
  QrUnidades.SQL.Add('WHERE car.Tipo=2');
  QrUnidades.SQL.Add('AND lan.FatID in(600,601,610)');
  QrUnidades.SQL.Add('AND lan.FatNum > 0');
  case RGModo.ItemIndex of
    CO_INADIMPL_APENAS_COM_PENDENCIAS (*1*):
    begin
      // Reparcelados n�o s�o d�bitos
      QrUnidades.SQL.Add('AND lan.Reparcel=0');
      QrUnidades.SQL.Add('AND lan.Sit < 2');
      QrUnidades.SQL.Add('AND lan.Credito > lan.Pago');
      //
      QrUnidades.Filtered := False;
    end;
    CO_INADIMPL_APENAS_SEM_PENDENCIAS(*2*):
    begin
      // Errado!! Usar filtro
      //QrUnidades.SQL.Add('AND lan.Sit = 2');
      QrUnidades.Filtered := True;
    end;
    CO_INADIMPL_COM_E_SEM_PENDENCIAS(*3*):
    begin
      //qualquer coisa
      QrUnidades.Filtered := False;
    end;
    else Geral.MensagemBox('"Tipo de pesquisa" n�o defenido!', 'Erro',
    MB_OK+MB_ICONERROR);
  end;
  QrUnidades.SQL.Add('AND Vencimento < "' + Vencto + '"');
  if EntiCond <> 0 then
    QrUnidades.SQL.Add('AND lan.CliInt=' + dmkPF.FFP(Entidade, 0));
  if Propriet <> 0 then
    QrUnidades.SQL.Add('AND lan.ForneceI=' + dmkPF.FFP(Propriet, 0));
  if Apto <> 0 then
    QrUnidades.SQL.Add('AND lan.Depto=' + dmkPF.FFP(Apto, 0));
  QrUnidades.SQL.Add('GROUP BY lan.Depto');
  //
  UnDmkDAC_PF.AbreQuery(QrUnidades, Dmod.MyDB);
end;

procedure TFmCertidaoNeg.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCertidaoNeg.CBCondImovKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = VK_DELETE then
  begin
    EdCondImov.Text := '';
    CBCondImov.KeyValue := Null;
  end;
end;

procedure TFmCertidaoNeg.CBProprietKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then ReopenPropriet(True);
end;

procedure TFmCertidaoNeg.DesfazPesquisa;
begin
  QrUnidades.Close;
end;

procedure TFmCertidaoNeg.EdCondImovChange(Sender: TObject);
begin
  DesfazPesquisa();
end;

procedure TFmCertidaoNeg.EdEmpresaChange(Sender: TObject);
begin
  FTabLctA  := DModG.NomeTab(TMeuDB, ntLct, False, ttA, QrEntiCondCodCond.Value);
  DesfazPesquisa();
  ReopenPropriet(False);
  ReopenCondImov();
end;

procedure TFmCertidaoNeg.EdProprietChange(Sender: TObject);
begin
  ReopenCondImov;
  DesfazPesquisa();
end;

procedure TFmCertidaoNeg.EdProprietKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then ReopenPropriet(True);
end;

procedure TFmCertidaoNeg.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCertidaoNeg.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  //
  CkMultaJur.Visible := False;
  CkMultaJur.Checked := False;
  //
  //FEmImpressao := False;
  UnDmkDAC_PF.AbreQuery(QrCartas, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrEnticond, Dmod.MyDB);
  TPDataPesq.Date := Date;
  //
  DModG.ReopenOpcoesGerl;
end;

procedure TFmCertidaoNeg.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCertidaoNeg.frxCNDCGetValue(const VarName: string;
  var Value: Variant);
const
  L = #13#10;
var
  EndParc, Txt_Multa, Txt_Juros, Txt_ATZ_VAL, Txt_Tot_Multa, Txt_Tot_Juros,
  Txt_Tot_ATZ_VAL: String;
  Val, Mul, Jur, Atz, Pag, Pen: Double;
begin
  if VarName = 'CONDOMIN' then
    Value := QrUnidadesNOME_CON.Value
  else
  if VarName = 'VAR_MYURL ' then
    Value := DModG.QrOpcoesGerl.FieldByName('Web_MyURL').AsString
  else
  if VarName = 'UNIDADE ' then
    Value := QrUnidadesUnidade.Value
  else
  if VarName = 'DATA_NEG' then
    Value := Geral.FDT(TPDataPesq.Date, 6)
  else
  if VarName = 'ADMINIST' then
    Value := Dmod.QrDonoNOMEDONO.Value
  else
  if VarName = 'PROPNOME' then
    Value := QrUnidadesNOME_PRP.Value
  else
  if VarName = 'COND_END' then
    Value := GOTOy.EnderecoDeEntidade1(QrUnidadesCOD_CON.Value, 0)
  else
  if VarName = 'PROP_END' then
    Value := GOTOy.EnderecoDeEntidade1(QrUnidadesCOD_PRP.Value, 0)
  else
  if VarName = 'PROP_ENDP' then
  begin
    if DModG.ReopenEndereco(QrUnidadesCOD_PRP.Value) then
    begin
      EndParc := DModG.QrEnderecoNOMELOGRAD.Value;
      if Trim(EndParc) <> '' then
        EndParc := EndParc + ' ';
      EndParc := EndParc + DModG.QrEnderecoRUA.Value;
      if Trim(DModG.QrEnderecoRUA.Value) <> '' then
        EndParc := EndParc + ', ' + DModG.QrEnderecoNUMERO_TXT.Value;
      if Trim(DModG.QrEnderecoCOMPL.Value) <> '' then
        EndParc := EndParc + ' ' + DModG.QrEnderecoCOMPL.Value;
      if Trim(DModG.QrEnderecoBAIRRO.Value) <> '' then
        EndParc := EndParc + ' - ' + DModG.QrEnderecoBAIRRO.Value;
      //
      Value := EndParc;
    end;
  end
  else
  if VarName = 'PROP_CEP' then
    Value := Geral.FormataCEP_TT(FormatFloat('0', QrUnidadesCEP_PRP.Value))
  else
  if VarName = 'PROP_UF' then
    Value := DModG.ObtemUFX(Trunc(QrUnidadesUF_PRP.Value))
  else
  if VarName = 'PROP_MUN' then
    Value := QrUnidadesMUNI_PRP.Value
  else
  if VarName = 'PROP_DOC' then
    Value := Geral.FormataCNPJ_TT(QrUnidadesDOC_PRP.Value)
  else
  if VarName = 'SIND_NOME' then
    Value := Dmod.ObtemSindicoCond(QrUnidadesCliInt.Value)
  else
  if VarName = 'USU_NOME' then
    Value := QrUnidadesNOMEUSU.Value
  else
  if VarName = 'USU__DOC' then
    Value := Geral.FormataCNPJ_TT(QrUnidadesDOC_USU.Value)
  else
  if VarName = 'UN_LOGIN' then
  begin
    QrUsers.Close;
    QrUsers.Params[0].AsInteger := QrUnidadesConta.Value;
    UnDmkDAC_PF.AbreQuery(QrUsers, Dmod.MyDB);
    Value := QrUsersUsername.Value;
  end else
  if VarName = 'UN_SENHA' then
  begin
    QrUsers.Close;
    QrUsers.Params[0].AsInteger := QrUnidadesConta.Value;
    UnDmkDAC_PF.AbreQuery(QrUsers, Dmod.MyDB);
    Value := QrUsersPassword.Value;
  end else
  if VarName = 'LDEBITOS' then
  begin
    Val := 0;
    Mul := 0;
    Jur := 0;
    Atz := 0;
    Pag := 0;
    Pen := 0;
    QrInad.First;
    Value := '';
    //
    if CkMultaJur.Checked = True then
      Value := Value + 'N�mero bloqueto  Compet.    Valor R$   Vencim.       Multa       Juros  Atualizado        Pago    Pendente' + L
    else
      Value := Value + 'N�mero bloqueto  Compet.    Valor R$   Vencim.       Pago    Pendente' + L;
    //
    //'---------------123456789012345--01/2009--123.654,00--01/02/09--123.456,00--123.456,00--123.456,00--123.456,00--123.456,00'+Chr(10)+Chr(13);
    while not QrInad.Eof do
    begin
      Val := Val + QrInadCREDITO.Value;
      Mul := Mul + QrInadMulta.Value;
      Jur := Jur + QrInadJuros.Value;
      Atz := Atz + QrInadATZ_VAL.Value;
      Pag := Pag + QrInadPAGO.Value;
      Pen := Pen + QrInadPEND_VAL.Value;
      //
      if CkMultaJur.Checked = True then
      begin
        Txt_Multa   := Geral.CompletaString(Geral.FFT(QrInadMulta.Value, 2, siPositivo), ' ', 10, taRightJustify, True) + '  ';
        Txt_Juros   := Geral.CompletaString(Geral.FFT(QrInadJuros.Value, 2, siPositivo), ' ', 10, taRightJustify, True) + '  ';
        Txt_ATZ_VAL := Geral.CompletaString(Geral.FFT(QrInadATZ_VAL.Value, 2, siPositivo), ' ', 10, taRightJustify, True) + '  ';
      end else
      begin
        Txt_Multa   := '';
        Txt_Juros   := '';
        Txt_ATZ_VAL := '';
      end;
      //
      Value := Value +
      Geral.CompletaString(dmkPF.FFP(QrInadFatNum.Value, 0),' ', 15, taRightJustify, True) + '  ' +
      Geral.CompletaString(QrInadMES.Value, ' ', 07, taRightJustify, True) + '  ' +
      Geral.CompletaString(Geral.FFT(QrInadCREDITO.Value, 2, siPositivo), ' ', 10, taRightJustify, True)+ '  '+
      Geral.CompletaString(FormatDateTime(VAR_FORMATDATE3, QrInadVencimento.Value), ' ', 8, taRightJustify, True)+ '  '+
      //
      Txt_Multa +
      Txt_Juros +
      Txt_ATZ_VAL +
      //
      Geral.CompletaString(Geral.FFT(QrInadPAGO.Value, 2, siPositivo), ' ', 10, taRightJustify, True) + '  ' +
      Geral.CompletaString(Geral.FFT(QrInadPEND_VAL.Value, 2, siPositivo), ' ', 10, taRightJustify, True) + '  ' +
      L;
      //
      QrInad.Next;
    end;
    if QrInad.RecordCount > 1 then
    begin
      if CkMultaJur.Checked = True then
      begin
        Txt_Tot_Multa   := Geral.CompletaString(Geral.FFT(Mul, 2, siPositivo), ' ', 10, taRightJustify, True) + '  ';
        Txt_Tot_Juros   := Geral.CompletaString(Geral.FFT(Jur, 2, siPositivo), ' ', 10, taRightJustify, True) + '  ';
        Txt_Tot_ATZ_VAL := Geral.CompletaString(Geral.FFT(Atz, 2, siPositivo), ' ', 10, taRightJustify, True) + '  ';
      end else
      begin
        Txt_Tot_Multa   := '';
        Txt_Tot_Juros   := '';
        Txt_Tot_ATZ_VAL := '';
      end;
      //
      if CkMultaJur.Checked = True then
        Value := Value + '----------------------------------------------------------------------------------------------------------' + L
      else
        Value := Value + '----------------------------------------------------------------------' + L;
      //
      Value := Value +
      Geral.CompletaString('TOTAL',' ', 15, taRightJustify, True) + '  ' +
      Geral.CompletaString('', ' ', 07, taRightJustify, True) + '  ' +
      Geral.CompletaString(Geral.FFT(Val, 2, siPositivo), ' ', 10, taRightJustify, True)+ '  '+
      Geral.CompletaString('', ' ', 8, taRightJustify, True)+ '  '+
      //
      Txt_Tot_Multa +
      Txt_Tot_Juros +
      Txt_Tot_ATZ_VAL +
      //
      Geral.CompletaString(Geral.FFT(Pag, 2, siPositivo), ' ', 10, taRightJustify, True) + '  ' +
      Geral.CompletaString(Geral.FFT(Pen, 2, siPositivo), ' ', 10, taRightJustify, True) + '  ' +
      L;
    end;
  end
end;

procedure TFmCertidaoNeg.ImprimeCartasImp();
var
  Hoje, Celular, Mensagem, LastExec: String;
  Codigo, DiarCEMIts, Metodo: Integer;
begin
  Application.CreateForm(TFmCartasImp, FmCartasImpCN);
  FmCartasImpCN.FMultaJur          := CkMultaJur.Checked;
  FmCartasImpCN.FTipoFormCartasImp := tfciCertidaoNeg;
  FmCartasImpCN.FCarta             := EdCarta.ValueVariant;
  FmCartasImpCN.FDataPesq          := TPDataPesq.Date;
  FmCartasImpCN.FEmpresa           := Geral.IMV(EdEmpresa.Text);
  FmCartasImpCN.FEmprEnti          := QrEntiCondCodEnti.Value;
  FmCartasImpCN.FDepto             := Geral.IMV(EdCondImov.Text);
  FmCartasImpCN.FPropriet          := Geral.IMV(EdPropriet.Text);
  FmCartasImpCN.FTabLctA           := FTabLctA;
  FmCartasImpCN.FUnidadesFiltered  := QrUnidades.Filtered;
  // Mode a ser impresso
  FmCartasImpCN.FRGModoItemIndex := RGModo.ItemIndex;
  //
  FmCartasImpCN.CkDesign.Checked := CkDesign.Checked;
  //
  FmCartasImpCN.GeraCertidoes(QrUnidades.SQL.Text);
  //
  FmCartasImpCN.Destroy;
end;

procedure TFmCertidaoNeg.ImprimeDaqui();
var
  //Entidade,
  //EntiCond, Propriet, Apto,
  Carta: Integer;
  Continua: Word;
  Vencto: String;
begin
  //FEmImpressao := True;
  try
    Carta    := Geral.IMV(EdCarta.Text);
    //EntiCond := Geral.IMV(EdEntiCond.Text);
    //Propriet := Geral.IMV(EdPropriet.Text);
    //Apto     := Geral.IMV(EdCondImov.Text);
    Vencto   := Geral.FDT(TPDataPesq.Date, 1);
    //
    QrTexto.Close;
    QrTexto.Params[0].AsInteger := Carta;
    UnDmkDAC_PF.AbreQuery(QrTexto, Dmod.MyDB);
    //
    case QrTextoTipo.Value of
      0: Geral.MensagemBox('Defina o modelo de certid�o a ser usado!',
         'Aviso', MB_OK+MB_ICONWARNING);
      1:
      begin
        if RGModo.ItemIndex in (
        [CO_INADIMPL_APENAS_COM_PENDENCIAS(*1*),
        CO_INADIMPL_COM_E_SEM_PENDENCIAS(*3*)])then
          Continua := Geral.MensagemBox('A pesquisa foi realizada ' +
          'incluindo unidades que podem ter pend�ncias! Deseja emitir o a ' +
          'certid�o negativa assim mesmo?',
          'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION)
        else Continua := ID_YES;
        if Continua = ID_YES then
          MyObjects.frxMostra(frxCNDC, 'Certid�o negativa de d�bitos condominiais');
      end;
      2:
      begin
        {
        if QrInad.RecordCount > 0 then
          Continua := Geral.MensagemBox('Foram encontradas pend�ncias ' +
          'para a unidade pesquisada. Deseja emitir o a certid�o negativa assim ' +
          'mesmo?'), 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION)
        else Continua := ID_YES;
        if Continua = ID_YES then
        }
          MyObjects.frxMostra(frxCDCU, 'Carta de cobran�a de d�bitos condominiais');
      end;
      else Geral.MensagemBox('Defina o modelo de certid�o a ser usado!',
         'Aviso', MB_OK+MB_ICONWARNING);
    end;
  finally
    //FEmImpressao := False;
  end;
end;

procedure TFmCertidaoNeg.QrUnidadesAfterOpen(DataSet: TDataSet);
begin
  EdUnidades.ValueVariant := QrUnidades.RecordCount;
  BtImprime.Enabled := QrUnidades.RecordCount > 0;
end;

procedure TFmCertidaoNeg.QrUnidadesAfterScroll(DataSet: TDataSet);
const
  Data = '0000-00-00';
  Mez = 0;
  FatNum = 0.000;
var
  Entidade, EntiCond, Propriet, Apto: Integer;
  //Continua: Word;
  Vencto: String;
  Filtered: Boolean;
begin
  //Carta    := Geral.IMV(EdCarta.Text);
  EntiCond := Geral.IMV(EdEmpresa.Text);
  Entidade := QrEntiCondCodEnti.Value;
  Propriet := Geral.IMV(EdPropriet.Text);
  Apto     := QrUnidadesDepto.Value;
  Vencto   := Geral.FDT(TPDataPesq.Date, 1);
  //
  if RGPesquisa.ItemIndex = 1 then
  begin
    QrInad.Close;
    QrInad.SQL.Text := UFinanceiro.Pesquisa4_Cartas(RGModo.ItemIndex, Data, Vencto,
      EntiCond, Entidade, Apto, Propriet, Mez, FatNum, FTabLctA, Filtered);
    QrInad.Filtered := Filtered;
    UnDmkDAC_PF.AbreQuery(QrInad, Dmod.MyDB);
  end else
  begin
    //ver juros, multas etc sobre pago!
    QrInad.Close;
    QrInad.SQL.Clear;
    QrInad.SQL.Add('SELECT CONCAT(RIGHT(Mez, 2), "/",  LEFT(Mez + 200000, 4)) MES,');
    QrInad.SQL.Add('');
    QrInad.SQL.Add('lan.Data, lan.FatNum, lan.Vencimento, lan.Mez, lan.Descricao,');
    QrInad.SQL.Add('');
    QrInad.SQL.Add('SUM(lan.Credito) CREDITO, SUM(lan.Pago) PAGO,');
    QrInad.SQL.Add('');

    // Juros
    QrInad.SQL.Add('IF(lan.Sit>1, SUM(PagJur),');
    QrInad.SQL.Add('  SUM(IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
    QrInad.SQL.Add('  ROUND(IF(lan.Sit<2,lan.Credito-lan.Pago,0) * (TO_DAYS(SYSDATE()) - TO_DAYS(lan.Vencimento)) *');
    QrInad.SQL.Add('  lan.MoraDia  / 3000, 2), 0))) Juros,');
    QrInad.SQL.Add('');
    // Multa
    QrInad.SQL.Add('IF(lan.Sit>1, SUM(PagMul),');
    QrInad.SQL.Add('  SUM(IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
    QrInad.SQL.Add('  ROUND(IF(lan.Sit<2,lan.Credito-lan.Pago,0) * lan.Multa  / 100, 2), 0))) Multa,');
    QrInad.SQL.Add('');
    // Total
    QrInad.SQL.Add('SUM(IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
    QrInad.SQL.Add('  ROUND(IF(lan.Sit<2,lan.Credito-lan.Pago,0) * (TO_DAYS(SYSDATE()) - TO_DAYS(lan.Vencimento)) *');
    QrInad.SQL.Add('  lan.MoraDia  / 3000 +');
    QrInad.SQL.Add('  ROUND(IF(lan.Sit<2,lan.Credito-lan.Pago,0) * lan.Multa  / 100, 2), 2), 0)+ Credito) TOTAL,');
    QrInad.SQL.Add('');
    // Saldo
    QrInad.SQL.Add('IF(lan.Sit>1, 0,');
    QrInad.SQL.Add('  SUM(IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
    QrInad.SQL.Add('  ROUND(IF(lan.Sit<2,lan.Credito-lan.Pago,0) * (TO_DAYS(SYSDATE()) - TO_DAYS(lan.Vencimento)) *');
    QrInad.SQL.Add('  lan.MoraDia  / 3000 +');
    QrInad.SQL.Add('  ROUND(IF(lan.Sit<2,lan.Credito-lan.Pago,0) * lan.Multa  / 100, 2), 2), 0)+ Credito - Pago)) SALDO,');
    QrInad.SQL.Add('');
    // Atualizado
    QrInad.SQL.Add('');
    QrInad.SQL.Add('IF(SUM(IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
    QrInad.SQL.Add('  ROUND(IF(lan.Sit<2,lan.Credito-lan.Pago,0) * (TO_DAYS(SYSDATE()) - TO_DAYS(lan.Vencimento)) *');
    QrInad.SQL.Add('  lan.MoraDia  / 3000 +');
    QrInad.SQL.Add('  ROUND(IF(lan.Sit<2,lan.Credito-lan.Pago,0) * lan.Multa  / 100, 2), 2), 0)+ Credito)');
    QrInad.SQL.Add('  < SUM(lan.Pago) , SUM(lan.Pago) ,');
    QrInad.SQL.Add('  SUM(IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
    QrInad.SQL.Add('  ROUND(IF(lan.Sit<2,lan.Credito-lan.Pago,0) * (TO_DAYS(SYSDATE()) - TO_DAYS(lan.Vencimento)) *');
    QrInad.SQL.Add('  lan.MoraDia  / 3000 +');
    QrInad.SQL.Add('  ROUND(IF(lan.Sit<2,lan.Credito-lan.Pago,0) * lan.Multa  / 100, 2), 2), 0)+ Credito))');
    QrInad.SQL.Add('  ATZ_VAL,');
    QrInad.SQL.Add('');
    // Pendente
    QrInad.SQL.Add('IF(');
    QrInad.SQL.Add('  (SUM(IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
    QrInad.SQL.Add('  ROUND(lan.Credito * (TO_DAYS(SYSDATE()) - TO_DAYS(lan.Vencimento)) *');
    QrInad.SQL.Add('  lan.MoraDia  / 3000 +');
    QrInad.SQL.Add('  ROUND(lan.Credito * lan.Multa  / 100, 2), 2), 0)+ Credito - Pago) < 0)');
    QrInad.SQL.Add('  OR (SUM(IF(lan.Sit<2,lan.Credito-lan.Pago,0))<=0), 0,');
    QrInad.SQL.Add('  SUM(IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
    QrInad.SQL.Add('  ROUND(lan.Credito * (TO_DAYS(SYSDATE()) - TO_DAYS(lan.Vencimento)) *');
    QrInad.SQL.Add('  lan.MoraDia  / 3000 +');
    QrInad.SQL.Add('  ROUND(lan.Credito * lan.Multa  / 100, 2), 2), 0)+ Credito - Pago)) PEND_VAL,');

    // fim mudan�a 2010-10-12

    QrInad.SQL.Add('SUM(IF(lan.Sit<2,lan.Credito-lan.Pago,0)) VALOR');
    QrInad.SQL.Add('');
    QrInad.SQL.Add('FROM ' + FTabLctA + ' lan');
    QrInad.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
    QrInad.SQL.Add('WHERE car.Tipo=2');
    case RGModo.ItemIndex of
      CO_INADIMPL_APENAS_COM_PENDENCIAS(*1*):
      begin
        // Reparcelados n�o s�o d�bitos
        QrInad.SQL.Add('AND lan.Reparcel=0');
        QrInad.SQL.Add('AND lan.Sit < 2');
        QrInad.SQL.Add('AND lan.Credito > lan.Pago');
        //
        QrInad.Filtered := False;
      end;
      CO_INADIMPL_APENAS_SEM_PENDENCIAS(*2*):
      begin
        // Errado!! Usar filtro
        //QrInad.SQL.Add('AND lan.Sit = 2');
        QrInad.Filtered := True;
      end;
      CO_INADIMPL_COM_E_SEM_PENDENCIAS(*3*):
      begin
        //qualquer coisa
        QrInad.Filtered := False;
      end;
      else Geral.MensagemBox('"Tipo de pesquisa" n�o defenido!', 'Erro',
      MB_OK+MB_ICONERROR);
    end;
    //QrInad.SQL.Add('AND lan.Sit < 2');  ?????
    QrInad.SQL.Add('AND lan.Vencimento < "' + Vencto + '"');
    QrInad.SQL.Add('AND lan.Depto=' + dmkPF.FFP(Apto, 0));
    if EntiCond <> 0 then
      QrInad.SQL.Add('AND lan.CliInt=' + dmkPF.FFP(Entidade, 0));
    if Propriet <> 0 then
      QrInad.SQL.Add('AND lan.ForneceI=' + dmkPF.FFP(Propriet, 0));
    QrInad.SQL.Add('GROUP BY lan.FatNum, lan.Vencimento');
    QrInad.SQL.Add('ORDER BY lan.Mez');
    QrInad.SQL.Add('');
    //dmkPF.LeMeuTexto(QrInad.SQL.Text);
    UnDmkDAC_PF.AbreQuery(QrInad, Dmod.MyDB);
    //
  end;
(*
  if FEmImpressao then
  begin
    QrTexto.Close;
    QrTexto.Params[0].AsInteger := Carta;
    UnDmkDAC_PF.AbreQuery(QrTexto, Dmod.MyDB);
  end;
*)
end;

procedure TFmCertidaoNeg.QrUnidadesBeforeClose(DataSet: TDataSet);
begin
  EdUnidades.ValueVariant := 0;
  QrInad.Close;
  BtImprime.Enabled := False;
end;

procedure TFmCertidaoNeg.ReopenPropriet(Todos: Boolean);
var
  EntiCond: Integer;
begin
  EdPropriet.Text := '';
  CBPropriet.KeyValue := Null;
  //
  EntiCond := Geral.IMV(EdEmpresa.Text);
  QrPropriet.Close;
  if EntiCond <> 0 then
  begin
    QrPropriet.SQL.Clear;
    {N�o usar muito lento
    QrPropriet.SQL.Add('SELECT DISTINCT ent.Codigo,');
    QrPropriet.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEPROP');
    QrPropriet.SQL.Add('FROM entidades ent');
    QrPropriet.SQL.Add('LEFT JOIN condimov cim ON ent.Codigo=cim.Propriet');
    QrPropriet.SQL.Add('WHERE ent.Cliente2="V"');
    QrPropriet.SQL.Add('');
    }
    QrPropriet.SQL.Add('SELECT DISTINCT ent.Codigo,');
    QrPropriet.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEPROP');
    QrPropriet.SQL.Add('FROM condimov cim');
    QrPropriet.SQL.Add('LEFT JOIN entidades ent ON cim.Propriet=ent.Codigo');
    QrPropriet.SQL.Add('WHERE ent.Cliente2="V"');
    QrPropriet.SQL.Add('');
    if (EntiCond <> 0) and not Todos then
      QrPropriet.SQL.Add('AND cim.Codigo=' + IntToStr(EntiCond));
    QrPropriet.SQL.Add('');
    QrPropriet.SQL.Add('ORDER BY NOMEPROP');
    UnDmkDAC_PF.AbreQuery(QrPropriet, Dmod.MyDB);
  end;
end;

procedure TFmCertidaoNeg.TPDataPesqChange(Sender: TObject);
begin
  DesfazPesquisa();
end;

procedure TFmCertidaoNeg.RGModoClick(Sender: TObject);
begin
  BtPesquisa.Enabled := RGModo.ItemIndex > CO_INADIMPL_NENHUM (*0*);
  CkMultaJur.Visible := RGModo.ItemIndex in [1,3];
  //
  DesfazPesquisa();
end;

procedure TFmCertidaoNeg.ReopenCondImov();
var
  Condominio, Propriet: Integer;
begin
  EdCondImov.Text := '';
  CBCondImov.KeyValue := Null;
  //
  Condominio := Geral.IMV(EdEmpresa.Text);
  Propriet := Geral.IMV(EdPropriet.Text);
  QrCondImov.Close;
  QrCondImov.SQL.Clear;
  QrCondImov.SQL.Add('SELECT cim.Codigo, cnd.Cliente, cim.Conta, cim.Unidade,');
  QrCondImov.SQL.Add('IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NOMECND,');
  QrCondImov.SQL.Add('IF(usu.Tipo=0, usu.RazaoSocial, usu.Nome) NOMEUSU,');
  QrCondImov.SQL.Add('IF(usu.Tipo=0, usu.CNPJ, usu.CPF) DOC_USU,');
  QrCondImov.SQL.Add('IF(prp.Tipo=0, prp.RazaoSocial, prp.Nome) NOMEPRP,');
  QrCondImov.SQL.Add('IF(prp.Tipo=0, prp.CNPJ, prp.CPF) DOC_PRP');
  QrCondImov.SQL.Add('FROM condimov cim');
  QrCondImov.SQL.Add('LEFT JOIN cond      cnd ON cnd.Codigo=cim.Codigo');
  QrCondImov.SQL.Add('LEFT JOIN entidades cli ON cli.Codigo=cnd.Cliente');
  QrCondImov.SQL.Add('LEFT JOIN entidades usu ON usu.Codigo=cim.Usuario');
  QrCondImov.SQL.Add('LEFT JOIN entidades prp ON prp.Codigo=cim.Propriet');
  QrCondImov.SQL.Add('WHERE cim.Codigo<>0');
  QrCondImov.SQL.Add('');
  if Condominio <> 0 then
    QrCondImov.SQL.Add('AND cim.Codigo=' + IntToStr(Condominio));
  if Propriet <> 0 then
    QrCondImov.SQL.Add('AND cim.Propriet=' + IntToStr(Propriet));
  QrCondImov.SQL.Add('');
  QrCondImov.SQL.Add('ORDER BY cim.Unidade');
  UnDmkDAC_PF.AbreQuery(QrCondImov, Dmod.MyDB);
end;

end.

