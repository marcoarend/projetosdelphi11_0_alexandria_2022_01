unit VerifiBloquetosValor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, mySQLDbTables, dmkDBGrid,
  UnDmkProcFunc, frxClass, frxDBSet, Menus, MyDBCheck, UnDmkEnums;

type
  TFmVerifiBloquetosValor = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    LaTitulo1C: TLabel;
    QrEmpresas: TmySQLQuery;
    DsEmpresas: TDataSource;
    QrEmpresasCodCliInt: TIntegerField;
    QrEmpresasCodEnti: TIntegerField;
    QrEmpresasCodFilial: TIntegerField;
    QrEmpresasNome: TWideStringField;
    QrEmpresasAtivo: TSmallintField;
    QrPrev: TmySQLQuery;
    QrLct: TmySQLQuery;
    QrArreLct: TmySQLQuery;
    QrArreLctVencimento: TDateField;
    QrArreLctTexto: TWideStringField;
    QrArreLctUserCad: TWideStringField;
    QrArreLctUserAlt: TWideStringField;
    QrArreLctDataCad: TDateField;
    QrArreLctDataAlt: TDateField;
    QrArreLctArreBaC: TIntegerField;
    DsArreLct: TDataSource;
    PB1: TProgressBar;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    Splitter1: TSplitter;
    DBGrid1: TdmkDBGrid;
    dmkDBGrid1: TdmkDBGrid;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida1: TBitBtn;
    Panel1: TPanel;
    BtCarrega: TBitBtn;
    TabSheet2: TTabSheet;
    dmkDBGrid2: TdmkDBGrid;
    QrBolval: TmySQLQuery;
    DsBolVal: TDataSource;
    QrBolvalCodigo: TIntegerField;
    QrBolvalControle: TIntegerField;
    QrBolvalLancto: TIntegerField;
    QrBolvalCliInt: TIntegerField;
    QrBolvalValorLct: TFloatField;
    QrBolvalValorPeri: TFloatField;
    QrBolvalTipo: TSmallintField;
    GroupBox2: TGroupBox;
    Panel5: TPanel;
    BtSaida2: TBitBtn;
    Panel6: TPanel;
    QrBolvalNO_EMPR: TWideStringField;
    QrBolvalNO_TIPO: TWideStringField;
    frxGER_CONDM_034: TfrxReport;
    frxDsBolVal: TfrxDBDataset;
    BtImprime: TBitBtn;
    BtCorrige: TBitBtn;
    QrBolvalCO_EMPR: TIntegerField;
    PMCorrige: TPopupMenu;
    Corrigelanamento1: TMenuItem;
    Corrigearrecadaoconsumo1: TMenuItem;
    procedure BtSaida1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtCarregaClick(Sender: TObject);
    procedure QrEmpresasAfterScroll(DataSet: TDataSet);
    procedure QrEmpresasBeforeClose(DataSet: TDataSet);
    procedure BtImprimeClick(Sender: TObject);
    procedure frxGER_CONDM_034GetValue(const VarName: string;
      var Value: Variant);
    procedure BtCorrigeClick(Sender: TObject);
    procedure Corrigelanamento1Click(Sender: TObject);
    procedure Corrigearrecadaoconsumo1Click(Sender: TObject);
  private
    { Private declarations }
    FMaster, FAtualisa: Boolean;
    FTabDiv, FTabLctPre: String;
    procedure CarregaEmpresas();
    procedure ReabreDadosArreLct(CliInt: Integer; TabLctPre: String);
    procedure CarregaLct;
  public
    { Public declarations }
  end;

  var
  FmVerifiBloquetosValor: TFmVerifiBloquetosValor;

implementation

uses UnMyObjects, Module, ModuleGeral, CreateGeral, UMySQLModule, DmkDAC_PF,
  UCreate;

{$R *.DFM}

procedure TFmVerifiBloquetosValor.BtCarregaClick(Sender: TObject);
var
  Ini: TDateTime;
  CliInt: Integer;
  TabAriA, TabLctA, TabCnsA, Empr_TXT: String;
begin
  dmkDBGrid1.Enabled := False;
  try
    Ini := Now;
    //
    Screen.Cursor := crHourGlass;
    PB1.Visible   := True;
    PB1.Max       := QrEmpresas.RecordCount;
    PB1.Position  := 0;
    //
    QrEmpresas.First;
    while not QrEmpresas.Eof do
    begin
      CliInt   := QrEmpresasCodCliInt.Value;
      Empr_TXT := Geral.FF0(CliInt);
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
        'Pesquisando empresa número ' + Empr_TXT);
      //
      TabAriA  := DModG.NomeTab(TMeuDB, ntAri, False, ttA, CliInt);
      TabLctA  := DModG.NomeTab(TMeuDB, ntLct, False, ttA, CliInt);
      TabCnsA  := DModG.NomeTab(TMeuDB, ntCns, False, ttA, CliInt);
      //
      UMyMod.ExecutaMySQLQuery1(DModG.QrUpdPID1, [
      'DROP TABLE IF EXISTS _tab_diverge_1;',
      'CREATE TABLE _tab_diverge_1',
      'SELECT Controle, Credito, CliInt',
      'FROM ' + TabLctA,
      'WHERE Compensado < "1900-01-01" ',
      'AND FatID IN (600,601); ',
      '',
(*
      'DROP TABLE IF EXISTS _tab_diverge_2;',
      'CREATE TABLE _tab_diverge_2',
      'SELECT Codigo, Controle AriCtrl, Lancto, Valor',
      'FROM ' + TabAriA,
      'WHERE Lancto IN (SELECT Controle FROM _tab_diverge_1);',
      '',
      'DROP TABLE IF EXISTS _tab_diverge_3;',
      'CREATE TABLE _tab_diverge_3',
      'SELECT Codigo, Controle CnsCtrl, Lancto, Valor',
      'FROM ' + TabCnsA,
      'WHERE Lancto IN (SELECT Controle FROM _tab_diverge_1);',
      '',
      'INSERT INTO ' + FTabLctPre,
      'SELECT te2.Codigo, te2.AriCtrl, te1.Controle, ' + Empr_TXT + ', ',
      'te1.Credito, te2.Valor, 0',
      'FROM _tab_diverge_1 te1',
      'LEFT JOIN _tab_diverge_2 te2 ON te2.Lancto = te1.Controle',
      'WHERE te1.Credito <> te2.Valor;',
      '',
      'INSERT INTO ' + FTabLctPre,
      'SELECT te3.Codigo, te3.CnsCtrl, te1.Controle, ' + Empr_TXT + ', ',
      'te1.Credito, te3.Valor, 1',
      'FROM _tab_diverge_1 te1',
      'LEFT JOIN _tab_diverge_3 te3 ON te3.Lancto = te1.Controle',
      'WHERE te1.Credito <> te3.Valor;',
*)
      '',
      'INSERT INTO ' + FTabLctPre,
      'SELECT te2.Codigo, te2.Controle, te1.Controle, ' + Empr_TXT + ', ',
      'te1.Credito, te2.Valor, 0',
      'FROM _tab_diverge_1 te1',
      'LEFT JOIN ' + TabAriA + ' te2 ON te2.Lancto = te1.Controle',
      'WHERE te1.Credito <> te2.Valor;',
      '',
      'INSERT INTO ' + FTabLctPre,
      'SELECT te3.Codigo, te3.Controle, te1.Controle, ' + Empr_TXT + ', ',
      'te1.Credito, te3.Valor, 1',
      'FROM _tab_diverge_1 te1',
      'LEFT JOIN ' + TabCnsA + ' te3 ON te3.Lancto = te1.Controle',
      'WHERE te1.Credito <> te3.Valor;',
      '']);
      //dmkPF.LeMeuTexto(DModG.QrUpdPID1.SQL.Text);
      //
      QrEmpresas.Next;
    end;
    UnDmkDAC_PF.AbreQuery(QrBolVal, DModG.MyPID_DB);
    PageControl1.ActivePageIndex := 1;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Registros = ' +
      Geral.FF0(QrBolval.RecordCount) + '. Tempo total = ' +
      FormatDateTime('hh:nn:ss', Now - Ini));
  finally
    dmkDBGrid1.Enabled := True;
    PB1.Visible   := False;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmVerifiBloquetosValor.CarregaLct;
  (*
  procedure AtualizaTmpTable(Codigo, Controle, Lancto, CliInt, Tipo: Integer;
    ValorPeri: Double; TabLct: String);
  var
    ValorLct: Double;
  begin
    //Tipo
    //0 => Arrecadação
    //1 => Consumo
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrLct, Dmod.MyDB, [
      'SELECT Credito, Debito ',
      'FROM ' + TabLct + ' ',
      'WHERE Controle=' + Geral.FF0(Lancto),
      '']);
    if QrLct.RecordCount > 0 then
    begin
      ValorLct := QrLct.FieldByName('Credito').AsFloat - QrLct.FieldByName('Debito').AsFloat;
      //
      if ValorPeri <> ValorLct then
      begin
        DModG.QrUpdPID1.SQL.Clear;
        DModG.QrUpdPID1.SQL.Add('INSERT INTO '  + FTabLctPre);
        DModG.QrUpdPID1.SQL.Add(' SET Codigo=:P0, Controle=:P1, Lancto=:P2, ');
        DModG.QrUpdPID1.SQL.Add('ValorLct=:P3, ValorPeri=:P4, CliInt=:P5, Tipo=:P6');
        DModG.QrUpdPID1.Params[0].AsInteger := Codigo;
        DModG.QrUpdPID1.Params[1].AsInteger := Controle;
        DModG.QrUpdPID1.Params[2].AsInteger := Lancto;
        DModG.QrUpdPID1.Params[3].AsFloat   := ValorLct;
        DModG.QrUpdPID1.Params[4].AsFloat   := ValorPeri;
        DModG.QrUpdPID1.Params[5].AsInteger := CliInt;
        DModG.QrUpdPID1.Params[6].AsInteger := Tipo;
        DModG.QrUpdPID1.ExecSQL;
      end;
    end;
  end;
var
  Codigo, Controle, Lancto, CliInt: Integer;
  ValorPeri, ValorLct: Double;
  TabAriA, TabCnsA, TabLctA: String;
  *)
begin
  (*
  try
    Screen.Cursor := crHourGlass;
    PB1.Max       := QrEmpresas.RecordCount;
    PB1.Visible   := True;
    PB1.Position  := 0;
    //
    FTabLctPre := UCriar.RecriaTempTableNovo(ntrttBoletosValor, DModG.QrUpdPID1, False);
    //
    QrEmpresas.First;
    //
    while not QrEmpresas.Eof do
    begin
      CliInt  := QrEmpresasCodCliInt.Value;
      TabAriA := DModG.NomeTab(TMeuDB, ntAri, False, ttA, CliInt);
      TabCnsA := DModG.NomeTab(TMeuDB, ntCns, False, ttA, CliInt);
      TabLctA := DModG.NomeTab(TMeuDB, ntLct, False, ttA, CliInt);
      //
      //Verifica arrecadações
      UnDmkDAC_PF.AbreMySQLQuery0(QrPrev, Dmod.MyDB, [
        'SELECT Codigo, Controle, Lancto, Valor ',
        'FROM ' + TabAriA + ' ',
        'WHERE Lancto<>0 ',
        '']);
      if QrPrev.RecordCount > 0 then
      begin
        QrPrev.First;
        while not QrPrev.Eof do
        begin
          Codigo    := QrPrev.FieldByName('Codigo').AsInteger;
          Controle  := QrPrev.FieldByName('Controle').AsInteger;
          Lancto    := QrPrev.FieldByName('Lancto').AsInteger;
          ValorPeri := QrPrev.FieldByName('Valor').AsFloat;
          //
          AtualizaTmpTable(Codigo, Controle, Lancto, CliInt, 0, ValorPeri, TabLctA);
          //
          QrPrev.Next;
        end;
      end;
      //
      //Verifica consumos
      UnDmkDAC_PF.AbreMySQLQuery0(QrPrev, Dmod.MyDB, [
        'SELECT Codigo, Controle, Lancto, Valor ',
        'FROM ' + TabCnsA + ' ',
        'WHERE Lancto<>0 ',
        '']);
      if QrPrev.RecordCount > 0 then
      begin
        QrPrev.First;
        while not QrPrev.Eof do
        begin
          Codigo    := QrPrev.FieldByName('Codigo').AsInteger;
          Controle  := QrPrev.FieldByName('Controle').AsInteger;
          Lancto    := QrPrev.FieldByName('Lancto').AsInteger;
          ValorPeri := QrPrev.FieldByName('Valor').AsFloat;
          //
          AtualizaTmpTable(Codigo, Controle, Lancto, CliInt, 1, ValorPeri, TabLctA);
        end;
      end;
      //
      PB1.Position := PB1.Position + 1;
      PB1.Update;
      Application.ProcessMessages;
      //
      QrEmpresas.Next;
    end;
  finally
    FAtualisa     := True;
    PB1.Visible   := False;
    Screen.Cursor := crDefault;
    //
    QrEmpresas.First;
  end;
  *)
end;

procedure TFmVerifiBloquetosValor.Corrigearrecadaoconsumo1Click(
  Sender: TObject);
var
  CliInt, Controle, Lancto: Integer;
  Valor: Double;
  TabAtualiz: String;
begin
  if (QrBolval.State <> dsInactive) and (QrBolval.RecordCount > 0) then
  begin
    CliInt   := QrBolvalCliInt.Value;
    Valor    := QrBolvalValorLct.Value;
    Controle := QrBolvalControle.Value;
    Lancto   := QrBolvalLancto.Value;
    //
    if QrBolvalTipo.Value = 0 then //Arrecadação
      TabAtualiz := DModG.NomeTab(TMeuDB, ntAri, False, ttA, CliInt)
    else //Consumo
      TabAtualiz := DModG.NomeTab(TMeuDB, ntCns, False, ttA, CliInt);
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, TabAtualiz, False,
      ['Valor'], ['Controle'], [Valor], [Controle], True);
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
    'DELETE FROM ' + FTabLctPre,
    'WHERE Lancto=' + Geral.FF0(Lancto)
    ]);
    //
    UnDmkDAC_PF.AbreQuery(QrBolVal, DModG.MyPID_DB);
    //
    Geral.MB_Aviso('Item atualizado!');
  end else
    Geral.MB_Aviso('Nenhum lançamento selecionado!');
end;

procedure TFmVerifiBloquetosValor.Corrigelanamento1Click(Sender: TObject);
var
  CliInt, Lancto: Integer;
  Valor: Double;
  TabLctA: String;
begin
  if (QrBolval.State <> dsInactive) and (QrBolval.RecordCount > 0) then
  begin
    CliInt  := QrBolvalCliInt.Value;
    TabLctA := DModG.NomeTab(TMeuDB, ntLct, False, ttA, CliInt);
    Valor   := QrBolvalValorPeri.Value;
    Lancto  := QrBolvalLancto.Value;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, TabLctA, False,
      ['Credito'], ['Controle'], [Valor], [Lancto], True);
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
    'DELETE FROM ' + FTabLctPre,
    'WHERE Lancto=' + Geral.FF0(Lancto)
    ]);
    //
    UnDmkDAC_PF.AbreQuery(QrBolVal, DModG.MyPID_DB);
    //
    Geral.MB_Aviso('Item atualizado!');
  end else
    Geral.MB_Aviso('Nenhum lançamento selecionado!');
end;

procedure TFmVerifiBloquetosValor.BtCorrigeClick(Sender: TObject);
begin
  if FMaster = False then
    if not DBCheck.LiberaPelaSenhaBoss then Exit;
  //
  FMaster := True;
  //
  MyObjects.MostraPopUpDeBotao(PMCorrige, BtCorrige);
end;

procedure TFmVerifiBloquetosValor.BtImprimeClick(Sender: TObject);
begin
  MyObjects.frxDefineDataSets(frxGER_CONDM_034, [
     DModG.frxDsDono,
     //
    frxDsBolVal
  ]);
  //
  MyObjects.frxMostra(frxGER_CONDM_034, LaTitulo1C.Caption);
end;

procedure TFmVerifiBloquetosValor.BtSaida1Click(Sender: TObject);
begin
  Close;
end;

procedure TFmVerifiBloquetosValor.CarregaEmpresas;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Criando tabelas temporárias');
  //
  QrEmpresas.Close;
  QrEmpresas.Database := DModG.MyPID_DB;
  UnCreateGeral.RecriaTempTableNovo(ntrtt_Empresas, DModG.QrUpdPID1,
    False, 1, TAB_TMP_EMPRESAS);
  UMyMod.ExecutaMySQLQuery1(DModG.QrUpdPID1, [
  'DELETE FROM ' + TAB_TMP_EMPRESAS + ';',
  'INSERT INTO '  + TAB_TMP_EMPRESAS,
  'SELECT eci.CodCliInt, eci.CodEnti, eci.CodFilial, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, Nome) Nome, ',
  '0 Ativo',
  'FROM ' + TMeuDB + '.enticliint eci',
  'LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo=eci.CodEnti'
  , 'WHERE eci.TipoTabLct=1'
  ]);
  QrEmpresas.Open;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmVerifiBloquetosValor.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVerifiBloquetosValor.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FAtualisa       := False;
  FMaster         := False;
  PageControl1.ActivePageIndex := 0;
  //
  CarregaEmpresas();
  FTabLctPre := UCriar.RecriaTempTableNovo(ntrttBoletosValor, DModG.QrUpdPID1, False);
end;

procedure TFmVerifiBloquetosValor.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVerifiBloquetosValor.frxGER_CONDM_034GetValue(
  const VarName: string; var Value: Variant);
begin
  if VarName ='VARF_DATA' then Value := Now()
  else
end;

procedure TFmVerifiBloquetosValor.QrEmpresasAfterScroll(DataSet: TDataSet);
begin
  if FAtualisa then
   ReabreDadosArreLct(QrEmpresasCodCliInt.Value, FTabLctPre);
end;

procedure TFmVerifiBloquetosValor.QrEmpresasBeforeClose(DataSet: TDataSet);
begin
  QrArreLct.Close;
end;

procedure TFmVerifiBloquetosValor.ReabreDadosArreLct(CliInt: Integer; TabLctPre: String);
var
  TabAriA, TabLctA: String;
begin
  if TabLctPre <> '' then
  begin
    TabAriA := DModG.NomeTab(TMeuDB, ntAri, False, ttA, CliInt);
    TabLctA := DModG.NomeTab(TMeuDB, ntLct, False, ttA, CliInt);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrArreLct, DmodG.MyPID_DB, [
    'SELECT bov.*, ari.Texto, lct.Vencimento, ari.ArreBaC, ',
    'sen.Login UserCad, se2.Login UserAlt, lct.DataCad, lct.DataAlt ',
    'FROM ' + TabLctPre + ' bov ',
    'LEFT JOIN ' + TabAriA + ' ari ON ari.Controle = bov.Controle ',
    'LEFT JOIN ' + TabLctA + ' lct ON lct.Controle = bov.Lancto ',
    'LEFT JOIN ' + TMeuDB + '.senhas sen ON sen.Numero = lct.UserCad ',
    'LEFT JOIN ' + TMeuDB + '.senhas se2 ON se2.Numero = lct.UserAlt ',
    'WHERE bov.CliInt=' + Geral.FF0(CliInt),
    '']);
  end;
end;

end.
