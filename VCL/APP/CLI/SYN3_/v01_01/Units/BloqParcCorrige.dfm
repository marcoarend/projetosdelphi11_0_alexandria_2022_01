object FmBloqParcCorrige: TFmBloqParcCorrige
  Left = 339
  Top = 185
  Caption = 'BLQ-REPAR-003 :: Recorre'#231#227'o de Valores de Parcelamento'
  ClientHeight = 215
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 53
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitLeft = 180
    ExplicitTop = 88
    ExplicitHeight = 58
    object Label1: TLabel
      Left = 12
      Top = 8
      Width = 98
      Height = 13
      Caption = 'Valor pago (parcela):'
    end
    object Label2: TLabel
      Left = 112
      Top = 8
      Width = 97
      Height = 13
      Caption = 'Data do pagamento:'
    end
    object Label3: TLabel
      Left = 244
      Top = 8
      Width = 120
      Height = 13
      Caption = 'Conta corrente a creditar:'
    end
    object EdValor: TdmkEdit
      Left = 12
      Top = 24
      Width = 97
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
    end
    object TPDataP: TdmkEditDateTimePicker
      Left = 112
      Top = 24
      Width = 129
      Height = 21
      Date = 39843.611432395830000000
      Time = 39843.611432395830000000
      TabOrder = 1
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
    end
    object EdCarteira: TdmkEditCB
      Left = 244
      Top = 24
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBCarteira
      IgnoraDBLookupComboBox = False
    end
    object CBCarteira: TdmkDBLookupComboBox
      Left = 300
      Top = 24
      Width = 465
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsCarteiras
      TabOrder = 3
      dmkEditCB = EdCarteira
      UpdType = utYes
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 635
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 587
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 539
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 488
        Height = 32
        Caption = 'Recorre'#231#227'o de Valores de Parcelamento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 488
        Height = 32
        Caption = 'Recorre'#231#227'o de Valores de Parcelamento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 488
        Height = 32
        Caption = 'Recorre'#231#227'o de Valores de Parcelamento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 101
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 186
    ExplicitWidth = 635
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 631
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 145
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 230
    ExplicitWidth = 635
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 631
      object PnSaiDesis: TPanel
        Left = 636
        Top = 0
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitLeft = 487
        object BtSaida: TBitBtn
          Tag = 15
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
          NumGlyphs = 2
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        TabOrder = 1
        OnClick = BtOKClick
        NumGlyphs = 2
      end
    end
  end
  object QrCarteiras: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Tipo '
      'FROM carteiras'
      'WHERE Tipo=1 '
      'AND ForneceI=:P0'
      'ORDER BY Nome')
    Left = 12
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCarteirasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCarteirasNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrCarteirasTipo: TIntegerField
      FieldName = 'Tipo'
    end
  end
  object DsCarteiras: TDataSource
    DataSet = QrCarteiras
    Left = 40
    Top = 12
  end
end
