object FmCondGerImpGer2a: TFmCondGerImpGer2a
  Left = 0
  Top = 0
  Caption = 'GER-CONDM-019 :: Relat'#243'rio de Arrecada'#231#245'es (Lista de Cond'#244'minos)'
  ClientHeight = 558
  ClientWidth = 1005
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1005
    Height = 48
    Align = alTop
    Caption = 'Relat'#243'rio de Arrecada'#231#245'es (Lista de Cond'#244'minos)'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 0
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 1003
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object PainelConfirma: TPanel
    Left = 0
    Top = 510
    Width = 1005
    Height = 48
    Align = alBottom
    TabOrder = 1
    object Label5: TLabel
      Left = 448
      Top = 28
      Width = 334
      Height = 13
      Caption = 
        'TDC*: Tipo de documento de cobran'#231'a. (Sigla da tarefa de protoco' +
        'lo)'
    end
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object Panel2: TPanel
      Left = 893
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object BtTitulo: TBitBtn
      Left = 332
      Top = 4
      Width = 90
      Height = 40
      Caption = 'T'#237'tulo'
      NumGlyphs = 2
      TabOrder = 2
      OnClick = BtTituloClick
    end
    object BitBtn2: TBitBtn
      Tag = 5
      Left = 800
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Cross'
      NumGlyphs = 2
      TabOrder = 3
      Visible = False
      OnClick = BitBtn2Click
    end
    object CkDesign: TCheckBox
      Left = 116
      Top = 24
      Width = 85
      Height = 17
      Caption = 'Design mode.'
      TabOrder = 4
    end
    object dmkEdit5: TdmkEdit
      Left = 200
      Top = 20
      Width = 64
      Height = 21
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-33000'
      ValMax = '33000'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object dmkEdit6: TdmkEdit
      Left = 264
      Top = 20
      Width = 64
      Height = 21
      Alignment = taRightJustify
      TabOrder = 6
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-33000'
      ValMax = '33000'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1005
    Height = 462
    Align = alClient
    TabOrder = 2
    object GradeA: TStringGrid
      Left = 1
      Top = 153
      Width = 1003
      Height = 143
      Align = alClient
      ColCount = 8
      DefaultColWidth = 32
      DefaultRowHeight = 18
      RowCount = 2
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Univers Light Condensed'
      Font.Style = []
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goDrawFocusSelected, goColSizing, goEditing]
      ParentFont = False
      TabOrder = 0
      OnDrawCell = GradeADrawCell
      OnMouseDown = GradeAMouseDown
      OnMouseUp = GradeAMouseUp
      OnSelectCell = GradeASelectCell
      ColWidths = (
        32
        32
        32
        32
        33
        32
        32
        32)
    end
    object GradeB: TStringGrid
      Left = 1
      Top = 296
      Width = 1003
      Height = 165
      Align = alBottom
      ColCount = 6
      DefaultRowHeight = 18
      RowCount = 4
      TabOrder = 1
      Visible = False
    end
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 1003
      Height = 152
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 2
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 233
        Height = 152
        Align = alLeft
        Caption = ' Configura'#231#245'es gerais: '
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 22
          Width = 58
          Height = 13
          Caption = 'Altura linha:'
        end
        object Label2: TLabel
          Left = 104
          Top = 22
          Width = 57
          Height = 13
          Caption = 'Tam. fonte:'
        end
        object Label3: TLabel
          Left = 8
          Top = 102
          Width = 108
          Height = 13
          Caption = 'Largura total da folha:'
        end
        object Label4: TLabel
          Left = 180
          Top = 102
          Width = 31
          Height = 13
          Caption = 'pixels.'
        end
        object EdLinhaAlt: TdmkEdit
          Left = 68
          Top = 18
          Width = 32
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '18'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 18
          ValWarn = False
          OnChange = EdLinhaAltChange
        end
        object EdTamFonte: TdmkEdit
          Left = 164
          Top = 18
          Width = 32
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '10'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 10
          ValWarn = False
          OnChange = EdTamFonteChange
          OnExit = EdTamFonteExit
        end
        object CkRecalcN: TCheckBox
          Left = 8
          Top = 40
          Width = 173
          Height = 17
          Caption = 'Recalcular larguras de valores.'
          Checked = True
          State = cbChecked
          TabOrder = 2
          OnClick = CkRecalcNClick
        end
        object CkRecalcT: TCheckBox
          Left = 8
          Top = 60
          Width = 165
          Height = 17
          Caption = 'Recalcular larguras de Textos.'
          Checked = True
          State = cbChecked
          TabOrder = 3
          OnClick = CkRecalcTClick
        end
        object CkLargTit: TCheckBox
          Left = 8
          Top = 80
          Width = 217
          Height = 17
          Caption = 'Incluir os t'#237'tulos de colunas no rec'#225'lculos.'
          TabOrder = 4
          OnClick = CkLargTitClick
        end
        object EdLarguraFolha: TdmkEdit
          Left = 120
          Top = 98
          Width = 57
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 5
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdLarguraFolhaChange
        end
      end
      object GroupBox1: TGroupBox
        Left = 245
        Top = 0
        Width = 521
        Height = 152
        Align = alLeft
        Caption = ' Ordena'#231#227'o e agrupamento dos boletos: '
        TabOrder = 1
        object RGAgrup: TRadioGroup
          Left = 417
          Top = 15
          Width = 95
          Height = 135
          Align = alLeft
          Caption = ' Agrupamentos: '
          ItemIndex = 0
          Items.Strings = (
            '0'
            '1'
            '2')
          TabOrder = 0
        end
        object Panel5: TPanel
          Left = 9
          Top = 15
          Width = 95
          Height = 135
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 1
          object Panel6: TPanel
            Left = 0
            Top = 109
            Width = 95
            Height = 26
            Align = alBottom
            BevelOuter = bvLowered
            TabOrder = 0
            object CkDescen1: TCheckBox
              Left = 4
              Top = 4
              Width = 97
              Height = 17
              Caption = 'Descendente'
              TabOrder = 0
              OnClick = RGOrdem1Click
            end
          end
          object RGOrdem1: TRadioGroup
            Left = 0
            Top = 0
            Width = 95
            Height = 109
            Align = alClient
            Caption = ' Ordem 1: '
            ItemIndex = 0
            Items.Strings = (
              'Cadastro'
              'Propriet'#225'rio'
              'Vencimento'
              'Quitado'
              'TDC')
            TabOrder = 1
            OnClick = RGOrdem1Click
          end
        end
        object Panel7: TPanel
          Left = 111
          Top = 15
          Width = 95
          Height = 135
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 2
          object Panel8: TPanel
            Left = 0
            Top = 109
            Width = 95
            Height = 26
            Align = alBottom
            BevelOuter = bvLowered
            TabOrder = 0
            object CkDescen2: TCheckBox
              Left = 4
              Top = 4
              Width = 97
              Height = 17
              Caption = 'Descendente'
              TabOrder = 0
              OnClick = RGOrdem1Click
            end
          end
          object RGOrdem2: TRadioGroup
            Left = 0
            Top = 0
            Width = 95
            Height = 109
            Align = alClient
            Caption = ' Ordem 2: '
            ItemIndex = 1
            Items.Strings = (
              'Cadastro'
              'Propriet'#225'rio'
              'Vencimento'
              'Quitado'
              'TDC')
            TabOrder = 1
            OnClick = RGOrdem1Click
          end
        end
        object Panel9: TPanel
          Left = 213
          Top = 15
          Width = 95
          Height = 135
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 3
          object Panel10: TPanel
            Left = 0
            Top = 109
            Width = 95
            Height = 26
            Align = alBottom
            BevelOuter = bvLowered
            TabOrder = 0
            object CkDescen3: TCheckBox
              Left = 4
              Top = 4
              Width = 97
              Height = 17
              Caption = 'Descendente'
              TabOrder = 0
              OnClick = RGOrdem1Click
            end
          end
          object RGOrdem3: TRadioGroup
            Left = 0
            Top = 0
            Width = 95
            Height = 109
            Align = alClient
            Caption = ' Ordem 3: '
            ItemIndex = 2
            Items.Strings = (
              'Cadastro'
              'Propriet'#225'rio'
              'Vencimento'
              'Quitado'
              'TDC')
            TabOrder = 1
            OnClick = RGOrdem1Click
          end
        end
        object Panel11: TPanel
          Left = 315
          Top = 15
          Width = 95
          Height = 135
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 4
          object Panel12: TPanel
            Left = 0
            Top = 109
            Width = 95
            Height = 26
            Align = alBottom
            BevelOuter = bvLowered
            TabOrder = 0
            object CkDescen4: TCheckBox
              Left = 4
              Top = 4
              Width = 97
              Height = 17
              Caption = 'Descendente'
              TabOrder = 0
              OnClick = RGOrdem1Click
            end
          end
          object RGOrdem4: TRadioGroup
            Left = 0
            Top = 0
            Width = 95
            Height = 109
            Align = alClient
            Caption = ' Ordem 4: '
            ItemIndex = 3
            Items.Strings = (
              'Cadastro'
              'Propriet'#225'rio'
              'Vencimento'
              'Quitado'
              'TDC')
            TabOrder = 1
            OnClick = RGOrdem1Click
          end
        end
        object Panel4: TPanel
          Left = 308
          Top = 15
          Width = 7
          Height = 135
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 5
        end
        object Panel14: TPanel
          Left = 2
          Top = 15
          Width = 7
          Height = 135
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 6
        end
        object Panel15: TPanel
          Left = 104
          Top = 15
          Width = 7
          Height = 135
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 7
        end
        object Panel16: TPanel
          Left = 206
          Top = 15
          Width = 7
          Height = 135
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 8
        end
        object Panel17: TPanel
          Left = 410
          Top = 15
          Width = 7
          Height = 135
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 9
        end
      end
      object Panel13: TPanel
        Left = 233
        Top = 0
        Width = 12
        Height = 152
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 2
      end
    end
  end
  object frxGrade: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    EngineOptions.MaxMemSize = 10000000
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38006.786384259300000000
    ReportOptions.LastChange = 39872.778306180600000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'var'
      '  LastLeft: Extended;'
      '  FLastRow: Extended;'
      '    '
      
        'procedure Cross1OnPrintCell(Memo: TfrxMemoView; RowIndex, Column' +
        'Index, CellIndex: Integer; RowValues, ColumnValues, Value: Varia' +
        'nt);'
      'var'
      '  i: Integer;'
      '  s1, s2: String;                         '
      'begin'
      
        '  //Memo.Height := ObtemRowHeight(RowIndex);                    ' +
        '                      '
      
        '  //ShowMessage('#39'Memo: '#39' + FloatToStr(RowIndex) + '#39' = '#39' + FloatT' +
        'oStr(Memo.Height));                                             ' +
        '                 '
      '  case ObtemHALign(ColumnIndex, RowIndex) of'
      '    0: Memo.HAlign := haLeft;'
      '    1: Memo.HAlign := haCenter;'
      '    2: Memo.HAlign := haRight;'
      '  end;'
      '  i := ObtemWidth(ColumnIndex);'
      '  Memo.Width := i;     '
      '  if ColumnIndex <> 0 then           '
      '  begin'
      
        '    Memo.Left := LastLeft;                                      ' +
        '                '
      
        '    LastLeft    := LastLeft + i;                                ' +
        '                         '
      '  end else begin'
      
        '    Memo.Left := 0;                                             ' +
        '         '
      '    LastLeft  := i;'
      '  end;'
      
        '  FLastRow := RowIndex;                                         ' +
        '       '
      'end;'
      ''
      
        'procedure Cross1OnCalcHeight(RowIndex: Integer; RowValues: Varia' +
        'nt; var Height: Extended);'
      'var'
      '  h: Extended;                                  '
      'begin'
      '  h := ObtemRowHeight(0);                  '
      '  if Height <> h then Height := h;'
      
        '  //ShowMessage('#39'Cross: ='#39' + FloatToStr(Height));               ' +
        '                                               '
      'end;'
      ''
      'begin'
      '  //MasterData1.Height := ObtemRowHeight(FLastRow);'
      
        '  //ShowMessage('#39'Band: '#39' + FloatToStr(FLastRow) + '#39' = '#39' + FloatT' +
        'oStr(MasterData1.Height));                                      ' +
        '                        '
      'end.')
    OnBeforePrint = frxGradeBeforePrint
    OnGetValue = frxGradeGetValue
    OnUserFunction = frxGradeUserFunction
    Left = 312
    Top = 224
    Datasets = <
      item
      end
      item
        DataSet = Dmod.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 15.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 61.000000000000000000
        Top = 173.858380000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'MasterData1OnBeforePrint'
        RowCount = 1
        object Cross1: TfrxCrossView
          AllowVectorExport = True
          Left = 2.047310000000000000
          Width = 100.000000000000000000
          Height = 61.000000000000000000
          DownThenAcross = False
          MinWidth = 30
          ShowColumnHeader = False
          ShowColumnTotal = False
          ShowRowHeader = False
          ShowRowTotal = False
          OnCalcHeight = 'Cross1OnCalcHeight'
          OnPrintCell = 'Cross1OnPrintCell'
          Memos = {
            3C3F786D6C2076657273696F6E3D22312E302220656E636F64696E673D227574
            662D3822207374616E64616C6F6E653D226E6F223F3E3C63726F73733E3C6365
            6C6C6D656D6F733E3C546672784D656D6F5669657720416C6C6F77566563746F
            724578706F72743D225472756522204C6566743D2232322C3034373331222054
            6F703D223139332C3835383338222057696474683D2236302220486569676874
            3D22323122205265737472696374696F6E733D22323422204F6E416674657244
            6174613D2243726F73733143656C6C304F6E41667465724461746122204F6E42
            65666F72655072696E743D2243726F73733143656C6C304F6E4265666F726550
            72696E742220416C6C6F7745787072657373696F6E733D2246616C7365222046
            6F6E742E436861727365743D22312220466F6E742E436F6C6F723D2230222046
            6F6E742E4865696768743D222D382220466F6E742E4E616D653D22417269616C
            2220466F6E742E5374796C653D223022204672616D652E5479703D2231352220
            4672616D652E57696474683D22302C312220476170583D22332220476170593D
            2233222048416C69676E3D22686143656E7465722220506172656E74466F6E74
            3D2246616C7365222056416C69676E3D22766143656E7465722220546578743D
            2230222F3E3C546672784D656D6F56696577205461673D22312220416C6C6F77
            566563746F724578706F72743D225472756522204C6566743D22302220546F70
            3D2230222057696474683D223022204865696768743D22302220526573747269
            6374696F6E733D22382220416C6C6F7745787072657373696F6E733D2246616C
            736522204672616D652E5479703D2231352220476170583D2233222047617059
            3D2233222048416C69676E3D226861526967687422205374796C653D2263656C
            6C222056416C69676E3D22766143656E7465722220546578743D22222F3E3C54
            6672784D656D6F56696577205461673D22322220416C6C6F77566563746F7245
            78706F72743D225472756522204C6566743D22302220546F703D223022205769
            6474683D223022204865696768743D223022205265737472696374696F6E733D
            22382220416C6C6F7745787072657373696F6E733D2246616C73652220467261
            6D652E5479703D2231352220476170583D22332220476170593D223322204841
            6C69676E3D226861526967687422205374796C653D2263656C6C222056416C69
            676E3D22766143656E7465722220546578743D22222F3E3C546672784D656D6F
            56696577205461673D22332220416C6C6F77566563746F724578706F72743D22
            5472756522204C6566743D22302220546F703D2230222057696474683D223022
            204865696768743D223022205265737472696374696F6E733D22382220416C6C
            6F7745787072657373696F6E733D2246616C736522204672616D652E5479703D
            2231352220476170583D22332220476170593D2233222048416C69676E3D2268
            61526967687422205374796C653D2263656C6C222056416C69676E3D22766143
            656E7465722220546578743D22222F3E3C2F63656C6C6D656D6F733E3C63656C
            6C6865616465726D656D6F733E3C546672784D656D6F5669657720416C6C6F77
            566563746F724578706F72743D225472756522204C6566743D22302220546F70
            3D2230222057696474683D223022204865696768743D22302220526573747269
            6374696F6E733D22382220416C6C6F7745787072657373696F6E733D2246616C
            736522204672616D652E5479703D2231352220476170583D2233222047617059
            3D2233222056416C69676E3D22766143656E7465722220546578743D22222F3E
            3C546672784D656D6F5669657720416C6C6F77566563746F724578706F72743D
            225472756522204C6566743D22302220546F703D2230222057696474683D2230
            22204865696768743D223022205265737472696374696F6E733D22382220416C
            6C6F7745787072657373696F6E733D2246616C736522204672616D652E547970
            3D2231352220476170583D22332220476170593D2233222056416C69676E3D22
            766143656E7465722220546578743D22222F3E3C2F63656C6C6865616465726D
            656D6F733E3C636F6C756D6E6D656D6F733E3C546672784D656D6F5669657720
            5461673D223130302220416C6C6F77566563746F724578706F72743D22547275
            6522204C6566743D22302220546F703D2230222057696474683D223230302220
            4865696768743D22313030303022205265737472696374696F6E733D22323422
            20416C6C6F7745787072657373696F6E733D2246616C736522204672616D652E
            5479703D2231352220476170583D22332220476170593D2233222048416C6967
            6E3D22686143656E74657222205374796C653D22636F6C756D6E222056416C69
            676E3D22766143656E7465722220546578743D22222F3E3C2F636F6C756D6E6D
            656D6F733E3C636F6C756D6E746F74616C6D656D6F733E3C546672784D656D6F
            56696577205461673D223330302220416C6C6F77566563746F724578706F7274
            3D225472756522204C6566743D22302220546F703D2230222057696474683D22
            333222204865696768743D22313030303022205265737472696374696F6E733D
            2238222056697369626C653D2246616C73652220416C6C6F7745787072657373
            696F6E733D2246616C73652220466F6E742E436861727365743D22312220466F
            6E742E436F6C6F723D22302220466F6E742E4865696768743D222D3133222046
            6F6E742E4E616D653D22417269616C2220466F6E742E5374796C653D22312220
            4672616D652E5479703D2231352220476170583D22332220476170593D223322
            2048416C69676E3D22686143656E7465722220506172656E74466F6E743D2246
            616C736522205374796C653D22636F6C6772616E64222056416C69676E3D2276
            6143656E7465722220546578743D224772616E6420546F74616C222F3E3C2F63
            6F6C756D6E746F74616C6D656D6F733E3C636F726E65726D656D6F733E3C5466
            72784D656D6F5669657720416C6C6F77566563746F724578706F72743D225472
            756522204C6566743D22302220546F703D2230222057696474683D2232303022
            204865696768743D223022205265737472696374696F6E733D22382220566973
            69626C653D2246616C73652220416C6C6F7745787072657373696F6E733D2246
            616C736522204672616D652E5479703D2231352220476170583D223322204761
            70593D2233222048416C69676E3D22686143656E746572222056416C69676E3D
            22766143656E7465722220546578743D22222F3E3C546672784D656D6F566965
            7720416C6C6F77566563746F724578706F72743D225472756522204C6566743D
            22302220546F703D2230222057696474683D2232303022204865696768743D22
            3022205265737472696374696F6E733D2238222056697369626C653D2246616C
            73652220416C6C6F7745787072657373696F6E733D2246616C73652220467261
            6D652E5479703D2231352220476170583D22332220476170593D223322204841
            6C69676E3D22686143656E746572222056416C69676E3D22766143656E746572
            2220546578743D22222F3E3C546672784D656D6F5669657720416C6C6F775665
            63746F724578706F72743D225472756522204C6566743D22302220546F703D22
            30222057696474683D223022204865696768743D223022205265737472696374
            696F6E733D2238222056697369626C653D2246616C73652220416C6C6F774578
            7072657373696F6E733D2246616C736522204672616D652E5479703D22313522
            20476170583D22332220476170593D2233222048416C69676E3D22686143656E
            746572222056416C69676E3D22766143656E7465722220546578743D22222F3E
            3C546672784D656D6F5669657720416C6C6F77566563746F724578706F72743D
            225472756522204C6566743D22302220546F703D2230222057696474683D2232
            303022204865696768743D223022205265737472696374696F6E733D22382220
            416C6C6F7745787072657373696F6E733D2246616C736522204672616D652E54
            79703D2231352220476170583D22332220476170593D2233222048416C69676E
            3D22686143656E746572222056416C69676E3D22766143656E74657222205465
            78743D22222F3E3C2F636F726E65726D656D6F733E3C726F776D656D6F733E3C
            546672784D656D6F56696577205461673D223230302220416C6C6F7756656374
            6F724578706F72743D225472756522204C6566743D22302220546F703D223022
            2057696474683D2232303022204865696768743D223130303030222052657374
            72696374696F6E733D2232342220416C6C6F7745787072657373696F6E733D22
            46616C736522204672616D652E5479703D2231352220476170583D2233222047
            6170593D2233222048416C69676E3D22686143656E74657222205374796C653D
            22726F77222056416C69676E3D22766143656E7465722220546578743D22222F
            3E3C2F726F776D656D6F733E3C726F77746F74616C6D656D6F733E3C54667278
            4D656D6F56696577205461673D223430302220416C6C6F77566563746F724578
            706F72743D225472756522204C6566743D22302220546F703D22302220576964
            74683D22333222204865696768743D2231303030302220526573747269637469
            6F6E733D2238222056697369626C653D2246616C73652220416C6C6F77457870
            72657373696F6E733D2246616C73652220466F6E742E436861727365743D2231
            2220466F6E742E436F6C6F723D22302220466F6E742E4865696768743D222D31
            332220466F6E742E4E616D653D22417269616C2220466F6E742E5374796C653D
            223122204672616D652E5479703D2231352220476170583D2233222047617059
            3D2233222048416C69676E3D22686143656E7465722220506172656E74466F6E
            743D2246616C736522205374796C653D22726F776772616E64222056416C6967
            6E3D22766143656E7465722220546578743D224772616E6420546F74616C222F
            3E3C2F726F77746F74616C6D656D6F733E3C63656C6C66756E6374696F6E733E
            3C6974656D20302F3E3C2F63656C6C66756E6374696F6E733E3C636F6C756D6E
            736F72743E3C6974656D20322F3E3C2F636F6C756D6E736F72743E3C726F7773
            6F72743E3C6974656D20322F3E3C2F726F77736F72743E3C2F63726F73733E}
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.236240000000000000
        Top = 340.157700000000000000
        Width = 680.315400000000000000
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 94.488250000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object frxShapeView1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_TITULO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Top = 45.354360000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCond."NOMECLI"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Top = 68.031540000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPrev."PERIODO_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 294.803340000000000000
        Width = 680.315400000000000000
      end
    end
  end
  object frxCrossObject1: TfrxCrossObject
    Left = 340
    Top = 224
  end
  object frxGrade2: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39873.799550636580000000
    ReportOptions.LastChange = 39873.799550636580000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 396
    Top = 224
    Datasets = <
      item
        DataSet = Dmod.frxDsDono
        DataSetName = 'frxDsDono'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      Frame.Typ = []
      MirrorMode = []
    end
  end
  object frxDsQuery: TfrxDBDataset
    UserName = 'frxDsQuery'
    CloseDataSource = False
    DataSet = Query
    BCDToCurrency = False
    
    Left = 424
    Top = 224
  end
  object PMGradeA: TPopupMenu
    OnPopup = PMGradeAPopup
    Left = 452
    Top = 224
    object Alterasiglanocadastrodacontaplanodecontas1: TMenuItem
      Caption = 'Altera a sigla no cadastro da &Conta (plano de contas)'
      OnClick = Alterasiglanocadastrodacontaplanodecontas1Click
    end
    object Alterasiglanocadastrodaarrecadaobase1: TMenuItem
      Caption = 'Altera a sigla no cadastro da &Arrecada'#231#227'o base'
      OnClick = Alterasiglanocadastrodaarrecadaobase1Click
    end
    object AlterasiglanocadastrodaLeitura1: TMenuItem
      Caption = 'Altera a sigla no cadastro da &Leitura'
      OnClick = AlterasiglanocadastrodaLeitura1Click
    end
    object Alteratextodottuloselecionado1: TMenuItem
      Caption = 'Altera o &Texto do t'#237'tulo selecionado'
      OnClick = Alteratextodottuloselecionado1Click
    end
    object Larguradacolunaatual1: TMenuItem
      Caption = 'Altera a lar&Gura da coluna atual'
      OnClick = Larguradacolunaatual1Click
    end
  end
  object QrPgBloq: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT imv.Conta Apto, lan.FatNum, '
      'lan.Compensado Data, lan.Carteira, lan.Controle, '
      'IF(lan.Compensado<2, "", DATE_FORMAT(lan.Compensado, '
      '"%d/%m/%Y")) DATA_TXT,'
      'IF(lan.Vencimento<2, "", DATE_FORMAT(lan.Vencimento, '
      '"%d/%m/%Y")) VENCTO_TXT, lan.Vencimento, lan.Pago'
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'LEFT JOIN condimov imv ON imv.Conta=lan.Depto'
      'LEFT JOIN contas con ON con.Codigo=lan.Genero'
      'WHERE car.Tipo in (0,2)'
      'AND lan.FatID in (600,601)')
    Left = 284
    Top = 224
    object QrPgBloqApto: TIntegerField
      FieldName = 'Apto'
    end
    object QrPgBloqFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrPgBloqData: TDateField
      FieldName = 'Data'
    end
    object QrPgBloqDATA_TXT: TWideStringField
      FieldName = 'DATA_TXT'
      Size = 10
    end
    object QrPgBloqVENCTO_TXT: TWideStringField
      FieldName = 'VENCTO_TXT'
      Size = 10
    end
    object QrPgBloqPago: TFloatField
      FieldName = 'Pago'
    end
    object QrPgBloqVencimento: TDateField
      FieldName = 'Vencimento'
    end
  end
  object QrAptos: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT imc.Conta, imc.Andar, imc.Unidade, sta.Sigla, ptc.Sigla T' +
        'DC,'
      
        'IF(imc.Propriet=0, "", IF(pro.Tipo=0, pro.RazaoSocial, pro.Nome)' +
        ') NOMEPROP,'
      
        'IF(imc.Usuario=0, "", IF (mor.Tipo=0, mor.RazaoSocial, mor.Nome)' +
        ') NOMEUSUARIO'
      'FROM condimov imc'
      'LEFT JOIN entidades pro ON pro.Codigo=imc.Propriet'
      'LEFT JOIN entidades mor ON mor.Codigo=imc.Usuario'
      'LEFT JOIN status sta ON sta.Codigo=imc.Status'
      'LEFT JOIN entidades end ON end.Codigo=imc.Imobiliaria'
      'LEFT JOIN protocolos ptc ON ptc.Codigo=imc.Protocolo'
      'LEFT JOIN condbloco cnb ON cnb.Controle=imc.Controle'
      'WHERE imc.Ativo=1'
      'AND imc.Codigo = :P0'
      'ORDER BY cnb.Ordem, imc.Andar, imc.Unidade')
    Left = 256
    Top = 224
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAptosAndar: TIntegerField
      FieldName = 'Andar'
      Required = True
    end
    object QrAptosUnidade: TWideStringField
      FieldName = 'Unidade'
      Required = True
      Size = 10
    end
    object QrAptosNOMEPROP: TWideStringField
      FieldName = 'NOMEPROP'
      Size = 100
    end
    object QrAptosNOMEUSUARIO: TWideStringField
      FieldName = 'NOMEUSUARIO'
      Size = 100
    end
    object QrAptosSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 1
    end
    object QrAptosConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrAptosDATAPG_TXT: TWideStringField
      FieldKind = fkLookup
      FieldName = 'DATAPG_TXT'
      LookupDataSet = QrPgBloq
      LookupKeyFields = 'Apto'
      LookupResultField = 'DATA_TXT'
      KeyFields = 'Conta'
      Lookup = True
    end
    object QrAptosVENCTO_TXT: TWideStringField
      FieldKind = fkLookup
      FieldName = 'VENCTO_TXT'
      LookupDataSet = QrPgBloq
      LookupKeyFields = 'Apto'
      LookupResultField = 'VENCTO_TXT'
      KeyFields = 'Conta'
      Size = 10
      Lookup = True
    end
    object QrAptosTDC: TWideStringField
      FieldName = 'TDC'
      Size = 3
    end
    object QrAptosPAGO: TFloatField
      FieldKind = fkLookup
      FieldName = 'PAGO'
      LookupDataSet = QrPgBloq
      LookupKeyFields = 'Apto'
      LookupResultField = 'Pago'
      KeyFields = 'Conta'
      Lookup = True
    end
    object QrAptosDATA: TDateField
      FieldKind = fkLookup
      FieldName = 'DATA'
      LookupDataSet = QrPgBloq
      LookupKeyFields = 'Apto'
      LookupResultField = 'Data'
      KeyFields = 'Conta'
      Lookup = True
    end
    object QrAptosVENCIMENTO: TDateField
      FieldKind = fkLookup
      FieldName = 'VENCIMENTO'
      LookupDataSet = QrPgBloq
      LookupKeyFields = 'Apto'
      LookupResultField = 'Vencimento'
      KeyFields = 'Conta'
      Lookup = True
    end
  end
  object QrBol3: TMySQLQuery
    Database = Dmod.MyDB
    Left = 228
    Top = 224
    object QrBol3Ordem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrBol3FracaoIdeal: TFloatField
      FieldName = 'FracaoIdeal'
    end
    object QrBol3Andar: TIntegerField
      FieldName = 'Andar'
    end
    object QrBol3Boleto: TFloatField
      FieldName = 'Boleto'
      Required = True
    end
    object QrBol3Apto: TIntegerField
      FieldName = 'Apto'
      Required = True
    end
    object QrBol3Propriet: TIntegerField
      FieldName = 'Propriet'
      Required = True
    end
    object QrBol3Vencto: TDateField
      FieldName = 'Vencto'
      Required = True
    end
    object QrBol3Unidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrBol3NOMEPROPRIET: TWideStringField
      FieldName = 'NOMEPROPRIET'
      Size = 100
    end
    object QrBol3BOLAPTO: TWideStringField
      FieldName = 'BOLAPTO'
      Required = True
      Size = 65
    end
    object QrBol3KGT: TLargeintField
      FieldName = 'KGT'
      Required = True
    end
  end
  object Query: TMySQLQuery
    Database = DModG.MyPID_DB
    Left = 368
    Top = 224
  end
  object QrLin: TMySQLQuery
    Database = DModG.MyPID_DB
    Left = 632
    Top = 248
    object QrLinLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrLinSeqLn: TIntegerField
      FieldName = 'SeqLn'
    end
  end
end
