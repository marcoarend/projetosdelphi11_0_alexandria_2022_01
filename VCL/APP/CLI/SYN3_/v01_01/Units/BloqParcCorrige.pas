unit BloqParcCorrige;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, ComCtrls, dmkEditDateTimePicker,
  dmkEdit, DBCtrls, dmkDBLookupComboBox, dmkEditCB, DB, mySQLDbTables, dmkGeral,
  dmkImage, UnDmkEnums;

type
  TFmBloqParcCorrige = class(TForm)
    Panel1: TPanel;
    EdValor: TdmkEdit;
    TPDataP: TdmkEditDateTimePicker;
    Label1: TLabel;
    Label2: TLabel;
    EdCarteira: TdmkEditCB;
    CBCarteira: TdmkDBLookupComboBox;
    Label3: TLabel;
    QrCarteiras: TmySQLQuery;
    DsCarteiras: TDataSource;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    QrCarteirasTipo: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FConfr: Boolean;
  end;

  var
  FmBloqParcCorrige: TFmBloqParcCorrige;

implementation

uses Module, UnMyObjects;

{$R *.DFM}

procedure TFmBloqParcCorrige.BtOKClick(Sender: TObject);
begin
  if Geral.IMV(EdCarteira.Text) = 0 then
  begin
    Geral.MensagemBox('Informe a carteira de destino dos lanšamentos!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  FConfr := True;
  Close;
end;

procedure TFmBloqParcCorrige.BtSaidaClick(Sender: TObject);
begin
  FConfr := False;
  Close;
end;

procedure TFmBloqParcCorrige.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmBloqParcCorrige.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FConfr := False;
  TPDataP.Date := Date;
end;

procedure TFmBloqParcCorrige.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
