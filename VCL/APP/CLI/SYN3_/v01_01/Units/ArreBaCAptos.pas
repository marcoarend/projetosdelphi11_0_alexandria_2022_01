unit ArreBaCAptos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Db, mySQLDbTables,
  Grids, DBGrids, dmkEdit, dmkLabel, dmkImage, UnDmkEnums;

type
  TFmArreBaCAptos = class(TForm)
    Panel1: TPanel;
    QrUnidCond: TmySQLQuery;
    DsUnidCond: TDataSource;
    QrUnidCondApto: TIntegerField;
    QrUnidCondUnidade: TWideStringField;
    QrUnidCondProprie: TWideStringField;
    QrUnidCondSelecio: TSmallintField;
    DBGrid1: TDBGrid;
    QrSolo: TmySQLQuery;
    QrSoloConta: TIntegerField;
    Panel3: TPanel;
    dmkLabel1: TdmkLabel;
    dmkEdPercent: TdmkEdit;
    Label1: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenUnidCond(Apto: Integer);
    procedure AtualizaTodos(Status: Integer);
  public
    { Public declarations }
  end;

  var
  FmArreBaCAptos: TFmArreBaCAptos;

implementation

uses Module, MyVCLSkin, ArreBaC, UMySQLModule, dmkGeral, UnFinanceiro,
ModuleGeral, UnMyObjects;

{$R *.DFM}

procedure TFmArreBaCAptos.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmArreBaCAptos.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmArreBaCAptos.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmArreBaCAptos.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  ReopenUnidCond(0);
end;

procedure TFmArreBaCAptos.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if Column.FieldName = 'Selecio' then
    MeuVCLSkin.DrawGrid(DBGrid1, Rect, 1, QrUnidCondSelecio.Value);
end;

procedure TFmArreBaCAptos.DBGrid1CellClick(Column: TColumn);
var
  Status, Apto: Integer;
begin
  if Column.FieldName = 'Selecio' then
  begin
    Status := QrUnidCondSelecio.Value;
    if Status = 0 then Status := 1 else Status := 0;
    Apto := QrUnidCondApto.Value;
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add('UPDATE unidcond SET Selecio=:P0');
    DModG.QrUpdPID1.SQL.Add('WHERE Apto=:P1');
    DModG.QrUpdPID1.Params[00].AsInteger := Status;
    DModG.QrUpdPID1.Params[01].AsInteger := Apto;
    DModG.QrUpdPID1.ExecSQL;
    //
    ReopenUnidCond(Apto);
  end;
end;

procedure TFmArreBaCAptos.ReopenUnidCond(Apto: Integer);
begin
  QrUnidCond.Close;
  QrUnidCond.Database := DModG.MyPID_DB;
  QrUnidCond.Open;
  QrUnidCond.Locate('Apto', Apto, []);
end;

procedure TFmArreBaCAptos.BtTodosClick(Sender: TObject);
begin
  AtualizaTodos(1);
end;

procedure TFmArreBaCAptos.BtNenhumClick(Sender: TObject);
begin
  AtualizaTodos(0);
end;

procedure TFmArreBaCAptos.AtualizaTodos(Status: Integer);
begin
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('UPDATE unidcond SET Selecio=:P0');
  DModG.QrUpdPID1.Params[00].AsInteger := Status;
  DModG.QrUpdPID1.ExecSQL;
  //
  ReopenUnidCond(QrUnidCondApto.Value);
end;

procedure TFmArreBaCAptos.BtOKClick(Sender: TObject);
var
  Codigo, Controle, Conta, Apto: Integer;
  SQLTipo: TSQLType;
begin
  Conta := 0;
  QrUnidCond.First;
  while not QrUnidCond.Eof do
  begin
    QrSolo.Close;
    QrSolo.Params[00].AsInteger := FmArreBaC.QrArreBaIControle.Value;
    QrSolo.Params[01].AsInteger := QrUnidCondApto.Value;
    QrSolo.Open;
    if QrUnidCondSelecio.Value = 1 then
    begin
      if (QrSolo.RecordCount > 0) and (ImgTipo.SQLType = stDel) then
      begin
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('DELETE FROM arrebau WHERE Conta=:P0');
        Dmod.QrUpd.SQL.Add('');
        while not QrSolo.Eof do
        begin
          Conta := QrSoloConta.Value;
          Dmod.QrUpd.Params[0].AsInteger := Conta;
          Dmod.QrUpd.ExecSQL;
          //
          QrSolo.Next;
        end;
      end else if (ImgTipo.SQLType = stIns) then
      begin
        Conta := 0;
        //
        if QrSolo.RecordCount = 0 then SQLTipo := stIns else
        begin
          SQLTipo := stUpd;
          Conta := QrSoloConta.Value;
        end;
        Apto     := QrUnidCondApto.Value;
        Codigo   := FmArreBaC.QrArreBaCCodigo.Value;
        Controle := FmArreBaC.QrArreBaIControle.Value;
        Conta    := UMyMod.BuscaEmLivreY_Def('ArreBaU', 'Conta',
          SQLTipo, Conta);
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, SQLTipo, 'ArreBaU', False, [
          'Codigo', 'Controle', 'Apto', 'Percent'
        ], ['Conta'], [
          Codigo, Controle, Apto, dmkEdPercent.ValueVariant
        ], [Conta], True);
      end;
    end;
    QrUnidCond.Next;
  end;
  //
  FmArreBaC.ReopenQrArreBaU(Conta);
  Close;
end;

end.

