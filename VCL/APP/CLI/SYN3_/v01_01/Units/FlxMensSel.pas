unit FlxMensSel;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, dmkPermissoes, UnDmkEnums;

type
  TFmFlxMensSel = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    RGFluxo: TRadioGroup;
    Label1: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    EdAnoMes: TdmkEdit;
    LaMes: TLabel;
    QrLocBal: TmySQLQuery;
    QrLocBalConci_Data: TDateTimeField;
    QrLocBalDocum_Data: TDateTimeField;
    QrLocBalContb_Data: TDateTimeField;
    QrLocBalAnali_Data: TDateTimeField;
    QrLocBalEncad_Data: TDateTimeField;
    QrLocBalEntrg_Data: TDateTimeField;
    QrLocBalConci_DLim: TDateField;
    QrLocBalDocum_DLim: TDateField;
    QrLocBalContb_DLim: TDateField;
    QrLocBalAnali_DLim: TDateField;
    QrLocBalEncad_DLim: TDateField;
    QrLocBalEntrg_DLim: TDateField;
    QrLocBlq: TmySQLQuery;
    QrLocBlqFolha_DLim: TDateField;
    QrLocBlqLeiAr_DLim: TDateField;
    QrLocBlqFecha_DLim: TDateField;
    QrLocBlqRisco_DLim: TDateField;
    QrLocBlqPrint_DLim: TDateField;
    QrLocBlqProto_DLim: TDateField;
    QrLocBlqRelat_DLim: TDateField;
    QrLocBlqEMail_DLim: TDateField;
    QrLocBlqPorta_DLim: TDateField;
    QrLocBlqPostl_DLim: TDateField;
    QrLocBlqFolha_Data: TDateTimeField;
    QrLocBlqLeiAr_Data: TDateTimeField;
    QrLocBlqFecha_Data: TDateTimeField;
    QrLocBlqRisco_Data: TDateTimeField;
    QrLocBlqPrint_Data: TDateTimeField;
    QrLocBlqProto_Data: TDateTimeField;
    QrLocBlqRelat_Data: TDateTimeField;
    QrLocBlqEMail_Data: TDateTimeField;
    QrLocBlqPorta_Data: TDateTimeField;
    QrLocBlqPostl_Data: TDateTimeField;
    QrLocBlqddVence: TSmallintField;
    QrCond: TmySQLQuery;
    QrCondFlxB_Ordem: TIntegerField;
    QrCondFlxB_Folha: TSmallintField;
    QrCondFlxB_LeiAr: TSmallintField;
    QrCondFlxB_Fecha: TSmallintField;
    QrCondFlxB_Risco: TSmallintField;
    QrCondFlxB_Print: TSmallintField;
    QrCondFlxB_Proto: TSmallintField;
    QrCondFlxB_Relat: TSmallintField;
    QrCondFlxB_EMail: TSmallintField;
    QrCondFlxB_Porta: TSmallintField;
    QrCondFlxB_Postl: TSmallintField;
    QrCondFlxM_Ordem: TIntegerField;
    QrCondFlxM_Conci: TSmallintField;
    QrCondFlxM_Docum: TSmallintField;
    QrCondFlxM_Anali: TSmallintField;
    QrCondFlxM_Contb: TSmallintField;
    QrCondFlxM_Encad: TSmallintField;
    QrCondFlxM_Entrg: TSmallintField;
    QrCondDiaVencto: TIntegerField;
    dmkPermissoes1: TdmkPermissoes;
    QrLocBalWeb01_DLim: TDateField;
    QrLocBalWeb01_Data: TDateTimeField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure RGFluxoClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmFlxMensSel: TFmFlxMensSel;

implementation

uses Module, UnMyObjects, UMySQLModule, MyDBCheck, FlxMensBalAdd, FlxMensBalUpd,
  FlxMensBlqAdd, FlxMensBlqUpd;

{$R *.DFM}

procedure TFmFlxMensSel.BtOKClick(Sender: TObject);
  function DiaUtil(Ano, Mes, Dia: Integer): Integer;
  var
    Data: TDateTime;
    D, M, A: Word;
  begin
    if Dia = 0 then
      Result := 0
    else begin
      Data := EncodeDate(Ano, Mes, Dia);
      Data := UMyMod.CalculaDataDeposito(Data);
      DecodeDate(Data, A, M, D);
      Result := D;
    end;
  end;
var
  Empresa, Fluxo, AnoMes, Etapa: Integer;
  Data: TDateTime;
  Ano, Mes, Dia: Word;
begin
  if MyObjects.FIC(Length(EdAnoMes.Text) = 0, EdAnoMes, 'Informe o m�s!') then Exit;
  //
  Empresa := EdEmpresa.ValueVariant;
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Informe a empresa!') then Exit;
  AnoMes := Geral.DTAM(EdAnoMes.ValueVariant);
  if MyObjects.FIC(AnoMes < 2, EdAnoMes, 'Informe o m�s!') then Exit;
  Fluxo := RGFluxo.ItemIndex;
  if MyObjects.FIC(Fluxo = 0, RGFluxo, 'Informe o fluxo!') then Exit;
  //
  QrCond.Close;
  QrCond.Params[00].AsInteger := EdEmpresa.ValueVariant;
  QrCond.Open;
  //
  case Fluxo of
    1:
    begin
      QrLocBal.Close;
      QrLocBal.Params[00].AsInteger := Empresa;
      QrLocBal.Params[01].AsInteger := AnoMes;
      QrLocBal.Open;
      //
      if QrLocBal.RecordCount = 0 then
      begin
        if DBCheck.CriaFm(TFmFlxMensBalAdd, FmFlxMensBalAdd, afmoNegarComAviso) then
        begin
          FmFlxMensBalAdd.EdEmpresa.ValueVariant    := EdEmpresa.ValueVariant;
          FmFlxMensBalAdd.cbEmpresa.kEYValue        := CBEmpresa.KeyValue;
          FmFlxMensBalAdd.EdAnoMes.ValueVariant     := EdAnoMes.ValueVariant;
          //
          Data := EdAnoMes.ValueVariant;
          DecodeDate(Data, Ano, Mes, Dia);
          FmFlxMensBalAdd.EdFlxM_Ordem.ValueVariant := QrCondFlxM_Ordem.Value;
          if
          QrCondFlxM_Ordem.Value + QrCondFlxM_Conci.Value +
          QrCondFlxM_Docum.Value + QrCondFlxM_Anali.Value +
          QrCondFlxM_Contb.Value + QrCondFlxM_Encad.Value +
          QrCondFlxM_Entrg.Value > 0 then
          begin
            FmFlxMensBalAdd.EdFlxM_Conci.ValueVariant := DiaUtil(Ano, Mes, QrCondFlxM_Conci.Value);
            FmFlxMensBalAdd.EdFlxM_Docum.ValueVariant := DiaUtil(Ano, Mes, QrCondFlxM_Docum.Value);
            FmFlxMensBalAdd.EdFlxM_Anali.ValueVariant := DiaUtil(Ano, Mes, QrCondFlxM_Anali.Value);
            FmFlxMensBalAdd.EdFlxM_Contb.ValueVariant := DiaUtil(Ano, Mes, QrCondFlxM_Contb.Value);
            FmFlxMensBalAdd.EdFlxM_Encad.ValueVariant := DiaUtil(Ano, Mes, QrCondFlxM_Encad.Value);
            FmFlxMensBalAdd.EdFlxM_Entrg.ValueVariant := DiaUtil(Ano, Mes, QrCondFlxM_Entrg.Value);
          end else begin
            FmFlxMensBalAdd.EdFlxM_Conci.ValueVariant := DiaUtil(Ano, Mes, Dmod.QrControle.FieldByName('FlxM_Conci').AsInteger);
            FmFlxMensBalAdd.EdFlxM_Docum.ValueVariant := DiaUtil(Ano, Mes, Dmod.QrControle.FieldByName('FlxM_Docum').AsInteger);
            FmFlxMensBalAdd.EdFlxM_Anali.ValueVariant := DiaUtil(Ano, Mes, Dmod.QrControle.FieldByName('FlxM_Anali').AsInteger);
            FmFlxMensBalAdd.EdFlxM_Contb.ValueVariant := DiaUtil(Ano, Mes, Dmod.QrControle.FieldByName('FlxM_Contb').AsInteger);
            FmFlxMensBalAdd.EdFlxM_Encad.ValueVariant := DiaUtil(Ano, Mes, Dmod.QrControle.FieldByName('FlxM_Encad').AsInteger);
            FmFlxMensBalAdd.EdFlxM_Entrg.ValueVariant := DiaUtil(Ano, Mes, Dmod.QrControle.FieldByName('FlxM_Entrg').AsInteger);
          end;
          //
          FmFlxMensBalAdd.ShowModal;
          FmFlxMensBalAdd.Destroy;
        end;
      end else begin
        Etapa := 0;
        //
        if (QrLocBalConci_Data.Value = 0) and (QrLocBalConci_DLim.Value > 0) then
          Etapa := 1
        else
        if (QrLocBalDocum_Data.Value = 0) and (QrLocBalDocum_DLim.Value > 0) then
          Etapa := 2
        else
        if (QrLocBalContb_Data.Value = 0) and (QrLocBalContb_DLim.Value > 0) then
          Etapa := 3
        else
        if (QrLocBalAnali_Data.Value = 0) and (QrLocBalAnali_DLim.Value > 0) then
          Etapa := 4
        else
        if (QrLocBalEncad_Data.Value = 0) and (QrLocBalEncad_DLim.Value > 0) then
          Etapa := 5
        else
        if (QrLocBalEntrg_Data.Value = 0) and (QrLocBalEntrg_DLim.Value > 0) then
          Etapa := 6
        else
        if (QrLocBalWeb01_Data.Value = 0) and (QrLocBalWeb01_DLim.Value > 0) then
          Etapa := 7;        
        //
        if Etapa > 0 then
        begin
          if DBCheck.CriaFm(TFmFlxMensBalUpd, FmFlxMensBalUpd, afmoNegarComAviso) then
          begin
            FmFlxMensBalUpd.EdEmpresa.ValueVariant := EdEmpresa.ValueVariant;
            FmFlxMensBalUpd.cbEmpresa.kEYValue     := CBEmpresa.KeyValue;
            FmFlxMensBalUpd.EdAnoMes.ValueVariant  := EdAnoMes.ValueVariant;
            FmFlxMensBalUpd.RGEtapa.ItemIndex      := Etapa;
            FmFlxMensBalUpd.ShowModal;
            FmFlxMensBalUpd.Destroy;
          end;
        end else Geral.MensagemBox('Per�odo j� encerrado para esta configura��o!',
        'Mensagem', MB_OK+MB_ICONINFORMATION);
      end;
    end;
    2:
    begin
      QrLocBlq.Close;
      QrLocBlq.Params[00].AsInteger := Empresa;
      QrLocBlq.Params[01].AsInteger := AnoMes;
      QrLocBlq.Open;
      //
      if QrLocBlq.RecordCount = 0 then
      begin
        if DBCheck.CriaFm(TFmFlxMensBlqAdd, FmFlxMensBlqAdd, afmoNegarComAviso) then
        begin
          FmFlxMensBlqAdd.EdEmpresa.ValueVariant    := EdEmpresa.ValueVariant;
          FmFlxMensBlqAdd.cbEmpresa.kEYValue        := CBEmpresa.KeyValue;
          FmFlxMensBlqAdd.EdAnoMes.ValueVariant     := EdAnoMes.ValueVariant;
          //
          Data := EdAnoMes.ValueVariant;
          DecodeDate(Data, Ano, Mes, Dia);
          if
          QrCondFlxB_Folha.Value + QrCondFlxB_LeiAr.Value +
          QrCondFlxB_Fecha.Value + QrCondFlxB_Risco.Value +
          QrCondFlxB_Print.Value + QrCondFlxB_Proto.Value +
          QrCondFlxB_Relat.Value + QrCondFlxB_EMail.Value +
          QrCondFlxB_Porta.Value + QrCondFlxB_Postl.Value > 0 then
          begin
            FmFlxMensBlqAdd.EdFlxB_Folha.ValueVariant := DiaUtil(Ano, Mes, QrCondFlxB_Folha.Value);
            FmFlxMensBlqAdd.EdFlxB_LeiAr.ValueVariant := DiaUtil(Ano, Mes, QrCondFlxB_LeiAr.Value);
            FmFlxMensBlqAdd.EdFlxB_Fecha.ValueVariant := DiaUtil(Ano, Mes, QrCondFlxB_Fecha.Value);
            FmFlxMensBlqAdd.EdFlxB_Risco.ValueVariant := DiaUtil(Ano, Mes, QrCondFlxB_Risco.Value);
            FmFlxMensBlqAdd.EdFlxB_Print.ValueVariant := DiaUtil(Ano, Mes, QrCondFlxB_Print.Value);
            FmFlxMensBlqAdd.EdFlxB_Proto.ValueVariant := DiaUtil(Ano, Mes, QrCondFlxB_Proto.Value);
            FmFlxMensBlqAdd.EdFlxB_Relat.ValueVariant := DiaUtil(Ano, Mes, QrCondFlxB_Relat.Value);
            FmFlxMensBlqAdd.EdFlxB_EMail.ValueVariant := DiaUtil(Ano, Mes, QrCondFlxB_EMail.Value);
            FmFlxMensBlqAdd.EdFlxB_Porta.ValueVariant := DiaUtil(Ano, Mes, QrCondFlxB_Porta.Value);
            FmFlxMensBlqAdd.EdFlxB_Postl.ValueVariant := DiaUtil(Ano, Mes, QrCondFlxB_Postl.Value);
          end else begin
            FmFlxMensBlqAdd.EdFlxB_Folha.ValueVariant := DiaUtil(Ano, Mes, Dmod.QrControle.FieldByName('FlxB_Folha').AsInteger);
            FmFlxMensBlqAdd.EdFlxB_LeiAr.ValueVariant := DiaUtil(Ano, Mes, Dmod.QrControle.FieldByName('FlxB_LeiAr').AsInteger);
            FmFlxMensBlqAdd.EdFlxB_Fecha.ValueVariant := DiaUtil(Ano, Mes, Dmod.QrControle.FieldByName('FlxB_Fecha').AsInteger);
            FmFlxMensBlqAdd.EdFlxB_Risco.ValueVariant := DiaUtil(Ano, Mes, Dmod.QrControle.FieldByName('FlxB_Risco').AsInteger);
            FmFlxMensBlqAdd.EdFlxB_Print.ValueVariant := DiaUtil(Ano, Mes, Dmod.QrControle.FieldByName('FlxB_Print').AsInteger);
            FmFlxMensBlqAdd.EdFlxB_Proto.ValueVariant := DiaUtil(Ano, Mes, Dmod.QrControle.FieldByName('FlxB_Proto').AsInteger);
            FmFlxMensBlqAdd.EdFlxB_Relat.ValueVariant := DiaUtil(Ano, Mes, Dmod.QrControle.FieldByName('FlxB_Relat').AsInteger);
            FmFlxMensBlqAdd.EdFlxB_EMail.ValueVariant := DiaUtil(Ano, Mes, Dmod.QrControle.FieldByName('FlxB_EMail').AsInteger);
            FmFlxMensBlqAdd.EdFlxB_Porta.ValueVariant := DiaUtil(Ano, Mes, Dmod.QrControle.FieldByName('FlxB_Porta').AsInteger);
            FmFlxMensBlqAdd.EdFlxB_Postl.ValueVariant := DiaUtil(Ano, Mes, Dmod.QrControle.FieldByName('FlxB_Postl').AsInteger);
          end;
          FmFlxMensBlqAdd.EdFlxB_Ordem.ValueVariant := QrCondFlxB_Ordem.Value;
          FmFlxMensBlqAdd.EdddVence.ValueVariant    := QrCondDiaVencto.Value;
          //
          FmFlxMensBlqAdd.ShowModal;
          FmFlxMensBlqAdd.Destroy;
        end;
      end else begin
        //
        if (QrLocBlqFolha_Data.Value = 0) and (QrLocBlqFolha_DLim.Value > 0) then
          Etapa := 1
        else
        if (QrLocBlqLeiAr_Data.Value = 0) and (QrLocBlqLeiAr_DLim.Value > 0) then
          Etapa := 2
        else
        if (QrLocBlqFecha_Data.Value = 0) and (QrLocBlqFecha_DLim.Value > 0) then
          Etapa := 3
        else
        if (QrLocBlqRisco_Data.Value = 0) and (QrLocBlqRisco_DLim.Value > 0) then
          Etapa := 4
        else
        if (QrLocBlqPrint_Data.Value = 0) and (QrLocBlqPrint_DLim.Value > 0) then
          Etapa := 5
        else
        if (QrLocBlqProto_Data.Value = 0) and (QrLocBlqProto_DLim.Value > 0) then
          Etapa := 6
        else
        if (QrLocBlqRelat_Data.Value = 0) and (QrLocBlqRelat_DLim.Value > 0) then
          Etapa := 7
        else
        if (QrLocBlqEMail_Data.Value = 0) and (QrLocBlqEMail_DLim.Value > 0) then
          Etapa := 8
        else
        if (QrLocBlqPorta_Data.Value = 0) and (QrLocBlqPorta_DLim.Value > 0) then
          Etapa := 9
        else
        if (QrLocBlqPostl_Data.Value = 0) and (QrLocBlqPostl_DLim.Value > 0) then
          Etapa := 10
        else
          Etapa := 0;
        //
        if Etapa > 0 then
        begin
          if DBCheck.CriaFm(TFmFlxMensBlqUpd, FmFlxMensBlqUpd, afmoNegarComAviso) then
          begin
            FmFlxMensBlqUpd.EdEmpresa.ValueVariant := EdEmpresa.ValueVariant;
            FmFlxMensBlqUpd.cbEmpresa.kEYValue     := CBEmpresa.KeyValue;
            FmFlxMensBlqUpd.EdAnoMes.ValueVariant  := EdAnoMes.ValueVariant;
            FmFlxMensBlqUpd.RGEtapa.ItemIndex      := Etapa;
            FmFlxMensBlqUpd.ShowModal;
            FmFlxMensBlqUpd.Destroy;
          end;
        end else Geral.MensagemBox('Per�odo j� encerrado para esta configura��o!',
        'Mensagem', MB_OK+MB_ICONINFORMATION);
      end;
    end;
  end;
  Close;
end;

procedure TFmFlxMensSel.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFlxMensSel.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  ImgTipo.SQLType := stPsq;
end;

procedure TFmFlxMensSel.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFlxMensSel.RGFluxoClick(Sender: TObject);
begin
  case RGFluxo.ItemIndex of
    1: EdAnoMes.ValueVariant := IncMonth(Date, -1);
    2: EdAnoMes.ValueVariant := Date;
  end;
end;

end.
