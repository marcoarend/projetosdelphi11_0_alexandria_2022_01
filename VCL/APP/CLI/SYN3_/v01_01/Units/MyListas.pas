unit MyListas;

interface

uses
  System.Generics.Collections,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs, Forms,
  ExtCtrls, ComCtrls, StdCtrls, DB, (*DBTables,*) mysqlDBTables, UnMyLinguas,
  UnInternalConsts, UnMLAGeral, dmkGeral, UnDmkProcFunc, UnDmkEnums, UnGrl_Vars;

type
  TMyListas = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    function CriaListaImpDOS(FImpDOS: TStringList): Boolean;
    function CriaListaUserSets(FUserSets: TStringList): Boolean;
    //
    function CriaListaTabelas(Database: TmySQLDatabase; Lista:
              TList<TTabelas>): Boolean;
    //function CriaListaTabelas(Database: TmySQLDatabase; FTabelas: TStringList): Boolean;
    function CriaListaTabelasLocais(Lista: TList<TTabelas>): Boolean;
    function CriaListaIndices(TabelaBase, TabelaNome: String;
             FLIndices: TList<TIndices>): Boolean;
    function CriaListaCampos(Tabela: String; FLCampos: TList<TCampos>;
             var TemControle: TTemControle): Boolean;
    function CriaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
    function CriaListaQeiLnk(TabelaCria: String; FLQeiLnk: TList<TQeiLnk>): Boolean;
    function CriaListaJanelas(FLJanelas: TList<TJanelas>): Boolean;
    function CompletaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
    function ExcluiTab: Boolean;
    function ExcluiReg: Boolean;
    function ExcluiIdx: Boolean;
    procedure VerificaOutrosAntes(DataBase: TmySQLDatabase; Memo: TMemo);
    procedure VerificaOutrosDepois(DataBase: TmySQLDatabase; Memo: TMemo);
    procedure ModificaDadosDeIndicesAlterados(Indice, Tabela: String;
              DataBase: TmySQLDatabase; Memo: TMemo);
    procedure ConfiguracoesIniciais(UsaCoresRel: Integer; AppIDtxt: String);
  end;

const
  //CO_VERSAO = 1710051038;
  CO_VERMCW = 1710051038;
  CO_VERMLA = 1710051038;
  CO_SIGLA_APP = 'SYN3';
  CO_DMKID_APP = 43;
  CO_GRADE_APP = False;
  CO_VLOCAL = True;
  CO_EXTRA_LCT_003 = False; // True somente para Credito2

var
  MyList: TMyListas;
  FRCampos  : TCampos;
  FRIndices : TIndices;
  FRJanelas : TJanelas;
  //
  _ArrClieSets: array[01..32] of String;
  _MaxClieSets: Integer;
  _ArrFornSets: array[01..32] of String;
  _MaxFornSets: Integer;

implementation

uses Module, UMySQLModule, UnDiario_Tabs, UnIBGE_DTB_Tabs, UnFinance_Tabs,
  UnProtocol_Tabs, UnPerfJan_Tabs, FPMin_Tabs, FPMax_Tabs, NFSe_TbTerc,
  NFSe_Tabs, Geral_TbTerc, ModuleGeral, SMS_Tabs, InflIdx_Tabs, Mail_Tabs,
  WUsers_Tabs, Arquivos_Tabs, UnCNAB_Tabs, UnEmpresas_Tabs, UnEnti_Tabs, UnALL_Tabs,
  UnAnotacoes_Tabs, UnMoedas_Tabs, UnTextos_Tabs, UnCheques_Tabs, UnBloq_0202_Tabs,
  UnBloqGerl_Tabs, UnEntities, UnNotificacoes_Tabs, WGerl_Tabs,
  UnConsumoGerl_Tabs, MyDBCheck;

function TMyListas.CriaListaTabelas(Database: TmySQLDatabase; Lista:
  TList<TTabelas>): Boolean;
  //
  procedure TabelasPorCliente((*Tab: TTabsCli*));
  var
    TbCI: String;
    CliInt: Integer;
  begin
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SHOW TABLES');
    Dmod.QrAux.SQL.Add('FROM ' + TMeuDB);
    Dmod.QrAux.SQL.Add('LIKE "entidades"');
    Dmod.QrAux.Open;
    if Dmod.QrAux.RecordCount > 0 then
    begin
      Dmod.QrAux.Close;
      Dmod.QrAux.SQL.Clear;
      Dmod.QrAux.SQL.Add('SELECT cliint');
      Dmod.QrAux.SQL.Add('FROM entidades');
      Dmod.QrAux.SQL.Add('WHERE cliint <> 0');
      Dmod.QrAux.Open;
      //
      while not Dmod.QrAux.Eof do
      begin
        CliInt := Dmod.QrAux.FieldByName('cliint').AsInteger;
        //
        if CliInt > 0 then
        begin
          TbCI := FormatFloat('0000', CliInt);
          //
          MyLinguas.AdTbLst(Lista, False, Lowercase('prv' + TbCI + 'A'), LowerCase(TAB_PRV));
          MyLinguas.AdTbLst(Lista, False, Lowercase('prv' + TbCI + 'B'), LowerCase(TAB_PRV));
          MyLinguas.AdTbLst(Lista, False, Lowercase('prv' + TbCI + 'D'), LowerCase(TAB_PRV));

          MyLinguas.AdTbLst(Lista, False, Lowercase('pri' + TbCI + 'A'), LowerCase(TAB_PRI));
          MyLinguas.AdTbLst(Lista, False, Lowercase('pri' + TbCI + 'B'), LowerCase(TAB_PRI));
          MyLinguas.AdTbLst(Lista, False, Lowercase('pri' + TbCI + 'D'), LowerCase(TAB_PRI));

          MyLinguas.AdTbLst(Lista, False, Lowercase('cns' + TbCI + 'A'), LowerCase(TAB_CNS));
          MyLinguas.AdTbLst(Lista, False, Lowercase('cns' + TbCI + 'B'), LowerCase(TAB_CNS));
          MyLinguas.AdTbLst(Lista, False, Lowercase('cns' + TbCI + 'D'), LowerCase(TAB_CNS));

          MyLinguas.AdTbLst(Lista, False, Lowercase('ari' + TbCI + 'A'), LowerCase(TAB_ARI));
          MyLinguas.AdTbLst(Lista, False, Lowercase('ari' + TbCI + 'B'), LowerCase(TAB_ARI));
          MyLinguas.AdTbLst(Lista, False, Lowercase('ari' + TbCI + 'D'), LowerCase(TAB_ARI));

          if Database.DatabaseName = VAR_DBWEB then
          begin
            MyLinguas.AdTbLst(Lista, False, Lowercase('lct' + TbCI + 'A'), LowerCase(LAN_CTOS));
            MyLinguas.AdTbLst(Lista, False, Lowercase('lct' + TbCI + 'B'), LowerCase(LAN_CTOS));
            MyLinguas.AdTbLst(Lista, False, Lowercase('lct' + TbCI + 'D'), LowerCase(LAN_CTOS));
          end;
        end;
        //
        Dmod.QrAux.Next;
      end;
    end;
  end;
begin
  Result := True;
  try
    if Database = Dmod.MyDB then
    begin
      MyLinguas.AdTbLst(Lista, False, 'Cond', '');
      MyLinguas.AdTbLst(Lista, False, 'CondBloco', '');
      MyLinguas.AdTbLst(Lista, False, 'CondGri', '');
      MyLinguas.AdTbLst(Lista, False, 'CondGriImv', '');
      MyLinguas.AdTbLst(Lista, False, 'CondImov', '');
      MyLinguas.AdTbLst(Lista, False, 'Controle', '');
      MyLinguas.AdTbLst(Lista, False, 'Status', '');
      //
      TabelasPorCliente();
      //
      Empresas_Tabs.CarregaListaTabelas(Lista);
      Enti_Tabs.CarregaListaTabelas(Lista);
      Finance_Tabs.CarregaListaTabelas(Lista);
      Finance_Tabs.ComplementaListaComLcts(Lista);
      ALL_Tabs.CarregaListaTabelas(Lista);
      Anotacoes_Tabs.CarregaListaTabelas(Lista);
      Moedas_Tabs.CarregaListaTabelas(Lista);
      Textos_Tabs.CarregaListaTabelas(Database, Lista);
      Cheques_Tabs.CarregaListaTabelas(Lista);
      PerfJan_Tabs.CarregaListaTabelas(Lista);
      CNAB_Tabs.CarregaListaTabelas(Database, Lista);
      Diario_Tabs.CarregaListaTabelas(Lista);
      Protocol_Tabs.CarregaListaTabelas(Database, Lista);
      UnFPMin_Tabs.CarregaListaTabelas(Lista);
      UnFPMax_Tabs.CarregaListaTabelas(Lista);
      UnNFSe_Tabs.CarregaListaTabelas(Database.Databasename, Lista);
      UnSMS_Tabs.CarregaListaTabelas(Lista);
      UnInflIdx_Tabs.CarregaListaTabelas(Lista);
      Mail_Tb.CarregaListaTabelas(Lista);
      Bloq_0202_Tabs.CarregaListaTabelas(Database, Lista);
      BloqGerl_Tabs.CarregaListaTabelas(Database, Lista);
      ConsumoGerl_Tabs.CarregaListaTabelas(Lista);
      Notificacoes_Tabs.CarregaListaTabelas(Lista);
      //
      //Ser�o eliminado ou melhoradas ap�s migra��o
      MyLinguas.AdTbLst(Lista, False, 'CNAB_Dir', '');
      MyLinguas.AdTbLst(Lista, False, 'OutMor', '');
      MyLinguas.AdTbLst(Lista, False, 'users', '');
      MyLinguas.AdTbLst(Lista, False, 'wclients', '');
      MyLinguas.AdTbLst(Lista, False, 'Veic', '');
      //Aparentemente n�o usa
      MyLinguas.AdTbLst(Lista, False, 'Animais', '');
      MyLinguas.AdTbLst(Lista, False, 'Cargos', '');
      MyLinguas.AdTbLst(Lista, False, 'CondAnimais', '');
      MyLinguas.AdTbLst(Lista, False, 'ArreRat', '');
      MyLinguas.AdTbLst(Lista, False, 'CB4Data4', '');
      MyLinguas.AdTbLst(Lista, False, 'CNAB240Dir', '');
      MyLinguas.AdTbLst(Lista, False, 'CNAB240Lei', '');
      MyLinguas.AdTbLst(Lista, False, 'Filhos', '');
      MyLinguas.AdTbLst(Lista, False, 'Financas', '');
      MyLinguas.AdTbLst(Lista, False, 'ImpCHMsg', '');
      MyLinguas.AdTbLst(Lista, False, 'PrevRat', '');
    end else
    if Database.DatabaseName = VAR_DBWEB then
    begin
      //Tem que ser aqui
      Textos_Tabs.CarregaListaTabelas(Database, Lista);
      //
      MyLinguas.AdTbLst(Lista, False, Lowercase(VAR_LCT), '');
      MyLinguas.AdTbLst(Lista, False, 'bloqparc', '');
      MyLinguas.AdTbLst(Lista, False, 'bloqparcpar', '');
      MyLinguas.AdTbLst(Lista, False, 'bloqparcits', '');
      MyLinguas.AdTbLst(Lista, False, 'carteiras', '');
      MyLinguas.AdTbLst(Lista, False, 'cond', '');
      MyLinguas.AdTbLst(Lista, False, 'condbloco', '');
      MyLinguas.AdTbLst(Lista, False, 'condgri', '');
      MyLinguas.AdTbLst(Lista, False, 'condgriimv', '');
      MyLinguas.AdTbLst(Lista, False, 'condimov', '');
      MyLinguas.AdTbLst(Lista, False, 'dirweb', '');
      MyLinguas.AdTbLst(Lista, False, 'entidades', '');
      MyLinguas.AdTbLst(Lista, False, 'enticliint', '');
      MyLinguas.AdTbLst(Lista, False, 'feriados', '');
      MyLinguas.AdTbLst(Lista, False, 'users', '');
      MyLinguas.AdTbLst(Lista, False, 'wclients', '');
      MyLinguas.AdTbLst(Lista, False, 'Lanctoz', LAN_CTOS);
      MyLinguas.AdTbLst(Lista, False, 'PreEmail', '');
      MyLinguas.AdTbLst(Lista, False, 'PreEmMsg', '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('CNAB_Cfg'), '');
      //
      MyLinguas.AdTbLst(Lista, False, Lowercase('bacen_pais'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('dtb_munici'), '');
      // s� web
      MyLinguas.AdTbLst(Lista, False, 'uploads', '');
      MyLinguas.AdTbLst(Lista, False, 'visitas', '');
      MyLinguas.AdTbLst(Lista, False, 'visitasits', '');
      MyLinguas.AdTbLst(Lista, False, 'visitascli', '');
      MyLinguas.AdTbLst(Lista, False, 'wcond', '');
      MyLinguas.AdTbLst(Lista, False, 'wfotos', '');
      MyLinguas.AdTbLst(Lista, False, 'wfotosedit', '');
      MyLinguas.AdTbLst(Lista, False, 'wslidefts', '');
      //
      TabelasPorCliente();
      //
      WUsers_Tb.CarregaListaTabelas(Database, Lista, CO_DMKID_APP);
      WGerl_Tb.CarregaListaTabelas(Database, Lista, CO_DMKID_APP);
      Arquivos_Tb.CarregaListaTabelas(Database, Lista);
      Protocol_Tabs.CarregaListaTabelas(Database, Lista);
      Finance_Tabs.ComplementaListaComLcts(Lista);
    end;
    if DModG <> nil then
    begin
      if Database = DModG.AllID_DB then
      begin
        UnGeral_TbTerc.CarregaListaTabelas(Lista);
        IBGE_DTB_Tabs.CarregaListaTabelas(Lista);
        UnNFSe_TbTerc.CarregaListaTabelas(Lista);
      end;
    end;
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaTabelasLocais(Lista: TList<TTabelas>): Boolean;
begin
  Result := True;
  try
    //CashTb.CarregaListaTabelasLocaisCashier(Lista);
    Finance_Tabs.CarregaListaTabelasLocais(Lista);
    //
    MyLinguas.AdTbLst(Lista, False, 'ArreBAA', '');
    MyLinguas.AdTbLst(Lista, False, 'ArreBAAUni', '');
    MyLinguas.AdTbLst(Lista, False, 'ChequesImp', '');
    MyLinguas.AdTbLst(Lista, False, 'CNAB_1', '');
    MyLinguas.AdTbLst(Lista, False, 'Concilia', '');
    MyLinguas.AdTbLst(Lista, False, 'ConsLei', '');
    MyLinguas.AdTbLst(Lista, False, 'CtasItsCtas', '');
    MyLinguas.AdTbLst(Lista, False, 'Enderecos', '');
    MyLinguas.AdTbLst(Lista, False, 'ExtratoCC', '');
    MyLinguas.AdTbLst(Lista, False, 'ExtratoCC2', '');
    MyLinguas.AdTbLst(Lista, False, 'InfoSeq', '');
    MyLinguas.AdTbLst(Lista, False, 'LocCtrl', '');
    //Linguas.AdTbLst(Lista, False, 'Mensalidade', '');
    MyLinguas.AdTbLst(Lista, False, 'MinhaEtiq', '');
    MyLinguas.AdTbLst(Lista, False, 'PrevBAB', '');
    MyLinguas.AdTbLst(Lista, False, 'PrevBAN', '');
    MyLinguas.AdTbLst(Lista, False, 'SdoNiveis', '');
    MyLinguas.AdTbLst(Lista, False, 'UnidCond', '');
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
begin
  Result := True;
  try
    if Uppercase(Tabela) = Uppercase('users') then
    begin
      FListaSQL.Add('User_ID|Username|Password|Tipo');
      FListaSQL.Add('-1|"admin"|"8i27ot4r"|9');
    end else
    if Uppercase(Tabela) = Uppercase('PerfisIts') then
    begin
      FListaSQL.Add('Janela|Descricao'); //N�O PODE TER V�RGULA NOS TEXTOS!!!!!!
      //FListaSQL.Add('""|""');
    end else
    {
    if Uppercase(Tabela) = Uppercase('ProtocoOco') then
    begin
      FListaSQL.Add('Codigo|Nome');
      FListaSQL.Add('0|"Lan�amentos"');
    end else
    }
    begin
      PerfJan_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      Empresas_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      Enti_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      Finance_Tabs.CarregaListaSQL(Tabela, FListaSQL, CO_DMKID_APP);
      ALL_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      Anotacoes_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      Moedas_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      Textos_Tabs.CarregaListaSQL(Tabela, FListaSQL, CO_DMKID_APP);
      Cheques_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      //CashTb.CarregaListaSQLCashier(Tabela, FListaSQL);
      CNAB_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      Diario_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      IBGE_DTB_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      Protocol_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      UnFPMin_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      UnFPMax_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      UnNFSe_TbTerc.CarregaListaSQL(Tabela, FListaSQL);
      UnNFSe_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      UnGeral_TbTerc.CarregaListaSQL(Tabela, FListaSQL);
      UnSMS_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      UnInflIdx_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      WUsers_Tb.CarregaListaSQL(Tabela, FListaSQL, CO_DMKID_APP);
      WGerl_Tb.CarregaListaSQL(Tabela, FListaSQL, CO_DMKID_APP);
      Arquivos_Tb.CarregaListaSQL(Tabela, FListaSQL);
      Mail_Tb.CarregaListaSQL(Tabela, FListaSQL);
      Bloq_0202_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      BloqGerl_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      ConsumoGerl_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      Notificacoes_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    end;
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CompletaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
const
  Parecer: WideString =
'{\\rtf1\\ansi\\ansicpg1252\\deff0{\\fonttbl{\\f0\\fnil\\fcharset0 MS Shell ' +
'Dlg;}{\\f1\\fnil MS Sans Serif;}}' + #13#10+
'{\\colortbl ;\\red0\\green0\\blue0;}' + #13#10 +
'\\viewkind4\\uc1\\pard\\li277\\ri18\\tx720\\cf1\\lang1046\\f0\\fs20 Os abaixo ' +
'assinados, membros do Conselho Fiscal, declaram que examinaram o Demonstrativo ' +
'relativo ao m�s de [MES_ANO ], bem como a documenta��o de Receitas e ' +
'Despesas, raz�o pela qual s�o de parecer favor�vel a aprova��o e por estar o ' +
'Demonstrativo de presta��o de contas em perfeita ordem e exatid�o.\\cf0\\par' +
'\\pard\\cf1\\f1\\fs16\\par' + #13#10 +
'}';
begin
  Result := True;
  try
    if Uppercase(Tabela) = Uppercase('Contas') then
    begin
      (*FListaSQL.Add('-129|"Compra de mercadorias diversas"');
      FListaSQL.Add('-130|"Frete de mercadorias"');
      FListaSQL.Add('-131|"Compra de filmes"');
      FListaSQL.Add('-132|"Frete de filmes"');
      FListaSQL.Add('-133|"Inv�lido"');
      FListaSQL.Add('-134|"Venda e/ou loca��o"');*)
    end else
    if Uppercase(Tabela) = Uppercase('UserTxts') then
    begin
      FListaSQL.Add('Codigo|Ordem|TxtSys|TxtMeu');
      FListaSQL.Add('-1|01|"Condom�nios"|"Condom�nios"');
      FListaSQL.Add('-2|02|"Condom�nio"|"Condom�nio"');
      FListaSQL.Add('-3|03|"Cond."|"Cond."');
      FListaSQL.Add('-4|04|"Condom."|"Condom."');
      FListaSQL.Add('-5|05|"Cond�minos"|"Cond�minos"');
      FListaSQL.Add('-6|06|"Cond�mino"|"Cond�mino"');
      FListaSQL.Add('-7|07|"Propriet�rios"|"Propriet�rios"');
      FListaSQL.Add('-8|08|"Propriet�rio"|"Propriet�rio"');
      FListaSQL.Add('-9|09|"Prop."|"Prop."');
      FListaSQL.Add('-10|10|"Administradora de riscos"|"Administradora de riscos"');
      FListaSQL.Add('-11|11|"Imobili�rias"|"Imobili�rias"');
      FListaSQL.Add('-12|12|"Imobili�ria"|"Imobili�ria"');
      FListaSQL.Add('-13|13|"Moradores"|"Moradores"');
      FListaSQL.Add('-14|14|"Morador"|"Morador"');
      FListaSQL.Add('-15|15|"Inquilinos"|"Inquilinos"');
      FListaSQL.Add('-16|16|"Inquilino"|"Inquilino"');
      FListaSQL.Add('-17|17|"Unidades Habitacionais"|"Unidades Habitacionais"');
      FListaSQL.Add('-18|18|"Unidades habitacionais"|"Unidades habitacionais"');
      FListaSQL.Add('-19|19|"Unidade Habitacional"|"Unidade Habitacional"');
      FListaSQL.Add('-20|20|"Unidade habitacional"|"Unidade habitacional"');
      FListaSQL.Add('-21|21|"Unidades"|"Unidades"');
      FListaSQL.Add('-22|22|"Unidade"|"Unidade"');
      FListaSQL.Add('-23|23|"Im�veis"|"Im�veis"');
      FListaSQL.Add('-24|24|"Im�vel"|"Im�vel"');
      FListaSQL.Add('-25|25|"UHs"|"UHs"');
      FListaSQL.Add('-26|26|"UH"|"UH"');
      FListaSQL.Add('-27|27|"U.H.s"|"U.H.s"');
      FListaSQL.Add('-28|28|"U.H."|"U.H."');
      FListaSQL.Add('-29|29|"Blocos"|"Blocos"');
      FListaSQL.Add('-30|30|"Bloco"|"Bloco"');
      FListaSQL.Add('-31|31|"Andares"|"Andares"');
      FListaSQL.Add('-32|32|"Andar"|"Andar"');
      FListaSQL.Add('-33|33|"S�ndicos"|"S�ndicos"');
      FListaSQL.Add('-34|34|"S�ndico"|"S�ndico"');
      FListaSQL.Add('-35|35|"da cota condominial"|"da cota condominial"');
      FListaSQL.Add('-36|36|"Cota condominial"|"Cota condominial"');
      FListaSQL.Add('-37|37|"da quota condominial"|"da quota condominial"');
      FListaSQL.Add('-38|38|"Quota condominial"|"Quota condominial"');
      FListaSQL.Add('-39|39|"Condominial"|"Condominial"');
      FListaSQL.Add('-40|40|"Condominiais"|"Condominiais"');
    end;
    //
    PerfJan_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Empresas_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Enti_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Finance_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    ALL_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Anotacoes_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    IBGE_DTB_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Textos_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Cheques_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Moedas_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    CNAB_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Diario_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Protocol_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    UnFPMin_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    UnFPMax_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    UnNFSe_TbTerc.ComplementaListaSQL(Tabela, FListaSQL);
    UnNFSe_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    UnGeral_TbTerc.ComplementaListaSQL(Tabela, FListaSQL);
    UnSMS_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    UnInflIdx_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    WUsers_Tb.ComplementaListaSQL(Tabela, FListaSQL);
    WGerl_Tb.ComplementaListaSQL(Tabela, FListaSQL);
    Arquivos_Tb.ComplementaListaSQL(Tabela, FListaSQL);
    Mail_Tb.ComplementaListaSQL(Tabela, FListaSQL);
    Bloq_0202_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    BloqGerl_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    ConsumoGerl_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Notificacoes_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaIndices(TabelaBase, TabelaNome: String;
  FLIndices: TList<TIndices>): Boolean;
begin
  Result := True;
  try
    if Uppercase(TabelaBase) = Uppercase('Animais') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('ArreRat') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('Cargos') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('CB4Data4') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Empresa';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 2;
      FRIndices.Column_name   := 'Tabela';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('CNAB240Dir') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('CNAB_Dir') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('CNAB240Lei') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('Cond') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('Condgri') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('CondgriImv') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('CondAnimais') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'SubConta';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('CondBloco') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('CondImov') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Conta';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('DirWeb') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('Veic') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'SubConta';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('Filhos') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'SubConta';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('Financas') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('ImpCHMsg') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Tabela';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 2;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('OutMor') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'SubConta';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('PrevRat') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
      //
    {
    end else if Uppercase(TabelaBase) = Uppercase('Protocolos') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('ProtocoOco') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('ProtocoMot') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('ProtocoPak') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('ProtPakIts') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Conta';
      FLIndices.Add(FRIndices);
      //
    }
    end else if Uppercase(TabelaBase) = Uppercase('Status') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'UNIQUE1';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'CodUsu';
      FLIndices.Add(FRIndices);
      // Fazer
{
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'UNIQUE2';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Sigla';
      FLIndices.Add(FRIndices);
      //
}
    end else if Uppercase(TabelaBase) = Uppercase('uploads') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('users') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'User_ID';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('visitasits') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('visitascli') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('wclients') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'User_ID';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('wcond') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('wfotos') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('wfotosedit') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
      //
    end else if Uppercase(TabelaBase) = Uppercase('wslidefts') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else begin
      PerfJan_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Empresas_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Enti_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Finance_Tabs.CarregaListaFRIndices(TabelaBase, TabelaNome, FRIndices, FLIndices);
      ALL_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Anotacoes_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Moedas_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Textos_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Cheques_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      IBGE_DTB_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      //CashTb.CarregaListaFRIndicesCashier(TabelaBase, TabelaNome, FRIndices, FLindices);
      CNAB_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Diario_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Protocol_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      UnFPMin_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      UnFPMax_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      UnNFSe_TbTerc.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      UnNFSe_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      UnGeral_TbTerc.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      UnSMS_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      UnInflIdx_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      WUsers_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      WGerl_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Arquivos_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      Mail_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Bloq_0202_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      BloqGerl_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      ConsumoGerl_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Notificacoes_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
    end;
  except
    raise;
    Result := False;
  end;
end;

procedure TMyListas.ConfiguracoesIniciais(UsaCoresRel: Integer;
  AppIDtxt: String);
begin
  dmkPF.ConfigIniApp(UsaCoresRel);
  if (Uppercase(AppIDtxt) = 'SYNDIC') or (Uppercase(AppIDtxt) = 'SYNDI3') then
  begin
    Entities.ConfiguracoesIniciaisEntidades(CO_DMKID_APP);
  end else
    Geral.MensagemBox(
    'Database para configura��es de "CheckBox" n�o definidos!', 'Aviso!',
    MB_OK+MB_ICONWARNING);
end;

function TMyListas.CriaListaCampos(Tabela: String; FLCampos: TList<TCampos>;
  var TemControle: TTemControle): Boolean;
begin
  TemControle := TemControle + cTemControleNao;
  try
    if Uppercase(Tabela) = Uppercase('ArreRat') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Apto';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Morardor';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Texto';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Valor';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('Cargos') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Descri';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('CB4Data4') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tabela'; // 1="FOTODOC"
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Registro'; // �ltimo registro inserido
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('CNAB240Dir') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Envio';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '2';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Carteira';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CliInt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('CNAB_Dir') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Envio';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '2';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Carteira';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CliInt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('CNAB240Lei') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Banco';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NossoNum';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SeuNum';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDNum';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OcorrCodi';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OcorrData';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValTitul';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValAbati';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValDesco';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValPago';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValJuros';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValMulta';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Motivo1';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Motivo2';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Motivo3';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Motivo4';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Motivo5';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QuitaData';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Diretorio';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Arquivo';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ItemArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Step';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Entidade';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Carteira';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DevJuros';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DevMulta';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('Controle') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
(*
      New(FRCampos);
      FRCampos.Field      := 'TxtCnd_10';  // Caption
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'Condom�nio';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TxtCnd_05';  // Caption
      FRCampos.Tipo       := 'varchar(5)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'Cond.';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TxtUsu_10';  // Caption
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'Cond�mino';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TxtPrp_15';  // Caption
      FRCampos.Tipo       := 'varchar(15)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'Propriet�rio';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TxtRis_25';  // Caption
      FRCampos.Tipo       := 'varchar(25)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'Administradora de riscos';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TxtImb_15';  // Caption
      FRCampos.Tipo       := 'varchar(15)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'Imobili�ria';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TxtMor_15';  // Caption
      FRCampos.Tipo       := 'varchar(15)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'Morador';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TxtMor_20';  // Caption
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'Moradores';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TxtUHs_20';  // Caption
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'Unidade Habitacional';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TxtUHs_10';  // Caption
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'Unidade';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TxtUHs_12';  // Caption
      FRCampos.Tipo       := 'varchar(12)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'Unidades';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TxtUHs_03';  // Caption
      FRCampos.Tipo       := 'char(3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'UH';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TxtUHs_05';  // Caption
      FRCampos.Tipo       := 'varchar(5)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'UHs';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TxtBlc_10';  // Caption
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'Bloco';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TxtBlc_11';  // Caption
      FRCampos.Tipo       := 'varchar(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'Blocos';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TxtNiv_10';  // Caption
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'Andar';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TxtNiv_11';  // Caption
      FRCampos.Tipo       := 'varchar(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'Andares';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TxtSin_10';  // Caption
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'S�ndico';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
*)
      New(FRCampos);
      FRCampos.Field      := 'AtualizouCamposWEB';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtaSincro';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LokMedAnt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Web_Raiz';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MyLogo';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      // Campos WEB
      //
      New(FRCampos);
      FRCampos.Field      := 'Web_Page';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      //   FIM WEB
      //
      New(FRCampos);
      FRCampos.Field      := 'BAL_TopoNomCon';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '2000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BAL_TopoTitulo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '5000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BAL_TopoPeriod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '25000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'InfoSeq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CNAB_Dir';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CNAB240Lei';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CNAB240Dir';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cond';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CondGri';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CondGriImv';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CondBloco';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CondImov';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      (*New(FRCampos);
      FRCampos.Field      := 'ConsPer';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //*)
      {
      New(FRCampos);
      FRCampos.Field      := 'Protocolos';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ProtocoOco';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ProtocoMot';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ProtocoPak';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ProtPakIts';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      }
      New(FRCampos);
      FRCampos.Field      := 'Status';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Veic';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Filhos';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Financas';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OutMor';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ArreRat';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PrevRat';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Animais';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CondAnimais';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cargos';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      //  Fluxo de Bloquetos
      New(FRCampos);
      FRCampos.Field      := 'FlxB_Folha';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxB_LeiAr';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxB_Fecha';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxB_Risco';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxB_Print';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxB_Proto';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxB_Relat';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxB_EMail';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxB_Porta';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxB_Postl';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      //  Fluxo de Encerramento de Per�odo
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxM_Conci';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxM_Docum';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxM_Anali';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxM_Contb';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxM_Encad';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxM_Entrg';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxM_Web';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CB4_Host';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CB4_Port';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '23165';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CB4_User';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := 'PUBLIC';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CB4_Pwd';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CB4_DB';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CB4_Uso';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EntiCargSind'; //Cargo utilizado para o s�ndico
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DdPrtstCrt'; //Protestar ap�s x dias do recebimento de carta.
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('Cond') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cliente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Andares';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TotApt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Observ';
      FRCampos.Tipo       := 'Longtext';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DVAgencia';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DiaVencto';
      FRCampos.Tipo       := 'int(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '5';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BcoLogoPath';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CliLogoPath';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Aceite';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := 'N';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CartConcil';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TxtParecer';
      FRCampos.Tipo       := 'text';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ModelBloq';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MBB'; // Mostra balancete no bloqueto
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MRB'; // Mostra resumo do balancete no bloqueto
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PBB'; // Per�odo do balancete no bloqueto
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MSB'; // Mostra saldos de contas correntes no bloqueto
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MSP'; // Mostra saldos do plano de contas no bloqueto
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PSB'; // Per�odo de saldos de contas no bloqueto
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MIB'; // Mostra inadimpencia bloqueto
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';  // 0=N�o 1=Ambos 2=Cond�mino 3=Condom�nio
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MPB'; // Mostra provis�es no bloqueto
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MAB'; // Mostra composicao de arrecadacao no bloqueto
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PwdSite'; // Mostra no bloqueto senha de acesso ao site (quando houver)
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Prolabore sobre (parte do sal�rio recebedor)
      New(FRCampos);
      FRCampos.Field      := 'ProLaINSSr';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '11.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Prolabore sobre (parte do pagador)
      New(FRCampos);
      FRCampos.Field      := 'ProLaINSSp';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '20.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // 
      New(FRCampos);
      FRCampos.Field      := 'ConfigBol';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '-1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      // ocultar valores de bloqueto no site quando tem cobradora que cuida
      // da cobran�a pelo condom�nio.
      New(FRCampos);
      FRCampos.Field      := 'OcultaBloq';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SomaFracao';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sigla';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BalAgrMens';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Compe';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HideCompe';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      //  Fluxo de Bloquetos
      New(FRCampos);
      FRCampos.Field      := 'FlxB_Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxB_Folha';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxB_LeiAr';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxB_Fecha';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxB_Risco';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxB_Print';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxB_Proto';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxB_Relat';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxB_EMail';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxB_Porta';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxB_Postl';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      //  Fluxo de Encerramento de Per�odo
      New(FRCampos);
      FRCampos.Field      := 'FlxM_Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxM_Conci';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxM_Docum';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxM_Anali';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxM_Contb';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxM_Encad';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxM_Entrg';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FlxM_Web';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Assistente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CfgInfl'; // Configura��o de reajuste por �ndice inflacionario
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NomeSindico';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Multa';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '2.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Juros';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      //Campos WEB - Ini
      New(FRCampos);
      FRCampos.Field      := 'NomeWeb';
      FRCampos.Tipo       := 'varchar(150)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Parc_ValMin';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '100.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Parc_QtdMax';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '6';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Perfil';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '2';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PerfilImv';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      //Campos WEB - Fim
      New(FRCampos);
      FRCampos.Field      := 'MigrouPCNAB_Cfg'; // Indica se o condom�nio foi migrado para o CNAB_Cfg
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('CondGri') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Username';
      FRCampos.Tipo       := 'varchar(32)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Password';
      FRCampos.Tipo       := 'varchar(32)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Email';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LoginID';
      FRCampos.Tipo       := 'varchar(32)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Perfil';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '3';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('CondGriImv') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cond';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Apto';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Propr';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('CondBloco') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Descri';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '99';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SomaFracao';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PrefixoUH';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDExporta';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('CondImov') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Propriet';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Usuario';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conjuge';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Andar';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      (*New(FRCampos);
      FRCampos.Field      := 'NApart';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);*)
      //
      New(FRCampos);
      FRCampos.Field      := 'Unidade';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QtdGaragem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Status';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Imobiliaria';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Contato';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ContTel';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ENome1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EHoraSSIni1';
      FRCampos.Tipo       := 'time';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EHoraSSSai1';
      FRCampos.Tipo       := 'time';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EHoraSDIni1';
      FRCampos.Tipo       := 'time';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EHoraSDSai1';
      FRCampos.Tipo       := 'time';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ESegunda1';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ETerca1';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EQuarta1';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EQuinta1';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ESexta1';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ESabado1';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EDomingo1';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EVeic1';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EFilho1';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ENome2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EHoraSSIni2';
      FRCampos.Tipo       := 'time';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EHoraSSSai2';
      FRCampos.Tipo       := 'time';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EHoraSDIni2';
      FRCampos.Tipo       := 'time';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EHoraSDSai2';
      FRCampos.Tipo       := 'time';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ESegunda2';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ETerca2';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EQuarta2';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EQuinta2';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ESexta2';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ESabado2';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EDomingo2';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EVeic2';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EFilho2';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ENome3';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EHoraSSIni3';
      FRCampos.Tipo       := 'time';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EHoraSSSai3';
      FRCampos.Tipo       := 'time';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EHoraSDIni3';
      FRCampos.Tipo       := 'time';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EHoraSDSai3';
      FRCampos.Tipo       := 'time';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ESegunda3';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ETerca3';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EQuarta3';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EQuinta3';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ESexta3';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ESabado3';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EDomingo3';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EVeic3';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EFilho3';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EmNome1';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EmTel1';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EmNome2';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EmTel2';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Observ';
      FRCampos.Tipo       := 'Longtext';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SitImv';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'WebLogin';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'WebPwd';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'WebNivel';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'WebLogID';
      FRCampos.Tipo       := 'varchar(32)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'WebLastLog';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Protocolo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Protocolo2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Protocolo3';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Moradores';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FracaoIdeal';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ModelBloq';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ConfigBol';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BloqEndTip'; //0: Autom (1/2) 1: Morador 2: Propriet�rio 3: Terceiro 4: Descritivo
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BloqEndEnt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EnderLin1';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EnderLin2';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EnderNome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Procurador';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ddVctEsp';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDExporta';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      {
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      }
      New(FRCampos);
      FRCampos.Field      := 'Juridico';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'InadCEMCad';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SMSCelTipo'; //0: Autom (1/2) 1: Morador 2: Propriet�rio 3: Terceiro 4: Descritivo
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '2';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SMSCelEnti'; // Terceiro
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SMSCelNumr';
      FRCampos.Tipo       := 'varchar(18)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SMSCelNome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('ImpCHMsg') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Tabela';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := 'NIL';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Agora';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Impressora';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodErr';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TipMsg';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodMsg';
      FRCampos.Tipo       := 'varchar(40)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Valor';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Benefic';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('dirweb') then   //Diret�rios FTP
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '';
      FRCampos.Extra      := 'auto_increment';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(32)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Descri';
      FRCampos.Tipo       := 'varchar(32)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pasta';
      FRCampos.Tipo       := 'varchar(32)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nivel';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Fotos';  //Diret�rio de fotos
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('wfotos') then
    begin
      TemControle := TemControle + cTemControleNao;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '';
      FRCampos.Extra      := 'auto_increment';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cond';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('wfotosedit') then
    begin
      TemControle := TemControle + cTemControleNao;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '';
      FRCampos.Extra      := 'auto_increment';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Legenda';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('wslidefts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'URLFoto';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Legenda';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    {
    end else if Uppercase(Tabela) = Uppercase('Protocolos') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tipo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Def_Client';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Def_Sender';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Def_Retorn';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PreEmeio';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Descricao';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sigla';
      FRCampos.Tipo       := 'char(3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '???';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('ProtocoOco') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('ProtocoMot') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('ProtocoPak') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';    // Protocolo
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataI';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataL';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataF';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Mez';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CNAB_Cfg';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SeqArq'; // Seq��ncia do arquivo de remessa CNAB
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('ProtPakIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Link_ID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CliInt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cliente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cedente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Fornece';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Periodo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Lancto';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Docum';
      FRCampos.Tipo       := 'double(20,0)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Depto';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataE';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataD';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Retorna';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cancelado';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Motivo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ID_Cod1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ID_Cod2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ID_Cod3';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ID_Cod4';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Vencto'; // Vencimento do do documento
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Valor'; // Valor do documento
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MoraDiaVal';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MultaVal';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ComoConf'; // 1=Manual(For�ado), 2=Emeio, 3=J� pagou
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SerieCH';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Manual';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Texto';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Saiu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Recebeu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Retornou';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LimiteSai';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LimiteRem';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LimiteRet';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataSai';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataRec';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataRet';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    }
    end else if Uppercase(Tabela) = Uppercase('Status') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Descri';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sigla';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';//'UNI';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('Animais') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Descri';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('CondAnimais') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SubConta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Animal';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Descri';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Qtd';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('Veic') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SubConta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cor';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Placa';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoM';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoF';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('Filhos') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SubConta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataNasc';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('Financas') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cliente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Periodo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Credito';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Debito';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataE';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataL';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataV';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('OutMor') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SubConta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataNasc';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Parentesco';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('PrevRat') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Apto';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Morardor';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Texto';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Valor';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('uploads') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '';
      FRCampos.Extra      := 'auto_increment';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(32)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Arquivo';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tags';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataHora';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cond';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DirWeb';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataExp'; //Data de expira��o do arquivo
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('users') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'User_ID'; // Cadastro do usu�rio na Web
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '';
      FRCampos.Extra      := 'auto_increment';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodCliEsp'; // Condominio > QrCondCodigo
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodigoEsp';  // Unidade -> QrCondImovConta
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Username';
      FRCampos.Tipo       := 'varchar(32)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Password';
      FRCampos.Tipo       := 'varchar(32)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LoginID';
      FRCampos.Tipo       := 'varchar(32)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LastAcess';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tipo';
(*
      case 1: $NomeNivel = 'Morador';
      case 2: $NomeNivel = 'Teste';
      case 3: $NomeNivel = 'cliente ';
      case 9: $NomeNivel = 'admin ';
*)
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Email';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('visitas') then
    begin
      TemControle := TemControle + cTemControleNao;
      //
      New(FRCampos);
      FRCampos.Field      := 'IP';
      FRCampos.Tipo       := 'varchar(64)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0.0.0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Visitas';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Primeira';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ultima';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('visitasits') then
    begin
      TemControle := TemControle + cTemControleNao;
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '';
      FRCampos.Extra      := 'auto_increment';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IP';
      FRCampos.Tipo       := 'varchar(64)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0.0.0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataHora';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('visitascli') then
    begin
      TemControle := TemControle + cTemControleNao;
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '';
      FRCampos.Extra      := 'auto_increment';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IP';
      FRCampos.Tipo       := 'varchar(64)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0.0.0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataHora';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Usuario';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tipo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('wclients') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'User_ID'; // Cadastro do condominio na Web
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '';
      FRCampos.Extra      := 'auto_increment';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodCliEsp';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NomeWeb';
      FRCampos.Tipo       := 'varchar(150)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '? ? ?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Parc_ValMin';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '100.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Parc_QtdMax';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '6';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Multa';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '2.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Juros';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Username';
      FRCampos.Tipo       := 'varchar(32)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Password';
      FRCampos.Tipo       := 'varchar(32)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LoginID';
      FRCampos.Tipo       := 'varchar(32)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NomeSindico';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Email';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('wcond') then
    begin
      TemControle := TemControle + cTemControleNao;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MsgNoImpBlq'; //Texto para os condom�nios que n�o
      FRCampos.Tipo       := 'int(11)';     //ir�o imprimir bloquetos
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
    end else
    begin
      PerfJan_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Empresas_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Enti_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Finance_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      ALL_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Anotacoes_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Moedas_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Textos_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Cheques_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      IBGE_DTB_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      //CashTb.CarregaListaFRCamposCashier(Tabela, FRCampos, FLCampos, TemControle);
      CNAB_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Diario_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, temControle);
      Protocol_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      UnFPMin_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      UnFPMax_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      UnNFSe_TbTerc.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      UnNFSe_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      UnGeral_TbTerc.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      UnSMS_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      UnInflIdx_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      WUsers_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle, CO_DMKID_APP);
      WGerl_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle, CO_DMKID_APP);
      Arquivos_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Mail_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Bloq_0202_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      BloqGerl_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      ConsumoGerl_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Notificacoes_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
    end;
    //
    PerfJan_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Empresas_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Enti_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Finance_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    ALL_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Anotacoes_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Moedas_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Cheques_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    //CashTb.CompletaListaFRCamposCashier(Tabela, FRCampos, FLCampos);
    CNAB_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Diario_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Protocol_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    UnFPMin_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    UnFPMax_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    UnNFSe_TbTerc.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    UnNFSe_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    UnGeral_TbTerc.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    UnSMS_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    UnInflIdx_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    WUsers_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    WGerl_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Arquivos_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Mail_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Bloq_0202_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
    BloqGerl_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
    ConsumoGerl_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Notificacoes_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    //
    Result := True;
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.ExcluiTab: Boolean;
begin
  Result := True;
end;

function TMyListas.ExcluiReg: Boolean;
begin
  Result := True;
end;

function TMyListas.ExcluiIdx: Boolean;
begin
  Result := True;
end;

function TMyListas.CriaListaUserSets(FUserSets: TStringList): Boolean;
begin
  Result := True;
  try
    FUserSets.Add('Edi��o de Itens de Mercadoria;C�digos Fiscais');
    FUserSets.Add('Edi��o de Itens de Mercadoria;Comiss�o de Representante');
    FUserSets.Add('Edi��o de Itens de Mercadoria;Comiss�o de Vendedor');
    FUserSets.Add('Edi��o de Itens de Mercadoria;IPI');
    FUserSets.Add('Edi��o de Itens de Mercadoria;ICMS');
    //
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaImpDOS(FImpDOS: TStringList): Boolean;
begin
  Result := True;
  try
    FImpDOS.Add('1001;01;NF (Topo)');
    FImpDOS.Add('1002;01;Sa�da [X]');
    FImpDOS.Add('1003;01;Entrada [X]');
    FImpDOS.Add('1004;01;Data emiss�o');
    FImpDOS.Add('1005;01;Data entra/sai');
    FImpDOS.Add('1006;01;C�digo CFOP');
    FImpDOS.Add('1007;01;Descri��o CFOP');
    FImpDOS.Add('1008;01;Base c�lculo ICMS');
    FImpDOS.Add('1009;01;Valor ICMS');
    FImpDOS.Add('1010;01;Base c�lc. ICMS subst.');
    FImpDOS.Add('1011;01;Valor ICMS subst.');
    FImpDOS.Add('1012;01;Valor frete');
    FImpDOS.Add('1013;01;Valor seguro');
    FImpDOS.Add('1014;01;Outras desp. aces.');
    FImpDOS.Add('1015;01;Valor total IPI');
    FImpDOS.Add('1016;01;Valor total produtos');
    FImpDOS.Add('1017;01;Valor total servicos');
    FImpDOS.Add('1018;01;Valor total nota');
    FImpDOS.Add('1019;01;Placa ve�culo');
    FImpDOS.Add('1020;01;UF placa ve�culo');
    FImpDOS.Add('1021;01;Vol. Quantidade');
    FImpDOS.Add('1022;01;Vol. Esp�cie');
    FImpDOS.Add('1023;01;Vol. Marca');
    FImpDOS.Add('1024;01;Vol. N�mero');
    FImpDOS.Add('1025;01;Vol. kg bruto');
    FImpDOS.Add('1026;01;Vol. kg l�quido');
    FImpDOS.Add('1027;01;Dados adicionais');
    FImpDOS.Add('1028;01;Frete por conta de ...');
    FImpDOS.Add('1029;01;Desconto especial');
    FImpDOS.Add('1030;01;NF (rodap�)');
    //
    FImpDOS.Add('2001;02;Nome ou Raz�o Social');
    FImpDOS.Add('2002;02;CNPJ ou CPF');
    FImpDOS.Add('2003;02;Endere�o');
    FImpDOS.Add('2004;02;Bairro');
    FImpDOS.Add('2005;02;CEP');
    FImpDOS.Add('2006;02;Cidade');
    FImpDOS.Add('2007;02;Telefone');
    FImpDOS.Add('2008;02;UF');
    FImpDOS.Add('2009;02;I.E. ou RG');
    FImpDOS.Add('2010;02;I.E.S.T.');
    //
    FImpDOS.Add('6001;06;Nome ou Raz�o Social');
    FImpDOS.Add('6002;06;CNPJ ou CPF');
    FImpDOS.Add('6003;06;Endere�o');
    FImpDOS.Add('6004;06;Bairro');
    FImpDOS.Add('6005;06;CEP');
    FImpDOS.Add('6006;06;Cidade');
    FImpDOS.Add('6007;06;Telefone');
    FImpDOS.Add('6008;06;UF');
    FImpDOS.Add('6009;06;I.E. ou RG');
    FImpDOS.Add('6010;06;I.E.S.T.');
    //
    FImpDOS.Add('3001;03;Descri��o');
    FImpDOS.Add('3002;03;Classifica��o Fiscal');
    FImpDOS.Add('3003;03;Situa��o Tribut�ria');
    FImpDOS.Add('3004;03;Unidade');
    FImpDOS.Add('3005;03;Quantidade');
    FImpDOS.Add('3006;03;Valor unit�rio');
    FImpDOS.Add('3007;03;Valor total');
    FImpDOS.Add('3008;03;Aliquota ICMS');
    FImpDOS.Add('3009;03;Aliquota IPI');
    FImpDOS.Add('3010;03;Valor IPI');
    FImpDOS.Add('3011;03;CFOP');
    FImpDOS.Add('3012;03;Refer�ncia');
    //
    FImpDOS.Add('4001;04;Descri��o');
    FImpDOS.Add('4002;04;Classifica��o Fiscal');
    FImpDOS.Add('4003;04;Situa��o Tribut�ria');
    FImpDOS.Add('4004;04;Unidade');
    FImpDOS.Add('4005;04;Quantidade');
    FImpDOS.Add('4006;04;Valor unit�rio');
    FImpDOS.Add('4007;04;Valor total');
    FImpDOS.Add('4008;04;Aliquota ICMS');
    FImpDOS.Add('4009;04;Aliquota IPI');
    FImpDOS.Add('4010;04;Valor IPI');
    FImpDOS.Add('4011;04;CFOP');
    FImpDOS.Add('4012;04;Refer�ncia');
    //
    FImpDOS.Add('5001;05;Parcela');
    FImpDOS.Add('5002;05;Valor');
    FImpDOS.Add('5003;05;Vencimento');
    //
  except
    raise;
    Result := False;
  end;
end;

procedure TMyListas.VerificaOutrosAntes(DataBase: TmySQLDatabase; Memo: TMemo);
begin
  //
end;

procedure TMyListas.VerificaOutrosDepois(DataBase: TmySQLDatabase; Memo: TMemo);
begin
  // Nada
end;

procedure TMyListas.ModificaDadosDeIndicesAlterados(Indice, Tabela: String;
  DataBase: TmySQLDatabase; Memo: TMemo);
begin
  // Nada
end;

function TMyListas.CriaListaQeiLnk(TabelaCria: String;
  FLQeiLnk: TList<TQeiLnk>): Boolean;
begin
  Result := True;
  try
(*
    if Uppercase(TabelaCria) = Uppercase('VSEntiMP') then
    begin
      New(FRQeiLnk);
      FRQeiLnk.AskrTab       := TabelaCria;
      FRQeiLnk.AskrCol       := 'Codigo';
      FRQeiLnk.RplyTab       := '';
      FRQeiLnk.RplyCol       := '';
      FRQeiLnk.Purpose       := TItemTuplePurpose.itpUSGSrvrInc;
      FRQeiLnk.Seq_in_pref   := 1;
      FLQeiLnk.Add(FRQeiLnk);
      //
      New(FRQeiLnk);
      FRQeiLnk.AskrTab       := TabelaCria;
      FRQeiLnk.AskrCol       := 'MPVImpOpc';
      FRQeiLnk.RplyTab       := 'MPVImpOpc';
      FRQeiLnk.RplyCol       := 'Codigo';
      FRQeiLnk.Purpose       := TItemTuplePurpose.itpRelatUsrSrv;
      FRQeiLnk.Seq_in_pref   := 1;
      FLQeiLnk.Add(FRQeiLnk);
      //
      //
    //end else if Uppercase(TabelaBase) = Uppercase('?) then
    //begin
    end else
*)
    begin
      All_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      Enti_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      PerfJan_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      Finance_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      IBGE_DTB_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      Mail_Tb.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      UnGeral_TbTerc.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      //
  	  Moedas_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      CNAB_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      Anotacoes_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      Empresas_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      //
      UnNFSe_TbTerc.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      //
    end;
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaJanelas(FLJanelas: TList<TJanelas>): Boolean;
begin
  // CB4-GDATA-001 :: Gerenciamento de Tabelas CB4
  New(FRJanelas);
  FRJanelas.ID        := 'CB4-GDATA-001';
  FRJanelas.Nome      := 'FmCond';
  FRJanelas.Descricao := 'Cadastro de Condom�nios';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-CONDO-001 :: Cadastro de Condom�nios
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-CONDO-001';
  FRJanelas.Nome      := 'FmCond';
  FRJanelas.Descricao := 'Cadastro de Condom�nios';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-CONDO-003 :: Blocos
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-CONDO-003';
  FRJanelas.Nome      := 'FmCondBloco';
  FRJanelas.Descricao := 'Blocos';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-CONDO-004 :: Im�veis
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-CONDO-004';
  FRJanelas.Nome      := 'FmCondImov';
  FRJanelas.Descricao := 'Im�veis';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-CONDO-005 :: Fluxos
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-CONDO-005';
  FRJanelas.Nome      := 'FmCondFlx';
  FRJanelas.Descricao := 'Fluxos';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-CONDO-006 :: Cadastro R�pido
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-CONDO-006';
  FRJanelas.Nome      := 'FmEntiRapidoCond';
  FRJanelas.Descricao := 'Cadastro R�pido';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-CONDO-009 :: Sele��o de UHs - Modelo e Config. Bloq.
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-CONDO-009';
  FRJanelas.Nome      := 'FmAptosModBol';
  FRJanelas.Descricao := 'Sele��o de UHs - Modelo e Config. Bloq.';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-CONDO-012 :: Impress�o de Im�veis
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-CONDO-012';
  FRJanelas.Nome      := 'FmCondImovImp';
  FRJanelas.Descricao := 'Impress�o de Im�veis';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-CONDO-013 :: Gerenciamento de U.H. / Cond�mino
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-CONDO-013';
  FRJanelas.Nome      := 'FmPesqCond';
  FRJanelas.Descricao := 'Gerenciamento de U.H. / Cond�mino';
  FLJanelas.Add(FRJanelas);
  //
  (*
  // CAD-ARREC-001 :: Cadastro de Arrecada��es Bases
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-ARREC-001';
  FRJanelas.Nome      := 'FmArreBaC';
  FRJanelas.Descricao := 'Cadastro de Arrecada��es Bases';
  FLJanelas.Add(FRJanelas);
  //
  *)
  // CAD-ARREC-002 :: Arrecada��o - Inclui Unidade
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-ARREC-002';
  FRJanelas.Nome      := 'FmArreBaCAddUni';
  FRJanelas.Descricao := 'Arrecada��o - Inclui Unidade';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-ARREC-003 :: Arrecada��o - Sele��o de Unidades
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-ARREC-003';
  FRJanelas.Nome      := 'FmArreBaCAptos';
  FRJanelas.Descricao := 'Arrecada��o - Sele��o de Unidades';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-ARREC-004 ::
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-ARREC-004';
  FRJanelas.Nome      := 'FmArreBaC';
  FRJanelas.Descricao := '';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-ARREC-005 ::
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-ARREC-005';
  FRJanelas.Nome      := 'FmArreBaC';
  FRJanelas.Descricao := '';
  FLJanelas.Add(FRJanelas);
  //
  (*
  // CAD-PROVI-001 :: Cadastro de Provis�es Bases
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-PROVI-001';
  FRJanelas.Nome      := 'FmPrevBaC';
  FRJanelas.Descricao := 'Cadastro de Provis�es Bases';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-PROVI-002 :: Agendamento de Provis�o
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-PROVI-002';
  FRJanelas.Nome      := 'FmPrevBaCLctos';
  FRJanelas.Descricao := 'Agendamento de Provis�o';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-PROVI-003 :: Agendamento de Provis�o
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-PROVI-003';
  FRJanelas.Nome      := 'FmCondGerAgefet';
  FRJanelas.Descricao := 'Agendamento de Provis�o';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-PROVI-004 :: Item de Provis�o Base
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-PROVI-004';
  FRJanelas.Nome      := 'FmPrevBaI';
  FRJanelas.Descricao := 'Item de Provis�o Base';
  FLJanelas.Add(FRJanelas);
  //
  *)
  (*
  // CFG-BOLET-001 :: Cfg. impress�o de boleto
  New(FRJanelas);
  FRJanelas.ID        := 'CFG-BOLET-001';
  FRJanelas.Nome      := 'FmConfigBol';
  FRJanelas.Descricao := 'Cfg. impress�o de boleto';
  FLJanelas.Add(FRJanelas);
  //
  *)
  // CAD-STATU-001 :: Status
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-STATU-001';
  FRJanelas.Nome      := 'FmStatus';
  FRJanelas.Descricao := 'Status';
  FLJanelas.Add(FRJanelas);
  //
  // CIA-GEREN-001 :: Cobran�a de Inadimplentes Automatizada
  New(FRJanelas);
  FRJanelas.ID        := 'CIA-GEREN-001';
  FRJanelas.Nome      := 'FmCobrarGer';
  FRJanelas.Descricao := 'Cobran�a de Inadimplentes Automatizada';
  FLJanelas.Add(FRJanelas);
  //
  // CIA-GEREN-003 :: Confirma��o de Impress�o de Cartas
  New(FRJanelas);
  FRJanelas.ID        := 'CIA-GEREN-003';
  FRJanelas.Nome      := 'FmCartasEnv';
  FRJanelas.Descricao := 'Confirma��o de Impress�o de Cartas';
  FLJanelas.Add(FRJanelas);
  //
  // WEB-OPCOE-002 :: Op��es Espec�ficas do Meu Site
  New(FRJanelas);
  FRJanelas.ID        := 'WEB-OPCOE-002';
  FRJanelas.Nome      := 'FmOpcoesSyndic';
  FRJanelas.Descricao := 'Op��es Espec�ficas do Meu Site';
  FRJanelas.Modulo    := 'WEB';
  FLJanelas.Add(FRJanelas);
  //
  // WEB-VISIT-001 :: Controle de usu�rios web
  New(FRJanelas);
  FRJanelas.ID        := 'WEB-VISIT-001';
  FRJanelas.Nome      := 'FmVisitasCli';
  FRJanelas.Descricao := 'Controle de usu�rios web';
  FRJanelas.Modulo    := 'WEB';
  FLJanelas.Add(FRJanelas);
  //
  // WEB-DIRET-001 :: Diret�rios WEB
  New(FRJanelas);
  FRJanelas.ID        := 'WEB-DIRET-001';
  FRJanelas.Nome      := 'FmDirWeb';
  FRJanelas.Descricao := 'Diret�rios WEB';
  FRJanelas.Modulo    := 'WEB';
  FLJanelas.Add(FRJanelas);
  //
  // WEB-UPLOA-001 :: Uploads de arquivos
  New(FRJanelas);
  FRJanelas.ID        := 'WEB-UPLOA-001';
  FRJanelas.Nome      := 'FmUploads';
  FRJanelas.Descricao := 'Uploads de arquivos';
  FRJanelas.Modulo    := 'WEB';
  FLJanelas.Add(FRJanelas);
  //
  // WEB-UPLOA-002 :: Uploads de arquivos - Itens
  New(FRJanelas);
  FRJanelas.ID        := 'WEB-UPLOA-002';
  FRJanelas.Nome      := 'FmUploadsIts';
  FRJanelas.Descricao := 'Uploads de arquivos - Itens';
  FRJanelas.Modulo    := 'WEB';
  FLJanelas.Add(FRJanelas);
  //
  // FER-OPCAO-002 :: Op��es Espec�ficas do Aplicativo
  New(FRJanelas);
  FRJanelas.ID        := 'FER-OPCAO-002';
  FRJanelas.Nome      := 'FmOpcoesSyndic';
  FRJanelas.Descricao := 'Op��es Espec�ficas do Aplicativo';
  FLJanelas.Add(FRJanelas);
  //
  {
  // CND-OCORR-001 :: Impress�o do Livro de Ocorr�ncias
  New(FRJanelas);
  FRJanelas.ID        := 'CND-OCORR-001';
  FRJanelas.Nome      := 'FmOcorrImp';
  FRJanelas.Descricao := 'Impress�o do Livro de Ocorr�ncias';
  FLJanelas.Add(FRJanelas);
  }
  //
  // CND-CADAS-002 :: Ficha Cadastral de Moradores
  New(FRJanelas);
  FRJanelas.ID        := 'CND-CADAS-002';
  FRJanelas.Nome      := 'FmCondFichaCad';
  FRJanelas.Descricao := 'Ficha Cadastral de Moradores';
  FLJanelas.Add(FRJanelas);
  //
  // FLX-ENCER-001 :: Sele��o de Fluxo
  New(FRJanelas);
  FRJanelas.ID        := 'FLX-ENCER-001';
  FRJanelas.Nome      := 'FmFlxMensSel';
  FRJanelas.Descricao := 'Sele��o de Fluxo';
  FLJanelas.Add(FRJanelas);
  //
  // FLX-ENCER-002 :: Confirma��o de Passo
  New(FRJanelas);
  FRJanelas.ID        := 'FLX-ENCER-002';
  FRJanelas.Nome      := 'FmFlxMensAti';
  FRJanelas.Descricao := 'Confirma��o de Passo';
  FLJanelas.Add(FRJanelas);
  //
  // FLX-BALAN-001 :: Abertura de Fluxo de Balancete
  New(FRJanelas);
  FRJanelas.ID        := 'FLX-BALAN-001';
  FRJanelas.Nome      := 'FmFlxMensBalAdd';
  FRJanelas.Descricao := 'Abertura de Fluxo de Balancete';
  FLJanelas.Add(FRJanelas);
  //
  // FLX-BALAN-002 :: Confirma��o de Etapa Mensal
  New(FRJanelas);
  FRJanelas.ID        := 'FLX-BALAN-002';
  FRJanelas.Nome      := 'FmFlxMensBalUpd';
  FRJanelas.Descricao := 'Confirma��o de Etapa Mensal';
  FLJanelas.Add(FRJanelas);
  //
  // FLX-BALAN-003 :: Panorama do Fluxo dos Balancetes
  New(FRJanelas);
  FRJanelas.ID        := 'FLX-BALAN-003';
  FRJanelas.Nome      := 'FmFlxMensBalViw';
  FRJanelas.Descricao := 'Panorama do Fluxo dos Balancetes';
  FLJanelas.Add(FRJanelas);
  //
  // FLX-BALAN-004 :: Novo Per�odo de Fluxo de Balancetes
  New(FRJanelas);
  FRJanelas.ID        := 'FLX-BALAN-004';
  FRJanelas.Nome      := 'FmFlxMensBalNew';
  FRJanelas.Descricao := 'Novo Per�odo de Fluxo de Balancetes';
  FLJanelas.Add(FRJanelas);
  //
  // FLX-BALAN-005 :: Novo Per�odo de Fluxo de Bloquetos
  New(FRJanelas);
  FRJanelas.ID        := 'FLX-BALAN-005';
  FRJanelas.Nome      := 'FmFlxMensBlqNew';
  FRJanelas.Descricao := 'Novo Per�odo de Fluxo de Bloquetos';
  FLJanelas.Add(FRJanelas);
  //
  // FLX-BLOQU-001 :: Abertura de Fluxo de Bloqueto
  New(FRJanelas);
  FRJanelas.ID        := 'FLX-BLOQU-001';
  FRJanelas.Nome      := 'FmFlxMensBlqAdd';
  FRJanelas.Descricao := 'Abertura de Fluxo de Bloqueto';
  FLJanelas.Add(FRJanelas);
  //
  // FLX-BLOQU-002 :: Confirma��o de Etapa Mensal
  New(FRJanelas);
  FRJanelas.ID        := 'FLX-BLOQU-002';
  FRJanelas.Nome      := 'FmFlxMensBlqUpd';
  FRJanelas.Descricao := 'Confirma��o de Etapa Mensal';
  FLJanelas.Add(FRJanelas);
  //
  // FLX-BLOQU-003 :: Panorama do Fluxo dos Bloquetos
  New(FRJanelas);
  FRJanelas.ID        := 'FLX-BLOQU-003';
  FRJanelas.Nome      := 'FmFlxMensBlqViw';
  FRJanelas.Descricao := 'Panorama do Fluxo dos Bloquetos';
  FLJanelas.Add(FRJanelas);
  //
  //
  {
  New(FRJanelas);
  FRJanelas.ID        := 'GER-PROTO-001';
  FRJanelas.Nome      := 'FmProtocolos';
  FRJanelas.Descricao := 'Cadastro e Gerenciamento de Protocolos';
  FLJanelas.Add(FRJanelas);
  //
  New(FRJanelas);
  FRJanelas.ID        := 'GER-PROTO-002';
  FRJanelas.Nome      := 'FmProtocoOco';
  FRJanelas.Descricao := 'Ocorr�ncias de Protocolos';
  FLJanelas.Add(FRJanelas);
  //
  New(FRJanelas);
  FRJanelas.ID        := 'GER-PROTO-003';
  FRJanelas.Nome      := 'FmProtocoMot';
  FRJanelas.Descricao := 'Motivos de N�o Entrega de Protocolos';
  FLJanelas.Add(FRJanelas);
  //
  New(FRJanelas);
  FRJanelas.ID        := 'GER-PROTO-004';
  FRJanelas.Nome      := 'FmProtocoPak';
  FRJanelas.Descricao := 'Novo Lote de Protocolos';
  FLJanelas.Add(FRJanelas);
  //
  New(FRJanelas);
  FRJanelas.ID        := 'GER-PROTO-005';
  FRJanelas.Nome      := 'FmProtocoMan';
  FRJanelas.Descricao := 'Item de Protocolos Manual';
  FLJanelas.Add(FRJanelas);
  //
  }
  // GER-CONDM-000 :: Sele��o de Condom�nio
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-000';
  FRJanelas.Nome      := 'FmCondSel';
  FRJanelas.Descricao := 'Sele��o de Condom�nio';
  FLJanelas.Add(FRJanelas);
  //
  (*
  // GER-CONDM-002 :: Exclus�o de Arrecada��es
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-002';
  FRJanelas.Nome      := 'FmCondGerDelArre';
  FRJanelas.Descricao := 'Exclus�o de Arrecada��es';
  FLJanelas.Add(FRJanelas);
  //
  *)
  // GER-CONDM-003 :: Impress�o de Carn� de Bloquetos
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-003';
  FRJanelas.Nome      := 'FmCondGerCarne';
  FRJanelas.Descricao := 'Impress�o de Carn� de Bloquetos';
  FLJanelas.Add(FRJanelas);
  //
  (*
  // GER-CONDM-004 :: Agendamento por Varredura de Lan�amentos
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-004';
  FRJanelas.Nome      := 'FmPrevVeri';
  FRJanelas.Descricao := 'Agendamento por Varredura de Lan�amentos';
  FLJanelas.Add(FRJanelas);
  //
  *)
  (*
  // GER-CONDM-005 :: Sele��o de Provis�o Base
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-005';
  FRJanelas.Nome      := 'FmPrevVeriBaC';
  FRJanelas.Descricao := 'Sele��o de Provis�o Base';
  FLJanelas.Add(FRJanelas);
  //
  *)
  // GER-CONDM-006 :: Impress�o de Arrecada��es
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-006';
  FRJanelas.Nome      := 'FmCondGerImpArre';
  FRJanelas.Descricao := 'Impress�o de Arrecada��es';
  FLJanelas.Add(FRJanelas);
  //
  // GER-CONDM-007 :: Desfazimento de Processamento de Item de Retorno CNAB
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-007';
  FRJanelas.Nome      := 'FmCondGerStep';
  FRJanelas.Descricao := 'Desfazimento de Processamento de Item de Retorno CNAB';
  FLJanelas.Add(FRJanelas);
  //
  (*
  // GER-CONDM-008 :: Provis�es Agendadas
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-008';
  FRJanelas.Nome      := 'FmPrevBaA';
  FRJanelas.Descricao := 'Provis�es Agendadas';
  FLJanelas.Add(FRJanelas);
  //
  *)
  (*
  // GER-CONDM-009 :: Adi��o de Contas Bases no Or�amento
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-009';
  FRJanelas.Nome      := 'FmPrevBaB';
  FRJanelas.Descricao := 'Adi��o de Contas Bases no Or�amento';
  FLJanelas.Add(FRJanelas);
  //
  *)
  // GER-CONDM-010 :: Protocolos de Entrega de Bloqueto
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-010';
  FRJanelas.Nome      := 'FmCondGerProto';
  FRJanelas.Descricao := 'Protocolos de Entrega de Bloqueto';
  FLJanelas.Add(FRJanelas);
  //
  (*
  // GER-CONDM-011 :: Item de Or�amento
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-011';
  FRJanelas.Nome      := 'FmCondGerPrev';
  FRJanelas.Descricao := 'Item de Or�amento';
  FLJanelas.Add(FRJanelas);
  //
  *)
  (*
  // GER-CONDM-012 :: Novo Or�amento
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-012';
  FRJanelas.Nome      := 'FmCondGerNew';
  FRJanelas.Descricao := 'Novo Or�amento';
  FLJanelas.Add(FRJanelas);
  //
  *)
  (*
  // GER-CONDM-013 :: Modelo de Bloqueto
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-013';
  FRJanelas.Nome      := 'FmCondGerModelBloq';
  FRJanelas.Descricao := 'Modelo de Bloqueto';
  FLJanelas.Add(FRJanelas);
  //
  *)
  // GER-CONDM-014 :: Per�odo do Or�amento
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-014';
  FRJanelas.Nome      := 'FmCondGerLocper';
  FRJanelas.Descricao := 'Per�odo do Or�amento';
  FLJanelas.Add(FRJanelas);
  //
  (*
  // GER-CONDM-016 :: Rateio de Consumo
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-016';
  FRJanelas.Nome      := 'FmCondGerLei2';
  FRJanelas.Descricao := 'Rateio de Consumo';
  FLJanelas.Add(FRJanelas);
  //
  *)
  (*
  // GER-CONDM-017 :: Leituras de Produtos Consumidos
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-017';
  FRJanelas.Nome      := 'FmCondGerLei1';
  FRJanelas.Descricao := 'Leituras de Produtos Consumidos';
  FLJanelas.Add(FRJanelas);
  //
  *)
  // GER-CONDM-018 :: Impress�es do Gerenciamento de Comdom�nios
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-018';
  FRJanelas.Nome      := 'FmCondGerImpGer2';
  FRJanelas.Descricao := 'Impress�es do Gerenciamento de Comdom�nios';
  FLJanelas.Add(FRJanelas);
  //
  // GER-CONDM-019 :: Relat�rio de Arrecada��es (Lista de Cond�minos)
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-019';
  FRJanelas.Nome      := 'FmCondGerImpGer2a';
  FRJanelas.Descricao := 'Relat�rio de Arrecada��es (Lista de Cond�minos)';
  FLJanelas.Add(FRJanelas);
  //
  // GER-CONDM-020 :: Largura Coluna
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-020';
  FRJanelas.Nome      := 'FmCondGerImpGerLar';
  FRJanelas.Descricao := 'Largura Coluna';
  FLJanelas.Add(FRJanelas);
  //
  // GER-CONDM-021 :: Impress�es do Gerenciamento de Comdom�nios
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-021';
  FRJanelas.Nome      := 'FmCondGerImpGer';
  FRJanelas.Descricao := 'Impress�es do Gerenciamento de Comdom�nios';
  FLJanelas.Add(FRJanelas);
  //
  (*
  // GER-CONDM-022 :: Adi��o de Contas Bases no Or�amento
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-022';
  FRJanelas.Nome      := 'FmArreBaA';
  FRJanelas.Descricao := 'Adi��o de Contas Bases no Or�amento';
  FLJanelas.Add(FRJanelas);
  //
  *)
  (*
  // GER-CONDM-023 :: Arrecada��es Duplicadas
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-023';
  FRJanelas.Nome      := 'FmArreBaADupl';
  FRJanelas.Descricao := 'Arrecada��es Duplicadas';
  FLJanelas.Add(FRJanelas);
  //
  *)
  // GER-CONDM-028 :: Envio de Bloquetos por Emeio
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-028';
  FRJanelas.Nome      := 'FmEmailBloqueto';
  FRJanelas.Descricao := 'Envio de Bloquetos por Emeio';
  FLJanelas.Add(FRJanelas);
  //
  // GER-CONDM-029 :: Escolha de Bloqueto
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-029';
  FRJanelas.Nome      := 'FmCondGerBolSel';
  FRJanelas.Descricao := 'Escolha de Bloqueto';
  FLJanelas.Add(FRJanelas);
  //
  // GER-CONDM-030 :: Informa��es que Precisam de Aten��o
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-030';
  FRJanelas.Nome      := 'FmAtencoes';
  FRJanelas.Descricao := 'Informa��es que Precisam de Aten��o';
  FLJanelas.Add(FRJanelas);
  //
  // GER-CONDM-031 :: Limpeza de Pacelamentos Exclu�dos
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-031';
  FRJanelas.Nome      := 'FmBloqParcDel';
  FRJanelas.Descricao := 'Limpeza de Pacelamentos Exclu�dos';
  FLJanelas.Add(FRJanelas);
  //
  // GER-CONDM-032 :: Localiza Lote de Protocolo
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-032';
  FRJanelas.Nome      := 'FmCondGerProtoLoc';
  FRJanelas.Descricao := 'Localiza Lote de Protocolo';
  FLJanelas.Add(FRJanelas);
  //
  // GER-CONDM-033 :: Gera Arquivo CNAB
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-033';
  FRJanelas.Nome      := 'FmGeraCNAB';
  FRJanelas.Descricao := 'Gera Arquivo CNAB';
  FLJanelas.Add(FRJanelas);
  //
  // GER-CONDM-034 :: Valor de Boleto - Gerencial X Financeiro
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-034';
  FRJanelas.Nome      := 'FmVerifiBloquetosValor';
  FRJanelas.Descricao := 'Valor de Boleto - Gerencial X Financeiro';
  FLJanelas.Add(FRJanelas);
  //
  // GER-LANCT-001 :: Lan�amentos a Vencer
  New(FRJanelas);
  FRJanelas.ID        := 'GER-LANCT-001';
  FRJanelas.Nome      := 'FmAVencer';
  FRJanelas.Descricao := 'Lan�amentos a Vencer';
  FLJanelas.Add(FRJanelas);
  //
  // GER-PROPR-001 :: Pesquisa Pagamentos
  New(FRJanelas);
  FRJanelas.ID        := 'GER-PROPR-001';
  FRJanelas.Nome      := 'FmInadimp';
  FRJanelas.Descricao := 'Pesquisa pagamentos';
  FLJanelas.Add(FRJanelas);
  //
  // GER-PROPR-002 :: Grupos de Unidades Habitacionais
  New(FRJanelas);
  FRJanelas.ID        := 'GER-PROPR-002';
  FRJanelas.Nome      := 'FmCondGri';
  FRJanelas.Descricao := 'Grupos de Unidades Habitacionais';
  FLJanelas.Add(FRJanelas);
  //
  // GER-PROPR-003 :: Impress�o de Certid�es e Cartas
  New(FRJanelas);
  FRJanelas.ID        := 'GER-PROPR-003';
  FRJanelas.Nome      := 'FmCertidaoNeg';
  FRJanelas.Descricao := 'Impress�o de Certid�es e Cartas';
  FLJanelas.Add(FRJanelas);
  //
  // GER-PROPR-004 :: Bloquetos Inadimplentes
  New(FRJanelas);
  FRJanelas.ID        := 'GER-PROPR-004';
  FRJanelas.Nome      := 'FmInadimpBloq';
  FRJanelas.Descricao := 'Bloquetos Inadimplentes';
  FLJanelas.Add(FRJanelas);
  //
  // IMP-PROVI-001 :: Impress�o de provis�es por per�odo
  New(FRJanelas);
  FRJanelas.ID        := 'IMP-PROVI-001';
  FRJanelas.Nome      := 'FmPrevImp';
  FRJanelas.Descricao := 'Impress�o de provis�es por per�odo';
  FLJanelas.Add(FRJanelas);
  //
  // INA-REPAR-001 :: Reparcelamento de D�bitos Condominiais
  New(FRJanelas);
  FRJanelas.ID        := 'INA-REPAR-001';
  FRJanelas.Nome      := 'FmReparc';
  FRJanelas.Descricao := 'Reparcelamento de D�bitos Condominiais';
  FLJanelas.Add(FRJanelas);
  //
  (*
  //  LEI-CADAS-001 :: Consumos por Leitura
  New(FRJanelas);
  FRJanelas.ID        := 'LEI-CADAS-001';
  FRJanelas.Nome      := 'FmCons';
  FRJanelas.Descricao := 'Consumos por Leitura';
  FLJanelas.Add(FRJanelas);
  //
  *)
  //  LEI-PRINT-001 :: Impress�es de Consumos
  New(FRJanelas);
  FRJanelas.ID        := 'LEI-PRINT-001';
  FRJanelas.Nome      := 'FmLeituraImp';
  FRJanelas.Descricao := 'Impress�es de Consumos';
  FLJanelas.Add(FRJanelas);
  //
  //  PRO-DUSYS-001 :: Exporta Produsys
  New(FRJanelas);
  FRJanelas.ID        := 'PRO-DUSYS-001';
  FRJanelas.Nome      := 'FmProdusys1';
  FRJanelas.Descricao := 'Exporta Produsys';
  FLJanelas.Add(FRJanelas);
  //
  //  PRO-DUSYS-002 :: Vencimento
  New(FRJanelas);
  FRJanelas.ID        := 'PRO-DUSYS-002';
  FRJanelas.Nome      := 'FmProdusys1Vencto';
  FRJanelas.Descricao := 'Vencimento';
  FLJanelas.Add(FRJanelas);
  //
  //  SYN-EXTRA-001:: Carrega Lan�amentos de Sal�rios - Modelo 1
  New(FRJanelas);
  FRJanelas.ID        := 'SYN-EXTRA-001';
  FRJanelas.Nome      := 'FmImportSal1';
  FRJanelas.Descricao := 'Carrega Lan�amentos de Sal�rios - Modelo 1';
  FLJanelas.Add(FRJanelas);
  //
  // BLQ-ANALI-001 :: An�lise de Lan�amentos de Bloquetos
  New(FRJanelas);
  FRJanelas.ID        := 'BLQ-ANALI-001';
  FRJanelas.Nome      := 'FmBloqAnalisa';
  FRJanelas.Descricao := 'An�lise de Lan�amentos de Bloquetos';
  FLJanelas.Add(FRJanelas);
  //
  // BLQ-REPAR-001 :: Parcelamentos de Bloquetos
  New(FRJanelas);
  FRJanelas.ID        := 'BLQ-REPAR-001';
  FRJanelas.Nome      := 'FmBloqParc';
  FRJanelas.Descricao := 'Parcelamentos de Bloquetos';
  FLJanelas.Add(FRJanelas);
  //
  // BLQ-REPAR-002 :: Edi��o de Parcela de Parcelamento
  New(FRJanelas);
  FRJanelas.ID        := 'BLQ-REPAR-002';
  FRJanelas.Nome      := 'FmBloqParcPar';
  FRJanelas.Descricao := 'Edi��o de Parcela de Parcelamento';
  FLJanelas.Add(FRJanelas);
  //
  // BLQ-REPAR-003 :: Recorre��o de Valores de Parcelamento
  New(FRJanelas);
  FRJanelas.ID        := 'BLQ-REPAR-003';
  FRJanelas.Nome      := 'FmBloqParcCorrige';
  FRJanelas.Descricao := 'Recorre��o de Valores de Parcelamento';
  FLJanelas.Add(FRJanelas);
  //
  // BLQ-REPAR-004 :: Pesquisa de Reparcelamentos
  New(FRJanelas);
  FRJanelas.ID        := 'BLQ-REPAR-004';
  FRJanelas.Nome      := 'FmBloqParcPesq';
  FRJanelas.Descricao := 'Pesquisa de Reparcelamentos';
  FLJanelas.Add(FRJanelas);
  //
  // BLQ-REPAR-005 :: Aviso de Reparcelamentos Abertos
  New(FRJanelas);
  FRJanelas.ID        := 'BLQ-REPAR-005';
  FRJanelas.Nome      := 'FmBloqParcAviso';
  FRJanelas.Descricao := 'Pesquisa de Reparcelamentos Abertos';
  FLJanelas.Add(FRJanelas);
  //
  // WEB-CLIEN-001 :: Clientes WEB
  New(FRJanelas);
  FRJanelas.ID        := 'WEB-CLIEN-001';
  FRJanelas.Nome      := 'FmWClients2';
  FRJanelas.Descricao := 'Clientes WEB';
  FLJanelas.Add(FRJanelas);
  //
  // WEB-CLIEN-002 :: Pesquisa de Usu�rios WEB
  New(FRJanelas);
  FRJanelas.ID        := 'WEB-CLIEN-002';
  FRJanelas.Nome      := 'FmWUsers';
  FRJanelas.Descricao := 'Pesquisa de Usu�rios WEB';
  FLJanelas.Add(FRJanelas);
  //
  PerfJan_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Empresas_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Enti_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Finance_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  ALL_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Anotacoes_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Moedas_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Textos_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Cheques_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  IBGE_DTB_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  CNAB_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Diario_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Protocol_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  UnFPMin_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  UnFPMax_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  UnNFSe_TbTerc.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  UnNFSe_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  UnGeral_TbTerc.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  UnSMS_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  UnInflIdx_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  WUsers_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  WGerl_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Arquivos_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Mail_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Bloq_0202_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  BloqGerl_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  ConsumoGerl_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Notificacoes_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  //
  Result := True;
end;

end.
