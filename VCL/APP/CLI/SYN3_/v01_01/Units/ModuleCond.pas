unit ModuleCond;

interface

uses
  Windows, Forms, Controls, SysUtils, Classes, DB, mySQLDbTables, frxClass,
  dmkGeral, dmkDBLookupComboBox, dmkEdit, frxDBSet, UnDmkProcFunc, ExtCtrls,
  DmkDAC_PF, UnDmkEnums;

type
  TSentidoAssistente = (saDeAssistenteParaAccManager,
    saDeAccManagerParaAssistente, saDeNaoZeradoParaZerado);
  TDmCond = class(TDataModule)
    QrEmails: TmySQLQuery;
    QrEmailsApto: TIntegerField;
    QrEmailsUnidade: TWideStringField;
    QrEmailsPropriet: TLargeintField;
    QrEmailsUsuario: TLargeintField;
    QrEmailsEMEIO: TWideStringField;
    QrEmailsNOME: TWideStringField;
    QrEmailsCodigo: TIntegerField;
    QrEmailsControle: TIntegerField;
    QrSumBaAUni: TmySQLQuery;
    QrSumBaAUniValor: TFloatField;
    QrCond: TmySQLQuery;
    QrCondNOMECLI: TWideStringField;
    QrCondCodigo: TIntegerField;
    QrCondLk: TIntegerField;
    QrCondDataCad: TDateField;
    QrCondDataAlt: TDateField;
    QrCondUserCad: TIntegerField;
    QrCondUserAlt: TIntegerField;
    QrCondCliente: TIntegerField;
    QrCondObserv: TWideMemoField;
    QrCondAndares: TIntegerField;
    QrCondTotApt: TIntegerField;
    QrCondDiaVencto: TIntegerField;
    QrCondBcoLogoPath: TWideStringField;
    QrCondCliLogoPath: TWideStringField;
    QrCondCIDADE: TWideStringField;
    QrCondNOMEUF: TWideStringField;
    QrCondModelBloq: TSmallintField;
    QrCondMIB: TIntegerField;
    QrCondMBB: TSmallintField;
    QrCondMRB: TSmallintField;
    QrCondPBB: TSmallintField;
    QrCondMSB: TSmallintField;
    QrCondPSB: TSmallintField;
    QrCondMPB: TSmallintField;
    QrCondMAB: TSmallintField;
    QrCondAlterWeb: TSmallintField;
    QrCondPwdSite: TSmallintField;
    QrCondProLaINSSr: TFloatField;
    QrCondProLaINSSp: TFloatField;
    QrCondAtivo: TSmallintField;
    QrCondConfigBol: TIntegerField;
    QrCondMSP: TSmallintField;
    QrCondCART_ATIVO: TSmallintField;
    QrCondCAR_TIPODOC: TSmallintField;
    QrCondOcultaBloq: TSmallintField;
    QrCondSomaFracao: TFloatField;
    QrCondSigla: TWideStringField;
    QrCondBalAgrMens: TSmallintField;
    QrCondCompe: TSmallintField;
    QrCondHideCompe: TSmallintField;
    DsCond: TDataSource;
    QrArreFutI: TmySQLQuery;
    QrArreFutIControle: TIntegerField;
    QrArreFutIConta: TIntegerField;
    QrArreFutICond: TIntegerField;
    QrArreFutIPeriodo: TIntegerField;
    QrArreFutIApto: TIntegerField;
    QrArreFutIPropriet: TIntegerField;
    QrArreFutIValor: TFloatField;
    QrArreFutITexto: TWideStringField;
    QrArreFutILk: TIntegerField;
    QrArreFutIDataCad: TDateField;
    QrArreFutIDataAlt: TDateField;
    QrArreFutIUserCad: TIntegerField;
    QrArreFutIUserAlt: TIntegerField;
    QrArreFutINOMEPROP: TWideStringField;
    QrArreFutIUNIDADE: TWideStringField;
    QrArreFutINOMECONTA: TWideStringField;
    QrArreFutIPERIODO_TXT: TWideStringField;
    DsArreFutI: TDataSource;
    QrContasSdo: TmySQLQuery;
    QrContasSdoMOMECTA: TWideStringField;
    QrContasSdoCodigo: TIntegerField;
    QrContasSdoEntidade: TIntegerField;
    QrContasSdoSdoIni: TFloatField;
    QrContasSdoSdoAtu: TFloatField;
    QrContasSdoSdoFut: TFloatField;
    QrContasSdoLk: TIntegerField;
    QrContasSdoDataCad: TDateField;
    QrContasSdoDataAlt: TDateField;
    QrContasSdoUserCad: TIntegerField;
    QrContasSdoUserAlt: TIntegerField;
    QrContasSdoTexto: TWideStringField;
    QrContasSdoPerAnt: TFloatField;
    QrContasSdoPerAtu: TFloatField;
    DsContasSdo: TDataSource;
    QrContasTrf: TmySQLQuery;
    QrContasTrfMOMECTA: TWideStringField;
    QrContasTrfDataT: TDateField;
    QrContasTrfControle: TIntegerField;
    QrContasTrfCtaOrig: TIntegerField;
    QrContasTrfCtaDest: TIntegerField;
    QrContasTrfVALOR: TFloatField;
    DsContasTrf: TDataSource;
    QrNIA1: TmySQLQuery;
    QrNIA1Codigo: TIntegerField;
    QrNIA1Nome: TWideStringField;
    QrNIA1Conta: TIntegerField;
    QrNIA1Valor: TFloatField;
    QrNIA1SitCobr: TIntegerField;
    QrNIA1Parcelas: TIntegerField;
    QrNIA1ParcPerI: TIntegerField;
    QrNIA1ParcPerF: TIntegerField;
    QrNIA1InfoParc: TSmallintField;
    QrNIA1Texto: TWideStringField;
    QrNIA1Controle: TIntegerField;
    QrNIA1Fator: TSmallintField;
    QrNIA1Percent: TFloatField;
    QrNIA1DeQuem: TSmallintField;
    QrNIA1DoQue: TSmallintField;
    QrNIA1ComoCobra: TSmallintField;
    QrNIA1NaoArreSobre: TSmallintField;
    QrNIA1Calculo: TSmallintField;
    QrNIA1Arredonda: TFloatField;
    QrNIA1ListaBaU: TIntegerField;
    QrNIA1DescriCota: TWideStringField;
    QrNIA1ValFracDef: TFloatField;
    QrNIA1ValFracAsk: TSmallintField;
    QrNIA1Partilha: TSmallintField;
    QrNIA1NaoArreRisco: TSmallintField;
    QrAptos: TmySQLQuery;
    QrAptosConta: TIntegerField;
    QrAptosUnidade: TWideStringField;
    QrAptosPropriet: TIntegerField;
    QrAptosNOMEPROPRIET: TWideStringField;
    QrAptosFracaoIdeal: TFloatField;
    QrAptosMoradores: TIntegerField;
    QrTMoradores: TmySQLQuery;
    QrTMoradoresTOTAL: TFloatField;
    QrTFracao: TmySQLQuery;
    QrTFracaoTOTAL: TFloatField;
    QrPercAptAll: TmySQLQuery;
    QrPercAptAllTotPerc: TFloatField;
    QrPercAptUni: TmySQLQuery;
    QrPercAptUniPercent: TFloatField;
    QrConsApt: TmySQLQuery;
    QrConsAptValor: TFloatField;
    QrSumArreUH: TmySQLQuery;
    QrSumArreUHValor: TFloatField;
    QrCons: TmySQLQuery;
    QrConsCodigo: TIntegerField;
    QrConsNome: TWideStringField;
    QrConsPreco: TFloatField;
    QrConsCasas: TSmallintField;
    QrConsUnidLei: TWideStringField;
    QrConsUnidImp: TWideStringField;
    QrConsUnidFat: TFloatField;
    QrConsTOT_UNID1: TFloatField;
    QrConsTOT_UNID2: TFloatField;
    QrConsTOT_VALOR: TFloatField;
    QrConsTOT_UNID1_TXT: TWideStringField;
    QrConsTOT_UNID2_TXT: TWideStringField;
    DsCons: TDataSource;
    DsCNS: TDataSource;
    QrCNS: TmySQLQuery;
    QrCNSUnidade: TWideStringField;
    QrCNSCodigo: TIntegerField;
    QrCNSControle: TIntegerField;
    QrCNSApto: TIntegerField;
    QrCNSMedAnt: TFloatField;
    QrCNSMedAtu: TFloatField;
    QrCNSLk: TIntegerField;
    QrCNSDataCad: TDateField;
    QrCNSDataAlt: TDateField;
    QrCNSUserCad: TIntegerField;
    QrCNSUserAlt: TIntegerField;
    QrCNSCond: TIntegerField;
    QrCNSPeriodo: TIntegerField;
    QrCNSConsumo: TFloatField;
    QrCNSValor: TFloatField;
    QrCNSPreco: TFloatField;
    QrCNSCasas: TSmallintField;
    QrCNSUnidLei: TWideStringField;
    QrCNSUnidImp: TWideStringField;
    QrCNSUnidFat: TFloatField;
    QrCNSCONSUMO1_TXT: TWideStringField;
    QrCNSCONSUMO2_TXT: TWideStringField;
    QrCNSPropriet: TIntegerField;
    QrCNSCONSUMO2: TFloatField;
    QrCNSBoleto: TFloatField;
    QrCNSGeraTyp: TSmallintField;
    QrCNSGeraFat: TFloatField;
    QrCNSCasRat: TSmallintField;
    QrCNSDifCaren: TSmallintField;
    QrCNSCarencia: TFloatField;
    QrSumBol: TmySQLQuery;
    QrSumBolVALOR: TFloatField;
    QrSumCP: TmySQLQuery;
    QrSumCPCONSUMO: TFloatField;
    QrSumCPVALOR: TFloatField;
    QrCopiaCH: TmySQLQuery;
    QrCopiaCHDebito: TFloatField;
    QrCopiaCHBanco1: TWideStringField;
    QrCopiaCHAgencia1: TWideStringField;
    QrCopiaCHConta1: TWideStringField;
    QrCopiaCHSerieCH: TWideStringField;
    QrCopiaCHDocumento: TWideStringField;
    QrCopiaCHData: TDateField;
    QrCopiaCHDescricao: TWideStringField;
    QrCopiaCHFornecedor: TIntegerField;
    QrCopiaCHNOMEFORNECE: TWideStringField;
    QrCopiaCHNOMEBANCO: TWideStringField;
    QrCopiaCHControle: TWideStringField;
    QrCopiaCHCRUZADO_SIM: TBooleanField;
    QrCopiaCHCRUZADO_NAO: TBooleanField;
    QrCopiaCHVISADO_SIM: TBooleanField;
    QrCopiaCHVISADO_NAO: TBooleanField;
    QrCopiaCHTipoCH: TSmallintField;
    QrContasEnt: TmySQLQuery;
    QrContasEntCntrDebCta: TWideStringField;
    QrNIO_B: TmySQLQuery;
    QrNIO_BCodigo: TIntegerField;
    QrNIO_BNome: TWideStringField;
    QrNIO_BConta: TIntegerField;
    QrNIO_BValor: TFloatField;
    QrNIO_BSitCobr: TIntegerField;
    QrNIO_BParcelas: TIntegerField;
    QrNIO_BParcPerI: TIntegerField;
    QrNIO_BParcPerF: TIntegerField;
    QrNIO_BInfoParc: TSmallintField;
    QrNIO_BTexto: TWideStringField;
    QrNIO_BControle: TIntegerField;
    QrNIO_BNOMECON: TWideStringField;
    QrMU6PM: TmySQLQuery;
    QrMU6PMMez: TIntegerField;
    QrMU6PMSUM_DEB: TFloatField;
    QrMU6PMMES: TWideStringField;
    QrMU6PT: TmySQLQuery;
    QrMU6PTSUM_DEB: TFloatField;
    QrMU6MT: TmySQLQuery;
    QrMU6MTSUM_DEB: TFloatField;
    QrNIO_A: TmySQLQuery;
    QrNIO_ACodigo: TIntegerField;
    QrNIO_ANome: TWideStringField;
    QrNIO_AConta: TIntegerField;
    QrNIO_AValor: TFloatField;
    QrNIO_ASitCobr: TIntegerField;
    QrNIO_ATexto: TWideStringField;
    QrNIO_AControle: TIntegerField;
    QrNIO_ANOMECON: TWideStringField;
    QrArreFutA: TmySQLQuery;
    QrArreFutANOMEPROP: TWideStringField;
    QrArreFutAUNIDADE: TWideStringField;
    QrArreFutANOMECONTA: TWideStringField;
    QrArreFutAControle: TIntegerField;
    QrArreFutAConta: TIntegerField;
    QrArreFutACond: TIntegerField;
    QrArreFutAPeriodo: TIntegerField;
    QrArreFutAApto: TIntegerField;
    QrArreFutAPropriet: TIntegerField;
    QrArreFutAValor: TFloatField;
    QrArreFutATexto: TWideStringField;
    QrArreFutALk: TIntegerField;
    QrArreFutADataCad: TDateField;
    QrArreFutADataAlt: TDateField;
    QrArreFutAUserCad: TIntegerField;
    QrArreFutAUserAlt: TIntegerField;
    QrArreFutAPERIODO_TXT: TWideStringField;
    QrArreFutAInclui: TIntegerField;
    DsArreFutA: TDataSource;
    DsInadTot: TDataSource;
    QrInadTot: TmySQLQuery;
    QrInadTotCredito: TFloatField;
    QrInadUsu: TmySQLQuery;
    QrInadUsuUnidade: TWideStringField;
    QrInadUsuDepto: TIntegerField;
    QrInadUsuCredito: TFloatField;
    QrInadUsuNOMEPROPRIET: TWideStringField;
    DsInadUsu: TDataSource;
    QrDebitos: TmySQLQuery;
    QrDebitosCOMPENSADO_TXT: TWideStringField;
    QrDebitosDATA: TWideStringField;
    QrDebitosDescricao: TWideStringField;
    QrDebitosDEBITO: TFloatField;
    QrDebitosNOTAFISCAL: TLargeintField;
    QrDebitosSERIECH: TWideStringField;
    QrDebitosDOCUMENTO: TFloatField;
    QrDebitosMEZ: TLargeintField;
    QrDebitosCompensado: TDateField;
    QrDebitosNOMECON: TWideStringField;
    QrDebitosNOMESGR: TWideStringField;
    QrDebitosNOMEGRU: TWideStringField;
    QrDebitosITENS: TLargeintField;
    QrDebitosMES: TWideStringField;
    QrDebitosSERIE_DOC: TWideStringField;
    QrDebitosNF_TXT: TWideStringField;
    QrDebitosMES2: TWideStringField;
    QrDebitosControle: TIntegerField;
    QrDebitosSub: TSmallintField;
    QrDebitosCarteira: TIntegerField;
    QrDebitosCartao: TIntegerField;
    QrDebitosVencimento: TDateField;
    QrDebitosSit: TIntegerField;
    QrDebitosGenero: TIntegerField;
    QrDebitosTipo: TSmallintField;
    DsDebitos: TDataSource;
    QrCreditos: TmySQLQuery;
    QrCreditosMez: TIntegerField;
    QrCreditosCredito: TFloatField;
    QrCreditosNOMECON: TWideStringField;
    QrCreditosNOMESGR: TWideStringField;
    QrCreditosNOMEGRU: TWideStringField;
    QrCreditosMES: TWideStringField;
    QrCreditosNOMECON_2: TWideStringField;
    QrCreditosSubPgto1: TIntegerField;
    QrCreditosControle: TIntegerField;
    QrCreditosSub: TSmallintField;
    QrCreditosCarteira: TIntegerField;
    QrCreditosCartao: TIntegerField;
    QrCreditosVencimento: TDateField;
    QrCreditosCompensado: TDateField;
    QrCreditosSit: TIntegerField;
    QrCreditosGenero: TIntegerField;
    QrCreditosTipo: TSmallintField;
    DsCreditos: TDataSource;
    QrResumo: TmySQLQuery;
    QrResumoCredito: TFloatField;
    QrResumoDebito: TFloatField;
    QrResumoSALDO: TFloatField;
    QrResumoFINAL: TFloatField;
    QrSaldoA: TmySQLQuery;
    QrSaldoAInicial: TFloatField;
    QrSaldoASALDO: TFloatField;
    QrSaldoATOTAL: TFloatField;
    QrDebitosNO_FORNECE: TWideStringField;
    frxDsCond: TfrxDBDataset;
    frxDsCons: TfrxDBDataset;
    frxDsCNS: TfrxDBDataset;
    QrAgefet: TmySQLQuery;
    QrAgefetPrevBaC: TIntegerField;
    QrAgefetPrevBaI: TIntegerField;
    QrAgefetCond: TIntegerField;
    QrAgefetNOMECOND: TWideStringField;
    QrAgefetPERIODO_TXT: TWideStringField;
    QrAgefetValor: TFloatField;
    QrAgefetTexto: TWideStringField;
    QrAgefetSitCobr: TIntegerField;
    QrAgefetParcelas: TIntegerField;
    QrAgefetPrevCod: TIntegerField;
    QrAgefetPeriodo: TIntegerField;
    QrAgefetNOMESITCOBR: TWideStringField;
    QrLocUHs: TmySQLQuery;
    QrLocUHsConta: TIntegerField;
    QrLocUHsUnidade: TWideStringField;
    QrMU6MM: TmySQLQuery;
    QrMU6MMSUM_DEB: TFloatField;
    QrMU6MMMES: TWideStringField;
    QrMU6MMMEZ: TIntegerField;
    QrSenha: TmySQLQuery;
    QrSenhaAssistente: TIntegerField;
    QrSenhaNumero: TIntegerField;
    QrAccMgr: TmySQLQuery;
    QrAccMgrCodCliInt: TIntegerField;
    QrAccMgrCodigo: TIntegerField;
    QrAccMgrAssistente: TIntegerField;
    QrAccMgrAccManager: TIntegerField;
    QrSumFracao: TmySQLQuery;
    QrSumFracaoTOTAL: TFloatField;
    QrCondDOCNUM_TXT: TWideStringField;
    QrCondDOCNUM: TWideStringField;
    QrCNAB_Cfg_: TmySQLQuery;
    QrCNAB_Cfg_Codigo: TIntegerField;
    QrCNAB_Cfg_Nome: TWideStringField;
    QrCNAB_Cfg_Cedente: TIntegerField;
    QrCNAB_Cfg_SacadAvali: TIntegerField;
    QrCNAB_Cfg_CedBanco: TIntegerField;
    QrCNAB_Cfg_CedAgencia: TIntegerField;
    QrCNAB_Cfg_CedConta: TWideStringField;
    QrCNAB_Cfg_CedDAC_A: TWideStringField;
    QrCNAB_Cfg_CedDAC_C: TWideStringField;
    QrCNAB_Cfg_CedDAC_AC: TWideStringField;
    QrCNAB_Cfg_CedNome: TWideStringField;
    QrCNAB_Cfg_SacAvaNome: TWideStringField;
    QrCNAB_Cfg_TipoCart: TSmallintField;
    QrCNAB_Cfg_CartNum: TWideStringField;
    QrCNAB_Cfg_CartCod: TWideStringField;
    QrCNAB_Cfg_EspecieTit: TWideStringField;
    QrCNAB_Cfg_AceiteTit: TSmallintField;
    QrCNAB_Cfg_InstrCobr1: TWideStringField;
    QrCNAB_Cfg_InstrCobr2: TWideStringField;
    QrCNAB_Cfg_CodEmprBco: TWideStringField;
    QrCNAB_Cfg_JurosTipo: TSmallintField;
    QrCNAB_Cfg_JurosPerc: TFloatField;
    QrCNAB_Cfg_JurosDias: TSmallintField;
    QrCNAB_Cfg_MultaTipo: TSmallintField;
    QrCNAB_Cfg_MultaPerc: TFloatField;
    QrCNAB_Cfg_Texto01: TWideStringField;
    QrCNAB_Cfg_Texto02: TWideStringField;
    QrCNAB_Cfg_Texto03: TWideStringField;
    QrCNAB_Cfg_Texto04: TWideStringField;
    QrCNAB_Cfg_Texto05: TWideStringField;
    QrCNAB_Cfg_Texto06: TWideStringField;
    QrCNAB_Cfg_Texto07: TWideStringField;
    QrCNAB_Cfg_Texto08: TWideStringField;
    QrCNAB_Cfg_Texto09: TWideStringField;
    QrCNAB_Cfg_Texto10: TWideStringField;
    QrCNAB_Cfg_Lk: TIntegerField;
    QrCNAB_Cfg_DataCad: TDateField;
    QrCNAB_Cfg_DataAlt: TDateField;
    QrCNAB_Cfg_UserCad: TIntegerField;
    QrCNAB_Cfg_UserAlt: TIntegerField;
    QrCNAB_Cfg_AlterWeb: TSmallintField;
    QrCNAB_Cfg_Ativo: TSmallintField;
    QrCNAB_Cfg_CNAB: TIntegerField;
    QrCNAB_Cfg_EnvEmeio: TSmallintField;
    QrCNAB_Cfg_CedPosto: TIntegerField;
    QrCNAB_Cfg_InstrDias: TSmallintField;
    QrCNAB_Cfg_Diretorio: TWideStringField;
    QrCNAB_Cfg_QuemPrint: TWideStringField;
    QrCNAB_Cfg__237Mens1: TWideStringField;
    QrCNAB_Cfg__237Mens2: TWideStringField;
    QrCNAB_Cfg_CodOculto: TWideStringField;
    QrCNAB_Cfg_SeqArq: TIntegerField;
    QrCNAB_Cfg_LastNosNum: TLargeintField;
    QrCNAB_Cfg_LocalPag: TWideStringField;
    QrCNAB_Cfg_EspecieVal: TWideStringField;
    QrCNAB_Cfg_OperCodi: TWideStringField;
    QrCNAB_Cfg_IDCobranca: TWideStringField;
    QrCNAB_Cfg_AgContaCed: TWideStringField;
    QrCNAB_Cfg_DirRetorno: TWideStringField;
    QrCNAB_Cfg_CartRetorno: TIntegerField;
    QrCNAB_Cfg_PosicoesBB: TSmallintField;
    QrCNAB_Cfg_IndicatBB: TWideStringField;
    QrCNAB_Cfg_TipoCobrBB: TWideStringField;
    QrCNAB_Cfg_Comando: TIntegerField;
    QrCNAB_Cfg_DdProtesBB: TIntegerField;
    QrCNAB_Cfg_Msg40posBB: TWideStringField;
    QrCNAB_Cfg_QuemDistrb: TWideStringField;
    QrCNAB_Cfg_Desco1Cod: TSmallintField;
    QrCNAB_Cfg_Desco1Dds: TIntegerField;
    QrCNAB_Cfg_Desco1Fat: TFloatField;
    QrCNAB_Cfg_Desco2Cod: TSmallintField;
    QrCNAB_Cfg_Desco2Dds: TIntegerField;
    QrCNAB_Cfg_Desco2Fat: TFloatField;
    QrCNAB_Cfg_Desco3Cod: TSmallintField;
    QrCNAB_Cfg_Desco3Dds: TIntegerField;
    QrCNAB_Cfg_Desco3Fat: TFloatField;
    QrCNAB_Cfg_ProtesCod: TSmallintField;
    QrCNAB_Cfg_ProtesDds: TSmallintField;
    QrCNAB_Cfg_BxaDevCod: TSmallintField;
    QrCNAB_Cfg_BxaDevDds: TIntegerField;
    QrCNAB_Cfg_MoedaCod: TWideStringField;
    QrCNAB_Cfg_Contrato: TFloatField;
    QrCNAB_Cfg_EspecieDoc: TWideStringField;
    QrCNAB_Cfg_MultaDias: TSmallintField;
    QrCNAB_Cfg_Variacao: TIntegerField;
    QrCNAB_Cfg_ConvCartCobr: TWideStringField;
    QrCNAB_Cfg_ConvVariCart: TWideStringField;
    QrCNAB_Cfg_InfNossoNum: TSmallintField;
    QrCNAB_Cfg_CodLidrBco: TWideStringField;
    QrCNAB_Cfg_CartEmiss: TIntegerField;
    QrCNAB_Cfg_TipoCobranca: TIntegerField;
    QrCNAB_Cfg_EspecieTxt: TWideStringField;
    QrCNAB_Cfg_CartTxt: TWideStringField;
    QrCNAB_Cfg_LayoutRem: TWideStringField;
    QrCNAB_Cfg_LayoutRet: TWideStringField;
    QrCNAB_Cfg_NaoRecebDd: TSmallintField;
    QrCNAB_Cfg_TipBloqUso: TWideStringField;
    QrCNAB_Cfg_ConcatCod: TWideStringField;
    QrCNAB_Cfg_ComplmCod: TWideStringField;
    QrCNAB_Cfg_NumVersaoI3: TIntegerField;
    QrCNAB_Cfg_ModalCobr: TSmallintField;
    QrCNAB_Cfg_CtaCooper: TWideStringField;
    QrCount: TmySQLQuery;
    QrCountITENS: TLargeintField;
    QrCopiaCHProtocolo: TIntegerField;
    QrCopiaCHEAN128: TWideStringField;
    QrCopiaCHEANTem: TLargeintField;
    QrCNSLancto: TIntegerField;
    QrCondUF: TFloatField;
    QrCNAB_Cfg_NosNumFxaI: TIntegerField;
    QrCNAB_Cfg_NosNumFxaF: TIntegerField;
    QrCNAB_Cfg_NosNumFxaU: TSmallintField;
    QrCNSBloco: TWideStringField;
    QrArreFutACNAB_Cfg: TIntegerField;
    QrArreFutICNAB_Cfg: TIntegerField;
    QrNIA1CNAB_Cfg: TIntegerField;
    QrCNSCNAB_Cfg: TIntegerField;
    procedure DataModuleCreate(Sender: TObject);
    procedure QrCondAfterOpen(DataSet: TDataSet);
    procedure QrCondAfterScroll(DataSet: TDataSet);
    procedure QrCondCalcFields(DataSet: TDataSet);
    procedure QrArreFutICalcFields(DataSet: TDataSet);
    procedure QrContasSdoAfterScroll(DataSet: TDataSet);
    procedure QrConsAfterScroll(DataSet: TDataSet);
    procedure QrConsBeforeClose(DataSet: TDataSet);
    procedure QrConsCalcFields(DataSet: TDataSet);
    procedure QrCNSAfterOpen(DataSet: TDataSet);
    procedure QrCNSCalcFields(DataSet: TDataSet);
    procedure QrCopiaCHCalcFields(DataSet: TDataSet);
    procedure QrMU6PMCalcFields(DataSet: TDataSet);
    procedure QrMU6MMCalcFields(DataSet: TDataSet);
    procedure QrArreFutACalcFields(DataSet: TDataSet);
    procedure QrInadTotAfterOpen(DataSet: TDataSet);
    procedure QrInadTotBeforeClose(DataSet: TDataSet);
    procedure QrAgefetCalcFields(DataSet: TDataSet);
    procedure QrCondBeforeClose(DataSet: TDataSet);
  private
    { Private declarations }
    FEstagioA, FEstagioL: String;
    FMU6MM, FMU6MT: String;
    procedure ReopenContasTrf(Entidade, Codigo, Controle: Integer);
    procedure ReopenContasSdo(Entidade, Codigo: Integer);
  public
    { Public declarations }
    FfrxGer: TfrxReport;
    FConfigGer, FConfigNew, FConfigOld,
    FModeloGer, FModeloNew: Integer;
    FSaldoDeContasJaAtualizado: Boolean;
    //
    //Tabelas de condom�nios
    FTabAriA, FTabCnsA, FTabPriA, FTabPrvA,
    FTabLctA, FTabLctB, FTabLctD: String;
    FDtEncer, FDtMorto: TDateTime;
    //
    function  AbreQrCopiaCheques(Controle: Integer; TabLct: String): Boolean;
    function  GetPercAptUni(Controle, ListaBaU, Conta: Integer;
              NomeConta: String): Double;
    function  GetPercAptAll(Controle, ListaBaU: Integer): Double;
    function AgendamentoDeProvisaoEfetivado(Lancamento: Integer;
              TabPrvA: String; Mostra: Boolean): Boolean;
    procedure ReopenArreFut(Cond, MesesAnt, Controle: Integer);
    procedure ReopenCreditosEDebitos(CliInt: Integer; DataI, DataF: String;
              Acordos, Periodos, Textos, NaoAgruparNada: Boolean;
              TabLctA, TabLctB, TabLctD: String);
    procedure ReopenMU6PM(CliInt, LastP, Genero: Integer);
    procedure ReopenMU6MM(CliInt, LastP, Genero: Integer);
    procedure ReopenQrCond(Condominio: Integer);
    procedure ReopenQrCons(Codigo: Integer);
    procedure ReopenCNS(Controle: Integer);
    procedure DefTabsEmpresa(CliInt: Integer);
    (*
    Esta procedure foi substitu�da pala procedure DmBloq.ReopenCNAB_Cfg_Cond

    procedure ReopenCNAB_Cfg(Codigo: Integer);
    *)
    (*
    function  AssistenteNaoHabilitadadoParaOCondominio(Cond: Integer;
              Avisa: Boolean): Boolean;
    *)
    procedure VerificaAccManager(Sentido: TSentidoAssistente);
    function  DefineCond(var Cond: Integer; const EdCond: TdmkEdit;
              const CBCond: TdmkDBLookupComboBox): Boolean;
    procedure AtualizaSomaFracaoApto(Controle, Codigo: Integer);
    procedure ReopenCNAB_CfgSel(Query: TmySQLQuery; Database: TmySQLDatabase;
                Cliente: Integer);
    (*
    23/04/2014 INI
    Foi criada a procedure DmBloq.ReopenCNAB_Cfg_Cond para ver de onde buscar e
    com isso esta function n�o � mais necess�ria

    function  NaoContinuaPeloCNAB_Cfg(var CNAB_Cfg: Integer; var UsaCfg: Boolean;
              var LastNosNum: Integer): Boolean;
    23/04/2014 FIM
    *)
  end;

var
  DmCond: TDmCond;

implementation

uses ModuleGeral,
//CondGer,
UnMLAGeral, UnInternalConsts, UMySQLModule, UCreate,
  Principal, MyDBCheck, CondGerAgefet, Module, CondSel, ModuleBloq, CNAB_Cond;

{$R *.dfm}

{var
  HEADERS: array of String;
}

function TDmCond.AbreQrCopiaCheques(Controle: Integer; TabLct: String): Boolean;
begin
  QrCopiaCH.Close;
  QrCopiaCH.SQL.Clear;
  QrCopiaCH.SQL.Add('SELECT lan.Debito, LPAD(car.Banco1, 3, "0") Banco1,');
  QrCopiaCH.SQL.Add('LPAD(car.Agencia1, 4, "0") Agencia1, car.Conta1,');
  QrCopiaCH.SQL.Add('lan.SerieCH, IF(lan.Documento=0, "",');
  QrCopiaCH.SQL.Add('LPAD(lan.Documento, 6, "0")) Documento, lan.Data,');
  QrCopiaCH.SQL.Add('lan.Descricao, lan.Fornecedor, IF(lan.Fornecedor=0, "",');
  QrCopiaCH.SQL.Add('CASE WHEN frn.Tipo=0 THEN frn.RazaoSocial');
  QrCopiaCH.SQL.Add('ELSE frn.Nome END) NOMEFORNECE,');
  QrCopiaCH.SQL.Add('ban.Nome NOMEBANCO, LPAD(lan.Controle, 6, "0") Controle,');
  QrCopiaCH.SQL.Add('lan.TipoCH, lan.Protocolo, ');
  QrCopiaCH.SQL.Add('IF(lan.Protocolo <> 0, 1, 0) EANTem ');
  QrCopiaCH.SQL.Add('FROM ' + TabLct + ' lan');
  QrCopiaCH.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
  QrCopiaCH.SQL.Add('LEFT JOIN entidades frn ON frn.Codigo=lan.Fornecedor');
  QrCopiaCH.SQL.Add('LEFT JOIN bancos ban ON ban.Codigo=car.Banco1');
  QrCopiaCH.SQL.Add('');
  QrCopiaCH.SQL.Add('WHERE lan.Controle=:P0');
  QrCopiaCH.Params[0].AsInteger := Controle;
  UMyMod.AbreQuery(QrCopiaCH, Dmod.MyDB);
  //
  Result := QrCopiaCH.RecordCount > 0;
end;

procedure TDmCond.DataModuleCreate(Sender: TObject);
begin
  FEstagioA  := '>';
  FEstagioL  := '>';
  FfrxGer    := nil;
  FConfigGer := 0;
  FConfigNew := 0;
  FConfigOld := 0;
  FModeloGer := 0;
  FModeloNew := 0;
  FSaldoDeContasJaAtualizado := False;
  frxDsCond.DataSet := QrCond;
  //
{ N�o pode!
  // DModG ainda n�o foi criado! Como Fazer
  QrMU6MM.Database := DModG.MyPID_DB;
  QrMU6MT.Database := DModG.MyPID_DB;
  QrMU6PM.Database := DModG.MyPID_DB;
  QrMU6PT.Database := DModG.MyPID_DB;
  QrSumBaAUni.Database := DmodG.MyPID_DB;
  QrCreditos.Database := DModG.MyPID_DB;
  QrDebitos.Database := DModG.MyPID_DB;
}
end;

function TDmCond.DefineCond(var Cond: Integer; const EdCond: TdmkEdit;
const CBCond: TdmkDBLookupComboBox): Boolean;
begin
  Cond := 0;
  Result := False;
  Application.CreateForm(TFmCondSel, FmCondSel);
  case DModG.QrEmpresas.RecordCount of
    0: Geral.MensagemBox('N�o h� condom�nio cadastrado ou liberado!', 'Aviso',
       MB_OK+MB_ICONWARNING);
    1:
    begin
      Cond := DModG.QrEmpresasFilial.Value;//FmCondSel.QrCondCodCliInt.Value;
    end else begin
      Screen.Cursor := crDefault;
      FmCondSel.ShowModal;
      Cond := FmCondSel.FCond;
    end;
  end;
  FmCondSel.Destroy;
  //
  if Cond <> 0 then
  begin
    if EdCond <> nil then
      EdCond.ValueVariant := Cond;
    if CBCond <> nil then
      CBCond.KeyValue := Cond;
  end;
end;

procedure TDmCond.DefTabsEmpresa(CliInt: Integer);
begin
  FTabAriA := DModG.NomeTab(TMeuDB, ntAri, False, ttA, CliInt);
  FTabCnsA := DModG.NomeTab(TMeuDB, ntCns, False, ttA, CliInt);
  FTabPriA := DModG.NomeTab(TMeuDB, ntPri, False, ttA, CliInt);
  FTabPrvA := DModG.NomeTab(TMeuDB, ntPrv, False, ttA, CliInt);
  //
  FTabLctA := DModG.NomeTab(TMeuDB, ntLct, False, ttA, CliInt);
  FTabLctB := DModG.NomeTab(TMeuDB, ntLct, False, ttB, CliInt);
  FTabLctD := DModG.NomeTab(TMeuDB, ntLct, False, ttD, CliInt);
end;

function TDmCond.GetPercAptAll(Controle, ListaBaU: Integer): Double;
var
  Ctrl: Integer;
begin
  if ListaBau <> 0 then Ctrl := ListaBaU else Ctrl := Controle;
  QrPercAptAll.Close;
  QrPercAptAll.Params[00].AsInteger := Ctrl;
  UMyMod.AbreQuery(QrPercAptAll, Dmod.MyDB);
  //
  Result := QrPercAptAllTotPerc.Value;
end;

function TDmCond.GetPercAptUni(Controle, ListaBaU, Conta: Integer;
  NomeConta: String): Double;
var
  Ctrl: Integer;
  UH: String;
begin
  if ListaBau <> 0 then Ctrl := ListaBaU else Ctrl := Controle;
  QrPercAptUni.Close;
  QrPercAptUni.Params[00].AsInteger := Ctrl;
  QrPercAptUni.Params[01].AsInteger := Conta;
  UMyMod.AbreQuery(QrPercAptUni, Dmod.MyDB);
  // Deve encontrar apenas um registro
  if QrPercAptUni.RecordCount = 1 then
    Result := QrPercAptUniPercent.Value
  else begin
    UH := DModG.ObtemNomeDepto(Conta);
    if QrPercAptUni.RecordCount = 0 then
      Geral.MensagemBox(
      'N�o h� item cadastrado para a unidade ' + UH + ' para a arrecada��o "' +
      NomeConta + '"! ', 'Aviso', MB_OK+MB_ICONWARNING)
    else
      Geral.MensagemBox(
      'H� mais de um item cadastrados para a unidade ' + UH +
      ' para a arrecada��o "' + NomeConta + '"! ',
      'Aviso', MB_OK+MB_ICONWARNING);
    Result := 0;
  end;
end;

(*
function TDmCond.NaoContinuaPeloCNAB_Cfg(var CNAB_Cfg: Integer;
  var UsaCfg: Boolean; var LastNosNum: Integer): Boolean;
var
  Msg: String;
  I: Integer;
begin
  Msg := '';
  if QrCondCNAB_Cfg.Value <> 0 then
  begin
    Result := True;
    ReopenCNAB_Cfg(QrCondCNAB_Cfg.Value);
    UsaCfg := True;
    CNAB_Cfg := QrCNAB_CfgCodigo.Value;
    LastNosNum := QrCNAB_CfgLastNosNum.Value;
    //
    if QrCNAB_CfgModalCobr.Value <> QrCondModalCobr.Value then
      Msg := Msg + '- Modalidade de Cobran�a (Com ou sem Registro)' + sLineBreak;
      //
    if QrCNAB_CfgCedBanco.Value <> QrCondBanco.Value then
      Msg := Msg + '- C�digo do Banco' + sLineBreak;
      //
    if QrCNAB_CfgCedAgencia.Value <> QrCondAgencia.Value then
      Msg := Msg + '- Ag�ncia Banc�ria' + sLineBreak;
      //
    if QrCNAB_CfgCedPosto.Value <> QrCondPosto.Value then
      Msg := Msg + '- Posto Banc�rio' + sLineBreak;
      //
    if QrCNAB_CfgCedConta.Value <> QrCondConta.Value then
      Msg := Msg + '- Conta Corrente Banc�ria' + sLineBreak;
      //
    if QrCNAB_CfgCartNum.Value <> QrCondCarteira.Value then
      Msg := Msg + '- Carteira de lan�amentos financeiros dos boletos' + sLineBreak;
      //
    if QrCNAB_CfgIDCobranca.Value <> QrCondIDCobranca.Value then
      Msg := Msg + '- Cond.IdentTipoCobr X CNAB.IDdaCarteira (Bco 104?)' + sLineBreak;
      //
    if QrCNAB_CfgCodEmprBco.Value <> QrCondCodCedente.Value then
      Msg := Msg + '- Cond.C�dCedente x CNAB.CedenteL�der' + sLineBreak;
      //
    if QrCNAB_CfgTipoCobranca.Value <> QrCondTipoCobranca.Value then
      Msg := Msg + '- Tipo de Cobran�a' + sLineBreak;
      //
    if QrCNAB_CfgCedDAC_A.Value <> QrCondDVAgencia.Value then
      Msg := Msg + '- D�gito Verificador da Ag�ncia' + sLineBreak;
      //
    if QrCNAB_CfgCedDAC_C.Value <> QrCondDVConta.Value then
      Msg := Msg + '- D�gito Verificador da Conta Corrente' + sLineBreak;
      //
    if QrCNAB_CfgOperCodi.Value <> QrCondOperCodi.Value then
      Msg := Msg + '- Opera��o C�digo (Bco 104?)' + sLineBreak;
      //
    if QrCNAB_CfgEspecieDoc.Value <> QrCondEspecieDoc.Value then
      Msg := Msg + '- Esp�cie do documento' + sLineBreak;
      //
    if QrCNAB_CfgAgContaCed.Value <> QrCondAgContaCed.Value then
      Msg := Msg + '- Ag�cia / Conta Cedente' + sLineBreak;
      //
    if QrCNAB_CfgEspecieTxt.Value <> QrCondEspecieVal.Value then
      Msg := Msg + '- Esp�cie do valor' + sLineBreak;
      //
    if QrCNAB_CfgCartTxt.Value <> QrCondCartTxt.Value then
      Msg := Msg + '- Carteira (Bloqueto) (Texto opcional para bloqueto)' + sLineBreak;
      //
    if QrCNAB_CfgCNAB.Value <> QrCondCNAB.Value then
      Msg := Msg + '- Tamanho do registro CNAB em arquivos remessa / retorno' + sLineBreak;
      //
    if QrCNAB_CfgCtaCooper.Value <> QrCondCtaCooper.Value then
      Msg := Msg + '- Conta Corrente do Cooperado (Nosso N�mero)' + sLineBreak;
      //
    if Msg <> '' then
    begin
      Geral.MensagemBox('ATEN��O! � necess�rio igualar as informa��es abaixo no ' +
      sLineBreak + 'cadastro do condom�nio e na configura��o CNAB deste condom�nio:' +
      sLineBreak + Msg, 'Aviso', MB_OK+MB_ICONWARNING);
      //
      if DBCheck.CriaFm(TFmCNAB_Cond, FmCNAB_Cond, afmoNegarComAviso) then
      begin
        FmCNAB_Cond.FCNAB_Cfg := QrCNAB_CfgCodigo.Value;
        FmCNAB_Cond.FCondominio := QrCondCodigo.Value;
        //
        FmCNAB_Cond.RGModalCobr.Items[0] := Geral.FF0(QrCondModalCobr.Value);
        FmCNAB_Cond.RGModalCobr.Items[1] := Geral.FF0(QrCNAB_CfgModalCobr.Value);
        //
        FmCNAB_Cond.RGBanco.Items[0] := Geral.FFN(QrCondBanco.Value, 3);
        FmCNAB_Cond.RGBanco.Items[1] := Geral.FFN(QrCNAB_CfgCedBanco.Value, 3);
        //
        FmCNAB_Cond.RGAgencia.Items[0] := Geral.FFN(QrCondAgencia.Value, 4);
        FmCNAB_Cond.RGAgencia.Items[1] := Geral.FFN(QrCNAB_CfgCedAgencia.Value, 4);
        //
        FmCNAB_Cond.RGPosto.Items[0] := Geral.FF0(QrCondPosto.Value);
        FmCNAB_Cond.RGPosto.Items[1] := Geral.FF0(QrCNAB_CfgCedPosto.Value);
        //
        FmCNAB_Cond.RGConta.Items[0] := QrCondConta.Value;
        FmCNAB_Cond.RGConta.Items[1] := QrCNAB_CfgCedConta.Value;
        //
        FmCNAB_Cond.RGCarteira.Items[0] := QrCondCarteira.Value;
        FmCNAB_Cond.RGCarteira.Items[1] := QrCNAB_CfgCartNum.Value;
        //
        FmCNAB_Cond.RGIDCobranca.Items[0] := QrCondIDCobranca.Value;
        FmCNAB_Cond.RGIDCobranca.Items[1] := QrCNAB_CfgIDCobranca.Value;
        //
        FmCNAB_Cond.RGCedente.Items[0] := QrCondCodCedente.Value;
        FmCNAB_Cond.RGCedente.Items[1] := QrCNAB_CfgCodEmprBco.Value;
        //
        FmCNAB_Cond.RGTipoCobranca.Items[0] := Geral.FF0(QrCondTipoCobranca.Value);
        FmCNAB_Cond.RGTipoCobranca.Items[1] := Geral.FF0(QrCNAB_CfgTipoCobranca.Value);
        //
        FmCNAB_Cond.RGDVAgencia.Items[0] := QrCondDVAgencia.Value;
        FmCNAB_Cond.RGDVAgencia.Items[1] := QrCNAB_CfgCedDAC_A.Value;
        //
        FmCNAB_Cond.RGDVConta.Items[0] := QrCondDVConta.Value;
        FmCNAB_Cond.RGDVConta.Items[1] := QrCNAB_CfgCedDAC_C.Value;
        //
        FmCNAB_Cond.RGOperCodi.Items[0] := QrCondOperCodi.Value;
        FmCNAB_Cond.RGOperCodi.Items[1] := QrCNAB_CfgOperCodi.Value;
        //
        FmCNAB_Cond.RGEspecieDoc.Items[0] := QrCondEspecieDoc.Value;
        FmCNAB_Cond.RGEspecieDoc.Items[1] := QrCNAB_CfgEspecieDoc.Value;
        //
        FmCNAB_Cond.RGAgContaCed.Items[0] := QrCondAgContaCed.Value;
        FmCNAB_Cond.RGAgContaCed.Items[1] := QrCNAB_CfgAgContaCed.Value;
        //
        FmCNAB_Cond.RGEspecieTxt.Items[0] := QrCondEspecieVal.Value;
        FmCNAB_Cond.RGEspecieTxt.Items[1] := QrCNAB_CfgEspecieTxt.Value;
        //
        FmCNAB_Cond.RGCartTxt.Items[0] := QrCondCartTxt.Value;
        FmCNAB_Cond.RGCartTxt.Items[1] := QrCNAB_CfgCartTxt.Value;
        //
        FmCNAB_Cond.RGCNAB.Items[0] := Geral.FFN(QrCondCNAB.Value, 3);
        FmCNAB_Cond.RGCNAB.Items[1] := Geral.FFN(QrCNAB_CfgCNAB.Value, 3);
        //
        FmCNAB_Cond.RGCtaCooper.Items[0] := QrCondCtaCooper.Value;
        FmCNAB_Cond.RGCtaCooper.Items[1] := QrCNAB_CfgCtaCooper.Value;
        //
        //
        for I := 0 to FmCNAB_Cond.ComponentCount - 1 do
        begin
          if FmCNAB_Cond.Components[I] is TRadioGroup then
          begin
            if TRadioGroup(FmCNAB_Cond.Components[I]).Items.Count = 2 then
            begin
              if AnsiUpperCase(TRadioGroup(FmCNAB_Cond.Components[I]).Items[0]) =
              AnsiUpperCase(TRadioGroup(FmCNAB_Cond.Components[I]).Items[1]) then
                TRadioGroup(FmCNAB_Cond.Components[I]).Itemindex := 0
              else
                TRadioGroup(FmCNAB_Cond.Components[I]).Itemindex := -1;
            end;
          end;
        end;
        //
        FmCNAB_Cond.ShowModal;
        if FmCNAB_Cond.FEncerrar then
        begin
          if Geral.MensagemBox(
          '� recomendado encerrar o aplicativo para reabrir corretamente as informa��es!' +
          sLineBreak + 'Deseja faz�-lo agora?', 'Pergunta',
          MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
          begin
            //FmCondGer.Close;
            //FmPrincipal.AGBAcesRapidClick(FmPrincipal.AGBAcesRapid);
            Application.Terminate;
          end;
        end;
        FmCNAB_Cond.Destroy;
      end;
    end else
      Result := False;
    //  
  end
  else begin
    Result := False;
    UsaCfg := False;
    CNAB_Cfg := 0;
  end;
end;
*)

function TDmCond.AgendamentoDeProvisaoEfetivado(Lancamento: Integer;
TabPrvA: String; Mostra: Boolean): Boolean;
begin
  QrAgefet.Close;
(*
SELECT DATE_FORMAT(DATE_ADD("1999-12-01",
  INTERVAL Periodo MONTH), '%m/%Y') PERIODO_TXT,
IF (ent.Tipo=0,
ent.RazaoSocial,ent.Nome) NOMECOND,

pba.Codigo PrevBaC, pba.Controle PrevBaI, pba.Cond,

pba.Valor, pba.Texto, pba.SitCobr,
pba.Parcelas, pba.PrevCod, prv.Periodo
FROM prevbailct pbl
LEFT JOIN prevbai pba ON pba.Controle=pbl.Controle
LEFT JOIN ? prv ON prv.Codigo=pba.PrevCod
LEFT JOIN cond cnd ON cnd.Codigo=pba.Cond
LEFT JOIN entidades ent ON ent.Codigo=cnd.Cliente
WHERE pbl.lancto=:P0
*)
  QrAgefet.SQL.Clear;
  QrAgefet.SQL.Add('SELECT DATE_FORMAT(DATE_ADD("1999-12-01",');
  QrAgefet.SQL.Add('  INTERVAL Periodo MONTH), "%m/%Y") PERIODO_TXT,');
  QrAgefet.SQL.Add('IF (ent.Tipo=0,');
  QrAgefet.SQL.Add('ent.RazaoSocial,ent.Nome) NOMECOND,');
  QrAgefet.SQL.Add('pba.Codigo PrevBaC, pba.Controle PrevBaI, pba.Cond,');
  QrAgefet.SQL.Add('pba.Valor, pba.Texto, pba.SitCobr,');
  QrAgefet.SQL.Add('pba.Parcelas, pba.PrevCod, prv.Periodo');
  QrAgefet.SQL.Add('FROM prevbailct pbl');
  QrAgefet.SQL.Add('LEFT JOIN prevbai pba ON pba.Controle=pbl.Controle');
  QrAgefet.SQL.Add('LEFT JOIN ' + TabPrvA + ' prv ON prv.Codigo=pba.PrevCod');
  QrAgefet.SQL.Add('LEFT JOIN cond cnd ON cnd.Codigo=pba.Cond');
  QrAgefet.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=cnd.Cliente');
  QrAgefet.SQL.Add('WHERE pbl.lancto=:P0');
  QrAgefet.SQL.Add('');

  QrAgefet.Params[0].AsInteger := Lancamento;
  UMyMod.AbreQuery(QrAgefet, Dmod.MyDB);
  //
  Result := QrAgefet.RecordCount > 0;
  if Mostra then
  begin
    if not Result then
    begin
      Geral.MensagemBox('O lan�amento atual n�o foi agendado em ' +
      'nenhum item de provis�o!', 'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end else begin
      if DBCheck.CriaFm(TFmCondGerAgefet, FmCondGerAgefet, afmoNegarComAviso) then
      begin
        FmCondGerAgefet.ShowModal;
        FmCondGerAgefet.Destroy;
      end;
    end;
  end;
end;

procedure TDmCond.AtualizaSomaFracaoApto(Controle, Codigo: Integer);
begin
  // Bloco
  QrSumFracao.SQL.Clear;
  QrSumFracao.SQL.Add('SELECT SUM(FracaoIdeal) TOTAL');
  QrSumFracao.SQL.Add('FROM condimov');
  QrSumFracao.SQL.Add('WHERE Controle='+FormatFloat('0', Controle));
  UMyMod.AbreQuery(QrSumFracao, Dmod.MyDB);
  //
  Dmod.QrUpdZ.SQL.Clear;
  Dmod.QrUpdZ.SQL.Add('UPDATE condbloco SET SomaFracao=:P0');
  Dmod.QrUpdZ.SQL.Add('WHERE Controle=:P1');
  Dmod.QrUpdZ.Params[00].AsFloat   := QrSumFracaoTOTAL.Value;
  Dmod.QrUpdZ.Params[01].AsInteger := Controle;
  Dmod.QrUpdZ.ExecSQL;

  // Condominio
  QrSumFracao.SQL.Clear;
  QrSumFracao.SQL.Add('SELECT SUM(FracaoIdeal) TOTAL');
  QrSumFracao.SQL.Add('FROM condimov');
  QrSumFracao.SQL.Add('WHERE Codigo='+FormatFloat('0', Codigo));
  UMyMod.AbreQuery(QrSumFracao, Dmod.MyDB);
  //
  Dmod.QrUpdZ.SQL.Clear;
  Dmod.QrUpdZ.SQL.Add('UPDATE cond SET SomaFracao=:P0');
  Dmod.QrUpdZ.SQL.Add('WHERE Codigo=:P1');
  Dmod.QrUpdZ.Params[00].AsFloat   := QrSumFracaoTOTAL.Value;
  Dmod.QrUpdZ.Params[01].AsInteger := Codigo;
  Dmod.QrUpdZ.ExecSQL;
  //
end;

{
function TDmCond.AssistenteNaoHabilitadadoParaOCondominio(
  Cond: Integer; Avisa: Boolean): Boolean;
begin
  if VAR_USUARIO < 0 then
    Result := False
  else
  begin
    UnDmkDAC_PF.AbreMySQLQuery1(QrSenha, [
    'SELECT cnd.Assistente, snh.Numero',
    'FROM Cond cnd',
    'LEFT JOIN senhas snh ON snh.Funcionario=cnd.Assistente',
    'WHERE cnd.Codigo=' + FormatFloat('0', Cond),
    '']);
    Result := (QrSenhaNumero.Value <> VAR_USUARIO)
              and (QrSenhaAssistente.Value <> 0);
    //
    if Result and Avisa then
      Geral.MensagemBox('Acesso negado ao usu�rio logado!',
      'Aviso', MB_OK+MB_ICONWARNING);
  end;
end;
}

procedure TDmCond.QrAgefetCalcFields(DataSet: TDataSet);
begin
  QrAgefetNOMESITCOBR.Value := FmPrincipal.RgSitCobr.Items[QrAgefetSitCobr.Value];
end;

procedure TDmCond.QrArreFutACalcFields(DataSet: TDataSet);
begin
  QrArreFutAPERIODO_TXT.Value :=
    dmkPF.PeriodoToMensal(QrArreFutAPeriodo.Value);
end;

procedure TDmCond.QrArreFutICalcFields(DataSet: TDataSet);
begin
  QrArreFutIPERIODO_TXT.Value :=
    dmkPF.PeriodoToMensal(QrArreFutIPeriodo.Value);
end;

procedure TDmCond.QrCondAfterOpen(DataSet: TDataSet);
begin
{  Alexandria
  //StaticText1.Caption := QrCondNOMECLI.Value;
  FmCondGer.ReopenQrPrevBaI(0);
}
end;

procedure TDmCond.QrCondAfterScroll(DataSet: TDataSet);
begin
  //EdMesesArreFutChange(Self);
  ReopenArreFut(QrCondCodigo.Value, {Meses}0, 0);
  ReopenContasSdo(QrCondCliente.Value, QrCondCodigo.Value);
end;

procedure TDmCond.QrCondBeforeClose(DataSet: TDataSet);
begin
  QrArreFutI.Close;
  QrContasSdo.Close;
end;

procedure TDmCond.QrCondCalcFields(DataSet: TDataSet);
begin
  QrCondDOCNUM_TXT.Value := Geral.FormataCNPJ_TT(QrCondDOCNUM.Value);
end;

procedure TDmCond.QrConsAfterScroll(DataSet: TDataSet);
var
  FmtTxT: String;
begin
  FmtTxT := MLAGeral.FormataCasas(QrConsCasas.Value);
  QrConsTOT_UNID1.DisplayFormat := FmtTxt;
  QrConsTOT_UNID2.DisplayFormat := FmtTxt;
  ReopenCNS(0);
end;

procedure TDmCond.QrConsBeforeClose(DataSet: TDataSet);
begin
  QrCNS.Close;
end;

procedure TDmCond.QrConsCalcFields(DataSet: TDataSet);
begin
{ Alexandria
  QrSumCP.Close;
  QrSumCP.SQL.Clear;
  QrSumCP.SQL.Add('SELECT SUM(Consumo) CONSUMO, SUM(Valor) VALOR');
  QrSumCP.SQL.Add('FROM ' + DmCond.FTabCnsA + ' cni');
  QrSumCP.SQL.Add('WHERE cni.Cond=:P0');
  QrSumCP.SQL.Add('AND cni.Codigo=:P1');
  QrSumCP.SQL.Add('AND cni.Periodo=:P2');
  QrSumCP.Params[00].AsInteger := FmCondGer.QrPrevCond.Value;
  QrSumCP.Params[01].AsInteger := QrConsCodigo.Value;
  QrSumCP.Params[02].AsInteger := FmCondGer.QrPrevPeriodo.Value;
  UMyMod.AbreQuery(QrSumCP, Dmod.MyDB);
  //
  QrConsTOT_UNID1.Value := QrSumCPCONSUMO.Value;
  QrConsTOT_UNID1_TXT.Value := Geral.FFT(QrSumCPCONSUMO.Value, QrConsCasas.Value, siNegativo);
  QrConsTOT_UNID2.Value := QrSumCPCONSUMO.Value * QrConsUnidFat.Value;
  QrConsTOT_UNID2_TXT.Value := Geral.FFT(QrConsTOT_UNID2.Value, 3, siNegativo);
  QrConsTOT_VALOR.Value := QrSumCPVALOR.Value;
  //
  QrSumBol.Close;
  QrSumBol.Params[00].AsInteger := FmCondGer.QrPrevCond.Value;
  QrSumBol.Params[01].AsInteger := QrConsCodigo.Value;
  QrSumBol.Params[02].AsInteger := FmCondGer.QrPrevPeriodo.Value;
  UMyMod.AbreQuery(QrSumBol, Dmod.MyDB);
  //
}
end;

procedure TDmCond.QrCNSAfterOpen(DataSet: TDataSet);
var
  FmtTxT: String;
begin
  FmtTxT := MLAGeral.FormataCasas(QrConsCasas.Value);
  QrCNSMedAnt.DisplayFormat  := FmtTxt;
  QrCNSMedAtu.DisplayFormat  := FmtTxT;
  QrCNSConsumo.DisplayFormat := FmtTxT;
  QrCNSCarencia.DisplayFormat := FmtTxT;
end;

procedure TDmCond.QrCNSCalcFields(DataSet: TDataSet);
begin
  if QrCNSGeraTyp.Value = 0 then
  begin
    QrCNSCONSUMO2.Value := QrCNSConsumo.Value * QrCNSUnidFat.Value;
    //
    QrCNSConsumo2_TXT.Value := Geral.FFT(QrCNSCONSUMO2.Value,
      QrCNSCasas.Value, siNegativo) + ' ' + QrCNSUnidImp.Value;
    //
    QrCNSConsumo1_TXT.Value := Geral.FFT(QrCNSConsumo.Value,
      QrCNSCasas.Value, siNegativo) + ' ' + QrCNSUnidLei.Value;
  end else begin
    // Parei aqui Fazer diferente???
    QrCNSCONSUMO2.Value := QrCNSGeraFat.Value;
    //
    QrCNSConsumo2_TXT.Value := Geral.FFT(QrCNSCONSUMO2.Value,
      QrCNSCasRat.Value, siNegativo) + ' ' + QrCNSUnidImp.Value;
    //
    QrCNSConsumo1_TXT.Value := Geral.FFT(QrCNSConsumo.Value,
      QrCNSCasas.Value, siNegativo) + ' ' + QrCNSUnidLei.Value;
  end;
end;

procedure TDmCond.QrContasSdoAfterScroll(DataSet: TDataSet);
begin
  ReopenContasTrf(QrContasSdoEntidade.Value, QrContasSdoCodigo.Value, 0);
end;

procedure TDmCond.QrCopiaCHCalcFields(DataSet: TDataSet);
begin
  QrCopiaCHVISADO_SIM.Value  := MLAGeral.IntInConjunto(1, QrCopiaCHTipoCH.Value);
  QrCopiaCHCRUZADO_SIM.Value := MLAGeral.IntInConjunto(2, QrCopiaCHTipoCH.Value);
  QrCopiaCHVISADO_NAO.Value  := not QrCopiaCHVISADO_SIM.Value;
  QrCopiaCHCRUZADO_NAO.Value := not QrCopiaCHCRUZADO_SIM.Value;
  QrCopiaCHEAN128.Value := '>I<' + Geral.FFN(QrCopiaCHProtocolo.Value, 11);
end;

procedure TDmCond.QrInadTotAfterOpen(DataSet: TDataSet);
begin
  QrInadUsu.Close;
  QrInadUsu.SQL.Clear;
  QrInadUsu.SQL.Add('SELECT imv.Unidade, lan.Depto, SUM(lan.Credito) Credito,');
  QrInadUsu.SQL.Add('CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
  QrInadUsu.SQL.Add('ELSE ent.Nome END NOMEPROPRIET');
  QrInadUsu.SQL.Add('FROM ' + FTabLctA + ' lan');
  QrInadUsu.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
  QrInadUsu.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=lan.ForneceI');
  QrInadUsu.SQL.Add('LEFT JOIN condimov  imv ON imv.Conta=lan.Depto');
  QrInadUsu.SQL.Add('WHERE car.Tipo=2');
  QrInadUsu.SQL.Add('AND car.ForneceI=:P0');
  QrInadUsu.SQL.Add('AND lan.Depto>0');
  QrInadUsu.SQL.Add('AND lan.Sit<2');
  QrInadUsu.SQL.Add('AND lan.Reparcel=0');
  QrInadUsu.SQL.Add('AND lan.Vencimento < SYSDATE()');
  QrInadUsu.SQL.Add('GROUP BY imv.Unidade');
  QrInadUsu.SQL.Add('ORDER BY imv.Unidade');
  QrInadUsu.Params[0].AsInteger := QrInadTot.Params[0].AsInteger;
  UMyMod.AbreQuery(QrInadUsu, Dmod.MyDB);
end;

procedure TDmCond.QrInadTotBeforeClose(DataSet: TDataSet);
begin
  QrInadUsu.Close;
end;

procedure TDmCond.QrMU6MMCalcFields(DataSet: TDataSet);
begin
  QrMU6MMMES.Value := dmkPF.MezToMesEAno(QrMU6MMMez.Value);
end;

procedure TDmCond.QrMU6PMCalcFields(DataSet: TDataSet);
begin
  QrMU6PMMES.Value := dmkPF.MezToMesEAno(QrMU6PMMez.Value);
end;

procedure TDmCond.ReopenArreFut(Cond, MesesAnt, Controle: Integer);
var
  Loc, Peri: Integer;
begin
  Peri := MLAGeral.DataToPeriodo(DModG.ObtemAgora());
  Loc := Controle;
  if Controle < 1 then
    if QrArreFutI.State = dsBrowse then
      Loc := QrArreFutIControle.Value;
  QrArreFutI.Close;
  QrArreFutI.SQL.Clear;
  QrArreFutI.SQL.Add('SELECT IF(prp.Tipo=0, prp.RazaoSocial, prp.Nome) NOMEPROP,');
  QrArreFutI.SQL.Add('imv.Unidade UNIDADE, con.Nome NOMECONTA, ');
  QrArreFutI.SQL.Add('arf.Depto Apto, arf.CliInt Cond, imv.Propriet, arf.*');
  QrArreFutI.SQL.Add('FROM arrefut arf');
  QrArreFutI.SQL.Add('LEFT JOIN condimov  imv ON imv.Conta=arf.Depto');
  QrArreFutI.SQL.Add('LEFT JOIN entidades prp ON prp.Codigo=imv.Propriet');
  QrArreFutI.SQL.Add('LEFT JOIN contas    con ON con.Codigo=arf.Conta');
  QrArreFutI.SQL.Add('WHERE arf.' + TAB_ARI + '=0');
  QrArreFutI.SQL.Add('AND arf.CliInt=:P0');
  QrArreFutI.SQL.Add('AND arf.Periodo>=:P1');
  QrArreFutI.SQL.Add('ORDER BY arf.Periodo DESC');
  QrArreFutI.Params[00].AsInteger := Cond;
  QrArreFutI.Params[01].AsInteger := Peri - MesesAnt;
  UMyMod.AbreQuery(QrArreFutI, Dmod.MyDB);
  //
  if Loc > 0 then
    QrArreFutI.Locate('Controle', Loc, []);
end;

procedure TDmCond.ReopenContasSdo(Entidade, Codigo: Integer);
begin
  QrContasSdo.Close;
  QrContasSdo.Params[0].AsInteger := Entidade;
  UMyMod.AbreQuery(QrContasSdo, Dmod.MyDB);
  //
  if Codigo > 0 then
    QrContasSdo.Locate('Codigo', Codigo, []);
end;

procedure TDmCond.ReopenContasTrf(Entidade, Codigo, Controle: Integer);
begin
  QrContasTrf.Close;
  QrContasTrf.Params[00].AsInteger := Codigo;
  QrContasTrf.Params[01].AsInteger := Codigo;
  QrContasTrf.Params[02].AsInteger := Codigo;
  QrContasTrf.Params[03].AsInteger := Codigo;
  QrContasTrf.Params[04].AsInteger := Entidade;
  UMyMod.AbreQuery(QrContasTrf, Dmod.MyDB);
  //
  if Controle > 0 then
    QrContasTrf.Locate('Controle', Controle, []);
end;

procedure TDmCond.ReopenCreditosEDebitos(CliInt: Integer; DataI, DataF: String;
  Acordos, Periodos, Textos, NaoAgruparNada: Boolean;
  TabLctA, TabLctB, TabLctD: String);

  procedure GeraParteSQL_Cred(TabLct, CliInt_TXT, DataI, DataF: String;
  NaoAgruparNada: Boolean);
  begin
    if NaoAgruparNada = True then
      QrCreditos.SQL.Add('SELECT lan.Mez, lan.Credito, ')
    else
      QrCreditos.SQL.Add('SELECT lan.Mez, SUM(lan.Credito) Credito, ');
    QrCreditos.SQL.Add('lan.Controle, lan.Sub, lan.Carteira, lan.Cartao, lan.Tipo,');
    QrCreditos.SQL.Add('lan.Vencimento, lan.Compensado, lan.Sit, lan.Genero, ');
    QrCreditos.SQL.Add('lan.SubPgto1, gru.OrdemLista GRU_OL, ');
    QrCreditos.SQL.Add('con.OrdemLista CON_OL, sgr.OrdemLista SG_OL,');
    QrCreditos.SQL.Add('con.Nome NOMECON, sgr.Nome NOMESGR, gru.Nome NOMEGRU');
    QrCreditos.SQL.Add('FROM ' + TabLct + ' lan');
    QrCreditos.SQL.Add('LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lan.Carteira');
    QrCreditos.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas    con ON con.Codigo=lan.Genero');
    QrCreditos.SQL.Add('LEFT JOIN ' + TMeuDB + '.subgrupos sgr ON sgr.Codigo=con.SubGrupo');
    QrCreditos.SQL.Add('LEFT JOIN ' + TMeuDB + '.grupos    gru ON gru.Codigo=sgr.Grupo');
    QrCreditos.SQL.Add('WHERE lan.Tipo <> 2');
    QrCreditos.SQL.Add('AND lan.Credito > 0');
    QrCreditos.SQL.Add('AND lan.Genero>0');
    QrCreditos.SQL.Add('AND car.ForneceI=' + CliInt_TXT);
    QrCreditos.SQL.Add('AND lan.Data BETWEEN "' + DataI + '" AND "' + DataF + '"');
    if NaoAgruparNada = False then
    begin
      QrCreditos.SQL.Add('GROUP BY lan.Genero ');
      if Periodos then
        QrCreditos.SQL.Add(', lan.Mez');
      if Acordos then
        QrCreditos.SQL.Add(', lan.SubPgto1');
    end;
  end;

  procedure GeraParteSQL_Debi(TabLct, CliInt_TXT, DataI, DataF: String;
  NaoAgruparNada: Boolean);
  begin
    if Textos and (NaoAgruparNada = False) then
    begin
      QrDebitos.SQL.Add('SELECT IF(COUNT(lan.Compensado) > 1, "V�rias", IF(lan.Compensado<2,"",');
      QrDebitos.SQL.Add('DATE_FORMAT(lan.Compensado, "%d/%m"))) COMPENSADO_TXT,');
      QrDebitos.SQL.Add('IF(COUNT(lan.Data)>1, "V�rias", DATE_FORMAT(lan.Data, "%d/%m/%y")) DATA,');
      QrDebitos.SQL.Add('lan.Descricao, SUM(lan.Debito) DEBITO,');
      QrDebitos.SQL.Add('IF(COUNT(lan.Data)>1, 0, lan.NotaFiscal) NOTAFISCAL,');
      QrDebitos.SQL.Add('IF(COUNT(lan.Data)>1, "", lan.SerieCH) SERIECH,');
      QrDebitos.SQL.Add('IF(COUNT(lan.Data)>1, 0, lan.Documento) DOCUMENTO,');
      QrDebitos.SQL.Add('IF(COUNT(lan.Data)>1, 0, lan.Mez) MEZ, lan.Compensado,');
      QrDebitos.SQL.Add('con.Nome NOMECON, sgr.Nome NOMESGR, gru.Nome NOMEGRU,');
      QrDebitos.SQL.Add('COUNT(lan.Data) ITENS, ');
    end else begin
      QrDebitos.SQL.Add('SELECT IF(lan.Compensado<2,"", DATE_FORMAT(');
      QrDebitos.SQL.Add('lan.Compensado, "%d/%m")) COMPENSADO_TXT,');
      QrDebitos.SQL.Add('DATE_FORMAT(lan.Data, "%d/%m/%y") DATA, ');
      QrDebitos.SQL.Add('lan.Descricao, lan.Debito, ');
      QrDebitos.SQL.Add('IF(lan.NotaFiscal = 0, 0, lan.NotaFiscal) NOTAFISCAL,');
      QrDebitos.SQL.Add('IF(lan.SerieCH = "", "", lan.SerieCH) SERIECH,');
      QrDebitos.SQL.Add('IF(lan.Documento = 0, 0, lan.Documento) DOCUMENTO,');
      QrDebitos.SQL.Add('IF(lan.Mez = 0, 0, lan.Mez) MEZ, lan.Compensado,');
      QrDebitos.SQL.Add('con.Nome NOMECON, sgr.Nome NOMESGR, gru.Nome NOMEGRU,');
      QrDebitos.SQL.Add('0 ITENS, ');
    end;
    QrDebitos.SQL.Add('lan.Vencimento, lan.Compensado, lan.Sit, lan.Genero, ');
    QrDebitos.SQL.Add('lan.Tipo, lan.Controle, lan.Sub, lan.Carteira, lan.Cartao,');
    QrDebitos.SQL.Add('gru.OrdemLista GRU_OL, con.OrdemLista CON_OL, ');
    QrDebitos.SQL.Add('sgr.OrdemLista SGR_OL, IF(frn.Codigo=0, "", ');
    QrDebitos.SQL.Add('IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome)) NO_FORNECE');
    QrDebitos.SQL.Add('FROM ' + TabLct + ' lan');
    QrDebitos.SQL.Add('LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lan.Carteira');
    QrDebitos.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas    con ON con.Codigo=lan.Genero');
    QrDebitos.SQL.Add('LEFT JOIN ' + TMeuDB + '.subgrupos sgr ON sgr.Codigo=con.SubGrupo');
    QrDebitos.SQL.Add('LEFT JOIN ' + TMeuDB + '.grupos    gru ON gru.Codigo=sgr.Grupo');
    QrDebitos.SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades frn ON frn.Codigo=lan.Fornecedor');
    QrDebitos.SQL.Add('WHERE lan.Tipo <> 2');
    QrDebitos.SQL.Add('AND lan.Debito > 0');
    QrDebitos.SQL.Add('AND lan.Genero>0');
    QrDebitos.SQL.Add('AND car.ForneceI = :P0');
    QrDebitos.SQL.Add('AND lan.Data BETWEEN :P1 AND :P2');

    if NaoAgruparNada = False then
    begin
      if Textos then
        QrDebitos.SQL.Add('GROUP BY lan.Descricao');
    end;
  end;

var
  CliInt_TXT: String;
begin
  QrCreditos.Database := DModG.MyPID_DB;
  QrDebitos.Database  := DModG.MyPID_DB;
  //
  CliInt_TXT := FormatFloat('0', CliInt);
  //// C R � D I T O S
  QrCreditos.Close;
  QrCreditos.SQL.Clear;
  //
  QrCreditos.SQL.Add('DROP TABLE IF EXISTS _MOD_COND_CRED_1_;');
  QrCreditos.SQL.Add('CREATE TABLE _MOD_COND_CRED_1_');
  QrCreditos.SQL.Add('');
  GeraParteSQL_Cred(TabLctA, CliInt_TXT, DataI, DataF, NaoAgruparNada);
  QrCreditos.SQL.Add('UNION');
  GeraParteSQL_Cred(TabLctB, CliInt_TXT, DataI, DataF, NaoAgruparNada);
  QrCreditos.SQL.Add('UNION');
  GeraParteSQL_Cred(TabLctD, CliInt_TXT, DataI, DataF, NaoAgruparNada);
  QrCreditos.SQL.Add(';');
  QrCreditos.SQL.Add('');
  QrCreditos.SQL.Add('SELECT * FROM _MOD_COND_CRED_1_');
  if NaoAgruparNada = False then
  begin
    QrCreditos.SQL.Add('GROUP BY Genero ');
    if Periodos then
      QrCreditos.SQL.Add(', Mez');
    if Acordos then
      QrCreditos.SQL.Add(', SubPgto1');
  end;
  QrCreditos.SQL.Add('ORDER BY GRU_OL, NOMEGRU, SGR_OL, ');
  QrCreditos.SQL.Add('NOMESGR, CON_OL, NOMECON, Mez, Data;');
  QrCreditos.SQL.Add('');
  QrCreditos.SQL.Add('DROP TABLE IF EXISTS _MOD_COND_CRED_1_;');
  QrCreditos.SQL.Add('');
  //
  UMyMod.AbreQuery(QrCreditos, DModG.MyPID_DB, 'TDCond.ReopenCreditosEDebitos()');
  //
  //
  //// D � B I T O S
  QrDebitos.Close;
  QrDebitos.SQL.Clear;
  //
  QrDebitos.SQL.Add('DROP TABLE IF EXISTS _MOD_COND_DEBI_1_;');
  QrDebitos.SQL.Add('CREATE TABLE _MOD_COND_DEBI_1_');
  QrDebitos.SQL.Add('');
  GeraParteSQL_Debi(TabLctA, CliInt_TXT, DataI, DataF, NaoAgruparNada);
  QrDebitos.SQL.Add('UNION');
  GeraParteSQL_Debi(TabLctB, CliInt_TXT, DataI, DataF, NaoAgruparNada);
  QrDebitos.SQL.Add('UNION');
  GeraParteSQL_Debi(TabLctD, CliInt_TXT, DataI, DataF, NaoAgruparNada);
  QrDebitos.SQL.Add(';');
  QrDebitos.SQL.Add('');
  QrDebitos.SQL.Add('SELECT * FROM _MOD_COND_DEBI_1_');
  if NaoAgruparNada = False then
  begin
    if Textos then
      QrDebitos.SQL.Add('GROUP BY lan.Descricao');
  end;
  QrDebitos.SQL.Add('ORDER BY GRU_OL, NOMEGRU, SGR_OL,');
  QrDebitos.SQL.Add('NOMESGR, CON_OL, NOMECON, Data');
  UMyMod.AbreQuery(QrDebitos, DModG.MyPID_DB, 'procedure TDmCond.ReopenDEBIitosEDebitos()');
end;

procedure TDmCond.ReopenMU6MM(CliInt, LastP, Genero: Integer);
  procedure GeraParteSQL_MU6MM(TabLct, Mez1, Mez6, CliTXT, GenTXT: String);
  begin
    QrMU6MM.SQL.Add('SELECT ((YEAR(lan.Data)-2000) * 100) + MONTH(lan.Data) MEZ,');
    QrMU6MM.SQL.Add('SUM(lan.Debito) SUM_DEB');
    QrMU6MM.SQL.Add('FROM ' + TabLct + ' lan');
    QrMU6MM.SQL.Add('LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lan.Carteira');
    QrMU6MM.SQL.Add('WHERE car.ForneceI=' + CliTXT);
    QrMU6MM.SQL.Add('AND car.Tipo<>2');
    QrMU6MM.SQL.Add('AND lan.Mez=0');
    QrMU6MM.SQL.Add('AND lan.Debito>0');
    QrMU6MM.SQL.Add('AND lan.Genero=' + GenTXT);
    QrMU6MM.SQL.Add('AND ((YEAR(lan.Data)-2000) * 100) + ');
    QrMU6MM.SQL.Add('MONTH(lan.Data) BETWEEN "' + Mez6 + '" AND "' + Mez1 + '"');
    QrMU6MM.SQL.Add('GROUP BY ((YEAR(lan.Data)-2000) * 100) + MONTH(lan.Data)');
    //QrMU6MM.SQL.Add('ORDER BY ((YEAR(lan.Data)-2000) * 100) + MONTH(lan.Data) DESC');
  end;
  procedure GeraParteSQL_MU6MT(TabLct, Mez1, Mez6, CliTXT, GenTXT: String);
  begin
    QrMU6MT.SQL.Add('SELECT SUM(lan.Debito) SUM_DEB');
    QrMU6MT.SQL.Add('FROM ' + TabLct + ' lan');
    QrMU6MT.SQL.Add('LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lan.Carteira');
    QrMU6MT.SQL.Add('WHERE car.ForneceI=' + CliTXT);
    QrMU6MT.SQL.Add('AND car.Tipo<>2');
    QrMU6MT.SQL.Add('AND lan.Debito>0');
    QrMU6MT.SQL.Add('AND lan.Mez=0');
    QrMU6MT.SQL.Add('AND lan.Genero=' + GenTXT);
    QrMU6MT.SQL.Add('AND ((YEAR(lan.Data)-2000) * 100) +');
    QrMU6MT.SQL.Add('MONTH(lan.Data) BETWEEN "' + Mez6 + '" AND "' + Mez1 + '"');
  end;
var
  Mez1, Mez6, CliTXT, GenTXT: String;
begin
  Mez1   := FormatFloat('0', MLAGeral.PeriodoToMez(LastP-1));
  Mez6   := FormatFloat('0', MLAGeral.PeriodoToMez(LastP-6));
  CliTXT := FormatFloat('0', CliInt);
  GenTXT := FormatFloat('0', Genero);
  //
  QrMU6MM.Close;
  QrMU6MM.Database := DModG.MyPID_DB;
  FMU6MM := UCriar.RecriaTempTableNovo(ntrttMU6MM, DmodG.QrUpdPID1, False);
  QrMU6MM.SQL.Clear;
  //
  {
  QrMU6MM.SQL.Add('DROP TABLE IF EXISTS _MOD_COND_MU6MM_;');
  QrMU6MM.SQL.Add('CREATE TABLE _MOD_COND_MU6MM_');
  }
  QrMU6MM.SQL.Add('INSERT INTO _MOD_COND_MU6MM_');
  QrMU6MM.SQL.Add('');
  GeraParteSQL_MU6MM(FTabLctA, Mez1, Mez6, CliTXT, GenTXT);
  QrMU6MM.SQL.Add('UNION');
  GeraParteSQL_MU6MM(FTabLctB, Mez1, Mez6, CliTXT, GenTXT);
  QrMU6MM.SQL.Add('UNION');
  GeraParteSQL_MU6MM(FTabLctD, Mez1, Mez6, CliTXT, GenTXT);
  QrMU6MM.SQL.Add(';');
  QrMU6MM.SQL.Add('');
  QrMU6MM.SQL.Add('SELECT MEZ, SUM(SUM_DEB) SUM_DEB ');
  QrMU6MM.SQL.Add('FROM _MOD_COND_MU6MM_');
  QrMU6MM.SQL.Add('GROUP BY MEZ ');
  QrMU6MM.SQL.Add('ORDER BY MEZ;');
  QrMU6MM.SQL.Add('');
  UMyMod.AbreQuery(QrMU6MM, DModG.MyPID_DB, 'TFmReceDesp.DemostrativoDeReceitasEDespesas()');
  //
  //
  QrMU6MT.Close;
  QrMU6MT.Database := DModG.MyPID_DB;
  FMU6MT := UCriar.RecriaTempTableNovo(ntrttMU6MT, DmodG.QrUpdPID1, False);
  QrMU6MT.SQL.Clear;
  //
  {
  QrMU6MT.SQL.Add('DROP TABLE IF EXISTS _MOD_COND_MU6MT_;');
  QrMU6MT.SQL.Add('CREATE TABLE _MOD_COND_MU6MT_');
  }
  QrMU6MT.SQL.Add('INSERT INTO _MOD_COND_MU6MT_');
  QrMU6MT.SQL.Add('');
  GeraParteSQL_MU6MT(FTabLctA, Mez1, Mez6, CliTXT, GenTXT);
  QrMU6MT.SQL.Add('UNION');
  GeraParteSQL_MU6MT(FTabLctB, Mez1, Mez6, CliTXT, GenTXT);
  QrMU6MT.SQL.Add('UNION');
  GeraParteSQL_MU6MT(FTabLctD, Mez1, Mez6, CliTXT, GenTXT);
  QrMU6MT.SQL.Add(';');
  QrMU6MT.SQL.Add('');
  QrMU6MT.SQL.Add('SELECT SUM(SUM_DEB) SUM_DEB ');
  QrMU6MT.SQL.Add('FROM _MOD_COND_MU6MT_;');
  QrMU6MT.SQL.Add('');
  UMyMod.AbreQuery(QrMU6MT, DModG.MyPID_DB, 'TFmReceDesp.DemostrativoDeReceitasEDespesas()');
  //
end;

procedure TDmCond.ReopenMU6PM(CliInt, LastP, Genero: Integer);
  procedure GeraParteSQL_MU6PM(TabLct, Mez1, Mez6, CliTXT, GenTXT: String);
  begin
    QrMU6PM.SQL.Add('SELECT lan.Mez, SUM(lan.Debito) SUM_DEB');
    QrMU6PM.SQL.Add('FROM ' + TabLct + ' lan');
    QrMU6PM.SQL.Add('LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lan.Carteira');
    QrMU6PM.SQL.Add('WHERE car.ForneceI=' + CliTXT);
    QrMU6PM.SQL.Add('AND car.Tipo<>2');
    QrMU6PM.SQL.Add('AND lan.Debito>0');
    QrMU6PM.SQL.Add('AND lan.Genero=' + GenTXT);
    QrMU6PM.SQL.Add('AND lan.Mez BETWEEN "' + Mez6 + '" AND "' + Mez1 + '"');
    QrMU6PM.SQL.Add('GROUP BY lan.Mez');
  end;
  procedure GeraParteSQL_MU6PT(TabLct, Mez1, Mez6, CliTXT, GenTXT: String);
  begin
    QrMU6PT.SQL.Add('SELECT SUM(lan.Debito) SUM_DEB');
    QrMU6PT.SQL.Add('FROM ' + TabLct + ' lan');
    QrMU6PT.SQL.Add('LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lan.Carteira');
    QrMU6PT.SQL.Add('WHERE car.ForneceI=' + CliTXT);
    QrMU6PT.SQL.Add('AND car.Tipo<>2');
    QrMU6PT.SQL.Add('AND lan.Debito>0');
    QrMU6PT.SQL.Add('AND lan.Genero=' + GenTXT);
    QrMU6PT.SQL.Add('AND lan.Mez BETWEEN "' + Mez6 + '" AND "' + Mez1 + '"');
  end;
var
  Mez1, Mez6, CliTXT, GenTxt: String;
begin
  Mez1   := FormatFloat('0', MLAGeral.PeriodoToMez(LastP-1));
  Mez6   := FormatFloat('0', MLAGeral.PeriodoToMez(LastP-6));
  CliTXT := FormatFloat('0', CliInt);
  GenTXT := FormatFloat('0', Genero);
  //
  QrMU6PM.Close;
  QrMU6PM.Database := DModG.MyPID_DB;
  QrMU6PM.SQL.Clear;
  //
  QrMU6PM.SQL.Add('DROP TABLE IF EXISTS _MOD_COND_MU6PM_;');
  QrMU6PM.SQL.Add('CREATE TABLE _MOD_COND_MU6PM_');
  QrMU6PM.SQL.Add('');
  GeraParteSQL_MU6PM(FTabLctA, Mez1, Mez6, CliTXT, GenTXT);
  QrMU6PM.SQL.Add('UNION');
  GeraParteSQL_MU6PM(FTabLctB, Mez1, Mez6, CliTXT, GenTXT);
  QrMU6PM.SQL.Add('UNION');
  GeraParteSQL_MU6PM(FTabLctD, Mez1, Mez6, CliTXT, GenTXT);
  QrMU6PM.SQL.Add(';');
  QrMU6PM.SQL.Add('');
  QrMU6PM.SQL.Add('SELECT Mez, SUM(SUM_DEB) SUM_DEB ');
  QrMU6PM.SQL.Add('FROM _MOD_COND_MU6PM_');
  QrMU6PM.SQL.Add('GROUP BY Mez');
  QrMU6PM.SQL.Add('ORDER BY Mez DESC');
  QrMU6PM.SQL.Add('');
  UMyMod.AbreQuery(QrMU6PM, DModG.MyPID_DB, 'TFmReceDesp.DemostrativoDeReceitasEDespesas()');
  //
  //
  QrMU6PT.Close;
  QrMU6MT.Database := DModG.MyPID_DB;
  QrMU6PT.SQL.Clear;
  //
  QrMU6PT.SQL.Add('DROP TABLE IF EXISTS _MOD_COND_MU6PT_;');
  QrMU6PT.SQL.Add('CREATE TABLE _MOD_COND_MU6PT_');
  QrMU6PT.SQL.Add('');
  GeraParteSQL_MU6PT(FTabLctA, Mez1, Mez6, CliTXT, GenTXT);
  QrMU6PT.SQL.Add('UNION');
  GeraParteSQL_MU6PT(FTabLctB, Mez1, Mez6, CliTXT, GenTXT);
  QrMU6PT.SQL.Add('UNION');
  GeraParteSQL_MU6PT(FTabLctD, Mez1, Mez6, CliTXT, GenTXT);
  QrMU6PT.SQL.Add(';');
  QrMU6PT.SQL.Add('');
  QrMU6PT.SQL.Add('SELECT SUM(SUM_DEB) SUM_DEB ');
  QrMU6PT.SQL.Add('FROM _MOD_COND_MU6PT_;');
  QrMU6PT.SQL.Add('');
  UMyMod.AbreQuery(QrMU6PT, DModG.MyPID_DB, 'TFmReceDesp.DemostrativoDeReceitasEDespesas()');
  //
end;

procedure TDmCond.ReopenQrCond(Condominio: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCond, Dmod.MyDB, [
    'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMECLI, mun.Nome CIDADE, ',
    'IF(ent.Tipo=0, ent.EUF, ent.PUF) + 0.000 UF, ufs.Nome NOMEUF, bcs.DVB, ',
    'car.Ativo CART_ATIVO, ',
    'car.TipoDoc CAR_TIPODOC, con.*, ',
    'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) DOCNUM ',
    'FROM cond con ',
    'LEFT JOIN entidades ent ON ent.Codigo=con.Cliente ',
    'LEFT JOIN cnab_cfg cfg ON (cfg.Cedente=con.Cliente OR cfg.SacadAvali=con.Cliente) ',
    'LEFT JOIN bancos   bcs ON bcs.Codigo=cfg.CedBanco ',
    'LEFT JOIN carteiras car ON car.Codigo=cfg.CartEmiss ',
    'LEFT JOIN ufs   ufs ON ufs.Codigo=IF(ent.Tipo=0, ent.EUF, ent.PUF) ',
    'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mun ON mun.Codigo = IF(ent.Tipo=0, ent.ECodMunici, ent.PCodMunici) ',
    'WHERE con.Codigo=' + Geral.FF0(Condominio),
    'GROUP BY con.Codigo ',
    '']);
end;

procedure TDmCond.ReopenQrCons(Codigo: Integer);
var
  Cond_Txt, Peri_Txt:String;
begin
{ Alexandria
  Cond_Txt := FormatFloat('0', QrCondCodigo.Value);
  Peri_Txt := FormatFloat('0', FmCondGer.QrPrevPeriodo.Value);
  //
  QrCons.Close;
  QrCons.SQL.Clear;
  QrCons.SQL.Add('SELECT DISTINCT cns.Codigo, cns.Nome, cnp.Preco,');
  QrCons.SQL.Add('cnp.Casas, cnp.UnidLei, cnp.UnidImp, cnp.UnidFat');
  QrCons.SQL.Add('FROM consprc cnp');
  QrCons.SQL.Add('LEFT JOIN cons    cns ON cns.Codigo=cnp.Codigo');
  QrCons.SQL.Add('LEFT JOIN ' + DmCond.FTabCnsA + ' cni ON cni.Codigo=cns.Codigo');
  QrCons.SQL.Add('WHERE cni.Cond=' + Cond_Txt);
  QrCons.SQL.Add('AND cnp.Cond=' + Cond_Txt);
  QrCons.SQL.Add('AND cni.Periodo=' + Peri_Txt);
  UMyMod.AbreQuery(QrCons, Dmod.MyDB);
  //
  QrCons.Locate('Codigo', Codigo, []);
}
end;


procedure TDmCond.VerificaAccManager(Sentido: TSentidoAssistente);
  procedure AtualizaCond();
  var
    Assistente, Codigo: Integer;
  begin
    Assistente := QrAccMgrAccManager.Value;
    Codigo  := QrAccMgrCodigo.Value;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'cond', False, [
    'Assistente'], ['Codigo'], [
    Assistente], [Codigo], True);
  end;
  //
  procedure AtualizaEntiCliInt();
  var
    AccManager, CodCliInt: Integer;
  begin
    AccManager := QrAccMgrAssistente.Value;
    CodCliInt  := QrAccMgrCodCliInt.Value;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'enticliint', False, [
    'AccManager'], ['CodCliInt'], [
    AccManager], [CodCliInt], True);
  end;
begin
  QrAccMgr.Close;
  UMyMod.AbreQuery(QrAccMgr, Dmod.MyDB);
  while not QrAccMgr.Eof do
  begin
    case Sentido of
      saDeAssistenteParaAccManager: AtualizaEntiCliInt();
      saDeAccManagerParaAssistente: AtualizaCond();
      saDeNaoZeradoParaZerado:
      begin
        if (QrAccMgrAccManager.Value = 0) and
        (QrAccMgrAssistente.Value <> 0) then
          AtualizaEntiCliInt()
        else
        if (QrAccMgrAccManager.Value <> 0) and
        (QrAccMgrAssistente.Value = 0) then
          AtualizaCond();
      end;
    end;
    //
    QrAccMgr.Next;
  end;
end;

(*
Esta procedure foi substitu�da pala procedure DmBloq.ReopenCNAB_Cfg_Cond

procedure TDmCond.ReopenCNAB_Cfg(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCNAB_Cfg, Dmod.MyDB, [
  'SELECT * ',
  'FROM cnab_cfg ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  '']);
end;
*)

procedure TDmCond.ReopenCNAB_CfgSel(Query: TmySQLQuery; Database: TmySQLDatabase;
  Cliente: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Database, [
    'SELECT Codigo, Nome ',
    'FROM CNAB_Cfg ',
    'WHERE Cedente=' + Geral.FF0(Cliente),
    '']);
end;

procedure TDmCond.ReopenCNS(Controle: Integer);
begin
{ Alexandria
  QrCNS.Close;
  QrCNS.SQL.Clear;
  QrCNS.SQL.Add('SELECT cdb.Descri Bloco, cdi.Unidade, cnp.Casas, cnp.UnidLei,');
  QrCNS.SQL.Add('cnp.UnidImp, cnp.UnidFat, cnp.CasRat, cni.*');
  QrCNS.SQL.Add('FROM cons cns');
  QrCNS.SQL.Add('LEFT JOIN ' + DmCond.FTabCnsA + '  cni ON cni.Codigo=cns.Codigo');
  QrCNS.SQL.Add('LEFT JOIN consprc  cnp ON cnp.Codigo=cns.Codigo');
  QrCNS.SQL.Add('LEFT JOIN condimov cdi ON cdi.Conta=cni.Apto');
  QrCNS.SQL.Add('LEFT JOIN condbloco cdb ON cdb.Controle=cdi.Controle');
  QrCNS.SQL.Add('WHERE cni.Cond=:P0');
  QrCNS.SQL.Add('AND cnp.Cond=:P1');
  QrCNS.SQL.Add('AND cni.Periodo=:P2');
  QrCNS.SQL.Add('AND cni.Codigo=:P3');
  QrCNS.SQL.Add('ORDER BY cdb.Ordem, cdi.Andar, cdi.Unidade');
  QrCNS.Params[00].AsInteger := QrCondCodigo.Value;
  QrCNS.Params[01].AsInteger := QrCondCodigo.Value;
  QrCNS.Params[02].AsInteger := FmCondGer.QrPrevPeriodo.Value;
  QrCNS.Params[03].AsInteger := QrConsCodigo.Value;
  UMyMod.AbreQuery(QrCNS, Dmod.MyDB);
  //
  QrCNS.Locate('Controle', Controle, []);
  // Ap�s exclus�o do �ltimo
  if QrCNS.RecordCount = 0 then
    ReopenQrCons(QrConsCodigo.Value);
}
end;

{
QrConsIts -> QrCNS
}

end.
