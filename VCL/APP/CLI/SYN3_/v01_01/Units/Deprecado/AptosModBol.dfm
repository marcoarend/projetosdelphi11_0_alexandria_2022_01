object FmAptosModBol: TFmAptosModBol
  Left = 339
  Top = 185
  Caption = 'CAD-CONDO-009 :: Sele'#231#227'o de UHs - Modelo e Config. Bloq.'
  ClientHeight = 580
  ClientWidth = 772
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 47
    Width = 772
    Height = 421
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 772
      Height = 252
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object RGModelBloq: TRadioGroup
        Left = 0
        Top = 0
        Width = 772
        Height = 145
        Align = alClient
        Caption = ' Modelo de impress'#227'o do bloqueto: '
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'FmPrincipal.PreencheModelosBloq')
        TabOrder = 0
      end
      object GBConfig: TGroupBox
        Left = 0
        Top = 145
        Width = 772
        Height = 44
        Align = alBottom
        Caption = ' Configura'#231#227'o do modelo H ou R: '
        TabOrder = 1
        object SpeedButton2: TSpeedButton
          Left = 738
          Top = 18
          Width = 20
          Height = 20
          Caption = '...'
          OnClick = SpeedButton2Click
        end
        object EdConfigBol: TdmkEditCB
          Left = 6
          Top = 18
          Width = 55
          Height = 20
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBConfigBol
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBConfigBol: TdmkDBLookupComboBox
          Left = 67
          Top = 18
          Width = 667
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsConfigBol
          TabOrder = 1
          dmkEditCB = EdConfigBol
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
      end
      object RGCompe: TRadioGroup
        Left = 0
        Top = 189
        Width = 772
        Height = 37
        Align = alBottom
        Caption = ' Ficha de compensa'#231#227'o (modelo E): '
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          '??'
          '1 (uma via)'
          '2 (duas vias)')
        TabOrder = 2
      end
      object Panel4: TPanel
        Left = 0
        Top = 226
        Width = 772
        Height = 26
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 3
        object CkBalAgrMens: TCheckBox
          Left = 6
          Top = 6
          Width = 400
          Height = 16
          Caption = 
            'Separar as somas de valores no balancete por compet'#234'ncia ao agru' +
            'par valores.'
          Checked = True
          State = cbChecked
          TabOrder = 0
        end
      end
    end
    object DBGrid1: TdmkDBGrid
      Left = 0
      Top = 252
      Width = 772
      Height = 169
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'Ativo'
          Title.Caption = 'OK'
          Width = 16
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEBLOCO'
          Title.Caption = 'Bloco'
          Width = 112
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Andar'
          Width = 27
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Unidade'
          Width = 64
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEPROPR'
          Title.Caption = 'Propriet'#225'rio'
          Width = 374
          Visible = True
        end>
      Color = clWindow
      DataSource = DataSource1
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnCellClick = DBGrid1CellClick
      Columns = <
        item
          Expanded = False
          FieldName = 'Ativo'
          Title.Caption = 'OK'
          Width = 16
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEBLOCO'
          Title.Caption = 'Bloco'
          Width = 112
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Andar'
          Width = 27
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Unidade'
          Width = 64
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEPROPR'
          Title.Caption = 'Propriet'#225'rio'
          Width = 374
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 772
    Height = 47
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 725
      Top = 0
      Width = 47
      Height = 47
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 31
        Height = 31
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 47
      Height = 47
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 47
      Top = 0
      Width = 678
      Height = 47
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 469
        Height = 31
        Caption = 'Sele'#231#227'o de UHs - Modelo e Config. Bloq.'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 469
        Height = 31
        Caption = 'Sele'#231#227'o de UHs - Modelo e Config. Bloq.'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 469
        Height = 31
        Caption = 'Sele'#231#227'o de UHs - Modelo e Config. Bloq.'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 468
    Width = 772
    Height = 43
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel2: TPanel
      Left = 2
      Top = 14
      Width = 768
      Height = 28
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 511
    Width = 772
    Height = 69
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 628
      Top = 14
      Width = 142
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 118
        Height = 39
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel5: TPanel
      Left = 2
      Top = 14
      Width = 626
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 118
        Height = 39
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrUHs: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      
        'SELECT imv.Conta Apto, imv.Andar, imv.Unidade, blc.Descri NOMEBL' +
        'OCO,'
      'IF(prp.Tipo=0,prp.RazaoSocial,prp.Nome) NOMEPROPR'#13
      'FROM condimov imv'
      'LEFT JOIN condbloco blc ON blc.Controle=imv.Controle'
      'LEFT JOIN entidades prp ON prp.Codigo=imv.Propriet'
      'WHERE imv.Codigo=:P0'
      'ORDER BY blc.Ordem, imv.Andar, imv.Unidade')
    Left = 68
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrUHsApto: TIntegerField
      FieldName = 'Apto'
      Required = True
    end
    object QrUHsUnidade: TWideStringField
      FieldName = 'Unidade'
      Required = True
      Size = 10
    end
    object QrUHsNOMEBLOCO: TWideStringField
      FieldName = 'NOMEBLOCO'
      Size = 100
    end
    object QrUHsNOMEPROPR: TWideStringField
      FieldName = 'NOMEPROPR'
      Size = 100
    end
    object QrUHsAndar: TIntegerField
      FieldName = 'Andar'
      Required = True
    end
  end
  object DataSource1: TDataSource
    Left = 40
    Top = 12
  end
  object QrConfigBol: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM configbol'
      'ORDER BY Nome')
    Left = 716
    Top = 12
    object QrConfigBolCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrConfigBolNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsConfigBol: TDataSource
    DataSet = QrConfigBol
    Left = 744
    Top = 12
  end
end
