object FmCondEmeiosAdd: TFmCondEmeiosAdd
  Left = 339
  Top = 185
  Caption = 'CAD-CONDO-011 :: Adi'#231#227'o de E-mails de UHs'
  ClientHeight = 507
  ClientWidth = 807
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 807
    Height = 345
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitLeft = 100
    ExplicitHeight = 411
    object dmkDBGridDAC1: TdmkDBGridDAC
      Left = 0
      Top = 0
      Width = 807
      Height = 345
      SQLFieldsToChange.Strings = (
        'Ativo'
        'Emeio')
      SQLIndexesOnUpdate.Strings = (
        'Item')
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'Ativo'
          Title.Caption = 'OK'
          Width = 18
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ExtrNom'
          Title.Caption = 'Unidade'
          Width = 70
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TipoStr'
          Title.Caption = 'Classe'
          Width = 84
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'EntiNom'
          Title.Caption = 'Nome do propriet'#225'rio / morador'
          Width = 300
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Emeio'
          Title.Caption = 'Emeio do propriet'#225'rio / morador'
          Width = 300
          Visible = True
        end>
      Color = clWindow
      DataSource = DsEmeios
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      SQLTable = 'emeios'
      EditForceNextYear = False
      Columns = <
        item
          Expanded = False
          FieldName = 'Ativo'
          Title.Caption = 'OK'
          Width = 18
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ExtrNom'
          Title.Caption = 'Unidade'
          Width = 70
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TipoStr'
          Title.Caption = 'Classe'
          Width = 84
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'EntiNom'
          Title.Caption = 'Nome do propriet'#225'rio / morador'
          Width = 300
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Emeio'
          Title.Caption = 'Emeio do propriet'#225'rio / morador'
          Width = 300
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 807
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 784
    object GB_R: TGroupBox
      Left = 759
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 736
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 711
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 688
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 317
        Height = 32
        Caption = 'Adi'#231#227'o de E-mails de UHs'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 317
        Height = 32
        Caption = 'Adi'#231#227'o de E-mails de UHs'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 317
        Height = 32
        Caption = 'Adi'#231#227'o de E-mails de UHs'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 393
    Width = 807
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 378
    ExplicitWidth = 784
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 803
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 780
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 437
    Width = 807
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 422
    ExplicitWidth = 784
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 803
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 636
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        TabOrder = 0
        OnClick = BtOKClick
        NumGlyphs = 2
      end
      object BtTodos: TBitBtn
        Tag = 127
        Left = 242
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Todos'
        TabOrder = 1
        OnClick = BtTodosClick
        NumGlyphs = 2
      end
      object BtNenhum: TBitBtn
        Tag = 128
        Left = 370
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Nenhum'
        TabOrder = 2
        OnClick = BtNenhumClick
        NumGlyphs = 2
      end
      object Panel3: TPanel
        Left = 656
        Top = 0
        Width = 147
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 3
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
          NumGlyphs = 2
        end
      end
    end
  end
  object QrEmeios: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT * FROM emeios')
    Left = 264
    Top = 204
    object QrEmeiosEntiCod: TIntegerField
      FieldName = 'EntiCod'
      Origin = 'emeios.EntiCod'
    end
    object QrEmeiosExtrCod: TIntegerField
      FieldName = 'ExtrCod'
      Origin = 'emeios.ExtrCod'
    end
    object QrEmeiosAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'emeios.Ativo'
      MaxValue = 1
    end
    object QrEmeiosEmeio: TWideStringField
      FieldName = 'Emeio'
      Origin = 'emeios.Emeio'
      Size = 255
    end
    object QrEmeiosEntiNom: TWideStringField
      FieldName = 'EntiNom'
      Origin = 'emeios.EntiNom'
      Size = 255
    end
    object QrEmeiosExtrNom: TWideStringField
      FieldName = 'ExtrNom'
      Origin = 'emeios.ExtrNom'
      Size = 255
    end
    object QrEmeiosTipoStr: TWideStringField
      FieldName = 'TipoStr'
      Origin = 'emeios.TipoStr'
      Size = 30
    end
    object QrEmeiosItem: TIntegerField
      FieldName = 'Item'
      Origin = 'emeios.Item'
    end
    object QrEmeiosExtrInt1: TIntegerField
      FieldName = 'ExtrInt1'
    end
    object QrEmeiosExtrInt2: TIntegerField
      FieldName = 'ExtrInt2'
    end
  end
  object DsEmeios: TDataSource
    DataSet = QrEmeios
    Left = 292
    Top = 204
  end
end
