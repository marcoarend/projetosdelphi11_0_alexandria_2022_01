unit InadimpBloq;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkLabel, dmkEdit,
  ComCtrls, dmkEditDateTimePicker, dmkDBGrid, DB, mySQLDbTables, frxClass,
  frxDBSet, dmkGeral, dmkDBGridDAC, DmkDAC_PF, dmkImage, UnDmkProcFunc,
  UnDmkEnums;

type
  TFmInadimpBloq = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    TPVencto: TdmkEditDateTimePicker;
    EdMulta: TdmkEdit;
    EdJuros: TdmkEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    DBGBloq: TdmkDBGridDAC;
    QrBloqInad: TmySQLQuery;
    DsBloqInad: TDataSource;
    QrBloqInadEmpresa: TIntegerField;
    QrBloqInadPropriet: TIntegerField;
    QrBloqInadUnidade: TWideStringField;
    QrBloqInadApto: TIntegerField;
    QrBloqInadImobiliaria: TIntegerField;
    QrBloqInadProcurador: TIntegerField;
    QrBloqInadUsuario: TIntegerField;
    QrBloqInadData: TDateField;
    QrBloqInadCliInt: TIntegerField;
    QrBloqInadCREDITO: TFloatField;
    QrBloqInadPAGO: TFloatField;
    QrBloqInadJuros: TFloatField;
    QrBloqInadMulta: TFloatField;
    QrBloqInadTOTAL: TFloatField;
    QrBloqInadSALDO: TFloatField;
    QrBloqInadPEND_VAL: TFloatField;
    QrBloqInadDescricao: TWideStringField;
    QrBloqInadMez: TIntegerField;
    QrBloqInadMEZ_TXT: TWideStringField;
    QrBloqInadVencimento: TDateField;
    QrBloqInadCompensado: TDateField;
    QrBloqInadControle: TIntegerField;
    QrBloqInadFatNum: TFloatField;
    QrBloqInadVCTO_TXT: TWideStringField;
    QrBloqInadNOMEEMPCOND: TWideStringField;
    QrBloqInadNOMEPRPIMOV: TWideStringField;
    QrBloqInadAtivo: TSmallintField;
    frxCondE2: TfrxReport;
    frxDsBoletos: TfrxDBDataset;
    QrBloqInadNewVencto: TDateField;
    QrBoletos: TmySQLQuery;
    QrBoletosSUB_TOT: TFloatField;
    QrBoletosVencto: TDateField;
    QrBoletosBoleto: TFloatField;
    QrBoletosUnidade: TWideStringField;
    QrBoletosApto: TIntegerField;
    frxDsInquilino: TfrxDBDataset;
    frxBloq: TfrxReport;
    CkZerado: TCheckBox;
    CkDesign: TCheckBox;
    QrBoletosBLOQUETO: TFloatField;
    QrBoletosMEZ_TXT: TWideStringField;
    Splitter1: TSplitter;
    dmkDBGrid1: TdmkDBGrid;
    QrBloqIts: TmySQLQuery;
    QrBloqItsGenero: TIntegerField;
    QrBloqItsDescricao: TWideStringField;
    QrBloqItsCredito: TFloatField;
    DsBloqIts: TDataSource;
    frxDsBoletosIts: TfrxDBDataset;
    QrBoletosIts: TmySQLQuery;
    QrBoletosItsGenero: TIntegerField;
    QrBoletosItsDescricao: TWideStringField;
    QrBoletosItsCredito: TFloatField;
    QrBoletosMez: TIntegerField;
    QrBoletosFatNum: TFloatField;
    QrBoletosVencimento: TDateField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtRecalcula: TBitBtn;
    BtImprime: TBitBtn;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    frxDsCNAB_Cfg_B: TfrxDBDataset;
    QrCNAB_Cfg_B: TmySQLQuery;
    QrCNAB_Cfg_BNOMEBANCO: TWideStringField;
    QrCNAB_Cfg_BLocalPag: TWideStringField;
    QrCNAB_Cfg_BNOMECED: TWideStringField;
    QrCNAB_Cfg_BEspecieDoc: TWideStringField;
    QrCNAB_Cfg_BACEITETIT_TXT: TWideStringField;
    QrCNAB_Cfg_BCART_IMP: TWideStringField;
    QrCNAB_Cfg_BEspecieVal: TWideStringField;
    QrCNAB_Cfg_BCedBanco: TIntegerField;
    QrCNAB_Cfg_BAgContaCed: TWideStringField;
    QrCNAB_Cfg_BCedAgencia: TIntegerField;
    QrCNAB_Cfg_BCedPosto: TIntegerField;
    QrCNAB_Cfg_BCedConta: TWideStringField;
    QrCNAB_Cfg_BCartNum: TWideStringField;
    QrCNAB_Cfg_BIDCobranca: TWideStringField;
    QrCNAB_Cfg_BCodEmprBco: TWideStringField;
    QrCNAB_Cfg_BTipoCobranca: TIntegerField;
    QrCNAB_Cfg_BCNAB: TIntegerField;
    QrCNAB_Cfg_BCtaCooper: TWideStringField;
    QrCNAB_Cfg_BModalCobr: TIntegerField;
    QrCNAB_Cfg_BMultaPerc: TFloatField;
    QrCNAB_Cfg_BJurosPerc: TFloatField;
    QrCNAB_Cfg_BTexto01: TWideStringField;
    QrCNAB_Cfg_BTexto02: TWideStringField;
    QrCNAB_Cfg_BTexto03: TWideStringField;
    QrCNAB_Cfg_BTexto04: TWideStringField;
    QrCNAB_Cfg_BTexto05: TWideStringField;
    QrCNAB_Cfg_BTexto06: TWideStringField;
    QrCNAB_Cfg_BTexto07: TWideStringField;
    QrCNAB_Cfg_BTexto08: TWideStringField;
    QrCNAB_Cfg_BCedDAC_C: TWideStringField;
    QrCNAB_Cfg_BCedDAC_A: TWideStringField;
    QrCNAB_Cfg_BOperCodi: TWideStringField;
    QrCNAB_Cfg_BLayoutRem: TWideStringField;
    QrCNAB_Cfg_BNOMESAC: TWideStringField;
    QrCNAB_Cfg_BCorreio: TWideStringField;
    QrCNAB_Cfg_BCartEmiss: TIntegerField;
    QrCNAB_Cfg_BCedente: TIntegerField;
    QrCNAB_Cfg_BCAR_TIPODOC: TIntegerField;
    QrCNAB_Cfg_BCART_ATIVO: TIntegerField;
    QrCNAB_Cfg_BDVB: TWideStringField;
    QrBloqInadFatID: TIntegerField;
    QrBoletosEmpresa: TIntegerField;
    QrBoletosFatID: TIntegerField;
    QrLoc: TmySQLQuery;
    QrBoletosControle: TIntegerField;
    QrCNAB_Cfg_BNosNumFxaU: TFloatField;
    QrCNAB_Cfg_BNosNumFxaI: TFloatField;
    QrCNAB_Cfg_BNosNumFxaF: TFloatField;
    QrCNAB_Cfg_BCodigo: TFloatField;
    QrBloqInadForneceI: TIntegerField;
    QrCNAB_Cfg_BCorresBco: TIntegerField;
    QrCNAB_Cfg_BCorresAge: TIntegerField;
    QrCNAB_Cfg_BCorresCto: TWideStringField;
    QrCNAB_Cfg_BNPrinBc: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure TPVenctoChange(Sender: TObject);
    procedure TPVenctoClick(Sender: TObject);
    procedure EdMultaChange(Sender: TObject);
    procedure EdJurosChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtRecalculaClick(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure QrBoletosAfterScroll(DataSet: TDataSet);
    procedure frxCondE2GetValue(const VarName: string; var Value: Variant);
    procedure QrBoletosCalcFields(DataSet: TDataSet);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure QrBloqInadBeforeClose(DataSet: TDataSet);
    procedure QrBloqInadAfterScroll(DataSet: TDataSet);
    procedure QrBoletosBeforeClose(DataSet: TDataSet);
    procedure DBGBloqCellClick(Column: TColumn);
  private
    { Private declarations }
    FNossoNumero: String;
    FLoteImp: Integer;
    function  ImprimeBoletos(Quais: TselType; Como: TfrxImpComo;
              Arquivo: String; Filtro: TfrxCustomExportFilter;
              PrevCod: Integer; Antigo: Boolean; var SdoJaAtz: Boolean): TfrxReport;
    function  frxMostra(PrevCod: Integer; Titulo: string; Tipo: TselType;
              Antigo: Boolean): Boolean;
    function  frxComposite(PrevCod, Apto: Integer; Titulo: string;
              var ModelBloqAnt, ConfigAnt: Integer; ClearLastReport, Antigo: Boolean):
              Boolean;
    function  ObtemCNAB_Cfg(FatNum: Double; FatID, Lancto: Integer;
              var CNAB_Cfg: Integer): Boolean;
    procedure FechaPesquisa();
    procedure AtualizaTodos(Status: Integer);
    procedure ReopenBloqInad(Controle: Integer);
    procedure DefineFrx(frx: TfrxReport);
  public
    { Public declarations }
    FTabLctA, FTabAriA, FTabCnsA, FTabBloqInad: String;
    FNivelJuridico: Integer;
  end;

  var
  FmInadimpBloq: TFmInadimpBloq;

implementation

uses ModuleGeral, UMySQLModule, ModuleCond, UnBancos, CondGerImpGer, MyGlyfs,
  MeuFrx, UnInternalConsts, UnMyObjects, ModuleBloq, Module;

{$R *.DFM}

procedure TFmInadimpBloq.AtualizaTodos(Status: Integer);
begin
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('UPDATE ' + FTabBloqInad + ' SET Ativo=:P0');
  DModG.QrUpdPID1.Params[00].AsInteger := Status;
  DModG.QrUpdPID1.ExecSQL;
  //
  ReopenBloqInad(QrBloqInadControle.Value);
end;

procedure TFmInadimpBloq.BtImprimeClick(Sender: TObject);
var
  SdoJaAtz: Boolean;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrBoletos, DModG.MyPID_DB, [
    'SELECT PEND_VAL SUB_TOT, NewVencto Vencto, ',
    'FatNum Boleto, Unidade, Apto, MEZ_TXT, FatNum, ',
    'Mez, Vencimento, Empresa, FatID, Controle ',
    'FROM ' + FTabBloqInad,
    'WHERE Ativo = 1 ',
    '']);
  //
  if QrBoletos.RecordCount = 0 then
  begin
    Geral.MensagemBox('N�o foi selecionado nenhum bloqueto para ser impresso!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  DmCond.ReopenQrCond(QrBloqInadEmpresa.Value);
  //
  SdoJaAtz := False;
  ImprimeBoletos(istTodos, ficMostra, '', nil,
    0(*QrPrevCodigo.Value*), False(*CkAntigo.Checked*), SdoJaAtz);
end;

procedure TFmInadimpBloq.BtNenhumClick(Sender: TObject);
begin
  AtualizaTodos(0);
end;

procedure TFmInadimpBloq.DBGBloqCellClick(Column: TColumn);
var
  Imp: Integer;
begin
  if Column.FieldName = 'Ativo' then
  begin
    Screen.Cursor := crHourGlass;
    try
      if QrBloqInadAtivo.Value = 1 then
        Imp := 0
      else
        Imp := 1;
      //
      DModG.QrUpdPID1.SQL.Clear;
      DModG.QrUpdPID1.SQL.Add('UPDATE ' + FTabBloqInad + ' SET Ativo=:P0');
      DModG.QrUpdPID1.SQL.Add('WHERE Empresa=:P1 AND ForneceI=:P2 AND Apto=:P3');
      DModG.QrUpdPID1.SQL.Add('AND Vencimento=:P4 AND FatNum=:P5');
      DModG.QrUpdPID1.Params[00].AsInteger := Imp;
      DModG.QrUpdPID1.Params[01].AsInteger := QrBloqInadEmpresa.Value;
      DModG.QrUpdPID1.Params[02].AsInteger := QrBloqInadForneceI.Value;
      DModG.QrUpdPID1.Params[03].AsInteger := QrBloqInadApto.Value;
      DModG.QrUpdPID1.Params[04].AsDate    := QrBloqInadVencimento.Value;
      DModG.QrUpdPID1.Params[05].AsFloat   := QrBloqInadFatNum.Value;
      DModG.QrUpdPID1.ExecSQL;
      //
      ReopenBloqInad(QrBloqInadControle.Value);
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmInadimpBloq.DefineFrx(frx: TfrxReport);
begin
  frxDsBoletos.DataSet      := QrBoletos;
  frxDsBoletosIts.DataSet   := QrBoletosIts;
  frxDsInquilino.DataSet    := DmBloq.QrInquilino;
  DModG.frxDsDono.DataSet   := DModG.QrDono;
  DModG.frxDsMaster.DataSet := DModG.QrMaster;
  DmCond.frxDsCond.DataSet  := DmCond.QrCond;
  frxDsCNAB_Cfg_B.DataSet   := QrCNAB_Cfg_B;
  //
  MyObjects.frxDefineDataSets(frx, [
    frxDsBoletos,
    frxDsBoletosIts,
    frxDsInquilino,
    DModG.frxDsDono,
    DModG.frxDsMaster,
    DmCond.frxDsCond,
    frxDsCNAB_Cfg_B
    ]);
end;

procedure TFmInadimpBloq.BtRecalculaClick(Sender: TObject);
var
  NewVencto: TDateTime;
  TOTAL, PEND_VAL, TaxaJ, Juros, Multa, ValJuros, ValMulta, Dias: Double;
  Controle: Integer;
begin
  Screen.Cursor := crHourGlass;
  if QrBloqInad.State = dsBrowse then
    Controle := QrBloqInadControle.Value
  else
    Controle := 0;
  //    
  ReopenBloqInad(0);
  DBGBloq.Visible := True;
  //
  NewVencto  := Int(TPVencto.Date);
  Multa      := EdMulta.ValueVariant;
  Juros      := EdJuros.ValueVariant;
  while not QrBloqInad.Eof do
  begin
    Dias     := NewVencto - QrBloqInadVencimento.Value;
    TaxaJ    := MLAGeral.CalculaJuroSimples(Juros, Dias);
    ValMulta := Round(QrBloqInadCREDITO.Value * Multa) / 100;
    ValJuros := Round(QrBloqInadCREDITO.Value * TaxaJ) / 100;
    TOTAL    := QrBloqInadCREDITO.Value + ValMulta + ValJuros;
    PEND_VAL := TOTAL - QrBloqInadPago.Value;
    //
    {
    MUDADO EM 07/08/2013 porque bloquetos com Fatnum muito grandes da erro na SQL
      pois chaves double est� colocando ,000000 no final e com isso n�o atualiza
    }
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, FTabBloqInad, False, [
      'Juros', 'Multa', 'TOTAL', 'PEND_VAL', 'NewVencto'], [
      'Controle', 'FatNum'], [ValJuros, ValMulta, TOTAL, PEND_VAL, NewVencto],
      [ QrBloqInadControle.Value, QrBloqInadFatNum.Value], False);
    QrBloqInad.Next;
  end;
  ReopenBloqInad(Controle);
  BtImprime.Enabled := QrBloqInad.RecordCount > 0;
  //
  Screen.Cursor := crDefault;
end;

procedure TFmInadimpBloq.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmInadimpBloq.BtTodosClick(Sender: TObject);
begin
  AtualizaTodos(1);
end;

procedure TFmInadimpBloq.EdJurosChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmInadimpBloq.EdMultaChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmInadimpBloq.FechaPesquisa;
begin
  BtImprime.Enabled := False;
  QrBloqInad.Close;
  DBGBloq.Visible := False;
end;

procedure TFmInadimpBloq.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmInadimpBloq.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FLoteImp               := -1000000000;
  TPVencto.Date          := UMyMod.CalculaDataDeposito(Date + 1);
  QrBloqInad.Database    := DModG.MyPID_DB;
  QrBoletos.Database     := DModG.MyPID_DB;
  frxDsInquilino.DataSet := DmBloq.QrInquilino;
end;

procedure TFmInadimpBloq.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmInadimpBloq.frxComposite(PrevCod, Apto: Integer; Titulo: string;
  var ModelBloqAnt, ConfigAnt: Integer; ClearLastReport,
  Antigo: Boolean): Boolean;
var
  s: TMemoryStream;
  frxAtu: TfrxReport;
begin
  Result := False;
  //
  frxAtu := frxCondE2;
  try
    if frxAtu <> nil then
    begin
      //frxBloq.Clear;
      frxBloq.OnGetValue := frxAtu.OnGetValue;
      frxBloq.OnUserFunction := frxAtu.OnUserFunction;
      frxBloq.ReportOptions.Name := Titulo;
      //  mudado de frxBloq para frxAtu
      frxAtu.Variables['MeuLogoExiste'] := VAR_MEULOGOEXISTE;
      frxAtu.Variables['MeuLogoCaminho'] := QuotedStr(VAR_MEULOGOPATH);
      frxAtu.Variables['LogoNFExiste'] := VAR_LOGONFEXISTE;
      frxAtu.Variables['LogoNFCaminho'] := QuotedStr(VAR_LOGONFPATH);
      //
      frxAtu.Variables['LogoBancoExiste'] := True;//FileExists(QrCondBcoLogoPath.Value);
      frxAtu.Variables['LogoBancoPath'] := QuotedStr(DmCond.QrCondBcoLogoPath.Value);
      //
      frxAtu.Variables['LogoEmpresaExiste'] := True;//FileExists(QrCondCliLogoPath.Value);
      frxAtu.Variables['LogoEmpresaPath'] := QuotedStr(DmCond.QrCondCliLogoPath.Value);
      //  FIM mudado de frxBloq para frxAtu


      // agregar frx selecionado ao frxBloq (frx virtual que est� agrupando v�rios frx para imprimir tudo junto)
      s := TMemoryStream.Create;
      frxAtu.SaveToStream(s);
      s.Position := 0;
      frxBloq.LoadFromStream(s);
      //#$%
      frxBloq.PrepareReport(ClearLastReport);
      //
      Result := True;
    end else
      Geral.MensagemBox('ERRO. "frxCond?" n�o definido.',
      'ERRO', MB_OK+MB_ICONERROR);
  except
    raise;
  end;
end;

procedure TFmInadimpBloq.frxCondE2GetValue(const VarName: string;
  var Value: Variant);
var
  DVB, LocalEData, UH, NossoNumero_Rem: String;
  ModelBloq, Banco: Integer;
begin
  // In�cio da Ficha de compensa��o
  if AnsiCompareText(VarName, 'VARF_AGCodCed') = 0 then
    Value := QrCNAB_Cfg_B.FieldByName('AgContaCed').AsString
  else if AnsiCompareText(VarName, 'VARF_NossoNumero') = 0 then
  begin
    UBancos.GeraNossoNumero(QrCNAB_Cfg_B.FieldByName('ModalCobr').AsInteger,
      QrCNAB_Cfg_B.FieldByName('CedBanco').AsInteger, QrCNAB_Cfg_B.FieldByName('CedAgencia').AsInteger,
      QrCNAB_Cfg_B.FieldByName('CedPosto').AsInteger, QrBoletosBLOQUETO.Value,
      QrCNAB_Cfg_B.FieldByName('CedConta').AsString, QrCNAB_Cfg_B.FieldByName('CartNum').AsString,
      QrCNAB_Cfg_B.FieldByName('IDCobranca').AsString,
      Geral.SoNumero_TT(QrCNAB_Cfg_B.FieldByName('CodEmprBco').AsString),
      QrBoletosVencto.Value, QrCNAB_Cfg_B.FieldByName('TipoCobranca').AsInteger,
      QrCNAB_Cfg_B.FieldByName('EspecieDoc').AsString,
      QrCNAB_Cfg_B.FieldByName('CNAB').AsInteger,
      QrCNAB_Cfg_B.FieldByName('CtaCooper').AsString,
      QrCNAB_Cfg_B.FieldByName('LayoutRem').AsString,
      FNossoNumero,
      NossoNumero_Rem);
    Value := FNossoNumero;
  end else
   if AnsiCompareText(VarName, 'VARF_CODIGOBARRAS') = 0 then
  begin
    UBancos.GeraNossoNumero(QrCNAB_Cfg_B.FieldByName('ModalCobr').AsInteger,
      QrCNAB_Cfg_B.FieldByName('CedBanco').AsInteger, QrCNAB_Cfg_B.FieldByName('CedAgencia').AsInteger,
      QrCNAB_Cfg_B.FieldByName('CedPosto').AsInteger, QrBoletosBLOQUETO.Value,
      QrCNAB_Cfg_B.FieldByName('CedConta').AsString, QrCNAB_Cfg_B.FieldByName('CartNum').AsString,
      QrCNAB_Cfg_B.FieldByName('IDCobranca').AsString,
      Geral.SoNumero_TT(QrCNAB_Cfg_B.FieldByName('CodEmprBco').AsString),
      QrBoletosVencto.Value, QrCNAB_Cfg_B.FieldByName('TipoCobranca').AsInteger,
      QrCNAB_Cfg_B.FieldByName('EspecieDoc').AsString,
      QrCNAB_Cfg_B.FieldByName('CNAB').AsInteger,
      QrCNAB_Cfg_B.FieldByName('CtaCooper').AsString,
      QrCNAB_Cfg_B.FieldByName('LayoutRem').AsString,
      FNossoNumero,
      NossoNumero_Rem);
    //
    Value := UBancos.CodigoDeBarra_BoletoDeCobranca(
               QrCNAB_Cfg_B.FieldByName('CedBanco').AsInteger,
               QrCNAB_Cfg_B.FieldByName('CedAgencia').AsInteger,
               QrCNAB_Cfg_B.FieldByName('CorresBco').AsInteger,
               QrCNAB_Cfg_B.FieldByName('CorresAge').AsInteger,
               QrCNAB_Cfg_B.FieldByName('CedDAC_A').AsString,
               QrCNAB_Cfg_B.FieldByName('CedPosto').AsInteger,
               QrCNAB_Cfg_B.FieldByName('CedConta').AsString,
               QrCNAB_Cfg_B.FieldByName('CedDAC_C').AsString,
               QrCNAB_Cfg_B.FieldByName('CorresCto').AsString,
               9, 3, 1, FNossoNumero,
               QrCNAB_Cfg_B.FieldByName('CodEmprBco').AsString,
               QrCNAB_Cfg_B.FieldByName('CartNum').AsString,
               QrCNAB_Cfg_B.FieldByName('CART_IMP').AsString,
               QrCNAB_Cfg_B.FieldByName('IDCobranca').AsString,
               QrCNAB_Cfg_B.FieldByName('OperCodi').AsString,
               QrBoletosVencto.Value, QrBoletosSUB_TOT.Value,
               0, 0, not CkZerado.Checked,
               QrCNAB_Cfg_B.FieldByName('ModalCobr').AsInteger,
               QrCNAB_Cfg_B.FieldByName('LayoutRem').AsString)
  end else if AnsiCompareText(VarName, 'VAX') = 0 then
  begin
    if (QrCNAB_Cfg_B.FieldByName('LayoutRem').AsString = CO_756_CORRESPONDENTE_BRADESCO_2015)
      or (QrCNAB_Cfg_B.FieldByName('LayoutRem').AsString = CO_756_CORRESPONDENTE_BB_2015) then
    begin
      Banco := QrCNAB_Cfg_B.FieldByName('CorresBco').AsInteger;
      //
      DVB := UBancos.DigitoVerificadorCodigoBanco(Banco);
    end else
    begin
      Banco := QrCNAB_Cfg_B.FieldByName('CedBanco').AsInteger;
      //
      if QrCNAB_Cfg_B.FieldByName('DVB').AsString <> '?' then
        DVB :=  QrCNAB_Cfg_B.FieldByName('DVB').AsString
      else
        DVB := UBancos.DigitoVerificadorCodigoBanco(Banco);
    end;
    Value := FormatFloat('000', Banco) + '-' + DVB;
  end
  else if AnsiCompareText(VarName, 'VARF_LINHADIGITAVEL') = 0 then
  begin
    UBancos.GeraNossoNumero(
      QrCNAB_Cfg_B.FieldByName('ModalCobr').AsInteger,
      QrCNAB_Cfg_B.FieldByName('CedBanco').AsInteger,
      QrCNAB_Cfg_B.FieldByName('CedAgencia').AsInteger,
      QrCNAB_Cfg_B.FieldByName('CedPosto').AsInteger,
      QrBoletosBLOQUETO.Value,
      QrCNAB_Cfg_B.FieldByName('CedConta').AsString,
      QrCNAB_Cfg_B.FieldByName('CartNum').AsString,
      QrCNAB_Cfg_B.FieldByName('IDCobranca').AsString,
      Geral.SoNumero_TT(QrCNAB_Cfg_B.FieldByName('CodEmprBco').AsString),
      QrBoletosVencto.Value,
      QrCNAB_Cfg_B.FieldByName('TipoCobranca').AsInteger,
      QrCNAB_Cfg_B.FieldByName('EspecieDoc').AsString,
      QrCNAB_Cfg_B.FieldByName('CNAB').AsInteger,
      QrCNAB_Cfg_B.FieldByName('CtaCooper').AsString,
      QrCNAB_Cfg_B.FieldByName('LayoutRem').AsString,
      FNossoNumero, NossoNumero_Rem);
    //
    Value := '';
    Value := UBancos.LinhaDigitavel_BoletoDeCobranca(
               UBancos.CodigoDeBarra_BoletoDeCobranca(
                 QrCNAB_Cfg_B.FieldByName('CedBanco').AsInteger,
                 QrCNAB_Cfg_B.FieldByName('CedAgencia').AsInteger,
                 QrCNAB_Cfg_B.FieldByName('CorresBco').AsInteger,
                 QrCNAB_Cfg_B.FieldByName('CorresAge').AsInteger,
                 QrCNAB_Cfg_B.FieldByName('CedDAC_A').AsString,
                 QrCNAB_Cfg_B.FieldByName('CedPosto').AsInteger,
                 QrCNAB_Cfg_B.FieldByName('CedConta').AsString,
                 QrCNAB_Cfg_B.FieldByName('CedDAC_C').AsString,
                 QrCNAB_Cfg_B.FieldByName('CorresCto').AsString,
                 9, 3, 1, FNossoNumero,
                 QrCNAB_Cfg_B.FieldByName('CodEmprBco').AsString,
                 QrCNAB_Cfg_B.FieldByName('CartNum').AsString,
                 QrCNAB_Cfg_B.FieldByName('CART_IMP').AsString,
                 QrCNAB_Cfg_B.FieldByName('IDCobranca').AsString,
                 QrCNAB_Cfg_B.FieldByName('OperCodi').AsString,
                 QrBoletosVencto.Value, QrBoletosSUB_TOT.Value,
                 0, 0, not CkZerado.Checked,
                 QrCNAB_Cfg_B.FieldByName('ModalCobr').AsInteger,
                 QrCNAB_Cfg_B.FieldByName('LayoutRem').AsString))
  end else if AnsiCompareText(VarName, 'LogoBancoExiste') = 0 then
  begin
    if (QrCNAB_Cfg_B.FieldByName('LayoutRem').AsString = CO_756_CORRESPONDENTE_BRADESCO_2015)
      or (QrCNAB_Cfg_B.FieldByName('LayoutRem').AsString = CO_756_CORRESPONDENTE_BB_2015)
    then
      Value := FmMyGlyfs.LogoBancoExiste(QrCNAB_Cfg_B.FieldByName('CorresBco').AsInteger)
    else
      Value := FmMyGlyfs.LogoBancoExiste(QrCNAB_Cfg_B.FieldByName('CedBanco').AsInteger);
  end
  else if AnsiCompareText(VarName, 'LogoBancoPath') = 0 then
  begin
    if (QrCNAB_Cfg_B.FieldByName('LayoutRem').AsString = CO_756_CORRESPONDENTE_BRADESCO_2015)
      or (QrCNAB_Cfg_B.FieldByName('LayoutRem').AsString = CO_756_CORRESPONDENTE_BB_2015)
    then
      Value := FmMyGlyfs.CaminhoLogoBanco(QrCNAB_Cfg_B.FieldByName('CorresBco').AsInteger)
    else
      Value := FmMyGlyfs.CaminhoLogoBanco(QrCNAB_Cfg_B.FieldByName('CedBanco').AsInteger);

    Value := FmMyGlyfs.CaminhoLogoBanco(QrCNAB_Cfg_B.FieldByName('CedBanco').AsInteger)
  end
  else if AnsiCompareText(VarName, 'VAR_INSTRUCAO1') = 0 then
    Value := FmCondGerImpGer.TraduzInstrucao(QrCNAB_Cfg_B.FieldByName('Texto01').AsString,
    QrCNAB_Cfg_B.FieldByName('MultaPerc').AsFloat, QrCNAB_Cfg_B.FieldByName('JurosPerc').AsFloat, QrBoletosSUB_TOT.Value)
  else if AnsiCompareText(VarName, 'VAR_INSTRUCAO2') = 0 then
    Value := FmCondGerImpGer.TraduzInstrucao(QrCNAB_Cfg_B.FieldByName('Texto02').AsString,
    QrCNAB_Cfg_B.FieldByName('MultaPerc').AsFloat, QrCNAB_Cfg_B.FieldByName('JurosPerc').AsFloat, QrBoletosSUB_TOT.Value)
  else if AnsiCompareText(VarName, 'VAR_INSTRUCAO3') = 0 then
    Value := FmCondGerImpGer.TraduzInstrucao(QrCNAB_Cfg_B.FieldByName('Texto03').AsString,
    QrCNAB_Cfg_B.FieldByName('MultaPerc').AsFloat, QrCNAB_Cfg_B.FieldByName('JurosPerc').AsFloat, QrBoletosSUB_TOT.Value)
  else if AnsiCompareText(VarName, 'VAR_INSTRUCAO4') = 0 then
    Value := FmCondGerImpGer.TraduzInstrucao(QrCNAB_Cfg_B.FieldByName('Texto04').AsString,
    QrCNAB_Cfg_B.FieldByName('MultaPerc').AsFloat, QrCNAB_Cfg_B.FieldByName('JurosPerc').AsFloat, QrBoletosSUB_TOT.Value)
  else if AnsiCompareText(VarName, 'VAR_INSTRUCAO5') = 0 then
    Value := FmCondGerImpGer.TraduzInstrucao(QrCNAB_Cfg_B.FieldByName('Texto05').AsString,
    QrCNAB_Cfg_B.FieldByName('MultaPerc').AsFloat, QrCNAB_Cfg_B.FieldByName('JurosPerc').AsFloat, QrBoletosSUB_TOT.Value)
  else if AnsiCompareText(VarName, 'VAR_INSTRUCAO6') = 0 then
    Value := FmCondGerImpGer.TraduzInstrucao(QrCNAB_Cfg_B.FieldByName('Texto06').AsString,
    QrCNAB_Cfg_B.FieldByName('MultaPerc').AsFloat, QrCNAB_Cfg_B.FieldByName('JurosPerc').AsFloat, QrBoletosSUB_TOT.Value)
  else if AnsiCompareText(VarName, 'VAR_INSTRUCAO7') = 0 then
    Value := FmCondGerImpGer.TraduzInstrucao(QrCNAB_Cfg_B.FieldByName('Texto07').AsString,
    QrCNAB_Cfg_B.FieldByName('MultaPerc').AsFloat, QrCNAB_Cfg_B.FieldByName('JurosPerc').AsFloat, QrBoletosSUB_TOT.Value)
  else if AnsiCompareText(VarName, 'VAR_INSTRUCAO8') = 0 then
    Value := FmCondGerImpGer.TraduzInstrucao(QrCNAB_Cfg_B.FieldByName('Texto08').AsString,
    QrCNAB_Cfg_B.FieldByName('MultaPerc').AsFloat, QrCNAB_Cfg_B.FieldByName('JurosPerc').AsFloat, QrBoletosSUB_TOT.Value)
  // Fim Ficha de compensa��o
end;

function TFmInadimpBloq.frxMostra(PrevCod: Integer; Titulo: string;
  Tipo: TselType; Antigo: Boolean): Boolean;
var
  ModelBloqAnt, ConfigAnt: Integer;
begin
  Result := True;
  ModelBloqAnt := -1000;
  ConfigAnt := -1000;
  case Tipo of
    istTodos:
    begin
      QrBoletos.First;
      while not QrBoletos.Eof do
      begin
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando bloqueto ' + IntToStr(
          QrBoletos.RecNo) + ' de ' + IntToStr(QrBoletos.RecordCount));
        if Result then
        begin
          if QrBoletos.RecNo = 1 then
            Result := frxComposite(PrevCod, QrBoletosApto.Value, Titulo, ModelBloqAnt, ConfigAnt, True, Antigo)
          else
            Result := frxComposite(PrevCod, QrBoletosApto.Value, Titulo, ModelBloqAnt, ConfigAnt, False, Antigo);
        end;
        //#$%
        QrBoletos.Next;
      end;
    end;
    istMarcados:
    begin
    end;
    istSelecionados:
    begin
    end;
    istAtual:
    begin
      Result := frxComposite(PrevCod, QrBoletosApto.Value, Titulo, ModelBloqAnt, ConfigAnt, True, Antigo);
    end;
  end;
  if (Tipo in ([istAtual, istTodos, istMarcados, istSelecionados])) and Result then
  begin
    Application.CreateForm(TFmMeufrx, FmMeufrx);
    if CkDesign.Checked then
    begin
      frxBloq.DesignReport;
    end else
    begin
      frxBloq.Preview := FmMeufrx.PvVer;
      //
      FmMeufrx.PvVer.OutlineWidth := frxBloq.PreviewOptions.OutlineWidth;
      FmMeufrx.PvVer.Zoom := frxBloq.PreviewOptions.Zoom;
      FmMeufrx.PvVer.ZoomMode := frxBloq.PreviewOptions.ZoomMode;
      FmMeufrx.UpdateZoom;
      //
      FmMeufrx.ShowModal;
      FmMeufrx.Destroy;
    end;
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

function TFmInadimpBloq.ImprimeBoletos(Quais: TselType; Como: TfrxImpComo;
  Arquivo: String; Filtro: TfrxCustomExportFilter; PrevCod: Integer;
  Antigo: Boolean; var SdoJaAtz: Boolean): TfrxReport;
const
  MaxHear = 4;
var
  Definido: Boolean;
begin
  Result     := nil;
  //
  DefineFrx(frxCondE2);
  //
  Screen.Cursor := crHourGlass;
  try
    Definido := False;
    if UBancos.DigitoVerificadorCodigoBanco(QrCNAB_Cfg_B.FieldByName('CedBanco').AsInteger) = '?' then
    begin
      if QrCNAB_Cfg_B.FieldByName('DVB').AsString = '?' then
      begin
        Geral.MensagemBox('� necess�rio definir o d�gito ' +
        'verificador (DVB) do banco selecionado em seu cadastro!', 'Aviso',
        MB_OK+MB_ICONWARNING);
        Exit;
      end;
    end;
    //
    if (QrCNAB_Cfg_B.FieldByName('LayoutRem').AsString = CO_756_CORRESPONDENTE_BRADESCO_2015)
      or (QrCNAB_Cfg_B.FieldByName('LayoutRem').AsString = CO_756_CORRESPONDENTE_BB_2015)
    then
      FmMyGlyfs.PreparaLogoBanco(QrCNAB_Cfg_B.FieldByName('CorresBco').AsInteger)
    else
      FmMyGlyfs.PreparaLogoBanco(QrCNAB_Cfg_B.FieldByName('CedBanco').AsInteger);
    //
    Application.ProcessMessages;
    //
    case Como of
      ficMostra: Definido := frxMostra(PrevCod, 'Bloquetos condom�nio', Quais, Antigo);
      else Geral.MensagemBox(
        'Tipo de sa�da para impress�o de bloqueto n�o definida.' +
        sLineBreak + 'Informe a DERMATEK!',
        'ERRO', MB_OK+MB_ICONINFORMATION);
    end;
    if Definido then Result := frxBloq;
    Application.ProcessMessages;
  finally
    frxBloq.Clear;
    Screen.Cursor := crDefault;
  end;
end;

function TFmInadimpBloq.ObtemCNAB_Cfg(FatNum: Double; FatID, Lancto: Integer;
  var CNAB_Cfg: Integer): Boolean;
begin
  if FatID = 601 then //Consumo
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
      'SELECT CNAB_Cfg ',
      'FROM ' + FTabCnsA,
      'WHERE Lancto=' + Geral.FF0(Lancto),
      'AND Controle=' + FloatToStr(FatNum),
      '']);
    if QrLoc.RecordCount > 0 then
    begin
      CNAB_Cfg := QrLoc.FieldByName('CNAB_Cfg').AsInteger;
      Result   := True;
    end else
    begin
      CNAB_Cfg := 0;
      Result   := True;
    end;
  end else
  if FatID = 600 then //Arrecada��o
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
      'SELECT CNAB_Cfg ',
      'FROM ' + FTabAriA,
      'WHERE Lancto=' + Geral.FF0(Lancto),
      'AND Controle=' + FloatToStr(FatNum),
      '']);
    if QrLoc.RecordCount > 0 then
    begin
      CNAB_Cfg := QrLoc.FieldByName('CNAB_Cfg').AsInteger;
      Result   := True;
    end else
    begin
      CNAB_Cfg := 0;
      Result   := True;
    end;
  end else
  if FatID = 610 then //Parcelamento
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
      'SELECT par.CNAB_Cfg ',
      'FROM lct0003a lct ',
      'LEFT JOIN bloqparcpar bpp ON bpp.Controle = lct.Atrelado',
      'LEFT JOIN bloqparc par ON par.Codigo = bpp.Codigo ',
      'WHERE lct.Controle=' + Geral.FF0(Lancto),
      'AND lct.FatNum=' + FloatToStr(FatNum),
      '']);
    if QrLoc.RecordCount > 0 then
    begin
      CNAB_Cfg := QrLoc.FieldByName('CNAB_Cfg').AsInteger;
      Result   := True;
    end else
    begin
      CNAB_Cfg := 0;
      Result   := True;
    end;
  end else
  begin
    Geral.MB_Aviso('Falha ao obter configura��o do boleto! FatID n�o implementado!');
    CNAB_Cfg := 0;
    Result   := False;
  end;
end;

procedure TFmInadimpBloq.QrBloqInadAfterScroll(DataSet: TDataSet);
var
  Depto, FatNum, Mez, Vencto: String;
begin
  Depto  := FormatFloat('0', QrBloqInadApto.Value);
  FatNum := FormatFloat('0', QrBloqInadFatNum.Value);
  Mez    := FormatFloat('0', QrBloqInadMez.Value);
  Vencto := Geral.FDT(QrBloqInadVencimento.Value, 1);
  //
  QrBloqIts.Close;
  QrBloqIts.SQL.Clear;
  QrBloqIts.SQL.Add('SELECT lct.Genero, lct.Descricao, lct.Credito');
  QrBloqIts.SQL.Add('FROM ' + FTabLctA + ' lct');
  QrBloqIts.SQL.Add('WHERE lct.Depto=' + Depto);
  QrBloqIts.SQL.Add('AND lct.FatNum=' + FatNum);
  QrBloqIts.SQL.Add('AND lct.Mez=' + Mez);
  QrBloqIts.SQL.Add('AND lct.Vencimento="' + Vencto + '"');
  QrBloqIts.Open;
end;

procedure TFmInadimpBloq.QrBloqInadBeforeClose(DataSet: TDataSet);
begin
  QrBloqIts.Close;
end;

procedure TFmInadimpBloq.QrBoletosAfterScroll(DataSet: TDataSet);
var
  CNAB_Cfg: Integer;
  Depto, FatNum, Mez, Vencto: String;
begin
  Depto  := FormatFloat('0', QrBoletosApto.Value);
  FatNum := FormatFloat('0', QrBoletosFatNum.Value);
  Mez    := FormatFloat('0', QrBoletosMez.Value);
  Vencto := Geral.FDT(QrBoletosVencimento.Value, 1);
  //
  QrBoletosIts.Close;
  QrBoletosIts.SQL.Clear;
  QrBoletosIts.SQL.Add('SELECT lct.Genero, lct.Descricao, lct.Credito');
  QrBoletosIts.SQL.Add('FROM ' + FTabLctA + ' lct');
  QrBoletosIts.SQL.Add('WHERE lct.Depto=' + Depto);
  QrBoletosIts.SQL.Add('AND lct.FatNum=' + FatNum);
  QrBoletosIts.SQL.Add('AND lct.Mez=' + Mez);
  QrBoletosIts.SQL.Add('AND lct.Vencimento="' + Vencto + '"');
  QrBoletosIts.Open;
  //
  DmBloq.ReopenEnderecoInquilino(QrBoletosApto.Value);
  //
  if not ObtemCNAB_Cfg(QrBoletosFatNum.Value, QrBoletosFatID.Value,
    QrBoletosControle.Value, CNAB_Cfg) then
  begin
    Geral.MB_Aviso('Falha ao obter dados para gera��o do boleto!');
    Exit;
  end;
  //
  DmBloq.ReopenCNAB_Cfg_Cond(QrCNAB_Cfg_B, Dmod.MyDB, QrBoletosEmpresa.Value,
    CNAB_Cfg);
end;

procedure TFmInadimpBloq.QrBoletosBeforeClose(DataSet: TDataSet);
begin
  QrBoletosIts.Close;
end;

procedure TFmInadimpBloq.QrBoletosCalcFields(DataSet: TDataSet);
begin
  if QrBoletosBoleto.Value = 0 then
    QrBoletosBLOQUETO.Value := -1
  else
    QrBoletosBLOQUETO.Value := QrBoletosBoleto.Value;
end;

procedure TFmInadimpBloq.ReopenBloqInad(Controle: Integer);
var
  Quando: String;
begin
{   N�VEL JUR�DICO
0 - Sem restri��o',
1 - Sem restri��o ou n�o vencidos (precisa de SENHA)',
2 - TODOS (precisa de SENHA)']
}
  case FNivelJuridico of
    0: Quando := 'WHERE Juridico=0';
    1: Quando := 'WHERE Juridico=0 OR Vencimento >= SYSDATE()';
    else Quando := '';
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrBloqInad, DModG.MyPID_DB, [
  'SELECT * ',
  'FROM ' + FTabBloqInad,
  Quando,
  '']);
  //
  QrBloqInad.Open;
  //
  if Controle <> 0 then
    QrBloqInad.Locate('Controle', Controle, []);
end;

procedure TFmInadimpBloq.TPVenctoChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmInadimpBloq.TPVenctoClick(Sender: TObject);
begin
  FechaPesquisa();
end;

end.
