object FmCondFichaCad: TFmCondFichaCad
  Left = 339
  Top = 185
  Caption = 'CND-CADAS-002 :: Ficha Cadastral de Moradores'
  ClientHeight = 500
  ClientWidth = 792
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 338
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 792
      Height = 48
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 12
        Top = 4
        Width = 60
        Height = 13
        Caption = 'Condom'#237'nio:'
      end
      object EdEmpresa: TdmkEditCB
        Left = 12
        Top = 20
        Width = 65
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 80
        Top = 20
        Width = 697
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 1
        dmkEditCB = EdEmpresa
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 48
      Width = 792
      Height = 56
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object RGFormato: TRadioGroup
        Left = 0
        Top = 0
        Width = 792
        Height = 56
        Align = alClient
        Caption = ' Formato de impress'#227'o: '
        ItemIndex = 0
        Items.Strings = (
          'Modelo 1')
        TabOrder = 0
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 744
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 696
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 368
        Height = 32
        Caption = 'Ficha Cadastral de Moradores'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 368
        Height = 32
        Caption = 'Ficha Cadastral de Moradores'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 368
        Height = 32
        Caption = 'Ficha Cadastral de Moradores'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 386
    Width = 792
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 788
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 430
    Width = 792
    Height = 70
    Align = alBottom
    TabOrder = 3
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 788
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 644
        Top = 0
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtImprime: TBitBtn
        Tag = 5
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Imprime'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtImprimeClick
      end
    end
  end
  object QrCondImov: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrCondImovAfterScroll
    OnCalcFields = QrCondImovCalcFields
    SQL.Strings = (
      'SELECT blc.Descri, blc.SomaFracao, blc.PrefixoUH, imc.Conta,'
      
        'IF(imc.Conjuge=0, "", con.Nome) NOMECONJUGE, sta.Descri STATUS1,' +
        ' '
      
        'IF(imc.Propriet=0, "", IF (pro.Tipo=0, pro.RazaoSocial, pro.Nome' +
        ')) NOMEPROP,'
      
        'IF(imc.Usuario=0, "", IF (mor.Tipo=0, mor.RazaoSocial, mor.Nome)' +
        ') NOMEUSUARIO,'
      
        'IF(imc.Procurador=0, "", IF (pcu.Tipo=0, pcu.RazaoSocial, pcu.No' +
        'me)) NOMEPROCURADOR,'
      
        'IF(imc.ENome1=0, "", IF (ena.Tipo=0, ena.RazaoSocial, ena.Nome))' +
        ' NOMEEMP1,'
      
        'IF(imc.ENome2=0, "", IF (enb.Tipo=0, enb.RazaoSocial, enb.Nome))' +
        ' NOMEEMP2,'
      
        'IF(imc.ENome3=0, "", IF (enc.Tipo=0, enc.RazaoSocial, enc.Nome))' +
        ' NOMEEMP3,'
      
        'IF(imc.Imobiliaria=0, "", end.RazaoSocial) IMOB, ptc.Nome NOMEPR' +
        'OTOCOLO, imc.*,'
      'ELT(imc.SitImv, '#39'Sim'#39', '#39'N'#227'o'#39') NOMESITIMOV'
      'FROM condimov imc'
      'LEFT JOIN condbloco blc ON blc.Controle=imc.Controle'
      'LEFT JOIN entidades pro ON pro.Codigo=imc.Propriet'
      'LEFT JOIN entidades mor ON mor.Codigo=imc.Usuario'
      'LEFT JOIN entidades pcu ON pcu.Codigo=imc.Procurador'
      'LEFT JOIN entidades con ON con.Codigo=imc.Conjuge'
      'LEFT JOIN entidades ena ON ena.Codigo=imc.ENome1'
      'LEFT JOIN entidades enb ON enb.Codigo=imc.ENome2'
      'LEFT JOIN entidades enc ON enc.Codigo=imc.ENome3'
      'LEFT JOIN status sta ON sta.Codigo=imc.Status'
      'LEFT JOIN entidades end ON end.Codigo=imc.Imobiliaria'
      'LEFT JOIN protocolos ptc ON ptc.Codigo=imc.Protocolo'
      'WHERE imc.Codigo=:P0'
      'ORDER BY  blc.Ordem, blc.Descri, imc.Andar, imc.Unidade'
      ''
      '')
    Left = 88
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCondImovSSINI1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SSINI1_TXT'
      Size = 8
      Calculated = True
    end
    object QrCondImovSSSAI1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SSSAI1_TXT'
      Size = 8
      Calculated = True
    end
    object QrCondImovSDINI1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SDINI1_TXT'
      Size = 8
      Calculated = True
    end
    object QrCondImovSDSAI1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SDSAI1_TXT'
      Size = 8
      Calculated = True
    end
    object QrCondImovSSINI2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SSINI2_TXT'
      Size = 8
      Calculated = True
    end
    object QrCondImovSSSAI2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SSSAI2_TXT'
      Size = 8
      Calculated = True
    end
    object QrCondImovSDINI2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SDINI2_TXT'
      Size = 8
      Calculated = True
    end
    object QrCondImovSDSAI2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SDSAI2_TXT'
      Size = 8
      Calculated = True
    end
    object QrCondImovSSINI3_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SSINI3_TXT'
      Size = 8
      Calculated = True
    end
    object QrCondImovSSSAI3_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SSSAI3_TXT'
      Size = 8
      Calculated = True
    end
    object QrCondImovSDINI3_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SDINI3_TXT'
      Size = 8
      Calculated = True
    end
    object QrCondImovSDSAI3_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SDSAI3_TXT'
      Size = 8
      Calculated = True
    end
    object QrCondImovDescri: TWideStringField
      FieldName = 'Descri'
      Size = 100
    end
    object QrCondImovSomaFracao: TFloatField
      FieldName = 'SomaFracao'
    end
    object QrCondImovPrefixoUH: TWideStringField
      FieldName = 'PrefixoUH'
    end
    object QrCondImovNOMECONJUGE: TWideStringField
      FieldName = 'NOMECONJUGE'
      Size = 100
    end
    object QrCondImovSTATUS1: TWideStringField
      FieldName = 'STATUS1'
      Size = 100
    end
    object QrCondImovNOMEPROP: TWideStringField
      FieldName = 'NOMEPROP'
      Size = 100
    end
    object QrCondImovNOMEUSUARIO: TWideStringField
      FieldName = 'NOMEUSUARIO'
      Size = 100
    end
    object QrCondImovNOMEPROCURADOR: TWideStringField
      FieldName = 'NOMEPROCURADOR'
      Size = 100
    end
    object QrCondImovNOMEEMP1: TWideStringField
      FieldName = 'NOMEEMP1'
      Size = 100
    end
    object QrCondImovNOMEEMP2: TWideStringField
      FieldName = 'NOMEEMP2'
      Size = 100
    end
    object QrCondImovNOMEEMP3: TWideStringField
      FieldName = 'NOMEEMP3'
      Size = 100
    end
    object QrCondImovIMOB: TWideStringField
      FieldName = 'IMOB'
      Size = 100
    end
    object QrCondImovPropriet: TIntegerField
      FieldName = 'Propriet'
    end
    object QrCondImovConjuge: TIntegerField
      FieldName = 'Conjuge'
    end
    object QrCondImovProcurador: TIntegerField
      FieldName = 'Procurador'
    end
    object QrCondImovUsuario: TIntegerField
      FieldName = 'Usuario'
    end
    object QrCondImovAndar: TIntegerField
      FieldName = 'Andar'
    end
    object QrCondImovUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrCondImovConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrCondImovESegunda1: TSmallintField
      FieldName = 'ESegunda1'
    end
    object QrCondImovETerca1: TSmallintField
      FieldName = 'ETerca1'
    end
    object QrCondImovEQuarta1: TSmallintField
      FieldName = 'EQuarta1'
    end
    object QrCondImovEQuinta1: TSmallintField
      FieldName = 'EQuinta1'
    end
    object QrCondImovESexta1: TSmallintField
      FieldName = 'ESexta1'
    end
    object QrCondImovESabado1: TSmallintField
      FieldName = 'ESabado1'
    end
    object QrCondImovEDomingo1: TSmallintField
      FieldName = 'EDomingo1'
    end
    object QrCondImovESegunda2: TSmallintField
      FieldName = 'ESegunda2'
    end
    object QrCondImovETerca2: TSmallintField
      FieldName = 'ETerca2'
    end
    object QrCondImovEQuarta2: TSmallintField
      FieldName = 'EQuarta2'
    end
    object QrCondImovEQuinta2: TSmallintField
      FieldName = 'EQuinta2'
    end
    object QrCondImovESexta2: TSmallintField
      FieldName = 'ESexta2'
    end
    object QrCondImovESabado2: TSmallintField
      FieldName = 'ESabado2'
    end
    object QrCondImovEDomingo2: TSmallintField
      FieldName = 'EDomingo2'
    end
    object QrCondImovESegunda3: TSmallintField
      FieldName = 'ESegunda3'
    end
    object QrCondImovETerca3: TSmallintField
      FieldName = 'ETerca3'
    end
    object QrCondImovEQuarta3: TSmallintField
      FieldName = 'EQuarta3'
    end
    object QrCondImovEQuinta3: TSmallintField
      FieldName = 'EQuinta3'
    end
    object QrCondImovESexta3: TSmallintField
      FieldName = 'ESexta3'
    end
    object QrCondImovESabado3: TSmallintField
      FieldName = 'ESabado3'
    end
    object QrCondImovEDomingo3: TSmallintField
      FieldName = 'EDomingo3'
    end
    object QrCondImovEmNome1: TWideStringField
      FieldName = 'EmNome1'
      Size = 100
    end
    object QrCondImovEmTel1: TWideStringField
      FieldName = 'EmTel1'
    end
    object QrCondImovEmNome2: TWideStringField
      FieldName = 'EmNome2'
      Size = 100
    end
    object QrCondImovEmTel2: TWideStringField
      FieldName = 'EmTel2'
    end
  end
  object frxModelo1: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40580.791108773090000000
    ReportOptions.LastChange = 40581.033642303200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxModelo1GetValue
    Left = 76
    Top = 116
    Datasets = <
      item
        DataSet = frxDsCondImov
        DataSetName = 'frxDsCondImov'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsOutMorad
        DataSetName = 'frxDsOutMorad'
      end
      item
        DataSet = frxDsPropriet
        DataSetName = 'frxDsPropriet'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        Height = 64.252010000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object frxShapeView1: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'CADASTRO DE MORADORES')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 529.134200000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_NOME_EMP]')
          ParentFont = False
        end
      end
      object PageFooter3: TfrxPageFooter
        Height = 26.456678270000000000
        Top = 222.992270000000000000
        Width = 680.315400000000000000
        object Memo120: TfrxMemoView
          Width = 582.047620000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Left = 582.047620000000000000
          Width = 98.267755590000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_MODELO]')
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        Height = 18.897650000000000000
        Top = 143.622140000000000000
        Width = 680.315400000000000000
        DataSet = frxDsCondImov
        DataSetName = 'frxDsCondImov'
        RowCount = 0
        StartNewPage = True
        object Subreport1: TfrxSubreport
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Page = frxModelo1.Page2
        end
      end
    end
    object Page2: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object MasterData1: TfrxMasterData
        Height = 668.976810000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        RowCount = 1
        object Memo4: TfrxMemoView
          Width = 340.157700000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'Descri'
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCondImov."Descri"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          Left = 340.157700000000000000
          Width = 340.157700000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'Unidade'
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCondImov."Unidade"]')
          ParentFont = False
          WordWrap = False
        end
        object frxDsCondImovNOMEPROP: TfrxMemoView
          Left = 75.590600000000000000
          Top = 18.897650000000000000
          Width = 604.724751180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'NOMEPROP'
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCondImov."NOMEPROP"]')
          ParentFont = False
          WordWrap = False
        end
        object frxDsCondImovNOMEUSUARIO: TfrxMemoView
          Left = 75.590600000000000000
          Top = 37.795300000000000000
          Width = 604.724409450000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'NOMEUSUARIO'
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCondImov."NOMEUSUARIO"]')
          ParentFont = False
          WordWrap = False
        end
        object frxDsCondImovNOMECONJUGE: TfrxMemoView
          Left = 75.590600000000000000
          Top = 56.692949999999990000
          Width = 604.724409450000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'NOMECONJUGE'
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCondImov."NOMECONJUGE"]')
          ParentFont = False
          WordWrap = False
        end
        object frxDsCondImovNOMEPROCURADOR: TfrxMemoView
          Left = 75.590600000000000000
          Top = 75.590600000000000000
          Width = 604.724409450000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'NOMEPROCURADOR'
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCondImov."NOMEPROCURADOR"]')
          ParentFont = False
          WordWrap = False
        end
        object frxDsCondImovIMOB: TfrxMemoView
          Left = 75.590600000000000000
          Top = 94.488250000000000000
          Width = 604.724409450000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'IMOB'
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCondImov."IMOB"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo1: TfrxMemoView
          Top = 18.897650000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Propriet'#225'rio:')
          ParentFont = False
          WordWrap = False
        end
        object Memo2: TfrxMemoView
          Top = 37.795300000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Locat'#225'rio:')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          Top = 56.692949999999990000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Conjuge:')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          Top = 75.590600000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Procurador:')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          Top = 94.488250000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Imobili'#225'ria:')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          Top = 113.385900000000000000
          Width = 18.897650000000000000
          Height = 94.488250000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Propriet'#225'rio')
          ParentFont = False
          Rotation = 90
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 18.897650000000000000
          Top = 113.385900000000000000
          Width = 120.944960000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nascimento:')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          Left = 230.551330000000000000
          Top = 113.385900000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'RG/IE:')
          ParentFont = False
          WordWrap = False
        end
        object Memo11: TfrxMemoView
          Left = 449.764070000000000000
          Top = 113.385900000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CPF/CNPJ:')
          ParentFont = False
          WordWrap = False
        end
        object frxDsProprietCNPJ_CPF: TfrxMemoView
          Left = 525.354670000000000000
          Top = 113.385900000000000000
          Width = 154.960730000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsPropriet
          DataSetName = 'frxDsPropriet'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_PROPRIET_CNPJ_CPF]')
          ParentFont = False
          WordWrap = False
        end
        object frxDsProprietIE_RG: TfrxMemoView
          Left = 275.905690000000000000
          Top = 113.385900000000000000
          Width = 173.858380000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsPropriet
          DataSetName = 'frxDsPropriet'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_PROPRIET_IERG]')
          ParentFont = False
          WordWrap = False
        end
        object frxDsProprietNATAL: TfrxMemoView
          Left = 98.267780000000000000
          Top = 113.385900000000000000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsPropriet
          DataSetName = 'frxDsPropriet'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_PROPRIET_NATAL]')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          Left = 18.897650000000000000
          Top = 132.283550000000000000
          Width = 64.252010000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Profiss'#227'o:')
          ParentFont = False
          WordWrap = False
        end
        object Memo18: TfrxMemoView
          Left = 325.039580000000000000
          Top = 132.283550000000000000
          Width = 64.252010000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Empresa:')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          Left = 83.149660000000000000
          Top = 132.283550000000000000
          Width = 241.889920000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsPropriet
          DataSetName = 'frxDsPropriet'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_PROPRIET_PROFISSAO]')
          ParentFont = False
          WordWrap = False
        end
        object Memo20: TfrxMemoView
          Left = 389.291590000000000000
          Top = 132.283550000000000000
          Width = 291.023810000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsPropriet
          DataSetName = 'frxDsPropriet'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_PROPRIET_EMPRESA]')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          Left = 18.897650000000000000
          Top = 151.181200000000000000
          Width = 64.252010000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Tel. res.:')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          Left = 83.149660000000000000
          Top = 151.181200000000000000
          Width = 154.960730000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsPropriet
          DataSetName = 'frxDsPropriet'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_PROPRIET_TE1]')
          ParentFont = False
          WordWrap = False
        end
        object Memo23: TfrxMemoView
          Left = 238.110390000000000000
          Top = 151.181200000000000000
          Width = 64.252010000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Tel. com.:')
          ParentFont = False
          WordWrap = False
        end
        object Memo24: TfrxMemoView
          Left = 302.362400000000000000
          Top = 151.181200000000000000
          Width = 154.960730000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsPropriet
          DataSetName = 'frxDsPropriet'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_PROPRIET_TE2]')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          Left = 457.323130000000000000
          Top = 151.181200000000000000
          Width = 64.252010000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Celular:')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          Left = 521.575140000000000000
          Top = 151.181200000000000000
          Width = 158.740260000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsPropriet
          DataSetName = 'frxDsPropriet'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_PROPRIET_CEL]')
          ParentFont = False
          WordWrap = False
        end
        object Memo27: TfrxMemoView
          Left = 18.897650000000000000
          Top = 170.078850000000000000
          Width = 64.252010000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'E-mail:')
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          Left = 83.149660000000000000
          Top = 170.078850000000000000
          Width = 597.165740000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsPropriet
          DataSetName = 'frxDsPropriet'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_PROPRIET_EMAIL]')
          ParentFont = False
          WordWrap = False
        end
        object Memo29: TfrxMemoView
          Left = 18.897650000000000000
          Top = 188.976500000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Estado civil:')
          ParentFont = False
          WordWrap = False
        end
        object Memo30: TfrxMemoView
          Left = 94.488250000000000000
          Top = 188.976500000000000000
          Width = 170.078850000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsPropriet
          DataSetName = 'frxDsPropriet'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_PROPRIET_ECIVIL]')
          ParentFont = False
          WordWrap = False
        end
        object Memo31: TfrxMemoView
          Left = 264.567100000000000000
          Top = 188.976500000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Conjuge:')
          ParentFont = False
          WordWrap = False
        end
        object Memo32: TfrxMemoView
          Left = 325.039580000000000000
          Top = 188.976500000000000000
          Width = 355.275600310000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsPropriet
          DataSetName = 'frxDsPropriet'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_PROPRIET_CONJUGE]')
          ParentFont = False
          WordWrap = False
        end
        object Memo33: TfrxMemoView
          Top = 207.874150000000000000
          Width = 18.897650000000000000
          Height = 94.488250000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Locat'#225'rio')
          ParentFont = False
          Rotation = 90
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          Left = 18.897650000000000000
          Top = 207.874150000000000000
          Width = 120.944960000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nascimento:')
          ParentFont = False
          WordWrap = False
        end
        object Memo35: TfrxMemoView
          Left = 230.551330000000000000
          Top = 207.874150000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'RG/IE:')
          ParentFont = False
          WordWrap = False
        end
        object Memo36: TfrxMemoView
          Left = 449.764070000000000000
          Top = 207.874150000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CPF/CNPJ:')
          ParentFont = False
          WordWrap = False
        end
        object Memo37: TfrxMemoView
          Left = 525.354670000000000000
          Top = 207.874150000000000000
          Width = 154.960730000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsPropriet
          DataSetName = 'frxDsPropriet'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_USUARIO_CNPJ_CPF]')
          ParentFont = False
          WordWrap = False
        end
        object Memo38: TfrxMemoView
          Left = 275.905690000000000000
          Top = 207.874150000000000000
          Width = 173.858380000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsPropriet
          DataSetName = 'frxDsPropriet'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_USUARIO_IERG]')
          ParentFont = False
          WordWrap = False
        end
        object Memo39: TfrxMemoView
          Left = 98.267780000000000000
          Top = 207.874150000000000000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsPropriet
          DataSetName = 'frxDsPropriet'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_USUARIO_NATAL]')
          ParentFont = False
          WordWrap = False
        end
        object Memo40: TfrxMemoView
          Left = 18.897650000000000000
          Top = 226.771800000000000000
          Width = 64.252010000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Profiss'#227'o:')
          ParentFont = False
          WordWrap = False
        end
        object Memo41: TfrxMemoView
          Left = 325.039580000000000000
          Top = 226.771800000000000000
          Width = 64.252010000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Empresa:')
          ParentFont = False
          WordWrap = False
        end
        object Memo42: TfrxMemoView
          Left = 83.149660000000000000
          Top = 226.771800000000000000
          Width = 241.889920000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsPropriet
          DataSetName = 'frxDsPropriet'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_USUARIO_PROFISSAO]')
          ParentFont = False
          WordWrap = False
        end
        object Memo43: TfrxMemoView
          Left = 389.291590000000000000
          Top = 226.771800000000000000
          Width = 291.023810000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsPropriet
          DataSetName = 'frxDsPropriet'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_USUARIO_EMPRESA]')
          ParentFont = False
          WordWrap = False
        end
        object Memo44: TfrxMemoView
          Left = 18.897650000000000000
          Top = 245.669450000000000000
          Width = 64.252010000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Tel. res.:')
          ParentFont = False
          WordWrap = False
        end
        object Memo45: TfrxMemoView
          Left = 83.149660000000000000
          Top = 245.669450000000000000
          Width = 154.960730000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsPropriet
          DataSetName = 'frxDsPropriet'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_USUARIO_TE1]')
          ParentFont = False
          WordWrap = False
        end
        object Memo46: TfrxMemoView
          Left = 238.110390000000000000
          Top = 245.669450000000000000
          Width = 64.252010000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Tel. com.:')
          ParentFont = False
          WordWrap = False
        end
        object Memo47: TfrxMemoView
          Left = 302.362400000000000000
          Top = 245.669450000000000000
          Width = 154.960730000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsPropriet
          DataSetName = 'frxDsPropriet'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_USUARIO_TE2]')
          ParentFont = False
          WordWrap = False
        end
        object Memo48: TfrxMemoView
          Left = 457.323130000000000000
          Top = 245.669450000000000000
          Width = 64.252010000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Celular:')
          ParentFont = False
          WordWrap = False
        end
        object Memo49: TfrxMemoView
          Left = 521.575140000000000000
          Top = 245.669450000000000000
          Width = 158.740260000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsPropriet
          DataSetName = 'frxDsPropriet'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_USUARIO_CEL]')
          ParentFont = False
          WordWrap = False
        end
        object Memo50: TfrxMemoView
          Left = 18.897650000000000000
          Top = 264.567100000000000000
          Width = 64.252010000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'E-mail:')
          ParentFont = False
          WordWrap = False
        end
        object Memo51: TfrxMemoView
          Left = 83.149660000000000000
          Top = 264.567100000000000000
          Width = 597.165740000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsPropriet
          DataSetName = 'frxDsPropriet'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_USUARIO_EMAIL]')
          ParentFont = False
          WordWrap = False
        end
        object Memo52: TfrxMemoView
          Left = 18.897650000000000000
          Top = 283.464750000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Estado civil:')
          ParentFont = False
          WordWrap = False
        end
        object Memo53: TfrxMemoView
          Left = 94.488250000000000000
          Top = 283.464750000000000000
          Width = 170.078850000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsPropriet
          DataSetName = 'frxDsPropriet'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_USUARIO_ECIVIL]')
          ParentFont = False
          WordWrap = False
        end
        object Memo54: TfrxMemoView
          Left = 264.567100000000000000
          Top = 283.464750000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Conjuge:')
          ParentFont = False
          WordWrap = False
        end
        object Memo55: TfrxMemoView
          Left = 325.039580000000000000
          Top = 283.464750000000000000
          Width = 355.275590550000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsPropriet
          DataSetName = 'frxDsPropriet'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_USUARIO_CONJUGE]')
          ParentFont = False
          WordWrap = False
        end
        object Memo60: TfrxMemoView
          Top = 302.362400000000000000
          Width = 680.315155910000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsOutMorad
          DataSetName = 'frxDsOutMorad'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Empregados dom'#233'sticos')
          ParentFont = False
          WordWrap = False
        end
        object Shape3: TfrxShapeView
          Top = 321.260050000000000000
          Width = 680.315400000000000000
          Height = 90.708720000000000000
          ShowHint = False
        end
        object Shape2: TfrxShapeView
          Left = 396.850393700000000000
          Top = 349.606299212598400000
          Width = 281.574803150000000000
          Height = 60.472480000000000000
          ShowHint = False
          Shape = skRoundRectangle
        end
        object Shape1: TfrxShapeView
          Left = 2.000000000000000000
          Top = 349.606299212598400000
          Width = 394.960629920000000000
          Height = 60.472480000000000000
          ShowHint = False
          Shape = skRoundRectangle
        end
        object frxDsCondImovNOMEEMP1: TfrxMemoView
          Left = 3.779530000000000000
          Top = 324.039580000000000000
          Width = 672.756095910000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome : [frxDsCondImov."NOMEEMP1"]')
          ParentFont = False
        end
        object frxDsCondImovEHoraSSIni1: TfrxMemoView
          Left = 328.819110000000000000
          Top = 362.834880000000000000
          Width = 64.252010000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object frxDsCondImovEHoraSSSai1: TfrxMemoView
          Left = 328.819110000000000000
          Top = 385.512060000000000000
          Width = 64.252010000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object frxDsCondImovEHoraSDIni1: TfrxMemoView
          Left = 608.504330000000000000
          Top = 362.834880000000000000
          Width = 64.252010000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '')
          ParentFont = False
        end
        object frxDsCondImovEHoraSDSai1: TfrxMemoView
          Left = 608.504330000000000000
          Top = 385.512060000000000000
          Width = 64.252010000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '')
          ParentFont = False
        end
        object CheckBox1: TfrxCheckBoxView
          Left = 3.779530000000000000
          Top = 362.834880000000000000
          Width = 18.897650000000000000
          Height = 18.897650000000000000
          ShowHint = False
          CheckColor = clBlack
          CheckStyle = csCheck
          DataField = 'ESegunda1'
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo61: TfrxMemoView
          Left = 22.677180000000000000
          Top = 362.834880000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Segunda')
          ParentFont = False
        end
        object CheckBox2: TfrxCheckBoxView
          Left = 109.606370000000000000
          Top = 362.834880000000000000
          Width = 18.897650000000000000
          Height = 18.897650000000000000
          ShowHint = False
          CheckColor = clBlack
          CheckStyle = csCheck
          DataField = 'ETerca1'
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo62: TfrxMemoView
          Left = 128.504020000000000000
          Top = 362.834880000000000000
          Width = 41.574790940000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Ter'#231'a')
          ParentFont = False
        end
        object CheckBox3: TfrxCheckBoxView
          Left = 3.779530000000000000
          Top = 385.512060000000000000
          Width = 18.897650000000000000
          Height = 18.897650000000000000
          ShowHint = False
          CheckColor = clBlack
          CheckStyle = csCheck
          DataField = 'EQuarta1'
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo63: TfrxMemoView
          Left = 22.677180000000000000
          Top = 385.512060000000000000
          Width = 49.133850940000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Quarta')
          ParentFont = False
        end
        object CheckBox4: TfrxCheckBoxView
          Left = 75.590600000000000000
          Top = 385.512060000000000000
          Width = 18.897650000000000000
          Height = 18.897650000000000000
          ShowHint = False
          CheckColor = clBlack
          CheckStyle = csCheck
          DataField = 'EQuinta1'
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo64: TfrxMemoView
          Left = 94.488250000000000000
          Top = 385.512060000000000000
          Width = 45.354320940000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Quinta')
          ParentFont = False
        end
        object CheckBox5: TfrxCheckBoxView
          Left = 143.622140000000000000
          Top = 385.512060000000000000
          Width = 18.897650000000000000
          Height = 18.897650000000000000
          ShowHint = False
          CheckColor = clBlack
          CheckStyle = csCheck
          DataField = 'ESexta1'
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo65: TfrxMemoView
          Left = 162.519790000000000000
          Top = 385.512060000000000000
          Width = 37.795260940000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Sexta')
          ParentFont = False
        end
        object CheckBox6: TfrxCheckBoxView
          Left = 400.630180000000000000
          Top = 362.834880000000000000
          Width = 18.897650000000000000
          Height = 18.897650000000000000
          ShowHint = False
          CheckColor = clBlack
          CheckStyle = csCheck
          DataField = 'ESabado1'
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo66: TfrxMemoView
          Left = 419.527830000000000000
          Top = 362.834880000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'S'#225'bado')
          ParentFont = False
        end
        object CheckBox7: TfrxCheckBoxView
          Left = 400.630180000000000000
          Top = 385.512060000000000000
          Width = 18.897650000000000000
          Height = 18.897650000000000000
          ShowHint = False
          CheckColor = clBlack
          CheckStyle = csCheck
          DataField = 'EDomingo1'
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo67: TfrxMemoView
          Left = 419.527830000000000000
          Top = 385.512060000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Domingo')
          ParentFont = False
        end
        object Memo68: TfrxMemoView
          Left = 18.897650000000000000
          Top = 343.937230000000000000
          Width = 90.708720000000000000
          Height = 17.007874015748030000
          ShowHint = False
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Periodicidade: ')
          ParentFont = False
        end
        object Memo69: TfrxMemoView
          Left = 204.094620000000000000
          Top = 362.834880000000000000
          Width = 124.724490000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Hora m'#237'nima entra:')
          ParentFont = False
        end
        object Memo70: TfrxMemoView
          Left = 204.094620000000000000
          Top = 385.512060000000000000
          Width = 124.724490000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Hora m'#225'xima sa'#237'da:')
          ParentFont = False
        end
        object Memo71: TfrxMemoView
          Left = 483.779840000000000000
          Top = 362.834880000000000000
          Width = 124.724490000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Hora m'#237'nima entra:')
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          Left = 483.779840000000000000
          Top = 385.512060000000000000
          Width = 124.724490000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Hora m'#225'xima sa'#237'da:')
          ParentFont = False
        end
        object Shape4: TfrxShapeView
          Top = 414.748300000000000000
          Width = 680.315400000000000000
          Height = 90.708720000000000000
          ShowHint = False
        end
        object Shape5: TfrxShapeView
          Left = 396.850393700000000000
          Top = 443.094549210000000000
          Width = 281.574803150000000000
          Height = 60.472480000000000000
          ShowHint = False
          Shape = skRoundRectangle
        end
        object Shape6: TfrxShapeView
          Left = 2.000000000000000000
          Top = 443.094549210000000000
          Width = 394.960629920000000000
          Height = 60.472480000000000000
          ShowHint = False
          Shape = skRoundRectangle
        end
        object Memo73: TfrxMemoView
          Left = 3.779530000000000000
          Top = 417.527830000000000000
          Width = 672.756095910000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome : [frxDsCondImov."NOMEEMP1"]')
          ParentFont = False
        end
        object Memo74: TfrxMemoView
          Left = 328.819110000000000000
          Top = 456.323130000000000000
          Width = 64.252010000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '')
          ParentFont = False
        end
        object Memo75: TfrxMemoView
          Left = 328.819110000000000000
          Top = 479.000310000000000000
          Width = 64.252010000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '')
          ParentFont = False
        end
        object Memo76: TfrxMemoView
          Left = 608.504330000000000000
          Top = 456.323130000000000000
          Width = 64.252010000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '')
          ParentFont = False
        end
        object Memo77: TfrxMemoView
          Left = 608.504330000000000000
          Top = 479.000310000000000000
          Width = 64.252010000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '')
          ParentFont = False
        end
        object CheckBox8: TfrxCheckBoxView
          Left = 3.779530000000000000
          Top = 456.323130000000000000
          Width = 18.897650000000000000
          Height = 18.897650000000000000
          ShowHint = False
          CheckColor = clBlack
          CheckStyle = csCheck
          DataField = 'ESegunda2'
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo78: TfrxMemoView
          Left = 22.677180000000000000
          Top = 456.323130000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Segunda')
          ParentFont = False
        end
        object CheckBox9: TfrxCheckBoxView
          Left = 109.606370000000000000
          Top = 456.323130000000000000
          Width = 18.897650000000000000
          Height = 18.897650000000000000
          ShowHint = False
          CheckColor = clBlack
          CheckStyle = csCheck
          DataField = 'ETerca2'
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo79: TfrxMemoView
          Left = 128.504020000000000000
          Top = 456.323130000000000000
          Width = 41.574790940000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Ter'#231'a')
          ParentFont = False
        end
        object CheckBox10: TfrxCheckBoxView
          Left = 3.779530000000000000
          Top = 479.000310000000000000
          Width = 18.897650000000000000
          Height = 18.897650000000000000
          ShowHint = False
          CheckColor = clBlack
          CheckStyle = csCheck
          DataField = 'EQuarta2'
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo80: TfrxMemoView
          Left = 22.677180000000000000
          Top = 479.000310000000000000
          Width = 49.133850940000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Quarta')
          ParentFont = False
        end
        object CheckBox11: TfrxCheckBoxView
          Left = 75.590600000000000000
          Top = 479.000310000000000000
          Width = 18.897650000000000000
          Height = 18.897650000000000000
          ShowHint = False
          CheckColor = clBlack
          CheckStyle = csCheck
          DataField = 'EQuinta2'
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo81: TfrxMemoView
          Left = 94.488250000000000000
          Top = 479.000310000000000000
          Width = 45.354320940000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Quinta')
          ParentFont = False
        end
        object CheckBox12: TfrxCheckBoxView
          Left = 143.622140000000000000
          Top = 479.000310000000000000
          Width = 18.897650000000000000
          Height = 18.897650000000000000
          ShowHint = False
          CheckColor = clBlack
          CheckStyle = csCheck
          DataField = 'ESexta2'
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo82: TfrxMemoView
          Left = 162.519790000000000000
          Top = 479.000310000000000000
          Width = 37.795260940000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Sexta')
          ParentFont = False
        end
        object CheckBox13: TfrxCheckBoxView
          Left = 400.630180000000000000
          Top = 456.323130000000000000
          Width = 18.897650000000000000
          Height = 18.897650000000000000
          ShowHint = False
          CheckColor = clBlack
          CheckStyle = csCheck
          DataField = 'ESabado2'
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo83: TfrxMemoView
          Left = 419.527830000000000000
          Top = 456.323130000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'S'#225'bado')
          ParentFont = False
        end
        object CheckBox14: TfrxCheckBoxView
          Left = 400.630180000000000000
          Top = 479.000310000000000000
          Width = 18.897650000000000000
          Height = 18.897650000000000000
          ShowHint = False
          CheckColor = clBlack
          CheckStyle = csCheck
          DataField = 'EDomingo2'
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo84: TfrxMemoView
          Left = 419.527830000000000000
          Top = 479.000310000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Domingo')
          ParentFont = False
        end
        object Memo85: TfrxMemoView
          Left = 18.897650000000000000
          Top = 437.425480000000000000
          Width = 90.708720000000000000
          Height = 17.007874020000000000
          ShowHint = False
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Periodicidade: ')
          ParentFont = False
        end
        object Memo86: TfrxMemoView
          Left = 204.094620000000000000
          Top = 456.323130000000000000
          Width = 124.724490000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Hora m'#237'nima entra:')
          ParentFont = False
        end
        object Memo87: TfrxMemoView
          Left = 204.094620000000000000
          Top = 479.000310000000000000
          Width = 124.724490000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Hora m'#225'xima sa'#237'da:')
          ParentFont = False
        end
        object Memo88: TfrxMemoView
          Left = 483.779840000000000000
          Top = 456.323130000000000000
          Width = 124.724490000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Hora m'#237'nima entra:')
          ParentFont = False
        end
        object Memo89: TfrxMemoView
          Left = 483.779840000000000000
          Top = 479.000310000000000000
          Width = 124.724490000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Hora m'#225'xima sa'#237'da:')
          ParentFont = False
        end
        object Shape7: TfrxShapeView
          Top = 508.457020000000100000
          Width = 680.315400000000000000
          Height = 90.708720000000000000
          ShowHint = False
        end
        object Shape8: TfrxShapeView
          Left = 396.850393700000000000
          Top = 536.803269210000100000
          Width = 281.574803150000000000
          Height = 60.472480000000000000
          ShowHint = False
          Shape = skRoundRectangle
        end
        object Shape9: TfrxShapeView
          Left = 2.000000000000000000
          Top = 536.803269210000100000
          Width = 394.960629920000000000
          Height = 60.472480000000000000
          ShowHint = False
          Shape = skRoundRectangle
        end
        object Memo90: TfrxMemoView
          Left = 3.779530000000000000
          Top = 511.236549999999900000
          Width = 672.756095910000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome : [frxDsCondImov."NOMEEMP1"]')
          ParentFont = False
        end
        object Memo91: TfrxMemoView
          Left = 328.819110000000000000
          Top = 550.031849999999900000
          Width = 64.252010000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '')
          ParentFont = False
        end
        object Memo92: TfrxMemoView
          Left = 328.819110000000000000
          Top = 572.709030000000000000
          Width = 64.252010000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '')
          ParentFont = False
        end
        object Memo93: TfrxMemoView
          Left = 608.504330000000000000
          Top = 550.031849999999900000
          Width = 64.252010000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '')
          ParentFont = False
        end
        object Memo94: TfrxMemoView
          Left = 608.504330000000000000
          Top = 572.709030000000000000
          Width = 64.252010000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '')
          ParentFont = False
        end
        object CheckBox15: TfrxCheckBoxView
          Left = 3.779530000000000000
          Top = 550.031849999999900000
          Width = 18.897650000000000000
          Height = 18.897650000000000000
          ShowHint = False
          CheckColor = clBlack
          CheckStyle = csCheck
          DataField = 'ESegunda3'
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo95: TfrxMemoView
          Left = 22.677180000000000000
          Top = 550.031849999999900000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Segunda')
          ParentFont = False
        end
        object CheckBox16: TfrxCheckBoxView
          Left = 109.606370000000000000
          Top = 550.031849999999900000
          Width = 18.897650000000000000
          Height = 18.897650000000000000
          ShowHint = False
          CheckColor = clBlack
          CheckStyle = csCheck
          DataField = 'ETerca3'
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo96: TfrxMemoView
          Left = 128.504020000000000000
          Top = 550.031849999999900000
          Width = 41.574790940000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Ter'#231'a')
          ParentFont = False
        end
        object CheckBox17: TfrxCheckBoxView
          Left = 3.779530000000000000
          Top = 572.709030000000000000
          Width = 18.897650000000000000
          Height = 18.897650000000000000
          ShowHint = False
          CheckColor = clBlack
          CheckStyle = csCheck
          DataField = 'EQuarta3'
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo97: TfrxMemoView
          Left = 22.677180000000000000
          Top = 572.709030000000000000
          Width = 49.133850940000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Quarta')
          ParentFont = False
        end
        object CheckBox18: TfrxCheckBoxView
          Left = 75.590600000000000000
          Top = 572.709030000000000000
          Width = 18.897650000000000000
          Height = 18.897650000000000000
          ShowHint = False
          CheckColor = clBlack
          CheckStyle = csCheck
          DataField = 'EQuinta3'
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo98: TfrxMemoView
          Left = 94.488250000000000000
          Top = 572.709030000000000000
          Width = 45.354320940000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Quinta')
          ParentFont = False
        end
        object CheckBox19: TfrxCheckBoxView
          Left = 143.622140000000000000
          Top = 572.709030000000000000
          Width = 18.897650000000000000
          Height = 18.897650000000000000
          ShowHint = False
          CheckColor = clBlack
          CheckStyle = csCheck
          DataField = 'ESexta3'
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo99: TfrxMemoView
          Left = 162.519790000000000000
          Top = 572.709030000000000000
          Width = 37.795260940000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Sexta')
          ParentFont = False
        end
        object CheckBox20: TfrxCheckBoxView
          Left = 400.630180000000000000
          Top = 550.031849999999900000
          Width = 18.897650000000000000
          Height = 18.897650000000000000
          ShowHint = False
          CheckColor = clBlack
          CheckStyle = csCheck
          DataField = 'ESabado3'
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo100: TfrxMemoView
          Left = 419.527830000000000000
          Top = 550.031849999999900000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'S'#225'bado')
          ParentFont = False
        end
        object CheckBox21: TfrxCheckBoxView
          Left = 400.630180000000000000
          Top = 572.709030000000000000
          Width = 18.897650000000000000
          Height = 18.897650000000000000
          ShowHint = False
          CheckColor = clBlack
          CheckStyle = csCheck
          DataField = 'EDomingo3'
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo101: TfrxMemoView
          Left = 419.527830000000000000
          Top = 572.709030000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Domingo')
          ParentFont = False
        end
        object Memo102: TfrxMemoView
          Left = 18.897650000000000000
          Top = 531.134199999999900000
          Width = 90.708720000000000000
          Height = 17.007874020000000000
          ShowHint = False
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Periodicidade: ')
          ParentFont = False
        end
        object Memo103: TfrxMemoView
          Left = 204.094620000000000000
          Top = 550.031849999999900000
          Width = 124.724490000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Hora m'#237'nima entra:')
          ParentFont = False
        end
        object Memo104: TfrxMemoView
          Left = 204.094620000000000000
          Top = 572.709030000000000000
          Width = 124.724490000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Hora m'#225'xima sa'#237'da:')
          ParentFont = False
        end
        object Memo105: TfrxMemoView
          Left = 483.779840000000000000
          Top = 550.031849999999900000
          Width = 124.724490000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Hora m'#237'nima entra:')
          ParentFont = False
        end
        object Memo106: TfrxMemoView
          Left = 483.779840000000000000
          Top = 572.709030000000000000
          Width = 124.724490000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Hora m'#225'xima sa'#237'da:')
          ParentFont = False
        end
        object Memo107: TfrxMemoView
          Top = 600.945270000000000000
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'TELEFONES PARA EMERG'#202'NCIA')
          ParentFont = False
          VAlign = vaCenter
        end
        object frxDsCondImovEmNome1: TfrxMemoView
          Top = 623.622450000000000000
          Width = 468.661671180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome: [frxDsCondImov."EmNome1"]')
          ParentFont = False
        end
        object frxDsCondImovEmTel1: TfrxMemoView
          Left = 468.661720000000000000
          Top = 623.622450000000000000
          Width = 211.653680000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Tel.: [frxDsCondImov."EmTel1"]')
          ParentFont = False
        end
        object Memo108: TfrxMemoView
          Top = 642.520100000000000000
          Width = 468.661671180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome: [frxDsCondImov."EmNome2"]')
          ParentFont = False
        end
        object Memo109: TfrxMemoView
          Left = 468.661720000000000000
          Top = 642.520100000000000000
          Width = 211.653680000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsCondImov
          DataSetName = 'frxDsCondImov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Tel.: [frxDsCondImov."EmTel2"]')
          ParentFont = False
        end
      end
      object DetailData1: TfrxDetailData
        Height = 18.897650000000000000
        Top = 752.126470000000000000
        Width = 718.110700000000000000
        DataSet = frxDsOutMorad
        DataSetName = 'frxDsOutMorad'
        RowCount = 0
        object frxDsOutMoradNOUTMOR: TfrxMemoView
          Width = 377.952755910000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'NOUTMOR'
          DataSet = frxDsOutMorad
          DataSetName = 'frxDsOutMorad'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOutMorad."NOUTMOR"]')
          ParentFont = False
          WordWrap = False
        end
        object frxDsOutMoradDataNasc: TfrxMemoView
          Left = 377.953000000000000000
          Width = 105.826840000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'DataNasc'
          DataSet = frxDsOutMorad
          DataSetName = 'frxDsOutMorad'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsOutMorad."DataNasc"]')
          ParentFont = False
          WordWrap = False
        end
        object frxDsOutMoradParentesco: TfrxMemoView
          Left = 483.779840000000000000
          Width = 196.535511180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'Parentesco'
          DataSet = frxDsOutMorad
          DataSetName = 'frxDsOutMorad'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOutMorad."Parentesco"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object Header1: TfrxHeader
        Height = 18.897650000000000000
        Top = 710.551640000000000000
        Width = 718.110700000000000000
        object Memo57: TfrxMemoView
          Width = 377.952755910000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsOutMorad
          DataSetName = 'frxDsOutMorad'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Outros moradores')
          ParentFont = False
          WordWrap = False
        end
        object Memo58: TfrxMemoView
          Left = 377.953000000000000000
          Width = 105.826840000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsOutMorad
          DataSetName = 'frxDsOutMorad'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Nascimento')
          ParentFont = False
          WordWrap = False
        end
        object Memo59: TfrxMemoView
          Left = 483.779840000000000000
          Width = 196.535511180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsOutMorad
          DataSetName = 'frxDsOutMorad'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Parentesco')
          ParentFont = False
          WordWrap = False
        end
      end
      object Footer1: TfrxFooter
        Height = 49.133890000000000000
        Top = 793.701300000000000000
        Width = 718.110700000000000000
        object Memo110: TfrxMemoView
          Top = 30.236239999999960000
          Width = 177.637910000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          WordWrap = False
        end
        object Memo111: TfrxMemoView
          Left = 219.212740000000000000
          Top = 30.236239999999960000
          Width = 461.102660000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Assinatura')
          ParentFont = False
          WordWrap = False
        end
      end
    end
  end
  object frxDsCondImov: TfrxDBDataset
    UserName = 'frxDsCondImov'
    CloseDataSource = False
    FieldAliases.Strings = (
      'SSINI1_TXT=SSINI1_TXT'
      'SSSAI1_TXT=SSSAI1_TXT'
      'SDINI1_TXT=SDINI1_TXT'
      'SDSAI1_TXT=SDSAI1_TXT'
      'SSINI2_TXT=SSINI2_TXT'
      'SSSAI2_TXT=SSSAI2_TXT'
      'SDINI2_TXT=SDINI2_TXT'
      'SDSAI2_TXT=SDSAI2_TXT'
      'SSINI3_TXT=SSINI3_TXT'
      'SSSAI3_TXT=SSSAI3_TXT'
      'SDINI3_TXT=SDINI3_TXT'
      'SDSAI3_TXT=SDSAI3_TXT'
      'Descri=Descri'
      'SomaFracao=SomaFracao'
      'PrefixoUH=PrefixoUH'
      'NOMECONJUGE=NOMECONJUGE'
      'STATUS1=STATUS1'
      'NOMEPROP=NOMEPROP'
      'NOMEUSUARIO=NOMEUSUARIO'
      'NOMEPROCURADOR=NOMEPROCURADOR'
      'NOMEEMP1=NOMEEMP1'
      'NOMEEMP2=NOMEEMP2'
      'NOMEEMP3=NOMEEMP3'
      'IMOB=IMOB'
      'Propriet=Propriet'
      'Conjuge=Conjuge'
      'Procurador=Procurador'
      'Usuario=Usuario'
      'Andar=Andar'
      'Unidade=Unidade'
      'Conta=Conta'
      'ESegunda1=ESegunda1'
      'ETerca1=ETerca1'
      'EQuarta1=EQuarta1'
      'EQuinta1=EQuinta1'
      'ESexta1=ESexta1'
      'ESabado1=ESabado1'
      'EDomingo1=EDomingo1'
      'ESegunda2=ESegunda2'
      'ETerca2=ETerca2'
      'EQuarta2=EQuarta2'
      'EQuinta2=EQuinta2'
      'ESexta2=ESexta2'
      'ESabado2=ESabado2'
      'EDomingo2=EDomingo2'
      'ESegunda3=ESegunda3'
      'ETerca3=ETerca3'
      'EQuarta3=EQuarta3'
      'EQuinta3=EQuinta3'
      'ESexta3=ESexta3'
      'ESabado3=ESabado3'
      'EDomingo3=EDomingo3'
      'EmNome1=EmNome1'
      'EmTel1=EmTel1'
      'EmNome2=EmNome2'
      'EmTel2=EmTel2')
    DataSet = QrCondImov
    BCDToCurrency = False
    Left = 116
    Top = 4
  end
  object QrPropriet: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrProprietCalcFields
    SQL.Strings = (
      'SELECT en.EstCivil, ConjugeNome,'
      'en.Codigo, en.Cadastro, Tipo, Respons1,'
      'en.ENatal, en.PNatal, Profissao, Empresa,'
      'IF(en.Tipo=0, en.RazaoSocial, en.Nome   ) NOME_ENT,'
      'IF(en.Tipo=0, en.Fantasia   , en.Apelido) NO_2_ENT,'
      'IF(en.Tipo=0, en.CNPJ       , en.CPF    ) CNPJ_CPF,'
      'IF(en.Tipo=0, en.IE         , en.RG     ) IE_RG,'
      'IF(en.Tipo=0, en.NIRE       , ""        ) NIRE_,'
      'IF(en.Tipo=0, en.ERua       , en.PRua   ) RUA,'
      'IF(en.Tipo=0, en.ENumero    , en.PNumero) NUMERO,'
      'IF(en.Tipo=0, en.ECompl     , en.PCompl ) COMPL,'
      'IF(en.Tipo=0, en.EBairro    , en.PBairro) BAIRRO,'
      'IF(en.Tipo=0, en.ECidade    , en.PCidade) CIDADE,'
      'IF(en.Tipo=0, lle.Nome      , llp.Nome  ) NOMELOGRAD,'
      'IF(en.Tipo=0, ufe.Nome      , ufp.Nome  ) NOMEUF,'
      'IF(en.Tipo=0, en.EPais      , en.PPais  ) Pais,'
      'IF(en.Tipo=0, en.ELograd    , en.PLograd) Lograd,'
      'IF(en.Tipo=0, en.ECEP       , en.PCEP   ) CEP,'
      'IF(en.Tipo=0, en.ETe1       , en.PTe1   ) TE1,'
      'IF(en.Tipo=0, en.ETe2       , en.PTe2   ) TE2,'
      'IF(en.Tipo=0, en.ECel       , en.PCel   ) CELULAR,'
      'IF(en.Tipo=0, en.EFax       , en.PFax   ) FAX,'
      'IF(en.Tipo=0, en.EEmail     , en.PEmail ) EMAIL,'
      'IF(en.Tipo=0, "", IF(lec.Codigo=0, "", lec.Nome)) NO_ECIVIL'
      'FROM entidades en'
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF'
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF'
      'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd'
      'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd'
      'LEFT JOIN listaecivil lec ON lec.Codigo=en.EstCivil'
      'WHERE en.Codigo=:P0'
      ''
      '')
    Left = 588
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrProprietTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrProprietNATAL: TDateField
      FieldKind = fkCalculated
      FieldName = 'NATAL'
      Calculated = True
    end
    object QrProprietENatal: TDateField
      FieldName = 'ENatal'
    end
    object QrProprietPNatal: TDateField
      FieldName = 'PNatal'
    end
    object QrProprietIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrProprietCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrProprietProfissao: TWideStringField
      FieldName = 'Profissao'
      Size = 60
    end
    object QrProprietEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrProprietTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrProprietTE2: TWideStringField
      FieldName = 'TE2'
    end
    object QrProprietCELULAR: TWideStringField
      FieldName = 'CELULAR'
    end
    object QrProprietEMAIL: TWideStringField
      FieldName = 'EMAIL'
      Size = 100
    end
    object QrProprietNO_ECIVIL: TWideStringField
      FieldName = 'NO_ECIVIL'
      Size = 10
    end
    object QrProprietConjugeNome: TWideStringField
      FieldName = 'ConjugeNome'
      Size = 35
    end
  end
  object frxDsPropriet: TfrxDBDataset
    UserName = 'frxDsPropriet'
    CloseDataSource = False
    DataSet = QrPropriet
    BCDToCurrency = False
    Left = 616
    Top = 8
  end
  object QrEntidade: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT'
      'IF(en.Tipo=0, en.RazaoSocial, en.Nome   ) NOME_ENT'
      'FROM entidades en'
      'WHERE en.Codigo=:P0')
    Left = 644
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntidadeNOME_ENT: TWideStringField
      FieldName = 'NOME_ENT'
      Required = True
      Size = 100
    end
  end
  object QrMorador: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrMoradorCalcFields
    SQL.Strings = (
      'SELECT en.EstCivil, ConjugeNome,'
      'en.Codigo, en.Cadastro, Tipo, Respons1,'
      'en.ENatal, en.PNatal, Profissao, Empresa,'
      'IF(en.Tipo=0, en.RazaoSocial, en.Nome   ) NOME_ENT,'
      'IF(en.Tipo=0, en.Fantasia   , en.Apelido) NO_2_ENT,'
      'IF(en.Tipo=0, en.CNPJ       , en.CPF    ) CNPJ_CPF,'
      'IF(en.Tipo=0, en.IE         , en.RG     ) IE_RG,'
      'IF(en.Tipo=0, en.NIRE       , ""        ) NIRE_,'
      'IF(en.Tipo=0, en.ERua       , en.PRua   ) RUA,'
      'IF(en.Tipo=0, en.ENumero    , en.PNumero) NUMERO,'
      'IF(en.Tipo=0, en.ECompl     , en.PCompl ) COMPL,'
      'IF(en.Tipo=0, en.EBairro    , en.PBairro) BAIRRO,'
      'IF(en.Tipo=0, en.ECidade    , en.PCidade) CIDADE,'
      'IF(en.Tipo=0, lle.Nome      , llp.Nome  ) NOMELOGRAD,'
      'IF(en.Tipo=0, ufe.Nome      , ufp.Nome  ) NOMEUF,'
      'IF(en.Tipo=0, en.EPais      , en.PPais  ) Pais,'
      'IF(en.Tipo=0, en.ELograd    , en.PLograd) Lograd,'
      'IF(en.Tipo=0, en.ECEP       , en.PCEP   ) CEP,'
      'IF(en.Tipo=0, en.ETe1       , en.PTe1   ) TE1,'
      'IF(en.Tipo=0, en.ETe2       , en.PTe2   ) TE2,'
      'IF(en.Tipo=0, en.ECel       , en.PCel   ) CELULAR,'
      'IF(en.Tipo=0, en.EFax       , en.PFax   ) FAX,'
      'IF(en.Tipo=0, en.EEmail     , en.PEmail ) EMAIL,'
      'IF(en.Tipo=0, "", IF(lec.Codigo=0, "", lec.Nome)) NO_ECIVIL'
      'FROM entidades en'
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF'
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF'
      'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd'
      'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd'
      'LEFT JOIN listaecivil lec ON lec.Codigo=en.EstCivil'
      'WHERE en.Codigo=:P0')
    Left = 672
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMoradorTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrMoradorNATAL: TDateField
      FieldKind = fkCalculated
      FieldName = 'NATAL'
      Calculated = True
    end
    object QrMoradorENatal: TDateField
      FieldName = 'ENatal'
    end
    object QrMoradorPNatal: TDateField
      FieldName = 'PNatal'
    end
    object QrMoradorIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrMoradorCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrMoradorProfissao: TWideStringField
      FieldName = 'Profissao'
      Size = 60
    end
    object QrMoradorEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrMoradorTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrMoradorTE2: TWideStringField
      FieldName = 'TE2'
    end
    object QrMoradorCELULAR: TWideStringField
      FieldName = 'CELULAR'
    end
    object QrMoradorEMAIL: TWideStringField
      FieldName = 'EMAIL'
      Size = 100
    end
    object QrMoradorNO_ECIVIL: TWideStringField
      FieldName = 'NO_ECIVIL'
      Size = 10
    end
    object QrMoradorConjugeNome: TWideStringField
      FieldName = 'ConjugeNome'
      Size = 35
    end
  end
  object frxDsMorador: TfrxDBDataset
    UserName = 'frxDsMorador'
    CloseDataSource = False
    DataSet = QrMorador
    BCDToCurrency = False
    Left = 700
    Top = 8
  end
  object QrOutMorad: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT *'
      'FROM condparent'
      'WHERE Tipo=1'
      'OR Conta=:P0'
      'ORDER BY Tipo, NOUTMOR')
    Left = 728
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOutMoradTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrOutMoradConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrOutMoradNOUTMOR: TWideStringField
      FieldName = 'NOUTMOR'
      Size = 100
    end
    object QrOutMoradDataNasc: TWideStringField
      FieldName = 'DataNasc'
    end
    object QrOutMoradParentesco: TWideStringField
      FieldName = 'Parentesco'
      Size = 100
    end
  end
  object frxDsOutMorad: TfrxDBDataset
    UserName = 'frxDsOutMorad'
    CloseDataSource = False
    DataSet = QrOutMorad
    BCDToCurrency = False
    Left = 756
    Top = 8
  end
end
