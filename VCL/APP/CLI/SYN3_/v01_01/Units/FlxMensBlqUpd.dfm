object FmFlxMensBlqUpd: TFmFlxMensBlqUpd
  Left = 339
  Top = 185
  Caption = 'FLX-BLOQU-002 :: Confirma'#231#227'o de Etapa Mensal'
  ClientHeight = 342
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 366
        Height = 32
        Caption = 'Confirma'#231#227'o de Etapa Mensal'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 366
        Height = 32
        Caption = 'Confirma'#231#227'o de Etapa Mensal'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 366
        Height = 32
        Caption = 'Confirma'#231#227'o de Etapa Mensal'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 180
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 180
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 784
        Height = 180
        Align = alClient
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 128
          Width = 780
          Height = 50
          Align = alClient
          TabOrder = 0
          object LaDataHora: TLabel
            Left = 20
            Top = 4
            Width = 58
            Height = 13
            Caption = 'Data / hora:'
          end
          object LaTexto: TLabel
            Left = 136
            Top = 4
            Width = 61
            Height = 13
            Caption = 'Observa'#231#227'o:'
          end
          object EdDataHora: TdmkEdit
            Left = 20
            Top = 20
            Width = 112
            Height = 21
            Enabled = False
            TabOrder = 0
            FormatType = dmktfDateTime
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfLong
            Texto = '30/12/1899 00:00:00'
            QryCampo = 'Abertura'
            UpdCampo = 'Abertura'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object EdObserv: TEdit
            Left = 135
            Top = 20
            Width = 629
            Height = 21
            MaxLength = 50
            TabOrder = 1
          end
        end
        object Panel6: TPanel
          Left = 2
          Top = 15
          Width = 780
          Height = 113
          Align = alTop
          Enabled = False
          TabOrder = 1
          object Label1: TLabel
            Left = 20
            Top = 4
            Width = 60
            Height = 13
            Caption = 'Condom'#237'nio:'
          end
          object LaMes: TLabel
            Left = 708
            Top = 4
            Width = 23
            Height = 13
            Caption = 'M'#234's:'
          end
          object EdEmpresa: TdmkEditCB
            Left = 20
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            DBLookupComboBox = CBEmpresa
            IgnoraDBLookupComboBox = False
          end
          object CBEmpresa: TdmkDBLookupComboBox
            Left = 76
            Top = 20
            Width = 629
            Height = 21
            KeyField = 'Filial'
            ListField = 'NOMEFILIAL'
            ListSource = DModG.DsEmpresas
            TabOrder = 1
            dmkEditCB = EdEmpresa
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdAnoMes: TdmkEdit
            Left = 708
            Top = 20
            Width = 56
            Height = 21
            Alignment = taCenter
            TabOrder = 2
            FormatType = dmktfMesAno
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfLong
            HoraFormat = dmkhfShort
            QryCampo = 'Mez'
            UpdType = utYes
            Obrigatorio = True
            PermiteNulo = False
            ValueVariant = Null
          end
          object RGEtapa: TRadioGroup
            Left = 20
            Top = 44
            Width = 744
            Height = 65
            Caption = ' Etapa a ser confirmada: '
            Columns = 6
            ItemIndex = 0
            Items.Strings = (
              'Abertura'
              'Folha de pagamento'
              'Leitura/arrecada'#231#227'o'
              'Fechamento'
              'Admin. Riscos'
              'Impress'#227'o'
              'Protocolo'
              'Relat'#243'rio'
              'Entrega e-mail'
              'Entrega portaria'
              'Entrega postal')
            TabOrder = 3
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 228
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 689
        Height = 16
        Caption = 
          'ATEN'#199#195'O: Caso seja preenchido o campo de observa'#231#227'o, a data / ho' +
          'ra ser'#225' ignorada e a confirma'#231#227'o ficar'#225' pendente!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 689
        Height = 16
        Caption = 
          'ATEN'#199#195'O: Caso seja preenchido o campo de observa'#231#227'o, a data / ho' +
          'ra ser'#225' ignorada e a confirma'#231#227'o ficar'#225' pendente!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 272
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 16
    Top = 8
  end
end
