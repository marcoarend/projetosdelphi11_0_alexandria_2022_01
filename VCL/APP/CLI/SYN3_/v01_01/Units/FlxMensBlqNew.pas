unit FlxMensBlqNew;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, dmkDBGridDAC, dmkPermissoes, UnDmkEnums;

type
  TFmFlxMensBlqNew = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1A: TLabel;
    LaAviso2A: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrPesq: TmySQLQuery;
    QrPesqAnoMes: TIntegerField;
    QrCtrl: TmySQLQuery;
    QrCtrlFlxB_Folha: TSmallintField;
    QrCtrlFlxB_LeiAr: TSmallintField;
    QrCtrlFlxB_Fecha: TSmallintField;
    QrCtrlFlxB_Risco: TSmallintField;
    QrCtrlFlxB_Print: TSmallintField;
    QrCtrlFlxB_Proto: TSmallintField;
    QrCtrlFlxB_Relat: TSmallintField;
    QrCtrlFlxB_EMail: TSmallintField;
    QrCtrlFlxB_Porta: TSmallintField;
    QrCtrlFlxB_Postl: TSmallintField;
    QrCndNDef: TmySQLQuery;
    QrCndNDefCodigo: TIntegerField;
    QrLocJa: TmySQLQuery;
    Panel5: TPanel;
    Label32: TLabel;
    LaAnoI: TLabel;
    CBMes: TComboBox;
    CBAno: TComboBox;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    BitBtn1: TBitBtn;
    PB1: TProgressBar;
    DBGrid1: TdmkDBGridDAC;
    TbFlxCond: TmySQLTable;
    TbFlxCondCodigo: TIntegerField;
    TbFlxCondAtivo: TSmallintField;
    TbFlxCondNO_CND: TWideStringField;
    TbFlxCondFlxB_Ordem: TIntegerField;
    TbFlxCondFlxB_Folha: TSmallintField;
    TbFlxCondFlxB_LeiAr: TSmallintField;
    TbFlxCondFlxB_Fecha: TSmallintField;
    TbFlxCondFlxB_Risco: TSmallintField;
    TbFlxCondFlxB_Print: TSmallintField;
    TbFlxCondFlxB_Proto: TSmallintField;
    TbFlxCondFlxB_Relat: TSmallintField;
    TbFlxCondFlxB_EMail: TSmallintField;
    TbFlxCondFlxB_Porta: TSmallintField;
    TbFlxCondFlxB_Postl: TSmallintField;
    TbFlxCondDiaVencto: TIntegerField;
    DsFlxCond: TDataSource;
    LaAviso1B: TLabel;
    LaAviso2B: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenFlxCond(Cond: Integer);
    procedure AtualizaTodos(Ativa: Integer);
  public
    { Public declarations }
  end;

  var
  FmFlxMensBlqNew: TFmFlxMensBlqNew;

implementation

uses Module, UnMyObjects, ModuleGeral, FlxMensBlqViw, Cond, UMySQLModule;

{$R *.DFM}

procedure TFmFlxMensBlqNew.AtualizaTodos(Ativa: Integer);
begin
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('UPDATE _FLX_COND_BLQ_ SET Ativo=:P0');
  DModG.QrUpdPID1.Params[00].AsInteger := Ativa;
  DModG.QrUpdPID1.ExecSQL;
  //
  ReopenFlxCond(TbFlxCondCodigo.Value);
end;

procedure TFmFlxMensBlqNew.BitBtn1Click(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
    QrCndNDef.Close;
    QrCndNDef.Open;
    //
    QrCtrl.Close;
    QrCtrl.Open;
    //
    PB1.Position := 0;
    PB1.Max := QrCndNDef.RecordCount;
    while not QrCndNDef.Eof do
    begin
      if TbFlxCond.Locate('Codigo', QrCndNDefCodigo.Value, []) then
      begin
        PB1.Position := PB1.Position + 1;
        PB1.Update;
        Application.ProcessMessages;
        //
        TbFlxCond.Edit;
        TbFlxCondFlxB_Folha.Value := QrCtrlFlxB_Folha.Value;
        TbFlxCondFlxB_LeiAr.Value := QrCtrlFlxB_LeiAr.Value;
        TbFlxCondFlxB_Fecha.Value := QrCtrlFlxB_Fecha.Value;
        TbFlxCondFlxB_Risco.Value := QrCtrlFlxB_Risco.Value;
        TbFlxCondFlxB_Print.Value := QrCtrlFlxB_Print.Value;
        TbFlxCondFlxB_Proto.Value := QrCtrlFlxB_Proto.Value;
        TbFlxCondFlxB_Relat.Value := QrCtrlFlxB_Relat.Value;
        TbFlxCondFlxB_EMail.Value := QrCtrlFlxB_EMail.Value;
        TbFlxCondFlxB_Porta.Value := QrCtrlFlxB_Porta.Value;
        TbFlxCondFlxB_Postl.Value := QrCtrlFlxB_Postl.Value;
        TbFlxCond.Post;
      end;
      //
      QrCndNDef.Next;
    end;
  finally
    PB1.Position := 0;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmFlxMensBlqNew.BtNenhumClick(Sender: TObject);
begin
  AtualizaTodos(0);
end;

procedure TFmFlxMensBlqNew.BtOKClick(Sender: TObject);
  procedure IncluiCondominio(AnoMes, Codigo, Ano, Mes: Integer);
    function CriaData(Ano, Mes, Dia: Integer): TDateTime;
    begin
      if Dia = 0 then
        Result := 0
      else
        Result := EncodeDate(Ano, Mes, 1) + Dia - 1;
    end;
  var
    Ordem: Integer;
    Folha_DLim,
    LeiAr_DLim,
    Fecha_DLim,
    Risco_DLim,
    Print_DLim,
    Proto_DLim,
    Relat_DLim,
    EMail_DLim,
    Porta_DLim,
    Postl_DLim,
    Abert_Data: TDateTime;
    Abert_User, ddVence: Integer;
  begin
    Ordem      := TbFlxCondFlxB_Ordem.Value;
    Folha_DLim := CriaData(Ano, Mes, TbFlxCondFlxB_Folha.Value);
    LeiAr_DLim := CriaData(Ano, Mes, TbFlxCondFlxB_LeiAr.Value);
    Fecha_DLim := CriaData(Ano, Mes, TbFlxCondFlxB_Fecha.Value);
    Risco_DLim := CriaData(Ano, Mes, TbFlxCondFlxB_Risco.Value);
    Print_DLim := CriaData(Ano, Mes, TbFlxCondFlxB_Print.Value);
    Proto_DLim := CriaData(Ano, Mes, TbFlxCondFlxB_Proto.Value);
    Relat_DLim := CriaData(Ano, Mes, TbFlxCondFlxB_Relat.Value);
    EMail_DLim := CriaData(Ano, Mes, TbFlxCondFlxB_EMail.Value);
    Porta_DLim := CriaData(Ano, Mes, TbFlxCondFlxB_Porta.Value);
    Postl_DLim := CriaData(Ano, Mes, TbFlxCondFlxB_Postl.Value);
    //
    Abert_User := VAR_USUARIO;
    Abert_Data := DModG.ObtemAgora();
    ddVence    := TbFlxCondDiaVencto.Value;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'flxbloq', False, [
    'Ordem', 'ddVence', 'Abert_User', 'Abert_Data',
    'Folha_DLim',
    'LeiAr_DLim',
    'Fecha_DLim',
    'Risco_DLim',
    'Print_DLim',
    'Proto_DLim',
    'Relat_DLim',
    'EMail_DLim',
    'Porta_DLim',
    'Postl_DLim'
    ], [
    'AnoMes', 'Codigo'], [
    Ordem, ddVence, Abert_User, Abert_Data,
    Folha_DLim,
    LeiAr_DLim,
    Fecha_DLim,
    Risco_DLim,
    Print_DLim,
    Proto_DLim,
    Relat_DLim,
    EMail_DLim,
    Porta_DLim,
    Postl_DLim
    ], [
    AnoMes, Codigo], True) then
  end;
var
  Ano, Mes, AnoMes: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    Ano    := Geral.IMV(CBAno.Text);
    Mes    := CBMes.Itemindex + 1;
    //
    if (Ano < 2)  or (Mes < 1) then
    begin
      Application.MessageBox('Informe ano e m�s corretamente!', 'Aviso',
      MB_OK+MB_ICONWARNING);
      Screen.Cursor := crDefault;
      Exit;
    end else begin
      AnoMes := (Ano * 100) + Mes;
      //
      TbFlxCond.First;
      while not TbFlxCond.Eof do
      begin
        if TbFlxCondFlxB_Folha.Value +
           TbFlxCondFlxB_LeiAr.Value +
           TbFlxCondFlxB_Fecha.Value +
           TbFlxCondFlxB_Risco.Value +
           TbFlxCondFlxB_Print.Value +
           TbFlxCondFlxB_Proto.Value +
           TbFlxCondFlxB_Relat.Value +
           TbFlxCondFlxB_EMail.Value +
           TbFlxCondFlxB_Porta.Value +
           TbFlxCondFlxB_Postl.Value > 0 then
        begin
          QrLocJa.Close;
          QrLocJa.Params[00].AsInteger := TbFlxCondCodigo.Value;
          QrLocJa.Params[01].AsInteger := AnoMes;
          QrLocJa.Open;
          //
          if QrLocJa.RecordCount = 0 then
          begin
            if TbFlxCondAtivo.Value = 1 then
              IncluiCondominio(AnoMes, TbFlxCondCodigo.Value, Ano, Mes);
          end;
        end;
        //
        TbFlxCond.Next;
      end;
    end;
    FmFlxMensBlqViw.ReopenFlx(AnoMes, 0);
    Close;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmFlxMensBlqNew.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFlxMensBlqNew.BtTodosClick(Sender: TObject);
begin
  AtualizaTodos(1);
end;

procedure TFmFlxMensBlqNew.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  ImgTipo.SQLType := stPsq;
end;

procedure TFmFlxMensBlqNew.FormCreate(Sender: TObject);
var
  i: Integer;
  //
  Ano, Mes: Word;
begin
  MyObjects.PreencheCBAnoECBMes(CBAno, CBMes, -1);
  //////////////////////////////////////////////////////////////////////////
  QrPesq.Close;
  QrPesq.SQL.Clear;
  QrPesq.SQL.Add('SELECT Max(AnoMes) AnoMes');
  QrPesq.SQL.Add('FROM flxbloq');
  QrPesq.Open;
  if QrPesqAnoMes.Value > 0 then
  begin
    MLAGeral.MesEAnoDePeriodoShort(QrPesqAnoMes.Value, Mes, Ano);
    if Mes = 12 then
    begin
      Mes := 1;
      Ano := Ano + 1;
    end else begin
      Mes := Mes + 1;
    end;
    for i := 0 to CBAno.Items.Count do
    begin
      if CBAno.Items[i] = IntToStr(Ano) then
      begin
        CBAno.ItemIndex := i;
        Break;
      end;
    end;
    CBMes.ItemIndex := Mes -1;
  end;
  //
  TbFlxCond.Close;
  TbFlxCond.Database := DModG.MyPID_DB;
  //
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('DROP TABLE IF EXISTS _FLX_COND_BLQ_;');
  DModG.QrUpdPID1.SQL.Add('');
  DModG.QrUpdPID1.SQL.Add('CREATE TABLE _FLX_COND_BLQ_');
  DModG.QrUpdPID1.SQL.Add('SELECT cnd.Codigo, cnd.Ativo,');
  DModG.QrUpdPID1.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_CND,');
  DModG.QrUpdPID1.SQL.Add('cnd.DiaVencto, cnd.FlxB_Ordem, cnd.FlxB_Folha,');
  DModG.QrUpdPID1.SQL.Add('cnd.FlxB_LeiAr, cnd.FlxB_Fecha, cnd.FlxB_Risco,');
  DModG.QrUpdPID1.SQL.Add('cnd.FlxB_Print, cnd.FlxB_Proto, cnd.FlxB_Relat,');
  DModG.QrUpdPID1.SQL.Add('cnd.FlxB_EMail, cnd.FlxB_Porta, cnd.FlxB_Postl');
  DModG.QrUpdPID1.SQL.Add('FROM ' + TMeuDB + '.cond cnd');
  DModG.QrUpdPID1.SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo=cnd.Cliente');
  DModG.QrUpdPID1.SQL.Add('WHERE cnd.Ativo=1;');
  DModG.QrUpdPID1.ExecSQL;
  //
  ReopenFlxCond(0);
end;

procedure TFmFlxMensBlqNew.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1A, LaAviso2A, LaAviso1B, LaAviso2B], Caption, False, taCenter, 2, 10, 20);
end;

procedure TFmFlxMensBlqNew.ReopenFlxCond(Cond: Integer);
begin
  TbFlxCond.Close;
  TbFlxCond.Open;
  //
  TbFlxCond.Locate('Codigo', Cond, []);
end;

end.
