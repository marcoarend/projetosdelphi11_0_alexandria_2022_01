unit Atencoes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkLabel, DB,
  mySQLDbTables, ComCtrls, dmkDBGrid, dmkGeral, Menus, dmkImage, UnDmkEnums,
  DmkDAC_PF;

type
  TFmAtencoes = class(TForm)
    Panel1: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    DBGErrRepa: TDBGrid;
    Splitter1: TSplitter;
    DBGErrRepaIts: TdmkDBGrid;
    Panel5: TPanel;
    BtRatifQuit: TBitBtn;
    DBGErrRepaSum: TDBGrid;
    BtLocPag: TBitBtn;
    BtQuitaOrig: TBitBtn;
    BtExclLancto: TBitBtn;
    TabSheet2: TTabSheet;
    DBGLct: TdmkDBGrid;
    Panel3: TPanel;
    BitBtn1: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    PBAtz1: TProgressBar;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtRatifQuitClick(Sender: TObject);
    procedure BtLocPagClick(Sender: TObject);
    procedure BtQuitaOrigClick(Sender: TObject);
    procedure BtExclLanctoClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FTabLctA: String;
  end;

  var
  FmAtencoes: TFmAtencoes;

implementation

uses Module, Principal, UnMyObjects, ModuleAtencoes, BloqParc, MyDBCheck,
  ModuleGeral, UnFinanceiro;

{$R *.DFM}

procedure TFmAtencoes.BitBtn1Click(Sender: TObject);
var
  TipoData: Integer;
  Vencto: TDataCompetencia;
begin
  TipoData := MyObjects.SelRadioGroup(
    'Data Base',
    'Selacione a data base do m�s de compet�ncia',
    ['Data do lan�amento', 'Vencimento', 'M�s anterior ao vencimento',
    'Dois meses antes do vencimento'], 1);
  if TipoData > -1 then
  begin 
    Vencto := TDataCompetencia(TipoData);
    UFinanceiro.ColocarMesDeCompetenciaOndeNaoTem(Vencto,
      DmAtencoes.QrSemMez, DmAtencoes.QrSemMez, FTabLctA);
  end;
end;

procedure TFmAtencoes.BtExclLanctoClick(Sender: TObject);
{
var
  i, k: Integer;
}
begin
{
  if DBGLct.SelectedRows.Count > 1 then
  begin
    if Geral.MensagemBox('Confirma a exclus�o dos itens selecionados?',
    'Pergunta', MB_ICONQUESTION+MB_YESNOCANCEL) =
    ID_YES then
    begin
      with DBGLct.DataSource.DataSet do
      for i:= 0 to DBGLct.SelectedRows.Count-1 do
      begin
        GotoBoo kmark(po inter(DBGLct.SelectedRows.Items[i]));
        //
        UFinanceiro.ExcluiItemCarteira(DmAtencoes.QrErrRepaControle.Value,
          DmAtencoes.QrErrRepaData.Value, DmAtencoes.QrErrRepaCarteira.Value,
          DmAtencoes.QrErrRepaSub.Value, DmAtencoes.QrErrRepaGenero.Value,
          DmAtencoes.QrErrRepaCartao.Value, DmAtencoes.QrErrRepaSit.Value,
          DmAtencoes.QrErrRepaTipo.Value, 0, DmAtencoes.QrErrRepaID_Pgto.Value,
          DmAtencoes.QrErrRepa, DmLct0.QrCrt, False, DmAtencoes.QrErrRepaCarteira.Value,
          DmLct0.FTabLctA);
      end;
      k := UMyMod.ProximoRegistro(DmAtencoes.QrErrRepa, 'Controle',  0);
      UFinanceiro.RecalcSaldoCarteira(
        DmLct0.QrCrtCodigo.Value, DmLct0.QrCrt, DmAtencoes.QrErrRepa, False, True);
      DmAtencoes.QrErrRepa.Locate('Controle', k, []);
    end;
  end else
}
  (*
  Atualizado em 2014-04-03 -> N�o permitir quitar os originais para quitar
    deve ser desfeito o parcelamento e quitado manualmente para evitar
    inconformidades entre lan�amentos e parcelamentos

    UFinanceiro.ExcluiItemCarteira(DmAtencoes.QrErrRepaControle.Value,
      DmAtencoes.QrErrRepaData.Value, DmAtencoes.QrErrRepaCarteira.Value,
      DmAtencoes.QrErrRepaSub.Value, DmAtencoes.QrErrRepaGenero.Value,
      DmAtencoes.QrErrRepaCartao.Value, DmAtencoes.QrErrRepaSit.Value,
      DmAtencoes.QrErrRepaTipo.Value, 0, DmAtencoes.QrErrRepaID_Pgto.Value,
      DmAtencoes.QrErrRepa, nil, True, DmAtencoes.QrErrRepaCarteira.Value,
      dmkPF.MotivDel_ValidaCodigo(300), FTabLctA, True);

  Fim atualizado em 2014-04-03
  *)
end;

procedure TFmAtencoes.BtLocPagClick(Sender: TObject);
var
  Parcelamento, Documento: Integer;
begin
  if(DmAtencoes.QrErrRepaIts.State = dsInactive) or
    (DmAtencoes.QrErrRepaIts.RecordCount = 0)
  then
    Exit;
  //
  if DBCheck.CriaFm(TFmBloqParc, FmBloqParc, afmoNegarComAviso) then
  begin
    FmPrincipal.FDatabase1A  := Dmod.MyDB;
    FmPrincipal.FDatabase1B  := DModG.MyPID_DB;
    FmPrincipal.FDatabase2A := nil;
    FmPrincipal.FDatabase2B := nil;
    Fmbloqparc.DefineDatabaseTabelas();
    Documento := Trunc(DmAtencoes.QrErrRepaDocumento.Value);
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT Codigo');
    Dmod.QrAux.SQL.Add('FROM bloqparcpar');
    Dmod.QrAux.SQL.Add('WHERE Lancto=:P0');
    Dmod.QrAux.Params[0].AsInteger := DmAtencoes.QrErrRepaID_Pgto.Value;
    Dmod.QrAux.Open;
    Parcelamento := Dmod.QrAux.FieldByName('Codigo').AsInteger;
    FmBloqParc.LocCod(Parcelamento, Parcelamento);
    if FmBloqParc.QrBloqParcCodigo.Value <> Parcelamento then
      Geral.MensagemBox('N�o foi poss�vel localizar o parcelamento' +
      ' n� ' + IntToStr(Parcelamento) + '!', 'Aviso', MB_OK+MB_ICONERROR)
    else
    if not FmBloqParc.QrBloqParcPar.Locate('FatNum', Documento, []) then
      Geral.MensagemBox('N�o foi poss�vel localizar o a parcela ' +
      ' (bloqueto n� ' + IntToStr(Documento) + ') do parcelamento n� ' +
      IntToStr(Parcelamento) + '!', 'Aviso', MB_OK+MB_ICONERROR)
    else
      Geral.MensagemBox('A parcela do bloqueto n� ' +
      IntToStr(Documento) + ' do parcelamento n� ' +
      IntToStr(Parcelamento) + ' foi localizada com sucesso!', 'Mensagem',
      MB_OK+MB_ICONINFORMATION);
    //
    FmBloqParc.ShowModal;
    FmBloqParc.Destroy;
    //
    DmAtencoes.QrErrRepa.Close;
    DmAtencoes.QrErrRepa.Open;
  end;
end;

procedure TFmAtencoes.BtQuitaOrigClick(Sender: TObject);
begin
{
  Atualizado em 2014-04-03 -> N�o permitir quitar os originais para quitar
    deve ser desfeito o parcelamento e quitado manualmente para evitar
    inconformidades entre lan�amentos e parcelamentos

::: Quita��o de original de reparcelamento
  if not DBCheck.LiberaPelaSenhaBoss then Exit;
  //
  if Geral.MensagemBox('Esta op��o s� deve ser usada quando o ' +
  'reparcelmento j� foi ratificado! Deseja continuar assim mesmo?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    (*Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE lan ctos SET Sit=2, Compensado=:P0');
    Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
    *)
    while not DmAtencoes.QrErrRepaIts.Eof do
    begin
      UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
        'Sit', 'Compesado'], ['Controle'], [
        2, Geral.FDT(DmAtencoes.QrErrRepaCompensado.Value, 1)], [
        DmAtencoes.QrErrRepaItsCTRL_LAN.Value], True, '', TabLct?);
      (*
      Dmod.QrUpd.Params[00].AsString := Geral.FDT(DmAtencoes.QrErrRepaCompensado.Value, 1);
      Dmod.QrUpd.Params[01].AsInteger := DmAtencoes.QrErrRepaItsCTRL_LAN.Value;
      Dmod.QrUpd.ExecSQL;
      *)
      //
      DmAtencoes.QrErrRepaIts.Next;
    end;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add(EXCLUI_DE + VAR LCT + ' WHERE Controle=:P0');
    Dmod.QrUpd.Params[00].AsInteger := DmAtencoes.QrErrRepaControle.Value;
    Dmod.QrUpd.ExecSQL;
    //
    VerificaOrelhas;
    Screen.Cursor := crDefault;
  end;

  Fim atualizado em 2014-04-03
}
end;

procedure TFmAtencoes.BtRatifQuitClick(Sender: TObject);
{:::
const
  ActivePageIndex = 3;
  QrBPP_Pgt = nil;
var
  BPPLancto: Integer;
}
begin
  if(DmAtencoes.QrErrRepaIts.State = dsInactive) or
    (DmAtencoes.QrErrRepaIts.RecordCount = 0)
  then
    Exit;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT * ',
    'FROM bloqparcpar ',
    'WHERE Controle=' + Geral.FF0(DmAtencoes.QrErrRepaOriAtrelado.Value),
    '']);
  if Dmod.QrAux.RecordCount > 0 then
  begin
    Screen.Cursor := crHourGlass;
    try
      DmAtencoes.AtualizaParcelaReparcNovo(Dmod.QrAux.FieldByName('Codigo').AsInteger,
        Dmod.QrAux.FieldByName('Controle').AsInteger, DmAtencoes.QrErrRepaControle.Value,
        Dmod.QrAux.FieldByName('Lancto').AsInteger, PBAtz1, nil, nil, nil, -1, nil,
        FTabLctA);
      DmAtencoes.ReopenErrRepa();
    finally
      Screen.Cursor := crDefault;
    end;
  end else
    Geral.MB_Erro('Falha ao localizar parcelamentos!' + sLineBreak + 'Utilize o bot�o "Localiza parcel."!');

{::: N�o pode ser aqui? Precisa do TabLctA!
  Antes de fazer, verificar se cliint j� usa o novo financeiro!!!
  BPPLancto := DmAtencoes.QrErrRepaID_Pgto.Value;
  Dmod.AtualizaParcelaReparcNovo(DmAtencoes.QrErrRepaReparcel.Value, DmAtencoes.QrErrRepaOriAtrelado.Value,
    DmAtencoes.QrErrRepaControle.Value, BPPLancto,
    PBAtz1, LaAtz1, PnAtz1, PageControl3, ActivePageIndex, QrBPP_Pgt);
  // Reopenbloqparcpar(QrbloqparcparControle.Value);
  (*
  Dmod.AtualizaParcelaReparcNovo(DmAtencoes.QrErrRepaOriAtrelado.Value,
    DmAtencoes.QrErrRepaControle.Value, QrbloqparcparLancto.Value, PBAtz1, LaAtz1, PnAtz1,
  PageControl3, ActivePageIndex, QrBPP_Pgt);
  //
  *)
  VerificaOrelhas;
}
end;

procedure TFmAtencoes.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmAtencoes.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmAtencoes.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  BtExclLancto.Visible := False;
  BtQuitaOrig.Visible  := False;
  //
  DBGErrRepa.DataSource      := DmAtencoes.DsErrRepa;
  DBGErrRepaIts.DataSource   := DmAtencoes.DsErrRepaIts;
  DBGErrRepaSum.DataSource   := DmAtencoes.DsErrRepaSum;
end;

procedure TFmAtencoes.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
