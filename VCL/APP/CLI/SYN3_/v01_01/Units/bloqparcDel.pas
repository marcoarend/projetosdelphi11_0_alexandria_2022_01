unit bloqparcDel;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkLabel, dmkEdit,
  DB, mySQLDbTables, DBCtrls, Mask, dmkGeral, ComCtrls, dmkImage, UnDmkEnums;

type
  TFmBloqParcDel = class(TForm)
    Panel1: TPanel;
    QrPesq1: TmySQLQuery;
    QrRepaN: TmySQLQuery;
    QrRepaL: TmySQLQuery;
    QrParcN: TmySQLQuery;
    QrParcL: TmySQLQuery;
    QrItnsN: TmySQLQuery;
    QrItnsL: TmySQLQuery;
    QrLctsN: TmySQLQuery;
    QrLctsL: TmySQLQuery;
    QrLctsLControle: TIntegerField;
    QrLctsLUH: TWideStringField;
    QrLctsLNOMERELACIONADO: TWideStringField;
    QrLctsLNO_CLIINT: TWideStringField;
    QrLctsLData: TDateField;
    QrLctsLFatNum: TFloatField;
    DsLctsL: TDataSource;
    DsLctsN: TDataSource;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DBGrid1: TDBGrid;
    QrLctsLDescricao: TWideStringField;
    QrLctsNControle: TIntegerField;
    QrLctsNUH: TWideStringField;
    QrLctsNNOMERELACIONADO: TWideStringField;
    QrLctsNNO_CLIINT: TWideStringField;
    QrLctsNData: TDateField;
    QrLctsNFatNum: TFloatField;
    QrLctsNDescricao: TWideStringField;
    DBGrid2: TDBGrid;
    QrAtreN: TmySQLQuery;
    QrAtreL: TmySQLQuery;
    QrAtreLControle: TIntegerField;
    QrAtreNControle: TIntegerField;
    Panel4: TPanel;
    Panel5: TPanel;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    EdRepaN: TdmkEdit;
    EdRepaL: TdmkEdit;
    EdParcN: TdmkEdit;
    EdParcL: TdmkEdit;
    EdItnsN: TdmkEdit;
    EdItnsL: TdmkEdit;
    EdLctsN: TdmkEdit;
    EdLctsL: TdmkEdit;
    EdAtreN: TdmkEdit;
    EdAtreL: TdmkEdit;
    Panel3: TPanel;
    Label1: TLabel;
    EdBloqParc: TdmkEdit;
    BtPesq: TBitBtn;
    GroupBox2: TGroupBox;
    Panel6: TPanel;
    Label9: TLabel;
    EdParcIni: TdmkEdit;
    EdParcFim: TdmkEdit;
    Label10: TLabel;
    BitBtn1: TBitBtn;
    QrPesq1Codigo: TAutoIncField;
    QrCad: TmySQLQuery;
    QrCadCodigo: TIntegerField;
    DsCad: TDataSource;
    DBGrid3: TDBGrid;
    QrLctsLCarteira: TIntegerField;
    QrLctsLNO_CART: TWideStringField;
    QrLctsNCarteira: TIntegerField;
    QrLctsNNO_CART: TWideStringField;
    QrPesq2: TmySQLQuery;
    QrPesq2Codigo: TAutoIncField;
    QrPesq2CodCliEsp: TIntegerField;
    QrPesq2CodCliEnt: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel7: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel8: TPanel;
    PnSaiDesis: TPanel;
    BtCorrige: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtPesqClick(Sender: TObject);
    procedure EdRepaNChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrLctsNAfterOpen(DataSet: TDataSet);
    procedure QrLctsLAfterOpen(DataSet: TDataSet);
    procedure BtCorrigeClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure DBGrid3DblClick(Sender: TObject);
    procedure EdBloqParcChange(Sender: TObject);
  private
    { Private declarations }
    FTabLctA: String;
    FItens: Integer;
    procedure Pesquisa(Avisa: Boolean);
    procedure ReopenCad(Codigo: Integer);
  public
    { Public declarations }
  end;

  var
  FmBloqParcDel: TFmBloqParcDel;

implementation

uses Module, UCreate, ModuleGeral, UMySQLModule, ModuleFin, UnInternalConsts,
UnMyObjects;

{$R *.DFM}

procedure TFmBloqParcDel.BitBtn1Click(Sender: TObject);
const
  CodUsu = 0;
  Nome   = '';
var
  I, Codigo: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    UCriar.RecriaTempTable('Cad_0', DModG.QrUpdPID1, False);
    //
    for I := EdParcIni.ValueVariant to EdParcFim.ValueVariant do
    begin
      if I <> 0  then
      begin
        EdBloqParc.ValueVariant := I;
        Pesquisa(False);
        if FItens > 0 then
        begin
          Codigo := I;
          //
          UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, 'cad_0', False, [
          'Codigo', 'CodUsu', 'Nome'], [
          ], [
          Codigo, CodUsu, Nome], [
          ], False);
        end;
      end;
    end;
    ReopenCad(0);
    Screen.Cursor := crDefault;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmBloqParcDel.BtCorrigeClick(Sender: TObject);
{
var
  BP: String;
}
begin
  // Parei Aqui!  2010-10-16
  Geral.MensagemBox('A��o n�o implementada! Solicite � Dermatek!', 'Informa��o',
  MB_OK+MB_ICONINFORMATION);
{
  if (EdBloqParc.ValueVariant <> 0) and
  // Evitar engano!
  (EdBloqParc.ValueVariant = QrRepaN.Params[0].AsInteger) then
  begin
    Screen.Cursor := crHourGlass;
    try
      BP := FormatFloat('0', EdBloqParc.ValueVariant);
      // BloqParc
      if QrRepaN.RecordCount = 1 then
        DMod.MyDBn.Execute('DELETE FROM bloqparc WHERE Codigo=' + BP);
      if QrRepaL.RecordCount = 1 then
        DMod.MyDB.Execute('DELETE FROM bloqparc WHERE Codigo=' + BP);
      //
      //
      // BloqParcPar
      if QrParcN.RecordCount = 1 then
        DMod.MyDBn.Execute('DELETE FROM bloqparcpar WHERE Codigo=' + BP);
      if QrParcL.RecordCount = 1 then
        DMod.MyDB.Execute('DELETE FROM bloqparcpar WHERE Codigo=' + BP);
      //
      //
      // BloqParcIts
      if QrItnsN.RecordCount > 0 then
        DMod.MyDBn.Execute('DELETE FROM bloqparcits WHERE Codigo=' + BP);
      if QrItnsL.RecordCount > 0 then
        DMod.MyDB.Execute('DELETE FROM bloqparcits WHERE Codigo=' + BP);
      //
      //
      // Lan�amentos originais
      if QrAtreN.RecordCount > 0 then
        DMod.MyDBn.Execute('UPDATE lct? SET Reparcel=0 WHERE Reparcel=' + BP);
      if QrAtreL.RecordCount >0 then
        DMod.MyDB.Execute('UPDATE lct? SET Reparcel=0 WHERE Reparcel=' + BP);
      //
      //
      //
      // Bloquetos (lan�amentos) de parcelamento n�o exclu�dos
      if QrLctsN.RecordCount >0 then
      begin
        QrLctsN.First;
        while not QrLctsN.Eof do
        begin
          DMod.MyDBn.Execute('DELETE FROM lct? WHERE Controle=' + FormatFloat('0', QrLctsNControle.Value));
          QrLctsN.Next;
        end;
      end;
      if QrLctsL.RecordCount >0 then
      begin
        QrLctsL.First;
        while not QrLctsL.Eof do
        begin
          DMod.MyDB.Execute('DELETE FROM lct? WHERE Controle=' + FormatFloat('0', QrLctsLControle.Value));
          QrLctsL.Next;
        end;
      end;
      //
      Pesquisa(True);
    finally
      Screen.Cursor := crDefault;
    end;
  end;
}
end;

procedure TFmBloqParcDel.BtPesqClick(Sender: TObject);
begin
  Pesquisa(True);
end;

procedure TFmBloqParcDel.Pesquisa(Avisa: Boolean);
{
var
  BloqParc: Integer;
}
begin
  // Parei Aqui!  2010-10-16
  Geral.MensagemBox('A��o n�o implementada! Solicite � Dermatek!', 'Informa��o',
  MB_OK+MB_ICONINFORMATION);
{
  FItens := 0;
  QrPesq1.Close;
  QrPesq2.Close;
  EdRepaN.ValueVariant := 0;
  EdRepaL.ValueVariant := 0;
  EdParcN.ValueVariant := 0;
  EdParcL.ValueVariant := 0;
  EdItnsN.ValueVariant := 0;
  EdItnsL.ValueVariant := 0;
  EdAtreN.ValueVariant := 0;
  EdAtreL.ValueVariant := 0;
  EdLctsN.ValueVariant := 0;
  EdLctsL.ValueVariant := 0;
  //
  BloqParc := EdBloqParc.ValueVariant;
  if BloqParc < 0 then
  begin
    QrPesq2.Database := Dmod.MyDB;
  end else if BloqParc > 0 then
  begin
    QrPesq2.Database := Dmod.MyDBn;
  end else
  begin
    Geral.MensagemBox('Informe o c�digo do parcelamento!', 'Aviso',
    MB_OK+MB_ICONWARNING);
    Exit;
  end;
  QrPesq2.SQL.Clear;
  QrPesq2.SQL.Add('SELECT Codigo, CodCliEsp, CodCliEnt');
  QrPesq2.SQL.Add('FROM bloqparc');
  QrPesq2.SQL.Add('WHERE Codigo=:P0');
  QrPesq2.SQL.Add('');
  QrPesq2.Params[00].AsInteger := BloqParc;
  QrPesq2.Open;
  //
  if QrPesq2.RecordCount > 0 then
  begin
    if Avisa then
      Geral.MensagemBox('O parcelamento n� ' + FormatFloat('0', BloqParc) +
      ' est� ativo. Se deseja exclu�-lo, acesse a janela apropriada!',
      'aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end else begin
    // Parei aqui! ver o que fazer!!!
    if DmodFin.EntidadeHabilitadadaParaFinanceiroNovo(
      QrPesq2CodCliEnt.Value, True, True) <> sfnNovoFin then Exit;
    FTabLctA  := DModG.NomeTab(ntLct, False, ttA, QrPesq2CodCliEnt.Value);
    //
    QrRepaN.Close;
    QrRepaN.Params[0].AsInteger := BloqParc;
    QrRepaN.Open;
    EdRepaN.ValueVariant := QrRepaN.RecordCount;
    //
    QrRepaL.Close;
    QrRepaL.Params[0].AsInteger := BloqParc;
    QrRepaL.Open;
    EdRepaL.ValueVariant := QrRepaL.RecordCount;
    //
    QrParcN.Close;
    QrParcN.Params[0].AsInteger := BloqParc;
    QrParcN.Open;
    EdParcN.ValueVariant := QrParcN.RecordCount;
    //
    QrParcL.Close;
    QrParcL.Params[0].AsInteger := BloqParc;
    QrParcL.Open;
    EdParcL.ValueVariant := QrParcL.RecordCount;
    //
    QrItnsN.Close;
    QrItnsN.Params[0].AsInteger := BloqParc;
    QrItnsN.Open;
    EdItnsN.ValueVariant := QrItnsN.RecordCount;
    //
    QrItnsL.Close;
    QrItnsL.Params[0].AsInteger := BloqParc;
    QrItnsL.Open;
    EdItnsL.ValueVariant := QrItnsL.RecordCount;
    //
    QrLctsN.Close;
    QrLctsN.SQL.Clear;
    QrLctsN.SQL.Add('SELECT ci.Unidade UH,');
    QrLctsN.SQL.Add('IF(la.Cliente>0,');
    QrLctsN.SQL.Add('  IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome),');
    QrLctsN.SQL.Add('  IF (la.Fornecedor>0,');
    QrLctsN.SQL.Add('    IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome), "")) NOMERELACIONADO,');
    QrLctsN.SQL.Add('IF(em.Tipo=0, em.RazaoSocial, em.Nome) NO_CLIINT,');
    QrLctsN.SQL.Add(' la.Data, la.Controle, la.FatNum, la.Descricao,');
    QrLctsN.SQL.Add('la.Carteira, ca.Nome NO_CART');
    QrLctsN.SQL.Add('FROM lct? la');
    QrLctsN.SQL.Add('LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira');
    QrLctsN.SQL.Add('LEFT JOIN entidades em ON em.Codigo=la.CliInt');
    QrLctsN.SQL.Add('LEFT JOIN entidades cl ON cl.Codigo=la.Cliente');
    QrLctsN.SQL.Add('LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor');
    QrLctsN.SQL.Add('LEFT JOIN condimov  ci ON ci.Conta=la.Depto');
    QrLctsN.SQL.Add('');
    QrLctsN.SQL.Add('');
    QrLctsN.SQL.Add('');
    QrLctsN.SQL.Add('');
    QrLctsN.SQL.Add('WHERE Genero=-10');
    QrLctsN.SQL.Add('AND FatID=610');
    QrLctsN.SQL.Add('AND Descricao LIKE "%(Reparc. '+FormatFloat('0', BloqParc) +')%"');
    QrLctsN.Open;
    EdLctsN.ValueVariant := QrLctsN.RecordCount;
    //
    QrLctsL.Close;
    QrLctsL.SQL := QrLctsN.SQL;
    QrLctsL.Open;
    EdLctsL.ValueVariant := QrLctsL.RecordCount;
    //
    QrAtreN.Close;
    QrAtreN.Params[0].AsInteger := BloqParc;
    QrAtreN.Open;
    EdAtreN.ValueVariant := QrAtreN.RecordCount;
    //
    QrAtreL.Close;
    QrAtreL.Params[0].AsInteger := BloqParc;
    QrAtreL.Open;
    EdAtreL.ValueVariant := QrAtreL.RecordCount;
    //
  end;
}
end;

procedure TFmBloqParcDel.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmBloqParcDel.DBGrid3DblClick(Sender: TObject);
begin
  if QrCad.RecordCount > 0 then
  begin
    EdBloqParc.ValueVariant := QrCadCodigo.Value;
    Pesquisa(True);
  end;
end;

procedure TFmBloqParcDel.EdBloqParcChange(Sender: TObject);
begin
  QrRepaN.Close;
  QrRepaL.Close;
  QrParcN.Close;
  QrParcL.Close;
  QrItnsN.Close;
  QrItnsL.Close;
  QrLctsN.Close;
  QrLctsL.Close;
  QrAtreN.Close;
  QrAtreL.Close;
  FTabLctA := '???';
end;

procedure TFmBloqParcDel.EdRepaNChange(Sender: TObject);
begin
  FItens := (
    EdRepaN.ValueVariant +
    EdRepaL.ValueVariant +
    EdParcN.ValueVariant +
    EdParcL.ValueVariant +
    EdItnsN.ValueVariant +
    EdItnsL.ValueVariant +
    EdAtreN.ValueVariant +
    EdAtreL.ValueVariant +
    EdLctsN.ValueVariant +
    EdLctsL.ValueVariant );
  //
  BtCorrige.Enabled := FItens > 0;
end;

procedure TFmBloqParcDel.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  //
  QrPesq1.Close;
  QrPesq1.Database := Dmod.MyDB;
  QrPesq1.SQL.Clear;
  QrPesq1.SQL.Add('SELECT MIN(Codigo) Codigo');
  QrPesq1.SQL.Add('FROM bloqparc');
  QrPesq1.SQL.Add('');
  QrPesq1.Open;
  EdParcIni.ValueVariant := QrPesq1Codigo.Value - 10;
  //
  QrPesq1.Close;
  QrPesq1.Database := Dmod.MyDBn;
  QrPesq1.SQL.Clear;
  QrPesq1.SQL.Add('SELECT MAX(Codigo) Codigo');
  QrPesq1.SQL.Add('FROM bloqparc');
  QrPesq1.SQL.Add('');
  QrPesq1.Open;
  EdParcFim.ValueVariant := QrPesq1Codigo.Value + 10;
  //
end;

procedure TFmBloqParcDel.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  QrCad.Close;
  QrCad.Database := DModG.MyPID_DB;
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmBloqParcDel.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmBloqParcDel.QrLctsLAfterOpen(DataSet: TDataSet);
begin
  if QrLctsL.RecordCount > 0 then
    PageControl1.ActivePageIndex := 0;
end;

procedure TFmBloqParcDel.QrLctsNAfterOpen(DataSet: TDataSet);
begin
  if QrLctsN.RecordCount > 0 then
    PageControl1.ActivePageIndex := 1;
end;

procedure TFmBloqParcDel.ReopenCad(Codigo: Integer);
begin
  QrCad.Close;
  QrCad.Open;
  QrCad.Locate('Codigo', Codigo, []);
end;

end.
