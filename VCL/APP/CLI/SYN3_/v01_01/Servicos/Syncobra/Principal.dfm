object FmPrincipal: TFmPrincipal
  Left = 0
  Top = 0
  Caption = 'Syncobra'
  ClientHeight = 300
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  WindowState = wsMinimized
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 244
    Width = 635
    Height = 56
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 0
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 631
      Height = 39
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 22
        Width = 631
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object Memo1: TMemo
    Left = 0
    Top = 70
    Width = 635
    Height = 174
    Align = alClient
    TabOrder = 1
    ExplicitTop = 0
    ExplicitHeight = 244
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 0
    Width = 635
    Height = 70
    Align = alTop
    TabOrder = 2
    ExplicitLeft = -69
    ExplicitTop = 230
    ExplicitWidth = 704
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 631
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitTop = 20
      object CkInicializacao: TdmkCheckBox
        Left = 13
        Top = 20
        Width = 250
        Height = 17
        Caption = 'Executar na inicializa'#231#227'o do sistema operacional'
        TabOrder = 0
        OnClick = CkInicializacaoClick
        UpdType = utYes
        ValCheck = '1'
        ValUncheck = '0'
        OldValor = #0
      end
    end
  end
  object TrayIcon1: TTrayIcon
    Visible = True
    OnDblClick = TrayIcon1DblClick
    Left = 56
    Top = 128
  end
  object ApplicationEvents1: TApplicationEvents
    OnActivate = ApplicationEvents1Activate
    OnMinimize = ApplicationEvents1Minimize
    Left = 84
    Top = 128
  end
  object Timer1: TTimer
    Interval = 10000
    OnTimer = Timer1Timer
    Left = 164
    Top = 132
  end
  object Timer2: TTimer
    Left = 292
    Top = 96
  end
end
