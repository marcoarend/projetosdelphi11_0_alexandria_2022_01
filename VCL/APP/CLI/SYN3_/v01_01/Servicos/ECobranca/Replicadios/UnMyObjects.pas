unit UnMyObjects;
interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db, DbCtrls, (*DBTables,*)
  UnInternalConsts2, ComCtrls, Registry, TypInfo, dmkEdit, dmkEditCB, dmkGeral,
  dmkEditDateTimePicker, dmkDBLookupCombobox, dmkCheckBox, dmkRadioGroup,
  dmkDBEdit, dmkValUsu, dmkCheckGroup, dmkMemo, dmkDBGrid, Variants, ScktComp,
  dmkDBGridDAC, IdStack, InvokeRegistry, AdvToolBar, AdvGlowButton, Grids,
  WinSkinData, Buttons, DBGrids, ComObj, Mask, MaskUtils, dmkCompoStore,
  dmkImage,
  // frxReport
  frxClass, frxPreview, frxRich, mySQLExceptions,
  // TMS Software
  AdvGrid, DBAdvGrid;
type
  TPriorityPath = (ppAtual_Edit, ppInfo_Ini);
  TTreeViewImagesSet = (cTreeViewError,           // 0   erro do treeview
                        cCBMay, cCBNot, cCBYes,   // 1..3
                        cRBMay, cRBNot, cRBYes);  // 4..6

  //TArrInt = array of Integer;
  THackDBGrid = class(TDBGrid);
  TUnMyObjects = class(TObject)
  private
    { Private declarations }
{
    procedure DefParams(Acao: Integer);
}
  public
    { Public declarations }
    //  G E R A L
{
    procedure MostraErro(Sender: TObject; E: Exception);
    function ErroDeSocket(Errno: word; ErrMsg: String): String;
    procedure ExecutaCmd(cmd: String; MeResult: TMemo);
    function CompactaArquivo(Arquivo, PastaOri, PastaDest: String; Memo: TMemo;
              ExcluiArqOri: Boolean; PastaDoZip: String): Boolean;
    function FIC(FaltaInfoCompo: Boolean; Compo: TWinControl;
             Mensagem: String; ExibeMsg: Boolean = True): Boolean;
    function FIC_Edit_Int(const Edit: TdmkEdit; const Mensagem: String;
             var Valor: Integer): Boolean;
    function FIC_Edit_Flu(const Edit: TdmkEdit; const Mensagem: String;
             var Valor: Double): Boolean;
    function SelRadioGroup(CaptionFm, CaptionRG: String; Itens: array of String;
             Colunas: Integer; Default: Integer = -1): Integer;
    function SelRadioGroup2(const CaptionFm, CaptionRG: String;
             const Itens: array of String; const Avisos: array of String;
             const Acoes: array of TSQLType; const Valores: array of String;
             const MyCods: array of Integer;
             const Colunas: Integer; var Valor: String;
             var MyOcorr: Integer; var SQLType: TSQLType;
             const DefaultItemIndex: Integer = -1): Boolean;
    procedure ControleStrings(ScreenActiveForm: TForm);
    function  CompoMoeda(Caption: String): String;
    procedure ControleCor(ScreenActiveForm: TForm);
    function  CorIniComponente(Handle: THandle = 0): Boolean;
    function  CompoMinusculo(Nome: String): Boolean;
    function  ArquivoExiste(Caminho: String; MemoOut: TMemo; DoLocalize:
              Boolean = True): Boolean;

    // T C O N T R O L
    // nada
    // T W I N C O N T R O L
    function  WinControlSetFocus(WinCtrlName: String): Boolean;
    // nada
    // S T R I N G   G R I D
    procedure LarguraAutomaticaGrade(AGrid: TStringGrid);
    function  ExcluiLinhaStringGrid(Grade: TStringGrid): Boolean; // ExcluiLinha
    function  Xls_To_StringGrid(AGrid: TStringGrid; AXLSFile: String;
              PB1: TProgressBar; LaAviso1, LaAviso2: TLabel): Boolean;
    procedure PulaCelulaGradeInput(GradePula, GradeCodi: TStringGrid;
              var Key: Word; Shift: TShiftState);
    procedure PulaCelulaGradeInputEx(GradePula, GradeCodi: TStringGrid;
              var Key: Word; Shift: TShiftState; BtOK: TBitBtn);
    procedure DesenhaTextoEmStringGrid(Grade: TStringGrid; Rect: TRect;
              CorTexto, CorFundo: TColor; Alinha: TAlignment; Texto: String;
              FixedCols, FixedRows: Integer; DesenhaSelecionado: Boolean);
    procedure DesenhaTextoEmStringGridEx(Grade: TStringGrid; Rect: TRect;
              CorTexto, CorFundo: TColor; Alinha: TAlignment; Texto: String;
              FixedCols, FixedRows: Integer; DesenhaSelecionado: Boolean;
              FonteNome: String; FonteSize: Integer; FonteStyle: TFontStyles;
              PasswordChar: Char);
    // DESENHOS EM GRADE
    //  Rota��o de texto
    procedure StringGridRotateTextOut(Grid: TStringGrid;
              ARow, ACol: Integer; Rect: TRect; NomeFonte: string;
              Size: Integer; Color: TColor; Alignment: TAlignment);
    procedure StringGridRotateTextOut2(Grid: TStringGrid; ARow, ACol:
              Integer; Rect: TRect; NomeFonte: String; Size: Integer;
              Color: TColor; Alignment:TAlignment);
    // Grade inteira de n�meros
    procedure DesenhaGradeNumeros(Grade: TStringGrid; ACol, ARow: Integer;
              Rect: TRect; State: TGridDrawState; AllColorFixo: Boolean);
    // Grades em geral
    procedure DesenhaGradeGeral(Grade: TStringGrid; ACol, ARow: Integer;
              Rect: TRect; State: TGridDrawState; Alinhamentos: array of TAlignment;
              AllColorFixo: Boolean);
    //  Grade de ativa��o
    procedure DesenhaGradeA(GradeA: TStringGrid; ACol, ARow: Integer;
              Rect: TRect; State: TGridDrawState; AllColorFixo: Boolean);
    procedure DesenhaGradeN(GradeDesenhar, GradeAtivos, GradeCompara:
              TStringGrid; ACol, ARow: Integer;Rect: TRect; State:
              TGridDrawState; Fmt: String;
              FixedCols, FixedRows: Integer; PermiteVazio: Boolean);
    procedure LimpaGrade(MinhaGrade: TStringGrid; ColIni, RowIni:
              Integer; ExcluiLinhas: Boolean);
    procedure LimpaGrades(Grades: array of TStringGrid; ColIni, RowIni:
              Integer; ExcluiLinhas: Boolean);
    // D B G R I D
    procedure AjustaTamanhoMaximoColunaDBGrid(Grid: TDBGrid; Campo: String;
              MinLarg: Integer);
    procedure DesenhaTextoEmDBGrid(Grade: TDBGrid; Rect: TRect;
              CorTexto, CorFundo: TColor; Alinha: TAlignment; Texto: String);
    procedure DBGridSelectAll(AGrid: TDBGrid);
    procedure DBGridLarguraSegundoTitulo(DBGTitul, DBGDados: TDBGrid;
              Colunas: array of Integer);
    procedure DefineTituloDBGrid(DBGrid: TDBGrid;
              FieldName, Titulo: String);
    procedure DefineCorTextoSitLancto(DBGrid: TDBGrid; Rect: TRect; FieldName,
              TextoSit: String);
    procedure SelecionarLinhasNoDBGrid(AGrid: TDBGrid; Selecionar: Boolean);

    // O P E N    D I A L O G
    function FileOpenDialog(Form: TForm; const IniDir, Arquivo, Titulo, Filtro:
         String; Opcoes: TOpenOptions; var Resultado: String): Boolean;
    function  DefineDiretorio(Form: TForm; dmkEdit: TdmkEdit): String;
    function  DefineArquivo1(Form: TForm; dmkEdit: TdmkEdit): Boolean;
    function  DefineArquivo2(Form: TForm; dmkEdit: TdmkEdit; Pasta, Arquivo:
              String; Prioridade: TPriorityPath; SoArquivo: Boolean = False):
              Boolean;
    // C O M B O    B O X
    function  PreencheCBAnoECBMes(CBAno, CBMes: TComboBox;
              IncremMes: Integer = 1; Nulo: Boolean = False): String;
    function  CBAnoECBMesToPeriodoTxt(CBAno, CBMes: TComboBox): String;
    function  CBAnoECBMesToDate(CBAno, CBMes: TComboBox;
              Dia, IncMes: Integer): TDateTime;
    function  DefineIndexComboBox(CBx: TComboBox; Valor: String): Boolean;
    function  SetaPeriodoDeCBANoMes(CBAno, CBMes: TComboBox): Integer;
    procedure CBAnoMesFromPeriodo(Periodo: Integer; CBAno, CBMes: TComboBox;
              Avisa: Boolean);
    // E D I T
    procedure UperCaseComponente();
    // L A B E L  (+ ProgressBar)
}
    procedure Informa(Label1: TLabel; Aguarde: Boolean; Texto: String);
    procedure InformaEUpdPB(PB: TProgressBar; Label1: TLabel;
              Aguarde: Boolean; Texto: String);
    procedure Informa2(Label1, Label2: TLabel; Aguarde: Boolean; Texto: String);
    procedure InformaN(Labels: array of TLabel; Aguarde: Boolean; Texto: String);
    procedure Informa2EUpdPB(PB: TProgressBar; Label1, Label2: TLabel;
              Aguarde: Boolean; Texto: String);
    procedure UpdPB(PB: TProgressBar; Label1, Label2: TLabel);
    // B I T M A P
{
    procedure CarregaImagemEmTImage(Image: TImage; Arquivo: String;
              Bitmap: TBitmap; BrushColor: TColor);
    procedure DisplayBitmap(const Bitmap: TBitmap; const Image: TImage;
              BrushColor: TColor);
    // T R A D I O G R O U P
    procedure ConfiguraRadioGroup(RadioGroup: TRadioGroup; Itens:
              array of String; Colunas, Default: Integer);
    procedure ConfiguraCheckGroup(CheckGroup: TdmkCheckGroup; Itens:
              array of String; Colunas, Default: Integer; FirstAll: Boolean);
    procedure ConfiguraTipoCobranca(RGTipoCobranca: TRadioGroup; Colunas, Default: Integer);
    procedure ConfiguraAllFmtStr(RGAllFmtStr: TRadioGroup; Colunas, Default: Integer);
    procedure SetaRGCNAB(RGCNAB: TRadioGroup; CNAB: Integer);
    // T A D V T O O L B A R P A G E R
    procedure CopiaItensDeMenu(PMGeral: TPopupMenu; Form: TForm);
    // F O R M S
    procedure Entitula(GroupBox: TGroupBox;
              Titulos, Avisos: array of TLabel; Titulo: String;
              RetiraIDForm: Boolean; Alignment: TAlignment;
              Left, Top, FontSize: Integer);
    function  CriaForm_AcessoTotal(InstanceClass: TComponentClass; var Reference): Boolean;
    function  FormShow(InstanceClass: TComponentClass; var Reference): Boolean;
    function  FormCria(InstanceClass: TComponentClass; var Reference): Boolean;
    function  Senha(ComponentClass: TComponentClass;
              Reference: TComponent; Tipo: Integer; LaVlr, EdVlr, LaPercent, EdPercent,
              VlrBase: String; Login: Boolean; LaData: String; Data: TDateTime;
              Janela: String): Integer;
    procedure FormMsg(var Msg: TMsg; var Handled: Boolean);
    function MeuwParamEnter(Controle : TWinControl) : Integer;
    function MeuwParamPonto(Controle : TWinControl) : Integer;

    // T R E E V I E W E V
    procedure ToggleTreeViewCheckBoxes_Unico(Node: TTreeNode);
    function  ToggleTreeViewCheckBoxes_Child(Node: TTreeNode): Integer;
    // M E N U    I T E M
    procedure HabilitaMenuItemCabDel(MenuItem: TMenuItem; QrCab, QrIts: TDataSet);
    procedure HabilitaMenuItemCabDelC1I2(MenuItem: TMenuItem; QrCab,
              QrIts1, QrIts2: TDataSet);
    procedure HabilitaMenuItemCabDelC1I3(MenuItem: TMenuItem; QrCab,
              QrIts1, QrIts2, QrIts3: TDataSet);
    procedure HabilitaMenuItemCabDelC1I4(MenuItem: TMenuItem; QrCab,
              QrIts1, QrIts2, QrIts3, QrIts4: TDataSet);
    procedure HabilitaMenuItemCabUpd(MenuItem: TMenuItem; QrCab: TDataSet);
    procedure HabilitaMenuItemItsIns(MenuItem: TMenuItem; QrCab: TDataSet);
    procedure HabilitaMenuItemItsDel(MenuItem: TMenuItem; QrIts: TDataSet);
    procedure HabilitaMenuItemItsUpd(MenuItem: TMenuItem; QrIts: TDataSet);
    // P O P   U P
    procedure MostraPopOnControlXY(PM:TPopUpMenu; ComponentePai:TControl;
              PosX, PosY: Integer);
    procedure MostraPopUpDeBotao(PM:TPopUpMenu; ComponentePai:TControl);
    procedure MostraPopUpNoCentro(PM:TPopUpMenu);
    procedure MostraPopUpDeBotaoObject(PM:TPopUpMenu; ComponentePai:
              TObject; MoreWid, MoreTop: Integer);
    function  MostraPMCEP(PMCEP: TPopupMenu; Botao: TObject): Boolean;
    // F A S T   R E P O R T
    procedure frxMemoAlignment(v: TfrxMemoView; Align: Integer);
    function  frxMostra(frx: TfrxReport; Titulo: string): Boolean;
    function  frxPrepara(frx: TfrxReport; Titulo: string): Boolean;
    function  frxImprime(frx: TfrxReport; Titulo: string): Boolean;
    function  frxSalva(frx: TfrxReport; Titulo, Arquivo: string): Boolean;
    function  frxNextDesign(): Boolean;
    procedure frxDefineDataSets(frx: TfrxReport;
              DataSets: array of TfrxDataset);
    // R E D E
    function TentaDefinirDiretorio(const Host, ServerDir: String;
             const Empresa: Integer; const Pasta: String;
             const LaAviso1, LaAviso2: TLabel; const AddDirEmp: Boolean;
             MemoNA: TMemo; var Dir: String): Boolean;
    // T L I S T B O X
    procedure ListaItensListBoxDeLista(ListBox: TListBox; Lista: array of String);
    // T R I C H E D I T
    procedure DefineTextoRichEdit(RichEdit: TRichEdit; Texto: String);
    procedure TextoEntreRichEdits(Sorc, Dest: TRichEdit);
    function  ObtemTextoRichEdit(Form: TForm; RichEdit: TRichEdit): WideString;
    function  EditaTextoRichEdit(RichEdit: TRichEdit; IniAtributesSize: Integer;
              IniAtributesName: String; ListaDeTags: array of String): WideString;
    function  GetRTF(RE: TRichedit): String;
    function  GetfrxRTF(RE: TfrxRichView): String;
    // T D M K C O M P O S T O R E
    procedure DefineValDeCompoEmbeded(Container: TdmkCompoStore; Valor: Variant);
    // T L A B E L
    procedure AjustaFonte(MyLabel: TLabel; Qualidade: Integer);
    // T A D V S T R I N G G R I D
    procedure UpdMergeDBAvGrid(ASG: TDBAdvGrid; Cols: array of Integer);
}
  end;

var
  MyObjects: TUnMyObjects;

implementation


(*
uses MyGlyfs, Principal, SelRadioGroup, UnDmkProcFunc, MeuFrx,
{$IFDEF UsaWSuport} UnDmkWeb, {$ENDIF} MeuDBUses, Textos2;
*)

{
procedure TUnMyObjects.SelecionarLinhasNoDBGrid(AGrid: TDBGrid;
  Selecionar: Boolean);
begin
  AGrid.SelectedRows.Clear;
  with AGrid.DataSource.DataSet do
  begin
    DisableControls;
    First;
    try
      while not EOF do
      begin
        AGrid.SelectedRows.CurrentRowSelected := Selecionar;
        Next;
      end;
    finally
      EnableControls;
    end;
  end;
end;

function TUnMyObjects.SelRadioGroup(CaptionFm, CaptionRG: String;
Itens: array of String; Colunas: Integer; Default: Integer = -1): Integer;
var
  I: Integer;
begin
  CriaForm_AcessoTotal(TFmSelRadioGroup, FmSelRadioGroup);
  FmSelRadioGroup.Caption := 'XXX-XXXXX-999 :: ' + CaptionFm;
  FmSelRadioGroup.RGItens.Caption := CaptionRG;
  FmSelRadioGroup.RGItens.Items.Clear;
  FmSelRadioGroup.RGItens.Columns := Colunas;
  for I := Low(Itens) to High(Itens) do
    FmSelRadioGroup.RGItens.Items.Add(Itens[I]);
  FmSelRadioGroup.RGItens.ItemIndex := Default;
  FmSelRadioGroup.ShowModal;
  Result := FmSelRadioGroup.RGItens.ItemIndex;
  FmSelRadioGroup.Destroy;
end;

function TUnMyObjects.SelRadioGroup2(const CaptionFm, CaptionRG: String;
  const Itens, Avisos: array of String; const Acoes: array of TSQLType;
  const Valores: array of String; const MyCods: array of Integer;
  const Colunas: Integer; var Valor: String; var MyOcorr: Integer;
  var SQLType: TSQLType; const DefaultItemIndex: Integer): Boolean;
var
  I, K: Integer;
  Aviso: String;
begin
  CriaForm_AcessoTotal(TFmSelRadioGroup, FmSelRadioGroup);
  FmSelRadioGroup.Caption := 'XXX-XXXXX-999 :: ' + CaptionFm;
  FmSelRadioGroup.RGItens.Caption := CaptionRG;
  FmSelRadioGroup.RGItens.Items.Clear;
  FmSelRadioGroup.RGItens.Columns := Colunas;
  for I := Low(Itens) to High(Itens) do
    FmSelRadioGroup.RGItens.Items.Add(Itens[I]);
  FmSelRadioGroup.RGItens.ItemIndex := DefaultItemIndex;
  //
  K := Length(Avisos);
  SetLength(FmSelRadioGroup.FAvisos, K);
  for I := Low(Avisos) to High(Avisos) do
    FmSelRadioGroup.FAvisos[I] := Avisos[I];
  //
  K := Length(Acoes);
  SetLength(FmSelRadioGroup.FAcoes, K);
  for I := Low(Acoes) to High(Acoes) do
    FmSelRadioGroup.FAcoes[I] := Acoes[I];
  //
  K := Length(Valores);
  SetLength(FmSelRadioGroup.FValores, K);
  for I := Low(Valores) to High(Valores) do
    FmSelRadioGroup.FValores[I] := Valores[I];
  //
  K := Length(MyCods);
  SetLength(FmSelRadioGroup.FMyCods, K);
  for I := Low(MyCods) to High(MyCods) do
    FmSelRadioGroup.FMyCods[I] := MyCods[I];
  //
  FmSelRadioGroup.ShowModal;
  Result := FmSelRadioGroup.RGItens.ItemIndex > -1;
  if Result then
  begin
    Valor := Valores[FmSelRadioGroup.RGItens.ItemIndex];
    SQLType := Acoes[FmSelRadioGroup.RGItens.ItemIndex];
    Aviso :=  Avisos[FmSelRadioGroup.RGItens.ItemIndex];
    MyOcorr := MyCods[FmSelRadioGroup.RGItens.ItemIndex];
    if Aviso <> '' then
      Geral.MensagemBox('AVISO!' + #13#10 + Aviso, 'AVISO!',
      MB_OK+MB_ICONWARNING);
  end else begin
    Valor := '';
    SQLType := stNil;
  end;
  FmSelRadioGroup.Destroy;
end;

function TUnMyObjects.Senha(ComponentClass: TComponentClass;
  Reference: TComponent; Tipo: Integer; LaVlr, EdVlr, LaPercent, EdPercent,
  VlrBase: String; Login: Boolean; LaData: String; Data: TDateTime;
  Janela: String): Integer;
var
  i: Integer;
begin
  VAR_VALORSENHA := 0;
  VAR_FSENHA := Tipo;
  FormCria(ComponentClass, Reference);
  with Reference do
  begin
    for i := 0 to ComponentCount -1 do
    begin
      if (Components[i] is TLabel) then
      begin
        if TLabel(Components[i]).Name = 'LaJanela' then
          TLabel(Components[i]).Caption := Janela;
        if TLabel(Components[i]).Name = 'LaBase' then
          TLabel(Components[i]).Caption := VlrBase;
        if TLabel(Components[i]).Name = 'LaLogin' then
        begin
          if Login then
            TLabel(Components[i]).Visible := True;
        end;
        if TLabel(Components[i]).Name = 'LaValor' then
        begin
          if LaVlr <> CO_VAZIO then
          begin
            TLabel(Components[i]).Visible := True;
            TLabel(Components[i]).Caption := LaVlr;
          end;
        end;
        if TLabel(Components[i]).Name = 'LaPorcentagem' then
        begin
          if LaPercent <> CO_VAZIO then
          begin
            TLabel(Components[i]).Visible := True;
            TLabel(Components[i]).Caption := LaPercent;
          end;
        end;
        if TLabel(Components[i]).Name = 'LaData' then
        begin
          if LaData <> CO_VAZIO then
          begin
            TLabel(Components[i]).Visible := True;
            TLabel(Components[i]).Caption := LaPercent;
          end;
        end;
      end else if (Components[i] is TdmkEdit) then
      begin
        if TdmkEdit(Components[i]).Name = 'EdLogin' then
        begin
          if Login then
            TdmkEdit(Components[i]).Visible := True;
        end;
        if TdmkEdit(Components[i]).Name = 'EdValor' then
        begin
          if LaVlr <> CO_VAZIO then
          begin
            TdmkEdit(Components[i]).Visible := True;
            TdmkEdit(Components[i]).Text    := EdVlr;
          end;
        end;
        if TdmkEdit(Components[i]).Name = 'EdPorcentagem' then
        begin
          if LaPercent <> CO_VAZIO then
          begin
            TdmkEdit(Components[i]).Visible := True;
            TdmkEdit(Components[i]).Text    := EdPercent;
          end;
        end;
      end else if (Components[i] is TDateTimePicker) then
      begin
        if LaData <> '' then
        begin
          TDateTimePicker(Components[i]).Visible := True;
          TDateTimePicker(Components[i]).Date := Data;
        end;
      end;
    end;
  end;
  TForm(Reference).ShowModal;
  Reference.Destroy;
  Result := VAR_SENHARESULT; //  2 = OK
end;

function TUnMyObjects.SetaPeriodoDeCBANoMes(CBAno, CBMes: TComboBox): Integer;
var
  Ano, Mes: Integer;
begin
  Result := 0;
  Mes := CBMes.ItemIndex + 1;
  Ano := Geral.IMV(CBAno.Text);
  if Mes > 0 then
  begin
    Result := Mes;
    if Ano > 2000 then
      Result := Result + ((Ano - 2000) * 12);
  end;
end;

procedure TUnMyObjects.SetaRGCNAB(RGCNAB: TRadioGroup; CNAB: Integer);
begin
  case CNAB of
    0: RGCNAB.ItemIndex := 0;
    240: RGCNAB.ItemIndex := 1;
    400: RGCNAB.ItemIndex := 2;
    else Geral.MensagemBox('Defini��o de CNAB desconhecida: ' +
    Geral.FF0(CNAB) + '!', 'ERRO', MB_OK+MB_ICONERROR);
  end;
end;

procedure TUnMyObjects.StringGridRotateTextOut(Grid: TStringGrid;
ARow, ACol: Integer; Rect: TRect; NomeFonte: string;
Size: Integer; Color: TColor; Alignment: TAlignment);
var
  lf: TLogFont;
  tf: TFont;
begin
  // if the font is to big, resize it
  // wenn Schrift zu gro� dann anpassen
  if (Size > Grid.ColWidths[ACol] div 2) then
    Size := Grid.ColWidths[ACol] div 2;
  with Grid.Canvas do
  begin
    // Replace the font
    // Font setzen
    Font.Name := NomeFonte;
    Font.Size := Size;
    Font.Color := Color;
    tf := TFont.Create;
    try
      tf.Assign(Font);
      GetObject(tf.Handle, SizeOf(lf), @lf);
      lf.lfEscapement  := 900;
      lf.lfOrientation := 0;
      tf.Handle := CreateFontIndirect(lf);
      Font.Assign(tf);
    finally
      tf.Free;
    end;
    // fill the rectangle
    // Rechteck f�llen
    FillRect(Rect);
    // Align text and write it
    // Text nach Ausrichtung ausgeben
    if Alignment = taLeftJustify then
      TextRect(Rect, Rect.Left + 2,Rect.Bottom - 2,Grid.Cells[ACol, ARow]);
    if Alignment = taCenter then
      TextRect(Rect, Rect.Left + Grid.ColWidths[ACol] div 2 - Size +
        Size div 3,Rect.Bottom - 2,Grid.Cells[ACol, ARow]);
    if Alignment = taRightJustify then
      TextRect(Rect, Rect.Right - Size - Size div 2 - 2,Rect.Bottom -
        2,Grid.Cells[ACol, ARow]);
  end;
end;

// 2. Alternative: Display text vertically in StringGrid cells
// 2. Variante: Vertikale Textausgabe in den Zellen eines StringGrid
procedure TUnMyObjects.StringGridRotateTextOut2(Grid: TStringGrid; ARow, ACol:
Integer; Rect: TRect; NomeFonte: String; Size: Integer;
Color: TColor; Alignment: TAlignment);
var
    NewFont, OldFont : Integer;
    FontStyle, FontItalic, FontUnderline, FontStrikeout: Integer;
begin
   // if the font is to big, resize it
   // wenn Schrift zu gro� dann anpassen
   If (Size > Grid.ColWidths[ACol] DIV 2) Then
       Size := Grid.ColWidths[ACol] DIV 2;
   with Grid.Canvas do
   begin
       // Set font
       // Font setzen
       If (fsBold IN Font.Style) Then
          FontStyle := FW_BOLD
       Else
          FontStyle := FW_NORMAL;

       If (fsItalic IN Font.Style) Then
          FontItalic := 1
       Else
          FontItalic := 0;

       If (fsUnderline IN Font.Style) Then
          FontUnderline := 1
       Else
          FontUnderline := 0;

       If (fsStrikeOut IN Font.Style) Then
          FontStrikeout:=1
       Else
          FontStrikeout:=0;

       Font.Color := Color;

       NewFont := CreateFont(Size, 0, 900, 0, FontStyle, FontItalic,
                             FontUnderline, FontStrikeout, DEFAULT_CHARSET,
                             OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
                             DEFAULT_PITCH, PChar(NomeFonte));

       OldFont := SelectObject(Handle, NewFont);
       // fill the rectangle
       // Rechteck f�llen
       FillRect(Rect);
       // Write text depending on the alignment
       // Text nach Ausrichtung ausgeben
       If Alignment = taLeftJustify Then
          TextRect(Rect,Rect.Left+2,Rect.Bottom-2,Grid.Cells[ACol,ARow]);
       If Alignment = taCenter Then
          TextRect(Rect,Rect.Left+Grid.ColWidths[ACol] DIV 2 - Size + Size DIV 3,
            Rect.Bottom-2,Grid.Cells[ACol,ARow]);
       If Alignment = taRightJustify Then
          TextRect(Rect,Rect.Right-Size - Size DIV 2 - 2,Rect.Bottom-2,Grid.Cells[ACol,ARow]);

       // Recreate reference to the old font
       // Referenz auf alten Font wiederherstellen
       SelectObject(Handle, OldFont);
       // Recreate reference to the new font
       // Referenz auf neuen Font l�schen
       DeleteObject(NewFont);
   end;
end;

function TUnMyObjects.TentaDefinirDiretorio(const Host, ServerDir: String;
  const Empresa: Integer; const Pasta: String; const LaAviso1, LaAviso2: TLabel;
  const AddDirEmp: Boolean; MemoNA: TMemo; var Dir: String): Boolean;
  procedure Msg(Txt: String);
  begin
    Geral.MensagemBox(Txt, 'ERRO', MB_OK+MB_ICONERROR);
  end;
const
  ArqImg = 'teste.~txt';
var
  //Res, Titulo, 
  SrvIP, SrvDir, ArqDir, IniDir, FullArq: String;
  //I, F: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    Result := False;
    //
    SrvIP :=  dmkPF.IgnoraLocalhost(Host);
    SrvDir := dmkPF.VeSeEhDirDmk(ServerDir, Pasta, False);
    //
    if AddDirEmp then
      ArqDir  := 'Emp_' + Geral.Substitui(Geral.FFN(Empresa, 4), '-', '_')
    else ArqDir := '';  
    //
    if SrvIP <> '' then
    begin
      SrvIP :=  '\\' + SrvIP + '\';
      // Tirar C:\
      if pos(':\', SrvDir) = 2 then
        SrvDir := Copy(SrvDir, 4);
    end;
    IniDir := SrvIP + SrvDir;
    //
    Informa2(LaAviso1, LaAviso2, True, 'Verificando pasta: ' + IniDir);
    if DirectoryExists(IniDir) then
    begin
      Result := True;
      FullArq := Geral.Substitui(IniDir + '\' + ArqDir + '\' + ArqImg, '\\', '\');
      while pos('\\', FullArq) > 0 do
        FullArq := Geral.Substitui(IniDir + '\' + ArqDir, '\\', '\');
      if FullArq[1] = '\' then
        FullArq := '\' + FullArq;
      if FileExists(FullArq) then
      begin
        try
          DeleteFile(FullArq);
        except
          Msg('N�o foi poss�vel excluir arquivo teste!');
        end;
      end;
      try
        Dir := ExtractFileDir(FullArq);
        Informa2(LaAviso1, LaAviso2, True, 'Verificando pasta: ' + Dir);
        ForceDirectories(Dir);
        Geral.SalvaTextoEmArquivo(FullArq, 'Teste', True);
        Result := True;
      except
          Msg('N�o foi poss�vel salvar arquivo teste: ' + #13#10 + FullArq);
      end;
    end else
    begin
      if MemoNA <> nil then
        ArquivoExiste(IniDir, MemoNA, False)
      else
      Msg(
      'O diret�rio raiz obrigat�rio definido nas op��es espec�ficas n�o foi localizado!' +
        #13#10 + IniDir);
    end;
  finally
    Informa2(LaAviso1, LaAviso2, False, '...');
    Screen.Cursor := crDefault;
  end;
end;

procedure TUnMyObjects.TextoEntreRichEdits(Sorc, Dest: TRichEdit);
var
  MemoryStream: TMemoryStream;
begin
  MemoryStream := TMemoryStream.Create;
  try
    Sorc.Lines.SaveToStream(MemoryStream);
    MemoryStream.Position := 0;
    Dest.Lines.LoadFromStream(MemoryStream);
  finally
    MemoryStream.Free;
  end;
end;

function TUnMyObjects.ToggleTreeViewCheckBoxes_Child(Node: TTreeNode): Integer;
  procedure SetaAncestrais(const Filho: TTreeNode; const StatusFilho:
  TTreeViewImagesSet; var Nivel: Integer);
  var
    Pai, Irmao: TTreeNode;
    StatusPai: TTreeViewImagesSet;
    Nao, Sim, May: Integer;
  begin
    Pai := Filho.Parent;
    if Assigned(Pai) then
    begin
      Nivel := Nivel + 1;
      if (Pai.Count = 1) or (StatusFilho = cCBMay) then
        StatusPai := StatusFilho
      else
      begin
        Nao := 0;
        Sim := 0;
        May := 0;
        Irmao := Pai.getFirstChild;
        while Assigned(Irmao) do
        begin
          case TTreeViewImagesSet(Irmao.StateIndex) of
            cCBMay: May := May + 1;
            cCBNot: Nao := Nao + 1;
            cCBYes: Sim := Sim + 1;
          end;
          if May > 0 then
            Break
          else
          if (Nao > 0) and (Sim > 0) then
            Break;
          Irmao := Irmao.getNextSibling;
        end;
        if Sim = Pai.Count then
          StatusPai := cCBYes
        else
        if Nao = Pai.Count then
          StatusPai := cCBNot
        else
          StatusPai := cCBMay;
      end;
      Pai.StateIndex := Integer(StatusPai);
      SetaAncestrais(Pai, StatusPai, Nivel);
    end;
  end;
  procedure SetaFilhos(Pai: TTreeNode; StatusPai: TTreeViewImagesSet);
  var
    Filho: TTreeNode;
  begin
    Filho := Pai.getFirstChild;
    while Assigned(Filho) do
    begin
      Filho.StateIndex := Integer(StatusPai);
      SetaFilhos(Filho, StatusPai);
      Filho := Filho.getNextSibling;
    end;
  end;
var
  Tmp: TTreeNode;
  Status: TTreeViewImagesSet;
begin
  Result := 0;
  if Assigned(Node) then
  begin
    Status := TTreeViewImagesSet(Node.StateIndex);
    case Status of
      cTreeViewError: ;
      cCBMay: Status := cCBNot;
      cCBNot: Status := cCBYes;
      cCBYes: Status := cCBNot;
      cRBMay: Status := cRBNot;
      cRBNot: Status := cRBYes;
      cRBYes: Status := cRBNot;
    end;
    if Status in ([cCBMay, cCBNot, cCBYes]) then
    begin
      Node.StateIndex := Integer(Status);
      SetaFilhos(Node, Status);
      SetaAncestrais(Node, Status, Result);
    end
    else
    begin
      tmp := Node.Parent;
      if not Assigned(tmp) then
        tmp := TTreeView(Node.TreeView).Items.getFirstNode
      else
        tmp := tmp.getFirstChild;
      while Assigned(tmp) do
      begin
        if (tmp.StateIndex in [Integer(cRBNot), Integer(cRBYes)]) then
          tmp.StateIndex := Integer(cRBNot);
        tmp := tmp.getNextSibling;
      end;
      Node.StateIndex := Integer(cRBYes);
    end;
  end;
end;

procedure TUnMyObjects.ToggleTreeViewCheckBoxes_Unico(Node: TTreeNode);
var
  tmp:TTreeNode;
begin
  if Assigned(Node) then
  begin
    if Node.StateIndex = Integer(cCBNot) then
      Node.StateIndex := Integer(cCBYes)
    else if Node.StateIndex = Integer(cCBYes) then
      Node.StateIndex := Integer(cCBNot)
    else if Node.StateIndex = Integer(cRBNot) then
    begin
      tmp := Node.Parent;
      if not Assigned(tmp) then
        tmp := TTreeView(Node.TreeView).Items.getFirstNode
      else
        tmp := tmp.getFirstChild;
      while Assigned(tmp) do
      begin
        if (tmp.StateIndex in [Integer(cRBNot), Integer(cRBYes)]) then
          tmp.StateIndex := Integer(cRBNot);
        tmp := tmp.getNextSibling;
      end;
      Node.StateIndex := Integer(cRBYes);
    end;
  end;
end;

procedure TUnMyObjects.UperCaseComponente;
var
  i : Integer;
  Compo : TComponent;
begin
  for i := 0 to Screen.ActiveForm.ComponentCount -1 do
  begin
    Compo := Screen.ActiveForm.Components[i];
    if Screen.ActiveForm.Components[i] is TEdit   then
       TEdit(Compo).CharCase := ecUpperCase;
    if Screen.ActiveForm.Components[i] is TdmkEdit then
    begin
      if TdmkEdit(Compo).CharCase = ecNormal then
        TdmkEdit(Compo).CharCase := ecUpperCase;
    end;
    if Screen.ActiveForm.Components[i] is TDBEdit then
       TDBEdit(Compo).CharCase := ecUpperCase;
    if Screen.ActiveForm.Components[i] is TMaskEdit then
       TMaskEdit(Compo).CharCase := ecUpperCase;
//    if Screen.ActiveForm.Components[i] is TDBMemo then
//       TDBMemo(Compo).CharCase := ecUpperCase;
//    if Screen.ActiveForm.Components[i] is TMemo then
//       TMemo(Compo).CharCase := ecUpperCase;
    if Screen.ActiveForm.Components[i] is TDBComboBox then
       TDBComboBox(Compo).CharCase := ecUpperCase;
//    if Screen.ActiveForm.Components[i] is TDBLookupComboBox then
//       TDBLookupComboBox(Compo).CharCase := ecUpperCase;
//    if Screen.ActiveForm.Components[i] is TListBox then
//       TListBox(Compo).CharCase := ecUpperCase;
  end;
end;

function TUnMyObjects.WinControlSetFocus(WinCtrlName: String): Boolean;
var
  WinCtrl: TComponent;
begin
  Result := False;
  WinCtrl := Screen.ActiveForm.FindComponent(WinCtrlName);
  if WinCtrl <> nil then
  begin
    TWinControl(WinCtrl).SetFocus;
    Result := True;
  end;
end;

function TUnMyObjects.Xls_To_StringGrid(AGrid: TStringGrid;
  AXLSFile: String; PB1: TProgressBar; LaAviso1, LaAviso2: TLabel): Boolean;
const
  xlCellTypeLastCell = $0000000B;
  CharWid = 7.2;
var
  XLApp, Sheet: OLEVariant;
  RangeMatrix: Variant;
  x, y, k, r, Larg, m, n: Integer;
  ColWid: array[0..255] of Integer;
begin
  Screen.Cursor := crHourGlass;
  Informa2(LaAviso1, LaAviso2, True, 'Abrindo arquivo excel ...');
  if PB1 <> nil then
    PB1.Position := 0;
  Result := False;
  try
    XLApp := CreateOleObject('Excel.Application');
  except
    try
      XLApp := CreateOleObject('Works.Application');
    except
      raise;
    end;
  end;
  try
    MyObjects.LimpaGrade(AGrid, 1, 1, True);
    XLApp.Visible := False;
    XLApp.Workbooks.Open(AXLSFile);
    Sheet := XLApp.Workbooks[ExtractFileName(AXLSFile)].WorkSheets[1];
    Sheet.Cells.SpecialCells(xlCellTypeLastCell, EmptyParam).Activate;
    x := XLApp.ActiveCell.Row;
    y := XLApp.ActiveCell.Column;
    AGrid.RowCount := x;
    AGrid.ColCount := y + 1;
    if PB1 <> nil then
    begin
      PB1.Max := x * (y + 1);
      PB1.Update;
    end;
    Informa2(LaAviso1, LaAviso2, True, 'Lendo arquivo excel. Pode demorar se o arquivo conter muitos dados ...');
    RangeMatrix := XLApp.Range['A1', XLApp.Cells.Item[X, Y]].Value;   // >... pode demorar
    k := 1; // 1?
    m := Round(CharWid * 5); // 2012-02-25 de "CharWid * 3" para "CharWid * 5"
    for n := 0 to 255 do ColWid[n] := m;
    repeat
      Informa2(LaAviso1, LaAviso2, True, 'Carregando linha ' + Geral.FF0(k));
      Application.ProcessMessages;  // lendo linha
      AGrid.Cells[0, (k - 1)] := FormatFloat('000', (k - 1));
      for r := 1 to y do
      begin
        if PB1 <> nil then
        begin
          PB1.Position := PB1.Position + 1;
          PB1.Update;
        end;
        Application.ProcessMessages;
        //
        AGrid.Cells[r, (k - 1)] := RangeMatrix[K, R];
        Larg := Round((Length(RangeMatrix[K, R]) + 1) * CharWid);
        if Larg > ColWid[r] then
          ColWid[r] := Larg;
      end;
      Inc(k, 1);
      AGrid.RowCount := k + 2;
    until k > x;
    if PB1 <> nil then
    begin
      PB1.Position := PB1.Max;
      PB1.Update;
    end;
    Informa2(LaAviso1, LaAviso2, True, 'Limpando mem�ria ...');
    RangeMatrix := Unassigned; // Limpando mem�ria
  finally
    Informa2(LaAviso1, LaAviso2, True, 'Fechando arquivo ...');
    if not VarIsEmpty(XLApp) then
    begin
      XLApp.Quit;
      XLAPP := Unassigned;
      Sheet := Unassigned;
      Result := True;
    end;
    Screen.Cursor := crDefault;
  end;
  Informa2(LaAviso1, LaAviso2, True, 'Ajustando largura de colunas ...');
  for n := 0 to AGrid.ColCount -1 do
    AGrid.ColWidths[n] := ColWid[n];
  //
  Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TUnMyObjects.LarguraAutomaticaGrade(AGrid: TStringGrid);
const
  xlCellTypeLastCell = $0000000B;
  CharWid = 7.2;
var
  Col, Row, T, L: Integer;
  ColWid: array[0..255] of Integer;
begin
  T := Round(CharWid * 5); // 2012-02-25 de "CharWid * 3" para "CharWid * 5"
  for Col := 1 to 255 do ColWid[Col] := T;
  for Row := 1 to AGrid.RowCount - 1 do
  begin
    for Col := 1 to 255 do
    begin
      L := Round((Length(AGrid.Cells[Col, Row]) + 1) * CharWid);
      if L > ColWid[Col] then
        ColWid[Col] := L;
    end;
  end;
  for Col := 1 to AGrid.ColCount -1 do
    AGrid.ColWidths[Col] := ColWid[Col];
end;

procedure TUnMyObjects.AjustaFonte(MyLabel: TLabel; Qualidade: Integer);
var
  tagLOGFONT: TLogFont;
  Font: TFont;
begin
  Font := MyLabel.Font;
  GetObject(
    Font.Handle,
    SizeOf(TLogFont),
    @tagLOGFONT);
  //
  tagLOGFONT.lfQuality  := Qualidade;
  (*
  tagLOGFONT.lfQuality  := DEFAULT_QUALITY;
  tagLOGFONT.lfQuality  := DRAFT_QUALITY;
  tagLOGFONT.lfQuality  := PROOF_QUALITY;
  tagLOGFONT.lfQuality  := NONANTIALIASED_QUALITY;
  tagLOGFONT.lfQuality  := ANTIALIASED_QUALITY;
  *)
  MyLabel.Font.Handle := CreateFontIndirect(tagLOGFONT);
end;

procedure TUnMyObjects.AjustaTamanhoMaximoColunaDBGrid(Grid: TDBGrid;
  Campo: String; MinLarg: Integer);
var
  K, I, N: Integer;
  Txt: String;
begin
  Txt := '';
  N := 25 + THackDBGrid(Grid).ColWidths[0]; // 25 = Laterais do grid (4 + 4) + Scrollbar (16) + 1 de folga
  K := 0;
  for I := 0 to Grid.Columns.Count - 1 do
  begin
    if Uppercase(Grid.Columns[I].FieldName) <> Uppercase(Campo) then
      N := N + THackDBGrid(Grid).ColWidths[I+1]
    else
      K := I+1;
  end;
  if K <> 0 then
  begin
    N := Grid.Width - N;
    if N < MinLarg then
      N := MinLarg;
    THackDBGrid(Grid).ColWidths[K] := N;
  end;
end;

function TUnMyObjects.CBAnoECBMesToPeriodoTxt(CBAno, CBMes: TComboBox): String;
var
  Ano, Mes: Word;
begin
  Ano := Geral.IMV(CBAno.Text);
  Mes := CBMes.ItemIndex + 1;
  Result := FormatDateTime('MMMM" de "YYYY', EncodeDate(Ano, Mes, 1));
end;

procedure TUnMyObjects.CBAnoMesFromPeriodo(Periodo: Integer; CBAno,
  CBMes: TComboBox; Avisa: Boolean);
var
  I: Integer;
  Ano, Mes: Word;
  Achou: Boolean;
  Ano_Txt: String;
begin
  if (CBMes <> nil) and (CBAno <> nil) then
  begin
    if Periodo = 0 then
    begin
      CBMes.ItemIndex := -1;
      CBMes.Text := '';
      CBAno.ItemIndex := -1;
      CBAno.Text := '';
    end else begin
      dmkPF.PeriodoDecode(Periodo, Ano, Mes);
      Ano_Txt := FormatFloat('0', Ano);
      CBMes.ItemIndex := Mes -1;
      Achou := False;
      for I := 0 to CBAno.Items.Count - 1 do
        if CBAno.Items[I] = Ano_Txt then
        begin
          CBAno.ItemIndex := I;
          Achou := CBAno.ItemIndex = I;
        end;
      if not Achou and Avisa then
        Geral.MensagemBox('N�o foi poss�vel setar o ano ' + Ano_Txt + #13#10 +
        'no componente "' + CBAno.Name + '"', 'Aviso', MB_OK+MB_ICONWARNING);
    end;
  end;
end;

function TUnMyObjects.CompactaArquivo(Arquivo, PastaOri, PastaDest: String;
Memo: TMemo; ExcluiArqOri: Boolean; PastaDoZip: String): Boolean;
var
  ExecDOS : PChar;
  i, Verificacoes: Integer;
  ArqOri, ArqDes, Options, ZipPath, Origem, Destino: String;
begin
//C:\PROGRA~1\WinZip\wzzip C:\_PARTI~1\Tmp\Cred1100512_113924.zip C:\_Particular\Tmp\Cred1100512_113924.sql
  Result := False;
  try
    Options := '';
    Origem := PastaOri;
    Geral.VerificaDir(Origem, '\', 'Compacta��o de arquivo', True);
    Destino := PastaDest;
    Geral.VerificaDir(Destino, '\', 'Compacta��o de arquivo', True);
    // Zipa arquivos criados no dir A: e C:
    if Destino[1] = 'A' then Options := Options + '&';
    // s� funciona se o arquivo existe!
    //if SobrepoeZip then Options := Options + 'w'; // exclui anterior?
    if Options <> '' then Options := ' -' + Options;
    Verificacoes := 500;
    /////////////////////////////////////////////////////////////////////////////
    // -yp deixa DOS Aberto
    // -& (Multiplos discos)
    /////////////////////////////////////////////////////////////////////////////
    //ZipPath := FmWetBlueMLA_BK.EdZipPath.Text;
    ZipPath := PastaDoZip;
    if not FileExists(ZipPath+'\wzzip.exe') then
    begin
      if Memo <> nil then
        Memo.Lines.Add('O arquivo "'+ZipPath+'\wzzip.exe" n�o foi encontrado!');
      raise EAbort.Create('');
    end else begin
      ZipPath := dmkPF.NomeLongoparaCurto(ZipPath);
      ArqOri  := ExtractShortPathName(Origem) + Arquivo;
      ArqDes  := ExtractShortPathName(Destino) + dmkPF.MudaExtensaoDeArquivo(Arquivo, 'zip');
      //
      ExecDOS := PChar(PChar(ZipPath + '\wzzip' + Options + ' ' + ArqDes + ' ' + ArqOri));
      if Memo <> nil then
      begin
        Memo.Lines.Add('');
        Memo.Lines.Add('*** Linha de comando: ***');
        Memo.Lines.Add(ExecDOS);
        Memo.Lines.Add('*** Fim linha de comando ***');
        Memo.Lines.Add('');
      end;
      (*
      Geral.MensagemBox(ExecDos, 'Exec DOS', MB_OK+MB_ICONWARNING);
      Exit;
      *)
      try
        //WinExec(ExecDOS, SW_SHOW);
        ExecutaCmd(ExecDOS, Memo);
        //dmkPF.WinExecAndWaitOrNot(ExecDOS, SW_SHOW, nil, True);
        // Erro!!!!!!!!!!!
        //MLAGeral.WinExecAndWait32(ExecDOS, SW_HIDE, Memo2);
        //MLAGeral.LeSaidaDOS(ExecDOS, Memo2);
      except
        Memo.Lines.Add('*** Compacta��o falhou! ***');
      end;
    end;
    for i := 1 to Verificacoes do
    begin
      Application.ProcessMessages;
      (*
      if FParar then
      begin
        FParar := False;
        Screen.Cursor := crDefault;
        Memo.Lines.Add('Compacta��o interrompida pelo usu�rio!');
        Break;
      end;
      *)
      Sleep(1000);
      if FileExists(ArqDes) then
      begin
        Result := True;
        if ExcluiArqOri then
        begin
          if FileExists(ArqOri) then
            DeleteFile(ArqOri);
        end;
        Break;
      end;
    end;
(*    ExecDOS := PChar('C:\Dermatek\wzzip C:\Dermatek\Backups\'+ArqNome+
                     ' C:\Dermatek\'+Arquivo);
    WinExec(ExecDOS, SW_HIDE);*)
    /////////////////////////////////////////////////////////////////////////////
  except
    if Memo <> nil then
      Memo.Lines.Add('Erro na compacta��o com Win Zip!');
  end;
end;

function TUnMyObjects.CompoMinusculo(Nome: String): Boolean;
var
  N: String;
begin
  N := Uppercase(Nome);
  Result := False;
  if N = 'EDEMMAIL' then Result := True
  else if N = 'EDEEMMAIL' then Result := True
  else if N = 'EDPEMMAIL' then Result := True
end;

function TUnMyObjects.CompoMoeda(Caption: String): String;
var
  N: String;
  i: Integer;
begin
  if pos(VAR_MOEDA, Caption) = 1 then
    N := Caption
  else begin
    for i := 1 to Length(Caption) do
    begin
      if Caption[i] <> '$' then N := N + Caption[i]
      else N := N + VAR_MOEDA;
    end;
  end;
  Result := N;
end;

procedure TUnMyObjects.ConfiguraAllFmtStr(RGAllFmtStr: TRadioGroup; Colunas,
  Default: Integer);
var
  I: Integer;
begin
  RGAllFmtStr.Columns := Colunas;
  RGAllFmtStr.Items.Clear;
  for I := 0 to Integer(dmktfUnknown) do
    RGAllFmtStr.Items.Add(Geral.ObtemTextoDeAllFormat(I));
  //
  RGAllFmtStr.ItemIndex := Default;
end;

procedure TUnMyObjects.ConfiguraCheckGroup(CheckGroup: TdmkCheckGroup;
  Itens: array of String; Colunas, Default: Integer; FirstAll: Boolean);
var
  I: Integer;
begin
  CheckGroup.Columns := Colunas;
  CheckGroup.Items.Clear;
  for I := Low(Itens) to High(Itens) do
    CheckGroup.Items.Add(Itens[I]);
  //
  CheckGroup.ItemIndex := Default;
  //
  if FirstAll then
    CheckGroup.Items[0] := 'TODOS';
end;

procedure TUnMyObjects.ConfiguraRadioGroup(RadioGroup: TRadioGroup;
  Itens: array of String; Colunas, Default: Integer);
var
  I: Integer;
begin
  RadioGroup.Columns := Colunas;
  RadioGroup.Items.Clear;
  for I := Low(Itens) to High(Itens) do
    RadioGroup.Items.Add(Itens[I]);
  //
  RadioGroup.ItemIndex := Default;
end;

procedure TUnMyObjects.ConfiguraTipoCobranca(RGTipoCobranca: TRadioGroup;
  Colunas, Default: Integer);
begin
  VAR_DROPPED := False;
  RGTipoCobranca.Columns := Colunas;
  RGTipoCobranca.ItemIndex := Default;
  //
  RGTipoCobranca.Items.Clear;
  RGTipoCobranca.Items.Add((*[0] :=*) 'Padr�o');
  RGTipoCobranca.Items.Add((*[1] :=*) 'SIGCB - CEF');
end;

procedure TUnMyObjects.ControleCor(ScreenActiveForm: TForm);
begin
  begin
    try
      if VAR_MUDACORCOMP < 1 then Exit;
      if VAR_ATUALIZANDO then Exit;
      if ScreenActiveForm = nil then exit;
      if ScreenActiveForm.Name = IC2_NOMEFORMAPP then
       exit;
      if ScreenActiveForm.Name = 'FmLogoff' then
       exit;
      if ScreenActiveForm.Name = 'FmLocalBD' then
       exit;
      IC2_COMPATU := ScreenActiveForm.ActiveControl;
(*
      if IC2_COMPATU is T L M D Edit then
      begin
         VAR_T L M D EDITNOME := T L M D Edit(IC2_COMPATU).Name;
         VAR_T L M D EDITNEW := VAR_T L M D EDITOLD;
         VAR_T L M D EDITOLD := T L M D Edit(IC2_COMPATU).Text;
      end;
*)
      //
      if VAR_SEMCONTROLECOR then Exit;
      //
      if IC2_COMPANT = IC2_COMPATU then
      begin
        IC2_COMPFM := ScreenActiveForm.Name;
        Exit;
      end;
      if IC2_COMPFM <> ScreenActiveForm.Name then IC2_COMPANT := nil;
      if IC2_COMPANT <> nil then
      if IC2_COMPANT is TForm then
        if IC2_COMPFM = TForm(IC2_COMPANT).Name then
      begin
        IC2_COMPFM := ScreenActiveForm.Name;
        Exit;
      end;
      if IC2_COMPATU <> nil then
      begin
        if IC2_COMPATU.TabStop = False then
        begin
          IC2_COMPFM := ScreenActiveForm.Name;
          Exit;
        end;
        if TComponent(IC2_COMPATU).Tag = 999 then
        begin
          IC2_COMPFM := ScreenActiveForm.Name;
          Exit;
        end;
(*
        if IC2_COMPATU is T L M D Edit then
        begin
           T L M D Edit(IC2_COMPATU).Color := IC2_AparenciasEditItemIn;
           T L M D Edit(IC2_COMPATU).Font.Color := IC2_AparenciasEditTextIn;
           VAR_T L M D EDITNOME := T L M D Edit(IC2_COMPATU).Name;
           VAR_T L M D EDITNEW := VAR_T L M D EDITOLD;
           VAR_T L M D EDITOLD := T L M D Edit(IC2_COMPATU).Text;
        end;
*)
        //
        if IC2_COMPATU is TEdit then
           TEdit(IC2_COMPATU).Color := IC2_AparenciasEditItemIn;
        if IC2_COMPATU is TEdit then
           TEdit(IC2_COMPATU).Font.Color := IC2_AparenciasEditTextIn;
        //
        if IC2_COMPATU is TDBLookupComboBox then
           TDBLookupComboBox(IC2_COMPATU).Color := IC2_AparenciasEditItemIn;
        //
        if IC2_COMPATU is TDBLookupComboBox then
           TDBLookupComboBox(IC2_COMPATU).Font.Color := IC2_AparenciasEditTextIn;
        //
        if IC2_COMPATU is TListBox then
           TListBox(IC2_COMPATU).Font.Color := IC2_AparenciasEditTextIn;
        //
        if IC2_COMPATU is TMemo then
           TMemo(IC2_COMPATU).Color := IC2_AparenciasEditItemIn;
        if IC2_COMPATU is TMemo then
           TMemo(IC2_COMPATU).Font.Color := IC2_AparenciasEditTextIn;
        //
        if IC2_COMPATU is TStaticText then
           TStaticText(IC2_COMPATU).Color := IC2_AparenciasEditItemIn;
        if IC2_COMPATU is TStaticText then
           TStaticText(IC2_COMPATU).Font.Color := IC2_AparenciasEditTextIn;
      end;
      if IC2_COMPANT <> nil then
      begin
        if IC2_COMPANT is TForm then
        begin
          IC2_COMPFM := ScreenActiveForm.Name;
          Exit;
        end;
        if ScreenActiveForm.Name = '' then
        begin
          IC2_COMPFM := ScreenActiveForm.Name;
          Exit;
        end;
        if TComponent(IC2_COMPANT).Tag = 999 then
        begin
          IC2_COMPFM := ScreenActiveForm.Name;
          Exit;
        end;
(*
        if IC2_COMPANT is T L M D Edit then
        begin
           T L M D Edit(IC2_COMPANT).Color := IC2_AparenciasEditItemOut;
           T L M D Edit(IC2_COMPANT).Font.Color := IC2_AparenciasEditTextOut;
           if VAR_SOMAIUSCULAS then
           T L M D Edit(IC2_COMPANT).Text := ConverteMinuscEmMaiusc(T L M D Edit(IC2_COMPANT).Text);
        end;
*)
        if IC2_COMPANT is TdmkEdit then
        begin
           TdmkEdit(IC2_COMPANT).Color := IC2_AparenciasEditItemOut;
           TdmkEdit(IC2_COMPANT).Font.Color := IC2_AparenciasEditTextOut;
           if VAR_SOMAIUSCULAS then
           TdmkEdit(IC2_COMPANT).Text := dmkPF.ConverteMinuscEmMaiusc(TdmkEdit(IC2_COMPANT).Text);
        end;
        if IC2_COMPANT is TdmkEditCB then
        begin
           TdmkEditCB(IC2_COMPANT).Color := IC2_AparenciasEditItemOut;
           TdmkEditCB(IC2_COMPANT).Font.Color := IC2_AparenciasEditTextOut;
           if VAR_SOMAIUSCULAS then
           TdmkEditCB(IC2_COMPANT).Text := dmkPF.ConverteMinuscEmMaiusc(TdmkEditCB(IC2_COMPANT).Text);
        end;
        if IC2_COMPANT is TEdit then
        begin
           TEdit(IC2_COMPANT).Color := IC2_AparenciasEditItemOut;
           TEdit(IC2_COMPANT).Font.Color := IC2_AparenciasEditTextOut;
           if VAR_SOMAIUSCULAS then
           TEdit(IC2_COMPANT).Text := dmkPF.ConverteMinuscEmMaiusc(TEdit(IC2_COMPANT).Text);
        end;
        if IC2_COMPANT is TDBLookupComboBox then
        begin
           TDBLookupComboBox(IC2_COMPANT).Color := IC2_AparenciasEditItemOut;
           TDBLookupComboBox(IC2_COMPANT).Font.Color := IC2_AparenciasEditTextOut;
        end;
        //
        if IC2_COMPANT is TListBox then
           TListBox(IC2_COMPANT).Font.Color := IC2_AparenciasEditTextOut;
        //
        if IC2_COMPANT is TMemo then
           TMemo(IC2_COMPANT).Color := IC2_AparenciasEditItemOut;
        if IC2_COMPANT is TMemo then
           TMemo(IC2_COMPANT).Font.Color := IC2_AparenciasEditTextOut;
        //
        if IC2_COMPANT is TStaticText then
           TStaticText(IC2_COMPANT).Color := IC2_AparenciasEditItemOut;
        if IC2_COMPANT is TStaticText then
           TStaticText(IC2_COMPANT).Font.Color := IC2_AparenciasEditTextOut;
      end;
      IC2_COMPANT := IC2_COMPATU;
    finally
    end;
  end;
  IC2_COMPFM := ScreenActiveForm.Name;
  //SoMaiusculas(ScreenActiveForm.ActiveControl);  n�o funciona ????
end;

procedure TUnMyObjects.ControleStrings(ScreenActiveForm: TForm);
var
  i, j: Integer;
  Compo: TComponent;
  KeyboardState: TKeyboardState;
begin
  GetKeyBoardState(KeyboardState);
  for i := 0 to ScreenActiveForm.ComponentCount -1 do
  begin
    Compo := ScreenActiveForm.Components[i];
    ////////////////////////////////////////////////////////////////////////////
    if ScreenActiveForm.Components[i] is TDBGrid then
    begin
      for j := 0 to TDBGrid(Compo).Columns.Count-1 do
      begin
        if Pos('$', TDBGrid(Compo).Columns[j].Title.Caption) > 0 then
            TDBGrid(Compo).Columns[j].Title.Caption :=
              CompoMoeda(TDBGrid(Compo).Columns[j].Title.Caption);
      end;
    end;
    ////////////////////////////////////////////////////////////////////////////
    if ScreenActiveForm.Components[i] is TLabel then
      if TLabel(Compo).Visible then
        if Pos('$', TLabel(Compo).Caption) > 0 then
          TLabel(Compo).Caption := CompoMoeda(TLabel(Compo).Caption);
    ////////////////////////////////////////////////////////////////////////////
    if ScreenActiveForm.Components[i] is TGroupBox then
      if TGroupBox(Compo).Visible then
        if Pos('$', TGroupBox(Compo).Caption) > 0 then
          TGroupBox(Compo).Caption := CompoMoeda(TGroupBox(Compo).Caption);
    ////////////////////////////////////////////////////////////////////////////
    if VAR_SOMAIUSCULAS and (not VAR_MAIUSC_MINUSC) then
    begin
      if GetKeyState(VK_CAPITAL) = 0 then
      KeyBoardState[VK_CAPITAL] := 1;
      ////////////////////////////////////////////////////////////////////////////
      if ScreenActiveForm.Components[i] is TEdit then
        if not CompoMinusculo(TEdit(Compo).Name) then
          TEdit(Compo).CharCase := ecUpperCase;
      ////////////////////////////////////////////////////////////////////////////
      if ScreenActiveForm.Components[i] is TDBEdit then
        if not CompoMinusculo(TDBEdit(Compo).Name) then
          TDBEdit(Compo).CharCase := ecUpperCase;
      ////////////////////////////////////////////////////////////////////////////
      if ScreenActiveForm.Components[i] is TdmkEdit then
        if not CompoMinusculo(TdmkEdit(Compo).Name) then
          TdmkEdit(Compo).CharCase := ecUpperCase;
      ////////////////////////////////////////////////////////////////////////////
      if ScreenActiveForm.Components[i] is TdmkDBEdit then
        if not CompoMinusculo(TdmkDBEdit(Compo).Name) then
          TdmkDBEdit(Compo).CharCase := ecUpperCase;
      ////////////////////////////////////////////////////////////////////////////
    end else KeyBoardState[VK_CAPITAL] := KeyBoardState[VK_CAPITAL] and not 1;
  end;
end;

procedure TUnMyObjects.CopiaItensDeMenu(PMGeral: TPopupMenu; Form: TForm);
  procedure ItensDeMenu(Item, Dest: TMenuItem);
  var
    I: Integer;
    Novo: TMenuItem;
  begin
    Novo := TMenuItem.Create(Form);
    Novo.Name := 'Item_Menu_' + Item.Name;
    Novo.Caption := Item.Caption;
    Novo.OnClick := Item.OnClick;
    try
      Dest.Add(Novo);
    except
      Geral.MensagemBox('ERRO!' + #13#10 + Novo.Caption + #13#10 +
      Dest.Caption, 'Mensagem', MB_OK+MB_ICONINFORMATION);
    end;
    // Sub-itens
    for I := 0 to Item.Count - 1 do
    begin
      ItensDeMenu(Item.Items[I], Novo);
    end;
  end;
  function TiraQuebras(Texto: String): String;
  var
    I: Integer;
  begin
    Result := '';
    for I := 1 to Length(Texto) do
    begin
      case Ord(Texto[I]) of
        10: Result := Result + ' ';
        13: ;//nada
        else Result := Result + Texto[I];
      end;
    end;
    Result := Trim(Result);
  end;
var
  I, J, K, L, Codigo, Controle, Conta: Integer;
  Compo, Page, TBar: TComponent;
  //
  NomePage, NomeTBar, NomeButn: String;
  //
  MenuItem, Dest, MItem: TMenuItem;
  Achou: Boolean;
  DropDownMenu: TPopupMenu;
begin
  //Seq := 0;
  Conta := 0;
  with Form do
  begin
    for i := 0 to ComponentCount -1 do
    begin
      Compo := Components[i];
      if Compo is TAdvPage then
      begin
        Codigo := TAdvPage(Compo).PageIndex + 1;
        NomePage := 'IPM_AdvPage_' + FormatFloat('0', Codigo);
        //
        Dest := TMenuItem.Create(Form);
        Dest.Name := NomePage;
        Dest.Caption := TiraQuebras(TAdvPage(Compo).Caption);
        PMGeral.Items.Add(Dest);
      end;
    end;
    for i := 0 to ComponentCount -1 do
    begin
      Compo := Components[i];
      if Compo is TAdvToolBar then
      begin
        Page := Compo.GetParentComponent;
        if (Page <> nil) and (Page is TAdvPage) then
        begin
          Codigo := TAdvPage(Page).PageIndex + 1;
          NomePage := 'IPM_AdvPage_' + FormatFloat('0', Codigo);
          Achou := False;
          for J := 0 to PMGeral.Items.Count - 1 do
          begin
            if PMGeral.Items[J].Name = NomePage then
            begin
              Achou := True;
              Controle := TAdvToolBar(Compo).ToolBarIndex + 1;
              NomeTBar := 'IPM_AdvTBar_' + FormatFloat('0', Codigo) + '_' + FormatFloat('0', Controle);
              //
              Dest := TMenuItem.Create(Form);
              Dest.Name := NomeTBar;
              Dest.Caption := TiraQuebras((TAdvToolBar(Compo).Caption));
              MenuItem := PMGeral.Items[J];
              MenuItem.Add(Dest);
            end;
          end;
          if not Achou then
            ShowMessage('N�o achei o AdvPage: ' + NomePage);
        end;
      end;
    end;
    for i := 0 to ComponentCount -1 do
    begin
      Compo := Components[i];
      if Compo is TAdvCustomGlowButton then
      begin
        TBar := Compo.GetParentComponent;
        if (TBar <> nil) and (TBar is TAdvToolBar) then
        begin
          Page := TBar.GetParentComponent;
          if (Page <> nil) and (Page is TAdvPage) then
          begin
            Codigo := TAdvPage(Page).PageIndex + 1;
            NomePage := 'IPM_AdvPage_' + FormatFloat('0', Codigo);
            Controle := TAdvToolBar(TBar).ToolBarIndex + 1;
            NomeTBar := 'IPM_AdvTBar_' + FormatFloat('0', Codigo) + '_' + FormatFloat('0', Controle);
            //
            for J := 0 to PMGeral.Items.Count - 1 do
            begin
              if PMGeral.Items[J].Name = NomePage then
              begin
                MItem := PMGeral.Items[J];
                Achou := False;
                for K := 0 to MItem.Count - 1 do
                begin
                  if MItem.Items[K].Name = NomeTBar then
                  begin
                    Achou := True;
                    Conta := Conta + 1;
                    NomeButn := 'IPM_AdvButn_' + FormatFloat('0', Codigo) + '_' +
                      FormatFloat('0', Controle) + '_' + FormatFloat('0', Conta);
                    //
                    if (TAdvCustomGlowButton(Compo).Enabled) and
                      (TAdvCustomGlowButton(Compo).Visible) then
                    begin
                      Dest := TMenuItem.Create(Form);
                      Dest.Name := NomeButn;
                      Dest.Caption := TiraQuebras((TAdvCustomGlowButton(Compo).Caption));
                      Dest.OnClick := TAdvCustomGlowButton(Compo).OnClick;
                      MenuItem := MItem.Items[K];
                      try
                        MenuItem.Add(Dest);
                      except
                        Geral.MensagemBox('ERRO!' + #13#10 + NomeButn + #13#10 +
                        Dest.Caption, 'Mensagem', MB_OK+MB_ICONINFORMATION);
                      end;
                      // 2012-09-23
                      if GetPropInfo(TAdvCustomGlowButton(Compo), 'DropDownMenu') <> nil then
                      begin
                        DropDownMenu := TAdvGlowButton(Compo).DropDownMenu;
                        if DropDownMenu <> nil then
                        begin
                          for L := 0 to DropDownMenu.Items.Count - 1 do
                          begin
                            ItensDeMenu(DropDownMenu.Items[L], Dest);
                          end;
                        end;
                      end;
                      // Fim 2012-09-23
                    end;
                  end;
                end;
                if not Achou then
                  ShowMessage('N�o achei o AdvToolBar: ' + NomeTbar);
              end;
            end;
          end;
        end;
      end;
    end;
  end;
end;

function TUnMyObjects.CorIniComponente(Handle: THandle): Boolean;
var
  H, W : Integer;
  Form: TForm;
begin
  H := 0;
  Result := False;
  //Form := nil;
  //
  if Handle <> 0 then
  begin
    Form := TForm(Handle);
    if Form = nil then
      Exit;
  end else
  begin
    Form := Screen.ActiveForm;
    if Form = nil then
      Exit;
      H := Form.Height;
      W := Form.Width;

      if H > 600 then H := 600;
      if W > 800 then W := 800;
      Form.Constraints.MinHeight := H;
      Form.Constraints.MinWidth  := W;
      if Form.WindowState = wsMaximized then
        Form.Top := 0
      else
      begin
        //Form.Height := 0;
      //Form.Width := 0;
    end;
  end;
  // 2012-11-05 - Child form
  if Form <> nil then
  begin
    Form.Font.Name := 'Tahoma';
    if (H > 530) and (Screen.Height <= 600) then Form.Top := 0;
    //
    FmMyGlyfs.DefineGlyfs(Form);
    //
    //DefineCompoCor;
    //
    ControleCor(Form);
    //
    ControleStrings(Form);
    //
    // 2011-09-21
    FmPrincipal.ReCaptionComponentesDeForm(Form);
    //
    //
  end;
  Result := True;
end;


function TUnMyObjects.CriaForm_AcessoTotal(InstanceClass: TComponentClass;
  var Reference): Boolean;
var
  Instance: TComponent;
  Form: String;
  MyCursor: TCursor;
  Campo: String;
begin
  Result := False;
  Form := InstanceClass.ClassName;
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    Instance := TComponent(InstanceClass.NewInstance);
    TComponent(Reference) := Instance;
    try
      Instance.Create(Application);
      Result := True;
    except
      TComponent(Reference) := nil;
      raise;
    end;
    with TForm(Reference) do
    begin
      Campo := 'ImgTipo';
      if (FindComponent(Campo) as TdmkImage) <> nil then
        TdmkImage(FindComponent(Campo) as TImage).SQLType := stLok;
    end;
    Screen.Cursor := MyCursor;
  finally
    Screen.Cursor := MyCursor;
  end;
end;

function TUnMyObjects.ArquivoExiste(Caminho: String; MemoOut: TMemo;
DoLocalize: Boolean): Boolean;
var
  I: Integer;
  Achou: Boolean;
begin
  if DoLocalize then
    Result := FileExists(Caminho)
  else
    Result := False;  
  //
  if not Result then
  begin
    Achou := False;
    for I := 0 to MemoOut.Lines.Count -1 do
    begin
      Achou := MemoOut.Lines[I] = Caminho;
      if Achou then
        Break;
    end;
    if not Achou then
      MemoOut.Text := Caminho + #13#10 + MemoOut.Text;
  end;
end;

procedure TUnMyObjects.CarregaImagemEmTImage(Image: TImage; Arquivo: String;
  Bitmap: TBitmap; BrushColor: TColor);
  function LoadGraphicsFile(const Filename: string): TBitmap;
  var
    Picture: TPicture;
  begin
    Result := nil;
    //
    if FileExists(Filename) then
    begin
      Result := TBitmap.Create;
      try
        Picture := TPicture.Create;
        try
          Picture.LoadFromFile(Filename);
          // Try converting picture to bitmap
          try
            Result.Assign(Picture.Graphic);
          except
            // Picture didn't support conversion to TBitmap.
            // Draw picture on bitmap instead.
            Result.Width  := Picture.Graphic.Width;
            Result.Height := Picture.Graphic.Height;
            Result.PixelFormat := pf24bit;
            Result.Canvas.Draw(0, 0, Picture.Graphic);
          end
        finally
          Picture.Free
        end
      except
        RESULT.Free;
        raise
      end
    end
  end;
begin
  if FileExists(Arquivo) then
  begin
    Bitmap.Free;  // get rid of old bitmap
    Bitmap := LoadGraphicsFile(Arquivo);
    DisplayBitmap(Bitmap, Image, BrushColor);
  end;
end;

function TUnMyObjects.CBAnoECBMesToDate(CBAno, CBMes: TComboBox;
Dia, IncMes: Integer): TDateTime;
var
  Ano, Mes: Word;
begin
  Ano := Geral.IMV(CBAno.Text);
  Mes := CBMes.ItemIndex + 1;
  Result := IncMonth(EncodeDate(Ano, Mes, Dia), incMes);
end;

procedure TUnMyObjects.DBGridSelectAll(AGrid: TDBGrid);
begin
  AGrid.SelectedRows.Clear;
  with AGrid.DataSource.DataSet do
  begin
    DisableControls;
    First;
    try
      while not EOF do
      begin
        AGrid.SelectedRows.CurrentRowSelected := True;
        Next;
      end;
    finally
      EnableControls;
    end;
  end;
end;

procedure TUnMyObjects.DBGridLarguraSegundoTitulo(DBGTitul, DBGDados: TDBGrid;
Colunas: array of Integer);
var
  I, J, T, N, K: Integer;
begin
  K := 0;
  for I := Low(Colunas) to High(Colunas) do
  begin
    T := 0;
    for J := 1 to Colunas[I] do
    begin
      K := K + 1;
      try
        N := THackDBGrid(DBGDados).ColWidths[K];
        T := T + N;
      except
        Geral.MensagemBox(PChar('N�o foi poss�vel obter a largura da' +
        'coluna ' + IntToStr(K+1) + ' da grade "' + DBGDados.Name + '"!'),
        'ERRO', MB_OK+MB_ICONERROR);
      end;  
    end;
    N := Colunas[I];
    THackDBGrid(DBGTitul).ColWidths[I + 1] := T + N -1;
  end;
end;

function TUnMyObjects.PreencheCBAnoECBMes(CBAno, CBMes: TComboBox;
IncremMes: Integer = 1; Nulo: Boolean = False): String;
var
  Data: TDateTime;
  Ano, Mes, Dia: Word;
  I: Integer;
begin
  with CBMes.Items do
  begin
    Add(FIN_JANEIRO);
    Add(FIN_FEVEREIRO);
    Add(FIN_MARCO);
    Add(FIN_ABRIL);
    Add(FIN_MAIO);
    Add(FIN_JUNHO);
    Add(FIN_JULHO);
    Add(FIN_AGOSTO);
    Add(FIN_SETEMBRO);
    Add(FIN_OUTUBRO);
    Add(FIN_NOVEMBRO);
    Add(FIN_DEZEMBRO);
  end;
  if Nulo then
  begin
    CBAno.ItemIndex := - 1;
    CBMes.ItemIndex := - 1;
  end else begin
    Data := IncMonth(Date, IncremMes);
    DecodeDate(Data, Ano, Mes, Dia);
    with CBAno.Items do
      for I := 2000 to Ano + 100 do Add(IntToStr(I));
    CBAno.ItemIndex := Ano - 2000;
    CBMes.ItemIndex := (Mes - 1);
  end;
end;

function TUnMyObjects.DefineIndexComboBox(CBx: TComboBox; Valor: String): Boolean;
var
  I: Integer;
begin
  Result := False;
  for I := 0 to CBx.Items.Count - 1 do
  begin
    if CBx.Items[I] = Valor then
    begin
      CBx.ItemIndex := I;
      Result := True;
      Break;
    end;
  end;
  if not Result then Geral.MensagemBox('N�o foi poss�vel setar o valor "' +
  Valor + '" para o ComboBox "' + CBx.Name + '".', 'Aviso', MB_OK+MB_ICONWARNING);
end;

procedure TUnMyObjects.PulaCelulaGradeInput(GradePula, GradeCodi: TStringGrid;
var Key: Word; Shift: TShiftState);
  function PulaCelula(): Boolean;
  var
    Col, Row: Integer;
  begin
    //Result := False;
    Col := GradePula.Col + 1;
    if Col > GradePula.ColCount -1 then
    begin
      Col := 1;
      Row := GradePula.Row + 1;
    end else Row := GradePula.Row;
    try
      GradePula.Col := Col;
      GradePula.Row := Row;
      GradePula.Invalidate;
      Result := True;
    finally
      ;
    end;
  end;
var
  //Col, Row: Integer;
  SemErro: Boolean;
begin
  if VAR_VK_13_PRESSED or (Key = VK_RETURN) then
  begin
    if ((GradePula.ColCount = 1) and (GradePula.RowCount = 1))
    or ((GradePula.Col = GradePula.ColCount -1)
    and (GradePula.Row = GradePula.RowCount -1)) then Exit;
    Key := 0;
    //
    PulaCelula;
    while (GradeCodi.Cells[GradePula.Col, GradePula.Row] = '')
    and ((GradePula.Col < GradePula.ColCount -1)
    or (GradePula.Row < GradePula.RowCount -1)) do
    begin
      SemErro := PulaCelula();
      if not SemErro then
        Exit;
    end;
  end;
end;

procedure TUnMyObjects.PulaCelulaGradeInputEx(GradePula, GradeCodi: TStringGrid;
var Key: Word; Shift: TShiftState; BtOK: TBitBtn);
  function PulaCelula(): Boolean;
  var
    Col, Row: Integer;
  begin
    //Result := False;
    Col := GradePula.Col + 1;
    if Col > GradePula.ColCount -1 then
    begin
      Col := 1;
      Row := GradePula.Row + 1;
    end else Row := GradePula.Row;
    try
      GradePula.Col := Col;
      GradePula.Row := Row;
      GradePula.Invalidate;
      Result := True;
    finally
      ;
    end;
  end;
var
  //Col, Row: Integer;
  SemErro: Boolean;
begin
  if VAR_VK_13_PRESSED or (Key = VK_RETURN) then
  begin
    if ((GradePula.ColCount = 1) and (GradePula.RowCount = 1))
    or ((GradePula.Col = GradePula.ColCount -1)
    and (GradePula.Row = GradePula.RowCount -1)) then
    begin
      if ((GradePula.Col = GradePula.ColCount -1)
      and (GradePula.Row = GradePula.RowCount -1)) then
      begin
        if BtOk <> nil then
          BtOk.SetFocus;
      end;
      Exit;
    end;
    Key := 0;
    //
    PulaCelula;
    while (GradeCodi.Cells[GradePula.Col, GradePula.Row] = '')
    and ((GradePula.Col < GradePula.ColCount -1)
    or (GradePula.Row < GradePula.RowCount -1)) do
    begin
      SemErro := PulaCelula();
      if not SemErro then
        Exit;
    end;
  end;
end;

procedure TUnMyObjects.DesenhaGradeGeral(Grade: TStringGrid; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState; Alinhamentos: array of TAlignment;
  AllColorFixo: Boolean);
var
  Cor: TColor;
  //N: integer;
  A: TAlignment;
begin
  if (ACol < Grade.FixedCols) or (ARow < Grade.FixedRows) then
    Cor := FmPrincipal.sd1.Colors[csScrollbar]
  else if AllColorFixo then
    Cor := FmPrincipal.sd1.Colors[csButtonFace]
  else
    Cor := clWindow;

  //

  if High(Alinhamentos) <> Grade.ColCount -1 then Geral.MensagemBox(
  'Quantidade de alinhamentos incorreto no desenho de grade!', 'Erro',
  MB_OK+MB_ICONWARNING);

  //

  A := taLeftJustify;
  if High(Alinhamentos) <= Grade.ColCount then
   A := Alinhamentos[ACol];

  DesenhaTextoEmStringGrid(Grade, Rect, clBlack, Cor, A,
    Grade.Cells[Acol, ARow], Grade.FixedCols, Grade.FixedRows, False)
end;

procedure TUnMyObjects.DesenhaGradeNumeros(Grade: TStringGrid; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState; AllColorFixo: Boolean);
var
  Cor: TColor;
  //N: integer;
  A: TAlignment;
begin
  if (ACol < Grade.FixedCols) or (ARow < Grade.FixedRows) then
    A := taLeftJustify
  else
    A := taRightJustify;

  //

  if (ACol < Grade.FixedCols) or (ARow < Grade.FixedRows) then
    Cor := FmPrincipal.sd1.Colors[csScrollbar]
  else if AllColorFixo then
    Cor := FmPrincipal.sd1.Colors[csButtonFace]
  else
    Cor := clWindow;

  //

  DesenhaTextoEmStringGrid(Grade, Rect, clBlack, Cor, A,
    Grade.Cells[Acol, ARow], Grade.FixedCols, Grade.FixedRows, False)
end;

procedure TUnMyObjects.DesenhaGradeA(GradeA: TStringGrid; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState; AllColorFixo: Boolean);
  procedure DrawGrid(Grade: TStringGrid; const Rect: TRect;
  FixedCols, FixedRows, Valor: Integer; CorFundo: TColor);
  var
    W1,H1,Glyph: Integer;
    Pos: MyArrayI02;
    Bmp: TBitmap;
  begin
    if Valor < 0 then Valor := 0;
    if Valor > 2 then Valor := 2;
    with Grade.Canvas do
    begin
      Brush.Color := CorFundo;
      FillRect(Rect);
      W1 := FmMyGlyfs.ListaCheck3.Width;
      H1 := FmMyGlyfs.ListaCheck3.Height;
      Pos := dmkPF.CoordenadasIniciais(Rect.Left,Rect.Top,Rect.Right,Rect.Bottom,W1,H1);
      //Brush.Color := FmPrincipal.sd1.colors[csButtonFace];
      //Font.Color  := FmPrincipal.sd1.colors[csButtonFace];
      FillRect(Rect);
      //TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
      Glyph := Valor;
      Bmp := TBitmap.Create;
      try
        Bmp.Width := W1;
        Bmp.Height := H1;
        FmMyGlyfs.ListaChecks.GetBitmap(Glyph, Bmp);
        Draw(Pos[1], Pos[2], Bmp);
      finally
        Bmp.Free;
      end;
    end;
    GradeA.FixedCols := FixedCols;
    GradeA.FixedRows := FixedRows;
  end;
var
  Ativo: Integer;
  Cor: TColor;
begin
  if (ACol = 0) or (ARow = 0) then
  begin
    with GradeA.Canvas do
    begin
      if ACol = 0 then
        DesenhaTextoEmStringGrid(GradeA, Rect, clBlack,
          FmPrincipal.sd1.Colors[csScrollbar], taLeftJustify,
          GradeA.Cells[Acol, ARow], 0, 0, False)
      else
        DesenhaTextoEmStringGrid(GradeA, Rect, clBlack,
          FmPrincipal.sd1.Colors[csScrollbar], taCenter,
          GradeA.Cells[Acol, ARow], 0, 0, False);
    end;
  end else begin
    if GradeA.Cells[Acol, ARow] = '' then
      Ativo := 0
    else
      Ativo := Geral.IMV(GradeA.Cells[ACol, ARow]) + 1;
    //
    if AllColorFixo then
      Cor := FmPrincipal.sd1.Colors[csButtonFace]
    else
      Cor := clWindow;
    DrawGrid(GradeA, Rect, 0, 0, Ativo, Cor);
  end;
end;

procedure TUnMyObjects.DesenhaGradeN(GradeDesenhar, GradeAtivos, GradeCompara:
              TStringGrid; ACol, ARow: Integer;Rect: TRect; State:
              TGridDrawState; Fmt: String;
              FixedCols, FixedRows: Integer; PermiteVazio: Boolean);
var
  CorCel, CorTxt: Integer;
  Texto: String;
  Valor, Origi: Double;
begin
  CorTxt := clGray;
  if (ACol = 0) or (ARow = 0) then
    CorCel := FmPrincipal.sd1.Colors[csScrollbar]//csButtonFace]
  else begin
    CorCel := FmPrincipal.SD1.Colors[csButtonFace];
    Valor := Geral.DMV(GradeDesenhar.Cells[ACol, ARow]);
    if PermiteVazio and (Valor = 0) then  Texto := '' else
    begin
      Texto := FormatFloat(Fmt(*Dmod.FStrFmtPrc*), Valor);
      CorTxt := FmPrincipal.sd1.Colors[csText];
      CorCel := clWindow;
      if GradeAtivos <> nil then
      begin
        if GradeAtivos.Cells[ACol, ARow] <> '' then
        begin
          if GradeCompara <> nil then
          begin
            Origi := Geral.DMV(GradeCompara.Cells[ACol, ARow]);
            if Origi > Valor then
              CorTxt := clRed
            else if Origi < Valor then
              CorTxt := clBlue
          end;
        end else begin
          if  (GradeDesenhar.Cells[ACol,ARow] <> '')
          then  GradeDesenhar.Cells[ACol,ARow] := '';
          Texto := '';
          CorCel := clMenu;
        end;
      end;
    end;
  end;
  //

  if (ACol = 0) or (ARow = 0) then
    DesenhaTextoEmStringGrid(GradeDesenhar, Rect, clBlack,
      CorCel(*FmPrincipal.sd1.Colors[csButtonFace]*), taLeftJustify,
      GradeDesenhar.Cells[Acol, ARow], FixedCols, FixedRows, False)
  else
    DesenhaTextoEmStringGrid(GradeDesenhar, Rect, CorTxt,
      CorCel(*FmPrincipal.sd1.Colors[csButtonFace]*), taRightJustify,
      (*GradeF.Cells[AcolARow]*)Texto, FixedCols, FixedRows, False);
end;

procedure TUnMyObjects.DesenhaTextoEmDBGrid(Grade: TDBGrid; Rect: TRect;
  CorTexto, CorFundo: TColor; Alinha: TAlignment; Texto: String);
var
  //FuRect,
  MyRect: TRect;
  OldAlign: Integer;
  LE, RI, CE, L, T, R, B: Integer;
begin
  LE := Rect.Left+2;
  RI := Rect.Right-2;
  CE := (Rect.Right + Rect.Left) div 2;
  MyRect.Left   := Rect.Left+2;
  MyRect.Top    := Rect.Top+2;
  MyRect.Right  := Rect.Right-2;
  MyRect.Bottom := Rect.Bottom;
  L := Rect.Right;
  T := Rect.Top;
  R := Rect.Right;
  B := Rect.Bottom;
  with Grade.Canvas do
  begin
    Brush.Color := CorFundo;
    FillRect(Rect);
    Font.Color := CorTexto;
    case Alinha of
      taRightJustify: OldAlign := SetTextAlign(Handle, TA_RIGHT);
      taLeftJustify:  OldAlign := SetTextAlign(Handle, TA_LEFT);
      taCenter:       OldAlign := SetTextAlign(Handle, TA_CENTER);
      else            OldAlign := SetTextAlign(Handle, TA_LEFT);
    end;
    FillRect(MyRect);
    case Alinha of
      taRightJustify: TextOut(RI, MyRect.Top, Texto);
      taLeftJustify:  TextOut(LE, MyRect.Top, Texto);
      taCenter:       TextOut(CE, MyRect.Top, Texto);
      else            TextOut(LE, MyRect.Top, Texto);
    end;
    SetTextAlign(Handle, OldAlign);
    Polygon([Point(L, T), Point(L, B), Point(R, B), Point(R, T)]);
  end;
  THackDBGrid(Grade).FixedCols := 1;
end;

procedure TUnMyObjects.DesenhaTextoEmStringGrid(Grade: TStringGrid; Rect: TRect;
  CorTexto, CorFundo: TColor; Alinha: TAlignment; Texto: String; FixedCols,
  FixedRows: Integer; DesenhaSelecionado: Boolean);
var
  //FuRect,
  MyRect: TRect;
  OldAlign: Integer;
  LE, RI, CE, L, T, R, B: Integer;
begin
  LE := Rect.Left+2;
  RI := Rect.Right-2;
  CE := (Rect.Right + Rect.Left) div 2;
  MyRect.Left   := Rect.Left+2;
  MyRect.Top    := Rect.Top+2;
  MyRect.Right  := Rect.Right-2;
  MyRect.Bottom := Rect.Bottom;
  L := Rect.Right;
  T := Rect.Top;
  R := Rect.Right;
  B := Rect.Bottom;
  with Grade.Canvas do
  begin
    Brush.Color := CorFundo;
    FillRect(Rect);
    Font.Color := CorTexto;
    //Font.Name  := 'Arial';
    case Alinha of
      taRightJustify: OldAlign := SetTextAlign(Handle, TA_RIGHT);
      taLeftJustify:  OldAlign := SetTextAlign(Handle, TA_LEFT);
      taCenter:       OldAlign := SetTextAlign(Handle, TA_CENTER);
      else            OldAlign := SetTextAlign(Handle, TA_LEFT);
    end;
    FillRect(MyRect);
    case Alinha of
      taRightJustify: TextOut(RI, MyRect.Top, Texto);
      taLeftJustify:  TextOut(LE, MyRect.Top, Texto);
      taCenter:       TextOut(CE, MyRect.Top, Texto);
      else            TextOut(LE, MyRect.Top, Texto);
    end;
    SetTextAlign(Handle, OldAlign);
    //if DesenhaSelecionado then  (Nada a ver; ver outra coisa)
    Polygon([Point(L, T), Point(L, B), Point(R, B), Point(R, T)]);
  end;
  //
  if FixedCols < THackDBGrid(Grade).ColCount then
    THackDBGrid(Grade).FixedCols := FixedCols
  else
    THackDBGrid(Grade).FixedCols := 0;
  //
  if FixedRows < THackDBGrid(Grade).RowCount then
    THackDBGrid(Grade).FixedRows := FixedRows
  else
    THackDBGrid(Grade).FixedRows := 0;
end;

procedure TUnMyObjects.DesenhaTextoEmStringGridEx(Grade: TStringGrid;
  Rect: TRect; CorTexto, CorFundo: TColor; Alinha: TAlignment; Texto: String;
  FixedCols, FixedRows: Integer; DesenhaSelecionado: Boolean; FonteNome: String;
  FonteSize: Integer; FonteStyle: TFontStyles; PasswordChar: Char);
var
  //FuRect,
  MyRect: TRect;
  OldAlign: Integer;
  LE, RI, CE, L, T, R, B: Integer;
begin
  LE := Rect.Left+2;
  RI := Rect.Right-2;
  CE := (Rect.Right + Rect.Left) div 2;
  MyRect.Left   := Rect.Left+2;
  MyRect.Top    := Rect.Top+2;
  MyRect.Right  := Rect.Right-2;
  MyRect.Bottom := Rect.Bottom;
  L := Rect.Right;
  T := Rect.Top;
  R := Rect.Right;
  B := Rect.Bottom;
  with Grade.Canvas do
  begin
    Brush.Color := CorFundo;
    FillRect(Rect);
    Font.Color  := CorTexto;
    Font.Name   := FonteNome;
    Font.Size   := FonteSize;
    Font.Style  := FonteStyle;
    if PasswordChar <> '' then
    //:= 'l'
    ;
    case Alinha of
      taRightJustify: OldAlign := SetTextAlign(Handle, TA_RIGHT);
      taLeftJustify:  OldAlign := SetTextAlign(Handle, TA_LEFT);
      taCenter:       OldAlign := SetTextAlign(Handle, TA_CENTER);
      else            OldAlign := SetTextAlign(Handle, TA_LEFT);
    end;
    FillRect(MyRect);
    case Alinha of
      taRightJustify: TextOut(RI, MyRect.Top, Texto);
      taLeftJustify:  TextOut(LE, MyRect.Top, Texto);
      taCenter:       TextOut(CE, MyRect.Top, Texto);
      else            TextOut(LE, MyRect.Top, Texto);
    end;
    SetTextAlign(Handle, OldAlign);
    //if DesenhaSelecionado then  (Nada a ver; ver outra coisa)
    Polygon([Point(L, T), Point(L, B), Point(R, B), Point(R, T)]);
  end;
  THackDBGrid(Grade).FixedCols := FixedCols;
  THackDBGrid(Grade).FixedRows := FixedRows;
end;

procedure TUnMyObjects.DisplayBitmap(const Bitmap: TBitmap;
  const Image: TImage; BrushColor: TColor);
var
  Half      :  Integer;
  Height    :  Integer;
  NewBitmap :  TBitmap;
  TargetArea:  TRect;
  Width     :  Integer;
begin
  if Bitmap = nil then
    Exit;
  //
  NewBitmap := TBitmap.Create;
  try
    NewBitmap.Width  := Image.Width;
    NewBitmap.Height := Image.Height;
    NewBitmap.PixelFormat := pf24bit;

    NewBitmap.Canvas.Brush.Style := bsSolid;
    NewBitmap.Canvas.Brush.Color := BrushColor;
    NewBitmap.Canvas.FillRect(NewBitmap.Canvas.ClipRect);

    // "equality" (=) case can go either way in this comparison

    if Bitmap.Width / Bitmap.Height  <  Image.Width / Image.Height then
    begin
      // Stretch Height to match.
      TargetArea.Top    := 0;
      TargetArea.Bottom := NewBitmap.Height;
      // Adjust and center Width.
      Width := MulDiv(NewBitmap.Height, Bitmap.Width, Bitmap.Height);
      Half := (NewBitmap.Width - Width) div 2;
      //
      TargetArea.Left  := Half;
      TargetArea.Right := TargetArea.Left + Width;
    end else
    begin
      // Stretch Width to match.
      TargetArea.Left    := 0;
      TargetArea.Right   := NewBitmap.Width;
      // Adjust and center Height.
      Height := MulDiv(NewBitmap.Width, Bitmap.Height, Bitmap.Width);
      Half := (NewBitmap.Height - Height) div 2;
      //
      TargetArea.Top    := Half;
      TargetArea.Bottom := TargetArea.Top + Height
    end;
    //
    NewBitmap.Canvas.StretchDraw(TargetArea, Bitmap);
    Image.Picture.Graphic := NewBitmap
  finally
    NewBitmap.Free
  end
end;

function TUnMyObjects.FIC(FaltaInfoCompo: Boolean; Compo: TWinControl;
  Mensagem: String; ExibeMsg: Boolean = True): Boolean;
begin
  if FaltaInfoCompo then
  begin
    Screen.Cursor := crDefault;
    Result := True;
    if (Mensagem <> '') and ExibeMsg then
      Geral.MensagemBox(Mensagem, 'Aviso', MB_OK+MB_ICONINFORMATION);
    if Compo <> nil then
    begin
      if Compo.Visible then
      try
        Compo.SetFocus;
      except
        ;
      end;
    end;
  end else Result := False;
end;

function TUnMyObjects.FIC_Edit_Int(const Edit: TdmkEdit; const Mensagem: String;
  var Valor: Integer): Boolean;
begin
  Valor := Edit.ValueVariant;
  Result := FIC(Valor = 0, Edit, Mensagem);
end;

function TUnMyObjects.FIC_Edit_Flu(const Edit: TdmkEdit; const Mensagem: String;
  var Valor: Double): Boolean;
begin
  Valor := Edit.ValueVariant;
  Result := FIC(Valor = 0, Edit, Mensagem);
end;
}

(*
function TUnMyObjects.FileOpenDialog(Form: TForm; const IniDir, Arquivo, Titulo, Filtro:
String; Opcoes: TOpenOptions; var Resultado: String): Boolean;
const
  TxtPadraoDir = 'Sele��o de Pastas.';
var
  VerWin: MyArrayI03;
  OpenDialog: TOpenDialog;
{$WARN UNIT_PLATFORM OFF}
{$WARN SYMBOL_PLATFORM OFF}
  FileOpenDialog: TFileOpenDialog;
{$WARN UNIT_PLATFORM ON}
{$WARN SYMBOL_PLATFORM ON}
  Arq, NovoFiltro, Item, Tipo, Extensao: String;
begin
  Arq := Arquivo;
  Result := false;
  Resultado := '';
  VerWin := dmkPF.VersaoWindows;
  if VerWin[1] < 6 then
  begin
    OpenDialog := TOpenDialog.Create(Form);
    try
      OpenDialog.InitialDir := IniDir;
      OpenDialog.Title      := Titulo;//'Abrir e editar arquivo Excel - Empresa';
      OpenDialog.Filter     := Filtro;//'Arquivo EXCEL (*.XLS)|*.xls';
      OpenDialog.FileName   := Arquivo;
      OpenDialog.Options    := OpenDialog.Options + Opcoes;
      //
      if Length(OpenDialog.FileName) = 0 then
        OpenDialog.FileName := TxtPadraoDir;
      //
      if OpenDialog.Execute then
      begin
        Resultado := OpenDialog.FileName;
        Result := True;
      end;
    finally
      OpenDialog.free;
    end;
  end else begin
{$WARN UNIT_PLATFORM OFF}
{$WARN SYMBOL_PLATFORM OFF}
    FileOpenDialog := TFileOpenDialog.Create(Form);
{$WARN UNIT_PLATFORM ON}
{$WARN SYMBOL_PLATFORM ON}
    try
      try
        FileOpenDialog.DefaultFolder := IniDir;
        FileOpenDialog.Title      := Titulo;//'Abrir Arquivo de Concilia��o Banc�ria';
        NovoFiltro := Filtro; // Ex.: Filtro := 'Arquivos OFC|*.ofc;Arquivos OFX|*.ofx';
        while NovoFiltro <> '' do
        begin
          if Geral.SeparaPrimeiraOcorrenciaDeTexto(';', NovoFiltro, Item, NovoFiltro) then
          begin
            if Geral.SeparaPrimeiraOcorrenciaDeTexto('|', Item, Tipo, Extensao) then
            begin
              with FileOpenDialog.FileTypes.Add do
              begin
                DisplayName := Tipo;
                FileMask := Extensao;
              end;
            end;
          end;
        end;
        FileOpenDialog.FileName   := Arq;
        //
        if Length(FileOpenDialog.FileName) = 0 then
          FileOpenDialog.FileName := TxtPadraoDir;
        //
        //FileOpenDialog.Options    := FileOpenDialog.Options + Opcoes;
        //
      except
        // nada
      end;
      if FileOpenDialog.Execute then
      begin
        Resultado := FileOpenDialog.FileName;
        Result := True;
      end;
    finally
      FileOpenDialog.free;
    end;
  end;
end;
*)
{
function TUnMyObjects.FormCria(InstanceClass: TComponentClass;
  var Reference): Boolean;
var
  Instance: TComponent;
begin
  Screen.Cursor := crHourGlass;
  Result := False;
  try
    Instance := TComponent(InstanceClass.NewInstance);
    TComponent(Reference) := Instance;
    try
      Instance.Create(Application);
      Result := True;
    except
      TComponent(Reference) := nil;
      raise;
    end;
    Screen.Cursor := crDefault;
  finally
    Screen.Cursor := crDefault;
  end;
end;
}
(*
procedure TUnMyObjects.FormMsg(var Msg: TMsg; var Handled: Boolean);
var
  Controle : TWinControl;
  LaTipo: TLabel;
  BitBtn: TBitBtn;
  NomeBtn: String;
  OK, OK1: Boolean;
begin
  if Msg.Message <> 275 then
  begin
    if VAR_TIMERIDLE <> nil then
    begin
      if VAR_TIMERIDLE.Interval > 0  then
      begin
        VAR_TIMERIDLE.Enabled := False;
        VAR_TIMERIDLE.Enabled := True;
      end;
    end;
  end;
  if Msg.Message= WM_KEYUP then
  begin
    if Msg.wParam = VK_CONTROL then
    VAR_GOTOYJUST := False;
    if Msg.wParam = VK_SNAPSHOT	then
    begin
      if Screen.ActiveForm = nil then Exit;
      Controle := Screen.ActiveForm.ActiveControl;
      if Controle <> nil then
      begin
        if (Controle is TDBGrid) or (Controle is TdmkDBGrid) or
        (Controle is TdmkDBGridDAC) then
          FmMeuDBUses.ImprimeDBGrid(TDBGrid(Controle), TDBGrid(Controle).Name)
        else
        if (Controle is TStringGrid) then
        begin
          FmMeuDBUses.ImprimeStringGrid(TStringGrid(Controle), TStringGrid(Controle).Name);
        end;
      end;
    end;
  end else if Msg.Message= WM_KEYDOWN then
  begin
    //VAR_VK_13_PRESSED := False;
    try
      if Msg.wParam = VK_CONTROL then
        VAR_GOTOYJUST := True;
      // n�o funciona
{$IFDEF UsaWSuport}
    if Msg.wParam = VK_F1 then
      DmkWeb.MostraWSuporte;
{$ENDIF}
      //
{$IFNDEF NO_USE_MYSQLMODULE}
      if (Msg.wParam = VK_F5) then
        FmMeuDBUses.VirtualKey_F5()
      else
      if (Msg.wParam = VK_F7) then
        FmMeuDBUses.VirtualKey_F7('')
      else
{$ENDIF}
      if (Msg.wParam = VK_F11) then
      begin
        if VAR_CALCULADORA_COMPONENTCLASS <> nil then
        begin
          OK := False;//OK := Calculadora(VAR_CALCULADORA_COMPONENTCLASS, VAR_CALCULADORA_REFERENCE);
          if OK then
          begin
            if Screen.ActiveForm = nil then Exit;
            Controle := Screen.ActiveForm.ActiveControl;
            if Controle <> nil then
            begin
              if Controle is TEdit then
                TEdit(Controle).Text := VAR_CALCTEXTO
            end;
          end;
        end;
      // Tecla do menu suspenso
      end else if (Msg.wParam = 93)
      //  Tecla do Windows
      or ( (Msg.wParam = 91) and VAR_GOTOYJUST) then
      begin
        if MAR_SQLx = nil then
        begin
          Geral.MensagemBox('"MAR_SQLx" n�o definido no aplicativo!',
          'Aviso', MB_OK+MB_ICONEXCLAMATION);
        end else begin
          DefParams(0);
          if VAR_CAD_POPUP <> nil then
          begin
            if VAR_SHOW_MASTER_POPUP then
            begin
              MostraPopUpNoCentro(VAR_CAD_POPUP);
              //ReabreQuery := True;
            end else Geral.MensagemBox('Master popup desabilitado.',
            'Aviso', MB_OK+MB_ICONINFORMATION);
          end else Geral.MensagemBox('"VAR_CAD_POPUP" n�o definido.',
          'Aviso', MB_OK+MB_ICONINFORMATION);
        end;
      end;
      if Screen.ActiveForm = nil then Exit;
      Controle := Screen.ActiveForm.ActiveControl;
      if Controle = nil then
        Exit;
      //if (Screen.ActiveForm.Name = 'FmCalculadora') then
      //Exit;
      if  (Controle.Name = 'EdSenha') then exit;
      if Controle is TDBGrid then
        Exit;
      if Controle is TStringGrid then
        Exit;
      if Controle is TMemo then
        Exit;
      if Controle is TDBMemo then
        Exit;
      if Controle is TListBox then
        Exit;
      if Controle is TDBListBox then
        Exit;
      if Controle is TComboBox then
        Exit;
      if Controle is TDBComboBox then
        Exit;
      if Controle is TRichEdit then
        Exit;
      if Controle is TDBRichEdit then
        Exit;
      if Controle <> nil then
      begin
        if (Msg.wParam = IC2_LOpcoesTeclaEnter) then
        begin
          if (Controle.Name = 'EdLeituraCodigoDeBarras') then
            OK1 := False
          else
          if (Controle is TdmkEdit) then
            OK1 := not TdmkEdit(Controle).NoEnterToTab
          else
            OK1 := True;
          if OK1 then
            Msg.wParam := MeuwParamEnter(Controle);
        end else if (Msg.wParam = IC2_LOpcoesTeclaPonto) then
            Msg.wParam := MeuwParamPonto(Controle)
        else if Msg.wParam in ([VK_PRIOR, VK_NEXT]) then
        begin
          LaTipo := nil;
          if Screen.ActiveForm.FindComponent('LaTipo') <> nil then
          begin
            try
              LaTipo := TLabel(Screen.ActiveForm.FindComponent('LaTipo'));
            except
              ShowMessage('Componente "LaTipo" n�o � TLabel');
            end;
            if LaTipo.Caption = CO_TRAVADO then
            begin
              if VAR_GOTOYJUST then
              begin
                case Msg.wParam of
                  VK_PRIOR: NomeBtn := 'SpeedButton4'; // Contr�rio
                  VK_NEXT : NomeBtn := 'SpeedButton1'; // Contr�rio
                  else NomeBtn := '';
                end;
              end else begin
                case Msg.wParam of
                  VK_PRIOR: NomeBtn := 'SpeedButton3'; // Contr�rio
                  VK_NEXT : NomeBtn := 'SpeedButton2'; // Contr�rio
                  else NomeBtn := '';
                end;
              end;
              //
              //if Screen.ActiveForm.FindComponent(NomeBtn) <> nil then
              BitBtn := TBitBtn(Screen.ActiveForm.FindComponent(NomeBtn));
              if BitBtn <> nil then BitBtn.Click;
            end;
          end;
        end;
      end;
    except
       Geral.MensagemBox(PChar('Erro em "WM_KEYDOWN" > Msg.wParam = ' +
       IntToStr(Msg.wParam)), 'Erro', MB_OK+MB_ICONERROR);
       raise;
    end;
  end else if Msg.message = WM_MOUSEWHEEL then
  begin
    if Screen.ActiveForm = nil then Exit;
    Controle := Screen.ActiveForm.ActiveControl;
    if Controle <> nil
    then if Controle is TDBGrid then
    begin
      Msg.message := WM_KEYDOWN;
      Msg.lParam := 0;
      {i := HiWord(Msg.wParam);
      if i > 0 then
        Msg.wParam := VK_UP
      else
        Msg.wParam := VK_DOWN;}
      if Msg.wParam > 0 then
        Msg.wParam := VK_UP
      else
        Msg.wParam := VK_DOWN;

      Handled := False;
    end;
  end;
end;
*)
{
function TUnMyObjects.FormShow(InstanceClass: TComponentClass;
  var Reference): Boolean;
var
  Instance: TComponent;
begin
  Screen.Cursor := crHourGlass;
  //Result := False;
  try
    Instance := TComponent(InstanceClass.NewInstance);
    TComponent(Reference) := Instance;
    try
      Instance.Create(FmPrincipal);
      //Result := True;
    except
      TComponent(Reference) := nil;
      raise;
    end;
    Screen.Cursor := crDefault;
  finally
    Screen.Cursor := crDefault;
  end;
  //
  TForm(Reference).ShowModal;
  TForm(Reference).Destroy;
  Result := True;
end;

procedure TUnMyObjects.frxDefineDataSets(frx: TfrxReport;
  DataSets: array of TfrxDataset);

var
  I, K, N, Ini, Fim: Integer;
begin
  N := 0;
  K := frx.Datasets.Count;
  frx.Datasets.Clear;
  Ini := Low(Datasets);
  Fim := High(Datasets);
  for I := Ini to Fim do
  begin
    frx.DataSets.Add(DataSets[I]);
    N := N + 1;
  end;
  if N <> K then
  begin
    Geral.MensagemBox('Quantidade de datasets difere!' + #13#10 +
    'Componente frx: ' + frx.Name + #13#10 +
    'Inicial: ' + Geral.FF0(K) + #13#10 +
    'Final:   ' + Geral.FF0(N),
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
end;

function TUnMyObjects.frxImprime(frx: TfrxReport; Titulo: string): Boolean;
begin
  Result := frxPrepara(frx, Titulo);
  if Result then
  begin
    // 2012-05-12
    if VAR_NEXT_FRX_IN_DESIGN_MODE = True then
    begin
      VAR_NEXT_FRX_IN_DESIGN_MODE := False;
      frx.DesignReport;
    end else
    begin
    // FIM 2012-05-12
      FmMeuFrx.PvVer.Print;
      //
      FmMeuFrx.Destroy;
    end;
  end;
end;

procedure TUnMyObjects.frxMemoAlignment(v: TfrxMemoView; Align: Integer);
begin
  case Align of
    000: //'Esquerda no topo';
    begin
      v.VAlign := frxClass.vaTop;
      v.HAlign := frxClass.haLeft;
    end;
    001:  //'Esquerda no centro';
    begin
      v.VAlign := frxClass.vaCenter;
      v.HAlign := frxClass.haLeft;
    end;
    002: //'Esquerda em baixo';
    begin
      v.VAlign := frxClass.vaBottom;
      v.HAlign := frxClass.haLeft;
    end;
    100: //'Centro no topo';
    begin
      v.VAlign := frxClass.vaTop;
      v.HAlign := frxClass.haCenter;
    end;
    101: //'Centralizado';
    begin
      v.VAlign := frxClass.vaCenter;
      v.HAlign := frxClass.haCenter;
    end;
    102: //'Centro em baixo';
    begin
      v.VAlign := frxClass.vaBottom;
      v.HAlign := frxClass.haCenter;
    end;
    200: //'Direita no topo';
    begin
      v.VAlign := frxClass.vaTop;
      v.HAlign := frxClass.haRight;
    end;
    201: //'Direita no centro';
    begin
      v.VAlign := frxClass.vaCenter;
      v.HAlign := frxClass.haRight;
    end;
    202: //'Direita em baixo';
    begin
      v.VAlign := frxClass.vaBottom;
      v.HAlign := frxClass.haRight;
    end;
  end;
end;

function TUnMyObjects.frxMostra(frx: TfrxReport; Titulo: string): Boolean;
begin
  frx.PreviewOptions.ZoomMode := zmPageWidth;
  Result := frxPrepara(frx, Titulo);
  if Result then
  begin
    // 2012-05-12
    if VAR_NEXT_FRX_IN_DESIGN_MODE = True then
    begin
      VAR_NEXT_FRX_IN_DESIGN_MODE := False;
      frx.DesignReport;
    end else
    // FIM 2012-05-12
    begin
      //FmMeuFrx.PvVer.Report.PreviewOptions.ZoomMode := zmPageWidth;
      FmMeuFrx.ShowModal;
      FmMeuFrx.Destroy;
    end;
  end;
end;

function TUnMyObjects.frxNextDesign(): Boolean;
begin
  Result := SelRadioGroup('Mostrar Pr�ximo Relat�rio em Modo Desenho',
  'Selecione Inten��o', ['Sim', 'N�o'], 1) = 0;
  //
  VAR_NEXT_FRX_IN_DESIGN_MODE := Result;
end;

function TUnMyObjects.frxPrepara(frx: TfrxReport; Titulo: string): Boolean;
begin
  Screen.Cursor := crHourGlass;
  Result := False;
  try
    try
      frx.PreviewOptions.AllowEdit := True;
      frx.ReportOptions.Name := Titulo;
      frx.Variables['MeuLogoExiste'] := VAR_MEULOGOEXISTE;
      frx.Variables['MeuLogoCaminho'] := QuotedStr(VAR_MEULOGOPATH);
      frx.Variables['LogoNFExiste'] := VAR_LOGONFEXISTE;
      frx.Variables['LogoNFCaminho'] := QuotedStr(VAR_LOGONFPATH);
      frx.Variables['LogoDuplExiste'] := VAR_LOGODUPLEXISTE;
      frx.Variables['LogoDuplCaminho'] := QuotedStr(VAR_LOGODUPLPATH);
      frx.Variables['LogoBig1Existe'] := VAR_LOGOBIG1EXISTE;
      frx.Variables['LogoBig1Caminho'] := QuotedStr(VAR_LOGOBIG1PATH);
      frx.Variables['SloganFooter'] := QuotedStr(VAR_SLOGAN_FOOTER);
      // N�o funciona - gera erro de vari�vel - USAR NO CODE DO FRX
      //MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Name));
      //frx.Variables['VARF_CODI_FRX'] := Copy(frx.Name, 4);
      //
      frx.PrepareReport;
      //
      FormCria(TFmMeuFrx, FmMeuFrx);
      frx.Preview := FmMeuFrx.PvVer;
      //
      FmMeuFrx.PvVer.OutlineWidth := frx.PreviewOptions.OutlineWidth;
      FmMeuFrx.PvVer.Zoom := frx.PreviewOptions.Zoom;
      FmMeuFrx.PvVer.ZoomMode := frx.PreviewOptions.ZoomMode;
      FmMeuFrx.UpdateZoom;
      //
      Result := True;
      Screen.Cursor := crDefault;
    except
      raise;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TUnMyObjects.frxSalva(frx: TfrxReport; Titulo,
  Arquivo: string): Boolean;
begin
  Result := frxPrepara(frx, Titulo);
  if Result then
  begin
    FmMeuFrx.PvVer.SaveToFile(Arquivo);
    FmMeuFrx.Destroy;
  end;
end;

procedure TUnMyObjects.HabilitaMenuItemCabDel(MenuItem: TMenuItem;
  QrCab, QrIts: TDataSet);
begin
  MenuItem.Enabled := (QrCab.State <> dsInactive) and (QrCab.RecordCount > 0);
  if MenuItem.Enabled and (QrIts <> nil) then
    MenuItem.Enabled := (QrIts.State <> dsInactive) and (QrIts.RecordCount = 0);
end;

procedure TUnMyObjects.HabilitaMenuItemCabDelC1I2(MenuItem: TMenuItem; QrCab,
  QrIts1, QrIts2: TDataSet);
begin
  MenuItem.Enabled := (QrCab.State <> dsInactive) and (QrCab.RecordCount > 0);
  if MenuItem.Enabled and (QrIts1 <> nil) then
    MenuItem.Enabled := (QrIts1.State <> dsInactive) and (QrIts1.RecordCount = 0);
  if MenuItem.Enabled and (QrIts2 <> nil) then
    MenuItem.Enabled := (QrIts2.State <> dsInactive) and (QrIts2.RecordCount = 0);
end;

procedure TUnMyObjects.HabilitaMenuItemCabDelC1I3(MenuItem: TMenuItem; QrCab,
  QrIts1, QrIts2, QrIts3: TDataSet);
begin
  MenuItem.Enabled := (QrCab.State <> dsInactive) and (QrCab.RecordCount > 0);
  if MenuItem.Enabled and (QrIts1 <> nil) then
    MenuItem.Enabled := (QrIts1.State <> dsInactive) and (QrIts1.RecordCount = 0);
  if MenuItem.Enabled and (QrIts2 <> nil) then
    MenuItem.Enabled := (QrIts2.State <> dsInactive) and (QrIts2.RecordCount = 0);
  if MenuItem.Enabled and (QrIts3 <> nil) then
    MenuItem.Enabled := (QrIts3.State <> dsInactive) and (QrIts3.RecordCount = 0);
end;

procedure TUnMyObjects.HabilitaMenuItemCabDelC1I4(MenuItem: TMenuItem; QrCab,
  QrIts1, QrIts2, QrIts3, QrIts4: TDataSet);
begin
  MenuItem.Enabled := (QrCab.State <> dsInactive) and (QrCab.RecordCount > 0);
  if MenuItem.Enabled and (QrIts1 <> nil) then
    MenuItem.Enabled := (QrIts1.State <> dsInactive) and (QrIts1.RecordCount = 0);
  if MenuItem.Enabled and (QrIts2 <> nil) then
    MenuItem.Enabled := (QrIts2.State <> dsInactive) and (QrIts2.RecordCount = 0);
  if MenuItem.Enabled and (QrIts3 <> nil) then
    MenuItem.Enabled := (QrIts3.State <> dsInactive) and (QrIts3.RecordCount = 0);
  if MenuItem.Enabled and (QrIts4 <> nil) then
    MenuItem.Enabled := (QrIts4.State <> dsInactive) and (QrIts4.RecordCount = 0);
end;

procedure TUnMyObjects.HabilitaMenuItemCabUpd(MenuItem: TMenuItem;
  QrCab: TDataSet);
begin
  MenuItem.Enabled := (QrCab.State <> dsInactive) and (QrCab.RecordCount > 0);
end;

procedure TUnMyObjects.HabilitaMenuItemItsDel(MenuItem: TMenuItem;
  QrIts: TDataSet);
begin
  MenuItem.Enabled := (QrIts.State <> dsInactive) and (QrIts.RecordCount > 0);
end;

procedure TUnMyObjects.HabilitaMenuItemItsIns(MenuItem: TMenuItem;
  QrCab: TDataSet);
begin
  MenuItem.Enabled := (QrCab.State <> dsInactive) and (QrCab.RecordCount > 0);
end;

procedure TUnMyObjects.HabilitaMenuItemItsUpd(MenuItem: TMenuItem;
  QrIts: TDataSet);
begin
  MenuItem.Enabled := (QrIts.State <> dsInactive) and (QrIts.RecordCount > 0);
end;
}

procedure TUnMyObjects.Informa(Label1: TLabel; Aguarde: Boolean; Texto: String);
begin
  if Label1 <> nil then
  begin
    if Aguarde then
      Label1.Caption := 'Aguarde... ' + Texto + '...'
    else
      Label1.Caption := Texto;
    Label1.Update;
    Application.ProcessMessages;
  end;
end;

procedure TUnMyObjects.Informa2(Label1, Label2: TLabel; Aguarde: Boolean; Texto: String);
begin
  if Label1 <> nil then
  begin
    if Aguarde then
      Label1.Caption := 'Aguarde... ' + Texto + '...'
    else
      Label1.Caption := Texto;
    Label1.Update;
  end;
  if Label2 <> nil then
  begin
    if Aguarde then
      Label2.Caption := 'Aguarde... ' + Texto + '...'
    else
      Label2.Caption := Texto;
    Label2.Update;
  end;
  Application.ProcessMessages;
end;

procedure TUnMyObjects.Informa2EUpdPB(PB: TProgressBar; Label1, Label2: TLabel;
  Aguarde: Boolean; Texto: String);
begin
  if PB <> nil then
  begin
    PB.Position := PB.Position + 1;
    PB.Update;
  end;
  Informa2(Label1, Label2, Aguarde, Texto);
end;

procedure TUnMyObjects.InformaEUpdPB(PB: TProgressBar; Label1: TLabel;
  Aguarde: Boolean; Texto: String);
begin
  if PB <> nil then
  begin
    PB.Position := PB.Position + 1;
    PB.Update;
  end;
  Informa(Label1, Aguarde, Texto);
end;

procedure TUnMyObjects.InformaN(Labels: array of TLabel; Aguarde: Boolean;
  Texto: String);
var
  I: Integer;
begin
  for I := Low(Labels) to High(Labels) do
  begin
    if Aguarde then
      Labels[I].Caption := 'Aguarde... ' + Texto + '...'
    else
      Labels[I].Caption := Texto;
    Labels[I].Update;
  end;
end;

procedure TUnMyObjects.UpdPB(PB: TProgressBar; Label1, Label2: TLabel);
var
  Txt: String;
begin
  PB.Position := PB.Position + 1;
  PB.Update;
  if (Label1 <> nil) or (Label2 <> nil) then
  begin
    Txt := IntToStr(PB.Position) + ' de ' + IntToStr(PB.Max);
    if Label1 <> nil then
      Label1.Caption := Txt;
    if Label2 <> nil then
      Label2.Caption := Txt;
  end;
  Application.ProcessMessages;
end;

{
procedure TUnMyObjects.LimpaGrade(MinhaGrade: TStringGrid; ColIni,
  RowIni: Integer; ExcluiLinhas: Boolean);
var
  i, j: Integer;
begin
  for i := ColIni to MinhaGrade.ColCount -1 do
    for j := RowIni to MinhaGrade.RowCount -1 do MinhaGrade.Cells[i,j] := '';
  if ExcluiLinhas then
    MinhaGrade.RowCount := 2;
end;

procedure TUnMyObjects.LimpaGrades(Grades: array of TStringGrid; ColIni,
  RowIni: Integer; ExcluiLinhas: Boolean);
var
  i, j, k: Integer;
begin
  j := High(Grades);
  k := Low(Grades);
  //
  for i := k to j do
  begin
    if Grades[i] <> nil then
      LimpaGrade(Grades[i], ColIni, RowIni, ExcluiLinhas);
  end;
end;

procedure TUnMyObjects.ListaItensListBoxDeLista(ListBox: TListBox;
  Lista: array of String);
var
  I: Integer;
begin
  ListBox.Clear;
  for I := Low(Lista) to High(Lista) do
    ListBox.Items.Add(Lista[I]);
end;

function TUnMyObjects.MeuwParamEnter(Controle: TWinControl): Integer;
begin
  Result := IC2_LOpcoesTeclaEnter;
  //Exit;
  if VAR_CANCEL_ENTER_TAB then Exit; // Fm???_MLA
  if Controle is TDBCombobox then
  begin
    if SendMessage(TDBComboBox(Controle).Handle, CB_GETDROPPEDSTATE, 0, 0) <> 1 then
     Result := IC2_LOpcoesTeclaTab
  end;

  if Controle is TdmkDBLookupCombobox then
  begin
    if not TdmkDBLookupCombobox(Controle).IsDropped then


       Result := IC2_LOpcoesTeclaTab;
  end else
  if Controle is TDBLookupCombobox then
  begin
    if SendMessage(TDBLookupCombobox(Controle).Handle, CB_GETDROPPEDSTATE, 0, 0) <> 1 then
     Result := IC2_LOpcoesTeclaTab
  end;


  if Controle is TEdit then Result := IC2_LOpcoesTeclaTab else
  if Controle is TDBEdit then Result := IC2_LOpcoesTeclaTab else
  if Controle is TdmkEditDateTimePicker then
    Result := IC2_LOpcoesTeclaTab else
  if Controle is TDateTimePickerdmkEdit then
    Result := IC2_LOpcoesTeclaTab else
  if Controle is TdmkEdit then
  begin
    // dmkEdit do dmkDBGridDAC
    if (TdmkEdit(Controle).Name <> '') or
    (TdmkEdit(Controle).FComponentePai <> '') then
    Result := IC2_LOpcoesTeclaTab;
  end else
  if Controle is TMaskEdit then Result := IC2_LOpcoesTeclaTab else
  if Controle is TDateTimePicker then Result := IC2_LOpcoesTeclaTab else
  if Controle is TCheckBox then Result := IC2_LOpcoesTeclaTab else
  if Controle is TRadioButton then Result := IC2_LOpcoesTeclaTab else
  if Controle is TComboBox then
  begin
    if not TComboBox(Controle).DroppedDown then Result := IC2_LOpcoesTeclaTab;
  end;
end;

function TUnMyObjects.MeuwParamPonto(Controle: TWinControl): Integer;
begin
  Result := IC2_LOpcoesTeclaPonto;
end;
}
(*
procedure TUnMyObjects.MostraErro(Sender: TObject; E: Exception);
var
  i, a, IniNum, Acao, n, p1, p2: Integer;
  Resultado, Texto, Erro,NumTxt, MensExtra, BancoDados, Tabela: String;
   Mensagem: WideString;
begin
  Texto := '';
  //Conta := 0;
  Acao  := 0;
  Erro := E.Message;
  if Erro = 'Null Remote Address' then
  begin
    Resultado := 'Erro. O endere�o desejado est� indispon�vel.';
  end else if (E is ESocketError) or (E is EIdSocketError) then
  begin
    //Socket Error # 10061 - Servidor inativo
    NumTxt := '';
    IniNum := 0;
    for i := 1 to Length(Erro) do
    begin
      if Erro[i] = '#' then IniNum := IniNum + 1
      else if (IniNum = 1) then
      begin
        if Erro[i] in ([' ','0'..'9']) then
          NumTxt := NumTxt + Erro[i]
        else
          IniNum := IniNum + 1;
      end;
    end;
    NumTxt := Trim(NumTxt);
    if NumTxt = '' then
      Resultado := Erro
    else
      Resultado := ErroDeSocket(StrToInt(NumTxt), Erro);
    Resultado := Resultado + ' (' + E.ClassName;
    if NumTxt <> '' then Resultado := Resultado + ' n� ' + NumTxt;
    Resultado := Resultado + ').';
  end else if E is EOleException then
  begin
    Resultado := E.ClassName +#13#10 + Erro;
  end else if E is EConvertError then
  begin
    IniNum := 0;
    a := 0;
    for i := 1 to Length(Erro) do if Erro[i] = '''' then a := i;
    for i := 1 to Length(Erro) do
    begin
      if Erro[i] = '''' then IniNum := IniNum + 1;
      if (IniNum>0) and (i < a) then NumTxt := NumTxt + Erro[i];
    end;
    Mensagem := Uppercase(Copy(Erro, a+1, Length(Erro)-a));
    if Mensagem = Uppercase(' is not a valid floating point value') then
    Resultado := 'Erro. Valor flutuante inv�lido: "'+NumTxt+'"'
    else  Resultado := Erro;
    //Resultado := E.ClassName +#13#10 + Resultado;
  end else if (E is EDatabaseError) or (E is EmySQLDataBaseError) then
  //EMySQLDACException
  begin
    IniNum := 0;
    for i := 1 to Length(Erro) do
    begin
      if Erro[i] = '(' then IniNum := IniNum + 1
      else if Erro[i] = ')' then IniNum := IniNum + 1
      else if (IniNum = 1) then NumTxt := NumTxt + Erro[i];
    end;
    //VOSTRO_1320
    if NumTxt = '' then
      Resultado := Erro
    else begin
      case StrToInt(NumTxt) of
        1146:
        begin
          Mensextra := Copy(Erro, 34, Length(erro)-48);
          Resultado := 'Erro. Tabela n�o existe: '+ MensExtra;
          Acao := 1146;
        end;
        1062: Resultado := 'Erro. O registro j� existe, ou tem chave que n�o pode ser duplicada!';
        //Unknown column 'Numero' in 'field list'
        1054:
        begin
          Resultado := 'Erro. Coluna desconhecida na lista de campos: "'+
          Copy(Erro, 43, Length(erro)-58)+'"'+Chr(13)+Chr(10)+'Fa�a uma verifica��o'+
          ' completa das tabelas dos bancos de dados de rede e local acessando no '+
          'menu principal -> Ferramentas -> BD -> verifica...'+Char(13)+Char(10)+
          'Caso o problema continue, contate a Dermatek!';
          //Acao := 1054;
        end
        else  Resultado := Erro;
      end;
    end;
    ////////////////////////// fim c�digos /////////////////////////////////////
    if Resultado = Erro then
    begin
      a := 0;
      for i := 1 to Length(Erro) do if Erro[i] = '''' then a := i;
      for i := 1 to Length(Erro) do
      begin
        if Erro[i] = '''' then IniNum := IniNum + 1;
        if (IniNum>0) and (i < a) then NumTxt := NumTxt + Erro[i];
      end;
      Mensagem := Uppercase(Copy(Erro, a+1, Length(Erro)-a));
      if Mensagem = Uppercase(' is not a valid floating point value') then
                    Resultado := 'Erro. Valor flutuante inv�lido: "'+NumTxt+'"'
      else if Mensagem = Uppercase(' in where clause is ambiguous') then
                    Resultado := 'Erro. Campo amb�guo na SQL.'
      else if Mensagem = Uppercase(' must have a value') then
                    Resultado := 'Erro. O campo "'+NumTxt+'" � obrigat�rio!'
      else if Copy(Mensagem, 1, 20) = Uppercase('Field value required') then
      begin
                    NumTxt := Copy(Mensagem, 31, Length(Mensagem)-30);
                    Resultado := 'Erro. O campo "'+NumTxt+'" � obrigat�rio!'
      end
      else  Resultado := Erro;
    end;
  end else if E is ECommonCalendarError then
  begin
    if LowerCase(e.Message) = 'failed to set calendar date or time' then
    Resultado := 'Erro ao definir data ou hora para o calend�rio!' else
    if Copy(LowerCase(e.Message), 1, 28) = 'date is less than minimum of' then
    Resultado := 'Data abaixo do m�nimo: ' + Copy(e.Message, 29) else
    Resultado := e.Message;
  end //else if Uppercase(Erro) = 'PROPERTY DATABASE IS NOT SET!' then Resultado := 'A "Query" ou "Table" n�o teve sua propriedade "Database" configurada.'
  else if E is ERemotableException then
  begin
    p1 := pos('Server did not recognize the value of HTTP Header SOAPAction:', Erro);
    p2 := pos(':', Erro);
    if p1 > 0 then
      Resultado :=
      'O servidor n�o reconheceu o valor definido para o cabe�alho HTTP SOAPAction'
      + Copy(Erro, p2) + #13+#10 +
      'Verifique se a vari�vel "InvRegistry.RegisterDefaultSOAPAction" est� correta!'
    else
      Resultado := Erro;
  end
  else if E is Exception then
  begin
    // erro se coloca antes do 1146, pois da� n�o passa por ele.
    p1 := pos('Field', Erro);
    p2 := pos('is not found in current dataset', Erro);
    if (p1 > 0) and (p2 > 0) then
    begin
      p1 := pos('"', Erro);
      Resultado := 'O campo ' + Copy(Erro, p1, p2-p1) + 'n�o existe ' +
      'na base de dados. (� possivel que seja um campo calculado.)';
    end else
      Resultado := E.ClassName + ' # ' + Erro;
  end
  else Application.ShowException(e);
  if (Acao = 0) and (Length(Resultado) > 0) then
  begin
    Geral.MensagemBox(Resultado, 'Erro', MB_OK+MB_ICONERROR);
    Screen.Cursor := crDefault;
  end;
  case Acao of
    1146:
    begin
      n := pos('.', MensExtra);
      if n > 0 then
      begin
        BancoDados := Copy(MensExtra, 1, n-1);
        Tabela     := Copy(MensExtra, n+1, Length(MensExtra)-n);
        {$IFNDEf SemDBLocal}
        if Uppercase(Bancodados) = Uppercase(TLocDB) then
        begin
          //if
          Geral.MensagemBox('A tabela ' + Tabela +
          ' pertencente ao banco de dados local ' + BancoDados+' e n�o existe. '+
          'Verifique se ela pode ser criada acessando o menu principal ' +
          '> Ferramentas > BD > Verifica', 'Aviso',
          MB_YESNO+MB_ICONQUESTION);
        end else
        {$EndIf}
          Application.ShowException(e);
      end;
    end;
  end;
end;
*)

{
function TUnMyObjects.MostraPMCEP(PMCEP: TPopupMenu; Botao: TObject): Boolean;
var
  Lista: TStringList;
  i: Integer;
  Achou: Boolean;
begin
  Result := False;
  if VAR_REGEDIT_MyCEP then
  begin
    Geral.MensagemBox(PChar('Foi configurada a base de dados de pesquisa'+
    ' de CEP. � necess�rio reiniciar o aplicativo!'#13#10+
    'Base de dados: [C:\Dermatek\Correios\Access]'), 'Aviso', MB_OK+
    MB_ICONWARNING);
    Exit;
  end;
  Achou := False;
  Lista := TStringList.Create;
  Session.GetDatabaseNames(Lista);
  if Lista.Count > 0 then
  begin
    for i := 0 to Lista.Count-1 do
      if Lista[i] = 'MyCEP' then
      begin
        Achou := True;
        Break;
      end;
  end;
  if not Achou then
  begin
    Geral.WriteAppKey_Total('Driver', '\Software\ODBC\ODBC.INI\MyCEP',
    'C:\WINDOWS\system32\odbcjt32.dll', ktString, HKEY_CURRENT_USER);
    Geral.WriteAppKey_Total('DBQ', '\Software\ODBC\ODBC.INI\MyCEP',
    'C:\Dermatek\Correios\Access\dbgpbe.mdb', ktString, HKEY_CURRENT_USER);
    Geral.WriteAppKey_Total('Description', '\Software\ODBC\ODBC.INI\MyCEP',
    'Consulta de CEP', ktString, HKEY_CURRENT_USER);
    Geral.WriteAppKey_Total('DriverId', '\Software\ODBC\ODBC.INI\MyCEP',
    25, ktInteger, HKEY_CURRENT_USER);
    Geral.WriteAppKey_Total('FIL', '\Software\ODBC\ODBC.INI\MyCEP',
    'MS Access;', ktString, HKEY_CURRENT_USER);
    Geral.WriteAppKey_Total('PWD', '\Software\ODBC\ODBC.INI\MyCEP',
    '�����в�ޢ����', ktString, HKEY_CURRENT_USER);
    Geral.WriteAppKey_Total('SafeTransactions', '\Software\ODBC\ODBC.INI\MyCEP',
    0, ktInteger, HKEY_CURRENT_USER);
    Geral.WriteAppKey_Total('UID', '\Software\ODBC\ODBC.INI\MyCEP',
    '', ktString, HKEY_CURRENT_USER);
    //
    Geral.WriteAppKey_Total('ImplicitCommitSync', '\Software\ODBC\ODBC.INI\MyCEP\Engines\JET',
    '', ktString, HKEY_CURRENT_USER);
    Geral.WriteAppKey_Total('MaxBufferSize', '\Software\ODBC\ODBC.INI\MyCEP\Engines\JET',
    2048, ktInteger, HKEY_CURRENT_USER);
    Geral.WriteAppKey_Total('PageTimeout', '\Software\ODBC\ODBC.INI\MyCEP\Engines\JET',
    5, ktInteger, HKEY_CURRENT_USER);
    Geral.WriteAppKey_Total('Threads', '\Software\ODBC\ODBC.INI\MyCEP\Engines\JET',
    3, ktInteger, HKEY_CURRENT_USER);
    Geral.WriteAppKey_Total('UserCommitSync', '\Software\ODBC\ODBC.INI\MyCEP\Engines\JET',
    'Yes', ktString, HKEY_CURRENT_USER);
    //
    Geral.WriteAppKey_Total('MyCEP', '\Software\ODBC\ODBC.INI\ODBC Data Sources',
    'Driver do Microsoft Access (*.mdb)', ktString, HKEY_CURRENT_USER);
    VAR_REGEDIT_MyCEP := True;
  end;
  if VAR_REGEDIT_MyCEP then
  begin
    Geral.MensagemBox(PChar('Foi configurada a base de dados de pesquisa'+
    ' de CEP. � necess�rio reiniciar o aplicativo!'), 'Aviso', MB_OK+
    MB_ICONWARNING);
    Exit;
  end;
  //
  MostraPopUpDeBotaoObject(PMCEP, Botao, 0, 0);
end;

procedure TUnMyObjects.MostraPopOnControlXY(PM: TPopUpMenu;
  ComponentePai: TControl; PosX, PosY: Integer);
var
  pt: TPoint;
  rt: TRect;
  i, X,Y, lin, col, Hei, Wid: Integer;
begin
  Hei := 0;
  Wid := 0;
  GetCursorPos(pt);
  X  := pt.Y;
  Y  := pt.X;
  if ComponentePai = nil then
  begin
    ;// N�o faz nada!
  end else
  if ComponentePai is TSpeedButton then
  begin
    X  := X + 11;
    Y  := Y + 11;
  end else begin
    //Y   := 0;
    //X   := 0;
    if ComponentePai is TBitBtn then GetWindowRect(TBitBtn(ComponentePai).Handle, rt)
    else if ComponentePai is TButton then GetWindowRect(TButton(ComponentePai).Handle, rt)
    else if ComponentePai is TSpeedButton then GetWindowRect(TButton(ComponentePai).Handle, rt)
    else if ComponentePai is TPanel then GetWindowRect(TBitBtn(ComponentePai).Handle, rt)
    //else if ComponentePai is T L M D Edit then GetWindowRect(T L M D Edit(ComponentePai).Handle, rt)
    else if ComponentePai is TListView then GetWindowRect(TListView(ComponentePai).Handle, rt)
    else if ComponentePai is TStringGrid then
    begin
      GetWindowRect(TStringGrid(ComponentePai).Handle, rt);
      lin := TStringGrid(ComponentePai).Row;
      for i := 0 to lin do Hei := Hei + TStringGrid(ComponentePai).RowHeights[i];
      col := TStringGrid(ComponentePai).Col;
      for i := 0 to Col-1 do Wid := Wid + TStringGrid(ComponentePai).ColWidths[i];
      Wid := Wid + 6;
      Hei := Hei + 12;
    end
    else if ComponentePai is TDBGrid then
    begin
      GetWindowRect(TDBGrid(ComponentePai).Handle, rt);
      lin := THackDBGrid(ComponentePai).Row;
      for i := 0 to lin do Hei := Hei + THackDBGrid(ComponentePai).RowHeights[i];
      col := THackDBGrid(ComponentePai).Col;
      for i := 0 to Col-1 do Wid := Wid + THackDBGrid(ComponentePai).ColWidths[i];
      Wid := Wid + 6;
      Hei := Hei + 12;
    end
    else if ComponentePai is TdmkDBGrid then
    begin
      GetWindowRect(TdmkDBGrid(ComponentePai).Handle, rt);
      lin := THackDBGrid(ComponentePai).Row;
      for i := 0 to lin do Hei := Hei + THackDBGrid(ComponentePai).RowHeights[i];
      col := THackDBGrid(ComponentePai).Col;
      for i := 0 to Col-1 do Wid := Wid + THackDBGrid(ComponentePai).ColWidths[i];
      Wid := Wid + 6;
      Hei := Hei + 12;
    end
    else if ComponentePai is TdmkDBGridDAC then
    begin
      GetWindowRect(TdmkDBGridDAC(ComponentePai).Handle, rt);
      lin := THackDBGrid(ComponentePai).Row;
      for i := 0 to lin do Hei := Hei + THackDBGrid(ComponentePai).RowHeights[i];
      col := THackDBGrid(ComponentePai).Col;
      for i := 0 to Col-1 do Wid := Wid + THackDBGrid(ComponentePai).ColWidths[i];
      Wid := Wid + 6;
      Hei := Hei + 12;
    end else if ComponentePai is TTabSheet then
    begin
      GetWindowRect(TTabSheet(ComponentePai).Handle, rt);
      //Wid := Wid + pt.x;
      //Hei := Hei + pt.y;
    (*end else if ComponentePai is TImage then
    begin
      N�o funciona  usar componente pai do Image
      GetWindowRect(TImage(ComponentePai).Picture.Bitmap.Handle, rt);
      //Wid := Wid + pt.x;
      //Hei := Hei + pt.y;*)
    end else begin
      Geral.MensagemBox('Componente pai n�o definido para mostrar popup!' +
      #13#10 + 'Classe: ' + TObject(ComponentePai).ClassName + #13#10 + 'Nome: ' +
      TControl(ComponentePai).Name, 'Erro', MB_OK+MB_ICONERROR);
      Exit;
    end;
    if (PosX <> 0) or (PosY <> 0) then
    begin
      Y := rt.Left + PosX;
      X := rt.Top + PosY;
    end else begin
      Y := rt.Left + Wid;
      X := (PM.Items.Count * 19) + 6;
      if ComponentePai is TDBGrid then X := rt.Top + Hei else
      begin
        if (X + rt.Bottom) > (Screen.Height - 50)
          then X := rt.Top - X else X := rt.Bottom;
      end;
    end;
  end;  
  PM.Popup(y, x);
end;

procedure TUnMyObjects.MostraPopUpDeBotao(PM: TPopUpMenu;
  ComponentePai: TControl);
var
  pt: TPoint;
  rt: TRect;
  i, X,Y, lin, col, Hei, Wid, Itens: Integer;
begin
  Hei := 0;
  Wid := 0;
  GetCursorPos(pt);
  if ComponentePai is TSpeedButton then
  begin
    GetCursorPos(pt);
    PM.Popup(pt.X, pt.Y);
    Exit;
  end
  else if ComponentePai is TBitBtn then
    GetWindowRect(TBitBtn(ComponentePai).Handle, rt)
  else if ComponentePai is TButton then GetWindowRect(TButton(ComponentePai).Handle, rt)
  else if ComponentePai is TAdvGlowButton then GetWindowRect(TAdvGlowButton(ComponentePai).Handle, rt)
  else if ComponentePai is TAdvGlowMenuButton then GetWindowRect(TAdvGlowMenuButton(ComponentePai).Handle, rt)
  //else if ComponentePai is T L M D Edit then GetWindowRect(T L M D Edit(ComponentePai).Handle, rt)
  else if ComponentePai is TListView then GetWindowRect(TListView(ComponentePai).Handle, rt)
  //else if ComponentePai is TSpeedButton then GetWindowRect(TSpeedButton(ComponentePai).Handle, rt)
  else if ComponentePai is TDBGrid then
  begin
    GetWindowRect(TDBGrid(ComponentePai).Handle, rt);
    lin := THackDBGrid(ComponentePai).Row;
    for i := 0 to lin do Hei := Hei + THackDBGrid(ComponentePai).RowHeights[i];
    col := THackDBGrid(ComponentePai).Col;
    for i := 0 to Col-1 do Wid := Wid + THackDBGrid(ComponentePai).ColWidths[i];
    Wid := Wid + 6;
    Hei := Hei + 12;
  end
  else if ComponentePai is TdmkDBGrid then
  begin
    GetWindowRect(TdmkDBGrid(ComponentePai).Handle, rt);
    lin := THackDBGrid(ComponentePai).Row;
    for i := 0 to lin do Hei := Hei + THackDBGrid(ComponentePai).RowHeights[i];
    col := THackDBGrid(ComponentePai).Col;
    for i := 0 to Col-1 do Wid := Wid + THackDBGrid(ComponentePai).ColWidths[i];
    Wid := Wid + 6;
    Hei := Hei + 12;
  end
  else if ComponentePai is TdmkDBGridDAC then
  begin
    GetWindowRect(TdmkDBGridDAC(ComponentePai).Handle, rt);
    lin := THackDBGrid(ComponentePai).Row;
    for i := 0 to lin do Hei := Hei + THackDBGrid(ComponentePai).RowHeights[i];
    col := THackDBGrid(ComponentePai).Col;
    for i := 0 to Col-1 do Wid := Wid + THackDBGrid(ComponentePai).ColWidths[i];
    Wid := Wid + 6;
    Hei := Hei + 12;
  end else if ComponentePai is TTabSheet then
  begin
    GetWindowRect(TTabSheet(ComponentePai).Handle, rt);
    //Wid := Wid + pt.x;
    //Hei := Hei + pt.y;
  (*end else if ComponentePai is TImage then
  begin
    N�o funciona  usar componente pai do Image
    GetWindowRect(TImage(ComponentePai).Picture.Bitmap.Handle, rt);
    //Wid := Wid + pt.x;
    //Hei := Hei + pt.y;*)
  end else begin
    Geral.MensagemBox('Componente pai n�o definido para mostrar popup!' +
    #13#10 + 'Classe: ' + TObject(ComponentePai).ClassName +
    #13#10 + 'Nome: ' + TControl(ComponentePai).Name, 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Y := rt.Left + Wid;
  Itens := 0;
  for i := 0 to PM.Items.Count -1 do
    if PM.Items[i].Visible then Itens := Itens + 1;
  X := (Itens * 19) + 6;
  //X := (PM.Items.Count * 19) + 6;
  if ComponentePai is TDBGrid then X := rt.Top + Hei else
  begin
    if (X + rt.Bottom) > (Screen.Height - 50)
      then X := rt.Top - X else X := rt.Bottom;
  end;
  PM.Popup(y, x);
end;

procedure TUnMyObjects.MostraPopUpDeBotaoObject(PM: TPopUpMenu;
  ComponentePai: TObject; MoreWid, MoreTop: Integer);
var
  pt: TPoint;
  rt: TRect;
  i, X,Y, lin, col, Hei, Wid: Integer;
begin
  Hei := 0;
  Wid := 0;
  GetCursorPos(pt);
  if ComponentePai is TBitBtn then GetWindowRect(TBitBtn(ComponentePai).Handle, rt)
  else if ComponentePai is TButton then GetWindowRect(TButton(ComponentePai).Handle, rt)
  //else if ComponentePai is T L M D Edit then GetWindowRect(T L M D Edit(ComponentePai).Handle, rt)
  else if ComponentePai is TListView then GetWindowRect(TListView(ComponentePai).Handle, rt)
  else if ComponentePai is TDBGrid then
  begin
    GetWindowRect(TDBGrid(ComponentePai).Handle, rt);
    lin := THackDBGrid(ComponentePai).Row;
    for i := 0 to lin do Hei := Hei + THackDBGrid(ComponentePai).RowHeights[i];
    col := THackDBGrid(ComponentePai).Col;
    for i := 0 to Col-1 do Wid := Wid + THackDBGrid(ComponentePai).ColWidths[i];
    Wid := Wid + 6;
    Hei := Hei + 12;
  end else if ComponentePai is TDrawGrid then // TStringGrid
  begin
    GetWindowRect(TDrawGrid(ComponentePai).Handle, rt);
    lin := TDrawGrid(ComponentePai).Row;
    for i := 0 to lin do Hei := Hei + TDrawGrid(ComponentePai).RowHeights[i];
    col := TDrawGrid(ComponentePai).Col;
    for i := 0 to Col-1 do Wid := Wid + TDrawGrid(ComponentePai).ColWidths[i];
    Wid := Wid + 6;
    Hei := Hei + 12;
  end else begin
    Geral.MensagemBox('Componente pai n�o definido para mostrar popup!' +
    #13#10 + 'Classe do objeto: ' + TObject(ComponentePai).ClassName,
    'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Y := rt.Left + Wid + MoreWid;
  X := (PM.Items.Count * 19) + 6;
  if ComponentePai is TDBGrid then X := rt.Top + Hei else
  begin
    if (X + rt.Bottom) > (Screen.Height - 50)
      then X := rt.Top - X else X := rt.Bottom;
  end;
  X := X + MoreTop;
  PM.Popup(y, x);
end;

procedure TUnMyObjects.MostraPopUpNoCentro(PM: TPopUpMenu);
begin
  PM.Popup(Screen.Width div 2, Screen.Height div 2);
end;

function TUnMyObjects.ObtemTextoRichEdit(Form: TForm; RichEdit: TRichEdit): WideString;
var
  Memo: TMemo;
  //Bufer: WideString;
  MemoryStream: TMemoryStream;
begin
  Result := #0;
  MemoryStream := TMemoryStream.Create;
  try
    RichEdit.Lines.SaveToStream(MemoryStream);
    MemoryStream.Position := 0;
    Memo := TMemo.Create(Form);
    try
      Memo.Parent := Form;
      Memo.Lines.LoadFromStream(MemoryStream);
      Result := Memo.Text;
    finally
      Memo.Free;
    end;
  finally
    MemoryStream.Free;
  end;
end;

function TUnMyObjects.GetfrxRTF(RE: TfrxRichView): String;
var
  strStream: TStringStream;
  p: Integer;
begin
  if Re.RichEdit.Lines.Text = '' then
    Result := '' else
  begin
    strStream := TStringStream.Create('');
    try
      RE.PlainText := False;
      Re.RichEdit.Lines.SaveToStream(strStream);
      Result := strStream.DataString;
      p := Length(Result);
      if Result[p] = #0 then
        Result := Copy(Result, 1, p -1);
    finally
      strStream.Free;
    end;
  end;
end;

function TUnMyObjects.GetRTF(RE: TRichedit): String;
var
  strStream: TStringStream;
  p: Integer;
begin
  if RE.Text = '' then
    Result := '' else
  begin
    strStream := TStringStream.Create('');
    try
      RE.PlainText := False;
      RE.Lines.SaveToStream(strStream);
      Result := strStream.DataString;
      p := Length(Result);
      if Result[p] = #0 then
        Result := Copy(Result, 1, p -1);
    finally
      strStream.Free;
    end;
  end;
end;

function TUnMyObjects.EditaTextoRichEdit(RichEdit: TRichEdit;
  IniAtributesSize: Integer; IniAtributesName: String;
  ListaDeTags: array of String): WideString;
var
  //MemoryStream: TMemoryStream;
  I: Integer;
begin
  Application.CreateForm(TFmTextos2, FmTextos2);
  FmTextos2.FAtributesSize   := IniAtributesSize;
  FmTextos2.FAtributesName   := IniAtributesName;
  FmTextos2.FSaveSit         := 3;
  FmTextos2.FRichEdit        := RichEdit;
  FmTextos2.LBItensMD.Sorted := False;
  for I := Low(ListaDeTags) to high(ListaDeTags) do
    FmTextos2.LBItensMD.Items.Add(ListaDeTags[I]);
  FmTextos2.LBItensMD.Sorted := True;
  //
  MyObjects.TextoEntreRichEdits(RichEdit, FmTextos2.Editor);
  //FmTextos2.DefineNomeFonte(IniAtributesName);

  FmTextos2.FontName.Canvas.Font.Name := 'Tahoma';
  FmTextos2.FontName.FonteNome := 'Tahoma';

  FmTextos2.ShowModal;
  FmTextos2.Destroy;
end;

procedure TUnMyObjects.Entitula(GroupBox: TGroupBox;
              Titulos, Avisos: array of TLabel; Titulo: String;
              RetiraIDForm: Boolean; Alignment: TAlignment;
              Left, Top, FontSize: Integer);
  procedure AjustaFonte(MyLabel: TLabel; Qualidade: Integer);
  var
    tagLOGFONT: TLogFont;
    Font: TFont;
  begin
    Font := MyLabel.Font;
    GetObject(
      Font.Handle,
      SizeOf(TLogFont),
      @tagLOGFONT);
    //
    tagLOGFONT.lfQuality  := Qualidade;
    MyLabel.Font.Handle := CreateFontIndirect(tagLOGFONT);
  end;
const
  T = 18;
var
  Texto: String;
  Tam, I, J: Integer;
begin
  Tam := GroupBox.Width - (2 * Left);
  if RetiraIDForm then
    Texto := Copy(Titulo, T)
  else
    Texto := Titulo;
  //
  j := High(Avisos);
  for I := 0 to J do
  begin
    if I div 2 = I / 2 then
    begin
      Avisos[I].Font.Color := VAR_COR_AVISO_A;
      Avisos[I].Visible := VAR_COR_AVISO_A <> clNone;
    end
    else
    begin
      Avisos[I].Font.Color := VAR_COR_AVISO_C;
      Avisos[I].Visible := VAR_COR_AVISO_C <> clNone;
    end;
    //AjustaFonte(Avisos[I], ANTIALIASED_QUALITY);
    AjustaFonte(Avisos[I], PROOF_QUALITY);
    // N�o sei se precisa, mas l� vai:
    Avisos[I].Invalidate;
  end;

  //

  j := High(Titulos);
  //
  if (j > -1) and (Titulos[0] <> nil) then
  begin
    Titulos[0].Font.Color := VAR_COR_TIT_A;
    Titulos[0].Visible := VAR_COR_TIT_A <> clNone;
    Titulos[0].Left := Left + VAR_COR_POS_A_X;
    Titulos[0].Top := Top + VAR_COR_POS_A_Y;
    Titulos[0].BringToFront;
    //Titulos[0].BringToFront;
  end;
  if (j > 0) and (Titulos[1] <> nil) then
  begin
    Titulos[1].Font.Color := VAR_COR_TIT_B;
    Titulos[1].Visible := VAR_COR_TIT_B <> clNone;
    Titulos[1].Left := Left + VAR_COR_POS_B_X;
    Titulos[1].Top := Top + VAR_COR_POS_B_Y;
    Titulos[1].BringToFront;
    //Titulos[1].BringToFront;
  end;
  if (j > 1) and (Titulos[2] <> nil) then
  begin
    Titulos[2].Font.Color := VAR_COR_TIT_C;
    Titulos[2].Visible := VAR_COR_TIT_C <> clNone;
    Titulos[2].Left := Left;
    Titulos[2].Top := Top;
    Titulos[2].BringToFront;
    //Titulos[2].BringToFront;
  end;
  for I := 0 to J do
  begin
    if Titulos[I] <> nil then
    begin
      // Colocar individual no futuro!
      //AjustaFonte(Titulos[I], ANTIALIASED_QUALITY);
      AjustaFonte(Titulos[I], PROOF_QUALITY);
      Titulos[I].AutoSize := False;
      Titulos[I].Width := Tam;
      Titulos[I].Caption := Texto;
      Titulos[I].Alignment := Alignment;
      Titulos[I].Font.Size := FontSize;
      // N�o sei se precisa, mas l� vai:
      //Titulos[I].Invalidate;
    end;
  end;
  //
  Application.ProcessMessages;
end;

function TUnMyObjects.ErroDeSocket(Errno: word; ErrMsg: String): String;
(*  Anonymous socket error list
� Gepost op: september 06, 2005, 12:17:45 pm �
http://www.wannagame.nl/forum/index.php?PHPSESSID=167f4e1ad1325519ee91295b0163a5a0&topic=1263.0;prev_next=prev
--------------------------------------------------------------------------------*)
begin
  case Errno of
    10004: Result := 'Interrup��o da fun��o de chamada';
    10013: Result := 'Permiss�o negada';
    10014: Result := 'Endere�o inv�lido';
    10022: Result := 'Argumento inv�lido';
    10024: Result := 'Muitos arquivos abertos';
    10035: Result := 'Recursos temporariamente indispon�veis';
    10036: Result := 'Opera��o em progresso agora';
    10037: Result := 'A opera��o ainda est� em progresso';
    10038: Result := 'Opera��o de socket sem socket';
    10039: Result := 'Endere�o de destino obrigat�rio';
    10040: Result := 'Mensagem muito longa';
    10041: Result := 'Protocolo errado para o socket';
    10042: Result := 'Opc�o inv�lida de protocolo';
    10043: Result := 'Protocolo n�o suportado';
    10044: Result := 'Tipo de socket n�o suportado';
    10045: Result := 'Opera��o n�o suportada';
    10046: Result := 'Fam�lia de protocolos n�o suportada';
    10047: Result := 'Endere�o de fam�lia n�o suportado pelo protocolo de fam�lia';
    10048: Result := 'Endere�o j� est� sendo usado';


    10049: Result := 'Endere�o requisitado n�o acess�vel';
    10050: Result := 'A rede n�o est� ativa';
    10051: Result := 'A rede est� inacess�vel';
    10052: Result := 'A conex�o da rede caiu ao resetar';
    10053: Result := 'O software abortou a conex�o';


    10054: Result := 'Conex�o resetada pelo parceiro';


    10055: Result := 'Sem espa�o de mem�ria buffer';
    10056: Result := 'O socket j� est� conectado';
    10057: Result := 'O socket n�o est� conectado';
    10058: Result := 'Imposs�vel enviar ap�s o socket desconectar';
    10060: Result := 'O tempo de conex�o expirou';


    10061: Result := 'Conex�o recusada (ou o aplicativo n�o est� rodando no servidor)';


    10064: Result := 'O hospedeiro caiu';
    10065: Result := 'N�o h� roteamento ao hospedeiro';
    10067: Result := 'Muitos processos';
    10091: Result := 'O subsistema da rede n�o est� dispon�vel';
    10092: Result := 'vers�o WINSOCK.DLL ultrapassada';
    10093: Result := 'O WSAStartup ainda n�o executou com sucesso';

    10094: Result := 'Desligamento normal em progresso';
    11001: Result := 'Hospedeiro n�o encontrado';
    11002: Result := 'Hospedeiro n�o-autoritativo n�o encontrado';
    11003: Result := 'Este � um erro n�o recuper�vel';
    11004: Result := 'Nome v�lido, sem registro para esta requisi��o';

    ///////

    (*10004: 'Interrupted function call'
    10013: 'Permission denied'
    10014: 'Bad address'
    10022: 'Invalid argument'
    10024: 'Too many open files'
    10035: 'Resource temporarily unavailable'
    10036: 'Operation now in progress'
    10037: 'Operation already in progress'
    10038: 'Socket operation on non-socket'
    10039: 'Destination address required'
    10040: 'Message too long'
    10041: 'Protocol wrong type for socket'
    10042: 'Bad protocol option'
    10043: 'Protocol not supported'
    10044: 'Socket type not supported'
    10045: 'Operation not supported'
    10046: 'Protocol family not supported'
    10047: 'Address family not supported by protocol family'
    10048: 'Address already in use'


    10049: 'Cannot assign requested address'
    10050: 'Network is down'
    10051: 'Network is unreachable'
    10052: 'Network dropped connection on reset'
    10053: 'Software caused connection abort'


    10054: 'Connection reset by peer'


    10055: 'No buffer space available'
    10056: 'Socket is already connected'
    10057: 'Socket is not connected'
    10058: 'Cannot send after socket shutdown'
    10060: 'Connection timed out'


    10061: 'Connection refused'


    10064: 'Host is down'
    10065: 'No route to host'
    10067: 'Too many processes'
    10091: 'Network subsystem is unavailable'
    10092: 'WINSOCK.DLL version out of range'
    10093: 'Successful WSAStartup not yet performed'

    10094: 'Graceful shutdown in progress'
    11001: 'Host not found'
    11002: 'Non-authoritative host not found'
    11003: 'This is a non-recoverable error'
    11004: 'Valid name, no data record of requested type'*)

    else Result := ErrMsg;
  end;
end;

procedure TUnMyObjects.UpdMergeDBAvGrid(ASG: TDBAdvGrid;
  Cols: array of Integer);
var
  I: Integer;
begin
  ASG.SplitAllCells();
  //
  for I := Low(Cols) to High(Cols) do
    ASG.MergeColumnCells(Cols[I], True);
  //
  ASG.Invalidate;
end;


function TUnMyObjects.ExcluiLinhaStringGrid(Grade: TStringGrid): Boolean;
var
  x, i, j, Line: Integer;
begin
  Result := False;
  if Geral.MensagemBox(PChar('Confirma a exclus�o de toda linha '+
  IntToStr(Grade.Row)+'?'), 'Exclus�o de Linha Inteira',
  MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    x := 1;
    Line := Grade.Row;
    for i := Line to Grade.RowCount -2  do
    begin
      for j := 1 to Grade.ColCount - x do
      begin
        Grade.Cells[j, i] := Grade.Cells[j, i+1];
      end;
    end;
    for i := 1 to Grade.ColCount do Grade.Cells[i, Grade.RowCount-1] := '';
    Line := Grade.RowCount - x;
    if Line < 2 then
    begin
      //for i := 1 to Grade.ColCount do Grade.Cells[i, Grade.RowCount-1] := '';
    end else Grade.RowCount := Line;
    Result := True;
  end;
end;

procedure TUnMyObjects.ExecutaCmd(cmd: String; MeResult: TMemo);
var
  buf: array[0..4095] of byte;
  si: STARTUPINFO;
  sa: SECURITY_ATTRIBUTES;
  sd: SECURITY_DESCRIPTOR;
  pi: PROCESS_INFORMATION;
  newstdin,newstdout,read_stdout,write_stdin: THandle;
  exitcod,bread,avail: Cardinal;
begin
  if dmkPF.IsWinNT  then
    begin
      InitializeSecurityDescriptor(@sd,SECURITY_DESCRIPTOR_REVISION);
      SetSecurityDescriptorDacl(@sd, true, nil, false);
      sa.lpSecurityDescriptor := @sd;
    end else sa.lpSecurityDescriptor := nil;
  sa.nLength := sizeof(SECURITY_ATTRIBUTES);
  sa.bInheritHandle := true;
  // Tuberias
  if CreatePipe(newstdin,write_stdin,@sa,0) then
  begin
    if CreatePipe(read_stdout,newstdout,@sa,0) then
    begin
      GetStartupInfo(si);
      with si do
      begin
        dwFlags := STARTF_USESTDHANDLES or STARTF_USESHOWWINDOW;
        wShowWindow := SW_HIDE;
        hStdOutput  := newstdout;
        hStdError   := newstdout;
        hStdInput   := newstdin;
      end;
      fillchar(buf,sizeof(buf),0);
      GetEnvironmentVariable('COMSPEC',@buf,sizeof(buf)-1);
      strcat(@buf, PChar(' /c '+cmd));
      if CreateProcess(nil,@buf,nil,nil,TRUE,CREATE_NEW_CONSOLE,nil,nil,si,pi) then
      begin
        if MeResult <> nil then
          MeResult.lines.Clear;
        GetExitCodeProcess(pi.hProcess,exitcod);
        PeekNamedPipe(read_stdout,@buf,sizeof(buf)-1,@bread,@avail,nil);
        while (exitcod = STILL_ACTIVE) or (bread > 0) do
        begin
          Application.ProcessMessages;
          if (bread > 0) then
          begin
            fillchar(buf,sizeof(buf),0);
            if (avail > sizeof(buf)-1) then
              while (bread >= sizeof(buf)-1)  do
              begin
                ReadFile(read_stdout,buf,sizeof(buf)-1,bread,nil);
                if MeResult <> nil then
                  MeResult.lines.text := MeResult.lines.Text + StrPas(@buf);
                fillchar(buf,sizeof(buf),0);
              end else
              begin
                ReadFile(read_stdout,buf,sizeof(buf)-1,bread,nil);
                if MeResult <> nil then
                  MeResult.lines.text := MeResult.lines.Text + StrPas(@buf);
              end;
            end;
            GetExitCodeProcess(pi.hProcess,exitcod);
            PeekNamedPipe(read_stdout,@buf,sizeof(buf)-1,@bread,@avail,nil);
          end;
        end;
       CloseHandle(read_stdout);
       CloseHandle(newstdout);
    end;
    CloseHandle(newstdin);
    CloseHandle(write_stdin);
  end;
end;

function TUnMyObjects.DefineDiretorio(Form: TForm; dmkEdit: TdmkEdit): String;
var
  IniDir, Arquivo: String;
begin
  Result := '';
  if dmkEdit <> nil then
  begin
    IniDir := ExtractFileDir(dmkEdit.Text);
    Arquivo := ExtractFileName(dmkEdit.Text);
  end else
  begin
    IniDir := '';
    Arquivo := '';
  end;
  if FileOpenDialog(Form, IniDir, Arquivo,
  'Selecione o diret�rio', '', [], Arquivo) then
  begin
    Result := ExtractFileDir(Arquivo);
    if dmkEdit <> nil then
      dmkEdit.Text := Result;
  end;
end;

function TUnMyObjects.DefineArquivo1(Form: TForm; dmkEdit: TdmkEdit): Boolean;
var
  IniDir, Arquivo: String;
begin
  Result := False;
  IniDir := ExtractFileDir(dmkEdit.Text);
  Arquivo := ExtractFileName(dmkEdit.Text);
  if FileOpenDialog(Form, IniDir, Arquivo,
  'Selecione o Arquivo', '', [], Arquivo) then
  begin
    dmkEdit.Text := Arquivo;
    Result := True;
  end;
end;

function TUnMyObjects.DefineArquivo2(Form: TForm; dmkEdit: TdmkEdit; Pasta,
Arquivo: String; Prioridade: TPriorityPath; SoArquivo: Boolean): Boolean;
var
  ArqA, ArqB, DirA, DirB, IniDir, IniArq: String;
begin
  Result := False;
  if Prioridade = ppInfo_Ini then
  begin
    DirA := Pasta;
    ArqA := Arquivo;
    DirB := ExtractFileDir(dmkEdit.Text);
    ArqB := ExtractFileName(dmkEdit.Text);
  end;
  if Prioridade = ppAtual_Edit then
  begin
    DirA := ExtractFileDir(dmkEdit.Text);
    ArqA := ExtractFileName(dmkEdit.Text);
    DirB := Pasta;
    ArqB := Arquivo;
  end;
  //
  if Trim(DirA) <> '' then
    IniDir := DirA
  else
    IniDir := DirB;
  if Trim(ArqA) <> '' then
    IniArq := ArqA
  else
    IniArq := ArqB;
  //
  if FileOpenDialog(Form, IniDir, IniArq,
  'Selecione o Arquivo', '', [], IniArq) then
  begin
    Result := True;
    if SoArquivo then
    begin
      if pos(IniDir, IniArq) = 1 then
        dmkEdit.Text := Copy(IniArq, Length(IniDir) + 1)
      else
      begin
        dmkEdit.Text := IniArq;
        Geral.MB_Aviso('Diret�rio base difere do esperado!');
      end;
    end else
      dmkEdit.Text := IniArq;
  end;
end;

procedure TUnMyObjects.DefineCorTextoSitLancto(DBGrid: TDBGrid; Rect: TRect;
  FieldName, TextoSit: String);
var
  Cor: TColor;
begin
  if (FieldName = 'NOMESIT') then
  begin
    if      TextoSit = CO_VENCIDA      then Cor := clRed
    else if TextoSit = CO_PAGTOPARCIAL then Cor := clPurple
    else if TextoSit = CO_PREDATADO    then Cor := $000202CC // averm
    else if TextoSit = CO_QUIT_AUTOM   then Cor := clGray
    else if TextoSit = CO_CANCELADO    then Cor := clSilver
    else if TextoSit = CO_BLOQUEADO    then Cor := clMaroon
    else Cor := clBlack;
    with DBGrid.Canvas do
    begin
      if Cor = clBlack then Font.Style := [] else Font.Style := [fsBold];
      Font.Color := Cor;
      FillRect(Rect);
      TextOut(Rect.Left + 2, Rect.Top  + 2, TextoSit (*Column.Field.DisplayText*));
    end;
  end;
end;

procedure TUnMyObjects.DefineTextoRichEdit(RichEdit: TRichEdit; Texto: String);
var
  MemoryStream: TMemoryStream;
begin
  //
  MemoryStream := TMemoryStream.Create;
  try
    // Write the string to the stream. We want to write from SourceString's
    // data, starting at a pointer to SourceString (this returns a pointer to
    // the first character), dereferenced (this gives the actual character).
    MemoryStream.WriteBuffer(Pointer(Texto)^, Length(Texto));
    // Go back to the start of the stream
    MemoryStream.Position := 0;
    RichEdit.Lines.LoadFromStream(MemoryStream);
    //
  finally
    MemoryStream.Free;
  end;
end;

procedure TUnMyObjects.DefineTituloDBGrid(DBGrid: TDBGrid;
FieldName, Titulo: String);
var
  I: Integer;
begin
  if Titulo <> '' then
  begin
    for I := 0 to DBGrid.Columns.Count - 1 do
      if Uppercase(DBGrid.Columns[I].FieldName) = Uppercase(FieldName) then
        DBGrid.Columns[I].Title.Caption := Titulo;
  end;
end;

procedure TUnMyObjects.DefineValDeCompoEmbeded(Container: TdmkCompoStore;
  Valor: Variant);
var
  Componente: TComponent;
begin
  if Container <> nil then
  begin
    Componente := Container.Component;
    if Componente <> nil then
    begin
      if Componente is TdmkEdit then
        TdmkEdit(Componente).ValueVariant := Valor
      else
        Geral.MB_Aviso('Componente n�o implementado na fun��o:' + #13#10 +
        '"TUnMyObjects.DefineValDeCompoEmbeded()"');
    end;
  end;
end;

procedure TUnMyObjects.DefParams(Acao: Integer);
begin
  if Acao = 0 then
  begin
    MAR_GOTOTABELA      := VAR_GOTOTABELA      ;
    MAR_GOTOMySQLTABLE  := VAR_GOTOMySQLTABLE  ;
    MAR_GOTONEG         := VAR_GOTONEG         ;
    MAR_GOTOCAMPO       := VAR_GOTOCAMPO       ;
    MAR_GOTONOME        := VAR_GOTONOME        ;
    MAR_GOTOMySQLDBNAME := VAR_GOTOMySQLDBNAME ;
    MAR_GOTOVAR         := VAR_GOTOVAR         ;
    //
    //MAR_SQLy.Clear;
    MAR_SQLx.Clear;
    MAR_SQL1.Clear;
    MAR_SQLa.Clear;
    //MAR_SQLy.Text := VAR_SQLy.Text;
    MAR_SQLx.Text := VAR_SQLx.Text;
    MAR_SQL1.Text := VAR_SQL1.Text;
    MAR_SQLa.Text := VAR_SQLa.Text;
  end else begin
    VAR_GOTOTABELA      := MAR_GOTOTABELA      ;
    VAR_GOTOMySQLTABLE  := MAR_GOTOMySQLTABLE  ;
    VAR_GOTONEG         := MAR_GOTONEG         ;
    VAR_GOTOCAMPO       := MAR_GOTOCAMPO       ;
    VAR_GOTONOME        := MAR_GOTONOME        ;
    VAR_GOTOMySQLDBNAME := MAR_GOTOMySQLDBNAME ;
    VAR_GOTOVAR         := MAR_GOTOVAR         ;
    //
    //VAR_SQLy.Clear;
    VAR_SQLx.Clear;
    VAR_SQL1.Clear;
    VAR_SQLa.Clear;
    //VAR_SQLy.Text := MAR_SQLy.Text;
    VAR_SQLx.Text := MAR_SQLx.Text;
    VAR_SQL1.Text := MAR_SQL1.Text;
    VAR_SQLa.Text := MAR_SQLa.Text;
  end;
end;
}

end.
