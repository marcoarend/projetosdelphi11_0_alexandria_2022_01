program ECobranca;

uses
  ExceptionLog,
  SvcMgr,
  Principal in 'Principal.pas' {FmPrincipal: TService},
  Module in 'Module.pas' {Dmod: TDataModule},
  ModCobranca in '..\..\..\..\..\Cobranca\ModCobranca.pas' {DmModCobranca: TDataModule},
  UnDmkProcFunc in '..\..\..\..\..\Geral\Units\UnDmkProcFunc.pas',
  CreateApp in '..\..\CreateApp.pas',
  dmkDAC_PF in '..\..\..\..\..\Outros\MySQL\dmkDAC_PF.pas' {$R *.RES},
  Diario_Tabs in '..\..\..\..\..\Outros\Diario\Diario_Tabs.pas',
  ModuleGeral in 'ModuleGeral.pas' {DModG: TDataModule},
  AxSms_TLB in '..\..\..\..\..\SMS\AxSms_TLB.pas',
  Protocol_Tabs in '..\..\..\..\..\Outros\Protocolos\Protocol_Tabs.pas',
  UMySQLModule in 'Replicadios\UMySQLModule.pas',
  UnMyObjects in 'Replicadios\UnMyObjects.pas',
  UnMailEnv in '..\..\..\..\..\Outros\Email\UnMailEnv.pas';

{$R *.RES}

begin
  // Windows 2003 Server requires StartServiceCtrlDispatcher to be
  // called before CoRegisterClassObject, which can be called indirectly
  // by Application.Initialize. TServiceApplication.DelayInitialize allows
  // Application.Initialize to be called from TService.Main (after
  // StartServiceCtrlDispatcher has been called).
  //
  // Delayed initialization of the Application object may affect
  // events which then occur prior to initialization, such as
  // TService.OnCreate. It is only recommended if the ServiceApplication
  // registers a class object with OLE and is intended for use with
  // Windows 2003 Server.
  //
  // Application.DelayInitialize := True;
  //
  if not Application.DelayInitialize or Application.Installing then
    Application.Initialize;
  Application.CreateForm(TFmPrincipal, FmPrincipal);
  Application.CreateForm(TDmod, Dmod);
  Application.CreateForm(TDmModCobranca, DmModCobranca);
  Application.CreateForm(TDModG, DModG);
  Application.Run;
  //
  {
Windows Server 2003 requer StartServiceCtrlDispatcher ser
chamado CoRegisterClassObject antes, que pode ser chamado indiretamente
por Application.Initialize. TServiceApplication.DelayInitialize permite
Application.Initialize a ser chamado de TService.Main (depois
StartServiceCtrlDispatcher foi chamado).

Inicializa��o atrasada do objeto Application pode afetar
eventos que ocorrem em seguida antes da inicializa��o, tais como o
TService.OnCreate. Ele s� � recomendado se o ServiceApplication
registra um objeto de classe com OLE e destina-se para uso com
Windows Server 2003.
}
  
end.
