unit dirweb;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, Grids,
  DBGrids, ComCtrls, IdBaseComponent, IdComponent, IdCustomTCPServer,
  IdTCPServer, IdCmdTCPServer, IdExplicitTLSClientServerBase, IdFTPServer,
  IdCustomTransparentProxy, IdSocks, IdTCPConnection, IdTCPClient, IdFTP,
  IdSocksServer, dmkGeral, dmkEdit, UnDmkProcFunc, dmkImage, UnDmkEnums;

type
  TFmDirWeb = class(TForm)
    PainelDados: TPanel;
    PainelEdita: TPanel;
    Label9: TLabel;
    Label10: TLabel;
    EdCodigo: TdmkEdit;
    Label149: TLabel;
    EdDescri: TdmkEdit;
    DBGrid1: TDBGrid;
    Label1: TLabel;
    QrDirWeb: TmySQLQuery;
    DsDirWeb: TDataSource;
    EdNome: TdmkEdit;
    RGNivel: TRadioGroup;
    Label2: TLabel;
    EdPasta: TdmkEdit;
    IdFTP1: TIdFTP;
    QrPesqUploads: TmySQLQuery;
    DsPesqUploads: TDataSource;
    QrPesqUploadsCodigo: TAutoIncField;
    QrPesqUploadsNome: TWideStringField;
    QrPesqUploadsArquivo: TWideStringField;
    QrPesqUploadsCond: TIntegerField;
    QrPesqUploadsLk: TIntegerField;
    QrPesqUploadsDataCad: TDateField;
    QrPesqUploadsDataAlt: TDateField;
    QrPesqUploadsUserCad: TIntegerField;
    QrPesqUploadsUserAlt: TIntegerField;
    QrPesqUploadsAlterWeb: TSmallintField;
    QrPesqUploadsAtivo: TSmallintField;
    QrPesqUploadsDirWeb: TIntegerField;
    QrPesqUploadsDIRCODIGO: TAutoIncField;
    QrDirWebCodigo: TAutoIncField;
    QrDirWebNome: TWideStringField;
    QrDirWebDescri: TWideStringField;
    QrDirWebPasta: TWideStringField;
    QrDirWebNivel: TIntegerField;
    QrDirWebLk: TIntegerField;
    QrDirWebDataCad: TDateField;
    QrDirWebDataAlt: TDateField;
    QrDirWebUserCad: TIntegerField;
    QrDirWebUserAlt: TIntegerField;
    QrDirWebAlterWeb: TSmallintField;
    QrDirWebAtivo: TSmallintField;
    QrDirWebNOMENIVEL: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GroupBox1: TGroupBox;
    Panel2: TPanel;
    Panel3: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    GBRodaPe: TGroupBox;
    Panel1: TPanel;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    procedure BtIncluiClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure IpFtpClient1FtpError(Sender: TObject; ErrorCode: Integer;
      const Error: String);
    procedure QrDirWebCalcFields(DataSet: TDataSet);
    //procedure IpFtpClient1FtpStatus(Sender: TObject;
      //StatusCode: TIpFtpStatusCode; const Info: String);
    procedure EdPastaExit(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    //FDesconectando: Boolean;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra : Boolean; SQLType: TSQLType; Codigo : Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure FTPLogin;
    procedure FTPLogout;

  public
    { Public declarations }
  end;

var
  FmDirWeb: TFmDirWeb;
const
  FFormatFloat = '00000';

implementation

uses Module, uploads, UnMyObjects;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmDirWeb.Va(Para: TVaiPara);
begin
  DefParams;
end;
/////////////////////////////////////////////////////////////////////////////////////
procedure TFmDirWeb.DefParams;
begin
  VAR_GOTOTABELA := 'dirweb';
  VAR_GOTOMYSQLTABLE := QrDirWeb;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := nil;
  VAR_GOTOMySQLDBNAME2 := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM dirweb');
  //
  VAR_SQL1.Add('WHERE Codigo=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmDirWeb.MostraEdicao(Mostra : Boolean; SQLType: TSQLType; Codigo : Integer);
begin
  if Mostra then
  begin
    PainelEdita.Visible := True;
    PainelDados.Visible := False;
    if SQLType = stIns then
    begin
      EdCodigo.Text     := '';
      EdNome.Text       := '';
      EdPasta.Text      := '';
      EdDescri.Text     := '';
      RGNivel.ItemIndex := 0;
    end else begin
      EdCodigo.Text     := IntToStr(QrDirWebCodigo.Value);
      EdNome.Text       := QrDirWebNome.Value;
      EdPasta.Text      := QrDirWebPasta.Value;
      EdDescri.Text     := QrDirWebDescri.Value;
      //
      case QrDirWebNivel.Value of
        0: RGNivel.ItemIndex := 0;
        1: RGNivel.ItemIndex := 1;
        7: RGNivel.ItemIndex := 2;
       -1: RGNivel.ItemIndex := 3;
      end;
    end;
    EdNome.SetFocus;
  end else begin
    PainelDados.Visible := True;
    PainelEdita.Visible := False;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmDirWeb.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast); 
end;

procedure TFmDirWeb.AlteraRegistro;
var
  DirWeb : Integer;
begin
  DirWeb := QrDirWebCodigo.Value;
  if QrDirWebCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(dirweb, Dmod.MyDBn, 'dirweb', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(dirweb, Dmod.MyDBn, 'dirweb', 'Codigo');
      MostraEdicao(True, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;          
end;

procedure TFmDirWeb.IncluiRegistro;
var
  Cursor : TCursor;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    MostraEdicao(True, stIns, 0);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmDirWeb.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmDirWeb.BtIncluiClick(Sender: TObject);
begin
  if IdFTP1.Connected then
    begin
      IncluiRegistro;
    end else
    begin
      Geral.MensagemBox('N�o foi posss�vel abrir uma conex�o com o servidor "'+
      Dmod.QrControleWeb_FTPh.Value +'"', 'Erro', MB_OK+MB_ICONWARNING);
    end;
end;

procedure TFmDirWeb.BtSaidaClick(Sender: TObject);
begin
  VAR_SERVICOG := QrDirWebCodigo.Value;
  Close;
end;

procedure TFmDirWeb.BtConfirmaClick(Sender: TObject);
var
  Nome, Descri, Pasta, Caminho, CaminhoN, Raiz: String;
  Nivel: Integer;
begin
  Screen.Cursor := crHourGlass;
  Nome    := EdNome.Text;
  Descri  := EdDescri.Text;
  Pasta   := EdPasta.Text;
  Nivel   := RGNivel.ItemIndex;
  Raiz    := Dmod.QrControleWeb_Raiz.Value;
  if Length(Nome) = 0 then
  begin
    Geral.MensagemBox('Defina um nome.', 'Erro', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if Length(Pasta) = 0 then
  begin
    Geral.MensagemBox('Defina um nome para a pasta.', 'Erro', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if Length(Descri) = 0 then
  begin
    Geral.MensagemBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if Nivel = 0
    then Nivel := 0 // Ambos
  else if Nivel = 1
    then Nivel := 1  // Cod�mino
  else if Nivel = 2
    then Nivel := 7 // S�ndico
  else if Nivel = 3
    then Nivel := -1; // Nenhum
  //
  if ImgTipo.SQLType = stIns then
  begin
    Caminho := Raiz + '/_local_/pdf/' + Pasta;
    IdFTP1.MakeDir(Caminho);
  end else
  begin
    if QrDirWebPasta.Value <> EdPasta.Text then
    begin
      Caminho  := Raiz + '/_local_/pdf/' + QrDirWebPasta.Value;
      CaminhoN := Raiz + '/_local_/pdf/' + Pasta;
      IdFTP1.Rename(Caminho, CaminhoN);
    end;
  end;
  if UMyMod.SQLInsUpd(Dmod.QrWeb, ImgTipo.SQLType, 'dirweb', True,
  [
    'Nome', 'Descri', 'Pasta', 'Nivel'
  ], ['Codigo'],
  [
    Nome, Descri, Pasta, Nivel
  ], [QrDirWebCodigo.Value], True) then
  begin
    QrDirWeb.Close;
    QrDirWeb.Open;
    Screen.Cursor := crDefault;
    MostraEdicao(False, stLok, 0);
  end;    
end;

procedure TFmDirWeb.BtDesisteClick(Sender: TObject);
begin
  MostraEdicao(False, stLok, 0);
end;

procedure TFmDirWeb.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  QrDirWeb.Open;
  DBGrid1.Align := alClient;
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  CriaOForm;
  FTPLogin;
end;

procedure TFmDirWeb.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmDirWeb.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'ServicosG', 'Livres', 99) then
  //BtInclui.Enabled := False;
end;

procedure TFmDirWeb.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmDirWeb.BtAlteraClick(Sender: TObject);
begin
  if IdFTP1.Connected then
    begin
      AlteraRegistro;
    end else
    begin
      Geral.MensagemBox('N�o foi posss�vel abrir uma conex�o com o servidor"'+ Dmod.QrControleWeb_FTPh.Value +'"',
      'Erro', MB_OK+MB_ICONWARNING);
    end;
end;

procedure TFmDirWeb.BtExcluiClick(Sender: TObject);
var
  Codigo : Integer;
  Caminho, Raiz : String;
begin
  if IdFTP1.Connected then
    begin
      Raiz    := Dmod.QrControleWeb_Raiz.Value;
      Caminho := Raiz + '/_local_/pdf/' + QrDirWebPasta.Value;

      if QrDirWeb.RecordCount > 0 then
      begin
        QrPesqUploads.Close;
        QrPesqUploads.Params[0].AsInteger := QrDirWebCodigo.Value;
        QrPesqUploads.Open;

        if QrPesqUploads.RecordCount = 0 then
        begin
          if Geral.MensagemBox('Confirma a exclus�o do �tem "'+
          QrDirWebNome.Value+'" ?',
          'Pergunta de exclus�o',
          MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
          begin
            Screen.Cursor := crHourGlass;

            IdFTP1.RemoveDir(Caminho);

            Codigo := QrDirWebCodigo.Value;
            //
            Dmod.QrWeb.SQL.Clear;
            Dmod.QrWeb.SQL.Add('DELETE FROM dirweb WHERE Codigo=:P0');
            Dmod.QrWeb.Params[0].AsInteger := Codigo;
            Dmod.QrWeb.ExecSQL;
            QrDirWeb.Close;
            QrDirWeb.Open;
            Screen.Cursor := crDefault;
          end;
        end else begin
          Geral.MensagemBox('Este diret�rio n�o pode ser excluido pois existem arquivos cadastrados nele.', 'Erro', MB_OK+MB_ICONWARNING);
        end;
      end;
    end else
    begin
      Geral.MensagemBox('N�o foi posss�vel abrir uma conex�o com o servidor"'+ Dmod.QrControleWeb_FTPh.Value +'"', 'Erro', MB_OK+MB_ICONWARNING);
    end; 
end;

procedure TFmDirWeb.IpFtpClient1FtpError(Sender: TObject;
  ErrorCode: Integer; const Error: String);
begin
  MessageDlg(Error, mtError, [mbOK], 0);
  Close;
end;

procedure TFmDirWeb.QrDirWebCalcFields(DataSet: TDataSet);
var
  Nivel: Integer;
begin
  Nivel := QrDirWebNivel.Value;
  //
  if Nivel = 0
    then QrDirWebNOMENIVEL.Value := 'Ambos'
  else if Nivel = 1
    then QrDirWebNOMENIVEL.Value := 'Cond�mino'
  else if Nivel = 7
    then QrDirWebNOMENIVEL.Value := 'S�ndico'
  else if Nivel = -1
    then QrDirWebNOMENIVEL.Value := 'Nenhum';
end;

procedure TFmDirWeb.FTPLogin;
var
  Host, Usuario, Senha: String;
begin
  Host    := Dmod.QrControleWeb_FTPh.Value;
  Usuario := Dmod.QrControleWeb_FTPu.Value;
  Senha   := Dmod.QrControleWeb_FTPs.Value;

  if IdFTP1.Connected then
    begin
      IdFTP1.Disconnect;
    end
  else
    begin
      IdFTP1.Host := Host;
      IdFTP1.Username := Usuario;
      IdFTP1.Password := Senha;
      IdFTP1.Connect;
    end;
end;

procedure TFmDirWeb.FTPLogout;
begin
  IdFTP1.Disconnect;
  Caption := 'Upload de arquivos';
end;

procedure TFmDirWeb.EdPastaExit(Sender: TObject);
begin
   EdPasta.Text := Geral.SemAcento(EdPasta.Text);
end;

procedure TFmDirWeb.FormDestroy(Sender: TObject);
begin
  FTPLogout;
end;

end.


