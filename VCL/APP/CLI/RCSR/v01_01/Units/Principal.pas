unit Principal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.ExtCtrls, Vcl.Imaging.pngimage, System.UITypes, IPPeerClient, IPPeerServer,
  System.Tether.Manager, System.Tether.AppProfile, Vcl.StdCtrls, Vcl.Menus,
  System.Win.Registry, Vcl.ButtonGroup, Vcl.CategoryButtons, Vcl.ComCtrls,
  Vcl.ToolWin, Vcl.Buttons, Vcl.WinXCtrls, Vcl.WinXCalendars, System.ImageList,
  Vcl.ImgList;

type
  TKeyType     = (ktString, ktBoolean, ktInteger, ktCurrency, ktDate, ktTime, ktDateTime);
  TFmPrincipal = class(TForm)
    PMMenu: TPopupMenu;
    Mostrar1: TMenuItem;
    Fechar1: TMenuItem;
    N1: TMenuItem;
    Inicializao1: TMenuItem;
    Executarnainicializao1: TMenuItem;
    NOexecutarnainicializao1: TMenuItem;
    TrayIcon1: TTrayIcon;
    TmHide: TTimer;
    PnMenu: TPanel;
    imgMenu: TImage;
    SVMenu: TSplitView;
    catMenuItems: TCategoryButtons;
    ImageList1: TImageList;
    Panel1: TPanel;
    RichEdit1: TRichEdit;
    ImgLogo: TImage;
    VCLMediaReceiver: TTetheringManager;
    VCLMediaReceiverApp: TTetheringAppProfile;
    Label1: TLabel;
    TmAtualiza: TTimer;
    Image1: TImage;
    procedure FormCreate(Sender: TObject);
    procedure PMMenuPopup(Sender: TObject);
    procedure Mostrar1Click(Sender: TObject);
    procedure Fechar1Click(Sender: TObject);
    procedure Executarnainicializao1Click(Sender: TObject);
    procedure NOexecutarnainicializao1Click(Sender: TObject);
    procedure TrayIcon1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure TmHideTimer(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure imgMenuClick(Sender: TObject);
    procedure catMenuItemsCategories0Items1Click(Sender: TObject);
    procedure VCLMediaReceiverRequestManagerPassword(const Sender: TObject;
      const ARemoteIdentifier: string; var Password: string);
    procedure VCLMediaReceiverAppResourceReceived(const Sender: TObject;
      const AResource: TRemoteResource);
    procedure SBRefreshClick(Sender: TObject);
    procedure catMenuItemsCategories1Items0Click(Sender: TObject);
    procedure catMenuItemsCategories0Items0Click(Sender: TObject);
    procedure VCLMediaReceiverAppDisconnect(const Sender: TObject;
      const AProfileInfo: TTetheringProfileInfo);
    procedure VCLMediaReceiverAppBeforeConnectProfile(const Sender: TObject;
      const AProfileInfo: TTetheringProfileInfo; var AllowConnect: Boolean);
    procedure ImgLogoDblClick(Sender: TObject);
    procedure TmAtualizaTimer(Sender: TObject);
  private
    { Private declarations }
    FFechar, FShowed, FHideFirst: Boolean;
    FHides: Integer;
    FInvariantFormatSettings: TFormatSettings;
    FConectou: Boolean;
    FIP: String;
    FPerfil: TTetheringProfileInfo;
    function  ReadKey(const Key, Path: String; KeyType: TKeyType;
              DefValue: Variant; xHKEY: HKEY): Variant;
    procedure ExecutaNaInicializacao(Executa: Boolean; Titulo, Programa: String);
    procedure ExecutaTecla(Tecla: Byte);
    procedure ExecutaTecla2(Tecla: array of Byte; SoltarTeclas: Boolean = True);
    procedure SoltarTeclas();
    procedure AtualizaComp();
  public
    { Public declarations }
  end;

var
  FmPrincipal: TFmPrincipal;
const
  CO_Titulo = 'Dermatek - Controle Remoto';

implementation

uses UnGrl_Consts, dmkGeral, UnDmkWeb, Sobre, MyListas;

{$R *.dfm}

procedure TFmPrincipal.AtualizaComp;
begin
  if FConectou then
    VCLMediaReceiverApp.Disconnect(FPerfil);
  //
  VCLMediaReceiverApp.Enabled := False;
  VCLMediaReceiverApp.Enabled := True;
  //
  VCLMediaReceiver.Enabled := False;
  VCLMediaReceiver.Enabled := True;
end;

procedure TFmPrincipal.catMenuItemsCategories0Items0Click(Sender: TObject);
begin
  AtualizaComp;
end;

procedure TFmPrincipal.catMenuItemsCategories0Items1Click(Sender: TObject);
begin
  //Sobre
  SVMenu.Close;
  //
  Application.CreateForm(TFmSobre, FmSobre);
  FmSobre.ShowModal;
  FmSobre.Destroy;
end;

procedure TFmPrincipal.catMenuItemsCategories1Items0Click(Sender: TObject);
var
  Versao: Integer;
  Arq: String;
begin
  //Atualiza
  SVMenu.Close;
  //
  DmkWeb.VerificaAtualizacaoVersao2('DmkRControlSrv', 'DmkRControlSrv',
    CO_VERSAO, CO_DMKID_APP, Versao, Arq, False);
end;

procedure TFmPrincipal.ExecutaNaInicializacao(Executa: Boolean; Titulo,
  Programa: String);
var
  Registry: TRegistry;
begin
  Registry := TRegistry.Create;
  try
    Registry.RootKey := HKEY_CURRENT_USER;
    Registry.OpenKey('\Software\Microsoft\Windows\CurrentVersion\Run', False);
    if Executa then
      Registry.WriteString(Titulo, Programa)
    else
      Registry.WriteString(Titulo, '');
  finally
    Registry.Free;
  end;
end;

procedure TFmPrincipal.Executarnainicializao1Click(Sender: TObject);
begin
  ExecutaNaInicializacao(True, CO_Titulo, Application.ExeName);
end;

procedure TFmPrincipal.Fechar1Click(Sender: TObject);
begin
  FFechar := True;
  Close;
end;

procedure TFmPrincipal.FormActivate(Sender: TObject);
begin
  if (FHides = 0) then
  begin
    FHides         := FHides + 1;
    TmHide.Enabled := True;
  end;
end;

procedure TFmPrincipal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if TrayIcon1 <> nil then
    TrayIcon1.Visible := False;
  TrayIcon1.Visible             := True;
  Application.MainFormOnTaskBar := FShowed;
  //
  FmPrincipal.Hide;
  //
  Application.MainFormOnTaskBar := FShowed;
end;

procedure TFmPrincipal.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
 if (CanClose = True) and (FFechar = False) then
   CanClose := False;
 //
  TrayIcon1.Visible             := True;
  Application.MainFormOnTaskBar := FShowed;
  //
  FmPrincipal.Hide;
  //
  Application.MainFormOnTaskBar := FShowed;
end;

procedure TFmPrincipal.FormCreate(Sender: TObject);
begin
  Geral.DefineFormatacoes;
  //
  SVMenu.Close;
  //
  FInvariantFormatSettings                   := TFormatSettings.Create;
  FInvariantFormatSettings.DecimalSeparator  := '.';
  FInvariantFormatSettings.ThousandSeparator := ',';
  //
  VCLMediaReceiverApp.Text := Geral.IndyComputerName;
  //
  FHides     := 0;
  FShowed    := True;
  FHideFirst := False;
  FFechar    := False;
  //
  catMenuItems.Images := ImageList1;
  TmAtualiza.Interval := 600000; //1 minutos
  TmAtualiza.Enabled  := True;
end;

procedure TFmPrincipal.ImgLogoDblClick(Sender: TObject);
begin
  ShowMessage(Geral.ObtemIP(1));
end;

procedure TFmPrincipal.imgMenuClick(Sender: TObject);
begin
  if SVMenu.Opened then
    SVMenu.Close
  else
    SVMenu.Open;
end;

procedure TFmPrincipal.Mostrar1Click(Sender: TObject);
begin
  FmPrincipal.Visible := True;
end;

procedure TFmPrincipal.NOexecutarnainicializao1Click(Sender: TObject);
begin
  ExecutaNaInicializacao(False, CO_Titulo, Application.ExeName);
end;

procedure TFmPrincipal.PMMenuPopup(Sender: TObject);
var
  Valor: String;
begin
  Valor := ReadKey(CO_Titulo, 'Software\Microsoft\Windows\CurrentVersion\Run', ktString, '', HKEY_CURRENT_USER);
  //
  if Valor <> '' then
  begin
    Executarnainicializao1.Checked   := True;
    NOexecutarnainicializao1.Checked := False;
  end else
  begin
    Executarnainicializao1.Checked   := False;
    NOexecutarnainicializao1.Checked := True;
  end;
end;

procedure TFmPrincipal.TmAtualizaTimer(Sender: TObject);
var
  IP: String;
begin
  IP := Geral.ObtemIP(1);
  //
  if FIP <> IP then
  begin
    AtualizaComp;
    //
    FIP := IP;
  end;
end;

procedure TFmPrincipal.TmHideTimer(Sender: TObject);
begin
  TmHide.Enabled := False;
  //
  if Visible then
  begin
    if not FHideFirst then
    begin
      FHideFirst := True;
      Application.MainFormOnTaskBar := FShowed;
      Hide;
      //FmPrincipal.WindowState :=  wsMinimized;
      Application.MainFormOnTaskBar := FShowed;
      TrayIcon1.Visible := True;
    end;
  end;
end;

procedure TFmPrincipal.TrayIcon1Click(Sender: TObject);
begin
  FmPrincipal.Visible := True;
end;

procedure TFmPrincipal.VCLMediaReceiverAppBeforeConnectProfile(
  const Sender: TObject; const AProfileInfo: TTetheringProfileInfo;
  var AllowConnect: Boolean);
begin
  FConectou := True;
  FPerfil   := AProfileInfo;
  //
  TmAtualiza.Enabled := False;
end;

procedure TFmPrincipal.VCLMediaReceiverAppDisconnect(const Sender: TObject;
  const AProfileInfo: TTetheringProfileInfo);
begin
  FConectou := False;
  //
  TmAtualiza.Enabled := True;
end;

procedure TFmPrincipal.VCLMediaReceiverAppResourceReceived(
  const Sender: TObject; const AResource: TRemoteResource);
var
  Descri, Texto: String;
begin
  Descri := AResource.Hint;
  Texto  := AResource.Value.AsString;
  //
  if UpperCase(Texto) = 'VK_LWIN' then //Menu do Windows
    ExecutaTecla(VK_LWIN)
  else if UpperCase(Texto) = 'VK_LEFT' then //Seta para esquerda
    ExecutaTecla(VK_LEFT)
  else if UpperCase(Texto) = 'VK_RIGHT' then //Seta para direita
    ExecutaTecla(VK_RIGHT)
  else if UpperCase(Texto) = 'VK_UP' then //Seta para cima
    ExecutaTecla(VK_UP)
  else if UpperCase(Texto) = 'VK_DOWN' then //Seta para baixo
    ExecutaTecla(VK_DOWN)
  else if UpperCase(Texto) = 'VK_F5' then //Tecla F5
  begin
    ExecutaTecla(VK_RETURN);//ExecutaTecla(VK_F5)
    SoltarTeclas();
  end
  else if UpperCase(Texto) = 'VK_ESCAPE' then //Tecla Esc
  begin
    ExecutaTecla(VK_ESCAPE);
    SoltarTeclas();
  end
  else if UpperCase(Texto) = 'VK_VOLUME_UP' then //Tecla subir volume
    ExecutaTecla(VK_VOLUME_UP)
  else if UpperCase(Texto) = 'VK_VOLUME_DOWN' then //Tecla baixar volume
    ExecutaTecla(VK_VOLUME_DOWN)
  else if UpperCase(Texto) = 'VK_VOLUME_MUTE' then //Tecla volume mudo
    ExecutaTecla(VK_VOLUME_MUTE)
  else if UpperCase(Texto) = 'VK_MEDIA_PLAY_PAUSE' then //Tecla play / pause
    ExecutaTecla(VK_MEDIA_PLAY_PAUSE)
  else if UpperCase(Texto) = 'VK_MEDIA_STOP' then //Tecla stop
    ExecutaTecla(VK_MEDIA_STOP)
  else if UpperCase(Texto) = 'VK_MEDIA_PREV_TRACK' then //Tecla prev
    ExecutaTecla(VK_MEDIA_PREV_TRACK)
  else if UpperCase(Texto) = 'VK_MEDIA_NEXT_TRACK' then //Tecla next
    ExecutaTecla(VK_MEDIA_NEXT_TRACK)
  else if UpperCase(Texto) = 'VK_MENU+VK_RETURN' then //Teclas Alt + Enter
    ExecutaTecla2([VK_MENU, VK_RETURN])
  else if UpperCase(Texto) = 'VK_OEM_COMMA' then //Tecla , (Quadro branco PowerPoint)
    ExecutaTecla(VK_OEM_COMMA)
  else if UpperCase(Texto) = 'VK_OEM_PERIOD' then //Tecla . (Quadro preto PowerPoint)
    ExecutaTecla(VK_OEM_PERIOD)
  else if UpperCase(Texto) = 'VK_LWIN+VKP' then //Tecla (Projetar (monitor))
    ExecutaTecla2([VK_LWIN, vkP])
  else if UpperCase(Texto) = 'VK_MENU+VK_TAB' then //Teclas Alt + Tab
    ExecutaTecla2([VK_MENU, VK_TAB]);
end;

procedure TFmPrincipal.VCLMediaReceiverRequestManagerPassword(
  const Sender: TObject; const ARemoteIdentifier: string; var Password: string);
begin
  Password := CO_SecureStr_001;
end;

function TFmPrincipal.ReadKey(const Key, Path: String; KeyType: TKeyType;
  DefValue: Variant; xHKEY: HKEY): Variant;
var
  r : TRegistry;
begin
  r := TRegistry.Create(KEY_READ);
  r.RootKey := xHKEY;
  try
    r.OpenKeyReadOnly(Path);
    Result := DefValue;
    if r.ValueExists(Key) then
    begin
      case KeyType of
        ktString: Result := r.ReadString(Key);
        ktBoolean: Result := r.ReadBool(Key);
        ktInteger: Result := r.ReadInteger(Key);
        ktCurrency: Result := r.ReadCurrency(Key);
        ktDate: Result := r.ReadDate(Key);
        ktTime: Result := r.ReadTime(Key);
        ktDateTime: Result := r.ReadDateTime(Key);
        //ktBinary: Result := r.ReadBinaryData(Key);
      end;
    end;
  finally
    r.Free;
  end;
end;

procedure TFmPrincipal.ExecutaTecla2(Tecla: array of Byte;
  SoltarTeclas: Boolean = True);
var
  I: Integer;
begin
  for I := Low(Tecla) to High(Tecla) do
  begin
    Keybd_event(Tecla[i], MapVirtualKey(VK_DOWN, 0), 0, 0);
  end;
  if SoltarTeclas then
  begin
    for I := Low(Tecla) to High(Tecla) do
    begin
      Keybd_event(Tecla[i], MapVirtualKey(VK_DOWN, 0), KEYEVENTF_KEYUP, 0);
    end;
  end;
end;

procedure TFmPrincipal.SBRefreshClick(Sender: TObject);
begin
  AtualizaComp;
end;

procedure TFmPrincipal.SoltarTeclas();
begin
  if GetKeyState(VK_MENU) < 0 then
    Keybd_event(VK_MENU, MapVirtualKey(VK_DOWN, 0), KEYEVENTF_KEYUP, 0);
  if GetKeyState(VK_TAB) < 0 then
    Keybd_event(VK_TAB, MapVirtualKey(VK_DOWN, 0), KEYEVENTF_KEYUP, 0);
  if GetKeyState(VK_LWIN) < 0 then
    Keybd_event(VK_LWIN, MapVirtualKey(VK_DOWN, 0), KEYEVENTF_KEYUP, 0);
  if GetKeyState(vkP) < 0 then
    Keybd_event(vkP, MapVirtualKey(VK_DOWN, 0), KEYEVENTF_KEYUP, 0);
end;

procedure TFmPrincipal.ExecutaTecla(Tecla: Byte);
begin
  Keybd_event(Tecla, MapVirtualKey(VK_DOWN, 0), 0, 0);
  Keybd_event(Tecla, MapVirtualKey(VK_DOWN, 0), KEYEVENTF_KEYUP, 0);
end;

end.
