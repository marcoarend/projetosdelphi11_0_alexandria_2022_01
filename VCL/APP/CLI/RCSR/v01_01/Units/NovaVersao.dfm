object FmNovaVersao: TFmNovaVersao
  Left = 200
  Top = 108
  BorderStyle = bsDialog
  Caption = 'Nova vers'#227'o'
  ClientHeight = 495
  ClientWidth = 679
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  PixelsPerInch = 120
  TextHeight = 16
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 409
    Width = 679
    Height = 86
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 0
    object PnSaiDesis: TPanel
      Left = 500
      Top = 18
      Width = 177
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 13
        Top = 4
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 18
      Width = 498
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BitBtn2: TBitBtn
        Tag = 19
        Left = 18
        Top = 4
        Width = 148
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Baixar'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BitBtn2Click
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 679
    Height = 59
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_M: TGroupBox
      Left = 0
      Top = 0
      Width = 679
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 0
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 305
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Download de vers'#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 59
    Width = 679
    Height = 350
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object LaLink: TLabel
      Left = 0
      Top = 151
      Width = 679
      Height = 20
      Cursor = crHandPoint
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Alignment = taCenter
      Caption = '___'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -17
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = LaLinkClick
      OnMouseEnter = LaLinkMouseEnter
      OnMouseLeave = LaLinkMouseLeave
      ExplicitWidth = 31
    end
    object STAppArqName: TStaticText
      Left = 0
      Top = 0
      Width = 679
      Height = 31
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Alignment = taCenter
      AutoSize = False
      Caption = '[.EXE]'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -17
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object STAppArqTitle: TStaticText
      Left = 0
      Top = 31
      Width = 679
      Height = 31
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Alignment = taCenter
      AutoSize = False
      Caption = '[TITULO]'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -17
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
    end
    object Panel1: TPanel
      Left = 0
      Top = 205
      Width = 679
      Height = 145
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 2
      object Label1: TLabel
        Left = 10
        Top = 10
        Width = 66
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Progresso:'
      end
      object GroupBox2: TGroupBox
        Left = 280
        Top = 58
        Width = 100
        Height = 75
        Caption = ' Percentual: '
        TabOrder = 0
        object Label9: TLabel
          Left = 10
          Top = 20
          Width = 68
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '% Baixado:'
        end
        object EdPerRec: TEdit
          Left = 10
          Top = 41
          Width = 80
          Height = 24
          Alignment = taRightJustify
          TabOrder = 0
          Text = '0,00'
        end
      end
      object GroupBox3: TGroupBox
        Left = 390
        Top = 58
        Width = 280
        Height = 75
        Caption = ' Percentual: '
        TabOrder = 1
        object Label6: TLabel
          Left = 10
          Top = 20
          Width = 34
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Total:'
        end
        object Label7: TLabel
          Left = 100
          Top = 20
          Width = 80
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Transcorrido:'
        end
        object Label8: TLabel
          Left = 190
          Top = 20
          Width = 57
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Restante:'
        end
        object EdTmpTot: TEdit
          Left = 10
          Top = 41
          Width = 80
          Height = 24
          Alignment = taRightJustify
          TabOrder = 0
          Text = '00:00:00'
        end
        object EdTmpRun: TEdit
          Left = 100
          Top = 41
          Width = 80
          Height = 24
          Alignment = taRightJustify
          TabOrder = 1
          Text = '00:00:00'
        end
        object EdTmpFal: TEdit
          Left = 190
          Top = 41
          Width = 80
          Height = 24
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -14
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 2
          Text = '00:00:00'
        end
      end
      object GroupBox1: TGroupBox
        Left = 10
        Top = 58
        Width = 260
        Height = 75
        Caption = ' Tamanho do arquivo: '
        TabOrder = 2
        object Label2: TLabel
          Left = 10
          Top = 20
          Width = 34
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Total:'
        end
        object LaTamTot: TLabel
          Left = 93
          Top = 44
          Width = 33
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'bytes'
        end
        object LaTamRec: TLabel
          Left = 217
          Top = 44
          Width = 33
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'bytes'
        end
        object Label3: TLabel
          Left = 133
          Top = 20
          Width = 63
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Recebido:'
        end
        object EdTamTot: TEdit
          Left = 10
          Top = 41
          Width = 80
          Height = 24
          Alignment = taRightJustify
          TabOrder = 0
          Text = '0'
        end
        object EdTamRec: TEdit
          Left = 133
          Top = 41
          Width = 80
          Height = 24
          Alignment = taRightJustify
          TabOrder = 1
          Text = '0'
        end
      end
      object ProgressBar1: TProgressBar
        Left = 10
        Top = 30
        Width = 660
        Height = 17
        TabOrder = 3
      end
    end
    object STAviso: TStaticText
      Left = 0
      Top = 62
      Width = 679
      Height = 30
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Alignment = taCenter
      Caption = '-----'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -23
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
    end
    object StVersao: TStaticText
      Left = 0
      Top = 92
      Width = 679
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Alignment = taCenter
      AutoSize = False
      Caption = '___'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -23
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
    end
  end
  object IdHTTP1: TIdHTTP
    OnWork = IdHTTP1Work
    OnWorkBegin = IdHTTP1WorkBegin
    OnWorkEnd = IdHTTP1WorkEnd
    AllowCookies = True
    ProtocolVersion = pv1_0
    ProxyParams.BasicAuthentication = False
    ProxyParams.ProxyPort = 0
    Request.ContentLength = -1
    Request.ContentRangeEnd = -1
    Request.ContentRangeStart = -1
    Request.ContentRangeInstanceLength = -1
    Request.Accept = 'text/html, */*'
    Request.BasicAuthentication = False
    Request.UserAgent = 'Mozilla/3.0 (compatible; Indy Library)'
    Request.Ranges.Units = 'bytes'
    Request.Ranges = <>
    HTTPOptions = [hoForceEncodeParams]
    Left = 132
    Top = 84
  end
  object Archiver: TZipForge
    ExtractCorruptedFiles = False
    CompressionLevel = clFastest
    CompressionMode = 1
    CurrentVersion = '6.80 '
    SpanningMode = smNone
    SpanningOptions.AdvancedNaming = True
    SpanningOptions.FirstVolumeSize = 0
    SpanningOptions.VolumeSize = vsAutoDetect
    SpanningOptions.CustomVolumeSize = 65536
    Options.FlushBuffers = True
    Options.OEMFileNames = True
    InMemory = False
    Zip64Mode = zmDisabled
    UnicodeFilenames = True
    EncryptionMethod = caPkzipClassic
    Left = 104
    Top = 84
  end
end
