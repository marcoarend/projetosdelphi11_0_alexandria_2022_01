unit NovaVersao;

interface

uses Winapi.Windows, System.SysUtils, System.Classes, Vcl.Graphics, Vcl.Forms,
  Vcl.Controls, Vcl.StdCtrls, Vcl.Buttons, Vcl.ExtCtrls, Winapi.ShellAPI,
  Vcl.ComCtrls, Winapi.Messages, IdComponent, ZipForge, IdBaseComponent,
  IdTCPConnection, IdTCPClient, IdHTTP;

type
  TFmNovaVersao = class(TForm)
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BitBtn2: TBitBtn;
    PnCabeca: TPanel;
    GB_M: TGroupBox;
    LaTitulo1C: TLabel;
    PainelDados: TPanel;
    STAppArqName: TStaticText;
    STAppArqTitle: TStaticText;
    Panel1: TPanel;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    Label1: TLabel;
    IdHTTP1: TIdHTTP;
    Archiver: TZipForge;
    Label9: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    EdPerRec: TEdit;
    EdTmpTot: TEdit;
    EdTmpRun: TEdit;
    EdTmpFal: TEdit;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    LaTamTot: TLabel;
    LaTamRec: TLabel;
    Label3: TLabel;
    EdTamTot: TEdit;
    EdTamRec: TEdit;
    ProgressBar1: TProgressBar;
    STAviso: TStaticText;
    StVersao: TStaticText;
    LaLink: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure IdHTTP1Work(ASender: TObject; AWorkMode: TWorkMode;
      AWorkCount: Int64);
    procedure IdHTTP1WorkBegin(ASender: TObject; AWorkMode: TWorkMode;
      AWorkCountMax: Int64);
    procedure IdHTTP1WorkEnd(ASender: TObject; AWorkMode: TWorkMode);
    procedure LaLinkClick(Sender: TObject);
    procedure LaLinkMouseEnter(Sender: TObject);
    procedure LaLinkMouseLeave(Sender: TObject);
  private
    { Private declarations }
    FWorkCount, FWorkCountMax: Integer;
    FTimeIni: TDateTime;
    function  DownloadFileHTTP(Fonte, Destino: String): Boolean;
    function  RenameAppOlder(AppName: String): Boolean;
    function  DesZipaArquivo(ArqNome, DirToExtract: String): Boolean;
    function  FecharOutraAplicacao(Nome: String; Handle: THandle): Integer;
    function  WinExecAndWaitOrNot(LinhaDeComando: string; Visibility: Integer;
              Memo: TMemo; Wait: Boolean): Longword;
    procedure RenameAppOlder_Cancel;
  public
    { Public declarations }
    FAplic, FVersao: Integer;
    FURLArq: String;
  end;

var
  FmNovaVersao: TFmNovaVersao;

implementation

uses UnInternalConsts, dmkGeral, UnDmkWeb, MyListas, UnDmkEnums, UnGrl_Vars;

{$R *.dfm}

procedure TFmNovaVersao.BitBtn2Click(Sender: TObject);
var
  Destino, Fonte, Arq, DestFim: String;
  Renomear: Boolean;
begin
  Renomear := True;
  Fonte    := FURLArq;
  Destino  := CO_DIR_RAIZ_DMK + '\';
  //
  ForceDirectories(Destino);
  //
  if Pos('.zip', STAppArqName.Caption) = 0 then
   Destino := Destino + STAppArqName.Caption + '.zip'
  else
    Destino := Destino + STAppArqName.Caption;
  //
  if FileExists(Destino) then
    DeleteFile(Destino);
  //
  STAviso.Caption := 'Aguarde... Baixando arquivo!';
  Application.ProcessMessages;
  //
  if DownloadFileHTTP(Fonte, Destino) then
  begin
    STAviso.Caption := 'Aguarde... Renomeando arquivo!';
    Application.ProcessMessages;
    //
    Arq := ExtractFilePath(Application.ExeName) + STAppArqName.Caption +
             ExtractFileExt(Application.ExeName);

    if STAppArqName.Caption <> ExtractFileName(Application.ExeName) then
    begin
      Renomear := FileExists(Arq);
    end;
    if Renomear then
    begin
      if not RenameAppOlder(Arq) then
      begin
        STAviso.Caption := 'Erro... N�o foi poss�vel renomear o arquivo!';
        Application.ProcessMessages;
        Exit;
      end;
    end;
    STAviso.Caption := 'Aguarde... Descompactando arquivo!';
    Application.ProcessMessages;
    //
    DestFim := ExtractFilePath(Application.ExeName);
    //
    if DesZipaArquivo(Destino, DestFim) then
    begin
      STAviso.Caption := 'OK... Download conclu�do!';
      Application.ProcessMessages;
      Geral.MB_Info('OK... Download conclu�do!');
      //
      DmkWeb.HistoricoDeAlteracoes(CO_DMKID_APP, CO_VERSAO, dtConfigura);
      //
      if STAppArqName.Caption = ExtractFileName(Application.ExeName) then
      begin
        Application.Terminate;
      end else
      begin
        //terminar aplicativo e come�ar novo
        FecharOutraAplicacao(STAppArqName.Caption, Handle);
        WinExecAndWaitOrNot(Arq, 0, nil, False);
        // For�ar encerramento
        Application.Terminate;
      end;
      VAR_DOWNLOAD_OK := True;
    end else
    begin
      RenameAppOlder_Cancel;
      STAviso.Caption := 'Erro ao descompactar arquivo!';
      Application.ProcessMessages;
    end;
  end else
    STAviso.Caption := 'Erro durante o download de ' + FURLArq;
end;

procedure TFmNovaVersao.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

function TFmNovaVersao.DesZipaArquivo(ArqNome, DirToExtract: String): Boolean;
begin
  try
    with Archiver do
    begin
      FileName := ArqNome;
      //
      OpenArchive(fmOpenRead + fmShareDenyWrite);
      //
      BaseDir := DirToExtract;
      //
      ExtractFiles('*.*');
      CloseArchive;
      //
      ProgressBar1.Position := 0;
    end;
    Result := True;
  finally
     ;
  end;
end;

function TFmNovaVersao.DownloadFileHTTP(Fonte, Destino: String): Boolean;
var
  MyFile: TFileStream;
begin
  FWorkCount := 0;
  FWorkCountMax := 0;
  MyFile := TFileStream.Create(Destino, fmCreate);
  try
    IdHTTP1.Get(Fonte, MyFile);
    Result := True;
  finally
    MyFile.Free;
  end;
end;

function TFmNovaVersao.FecharOutraAplicacao(Nome: String;
  Handle: THandle): Integer;
var
  TopWindow: HWND;
  WinName, WinClass: array[0..80] of Char;
  x: Integer;
  NoError: Boolean;
  WindowList1: TList;
begin
  Result      := -1;
  WindowList1 := nil;
  try
    WindowList1 := TList.Create;
    TopWindow   := Handle;
    NoError     := EnumWindows(@GetWindow, LongInt(@TopWindow));
    //
    if NoError then
      Exit;
    //
    for x := 0 to WindowList1.Count -1 do
    begin
      GetWindowText(HWND(WindowList1[x]), WinName, SizeOf(WinName)-1);
      GetClassName(HWND(WindowList1[x]), WinClass, SizeOf(WinName)-1);
      //
      if WinClass = 'TApplication' then
        if String(WinName) = Nome then
          Result := SendMessage(HWND(WindowList1[x]),
          WM_CLOSE, 0, 0);
    end;
  finally
    if WindowList1 <> nil then
      WindowList1.Free;
  end;
end;

procedure TFmNovaVersao.IdHTTP1Work(ASender: TObject; AWorkMode: TWorkMode;
  AWorkCount: Int64);
var
  PerNum, Tam: Double;
  Niv: Integer;
  Nom: String;
  TimeAtu, TimeTot: TDateTime;
begin
  PerNum     := 0;
  TimeAtu    := Now() - FTimeIni;
  FWorkCount := AWorkCount;
  //
  if FWorkCountMax > 0 then
    PerNum := FWorkCount / FWorkCountMax;
  //
  if PerNum > 0 then
    TimeTot := TimeAtu / PerNum
  else
    TimeTot := 0;
  //
  PerNum                := PerNum * 100;
  ProgressBar1.Position := AWorkCount;
  STAviso.Caption       := 'Download em andamento...';
  Geral.AdequaMedida(grandezaBytes, FWorkCount, 0, Tam, Niv, Nom);
  //
  EdTamRec.Text    := Geral.FFT(Tam, Niv, siPositivo);
  LaTamRec.Caption := Nom;
  EdPerRec.Text    := Geral.FFT(PerNum, 2, siPositivo);
  EdTmpRun.Text    := Geral.FDT(TimeAtu, 100);
  EdTmpTot.Text    := Geral.FDT(TimeTot, 100);
  EdTmpFal.Text    := Geral.FDT(TimeTot - TimeAtu, 100);
  Update;
  Application.ProcessMessages;
end;

procedure TFmNovaVersao.IdHTTP1WorkBegin(ASender: TObject; AWorkMode: TWorkMode;
  AWorkCountMax: Int64);
var
  Tam: Double;
  Niv: Integer;
  Nom: String;
begin
  FTimeIni              := Now();
  ProgressBar1.Position := 0;
  STAviso.Caption       := 'Download sendo iniciado! Aguarde...';
  FWorkCount            := 0;
  FWorkCountMax         := AWorkCountMax;
  ProgressBar1.Max      := FWorkCountMax;
  //
  Geral.AdequaMedida(grandezaBytes, FWorkCountMax, 0, Tam, Niv, Nom);
  //
  EdTamTot.Text    := Geral.FFT(Tam, Niv, siPositivo);
  LaTamTot.Caption := Nom;
  //
  Update;
  Application.ProcessMessages;
end;

procedure TFmNovaVersao.IdHTTP1WorkEnd(ASender: TObject; AWorkMode: TWorkMode);
begin
  ProgressBar1.Position := ProgressBar1.Max;
  STAviso.Caption       := 'Download Conclu�do! Aguarde a descompacta��o...';
end;

procedure TFmNovaVersao.LaLinkClick(Sender: TObject);
var
  Link: String;
begin
  Link := 'http://www.dermatek.net.br/?page=hisalt&aplicativo=' +
    Geral.FF0(FAplic) + '&versao=' + Geral.FF0(FVersao) + '&logid='+ VAR_WEB_IDLOGIN;
  //
  DmkWeb.MostraWebBrowser(Link);
end;

procedure TFmNovaVersao.LaLinkMouseEnter(Sender: TObject);
begin
  LaLink.Font.Color := clBlue;
  LaLink.Font.Style := [fsBold, fsUnderline];
end;

procedure TFmNovaVersao.LaLinkMouseLeave(Sender: TObject);
begin
  LaLink.Font.Color := clBlue;
  LaLink.Font.Style := [fsBold];
end;

function TFmNovaVersao.RenameAppOlder(AppName: String): Boolean;
var
  App, Older: String;
  poi: Integer;
begin
  App := ExtractFileName(AppName);
  poi := Pos('.', App);
  //
  if poi > 0 then
    App := Copy(App, 1, poi-1);
  //
  Older := ExtractFilePath(AppName) + App +
             Geral.FF0(Geral.VersaoInt2006(Geral.FileVerInfo(AppName, 3)))  +
             ExtractFileExt(AppName);
  //
  if FileExists(Older) then
  begin
    Geral.MB_Erro('Imposs�vel renomear ' + AppName + ' para '+Older+', pois este j� existe!');
    Result := False;
  end else
  begin
    RenameFile(AppName, Older);
    Result := True;
  end;
end;

procedure TFmNovaVersao.RenameAppOlder_Cancel;
var
  Older: String;
begin
  Older := ExtractFilePath(Application.ExeName) + '\Old\' + Application.Title +
           Geral.FF0(Geral.VersaoInt2006(Geral.FileVerInfo(Application.ExeName,
           3))) + '.exe';
  //
  RenameFile(Older, Application.ExeName);
end;

function TFmNovaVersao.WinExecAndWaitOrNot(LinhaDeComando: string;
  Visibility: Integer; Memo: TMemo; Wait: Boolean): Longword;
var { by Pat Ritchey }
  zAppName: array[0..512] of Char;
  zCurDir: array[0..255] of Char;
  WorkDir: string;
  StartupInfo: TStartupInfo;
  ProcessInfo: TProcessInformation;
  //
  Handle : THandle;
  Saida: Boolean;
  Text: TextFile;
  ArqTemp, S: String;
begin
  if (Visibility = SW_HIDE) and (Memo <> nil) then
    Saida := True
  else
    Saida := False;
  //
  if Saida then
  begin
    ArqTemp := 'C:\_cltest.txt';
    //
    if FileExists(ArqTemp) then
      DeleteFile(ArqTemp);
    //
    GetStartupInfo(StartupInfo);
    //
    Handle := CreateFile(PChar(ArqTemp), GENERIC_READ or GENERIC_WRITE, 0, nil,
              CREATE_ALWAYS,0,0);
  end else
    Handle := 0;
  //
  VAR_APLICATION_TERMINATE := False;
  StrPCopy(zAppName, LinhaDeComando);
  GetDir(0, WorkDir);
  StrPCopy(zCurDir, WorkDir);
  FillChar(StartupInfo, SizeOf(StartupInfo), #0);
  //
  if Saida then
    StartupInfo.hStdOutput := Handle;
  //
  StartupInfo.cb := SizeOf(StartupInfo);
  //
  if Saida then
    StartupInfo.dwFlags := STARTF_USESHOWWINDOW or STARTF_USESTDHANDLES
  else
    StartupInfo.dwFlags := STARTF_USESHOWWINDOW;
  //
  StartupInfo.wShowWindow := Visibility;
  //
  if not CreateProcess(nil,
    zAppName, // pointer to command line string
    nil, // pointer to process security attributes
    nil, // pointer to thread security attributes
    False, // handle inheritance flag
    CREATE_NEW_CONSOLE or // creation flags
    NORMAL_PRIORITY_CLASS,
    nil, //pointer to new environment block
    nil, // pointer to current directory name
    StartupInfo, // pointer to STARTUPINFO
    ProcessInfo) // pointer to PROCESS_INF
  then
    Result := WAIT_FAILED
  else
  begin
    if Wait then
    begin
      WaitForSingleObject(ProcessInfo.hProcess, INFINITE);
      GetExitCodeProcess(ProcessInfo.hProcess, Result);
      CloseHandle(ProcessInfo.hProcess);
      CloseHandle(ProcessInfo.hThread);
      VAR_APLICATION_TERMINATE := True;
    end;
  end;
  if Saida then
  begin
    CloseHandle(Handle);
    if FileExists(ArqTemp) then
    begin
      AssignFile(Text, ArqTemp);
      // Est� gerando erro I/O 32
      Reset(Text);
      while not Eof(Text) do
      begin
        Readln(Text, S);
        Memo.Lines.Add(S);
      end;
      CloseFile(Text);
    end;
  end;
end;

end.

