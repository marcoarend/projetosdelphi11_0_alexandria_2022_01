unit UnDmkWeb;

interface

uses Vcl.Controls, Vcl.Forms, System.SysUtils, Winapi.Windows, Winapi.WinInet,
  Winapi.ShellAPI;

type
  TAcao     = (dtConfigura, dtMostra);
  TUnDmkWeb = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  RemoteConnection(): Boolean; // TemInternet()
    function  VerificaAtualizacaoVersao2(AppName, AppTitle: String; VersaoAtual,
              CodAplic: Integer; var Versao: Integer; var Arquivo: String;
              ApenasVerifica: Boolean): Boolean;
    procedure HistoricoDeAlteracoes(DmkId_App, Versao: Integer; Acao: TAcao);
    procedure MostraWebBrowser(URL: String);
  end;

var
  DmkWeb: TUnDmkWeb;

implementation

uses dmkGeral, NovaVersao, dmkSOAP;

{ TUnDmkWeb }

procedure TUnDmkWeb.HistoricoDeAlteracoes(DmkId_App, Versao: Integer;
  Acao: TAcao);
var
  NVersao, IDMsg: Integer;
  Link: String;
  Res: ArrayOfStrings;
begin
  case Acao of
    dtConfigura:
      Geral.WriteAppKeyCU('NovaVersao', Application.Title, '1', ktInteger);
    dtMostra:
    begin
      NVersao := Geral.ReadAppKeyCU('NovaVersao', Application.Title, ktInteger, 0);

      if NVersao = 1 then
      begin
        if RemoteConnection then
        begin
          try
            Res   := GetVCLWebServicePortType2.SOAP_VerificaHistoricoDeVersao(DmkId_App, Versao);
            IDMsg := Geral.IMV(Res[0]);

            if IDMsg = 101 then
            begin
              Link := 'http://www.dermatek.net.br/?page=hisalt&aplicativo=' +
                        Geral.FF0(DmkId_App) + '&versao=' + Geral.FF0(Versao);

              dmkWeb.MostraWebBrowser(Link);
            end;
            Geral.WriteAppKeyCU('NovaVersao', Application.Title, 0, ktInteger);
          except
            ;
          end;
        end;
      end;
    end;
  end;
end;

procedure TUnDmkWeb.MostraWebBrowser(URL: String);
begin
  ShellExecute(Application.Handle, 'open', PChar(URL), '', '', 1);
end;

function TUnDmkWeb.RemoteConnection: Boolean;
var
  flags: DWORD;
begin
  if InternetGetConnectedState(@flags, 0) then
  begin
    if not InternetCheckConnection('http://www.dermatek.net.br/', 1, 0) then
    begin
      Result := False;
    end else
      Result := True;
  end else
    Result := False;
end;

function TUnDmkWeb.VerificaAtualizacaoVersao2(AppName, AppTitle: String;
  VersaoAtual, CodAplic: Integer; var Versao: Integer; var Arquivo: String;
  ApenasVerifica: Boolean): Boolean;

  procedure MostraJanelaVersao(Versao: Integer; Arq, ArqTxt: String);
  begin
    Application.CreateForm(TFmNovaVersao, FmNovaVersao);
    FmNovaVersao.FAplic                := CodAplic;
    FmNovaVersao.FVersao               := Versao;
    FmNovaVersao.STAppArqTitle.Caption := AppTitle;
    FmNovaVersao.STAviso.Caption       := 'H� uma nova vers�o de: "' + AppTitle + '" no site da dermatek:';
    FmNovaVersao.STAviso.Visible       := True;
    FmNovaVersao.StVersao.Caption      := Geral.VersaoTxt2006(Versao);
    FmNovaVersao.FURLArq               := Arq;
    //
    if CodAplic <> 0 then
      FmNovaVersao.STAppArqName.Caption := AppName
    else
      FmNovaVersao.STAppArqName.Caption := ArqTxt;
    //
    FmNovaVersao.LaLink.Caption := 'Clique aqui para visualizar o hist�rico de atualiza��es';
    FmNovaVersao.LaLink.Visible := True;
    FmNovaVersao.ShowModal;
    FmNovaVersao.Destroy;
  end;

  function VerificaVersao(var Arq, ArqTxt: String; var MsgCod: Integer;
    var MsgTxt: String): Integer;
  var
    Resul: ArrayOfStrings;
    Txt: String;
    Cod, Versao: Integer;
  begin
    try
      Versao := VersaoAtual;
      Resul  := GetVCLWebServicePortType2.SOAP_VerificaNovaVersaoDmk2(AppName,
                  CodAplic, Versao, False, '', '');
    except
      SetLength(Resul, 2);
      Resul[0] := Geral.FF0(-1);
      Resul[1] := 'Falha ao verificar vers�o!';
      Resul[2] := '0';
      Resul[3] := '';
      Resul[4] := '';
    end;
    //
    if Geral.IMV(Resul[0]) = -1 then
      Txt := Resul[1]
    else
      //Est� vindo em ANSI Txt := Utf8ToAnsi(Resul[1]);
      Txt := Resul[1];
    //
    Cod    := Geral.IMV(Resul[0]);
    Versao := Geral.IMV(Resul[2]);
    Arq    := Resul[3];
    ArqTxt := Resul[4];
    MsgCod := Cod;
    MsgTxt := Txt;
    Result := Versao;
  end;

var
  MsgCod: Integer;
  Arq, ArqTxt, MsgTxt: String;
begin
  if RemoteConnection then
  begin
    try
      Screen.Cursor := crHourGlass;
      Result        := False;
      Versao        := VerificaVersao(Arq, ArqTxt, MsgCod, MsgTxt);
      //
      if MsgCod = 105 then //Vers�o obtida com �xito!
      begin
        if not ApenasVerifica then
        begin
          MostraJanelaVersao(Versao, Arq, ArqTxt);
        end;
      end else
      if MsgCod = 106 then //N�o h� nova vers�o!
      begin
        if not ApenasVerifica then
          Geral.MB_Aviso(Geral.FF0(MsgCod) + ' - ' + MsgTxt + sLineBreak +
            'Vers�o atual: ' + Geral.VersaoTxt2006(VersaoAtual) + sLineBreak +
            'Vers�o no site: ' + Geral.VersaoTxt2006(Versao));
      end else
      begin
        Geral.MB_Aviso(Geral.FF0(MsgCod) + ' - ' + MsgTxt);
      end;
    finally
      //if MsgCod in [105, 106] then
      if MsgCod = 105 then
        Result := True;
      //
      Screen.Cursor := crDefault;
    end;
  end else
    Result := False;
  //
  if Result then
    Arquivo := ChangeFileExt(ArqTxt, '')
  else
    Arquivo := '';
end;

end.
