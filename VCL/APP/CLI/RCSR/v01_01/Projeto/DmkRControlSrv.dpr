program DmkRControlSrv;

uses
  Vcl.Forms,
  Principal in '..\Units\Principal.pas' {FmPrincipal},
  UnGrl_Consts in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Consts.pas',
  UnDmkEnums in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnDmkEnums.pas',
  Vcl.Themes,
  Vcl.Styles,
  Sobre in '..\..\..\..\..\UTL\_FRM\v01_01\Sobre.pas' {FmSobre},
  dmkGeral in '..\..\..\..\..\..\dmkComp\dmkGeral.pas',
  dmkMsg in '..\..\..\..\..\..\dmkComp\dmkMsg.pas' {FmdmkMsg},
  dmkImage in '..\..\..\..\..\..\dmkComp\dmkImage.pas',
  UnInternalConsts in '..\..\..\..\..\UTL\_UNT\v01_01\UnInternalConsts.pas',
  UnMsgInt in '..\..\..\..\..\UTL\_UNT\v01_01\UnMsgInt.pas',
  dmkSelLista in '..\..\..\..\..\..\dmkComp\dmkSelLista.pas' {FmdmkSelLista},
  MyListas in '..\Units\MyListas.pas',
  UnGrl_AllOS in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_AllOS.pas',
  UnitMD5 in '..\..\..\..\..\UTL\Encrypt\v01_01\UnitMD5.pas',
  NovaVersao in '..\Units\NovaVersao.pas' {FmNovaVersao},
  UnDmkWeb in '..\Units\UnDmkWeb.pas',
  dmkSOAP in '..\..\..\..\..\MDL\WEB\v01_01\WebService\dmkSOAP.pas',
  UnGrl_Vars in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Vars.pas',
  UnMyLinguas in '..\..\..\..\..\..\MultiOS\AllOS\Listas\UnMyLinguas.pas',
  UnGrl_OS in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\Windows\UnGrl_OS.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  TStyleManager.TrySetStyle('Windows10 SlateGray');
  Application.CreateForm(TFmPrincipal, FmPrincipal);
  Application.Run;
end.
