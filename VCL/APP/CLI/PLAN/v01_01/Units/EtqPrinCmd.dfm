object FmEtqPrinCmd: TFmEtqPrinCmd
  Left = 339
  Top = 185
  Caption = 'ETQ-PRINT-002 :: Linha de Comando'
  ClientHeight = 568
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 520
    Width = 1008
    Height = 48
    Align = alBottom
    TabOrder = 0
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object Panel2: TPanel
      Left = 896
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Fechar'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object BitBtn1: TBitBtn
      Tag = 253
      Left = 112
      Top = 4
      Width = 90
      Height = 40
      Caption = '&C'#243'digo'
      NumGlyphs = 2
      TabOrder = 2
      OnClick = BitBtn1Click
    end
    object BitBtn3: TBitBtn
      Left = 444
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Cria linha(s)'
      NumGlyphs = 2
      TabOrder = 3
      OnClick = BitBtn3Click
    end
    object BtImprime: TBitBtn
      Tag = 5
      Left = 628
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Imprime'
      NumGlyphs = 2
      TabOrder = 4
      OnClick = BtImprimeClick
    end
    object CkLinhaALinha: TCheckBox
      Left = 776
      Top = 4
      Width = 85
      Height = 17
      Caption = 'Linha a linha.'
      TabOrder = 5
    end
    object CkDOS: TCheckBox
      Left = 728
      Top = 4
      Width = 41
      Height = 17
      Caption = 'DOS.'
      TabOrder = 6
      OnClick = CkDOSClick
    end
    object CkTodoLabel: TCheckBox
      Left = 728
      Top = 24
      Width = 73
      Height = 17
      Caption = 'Todo label.'
      TabOrder = 7
      OnClick = CkDOSClick
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    Caption = 'Linha de Comando'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 924
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
    object LaTipo: TdmkLabel
      Left = 925
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Inclus'#227'o'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stIns
      ExplicitLeft = 620
      ExplicitTop = 2
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 495
    Width = 1008
    Height = 25
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object CkContinuar: TCheckBox
      Left = 8
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      TabOrder = 0
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 169
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object Panel9: TPanel
      Left = 0
      Top = 0
      Width = 817
      Height = 169
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      object Label18: TLabel
        Left = 8
        Top = 4
        Width = 32
        Height = 13
        Caption = 'Grupo:'
        FocusControl = DBEdit12
      end
      object Label17: TLabel
        Left = 224
        Top = 4
        Width = 38
        Height = 13
        Caption = 'Modelo:'
        FocusControl = DBEdit11
      end
      object Label13: TLabel
        Left = 756
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdit7
      end
      object Label19: TLabel
        Left = 8
        Top = 44
        Width = 89
        Height = 13
        Caption = 'Tipo de impress'#227'o:'
        FocusControl = DBEdit14
      end
      object Label20: TLabel
        Left = 224
        Top = 44
        Width = 106
        Height = 13
        Caption = 'Modelo da impressora:'
        FocusControl = DBEdit16
      end
      object Label21: TLabel
        Left = 520
        Top = 44
        Width = 93
        Height = 13
        Caption = 'Porta de impress'#227'o:'
        FocusControl = DBEdit17
      end
      object Label22: TLabel
        Left = 636
        Top = 44
        Width = 42
        Height = 13
        Caption = 'N'#186' porta:'
        FocusControl = DBEdit19
      end
      object DBEdit12: TDBEdit
        Left = 8
        Top = 20
        Width = 36
        Height = 21
        DataField = 'TypeUso'
        DataSource = FmEtqPrinCad.DsEtqPrinCad
        TabOrder = 0
      end
      object DBEdit13: TDBEdit
        Left = 48
        Top = 20
        Width = 173
        Height = 21
        DataField = 'NOMETYPEUSO'
        DataSource = FmEtqPrinCad.DsEtqPrinCad
        TabOrder = 1
      end
      object DBEdit11: TDBEdit
        Left = 224
        Top = 20
        Width = 56
        Height = 21
        DataField = 'CodUsu'
        DataSource = FmEtqPrinCad.DsEtqPrinCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
      end
      object DBEdit8: TDBEdit
        Left = 284
        Top = 20
        Width = 469
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = FmEtqPrinCad.DsEtqPrinCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
      end
      object DBEdit7: TDBEdit
        Left = 756
        Top = 20
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = FmEtqPrinCad.DsEtqPrinCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 4
      end
      object DBEdit14: TDBEdit
        Left = 8
        Top = 60
        Width = 36
        Height = 21
        DataField = 'TypePrn'
        DataSource = FmEtqPrinCad.DsEtqPrinCad
        TabOrder = 5
      end
      object DBEdit15: TDBEdit
        Left = 48
        Top = 60
        Width = 173
        Height = 21
        DataField = 'NOMETYPEPRN'
        DataSource = FmEtqPrinCad.DsEtqPrinCad
        TabOrder = 6
      end
      object DBEdit16: TDBEdit
        Left = 224
        Top = 60
        Width = 293
        Height = 21
        DataField = 'Modelo'
        DataSource = FmEtqPrinCad.DsEtqPrinCad
        TabOrder = 7
      end
      object DBEdit17: TDBEdit
        Left = 520
        Top = 60
        Width = 36
        Height = 21
        DataField = 'PortaImp'
        DataSource = FmEtqPrinCad.DsEtqPrinCad
        TabOrder = 8
      end
      object DBEdit18: TDBEdit
        Left = 560
        Top = 60
        Width = 72
        Height = 21
        DataField = 'NOMEPORTAIMP'
        DataSource = FmEtqPrinCad.DsEtqPrinCad
        TabOrder = 9
      end
      object DBEdit19: TDBEdit
        Left = 636
        Top = 60
        Width = 45
        Height = 21
        DataField = 'PortaIdx'
        DataSource = FmEtqPrinCad.DsEtqPrinCad
        TabOrder = 10
      end
      object GroupBox2: TGroupBox
        Left = 8
        Top = 84
        Width = 285
        Height = 41
        Caption = ' Usar configura'#231#245'es padr'#245'es: '
        TabOrder = 11
        object DBCheckBox1: TDBCheckBox
          Left = 12
          Top = 20
          Width = 49
          Height = 17
          Caption = 'In'#237'cio.'
          DataField = 'DefaultIni'
          DataSource = FmEtqPrinCad.DsEtqPrinCad
          TabOrder = 0
        end
        object DBCheckBox2: TDBCheckBox
          Left = 112
          Top = 20
          Width = 53
          Height = 17
          Caption = 'Final.'
          DataField = 'DefaultIni'
          DataSource = FmEtqPrinCad.DsEtqPrinCad
          TabOrder = 1
        end
      end
      object GroupBox7: TGroupBox
        Left = 8
        Top = 128
        Width = 285
        Height = 41
        Caption = 'Padr'#245'es de configura'#231#227'o: '
        TabOrder = 12
        object DBCheckBox3: TDBCheckBox
          Left = 12
          Top = 20
          Width = 113
          Height = 17
          Caption = 'Retorno autom'#225'tico.'
          DataField = 'DefFeedBac'
          DataSource = FmEtqPrinCad.DsEtqPrinCad
          TabOrder = 0
        end
        object DBCheckBox4: TDBCheckBox
          Left = 132
          Top = 20
          Width = 145
          Height = 17
          Caption = 'Tamanho m'#237'nimo de pixel.'
          DataField = 'DefMinPixel'
          DataSource = FmEtqPrinCad.DsEtqPrinCad
          TabOrder = 1
        end
      end
      object GroupBox8: TGroupBox
        Left = 296
        Top = 84
        Width = 517
        Height = 85
        Caption = ' Formul'#225'rio: '
        TabOrder = 13
        object GBMedidasLabel: TGroupBox
          Left = 181
          Top = 15
          Width = 284
          Height = 68
          Align = alLeft
          Caption = ' Label (em ??): '
          TabOrder = 0
          object Label14: TLabel
            Left = 8
            Top = 16
            Width = 39
            Height = 13
            Caption = 'Largura:'
          end
          object Label15: TLabel
            Left = 76
            Top = 16
            Width = 30
            Height = 13
            Caption = 'Altura:'
          end
          object Label16: TLabel
            Left = 144
            Top = 16
            Width = 53
            Height = 13
            Caption = 'Marg. esq.:'
          end
          object Label24: TLabel
            Left = 212
            Top = 16
            Width = 64
            Height = 13
            Caption = 'Gap (coluna):'
          end
          object DBEdit3: TDBEdit
            Left = 8
            Top = 32
            Width = 64
            Height = 21
            DataField = 'LabelLarg'
            DataSource = FmEtqPrinCad.DsEtqPrinCad
            TabOrder = 0
          end
          object DBEdit4: TDBEdit
            Left = 76
            Top = 32
            Width = 64
            Height = 21
            DataField = 'LabelAltu'
            DataSource = FmEtqPrinCad.DsEtqPrinCad
            TabOrder = 1
          end
          object DBEdit5: TDBEdit
            Left = 144
            Top = 32
            Width = 64
            Height = 21
            DataField = 'LabelMEsq'
            DataSource = FmEtqPrinCad.DsEtqPrinCad
            TabOrder = 2
          end
          object DBEdit6: TDBEdit
            Left = 212
            Top = 32
            Width = 64
            Height = 21
            DataField = 'LabelLGap'
            DataSource = FmEtqPrinCad.DsEtqPrinCad
            TabOrder = 3
          end
        end
        object GroupBox10: TGroupBox
          Left = 2
          Top = 15
          Width = 179
          Height = 68
          Align = alLeft
          Caption = ' Geral: '
          TabOrder = 1
          object LaFormLargura: TLabel
            Left = 108
            Top = 16
            Width = 62
            Height = 13
            Caption = 'Largura (cm):'
          end
          object Label23: TLabel
            Left = 60
            Top = 16
            Width = 21
            Height = 13
            Caption = 'DPI:'
            FocusControl = DBEdit20
          end
          object Label3: TLabel
            Left = 12
            Top = 16
            Width = 41
            Height = 13
            Caption = 'Colunas:'
            FocusControl = DBEdit1
          end
          object DBEdit20: TDBEdit
            Left = 60
            Top = 32
            Width = 45
            Height = 21
            DataField = 'DPI'
            DataSource = FmEtqPrinCad.DsEtqPrinCad
            TabOrder = 0
          end
          object DBEdit1: TDBEdit
            Left = 12
            Top = 32
            Width = 45
            Height = 21
            DataField = 'Colunas'
            DataSource = FmEtqPrinCad.DsEtqPrinCad
            TabOrder = 1
          end
          object DBEdit2: TDBEdit
            Left = 108
            Top = 32
            Width = 64
            Height = 21
            DataField = 'FormuLarg'
            DataSource = FmEtqPrinCad.DsEtqPrinCad
            TabOrder = 2
          end
        end
      end
      object DBRadioGroup1: TDBRadioGroup
        Left = 684
        Top = 44
        Width = 129
        Height = 38
        Caption = ' Unidade de medida: '
        Columns = 2
        DataField = 'Measure'
        DataSource = FmEtqPrinCad.DsEtqPrinCad
        Items.Strings = (
          'M'#233'trico'
          'Inches')
        TabOrder = 14
        TabStop = True
        Values.Strings = (
          '0'
          '1')
        OnChange = DBRadioGroup1Change
      end
    end
    object Panel10: TPanel
      Left = 817
      Top = 0
      Width = 191
      Height = 169
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 191
        Height = 169
        Align = alClient
        Caption = ' Linha de comando: '
        TabOrder = 0
        object Label1: TLabel
          Left = 56
          Top = 68
          Width = 34
          Height = 13
          Caption = 'Ordem:'
        end
        object Label2: TLabel
          Left = 104
          Top = 68
          Width = 14
          Height = 13
          Caption = 'ID:'
        end
        object Label7: TLabel
          Left = 12
          Top = 68
          Width = 36
          Height = 13
          Caption = 'Coluna:'
        end
        object EdOrdem: TdmkEdit
          Left = 56
          Top = 84
          Width = 44
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '1'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '1'
          QryCampo = 'Ordem'
          UpdCampo = 'Ordem'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 1
          ValWarn = False
        end
        object EdControle: TdmkEdit
          Left = 104
          Top = 84
          Width = 76
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Controle'
          UpdCampo = 'Controle'
          UpdType = utInc
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object RGTipo: TdmkRadioGroup
          Left = 8
          Top = 16
          Width = 173
          Height = 49
          Caption = ' Tipo de sequencial de instru'#231#227'o: '
          Columns = 3
          Enabled = False
          ItemIndex = 1
          Items.Strings = (
            'In'#237'cio'
            'Dados'
            'Final')
          TabOrder = 0
          OnClick = RGTipoClick
          QryCampo = 'Tipo'
          UpdCampo = 'Tipo'
          UpdType = utYes
          OldValor = 0
        end
        object EdColuna: TdmkEdit
          Left = 12
          Top = 84
          Width = 40
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '1'
          ValMax = '4'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '1'
          QryCampo = 'Coluna'
          UpdCampo = 'Coluna'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 1
          ValWarn = False
        end
      end
    end
  end
  object GroupBox3: TGroupBox
    Left = 0
    Top = 217
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Texto codificado: '
    TabOrder = 4
    object EdConfig: TdmkEdit
      Left = 12
      Top = 16
      Width = 989
      Height = 21
      TabOrder = 0
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Config'
      UpdCampo = 'Config'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 261
    Width = 1008
    Height = 234
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 5
    object TabSheet1: TTabSheet
      Caption = ' Dados  '
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel6: TPanel
        Left = 0
        Top = 0
        Width = 1000
        Height = 206
        Align = alClient
        TabOrder = 0
        object MeResult: TMemo
          Left = 600
          Top = 1
          Width = 399
          Height = 204
          Align = alRight
          Color = 4539717
          Font.Charset = ANSI_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Lucida Console'
          Font.Style = [fsBold]
          Lines.Strings = (
            'Teste 1'
            'C:\Meus Documentos')
          ParentFont = False
          TabOrder = 0
        end
        object Panel4: TPanel
          Left = 1
          Top = 1
          Width = 366
          Height = 204
          Align = alClient
          ParentBackground = False
          TabOrder = 1
          object MeLinesCmd: TMemo
            Left = 1
            Top = 140
            Width = 364
            Height = 63
            Align = alBottom
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Courier New'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
          end
          object TCLinhas: TTabControl
            Left = 1
            Top = 1
            Width = 364
            Height = 139
            Align = alClient
            TabOrder = 1
            OnChange = TCLinhasChange
            object DBGTxt: TDBGrid
              Left = 4
              Top = 6
              Width = 356
              Height = 129
              Align = alClient
              DataSource = DataSource1
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              OnKeyDown = DBGTxtKeyDown
              Columns = <
                item
                  Expanded = False
                  FieldName = 'TxtBar'
                  Title.Caption = 'Tipo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Coluna'
                  Width = 32
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Linha'
                  Width = 32
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'AutoSeq'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DataHora'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Texto'
                  Visible = True
                end>
            end
          end
        end
        object ListBox1: TListBox
          Left = 367
          Top = 1
          Width = 233
          Height = 204
          Align = alRight
          ItemHeight = 13
          TabOrder = 2
        end
      end
    end
  end
  object Query: TABSQuery
    CurrentVersion = '7.91 '
    DatabaseName = 'MEMORY'
    InMemory = True
    ReadOnly = False
    BeforePost = QueryBeforePost
    RequestLive = True
    Left = 12
    Top = 12
    object QueryColuna: TIntegerField
      FieldName = 'Coluna'
      DisplayFormat = '000'
    end
    object QueryLinha: TIntegerField
      FieldName = 'Linha'
      DisplayFormat = '000'
    end
    object QueryTexto: TWideStringField
      FieldName = 'Texto'
      Size = 255
    end
    object QueryAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QueryTxtBar: TWideStringField
      FieldName = 'TxtBar'
      Size = 3
    end
    object QueryDataHora: TFloatField
      FieldName = 'DataHora'
    end
    object QueryAutoSeq: TIntegerField
      FieldName = 'AutoSeq'
    end
  end
  object DataSource1: TDataSource
    DataSet = Query
    Left = 40
    Top = 12
  end
  object Quer2: TABSQuery
    CurrentVersion = '7.91 '
    DatabaseName = 'MEMORY'
    InMemory = True
    ReadOnly = False
    RequestLive = True
    Left = 68
    Top = 12
    object IntegerField1: TIntegerField
      FieldName = 'Coluna'
      DisplayFormat = '000'
    end
    object IntegerField2: TIntegerField
      FieldName = 'Linha'
      DisplayFormat = '000'
    end
    object StringField1: TWideStringField
      FieldName = 'Texto'
      Size = 255
    end
    object SmallintField1: TSmallintField
      FieldName = 'Ativo'
    end
    object StringField2: TWideStringField
      FieldName = 'TxtBar'
      Size = 3
    end
  end
  object QrAllLin: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM etqprincmd'
      'WHERE Codigo=:P0'
      'AND Tipo=:P1'
      'ORDER BY Ordem')
    Left = 100
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrAllLinCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAllLinTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrAllLinOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrAllLinControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrAllLinConfig: TWideStringField
      FieldName = 'Config'
      Size = 255
    end
    object QrAllLinColuna: TIntegerField
      FieldName = 'Coluna'
    end
  end
end
