object FmEPC_Argox_PPLA: TFmEPC_Argox_PPLA
  Left = 339
  Top = 185
  Caption = 'ETQ-PROGR-001 :: Programa'#231#227'o Argox - PPLA'
  ClientHeight = 555
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 507
    Width = 1008
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object Panel2: TPanel
      Left = 896
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object BtAcao: TBitBtn
      Left = 304
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Teste'
      NumGlyphs = 2
      TabOrder = 2
      OnClick = BtAcaoClick
    end
    object CkDOS: TCheckBox
      Left = 404
      Top = 4
      Width = 41
      Height = 17
      Caption = 'DOS.'
      TabOrder = 3
    end
    object CkTodoLabel: TCheckBox
      Left = 404
      Top = 24
      Width = 73
      Height = 17
      Caption = 'Todo label.'
      TabOrder = 4
    end
    object CkLinhaALinha: TCheckBox
      Left = 452
      Top = 4
      Width = 85
      Height = 17
      Caption = 'Linha a linha.'
      TabOrder = 5
    end
    object CkContinuar: TCheckBox
      Left = 116
      Top = 20
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      TabOrder = 6
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    Caption = 'Programa'#231#227'o Argox - PPLA'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 1006
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 459
    Align = alClient
    TabOrder = 0
    object Panel4: TPanel
      Left = 1
      Top = 1
      Width = 1006
      Height = 457
      Align = alClient
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 0
      object PageControl1: TPageControl
        Left = 0
        Top = 0
        Width = 585
        Height = 457
        ActivePage = TabSheet1
        Align = alLeft
        TabOrder = 0
        object TabSheet2: TTabSheet
          Caption = ' C'#243'digo de barras '
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 869
          ExplicitHeight = 0
          object Panel3: TPanel
            Left = 0
            Top = 0
            Width = 577
            Height = 429
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            ExplicitWidth = 869
            object RGCodeBar_Bar: TRadioGroup
              Left = 0
              Top = 0
              Width = 577
              Height = 168
              Align = alTop
              Caption = ' Tipo de c'#243'digo de barras: '
              Columns = 3
              ItemIndex = 0
              Items.Strings = (
                '?'
                'A: C'#243'digo 3 de 9'
                'B: UPC-A'
                'C: UPC-E'
                'D: Intercalado 2 de 5 (I25)'
                'E: C'#243'digo 128 (A, B e C)'
                'F: EAN-13'
                'G: EAN-8'
                'H: HBIC ("A:" + conf. m'#243'dulo 43)'
                'I: Coda bar'
                'J: Intercalado 2 de 5 (+ m'#243'dulo 10)'
                'K: Plessey'
                'L: Modelo "J" + barras horizontais'
                'M: UPC2'
                'N: UPC5'
                'O: C'#243'digo 93'
                'P: Postnet (Correios)'
                'Q: UCC/EAN Codigo 128'
                'R: UCC/EAN Codigo 128 K-MART'
                'T: Telepen'
                'U: UPS MaxiCode'
                'V: FIM (Facing Identification Mark)'
                'W: DataMatrix'
                'Z: PDF-417')
              TabOrder = 0
              OnClick = RGCodeBar_BarClick
              ExplicitWidth = 869
              ExplicitHeight = 161
            end
            object Panel9: TPanel
              Left = 0
              Top = 168
              Width = 577
              Height = 96
              Align = alTop
              TabOrder = 1
              ExplicitTop = 197
              ExplicitWidth = 774
              object RGCodBarE_Bar: TRadioGroup
                Left = 185
                Top = 1
                Width = 45
                Height = 94
                Align = alLeft
                Caption = ' E: '
                ItemIndex = 2
                Items.Strings = (
                  'A'
                  'B'
                  'C')
                TabOrder = 3
                OnClick = RGCodBarE_BarClick
              end
              object RGShowTxt_Bar: TRadioGroup
                Left = 133
                Top = 1
                Width = 52
                Height = 94
                Align = alLeft
                Caption = ' Texto: '
                ItemIndex = 1
                Items.Strings = (
                  'N'#227'o'
                  'Sim')
                TabOrder = 1
                OnClick = RGShowTxt_BarClick
              end
              object RGOrienta_Bar: TRadioGroup
                Left = 1
                Top = 1
                Width = 132
                Height = 94
                Align = alLeft
                Caption = ' Orienta'#231#227'o: '
                ItemIndex = 0
                Items.Strings = (
                  '1 - Retrato'
                  '2 - Paisagem reversa'
                  '3 - Retrato reverso'
                  '4 - Paisagem')
                TabOrder = 0
                OnClick = RGOrienta_BarClick
              end
              object RGLargMul_Bar: TRadioGroup
                Left = 403
                Top = 1
                Width = 173
                Height = 94
                Align = alClient
                Caption = ' Largura da barra larga: '
                Columns = 5
                ItemIndex = 0
                Items.Strings = (
                  '0'
                  '1'
                  '2'
                  '3'
                  '4'
                  '5'
                  '6'
                  '7'
                  '8'
                  '9'
                  'A'
                  'B'
                  'C'
                  'D'
                  'E'
                  'F'
                  'G'
                  'H'
                  'I'
                  'J'
                  'K'
                  'L'
                  'M'
                  'N'
                  'O')
                TabOrder = 2
                OnClick = RGLargMul_BarClick
                ExplicitLeft = 401
                ExplicitWidth = 172
              end
              object RGAltuMul_Bar: TRadioGroup
                Left = 230
                Top = 1
                Width = 173
                Height = 94
                Align = alLeft
                Caption = ' Largura da barra esteita: '
                Columns = 5
                ItemIndex = 0
                Items.Strings = (
                  '0'
                  '1'
                  '2'
                  '3'
                  '4'
                  '5'
                  '6'
                  '7'
                  '8'
                  '9'
                  'A'
                  'B'
                  'C'
                  'D'
                  'E'
                  'F'
                  'G'
                  'H'
                  'I'
                  'J'
                  'K'
                  'L'
                  'M'
                  'N'
                  'O')
                TabOrder = 4
                OnClick = RGAltuMul_BarClick
                ExplicitLeft = 229
              end
            end
            object Panel10: TPanel
              Left = 0
              Top = 360
              Width = 577
              Height = 69
              Align = alClient
              ParentBackground = False
              TabOrder = 2
              ExplicitWidth = 869
              object Label1: TLabel
                Left = 52
                Top = 8
                Width = 117
                Height = 13
                Caption = 'Texto (valor)  a codificar:'
              end
              object Label6: TLabel
                Left = 8
                Top = 8
                Width = 29
                Height = 13
                Caption = 'Linha:'
              end
              object Label13: TLabel
                Left = 8
                Top = 52
                Width = 446
                Height = 13
                Caption = 
                  'Observa'#231#227'o: Para excluir as linhas geradas, selecione o c'#243'digo d' +
                  'e barras "?".'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clRed
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object EdLinha_Bar: TdmkEdit
                Left = 8
                Top = 24
                Width = 40
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnExit = EdLinha_BarExit
                OnKeyDown = EdLinha_BarKeyDown
              end
              object EdTexto_Bar: TEdit
                Left = 52
                Top = 24
                Width = 425
                Height = 21
                TabOrder = 1
                Text = '12345678901234567890'
                OnExit = EdTexto_BarExit
                OnKeyDown = EdTexto_BarKeyDown
              end
              object BitBtn2: TBitBtn
                Tag = 253
                Left = 480
                Top = 12
                Width = 90
                Height = 40
                Caption = '&C'#243'digo'
                NumGlyphs = 2
                TabOrder = 2
                OnClick = BitBtn2Click
              end
            end
            object Panel15: TPanel
              Left = 0
              Top = 264
              Width = 577
              Height = 96
              Align = alTop
              TabOrder = 3
              ExplicitTop = 221
              ExplicitWidth = 869
              object GroupBox1: TGroupBox
                Left = 1
                Top = 1
                Width = 228
                Height = 94
                Align = alLeft
                Caption = ' Coordenadas: (* = padr'#227'o)'
                TabOrder = 0
                object Label2: TLabel
                  Left = 4
                  Top = 32
                  Width = 92
                  Height = 13
                  Caption = 'Margem do rodap'#233':'
                end
                object Label3: TLabel
                  Left = 4
                  Top = 52
                  Width = 88
                  Height = 13
                  Caption = 'Margem esquerda:'
                end
                object LaMedida_Bar: TLabel
                  Left = 136
                  Top = 12
                  Width = 17
                  Height = 13
                  Caption = 'cm:'
                end
                object Label5: TLabel
                  Left = 180
                  Top = 12
                  Width = 41
                  Height = 13
                  Caption = 'Posi'#231#227'o:'
                end
                object Label7: TLabel
                  Left = 4
                  Top = 72
                  Width = 131
                  Height = 13
                  Caption = 'Altura do c'#243'digo de barras*:'
                end
                object EdBarAltuCm_Bar: TdmkEdit
                  Left = 136
                  Top = 69
                  Width = 40
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 2
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 3
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,000'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                  OnChange = EdBarAltuCm_BarChange
                end
                object EdBarAltuPix_Bar: TdmkEdit
                  Left = 180
                  Top = 69
                  Width = 40
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 5
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 3
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '000'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  OnChange = EdBarAltuPix_BarChange
                end
                object EdTxtTopoCm_Bar: TdmkEdit
                  Left = 136
                  Top = 27
                  Width = 40
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 3
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,000'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                  OnChange = EdTxtTopoCm_BarChange
                end
                object EdTxtTopoPix_Bar: TdmkEdit
                  Left = 180
                  Top = 27
                  Width = 40
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 3
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 4
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0000'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  OnChange = EdTxtTopoPix_BarChange
                end
                object EdTxtLeftCm_Bar: TdmkEdit
                  Left = 136
                  Top = 48
                  Width = 40
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 3
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,000'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                  OnChange = EdTxtLeftCm_BarChange
                end
                object EdTxtLeftPix_Bar: TdmkEdit
                  Left = 180
                  Top = 48
                  Width = 40
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 4
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 4
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0000'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  OnChange = EdTxtLeftPix_BarChange
                end
              end
            end
          end
        end
        object TabSheet1: TTabSheet
          Caption = ' Texto '
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel5: TPanel
            Left = 0
            Top = 0
            Width = 577
            Height = 109
            Align = alTop
            ParentBackground = False
            TabOrder = 0
            object RGOrienta_Txt: TRadioGroup
              Left = 1
              Top = 1
              Width = 133
              Height = 107
              Align = alLeft
              Caption = ' Orienta'#231#227'o: '
              ItemIndex = 0
              Items.Strings = (
                '1 - Retrato'
                '2 - Paisagem reversa'
                '3 - Retrato reverso'
                '4 - Paisagem')
              TabOrder = 0
              OnClick = RGOrienta_TxtClick
            end
            object RGTipFont_Txt: TRadioGroup
              Left = 134
              Top = 1
              Width = 88
              Height = 107
              Align = alLeft
              Caption = ' Fonte: '
              Columns = 3
              ItemIndex = 9
              Items.Strings = (
                '0'
                '1'
                '2'
                '3'
                '4'
                '5'
                '6'
                '7'
                '8'
                '9'
                ':'
                '?')
              TabOrder = 1
              OnClick = RGTipFont_TxtClick
            end
            object RGLargMul_Txt: TRadioGroup
              Left = 222
              Top = 1
              Width = 177
              Height = 107
              Align = alLeft
              Caption = ' Multiplicador da largura: '
              Columns = 5
              ItemIndex = 1
              Items.Strings = (
                '0'
                '1'
                '2'
                '3'
                '4'
                '5'
                '6'
                '7'
                '8'
                '9'
                'A'
                'B'
                'C'
                'D'
                'E'
                'F'
                'G'
                'H'
                'I'
                'J'
                'K'
                'L'
                'M'
                'N'
                'O')
              TabOrder = 2
              OnClick = RGLargMul_TxtClick
            end
            object RGAltuMul_Txt: TRadioGroup
              Left = 399
              Top = 1
              Width = 177
              Height = 107
              Align = alClient
              Caption = ' Multiplicador da altura: '
              Columns = 5
              ItemIndex = 1
              Items.Strings = (
                '0'
                '1'
                '2'
                '3'
                '4'
                '5'
                '6'
                '7'
                '8'
                '9'
                'A'
                'B'
                'C'
                'D'
                'E'
                'F'
                'G'
                'H'
                'I'
                'J'
                'K'
                'L'
                'M'
                'N'
                'O')
              TabOrder = 3
              OnClick = RGAltuMul_TxtClick
            end
          end
          object Panel6: TPanel
            Left = 0
            Top = 109
            Width = 577
            Height = 320
            Align = alClient
            ParentBackground = False
            TabOrder = 1
            object Panel14: TPanel
              Left = 1
              Top = 1
              Width = 575
              Height = 84
              Align = alTop
              TabOrder = 0
              object RGCoor: TGroupBox
                Left = 1
                Top = 1
                Width = 221
                Height = 82
                Align = alLeft
                Caption = ' Coordenadas: '
                TabOrder = 0
                object Label8: TLabel
                  Left = 4
                  Top = 32
                  Width = 92
                  Height = 13
                  Caption = 'Margem do rodap'#233':'
                end
                object Label9: TLabel
                  Left = 4
                  Top = 56
                  Width = 88
                  Height = 13
                  Caption = 'Margem esquerda:'
                end
                object LaMedida_Txt: TLabel
                  Left = 100
                  Top = 12
                  Width = 17
                  Height = 13
                  Caption = 'cm:'
                end
                object Label12: TLabel
                  Left = 144
                  Top = 12
                  Width = 41
                  Height = 13
                  Caption = 'Posi'#231#227'o:'
                end
                object EdTxtTopoPix_Txt: TdmkEdit
                  Left = 144
                  Top = 28
                  Width = 40
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 2
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 4
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0000'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  OnChange = EdTxtTopoPix_TxtChange
                end
                object EdTxtLeftPix_Txt: TdmkEdit
                  Left = 144
                  Top = 52
                  Width = 40
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 3
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 4
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0000'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  OnChange = EdTxtLeftPix_TxtChange
                end
                object EdTxtTopoCm_Txt: TdmkEdit
                  Left = 100
                  Top = 28
                  Width = 40
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 3
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,000'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                  OnChange = EdTxtTopoCm_TxtChange
                end
                object EdTxtLeftCm_Txt: TdmkEdit
                  Left = 100
                  Top = 52
                  Width = 40
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 3
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,000'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                  OnChange = EdTxtLeftCm_TxtChange
                end
              end
            end
            object Panel8: TPanel
              Left = 1
              Top = 85
              Width = 575
              Height = 234
              Align = alClient
              ParentBackground = False
              TabOrder = 1
              object Label10: TLabel
                Left = 52
                Top = 8
                Width = 76
                Height = 13
                Caption = 'Texto a imprimir:'
              end
              object Label4: TLabel
                Left = 8
                Top = 8
                Width = 29
                Height = 13
                Caption = 'Linha:'
              end
              object Label11: TLabel
                Left = 8
                Top = 52
                Width = 380
                Height = 13
                Caption = 
                  'Observa'#231#227'o: Para excluir as linhas geradas, selecione a fonte "?' +
                  '".'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clRed
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object EdTexto_Txt: TEdit
                Left = 52
                Top = 24
                Width = 425
                Height = 21
                TabOrder = 0
                Text = '[.*.]'
                OnExit = EdTexto_TxtExit
                OnKeyDown = EdTexto_TxtKeyDown
              end
              object EdLinha_Txt: TdmkEdit
                Left = 8
                Top = 24
                Width = 40
                Height = 21
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnExit = EdLinha_TxtExit
                OnKeyDown = EdLinha_TxtKeyDown
              end
              object BitBtn1: TBitBtn
                Tag = 253
                Left = 480
                Top = 12
                Width = 90
                Height = 40
                Caption = '&C'#243'digo'
                NumGlyphs = 2
                TabOrder = 2
                OnClick = BitBtn1Click
              end
            end
          end
        end
      end
      object Panel13: TPanel
        Left = 585
        Top = 0
        Width = 421
        Height = 457
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 421
          Height = 333
          Align = alTop
          ParentBackground = False
          TabOrder = 0
          object DBGTxt: TDBGrid
            Left = 1
            Top = 1
            Width = 419
            Height = 331
            Align = alClient
            DataSource = DataSource1
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'TxtBar'
                Title.Caption = 'Tipo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DataHora'
                Width = 86
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Coluna'
                Width = 32
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Linha'
                Width = 32
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Texto'
                Visible = True
              end>
          end
        end
        object MeResult: TMemo
          Left = 0
          Top = 333
          Width = 421
          Height = 124
          Align = alClient
          Color = 4539717
          Font.Charset = ANSI_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Lucida Console'
          Font.Style = [fsBold]
          Lines.Strings = (
            'Teste 1'
            'C:\Meus Documentos')
          ParentFont = False
          TabOrder = 1
        end
        object RGRatio: TRadioGroup
          Left = 25
          Top = 69
          Width = 80
          Height = 80
          Caption = 'Ratio (def=0):'
          TabOrder = 2
          Visible = False
        end
      end
    end
  end
  object PMAcao: TPopupMenu
    Left = 332
    Top = 436
    object Sgera1: TMenuItem
      Caption = '&S'#243' gera'
      OnClick = Sgera1Click
    end
    object Geraeimprime1: TMenuItem
      Caption = '&Gera e imprime'
      OnClick = Geraeimprime1Click
    end
    object Simprime1: TMenuItem
      Caption = 'S'#243' &imprime'
      OnClick = Simprime1Click
    end
  end
  object PMOK: TPopupMenu
    Left = 36
    Top = 276
    object AdicionasoTexto1: TMenuItem
      Caption = 'Adiciona s'#243' o &Texto'
      OnClick = AdicionasoTexto1Click
    end
    object AdicionasoCdigodeBarras1: TMenuItem
      Caption = 'Adiciona s'#243' o C'#243'digo de &Barras'
      OnClick = AdicionasoCdigodeBarras1Click
    end
    object AdicionaAmbos1: TMenuItem
      Caption = 'Adiciona &Ambos'
      OnClick = AdicionaAmbos1Click
    end
  end
  object Query: TABSQuery
    CurrentVersion = '7.91 '
    DatabaseName = 'MEMORY'
    InMemory = True
    ReadOnly = False
    RequestLive = True
    Left = 12
    Top = 12
    object QueryColuna: TIntegerField
      FieldName = 'Coluna'
      DisplayFormat = '000'
    end
    object QueryLinha: TIntegerField
      FieldName = 'Linha'
      DisplayFormat = '000'
    end
    object QueryTexto: TWideStringField
      FieldName = 'Texto'
      Size = 255
    end
    object QueryAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QueryTxtBar: TWideStringField
      FieldName = 'TxtBar'
      Size = 3
    end
    object QueryDataHora: TFloatField
      FieldName = 'DataHora'
    end
  end
  object DataSource1: TDataSource
    DataSet = Query
    Left = 40
    Top = 12
  end
end
