object DmPlanCut: TDmPlanCut
  OldCreateOrder = False
  Height = 515
  Width = 742
  object QrPCIEmpCad: TMySQLQuery
    SQL.Strings = (
      'SELECT Filial, Codigo,'
      'IF(Tipo=0,RazaoSocial,Nome) NOMEENT'
      'FROM entidades'
      'WHERE Codigo < -10'
      'ORDER BY NOMEENT')
    Left = 28
    object QrPCIEmpCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPCIEmpCadFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrPCIEmpCadNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
  end
  object DsPCIEmpCad: TDataSource
    DataSet = QrPCIEmpCad
    Left = 100
  end
  object QrPCIEmpIts: TMySQLQuery
    RequestLive = True
    Left = 172
    object QrPCIEmpItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPCIEmpItsCodUsu: TIntegerField
      FieldName = 'CodUsu'
      DisplayFormat = '00000000'
    end
    object QrPCIEmpItsNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrPCIEmpItsAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
  end
  object DsPCIEmpIts: TDataSource
    DataSet = QrPCIEmpIts
    Left = 248
  end
  object QrPCIEmpAti: TMySQLQuery
    RequestLive = True
    SQL.Strings = (
      'SELECT Count(*)  Itens'
      'FROM pciemp'
      'WHERE Ativo=1'
      '')
    Left = 316
    object QrPCIEmpAtiItens: TIntegerField
      FieldName = 'Itens'
    end
  end
  object QrPCICenCad: TMySQLQuery
    SQL.Strings = (
      'SELECT CodUsu, Codigo, Nome'
      'FROM stqcencad'
      'ORDER BY Nome')
    Left = 28
    Top = 48
    object QrPCICenCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPCICenCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPCICenCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsPCICenCad: TDataSource
    DataSet = QrPCICenCad
    Left = 100
    Top = 48
  end
  object QrPCICenIts: TMySQLQuery
    RequestLive = True
    Left = 172
    Top = 48
    object QrPCICenItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPCICenItsCodUsu: TIntegerField
      FieldName = 'CodUsu'
      DisplayFormat = '00000000'
    end
    object QrPCICenItsNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrPCICenItsAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
  end
  object DsPCICenIts: TDataSource
    DataSet = QrPCICenIts
    Left = 248
    Top = 48
  end
  object QrPCICenAti: TMySQLQuery
    RequestLive = True
    SQL.Strings = (
      'SELECT Count(*)  Itens'
      'FROM pcicen'
      'WHERE Ativo=1'
      '')
    Left = 316
    Top = 48
    object QrPCICenAtiItens: TIntegerField
      FieldName = 'Itens'
    end
  end
  object QrPCIPrdCad: TMySQLQuery
    SQL.Strings = (
      'SELECT gg1.Codusu, gg1.Nivel1, '
      'gg1.Nome'
      'FROM gragru1 gg1'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'WHERE pgt.TipPrd=1'
      'ORDER BY gg1.Nome'
      '')
    Left = 28
    Top = 96
    object QrPCIPrdCadCodusu: TIntegerField
      FieldName = 'Codusu'
      Required = True
    end
    object QrPCIPrdCadNivel1: TIntegerField
      FieldName = 'Nivel1'
      Required = True
    end
    object QrPCIPrdCadNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 30
    end
  end
  object DsPCIPrdCad: TDataSource
    DataSet = QrPCIPrdCad
    Left = 100
    Top = 96
  end
  object QrPCIPrdIts: TMySQLQuery
    RequestLive = True
    Left = 172
    Top = 96
    object QrPCIPrdItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPCIPrdItsCodUsu: TIntegerField
      FieldName = 'CodUsu'
      DisplayFormat = '00000000'
    end
    object QrPCIPrdItsNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrPCIPrdItsAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
  end
  object DsPCIPrdIts: TDataSource
    DataSet = QrPCIPrdIts
    Left = 244
    Top = 96
  end
  object QrPCIPrdAti: TMySQLQuery
    RequestLive = True
    SQL.Strings = (
      'SELECT Count(*)  Itens'
      'FROM pciprd'
      'WHERE Ativo=1'
      '')
    Left = 316
    Top = 96
    object QrPCIPrdAtiItens: TIntegerField
      FieldName = 'Itens'
    end
  end
  object QrPCIAtrPrdCad: TMySQLQuery
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM graatrcad'
      'ORDER BY Nome')
    Left = 28
    Top = 144
    object QrPCIAtrPrdCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPCIAtrPrdCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPCIAtrPrdCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsPCIAtrPrdCad: TDataSource
    DataSet = QrPCIAtrPrdCad
    Left = 100
    Top = 144
  end
  object QrPCIAtrPrdIts: TMySQLQuery
    RequestLive = True
    Left = 172
    Top = 144
    object QrPCIAtrPrdItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPCIAtrPrdItsCodUsu: TIntegerField
      FieldName = 'CodUsu'
      DisplayFormat = '00000000'
    end
    object QrPCIAtrPrdItsNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrPCIAtrPrdItsAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
  end
  object DsPCIAtrPrdIts: TDataSource
    DataSet = QrPCIAtrPrdIts
    Left = 244
    Top = 144
  end
  object QrPCIAtrPrdAti: TMySQLQuery
    RequestLive = True
    SQL.Strings = (
      'SELECT Count(*)  Itens'
      'FROM pciatrprd'
      'WHERE Ativo=1'
      '')
    Left = 316
    Top = 144
    object QrPCIAtrPrdAtiItens: TIntegerField
      FieldName = 'Itens'
    end
  end
  object QrAtrPrdSubIts: TMySQLQuery
    SQL.Strings = (
      'SELECT Controle Codigo, CodUsu, Nome'
      'FROM graatrits'
      'WHERE Codigo=:P0'
      'ORDER BY Nome')
    Left = 380
    Top = 144
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAtrPrdSubItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAtrPrdSubItsCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrAtrPrdSubItsNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
end
