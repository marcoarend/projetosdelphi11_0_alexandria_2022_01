object FmEtqGeraLot: TFmEtqGeraLot
  Left = 339
  Top = 185
  Caption = 'ETQ-GERAR-001 :: Gera'#231#227'o de Etiquetas - Gerenciamento'
  ClientHeight = 494
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 446
    Width = 784
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtLotes: TBitBtn
      Tag = 10054
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Lote'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtLotesClick
    end
    object Panel2: TPanel
      Left = 672
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object BitBtn1: TBitBtn
      Tag = 10053
      Left = 112
      Top = 4
      Width = 120
      Height = 40
      Caption = '&Impress'#227'o'
      NumGlyphs = 2
      TabOrder = 2
      OnClick = BitBtn1Click
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    Caption = 'Gera'#231#227'o de Etiquetas - Gerenciamento'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 782
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 398
    Align = alClient
    TabOrder = 0
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 188
      Height = 396
      Align = alLeft
      TabOrder = 0
      object Panel4: TPanel
        Left = 1
        Top = 1
        Width = 186
        Height = 80
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 4
          Top = 20
          Width = 30
          Height = 13
          Caption = 'Inicial:'
        end
        object Label2: TLabel
          Left = 84
          Top = 20
          Width = 58
          Height = 13
          Caption = 'Quantidade:'
        end
        object StaticText1: TStaticText
          Left = 0
          Top = 0
          Width = 101
          Height = 17
          Align = alTop
          Alignment = taCenter
          BevelInner = bvLowered
          BorderStyle = sbsSunken
          Caption = 'Lotes: (decrescente)'
          TabOrder = 0
        end
        object EdLoteIni: TdmkEdit
          Left = 4
          Top = 36
          Width = 76
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          OnChange = EdLoteIniChange
        end
        object EdLoteQtd: TdmkEdit
          Left = 84
          Top = 36
          Width = 76
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          OnChange = EdLoteQtdChange
        end
        object CkNaoImpresso: TCheckBox
          Left = 8
          Top = 60
          Width = 165
          Height = 17
          Caption = 'Somente lotes n'#227'o impressos.'
          Checked = True
          State = cbChecked
          TabOrder = 3
          OnClick = CkNaoImpressoClick
        end
      end
      object DBGCab: TdmkDBGrid
        Left = 1
        Top = 81
        Width = 186
        Height = 314
        Align = alClient
        Columns = <
          item
            Expanded = False
            FieldName = 'Imprimir'
            Title.Alignment = taCenter
            Title.Caption = '?'
            Width = 17
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'Lote'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DataCad'
            Title.Caption = 'Data cad.'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Impresso'
            Title.Alignment = taCenter
            Title.Caption = '!'
            Width = 17
            Visible = True
          end>
        Color = clWindow
        DataSource = DsEtqGeraLot
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnCellClick = DBGCabCellClick
        OnEnter = DBGCabEnter
        OnExit = DBGCabExit
        Columns = <
          item
            Expanded = False
            FieldName = 'Imprimir'
            Title.Alignment = taCenter
            Title.Caption = '?'
            Width = 17
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'Lote'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DataCad'
            Title.Caption = 'Data cad.'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Impresso'
            Title.Alignment = taCenter
            Title.Caption = '!'
            Width = 17
            Visible = True
          end>
      end
    end
    object Panel5: TPanel
      Left = 189
      Top = 1
      Width = 594
      Height = 396
      Align = alClient
      Caption = 'Panel5'
      TabOrder = 1
      object PageControl1: TPageControl
        Left = 1
        Top = 29
        Width = 592
        Height = 366
        ActivePage = TabSheet5
        Align = alClient
        TabOrder = 0
        TabPosition = tpBottom
        object TabSheet5: TTabSheet
          Caption = ' Sele'#231#227'o '
          ImageIndex = 2
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object GradeQ: TStringGrid
            Left = 0
            Top = 0
            Width = 584
            Height = 340
            Align = alClient
            ColCount = 2
            DefaultColWidth = 65
            DefaultRowHeight = 18
            FixedCols = 0
            RowCount = 2
            FixedRows = 0
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goTabs]
            ParentFont = False
            TabOrder = 0
            OnDrawCell = GradeQDrawCell
          end
        end
        object TabSheet6: TTabSheet
          Caption = ' C'#243'digos '
          ImageIndex = 3
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object GradeC: TStringGrid
            Left = 0
            Top = 0
            Width = 606
            Height = 370
            Align = alClient
            ColCount = 2
            DefaultColWidth = 65
            DefaultRowHeight = 18
            RowCount = 2
            TabOrder = 0
            OnDrawCell = GradeCDrawCell
            ExplicitHeight = 353
            RowHeights = (
              18
              19)
          end
        end
        object TabSheet3: TTabSheet
          Caption = ' Ativos '
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object GradeA: TStringGrid
            Left = 0
            Top = 0
            Width = 606
            Height = 370
            Align = alClient
            ColCount = 2
            DefaultColWidth = 65
            DefaultRowHeight = 18
            FixedCols = 0
            RowCount = 2
            FixedRows = 0
            TabOrder = 0
            OnDrawCell = GradeADrawCell
            ExplicitHeight = 353
            RowHeights = (
              18
              18)
          end
        end
        object TabSheet9: TTabSheet
          Caption = ' X '
          ImageIndex = 6
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object GradeX: TStringGrid
            Left = 0
            Top = 0
            Width = 584
            Height = 340
            Align = alClient
            ColCount = 2
            DefaultColWidth = 65
            DefaultRowHeight = 18
            FixedCols = 0
            RowCount = 2
            FixedRows = 0
            Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goAlwaysShowEditor]
            TabOrder = 0
            RowHeights = (
              18
              18)
          end
        end
      end
      object Panel6: TPanel
        Left = 1
        Top = 1
        Width = 592
        Height = 28
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object DBEdit1: TDBEdit
          Left = 4
          Top = 4
          Width = 72
          Height = 21
          DataField = 'CodUsu'
          DataSource = DsEtqGeraLot
          TabOrder = 0
        end
        object DBEdit2: TDBEdit
          Left = 80
          Top = 4
          Width = 505
          Height = 21
          DataField = 'Nome'
          DataSource = DsEtqGeraLot
          TabOrder = 1
        end
      end
    end
  end
  object QrEtqGeraLot: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrEtqGeraLotAfterScroll
    SQL.Strings = (
      'SELECT egl.Imprimir, egl.Impresso, egl.Codigo, '
      'egl.DataCad, egl.Nivel1, gg1.GraTamCad,'
      'gg1.CodUsu, gg1.Nome'
      'FROM etqgeralot egl'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=egl.Nivel1'
      'ORDER BY Codigo DESC'#13
      'LIMIT :P0, :P1')
    Left = 4
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrEtqGeraLotImprimir: TSmallintField
      FieldName = 'Imprimir'
      Required = True
      MaxValue = 1
    end
    object QrEtqGeraLotCodigo: TIntegerField
      FieldName = 'Codigo'
      DisplayFormat = '000000;-000000; '
    end
    object QrEtqGeraLotDataCad: TDateField
      FieldName = 'DataCad'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEtqGeraLotNivel1: TIntegerField
      FieldName = 'Nivel1'
      Required = True
    end
    object QrEtqGeraLotGraTamCad: TIntegerField
      FieldName = 'GraTamCad'
    end
    object QrEtqGeraLotImpresso: TSmallintField
      FieldName = 'Impresso'
      MaxValue = 1
    end
    object QrEtqGeraLotCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrEtqGeraLotNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsEtqGeraLot: TDataSource
    DataSet = QrEtqGeraLot
    Left = 32
    Top = 4
  end
  object PMLotes: TPopupMenu
    Left = 60
    Top = 4
    object Crianovolote1: TMenuItem
      Caption = '&Cria novo lote'
      OnClick = Crianovolote1Click
    end
    object Marcalote1: TMenuItem
      Caption = '&Marca lote como impresso'
      Enabled = False
    end
  end
end
