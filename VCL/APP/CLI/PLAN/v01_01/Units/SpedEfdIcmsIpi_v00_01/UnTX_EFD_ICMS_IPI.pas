unit UnTX_EFD_ICMS_IPI;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnInternalConsts2, Buttons, ComCtrls, CommCtrl, Consts,
  Variants, UnInternalConsts, ZCF2, StrUtils, dmkGeral, UnDmkEnums, dmkEditCB,
  dmkEdit, dmkDBLookupComboBox, mySQLDbTables, Data.Db, DBGrids, AppListas,
  dmkDBGridZTO, UnDmkProcFunc, UnProjGroup_Vars, TypInfo, System.Math,
  UnProjGroup_Consts, UnEntities, DBCtrls, Grids, Mask, UnTX_Tabs,
  dmkEditDateTimePicker, mySQLDirectQuery, UnGrl_Consts, UnGrl_Geral, UnTX_PF,
  UnProjGroup_PF, UnAppEnums;

type
  TUnTX_EFD_ICMS_IPI = class(TObject)
  private
    { Private declarations }
     //
  public
    { Public declarations }
    //
    function  ReopenTXOriPQx_Fast(Qry: TmySQLDirectQuery; MovimCod: Integer;
              SQL_PeriodoPQ: String; ShowSQL: Boolean): Boolean;
    procedure ReopenTXOriPQx(Qry: TmySQLQuery; MovimCod: Integer;
              SQL_PeriodoPQ: String; ShowSQL: Boolean);
    procedure ReopenTXRibItss(QrTXAtu, QrTXOriIMEI, QrTXOriPallet,
              QrTXDst, QrTXInd, QrSubPrd, QrDescl, QrForcados, QrEmit, QrPQO: TmySQLQuery;
              Codigo, MovimCod, TemIMEIMrt: Integer; MovIDEmProc, MovIDPronto,
              MovIDIndireto: TEstqMovimID; MovNivInn, MovNivSrc, MovNivDst:
              TEstqMovimNiv);
    procedure ReopenTXRibItss_Fast(DqTXAtu, DqTXOriIMEI, DqTXOriPallet,
              DqTXDst, DqTXInd, DqSubPrd, DqDescl, DqForcados, DqEmit, DqPQO:
              TmySQLDirectQuery; Codigo, MovimCod, TemIMEIMrt: Integer;
              MovIDEmProc, MovIDPronto, MovIDIndireto: TEstqMovimID;
              MovNivInn, MovNivSrc, MovNivDst: TEstqMovimNiv; TabIMEI: String;
              DB_IMEI: TmySQLDatabase; BaseDadosDiminuida: Boolean);
    procedure ReopenTXMovCod_Emit(QrEmit: TmySQLQuery; MovimCod: Integer);
    procedure ReopenTXMovCod_Emit_Fast(QrEmit: TmySQLDirectQuery; MovimCod:
              Integer; TabIMEI: String; DB_IMEI: TmySQLDatabase);
    procedure ReopenTXMovCod_PQO(QrPQO: TmySQLQuery; MovimCod, Codigo: Integer);
    procedure ReopenTXMovCod_PQO_Fast(QrPQO: TmySQLDirectQuery; MovimCod,
              Codigo: Integer; TabIMEI: String; DB_IMEI: TmySQLDatabase);
    procedure ReopenTXMovCod_PQOIts(QrPQOIts: TmySQLQuery; Codigo, Controle: Integer);
    procedure ReopenTXIndPrcAtu(Qry: TmySQLQuery; MovimCod, Controle, TemIMEIMrt:
              Integer; MovimNiv: TEstqMovimNiv);
    procedure ReopenTXIndPrcAtu_Fast(Qry: TmySQLDirectQuery; MovimCod, (*Controle,*)
              TemIMEIMrt: Integer; MovimNiv: TEstqMovimNiv; TabIMEI: String; DB_IMEI: TmySQLDatabase);
    procedure ReopenTXIndPrcForcados(Qry: TmySQLQuery; Controle, SrcNivel1,
              SrcNivel2, TemIMEIMrt: Integer; SrcMovID: TEstqMovimID);
    procedure ReopenTXIndPrcForcados_Fast(Qry: TmySQLDirectQuery; Controle,
              SrcNivel1, SrcNivel2, TemIMEIMrt: Integer; SrcMovID: TEstqMovimID;
              TabIMEI: String; DB_IMEI: TmySQLDatabase);
    procedure ReopenTXIndPrcOriIMEI(Qry: TmySQLQuery; MovimCod, Controle,
              TemIMEIMrt: Integer; MovimNiv: TEstqMovimNiv; SQL_Limit: String);
    procedure ReopenTXIndPrcOriIMEI_Fast(Qry: TmySQLDirectQuery; MovimCod, (*Controle,*)
              TemIMEIMrt: Integer; MovimNiv: TEstqMovimNiv; TabIMEI: String;
              DB_IMEI: TmySQLDatabase; BaseDadosDiminuida: Boolean);
    procedure ReopenTXIndPrcOriPallet(QrTXCalOriPallet: TmySQLQuery; MovimCod:
              Integer; MovimNiv: TEstqMovimNiv; TemIMEIMrt, LocPallet: Integer);
    procedure ReopenTXIndPrcOriPallet_Fast(QrTXCalOriPallet: TmySQLDirectQuery; MovimCod:
              Integer; MovimNiv: TEstqMovimNiv; TemIMEIMrt(*, LocPallet*):
              Integer; TabIMEI: String; DB_IMEI: TmySQLDatabase);
    procedure ReopenTXPrcPrcDst(Qry: TmySQLQuery; SrcNivel1, MovimCod, Controle,
              TemIMEIMrt: Integer; SrcMovID: TEstqMovimID;
              DstMovimNiv: TEstqMovimNiv; SQL_Limit: String = '');
    procedure ReopenTXPrcPrcDst_Fast(Qry: TmySQLDirectQuery; SrcNivel1, MovimCod,
              (*Controle,*) TemIMEIMrt: Integer; SrcMovID: TEstqMovimID;
              DstMovimNiv: TEstqMovimNiv; TabIMEI: String; DB_IMEI: TmySQLDatabase; SQL_Limit: String = '');
    procedure ReopenTXPrcPrcInd(Qry: TmySQLQuery; SrcNivel1, MovimCod, Controle,
              TemIMEIMrt: Integer; SrcMovID: TEstqMovimID;
              DstMovimNiv: TEstqMovimNiv; SQL_Limit: String = '');
    procedure ReopenTXPrcPrcInd_Fast(Qry: TmySQLDirectQuery; SrcNivel1, MovimCod,
              (*Controle,*) TemIMEIMrt: Integer; SrcMovID: TEstqMovimID;
              DstMovimNiv: TEstqMovimNiv; TabIMEI: String; DB_IMEI:
              TmySQLDatabase; BaseDadosDiminuida: Boolean);
  end;

var
  TX_EFD_ICMS_IPI: TUnTX_EFD_ICMS_IPI;

implementation

uses DmkDAC_PF, Module, ModuleGeral;

{ TUnTX_EFD_ICMS_IPI }

procedure TUnTX_EFD_ICMS_IPI.ReopenTXRibItss(QrTXAtu, QrTXOriIMEI,
  QrTXOriPallet, QrTXDst, QrTXInd, QrSubPrd, QrDescl, QrForcados, QrEmit, QrPQO:
  TmySQLQuery; Codigo, MovimCod, TemIMEIMrt: Integer; MovIDEmProc, MovIDPronto,
  MovIDIndireto: TEstqMovimID; MovNivInn, MovNivSrc, MovNivDst: TEstqMovimNiv);
const
  Controle = 0;
  Pallet   = 0;
  //
begin
  // Em processo. Deve ser antes do Forcados!!
  ReopenTXIndPrcAtu(QrTXAtu, (*QrTXCab*)MovimCod(*.Value*), Controle,
  (*QrTXCab*)TemIMEIMrt(*.Value*), MovNivInn(*eminEmInn*));
////////////////////////////////////////////////////////////////////////////////
  // Origem
  if QrTXOriIMEI <> nil then
    ReopenTXIndPrcOriIMEI(QrTXOriIMEI,
    (*QrTXCab*)MovimCod(*.Value*), Controle,
    TemIMEIMrt, MovNivSrc(*eminSorc*), (*SQL_Limit*)'');
  if QrTXOriPallet <> nil then
    ReopenTXIndPrcOriPallet(QrTXOriPallet, MovimCod,
    MovNivSrc(*eminSorc*), TemIMEIMrt, Pallet);
////////////////////////////////////////////////////////////////////////////////
  // Destino Direto
  if QrTXDst <> nil then
    ReopenTXPrcPrcDst(QrTXDst, (*QrTXCab*)Codigo(*.Value*),
    (*QrTXCab*)MovimCod(*.Value*), Controle, (*QrTXCab*)TemIMEIMrt(*.Value*),
    MovIDPronto(*TEstqMovimID.emideado*),
    MovNivDst(*eminDest*));
////////////////////////////////////////////////////////////////////////////////
  // Destino Indireto
  if QrTXInd <> nil then
    ReopenTXPrcPrcInd(QrTXInd, (*QrTXCab*)Codigo(*.Value*),
    (*QrTXCab*)MovimCod(*.Value*), Controle,
    (*QrTXCab*)TemIMEIMrt(*.Value*),
    MovIDIndireto(*TEstqMovimID.emideado*),
    MovNivDst(*eminDest*));
////////////////////////////////////////////////////////////////////////////////
  // For�ados. Deve ser depois do Forcados!!
  if QrForcados <> nil then
    ReopenTXIndPrcForcados(QrForcados, 0, (*QrTXCab*)Codigo(*.Value*),
      (*QrTXAtuControle.Value*)QrTXAtu.FieldByName('Controle').AsInteger,
      (*QrTXCab*)TemIMEIMrt(*.Value*), MovIDEmProc(*emidEmProc*));
////////////////////////////////////////////////////////////////////////////////
  // Insumos
  if QrEmit <> nil then
    ReopenTXMovCod_Emit(QrEmit, MovimCod);
  if QrPQO <> nil then
    ReopenTXMovCod_PQO(QrPQO, MovimCod, 0);
////////////////////////////////////////////////////////////////////////////////
  //
end;

procedure TUnTX_EFD_ICMS_IPI.ReopenTXRibItss_Fast(DqTXAtu, DqTXOriIMEI,
  DqTXOriPallet, DqTXDst, DqTXInd, DqSubPrd, DqDescl, DqForcados, DqEmit,
  DqPQO: TmySQLDirectQuery; Codigo, MovimCod, TemIMEIMrt: Integer; MovIDEmProc,
  MovIDPronto, MovIDIndireto: TEstqMovimID; MovNivInn, MovNivSrc,
  MovNivDst: TEstqMovimNiv; TabIMEI: String; DB_IMEI: TmySQLDatabase;
  BaseDadosDiminuida: Boolean);
{
const
  Controle = 0;
  Pallet   = 0;
  //
}
begin
  // Em processo. Deve ser antes do Forcados!!
  ReopenTXIndPrcAtu_Fast(DqTXAtu, (*DqTXCab*)MovimCod(*.Value*), (*Controle,*)
  (*DqTXCab*)TemIMEIMrt(*.Value*), MovNivInn(*eminEmInn*), TabIMEI, DB_IMEI);
////////////////////////////////////////////////////////////////////////////////
  // Origem
  if DqTXOriIMEI <> nil then
    ReopenTXIndPrcOriIMEI_Fast(DqTXOriIMEI,
    (*DqTXCab*)MovimCod(*.Value*), (*Controle,*)
    TemIMEIMrt, MovNivSrc(*eminSorc*), TabIMEI, DB_IMEI, BaseDadosDiminuida);
  if DqTXOriPallet <> nil then
    ReopenTXIndPrcOriPallet_Fast(DqTXOriPallet, MovimCod,
    MovNivSrc(*eminSorc*), TemIMEIMrt(*, Pallet*), TabIMEI, DB_IMEI);
////////////////////////////////////////////////////////////////////////////////
  // Destino Direto
  if DqTXDst <> nil then
    ReopenTXPrcPrcDst_Fast(DqTXDst, (*DqTXCab*)Codigo(*.Value*),
    (*DqTXCab*)MovimCod(*.Value*), (*Controle,*) (*DqTXCab*)TemIMEIMrt(*.Value*),
    MovIDPronto(*TEstqMovimID.emideado*),
    MovNivDst(*eminDest*), TabIMEI, DB_IMEI);
////////////////////////////////////////////////////////////////////////////////
  // Destino Indireto
  if DqTXInd <> nil then
    ReopenTXPrcPrcInd_Fast(DqTXInd, (*DqTXCab*)Codigo(*.Value*),
    (*DqTXCab*)MovimCod(*.Value*), (*Controle,*)
    (*DqTXCab*)TemIMEIMrt(*.Value*),
    MovIDIndireto(*TEstqMovimID.emideado*),
    MovNivDst(*eminDest*), TabIMEI, DB_IMEI,
    BaseDadosDiminuida);
////////////////////////////////////////////////////////////////////////////////
  // For�ados. Deve ser depois do Forcados!!
  if DqForcados <> nil then
  begin
  //'SELECT MovimID, MovimCod, Codigo, DataHora, DtCorrApo, ',
  //'Empresa, FornecMO, Controle, GraGruX, MovimNiv, MovimTwn ',
    ReopenTXIndPrcForcados_Fast(DqForcados, 0, (*DqTXCab*)Codigo(*.Value*),
      (*DqTXAtuControle.Value*)Geral.IMV(DqTXAtu.FieldValueByFieldName('Controle')),
      (*DqTXCab*)TemIMEIMrt(*.Value*), MovIDEmProc(*emidEmProc*), TabIMEI, DB_IMEI);
  end;
////////////////////////////////////////////////////////////////////////////////
  // Insumos
  if DqEmit <> nil then
    ReopenTXMovCod_Emit_Fast(DqEmit, MovimCod, TabIMEI, DB_IMEI);
  if DqPQO <> nil then
    ReopenTXMovCod_PQO_Fast(DqPQO, MovimCod, 0, TabIMEI, DB_IMEI);
////////////////////////////////////////////////////////////////////////////////
  //
end;

procedure TUnTX_EFD_ICMS_IPI.ReopenTXMovCod_Emit(QrEmit: TmySQLQuery; MovimCod:
  Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmit, Dmod.MyDB, [
  'SELECT emi.*, ',
  'IF(emi.DtaBaixa < 2, "??/??/????", DATE_FORMAT(emi.DtaBaixa, "%d/%m/%Y") ) DtaBaixa_TXT, ',
  'IF(emi.DtCorrApo < 2, "", DATE_FORMAT(emi.DtCorrApo, "%d/%m/%Y") ) DtCorrApo_TXT ',
  'FROM emit emi ',
  'WHERE emi.TXMovCod=' + Geral.FF0((*QrTXCalCab*)MovimCod(*.Value*)),
  '']);
end;

procedure TUnTX_EFD_ICMS_IPI.ReopenTXMovCod_Emit_Fast(QrEmit: TmySQLDirectQuery;
  MovimCod: Integer; TabIMEI: String; DB_IMEI: TmySQLDatabase);
begin
  Geral.MB_Info('FaltaFazer: TUnTX_EFD_ICMS_IPI.ReopenTXMovCod_Emit_Fast(');
end;

procedure TUnTX_EFD_ICMS_IPI.ReopenTXMovCod_PQO(QrPQO: TmySQLQuery; MovimCod,
  Codigo: Integer);
begin
{$IfDef sAllTX}
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQO, Dmod.MyDB, [
  'SELECT lse.Nome NOMESETOR, ',
  'emg.Nome NO_EMITGRU, pqo.* ',
  'FROM pqo pqo ',
  'LEFT JOIN listasetores lse ON lse.Codigo=pqo.Setor ',
  'LEFT JOIN emitgru emg ON emg.Codigo=pqo.EmitGru ',
  'WHERE TXMovCod=' + Geral.FF0(MovimCod),
  '']);
  if Codigo <> 0 then
    QrPQO.Locate('Codigo', Codigo, []);
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TUnTX_EFD_ICMS_IPI.ReopenTXMovCod_PQOIts(QrPQOIts: TmySQLQuery; Codigo,
  Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQOIts, Dmod.MyDB, [
  'SELECT DISTINCT pqx.*, pq_.Nome NOMEPQ, pq_.GrupoQuimico,  ',
  'pqg.Nome NOMEGRUPO ',
  'FROM pqx pqx ',
  'LEFT JOIN pqcli pci ON pci.PQ=pqx.Insumo ',
  'LEFT JOIN pq    pq_ ON pq_.Codigo=pci.PQ ',
  'LEFT JOIN pqg   pqg ON pqg.Codigo=pq_.GrupoQuimico ',
  'WHERE pqx.Tipo=' + Geral.FF0(VAR_FATID_0190),
  'AND pqx.OrigemCodi=' + Geral.FF0(Codigo),
  '']);
  if Controle <> 0 then
    QrPQOIts.Locate('OrigemCtrl', Controle, []);
end;

procedure TUnTX_EFD_ICMS_IPI.ReopenTXMovCod_PQO_Fast(QrPQO: TmySQLDirectQuery;
  MovimCod, Codigo: Integer; TabIMEI: String; DB_IMEI: TmySQLDatabase);
begin
  Geral.MB_Info('FaltaFazer: TUnTX_EFD_ICMS_IPI.ReopenTXMovCod_PQO_Fast(');
end;

procedure TUnTX_EFD_ICMS_IPI.ReopenTXIndPrcAtu(Qry: TmySQLQuery; MovimCod,
  Controle, TemIMEIMrt: Integer; MovimNiv: TEstqMovimNiv);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
begin
  SQL_Flds := Geral.ATS([
  TX_PF.SQL_NO_GGX(),
  TX_PF.SQL_NO_FRN(),
  TX_PF.SQL_NO_SCL(),
  'IF(fmo.Tipo=0, fmo.RazaoSocial, fmo.Nome)NO_FORNEC_MO, ',
  'IF(Qtde=0, 0, ValorT / QTDE) CUSTO_UNI, ',
  'txp.Nome NO_Pallet ',
  '']);
  SQL_Left := Geral.ATS([
  TX_PF.SQL_LJ_GGX(),
  TX_PF.SQL_LJ_FRN(),
  TX_PF.SQL_LJ_SCL(),
  'LEFT JOIN txpalleta  txp ON txp.Codigo=tmi.Pallet ',
  'LEFT JOIN entidades  fmo ON fmo.Codigo=tmi.FornecMO',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE tmi.MovimCod=' + Geral.FF0(MovimCod),
  'AND tmi.MovimNiv=' + Geral.FF0(Integer(MovimNiv)),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  TX_PF.GeraSQLTXMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  TX_PF.GeraSQLTXMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Pallet, Controle ',
  '']);
  //*Geral.MB_SQL(nil, Qry);
  Qry.Locate('Controle', Controle, []);
end;

procedure TUnTX_EFD_ICMS_IPI.ReopenTXIndPrcAtu_Fast(Qry: TmySQLDirectQuery;
  MovimCod, (*Controle,*) TemIMEIMrt: Integer; MovimNiv: TEstqMovimNiv;
  TabIMEI: String; DB_IMEI: TmySQLDatabase);
const
  sProcName = 'TUnTX_EFD_ICMS_IPI.ReopenTXIndPrcAtu_Fast()';
begin
  UnDmkDAC_PF.AbreMySQLDirectQuery0(Qry, DB_IMEI, [
  'SELECT MovimID, MovimCod, Codigo, DataHora, DtCorrApo, ',
  'Empresa, FornecMO, Controle, GraGruX, MovimNiv, MovimTwn ',
  'FROM ' + TabIMEI,
  'WHERE MovimCod=' + Geral.FF0(MovimCod),
  'AND MovimNiv=' + Geral.FF0(Integer(MovimNiv)),
  '']);
  //
  if Qry.RecordCount = 0 then
  begin
    UnDmkDAC_PF.AbreMySQLDirectQuery0(Qry, DModG.MyCompressDB, [
    'SELECT MovimID, MovimCod, Codigo, DataHora, DtCorrApo, ',
    'Empresa, FornecMO, Controle, GraGruX, MovimNiv, MovimTwn ',
    'FROM txmovits ',
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimNiv=' + Geral.FF0(Integer(MovimNiv)),
    '']);
    //
    if Qry.RecordCount = 0 then
      Geral.MB_Info('RecordCount=0' + sLineBreak + sProcName + sLineBreak + Qry.SQL.Text);
  end;
end;

procedure TUnTX_EFD_ICMS_IPI.ReopenTXIndPrcForcados(Qry: TmySQLQuery; Controle,
  SrcNivel1, SrcNivel2, TemIMEIMrt: Integer; SrcMovID: TEstqMovimID);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  //TemIMEIMrt: Integer;
begin
  //TemIMEIMrt := QrTXOpeCabTemIMEIMrt.Value;
  SQL_Flds := Geral.ATS([
  TX_PF.SQL_NO_GGX(),
  'txp.Nome NO_Pallet ',
  '']);
  SQL_Left := Geral.ATS([
  TX_PF.SQL_LJ_GGX(),
  'LEFT JOIN txpalleta  txp ON txp.Codigo=tmi.Pallet ',
  '']);
  SQL_Wher := Geral.ATS([
(*
  'WHERE tmi.MovimCod=' + Geral.FF0(MovimCod), //QrTXOpeCabMovimCod.Value),
  'AND tmi.SrcMovID=' + Geral.FF0(Integer(SrcMovID)), //emidEmOperacao)),  // 11
  'AND tmi.SrcNivel1=' + Geral.FF0(SrcNivel1), //QrTXOpeCabCodigo.Value),
*)
  'WHERE tmi.SrcMovID=' + Geral.FF0(Integer(SrcMovID)), //emidEmOperacao)),  // 11
  'AND tmi.SrcNivel1=' + Geral.FF0(SrcNivel1), //QrTXOpeCabCodigo.Value),
  'AND tmi.SrcNivel2=' + Geral.FF0(SrcNivel2), //QrTXOpeCabCodigo.Value),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  TX_PF.GeraSQLTXMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  TX_PF.GeraSQLTXMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Controle ',
  '']);
  //
  //Geral.MB_SQL(nil, Qry);
  Qry.Locate('Controle', Controle, []);
end;

procedure TUnTX_EFD_ICMS_IPI.ReopenTXIndPrcForcados_Fast(Qry: TmySQLDirectQuery;
  Controle, SrcNivel1, SrcNivel2, TemIMEIMrt: Integer; SrcMovID: TEstqMovimID;
  TabIMEI: String; DB_IMEI: TmySQLDatabase);
begin
  Geral.MB_Info('FaltaFazer: TUnTX_EFD_ICMS_IPI.ReopenTXIndPrcForcados_Fast(');
end;

procedure TUnTX_EFD_ICMS_IPI.ReopenTXIndPrcOriIMEI(Qry: TmySQLQuery; MovimCod, Controle,
  TemIMEIMrt: Integer; MovimNiv: TEstqMovimNiv; SQL_Limit: String);
begin
  ProjGroup_PF.ReopenTXIndPrcOriIMEI(Qry, MovimCod, Controle, TemIMEIMrt,
  MovimNiv, SQL_Limit);
end;

procedure TUnTX_EFD_ICMS_IPI.ReopenTXIndPrcOriIMEI_Fast(Qry: TmySQLDirectQuery;
  MovimCod, TemIMEIMrt: Integer; MovimNiv: TEstqMovimNiv; TabIMEI: String;
  DB_IMEI: TmySQLDatabase; BaseDadosDiminuida: Boolean);
const
  sProcName = 'TUnTX_EFD_ICMS_IPI.ReopenTXIndPrcOriIMEI_Fast()';
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
begin
  //Geral.MB_Info('FaltaFazer: TUnTX_EFD_ICMS_IPI.ReopenTXIndPrcOriIMEI_Fast(');
  if BaseDadosDiminuida then
  begin
    UnDmkDAC_PF.AbreMySQLDirectQuery0(Qry, DB_IMEI, [
    'SELECT DataHora, MovimID, MovimCod, Codigo, DtCorrApo, ',
    'Empresa, FornecMO, Controle, RmsGGX, DstGGX, MovimNiv, ',
    'MovimTwn, AreaM2, PesoKg, Pecas, QtdAntPeca, QtdAntPeso, ',
    'QtdAntArM2, ClientMO, StqCenLoc, scc_EntiSitio, TipoEstq ',
    'FROM ' + TabIMEI,
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimNiv=' + Geral.FF0(Integer(MovimNiv)),
    '']);
  end;
  //
  if (BaseDadosDiminuida =  False) or (Qry.RecordCount = 0) then
  begin
    UnDmkDAC_PF.AbreMySQLDirectQuery0(Qry, DModG.MyCompressDB, [
    'SELECT tmi.DataHora, tmi.MovimID, tmi.MovimCod, tmi.Codigo, ',
    'tmi.DtCorrApo, tmi.Empresa, tmi.FornecMO, tmi.Controle, tmi.RmsGGX, ',
    'tmi.DstGGX, tmi.MovimNiv, tmi.MovimTwn, tmi.AreaM2, tmi.PesoKg, ',
    'tmi.Pecas, tmi.QtdAntPeca, tmi.QtdAntPeso, tmi.QtdAntArM2, tmi.ClientMO, ',
    TX_PF.SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True, 'tmi.Qtde'),
    'tmi.StqCenLoc, scc.EntiSitio scc_EntiSitio ', //TipoEstq ', // vai dar erro TipoEstq ver outrs como foi aberto!
    'FROM ' + CO_SEL_TAB_TMI + ' tmi',
     //
    'LEFT JOIN stqcenloc scl ON scl.Controle=tmi.StqCenLoc ',
    'LEFT JOIN stqcencad scc ON scc.Codigo=scl.Codigo ',
    //
    'LEFT JOIN gragrux    ggx ON ggx.Controle=tmi.GraGruX ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
    'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed',
    'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
    //
    'WHERE tmi.MovimCod=' + Geral.FF0(MovimCod),
    'AND tmi.MovimNiv=' + Geral.FF0(Integer(MovimNiv)),
    '']);
    //if Qry.RecordCount = 0 then
      //Geral.MB_Info('RecordCount=0' + sLineBreak + sProcName + sLineBreak + Qry.SQL.Text);
  end;
end;

procedure TUnTX_EFD_ICMS_IPI.ReopenTXIndPrcOriPallet(QrTXCalOriPallet:
  TmySQLQuery; MovimCod: Integer; MovimNiv: TEstqMovimNiv;
  TemIMEIMrt, LocPallet: Integer);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
begin
  SQL_Flds := Geral.ATS([
  '']);
  SQL_Left := Geral.ATS([
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE tmi.MovimCod=' + Geral.FF0((*QrTXCalCab*)MovimCod(*.Value*)),
  'AND tmi.MovimNiv=' + Geral.FF0(Integer((*eminSorcCal*)MovimNiv)),
  '']);
  SQL_Group := 'GROUP BY Pallet ';
  UnDmkDAC_PF.AbreMySQLQuery0(QrTXCalOriPallet, Dmod.MyDB, [
  TX_PF.GeraSQLTXMovItx_Pall(SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  TX_PF.GeraSQLTXMovItx_Pall(SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Pallet ',
  '']);
  QrTXCalOriPallet.Locate('Pallet', LocPallet, []);
end;

procedure TUnTX_EFD_ICMS_IPI.ReopenTXIndPrcOriPallet_Fast(
  QrTXCalOriPallet: TmySQLDirectQuery; MovimCod: Integer;
  MovimNiv: TEstqMovimNiv; TemIMEIMrt(*, LocPallet*): Integer; TabIMEI: String; DB_IMEI: TmySQLDatabase);
begin
  Geral.MB_Info('FaltaFazer: TUnTX_EFD_ICMS_IPI.ReopenTXIndPrcOriPallet_Fast(');
end;

procedure TUnTX_EFD_ICMS_IPI.ReopenTXOriPQx(Qry: TmySQLQuery; MovimCod: Integer;
  SQL_PeriodoPQ: String; ShowSQL: Boolean);
begin
(*
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT OrigemCodi, OrigemCtrl, Insumo, DataX, Peso, ',
  'YEAR(DtCorrApo) Ano, MONTH(DtCorrApo) Mes, DtCorrApo, ',
  'CliDest ClientMO ',
  'FROM pqx ',
  'WHERE ( ',
  '  Tipo=' + Geral.FF0(VAR_FATID_0110),
  '  AND OrigemCodi IN ( ',
  '    SELECT Codigo ',
  '    FROM emit ',
  '    WHERE TXMovCod=' + Geral.FF0(MovimCod),
  '  ) ',
  ') ',
  'OR ',
  '( ',
  '  Tipo=' + Geral.FF0(VAR_FATID_0190),
  '  AND OrigemCodi IN ( ',
  '    SELECT Codigo ',
  '    FROM PQO ',
  '    WHERE TXMovCod=' + Geral.FF0(MovimCod),
  '  ) ',
  ') ',
  'AND Peso <> 0',
  SQL_PeriodoPQ,
  '']);
*)
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT OrigemCodi, OrigemCtrl, Insumo, DataX, Peso, ',
  'YEAR(DtCorrApo) Ano, MONTH(DtCorrApo) Mes, DtCorrApo, ',
  'CliDest ClientMO ',
  'FROM pqx ',
  'WHERE ( ',
  '  ( ',
  '    Tipo=' + Geral.FF0(VAR_FATID_0110),
  '    AND OrigemCodi IN ( ',
  '      SELECT Codigo ',
  '      FROM emit ',
  '      WHERE TXMovCod=' + Geral.FF0(MovimCod),
  '    ) ',
  '  ) ',
  '  OR ',
  '  ( ',
  '    Tipo=' + Geral.FF0(VAR_FATID_0190),
  '    AND OrigemCodi IN ( ',
  '      SELECT Codigo ',
  '      FROM PQO ',
  '      WHERE TXMovCod=' + Geral.FF0(MovimCod),
  '    ) ',
  '  ) ',
  ') ',
  'AND Peso <> 0',
  'AND Insumo > 0',
  SQL_PeriodoPQ,
  '']);
  //
  if ShowSQL then
    Geral.MB_Info(Qry.SQL.Text);
end;

function TUnTX_EFD_ICMS_IPI.ReopenTXOriPQx_Fast(Qry: TmySQLDirectQuery;
  MovimCod: Integer; SQL_PeriodoPQ: String; ShowSQL: Boolean): Boolean;
var
  SQL_Emit, SQL_PQO, SQL_Join: String;
begin
{
  Result := UnDmkDAC_PF.AbreMySQLDirectQuery0(Qry, Dmod.MyDB, [
  'SELECT OrigemCodi, OrigemCtrl, Insumo, DataX, Peso, ',
  'YEAR(DtCorrApo) Ano, MONTH(DtCorrApo) Mes, DtCorrApo, ',
  'CliDest ClientMO ',
  'FROM pqx ',
  'WHERE ( ',
  '  ( ',
  '    Tipo=' + Geral.FF0(VAR_FATID_0110),
  '    AND OrigemCodi IN ( ',
  '      SELECT Codigo ',
  '      FROM emit ',
  '      WHERE TXMovCod=' + Geral.FF0(MovimCod),
  '    ) ',
  '  ) ',
  '  OR ',
  '  ( ',
  '    Tipo=' + Geral.FF0(VAR_FATID_0190),
  '    AND OrigemCodi IN ( ',
  '      SELECT Codigo ',
  '      FROM PQO ',
  '      WHERE TXMovCod=' + Geral.FF0(MovimCod),
  '    ) ',
  '  ) ',
  ') ',
  'AND Peso <> 0',
  'AND Insumo > 0',
  SQL_PeriodoPQ,
  '']);
  //
  if ShowSQL then
    Geral.MB_Info(Qry.SQL.Text);
}
////////////////////////////////////////////////////////////////////////////////
  Result   := False;
  SQL_Emit := '';
  SQL_PQO  := '';
  SQL_Join := '';
  //
  UnDmkDAC_PF.AbreMySQLDirectQuery0(Qry, Dmod.MyDB, [
  'SELECT Codigo ',
  'FROM emit ',
  'WHERE TXMovCod=' + Geral.FF0(MovimCod),
  '']);
  Qry.First;
  if Qry.RecordCount > 0 then
  begin
    Result := True;
    SQL_Emit := Qry.FieldValueByFieldName('Codigo');
  end;
  if Qry.RecordCount > 1 then
  begin
    Qry.Next;
    while not Qry.Eof do
    begin
      SQL_Emit := SQL_Emit + ',' + Qry.FieldValueByFieldName('Codigo');
      Qry.Next;
    end;
  end;
  //
  UnDmkDAC_PF.AbreMySQLDirectQuery0(Qry, Dmod.MyDB, [
  'SELECT Codigo ',
  'FROM PQO ',
  'WHERE TXMovCod=' + Geral.FF0(MovimCod),
  '']);
  Qry.First;
  if Qry.RecordCount > 0 then
  begin
    Result := True;
    SQL_PQO := Qry.FieldValueByFieldName('Codigo');
  end;
  if Qry.RecordCount > 1 then
  begin
    Qry.Next;
    while not Qry.Eof do
    begin
      SQL_PQO := SQL_PQO + ',' + Qry.FieldValueByFieldName('Codigo');
      Qry.Next;
    end;
  end;
  //
  if Result then
  begin
    if SQL_Emit <> '' then
      SQL_Emit :=
      '    Tipo=' + Geral.FF0(VAR_FATID_0110) +
      '    AND OrigemCodi IN ( ' + SQL_Emit + ') ';
    if SQL_PQO <> '' then
      SQL_PQO :=
      '    Tipo=' + Geral.FF0(VAR_FATID_0190) +
      '    AND OrigemCodi IN ( ' + SQL_PQO + ') ';
    if (SQL_Emit <> '') and (SQL_PQO <> '') then
       SQL_Join :=
      '  ) ' +
      '  OR ' +
      '  ( ';
    //
    UnDmkDAC_PF.AbreMySQLDirectQuery0(Qry, Dmod.MyDB, [
    'SELECT OrigemCodi, OrigemCtrl, Insumo, DataX, Peso, ',
    'YEAR(DtCorrApo) Ano, MONTH(DtCorrApo) Mes, DtCorrApo, ',
    'CliDest ClientMO ',
    'FROM pqx ',
    'WHERE ( ',
    '  ( ',
    SQL_Emit,
    SQL_Join,
    SQL_PQO,
    '  ) ',
    ') ',
    'AND Peso <> 0',
    'AND Insumo > 0',
    SQL_PeriodoPQ,
    '']);
    //
    if ShowSQL then
      Geral.MB_Info(Qry.SQL.Text);
  end;
end;

procedure TUnTX_EFD_ICMS_IPI.ReopenTXPrcPrcDst(Qry: TmySQLQuery; SrcNivel1,
  MovimCod, Controle, TemIMEIMrt: Integer; SrcMovID: TEstqMovimID;
  DstMovimNiv: TEstqMovimNiv; SQL_Limit: String);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  ATT_MovimID, ATT_MovimNiv: String;
begin
  ATT_MovimID := dmkPF.ArrayToTexto('tmi.MovimID', 'NO_MovimID', pvPos, True,
    sEstqMovimID);
  ATT_MovimNiv := dmkPF.ArrayToTexto('tmi.MovimNiv', 'NO_MovimNiv', pvPos, True,
    sEstqMovimNiv);
  SQL_Flds := Geral.ATS([
  TX_PF.SQL_NO_GGX(),
  TX_PF.SQL_NO_FRN(),
  TX_PF.SQL_NO_SCL(),
  ATT_MovimID,
  ATT_MovimNiv,
  'txp.Nome NO_Pallet ',
  '']);
  SQL_Left := Geral.ATS([
  TX_PF.SQL_LJ_GGX(),
  TX_PF.SQL_LJ_FRN(),
  TX_PF.SQL_LJ_SCL(),
  'LEFT JOIN txpalleta  txp  ON txp.Codigo=tmi.Pallet ',
  '']);
  SQL_Wher := Geral.ATS([
  //'WHERE (',
  'WHERE ',
  'tmi.MovimID=' + Geral.FF0(Integer(SrcMovID)),
  'AND tmi.MovimCod=' + Geral.FF0(MovimCod),
  'AND tmi.MovimNiv=' + Geral.FF0(Integer(DstMovimNiv)),
(*
  'tmi.SrcMovID=' + Geral.FF0(Integer(SrcMovID)),
  'AND tmi.SrcNivel1=' + Geral.FF0(SrcNivel1),
  ') or ( ',
  'tmi.MovimCod=' + Geral.FF0(MovimCod),
  'AND tmi.MovimNiv=' + Geral.FF0(Integer(DstMovimNiv)),
  ') ',
*)
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  TX_PF.GeraSQLTXMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  TX_PF.GeraSQLTXMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Pallet, Controle ',
  '']);
  //Geral.MB_SQL(nil, Qry);
  if Controle <> 0 then
    Qry.Locate('Controle', Controle, []);
end;

procedure TUnTX_EFD_ICMS_IPI.ReopenTXPrcPrcDst_Fast(Qry: TmySQLDirectQuery;
  SrcNivel1, MovimCod, (*Controle,*) TemIMEIMrt: Integer; SrcMovID: TEstqMovimID;
  DstMovimNiv: TEstqMovimNiv; TabIMEI: String; DB_IMEI: TmySQLDatabase; SQL_Limit: String);
const
  sProcName = 'TUnTX_EFD_ICMS_IPI.ReopenTXPrcPrcDst_Fast()';
begin
  UnDmkDAC_PF.AbreMySQLDirectQuery0(Qry, DB_IMEI, [
  'SELECT DataHora, MovimID, MovimCod, Codigo, DtCorrApo, ',
  'Empresa, FornecMO, Controle, GraGruX, MovimNiv, ',
  'MovimTwn, AreaM2, PesoKg, Pecas ',
  'FROM ' + TabIMEI,
  'WHERE MovimID=' + Geral.FF0(Integer(SrcMovID)),
  'AND MovimCod=' + Geral.FF0(MovimCod),
  'AND MovimNiv=' + Geral.FF0(Integer(DstMovimNiv)),
  '']);
  //
  //Geral.MB_Info(Qry.SQL.Text);
(* Na teoria n�o vai dar problema, pois s� se obtem dentro do per�odo,
  ent�o n�o tem como n�o estar na base compactada se existir!
  //////////////////////////////////////////////////////////////////////////////
  if Qry.RecordCount = 0 then
  begin
    UnDmkDAC_PF.AbreMySQLDirectQuery0(Qry, DModG.MyCompressDB, [
    'SELECT DataHora, MovimID, MovimCod, Codigo, DtCorrApo, ',
    'Empresa, FornecMO, Controle, GraGruX, MovimNiv, ',
    'MovimTwn, AreaM2, PesoKg, Pecas ',
    'FROM txmovits ',
    'WHERE MovimID=' + Geral.FF0(Integer(SrcMovID)),
    'AND MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimNiv=' + Geral.FF0(Integer(DstMovimNiv)),
    '']);
    //
    if Qry.RecordCount = 0 then
      Geral.MB_Info('RecordCount=0' + sLineBreak + sProcName + sLineBreak + Qry.SQL.Text);
  end;
*)
end;

procedure TUnTX_EFD_ICMS_IPI.ReopenTXPrcPrcInd(Qry: TmySQLQuery; SrcNivel1,
  MovimCod, Controle, TemIMEIMrt: Integer; SrcMovID: TEstqMovimID;
  DstMovimNiv: TEstqMovimNiv; SQL_Limit: String);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  ATT_MovimID, ATT_MovimNiv: String;
begin
  ATT_MovimID := dmkPF.ArrayToTexto('tmi.MovimID', 'NO_MovimID', pvPos, True,
    sEstqMovimID);
  ATT_MovimNiv := dmkPF.ArrayToTexto('tmi.MovimNiv', 'NO_MovimNiv', pvPos, True,
    sEstqMovimNiv);
  SQL_Flds := Geral.ATS([
  TX_PF.SQL_NO_GGX(),
  TX_PF.SQL_NO_FRN(),
  TX_PF.SQL_NO_SCL(),
  ATT_MovimID,
  ATT_MovimNiv,
  'txp.Nome NO_Pallet ',
  '']);
  SQL_Left := Geral.ATS([
  TX_PF.SQL_LJ_GGX(),
  TX_PF.SQL_LJ_FRN(),
  TX_PF.SQL_LJ_SCL(),
  'LEFT JOIN txpalleta  txp  ON txp.Codigo=tmi.Pallet ',
  '']);
  SQL_Wher := Geral.ATS([
  //'WHERE (',
  'WHERE ',
  'tmi.SrcMovID=' + Geral.FF0(Integer(SrcMovID)),
  //'WHERE tmi.MovimCod=' + Geral.FF0(MovimCod),
  'AND tmi.MovCodPai<>tmi.MovimCod AND tmi.MovCodPai <> 0 AND tmi.MovCodPai=' + Geral.FF0(MovimCod),
{
  ') or ( ',
  'tmi.MovimCod=' + Geral.FF0(MovimCod),
  'AND tmi.MovimNiv=' + Geral.FF0(Integer(DstMovimNiv)),
  ') ',
}
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  TX_PF.GeraSQLTXMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  TX_PF.GeraSQLTXMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Pallet, Controle ',
  '']);
  //Geral.MB_SQL(nil, Qry);
  if Controle <> 0 then
    Qry.Locate('Controle', Controle, []);
end;

procedure TUnTX_EFD_ICMS_IPI.ReopenTXPrcPrcInd_Fast(Qry: TmySQLDirectQuery;
  SrcNivel1, MovimCod, (*Controle,*) TemIMEIMrt: Integer; SrcMovID: TEstqMovimID;
  DstMovimNiv: TEstqMovimNiv; TabIMEI: String; DB_IMEI: TmySQLDatabase;
  BaseDadosDiminuida: Boolean);
const
  sProcName = 'TUnTX_EFD_ICMS_IPI.ReopenTXPrcPrcInd_Fast(';
begin
  if BaseDadosDiminuida then
  begin
    UnDmkDAC_PF.AbreMySQLDirectQuery0(Qry, DB_IMEI, [
    'SELECT  ',
    'DataHora, MovimID, MovimCod, Codigo, ',
    'DtCorrApo, Empresa, FornecMO, ',
    'Controle, GraGruX, MovimNiv, MovimTwn, ',
    'AreaM2, PesoKg, Pecas, SrcMovID,  ',
    'SrcNivel1, SrcNivel2, RmsGGX, JmpGGX, QtdGerPeca, ',
    'QtdGerPeso, QtdGerArM2, JmpMovID, JmpNivel1, ',
    'ClientMO, StqCenLoc, scc_EntiSitio, TipoEstq ',
    'FROM ' + TabIMEI,
    'WHERE SrcMovID=' + Geral.FF0(Integer(SrcMovID)),
    'AND MovCodPai<>MovimCod AND MovCodPai <> 0 AND MovCodPai=' + Geral.FF0(MovimCod),
    '']);
  end;
(* Na teoria n�o vai dar problema, pois s� se obtem dentro do per�odo,
  ent�o n�o tem como n�o estar na base compactada se existir!
  //////////////////////////////////////////////////////////////////////////////
*) // Ver se precisa desmarcar como o ReopenTXPrcPrcDst_Fast
  if (BaseDadosDiminuida = False) or (Qry.RecordCount = 0) then
  begin
    UnDmkDAC_PF.AbreMySQLDirectQuery0(Qry, DModG.MyCompressDB, [
    'SELECT  ',
    'tmi.DataHora, tmi.MovimID, tmi.MovimCod, tmi.Codigo, ',
    'tmi.DtCorrApo, tmi.Empresa, tmi.FornecMO, ',
    'tmi.Controle, tmi.GraGruX, tmi.MovimNiv, tmi.MovimTwn, ',
    'tmi.AreaM2, tmi.PesoKg, tmi.Pecas, tmi.SrcMovID,  ',
    'tmi.SrcNivel1, tmi.SrcNivel2, tmi. RmsGGX, tmi.JmpGGX, tmi.QtdGerPeca, ',
    'tmi.QtdGerPeso, tmi.QtdGerArM2, tmi.JmpMovID, tmi.JmpNivel1, ',
    TX_PF.SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True, 'tmi.Qtde'),
    'tmi.ClientMO, tmi.StqCenLoc, scc.EntiSitio scc_EntiSitio ',
    'FROM ' + CO_SEL_TAB_TMI + ' tmi',
     //
    'LEFT JOIN stqcenloc scl ON scl.Controle=tmi.StqCenLoc ',
    'LEFT JOIN stqcencad scc ON scc.Codigo=scl.Codigo ',
    //
    'LEFT JOIN gragrux    ggx ON ggx.Controle=tmi.GraGruX ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
    'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed',
    'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
    //
    'WHERE tmi.SrcMovID=' + Geral.FF0(Integer(SrcMovID)),
    'AND tmi.MovCodPai<>tmi.MovimCod AND tmi.MovCodPai <> 0 AND tmi.MovCodPai=' + Geral.FF0(MovimCod),
    '']);
    //if Qry.RecordCount = 0 then
      //Geral.MB_Info('RecordCount=0' + sLineBreak + sProcName + sLineBreak + Qry.SQL.Text);
  end;
end;

end.
