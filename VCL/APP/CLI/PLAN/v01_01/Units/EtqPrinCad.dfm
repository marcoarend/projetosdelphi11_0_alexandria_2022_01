object FmEtqPrinCad: TFmEtqPrinCad
  Left = 368
  Top = 194
  Caption = 'ETQ-PRINT-001 :: Configura'#231#227'o de Impressoras'
  ClientHeight = 593
  ClientWidth = 784
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 545
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Visible = False
    object PainelConfirma: TPanel
      Left = 1
      Top = 496
      Width = 782
      Height = 48
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 8
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 673
        Top = 1
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object PainelEdit: TPanel
      Left = 1
      Top = 1
      Width = 782
      Height = 400
      Align = alTop
      TabOrder = 0
      object Label7: TLabel
        Left = 720
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
      end
      object Label8: TLabel
        Left = 224
        Top = 4
        Width = 158
        Height = 13
        Caption = 'Modelo de impress'#227'o (descri'#231#227'o):'
      end
      object SpeedButton5: TSpeedButton
        Left = 280
        Top = 20
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton5Click
      end
      object Label2: TLabel
        Left = 224
        Top = 44
        Width = 106
        Height = 13
        Caption = 'Modelo da impressora:'
      end
      object Label9: TLabel
        Left = 732
        Top = 44
        Width = 42
        Height = 13
        Caption = 'N'#186' porta:'
      end
      object EdCodigo: TdmkEdit
        Left = 720
        Top = 20
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdNome: TdmkEdit
        Left = 304
        Top = 20
        Width = 281
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdCodUsu: TdmkEdit
        Left = 224
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CodUsu'
        UpdCampo = 'CodUsu'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnKeyDown = EdCodUsuKeyDown
      end
      object RGTypeUso: TdmkRadioGroup
        Left = 8
        Top = 4
        Width = 213
        Height = 38
        Caption = ' Grupo: '
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          'Produto'
          'Pessoa'
          'Expedi'#231#227'o')
        TabOrder = 0
        QryCampo = 'TypeUso'
        UpdCampo = 'TypeUso'
        UpdType = utYes
        OldValor = 0
      end
      object RGTypePrn: TdmkRadioGroup
        Left = 8
        Top = 44
        Width = 213
        Height = 38
        Caption = ' Tipo de impressora: '
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'T'#233'rmica'
          'Matricial')
        TabOrder = 5
        QryCampo = 'TypePrn'
        UpdCampo = 'TypePrn'
        UpdType = utYes
        OldValor = 0
      end
      object EdModelo: TdmkEdit
        Left = 224
        Top = 60
        Width = 389
        Height = 21
        TabOrder = 6
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Modelo'
        UpdCampo = 'Modelo'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object RGPortaImp: TdmkRadioGroup
        Left = 616
        Top = 44
        Width = 113
        Height = 38
        Caption = ' Porta de impress'#227'o: '
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'COM'
          'LPT')
        TabOrder = 7
        TabStop = True
        QryCampo = 'PortaImp'
        UpdCampo = 'PortaImp'
        UpdType = utYes
        OldValor = 0
      end
      object EdPortaIdx: TdmkEdit
        Left = 732
        Top = 60
        Width = 44
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'PortaIdx'
        UpdCampo = 'PortaIdx'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object GroupBox4: TGroupBox
        Left = 8
        Top = 144
        Width = 465
        Height = 77
        Caption = ' Formul'#225'rio: '
        TabOrder = 9
        TabStop = True
        object dmkGBMedidasLabel: TGroupBox
          Left = 177
          Top = 15
          Width = 286
          Height = 60
          Align = alClient
          Caption = ' Label (em ?????????): '
          TabOrder = 1
          TabStop = True
          object Label4: TLabel
            Left = 8
            Top = 16
            Width = 39
            Height = 13
            Caption = 'Largura:'
          end
          object Label1: TLabel
            Left = 76
            Top = 16
            Width = 30
            Height = 13
            Caption = 'Altura:'
          end
          object Label11: TLabel
            Left = 144
            Top = 16
            Width = 53
            Height = 13
            Caption = 'Marg. esq.:'
          end
          object Label12: TLabel
            Left = 212
            Top = 16
            Width = 64
            Height = 13
            Caption = 'Gap (coluna):'
          end
          object EdLabelLarg: TdmkEdit
            Left = 8
            Top = 32
            Width = 65
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            QryCampo = 'LabelLarg'
            UpdCampo = 'LabelLarg'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object EdLabelAltu: TdmkEdit
            Left = 76
            Top = 32
            Width = 65
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            QryCampo = 'LabelAltu'
            UpdCampo = 'LabelAltu'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object EdLabelMEsq: TdmkEdit
            Left = 144
            Top = 32
            Width = 65
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            QryCampo = 'LabelMEsq'
            UpdCampo = 'LabelMEsq'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object EdLabelLGap: TdmkEdit
            Left = 212
            Top = 32
            Width = 65
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            QryCampo = 'LabelLGap'
            UpdCampo = 'LabelLGap'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
        end
        object GroupBox6: TGroupBox
          Left = 2
          Top = 15
          Width = 175
          Height = 60
          Align = alLeft
          Caption = ' Geral: '
          TabOrder = 0
          TabStop = True
          object dmkLaFormLargura: TLabel
            Left = 104
            Top = 16
            Width = 66
            Height = 13
            Caption = 'Largura (???):'
          end
          object Label6: TLabel
            Left = 56
            Top = 16
            Width = 21
            Height = 13
            Caption = 'DPI:'
          end
          object Label10: TLabel
            Left = 8
            Top = 16
            Width = 41
            Height = 13
            Caption = 'Colunas:'
          end
          object EdFormuLarg: TdmkEdit
            Left = 104
            Top = 32
            Width = 65
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            QryCampo = 'FormuLarg'
            UpdCampo = 'FormuLarg'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object EdDPI: TdmkEdit
            Left = 56
            Top = 32
            Width = 44
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'DPI'
            UpdCampo = 'DPI'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object EdColunas: TdmkEdit
            Left = 8
            Top = 32
            Width = 44
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Colunas'
            UpdCampo = 'Colunas'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
        end
      end
      object GroupBox11: TGroupBox
        Left = 8
        Top = 84
        Width = 465
        Height = 61
        Caption = ' Padr'#245'es de configura'#231#227'o: '
        TabOrder = 10
        object GroupBox1: TGroupBox
          Left = 2
          Top = 15
          Width = 111
          Height = 44
          Align = alLeft
          Caption = ' Usar padr'#245'es de: '
          TabOrder = 0
          object CkDefaultIni: TdmkCheckBox
            Left = 12
            Top = 20
            Width = 49
            Height = 17
            Caption = 'In'#237'cio.'
            TabOrder = 0
            QryCampo = 'DefaultIni'
            UpdCampo = 'DefaultIni'
            UpdType = utYes
            ValCheck = #0
            ValUncheck = #0
            OldValor = #0
          end
          object CkDefaultFim: TdmkCheckBox
            Left = 64
            Top = 20
            Width = 45
            Height = 17
            Caption = 'Final.'
            TabOrder = 1
            QryCampo = 'DefaultFim'
            UpdCampo = 'DefaultFim'
            UpdType = utYes
            ValCheck = #0
            ValUncheck = #0
            OldValor = #0
          end
        end
        object GroupBox3: TGroupBox
          Left = 113
          Top = 15
          Width = 350
          Height = 44
          Align = alClient
          Caption = 'Configura'#231#245'es selecion'#225'veis de in'#237'cio: '
          TabOrder = 1
          object CkDefFeedBac: TdmkCheckBox
            Left = 12
            Top = 20
            Width = 113
            Height = 17
            Caption = 'Retorno autom'#225'tico.'
            TabOrder = 0
            QryCampo = 'DefFeedBac'
            UpdCampo = 'DefFeedBac'
            UpdType = utYes
            ValCheck = #0
            ValUncheck = #0
            OldValor = #0
          end
          object CkDefMinPixel: TdmkCheckBox
            Left = 128
            Top = 20
            Width = 141
            Height = 17
            Caption = 'Tamanho m'#237'nimo de pixel.'
            TabOrder = 1
            QryCampo = 'DefMinPixel'
            UpdCampo = 'DefMinPixel'
            UpdType = utYes
            ValCheck = #0
            ValUncheck = #0
            OldValor = #0
          end
        end
      end
      object RGLinguagem: TdmkRadioGroup
        Left = 476
        Top = 84
        Width = 301
        Height = 137
        Caption = ' Linguagem de programa'#231#227'o: '
        ItemIndex = 0
        Items.Strings = (
          'N'#227'o implementada neste aplicativo.'
          'Argox PPLA (Datamax)'
          'Argox PPLB (Eltron)')
        TabOrder = 11
        QryCampo = 'Linguagem'
        UpdCampo = 'Linguagem'
        UpdType = utYes
        OldValor = 0
      end
      object RGMeasure: TdmkRadioGroup
        Left = 588
        Top = 4
        Width = 129
        Height = 38
        Caption = ' Unidade de medida: '
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'M'#233'trico'
          'Inches')
        TabOrder = 3
        TabStop = True
        OnClick = RGMeasureClick
        QryCampo = 'Measure'
        UpdCampo = 'Measure'
        UpdType = utYes
        OldValor = 0
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 545
    Align = alClient
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object PainelControle: TPanel
      Left = 1
      Top = 496
      Width = 782
      Height = 48
      Align = alBottom
      TabOrder = 0
      object LaRegistro: TdmkLabel
        Left = 173
        Top = 1
        Width = 26
        Height = 13
        Align = alClient
        Caption = '[N]: 0'
        UpdType = utYes
        SQLType = stNil
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 172
        Height = 46
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object Panel3: TPanel
        Left = 312
        Top = 1
        Width = 469
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object BtLinha: TBitBtn
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Linhas'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtLinhaClick
        end
        object BtConfigura: TBitBtn
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Configura'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtConfiguraClick
        end
        object Panel2: TPanel
          Left = 360
          Top = 0
          Width = 109
          Height = 46
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 2
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtLotes: TBitBtn
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = 'L&otes'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtLotesClick
        end
      end
    end
    object Panel4: TPanel
      Left = 1
      Top = 232
      Width = 782
      Height = 264
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 1
      object Panel_1: TPanel
        Left = 0
        Top = 88
        Width = 782
        Height = 88
        Align = alClient
        TabOrder = 0
        object Panel7: TPanel
          Left = 1
          Top = 1
          Width = 48
          Height = 86
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object BtAcima_1: TBitBtn
            Tag = 32
            Left = 4
            Top = 3
            Width = 40
            Height = 40
            Cursor = crHandPoint
            Enabled = False
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtAcima_1Click
          end
          object BtAbaixo_1: TBitBtn
            Tag = 31
            Left = 4
            Top = 43
            Width = 40
            Height = 40
            Cursor = crHandPoint
            Enabled = False
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BtAbaixo_1Click
          end
        end
        object DBGCmd_1: TDBGrid
          Left = 49
          Top = 1
          Width = 732
          Height = 86
          Align = alClient
          DataSource = DsEtqPrinCmd_1
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDblClick = DBGCmd_1DblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Ordem'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Config'
              Width = 660
              Visible = True
            end>
        end
      end
      object Panel_0: TPanel
        Left = 0
        Top = 0
        Width = 782
        Height = 88
        Align = alTop
        TabOrder = 1
        object Panel9: TPanel
          Left = 1
          Top = 1
          Width = 48
          Height = 86
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object BtAcima_0: TBitBtn
            Tag = 32
            Left = 4
            Top = 3
            Width = 40
            Height = 40
            Cursor = crHandPoint
            Enabled = False
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtAcima_0Click
          end
          object BtAbaixo_0: TBitBtn
            Tag = 31
            Left = 4
            Top = 43
            Width = 40
            Height = 40
            Cursor = crHandPoint
            Enabled = False
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BtAbaixo_0Click
          end
        end
        object DBGCmd_0: TDBGrid
          Left = 49
          Top = 1
          Width = 732
          Height = 86
          Align = alClient
          DataSource = DsEtqPrinCmd_0
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDblClick = DBGCmd_0DblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Ordem'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Config'
              Width = 660
              Visible = True
            end>
        end
      end
      object Panel_2: TPanel
        Left = 0
        Top = 176
        Width = 782
        Height = 88
        Align = alBottom
        TabOrder = 2
        object Panel11: TPanel
          Left = 1
          Top = 1
          Width = 48
          Height = 86
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object BtAcima_2: TBitBtn
            Tag = 32
            Left = 4
            Top = 3
            Width = 40
            Height = 40
            Cursor = crHandPoint
            Enabled = False
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtAcima_2Click
          end
          object BtAbaixo_2: TBitBtn
            Tag = 31
            Left = 4
            Top = 43
            Width = 40
            Height = 40
            Cursor = crHandPoint
            Enabled = False
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BtAbaixo_2Click
          end
        end
        object DBGCmd_2: TDBGrid
          Left = 49
          Top = 1
          Width = 732
          Height = 86
          Align = alClient
          DataSource = DsEtqPrinCmd_2
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDblClick = DBGCmd_2DblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Ordem'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Config'
              Width = 660
              Visible = True
            end>
        end
      end
    end
    object PnCabeca: TPanel
      Left = 1
      Top = 1
      Width = 782
      Height = 224
      Align = alTop
      Enabled = False
      TabOrder = 2
      object Label13: TLabel
        Left = 720
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdit7
      end
      object Label18: TLabel
        Left = 8
        Top = 4
        Width = 32
        Height = 13
        Caption = 'Grupo:'
        FocusControl = DBEdit12
      end
      object Label19: TLabel
        Left = 8
        Top = 44
        Width = 89
        Height = 13
        Caption = 'Tipo de impress'#227'o:'
        FocusControl = DBEdit14
      end
      object Label20: TLabel
        Left = 224
        Top = 44
        Width = 106
        Height = 13
        Caption = 'Modelo da impressora:'
        FocusControl = DBEdit16
      end
      object Label21: TLabel
        Left = 616
        Top = 44
        Width = 93
        Height = 13
        Caption = 'Porta de impress'#227'o:'
        FocusControl = DBEdit17
      end
      object Label22: TLabel
        Left = 732
        Top = 44
        Width = 42
        Height = 13
        Caption = 'N'#186' porta:'
        FocusControl = DBEdit19
      end
      object Label17: TLabel
        Left = 224
        Top = 4
        Width = 158
        Height = 13
        Caption = 'Modelo de impress'#227'o (descri'#231#227'o):'
      end
      object DBEdit7: TDBEdit
        Left = 720
        Top = 20
        Width = 57
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsEtqPrinCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxLength = 1
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdit8: TDBEdit
        Left = 284
        Top = 20
        Width = 301
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsEtqPrinCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxLength = 5
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object DBEdit11: TDBEdit
        Left = 224
        Top = 20
        Width = 56
        Height = 21
        DataField = 'CodUsu'
        DataSource = DsEtqPrinCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
      end
      object DBEdit12: TDBEdit
        Left = 8
        Top = 20
        Width = 36
        Height = 21
        DataField = 'TypeUso'
        DataSource = DsEtqPrinCad
        TabOrder = 3
      end
      object DBEdit13: TDBEdit
        Left = 48
        Top = 20
        Width = 173
        Height = 21
        DataField = 'NOMETYPEUSO'
        DataSource = DsEtqPrinCad
        TabOrder = 4
      end
      object DBEdit14: TDBEdit
        Left = 8
        Top = 60
        Width = 36
        Height = 21
        DataField = 'TypePrn'
        DataSource = DsEtqPrinCad
        TabOrder = 5
      end
      object DBEdit15: TDBEdit
        Left = 48
        Top = 60
        Width = 173
        Height = 21
        DataField = 'NOMETYPEPRN'
        DataSource = DsEtqPrinCad
        TabOrder = 6
      end
      object DBEdit16: TDBEdit
        Left = 224
        Top = 60
        Width = 389
        Height = 21
        DataField = 'Modelo'
        DataSource = DsEtqPrinCad
        TabOrder = 7
      end
      object DBEdit17: TDBEdit
        Left = 616
        Top = 60
        Width = 36
        Height = 21
        DataField = 'PortaImp'
        DataSource = DsEtqPrinCad
        TabOrder = 8
      end
      object DBEdit18: TDBEdit
        Left = 656
        Top = 60
        Width = 72
        Height = 21
        DataField = 'NOMEPORTAIMP'
        DataSource = DsEtqPrinCad
        TabOrder = 9
      end
      object DBEdit19: TDBEdit
        Left = 732
        Top = 60
        Width = 45
        Height = 21
        DataField = 'PortaIdx'
        DataSource = DsEtqPrinCad
        TabOrder = 10
      end
      object GroupBox8: TGroupBox
        Left = 8
        Top = 144
        Width = 465
        Height = 77
        Caption = ' Formul'#225'rio: '
        TabOrder = 11
        object DBGBMedidasLabel: TGroupBox
          Left = 177
          Top = 15
          Width = 286
          Height = 60
          Align = alClient
          Caption = ' Label (em ??): '
          TabOrder = 0
          object Label14: TLabel
            Left = 8
            Top = 16
            Width = 39
            Height = 13
            Caption = 'Largura:'
          end
          object Label15: TLabel
            Left = 76
            Top = 16
            Width = 30
            Height = 13
            Caption = 'Altura:'
          end
          object Label16: TLabel
            Left = 144
            Top = 16
            Width = 53
            Height = 13
            Caption = 'Marg. esq.:'
          end
          object Label24: TLabel
            Left = 212
            Top = 16
            Width = 64
            Height = 13
            Caption = 'Gap (coluna):'
          end
          object DBEdit3: TDBEdit
            Left = 8
            Top = 32
            Width = 64
            Height = 21
            DataField = 'LabelLarg'
            DataSource = DsEtqPrinCad
            TabOrder = 0
          end
          object DBEdit4: TDBEdit
            Left = 76
            Top = 32
            Width = 64
            Height = 21
            DataField = 'LabelAltu'
            DataSource = DsEtqPrinCad
            TabOrder = 1
          end
          object DBEdit5: TDBEdit
            Left = 144
            Top = 32
            Width = 64
            Height = 21
            DataField = 'LabelMEsq'
            DataSource = DsEtqPrinCad
            TabOrder = 2
          end
          object DBEdit6: TDBEdit
            Left = 212
            Top = 32
            Width = 64
            Height = 21
            DataField = 'LabelLGap'
            DataSource = DsEtqPrinCad
            TabOrder = 3
          end
        end
        object GroupBox10: TGroupBox
          Left = 2
          Top = 15
          Width = 175
          Height = 60
          Align = alLeft
          Caption = ' Geral: '
          TabOrder = 1
          object LaFormLargura: TLabel
            Left = 104
            Top = 16
            Width = 60
            Height = 13
            Caption = 'Largura (??):'
          end
          object Label23: TLabel
            Left = 56
            Top = 16
            Width = 21
            Height = 13
            Caption = 'DPI:'
            FocusControl = DBEdit20
          end
          object Label3: TLabel
            Left = 8
            Top = 16
            Width = 41
            Height = 13
            Caption = 'Colunas:'
            FocusControl = DBEdit1
          end
          object DBEdit20: TDBEdit
            Left = 56
            Top = 32
            Width = 45
            Height = 21
            DataField = 'DPI'
            DataSource = DsEtqPrinCad
            TabOrder = 0
          end
          object DBEdit1: TDBEdit
            Left = 8
            Top = 32
            Width = 45
            Height = 21
            DataField = 'Colunas'
            DataSource = DsEtqPrinCad
            TabOrder = 1
          end
          object DBEdit2: TDBEdit
            Left = 104
            Top = 32
            Width = 64
            Height = 21
            DataField = 'FormuLarg'
            DataSource = DsEtqPrinCad
            TabOrder = 2
          end
        end
      end
      object GroupBox12: TGroupBox
        Left = 8
        Top = 84
        Width = 465
        Height = 61
        Caption = ' Padr'#245'es de configura'#231#227'o: '
        TabOrder = 12
        object GroupBox2: TGroupBox
          Left = 2
          Top = 15
          Width = 111
          Height = 44
          Align = alLeft
          Caption = ' Usar padr'#245'es de: '
          TabOrder = 0
          object DBCheckBox1: TDBCheckBox
            Left = 8
            Top = 20
            Width = 49
            Height = 17
            Caption = 'In'#237'cio.'
            DataField = 'DefaultIni'
            DataSource = DsEtqPrinCad
            TabOrder = 0
            ValueChecked = 'True'
            ValueUnchecked = 'False'
          end
          object DBCheckBox2: TDBCheckBox
            Left = 64
            Top = 20
            Width = 40
            Height = 17
            Caption = 'Final.'
            DataField = 'DefaultIni'
            DataSource = DsEtqPrinCad
            TabOrder = 1
            ValueChecked = 'True'
            ValueUnchecked = 'False'
          end
        end
        object GroupBox7: TGroupBox
          Left = 113
          Top = 15
          Width = 350
          Height = 44
          Align = alClient
          Caption = 'Padr'#245'es de configura'#231#227'o: '
          TabOrder = 1
          object DBCheckBox3: TDBCheckBox
            Left = 12
            Top = 20
            Width = 113
            Height = 17
            Caption = 'Retorno autom'#225'tico.'
            DataField = 'DefFeedBac'
            DataSource = DsEtqPrinCad
            TabOrder = 0
            ValueChecked = 'True'
            ValueUnchecked = 'False'
          end
          object DBCheckBox4: TDBCheckBox
            Left = 132
            Top = 20
            Width = 145
            Height = 17
            Caption = 'Tamanho m'#237'nimo de pixel.'
            DataField = 'DefMinPixel'
            DataSource = DsEtqPrinCad
            TabOrder = 1
            ValueChecked = 'True'
            ValueUnchecked = 'False'
          end
        end
      end
      object DBRadioGroup1: TDBRadioGroup
        Left = 476
        Top = 84
        Width = 301
        Height = 137
        Caption = ' Linguagem de programa'#231#227'o: '
        DataField = 'Linguagem'
        DataSource = DsEtqPrinCad
        Items.Strings = (
          'N'#227'o implementada neste aplicativo.'
          'Argox PPLA (Datamax)'
          'Argox PPLB (Eltron)')
        ParentBackground = True
        TabOrder = 13
        Values.Strings = (
          '0'
          '1'
          '2'
          '3'
          '4'
          '5'
          '6'
          '7'
          '8'
          '9')
      end
      object dmkRadioGroup1: TDBRadioGroup
        Left = 588
        Top = 4
        Width = 129
        Height = 38
        Caption = ' Unidade de medida: '
        Columns = 2
        DataField = 'Measure'
        DataSource = DsEtqPrinCad
        Items.Strings = (
          'M'#233'trico'
          'Inches')
        ParentBackground = True
        TabOrder = 14
        TabStop = True
        Values.Strings = (
          '0'
          '1')
        OnChange = dmkRadioGroup1Change
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    Caption = '                              Configura'#231#227'o de Impressoras'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object LaTipo: TdmkLabel
      Left = 701
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 603
      ExplicitTop = 2
      ExplicitHeight = 44
    end
    object Image1: TImage
      Left = 226
      Top = 1
      Width = 475
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 227
      ExplicitTop = 2
      ExplicitWidth = 376
      ExplicitHeight = 44
    end
    object PanelFill2: TPanel
      Left = 1
      Top = 1
      Width = 225
      Height = 46
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
  end
  object DsEtqPrinCad: TDataSource
    DataSet = QrEtqPrinCad
    Left = 40
    Top = 12
  end
  object QrEtqPrinCad: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrEtqPrinCadBeforeOpen
    AfterOpen = QrEtqPrinCadAfterOpen
    BeforeClose = QrEtqPrinCadBeforeClose
    AfterScroll = QrEtqPrinCadAfterScroll
    SQL.Strings = (
      'SELECT prc.Codigo, prc.CodUsu, prc.Nome, prc.TypeUso,'
      'ELT(prc.TypeUso + 1, "Produto", "Pessoa", "Expedi'#231#227'o") '
      'NOMETYPEUSO,  prc.TypePrn, ELT(prc.TypePrn + 1, "T'#233'rmica", '
      '"Matricial") NOMETYPEPRN, prc.PortaImp, ELT(prc.PortaImp+1, '
      '"COM", "LPT") NOMEPORTAIMP, prc.PortaIdx, prc.Modelo, '
      'prc.Colunas, prc.IdVarIni, prc.IdVarFmt, prc.IdVarFim, prc.DPI,'
      'prc.DefaultIni, DefaultFim, DefMinPixel, DefFeedBac,'
      'LabelLarg, LabelAltu, LabelMEsq, LabelLGap, FormuLarg,'
      'Linguagem, Measure'
      'FROM etqprincad prc')
    Left = 12
    Top = 12
    object QrEtqPrinCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'etqprincad.Codigo'
      Required = True
    end
    object QrEtqPrinCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'etqprincad.CodUsu'
      Required = True
    end
    object QrEtqPrinCadNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'etqprincad.Nome'
      Required = True
      Size = 50
    end
    object QrEtqPrinCadTypeUso: TSmallintField
      FieldName = 'TypeUso'
      Origin = 'etqprincad.TypeUso'
      Required = True
    end
    object QrEtqPrinCadNOMETYPEUSO: TWideStringField
      FieldName = 'NOMETYPEUSO'
      Size = 9
    end
    object QrEtqPrinCadTypePrn: TSmallintField
      FieldName = 'TypePrn'
      Origin = 'etqprincad.TypePrn'
      Required = True
    end
    object QrEtqPrinCadNOMETYPEPRN: TWideStringField
      FieldName = 'NOMETYPEPRN'
      Size = 9
    end
    object QrEtqPrinCadPortaImp: TSmallintField
      FieldName = 'PortaImp'
      Origin = 'etqprincad.PortaImp'
      Required = True
    end
    object QrEtqPrinCadNOMEPORTAIMP: TWideStringField
      FieldName = 'NOMEPORTAIMP'
      Size = 3
    end
    object QrEtqPrinCadPortaIdx: TSmallintField
      FieldName = 'PortaIdx'
      Origin = 'etqprincad.PortaIdx'
      Required = True
    end
    object QrEtqPrinCadModelo: TWideStringField
      FieldName = 'Modelo'
      Origin = 'etqprincad.Modelo'
      Size = 30
    end
    object QrEtqPrinCadColunas: TSmallintField
      FieldName = 'Colunas'
      Origin = 'etqprincad.Colunas'
      Required = True
    end
    object QrEtqPrinCadIdVarIni: TWideStringField
      FieldName = 'IdVarIni'
      Origin = 'etqprincad.IdVarIni'
      Required = True
      Size = 1
    end
    object QrEtqPrinCadIdVarFmt: TWideStringField
      FieldName = 'IdVarFmt'
      Origin = 'etqprincad.IdVarFmt'
      Required = True
      Size = 1
    end
    object QrEtqPrinCadIdVarFim: TWideStringField
      FieldName = 'IdVarFim'
      Origin = 'etqprincad.IdVarFim'
      Required = True
      Size = 1
    end
    object QrEtqPrinCadDPI: TIntegerField
      FieldName = 'DPI'
      Origin = 'etqprincad.DPI'
      Required = True
    end
    object QrEtqPrinCadDefaultIni: TSmallintField
      FieldName = 'DefaultIni'
      Origin = 'etqprincad.DefaultIni'
      Required = True
    end
    object QrEtqPrinCadDefaultFim: TSmallintField
      FieldName = 'DefaultFim'
      Origin = 'etqprincad.DefaultFim'
      Required = True
    end
    object QrEtqPrinCadDefMinPixel: TSmallintField
      FieldName = 'DefMinPixel'
      Origin = 'etqprincad.DefMinPixel'
      Required = True
    end
    object QrEtqPrinCadDefFeedBac: TSmallintField
      FieldName = 'DefFeedBac'
      Origin = 'etqprincad.DefFeedBac'
      Required = True
    end
    object QrEtqPrinCadLabelLarg: TFloatField
      FieldName = 'LabelLarg'
      Origin = 'etqprincad.LabelLarg'
      Required = True
      DisplayFormat = '#,###,##0.000'
    end
    object QrEtqPrinCadLabelAltu: TFloatField
      FieldName = 'LabelAltu'
      Origin = 'etqprincad.LabelAltu'
      Required = True
      DisplayFormat = '#,###,##0.000'
    end
    object QrEtqPrinCadLabelMEsq: TFloatField
      FieldName = 'LabelMEsq'
      Origin = 'etqprincad.LabelMEsq'
      Required = True
      DisplayFormat = '#,###,##0.000'
    end
    object QrEtqPrinCadLabelLGap: TFloatField
      FieldName = 'LabelLGap'
      Origin = 'etqprincad.LabelLGap'
      Required = True
      DisplayFormat = '#,###,##0.000'
    end
    object QrEtqPrinCadFormuLarg: TFloatField
      FieldName = 'FormuLarg'
      Origin = 'etqprincad.FormuLarg'
      Required = True
      DisplayFormat = '#,###,##0.000'
    end
    object QrEtqPrinCadLinguagem: TSmallintField
      FieldName = 'Linguagem'
      Origin = 'etqprincad.Linguagem'
      Required = True
    end
    object QrEtqPrinCadMeasure: TSmallintField
      FieldName = 'Measure'
      Required = True
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtConfigura
    CanUpd01 = BtLinha
    Left = 68
    Top = 12
  end
  object PMConfigura: TPopupMenu
    OnPopup = PMConfiguraPopup
    Left = 376
    Top = 444
    object Incluinovaconfigurao1: TMenuItem
      Caption = 'Inclui nova configura'#231#227'o'
      OnClick = Incluinovaconfigurao1Click
    end
    object Alteraconfiguraoatual1: TMenuItem
      Caption = '&Altera configura'#231#227'o atual'
      OnClick = Alteraconfiguraoatual1Click
    end
    object Excluiconfiguraoatual1: TMenuItem
      Caption = '&Exclui configura'#231#227'o atual'
      Enabled = False
    end
  end
  object QrEtqPrinCmd_0: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrEtqPrinCmd_0AfterOpen
    BeforeClose = QrEtqPrinCmd_0BeforeClose
    AfterScroll = QrEtqPrinCmd_0AfterScroll
    SQL.Strings = (
      'SELECT * '
      'FROM etqprincmd'
      'WHERE Tipo=0'
      'AND Codigo=:P0'
      'ORDER BY Ordem')
    Left = 196
    Top = 444
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEtqPrinCmd_0Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEtqPrinCmd_0Tipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEtqPrinCmd_0Ordem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrEtqPrinCmd_0Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEtqPrinCmd_0Config: TWideStringField
      FieldName = 'Config'
      Size = 255
    end
    object QrEtqPrinCmd_0Coluna: TIntegerField
      FieldName = 'Coluna'
    end
  end
  object DsEtqPrinCmd_0: TDataSource
    DataSet = QrEtqPrinCmd_0
    Left = 224
    Top = 444
  end
  object QrEtqPrinCmd_1: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEtqPrinCmd_1BeforeClose
    AfterScroll = QrEtqPrinCmd_1AfterScroll
    SQL.Strings = (
      'SELECT * '
      'FROM etqprincmd'
      'WHERE Tipo=1'
      'AND Codigo=:P0'
      'ORDER BY Ordem')
    Left = 256
    Top = 444
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEtqPrinCmd_1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEtqPrinCmd_1Tipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEtqPrinCmd_1Ordem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrEtqPrinCmd_1Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEtqPrinCmd_1Config: TWideStringField
      FieldName = 'Config'
      Size = 255
    end
    object QrEtqPrinCmd_1Coluna: TIntegerField
      FieldName = 'Coluna'
    end
  end
  object DsEtqPrinCmd_1: TDataSource
    DataSet = QrEtqPrinCmd_1
    Left = 284
    Top = 444
  end
  object QrEtqPrinCmd_2: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrEtqPrinCmd_2AfterOpen
    BeforeClose = QrEtqPrinCmd_2BeforeClose
    AfterScroll = QrEtqPrinCmd_2AfterScroll
    SQL.Strings = (
      'SELECT * '
      'FROM etqprincmd'
      'WHERE Tipo=2'
      'AND Codigo=:P0'
      'ORDER BY Ordem')
    Left = 316
    Top = 444
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEtqPrinCmd_2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEtqPrinCmd_2Tipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEtqPrinCmd_2Ordem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrEtqPrinCmd_2Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEtqPrinCmd_2Config: TWideStringField
      FieldName = 'Config'
      Size = 255
    end
    object QrEtqPrinCmd_2Coluna: TIntegerField
      FieldName = 'Coluna'
    end
  end
  object DsEtqPrinCmd_2: TDataSource
    DataSet = QrEtqPrinCmd_2
    Left = 344
    Top = 444
  end
  object PMLinha: TPopupMenu
    Left = 404
    Top = 444
    object Incio2: TMenuItem
      Caption = '&In'#237'cio'
      OnClick = Incio2Click
    end
    object Dados3: TMenuItem
      Caption = '&Dados'
      OnClick = Dados3Click
    end
    object Fim1: TMenuItem
      Caption = '&Fim'
      OnClick = Fim1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Antigo1: TMenuItem
      Caption = '&Antigo'
      object Alteralinhadecomandoatual1: TMenuItem
        Caption = '&Altera linha de comando atual'
        OnClick = Alteralinhadecomandoatual1Click
        object Incio1: TMenuItem
          Caption = '&In'#237'cio'
          OnClick = Incio1Click
        end
        object Dados1: TMenuItem
          Caption = '&Dados'
          OnClick = Dados1Click
        end
        object Final1: TMenuItem
          Caption = '&Final'
          OnClick = Final1Click
        end
      end
      object Excluilinhadecomandoatual1: TMenuItem
        Caption = '&Exclui linha de comando atual'
        object Inicio1: TMenuItem
          Caption = '&In'#237'cio'
          OnClick = Inicio1Click
        end
        object Dados2: TMenuItem
          Caption = '&Dados'
          OnClick = Dados2Click
        end
        object Final2: TMenuItem
          Caption = '&Final'
          OnClick = Final2Click
        end
      end
      object Incluinovalinhadecomando1: TMenuItem
        Caption = '&Inclui nova linha de comando'
        OnClick = Incluinovalinhadecomando1Click
      end
    end
  end
  object QrOrd: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM etqprincmd'
      'WHERE Tipo=:P0'
      'AND Codigo=:P1'
      'ORDER BY Ordem')
    Left = 436
    Top = 444
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrOrdOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrOrdControle: TIntegerField
      FieldName = 'Controle'
    end
  end
end
