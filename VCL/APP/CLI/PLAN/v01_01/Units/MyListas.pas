unit MyListas;

interface

uses
  System.Generics.Collections,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs, Forms,
  ExtCtrls, ComCtrls, StdCtrls, DB, (*DBTables,*) mysqlDBTables, UnMyLinguas,
  UnInternalConsts, UnMLAGeral, dmkGeral, UnDmkProcFunc, UnDmkEnums,
  UnAppEnums, AppListas;

type
  TMyListas = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    (*
    function CriaListaImpDOS(FImpDOS: TStringList): Boolean;
    function CriaListaUserSets(FUserSets: TStringList): Boolean;
    *)
    //
    function CriaListaTabelas(Database: TmySQLDatabase; Lista:
              TList<TTabelas>): Boolean;
    function CriaListaTabelasLocais(Lista: TList<TTabelas>): Boolean;
    function CriaListaIndices(TabelaBase, TabelaNome: String;
             FLIndices: TList<TIndices>): Boolean;
    function CriaListaCampos(Tabela: String; FLCampos: TList<TCampos>;
             var TemControle: TTemControle): Boolean;
    function CriaListaQeiLnk(TabelaCria: String; FLQeiLnk: TList<TQeiLnk>): Boolean;
    function CriaListaJanelas(FLJanelas: TList<TJanelas>): Boolean;
    function CriaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
    function CompletaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
    function ExcluiTab: Boolean;
    function ExcluiReg: Boolean;
    function ExcluiIdx: Boolean;
    procedure VerificaOutrosAntes(DataBase: TmySQLDatabase; Memo: TMemo);
    procedure VerificaOutrosDepois(DataBase: TmySQLDatabase; Memo: TMemo);
    procedure ModificaDadosDeIndicesAlterados(Indice, Tabela: String;
              DataBase: TmySQLDatabase; Memo: TMemo);
    procedure ConfiguracoesIniciais(UsaCoresRel: integer);
  end;

const
  CO_VERMCW = 1904101739;
  CO_VERMLA = 2001151415;
  //CO_VERSAO = 1904101739;
  CO_VERNEW = 0;
  CO_SIGLA_APP = 'PLAN';
  CO_DMKID_APP = 6;
  CO_GRADE_APP = True;
  CO_VLOCAL = False;
  CO_EXTRA_LCT_003 = False; // True somente para Credito2

var
  MyList    : TMyListas;
  FRCampos  : TCampos;
  FRIndices : TIndices;
  FRJanelas : TJanelas;
  //
  _ArrClieSets: array[01..32] of String;
  _MaxClieSets: Integer;
  _ArrFornSets: array[01..32] of String;
  _MaxFornSets: Integer;

implementation

uses Module, ModuleGeral, MyDBCheck, UnPerfJan_Tabs, Mail_Tabs, UnFinance_Tabs,
UnIBGE_DTB_Tabs, NFe_Tabs, UnAnotacoes_Tabs, UnGrade_Tabs, Geral_TbTerc,
UnEnti_Tabs, UnGFat_Tabs, UnALL_Tabs, UnImprime_Tabs, UnPraz_Tabs,
UnCheques_Tabs, UnMoedas_Tabs, UnFiscal_Tabs, UnGPed_Tabs, UnEmpresas_Tabs,
UnCNAB_Tabs, UnUMedi_Tabs, UnEntities, UnNotificacoes_Tabs, UnOrdProd_Tabs,
SPED_EFD_Tabs, UnTX_Tabs, UnGraTx_Tabs, UnOSP_Tabs;

function TMyListas.CriaListaTabelas(Database: TmySQLDatabase; Lista:
  TList<TTabelas>): Boolean;
begin
  Result := True;
  try
    if Database = Dmod.MyDB then
    begin
      PerfJan_Tabs.CarregaListaTabelas(Lista);
      //CashTb.CarregaListaTabelasCashier(FTabelas);
      //CashTb.ComplementaListaComLcts(Lista);
      Finance_Tabs.CarregaListaTabelas(FTabelas);
      Finance_Tabs.ComplementaListaComLcts(Lista);
      Enti_Tabs.CarregaListaTabelas(Lista);
      CNAB_Tabs.CarregaListaTabelas(Database, Lista);
      Empresas_Tabs.CarregaListaTabelas(Lista);
      GPed_Tabs.CarregaListaTabelas(Lista);
      Cheques_Tabs.CarregaListaTabelas(Lista);
      Fiscal_Tabs.CarregaListaTabelas(Lista);
      Moedas_Tabs.CarregaListaTabelas(Lista);
      Imprime_Tabs.CarregaListaTabelas(Lista);
      ALL_Tabs.CarregaListaTabelas(Lista);
      Anotacoes_Tabs.CarregaListaTabelas(Lista);
      GFat_Tabs.CarregaListaTabelas(Lista);
      Grade_Tabs.CarregaListaTabelas(FTabelas);
      NFe_Tb.CarregaListaTabelas(Database, FTabelas);
      Mail_Tb.CarregaListaTabelas(Lista);
      Praz_Tabs.CarregaListaTabelas(Database, Lista);
      UMedi_Tabs.CarregaListaTabelas(Database, Lista);
      Notificacoes_Tabs.CarregaListaTabelas(FTabelas);
      OrdProd_Tabs.CarregaListaTabelas(FTabelas);
      SPEDEFD_Tb.CarregaListaTabelas(Database, FTabelas);
      TX_Tabs.CarregaListaTabelas(FTabelas);
      GraTX_Tabs.CarregaListaTabelas(FTabelas);
      OSP_Tabs.CarregaListaTabelas(FTabelas);
      //
      MyLinguas.AdTbLst(Lista, False, Lowercase('Controle'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SugVPedCab'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SugVPedCfg'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SugVPedIts'), '');
      //
      MyLinguas.AdTbLst(Lista, False, Lowercase('FluxoCab'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('FluxoIts'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('FluxoSet'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('SetorCad'), '');
      //
    end
    else if Database = DModG.AllID_DB then
    begin
      IBGE_DTB_Tabs.CarregaListaTabelas(Lista);
      UnGeral_TbTerc.CarregaListaTabelas(Lista);
      NFe_Tb.CarregaListaTabelas(Database, FTabelas);
      SPEDEFD_Tb.CarregaListaTabelas(Database, FTabelas);
    end;
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaTabelasLocais(Lista: TList<TTabelas>): Boolean;
begin
  Result := True;
  try
    //CashTb.CarregaListaTabelasLocaisCashier(Lista);
    Finance_Tabs.CarregaListaTabelasLocais(Lista);
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
begin
  Result := True;
  try
    if Uppercase(Tabela) = Uppercase('GraGruY') then
    begin
      FListaSQL.Add('Codigo|Tabela|Nome|Ordem');
      FListaSQL.Add('0|""|"(N�o atrelado)"|0');
      FListaSQL.Add(Geral.FF0(CO_GraGruY_1024_TXCadNat) +'|"txcadnat"|"Mat�ria-prima"|1024');
      FListaSQL.Add(Geral.FF0(CO_GraGruY_2048_TXCadInd) +'|"txcadind"|"Produto em confec��o"|2048');
      FListaSQL.Add(Geral.FF0(CO_GraGruY_4096_TXCadInt) +'|"txcadint"|"Produto intermedi�rio"|4096');
      FListaSQL.Add(Geral.FF0(CO_GraGruY_6144_TXCadFcc) +'|"txcadfcc"|"Produto confeccionado"|6144');
    end else
    if Uppercase(Tabela) = Uppercase('FluxoCab') then
    begin
      FListaSQL.Add('Codigo|Nome');
      FListaSQL.Add('0|"(N�O DEFINIDO)"');
    end else
    if Uppercase(Tabela) = Uppercase('SetorCad') then
    begin
      FListaSQL.Add('Codigo|Nome');
      FListaSQL.Add('0|"(N�O DEFINIDO)"');
    end else
    begin
      PerfJan_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      //CashTb.CarregaListaSQLCashier(Tabela, FListaSQL);
      Finance_Tabs.CarregaListaSQL(Tabela, FListaSQL, CO_DMKID_APP);
      Enti_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      CNAB_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      Empresas_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      GPed_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      Cheques_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      Fiscal_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      Moedas_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      Imprime_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      ALL_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      Anotacoes_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      GFat_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      IBGE_DTB_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      NFe_Tb.CarregaListaSQL(Tabela, FListaSQL);
      Grade_Tabs.CarregaListaSQL(Tabela, FListaSQL, CO_DMKID_APP);
      Mail_Tb.CarregaListaSQL(Tabela, FListaSQL);
      UMedi_Tabs.CarregaListaSQL(Tabela, FListaSQL, CO_DMKID_APP);
      Praz_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      Notificacoes_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      OrdProd_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      //
      SPEDEFD_Tb.CarregaListaSQL(Tabela, FListaSQL);
      UnGeral_TbTerc.CarregaListaSQL(Tabela, FListaSQL);
      TX_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      GraTX_Tabs.CarregaListaSQL(Tabela, FListaSQL, CO_DMKID_APP);
      OSP_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    end;
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CompletaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
begin
  Result := True;
  try
    if Uppercase(Tabela) = Uppercase('Contas') then
    begin
      (*FListaSQL.Add('-129|"Compra de mercadorias diversas"');
      FListaSQL.Add('-130|"Frete de mercadorias"');
      FListaSQL.Add('-131|"Compra de filmes"');
      FListaSQL.Add('-132|"Frete de filmes"');
      FListaSQL.Add('-133|"Inv�lido"');
      FListaSQL.Add('-134|"Venda e/ou loca��o"');*)
    end;
    Finance_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Enti_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    CNAB_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Empresas_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    GPed_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Cheques_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Fiscal_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Moedas_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Imprime_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    ALL_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Anotacoes_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    GFat_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    PerfJan_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Praz_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Notificacoes_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    OrdProd_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    //
    UnGeral_TbTerc.ComplementaListaSQL(Tabela, FListaSQL);
    Mail_Tb.ComplementaListaSQL(Tabela, FListaSQL);
    UMedi_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaIndices(TabelaBase, TabelaNome: String;
  FLIndices: TList<TIndices>): Boolean;
begin
  Result := True;
  try
    if Uppercase(TabelaBase) = Uppercase('FluxoCab') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('FluxoIts') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('FluxoSet') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('SetorCad') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('SugVPedCab') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'UNIQUE1';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'CodUsu';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('SugVPedCfg') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('SugVPedIts') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
      //
    end else
    begin
      PerfJan_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      //CashTb.CarregaListaFRIndicesCashier(TabelaBase, TabelaNome, FRIndices, FLindices);
      Finance_Tabs.CarregaListaFRIndices(TabelaBase, TabelaNome, FRIndices, FLindices);
      Enti_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      CNAB_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      Empresas_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      GPed_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      Cheques_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      Fiscal_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      Moedas_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      Imprime_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      ALL_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      Anotacoes_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      GFat_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      IBGE_DTB_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      NFe_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      Grade_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      Mail_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      UMedi_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Praz_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Notificacoes_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      OrdProd_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      //
      UnGeral_TbTerc.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      SPEDEFD_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      TX_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      GraTX_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      OSP_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
    end;
  except
    raise;
    Result := False;
  end;
end;

procedure TMyListas.ConfiguracoesIniciais(UsaCoresRel: integer);
begin
  dmkPF.ConfigIniApp(UsaCoresRel);
  //
  if not Entities.ConfiguracoesIniciaisEntidades(CO_DMKID_APP) then
    Geral.MB_Aviso('Database para configura��es de "CheckBox" n�o definidos!');
end;

function TMyListas.CriaListaCampos(Tabela: String; FLCampos: TList<TCampos>;
  var TemControle: TTemControle): Boolean;
begin
  try
    if Uppercase(Tabela) = Uppercase('Controle') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'SugVPedCab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SugVPedCfg';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SugVPedIts';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CanAltBalTX';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('FluxoCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('FluxoIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Setor';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Operacao';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Acao1';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Acao2';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Acao3';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Acao4';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('FluxoSet') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Setor';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('SetorCad') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sigla';
      FRCampos.Tipo       := 'char(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('SugVPedCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('SugVPedCfg') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Multiplo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FiltroTip';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FiltroCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('SugVPedIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
    end else begin
      PerfJan_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      //CashTb.CarregaListaFRCamposCashier(Tabela, FRCampos, FLCampos, TemControle);
      Finance_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Enti_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      CNAB_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Cheques_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Empresas_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      GPed_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Fiscal_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Moedas_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Imprime_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      ALL_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Anotacoes_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      GFat_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      IBGE_DTB_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      NFe_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Grade_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Mail_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      UMedi_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Praz_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Notificacoes_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      OrdProd_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      //
      UnGeral_TbTerc.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      SPEDEFD_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, temControle);
      TX_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, temControle);
      GraTX_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, temControle);
      OSP_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, temControle);
    end;
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
    PerfJan_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    //CashTb.CompletaListaFRCamposCashier(Tabela, FRCampos, FLCampos);
    Finance_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Enti_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    CNAB_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Empresas_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    GPed_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Cheques_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Fiscal_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Moedas_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Imprime_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    ALL_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Anotacoes_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    GFat_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Grade_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    NFe_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Mail_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    UMedi_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Praz_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Notificacoes_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    OrdProd_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    //
    UnGeral_TbTerc.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    TX_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    GraTX_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    OSP_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    //
    Result := True;
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.ExcluiTab: Boolean;
begin
  Result := True;
end;

function TMyListas.ExcluiReg: Boolean;
begin
  Result := True;
end;

function TMyListas.ExcluiIdx: Boolean;
begin
  Result := True;
end;

(*
function TMyListas.CriaListaUserSets(FUserSets: TStringList): Boolean;
begin
  Result := True;
  try
    FUserSets.Add('Edi��o de Itens de Mercadoria;C�digos Fiscais');
    FUserSets.Add('Edi��o de Itens de Mercadoria;Comiss�o de Representante');
    FUserSets.Add('Edi��o de Itens de Mercadoria;Comiss�o de Vendedor');
    FUserSets.Add('Edi��o de Itens de Mercadoria;IPI');
    FUserSets.Add('Edi��o de Itens de Mercadoria;ICMS');
    //
  except
    raise;
    Result := False;
  end;
end;
*)

(*
function TMyListas.CriaListaImpDOS(FImpDOS: TStringList): Boolean;
begin
  Result := True;
  try
    FImpDOS.Add('1001;01;NF (Topo)');
    FImpDOS.Add('1002;01;Sa�da [X]');
    FImpDOS.Add('1003;01;Entrada [X]');
    FImpDOS.Add('1004;01;Data emiss�o');
    FImpDOS.Add('1005;01;Data entra/sai');
    FImpDOS.Add('1006;01;C�digo CFOP');
    FImpDOS.Add('1007;01;Descri��o CFOP');
    FImpDOS.Add('1008;01;Base c�lculo ICMS');
    FImpDOS.Add('1009;01;Valor ICMS');
    FImpDOS.Add('1010;01;Base c�lc. ICMS subst.');
    FImpDOS.Add('1011;01;Valor ICMS subst.');
    FImpDOS.Add('1012;01;Valor frete');
    FImpDOS.Add('1013;01;Valor seguro');
    FImpDOS.Add('1014;01;Outras desp. aces.');
    FImpDOS.Add('1015;01;Valor total IPI');
    FImpDOS.Add('1016;01;Valor total produtos');
    FImpDOS.Add('1017;01;Valor total servicos');
    FImpDOS.Add('1018;01;Valor total nota');
    FImpDOS.Add('1019;01;Placa ve�culo');
    FImpDOS.Add('1020;01;UF placa ve�culo');
    FImpDOS.Add('1021;01;Vol. Quantidade');
    FImpDOS.Add('1022;01;Vol. Esp�cie');
    FImpDOS.Add('1023;01;Vol. Marca');
    FImpDOS.Add('1024;01;Vol. N�mero');
    FImpDOS.Add('1025;01;Vol. kg bruto');
    FImpDOS.Add('1026;01;Vol. kg l�quido');
    FImpDOS.Add('1027;01;Dados adicionais');
    FImpDOS.Add('1028;01;Frete por conta de ...');
    FImpDOS.Add('1029;01;Desconto especial');
    FImpDOS.Add('1030;01;NF (rodap�)');
    //
    FImpDOS.Add('2001;02;Nome ou Raz�o Social');
    FImpDOS.Add('2002;02;CNPJ ou CPF');
    FImpDOS.Add('2003;02;Endere�o');
    FImpDOS.Add('2004;02;Bairro');
    FImpDOS.Add('2005;02;CEP');
    FImpDOS.Add('2006;02;Cidade');
    FImpDOS.Add('2007;02;Telefone');
    FImpDOS.Add('2008;02;UF');
    FImpDOS.Add('2009;02;I.E. ou RG');
    FImpDOS.Add('2010;02;I.E.S.T.');
    //
    FImpDOS.Add('6001;06;Nome ou Raz�o Social');
    FImpDOS.Add('6002;06;CNPJ ou CPF');
    FImpDOS.Add('6003;06;Endere�o');
    FImpDOS.Add('6004;06;Bairro');
    FImpDOS.Add('6005;06;CEP');
    FImpDOS.Add('6006;06;Cidade');
    FImpDOS.Add('6007;06;Telefone');
    FImpDOS.Add('6008;06;UF');
    FImpDOS.Add('6009;06;I.E. ou RG');
    FImpDOS.Add('6010;06;I.E.S.T.');
    //
    FImpDOS.Add('3001;03;Descri��o');
    FImpDOS.Add('3002;03;Classifica��o Fiscal');
    FImpDOS.Add('3003;03;Situa��o Tribut�ria');
    FImpDOS.Add('3004;03;Unidade');
    FImpDOS.Add('3005;03;Quantidade');
    FImpDOS.Add('3006;03;Valor unit�rio');
    FImpDOS.Add('3007;03;Valor total');
    FImpDOS.Add('3008;03;Aliquota ICMS');
    FImpDOS.Add('3009;03;Aliquota IPI');
    FImpDOS.Add('3010;03;Valor IPI');
    FImpDOS.Add('3011;03;CFOP');
    FImpDOS.Add('3012;03;Refer�ncia');
    //
    FImpDOS.Add('4001;04;Descri��o');
    FImpDOS.Add('4002;04;Classifica��o Fiscal');
    FImpDOS.Add('4003;04;Situa��o Tribut�ria');
    FImpDOS.Add('4004;04;Unidade');
    FImpDOS.Add('4005;04;Quantidade');
    FImpDOS.Add('4006;04;Valor unit�rio');
    FImpDOS.Add('4007;04;Valor total');
    FImpDOS.Add('4008;04;Aliquota ICMS');
    FImpDOS.Add('4009;04;Aliquota IPI');
    FImpDOS.Add('4010;04;Valor IPI');
    FImpDOS.Add('4011;04;CFOP');
    FImpDOS.Add('4012;04;Refer�ncia');
    //
    FImpDOS.Add('5001;05;Parcela');
    FImpDOS.Add('5002;05;Valor');
    FImpDOS.Add('5003;05;Vencimento');
    //
  except
    raise;
    Result := False;
  end;
end;
*)

procedure TMyListas.VerificaOutrosAntes(DataBase: TmySQLDatabase; Memo: TMemo);
begin
  //
end;

procedure TMyListas.VerificaOutrosDepois(DataBase: TmySQLDatabase; Memo: TMemo);
begin
  // Nada
end;

procedure TMyListas.ModificaDadosDeIndicesAlterados(Indice, Tabela: String;
  DataBase: TmySQLDatabase; Memo: TMemo);
begin
  // Nada
end;

// XXX-XXXXX-000
function TMyListas.CriaListaJanelas(FLJanelas: TList<TJanelas>): Boolean;
begin
  PerfJan_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Finance_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  IBGE_DTB_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Enti_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  CNAB_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Cheques_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Empresas_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  GPed_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Fiscal_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Moedas_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Imprime_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  ALL_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Anotacoes_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  GFat_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  NFe_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Grade_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Mail_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  UMedi_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Praz_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Notificacoes_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  OrdProd_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  SPEDEFD_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  TX_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  GraTX_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  OSP_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  //
  UnGeral_TbTerc.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  //
  Result := True;
  // FER-OPCAO-002 :: Op��es Espec�ficas do Aplicativo
  New(FRJanelas);
  FRJanelas.ID        := 'FER-OPCAO-002';
  FRJanelas.Nome      := 'FmOpcoesPlanning';
  FRJanelas.Descricao := 'Op��es Espec�ficas do Aplicativo';
  FLJanelas.Add(FRJanelas);
  //
  // FLU-PRODU-001 :: Cadastro de Fluxos de Produ��o
  New(FRJanelas);
  FRJanelas.ID        := 'FLU-PRODU-001';
  FRJanelas.Nome      := 'FmFluxoCab';
  FRJanelas.Descricao := 'Cadastro de Fluxos de Produ��o';
  FLJanelas.Add(FRJanelas);
  //
  // FLU-PRODU-002 :: Opera��o de Fluxo de Produ��o
  New(FRJanelas);
  FRJanelas.ID        := 'FLU-PRODU-002';
  FRJanelas.Nome      := 'FmFluxoItsUni';
  FRJanelas.Descricao := 'Opera��o de Fluxo de Produ��o';
  FLJanelas.Add(FRJanelas);
  //
  // FLU-PRODU-003 :: Setor de Passagem de Fluxo de Produ��o
  New(FRJanelas);
  FRJanelas.ID        := 'FLU-PRODU-003';
  FRJanelas.Nome      := 'FmFluxoSet';
  FRJanelas.Descricao := 'Setor de Passagem de Fluxo de Produ��o';
  FLJanelas.Add(FRJanelas);
  //
  // FLU-PRODU-004 :: Opera��o de Fluxo de Produ��o - Edi��o m�ltipla
  New(FRJanelas);
  FRJanelas.ID        := 'FLU-PRODU-004';
  FRJanelas.Nome      := 'FmFluxoItsMul';
  FRJanelas.Descricao := 'Opera��o de Fluxo de Produ��o - Edi��o m�ltipla';
  FLJanelas.Add(FRJanelas);
  //
  // IND-GERAL-001 :: Cadastro de Setores
  New(FRJanelas);
  FRJanelas.ID        := 'IND-GERAL-001';
  FRJanelas.Nome      := 'FmSetorCad';
  FRJanelas.Descricao := 'Cadastro de Setores';
  FLJanelas.Add(FRJanelas);
  //
  // PLA-CORTE-000 :: Relat�rios de Corte Simples
  New(FRJanelas);
  FRJanelas.ID        := 'PLA-CORTE-000';
  FRJanelas.Nome      := 'FmPlanCutImp';
  FRJanelas.Descricao := 'Relat�rios de Corte Simples';
  FLJanelas.Add(FRJanelas);
  //
  // SUG-PEDVD-001 :: Sugest�es de Venda por Pedidos
  New(FRJanelas);
  FRJanelas.ID        := 'SUG-PEDVD-001';
  FRJanelas.Nome      := 'FmSugVPedCab';
  FRJanelas.Descricao := 'Sugest�es de Venda por Pedidos';
  FLJanelas.Add(FRJanelas);
  //
  // SUG-PEDVD-002 :: C�lculo de Sugest�o de Venda por Pedidos
  New(FRJanelas);
  FRJanelas.ID        := 'SUG-PEDVD-002';
  FRJanelas.Nome      := 'FmSugVPedIts';
  FRJanelas.Descricao := 'C�lculo de Sugest�o de Venda por Pedidos';
  FLJanelas.Add(FRJanelas);
  //
end;

function TMyListas.CriaListaQeiLnk(TabelaCria: String;
  FLQeiLnk: TList<TQeiLnk>): Boolean;
begin
  Result := True;
  try
(*
    if Uppercase(TabelaCria) = Uppercase('TXEntiMP') then
    begin
      New(FRQeiLnk);
      FRQeiLnk.AskrTab       := TabelaCria;
      FRQeiLnk.AskrCol       := 'Codigo';
      FRQeiLnk.RplyTab       := '';
      FRQeiLnk.RplyCol       := '';
      FRQeiLnk.Purpose       := TItemTuplePurpose.itpUSGSrvrInc;
      FRQeiLnk.Seq_in_pref   := 1;
      FLQeiLnk.Add(FRQeiLnk);
      //
      New(FRQeiLnk);
      FRQeiLnk.AskrTab       := TabelaCria;
      FRQeiLnk.AskrCol       := 'MPVImpOpc';
      FRQeiLnk.RplyTab       := 'MPVImpOpc';
      FRQeiLnk.RplyCol       := 'Codigo';
      FRQeiLnk.Purpose       := TItemTuplePurpose.itpRelatUsrSrv;
      FRQeiLnk.Seq_in_pref   := 1;
      FLQeiLnk.Add(FRQeiLnk);
      //
      //
    //end else if Uppercase(TabelaBase) = Uppercase('?) then
    //begin
    end else
*)
    begin
      All_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      Enti_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      PerfJan_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      Finance_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      IBGE_DTB_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      NFe_Tb.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      Grade_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      Mail_Tb.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      UnGeral_TbTerc.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      //
      Imprime_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      Praz_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
  	  Moedas_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      Fiscal_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      GFat_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      GPed_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      CNAB_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      Anotacoes_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      Empresas_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      UMedi_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      //Tributos_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      Cheques_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      Notificacoes_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      OrdProd_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      //
      SPEDEFD_Tb.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      TX_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      GraTX_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      OSP_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
    end;
  except
    raise;
    Result := False;
  end;
end;

end.
