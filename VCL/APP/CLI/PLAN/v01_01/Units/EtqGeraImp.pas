unit EtqGeraImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DBCtrls, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, DB, mySQLDbTables, dmkRadioGroup, printers, Menus,
  dmkGeral, UnDmkProcFunc, UnDmkEnums;

type
  TFmEtqGeraImp = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Panel3: TPanel;
    EdEtqPrinCad: TdmkEditCB;
    CBEtqPrinCad: TdmkDBLookupComboBox;
    Label1: TLabel;
    QrEtqPrinCad: TmySQLQuery;
    DsEtqPrinCad: TDataSource;
    QrEtqPrinCadCodigo: TIntegerField;
    QrEtqPrinCadCodUsu: TIntegerField;
    QrEtqPrinCadNome: TWideStringField;
    QrEtqPrinCadTypeUso: TSmallintField;
    QrEtqPrinCadTypePrn: TSmallintField;
    QrEtqPrinCadPortaImp: TSmallintField;
    QrEtqPrinCadPortaIdx: TSmallintField;
    QrEtqPrinCadModelo: TWideStringField;
    QrEtqPrinCadColunas: TSmallintField;
    QrEtqPrinCadIdVarIni: TWideStringField;
    QrEtqPrinCadIdVarFmt: TWideStringField;
    QrEtqPrinCadIdVarFim: TWideStringField;
    RGTypePrn: TdmkRadioGroup;
    RGPortaImp: TdmkRadioGroup;
    EdPortaIdx: TdmkEdit;
    Label14: TLabel;
    Label9: TLabel;
    EdColunas: TdmkEdit;
    BtImprime: TBitBtn;
    Memo1: TMemo;
    QrEtqPrinCmd_0: TmySQLQuery;
    QrEtqGeraIts: TmySQLQuery;
    QrEtqGeraItsREDUZIDO: TIntegerField;
    QrEtqGeraItsSequencia: TIntegerField;
    QrEtqGeraItsGraGruC: TIntegerField;
    QrEtqGeraItsNIVEL1: TIntegerField;
    QrEtqGeraItsCOD_TAM: TIntegerField;
    QrEtqGeraItsNOMETAM: TWideStringField;
    QrEtqGeraItsCOD_GRADE: TIntegerField;
    QrEtqGeraItsNOMEGRADE: TWideStringField;
    QrEtqGeraItsCOD_COR: TIntegerField;
    QrEtqGeraItsNOMECOR: TWideStringField;
    QrEtqGeraItsNOMENIVEL1: TWideStringField;
    ListBox1: TListBox;
    CkSeleciona: TCheckBox;
    BitBtn2: TBitBtn;
    EdLinha: TdmkEdit;
    Label2: TLabel;
    MeResult: TMemo;
    QrEtqPrinCadDPI: TIntegerField;
    QrEtqPrinCadDefaultIni: TSmallintField;
    QrEtqPrinCadDefaultFim: TSmallintField;
    QrEtqPrinCadDefMinPixel: TSmallintField;
    QrEtqPrinCadDefFeedBac: TSmallintField;
    QrEtqPrinCadLabelLarg: TFloatField;
    QrEtqPrinCadLabelAltu: TFloatField;
    QrEtqPrinCadLabelMEsq: TFloatField;
    QrEtqPrinCadLabelLGap: TFloatField;
    QrEtqPrinCadFormuLarg: TFloatField;
    QrEtqPrinCadLinguagem: TSmallintField;
    QrEtqPrinCadMeasure: TSmallintField;
    QrEtqPrinCmd_2: TmySQLQuery;
    QrEtqPrinCmd_0Codigo: TIntegerField;
    QrEtqPrinCmd_0Tipo: TSmallintField;
    QrEtqPrinCmd_0Ordem: TIntegerField;
    QrEtqPrinCmd_0Controle: TIntegerField;
    QrEtqPrinCmd_0Config: TWideStringField;
    QrEtqPrinCmd_2Codigo: TIntegerField;
    QrEtqPrinCmd_2Tipo: TSmallintField;
    QrEtqPrinCmd_2Ordem: TIntegerField;
    QrEtqPrinCmd_2Controle: TIntegerField;
    QrEtqPrinCmd_2Config: TWideStringField;
    QrEtqPrinCmd_1: TmySQLQuery;
    QrEtqPrinCmd_1Codigo: TIntegerField;
    QrEtqPrinCmd_1Tipo: TSmallintField;
    QrEtqPrinCmd_1Ordem: TIntegerField;
    QrEtqPrinCmd_1Controle: TIntegerField;
    QrEtqPrinCmd_1Config: TWideStringField;
    CkDOS: TCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CBEtqPrinCadClick(Sender: TObject);
    procedure EdEtqPrinCadChange(Sender: TObject);
    procedure EdEtqPrinCadExit(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure Memo1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    { Private declarations }
    procedure AtualizaConfig();
    function ValorDeVariavel(Codigo: Integer): String;
    function ValorDeVariavel_LimitTam(Texto: String): String;
    function GeraSequenciaDeCodigoDeBarraProduto(): String;
    function RichRow(m: TCustomMemo): Longint;
    procedure ViaDOS();
    procedure Viawindows();
  public
    { Public declarations }
  end;

  var
  FmEtqGeraImp: TFmEtqGeraImp;

implementation

uses UnMyObjects, EtqGeraLot, BarComands, Module;

{$R *.DFM}

procedure TFmEtqGeraImp.AtualizaConfig;
begin
  RGTypePrn.ItemIndex     := QrEtqPrinCadTypePrn.Value;
  RGPortaImp.ItemIndex    := QrEtqPrinCadPortaImp.Value;
  EdPortaIdx.ValueVariant := QrEtqPrinCadPortaIdx.Value;
  EdColunas.ValueVariant  := QrEtqPrinCadColunas.Value;
end;

procedure TFmEtqGeraImp.BtImprimeClick(Sender: TObject);
begin
  if CkDOS.Checked then
    ViaDOS()
  else Viawindows;
  //
  if Geral.MensagemBox('As etiquetas foram impressas com sucesso?',
  'Pergunta', MB_YESNO+ MB_ICONQUESTION) = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE etqgeralot SET Impresso=1');
    Dmod.QrUpd.SQL.Add('WHERE Codigo=:P0');
    FmEtqGeraLot.QrEtqGeraLot.First;
    while not FmEtqGeraLot.QrEtqGeraLot.Eof do
    begin
      if (FmEtqGeraLot.QrEtqGeraLotImprimir.Value = 1) and
      (FmEtqGeraLot.QrEtqGeraLotImpresso.Value = 0) then
      begin
        Dmod.QrUpd.Params[0].AsInteger := FmEtqGeraLot.QrEtqGeraLotCodigo.Value;
        Dmod.QrUpd.ExecSQL;
      end;
      FmEtqGeraLot.QrEtqGeraLot.Next;
    end;
    FmEtqGeraLot.ReopenLastTenLot;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmEtqGeraImp.BitBtn2Click(Sender: TObject);
var
 i, k, l: Integer;
begin
  ListBox1.Items.Clear;
  l := EdLinha.ValueVariant;
  k := Length(Memo1.Lines[l]);
  for i := 1 to k do
    ListBox1.Items.Add(Memo1.Lines[l][i] +
    ' = ' + FormatFloat('000', Ord(Memo1.Lines[l][i])));
end;

procedure TFmEtqGeraImp.BtOKClick(Sender: TObject);
var
  EtqPrinCad, p, MaxCol, AtuCol: Integer;
  LinR, LinN, x, vi, vf, _i, _f: String;
  pm: TPrnMeasure;
  ppl: TPrnProgLang;
begin
  Memo1.Lines.Clear;
  vi := '@';//QrEtqPrinCadIdVarIni.Value;
  vf := '@';//QrEtqPrinCadIdVarFim.Value;
  _i := '{';
  _f := '}';
  AtuCol := 0;
  MaxCol := QrEtqPrinCadColunas.Value;
  case QrEtqPrinCadLinguagem.Value of
    //0: ppl := pplNone;
    1: ppl := pplArgoxA;
    2: ppl := pplArgoxB;
    else ppl := pplNone;
  end;
  case QrEtqPrinCadMeasure.Value of
    0: pm := pmMetric;
    1: pm := pmInches;
    else pm := pmNone;
  end;
  //
  EtqPrinCad := Geral.IMV(EdEtqPrinCad.Text);
  if MyObjects.FIC(EtqPrinCad = 0, EdEtqPrinCad,
    'Informe a configura��o de impress�o!') then Exit;
  //
  if QrEtqPrinCadDefaultIni.Value = 0 then
  begin
    QrEtqPrinCmd_0.Close;
    QrEtqPrinCmd_0.Params[00].AsInteger := EtqPrinCad;
    QrEtqPrinCmd_0.Open;
  end;
  //
  if QrEtqPrinCadDefaultFim.Value = 0 then
  begin
    QrEtqPrinCmd_2.Close;
    QrEtqPrinCmd_2.Params[00].AsInteger := EtqPrinCad;
    QrEtqPrinCmd_2.Open;
  end;
  //
  with FmEtqGeraLot do
  begin
    QrEtqGeraLot.First;
    while not QrEtqGeraLot.Eof do
    begin
      if QrEtqGeraLotImprimir.Value = 1 then
      begin
        QrEtqGeraIts.Close;
        QrEtqGeraIts.Params[0].AsInteger := QrEtqGeraLotCodigo.Value;
        QrEtqGeraIts.Open;
        while not QrEtqGeraIts.Eof do
        begin
          if (AtuCol = 0) or  (AtuCol = MaxCol) then
            AtuCol := 1
          else
            AtuCol := AtuCol + 1;
          //


          // Inicio de fileira
          if AtuCol = 1 then
          begin
            if QrEtqPrinCadDefaultIni.Value = 0 then
            begin
              QrEtqPrinCmd_0.First;
              while not QrEtqPrinCmd_0.Eof do
              begin
                Memo1.Lines.Add(QrEtqPrinCmd_0Config.Value);
                //
                QrEtqPrinCmd_0.Next;
              end;
            end else UBarComands.ComandosAutomaticos(bctIni, ppl, pm, Memo1);
          end;

          // Dados
          QrEtqPrinCmd_1.Close;
          QrEtqPrinCmd_1.Params[00].AsInteger := EtqPrinCad;
          QrEtqPrinCmd_1.Params[01].AsInteger := AtuCol;
          QrEtqPrinCmd_1.Open;
          QrEtqPrinCmd_1.First;
          while not QrEtqPrinCmd_1.Eof do
          begin
            LinR := QrEtqPrinCmd_1Config.Value;
            LinN := '';
            // in�cio da primeira vari�vel
            p := pos(vi, LinR);
            while p > 0 do
            begin
              LinN := LinN + Copy(LinR, 1, p - 1);
              LinR := Copy(LinR, p + 1);
              // Fim da vari�vel atual
              p := pos(vf, LinR);
              if p > 0 then
              begin
                x := Copy(LinR, 1, p);
                LinR := Copy(LinR, p + 1);
                //n := Geral.IMV(x);
                //x := ValorDeVariavel(n);
                x := ValorDeVariavel_LimitTam(x);
                LinN := LinN + x;
                // in�cio da pr�xima vari�vel
                p := pos(vi, LinR);
              end;
              if p = 0 then
              begin
                LinN := LinN + LinR;
                LinR := '';
              end;
            end;
            LinN := LinN + LinR;


            // in�cio da primeira ascii
            LinR := LinN;
            LinN := '';
            p := pos(_i, LinR);
            while p > 0 do
            begin
              LinN := LinN + Copy(LinR, 1, p - 1);
              LinR := Copy(LinR, p + 1);
              // Fim da vari�vel atual
              p := pos(_f, LinR);
              if p > 0 then
              begin
                x := Copy(LinR, 1, p - 1);
                LinR := Copy(LinR, p + 1);
                //n := Geral.IMV(x);
                //x := ValorDeVariavel(n);
                x := Char(StrToInt(x));
                LinN := LinN + x;
                // in�cio da pr�xima vari�vel
                p := pos(_i, LinR);
              end;
              if p = 0 then
              begin
                LinN := LinN + LinR;
                LinR := '';
              end;
            end;
            LinN := LinN + LinR;

            //

            Memo1.Lines.Add(LinN);
            //
            QrEtqPrinCmd_1.Next;
          end;
          // Fim dados etiqueta atual


          // Fim de fileira
          if AtuCol = MaxCol then
          begin
            if QrEtqPrinCadDefaultFim.Value = 0 then
            begin
              QrEtqPrinCmd_2.First;
              while not QrEtqPrinCmd_2.Eof do
              begin
                Memo1.Lines.Add(QrEtqPrinCmd_2Config.Value);
                //
                QrEtqPrinCmd_2.Next;
              end;
            end else UBarComands.ComandosAutomaticos(bctFim, ppl, pm, Memo1);
          end;

          QrEtqGeraIts.Next;
          //}
        end;
      end;
      //
      QrEtqGeraLot.Next;
    end;
    if AtuCol < MaxCol then
    begin
      if QrEtqPrinCadDefaultFim.Value = 0 then
      begin
        QrEtqPrinCmd_2.First;
        while not QrEtqPrinCmd_2.Eof do
        begin
          Memo1.Lines.Add(QrEtqPrinCmd_2Config.Value);
          //
          QrEtqPrinCmd_2.Next;
        end;
      end else UBarComands.ComandosAutomaticos(bctFim, ppl, pm, Memo1);
    end;
  end;
  ListBox1.Items.Clear;
  ListBox1.Items.Assign(Printer.Printers);
end;

procedure TFmEtqGeraImp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEtqGeraImp.CBEtqPrinCadClick(Sender: TObject);
begin
  AtualizaConfig();
end;

procedure TFmEtqGeraImp.EdEtqPrinCadChange(Sender: TObject);
begin
  if not EdEtqPrinCad.Focused then
    AtualizaConfig();
end;

procedure TFmEtqGeraImp.EdEtqPrinCadExit(Sender: TObject);
begin
  AtualizaConfig();
end;

procedure TFmEtqGeraImp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEtqGeraImp.FormCreate(Sender: TObject);
begin
  QrEtqPrinCad.Open;
  if QrEtqPrinCad.RecordCount = 1 then
  begin
    EdEtqPrinCad.ValueVariant := QrEtqPrinCadCodUsu.Value;
    CBEtqPrinCad.KeyValue     := QrEtqPrinCadCodUsu.Value;
  end;
end;

procedure TFmEtqGeraImp.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

function TFmEtqGeraImp.GeraSequenciaDeCodigoDeBarraProduto: String;
var
  Txt: String;
begin
  Txt := '000000' +
    MLAGeral.FTX(QrEtqGeraItsREDUZIDO.Value, 6, 0, siPositivo) +
    MLAGeral.FTX(QrEtqGeraItsSequencia.Value, 8, 0, siPositivo);
  Result := Txt;  
end;

procedure TFmEtqGeraImp.Memo1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  EdLinha.ValueVariant := RichRow(Memo1);
end;

function TFmEtqGeraImp.ValorDeVariavel(Codigo: Integer): String;
var
  x: String;
begin
  case Codigo of
    //001,'PRDTIPCOD', 'C�digo do tipo de produto');
    001: x := '1';
    //002,'PRDTIPNOM', 'Descri��o do tipo de produto');
    002: x := 'PRODUTO';
    //003,'PRDNI3COD', 'C�digo do 3� n�vel de produtos');
    003: x := '';
    //004,'PRDNI3NOM', 'Descri��o do 3� n�vel de produtos');
    004: x := '';
    //005,'PRDNI2COD', 'C�digo do 2� n�vel de produtos');
    005: x := '';
    //006,'PRDNI2NOM', 'Descri��o do 2� n�vel de produtos');
    006: x := '';
    //007,'PRDNI1COD', 'C�digo do 1� n�vel de produtos');
    007: x := dmkPF.FFP(QrEtqGeraItsNIVEL1.Value, 0);
    //008,'PRDNI1NOM', 'Descri��o do 1� n�vel de produtos');
    008: x := QrEtqGeraItsNOMENIVEL1.Value;
    //009,'PRDGRACOD', 'C�digo da grade de tamanhos');
    009: x := dmkPF.FFP(QrEtqGeraItsCOD_GRADE.Value, 0);
    //010,'PRDGRANOM', 'Descri��o da grade de tamanhos');
    010: x := QrEtqGeraItsNOMEGRADE.Value;
    //011,'PRDTAMCOD', 'C�digo do tamanho');
    011: x := dmkPF.FFP(QrEtqGeraItsCOD_TAM.Value, 0);
    //012,'PRDTAMNOM', 'Descri��o do tamanho');
    012: x := QrEtqGeraItsNOMETAM.Value;
    //013,'PRDCORCOD', 'C�digo da cor');
    013: x := dmkPF.FFP(QrEtqGeraItsCOD_COR.Value, 0);
    //014,'PRDCORNOM', 'Descri��o da cor');
    014: x := QrEtqGeraItsNOMECOR.Value;
    //015,'PRDREDCOD', 'C�digo reduzido');
    015: x := MLAGeral.FTX(QrEtqGeraItsREDUZIDO.Value, 6, 0, siPositivo);
    //016,'PRDCODBAR', 'C�digo de barras do produto');
    016: x := GeraSequenciaDeCodigoDeBarraProduto();
    //017,'PRDSEQUE1', 'Sequencia de impress�o do reduzido');
    017: x := MLAGeral.FTX(QrEtqGeraItsSequencia.Value, 8, 0, siPositivo);
    else begin
      Geral.MensagemBox('O c�digo ' + IntToStr(Codigo) +
      ' n�o est� implementado na gera��o de etiqueta de produtos!', 'Aviso',
      MB_OK+MB_ICONWARNING);
      x := '';
    end;
  end;
  Result := x;
end;

function TFmEtqGeraImp.ValorDeVariavel_LimitTam(Texto: String): String;
var
  p, Cod, Tam: Integer;
  Txt: String;
begin
  Tam := 0;
  p := pos('#', Texto);
  if p > 0 then
  begin
    Txt := Copy(Texto, 1, p-1);
    Cod := Geral.IMV(Txt);
    Txt := Copy(Texto, p+1);
    p := pos('#', Texto);
    if p > 0 then
      Txt := Copy(Txt, 1, p);
    Tam := Geral.IMV(Txt);
  end else Cod := Geral.IMV(Texto);
  Txt := ValorDeVariavel(Cod);
  if Tam > 0 then
    Result := Copy(Txt, 1, Tam)
  else
    Result := Txt;
end;

procedure TFmEtqGeraImp.ViaDOS();
const
  Arq = 'C:\Dermatek\Etiketa.txt';
var
  Porta: String;
begin
  //via dos
  MLAGeral.ExportaMemoToFile(Memo1, Arq, True, True, False);
  Porta := RGPortaImp.Items[RGPortaImp.ItemIndex] +
    dmkPF.FFP(EdPortaIdx.ValueVariant, 0);
  MLAGeral.ExecutaCmd('type ' + Arq + ' > ' + Porta, MeResult, True);
end;

procedure TFmEtqGeraImp.Viawindows();
var
  F: TextFile;
  Porta: String;
begin
  // Via windows
  Porta := RGPortaImp.Items[RGPortaImp.ItemIndex] +
    dmkPF.FFP(EdPortaIdx.ValueVariant, 0);
  AssignFile(F, Porta);
  ReWrite(F);
  WriteLn(F, Memo1.Text);
  CloseFile(F);
end;

function TFmEtqGeraImp.RichRow(m: TCustomMemo): Longint;
begin
  Result := SendMessage(m.Handle, EM_LINEFROMchar, m.SelStart, 0);
end;

end.

