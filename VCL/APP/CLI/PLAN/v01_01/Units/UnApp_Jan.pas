unit UnApp_Jan;

interface

uses
  Windows, SysUtils, Classes, DB, Menus, UnDmkEnums, mySQLDbTables, Forms,
  Grids, DBGrids, UnInternalConsts;

type
  TUnApp_Jan = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    procedure CadastroOpcoesPlanning();
    procedure MostraFormDeGridDeEfdIcmsIpiE001(Sender: TObject);
    procedure MostroFormMovimCod(MovimCod: Integer);
 end;

var
  App_Jan: TUnApp_Jan;


implementation

uses
  MyDBCheck, Module(*, OpcoesPlanning, UnVS_PF*);

{ TUnApp_Jan }

procedure TUnApp_Jan.CadastroOpcoesPlanning();
begin
(*
  if DBCheck.CriaFm(TFmOpcoesPlanning, FmOpcoesPlanning, afmoSoBoss) then
  begin
    FmOpcoesPlanning.ShowModal;
    FmOpcoesPlanning.Destroy;
  end;
*)
end;

procedure TUnApp_Jan.MostraFormDeGridDeEfdIcmsIpiE001(Sender: TObject);
(*
  Compatibilidade?
var
  IMEC, IMEI: Integer;
  DBGrid: TDBGrid;
*)
begin
(*
  Compatibilidade?
  IMEI := 0;
  IMEC := 0;
  DBGrid := TDBGrid(Sender);
  //
  if (DBGrid.SelectedField.FieldName = 'KndNSU') then
    IMEC := DBGrid.SelectedField.AsInteger;
  if (DBGrid.SelectedField.FieldName = 'KndItm')
  or (DBGrid.SelectedField.FieldName = 'KndItmOri')
  or (DBGrid.SelectedField.FieldName = 'KndItmDst') then
    IMEI := DBGrid.SelectedField.AsInteger;
  //
  if IMEI <> 0 then
    VS_PF.MostraFormVSMovIts(IMEI);
  if IMEC <> 0 then
    VS_PF.MostraFormVSMovCab(IMEC);
*)
end;

procedure TUnApp_Jan.MostroFormMovimCod(MovimCod: Integer);
begin
(*
  Compatibilidade?
  VS_PF.MostroFormVSMovimCod(MovimCod);
*)
end;

end.
