  // Desmarcar
  //Lic_Dmk.LiberaUso5;
unit Module;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, (*DBTables,*) UnInternalConsts, UnMLAGeral, UMySQLModule,
  mySQLDbTables, UnGOTOy, Winsock, MySQLBatch, frxClass, frxDBSet, dmkGeral,
  StdCtrls, Variants, UnDmkProcFunc, UnDmkEnums, UnProjGroup_Vars,
  UnGrl_Vars;

type
  TDmod = class(TDataModule)
    QrFields: TMySQLQuery;
    QrMaster: TMySQLQuery;
    QrLivreY_D: TmySQLQuery;
    QrRecCountX: TMySQLQuery;
    QrRecCountXRecord: TIntegerField;
    QrUser: TMySQLQuery;
    QrSelX: TMySQLQuery;
    QrSelXLk: TIntegerField;
    QrSB: TMySQLQuery;
    QrSBCodigo: TIntegerField;
    QrSBNome: TWideStringField;
    DsSB: TDataSource;
    QrSB2: TMySQLQuery;
    QrSB2Codigo: TFloatField;
    QrSB2Nome: TWideStringField;
    DsSB2: TDataSource;
    QrSB3: TMySQLQuery;
    QrSB3Codigo: TDateField;
    QrSB3Nome: TWideStringField;
    DsSB3: TDataSource;
    QrDuplicIntX: TMySQLQuery;
    QrDuplicIntXINTEIRO1: TIntegerField;
    QrDuplicIntXINTEIRO2: TIntegerField;
    QrDuplicIntXCODIGO: TIntegerField;
    QrDuplicStrX: TMySQLQuery;
    QrDuplicStrXNOME: TWideStringField;
    QrDuplicStrXCODIGO: TIntegerField;
    QrDuplicStrXANTERIOR: TIntegerField;
    QrInsLogX: TMySQLQuery;
    QrDelLogX: TMySQLQuery;
    QrDataBalY: TmySQLQuery;
    QrDataBalYData: TDateField;
    QrSenha: TMySQLQuery;
    QrSenhaLogin: TWideStringField;
    QrSenhaNumero: TIntegerField;
    QrSenhaSenha: TWideStringField;
    QrSenhaPerfil: TIntegerField;
    QrSenhaLk: TIntegerField;
    QrUserLogin: TWideStringField;
    QrMaster2: TMySQLQuery;
    QrSomaM: TMySQLQuery;
    QrSomaMValor: TFloatField;
    QrMasterCNPJ_TXT: TWideStringField;
    QrMasterTE1_TXT: TWideStringField;
    QrMasterCEP_TXT: TWideStringField;
    QrProduto: TMySQLQuery;
    QrVendas: TMySQLQuery;
    QrEntrada: TMySQLQuery;
    QrEntradaPecas: TFloatField;
    QrEntradaValor: TFloatField;
    QrVendasPecas: TFloatField;
    QrVendasValor: TFloatField;
    QrUpdM: TmySQLQuery;
    QrUpdU: TmySQLQuery;
    QrUpdL: TmySQLQuery;
    QrUpdY: TmySQLQuery;
    QrLivreY: TmySQLQuery;
    QrLivreYCodigo: TIntegerField;
    QrMaster2Em: TWideStringField;
    QrMaster2CNPJ: TWideStringField;
    QrMaster2Monitorar: TSmallintField;
    QrMaster2Distorcao: TIntegerField;
    QrMaster2DataI: TDateField;
    QrMaster2DataF: TDateField;
    QrMaster2Hoje: TDateField;
    QrMaster2Hora: TTimeField;
    QrMaster2MasLogin: TWideStringField;
    QrMaster2MasSenha: TWideStringField;
    QrMaster2MasAtivar: TWideStringField;
    QrMasterEm: TWideStringField;
    QrMasterTipo: TSmallintField;
    QrMasterLogo: TBlobField;
    QrMasterDono: TIntegerField;
    QrMasterVersao: TIntegerField;
    QrMasterCNPJ: TWideStringField;
    QrMasterIE: TWideStringField;
    QrMasterECidade: TWideStringField;
    QrMasterNOMEUF: TWideStringField;
    QrMasterEFax: TWideStringField;
    QrMasterERua: TWideStringField;
    QrMasterEBairro: TWideStringField;
    QrMasterECompl: TWideStringField;
    QrMasterEContato: TWideStringField;
    QrMasterECel: TWideStringField;
    QrMasterETe1: TWideStringField;
    QrMasterETe2: TWideStringField;
    QrMasterETe3: TWideStringField;
    QrMasterEPais: TWideStringField;
    QrMasterRespons1: TWideStringField;
    QrMasterRespons2: TWideStringField;
    QrCountY: TmySQLQuery;
    QrCountYRecord: TIntegerField;
    QrLocY: TmySQLQuery;
    QrLocYRecord: TIntegerField;
    QrLocDataY: TmySQLQuery;
    QrLocDataYRecord: TDateField;
    QrLivreY_DCodigo: TLargeintField;
    QrMasterECEP: TIntegerField;
    QrIdx: TmySQLQuery;
    QrMas: TmySQLQuery;
    QrUpd: TmySQLQuery;
    QrAux: TmySQLQuery;
    QrSQL: TmySQLQuery;
    MyDB: TmySQLDatabase;
    MyLocDatabase: TmySQLDatabase;
    QrSB4: TmySQLQuery;
    DsSB4: TDataSource;
    QrSB4Codigo: TWideStringField;
    QrSB4Nome: TWideStringField;
    QrPriorNext: TmySQLQuery;
    QrAuxL: TmySQLQuery;
    QrSoma1: TmySQLQuery;
    QrSoma1TOTAL: TFloatField;
    QrSoma1Qtde: TFloatField;
    QrControle: TmySQLQuery;
    QrControleSoMaiusculas: TWideStringField;
    QrControleMoeda: TWideStringField;
    QrControleUFPadrao: TIntegerField;
    QrControleCidade: TWideStringField;
    QrMasterLogo2: TBlobField;
    QrEstoque: TmySQLQuery;
    QrEstoqueTipo: TSmallintField;
    QrEstoqueQtde: TFloatField;
    QrEstoqueValorCus: TFloatField;
    QrPeriodoBal: TmySQLQuery;
    QrPeriodoBalPeriodo: TIntegerField;
    QrMin: TmySQLQuery;
    QrMinPeriodo: TIntegerField;
    QrBalancosIts: TmySQLQuery;
    QrBalancosItsEstqQ: TFloatField;
    QrBalancosItsEstqV: TFloatField;
    QrEstqperiodo: TmySQLQuery;
    QrBalancosItsEstqQ_G: TFloatField;
    QrBalancosItsEstqV_G: TFloatField;
    QrEstqperiodoTipo: TSmallintField;
    QrEstqperiodoQtde: TFloatField;
    QrEstqperiodoValorCus: TFloatField;
    QrControleCartVen: TIntegerField;
    QrControleCartCom: TIntegerField;
    QrControleCartDeS: TIntegerField;
    QrControleCartReS: TIntegerField;
    QrControleCartDeG: TIntegerField;
    QrControleCartReG: TIntegerField;
    QrControleCartCoE: TIntegerField;
    QrControleCartCoC: TIntegerField;
    QrControleCartEmD: TIntegerField;
    QrControleCartEmA: TIntegerField;
    QrTerminal: TmySQLQuery;
    QrTerminalIP: TWideStringField;
    QrTerminalTerminal: TIntegerField;
    QrControleContraSenha: TWideStringField;
    QrProdutoCodigo: TIntegerField;
    QrProdutoControla: TWideStringField;
    QrControlePaperTop: TIntegerField;
    QrControlePaperLef: TIntegerField;
    QrControlePaperWid: TIntegerField;
    QrControlePaperHei: TIntegerField;
    QrControlePaperFcl: TIntegerField;
    QrMasterLimite: TSmallintField;
    QrNTV: TmySQLQuery;
    QrNTI: TmySQLQuery;
    ZZDB: TmySQLDatabase;
    QrPerfis: TmySQLQuery;
    QrPerfisLibera: TSmallintField;
    QrPerfisJanela: TWideStringField;
    QrControleTravaCidade: TSmallintField;
    QrMasterSolicitaSenha: TIntegerField;
    QrControleMoraDD: TFloatField;
    QrControleMulta: TFloatField;
    QrControleMensalSempre: TIntegerField;
    QrControleEntraSemValor: TIntegerField;
    QrTerceiro: TmySQLQuery;
    QrTerceiroNOMEpUF: TWideStringField;
    QrTerceiroNOMEeUF: TWideStringField;
    QrTerceiroCodigo: TIntegerField;
    QrTerceiroRazaoSocial: TWideStringField;
    QrTerceiroFantasia: TWideStringField;
    QrTerceiroRespons1: TWideStringField;
    QrTerceiroRespons2: TWideStringField;
    QrTerceiroPai: TWideStringField;
    QrTerceiroMae: TWideStringField;
    QrTerceiroCNPJ: TWideStringField;
    QrTerceiroIE: TWideStringField;
    QrTerceiroIEST: TWideStringField;
    QrTerceiroNome: TWideStringField;
    QrTerceiroApelido: TWideStringField;
    QrTerceiroCPF: TWideStringField;
    QrTerceiroRG: TWideStringField;
    QrTerceiroELograd: TSmallintField;
    QrTerceiroERua: TWideStringField;
    QrTerceiroENumero: TIntegerField;
    QrTerceiroECompl: TWideStringField;
    QrTerceiroEBairro: TWideStringField;
    QrTerceiroECidade: TWideStringField;
    QrTerceiroEUF: TSmallintField;
    QrTerceiroECEP: TIntegerField;
    QrTerceiroEPais: TWideStringField;
    QrTerceiroETe1: TWideStringField;
    QrTerceiroEte2: TWideStringField;
    QrTerceiroEte3: TWideStringField;
    QrTerceiroECel: TWideStringField;
    QrTerceiroEFax: TWideStringField;
    QrTerceiroEEmail: TWideStringField;
    QrTerceiroEContato: TWideStringField;
    QrTerceiroENatal: TDateField;
    QrTerceiroPLograd: TSmallintField;
    QrTerceiroPRua: TWideStringField;
    QrTerceiroPNumero: TIntegerField;
    QrTerceiroPCompl: TWideStringField;
    QrTerceiroPBairro: TWideStringField;
    QrTerceiroPCidade: TWideStringField;
    QrTerceiroPUF: TSmallintField;
    QrTerceiroPCEP: TIntegerField;
    QrTerceiroPPais: TWideStringField;
    QrTerceiroPTe1: TWideStringField;
    QrTerceiroPte2: TWideStringField;
    QrTerceiroPte3: TWideStringField;
    QrTerceiroPCel: TWideStringField;
    QrTerceiroPFax: TWideStringField;
    QrTerceiroPEmail: TWideStringField;
    QrTerceiroPContato: TWideStringField;
    QrTerceiroPNatal: TDateField;
    QrTerceiroSexo: TWideStringField;
    QrTerceiroResponsavel: TWideStringField;
    QrTerceiroProfissao: TWideStringField;
    QrTerceiroCargo: TWideStringField;
    QrTerceiroRecibo: TSmallintField;
    QrTerceiroDiaRecibo: TSmallintField;
    QrTerceiroAjudaEmpV: TFloatField;
    QrTerceiroAjudaEmpP: TFloatField;
    QrTerceiroCliente1: TWideStringField;
    QrTerceiroCliente2: TWideStringField;
    QrTerceiroFornece1: TWideStringField;
    QrTerceiroFornece2: TWideStringField;
    QrTerceiroFornece3: TWideStringField;
    QrTerceiroFornece4: TWideStringField;
    QrTerceiroTerceiro: TWideStringField;
    QrTerceiroCadastro: TDateField;
    QrTerceiroInformacoes: TWideStringField;
    QrTerceiroLogo: TBlobField;
    QrTerceiroVeiculo: TIntegerField;
    QrTerceiroMensal: TWideStringField;
    QrTerceiroObservacoes: TWideMemoField;
    QrTerceiroTipo: TSmallintField;
    QrTerceiroCLograd: TSmallintField;
    QrTerceiroCRua: TWideStringField;
    QrTerceiroCNumero: TIntegerField;
    QrTerceiroCCompl: TWideStringField;
    QrTerceiroCBairro: TWideStringField;
    QrTerceiroCCidade: TWideStringField;
    QrTerceiroCUF: TSmallintField;
    QrTerceiroCCEP: TIntegerField;
    QrTerceiroCPais: TWideStringField;
    QrTerceiroCTel: TWideStringField;
    QrTerceiroCCel: TWideStringField;
    QrTerceiroCFax: TWideStringField;
    QrTerceiroCContato: TWideStringField;
    QrTerceiroLLograd: TSmallintField;
    QrTerceiroLRua: TWideStringField;
    QrTerceiroLNumero: TIntegerField;
    QrTerceiroLCompl: TWideStringField;
    QrTerceiroLBairro: TWideStringField;
    QrTerceiroLCidade: TWideStringField;
    QrTerceiroLUF: TSmallintField;
    QrTerceiroLCEP: TIntegerField;
    QrTerceiroLPais: TWideStringField;
    QrTerceiroLTel: TWideStringField;
    QrTerceiroLCel: TWideStringField;
    QrTerceiroLFax: TWideStringField;
    QrTerceiroLContato: TWideStringField;
    QrTerceiroComissao: TFloatField;
    QrTerceiroSituacao: TSmallintField;
    QrTerceiroNivel: TWideStringField;
    QrTerceiroGrupo: TIntegerField;
    QrTerceiroAccount: TIntegerField;
    QrTerceiroLogo2: TBlobField;
    QrTerceiroConjugeNome: TWideStringField;
    QrTerceiroConjugeNatal: TDateField;
    QrTerceiroNome1: TWideStringField;
    QrTerceiroNatal1: TDateField;
    QrTerceiroNome2: TWideStringField;
    QrTerceiroNatal2: TDateField;
    QrTerceiroNome3: TWideStringField;
    QrTerceiroNatal3: TDateField;
    QrTerceiroNome4: TWideStringField;
    QrTerceiroNatal4: TDateField;
    QrTerceiroCreditosI: TIntegerField;
    QrTerceiroCreditosL: TIntegerField;
    QrTerceiroCreditosF2: TFloatField;
    QrTerceiroCreditosD: TDateField;
    QrTerceiroCreditosU: TDateField;
    QrTerceiroCreditosV: TDateField;
    QrTerceiroMotivo: TIntegerField;
    QrTerceiroQuantI1: TIntegerField;
    QrTerceiroQuantI2: TIntegerField;
    QrTerceiroQuantI3: TIntegerField;
    QrTerceiroQuantI4: TIntegerField;
    QrTerceiroQuantN1: TFloatField;
    QrTerceiroQuantN2: TFloatField;
    QrTerceiroAgenda: TWideStringField;
    QrTerceiroSenhaQuer: TWideStringField;
    QrTerceiroSenha1: TWideStringField;
    QrTerceiroLimiCred: TFloatField;
    QrTerceiroDesco: TFloatField;
    QrTerceiroCasasApliDesco: TSmallintField;
    QrTerceiroTempD: TFloatField;
    QrTerceiroLk: TIntegerField;
    QrTerceiroDataCad: TDateField;
    QrTerceiroDataAlt: TDateField;
    QrTerceiroUserCad: TIntegerField;
    QrTerceiroUserAlt: TIntegerField;
    QrTerceiroCPF_Pai: TWideStringField;
    QrTerceiroSSP: TWideStringField;
    QrTerceiroCidadeNatal: TWideStringField;
    QrTerceiroUFNatal: TSmallintField;
    QlLocal: TMySQLBatchExecute;
    QrControleMyPagTip: TIntegerField;
    QrControleMyPagCar: TIntegerField;
    QrControleDono: TIntegerField;
    QrTerminais: TmySQLQuery;
    QrTerminaisIP: TWideStringField;
    QrTerminaisTerminal: TIntegerField;
    QrEndereco: TmySQLQuery;
    QrEnderecoCodigo: TIntegerField;
    QrEnderecoCadastro: TDateField;
    QrEnderecoNOMEDONO: TWideStringField;
    QrEnderecoCNPJ_CPF: TWideStringField;
    QrEnderecoIE_RG: TWideStringField;
    QrEnderecoNIRE_: TWideStringField;
    QrEnderecoRUA: TWideStringField;
    QrEnderecoNUMERO: TLargeintField;
    QrEnderecoCOMPL: TWideStringField;
    QrEnderecoBAIRRO: TWideStringField;
    QrEnderecoCIDADE: TWideStringField;
    QrEnderecoNOMELOGRAD: TWideStringField;
    QrEnderecoNOMEUF: TWideStringField;
    QrEnderecoPais: TWideStringField;
    QrEnderecoLograd: TLargeintField;
    QrEnderecoTipo: TSmallintField;
    QrEnderecoCEP: TLargeintField;
    QrEnderecoTE1: TWideStringField;
    QrEnderecoFAX: TWideStringField;
    QrEnderecoENatal: TDateField;
    QrEnderecoPNatal: TDateField;
    QrEnderecoECEP_TXT: TWideStringField;
    QrEnderecoNUMERO_TXT: TWideStringField;
    QrEnderecoE_ALL: TWideStringField;
    QrEnderecoCNPJ_TXT: TWideStringField;
    QrEnderecoFAX_TXT: TWideStringField;
    QrEnderecoTE1_TXT: TWideStringField;
    QrEnderecoNATAL_TXT: TWideStringField;
    QrControleCorRecibo: TIntegerField;
    QrControleIdleMinutos: TIntegerField;
    QrEnderecoRespons1: TWideStringField;
    QrControleMyPgParc: TSmallintField;
    QrControleMyPgQtdP: TSmallintField;
    QrControleMyPgPeri: TSmallintField;
    QrControleMyPgDias: TSmallintField;
    QrControleMeuLogoPath: TWideStringField;
    QrControleLogoNF: TWideStringField;
    QrMasterENumero: TFloatField;
    QrControleVendaCartPg: TIntegerField;
    QrControleVendaParcPg: TIntegerField;
    QrControleVendaPeriPg: TIntegerField;
    QrControleVendaDiasPg: TIntegerField;
    QrControleCNABCtaJur: TIntegerField;
    QrControleCNABCtaMul: TIntegerField;
    QrControleCNABCtaTar: TIntegerField;
    QrMaster2Limite: TSmallintField;
    QrMaster2Licenca: TWideStringField;
    QrTerminaisLicenca: TWideStringField;
    QrTransf: TmySQLQuery;
    QrTransfData: TDateField;
    QrTransfTipo: TSmallintField;
    QrTransfCarteira: TIntegerField;
    QrTransfControle: TIntegerField;
    QrTransfGenero: TIntegerField;
    QrTransfDebito: TFloatField;
    QrTransfCredito: TFloatField;
    QrTransfDocumento: TFloatField;
    DsCarteiras: TDataSource;
    QrCarteiras: TmySQLQuery;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasTipo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    QrCarteirasInicial: TFloatField;
    QrCarteirasBanco: TIntegerField;
    QrCarteirasID: TWideStringField;
    QrCarteirasFatura: TWideStringField;
    QrCarteirasID_Fat: TWideStringField;
    QrCarteirasSaldo: TFloatField;
    QrCarteirasLk: TIntegerField;
    QrCarteirasEmCaixa: TFloatField;
    QrCarteirasDIFERENCA: TFloatField;
    QrCarteirasFechamento: TIntegerField;
    QrCarteirasPrazo: TSmallintField;
    QrCarteirasTIPOPRAZO: TWideStringField;
    QrCarteirasDataCad: TDateField;
    QrCarteirasDataAlt: TDateField;
    QrCarteirasUserCad: TSmallintField;
    QrCarteirasUserAlt: TSmallintField;
    QrCarteirasNOMEPAGREC: TWideStringField;
    QrCarteirasPagRec: TSmallintField;
    QrCarteirasDiaMesVence: TSmallintField;
    QrCarteirasExigeNumCheque: TSmallintField;
    QrCarteirasForneceI: TIntegerField;
    QrCarteirasNome2: TWideStringField;
    QrCarteirasNOMEFORNECEI: TWideStringField;
    QrCarteirasTipoDoc: TSmallintField;
    QrCarteirasBanco1: TIntegerField;
    QrCarteirasAgencia1: TIntegerField;
    QrCarteirasConta1: TWideStringField;
    QrCarteirasCheque1: TIntegerField;
    QrCarteirasContato1: TWideStringField;
    QrLct: TmySQLQuery;
    QrLctData: TDateField;
    QrLctTipo: TSmallintField;
    QrLctCarteira: TIntegerField;
    QrLctAutorizacao: TIntegerField;
    QrLctGenero: TIntegerField;
    QrLctDescricao: TWideStringField;
    QrLctNotaFiscal: TIntegerField;
    QrLctDebito: TFloatField;
    QrLctCredito: TFloatField;
    QrLctCompensado: TDateField;
    QrLctDocumento: TFloatField;
    QrLctSit: TIntegerField;
    QrLctVencimento: TDateField;
    QrLctLk: TIntegerField;
    QrLctFatID: TIntegerField;
    QrLctFatNum: TIntegerField;
    QrLctFatParcela: TIntegerField;
    QrLctCONTA: TIntegerField;
    QrLctNOMECONTA: TWideStringField;
    QrLctNOMEEMPRESA: TWideStringField;
    QrLctNOMESUBGRUPO: TWideStringField;
    QrLctNOMEGRUPO: TWideStringField;
    QrLctNOMECONJUNTO: TWideStringField;
    QrLctNOMESIT: TWideStringField;
    QrLctAno: TFloatField;
    QrLctMENSAL: TWideStringField;
    QrLctMENSAL2: TWideStringField;
    QrLctBanco: TIntegerField;
    QrLctLocal: TIntegerField;
    QrLctFatura: TWideStringField;
    QrLctSub: TSmallintField;
    QrLctCartao: TIntegerField;
    QrLctLinha: TIntegerField;
    QrLctPago: TFloatField;
    QrLctSALDO: TFloatField;
    QrLctID_Sub: TSmallintField;
    QrLctMez: TIntegerField;
    QrLctFornecedor: TIntegerField;
    QrLctcliente: TIntegerField;
    QrLctMoraDia: TFloatField;
    QrLctNOMECLIENTE: TWideStringField;
    QrLctNOMEFORNECEDOR: TWideStringField;
    QrLctTIPOEM: TWideStringField;
    QrLctNOMERELACIONADO: TWideStringField;
    QrLctOperCount: TIntegerField;
    QrLctLancto: TIntegerField;
    QrLctMulta: TFloatField;
    QrLctATRASO: TFloatField;
    QrLctJUROS: TFloatField;
    QrLctDataDoc: TDateField;
    QrLctNivel: TIntegerField;
    QrLctVendedor: TIntegerField;
    QrLctAccount: TIntegerField;
    QrLctMes2: TLargeintField;
    QrLctProtesto: TDateField;
    QrLctDataCad: TDateField;
    QrLctDataAlt: TDateField;
    QrLctUserCad: TSmallintField;
    QrLctUserAlt: TSmallintField;
    QrLctControle: TIntegerField;
    QrLctID_Pgto: TIntegerField;
    QrLctCtrlIni: TIntegerField;
    QrLctFatID_Sub: TIntegerField;
    QrLctICMS_P: TFloatField;
    QrLctICMS_V: TFloatField;
    QrLctDuplicata: TWideStringField;
    QrLctCOMPENSADO_TXT: TWideStringField;
    QrLctCliInt: TIntegerField;
    QrLctDepto: TIntegerField;
    QrLctDescoPor: TIntegerField;
    QrLctForneceI: TIntegerField;
    QrLctQtde: TFloatField;
    DsLct: TDataSource;
    QrEstqR: TmySQLQuery;
    QrEstqRQtd: TFloatField;
    QrEstqRVal: TFloatField;
    QrEstqP: TmySQLQuery;
    QrEstqPQtd: TFloatField;
    QrSel: TmySQLQuery;
    QrSelMovix: TIntegerField;
    QrControleVerBcoTabs: TIntegerField;
    QrControleLk: TIntegerField;
    QrControleDataCad: TDateField;
    QrControleDataAlt: TDateField;
    QrControleUserCad: TIntegerField;
    QrControleUserAlt: TIntegerField;
    QrControleCNAB_Rem: TIntegerField;
    QrControleCNAB_Rem_I: TIntegerField;
    QrControlePreEmMsgIm: TIntegerField;
    QrControlePreEmMsg: TIntegerField;
    QrControlePreEmail: TIntegerField;
    QrControleContasMes: TIntegerField;
    QrControleMultiPgto: TIntegerField;
    QrControleImpObs: TIntegerField;
    QrControleProLaINSSr: TFloatField;
    QrControleProLaINSSp: TFloatField;
    QrControleVerSalTabs: TIntegerField;
    QrControleCNAB_CaD: TIntegerField;
    QrControleCNAB_CaG: TIntegerField;
    QrControleAtzCritic: TIntegerField;
    QrControleContasTrf: TIntegerField;
    QrControleSomaIts: TIntegerField;
    QrControleContasAgr: TIntegerField;
    QrControleConfJanela: TIntegerField;
    QrControleDataPesqAuto: TSmallintField;
    QrControleAtividad: TSmallintField;
    QrControleCidades: TSmallintField;
    QrControleChequez: TSmallintField;
    QrControlePaises: TSmallintField;
    QrControleCambiosData: TDateField;
    QrControleCambiosUsuario: TIntegerField;
    QrControlereg10: TSmallintField;
    QrControlereg11: TSmallintField;
    QrControlereg50: TSmallintField;
    QrControlereg51: TSmallintField;
    QrControlereg53: TSmallintField;
    QrControlereg54: TSmallintField;
    QrControlereg56: TSmallintField;
    QrControlereg60: TSmallintField;
    QrControlereg75: TSmallintField;
    QrControlereg88: TSmallintField;
    QrControlereg90: TSmallintField;
    QrControleNumSerieNF: TSmallintField;
    QrControleSerieNF: TSmallintField;
    QrControleModeloNF: TSmallintField;
    QrControleControlaNeg: TSmallintField;
    QrControleFamilias: TSmallintField;
    QrControleFamiliasIts: TSmallintField;
    QrControleAskNFOrca: TSmallintField;
    QrControlePreviewNF: TSmallintField;
    QrControleOrcaRapido: TSmallintField;
    QrControleDistriDescoItens: TSmallintField;
    QrControleBalType: TSmallintField;
    QrControleOrcaOrdem: TSmallintField;
    QrControleOrcaLinhas: TSmallintField;
    QrControleOrcaLFeed: TIntegerField;
    QrControleOrcaModelo: TSmallintField;
    QrControleOrcaRodaPos: TSmallintField;
    QrControleOrcaRodape: TSmallintField;
    QrControleOrcaCabecalho: TSmallintField;
    QrControleCoresRel: TSmallintField;
    QrControleCFiscalPadr: TWideStringField;
    QrControleSitTribPadr: TWideStringField;
    QrControleCFOPPadr: TWideStringField;
    QrControleAvisosCxaEdit: TSmallintField;
    QrControleChConfCab: TIntegerField;
    QrControleImpDOS: TIntegerField;
    QrControleUnidadePadrao: TIntegerField;
    QrControleProdutosV: TIntegerField;
    QrControleCartDespesas: TIntegerField;
    QrControleReserva: TSmallintField;
    QrControleCNPJ: TWideStringField;
    QrControleVersao: TIntegerField;
    QrControleVerWeb: TIntegerField;
    QrControleErroHora: TIntegerField;
    QrControleSenhas: TIntegerField;
    QrControleSalarios: TIntegerField;
    QrControleEntidades: TIntegerField;
    QrControleUFs: TIntegerField;
    QrControleListaECivil: TIntegerField;
    QrControlePerfis: TIntegerField;
    QrControleUsuario: TIntegerField;
    QrControleContas: TIntegerField;
    QrControleCentroCusto: TIntegerField;
    QrControleDepartamentos: TIntegerField;
    QrControleDividas: TIntegerField;
    QrControleDividasIts: TIntegerField;
    QrControleDividasPgs: TIntegerField;
    QrControleCarteiras: TIntegerField;
    QrControleCartaG: TIntegerField;
    QrControleCartas: TIntegerField;
    QrControleConsignacao: TIntegerField;
    QrControleGrupo: TIntegerField;
    QrControleSubGrupo: TIntegerField;
    QrControleConjunto: TIntegerField;
    QrControlePlano: TIntegerField;
    QrControleInflacao: TIntegerField;
    QrControlekm: TIntegerField;
    QrControlekmMedia: TIntegerField;
    QrControlekmIts: TIntegerField;
    QrControleFatura: TIntegerField;
    QrControleLanctos: TLargeintField;
    QrControleEntiGrupos: TIntegerField;
    QrControleAparencias: TIntegerField;
    QrControlePages: TIntegerField;
    QrControleMultiEtq: TIntegerField;
    QrControleEntiTransp: TIntegerField;
    QrControleExcelGru: TIntegerField;
    QrControleExcelGruImp: TIntegerField;
    QrControleMediaCH: TIntegerField;
    QrControleImprime: TIntegerField;
    QrControleImprimeBand: TIntegerField;
    QrControleImprimeView: TIntegerField;
    QrControleComProdPerc: TIntegerField;
    QrControleComProdEdit: TIntegerField;
    QrControleComServPerc: TIntegerField;
    QrControleComServEdit: TIntegerField;
    QrControlePadrPlacaCar: TWideStringField;
    QrControleServSMTP: TWideStringField;
    QrControleNomeMailOC: TWideStringField;
    QrControleDonoMailOC: TWideStringField;
    QrControleMailOC: TWideStringField;
    QrControleCorpoMailOC: TWideMemoField;
    QrControleConexaoDialUp: TWideStringField;
    QrControleMailCCCega: TWideStringField;
    QrControleMoedaVal: TFloatField;
    QrControleTela1: TIntegerField;
    QrControleChamarPgtoServ: TIntegerField;
    QrControleFormUsaTam: TIntegerField;
    QrControleFormHeight: TIntegerField;
    QrControleFormWidth: TIntegerField;
    QrControleFormPixEsq: TIntegerField;
    QrControleFormPixDir: TIntegerField;
    QrControleFormPixTop: TIntegerField;
    QrControleFormPixBot: TIntegerField;
    QrControleFormFoAlt: TIntegerField;
    QrControleFormFoPro: TFloatField;
    QrControleFormUsaPro: TIntegerField;
    QrControleFormSlides: TIntegerField;
    QrControleFormNeg: TIntegerField;
    QrControleFormIta: TIntegerField;
    QrControleFormSub: TIntegerField;
    QrControleFormExt: TIntegerField;
    QrControleFormFundoTipo: TIntegerField;
    QrControleFormFundoBMP: TWideStringField;
    QrControleServInterv: TIntegerField;
    QrControleServAntecip: TIntegerField;
    QrControleAdiLancto: TIntegerField;
    QrControleContaSal: TIntegerField;
    QrControleContaVal: TIntegerField;
    QrControlePronomeE: TWideStringField;
    QrControlePronomeM: TWideStringField;
    QrControlePronomeF: TWideStringField;
    QrControlePronomeA: TWideStringField;
    QrControleSaudacaoE: TWideStringField;
    QrControleSaudacaoM: TWideStringField;
    QrControleSaudacaoF: TWideStringField;
    QrControleSaudacaoA: TWideStringField;
    QrControleNiver: TSmallintField;
    QrControleNiverddA: TSmallintField;
    QrControleNiverddD: TSmallintField;
    QrControleLastPassD: TDateTimeField;
    QrControleMultiPass: TIntegerField;
    QrControleCodigo: TIntegerField;
    QrControleAlterWeb: TSmallintField;
    QrControleAtivo: TSmallintField;
    QrControleLastBco: TIntegerField;
    QrUpdW: TmySQLQuery;
    frxDsDono: TfrxDBDataset;
    QrControleMyPerJuros: TFloatField;
    QrControleMyPerMulta: TFloatField;
    QrDono: TmySQLQuery;
    QrDonoCodigo: TIntegerField;
    QrDonoCadastro: TDateField;
    QrDonoNOMEDONO: TWideStringField;
    QrDonoCNPJ_CPF: TWideStringField;
    QrDonoIE_RG: TWideStringField;
    QrDonoNIRE_: TWideStringField;
    QrDonoRUA: TWideStringField;
    QrDonoCOMPL: TWideStringField;
    QrDonoBAIRRO: TWideStringField;
    QrDonoCIDADE: TWideStringField;
    QrDonoNOMELOGRAD: TWideStringField;
    QrDonoNOMEUF: TWideStringField;
    QrDonoPais: TWideStringField;
    QrDonoTipo: TSmallintField;
    QrDonoTE1: TWideStringField;
    QrDonoTE2: TWideStringField;
    QrDonoFAX: TWideStringField;
    QrDonoENatal: TDateField;
    QrDonoPNatal: TDateField;
    QrDonoECEP_TXT: TWideStringField;
    QrDonoNUMERO_TXT: TWideStringField;
    QrDonoCNPJ_TXT: TWideStringField;
    QrDonoFAX_TXT: TWideStringField;
    QrDonoTE1_TXT: TWideStringField;
    QrDonoTE2_TXT: TWideStringField;
    QrDonoNATAL_TXT: TWideStringField;
    QrDonoE_LNR: TWideStringField;
    QrDonoE_CUC: TWideStringField;
    QrDonoEContato: TWideStringField;
    QrDonoECel: TWideStringField;
    QrDonoCEL_TXT: TWideStringField;
    QrDonoE_LN2: TWideStringField;
    QrDonoAPELIDO: TWideStringField;
    QrDonoNUMERO: TFloatField;
    QrDonoRespons1: TWideStringField;
    QrDonoRespons2: TWideStringField;
    QrDonoFONES: TWideStringField;
    QrBoss: TmySQLQuery;
    QrBossMasSenha: TWideStringField;
    QrBossMasLogin: TWideStringField;
    QrBossEm: TWideStringField;
    QrBossCNPJ: TWideStringField;
    QrBossMasZero: TWideStringField;
    QrBSit: TmySQLQuery;
    QrBSitSitSenha: TSmallintField;
    QrSenhas: TmySQLQuery;
    QrSenhasSenhaNew: TWideStringField;
    QrSenhasSenhaOld: TWideStringField;
    QrSenhaslogin: TWideStringField;
    QrSenhasNumero: TIntegerField;
    QrSenhasPerfil: TIntegerField;
    QrSenhasFuncionario: TIntegerField;
    QrSSit: TmySQLQuery;
    QrSSitSitSenha: TSmallintField;
    frxDsMaster: TfrxDBDataset;
    QrControleLogoBig1: TWideStringField;
    QrControleMoedaBr: TIntegerField;
    QrControleCasasProd: TSmallintField;
    QrSenhasIP_Default: TWideStringField;
    QrCarteirasNOMEDOBANCO: TWideStringField;
    QrAgora: TmySQLQuery;
    QrAgoraANO: TLargeintField;
    QrAgoraMES: TLargeintField;
    QrAgoraDIA: TLargeintField;
    QrAgoraHORA: TLargeintField;
    QrAgoraMINUTO: TLargeintField;
    QrAgoraSEGUNDO: TLargeintField;
    QrAgoraAGORA: TDateTimeField;
    QrEnderecoNO_TIPO_DOC: TWideStringField;
    QrMasterUsaAccMngr: TSmallintField;
    QrNotifi: TmySQLQuery;
    QrControleCanAltBalTX: TSmallintField;
    QrDonoLograd: TFloatField;
    QrDonoCEP: TFloatField;
    procedure DataModuleCreate(Sender: TObject);
    procedure QrMasterAfterOpen(DataSet: TDataSet);
    procedure QrMasterCalcFields(DataSet: TDataSet);
    procedure TbMovCalcFields(DataSet: TDataSet);
    procedure TbMovBeforePost(DataSet: TDataSet);
    procedure TbMovBeforeDelete(DataSet: TDataSet);
    procedure DataModuleDestroy(Sender: TObject);
    procedure QrEnderecoCalcFields(DataSet: TDataSet);
    procedure QrDonoCalcFields(DataSet: TDataSet);
    procedure QrControleAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    FFmtPrc, FProdDelete: Integer;
    FStrFmtPrc, FStrFmtCus, FStrFmtQtd: String;
    function Privilegios(Usuario : Integer) : Boolean;
    procedure RecalcSaldoCarteira(Tipo, Carteira: Integer; Localiza: Byte);
    procedure VerificaSenha(Index: Integer; Login, Senha: String);

    function ConcatenaRegistros(Matricula: Integer): String;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    function BuscaProximoMovix: Integer;
    procedure InsereRegistrosExtraBalanceteMensal_01(QrInd01: TmySQLQuery;
              DataIDate, DataFDate: TDateTime; DataITrue, DataFTrue: Boolean;
              CliInt: Integer; Campos: String; DescrSubstit: Boolean);
    procedure AlteraSMIA(Qry: TmySQLQuery);
    procedure ExcluiItensEstqNFsCouroImportadas();
    procedure GeraEstoqueEm_2(Empresa_Txt: String; DataIni, DataFim: TDateTime;
              PGT, GG1, GG2, GG3, TipoPesq: Integer; GraCusPrc, TabePrcCab:
              Variant; GraCusPrcCodigo, TabePrcCabCodigo, PrdGrupTipCodigo,
              GraGru1Nivel1, GraGru2Nivel2, GraGru3Nivel3: Integer;
              QrPGT_SCC, QrAbertura2a, QrAbertura2b: TmySQLQuery;
              UsaTabePrcCab, SoPositivos: Boolean; LaAviso: TLabel;
              NomeTb_SMIC: String; QrSMIC_X: TmySQLQuery;
              var TxtSQL_Part1, TxtSQL_Part2: String);
    function  TabelasQueNaoQueroCriar(): String;
    procedure ReopenControle();
    procedure ReopenParamsEspecificos(Empresa: Integer);
  end;

var
  Dmod: TDmod;
  FVerifi: Boolean;
  //
  QvEntra3: TmySQLQuery;
  QvVenda3: TmySQLQuery;
  QvDevol3: TmySQLQuery;
  QvRecom3: TmySQLQuery;

implementation

uses UnMyObjects, Principal, SenhaBoss, UnLock_MLA, Servidor, VerifiDB, MyDBCheck,
  InstallMySQL41, Planning_Dmk, MyListas, ModuleGeral, ModuleFin, SMI_Edita,
  UnLic_Dmk, DmkDAC_PF, VerifiDBTerceiros, ModTX_CRC;

{$R *.DFM}

procedure TDmod.DataModuleCreate(Sender: TObject);
var
  Versao, VerZero, Resp: Integer;
  BD: String;
  Verifica, VerificaDBTerc: Boolean;
  //
  OthSetCon, Database: String;
begin
  VerificaDBTerc := False;
  //
  if MyDB.Connected then
    Geral.MensagemBox('MyDB est� conectado antes da configura��o!',
    'Aviso!', MB_OK+MB_ICONWARNING);
  if MyLocDataBase.Connected then
    Geral.MensagemBox('MyLocDataBase est� conectado antes da configura��o!',
    'Aviso!', MB_OK+MB_ICONWARNING);





  // 2020-02-04
  //VAR_BDSENHA := 'wkljweryhvbirt';
  //VAR_PORTA := Geral.ReadAppKeyCU('Porta', 'Dermatek', ktInteger, 3306);

  OthSetCon := Geral.ReadAppKeyCU('OthSetCon', Application.Title, ktString, '');
  if OthSetCon = EmptyStr then
    VAR_BDSENHA := CO_USERSPNOW
  else
    VAR_BDSENHA := dmkPF.Criptografia(OthSetCon, CO_LLM_SecureStr);
  //
  VAR_PORTA := Geral.ReadAppKeyCU('Porta', 'Dermatek', ktInteger, 3306);
  VAR_SERVIDOR := Geral.ReadAppKeyCU('Server', Application.Title, ktInteger, 0);
  //
{
  case Server of
    1:   VAR_SERVIDOR := 0;
    2:   VAR_SERVIDOR := 1;
    3:   VAR_SERVIDOR := 2;
    else VAR_SERVIDOR := -1;
  end;
}
  VAR_IP       := Geral.ReadAppKeyCU('IPServer', Application.Title, ktString, CO_VAZIO);
  DataBase     := Geral.ReadAppKeyCU('Database', Application.Title, ktString, 'mysql');
  VAR_SQLUSER  := Geral.ReadAppKeyCU('OthUser', Application.Title, ktString, 'root');
  if VAR_BDSENHA = '' then
    VAR_BDSENHA := CO_USERSPNOW;
  if VAR_SQLUSER = '' then
    VAR_SQLUSER := 'root';
  //Geral.WriteAppKeyCU('Porta', 'Dermatek', VAR_PORTA, ktInteger);
  // FIM 2020-02-04









{
  if not GOTOy.OMySQLEstaInstalado(
  FmPlanning_Dmk.LaAviso1, FmPlanning_Dmk.LaAviso2, FmPlanning_Dmk.ProgressBar1) then
  begin
    //raise EAbort.Create('');
    //
    MyObjects.Informa2(FmPlanning_Dmk.LaAviso1, FmPlanning_Dmk.LaAviso2, False,
      'N�o foi poss�vel a conex�o ao IP: [' + VAR_IP + ']');
    FmPlanning_Dmk.BtEntra.Visible := False;
    //
    Exit;
  end;
}
  /////////////////////////////////////////
  if GOTOy.SenhaDesconhecida then
  begin
    raise EAbort.Create('Senha desconhecida');
    Exit;
  end;
  ///////////////////////////////////////////////////////////
{
  ZZDB.UserPassword := VAR_BDSENHA;
  try
    ZZDB.Connected := True;
  except
    ZZDB.UserPassword := '852456';
    try
      ZZDB.Connected := True;
      QrUpd.Database := ZZDB;
      QrUpd.SQL.Clear;
      QrUpd.SQL.Add('UPDATE user SET Password=PASSWORD("'+VAR_BDSENHA+'")');
      QrUpd.SQL.Add('');
      QrUpd.SQL.Add('WHERE User="root"');
      QrUpd.ExecSQL;
      ///////////
      QrUpd.SQL.Clear;
      QrUpd.SQL.Add('FLUSH PRIVILEGES');
      QrUpd.ExecSQL;
      ///////////
      FmPrincipal.Close;
      Application.Terminate;
      Exit;
    except
      if VAR_SERVIDOR = 2 then ShowMessage('Banco de dados teste n�o se conecta!');
    end;
  end;
}
  /////////////////////////////////////////
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  //
  VAR_SQLx := TStringList.Create;
  VAR_SQL1 := TStringList.Create;
  VAR_SQL2 := TStringList.Create;
  VAR_SQLa := TStringList.Create;
  //
  MAR_SQLx := TStringList.Create;
  MAR_SQL1 := TStringList.Create;
  MAR_SQLa := TStringList.Create;
  try
    if VAR_SERVIDOR = 1 then
    begin
      if not GetSystemMetrics(SM_NETWORK) and $01 = $01 then
      begin
      Geral.MensagemBox('M�quina cliente sem rede.', 'Erro', MB_OK+MB_ICONERROR);
        //Application.Terminate;
      end;
    end;
  except
    Application.Terminate;
    Exit;
  end;
  if VAR_APPTERMINATE then
  begin
    Application.Terminate;
    Exit;
  end;
  Geral.DefineFormatacoes;
  //
{
  VAR_SERVIDOR := Geral.ReadAppKey('Server', Application.Title, ktInteger, 0,
    HKEY_LOCAL_MACHINE);
  if not (VAR_SERVIDOR in [1,2]) then
  begin
    Application.CreateForm(TFmServidor, FmServidor);
    FmServidor.ShowModal;
    FmServidor.Destroy;
  end;
}
  if VAR_IP = CO_VAZIO then
  begin
    VAR_IP := '127.0.0.1';
    if VAR_SERVIDOR = 1 then
    begin
      if InputQuery('IP do Servidor', 'Defina o IP do Servidor', VAR_IP) then
      begin
        Geral.WriteAppKey('IPServer', Application.Title, VAR_IP, ktString,
          HKEY_LOCAL_MACHINE);
      end else
      begin
        Application.Terminate;
        Exit;
      end;
    end;
  end;
(*
  MyDB.UserPassword := VAR_BDSENHA;
  MyDB.Host := VAR_IP;
  MyDB.DatabaseName := 'mysql';// existe com certeza se estiver instalado
*)
  //
{
  UnDmkDAC_PF.ConectaMyDB_DAC(MyDB, 'mysql', VAR_IP, VAR_PORTA, 'root',
  VAR_BDSENHA, (*Desconecta*)True, (*Configura*)True, (*Conecta*)False);
}
  UnDmkDAC_PF.ConectaMyDB_DAC(MyDB, Database, VAR_IP, VAR_PORTA, VAR_SQLUSER,
  VAR_BDSENHA, (*Desconecta*)True, (*Configura*)True, (*Conecta*)False);
  //
  //
  QrAux.Close;
  QrAux.SQL.Clear;
  QrAux.SQL.Add('SHOW DATABASES');
  QrAux.Open;
  BD := CO_VAZIO;
  while not QrAux.Eof do
  begin
    if Uppercase(QrAux.FieldByName('Database').AsString)=Uppercase(TMeuDB) then
      BD := TMeuDB;
    QrAux.Next;
  end;
  MyDB.Close;
  MyDB.DataBaseName := BD;
  if MyDB.DataBaseName = CO_VAZIO then
  begin
    Resp := Geral.MensagemBox('O banco de dados '+TMeuDB+
      ' n�o existe e deve ser criado. Confirma a cria��o?', 'Banco de Dados',
      MB_YESNOCANCEL+MB_ICONQUESTION);
    if Resp = ID_YES then
    begin
      QrAux.Close;
      QrAux.SQL.Clear;
      QrAux.SQL.Add('CREATE DATABASE '+TMeuDB);
      QrAux.ExecSQL;
      //
      if MyDB.Connected then
        MyDB.Disconnect;
      MyDB.DatabaseName := TMeuDB;
    end else if Resp = ID_CANCEL then
    begin
      Geral.MensagemBox('O aplicativo ser� encerrado!', 'Banco de Dados',
      MB_OK+MB_ICONWARNING);
      Application.Terminate;
      Exit;
    end;
  end;
  //
  GOTOy.DefinePathMySQL;
  //
{ Precisa ainda?
  MyLocDatabase.UserPassword := VAR_BDSENHA;
  MyLocDatabase.DatabaseName := 'mysql';// existe com certeza se estiver instalado
  QrAuxL.Close;
  QrAuxL.SQL.Clear;
  QrAuxL.SQL.Add('SHOW DATABASES');
  QrAuxL.Open;
  BD := CO_VAZIO;
  while not QrAuxL.Eof do
  begin
    if Uppercase(QrAuxL.FieldByName('Database').AsString)=Uppercase(TLocDB) then
      BD := TLocDB;
    QrAuxL.Next;
  end;
  MyLocDatabase.Close;
  MyLocDatabase.DatabaseName := BD;
  if MyLocDatabase.DataBaseName = CO_VAZIO then
  begin
    Resp := Geral.MensagemBox('O banco de dados local '+TLocDB+
      ' n�o existe e deve ser criado. Confirma a cria��o?',
      'Banco de Dados Local', MB_YESNOCANCEL+MB_ICONQUESTION);
    if Resp = ID_YES then
    begin
      QrAuxL.Close;
      QrAuxL.SQL.Clear;
      QrAuxL.SQL.Add('CREATE DATABASE '+TLocDB);
      QrAuxL.ExecSQL;
      MyLocDatabase.DatabaseName := TLocDB;
    end else if Resp = ID_CANCEL then
    begin
      Geral.MensagemBox('O aplicativo ser� encerrado!',
      'Banco de Dados Local', MB_OK+MB_ICONWARNING);
      Application.Terminate;
      Exit;
    end;
  end;
}
  //
  VerZero := Geral.ReadAppKey('Versao', Application.Title, ktInteger,
    0, HKEY_LOCAL_MACHINE);
  Versao := Geral.ReadAppKey('Versao', Application.Title, ktInteger,
    CO_VERSAO, HKEY_LOCAL_MACHINE);
  Verifica := False;
  if Versao < CO_VERSAO then Verifica := True;
  if VerZero = 0 then
  begin
    Resp := Geral.MensagemBox('N�o h� informa��o de vers�o no registro, '+
    'se esta n�o for a primeira execu��o do aplicativo selecione cancelar e '+
    'informe o respons�vel!. Caso confirme, ser� verificado a composi��o do '+
    'banco de dados. Confirma a Verifica��o?',
    'Aus�ncia de Informa��o de Vers�o no Registro',
    MB_YESNOCANCEL+MB_ICONWARNING);
    if Resp = ID_YES then Verifica := True
    else if Resp = ID_CANCEL then
    begin
      Application.Terminate;
      Exit;
    end;
  end;
  //if VAR_SERVIDOR = 2 then GOTOy.LiberaUso;
  //VAR_MyBDFINANCAS := MyDB;  // MyPagtos : CASHIER
  //FmPrincipal.VerificaTerminal;
  MyList.ConfiguracoesIniciais(1);

  //

  try
    Application.CreateForm(TDmodG, DmodG);
  except
    Geral.MensagemBox('Imposs�vel criar Modulo de dados Geral', 'Erro', MB_OK+MB_ICONERROR);
    Application.Terminate;
    Exit;
  end;
  try
    Application.CreateForm(TDModFin, DModFin);
  except
    Geral.MensagemBox('Imposs�vel criar Modulo Financeiro', 'Erro', MB_OK+MB_ICONERROR);
    Application.Terminate;
    Exit;
  end;
  if Verifica then
  begin
    Application.CreateForm(TFmVerifiDB, FmVerifiDB);
    with FmVerifiDb do
    begin
      BtSair.Enabled := False;
      FVerifi := True;
      ShowModal;
      FVerifi := False;
      Destroy;
    end;
  end;
  //
  try
    QrMaster.Open;
    VAR_EMPRESANOME := QrMasterEm.Value;
  except
    try
      Application.CreateForm(TFmVerifiDB, FmVerifiDB);
      with FmVerifiDb do
      begin
        BtSair.Enabled := False;
        FVerifi := True;
        ShowModal;
        FVerifi := False;
        Destroy;
        //
        VerificaDBTerc := True;
      end;
    except;
      MyDB.DatabaseName := CO_VAZIO;
      raise;
    end;
  end;
  if VerificaDBTerc then
  begin
    Application.CreateForm(TFmVerifiDBTerceiros, FmVerifiDBTerceiros);
    FmVerifiDBTerceiros.FVerifi := True;
    FmVerifiDBTerceiros.ShowModal;
    FmVerifiDBTerceiros.FVerifi := False;
    FmVerifiDBTerceiros.Destroy;
  end;
  //
  Application.CreateForm(TDmModTX_CRC, DmModTX_CRC);
  // Desmarcar
  Lic_Dmk.LiberaUso6;
end;

procedure TDmod.RecalcSaldoCarteira(Tipo, Carteira: Integer; Localiza: Byte);
var
  Saldo: Double;
begin
  if Tipo < 2 then
  begin
    QrSomaM.Close;
    QrSomaM.SQL.Clear;
    QrSomaM.SQL.Add('SELECT Inicial Valor FROM carteiras');
    QrSomaM.SQL.Add('WHERE Tipo=:P0 AND Codigo=:P1');
    QrSomaM.Params[0].AsInteger := Tipo;
    QrSomaM.Params[1].AsInteger := Carteira;
    QrSomaM.Open;
    Saldo := QrSomaMValor.Value;
  end else Saldo := 0;
  QrSomaM.Close;
  QrSomaM.SQL.Clear;
  if Tipo = 2 then begin
    QrSomaM.SQL.Add('SELECT SUM(IF(Sit=0, (Credito-Debito),');
    QrSomaM.SQL.Add('IF(Sit=1, (Credito-Debito+Pago), 0))) Valor FROM lanctos');
  end else
    QrSomaM.SQL.Add('SELECT (SUM(Credito) - SUM(Debito)) Valor FROM lanctos');
  QrSomaM.SQL.Add('WHERE Tipo=:P0 AND Carteira=:P1');
  QrSomaM.Params[0].AsInteger := Tipo;
  QrSomaM.Params[1].AsInteger := Carteira;
  QrSomaM.Open;
  Saldo := Saldo + QrSomaMValor.Value;
  QrSomaM.Close;

  QrUpdM.Close;
  QrUpdM.SQL.Clear;
  QrUpdM.SQL.Add('UPDATE carteiras SET Saldo=:P0');
  QrUpdM.SQL.Add('WHERE Tipo=:P1 AND Codigo=:P2');
  QrUpdM.Params[0].AsFloat := Saldo;
  QrUpdM.Params[1].AsInteger := Tipo;
  QrUpdM.Params[2].AsInteger := Carteira;
  QrUpdM.ExecSQL;
(*  if Localiza = 3 then
  begin
    if Dmod.QrLanctos.State in [dsBrowse] then
    if (Dmod.QrLanctosTipo.Value = Tipo)
    and (Dmod.QrLanctosCarteira.Value = Carteira) then
    Localiza := 1;
    Dmod.DefParams;
  end;
  if Localiza = 1 then GOTOx.LocalizaCodigo(Carteira, Carteira);*)
end;

procedure TDmod.ReopenControle();
begin
  QrControle.Close;
  QrControle.Open;
end;

procedure TDmod.ReopenParamsEspecificos(Empresa: Integer);
begin
  //Compatibilidade
end;

procedure TDmod.VerificaSenha(Index: Integer; Login, Senha: String);
var
  s: String;
begin
  s := Uppercase(Senha);
  if (s = VAR_BOSS) or (s = CO_MASTER)
  or ((s = VAR_ADMIN) and (VAR_ADMIN <> ''))
  then VAR_SENHARESULT := 2 else
  begin
    if Index = 7 then
    begin
(*      QrSenha.Close;
      QrSenha.Params[0].AsString := Login;
      QrSenha.Params[1].AsString := Senha;
      QrSenha.Open;
      if QrSenha.RecordCount > 0 then
      begin
        QrPerfis.Close;
        QrPerfis.Params[0].AsInteger := QrSenhaPerfilW.Value;
        QrPerfis.Open;
        if QrPerfisVendas.Value = 'V' then VAR_SENHARESULT := 2
        else Geral.MensagemBox(0,'Acesso negado. Senha Inv�lida',
        'Permiss�o por senha', MB_OK+MB_ICONEXCLAMATION);
        QrPerfis.Close;
      end else ShowMessage('Login inv�lido.');
      QrSenha.Close;                      *)
    end else ShowMessage('Em constru��o');
  end;
end;

procedure TDmod.QrMasterAfterOpen(DataSet: TDataSet);
begin
  if Trim(QrMasterCNPJ.Value) = CO_VAZIO then
  ShowMessage('CNPJ n�o definido ou Empresa n�o definida. '+Chr(13)+Chr(10)+
  'Algumas ferramentas do aplicativo poder�o ficar inacess�veis.');
  FmPrincipal.Caption := Application.Title+'  ::  '+QrMasterEm.Value+' # '+
  QrMasterCNPJ.Value;
  //
  ReopenControle();
  //
  if QrControleSoMaiusculas.Value = 'V' then VAR_SOMAIUSCULAS := True;
  VAR_UFPADRAO        := QrControleUFPadrao.Value;
  VAR_CIDADEPADRAO    := QrControleCidade.Value;
  VAR_TRAVACIDADE     := QrControleTravaCidade.Value;
  VAR_MOEDA           := QrControleMoeda.Value;
  //
  VAR_CARTVEN         := QrControleCartVen.Value;
  VAR_CARTCOM         := QrControleCartCom.Value;
  VAR_CARTRES         := QrControleCartReS.Value;
  VAR_CARTDES         := QrControleCartDeS.Value;
  VAR_CARTREG         := QrControleCartReG.Value;
  VAR_CARTDEG         := QrControleCartDeG.Value;
  VAR_CARTCOE         := QrControleCartCoE.Value;
  VAR_CARTCOC         := QrControleCartCoC.Value;
  VAR_CARTEMD         := QrControleCartEmD.Value;
  VAR_CARTEMA         := QrControleCartEmA.Value;
  //
  VAR_PAPERSET        := True;
  VAR_PAPERTOP        := QrControlePaperTop.Value;
  VAR_PAPERLEF        := QrControlePaperLef.Value;
  VAR_PAPERWID        := QrControlePaperWid.Value;
  VAR_PAPERHEI        := QrControlePaperHei.Value;
  VAR_PAPERFCL        := QrControlePaperFcl.Value;
  //
end;

procedure TDmod.QrMasterCalcFields(DataSet: TDataSet);
begin
(*  case QrMasterTipo.Value of
    0: QrMasterEm.Value := QrMasterRazaoSocial.Value;
    1: QrMasterEm.Value := QrMasterNome.Value;
    else QrMasterEm.Value := QrMasterRazaoSocial.Value;
  end;*)
  QrMasterTE1_TXT.Value := Geral.FormataTelefone_TT(QrMasterETe1.Value);
  QrMasterCEP_TXT.Value :=Geral.FormataCEP_NT(QrMasterECEP.Value);
  QrMasterCNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrMasterCNPJ.Value);
end;

function TDmod.ConcatenaRegistros(Matricula: Integer): String;
(*var
  Registros, Conta: Integer;
  Liga: String;*)
begin
(*  Qr??.Close;
  Qr??.Params[0].AsInteger := Matricula;
  Qr??.Open;
  //
  Registros := MLAGeral.Registros(Qr??);
  if Registros > 1 then Result := 'Pagamento de '
  else if Registros = 1 then Result := 'Pagamento de ';
  Result := Result + Qr??NOMECURSO.Value;
  Qr??.Next;
  Conta := 1;
  Liga := ', ';
  while not Qr??.Eof do
  begin
    Conta := Conta + 1;
    if Conta = Registros then Liga := ' e ';
    Result := Result + Liga + Qr??NOMECURSO.Value;
    Qr?
    ?.Next;
  end;*)
end;


procedure TDmod.TbMovCalcFields(DataSet: TDataSet);
begin
(*  TbMovSUBT.Value := TbMovQtde.Value * TbMovPreco.Value;
  TbMovTOTA.Value := TbMovSUBT.Value - TbMovDesco.Value;
  case TbMovTipo.Value of
    -1: TbMovNOMETIPO.Value := 'Venda';
     1: TbMovNOMETIPO.Value := 'Compra';
    else TbMovNOMETIPO.Value := 'INDEFINIDO';
  end;
  case TbMovENTITIPO.Value of
    0: TbMovNOMEENTIDADE.Value := TbMovENTIRAZAO.Value;
    1: TbMovNOMEENTIDADE.Value := TbMovENTINOME.Value;
    else TbMovNOMEENTIDADE.Value := 'INDEFINIDO';
  end;
  if TbMovESTQQ.Value <> 0 then
  TbMovCUSTOPROD.Value := TbMovESTQV.Value / TbMovESTQQ.Value else
  TbMovCUSTOPROD.Value := 0;*)
end;

procedure TDmod.TbMovBeforePost(DataSet: TDataSet);
begin
(*  case TbMovTipo.Value of
   -1:
    begin
      TbMovTotalV.Value := (TbMovQtde.Value * TbMovPreco.Value) - TbMovDesco.Value;
      TbMovTotalC.Value := TbMovQtde.Value * TbMovCUSTOPROD.Value;
    end;
    1:
    begin
      TbMovTotalV.Value := 0;
      TbMovTotalC.Value := (TbMovQtde.Value * TbMovPreco.Value) - TbMovDesco.Value;
    end;
    else begin
      //
    end;
  end;*)
end;

function TDmod.TabelasQueNaoQueroCriar: String;
begin
  Result := '';
end;

procedure TDmod.TbMovBeforeDelete(DataSet: TDataSet);
begin
  //FProdDelete := TbMovProduto.Value;
end;

procedure TDmod.DataModuleDestroy(Sender: TObject);
begin
  WSACleanup;
end;

function TDMod.Privilegios(Usuario : Integer) : Boolean;
begin
  Result := False;
  FM_MASTER := 'F';
  Dmod.QrPerfis.Params[0].AsInteger := Usuario;
  Dmod.QrPerfis.Open;
  if Dmod.QrPerfis.RecordCount > 0 then
  begin
    Result := True;
    //FM_EQUIPAMENTOS             := Dmod.QrPerfisEquipamentos.Value;
    //FM_MARCAS          := Dmod.QrPerfisMarcas.Value;
    //FM_MODELOS                := Dmod.QrPerfisModelos.Value;
    //FM_SERVICOS            := Dmod.QrPerfisServicos.Value;
    //FM_PRODUTOS            := Dmod.QrPerfisProdutos.Value;
  end;
  Dmod.QrPerfis.Close;
end;

procedure TDmod.QrEnderecoCalcFields(DataSet: TDataSet);
var
  Natal: TDateTime;
begin
  QrEnderecoTE1_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrEnderecoTe1.Value);
  QrEnderecoFAX_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrEnderecoFax.Value);
  QrEnderecoCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrEnderecoCNPJ_CPF.Value);
  //
  QrEnderecoNUMERO_TXT.Value :=
    MLAGeral.FormataNumeroDeRua(QrEnderecoRUA.Value, QrEnderecoNumero.Value, False);
  QrEnderecoE_ALL.Value := QrEnderecoNOMELOGRAD.Value;
  if Trim(QrEnderecoE_ALL.Value) <> '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' ';
  QrEnderecoE_ALL.Value := QrEnderecoE_ALL.Value + QrEnderecoRua.Value;
  if Trim(QrEnderecoRua.Value) <> '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ', ' + QrEnderecoNUMERO_TXT.Value;
  if Trim(QrEnderecoCompl.Value) <>  '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' ' + QrEnderecoCompl.Value;
  if Trim(QrEnderecoBairro.Value) <>  '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' - ' + QrEnderecoBairro.Value;
  if QrEnderecoCEP.Value > 0 then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' CEP ' +Geral.FormataCEP_NT(QrEnderecoCEP.Value);
  if Trim(QrEnderecoCidade.Value) <>  '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' - ' + QrEnderecoCidade.Value;
  //
  QrEnderecoECEP_TXT.Value :=Geral.FormataCEP_NT(QrEnderecoCEP.Value);
  //
  if QrEnderecoTipo.Value = 0 then
  begin
    Natal := QrEnderecoENatal.Value;
    QrEnderecoNO_TIPO_DOC.Value := 'CNPJ';
  end else begin
    Natal := QrEnderecoPNatal.Value;
    QrEnderecoNO_TIPO_DOC.Value := 'CPF';
  end;
  if Natal < 2 then QrEnderecoNATAL_TXT.Value := ''
  else QrEnderecoNATAL_TXT.Value := FormatDateTime(VAR_FORMATDATE2, Natal);
end;

procedure TDmod.DefParams;
begin
  FmPrincipal.DefParams;
end;

procedure TDmod.ExcluiItensEstqNFsCouroImportadas;
var
  MyCursor: TCursor;
begin
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  try
    Dmod.QrUpdM.SQL.Clear;
    Dmod.QrUpdM.SQL.Add('DELETE FROM stqmovitsa ');
    Dmod.QrUpdM.SQL.Add('WHERE GraGruX < 0 ');
    Dmod.QrUpdM.SQL.Add('AND GraGruX > -90000');
    Dmod.QrUpdM.SQL.Add('AND Tipo IN (51,151)');
    Dmod.QrUpdM.ExecSQL;
  finally
    Screen.Cursor := MyCursor;
  end;
end;

procedure TDmod.GeraEstoqueEm_2(Empresa_Txt: String; DataIni,
  DataFim: TDateTime; PGT, GG1, GG2, GG3, TipoPesq: Integer; GraCusPrc,
  TabePrcCab: Variant; GraCusPrcCodigo, TabePrcCabCodigo, PrdGrupTipCodigo,
  GraGru1Nivel1, GraGru2Nivel2, GraGru3Nivel3: Integer; QrPGT_SCC, QrAbertura2a,
  QrAbertura2b: TmySQLQuery; UsaTabePrcCab, SoPositivos: Boolean;
  LaAviso: TLabel; NomeTb_SMIC: String; QrSMIC_X: TmySQLQuery; var TxtSQL_Part1,
  TxtSQL_Part2: String);
var
  PrdGrupTip_Txt, StqCenCad_Txt, DiaSeguinte, GrupoBal_Txt,
  GraCusPrc_Txt, TabePrcCab_Txt, TextoA, TextoB, TextoC, CliInt_Txt,
  EntiSitio_Txt: String;
begin
  Screen.Cursor := crHourGlass;
  try
    //
    //DiaSeguinte := FormatDateTime('yyyy-mm-dd', TPDataFim2.Date + 1);
    DiaSeguinte := FormatDateTime('yyyy-mm-dd', DataFim + 1);
    if TipoPesq = 1 then
    begin
      if GraCusPrc <> NULL then
        GraCusPrc_Txt := FormatFloat('0', GraCusPrcCodigo)
      else // N�o pode ser nulo!
        GraCusPrc_Txt := '0';
      //
    end;
    if TabePrcCab <> NULL then
      TabePrcCab_Txt := FormatFloat('0', TabePrcCabCodigo)
    else // N�o pode ser nulo?
      TabePrcCab_Txt := '0';
    //
    MyObjects.Informa(LaAviso, True, 'Tabela temporaria - Excluindo caso exista');
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('DROP TABLE IF EXISTS stqmovitsc;');
    DmodG.QrUpdPID1.ExecSQL;
    //
    MyObjects.Informa(LaAviso, True, 'Tabela temporaria - Adicionando dados do arquivo morto');
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('CREATE TABLE stqmovitsc');
    DmodG.QrUpdPID1.SQL.Add('SELECT smi.*');
    DmodG.QrUpdPID1.SQL.Add('FROM '+TMeuDB+'.stqmovitsb smi');
    DmodG.QrUpdPID1.SQL.Add('LEFT JOIN '+TMeuDB+'.gragrux ggx ON ggx.Controle=smi.GraGruX');
    DmodG.QrUpdPID1.SQL.Add('LEFT JOIN '+TMeuDB+'.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1');
    DmodG.QrUpdPID1.SQL.Add('WHERE '(*NOT *)+'gg1.PrdGrupTip IN');
    DmodG.QrUpdPID1.SQL.Add('(');
    DmodG.QrUpdPID1.SQL.Add('  SELECT DISTINCT prdgruptip');
    DmodG.QrUpdPID1.SQL.Add('  FROM '+TMeuDB+'.stqbalcad');
    DmodG.QrUpdPID1.SQL.Add('  WHERE Abertura >= "' + DiaSeguinte + '"');
    DmodG.QrUpdPID1.SQL.Add(')');
    DmodG.QrUpdPID1.ExecSQL;
    //
    MyObjects.Informa(LaAviso, True, 'Tabela temporaria - Adicionando dados do arquivo ativo');
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('INSERT INTO stqmovitsc');
    DmodG.QrUpdPID1.SQL.Add('SELECT * FROM '+TMeuDB+'.stqmovitsa');
    DmodG.QrUpdPID1.ExecSQL;
    //
    MyObjects.Informa(LaAviso, True, 'Tipos de Grupos de Produtos - abrindo tabela');
    QrPGT_SCC.Close;
    QrPGT_SCC.SQL.Clear;
    case TipoPesq of
      1:
      begin
        QrPGT_SCC.SQL.Add('SELECT DISTINCT gg1.PrdGrupTip, smic.StqCenCad, ');
        QrPGT_SCC.SQL.Add('pgt.LstPrcFisc, ' + Empresa_Txt + ' Empresa, ' + '0' + ' EntiSitio');
        QrPGT_SCC.SQL.Add('FROM stqmovitsc smic');
        QrPGT_SCC.SQL.Add('LEFT JOIN '+TMeuDB+'.gragrux ggx ON ggx.Controle=smic.GraGruX');
        QrPGT_SCC.SQL.Add('LEFT JOIN '+TMeuDB+'.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1');
        QrPGT_SCC.SQL.Add('LEFT JOIN '+TMeuDB+'.prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip');
        QrPGT_SCC.SQL.Add('WHERE GraGruX <> 0');
        QrPGT_SCC.SQL.Add('AND Baixa = 1');
        QrPGT_SCC.SQL.Add('AND smic.Empresa=' + Empresa_Txt);
        if GG1 <> 0 then
          QrPGT_SCC.SQL.Add('AND gg1.Nivel1=' + FormatFloat('0', GraGru1Nivel1))
        else
        if GG2 <> 0 then
          QrPGT_SCC.SQL.Add('AND gg1.Nivel2=' + FormatFloat('0', GraGru2Nivel2))
        else
        if GG3 <> 0 then
          QrPGT_SCC.SQL.Add('AND gg1.Nivel3=' + FormatFloat('0', GraGru3Nivel3))
        else
        if PGT <> 0 then
          QrPGT_SCC.SQL.Add('AND pgt.Codigo=' + FormatFloat('0', PrdGrupTipCodigo));
        QrPGT_SCC.SQL.Add('ORDER BY gg1.PrdGrupTip, smic.StqCenCad');//, smic.GraGruX');
      end;
      2:
      begin
        QrPGT_SCC.SQL.Add('SELECT DISTINCT gg1.PrdGrupTip, smic.StqCenCad,');
        QrPGT_SCC.SQL.Add('pgt.LstPrcFisc, FLOOR(smic.Empresa + 0.1) Empresa, FLOOR(scc.EntiSitio + 0.1) EntiSitio');
        QrPGT_SCC.SQL.Add('FROM stqmovitsc smic');
        QrPGT_SCC.SQL.Add('LEFT JOIN '+TMeuDB+'.gragrux ggx ON ggx.Controle=smic.GraGruX');
        QrPGT_SCC.SQL.Add('LEFT JOIN '+TMeuDB+'.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1');
        QrPGT_SCC.SQL.Add('LEFT JOIN '+TMeuDB+'.prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip');
        QrPGT_SCC.SQL.Add('LEFT JOIN '+TMeuDB+'.stqcencad scc ON scc.Codigo=smic.StqCenCad');
        QrPGT_SCC.SQL.Add('WHERE (smic.Empresa=' + Empresa_Txt + ' OR scc.EntiSitio=' + Empresa_Txt + ')');
        QrPGT_SCC.SQL.Add('AND GraGruX <> 0');
        QrPGT_SCC.SQL.Add('AND Baixa = 1');
        if GG1 <> 0 then
          QrPGT_SCC.SQL.Add('AND gg1.Nivel1=' + FormatFloat('0', GraGru1Nivel1))
        else
        if GG2 <> 0 then
          QrPGT_SCC.SQL.Add('AND gg1.Nivel2=' + FormatFloat('0', GraGru2Nivel2))
        else
        if GG3 <> 0 then
          QrPGT_SCC.SQL.Add('AND gg1.Nivel3=' + FormatFloat('0', GraGru3Nivel3))
        else
        if PGT <> 0 then
          QrPGT_SCC.SQL.Add('AND pgt.Codigo=' + FormatFloat('0', PrdGrupTipCodigo));
        QrPGT_SCC.SQL.Add('ORDER BY gg1.PrdGrupTip, smic.StqCenCad');
      end;
    end;
    QrPGT_SCC.Open;
    //
    TextoA     := '';
    TextoB     := '';
    TextoC     := '';
    // Caso n�o ache nada!
    if QrPGT_SCC.RecordCount = 0 then
    begin
      QrSMIC_X.Close;
      QrSMIC_X.SQL.Clear;
      QrSMIC_X.SQL.Add('DELETE FROM ' + NomeTb_SMIC + ';');
      QrSMIC_X.ExecSQL;
    end;
    //
    while not QrPGT_SCC.Eof do
    begin
      CliInt_Txt    := FormatFloat('0', QrPGT_SCC.FieldByName('Empresa').AsInteger);
      EntiSitio_Txt := FormatFloat('0', QrPGT_SCC.FieldByName('EntiSitio').AsInteger);
      if TipoPesq = 2 then
      begin
        GraCusPrc_Txt := FormatFloat('0', QrPGT_SCC.FieldByName('LstPrcFisc').AsInteger)
      end;
      PrdGrupTip_Txt := FormatFloat('0', QrPGT_SCC.FieldByName('PrdGrupTip').AsInteger);
      StqCenCad_Txt  := FormatFloat('0', QrPGT_SCC.FieldByName('StqCenCad').AsInteger);
      TextoA := 'Tipo Prod.: ' + PrdGrupTip_Txt + '  -  ' +
                'Centro estq: ' + StqCenCad_Txt + '  -  ' +
                'Sitio: ' + EntiSitio_Txt + '  -  ' +
                'Empresa: ' + CliInt_Txt;
      TextoB := '';
      TextoC := '';
      MyObjects.Informa(LaAviso, False, TextoA + TextoB + TextoC);
      //
      QrAbertura2b.Close;
      QrAbertura2b.SQL.Clear;
      QrAbertura2b.SQL.Add('SELECT Abertura');
      QrAbertura2b.SQL.Add('FROM stqbalcad');
      QrAbertura2b.SQL.Add('WHERE PrdGrupTip=' + PrdGrupTip_Txt);
      QrAbertura2b.SQL.Add('AND StqCenCad=' + StqCenCad_Txt);
      QrAbertura2b.SQL.Add('AND Abertura >= "' + DiaSeguinte + '"');
      QrAbertura2b.SQL.Add('AND Empresa=' + Empresa_Txt);
      QrAbertura2b.SQL.Add('ORDER BY Abertura DESC');
      QrAbertura2b.SQL.Add('LIMIT 1');
      QrAbertura2b.Open;
      if QrAbertura2b.RecordCount > 0 then
      begin
        QrAbertura2a.Close;
        QrAbertura2a.SQL.Clear;
        QrAbertura2a.SQL.Add('SELECT Abertura, GrupoBal');
        QrAbertura2a.SQL.Add('FROM stqbalcad');
        QrAbertura2a.SQL.Add('WHERE PrdGrupTip=' + PrdGrupTip_Txt);
        QrAbertura2a.SQL.Add('AND StqCenCad=' + StqCenCad_Txt);
        QrAbertura2a.SQL.Add('AND Abertura < "' + DiaSeguinte + '"');
        QrAbertura2a.SQL.Add('AND Empresa=' + Empresa_Txt);
        QrAbertura2a.SQL.Add('ORDER BY Abertura DESC');
        QrAbertura2a.SQL.Add('LIMIT 1');
        QrAbertura2a.Open;
        //
        //if QrAbertura2aAbertura.Value > Int(TPDataFim2.Date) + 1 then
        if QrAbertura2a.FieldByName('Abertura').AsDateTime > Int(DataFim) + 1 then
          GrupoBal_Txt := FormatFloat('0', QrAbertura2a.FieldByName('GrupoBal').AsInteger)
        else
          GrupoBal_Txt := '0';
      end else GrupoBal_Txt := '0';
      //
      QrSMIC_X.Close;
      QrSMIC_X.SQL.Clear;
      if QrPGT_SCC.RecNo = 1 then
      begin
        QrSMIC_X.SQL.Add('DROP TABLE IF EXISTS ' + NomeTb_SMIC + ';');
        QrSMIC_X.SQL.Add('CREATE TABLE ' + NomeTb_SMIC);
      end else begin
        QrSMIC_X.SQL.Add('INSERT INTO ' + NomeTb_SMIC);
      end;
      QrSMIC_X.SQL.Add('SELECT smic.IDCtrl My_Idx, gg1.Nivel1, ');
      QrSMIC_X.SQL.Add('gg1.Nome NO_PRD, gg1.PrdGrupTip, gg1.UnidMed, ');
      QrSMIC_X.SQL.Add('gg1.NCM, pgt.Nome NO_PGT, ');
      QrSMIC_X.SQL.Add('med.Sigla SIGLA, gti.PrintTam, gti.Nome NO_TAM, ');
      QrSMIC_X.SQL.Add('gcc.PrintCor, gcc.Nome NO_COR, ggc.GraCorCad, ');
      QrSMIC_X.SQL.Add('ggx.GraGruC, ggx.GraGru1, ggx.GraTamI, ');
      QrSMIC_X.SQL.Add('scc.SitProd, scc.EntiSitio,');
      QrSMIC_X.SQL.Add('smic.GraGruX, smic.Empresa, smic.StqCenCad,');
      QrSMIC_X.SQL.Add('SUM(smic.Qtde * smic.Baixa) QTDE, SUM(smic.Pecas) PECAS,');
      QrSMIC_X.SQL.Add('SUM(smic.Peso) PESO, SUM(smic.AreaM2) AREAM2,');
      QrSMIC_X.SQL.Add('SUM(smic.AreaP2) AREAP2,');
      if UsaTabePrcCab then
      begin
        // Por tabela de pre�o de GraGru1 (TPC_GCP_ID = 2) ou de GraGruX (TPC_GCP_ID = 3)
        QrSMIC_X.SQL.Add('TRUNCATE(IF(tpi.Preco IS NULL, 2, 3), 0) TPC_GCP_ID,');
        QrSMIC_X.SQL.Add('IF(tpi.Preco IS NULL, tpc.Codigo, tpi.TabePrcCab) TPC_GCP_TAB,');
        QrSMIC_X.SQL.Add('IF(tpi.Preco IS NULL, TRUNCATE(tpc.Preco, 6), TRUNCATE(tpi.Preco, 6)) PrcCusUni,');
        QrSMIC_X.SQL.Add('IF(tpi.Preco IS NULL, TRUNCATE(SUM(smic.Qtde*smic.Baixa) * TRUNCATE(tpc.Preco, 6), 2),');
        QrSMIC_X.SQL.Add('TRUNCATE(SUM(smic.Qtde*smic.Baixa) * TRUNCATE(tpi.Preco, 6), 2)) ValorTot,');
        QrSMIC_X.SQL.Add('');
      end else
      begin
        // Por lista de Pre�o (TPC_GCP_ID = 1)
        QrSMIC_X.SQL.Add('TRUNCATE(1,0) TPC_GCP_ID, ggv.GraCusPrc TPC_GCP_TAB, ');
        QrSMIC_X.SQL.Add('TRUNCATE(ggv.CustoPreco, 6) PrcCusUni, ');
        QrSMIC_X.SQL.Add('TRUNCATE(SUM(smic.Qtde*smic.Baixa) * ');
        QrSMIC_X.SQL.Add('TRUNCATE(ggv.CustoPreco, 6), 2) ValorTot, ');
      end;
      QrSMIC_X.SQL.Add('1 Exportado ');
      //QrSMIC_X.SQL.Add('1 Exportado, 0 Mot_SitPrd, 0 Mot_Sitio, 0 MotQtdVal, 0 MotProduto');
      QrSMIC_X.SQL.Add('FROM stqmovitsc smic');
      QrSMIC_X.SQL.Add('LEFT JOIN '+TMeuDB+'.gragrux    ggx ON ggx.Controle=smic.GraGruX');
      QrSMIC_X.SQL.Add('LEFT JOIN '+TMeuDB+'.gragruc    ggc ON ggc.Controle=ggx.GraGruC');
      QrSMIC_X.SQL.Add('LEFT JOIN '+TMeuDB+'.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad');
      QrSMIC_X.SQL.Add('LEFT JOIN '+TMeuDB+'.gratamits  gti ON gti.Controle=ggx.GraTamI');
      QrSMIC_X.SQL.Add('LEFT JOIN '+TMeuDB+'.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1');
      QrSMIC_X.SQL.Add('LEFT JOIN '+TMeuDB+'.prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip');
      QrSMIC_X.SQL.Add('LEFT JOIN '+TMeuDB+'.unidmed    med ON med.Codigo=gg1.UnidMed');
      if UsaTabePrcCab then
      begin
        QrSMIC_X.SQL.Add('LEFT JOIN '+TMeuDB+'.tabeprcgrg  tpc ON tpc.Nivel1=ggx.GraGru1');
        QrSMIC_X.SQL.Add('          AND tpc.Codigo=' + TabePrcCab_Txt);
        QrSMIC_X.SQL.Add('LEFT JOIN '+TMeuDB+'.tabeprcgri  tpi ON tpi.GraGruX=smic.GraGruX');
        QrSMIC_X.SQL.Add('          AND tpi.TabePrcCab=' + TabePrcCab_Txt);
      end else begin
        QrSMIC_X.SQL.Add('LEFT JOIN '+TMeuDB+'.gragruval  ggv ON ggv.GraGruX=smic.GraGruX');
        // Evitar n�o encontrar registros sem pre�o!
        QrSMIC_X.SQL.Add('          AND ggv.GraCusPrc=' + GraCusPrc_Txt);
      end;
      QrSMIC_X.SQL.Add('LEFT JOIN '+TMeuDB+'.stqcencad scc ON scc.Codigo=smic.StqCenCad');
      //
      QrSMIC_X.SQL.Add('WHERE smic.Baixa> 0');
      //QrSMIC_X.SQL.Add('AND smic.GrupoBal=' + GrupoBal_Txt);
      QrSMIC_X.SQL.Add('AND smic.DataHora < "' + DiaSeguinte  + '"');
      QrSMIC_X.SQL.Add('AND smic.StqCenCad=' + StqCenCad_Txt);
      QrSMIC_X.SQL.Add('AND gg1.PrdGrupTip=' + PrdGrupTip_Txt);
      //
      QrSMIC_X.SQL.Add('AND smic.EMPRESA=' + CliInt_Txt);
      if TipoPesq = 2 then
        QrSMIC_X.SQL.Add('AND scc.EntiSitio=' + EntiSitio_Txt);
      //
      //  Filtros de pesquisa
      if GG1 <> 0 then
        QrSMIC_X.SQL.Add('AND gg1.Nivel1=' + FormatFloat('0', GraGru1Nivel1))
      else
      if GG2 <> 0 then
        QrSMIC_X.SQL.Add('AND gg1.Nivel2=' + FormatFloat('0', GraGru2Nivel2))
      else
      if GG3 <> 0 then
        QrSMIC_X.SQL.Add('AND gg1.Nivel3=' + FormatFloat('0', GraGru3Nivel3))
      else
      if PGT <> 0 then
        QrSMIC_X.SQL.Add('AND pgt.Codigo=' + FormatFloat('0', PrdGrupTipCodigo));
      //
      //
      QrSMIC_X.SQL.Add('GROUP BY smic.GraGruX');
      //dmkPF.LeMeuTexto(QrSMIC_X.SQL.Text);
      QrSMIC_X.ExecSQL;
      //
      QrPGT_SCC.Next;
    end;
    //
    MyObjects.Informa(LaAviso, False, 'Abrindo dados gerados');
    QrSMIC_X.Close;
    QrSMIC_X.SQL.Clear;
    QrSMIC_X.SQL.Add('SELECT sm2.*, scc.SitProd,');
    QrSMIC_X.SQL.Add('ens.CNPJ ENS_CNPJ, ens.IE ENS_IE, uf1.Nome ENS_NO_UF,');
    QrSMIC_X.SQL.Add('emp.CNPJ EMP_CNPJ, emp.IE EMP_IE, uf2.Nome EMP_NO_UF');
    QrSMIC_X.SQL.Add('FROM ' + NomeTb_SMIC + ' sm2');
    QrSMIC_X.SQL.Add('LEFT JOIN '+TMeuDB+'.stqcencad scc ON scc.Codigo=sm2.StqCenCad');
    QrSMIC_X.SQL.Add('LEFT JOIN '+TMeuDB+'.entidades ens ON ens.Codigo=scc.EntiSitio');
    QrSMIC_X.SQL.Add('LEFT JOIN '+TMeuDB+'.entidades emp ON emp.Codigo=sm2.Empresa');
    QrSMIC_X.SQL.Add('LEFT JOIN '+TMeuDB+'.ufs uf1 ON uf1.Codigo=ens.EUF');
    QrSMIC_X.SQL.Add('LEFT JOIN '+TMeuDB+'.ufs uf2 ON uf2.Codigo=emp.EUF');
    if SoPositivos then
      QrSMIC_X.SQL.Add('WHERE sm2.QTDE >= 0.001');
    QrSMIC_X.SQL.Add('ORDER BY NO_PGT, NO_PRD');
    QrSMIC_X.Open;
    //
    TXTSQL_Part1 := 'SELECT SUM(smic.Qtde) QtdSum,' + #13#10 +
    'SUM(IF(smic.Qtde>0, smic.Qtde * smic.Baixa, 0)) QtdPos,' + #13#10 +
    'SUM(IF(smic.Qtde<0, smic.Qtde * smic.Baixa, 0)) QtdNeg' + #13#10 +
    'FROM stqmovitsc smic' + #13#10 +
    'LEFT JOIN '+TMeuDB+'.gragrux ggx ON ggx.Controle=smic.GraGruX' + #13#10 +
    'LEFT JOIN '+TMeuDB+'.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1' + #13#10 +
    'LEFT JOIN '+TMeuDB+'.prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip' + #13#10 +
    'LEFT JOIN '+TMeuDB+'.stqcencad scc ON scc.Codigo=smic.StqCenCad';
    //TXTSQL_Part := 'WHERE Tipo > 0' + #13#10 +
    TXTSQL_Part2 := dmkPF.SQL_Periodo('AND smic.DataHora ', DataIni, DataFim, True, True) + #13#10 +
    //
    'AND smic.EMPRESA=' + CliInt_Txt + #13#10;

{
    if TipoPesq = 2 then
      TXTSQL_Part2 := TXTSQL_Part2 + 'AND scc.EntiSitio=' + EntiSitio_Txt + #13#10;
}
    //
    //  Filtros de pesquisa
    if GG1 <> 0 then
      TXTSQL_Part2 := TXTSQL_Part2 + 'AND gg1.Nivel1=' + FormatFloat('0', GraGru1Nivel1) + #13#10
    else
    if GG2 <> 0 then
      TXTSQL_Part2 := TXTSQL_Part2 + 'AND gg1.Nivel2=' + FormatFloat('0', GraGru2Nivel2) + #13#10
    else
    if GG3 <> 0 then
      TXTSQL_Part2 := TXTSQL_Part2 + 'AND gg1.Nivel3=' + FormatFloat('0', GraGru3Nivel3) + #13#10
    else
    if PGT <> 0 then
      TXTSQL_Part2 := TXTSQL_Part2 + 'AND pgt.Codigo=' + FormatFloat('0', PrdGrupTipCodigo) + #13#10;
    //
    TXTSQL_Part2 := TXTSQL_Part2 + 'AND GraGruX=:P0';
    //


    MyObjects.Informa(LaAviso, False, '...');
    //
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TDmod.LocCod(Atual, Codigo: Integer);
begin
  FmPrincipal.LocCod(Atual, Codigo);
end;

procedure TDmod.AlteraSMIA(Qry: TmySQLQuery);
var
  Tipo, IDCtrl, GraGruX, StqCenCad: Integer;
  Qtde, Pecas, AreaM2, AreaP2, Peso: Double;
begin
  Tipo := Qry.FieldByName('Tipo').AsInteger;
  IDCtrl    := Qry.FieldByName('IDCtrl').AsInteger;
  GraGruX   := Qry.FieldByName('GraGruX').AsInteger;
  StqCenCad := Qry.FieldByName('StqCenCad').AsInteger;
  case Tipo of
    VAR_FATID_0101:
    begin
      Qtde      := Qry.FieldByName('Qtde').AsFloat;
      Pecas     := Qry.FieldByName('Pecas').AsFloat;
      AreaM2    := Qry.FieldByName('AreaM2').AsFloat;
      AreaP2    := Qry.FieldByName('AreaP2').AsFloat;
      Peso      := Qry.FieldByName('Peso').AsFloat;
    end;
    VAR_FATID_0104:
    begin
      Qtde      := - Qry.FieldByName('Qtde').AsFloat;
      Pecas     := - Qry.FieldByName('Pecas').AsFloat;
      AreaM2    := - Qry.FieldByName('AreaM2').AsFloat;
      AreaP2    := - Qry.FieldByName('AreaP2').AsFloat;
      Peso      := - Qry.FieldByName('Peso').AsFloat;
    end;
    else begin
      Geral.MensagemBox('Altera��o cancelada!' + #13#10 +
      'Tipo de lan�amento n�o implementado: ' + FormatFloat('0', Tipo) + #13#10
      + 'AVISE A DERMATEK!', 'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
  end;
  if DBCheck.CriaFm(TFmSMI_Edita, FmSMI_Edita, afmoNegarComAviso) then
  begin
    FmSMI_Edita.FNegativos := Tipo = VAR_FATID_0104;
    //
    FmSMI_Edita.QrSMIA.Close;
    FmSMI_Edita.QrSMIA.Params[0].AsInteger := IDCtrl;
    FmSMI_Edita.QrSMIA.Open;
    //
    FmSMI_Edita.EdGraGruX.ValueVariant   := GraGruX;
    FmSMI_Edita.CBGraGruX.KeyValue       := GraGruX;
    //
    FmSMI_Edita.EdStqCenCad.ValueVariant := StqCenCad;
    FmSMI_Edita.CBStqCenCad.KeyValue     := StqCenCad;

    FmSMI_Edita.EdQtde.ValueVariant      := Qtde;
    FmSMI_Edita.EdPecas.ValueVariant     := Pecas;
    FmSMI_Edita.EdAreaM2.ValueVariant    := AreaM2;
    FmSMI_Edita.EdAreaP2.ValueVariant    := AreaP2;
    FmSMI_Edita.EdPeso.ValueVariant      := Peso;
    //
    FmSMI_Edita.ShowModal;
    FmSMI_Edita.Destroy;
  end;
end;

function TDmod.BuscaProximoMovix: Integer;
begin
  QrSel.Close;
  QrSel.Open;
  Result := QrSelMovix.Value;
end;

procedure TDmod.QrControleAfterOpen(DataSet: TDataSet);
begin
  FFmtPrc    := Dmod.QrControleCasasProd.Value;
  FStrFmtPrc := dmkPF.StrFmt_Double(Dmod.QrControleCasasProd.Value);
  FStrFmtCus := dmkPF.StrFmt_Double(4);
  FStrFmtQtd := dmkPF.StrFmt_Double(3);
end;

procedure TDmod.QrDonoCalcFields(DataSet: TDataSet);
var
  Natal: TDateTime;
begin
  QrDonoTE1_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrDonoTe1.Value);
  QrDonoTE2_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrDonoTe2.Value);
  QrDonoFAX_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrDonoFax.Value);
  QrDonoCEL_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrDonoECel.Value);
  QrDonoCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrDonoCNPJ_CPF.Value);
  //
  QrDonoNUMERO_TXT.Value :=
    MLAGeral.FormataNumeroDeRua(QrDonoRUA.Value, Trunc(QrDonoNumero.Value + 0.1), False);
  QrDonoE_LNR.Value := QrDonoNOMELOGRAD.Value;
  if Trim(QrDonoE_LNR.Value) <> '' then QrDonoE_LNR.Value :=
    QrDonoE_LNR.Value + ' ';
  QrDonoE_LNR.Value := QrDonoE_LNR.Value + QrDonoRua.Value;
  if Trim(QrDonoRua.Value) <> '' then QrDonoE_LNR.Value :=
    QrDonoE_LNR.Value + ', ' + QrDonoNUMERO_TXT.Value;
  if Trim(QrDonoCompl.Value) <>  '' then QrDonoE_LNR.Value :=
    QrDonoE_LNR.Value + ' ' + QrDonoCompl.Value;
  if Trim(QrDonoBairro.Value) <>  '' then QrDonoE_LNR.Value :=
    QrDonoE_LNR.Value + ' - ' + QrDonoBairro.Value;
  //
  QrDonoE_LN2.Value := '';
  if Trim(QrDonoCidade.Value) <>  '' then QrDonoE_LN2.Value :=
    QrDonoE_LN2.Value + ' - '+QrDonoCIDADE.Value;
  QrDonoE_LN2.Value := QrDonoE_LN2.Value + ' - '+QrDonoNOMEUF.Value;
  if QrDonoCEP.Value > 0 then QrDonoE_LN2.Value :=
    QrDonoE_LN2.Value + '  CEP ' +Geral.FormataCEP_NT(QrDonoCEP.Value);
  //
  QrDonoE_CUC.Value := QrDonoE_LNR.Value + QrDonoE_LN2.Value;
  //
  QrDonoECEP_TXT.Value :=Geral.FormataCEP_NT(QrDonoCEP.Value);
  //
  if QrDonoTipo.Value = 0 then Natal := QrDonoENatal.Value
  else Natal := QrDonoPNatal.Value;
  if Natal < 2 then QrDonoNATAL_TXT.Value := ''
  else QrDonoNATAL_TXT.Value := FormatDateTime(VAR_FORMATDATE2, Natal);
  //
  if Trim(QrDonoTE1.Value) <> '' then
    QrDonoFONES.Value := QrDonoTE1_TXT.Value;
  if Trim(QrDonoTe2.Value) <> '' then
    QrDonoFONES.Value := QrDonoFONES.Value + ' - ' + QrDonoTE2_TXT.Value;
end;

procedure TDmod.InsereRegistrosExtraBalanceteMensal_01(QrInd01: TmySQLQuery;
DataIDate, DataFDate: TDateTime; DataITrue, DataFTrue: Boolean;
CliInt: Integer; Campos: String; DescrSubstit: Boolean);
begin
  Geral.MensagemBox('Este balancete n�o possui dados industriais ' +
  'adicionais neste aplicativo!', 'Aviso', MB_OK+MB_ICONWARNING);
end;

end.

