object FmFatPedGra: TFmFatPedGra
  Left = 339
  Top = 185
  Caption = 'FAT-PEDID-004 :: Itens de Faturamento de Pedido por Grade'
  ClientHeight = 494
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 446
    Width = 784
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object Panel2: TPanel
      Left = 672
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object PB1: TProgressBar
      Left = 120
      Top = 18
      Width = 541
      Height = 17
      TabOrder = 2
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    Caption = 'Itens de Faturamento de Pedido por Grade'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 782
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 398
    Align = alClient
    TabOrder = 0
    object PnSeleciona: TPanel
      Left = 1
      Top = 1
      Width = 782
      Height = 48
      Align = alTop
      TabOrder = 0
      object Label1: TLabel
        Left = 12
        Top = 4
        Width = 40
        Height = 13
        Caption = 'Produto:'
      end
      object EdGraGru1: TdmkEditCB
        Left = 12
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnChange = EdGraGru1Change
        OnExit = EdGraGru1Exit
        DBLookupComboBox = CBGraGru1
        IgnoraDBLookupComboBox = False
      end
      object CBGraGru1: TdmkDBLookupComboBox
        Left = 72
        Top = 20
        Width = 549
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsGraGru1
        TabOrder = 1
        dmkEditCB = EdGraGru1
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object PageControl1: TPageControl
      Left = 1
      Top = 49
      Width = 782
      Height = 348
      ActivePage = TabSheet9
      Align = alClient
      TabOrder = 1
      TabPosition = tpBottom
      object TabSheet5: TTabSheet
        Caption = ' Sele'#231#227'o '
        ImageIndex = 2
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object GradeQ: TStringGrid
          Left = 0
          Top = 0
          Width = 774
          Height = 305
          Align = alClient
          ColCount = 2
          DefaultColWidth = 65
          DefaultRowHeight = 18
          FixedCols = 0
          RowCount = 2
          FixedRows = 0
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing, goTabs]
          ParentFont = False
          TabOrder = 0
          OnDblClick = GradeQDblClick
          OnDrawCell = GradeQDrawCell
          OnKeyDown = GradeQKeyDown
        end
        object StaticText1: TStaticText
          Left = 0
          Top = 305
          Width = 629
          Height = 17
          Align = alBottom
          Alignment = taCenter
          BorderStyle = sbsSunken
          Caption = 
            'D'#234' um duplo clique na c'#233'lula, coluna ou linha correspondente par' +
            'a incluir / alterar a quantidade de etiquetas.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          Visible = False
        end
      end
      object TabSheet6: TTabSheet
        Caption = ' C'#243'digos '
        ImageIndex = 3
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object GradeC: TStringGrid
          Left = 0
          Top = 0
          Width = 774
          Height = 305
          Align = alClient
          ColCount = 2
          DefaultColWidth = 65
          DefaultRowHeight = 18
          RowCount = 2
          TabOrder = 0
          OnDrawCell = GradeCDrawCell
          RowHeights = (
            18
            19)
        end
        object StaticText6: TStaticText
          Left = 0
          Top = 305
          Width = 502
          Height = 17
          Align = alBottom
          Alignment = taCenter
          BorderStyle = sbsSunken
          Caption = 
            'Para gerar o c'#243'digo, clique na c'#233'lula, coluna ou linha correspon' +
            'dente na guia "Ativos".'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          Visible = False
        end
      end
      object TabSheet3: TTabSheet
        Caption = ' Ativos '
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object GradeA: TStringGrid
          Left = 0
          Top = 0
          Width = 774
          Height = 305
          Align = alClient
          ColCount = 2
          DefaultColWidth = 65
          DefaultRowHeight = 18
          FixedCols = 0
          RowCount = 2
          FixedRows = 0
          TabOrder = 0
          OnDrawCell = GradeADrawCell
          RowHeights = (
            18
            18)
        end
        object StaticText2: TStaticText
          Left = 0
          Top = 305
          Width = 475
          Height = 17
          Align = alBottom
          Alignment = taCenter
          BorderStyle = sbsSunken
          Caption = 
            'Clique na c'#233'lula, coluna ou linha correspondente para ativar / d' +
            'esativar o produto.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          Visible = False
        end
      end
      object TabSheet9: TTabSheet
        Caption = ' X '
        ImageIndex = 6
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object GradeX: TStringGrid
          Left = 0
          Top = 0
          Width = 774
          Height = 322
          Align = alClient
          ColCount = 2
          DefaultColWidth = 65
          DefaultRowHeight = 18
          FixedCols = 0
          RowCount = 2
          FixedRows = 0
          TabOrder = 0
          RowHeights = (
            18
            18)
        end
      end
    end
  end
  object QrGraGru1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.Codusu, gg1.Nivel3, gg1.Nivel2, gg1.Nivel1, '
      'gg1.Nome, gg1.PrdGrupTip, gg1.GraTamCad,'
      'gtc.Nome NOMEGRATAMCAD, gtc.CodUsu CODUSUGRATAMCAD,'
      'gg1.CST_A, gg1.CST_B, gg1.UnidMed, gg1.NCM, gg1.Peso,'
      'unm.Sigla SIGLAUNIDMED, unm.CodUsu CODUSUUNIDMED, '
      'unm.Nome NOMEUNIDMED '
      'FROM gragru1 gg1'
      'LEFT JOIN gratamcad gtc ON gtc.Codigo=gg1.GraTamCad'
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'WHERE pgt.TipPrd=1 '
      'ORDER BY gg1.Nome')
    Left = 8
    Top = 8
    object QrGraGru1Codusu: TIntegerField
      FieldName = 'Codusu'
      Required = True
    end
    object QrGraGru1Nivel3: TIntegerField
      FieldName = 'Nivel3'
      Required = True
    end
    object QrGraGru1Nivel2: TIntegerField
      FieldName = 'Nivel2'
      Required = True
    end
    object QrGraGru1Nivel1: TIntegerField
      FieldName = 'Nivel1'
      Required = True
    end
    object QrGraGru1Nome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 30
    end
    object QrGraGru1PrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
      Required = True
    end
    object QrGraGru1GraTamCad: TIntegerField
      FieldName = 'GraTamCad'
      Required = True
    end
    object QrGraGru1NOMEGRATAMCAD: TWideStringField
      FieldName = 'NOMEGRATAMCAD'
      Size = 50
    end
    object QrGraGru1CODUSUGRATAMCAD: TIntegerField
      FieldName = 'CODUSUGRATAMCAD'
      Required = True
    end
    object QrGraGru1CST_A: TSmallintField
      FieldName = 'CST_A'
      Required = True
    end
    object QrGraGru1CST_B: TSmallintField
      FieldName = 'CST_B'
      Required = True
    end
    object QrGraGru1UnidMed: TIntegerField
      FieldName = 'UnidMed'
      Required = True
    end
    object QrGraGru1NCM: TWideStringField
      FieldName = 'NCM'
      Required = True
      Size = 10
    end
    object QrGraGru1Peso: TFloatField
      FieldName = 'Peso'
      Required = True
    end
    object QrGraGru1SIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 3
    end
    object QrGraGru1CODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
      Required = True
    end
    object QrGraGru1NOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsGraGru1: TDataSource
    DataSet = QrGraGru1
    Left = 36
    Top = 8
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MAX(Sequencia) Ultimo'
      'FROM etqgeraits'
      'WHERE GraGruX = :P0'
      '')
    Left = 64
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocUltimo: TIntegerField
      FieldName = 'Ultimo'
      Required = True
    end
  end
end
