unit PlanCutImpPrd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DB, DBGrids,
  dmkDBGrid, DBCtrls, mySQLDbTables, Menus, ComCtrls, UnDmkProcFunc;

type
  THackDBGrid = class(TDBGrid);
  TFmPlanCutImpPrd = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    PMPesq: TPopupMenu;
    Ativa1: TMenuItem;
    Desativa1: TMenuItem;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel3: TPanel;
    Label1: TLabel;
    EdPesq: TEdit;
    DBGrid1: TdmkDBGrid;
    Panel4: TPanel;
    DBGrid2: TdmkDBGrid;
    RGSelecao: TRadioGroup;
    BtTodos: TBitBtn;
    BtPesq: TBitBtn;
    BtNenhum: TBitBtn;
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure FormStartDock(Sender: TObject; var DragObject: TDragDockObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure EdPesqChange(Sender: TObject);
    procedure FormUnDock(Sender: TObject; Client: TControl;
      NewTarget: TWinControl; var Allow: Boolean);
    procedure BtNenhumClick(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtPesqClick(Sender: TObject);
    procedure Ativa1Click(Sender: TObject);
    procedure Desativa1Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure RGSelecaoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure Pesquisar();
    procedure AtivaItens(Status: Integer);
    procedure AtivaSelecionados(Status: Integer);
    procedure HabilitaItemUnico();
  public
    { Public declarations }
    class function CreateDockForm(const aColor: TColor): TCustomForm;
    //class procedure FiltaAtivos();
    procedure LimpaPesquisa();
  end;

var
  FmPlanCutImpPrd: TFmPlanCutImpPrd;

implementation

uses UnMyObjects, Module, UMySQLModule, TabePrcCab, PlanCutImp, ModPlanCut, MyGlyfs;

{$R *.DFM}

///// PROCEDURES E FUNCTIONS DO DOCKFORM

procedure TFmPlanCutImpPrd.Ativa1Click(Sender: TObject);
begin
  AtivaSelecionados(1);
end;

procedure TFmPlanCutImpPrd.AtivaItens(Status: Integer);
begin
  Screen.Cursor := crHourGlass;
  DmPlanCut.QrPCIPrdIts.Close;
  DmPlanCut.QrPCIPrdIts.SQL.Clear;
  DmPlanCut.QrPCIPrdIts.SQL.Add('UPDATE pciprd SET Ativo=' + dmkPF.FFP(Status, 0) + ';');
  DmPlanCut.QrPCIPrdIts.SQL.Add('SELECT * FROM pciprd;');
  DmPlanCut.QrPCIPrdIts.Open;
  //
  HabilitaItemUnico();
  Screen.Cursor := crDefault;
end;

procedure TFmPlanCutImpPrd.AtivaSelecionados(Status: Integer);
begin
  Screen.Cursor := crHourGlass;
  DmPlanCut.QrPCIPrdIts.First;
  while not DmPlanCut.QrPCIPrdIts.Eof do
  begin
    if DmPlanCut.QrPCIPrdItsAtivo.Value <> Status then
    begin
      DmPlanCut.QrPCIPrdIts.Edit;
      DmPlanCut.QrPCIPrdItsAtivo.Value := Status;
      DmPlanCut.QrPCIPrdIts.Post;
    end;
    DmPlanCut.QrPCIPrdIts.Next;
  end;
  HabilitaItemUnico();
  Screen.Cursor := crDefault;
end;

procedure TFmPlanCutImpPrd.BtPesqClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPesq, BtPesq);
end;

procedure TFmPlanCutImpPrd.BtNenhumClick(Sender: TObject);
begin
  AtivaItens(0);
end;

procedure TFmPlanCutImpPrd.BtTodosClick(Sender: TObject);
begin
  AtivaItens(1);
end;

class function TFmPlanCutImpPrd.CreateDockForm(const aColor: TColor): TCustomForm;
begin
  result := TFmPlanCutImpPrd.Create(Application);
  //result.Color := aColor;
  //result.Caption := ColorToString(aColor);
  FmMyGlyfs.ConfiguraFormDock(result);
  result.Show;
end;

procedure TFmPlanCutImpPrd.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  DmPlanCut.QrPCIPrdIts.Close;
  DmPlanCut.QrPCIPrdIts.SQL.Clear;
  DmPlanCut.QrPCIPrdIts.SQL.Add('DROP TABLE PCIPrd; ');
  DmPlanCut.QrPCIPrdIts.ExecSQL;
  //
  ManualFloat(Rect(0, 0, 0, 0));
  Action := caFree;
end;

procedure TFmPlanCutImpPrd.FormShow(Sender: TObject);
begin
  FmPlanCutImp.FechaPesquisa();
end;

procedure TFmPlanCutImpPrd.FormStartDock(Sender: TObject;
  var DragObject: TDragDockObject);
begin
  DragObject := TDragDockObjectEx.Create(Self);
  DragObject.Brush.Color := clAqua; // this will display a red outline
end;


procedure TFmPlanCutImpPrd.FormUnDock(Sender: TObject; Client: TControl;
  NewTarget: TWinControl; var Allow: Boolean);
begin
end;

///// FIM DAS PROCEDURES E FUNCTIONS DO DOCKFORM

procedure TFmPlanCutImpPrd.PageControl1Change(Sender: TObject);
begin
  BtPesq.Enabled := PageControl1.ActivePageIndex = 0;
  case PageControl1.ActivePageIndex of
    0: Pesquisar();
    1: RGSelecaoClick(Self);
  end;
end;

procedure TFmPlanCutImpPrd.Pesquisar();
begin
  Screen.Cursor := crHourGlass;
  //
  DmPlanCut.QrPCIPrdIts.Filter   := 'Nome LIKE "%' + EdPesq.Text + '%"';
  DmPlanCut.QrPCIPrdIts.Filtered := True;
  //
  Screen.Cursor := crDefault;
end;

procedure TFmPlanCutImpPrd.RGSelecaoClick(Sender: TObject);
begin
  DmPlanCut.QrPCIPrdIts.Filtered := False;
  case RGSelecao.ItemIndex of
    0: DmPlanCut.QrPCIPrdIts.Filter := 'Ativo=0';
    1: DmPlanCut.QrPCIPrdIts.Filter := 'Ativo=1';
    2: DmPlanCut.QrPCIPrdIts.Filter := '';
  end;
  if RGSelecao.ItemIndex < 2 then
    DmPlanCut.QrPCIPrdIts.Filtered := True;
end;

procedure TFmPlanCutImpPrd.DBGrid1CellClick(Column: TColumn);
var
  Status: Integer;
begin
  if Column.FieldName = 'Ativo' then
  begin
    if DmPlanCut.QrPCIPrdIts.FieldByName('Ativo').Value = 1 then
      Status := 0
    else
      Status := 1;
    //
    DmPlanCut.QrPCIPrdIts.Edit;
    DmPlanCut.QrPCIPrdIts.FieldByName('Ativo').Value := Status;
    DmPlanCut.QrPCIPrdIts.Post;
  end;
  HabilitaItemUnico();
end;

procedure TFmPlanCutImpPrd.HabilitaItemUnico();
var
  Habilita: Boolean;
begin
  DmPlanCut.QrPCIPrdAti.Close;
  DmPlanCut.QrPCIPrdAti.Open;
  Habilita :=  DmPlanCut.QrPCIPrdAtiItens.Value = 0;
  FmPlanCutImp.LaPCIPrd.Enabled := Habilita;
  FmPlanCutImp.EdPCIPrd.Enabled := Habilita;
  FmPlanCutImp.CBPCIPrd.Enabled := Habilita;
  if not Habilita then
  begin
    FmPlanCutImp.EdPCIPrd.ValueVariant := 0;
    FmPlanCutImp.CBPCIPrd.KeyValue     := 0;
  end;
end;

procedure TFmPlanCutImpPrd.LimpaPesquisa;
var
  K, I: Integer;
  //Form: TForm;
begin
  for K := 0 to Screen.FormCount - 1 do
  begin
    if Screen.Forms[K] is TFmPlanCutImpPrd then
    begin
      for I := 0 to Screen.Forms[K].ComponentCount -1 do
      if Screen.Forms[K].Components[I] is TEdit then
        TEdit(Screen.Forms[K].Components[I]).Text := '';
    end;
  end;
  AtivaItens(0);
end;

procedure TFmPlanCutImpPrd.Desativa1Click(Sender: TObject);
begin
  AtivaSelecionados(0);
end;

procedure TFmPlanCutImpPrd.EdPesqChange(Sender: TObject);
begin
  Pesquisar();
end;

procedure TFmPlanCutImpPrd.FormActivate(Sender: TObject);
begin
  //MyObjects.CorIniComponente();
end;

procedure TFmPlanCutImpPrd.FormCreate(Sender: TObject);
const
  Txt1 = 'INSERT INTO pciprd (Codigo,CodUsu,Nome,Ativo) VALUES(';
var
  Txt2: String;
begin
  DmPlanCut.QrPCIPrdIts.Close;
  DmPlanCut.QrPCIPrdIts.SQL.Clear;
  DmPlanCut.QrPCIPrdIts.SQL.Add('DROP TABLE PCIPrd; ');
  DmPlanCut.QrPCIPrdIts.SQL.Add('CREATE TABLE PCIPrd (');
  DmPlanCut.QrPCIPrdIts.SQL.Add('  Codigo  integer      ,');
  DmPlanCut.QrPCIPrdIts.SQL.Add('  CodUsu  integer      ,');
  DmPlanCut.QrPCIPrdIts.SQL.Add('  Nome    varchar(50)  ,');
  DmPlanCut.QrPCIPrdIts.SQL.Add('  Ativo   smallint      ');
  DmPlanCut.QrPCIPrdIts.SQL.Add(');');
  //
  DmPlanCut.QrPCIPrdCad.Close;
  DmPlanCut.QrPCIPrdCad.Open;
  while not DmPlanCut.QrPCIPrdCad.Eof do
  begin
    Txt2 :=
      dmkPF.FFP(DmPlanCut.QrPCIPrdCadNivel1.Value, 0) + ',' +
      dmkPF.FFP(DmPlanCut.QrPCIPrdCadCodUsu.Value, 0) + ',' +
      '"' + DmPlanCut.QrPCIPrdCadNome.Value + '",' +
      '0);';
  //for i  := 1 to 10 do
    DmPlanCut.QrPCIPrdIts.SQL.Add(Txt1 + Txt2);
    DmPlanCut.QrPCIPrdCad.Next;
  end;
  //
  DmPlanCut.QrPCIPrdIts.SQL.Add('SELECT * FROM pciprd;');
  DmPlanCut.QrPCIPrdIts.Open;
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmPlanCutImpPrd.FormDeactivate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 1;
  RGSelecao.ItemIndex := 1;
  //
  if FindWindow('TFmPlanCutImp', nil) > 0 then
    FmPlanCutImp.AtivaBtConfirma();
end;

end.

