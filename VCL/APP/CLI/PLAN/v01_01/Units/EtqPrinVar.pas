unit EtqPrinVar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DB, ABSMain, DBGrids,
  dmkDBGrid, DBCtrls, mySQLDbTables, dmkCheckGroup, dmkEdit, dmkGeral,
  UnDmkProcFunc;

type
  THackDBGrid = class(TDBGrid);
  TFmEtqPrinVar = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Panel3: TPanel;
    Label1: TLabel;
    EdNome: TEdit;
    QrPesq: TmySQLQuery;
    QrPesqNivel1: TIntegerField;
    QrPesqCodUsu: TIntegerField;
    QrPesqNome: TWideStringField;
    DataSource1: TDataSource;
    Query: TABSQuery;
    DBGrid1: TdmkDBGrid;
    QrPesqPreco: TFloatField;
    CGGrupo: TdmkCheckGroup;
    QueryGrupo: TIntegerField;
    QueryAtivo: TSmallintField;
    QueryGRU_TXT: TWideStringField;
    QueryCodigo: TIntegerField;
    QueryVariavel: TWideStringField;
    QueryNome: TWideStringField;
    EdVariavel: TEdit;
    Label2: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure DBGrid1ColEnter(Sender: TObject);
    procedure DBGrid1ColExit(Sender: TObject);
    procedure CGGrupoClick(Sender: TObject);
    procedure EdNomeChange(Sender: TObject);
    procedure QueryCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    procedure ReabreQuery();
  public
    { Public declarations }
    FTexto: String;
    FSelStart: Integer;
  end;

  var
  FmEtqPrinVar: TFmEtqPrinVar;

implementation

uses UnMyObjects, Module, UMySQLModule;

{$R *.DFM}

const
  FSeparaI = '@';
  FSeparaF = '@';

procedure TFmEtqPrinVar.BtOKClick(Sender: TObject);
var
  i: Integer;
  Txt, Str, SeparaI, SeparaF: String;
begin
  {
  if FdmkEdit = nil then
  begin
    Geral.MensagemBox('O dmkEdit n�o foi defido!', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  }
  Screen.Cursor := crHourGlass;
  Txt := '';
  //SeparaI := FmEtqPrinCad.QrEtqPrinCadIdVarIni.Value;
  //SeparaF := FmEtqPrinCad.QrEtqPrinCadIdVarFim.Value;
  //
  Query.Filter   := 'Ativo=1';
  Query.Filtered := True;
  //
  if Query.RecordCount > 0 then
  begin
    i := FSelStart;
    Query.First;
    while not Query.Eof do
    begin
      Txt := Txt + SeparaI + dmkPF.FFP(QueryCodigo.Value, 0) + SeparaF;
      //
      Query.Next;
    end;
    //
    Str := FTexto;
    Insert(Txt, Str, i+1);
    FTexto := Str;
    //
    Close;
  end else begin
    Query.Filtered := False;
    Geral.MensagemBox('Nenhum item foi selecionado!', 'Avido',
    MB_OK+MB_ICONWARNING);
  end;
  Screen.Cursor := crDefault;
end;

procedure TFmEtqPrinVar.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEtqPrinVar.CGGrupoClick(Sender: TObject);
begin
  ReabreQuery();
end;

procedure TFmEtqPrinVar.DBGrid1CellClick(Column: TColumn);
var
  Status: Byte;
begin
  if Column.FieldName = 'Ativo' then
  begin
    if Query.FieldByName('Ativo').Value = 1 then
      Status := 0
    else
      Status := 1;
    //
    Query.Edit;
    Query.FieldByName('Ativo').Value := Status;
    Query.Post;
  end;
end;

procedure TFmEtqPrinVar.DBGrid1ColEnter(Sender: TObject);
begin
  if DBGrid1.Columns[THackDBGrid(DBGrid1).Col -1].FieldName = 'Preco' then
    DBGrid1.Options := DBGrid1.Options + [dgEditing] else
    DBGrid1.Options := DBGrid1.Options - [dgEditing];
end;

procedure TFmEtqPrinVar.DBGrid1ColExit(Sender: TObject);
begin
  DBGrid1.Options := DBGrid1.Options - [dgEditing];
end;

procedure TFmEtqPrinVar.EdNomeChange(Sender: TObject);
begin
  ReabreQuery();
end;

procedure TFmEtqPrinVar.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  //EdNome.CharCase := ecNormal;
end;

procedure TFmEtqPrinVar.FormCreate(Sender: TObject);
  procedure InsereDados(Grupo, Codigo: Integer; Variavel, Texto: String);
  var
    Txt: String;
  begin
    Txt := Geral.SemAcento(Geral.Maiusculas(Texto, False));
    Query.SQL.Add(
    'INSERT INTO etqprinvar (Grupo,Codigo,Variavel,Nome,Ativo) VALUES(' +
    dmkPF.FFP(Grupo, 0) + ',' + dmkPF.FFP(Codigo, 0) + ',"' +
    Variavel + '", "' + Txt + '", 0);');
  end;
begin
  Query.Close;
  Query.SQL.Clear;
  Query.SQL.Add('DROP TABLE EtqPrinVar; ');
  Query.SQL.Add('CREATE TABLE EtqPrinVar (');
  Query.SQL.Add('  Grupo    integer       ,');
  Query.SQL.Add('  Codigo   integer       ,');
  Query.SQL.Add('  Variavel varchar(20)   ,');
  Query.SQL.Add('  Nome     varchar(100)  ,');
  Query.SQL.Add('  Ativo    smallint       ');
  Query.SQL.Add(');');
  //
  InsereDados(1, 001,'PRDTIPCOD', 'C�digo do tipo de produto');
  InsereDados(1, 002,'PRDTIPNOM', 'Descri��o do tipo de produto');
  InsereDados(1, 003,'PRDNI3COD', 'C�digo do 3� n�vel de produtos');
  InsereDados(1, 004,'PRDNI3NOM', 'Descri��o do 3� n�vel de produtos');
  InsereDados(1, 005,'PRDNI2COD', 'C�digo do 2� n�vel de produtos');
  InsereDados(1, 006,'PRDNI2NOM', 'Descri��o do 2� n�vel de produtos');
  InsereDados(1, 007,'PRDNI1COD', 'C�digo do 1� n�vel de produtos');
  InsereDados(1, 008,'PRDNI1NOM', 'Descri��o do 1� n�vel de produtos');
  InsereDados(1, 009,'PRDGRACOD', 'C�digo da grade de tamanhos');
  InsereDados(1, 010,'PRDGRANOM', 'Descri��o da grade de tamanhos');
  InsereDados(1, 011,'PRDTAMCOD', 'C�digo do tamanho');
  InsereDados(1, 012,'PRDTAMNOM', 'Descri��o do tamanho');
  InsereDados(1, 013,'PRDCORCOD', 'C�digo da cor');
  InsereDados(1, 014,'PRDCORNOM', 'Descri��o da cor');
  InsereDados(1, 015,'PRDREDCOD', 'C�digo reduzido');
  InsereDados(1, 016,'PRDCODBAR', 'C�digo de barras do produto');
  InsereDados(1, 017,'PRDSEQUE1', 'Sequencia de impress�o do reduzido');

  {
  InsereDados('', '', '');
  }
  // Abrir sem mostrar nada, apenas criar tabela e incluir itens
  Query.SQL.Add('SELECT * FROM etqprinvar WHERE Ativo=1');
  Query.Open;
end;

procedure TFmEtqPrinVar.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmEtqPrinVar.QueryCalcFields(DataSet: TDataSet);
begin
  case QueryGrupo.Value of
    1: QueryGRU_TXT.Value := 'PRD';
    2: QueryGRU_TXT.Value := 'ENT';
    3: QueryGRU_TXT.Value := 'EXP';
    else QueryGRU_TXT.Value := '???';
  end;
end;

procedure TFmEtqPrinVar.ReabreQuery;
var
  PRD, ENT, EXP, Txt, SQL: String;
begin
  PRD := dmkPF.FFP(MLAGeral.IntInConjunto2Def(1, CGGrupo.Value, 1, 0), 0);
  ENT := dmkPF.FFP(MLAGeral.IntInConjunto2Def(2, CGGrupo.Value, 2, 0), 0);
  EXP := dmkPF.FFP(MLAGeral.IntInConjunto2Def(3, CGGrupo.Value, 3, 0), 0);
  //
  Txt := PRD + ',' + ENT + ',' + EXP;
  //
  Query.Close;
  Query.SQL.Clear;
  SQL := 'SELECT * FROM etqprinvar WHERE Grupo in (' + Txt + ') ';
  Txt := Trim(EdNome.Text);
  if Txt <> '' then
    SQL := SQL + 'AND Nome LIKE "%' + Txt + '%" ';
  Txt := Trim(EdVariavel.Text);
  if Txt <> '' then
    SQL := SQL + 'AND Variavel LIKE "%' + Txt + '%" ';
  Query.SQL.Add(SQL + ';');
  Query.Open;
end;

end.

