unit UnVS_Jan;

interface

uses
  Windows, SysUtils, Classes, DB, Menus, UnDmkEnums, mySQLDbTables, Forms,
  Grids, DBGrids, UnInternalConsts;

type
  TUnVS_Jan = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    {
    procedure MostraFormVSCadNat(GraGruX: Integer; Edita: Boolean;
              QryMul: TmySQLQuery);
    procedure MostraFormVSFccArt(GraGruX: Integer; Edita: Boolean);
    }
 end;

var
  VS_Jan: TUnVS_Jan;


implementation

uses
  MyDBCheck, Module(*,
  VSCadNat, VSFccArt*);

{ TUnVS_Jan }

{
procedure TUnVS_Jan.MostraFormVSCadNat(GraGruX: Integer; Edita: Boolean;
  QryMul: TmySQLQuery);
begin
end;

procedure TUnVS_Jan.MostraFormVSFccArt(GraGruX: Integer; Edita: Boolean);
begin
end;
}

end.
