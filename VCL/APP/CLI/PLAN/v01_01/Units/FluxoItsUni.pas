unit FluxoItsUni;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmFluxoItsUni = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    Panel3: TPanel;
    Label2: TLabel;
    EdOperacao: TdmkEditCB;
    CBOperacao: TdmkDBLookupComboBox;
    Label8: TLabel;
    EdOrdem: TdmkEdit;
    Label6: TLabel;
    EdAcao1: TdmkEdit;
    Label7: TLabel;
    EdAcao2: TdmkEdit;
    Label9: TLabel;
    EdAcao3: TdmkEdit;
    Label10: TLabel;
    EdAcao4: TdmkEdit;
    SpeedButton1: TSpeedButton;
    QrOperacoes: TmySQLQuery;
    QrOperacoesCodigo: TIntegerField;
    QrOperacoesNome: TWideStringField;
    DsOperacoes: TDataSource;
    EdControle: TdmkEdit;
    Label1: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenCadastro_Com_Itens_ITS(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmFluxoItsUni: TFmFluxoItsUni;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  FluxoCab, Principal, UnReordena, UnTX_Jan;

{$R *.DFM}

procedure TFmFluxoItsUni.BtOKClick(Sender: TObject);
var
  Acao1, Acao2, Acao3, Acao4: String;
  Codigo, Controle, Ordem, Operacao: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := FQrCab.FieldByName('Codigo').AsInteger;
  Controle       := EdControle.ValueVariant;
  Ordem          := EdOrdem.ValueVariant;
  Operacao       := EdOperacao.ValueVariant;
  Acao1          := EdAcao1.Text;
  Acao2          := EdAcao2.Text;
  Acao3          := EdAcao3.Text;
  Acao4          := EdAcao4.Text;
  //
  Controle := UMyMod.BPGS1I32('fluxoits', 'Controle', '', '', tsPos, SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'fluxoits', False, [
  'Codigo', 'Ordem', 'Operacao',
  'Acao1', 'Acao2', 'Acao3',
  'Acao4'], [
  'Controle'], [
  Codigo, Ordem, Operacao,
  Acao1, Acao2, Acao3,
  Acao4], [
  Controle], True) then
  begin
    FmFluxoCab.Reordena(False, 0);
    ReopenCadastro_Com_Itens_ITS(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType         := stIns;
      EdControle.ValueVariant := 0;
      EdOperacao.ValueVariant := 0;
      CBOperacao.KeyValue     := Null;
      EdOrdem.ValueVariant    := UReordena.VerificaOrdem(Dmod.QrAux, Dmod.MyDB,
                                 'Codigo', 'Ordem', 'fluxoits',
                                 FmFluxoCab.QrFluxoCabCodigo.Value, 0, True);
      EdAcao1.ValueVariant    := '';
      EdAcao2.ValueVariant    := '';
      EdAcao3.ValueVariant    := '';
      EdAcao4.ValueVariant    := '';
      //
      EdOperacao.SetFocus;
      //
    end else Close;
  end;
end;

procedure TFmFluxoItsUni.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFluxoItsUni.FormActivate(Sender: TObject);
var
  Zerar: Boolean;
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdNome.DataSource   := FDsCab;
  //
  MyObjects.CorIniComponente();
  //
  Zerar := ImgTipo.SQLType = stIns;
  //
  EdOrdem.ValueVariant := UReordena.VerificaOrdem(Dmod.QrAux, Dmod.MyDB,
                            'Codigo', 'Ordem', 'fluxoits',
                            FmFluxoCab.QrFluxoCabCodigo.Value,
                            EdOrdem.ValueVariant, Zerar);
end;

procedure TFmFluxoItsUni.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrOperacoes, Dmod.MyDB);
end;

procedure TFmFluxoItsUni.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFluxoItsUni.ReopenCadastro_Com_Itens_ITS(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmFluxoItsUni.SpeedButton1Click(Sender: TObject);
var
  Operacao: Integer;
begin
  VAR_CADASTRO := 0;
  Operacao     := EdOperacao.ValueVariant;
  //
  TX_Jan.MostraFormOperacoes(Operacao);
  //
  UMyMod.SetaCodigoPesquisado(EdOperacao, CBOperacao, QrOperacoes, VAR_CADASTRO);
end;

end.
