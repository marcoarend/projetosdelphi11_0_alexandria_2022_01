unit FluxoCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkEnums;

type
  TFmFluxoCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    QrFluxoCab: TmySQLQuery;
    QrFluxoCabCodigo: TSmallintField;
    QrFluxoCabNome: TWideStringField;
    DsFluxoCab: TDataSource;
    QrFluxoIts: TmySQLQuery;
    QrFluxoItsCodigo: TIntegerField;
    QrFluxoItsControle: TIntegerField;
    QrFluxoItsOrdem: TIntegerField;
    QrFluxoItsOperacao: TIntegerField;
    QrFluxoItsLk: TIntegerField;
    QrFluxoItsDataCad: TDateField;
    QrFluxoItsDataAlt: TDateField;
    QrFluxoItsUserCad: TIntegerField;
    QrFluxoItsUserAlt: TIntegerField;
    QrFluxoItsNOMEOPERACAO: TWideStringField;
    QrFluxoItsSEQ: TIntegerField;
    DsFluxoIts: TDataSource;
    QrOrdena: TmySQLQuery;
    QrOrdenaCodigo: TIntegerField;
    QrOrdenaControle: TIntegerField;
    QrOrdenaOrdem: TIntegerField;
    QrOrdenaOperacao: TIntegerField;
    QrOrdenaAcao1: TWideStringField;
    QrOrdenaAcao2: TWideStringField;
    QrOrdenaAcao3: TWideStringField;
    QrOrdenaAcao4: TWideStringField;
    QrOrdenaLk: TIntegerField;
    QrOrdenaDataCad: TDateField;
    QrOrdenaDataAlt: TDateField;
    QrOrdenaUserCad: TIntegerField;
    QrOrdenaUserAlt: TIntegerField;
    QrOrdenaNOMEOPERACAO: TWideStringField;
    QrOrdenaSEQ: TIntegerField;
    DBGFluxoIts: TDBGrid;
    QrFluxoSet: TmySQLQuery;
    DsFluxoSet: TDataSource;
    DBGrid2: TDBGrid;
    QrFluxoSetNO_SETOR: TWideStringField;
    QrFluxoSetCodigo: TIntegerField;
    QrFluxoSetControle: TIntegerField;
    QrFluxoSetOrdem: TIntegerField;
    QrFluxoSetSetor: TIntegerField;
    BtSetor: TBitBtn;
    PMSetor: TPopupMenu;
    Adicionasetor1: TMenuItem;
    Removeosetorselecionado1: TMenuItem;
    N1: TMenuItem;
    Duplicaoperaoatual1: TMenuItem;
    N2: TMenuItem;
    ReordenaOperacoes1: TMenuItem;
    QrFluxoItsAcao1: TWideStringField;
    QrFluxoItsAcao2: TWideStringField;
    QrFluxoItsAcao3: TWideStringField;
    QrFluxoItsAcao4: TWideStringField;
    N3: TMenuItem;
    Duplicafluxoatual1: TMenuItem;
    N4: TMenuItem;
    Gerenciaritens1: TMenuItem;
    QrFluxoCabLk: TIntegerField;
    QrFluxoCabDataCad: TDateField;
    QrFluxoCabDataAlt: TDateField;
    QrFluxoCabUserCad: TIntegerField;
    QrFluxoCabUserAlt: TIntegerField;
    QrFluxoCabAlterWeb: TSmallintField;
    QrFluxoCabAWServerID: TIntegerField;
    QrFluxoCabAWStatSinc: TSmallintField;
    QrFluxoCabAtivo: TSmallintField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrFluxoCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrFluxoCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrFluxoCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrFluxoItsCalcFields(DataSet: TDataSet);
    procedure Adicionasetor1Click(Sender: TObject);
    procedure Removeosetorselecionado1Click(Sender: TObject);
    procedure BtSetorClick(Sender: TObject);
    procedure QrFluxoCabBeforeClose(DataSet: TDataSet);
    procedure ItsExclui1Click(Sender: TObject);
    procedure Duplicaoperaoatual1Click(Sender: TObject);
    procedure ReordenaOperacoes1Click(Sender: TObject);
    procedure Duplicafluxoatual1Click(Sender: TObject);
    procedure Gerenciaritens1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraFormFluxoIts(SQLType: TSQLType);
    procedure MostraFormFluxoItsMul();
    procedure MostraFormFluxoSet(SQLType: TSQLType);
  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenFluxoIts(Controle: Integer);
    procedure ReopenFluxoSet(Controle: Integer);
    procedure Reordena(Pula: Boolean; Posicao: Integer);
  end;

var
  FmFluxoCab: TFmFluxoCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, ModuleGeral, Module, MyDBCheck, DmkDAC_PF, UnReordena,
  FluxoItsUni, FluxoSet, FluxoItsMul;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmFluxoCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmFluxoCab.MostraFormFluxoIts(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmFluxoItsUni, FmFluxoItsUni, afmoNegarComAviso) then
  begin
    FmFluxoItsUni.ImgTipo.SQLType := SQLType;
    FmFluxoItsUni.FQrCab := QrFluxoCab;
    FmFluxoItsUni.FDsCab := DsFluxoCab;
    FmFluxoItsUni.FQrIts := QrFluxoIts;
    if SQLType = stIns then
      //
    else
    begin
      FmFluxoItsUni.EdControle.ValueVariant := QrFluxoItsControle.Value;
      //
      FmFluxoItsUni.EdOperacao.ValueVariant := QrFluxoItsOperacao.Value;
      FmFluxoItsUni.CBOperacao.KeyValue := QrFluxoItsOperacao.Value;
      FmFluxoItsUni.EdOrdem.ValueVariant := QrFluxoItsOrdem.Value;

      FmFluxoItsUni.EdAcao1.Text := QrFluxoItsAcao1.Value;
      FmFluxoItsUni.EdAcao2.Text := QrFluxoItsAcao2.Value;
      FmFluxoItsUni.EdAcao3.Text := QrFluxoItsAcao3.Value;
      FmFluxoItsUni.EdAcao4.Text := QrFluxoItsAcao4.Value;

    end;
    FmFluxoItsUni.ShowModal;
    FmFluxoItsUni.Destroy;
  end;
end;

procedure TFmFluxoCab.MostraFormFluxoItsMul();
begin
  if DBCheck.CriaFm(TFmFluxoItsMul, FmFluxoItsMul, afmoNegarComAviso) then
  begin
    FmFluxoItsMul.FCodigo := QrFluxoCabCodigo.Value;
    FmFluxoItsMul.ShowModal;
    FmFluxoItsMul.Destroy;
    //
    ReopenFluxoIts(0);
  end;
end;

procedure TFmFluxoCab.MostraFormFluxoSet(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmFluxoSet, FmFluxoSet, afmoNegarComAviso) then
  begin
    FmFluxoSet.ImgTipo.SQLType := SQLType;
    FmFluxoSet.FQrCab := QrFluxoCab;
    FmFluxoSet.FDsCab := DsFluxoCab;
    FmFluxoSet.FQrIts := QrFluxoSet;
    if SQLType = stIns then
      //
    else
    begin
      FmFluxoSet.EdControle.ValueVariant := QrFluxoSetControle.Value;
      //
      FmFluxoSet.EdSetor.ValueVariant := QrFluxoSetSetor.Value;
      FmFluxoSet.CBSetor.KeyValue := QrFluxoSetSetor.Value;
      FmFluxoSet.EdOrdem.ValueVariant := QrFluxoSetOrdem.Value;

    end;
    FmFluxoSet.ShowModal;
    FmFluxoSet.Destroy;
  end;
end;

procedure TFmFluxoCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrFluxoCab);
  MyObjects.HabilitaMenuItemCabUpd(Duplicafluxoatual1, QrFluxoCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrFluxoCab, QrFluxoIts);
end;

procedure TFmFluxoCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrFluxoCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrFluxoIts);
  MyObjects.HabilitaMenuItemItsUpd(Gerenciaritens1, QrFluxoCab);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrFluxoIts);
  MyObjects.HabilitaMenuItemItsDel(Duplicaoperaoatual1, QrFluxoIts);
  MyObjects.HabilitaMenuItemItsDel(ReordenaOperacoes1, QrFluxoIts);
end;

procedure TFmFluxoCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrFluxoCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmFluxoCab.DefParams;
begin
  VAR_GOTOTABELA := 'fluxocab';
  VAR_GOTOMYSQLTABLE := QrFluxoCab;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM fluxocab');
  VAR_SQLx.Add('WHERE Codigo > -1000');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQLa.Add('AND Nome LIKE :P0');
  //
end;

procedure TFmFluxoCab.Duplicafluxoatual1Click(Sender: TObject);
var
  Codigo, Controle: Integer;
  Nome: String;
begin
  if Geral.MB_Pergunta('Confirma a duplica��o do fluxo atual?') = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    QrFluxoCab.DisableControls;
    QrFluxoIts.DisableControls;
    QrFluxoSet.DisableControls;
    //
    try
      Codigo := UMyMod.BuscaEmLivreY_Def('fluxocab', 'Codigo', stIns, 0);
      Nome   := Copy('<c�pia> ' + QrFluxoCabNome.Value, 1, 100);
      //
      if UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'fluxocab', TMeuDB,
        ['Codigo'], [QrFluxoCabCodigo.Value],
        ['Codigo', 'Nome'],
        [Codigo, Nome],
        '', True, LaAviso1, LaAviso2) then
      begin
        QrFluxoIts.First;
        while not QrFluxoIts.Eof do
        begin
          Controle := UMyMod.BuscaEmLivreY_Def('fluxoits', 'Controle', stIns, 0);
          //
          UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'fluxoits', TMeuDB,
            ['Controle'], [QrFluxoItsControle.Value],
            ['Codigo', 'Controle'],
            [Codigo, Controle],
            '', True, LaAviso1, LaAviso2);
          //
          QrFluxoIts.Next;
        end;
        QrFluxoSet.First;
        while not QrFluxoSet.Eof do
        begin
          Controle := UMyMod.BPGS1I32('fluxoset', 'Controle', '', '', tsPos, stIns, 0);
          //
          if UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'fluxoset', TMeuDB,
          ['Controle'], [QrFluxoSetControle.Value],
          ['Controle', 'Codigo'],
          [Controle, Codigo],
          '', True, LaAviso1, LaAviso2) then
          begin
            //
          end;
          //
          QrFluxoSet.Next;
        end;
      end;
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
      //
      LocCod(Codigo, Codigo);
      //
      Screen.Cursor := crDefault;
      Geral.MB_Info('Duplica��o conclu�da com sucesso!');
    finally
      QrFluxoCab.EnableControls;
      QrFluxoIts.EnableControls;
      QrFluxoSet.EnableControls;
    end;
  end;
end;

procedure TFmFluxoCab.Duplicaoperaoatual1Click(Sender: TObject);
var
  Controle, Ordem, Numero: Integer;
begin
  if (QrFluxoIts.State <> dsInactive) and (QrFluxoIts.RecordCount > 0) then
  begin
    if Geral.MB_Pergunta('Deseja realmente duplicar esta opera��o?') <> ID_YES then Exit;
    //
    Screen.Cursor := crHourGlass;
    try
      QrFluxoIts.DisableControls;
      QrFluxoIts.DisableControls;
      //
      Controle := UMyMod.BuscaEmLivreY_Def('fluxoits', 'Controle', stIns,
                    QrFluxoItsControle.Value);
      Ordem    := UReordena.VerificaOrdem(Dmod.QrAux, Dmod.MyDB, 'Codigo',
                    'Ordem', 'fluxoits', QrFluxoCabCodigo.Value, 0, True);
      //
      if not UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'fluxoits', TMeuDB,
        ['Controle', 'Ordem'], [QrFluxoItsControle.Value, QrFluxoItsOrdem.Value],
        ['Controle', 'Ordem'], [Controle, Ordem],
        '', True, LaAviso1, LaAviso2) then
      begin
        Geral.MB_Erro('Falha ao duplicar opera��o!');
        Exit;
      end;
    finally
      QrFluxoIts.EnableControls;
      QrFluxoIts.EnableControls;
      //
      ReopenFluxoIts(Controle);
      //
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmFluxoCab.ItsAltera1Click(Sender: TObject);
begin
  MostraFormFluxoIts(stUpd);
end;

procedure TFmFluxoCab.ItsExclui1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrFluxoIts, DBGFluxoIts,
    'fluxoits', ['Controle'], ['Controle'], istPergunta, '');
end;

procedure TFmFluxoCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Aviso('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmFluxoCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmFluxoCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmFluxoCab.Removeosetorselecionado1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a retirada do setor selecionado?',
  'fluxoset', 'Controle', QrFluxoSetControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrFluxoSet,
      QrFluxoSetControle, QrFluxoSetControle.Value);
    ReopenFluxoSet(Controle);
  end;
end;

procedure TFmFluxoCab.ReopenFluxoIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFluxoIts, Dmod.MyDB, [
  'SELECT ope.Nome NOMEOPERACAO, fli.* ',
  'FROM fluxoits fli ',
  'LEFT JOIN operacoes ope ON ope.Codigo=fli.Operacao ',
  'WHERE fli.Codigo=' + Geral.FF0(QrFluxoCabCodigo.Value),
  'ORDER BY fli.Ordem, fli.Controle DESC ',
  '']);
  //
  QrFluxoIts.Locate('Controle', Controle, []);
end;


procedure TFmFluxoCab.ReopenFluxoSet(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFluxoSet, Dmod.MyDB, [
  'SELECT lse.Nome NO_SETOR, fls.* ',
  'FROM fluxoset fls ',
  'LEFT JOIN setorcad lse ON lse.Codigo=fls.Setor ',
  'WHERE fls.Codigo=' + Geral.FF0(QrFluxoCabCodigo.Value),
  'ORDER BY fls.Ordem, fls.Controle DESC ',
  '']);
  //
  QrFluxoSet.Locate('Controle', Controle, []);
end;

procedure TFmFluxoCab.Reordena(Pula: Boolean; Posicao: Integer);
var
  Ordem, Real, Controle: Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOrdena, Dmod.MyDB, [
  'SELECT ope.Nome NOMEOPERACAO, fli.* ',
  'FROM fluxoits fli ',
  'LEFT JOIN operacoes ope ON ope.Codigo=fli.Operacao ',
  'WHERE fli.Codigo=:P0 ',
  'ORDER BY fli.Ordem, fli.Controle DESC ',
  '']);
  //
  Ordem := 0;
  while not QrOrdena.Eof do
  begin
    Ordem := Ordem + 1;
    Real := Ordem;
    if Pula then if Real >= Posicao then Real := Real + 1;
    //
    Controle := QrOrdenaControle.Value;
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'fluxoits', False, [
    'Ordem'], ['Controle'], [Ordem], [Controle], True);
    //
    QrOrdena.Next;
  end;
end;

procedure TFmFluxoCab.ReordenaOperacoes1Click(Sender: TObject);
begin
  if (QrFluxoIts.State <> dsInactive) and(QrFluxoIts.RecordCount > 0) then
  begin
    UReordena.ReordenaItens(QrFluxoIts, Dmod.QrUpd, 'fluxoits',
      'Ordem', 'Controle', 'NOMEOPERACAO', '', '', '', nil);
    //
    ReopenFluxoIts(0);
  end;
end;

procedure TFmFluxoCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmFluxoCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmFluxoCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmFluxoCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmFluxoCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmFluxoCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFluxoCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrFluxoCabCodigo.Value;
  Close;
end;

procedure TFmFluxoCab.ItsInclui1Click(Sender: TObject);
begin
  MostraFormFluxoIts(stIns);
end;

procedure TFmFluxoCab.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrFluxoCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'fluxocab');
end;

procedure TFmFluxoCab.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
  SQLType: TSQLType;
begin
  SQLType := ImgTipo.SQLType;
  Nome    := EdNome.ValueVariant;
  Codigo  := EdCodigo.ValueVariant;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('fluxocab', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita, 'fluxocab',
  Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmFluxoCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'fluxocab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'fluxocab', 'Codigo');
end;

procedure TFmFluxoCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmFluxoCab.Adicionasetor1Click(Sender: TObject);
begin
  MostraFormFluxoSet(stIns);
end;

procedure TFmFluxoCab.BtSetorClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMSetor, BtSetor);
end;

procedure TFmFluxoCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmFluxoCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DBGFluxoIts.Align := alClient;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmFluxoCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrFluxoCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmFluxoCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmFluxoCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrFluxoCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmFluxoCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmFluxoCab.QrFluxoCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmFluxoCab.QrFluxoCabAfterScroll(DataSet: TDataSet);
begin
  ReopenFluxoIts(0);
  ReopenFluxoSet(0);
end;

procedure TFmFluxoCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrFluxoCabCodigo.Value <> FCabIni then Geral.MB_Aviso(
    'Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmFluxoCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrFluxoCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'fluxocab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmFluxoCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFluxoCab.Gerenciaritens1Click(Sender: TObject);
begin
  MostraFormFluxoItsMul();
end;

procedure TFmFluxoCab.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrFluxoCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'fluxocab');
end;

procedure TFmFluxoCab.QrFluxoCabBeforeClose(DataSet: TDataSet);
begin
  QrFluxoIts.Close;
  QrFluxoSet.Close;
end;

procedure TFmFluxoCab.QrFluxoCabBeforeOpen(DataSet: TDataSet);
begin
  QrFluxoCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmFluxoCab.QrFluxoItsCalcFields(DataSet: TDataSet);
begin
  QrFluxoItsSEQ.Value := QrFluxoIts.RecNo;
end;

end.

