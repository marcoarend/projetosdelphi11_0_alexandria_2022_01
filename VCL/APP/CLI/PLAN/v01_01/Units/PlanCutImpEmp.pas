unit PlanCutImpEmp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DB, DBGrids,
  dmkDBGrid, DBCtrls, mySQLDbTables, Menus, ComCtrls, UnDmkProcFunc;

type
  THackDBGrid = class(TDBGrid);
  TFmPlanCutImpEmp = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    BtNenhum: TBitBtn;
    BtTodos: TBitBtn;
    BtPesq: TBitBtn;
    PMPesq: TPopupMenu;
    Ativa1: TMenuItem;
    Desativa1: TMenuItem;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel3: TPanel;
    Label1: TLabel;
    EdPesq: TEdit;
    DBGrid1: TdmkDBGrid;
    Panel4: TPanel;
    DBGrid2: TdmkDBGrid;
    RGSelecao: TRadioGroup;
    procedure FormCreate(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure FormStartDock(Sender: TObject; var DragObject: TDragDockObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure EdPesqChange(Sender: TObject);
    procedure FormUnDock(Sender: TObject; Client: TControl;
      NewTarget: TWinControl; var Allow: Boolean);
    procedure BtNenhumClick(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtPesqClick(Sender: TObject);
    procedure Ativa1Click(Sender: TObject);
    procedure Desativa1Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure RGSelecaoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure Pesquisar();
    procedure AtivaItens(Status: Integer);
    procedure AtivaSelecionados(Status: Integer);
    procedure HabilitaItemUnico();
  public
    { Public declarations }
    class function CreateDockForm(const aColor: TColor): TCustomForm;
    //class procedure FiltaAtivos();
    procedure LimpaPesquisa();
  end;

var
  FmPlanCutImpEmp: TFmPlanCutImpEmp;

implementation

uses UnMyObjects, Module, UMySQLModule, TabePrcCab, PlanCutImp, ModPlanCut, MyGlyfs;

{$R *.DFM}

///// PROCEDURES E FUNCTIONS DO DOCKFORM

procedure TFmPlanCutImpEmp.Ativa1Click(Sender: TObject);
begin
  AtivaSelecionados(1);
end;

procedure TFmPlanCutImpEmp.AtivaItens(Status: Integer);
begin
  Screen.Cursor := crHourGlass;
  DmPlanCut.QrPCIEmpIts.Close;
  DmPlanCut.QrPCIEmpIts.SQL.Clear;
  DmPlanCut.QrPCIEmpIts.SQL.Add('UPDATE pciemp SET Ativo=' + dmkPF.FFP(Status, 0) + ';');
  DmPlanCut.QrPCIEmpIts.SQL.Add('SELECT * FROM pciemp;');
  DmPlanCut.QrPCIEmpIts.Open;
  HabilitaItemUnico();
  Screen.Cursor := crDefault;
end;

procedure TFmPlanCutImpEmp.AtivaSelecionados(Status: Integer);
begin
  Screen.Cursor := crHourGlass;
  DmPlanCut.QrPCIEmpIts.First;
  while not DmPlanCut.QrPCIEmpIts.Eof do
  begin
    if DmPlanCut.QrPCIEmpItsAtivo.Value <> Status then
    begin
      DmPlanCut.QrPCIEmpIts.Edit;
      DmPlanCut.QrPCIEmpItsAtivo.Value := Status;
      DmPlanCut.QrPCIEmpIts.Post;
    end;
    DmPlanCut.QrPCIEmpIts.Next;
  end;
  HabilitaItemUnico();
  Screen.Cursor := crDefault;
end;

procedure TFmPlanCutImpEmp.BtPesqClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPesq, BtPesq);
end;

procedure TFmPlanCutImpEmp.BtNenhumClick(Sender: TObject);
begin
  AtivaItens(0);
end;

procedure TFmPlanCutImpEmp.BtTodosClick(Sender: TObject);
begin
  AtivaItens(1);
end;

class function TFmPlanCutImpEmp.CreateDockForm(const aColor: TColor): TCustomForm;
begin
  result := TFmPlanCutImpEmp.Create(Application);
  //result.Color := aColor;
  //result.Caption := ColorToString(aColor);
  FmMyGlyfs.ConfiguraFormDock(result);
  result.Show;
end;

procedure TFmPlanCutImpEmp.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  DmPlanCut.QrPCIEmpIts.Close;
  DmPlanCut.QrPCIEmpIts.SQL.Clear;
  DmPlanCut.QrPCIEmpIts.SQL.Add('DROP TABLE PCIEmp; ');
  DmPlanCut.QrPCIEmpIts.ExecSQL;
  //
  ManualFloat(Rect(0, 0, 0, 0));
  Action := caFree;
end;

procedure TFmPlanCutImpEmp.FormShow(Sender: TObject);
begin
  FmPlanCutImp.FechaPesquisa();
end;

procedure TFmPlanCutImpEmp.FormStartDock(Sender: TObject;
  var DragObject: TDragDockObject);
begin
  DragObject := TDragDockObjectEx.Create(Self);
  DragObject.Brush.Color := clAqua; // this will display a red outline
end;


procedure TFmPlanCutImpEmp.FormUnDock(Sender: TObject; Client: TControl;
  NewTarget: TWinControl; var Allow: Boolean);
begin
end;

///// FIM DAS PROCEDURES E FUNCTIONS DO DOCKFORM

procedure TFmPlanCutImpEmp.PageControl1Change(Sender: TObject);
begin
  BtPesq.Enabled := PageControl1.ActivePageIndex = 0;
  case PageControl1.ActivePageIndex of
    0: Pesquisar();
    1: RGSelecaoClick(Self);
  end;
end;

procedure TFmPlanCutImpEmp.Pesquisar();
begin
  Screen.Cursor := crHourGlass;
  //
  DmPlanCut.QrPCIEmpIts.Filter   := 'Nome LIKE "%' + EdPesq.Text + '%"';
  DmPlanCut.QrPCIEmpIts.Filtered := True;
  //
  Screen.Cursor := crDefault;
end;

procedure TFmPlanCutImpEmp.RGSelecaoClick(Sender: TObject);
begin
  DmPlanCut.QrPCIEmpIts.Filtered := False;
  case RGSelecao.ItemIndex of
    0: DmPlanCut.QrPCIEmpIts.Filter := 'Ativo=0';
    1: DmPlanCut.QrPCIEmpIts.Filter := 'Ativo=1';
    2: DmPlanCut.QrPCIEmpIts.Filter := '';
  end;
  if RGSelecao.ItemIndex < 2 then
    DmPlanCut.QrPCIEmpIts.Filtered := True;
end;

procedure TFmPlanCutImpEmp.DBGrid1CellClick(Column: TColumn);
var
  Status: Integer;
begin
  if Column.FieldName = 'Ativo' then
  begin
    if DmPlanCut.QrPCIEmpIts.FieldByName('Ativo').Value = 1 then
      Status := 0
    else
      Status := 1;
    //
    DmPlanCut.QrPCIEmpIts.Edit;
    DmPlanCut.QrPCIEmpIts.FieldByName('Ativo').Value := Status;
    DmPlanCut.QrPCIEmpIts.Post;
  end;
  HabilitaItemUnico();
end;

procedure TFmPlanCutImpEmp.HabilitaItemUnico();
var
  Habilita: Boolean;
begin
  DmPlanCut.QrPCIEmpAti.Close;
  DmPlanCut.QrPCIEmpAti.Open;
  Habilita :=  DmPlanCut.QrPCIEmpAtiItens.Value = 0;
  FmPlanCutImp.LaPCIEmp.Enabled := Habilita;
  FmPlanCutImp.EdPCIEmp.Enabled := Habilita;
  FmPlanCutImp.CBPCIEmp.Enabled := Habilita;
  if not Habilita then
  begin
    FmPlanCutImp.EdPCIEmp.ValueVariant := 0;
    FmPlanCutImp.CBPCIEmp.KeyValue     := 0;
  end;
end;

procedure TFmPlanCutImpEmp.LimpaPesquisa;
var
  K, I: Integer;
  //Form: TForm;
begin
  for K := 0 to Screen.FormCount - 1 do
  begin
    if Screen.Forms[K] is TFmPlanCutImpEmp then
    begin
      for I := 0 to Screen.Forms[K].ComponentCount -1 do
      if Screen.Forms[K].Components[I] is TEdit then
        TEdit(Screen.Forms[K].Components[I]).Text := '';
    end;
  end;
  AtivaItens(0);
end;

procedure TFmPlanCutImpEmp.Desativa1Click(Sender: TObject);
begin
  AtivaSelecionados(0);
end;

procedure TFmPlanCutImpEmp.EdPesqChange(Sender: TObject);
begin
  Pesquisar();
end;

procedure TFmPlanCutImpEmp.FormCreate(Sender: TObject);
const
  Txt1 = 'INSERT INTO pciemp (Codigo,CodUsu,Nome,Ativo) VALUES(';
var
  Txt2: String;
begin
{ Falta fazer: PlanCutImpEmp temp table
  DmPlanCut.QrPCIEmpIts.Close;
  DmPlanCut.QrPCIEmpIts.SQL.Clear;
  DmPlanCut.QrPCIEmpIts.SQL.Add('DROP TABLE PCIEmp; ');
  DmPlanCut.QrPCIEmpIts.SQL.Add('CREATE TABLE PCIEmp (');
  DmPlanCut.QrPCIEmpIts.SQL.Add('  Codigo  integer      ,');
  DmPlanCut.QrPCIEmpIts.SQL.Add('  CodUsu  integer      ,');
  DmPlanCut.QrPCIEmpIts.SQL.Add('  Nome    varchar(50)  ,');
  DmPlanCut.QrPCIEmpIts.SQL.Add('  Ativo   smallint      ');
  DmPlanCut.QrPCIEmpIts.SQL.Add(');');
  //
  DmPlanCut.QrPCIEmpCad.Close;
  DmPlanCut.QrPCIEmpCad.Open;
  while not DmPlanCut.QrPCIEmpCad.Eof do
  begin
    Txt2 :=
      dmkPF.FFP(DmPlanCut.QrPCIEmpCadCodigo.Value, 0) + ',' +
      dmkPF.FFP(DmPlanCut.QrPCIEmpCadFilial.Value, 0) + ',' +
      '"' + DmPlanCut.QrPCIEmpCadNOMEENT.Value + '",' +
      '0);';
  //for i  := 1 to 10 do
    DmPlanCut.QrPCIEmpIts.SQL.Add(Txt1 + Txt2);
    DmPlanCut.QrPCIEmpCad.Next;
  end;
  //
  DmPlanCut.QrPCIEmpIts.SQL.Add('SELECT * FROM pciemp;');
  DmPlanCut.QrPCIEmpIts.Open;
  PageControl1.ActivePageIndex := 0;
}
end;

procedure TFmPlanCutImpEmp.FormDeactivate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 1;
  RGSelecao.ItemIndex := 1;
  //
  if FindWindow('TFmPlanCutImp', nil) > 0 then
    FmPlanCutImp.AtivaBtConfirma();
end;

end.

