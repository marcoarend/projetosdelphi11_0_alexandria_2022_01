object FmEtqPrinVar: TFmEtqPrinVar
  Left = 339
  Top = 185
  Caption = 'ETQ-PRINT-003 :: Etiquetas - Vari'#225'veis de Impress'#227'o'
  ClientHeight = 521
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 473
    Width = 784
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      TabOrder = 0
      OnClick = BtOKClick
      NumGlyphs = 2
    end
    object Panel2: TPanel
      Left = 672
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 15
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Desiste'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    Caption = 'Etiquetas - Vari'#225'veis de Impress'#227'o'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 782
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 425
    Align = alClient
    TabOrder = 0
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 782
      Height = 48
      Align = alTop
      TabOrder = 0
      object Label1: TLabel
        Left = 320
        Top = 4
        Width = 129
        Height = 13
        Caption = 'Informe parte da descri'#231#227'o:'
      end
      object Label2: TLabel
        Left = 652
        Top = 4
        Width = 120
        Height = 13
        Caption = 'Informe parte da vari'#225'vel:'
      end
      object EdNome: TEdit
        Left = 320
        Top = 20
        Width = 329
        Height = 21
        TabOrder = 0
        OnChange = EdNomeChange
      end
      object CGGrupo: TdmkCheckGroup
        Left = 12
        Top = 4
        Width = 301
        Height = 38
        Caption = ' Grupo:'
        Columns = 3
        Items.Strings = (
          'Produto'
          'Pessoa'
          'Expedi'#231#227'o')
        TabOrder = 1
        OnClick = CGGrupoClick
        UpdType = utYes
        Value = 0
        OldValor = 0
      end
      object EdVariavel: TEdit
        Left = 652
        Top = 20
        Width = 121
        Height = 21
        TabOrder = 2
        OnChange = EdNomeChange
      end
    end
    object DBGrid1: TdmkDBGrid
      Left = 1
      Top = 49
      Width = 782
      Height = 375
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'Ativo'
          Title.Caption = 'OK'
          Width = 20
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GRU_TXT'
          Title.Caption = 'Grupo'
          Width = 34
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'C'#243'digo'
          Width = 40
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Variavel'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          Title.Caption = 'Vari'#225'vel'
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Title.Caption = 'Descri'#231#227'o'
          Width = 568
          Visible = True
        end>
      Color = clWindow
      DataSource = DataSource1
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnCellClick = DBGrid1CellClick
      OnColEnter = DBGrid1ColEnter
      OnColExit = DBGrid1ColExit
      Columns = <
        item
          Expanded = False
          FieldName = 'Ativo'
          Title.Caption = 'OK'
          Width = 20
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GRU_TXT'
          Title.Caption = 'Grupo'
          Width = 34
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'C'#243'digo'
          Width = 40
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Variavel'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          Title.Caption = 'Vari'#225'vel'
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Title.Caption = 'Descri'#231#227'o'
          Width = 568
          Visible = True
        end>
    end
  end
  object QrPesq: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.Nivel1, gg1.CodUsu, gg1.Nome, tpg.Preco'
      'FROM gragru1 gg1'
      
        'LEFT JOIN tabeprcgrg tpg ON tpg.Nivel1=gg1.Nivel1 AND tpg.Codigo' +
        '=:P0'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'WHERE gg1.Nome LIKE :P1'
      'AND pgt.TipPrd=1')
    Left = 68
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPesqNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrPesqCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPesqNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrPesqPreco: TFloatField
      FieldName = 'Preco'
    end
  end
  object DataSource1: TDataSource
    DataSet = Query
    Left = 40
    Top = 12
  end
  object Query: TABSQuery
    CurrentVersion = '6.01 '
    DatabaseName = 'MEMORY'
    InMemory = True
    ReadOnly = False
    OnCalcFields = QueryCalcFields
    RequestLive = True
    Left = 12
    Top = 12
    object QueryGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QueryAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
    object QueryGRU_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'GRU_TXT'
      Size = 3
      Calculated = True
    end
    object QueryCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QueryVariavel: TWideStringField
      FieldName = 'Variavel'
    end
    object QueryNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
end
