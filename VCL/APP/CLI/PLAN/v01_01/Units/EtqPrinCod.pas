unit EtqPrinCod;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DB, ABSMain, DBGrids,
  dmkDBGrid, DBCtrls, mySQLDbTables, dmkGeral, UnDmkProcFunc;

type
  THackDBGrid = class(TDBGrid);
  TFmTabePrcGrGMul1 = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Panel3: TPanel;
    Label1: TLabel;
    EdPesq: TEdit;
    BtPesquisa: TBitBtn;
    QrPesq: TmySQLQuery;
    Label11: TLabel;
    QrPesqNivel1: TIntegerField;
    QrPesqCodUsu: TIntegerField;
    QrPesqNome: TWideStringField;
    DataSource1: TDataSource;
    Query: TABSQuery;
    QueryNivel1: TIntegerField;
    QueryCodUsu: TIntegerField;
    QueryNome: TWideStringField;
    QueryPreco: TFloatField;
    QueryAtivo: TSmallintField;
    DBGrid1: TdmkDBGrid;
    QrPesqPreco: TFloatField;
    StaticText1: TStaticText;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure DBGrid1ColEnter(Sender: TObject);
    procedure DBGrid1ColExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCadastrar: Boolean;
  end;

  var
  FmTabePrcGrGMul1: TFmTabePrcGrGMul1;

implementation

uses UnMyObjects, Module, UMySQLModule, TabePrcCab;

{$R *.DFM}

procedure TFmTabePrcGrGMul1.BtOKClick(Sender: TObject);
var
  Codigo, Nivel1: Integer;
begin
  Screen.Cursor := crHourGlass;
  //
  FCadastrar := True;
  Query.Filter   := 'Ativo=1';
  Query.Filtered := True;
  //
  if Query.RecordCount > 0 then
  begin
    Codigo := FmTabePrcCab.QrTabePrcCabCodigo.Value;
    Query.First;
    while not Query.Eof do
    begin
      UMyMod.SQLIns_ON_DUPLICATE_KEY(Dmod.QrUpd, 'tabeprcgrg', False, ['Preco'],
        ['Codigo', 'Nivel1'], ['Preco'], [QueryPreco.Value],
        [Codigo, QueryNivel1.Value], [QueryPreco.Value], True);
      Query.Next;
    end;
    //
    Nivel1 := FmTabePrcCab.QrTabePrcGrGNivel1.Value;
    FmTabePrcCab.ReopenTabePrcGrG(Nivel1);
    //
    Close;
  end else begin
    Query.Filtered := False;
    Geral.MensagemBox('Nenhum item foi selecionado!', 'Avido',
    MB_OK+MB_ICONWARNING);
  end;
  Screen.Cursor := crDefault;
end;

procedure TFmTabePrcGrGMul1.BtPesquisaClick(Sender: TObject);
var
  Lin: String;
begin
  Screen.Cursor := crHourGlass;
  QrPesq.Close;
  QrPesq.Params[00].AsInteger := FmTabePrcCab.QrTabePrcCabCodigo.Value;
  QrPesq.Params[01].AsString  := '%' + EdPesq.Text + '%';
  QrPesq.Open;

  //

  Query.Close;
  Query.SQL.Clear;
  Query.SQL.Add('DELETE FROM prcgrgmul;');
  Lin := 'INSERT INTO prcgrgmul (Nivel1,CodUsu,Nome,Preco,Ativo) Values (';
  QrPesq.First;
  while not QrPesq.Eof do
  begin
    Query.SQL.Add(Lin + //'(' +
      dmkPF.FFP(QrPesqNivel1.Value, 0) + ',' +
      dmkPF.FFP(QrPesqCodUsu.Value, 0) + ',' +
      '"' + QrPesqNome.Value + '",' +
      dmkPF.FFP(QrPesqPreco.Value , 6) + ',' +
    '1);');
    //
    QrPesq.Next;
  end;
  //
  Query.SQL.Add('SELECT * FROM prcgrgmul ORDER BY CodUsu');
  Query.Open;

  //

  Screen.Cursor := crDefault;
end;

procedure TFmTabePrcGrGMul1.BtSaidaClick(Sender: TObject);
begin
  FCadastrar := False;
  Close;
end;

procedure TFmTabePrcGrGMul1.DBGrid1CellClick(Column: TColumn);
var
  Status: Byte;
begin
  if Column.FieldName = 'Ativo' then
  begin
    if Query.FieldByName('Ativo').Value = 1 then
      Status := 0
    else
      Status := 1;
    //
    Query.Edit;
    Query.FieldByName('Ativo').Value := Status;
    Query.Post;
  end;
end;

procedure TFmTabePrcGrGMul1.DBGrid1ColEnter(Sender: TObject);
begin
  if DBGrid1.Columns[THackDBGrid(DBGrid1).Col -1].FieldName = 'Preco' then
    DBGrid1.Options := DBGrid1.Options + [dgEditing] else
    DBGrid1.Options := DBGrid1.Options - [dgEditing];
end;

procedure TFmTabePrcGrGMul1.DBGrid1ColExit(Sender: TObject);
begin
  DBGrid1.Options := DBGrid1.Options - [dgEditing];
end;

procedure TFmTabePrcGrGMul1.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmTabePrcGrGMul1.FormCreate(Sender: TObject);
begin
  FCadastrar := False;
  Query.Close;
  Query.SQL.Clear;
  Query.SQL.Add('DROP TABLE PrcGrGMul; ');
  Query.SQL.Add('CREATE TABLE PrcGrGMul (');
  Query.SQL.Add('  Nivel1  integer      ,');
  Query.SQL.Add('  CodUsu  integer      ,');
  Query.SQL.Add('  Nome    varchar(30)  ,');
  Query.SQL.Add('  Preco   float        ,');
  Query.SQL.Add('  Ativo   smallint      ');
  Query.SQL.Add(');');
  //
  Query.SQL.Add('SELECT * FROM prcgrgmul');
  Query.Open;
  QueryPreco.DisplayFormat := MLAGeral.FormataCasas(
    DMod.QrControleCasasProd.Value);
end;

procedure TFmTabePrcGrGMul1.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

end.

