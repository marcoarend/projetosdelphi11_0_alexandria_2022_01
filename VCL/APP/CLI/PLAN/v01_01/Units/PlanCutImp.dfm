object FmPlanCutImp: TFmPlanCutImp
  Left = 0
  Top = 0
  Caption = 'PLA-CORTE-000 :: Relat'#243'rio de Corte Simples'
  ClientHeight = 734
  ClientWidth = 1318
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 1288
    Top = 63
    Width = 4
    Height = 608
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alRight
  end
  object DockTabSet1: TTabSet
    Left = 1292
    Top = 63
    Width = 26
    Height = 608
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alRight
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -17
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ShrinkToFit = True
    Style = tsModernPopout
    TabPosition = tpLeft
    DockSite = False
    DestinationDockSite = pDockLeft
    OnDockDrop = DockTabSet1DockDrop
    OnTabAdded = DockTabSet1TabAdded
    OnTabRemoved = DockTabSet1TabRemoved
  end
  object pDockLeft: TPanel
    Left = 1292
    Top = 63
    Width = 0
    Height = 608
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alRight
    BevelOuter = bvNone
    DockSite = True
    TabOrder = 1
    OnDockDrop = pDockLeftDockDrop
    OnDockOver = pDockLeftDockOver
    OnUnDock = pDockLeftUnDock
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1318
    Height = 63
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = 'Relat'#243'rio de Corte Simples'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -42
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 1316
      Height = 61
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Transparent = True
      ExplicitHeight = 60
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 63
    Width = 1288
    Height = 608
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object Panel2: TPanel
      Left = 0
      Top = 0
      Width = 1288
      Height = 216
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Panel5: TPanel
        Left = 0
        Top = 0
        Width = 634
        Height = 216
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object Panel10: TPanel
          Left = 0
          Top = 0
          Width = 634
          Height = 101
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object LaPCIEmp: TLabel
            Left = 10
            Top = 10
            Width = 45
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Empresa:'
          end
          object LaPCICen: TLabel
            Left = 10
            Top = 42
            Width = 80
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Centro de estq.:'
          end
          object LaPCIPrd: TLabel
            Left = 10
            Top = 73
            Width = 77
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Grupo de prod.:'
          end
          object EdPCIEmp: TdmkEditCB
            Left = 126
            Top = 5
            Width = 62
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdPCIEmpChange
            DBLookupComboBox = CBPCIEmp
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object EdPCICen: TdmkEditCB
            Left = 126
            Top = 37
            Width = 62
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Cliente'
            UpdCampo = 'Cliente'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdPCICenChange
            DBLookupComboBox = CBPCICen
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object EdPCIPrd: TdmkEditCB
            Left = 126
            Top = 68
            Width = 62
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdPCIPrdChange
            DBLookupComboBox = CBPCIPrd
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBPCIPrd: TdmkDBLookupComboBox
            Left = 188
            Top = 68
            Width = 440
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'CodUsu'
            ListField = 'Nome'
            TabOrder = 5
            dmkEditCB = EdPCIPrd
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object CBPCICen: TdmkDBLookupComboBox
            Left = 188
            Top = 37
            Width = 440
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'CodUsu'
            ListField = 'Nome'
            TabOrder = 3
            dmkEditCB = EdPCICen
            QryCampo = 'Cliente'
            UpdType = utNil
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object CBPCIEmp: TdmkDBLookupComboBox
            Left = 188
            Top = 5
            Width = 440
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Filial'
            ListField = 'NOMEENT'
            TabOrder = 1
            dmkEditCB = EdPCIEmp
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
        end
        object Panel7: TPanel
          Left = 0
          Top = 101
          Width = 634
          Height = 47
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object RGOrdemGru: TRadioGroup
            Left = 0
            Top = 0
            Width = 310
            Height = 47
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alLeft
            Caption = ' Ordena'#231#227'o dos grupos de produtos: '
            Columns = 2
            ItemIndex = 1
            Items.Strings = (
              'Codigo'
              'Nome')
            TabOrder = 0
            OnClick = RGOrdemGruClick
          end
          object RGOrdemCor: TRadioGroup
            Left = 310
            Top = 0
            Width = 319
            Height = 47
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alLeft
            Caption = ' Ordena'#231#227'o das cores: '
            Columns = 2
            ItemIndex = 1
            Items.Strings = (
              'Codigo'
              'Nome')
            TabOrder = 1
            OnClick = RGOrdemCorClick
          end
        end
        object Panel8: TPanel
          Left = 0
          Top = 148
          Width = 634
          Height = 68
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 2
          object LaAviso1: TLabel
            Left = 0
            Top = 50
            Width = 634
            Height = 18
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alBottom
            Alignment = taCenter
            Caption = '...'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -15
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            ExplicitWidth = 15
          end
          object Panel11: TPanel
            Left = 0
            Top = 0
            Width = 268
            Height = 50
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object CkSemNegativos: TCheckBox
              Left = 5
              Top = 4
              Width = 249
              Height = 22
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Desconsiderar estq. negativos.'
              Checked = True
              State = cbChecked
              TabOrder = 0
              OnClick = CkSemNegativosClick
            end
            object CkPrdGrupo: TCheckBox
              Left = 5
              Top = 26
              Width = 249
              Height = 22
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Sintetizar por grupo de produto'
              TabOrder = 1
              OnClick = CkPrdGrupoClick
            end
          end
          object RGMostrar: TRadioGroup
            Left = 268
            Top = 0
            Width = 366
            Height = 50
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            Caption = ' Mostrar: '
            Columns = 3
            ItemIndex = 1
            Items.Strings = (
              'Tudo'
              'Movimentados'
              'A produzir')
            TabOrder = 1
            OnClick = RGMostrarClick
          end
        end
      end
      object GroupBox5: TGroupBox
        Left = 634
        Top = 0
        Width = 654
        Height = 216
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        Caption = ' Pedidos de Venda: '
        TabOrder = 1
        object GroupBox1: TGroupBox
          Left = 5
          Top = 21
          Width = 319
          Height = 85
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Per'#237'odo de inclus'#227'o: '
          TabOrder = 0
          object TPIncluIni: TdmkEditDateTimePicker
            Left = 10
            Top = 47
            Width = 147
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Date = 39892.420627939810000000
            Time = 39892.420627939810000000
            TabOrder = 1
            OnClick = CkIncluIniClick
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object TPIncluFim: TdmkEditDateTimePicker
            Left = 162
            Top = 47
            Width = 147
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Date = 39892.420627939810000000
            Time = 39892.420627939810000000
            TabOrder = 3
            OnClick = CkIncluIniClick
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object CkIncluIni: TCheckBox
            Left = 10
            Top = 21
            Width = 101
            Height = 22
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Data inicial:'
            Checked = True
            Enabled = False
            State = cbChecked
            TabOrder = 0
            OnClick = CkIncluIniClick
          end
          object CkIncluFim: TCheckBox
            Left = 162
            Top = 21
            Width = 101
            Height = 22
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Data final:'
            Checked = True
            Enabled = False
            State = cbChecked
            TabOrder = 2
            OnClick = CkIncluIniClick
          end
        end
        object GroupBox2: TGroupBox
          Left = 330
          Top = 21
          Width = 319
          Height = 85
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Per'#237'odo de emiss'#227'o: '
          TabOrder = 1
          object TPEmissIni: TdmkEditDateTimePicker
            Left = 10
            Top = 47
            Width = 147
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Date = 39892.420627939810000000
            Time = 39892.420627939810000000
            TabOrder = 1
            OnClick = CkIncluIniClick
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object TPEmissFim: TdmkEditDateTimePicker
            Left = 162
            Top = 47
            Width = 147
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Date = 39892.420627939810000000
            Time = 39892.420627939810000000
            TabOrder = 3
            OnClick = CkIncluIniClick
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object CkEmissIni: TCheckBox
            Left = 10
            Top = 21
            Width = 101
            Height = 22
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Data inicial:'
            Checked = True
            Enabled = False
            State = cbChecked
            TabOrder = 0
            OnClick = CkIncluIniClick
          end
          object CkEmissFim: TCheckBox
            Left = 162
            Top = 21
            Width = 101
            Height = 22
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Data final:'
            Checked = True
            Enabled = False
            State = cbChecked
            TabOrder = 2
            OnClick = CkIncluIniClick
          end
        end
        object GroupBox3: TGroupBox
          Left = 5
          Top = 110
          Width = 319
          Height = 85
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Per'#237'odo de chegada: '
          TabOrder = 2
          object TPEntraIni: TdmkEditDateTimePicker
            Left = 10
            Top = 47
            Width = 147
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Date = 39892.420627939810000000
            Time = 39892.420627939810000000
            TabOrder = 1
            OnClick = CkIncluIniClick
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object TPEntraFim: TdmkEditDateTimePicker
            Left = 162
            Top = 47
            Width = 147
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Date = 39892.420627939810000000
            Time = 39892.420627939810000000
            TabOrder = 3
            OnClick = CkIncluIniClick
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object CkEntraIni: TCheckBox
            Left = 10
            Top = 21
            Width = 101
            Height = 22
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Data inicial:'
            Checked = True
            Enabled = False
            State = cbChecked
            TabOrder = 0
            OnClick = CkIncluIniClick
          end
          object CkEntraFim: TCheckBox
            Left = 162
            Top = 21
            Width = 101
            Height = 22
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Data final:'
            Checked = True
            Enabled = False
            State = cbChecked
            TabOrder = 2
            OnClick = CkIncluIniClick
          end
        end
        object GroupBox4: TGroupBox
          Left = 330
          Top = 110
          Width = 319
          Height = 85
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Per'#237'odo de previs'#227'o de entrega: '
          TabOrder = 3
          object TPPreviIni: TdmkEditDateTimePicker
            Left = 10
            Top = 47
            Width = 147
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Date = 39892.420627939810000000
            Time = 39892.420627939810000000
            TabOrder = 1
            OnClick = CkIncluIniClick
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object TPPreviFim: TdmkEditDateTimePicker
            Left = 162
            Top = 47
            Width = 147
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Date = 39892.420627939810000000
            Time = 39892.420627939810000000
            TabOrder = 3
            OnClick = CkIncluIniClick
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object CkPreviIni: TCheckBox
            Left = 10
            Top = 21
            Width = 101
            Height = 22
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Data inicial:'
            Checked = True
            Enabled = False
            State = cbChecked
            TabOrder = 0
            OnClick = CkIncluIniClick
          end
          object CkPreviFim: TCheckBox
            Left = 162
            Top = 21
            Width = 101
            Height = 22
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Data final:'
            Checked = True
            Enabled = False
            State = cbChecked
            TabOrder = 2
            OnClick = CkIncluIniClick
          end
        end
      end
    end
    object Panel9: TPanel
      Left = 0
      Top = 216
      Width = 1288
      Height = 392
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object DBGrid1: TDBGrid
        Left = 0
        Top = 0
        Width = 1288
        Height = 392
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        DataSource = DsGgx
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        OnDrawColumnCell = DBGrid1DrawColumnCell
        Columns = <
          item
            Expanded = False
            FieldName = 'CU_NIVEL1'
            Title.Alignment = taCenter
            Title.Caption = 'Grupo'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_NIVEL1'
            Title.Alignment = taCenter
            Title.Caption = 'Descri'#231#227'o do grupo'
            Width = 212
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CU_COR'
            Title.Alignment = taCenter
            Title.Caption = 'Cor'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_COR'
            Title.Alignment = taCenter
            Title.Caption = 'Descri'#231#227'o da cor'
            Width = 140
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_TAM'
            Title.Alignment = taCenter
            Title.Caption = 'Tamanho'
            Width = 52
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Estq'
            Title.Alignment = taCenter
            Title.Caption = 'Estoque'
            Width = 58
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pedv'
            Title.Alignment = taCenter
            Title.Caption = 'Pedido'
            Width = 58
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nec1'
            Title.Alignment = taCenter
            Title.Caption = 'Saldo 1'
            Width = 58
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Comp'
            Title.Alignment = taCenter
            Title.Caption = 'Compras'
            Width = 58
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Prdz'
            Title.Alignment = taCenter
            Title.Caption = 'Produ'#231#227'o'
            Width = 58
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nec2'
            Title.Alignment = taCenter
            Title.Caption = 'A produzir'
            Width = 58
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Sobr'
            Title.Alignment = taCenter
            Title.Caption = 'Sobras'
            Width = 58
            Visible = True
          end>
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 671
    Width = 1318
    Height = 63
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 4
    object Label1: TLabel
      Left = 664
      Top = 5
      Width = 36
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Tempo:'
    end
    object BtConfirma: TBitBtn
      Tag = 20
      Left = 26
      Top = 5
      Width = 118
      Height = 53
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Pesquisa'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtConfirmaClick
    end
    object Panel4: TPanel
      Left = 1172
      Top = 1
      Width = 145
      Height = 61
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtDesiste: TBitBtn
        Tag = 13
        Left = 3
        Top = 4
        Width = 117
        Height = 52
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sair'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtDesisteClick
      end
    end
    object BtImprime: TBitBtn
      Tag = 5
      Left = 147
      Top = 5
      Width = 118
      Height = 53
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Imprime'
      Enabled = False
      NumGlyphs = 2
      TabOrder = 2
      OnClick = BtImprimeClick
    end
    object BtLimpa: TBitBtn
      Tag = 20
      Left = 267
      Top = 5
      Width = 117
      Height = 53
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Limpa'
      NumGlyphs = 2
      TabOrder = 3
      OnClick = BtLimpaClick
    end
    object CkRolar: TCheckBox
      Left = 398
      Top = 10
      Width = 215
      Height = 23
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Rolar itens ao gerar relat'#243'rio.'
      TabOrder = 4
    end
    object EdTempo: TdmkEdit
      Left = 664
      Top = 26
      Width = 127
      Height = 28
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Enabled = False
      TabOrder = 5
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfLong
      Texto = '00:00:00:000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = '00:00:00:000'
      ValWarn = False
    end
  end
  object frxPED_INPRI_000_01: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39596.679376979200000000
    ReportOptions.LastChange = 39596.679376979200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'var'
      '  Fmt: String;                                  '
      'begin'
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      '  //        '
      '  Fmt := <VARF_FMT>;                        '
      
        '  Memo17.Memo.Text := '#39'[FormatFloat('#39#39#39' + Fmt + '#39#39#39',<frxDsGgx."E' +
        'stq">)]'#39';'
      
        '  Memo10.Memo.Text := '#39'[FormatFloat('#39#39#39' + Fmt + '#39#39#39',<frxDsGgx."P' +
        'edv">)]'#39';'
      
        '  Memo9.Memo.Text  := '#39'[FormatFloat('#39#39#39' + Fmt + '#39#39#39',<frxDsGgx."N' +
        'ec1">)]'#39';'
      
        '  Memo12.Memo.Text := '#39'[FormatFloat('#39#39#39' + Fmt + '#39#39#39',<frxDsGgx."C' +
        'omp">)]'#39';'
      
        '  Memo8.Memo.Text  := '#39'[FormatFloat('#39#39#39' + Fmt + '#39#39#39',<frxDsGgx."P' +
        'rdz">)]'#39';'
      
        '  Memo42.Memo.Text := '#39'[FormatFloat('#39#39#39' + Fmt + '#39#39#39',<frxDsGgx."N' +
        'ec2">)]'#39';'
      
        '  Memo23.Memo.Text := '#39'[FormatFloat('#39#39#39' + Fmt + '#39#39#39',<frxDsGgx."S' +
        'obr">)]'#39';'
      '  //'
      
        '  Memo40.Memo.Text := '#39'[FormatFloat('#39#39#39' + Fmt +'#39#39#39',SUM(<frxDsGgx' +
        '."Estq">))]'#39';'
      
        '  Memo38.Memo.Text := '#39'[FormatFloat('#39#39#39' + Fmt +'#39#39#39',SUM(<frxDsGgx' +
        '."Pedv">))]'#39';'
      
        '  Memo37.Memo.Text := '#39'[FormatFloat('#39#39#39' + Fmt +'#39#39#39',SUM(<frxDsGgx' +
        '."Nec1">))]'#39';'
      
        '  Memo39.Memo.Text := '#39'[FormatFloat('#39#39#39' + Fmt +'#39#39#39',SUM(<frxDsGgx' +
        '."Comp">))]'#39';'
      
        '  Memo25.Memo.Text := '#39'[FormatFloat('#39#39#39' + Fmt +'#39#39#39',SUM(<frxDsGgx' +
        '."Prdz">))]'#39';'
      
        '  Memo43.Memo.Text := '#39'[FormatFloat('#39#39#39' + Fmt +'#39#39#39',SUM(<frxDsGgx' +
        '."Nec2">))]'#39';'
      
        '  Memo24.Memo.Text := '#39'[FormatFloat('#39#39#39' + Fmt +'#39#39#39',SUM(<frxDsGgx' +
        '."Sobr">))]'#39';'
      '  //        '
      'end.')
    OnGetValue = frxPED_INPRI_000_01GetValue
    Left = 692
    Top = 8
    Datasets = <
      item
      end
      item
        DataSet = frxDsGgx
        DataSetName = 'frxDsGgx'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 41.574830000000000000
        Top = 56.692950000000010000
        Width = 699.213050000000000000
        object Shape1: TfrxShapeView
          Width = 699.213050000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          Left = 7.559060000000000000
          Width = 684.094930000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Top = 18.897650000000010000
          Width = 699.213050000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          Left = 120.944960000000000000
          Top = 18.897650000000010000
          Width = 457.323130000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'CORTE SIMPLES')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000010000
          Width = 113.385900000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 578.267716540000000000
          Top = 18.897650000000010000
          Width = 113.385826770000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 18.897650000000000000
        Width = 699.213050000000000000
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 15.118110236220470000
        Top = 306.141930000000000000
        Width = 699.213050000000000000
        DataSet = frxDsGgx
        DataSetName = 'frxDsGgx'
        RowCount = 0
        object Memo3: TfrxMemoView
          Left = 45.354360000000000000
          Width = 60.472480000000000000
          Height = 15.118110236220470000
          DataSet = frxDsGgx
          DataSetName = 'frxDsGgx'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsGgx."NO_TAM"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Left = 604.724800000000000000
          Width = 94.488196300000000000
          Height = 15.118110236220470000
          DataSet = frxDsGgx
          DataSetName = 'frxDsGgx'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 438.425480000000000000
          Width = 83.149606300000000000
          Height = 15.118110236220470000
          DataSet = frxDsGgx
          DataSetName = 'frxDsGgx'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 272.126160000000000000
          Width = 83.149606300000000000
          Height = 15.118110236220470000
          DataSet = frxDsGgx
          DataSetName = 'frxDsGgx'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 188.976500000000000000
          Width = 83.149606300000000000
          Height = 15.118110236220470000
          DataSet = frxDsGgx
          DataSetName = 'frxDsGgx'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Left = 355.275820000000000000
          Width = 83.149606300000000000
          Height = 15.118110236220470000
          DataSet = frxDsGgx
          DataSetName = 'frxDsGgx'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 105.826840000000000000
          Width = 83.149606300000000000
          Height = 15.118110236220470000
          DataSet = frxDsGgx
          DataSetName = 'frxDsGgx'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Left = 521.575140000000000000
          Width = 83.149606300000000000
          Height = 15.118110236220470000
          DataSet = frxDsGgx
          DataSetName = 'frxDsGgx'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 449.764070000000000000
        Width = 699.213050000000000000
        object Memo1: TfrxMemoView
          Left = 7.559060000000000000
          Width = 362.834880000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'www.dermatek.com.br - software customizado')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 370.393940000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData3: TfrxMasterData
        FillType = ftBrush
        Height = 30.125996460000000000
        Top = 158.740260000000000000
        Width = 699.213050000000000000
        RowCount = 1
        object Memo29: TfrxMemoView
          Width = 177.637795280000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsPCI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data de inclus'#227'o')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Top = 13.118120000000000000
          Width = 177.637795280000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsPCI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PERIODO_INCLU]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 177.637910000000000000
          Width = 173.858267720000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsPCI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data de emiss'#227'o')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 177.637910000000000000
          Top = 13.118120000000000000
          Width = 173.858267720000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsPCI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PERIODO_EMISS]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 351.496290000000000000
          Width = 173.858267720000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsPCI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data de chegada')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 351.496290000000000000
          Top = 13.118120000000000000
          Width = 173.858267720000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsPCI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PERIODO_ENTRA]')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 525.354670000000000000
          Width = 173.858267720000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsPCI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Previs'#227'o de entrega')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 525.354670000000000000
          Top = 13.118120000000000000
          Width = 173.858267720000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsPCI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PERIODO_PREVI]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Height = 20.787404020000000000
        Top = 211.653680000000000000
        Width = 699.213050000000000000
        Condition = 'frxDsGgx."CU_NIVEL1"'
        object Memo2: TfrxMemoView
          Top = 3.779529999999994000
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Grupo:')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          Left = 37.795300000000000000
          Top = 3.779529999999994000
          Width = 94.488250000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsGgx."CU_NIVEL1"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          Left = 132.283550000000000000
          Top = 3.779529999999994000
          Width = 510.236550000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsGgx."NO_NIVEL1"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Height = 7.559060000000000000
        Top = 381.732530000000000000
        Width = 699.213050000000000000
        object Memo20: TfrxMemoView
          Width = 699.213050000000000000
          Height = 7.559060000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 343.937230000000000000
        Width = 699.213050000000000000
        object Memo22: TfrxMemoView
          Left = 45.354360000000000000
          Width = 60.472480000000000000
          Height = 15.118110236220470000
          DataSet = frxDsGgx
          DataSetName = 'frxDsGgx'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'T O T A L')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Left = 604.724800000000000000
          Width = 94.488196300000000000
          Height = 15.118110236220470000
          DataSet = frxDsGgx
          DataSetName = 'frxDsGgx'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Left = 438.425480000000000000
          Width = 83.149606300000000000
          Height = 15.118110236220470000
          DataSet = frxDsGgx
          DataSetName = 'frxDsGgx'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Left = 272.126160000000000000
          Width = 83.149606300000000000
          Height = 15.118110236220470000
          DataSet = frxDsGgx
          DataSetName = 'frxDsGgx'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Left = 188.976500000000000000
          Width = 83.149606300000000000
          Height = 15.118110236220470000
          DataSet = frxDsGgx
          DataSetName = 'frxDsGgx'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Left = 355.275820000000000000
          Width = 83.149606300000000000
          Height = 15.118110236220470000
          DataSet = frxDsGgx
          DataSetName = 'frxDsGgx'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 105.826840000000000000
          Width = 83.149606300000000000
          Height = 15.118110236220470000
          DataSet = frxDsGgx
          DataSetName = 'frxDsGgx'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Left = 521.575140000000000000
          Width = 83.149606300000000000
          Height = 15.118110236220470000
          DataSet = frxDsGgx
          DataSetName = 'frxDsGgx'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        Height = 26.456690480000000000
        Top = 257.008040000000000000
        Width = 699.213050000000000000
        Condition = 'frxDsGgx."CU_COR"'
        object Memo11: TfrxMemoView
          Left = 45.354360000000000000
          Top = 13.228344020000010000
          Width = 60.472480000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Tamanho')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Left = 272.126160000000000000
          Top = 13.228344020000010000
          Width = 83.149606300000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Saldo 1')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 438.425480000000000000
          Top = 13.228344020000010000
          Width = 83.149606300000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Produ'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 604.724800000000000000
          Top = 13.228344020000010000
          Width = 94.488196300000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Sobras')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 37.795300000000000000
          Width = 56.692950000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsGgx."CU_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 94.488250000000000000
          Width = 548.031850000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsGgx."NO_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 105.826840000000000000
          Top = 13.228344020000010000
          Width = 83.149606300000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Estoque')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Left = 355.275820000000000000
          Top = 13.228344020000010000
          Width = 83.149606300000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Compras')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Left = 188.976500000000000000
          Top = 13.228344020000010000
          Width = 83.149606300000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pedido pendente')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Left = 521.575140000000000000
          Top = 13.228346460000010000
          Width = 83.149606300000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'A produzir')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          Left = 15.118120000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cor:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object QrGgx: TMySQLQuery
    Database = DModG.MyPID_DB
    AfterOpen = QrGgxAfterOpen
    BeforeClose = QrGgxBeforeClose
    SQL.Strings = (
      'SELECT gg1.CodUsu CU_NIVEL1, gg1.Nome NO_NIVEL1,'
      'gti.Controle CO_TAM, gti.Nome NO_TAM,'
      'gcc.CodUsu CU_COR, gcc.Nome NO_COR, pci.*'
      'FROM pci1 pci'
      'LEFT JOIN planning.gragru1 gg1 ON gg1.Nivel1=pci.GraGru1'
      'LEFT JOIN planning.gratamits gti ON gti.Controle=pci.GraTamI'
      'LEFT JOIN planning.gratamcad gtc ON gtc.Codigo=gti.Codigo'
      'LEFT JOIN planning.gragruc   ggc ON ggc.Controle=pci.GraGruC'
      'LEFT JOIN planning.gracorcad gcc ON gcc.Codigo=ggc.GraCorCad'
      'ORDER BY NO_NIVEL1, NO_NIVEL1, CO_TAM')
    Left = 720
    Top = 8
    object QrGgxCU_NIVEL1: TIntegerField
      FieldName = 'CU_NIVEL1'
    end
    object QrGgxNO_NIVEL1: TWideStringField
      FieldName = 'NO_NIVEL1'
      Size = 30
    end
    object QrGgxCO_TAM: TIntegerField
      FieldName = 'CO_TAM'
    end
    object QrGgxNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object QrGgxCU_COR: TIntegerField
      FieldName = 'CU_COR'
    end
    object QrGgxNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrGgxGraGruC: TIntegerField
      FieldName = 'GraGruC'
      Required = True
    end
    object QrGgxGraGru1: TIntegerField
      FieldName = 'GraGru1'
      Required = True
    end
    object QrGgxGraTamI: TIntegerField
      FieldName = 'GraTamI'
      Required = True
    end
    object QrGgxControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrGgxEstq: TFloatField
      FieldName = 'Estq'
      Required = True
    end
    object QrGgxPedv: TFloatField
      FieldName = 'Pedv'
      Required = True
    end
    object QrGgxNec1: TFloatField
      FieldName = 'Nec1'
      Required = True
    end
    object QrGgxComp: TFloatField
      FieldName = 'Comp'
      Required = True
    end
    object QrGgxPrdz: TFloatField
      FieldName = 'Prdz'
      Required = True
    end
    object QrGgxNec2: TFloatField
      FieldName = 'Nec2'
      Required = True
    end
    object QrGgxSobr: TFloatField
      FieldName = 'Sobr'
      Required = True
    end
    object QrGgxAtivo: TIntegerField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object DsGgx: TDataSource
    DataSet = QrGgx
    Left = 748
    Top = 8
  end
  object frxDsGgx: TfrxDBDataset
    UserName = 'frxDsGgx'
    CloseDataSource = False
    DataSet = QrGgx
    BCDToCurrency = False
    Left = 776
    Top = 8
  end
  object frxPED_INPRI_000_02: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39596.679376979200000000
    ReportOptions.LastChange = 42282.455221666660000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'var'
      '  Fmt: String;                                  '
      'begin'
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      '  //        '
      '  Fmt := <VARF_FMT>;                        '
      
        '  Memo17.Memo.Text := '#39'[FormatFloat('#39#39#39' + Fmt + '#39#39#39',<frxDsGgx."E' +
        'stq">)]'#39';'
      
        '  Memo10.Memo.Text := '#39'[FormatFloat('#39#39#39' + Fmt + '#39#39#39',<frxDsGgx."P' +
        'edv">)]'#39';'
      
        '  Memo9.Memo.Text  := '#39'[FormatFloat('#39#39#39' + Fmt + '#39#39#39',<frxDsGgx."N' +
        'ec1">)]'#39';'
      
        '  Memo12.Memo.Text := '#39'[FormatFloat('#39#39#39' + Fmt + '#39#39#39',<frxDsGgx."C' +
        'omp">)]'#39';'
      
        '  Memo8.Memo.Text  := '#39'[FormatFloat('#39#39#39' + Fmt + '#39#39#39',<frxDsGgx."P' +
        'rdz">)]'#39';'
      
        '  Memo42.Memo.Text := '#39'[FormatFloat('#39#39#39' + Fmt + '#39#39#39',<frxDsGgx."N' +
        'ec2">)]'#39';'
      
        '  Memo23.Memo.Text := '#39'[FormatFloat('#39#39#39' + Fmt + '#39#39#39',<frxDsGgx."S' +
        'obr">)]'#39';'
      
        '  //                                                            ' +
        '                                    '
      
        '  Memo40.Memo.Text := '#39'[FormatFloat('#39#39#39' + Fmt +'#39#39#39',SUM(<frxDsGgx' +
        '."Estq">, MasterData1))]'#39';'
      
        '  Memo38.Memo.Text := '#39'[FormatFloat('#39#39#39' + Fmt +'#39#39#39',SUM(<frxDsGgx' +
        '."Pedv">, MasterData1))]'#39';'
      
        '  Memo37.Memo.Text := '#39'[FormatFloat('#39#39#39' + Fmt +'#39#39#39',SUM(<frxDsGgx' +
        '."Nec1">, MasterData1))]'#39';'
      
        '  Memo39.Memo.Text := '#39'[FormatFloat('#39#39#39' + Fmt +'#39#39#39',SUM(<frxDsGgx' +
        '."Comp">, MasterData1))]'#39';'
      
        '  Memo25.Memo.Text := '#39'[FormatFloat('#39#39#39' + Fmt +'#39#39#39',SUM(<frxDsGgx' +
        '."Prdz">, MasterData1))]'#39';'
      
        '  Memo43.Memo.Text := '#39'[FormatFloat('#39#39#39' + Fmt +'#39#39#39',SUM(<frxDsGgx' +
        '."Nec2">, MasterData1))]'#39';'
      
        '  Memo24.Memo.Text := '#39'[FormatFloat('#39#39#39' + Fmt +'#39#39#39',SUM(<frxDsGgx' +
        '."Sobr">, MasterData1))]'#39';      '
      'end.')
    OnGetValue = frxPED_INPRI_000_01GetValue
    Left = 276
    Top = 376
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsGgx
        DataSetName = 'frxDsGgx'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 41.574830000000000000
        Top = 56.692950000000010000
        Width = 699.213050000000000000
        object Shape1: TfrxShapeView
          Width = 699.213050000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          Left = 7.559060000000000000
          Width = 684.094930000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Top = 18.897649999999990000
          Width = 699.213050000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          Left = 120.944960000000000000
          Top = 18.897649999999990000
          Width = 457.323130000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'CORTE SIMPLES')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897649999999990000
          Width = 113.385900000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 578.267716540000000000
          Top = 18.897649999999990000
          Width = 113.385826770000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 18.897650000000000000
        Width = 699.213050000000000000
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 257.008040000000000000
        Width = 699.213050000000000000
        DataSet = frxDsGgx
        DataSetName = 'frxDsGgx'
        RowCount = 0
        object Memo23: TfrxMemoView
          Left = 631.181510000000000000
          Width = 68.031486300000000000
          Height = 15.118110240000000000
          DataSet = frxDsGgx
          DataSetName = 'frxDsGgx'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 495.118430000000000000
          Width = 68.031486300000000000
          Height = 15.118110240000000000
          DataSet = frxDsGgx
          DataSetName = 'frxDsGgx'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 359.055350000000000000
          Width = 68.031486300000000000
          Height = 15.118110240000000000
          DataSet = frxDsGgx
          DataSetName = 'frxDsGgx'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 291.023810000000000000
          Width = 68.031486300000000000
          Height = 15.118110240000000000
          DataSet = frxDsGgx
          DataSetName = 'frxDsGgx'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Left = 427.086890000000000000
          Width = 68.031486300000000000
          Height = 15.118110240000000000
          DataSet = frxDsGgx
          DataSetName = 'frxDsGgx'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 222.992270000000000000
          Width = 68.031486300000000000
          Height = 15.118110240000000000
          DataSet = frxDsGgx
          DataSetName = 'frxDsGgx'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Left = 563.149970000000100000
          Width = 68.031486300000000000
          Height = 15.118110240000000000
          DataSet = frxDsGgx
          DataSetName = 'frxDsGgx'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 68.031540000000010000
          Width = 154.960730000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsGgx."NO_NIVEL1"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          Width = 68.031540000000010000
          Height = 15.118110240000000000
          DataField = 'CU_NIVEL1'
          DataSet = frxDsGgx
          DataSetName = 'frxDsGgx'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsGgx."CU_NIVEL1"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 393.071120000000000000
        Width = 699.213050000000000000
        object Memo1: TfrxMemoView
          Left = 7.559060000000000000
          Width = 362.834880000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'www.dermatek.com.br - software customizado')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 370.393940000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData3: TfrxMasterData
        FillType = ftBrush
        Height = 26.346466460000000000
        Top = 158.740260000000000000
        Width = 699.213050000000000000
        RowCount = 1
        object Memo29: TfrxMemoView
          Width = 177.637795280000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsPCI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data de inclus'#227'o')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Top = 13.118120000000000000
          Width = 177.637795280000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsPCI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PERIODO_INCLU]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 177.637910000000000000
          Width = 173.858267720000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsPCI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data de emiss'#227'o')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 177.637910000000000000
          Top = 13.118120000000000000
          Width = 173.858267720000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsPCI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PERIODO_EMISS]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 351.496290000000000000
          Width = 173.858267720000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsPCI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data de chegada')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 351.496290000000000000
          Top = 13.118120000000000000
          Width = 173.858267720000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsPCI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PERIODO_ENTRA]')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 525.354670000000000000
          Width = 173.858267720000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsPCI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Previs'#227'o de entrega')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 525.354670000000000000
          Top = 13.118120000000000000
          Width = 173.858267720000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsPCI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PERIODO_PREVI]')
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Height = 26.456692920000000000
        Top = 207.874150000000000000
        Width = 699.213050000000000000
        object Memo26: TfrxMemoView
          Left = 359.055350000000000000
          Top = 13.228346460000010000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Saldo 1')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 495.118430000000000000
          Top = 13.228346460000010000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Produ'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 631.181510000000000000
          Top = 13.228346460000010000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Sobras')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 222.992270000000000000
          Top = 13.228346460000010000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Estoque')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Left = 427.086890000000000000
          Top = 13.228346460000010000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Compras')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Left = 291.023810000000000000
          Top = 13.228346460000010000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pedido pendente')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Left = 563.149970000000100000
          Top = 13.228346460000010000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'A produzir')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Top = 13.228346460000010000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          Left = 68.031540000000010000
          Top = 13.228346460000010000
          Width = 154.960730000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o / Nome')
          ParentFont = False
          WordWrap = False
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        Top = 294.803340000000000000
        Width = 699.213050000000000000
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 355.275820000000000000
        Width = 699.213050000000000000
        object Memo22: TfrxMemoView
          Width = 222.992270000000000000
          Height = 15.118110240000000000
          DataSet = frxDsGgx
          DataSetName = 'frxDsGgx'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'T O T A L')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Left = 631.181510000000000000
          Width = 68.031486300000000000
          Height = 15.118110240000000000
          DataSet = frxDsGgx
          DataSetName = 'frxDsGgx'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Left = 495.118430000000000000
          Width = 68.031486300000000000
          Height = 15.118110240000000000
          DataSet = frxDsGgx
          DataSetName = 'frxDsGgx'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Left = 359.055350000000000000
          Width = 68.031486300000000000
          Height = 15.118110240000000000
          DataSet = frxDsGgx
          DataSetName = 'frxDsGgx'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Left = 291.023810000000000000
          Width = 68.031486300000000000
          Height = 15.118110240000000000
          DataSet = frxDsGgx
          DataSetName = 'frxDsGgx'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Left = 427.086890000000000000
          Width = 68.031486300000000000
          Height = 15.118110240000000000
          DataSet = frxDsGgx
          DataSetName = 'frxDsGgx'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 222.992270000000000000
          Width = 68.031486300000000000
          Height = 15.118110240000000000
          DataSet = frxDsGgx
          DataSetName = 'frxDsGgx'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Left = 563.149970000000100000
          Width = 68.031486300000000000
          Height = 15.118110240000000000
          DataSet = frxDsGgx
          DataSetName = 'frxDsGgx'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
end
