unit EPC_Argox_PPLA;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, dmkEdit, ComCtrls, Menus, DB,
  Grids, DBGrids, dmkGeral, UnDmkProcFunc, UnDmkEnums, UnDmkABS_PF, ABSMain;

type
  TFmEPC_Argox_PPLA = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Panel4: TPanel;
    BtAcao: TBitBtn;
    CkDOS: TCheckBox;
    CkTodoLabel: TCheckBox;
    CkLinhaALinha: TCheckBox;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel5: TPanel;
    RGOrienta_Txt: TRadioGroup;
    RGTipFont_Txt: TRadioGroup;
    RGLargMul_Txt: TRadioGroup;
    TabSheet2: TTabSheet;
    Panel6: TPanel;
    Panel3: TPanel;
    RGCodeBar_Bar: TRadioGroup;
    Panel9: TPanel;
    RGCodBarE_Bar: TRadioGroup;
    RGShowTxt_Bar: TRadioGroup;
    RGOrienta_Bar: TRadioGroup;
    Panel10: TPanel;
    RGLargMul_Bar: TRadioGroup;
    PMAcao: TPopupMenu;
    Sgera1: TMenuItem;
    Geraeimprime1: TMenuItem;
    Simprime1: TMenuItem;
    PMOK: TPopupMenu;
    AdicionasoTexto1: TMenuItem;
    AdicionasoCdigodeBarras1: TMenuItem;
    AdicionaAmbos1: TMenuItem;
    CkContinuar: TCheckBox;
    Query: TABSQuery;
    DataSource1: TDataSource;
    QueryColuna: TIntegerField;
    QueryLinha: TIntegerField;
    QueryTexto: TWideStringField;
    QueryAtivo: TSmallintField;
    QueryTxtBar: TWideStringField;
    Panel13: TPanel;
    Panel7: TPanel;
    DBGTxt: TDBGrid;
    MeResult: TMemo;
    Panel14: TPanel;
    RGCoor: TGroupBox;
    Label8: TLabel;
    Label9: TLabel;
    LaMedida_Txt: TLabel;
    Label12: TLabel;
    EdTxtTopoPix_Txt: TdmkEdit;
    EdTxtLeftPix_Txt: TdmkEdit;
    EdTxtTopoCm_Txt: TdmkEdit;
    EdTxtLeftCm_Txt: TdmkEdit;
    Panel8: TPanel;
    Label10: TLabel;
    Label4: TLabel;
    EdTexto_Txt: TEdit;
    EdLinha_Txt: TdmkEdit;
    RGRatio: TRadioGroup;
    Panel15: TPanel;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    LaMedida_Bar: TLabel;
    Label5: TLabel;
    Label7: TLabel;
    EdBarAltuCm_Bar: TdmkEdit;
    EdBarAltuPix_Bar: TdmkEdit;
    EdTxtTopoCm_Bar: TdmkEdit;
    EdTxtTopoPix_Bar: TdmkEdit;
    EdTxtLeftCm_Bar: TdmkEdit;
    EdTxtLeftPix_Bar: TdmkEdit;
    Label1: TLabel;
    Label6: TLabel;
    EdLinha_Bar: TdmkEdit;
    EdTexto_Bar: TEdit;
    RGAltuMul_Bar: TRadioGroup;
    RGAltuMul_Txt: TRadioGroup;
    Label11: TLabel;
    Label13: TLabel;
    QueryDataHora: TFloatField;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdBarAltuCm_BarChange(Sender: TObject);
    procedure EdTxtTopoCm_TxtChange(Sender: TObject);
    procedure EdTxtLeftCm_TxtChange(Sender: TObject);
    procedure EdBarAltuPix_BarChange(Sender: TObject);
    procedure EdTxtTopoPix_TxtChange(Sender: TObject);
    procedure EdTxtLeftPix_TxtChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtAcaoClick(Sender: TObject);
    procedure RGCodeBar_BarClick(Sender: TObject);
    procedure RGOrienta_BarClick(Sender: TObject);
    procedure RGShowTxt_BarClick(Sender: TObject);
    procedure RGLargMul_BarClick(Sender: TObject);
    procedure RGAltuMul_BarClick(Sender: TObject);
    procedure RGCodBarE_BarClick(Sender: TObject);
    procedure EdTxtTopoPix_BarChange(Sender: TObject);
    procedure EdTxtLeftPix_BarChange(Sender: TObject);
    procedure RGOrienta_TxtClick(Sender: TObject);
    procedure RGTipFont_TxtClick(Sender: TObject);
    procedure RGLargMul_TxtClick(Sender: TObject);
    procedure RGAltuMul_TxtClick(Sender: TObject);
    procedure EdTxtTopoCm_BarChange(Sender: TObject);
    procedure EdTxtLeftCm_BarChange(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure Sgera1Click(Sender: TObject);
    procedure Geraeimprime1Click(Sender: TObject);
    procedure Simprime1Click(Sender: TObject);
    procedure AdicionasoTexto1Click(Sender: TObject);
    procedure AdicionasoCdigodeBarras1Click(Sender: TObject);
    procedure AdicionaAmbos1Click(Sender: TObject);
    procedure EdTexto_TxtExit(Sender: TObject);
    procedure EdTexto_TxtKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdLinha_TxtKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdLinha_TxtExit(Sender: TObject);
    procedure EdLinha_BarExit(Sender: TObject);
    procedure EdLinha_BarKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdTexto_BarExit(Sender: TObject);
    procedure EdTexto_BarKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
  private
    { Private declarations }
    function MedidaToPos(Medida: Double): Integer;
    function PosToMedida(Posicao: Integer): Double;
    procedure CriaLinhas_Txt();
    procedure CriaLinhas_Bar();
    procedure ImprimeEOuGera(Gera, Imprime: Boolean);
    procedure ImprimeLinhas(MeResult: TMemo; Gera, Imprime: Boolean);
    procedure ImprimeViaWin(MeResult: TMemo);
    procedure ImprimeViaDOS(MeResult: TMemo);
    procedure AdicionaOCodBarGerado();
    procedure AdicionaOTextoGerado();
  public
    { Public declarations }
  end;

  var
  FmEPC_Argox_PPLA: TFmEPC_Argox_PPLA;

implementation

uses UnMyObjects, EtqPrinCad, UnInternalConsts, BarComands, EtqPrinCmd, UMySQLModule,
  EtqPrinVar, MyDBCheck;


{$R *.DFM}

procedure TFmEPC_Argox_PPLA.CriaLinhas_Txt();
var
  Txt_01_1,Txt_02_1,Txt_03_1,Txt_04_1,Txt_05_3,Txt_08_4,Txt_12_4,
  SQL, Texto, Vals, Lin, DH: String;
  i: Integer;
  FatorPosi, LabelMEsq, LabelLarg, LabelLGap, ProxiMEsq, ExtraLarg: Double;
begin
  Query.Close;
  Query.SQL.Clear;
  Query.SQL.Add('DELETE FROM argox_ppla WHERE TxtBar = "TXT";');
  if RGTipFont_Txt.ItemIndex < 11 then // Fonte "?"
  begin
    SQL := 'INSERT INTO argox_ppla (TxtBar,Coluna,Linha,Texto,DataHora,Ativo)' +
           ' Values("TXT",';
    //
    (**MeGeraTxt_Txt.lines.Clear**);
    LabelMEsq := FmEtqPrinCad.QrEtqPrinCadLabelMEsq.Value;
    LabelLarg := FmEtqPrinCad.QrEtqPrinCadLabelLarg.Value;
    LabelLGap := FmEtqPrinCad.QrEtqPrinCadLabelLGap.Value;
    if FmEtqPrinCad.QrEtqPrinCadMeasure.Value = 0 then
      FatorPosi := 100
    else
      FatorPosi := 100;
    for i := 1 to FmEtqPrinCad.QrEtqPrinCadColunas.Value do
    begin
      ProxiMEsq := LabelMEsq + ((i-1) * (LabelLarg + LabelLGap));
      Txt_01_1 := RGOrienta_Txt.Items[RGOrienta_Txt.ItemIndex][1];
      Txt_02_1 := RGTipFont_Txt.Items[RGTipFont_Txt.ItemIndex];
      Txt_03_1 := RGLargMul_Txt.Items[RGLargMul_Txt.ItemIndex];
      Txt_04_1 := RGAltuMul_Txt.Items[RGAltuMul_Txt.ItemIndex];
      Txt_05_3 := '000';//EdBarAltuPix_Txt.Text;
      Txt_08_4 := EdTxtTopoPix_Txt.Text;
      ExtraLarg := Geral.DMV(EdTxtLeftCm_Txt.Text);
      Txt_12_4 := MLAGeral.FTX(Round((ProxiMEsq + ExtraLarg) * FatorPosi), 4, 0, siPositivo);
      //
      Texto := Txt_01_1 + Txt_02_1 + Txt_03_1 + Txt_04_1 + Txt_05_3 + Txt_08_4 + Txt_12_4;
      //
      Texto := Texto + EdTexto_Txt.Text;
      (**MeGeraTxt_Txt.Lines.Add(Texto);**)
      //
      Lin  := dmkPF.FFP(EdLinha_Txt.ValueVariant, 0);
      DH   := dmkPF.FFP(Now(), 15);
      Vals := dmkPF.FFP(i,0) + ',' + Lin + ',"' + Texto + '", ' + DH + ', 1);';
      Query.SQL.Add(SQL + Vals);
    end;
  end;
  Query.SQL.Add('SELECT * FROM argox_ppla;');
  DmkABS_PF.AbreQuery(Query);
end;

procedure TFmEPC_Argox_PPLA.CriaLinhas_Bar();
var
  Txt_01_1,Txt_02_1,Txt_03_1,Txt_04_1,Txt_05_3,Txt_08_4,Txt_12_4,
  SQL, Lin, DH, Vals, Texto, T, ExtraTxt: String;
  i: Integer;
  FatorPosi, LabelMEsq, LabelLarg, LabelLGap, ProxiMEsq, ExtraLarg: Double;
begin
  (**MeGeraTxt_Bar.lines.Clear;**)
  Query.Close;
  Query.SQL.Clear;
  Query.SQL.Add('DELETE FROM argox_ppla WHERE TxtBar = "BAR";');
  if RGCodeBar_Bar.ItemIndex > 0 then // Fonte "?"
  begin
    SQL := 'INSERT INTO argox_ppla (TxtBar,Coluna,Linha,Texto,DataHora,Ativo)' +
           ' Values("BAR",';
    LabelMEsq := FmEtqPrinCad.QrEtqPrinCadLabelMEsq.Value;
    LabelLarg := FmEtqPrinCad.QrEtqPrinCadLabelLarg.Value;
    LabelLGap := FmEtqPrinCad.QrEtqPrinCadLabelLGap.Value;
    if FmEtqPrinCad.QrEtqPrinCadMeasure.Value = 0 then
      FatorPosi := 100
    else
      FatorPosi := 100;
    T := RGCodeBar_Bar.Items[RGCodeBar_Bar.ItemIndex][1];
    if RGShowTxt_Bar.ItemIndex = 0 then
      T := LowerCase(T);
    for i := 1 to FmEtqPrinCad.QrEtqPrinCadColunas.Value do
    begin
      ProxiMEsq := LabelMEsq + ((i-1) * (LabelLarg + LabelLGap));
      Txt_01_1 := RGOrienta_Bar.Items[RGOrienta_Bar.ItemIndex][1];
      Txt_02_1 := T;//RGTipFont_Bar.Items[RGTipFont_Bar.ItemIndex];
      Txt_03_1 := RGLargMul_Bar.Items[RGLargMul_Bar.ItemIndex];
      Txt_04_1 := RGAltuMul_Bar.Items[RGAltuMul_Bar.ItemIndex];
      Txt_05_3 := EdBarAltuPix_Bar.Text;
      Txt_08_4 := EdTxtTopoPix_Bar.Text;
      ExtraLarg := Geral.DMV(EdTxtLeftCm_Bar.Text);
      Txt_12_4 := MLAGeral.FTX(Round((ProxiMEsq + ExtraLarg) * FatorPosi), 4, 0, siPositivo);
      //
      Texto := Txt_01_1 + Txt_02_1 + Txt_03_1 + Txt_04_1 + Txt_05_3 + Txt_08_4 + Txt_12_4;
      //
      ExtraTxt := '';
      case RGCodeBar_Bar.ItemIndex of
        //E:
        5: if RGCodBarE_Bar.ItemIndex <> 1 then
           ExtraTxt := RGCodBarE_Bar.Items[RGCodBarE_Bar.ItemIndex][1];
      end;
      DH   := dmkPF.FFP(Now(), 15);
      Texto := Texto + ExtraTxt + EdTexto_Bar.Text;
      (**MeGeraTxt_Bar.Lines.Add(Texto);**)
      Lin  := dmkPF.FFP(EdLinha_Bar.ValueVariant, 0);
      Vals := dmkPF.FFP(i,0) + ',' + Lin + ',"' + Texto + '",' + DH + ', 1);';
      Query.SQL.Add(SQL + Vals);
    end;
  end;
  Query.SQL.Add('SELECT * FROM argox_ppla;');
  DmkABS_PF.AbreQuery(Query);
end;

function TFmEPC_Argox_PPLA.PosToMedida(Posicao: Integer): Double;
begin
  Result := 0;
  case FmEtqPrinCad.QrEtqPrinCadMeasure.Value of
    0: Result := Posicao / 100;
    1: Result := Posicao / 100 * VAR_POLEGADA;
  end;
end;

procedure TFmEPC_Argox_PPLA.RGAltuMul_BarClick(Sender: TObject);
begin
  CriaLinhas_Bar();
end;

procedure TFmEPC_Argox_PPLA.RGAltuMul_TxtClick(Sender: TObject);
begin
  CriaLinhas_Txt();
end;

procedure TFmEPC_Argox_PPLA.RGCodBarE_BarClick(Sender: TObject);
begin
  CriaLinhas_Bar();
end;

procedure TFmEPC_Argox_PPLA.RGCodeBar_BarClick(Sender: TObject);
  procedure ConfiuraRG_23A(r: TRadioGroup);
  begin
    r.Items.Add('2 : 1 ~');
    r.Items.Add('3 : 1');
    r.Columns := 1;
  end;
  procedure ConfiuraRG_2a4(r: TRadioGroup);
  begin
    r.Items.Add('2');
    r.Items.Add('3');
    r.Items.Add('4');
    r.Columns := 1;
  end;
var
  r: TRadioGroup;
begin
  r := RGRatio;
  r.ItemIndex := -1;
  r.Items.Clear;
  r.Items.Add('0');
  r.ItemIndex := 0;
  case RGCodeBar_Bar.ItemIndex of
    //A: Code 3 of 9
    1: ConfiuraRG_23A(r);
    //B: UPC-A
    2: ConfiuraRG_2a4(r);
    //C: UPC-E
    3: ConfiuraRG_2a4(r);
    //D: Interleaved 2 of 5 (I25)
    4: ConfiuraRG_23A(r);
    //E: Code 128 including subset A, B and C
    5: ConfiuraRG_2a4(r);
    //F: EAN-13
    6: ConfiuraRG_2a4(r);
    //G: EAN-8
    7: ConfiuraRG_2a4(r);
    //H: HBIC
    8: ConfiuraRG_23A(r);
    //I: Coda bar
    9: ConfiuraRG_23A(r);
    //J: Interleaved 2 of 5 with a modulo 10 checksum
   10: ConfiuraRG_23A(r);
    //K: Plessey
   11: ConfiuraRG_23A(r);
    //L: Interleaved 2 of 5 with a modulo 10 checksum and shipping bearer bars
   12: ConfiuraRG_23A(r);
    //M: UPC2
   13: ConfiuraRG_2a4(r);
    //N: UPC5
   14: ConfiuraRG_2a4(r);
    //O: Code 93
   15: ConfiuraRG_2a4(r);
    //P: Postnet
   16: (*NADA*);
    //Q: UCC/EAN Code 128
   17: ConfiuraRG_2a4(r);
    //R: UCC/EAN Code 128 K-MART
   18: ConfiuraRG_2a4(r);
    //T: Telepen
   19: ConfiuraRG_2a4(r);
    //U: UPS MaxiCode
   20: (*NADA*);
    //V: FIM (Facing Identification Mark)
   21: (*NADA*);
    //W: DataMatrix
   22: (*NADA*);
    //Z: PDF-417
   23: (*NADA*);
  end;
  //r := nil;
  CriaLinhas_Bar();
end;

procedure TFmEPC_Argox_PPLA.RGLargMul_BarClick(Sender: TObject);
begin
  CriaLinhas_Bar();
end;

procedure TFmEPC_Argox_PPLA.RGLargMul_TxtClick(Sender: TObject);
begin
  CriaLinhas_Txt();
end;

procedure TFmEPC_Argox_PPLA.RGOrienta_BarClick(Sender: TObject);
begin
  CriaLinhas_Bar();
end;

procedure TFmEPC_Argox_PPLA.RGOrienta_TxtClick(Sender: TObject);
begin
  CriaLinhas_Txt();
end;

procedure TFmEPC_Argox_PPLA.RGShowTxt_BarClick(Sender: TObject);
begin
  CriaLinhas_Bar();
end;

procedure TFmEPC_Argox_PPLA.RGTipFont_TxtClick(Sender: TObject);
begin
  CriaLinhas_Txt();
end;

procedure TFmEPC_Argox_PPLA.Sgera1Click(Sender: TObject);
begin
  ImprimeEOuGera(True, False);
end;

procedure TFmEPC_Argox_PPLA.Simprime1Click(Sender: TObject);
begin
 ImprimeEOuGera(False, True);
end;

function TFmEPC_Argox_PPLA.MedidaToPos(Medida: Double): Integer;
begin
  Result := 0;
  case FmEtqPrinCad.QrEtqPrinCadMeasure.Value of
    0: Result := Round(Medida * 100);
    1: Result := Round(Medida / VAR_POLEGADA * 100);
  end;
end;

procedure TFmEPC_Argox_PPLA.AdicionaAmbos1Click(Sender: TObject);
begin
  AdicionaOCodBarGerado();
  AdicionaOTextoGerado();
end;

procedure TFmEPC_Argox_PPLA.AdicionaOCodBarGerado;
begin
  {**
  FmEtqPrinCmd.MeLinesCmd.Text := FmEtqPrinCmd.MeLinesCmd.Text +
    MeGeraTxt_Bar.Text;
  MeGeraTxt_Bar.Lines.Clear;
  **}
end;

procedure TFmEPC_Argox_PPLA.AdicionaOTextoGerado;
begin
  {**FmEtqPrinCmd.MeLinesCmd.Text := FmEtqPrinCmd.MeLinesCmd.Text +
    MeGeraTxt_Txt.Text;
  MeGeraTxt_Txt.Lines.Clear;
  **}
end;

procedure TFmEPC_Argox_PPLA.AdicionasoCdigodeBarras1Click(Sender: TObject);
begin
  AdicionaOCodBarGerado();
end;

procedure TFmEPC_Argox_PPLA.AdicionasoTexto1Click(Sender: TObject);
begin
  AdicionaOTextoGerado();
end;

procedure TFmEPC_Argox_PPLA.BitBtn1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEtqPrinVar, FmEtqPrinVar, afmoNegarComAviso) then
  begin
    FmEtqPrinVar.CGGrupo.Value := FmEtqPrinCad.QrEtqPrinCadTypeUso.Value + 1;
    FmEtqPrinVar.FTexto := EdTexto_Txt.Text;
    FmEtqPrinVar.FSelStart := EdTexto_Txt.SelStart;
    FmEtqPrinVar.ShowModal;
    EdTexto_Txt.Text := FmEtqPrinVar.FTexto;
    FmEtqPrinVar.Destroy;
  end;
end;

procedure TFmEPC_Argox_PPLA.BitBtn2Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEtqPrinVar, FmEtqPrinVar, afmoNegarComAviso) then
  begin
    FmEtqPrinVar.CGGrupo.Value := FmEtqPrinCad.QrEtqPrinCadTypeUso.Value + 1;
    FmEtqPrinVar.FTexto := EdTexto_Bar.Text;
    FmEtqPrinVar.FSelStart := EdTexto_Bar.SelStart;
    FmEtqPrinVar.ShowModal;
    EdTexto_Bar.Text := FmEtqPrinVar.FTexto;
    FmEtqPrinVar.Destroy;
  end;
end;

procedure TFmEPC_Argox_PPLA.BtAcaoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAcao, BtAcao);
end;

procedure TFmEPC_Argox_PPLA.ImprimeLinhas(MeResult: TMemo;
Gera, Imprime: Boolean);
var
  i: Integer;
  pm: TPrnMeasure;
begin
  if Gera then
  begin
    case FmEtqPrinCad.QrEtqPrinCadMeasure.Value of
      0: pm := pmMetric;
      1: pm := pmInches;
      else pm := pmNone;
    end;
    MeResult.Lines.Clear;
    if FmEtqPrinCad.QrEtqPrinCadDefaultIni.Value = 1 then
      UBarComands.ComandosAutomaticos(bctIni, pplArgoxA, pm, MeResult)
    else begin
      FmEtqPrinCad.QrEtqPrinCmd_0.First;
      while not FmEtqPrinCad.QrEtqPrinCmd_0.Eof do
      begin
        MeResult.Lines.Add(FmEtqPrinCad.QrEtqPrinCmd_0Config.Value);
           //
        FmEtqPrinCad.QrEtqPrinCmd_0.Next;
      end;
    end;
    //
    if CkTodoLabel.Checked then
    begin
      (**for i := 0 to FmEtqPrinCmd.MeLinesCmd.Lines.Count - 1 do
        MeResult.Lines.Add(FmEtqPrinCmd.MeLinesCmd.Lines[i]);
      for i := 0 to MeGeraTxt_Txt.Lines.Count - 1 do
        MeResult.Lines.Add(MeGeraTxt_Txt.Lines[i]);
      for i := 0 to MeGeraTxt_Bar.Lines.Count - 1 do
        MeResult.Lines.Add(MeGeraTxt_Bar.Lines[i]);**)
      for i := 0 to FmEtqPrinCmd.TCLinhas.Tabs.Count - 1 do
      begin
        FmEtqPrinCmd.TCLinhas.TabIndex := i;
        FmEtqPrinCmd.ReopenQuery(0);
        //
        FmEtqPrinCmd.Query.First;
        while not FmEtqPrinCmd.Query.Eof do
        begin
          MeResult.Lines.Add(FmEtqPrinCmd.QueryTexto.Value);
          //
          FmEtqPrinCmd.Query.Next;
        end;
      end;
    end;
    Query.First;
    while not Query.Eof do
    begin
      MeResult.Lines.Add(QueryTexto.Value);
      //
      Query.Next;
    end;
    //
    if FmEtqPrinCad.QrEtqPrinCadDefaultFim.Value = 1 then
      UBarComands.ComandosAutomaticos(bctFim, pplArgoxA, pm, MeResult)
    else begin
      FmEtqPrinCad.QrEtqPrinCmd_2.First;
      while not FmEtqPrinCad.QrEtqPrinCmd_2.Eof do
      begin
        MeResult.Lines.Add(FmEtqPrinCad.QrEtqPrinCmd_2Config.Value);
           //
        FmEtqPrinCad.QrEtqPrinCmd_2.Next;
      end;
    end;
  end;
  //
  if Imprime then
  begin
    if CkDOS.Checked then
      ImprimeViaDOS(MeResult)
    else
      ImprimeViaWin(MeResult);
  end;
end;

procedure TFmEPC_Argox_PPLA.BtOKClick(Sender: TObject);
var
  SQL, Vals: String;
  Linha: Integer;
begin
  if FmEtqPrinCmd.Query.State <> dsInactive then
    Linha := FmEtqPrinCmd.QueryLinha.Value
  else
    Linha := 0;
  FmEtqPrinCmd.Query.Close;
  FmEtqPrinCmd.Query.SQL.Clear;
  SQL := 'INSERT INTO e_p_c (TxtBar,Coluna,Linha,Texto,DataHora,AutoSeq,Ativo)' +
           ' Values("';
  Query.First;
  while not Query.Eof do
  begin
    inc(FmEtqPrinCmd.FAutoSeq, 1);
    Vals := QueryTxtBar.Value + '",' +
    dmkPF.FFP(QueryColuna.Value, 0) + ',' +
    dmkPF.FFP(QueryLinha.Value, 0) + ',' +
    '"' + QueryTexto.Value + '",' +
    dmkPF.FFP(QueryDataHora.Value, 15) + ',' +
    dmkPF.FFP(FmEtqPrinCmd.FAutoSeq, 0) + ',' +
    '1);';
    //
    FmEtqPrinCmd.Query.SQL.Add(SQL + Vals);
    //
    Query.Next;
  end;
  // incluir linhas
  FmEtqPrinCmd.Query.SQL.Add('SELECT * FROM e_p_c;');
  FmEtqPrinCmd.Query.Open;
  // reordenar e reabrir
  FmEtqPrinCmd.ReordenaQuery(Linha);
  {**
  TxtB := Trim(MeGeraTxt_Bar.Text);
  TxtT := Trim(MeGeraTxt_Txt.Text);
  if (Length(TxtB) > 0) and (Length(TxtT) > 0) then
    MyObjects.MostraPopUpDeBotao(PMOK, BtOK)
  else
  if (Length(TxtB) > 0) then
    AdicionaOCodBarGerado()
  else
  if (Length(TxtT) > 0) then
    AdicionaOTextoGerado();
  //
  **}
  if not CkContinuar.Checked then
    Close
  else begin
    Query.Close;
    Query.SQL.Clear;
    Query.SQL.Add('DELETE FROM argox_ppla;');
    Query.SQL.Add('SELECT * FROM argox_ppla;');
    Query.Open;
  end;
end;

procedure TFmEPC_Argox_PPLA.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEPC_Argox_PPLA.EdBarAltuCm_BarChange(Sender: TObject);
begin
  if EdBarAltuCm_Bar.Focused then
    EdBarAltuPix_Bar.ValueVariant := MedidaToPos(EdBarAltuCm_Bar.ValueVariant);
end;

procedure TFmEPC_Argox_PPLA.EdBarAltuPix_BarChange(Sender: TObject);
begin
 if EdBarAltuPix_Bar.Focused then
   EdBarAltuCm_Bar.ValueVariant := PosToMedida(EdBarAltuPix_Bar.ValueVariant);
  CriaLinhas_Bar;
end;

procedure TFmEPC_Argox_PPLA.EdLinha_BarExit(Sender: TObject);
begin
  CriaLinhas_Bar();
end;

procedure TFmEPC_Argox_PPLA.EdLinha_BarKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if VAR_VK_13_PRESSED  then
    CriaLinhas_Bar();
end;

procedure TFmEPC_Argox_PPLA.EdLinha_TxtExit(Sender: TObject);
begin
  CriaLinhas_Txt();
end;

procedure TFmEPC_Argox_PPLA.EdLinha_TxtKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if VAR_VK_13_PRESSED  then
    CriaLinhas_Txt();
end;

procedure TFmEPC_Argox_PPLA.EdTexto_BarExit(Sender: TObject);
begin
  CriaLinhas_Bar();
end;

procedure TFmEPC_Argox_PPLA.EdTexto_BarKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if VAR_VK_13_PRESSED  then
    CriaLinhas_Bar();
end;

procedure TFmEPC_Argox_PPLA.EdTexto_TxtExit(Sender: TObject);
begin
  CriaLinhas_Txt();
end;

procedure TFmEPC_Argox_PPLA.EdTexto_TxtKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if VAR_VK_13_PRESSED  then
    CriaLinhas_Txt();
end;

procedure TFmEPC_Argox_PPLA.EdTxtLeftCm_BarChange(Sender: TObject);
begin
 if EdTxtLeftCm_Bar.Focused then
   EdTxtLeftPix_Bar.ValueVariant := MedidaToPos(EdTxtLeftCm_Bar.ValueVariant);
end;

procedure TFmEPC_Argox_PPLA.EdTxtLeftCm_TxtChange(Sender: TObject);
begin
 if EdTxtLeftCm_Txt.Focused then
   EdTxtLeftPix_txt.ValueVariant := MedidaToPos(EdTxtLeftCm_Txt.ValueVariant);
end;

procedure TFmEPC_Argox_PPLA.EdTxtLeftPix_BarChange(Sender: TObject);
begin
 if EdTxtLeftPix_Bar.Focused then
   EdTxtLeftCm_Bar.ValueVariant := PosToMedida(EdTxtLeftPix_Bar.ValueVariant);
  CriaLinhas_Bar();
end;

procedure TFmEPC_Argox_PPLA.EdTxtLeftPix_TxtChange(Sender: TObject);
begin
 if EdTxtLeftPix_Txt.Focused then
   EdTxtLeftCm_Txt.ValueVariant := PosToMedida(EdTxtLeftPix_Txt.ValueVariant);
  CriaLinhas_Txt;
end;

procedure TFmEPC_Argox_PPLA.EdTxtTopoCm_BarChange(Sender: TObject);
begin
 if EdTxtTopoCm_Bar.Focused then
   EdTxtTopoPix_Bar.ValueVariant := MedidaToPos(EdTxtTopoCm_Bar.ValueVariant);
end;

procedure TFmEPC_Argox_PPLA.EdTxtTopoCm_TxtChange(Sender: TObject);
begin
 if EdTxtTopoCm_Txt.Focused then
   EdTxtTopoPix_Txt.ValueVariant := MedidaToPos(EdTxtTopoCm_Txt.ValueVariant);
end;

procedure TFmEPC_Argox_PPLA.EdTxtTopoPix_BarChange(Sender: TObject);
begin
 if EdTxtTopoPix_Bar.Focused then
   EdTxtTopoCm_Bar.ValueVariant := PosToMedida(EdTxtTopoPix_Bar.ValueVariant);
  CriaLinhas_Bar();
end;

procedure TFmEPC_Argox_PPLA.EdTxtTopoPix_TxtChange(Sender: TObject);
begin
 if EdTxtTopoPix_Txt.Focused then
   EdTxtTopoCm_Txt.ValueVariant := PosToMedida(EdTxtTopoPix_Txt.ValueVariant);
  CriaLinhas_Txt;
end;

procedure TFmEPC_Argox_PPLA.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEPC_Argox_PPLA.FormCreate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 1;
  //
  MeResult.Lines.Clear;
  //
  Query.SQL.Add('DROP TABLE Argox_PPLA;    ');
  Query.SQL.Add('CREATE TABLE Argox_PPLA  (');
  Query.SQL.Add('  Coluna  integer         ,');
  Query.SQL.Add('  Linha   integer         ,');
  Query.SQL.Add('  Texto   varchar(255)    ,');
  Query.SQL.Add('  TxtBar  varchar(3)      ,');
  Query.SQL.Add('  DataHora float          ,');
  Query.SQL.Add('  Ativo smallint          ');
  Query.SQL.Add(');');
  //
  Query.SQL.Add('SELECT * FROM argox_ppla;');
  Query.Open;
end;

procedure TFmEPC_Argox_PPLA.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmEPC_Argox_PPLA.Geraeimprime1Click(Sender: TObject);
begin
 ImprimeEOuGera(True, True);
end;

procedure TFmEPC_Argox_PPLA.ImprimeViaDOS(MeResult: TMemo);
const
  Arq = 'C:\Dermatek\Etiketa.txt';
var
  Porta: String;
begin
  //via dos
  MLAGeral.ExportaMemoToFile(MeResult, Arq, True, True, False);
  Porta := FmEtqPrinCad.QrEtqPrinCadNOMEPORTAIMP.Value +
    dmkPF.FFP(FmEtqPrinCad.QrEtqPrinCadPortaIdx.Value, 0);
  MLAGeral.ExecutaCmd('type ' + Arq + ' > ' + Porta, MeResult, False);
end;

procedure TFmEPC_Argox_PPLA.ImprimeViaWin(MeResult: TMemo);
var
  F: TextFile;
  i: Integer;
  Porta: String;
begin
  // Via windows
  Porta := FmEtqPrinCad.QrEtqPrinCadNOMEPORTAIMP.Value +
    dmkPF.FFP(FmEtqPrinCad.QrEtqPrinCadPortaIdx.Value, 0);
  AssignFile(F, Porta);
  ReWrite(F);
  if CkLinhaAlinha.Checked then
  begin
    for i := 0 to MeResult.Lines.Count - 1 do
      Writeln(F, MeResult.Lines[i] + Char(13));
  end else Writeln(F, MeResult.Text);
  CloseFile(F);
end;

procedure TFmEPC_Argox_PPLA.ImprimeEOuGera(Gera, Imprime: Boolean);
begin
  case PageControl1.ActivePageIndex of
    0: ImprimeLinhas(MeResult, Gera, Imprime);
    1: ImprimeLinhas(MeResult, Gera, Imprime);
  end;
end;

end.

