unit EtqPrinCad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, Grids, DBGrids, dmkDBGrid,
  Menus, dmkCheckGroup, dmkCheckBox, UnDmkProcFunc, UnDmkEnums;

type
  TFmEtqPrinCad = class(TForm)
    PainelDados: TPanel;
    DsEtqPrinCad: TDataSource;
    QrEtqPrinCad: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TdmkLabel;
    Image1: TImage;
    PanelFill2: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TdmkLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtLinha: TBitBtn;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    Label8: TLabel;
    EdCodUsu: TdmkEdit;
    PMConfigura: TPopupMenu;
    QrEtqPrinCmd_0: TmySQLQuery;
    DsEtqPrinCmd_0: TDataSource;
    BtConfigura: TBitBtn;
    Panel4: TPanel;
    QrEtqPrinCadCodigo: TIntegerField;
    QrEtqPrinCadCodUsu: TIntegerField;
    QrEtqPrinCadNome: TWideStringField;
    QrEtqPrinCadTypeUso: TSmallintField;
    QrEtqPrinCadNOMETYPEUSO: TWideStringField;
    QrEtqPrinCadTypePrn: TSmallintField;
    QrEtqPrinCadNOMETYPEPRN: TWideStringField;
    QrEtqPrinCadPortaImp: TSmallintField;
    QrEtqPrinCadNOMEPORTAIMP: TWideStringField;
    QrEtqPrinCadPortaIdx: TSmallintField;
    QrEtqPrinCadModelo: TWideStringField;
    QrEtqPrinCadColunas: TSmallintField;
    SpeedButton5: TSpeedButton;
    RGTypeUso: TdmkRadioGroup;
    RGTypePrn: TdmkRadioGroup;
    EdModelo: TdmkEdit;
    Label2: TLabel;
    RGPortaImp: TdmkRadioGroup;
    EdPortaIdx: TdmkEdit;
    Label9: TLabel;
    PnCabeca: TPanel;
    Label13: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit19: TDBEdit;
    Incluinovaconfigurao1: TMenuItem;
    Alteraconfiguraoatual1: TMenuItem;
    Excluiconfiguraoatual1: TMenuItem;
    QrEtqPrinCmd_0Codigo: TIntegerField;
    QrEtqPrinCmd_0Tipo: TSmallintField;
    QrEtqPrinCmd_0Ordem: TIntegerField;
    QrEtqPrinCmd_0Controle: TIntegerField;
    QrEtqPrinCmd_0Config: TWideStringField;
    QrEtqPrinCmd_1: TmySQLQuery;
    DsEtqPrinCmd_1: TDataSource;
    QrEtqPrinCmd_2: TmySQLQuery;
    DsEtqPrinCmd_2: TDataSource;
    QrEtqPrinCmd_1Codigo: TIntegerField;
    QrEtqPrinCmd_1Tipo: TSmallintField;
    QrEtqPrinCmd_1Ordem: TIntegerField;
    QrEtqPrinCmd_1Controle: TIntegerField;
    QrEtqPrinCmd_1Config: TWideStringField;
    QrEtqPrinCmd_2Codigo: TIntegerField;
    QrEtqPrinCmd_2Tipo: TSmallintField;
    QrEtqPrinCmd_2Ordem: TIntegerField;
    QrEtqPrinCmd_2Controle: TIntegerField;
    QrEtqPrinCmd_2Config: TWideStringField;
    PMLinha: TPopupMenu;
    Incluinovalinhadecomando1: TMenuItem;
    Alteralinhadecomandoatual1: TMenuItem;
    Excluilinhadecomandoatual1: TMenuItem;
    QrOrd: TmySQLQuery;
    QrOrdOrdem: TIntegerField;
    QrOrdControle: TIntegerField;
    Incio1: TMenuItem;
    Dados1: TMenuItem;
    Final1: TMenuItem;
    Panel_1: TPanel;
    Panel7: TPanel;
    DBGCmd_1: TDBGrid;
    Panel_0: TPanel;
    Panel9: TPanel;
    DBGCmd_0: TDBGrid;
    Panel_2: TPanel;
    Panel11: TPanel;
    DBGCmd_2: TDBGrid;
    BtAcima_0: TBitBtn;
    BtAbaixo_0: TBitBtn;
    BtAcima_1: TBitBtn;
    BtAbaixo_1: TBitBtn;
    BtAcima_2: TBitBtn;
    BtAbaixo_2: TBitBtn;
    QrEtqPrinCadIdVarIni: TWideStringField;
    QrEtqPrinCadIdVarFmt: TWideStringField;
    QrEtqPrinCadIdVarFim: TWideStringField;
    Inicio1: TMenuItem;
    Dados2: TMenuItem;
    Final2: TMenuItem;
    BtLotes: TBitBtn;
    QrEtqPrinCadDPI: TIntegerField;
    GroupBox4: TGroupBox;
    dmkGBMedidasLabel: TGroupBox;
    EdLabelLarg: TdmkEdit;
    Label4: TLabel;
    GroupBox6: TGroupBox;
    dmkLaFormLargura: TLabel;
    Label6: TLabel;
    Label10: TLabel;
    EdFormuLarg: TdmkEdit;
    EdDPI: TdmkEdit;
    EdColunas: TdmkEdit;
    EdLabelAltu: TdmkEdit;
    Label1: TLabel;
    EdLabelMEsq: TdmkEdit;
    Label11: TLabel;
    EdLabelLGap: TdmkEdit;
    Label12: TLabel;
    GroupBox8: TGroupBox;
    DBGBMedidasLabel: TGroupBox;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label24: TLabel;
    GroupBox10: TGroupBox;
    LaFormLargura: TLabel;
    DBEdit20: TDBEdit;
    Label23: TLabel;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    QrEtqPrinCadDefaultIni: TSmallintField;
    QrEtqPrinCadDefaultFim: TSmallintField;
    QrEtqPrinCadDefMinPixel: TSmallintField;
    QrEtqPrinCadDefFeedBac: TSmallintField;
    QrEtqPrinCadLabelLarg: TFloatField;
    QrEtqPrinCadLabelAltu: TFloatField;
    QrEtqPrinCadLabelMEsq: TFloatField;
    QrEtqPrinCadLabelLGap: TFloatField;
    QrEtqPrinCadFormuLarg: TFloatField;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    QrEtqPrinCmd_0Coluna: TIntegerField;
    QrEtqPrinCmd_1Coluna: TIntegerField;
    QrEtqPrinCmd_2Coluna: TIntegerField;
    Antigo1: TMenuItem;
    Incio2: TMenuItem;
    Dados3: TMenuItem;
    Fim1: TMenuItem;
    GroupBox11: TGroupBox;
    GroupBox1: TGroupBox;
    CkDefaultIni: TdmkCheckBox;
    CkDefaultFim: TdmkCheckBox;
    GroupBox3: TGroupBox;
    CkDefFeedBac: TdmkCheckBox;
    CkDefMinPixel: TdmkCheckBox;
    RGLinguagem: TdmkRadioGroup;
    QrEtqPrinCadLinguagem: TSmallintField;
    GroupBox12: TGroupBox;
    GroupBox2: TGroupBox;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    GroupBox7: TGroupBox;
    DBCheckBox3: TDBCheckBox;
    DBCheckBox4: TDBCheckBox;
    DBRadioGroup1: TDBRadioGroup;
    Label17: TLabel;
    RGMeasure: TdmkRadioGroup;
    dmkRadioGroup1: TDBRadioGroup;
    QrEtqPrinCadMeasure: TSmallintField;
    N1: TMenuItem;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrEtqPrinCadAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrEtqPrinCadBeforeOpen(DataSet: TDataSet);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure BtConfiguraClick(Sender: TObject);
    procedure PMConfiguraPopup(Sender: TObject);
    procedure QrEtqPrinCadBeforeClose(DataSet: TDataSet);
    procedure QrEtqPrinCadAfterScroll(DataSet: TDataSet);
    procedure SpeedButton5Click(Sender: TObject);
    procedure Incluinovaconfigurao1Click(Sender: TObject);
    procedure Alteraconfiguraoatual1Click(Sender: TObject);
    procedure QrEtqPrinCmd_0AfterOpen(DataSet: TDataSet);
    procedure QrEtqPrinCmd_2AfterOpen(DataSet: TDataSet);
    procedure Incluinovalinhadecomando1Click(Sender: TObject);
    procedure Alteralinhadecomandoatual1Click(Sender: TObject);
    procedure BtLinhaClick(Sender: TObject);
    procedure Incio1Click(Sender: TObject);
    procedure Dados1Click(Sender: TObject);
    procedure Final1Click(Sender: TObject);
    procedure BtAcima_0Click(Sender: TObject);
    procedure BtAbaixo_0Click(Sender: TObject);
    procedure BtAcima_1Click(Sender: TObject);
    procedure BtAbaixo_1Click(Sender: TObject);
    procedure BtAcima_2Click(Sender: TObject);
    procedure BtAbaixo_2Click(Sender: TObject);
    procedure QrEtqPrinCmd_0BeforeClose(DataSet: TDataSet);
    procedure QrEtqPrinCmd_1BeforeClose(DataSet: TDataSet);
    procedure QrEtqPrinCmd_2BeforeClose(DataSet: TDataSet);
    procedure QrEtqPrinCmd_0AfterScroll(DataSet: TDataSet);
    procedure QrEtqPrinCmd_1AfterScroll(DataSet: TDataSet);
    procedure QrEtqPrinCmd_2AfterScroll(DataSet: TDataSet);
    procedure DBGCmd_1DblClick(Sender: TObject);
    procedure DBGCmd_0DblClick(Sender: TObject);
    procedure DBGCmd_2DblClick(Sender: TObject);
    procedure Inicio1Click(Sender: TObject);
    procedure Dados2Click(Sender: TObject);
    procedure Final2Click(Sender: TObject);
    procedure BtLotesClick(Sender: TObject);
    procedure dmkRadioGroup1Change(Sender: TObject);
    procedure RGMeasureClick(Sender: TObject);
    procedure Incio2Click(Sender: TObject);
    procedure Dados3Click(Sender: TObject);
    procedure Fim1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; Status: String; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraEdicaoCmd(TipoLinha: Integer);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenPrinCmd_0(Controle: Integer);
    procedure ReopenPrinCmd_1(Controle: Integer);
    procedure ReopenPrinCmd_2(Controle: Integer);
    procedure ReordenaLinhas(Tipo, Controle, Ordem: Integer);
    procedure ExcluiLinhaDeComando(Tipo: integer);
    procedure IncluiLinhasNovo(TipoLinha: Integer);
  end;

var
  FmEtqPrinCad: TFmEtqPrinCad;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, MasterSelFilial, EtqPrinCmd, EtqGeraLot;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmEtqPrinCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmEtqPrinCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrEtqPrinCadCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmEtqPrinCad.DefParams;
begin
  VAR_GOTOTABELA := 'EtqPrinCad';
  VAR_GOTOMYSQLTABLE := QrEtqPrinCad;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT prc.Codigo, prc.CodUsu, prc.Nome, prc.TypeUso, ');
  VAR_SQLx.Add('ELT(prc.TypeUso + 1, "Produto", "Pessoa", "Expedi��o") ');
  VAR_SQLx.Add('NOMETYPEUSO,  prc.TypePrn, ELT(prc.TypePrn + 1, "T�rmica", ');
  VAR_SQLx.Add('"Matricial") NOMETYPEPRN, prc.PortaImp, ELT(prc.PortaImp+1, ');
  VAR_SQLx.Add('"COM", "LPT") NOMEPORTAIMP, prc.PortaIdx, prc.Modelo, ');
  VAR_SQLx.Add('prc.Colunas, prc.IdVarIni, prc.IdVarFmt, prc.IdVarFim, ');
  VAR_SQLx.Add('prc.DPI, prc.DefaultIni, DefaultFim, DefMinPixel, DefFeedBac,');
  VAR_SQLx.Add('LabelLarg, LabelAltu, LabelMEsq, LabelLGap, FormuLarg, ');
  VAR_SQLx.Add('Linguagem, Measure ');
  VAR_SQLx.Add('FROM etqprincad prc');
  VAR_SQLx.Add('WHERE prc.Codigo > -1000');
  //
  VAR_SQL1.Add('AND prc.Codigo=:P0');
  //
  VAR_SQL2.Add('AND prc.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND prc.Nome Like :P0');
  //
end;

procedure TFmEtqPrinCad.dmkRadioGroup1Change(Sender: TObject);
begin
  case FmEtqPrinCad.QrEtqPrinCadMeasure.Value of
    0:
    begin
      DBGBMedidasLabel.Caption := ' Label (em cent�mentros): ';
      LaFormLargura.Caption    := ' Label (cm): ';
    end;
    1:
    begin
      DBGBMedidasLabel.Caption := ' Label (em polegadas): ';
      LaFormLargura.Caption    := 'Label (pol.): ';
    end;
  end;
end;

procedure TFmEtqPrinCad.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (LaTipo.SQLType = stIns) then
    EdCodUsu.ValueVariant :=
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'EtqPrinCad', 'CodUsu', [], [], stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmEtqPrinCad.ExcluiLinhaDeComando(Tipo: integer);
begin
  //
end;

procedure TFmEtqPrinCad.MostraEdicao(Mostra: Integer; Status: String; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelControle.Visible:=True;
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    else Geral.MensagemBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  LaTipo.Caption := Status;
  GOTOy.BotoesSb(LaTipo.Caption);
end;

procedure TFmEtqPrinCad.MostraEdicaoCmd(TipoLinha: Integer);
var
  Query: TmySQLQuery;
begin
  case TipoLinha of
    0: Query := QrEtqPrinCmd_0;
    1: Query := QrEtqPrinCmd_1;
    2: Query := QrEtqPrinCmd_2;
    else Query := nil;
  end;
  UmyMod.FormInsUpd_Show(TFmEtqPrinCmd, FmEtqPrinCmd, afmoNegarComAviso, Query, stUpd);
end;

procedure TFmEtqPrinCad.PMConfiguraPopup(Sender: TObject);
begin
  Alteraconfiguraoatual1.Enabled :=
    (QrEtqPrinCad.State <> dsInactive) and (QrEtqPrinCad.RecordCount > 0);
end;

procedure TFmEtqPrinCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmEtqPrinCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmEtqPrinCad.Dados1Click(Sender: TObject);
begin
  MostraEdicaoCmd(1);
end;

procedure TFmEtqPrinCad.Dados2Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrEtqPrinCmd_1, DBGCmd_1,
      'etqprincmd', ['Controle'], ['Controle'], istPergunta, '');
end;

procedure TFmEtqPrinCad.Dados3Click(Sender: TObject);
begin
  IncluiLinhasNovo(1);
end;

procedure TFmEtqPrinCad.DBGCmd_0DblClick(Sender: TObject);
begin
  MostraEdicaoCmd(0);
end;

procedure TFmEtqPrinCad.DBGCmd_1DblClick(Sender: TObject);
begin
  MostraEdicaoCmd(1);
end;

procedure TFmEtqPrinCad.DBGCmd_2DblClick(Sender: TObject);
begin
  MostraEdicaoCmd(2);
end;

procedure TFmEtqPrinCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmEtqPrinCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmEtqPrinCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmEtqPrinCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmEtqPrinCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmEtqPrinCad.SpeedButton5Click(Sender: TObject);
begin
  EdCodUsu.ValueVariant := DBCheck.ObtemCodUsuZStepCodUni(tscEtqPrinCad);
end;

procedure TFmEtqPrinCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrEtqPrinCadCodigo.Value;
  Close;
end;

procedure TFmEtqPrinCad.Alteraconfiguraoatual1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrEtqPrinCad, [PainelDados],
  [PainelEdita], EdCodUsu, LaTipo, 'EtqPrinCad');
end;

procedure TFmEtqPrinCad.Alteralinhadecomandoatual1Click(Sender: TObject);
begin
//  Parei Aqui! Fazer
end;

procedure TFmEtqPrinCad.BtAcima_0Click(Sender: TObject);
begin
  ReordenaLinhas(0, QrEtqPrinCmd_0Controle.Value, QrEtqPrinCmd_0Ordem.Value - 1);
end;

procedure TFmEtqPrinCad.BtAbaixo_0Click(Sender: TObject);
begin
  ReordenaLinhas(0, QrEtqPrinCmd_0Controle.Value, QrEtqPrinCmd_0Ordem.Value + 1);
end;

procedure TFmEtqPrinCad.BtAcima_1Click(Sender: TObject);
begin
  ReordenaLinhas(1, QrEtqPrinCmd_1Controle.Value, QrEtqPrinCmd_1Ordem.Value - 1);
end;

procedure TFmEtqPrinCad.BtAbaixo_1Click(Sender: TObject);
begin
  ReordenaLinhas(1, QrEtqPrinCmd_1Controle.Value, QrEtqPrinCmd_1Ordem.Value + 1);
end;

procedure TFmEtqPrinCad.BtAcima_2Click(Sender: TObject);
begin
  ReordenaLinhas(2, QrEtqPrinCmd_2Controle.Value, QrEtqPrinCmd_2Ordem.Value - 1);
end;

procedure TFmEtqPrinCad.BtAbaixo_2Click(Sender: TObject);
begin
  ReordenaLinhas(2, QrEtqPrinCmd_2Controle.Value, QrEtqPrinCmd_2Ordem.Value + 1);
end;

procedure TFmEtqPrinCad.BtConfiguraClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMConfigura, BtConfigura);
end;

procedure TFmEtqPrinCad.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Geral.MensagemBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Codigo := UMyMod.BuscaEmLivreY_Def('EtqPrinCad', 'Codigo', LaTipo.SQLType,
    QrEtqPrinCadCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(LaTipo.SQLType, FmEtqPrinCad, PainelEdit,
    'EtqPrinCad', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], LaTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmEtqPrinCad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if LaTipo.Caption = CO_INCLUSAO then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'EtqPrinCad', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'EtqPrinCad', 'Codigo');
  MostraEdicao(0, CO_TRAVADO, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'EtqPrinCad', 'Codigo');
end;

procedure TFmEtqPrinCad.BtLinhaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMLinha, BtLinha);
end;

procedure TFmEtqPrinCad.BtLotesClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEtqGeraLot, FmEtqGeraLot, afmoNegarComAviso) then
  begin
    FmEtqGeraLot.ShowModal;
    FmEtqGeraLot.Destroy;
  end;
end;

procedure TFmEtqPrinCad.FormCreate(Sender: TObject);
begin
  PainelEdit.Align  := alClient;
  Panel4.Align      := alClient;
  CriaOForm;
end;

procedure TFmEtqPrinCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrEtqPrinCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmEtqPrinCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmEtqPrinCad.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrEtqPrinCadCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmEtqPrinCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmEtqPrinCad.QrEtqPrinCadAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmEtqPrinCad.QrEtqPrinCadAfterScroll(DataSet: TDataSet);
begin
  ReopenPrinCmd_0(0);
  ReopenPrinCmd_1(0);
  ReopenPrinCmd_2(0);
end;

procedure TFmEtqPrinCad.Fim1Click(Sender: TObject);
begin
  IncluiLinhasNovo(2);
end;

procedure TFmEtqPrinCad.Final1Click(Sender: TObject);
begin
  MostraEdicaoCmd(2);
end;

procedure TFmEtqPrinCad.Final2Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrEtqPrinCmd_2, DBGCmd_2,
      'etqprincmd', ['Controle'], ['Controle'], istPergunta, '');
end;

procedure TFmEtqPrinCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEtqPrinCad.SbQueryClick(Sender: TObject);
begin
  LocCod(QrEtqPrinCadCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'EtqPrinCad', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmEtqPrinCad.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmEtqPrinCad.Incio1Click(Sender: TObject);
begin
  MostraEdicaoCmd(0);
end;

procedure TFmEtqPrinCad.Incio2Click(Sender: TObject);
begin
  IncluiLinhasNovo(0);
end;

procedure TFmEtqPrinCad.IncluiLinhasNovo(TipoLinha: Integer);
var
  i: Integer;
  SQL, Vals: String;
  Query: TmySQLQuery;
begin
  if DBCheck.CriaFm(TFmEtqPrinCmd, FmEtqPrinCmd, afmoNegarComAviso) then
  begin
    FmEtqPrinCmd.RGTipo.ItemIndex := TipoLinha;

    FmEtqPrinCmd.TCLinhas.Tabs.Clear;
    if TipoLinha = 1 then
    begin
      for i := 1 to FmEtqPrinCad.QrEtqPrinCadColunas.Value do
        FmEtqPrinCmd.TCLinhas.Tabs.Add(' Coluna ' + MLAGeral.FTX(i, 3, 0, siPositivo) + ' ');
    end else FmEtqPrinCmd.TCLinhas.Tabs.Add(' Coluna �nica ');
    //
    //
    case TipoLinha of
      0: Query := QrEtqPrinCmd_0;
      1: Query := QrEtqPrinCmd_1;
      2: Query := QrEtqPrinCmd_2;
      else Query := nil;
    end;
    FmEtqPrinCmd.Query.Close;
    FmEtqPrinCmd.Query.SQL.Clear;
    SQL := 'INSERT INTO e_p_c (TxtBar,Coluna,Linha,Texto,DataHora,AutoSeq,Ativo)' +
             ' Values("???",';
    Query.First;
    while not Query.Eof do
    begin
      inc(FmEtqPrinCmd.FAutoSeq, 1);
      Vals := dmkPF.FFP(Query.FieldByName('Coluna').Value, 0) + ',' +
      dmkPF.FFP(Query.FieldByName('Ordem').Value, 0) + ',' +
      '"' + Query.FieldByName('Config').Value + '",' +
      dmkPF.FFP(0, 15) + ',' +
      dmkPF.FFP(FmEtqPrinCmd.FAutoSeq, 0) + ',' +
      '1);';
      //
      FmEtqPrinCmd.Query.SQL.Add(SQL + Vals);
      //
      Query.Next;
    end;
    // incluir linhas
    FmEtqPrinCmd.Query.SQL.Add('SELECT * FROM e_p_c;');
    FmEtqPrinCmd.Query.Open;
    // reordenar e reabrir
    FmEtqPrinCmd.ReordenaQuery(0);

    //
    FmEtqPrinCmd.ShowModal;
    FmEtqPrinCmd.Destroy;
  end;
end;

procedure TFmEtqPrinCad.Incluinovaconfigurao1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrEtqPrinCad, [PainelDados],
  [PainelEdita], EdCodUsu, LaTipo, 'EtqPrinCad');
end;

procedure TFmEtqPrinCad.Incluinovalinhadecomando1Click(Sender: TObject);
begin
  UmyMod.FormInsUpd_Show(TFmEtqPrinCmd, FmEtqPrinCmd, afmoNegarComAviso,
  nil, stIns);
end;

procedure TFmEtqPrinCad.Inicio1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrEtqPrinCmd_0, DBGCmd_0,
      'etqprincmd', ['Controle'], ['Controle'], istPergunta, '');
end;

procedure TFmEtqPrinCad.QrEtqPrinCadBeforeClose(DataSet: TDataSet);
begin
  QrEtqPrinCmd_0.Close;
  QrEtqPrinCmd_1.Close;
  QrEtqPrinCmd_2.Close;
end;

procedure TFmEtqPrinCad.QrEtqPrinCadBeforeOpen(DataSet: TDataSet);
begin
  QrEtqPrinCadCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmEtqPrinCad.QrEtqPrinCmd_0AfterOpen(DataSet: TDataSet);
var
  Tam: Integer;
begin
  Tam := 57 + ((QrEtqPrinCmd_0.RecordCount -1) * 18);
  if Tam < 88 then Tam := 88;
  DBGCmd_0.Height := Tam;
end;

procedure TFmEtqPrinCad.QrEtqPrinCmd_0AfterScroll(DataSet: TDataSet);
begin
  BtAcima_0.Enabled := QrEtqPrinCmd_0.RecNo > 1;
  BtAbaixo_0.Enabled := QrEtqPrinCmd_0.RecNo < QrEtqPrinCmd_0.RecordCount;
end;

procedure TFmEtqPrinCad.QrEtqPrinCmd_0BeforeClose(DataSet: TDataSet);
begin
  BtAcima_0.Enabled := False;
  BtAbaixo_0.Enabled := False;
end;

procedure TFmEtqPrinCad.QrEtqPrinCmd_1AfterScroll(DataSet: TDataSet);
begin
  BtAcima_1.Enabled := QrEtqPrinCmd_1.RecNo > 1;
  BtAbaixo_1.Enabled := QrEtqPrinCmd_1.RecNo < QrEtqPrinCmd_1.RecordCount;
end;

procedure TFmEtqPrinCad.QrEtqPrinCmd_1BeforeClose(DataSet: TDataSet);
begin
  BtAcima_1.Enabled := False;
  BtAbaixo_1.Enabled := False;
end;

procedure TFmEtqPrinCad.QrEtqPrinCmd_2AfterOpen(DataSet: TDataSet);
var
  Tam: Integer;
begin
  Tam := 57 + ((QrEtqPrinCmd_2.RecordCount -1) * 18);
  if Tam < 88 then Tam := 88;
  DBGCmd_2.Height := Tam;
end;

procedure TFmEtqPrinCad.QrEtqPrinCmd_2AfterScroll(DataSet: TDataSet);
begin
  BtAcima_2.Enabled := QrEtqPrinCmd_2.RecNo > 1;
  BtAbaixo_2.Enabled := QrEtqPrinCmd_2.RecNo < QrEtqPrinCmd_2.RecordCount;
end;

procedure TFmEtqPrinCad.QrEtqPrinCmd_2BeforeClose(DataSet: TDataSet);
begin
  BtAcima_2.Enabled := False;
  BtAbaixo_2.Enabled := False;
end;

procedure TFmEtqPrinCad.ReopenPrinCmd_0(Controle: Integer);
begin
  QrEtqPrinCmd_0.Close;
  QrEtqPrinCmd_0.Params[0].AsInteger := QrEtqPrinCadCodigo.Value;
  QrEtqPrinCmd_0.Open;
  //
  if Controle <> 0 then
    QrEtqPrinCmd_0.Locate('Controle', Controle, []);
end;

procedure TFmEtqPrinCad.ReopenPrinCmd_1(Controle: Integer);
begin
  QrEtqPrinCmd_1.Close;
  QrEtqPrinCmd_1.Params[0].AsInteger := QrEtqPrinCadCodigo.Value;
  QrEtqPrinCmd_1.Open;
  //
  if Controle <> 0 then
    QrEtqPrinCmd_1.Locate('Controle', Controle, []);
end;

procedure TFmEtqPrinCad.ReopenPrinCmd_2(Controle: Integer);
begin
  QrEtqPrinCmd_2.Close;
  QrEtqPrinCmd_2.Params[0].AsInteger := QrEtqPrinCadCodigo.Value;
  QrEtqPrinCmd_2.Open;
  //
  if Controle <> 0 then
    QrEtqPrinCmd_2.Locate('Controle', Controle, []);
end;

procedure TFmEtqPrinCad.ReordenaLinhas(Tipo, Controle, Ordem: Integer);
var
  n: Integer;
begin
  QrOrd.Close;
  QrOrd.Params[00].AsInteger := Tipo;
  QrOrd.Params[01].AsInteger := QrEtqPrinCadCodigo.Value;
  QrOrd.Open;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE etqprincmd SET Ordem=:P0 ');
  Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
  n := 0;
  while not QrOrd.Eof do
  begin
    if QrOrdControle.Value <> Controle then
    begin
      n := n + 1;
      if n = Ordem then
        n := n + 1;
      Dmod.QrUpd.Params[00].AsInteger := n;
      Dmod.QrUpd.Params[01].AsInteger := QrOrdControle.Value;
      Dmod.QrUpd.ExecSQL;
    end else begin
      Dmod.QrUpd.Params[00].AsInteger := Ordem;
      Dmod.QrUpd.Params[01].AsInteger := QrOrdControle.Value;
      Dmod.QrUpd.ExecSQL;
    end;
    QrOrd.Next;
  end;
  case Tipo of
    0: ReopenPrinCmd_0(Controle);
    1: ReopenPrinCmd_1(Controle);
    2: ReopenPrinCmd_2(Controle);
  end;
end;

procedure TFmEtqPrinCad.RGMeasureClick(Sender: TObject);
begin
  case RGMeasure.ItemIndex of
    0:
    begin
      dmkGBMedidasLabel.Caption := ' Label (em cent�mentros): ';
      dmkLaFormLargura.Caption    := ' Label (cm): ';
    end;
    1:
    begin
      dmkGBMedidasLabel.Caption := ' Label (em polegadas): ';
      dmkLaFormLargura.Caption    := 'Label (pol.): ';
    end;
  end;
end;

end.

