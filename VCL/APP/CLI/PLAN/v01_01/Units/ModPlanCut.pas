unit ModPlanCut;

interface

uses
  SysUtils, Classes, DB, mySQLDbTables;

type
  TDmPlanCut = class(TDataModule)
    QrPCIEmpCad: TmySQLQuery;
    QrPCIEmpCadCodigo: TIntegerField;
    QrPCIEmpCadFilial: TIntegerField;
    QrPCIEmpCadNOMEENT: TWideStringField;
    DsPCIEmpCad: TDataSource;
    QrPCIEmpIts: TmySQLQuery;
    QrPCIEmpItsCodigo: TIntegerField;
    QrPCIEmpItsCodUsu: TIntegerField;
    QrPCIEmpItsNome: TWideStringField;
    QrPCIEmpItsAtivo: TSmallintField;
    DsPCIEmpIts: TDataSource;
    QrPCIEmpAti: TmySQLQuery;
    QrPCIEmpAtiItens: TIntegerField;
    QrPCICenCad: TmySQLQuery;
    DsPCICenCad: TDataSource;
    QrPCICenIts: TmySQLQuery;
    DsPCICenIts: TDataSource;
    QrPCICenAti: TmySQLQuery;
    QrPCICenItsCodigo: TIntegerField;
    QrPCICenItsCodUsu: TIntegerField;
    QrPCICenItsNome: TWideStringField;
    QrPCICenItsAtivo: TSmallintField;
    QrPCICenAtiItens: TIntegerField;
    QrPCICenCadCodUsu: TIntegerField;
    QrPCICenCadCodigo: TIntegerField;
    QrPCICenCadNome: TWideStringField;
    QrPCIPrdCad: TmySQLQuery;
    QrPCIPrdCadCodusu: TIntegerField;
    QrPCIPrdCadNivel1: TIntegerField;
    QrPCIPrdCadNome: TWideStringField;
    DsPCIPrdCad: TDataSource;
    QrPCIPrdIts: TmySQLQuery;
    QrPCIPrdItsCodigo: TIntegerField;
    QrPCIPrdItsCodUsu: TIntegerField;
    QrPCIPrdItsNome: TWideStringField;
    QrPCIPrdItsAtivo: TSmallintField;
    DsPCIPrdIts: TDataSource;
    QrPCIPrdAti: TmySQLQuery;
    QrPCIPrdAtiItens: TIntegerField;
    QrPCIAtrPrdCad: TmySQLQuery;
    QrPCIAtrPrdCadCodigo: TIntegerField;
    QrPCIAtrPrdCadCodUsu: TIntegerField;
    QrPCIAtrPrdCadNome: TWideStringField;
    DsPCIAtrPrdCad: TDataSource;
    QrPCIAtrPrdIts: TmySQLQuery;
    QrPCIAtrPrdItsCodigo: TIntegerField;
    QrPCIAtrPrdItsCodUsu: TIntegerField;
    QrPCIAtrPrdItsNome: TWideStringField;
    QrPCIAtrPrdItsAtivo: TSmallintField;
    DsPCIAtrPrdIts: TDataSource;
    QrPCIAtrPrdAti: TmySQLQuery;
    QrPCIAtrPrdAtiItens: TIntegerField;
    QrAtrPrdSubIts: TmySQLQuery;
    QrAtrPrdSubItsCodigo: TIntegerField;
    QrAtrPrdSubItsCodUsu: TIntegerField;
    QrAtrPrdSubItsNome: TWideStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DmPlanCut: TDmPlanCut;

implementation

{$R *.dfm}

end.
