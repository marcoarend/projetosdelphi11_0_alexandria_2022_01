unit UnProjGroup_PF;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnInternalConsts2, Buttons, ComCtrls, CommCtrl, Consts,
  Variants, UnInternalConsts, ZCF2, StrUtils, dmkGeral, UnDmkEnums, dmkEditCB,
  dmkEdit, dmkDBLookupComboBox, mySQLDbTables, Data.Db, DBGrids, AppListas,
  dmkDBGridZTO, UnDmkProcFunc, UnProjGroup_Vars, TypInfo, System.Math,
  UnProjGroup_Consts, UnEntities, DBCtrls, Grids, Mask, UnTX_Tabs,
  dmkEditDateTimePicker, UnGrl_Consts, UnGrl_Geral, UnTX_PF, UnAppEnums;

type
  TUnProjGroup_PF = class(TObject)
  private
    { Private declarations }
     //
  public
    { Public declarations }
    //
    procedure ReopenTXIndPrcOriIMEI(Qry: TmySQLQuery; MovimCod, Controle,
              TemIMEIMrt: Integer; MovimNiv: TEstqMovimNiv; SQL_Limit: String);
    function  ObtemNomeTabelaMulTabs(EnumKind: PTypeInfo; EnumID: Integer;
              Avisa: Boolean = True): String;
    function  ObtemTipo_ItemDeGraGruY_CodTxt(GraGruY: Integer): String;
    function  ObtemTipo_ItemDeGraGruY_Descri(GraGruY: Integer): String;
  end;

var
  ProjGroup_PF: TUnProjGroup_PF;

implementation

uses DmkDAC_PF, Module;


{ TUnProjGroup_PF }

function TUnProjGroup_PF.ObtemNomeTabelaMulTabs(EnumKind: PTypeInfo; EnumID:
  Integer; Avisa: Boolean): String;
const
  sProcName = 'UnProjGroup_PF.ObtemNomeTabelaMulTabs()';
begin
  if EnumKind = TypeInfo(TEstqMovimID) then
    Result := TX_PF.ObtemNomeTabelaTXXxxCab(TEstqMovimID(EnumID), Avisa)
  else
  if EnumKind = TypeInfo(TEstqDefMulFldEMxx) then
  begin
    case TEstqDefMulFldEMxx(EnumID) of
      //edmfIndef=0,
      (*1*)edmfSrcNiv2,
      (*3*)edmfIMEI:
      begin
        Result := CO_TAB_TMI;
      end;
      (*2*)edmfMovCod:
      begin
        Result := 'txmovcab';
      end;
      else Geral.MB_Erro('"TEstqDefMulFldEMxx" n�o implementado em ' + sProcName);
    end;
  end else
    Geral.MB_Erro('"EnumKind" n�o implementado em ' + sProcName);
end;

function TUnProjGroup_PF.ObtemTipo_ItemDeGraGruY_CodTxt(
  GraGruY: Integer): String;
begin
  Result := '';
  case GraGruY of
    CO_GraGruY_1024_TXCadNat: Result := '01';
    CO_GraGruY_2048_TXCadInd: Result := '03';
    CO_GraGruY_4096_TXCadInt: Result := '06';
    CO_GraGruY_6144_TXCadFcc: Result := '04';
    else Result := '';
  end;
end;

function TUnProjGroup_PF.ObtemTipo_ItemDeGraGruY_Descri(
  GraGruY: Integer): String;
begin
  Result := '';
  case GraGruY of
    CO_GraGruY_1024_TXCadNat: Result := '01 - Mat�ria prima';
    CO_GraGruY_2048_TXCadInd: Result := '03 - Produto em processo';
    CO_GraGruY_4096_TXCadInt: Result := '06 - Produto intermedi�rio';
    CO_GraGruY_6144_TXCadFcc: Result := '04 - Produto acabado'; // + sLineBreak +                                        '';
    else Result := '##ERRO## GraGruY indefinido: ' + Geral.FF0(GraGruY);
  end;
end;

procedure TUnProjGroup_PF.ReopenTXIndPrcOriIMEI(Qry: TmySQLQuery; MovimCod,
  Controle, TemIMEIMrt: Integer; MovimNiv: TEstqMovimNiv; SQL_Limit: String);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
begin
  //TemIMEIMrt := QrTXOpeCabTemIMEIMrt.Value;
  SQL_Flds := Geral.ATS([
  TX_PF.SQL_NO_GGX(),
  TX_PF.SQL_NO_FRN(),
  TX_PF.SQL_NO_CMO(),
  'txp.Nome NO_Pallet, ',
  '-1.000 FatorImp ',
  '']);
  SQL_Left := Geral.ATS([
  TX_PF.SQL_LJ_GGX(),
  TX_PF.SQL_LJ_FRN(),
  TX_PF.SQL_LJ_CMO(),
  'LEFT JOIN txpalleta   txp ON txp.Codigo=tmi.Pallet ',
  '']);
  SQL_Wher := Geral.ATS([
  //'WHERE tmi.MovimCod=' + Geral.FF0(QrTXOpeCabMovimCod.Value),
  'WHERE tmi.MovimCod=' + Geral.FF0(MovimCod),
  'AND tmi.MovimNiv=' + Geral.FF0(Integer(MovimNiv)),
  '']);
  SQL_Group := '';
  //UnDmkDAC_PF.AbreMySQLQuery0(QrTXOpeOriIMEI, Dmod.MyDB, [
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  TX_PF.GeraSQLTXMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  TX_PF.GeraSQLTXMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY NO_Pallet, Controle ',
  '']);
  //Geral.MB_SQL(nil, Qry);
  //
  Qry.Locate('Controle', Controle, []);
  //
end;

end.
