object FmEtqGeraImp: TFmEtqGeraImp
  Left = 339
  Top = 185
  Caption = 'ETQ-GERAR-003 :: Gera'#231#227'o de Etiquetas - Impress'#227'o'
  ClientHeight = 494
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 446
    Width = 1008
    Height = 48
    Align = alBottom
    TabOrder = 1
    object Label2: TLabel
      Left = 776
      Top = 4
      Width = 29
      Height = 13
      Caption = 'Linha:'
    end
    object BtOK: TBitBtn
      Tag = 163
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Gera'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object Panel2: TPanel
      Left = 896
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object BtImprime: TBitBtn
      Tag = 5
      Left = 112
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Imprime'
      NumGlyphs = 2
      TabOrder = 2
      OnClick = BtImprimeClick
    end
    object CkSeleciona: TCheckBox
      Left = 540
      Top = 20
      Width = 125
      Height = 17
      Caption = 'Selecionar impressora.'
      TabOrder = 3
    end
    object BitBtn2: TBitBtn
      Tag = 5
      Left = 204
      Top = 4
      Width = 90
      Height = 40
      Caption = '&ASCII'
      NumGlyphs = 2
      TabOrder = 4
      OnClick = BitBtn2Click
    end
    object EdLinha: TdmkEdit
      Left = 776
      Top = 20
      Width = 37
      Height = 21
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
    object CkDOS: TCheckBox
      Left = 304
      Top = 20
      Width = 41
      Height = 17
      Caption = 'DOS.'
      TabOrder = 6
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    Caption = 'Gera'#231#227'o de Etiquetas - Impress'#227'o'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 1006
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 398
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 49
      Align = alTop
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 4
        Width = 131
        Height = 13
        Caption = 'Configura'#231#227'o de impress'#227'o:'
      end
      object Label14: TLabel
        Left = 952
        Top = 4
        Width = 41
        Height = 13
        Caption = 'Colunas:'
      end
      object Label9: TLabel
        Left = 904
        Top = 4
        Width = 42
        Height = 13
        Caption = 'N'#186' porta:'
      end
      object EdEtqPrinCad: TdmkEditCB
        Left = 8
        Top = 20
        Width = 64
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnChange = EdEtqPrinCadChange
        OnExit = EdEtqPrinCadExit
        DBLookupComboBox = CBEtqPrinCad
        IgnoraDBLookupComboBox = False
      end
      object CBEtqPrinCad: TdmkDBLookupComboBox
        Left = 76
        Top = 20
        Width = 417
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsEtqPrinCad
        TabOrder = 1
        OnClick = CBEtqPrinCadClick
        dmkEditCB = EdEtqPrinCad
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object RGTypePrn: TdmkRadioGroup
        Left = 496
        Top = 4
        Width = 149
        Height = 38
        Caption = ' Tipo de impressora: '
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'T'#233'rmica'
          'Matricial')
        TabOrder = 2
        QryCampo = 'TypePrn'
        UpdCampo = 'TypePrn'
        UpdType = utYes
        OldValor = 0
      end
      object RGPortaImp: TdmkRadioGroup
        Left = 648
        Top = 4
        Width = 253
        Height = 38
        Caption = ' Porta de impress'#227'o: '
        Columns = 4
        ItemIndex = 0
        Items.Strings = (
          'COM'
          'LPT'
          'FILE'
          'USB00')
        TabOrder = 3
        QryCampo = 'PortaImp'
        UpdCampo = 'PortaImp'
        UpdType = utYes
        OldValor = 0
      end
      object EdPortaIdx: TdmkEdit
        Left = 904
        Top = 20
        Width = 44
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'PortaIdx'
        UpdCampo = 'PortaIdx'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdColunas: TdmkEdit
        Left = 952
        Top = 20
        Width = 44
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Colunas'
        UpdCampo = 'Colunas'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
    end
    object Memo1: TMemo
      Left = 0
      Top = 49
      Width = 260
      Height = 349
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Courier New'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 1
      WordWrap = False
      OnMouseUp = Memo1MouseUp
    end
    object ListBox1: TListBox
      Left = 772
      Top = 49
      Width = 236
      Height = 349
      Align = alRight
      ItemHeight = 13
      TabOrder = 2
    end
    object MeResult: TMemo
      Left = 260
      Top = 49
      Width = 512
      Height = 349
      Align = alRight
      Color = 4539717
      Font.Charset = ANSI_CHARSET
      Font.Color = clSilver
      Font.Height = -13
      Font.Name = 'Lucida Console'
      Font.Style = [fsBold]
      Lines.Strings = (
        'Teste 1'
        'C:\Meus Documentos')
      ParentFont = False
      TabOrder = 3
      Visible = False
    end
  end
  object QrEtqPrinCad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM etqprincad'
      'ORDER BY Nome'
      '')
    Left = 12
    Top = 12
    object QrEtqPrinCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEtqPrinCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrEtqPrinCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrEtqPrinCadTypeUso: TSmallintField
      FieldName = 'TypeUso'
    end
    object QrEtqPrinCadTypePrn: TSmallintField
      FieldName = 'TypePrn'
    end
    object QrEtqPrinCadPortaImp: TSmallintField
      FieldName = 'PortaImp'
    end
    object QrEtqPrinCadPortaIdx: TSmallintField
      FieldName = 'PortaIdx'
    end
    object QrEtqPrinCadModelo: TWideStringField
      FieldName = 'Modelo'
      Size = 30
    end
    object QrEtqPrinCadColunas: TSmallintField
      FieldName = 'Colunas'
    end
    object QrEtqPrinCadIdVarIni: TWideStringField
      FieldName = 'IdVarIni'
      Size = 1
    end
    object QrEtqPrinCadIdVarFmt: TWideStringField
      FieldName = 'IdVarFmt'
      Size = 1
    end
    object QrEtqPrinCadIdVarFim: TWideStringField
      FieldName = 'IdVarFim'
      Size = 1
    end
    object QrEtqPrinCadDPI: TIntegerField
      FieldName = 'DPI'
    end
    object QrEtqPrinCadDefaultIni: TSmallintField
      FieldName = 'DefaultIni'
    end
    object QrEtqPrinCadDefaultFim: TSmallintField
      FieldName = 'DefaultFim'
    end
    object QrEtqPrinCadDefMinPixel: TSmallintField
      FieldName = 'DefMinPixel'
    end
    object QrEtqPrinCadDefFeedBac: TSmallintField
      FieldName = 'DefFeedBac'
    end
    object QrEtqPrinCadLabelLarg: TFloatField
      FieldName = 'LabelLarg'
    end
    object QrEtqPrinCadLabelAltu: TFloatField
      FieldName = 'LabelAltu'
    end
    object QrEtqPrinCadLabelMEsq: TFloatField
      FieldName = 'LabelMEsq'
    end
    object QrEtqPrinCadLabelLGap: TFloatField
      FieldName = 'LabelLGap'
    end
    object QrEtqPrinCadFormuLarg: TFloatField
      FieldName = 'FormuLarg'
    end
    object QrEtqPrinCadLinguagem: TSmallintField
      FieldName = 'Linguagem'
    end
    object QrEtqPrinCadMeasure: TSmallintField
      FieldName = 'Measure'
    end
  end
  object DsEtqPrinCad: TDataSource
    DataSet = QrEtqPrinCad
    Left = 40
    Top = 12
  end
  object QrEtqPrinCmd_0: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM etqprincmd'
      'WHERE Tipo = 0'
      'AND Codigo=:P0'
      'ORDER BY Tipo, Ordem')
    Left = 124
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEtqPrinCmd_0Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEtqPrinCmd_0Tipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEtqPrinCmd_0Ordem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrEtqPrinCmd_0Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEtqPrinCmd_0Config: TWideStringField
      FieldName = 'Config'
      Size = 255
    end
  end
  object QrEtqGeraIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT etq.GraGruX REDUZIDO, etq.Sequencia, '
      'ggx.GraGruC, gg1.CodUsu NIVEL1, '
      'gti.Controle COD_TAM, gti.Nome NOMETAM, '
      'gtc.CodUsu COD_GRADE, gtc.Nome NOMEGRADE, '
      'gcc.CodUsu COD_COR, gcc.Nome NOMECOR,'
      'gg1.Nome NOMENIVEL1'
      'FROM etqgeraits     etq'
      'LEFT JOIN gragrux   ggx ON ggx.Controle=etq.GraGruX'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gratamcad gtc ON gtc.Codigo=gti.Codigo'
      'LEFT JOIN gragruc   ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE etq.Codigo=:P0'
      'ORDER BY etq.GraGruX, etq.Sequencia')
    Left = 68
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEtqGeraItsREDUZIDO: TIntegerField
      FieldName = 'REDUZIDO'
      Required = True
    end
    object QrEtqGeraItsSequencia: TIntegerField
      FieldName = 'Sequencia'
      Required = True
    end
    object QrEtqGeraItsGraGruC: TIntegerField
      FieldName = 'GraGruC'
    end
    object QrEtqGeraItsNIVEL1: TIntegerField
      FieldName = 'NIVEL1'
    end
    object QrEtqGeraItsCOD_TAM: TIntegerField
      FieldName = 'COD_TAM'
    end
    object QrEtqGeraItsNOMETAM: TWideStringField
      FieldName = 'NOMETAM'
      Size = 5
    end
    object QrEtqGeraItsCOD_GRADE: TIntegerField
      FieldName = 'COD_GRADE'
    end
    object QrEtqGeraItsNOMEGRADE: TWideStringField
      FieldName = 'NOMEGRADE'
      Size = 50
    end
    object QrEtqGeraItsCOD_COR: TIntegerField
      FieldName = 'COD_COR'
    end
    object QrEtqGeraItsNOMECOR: TWideStringField
      FieldName = 'NOMECOR'
      Size = 30
    end
    object QrEtqGeraItsNOMENIVEL1: TWideStringField
      FieldName = 'NOMENIVEL1'
      Size = 30
    end
  end
  object QrEtqPrinCmd_2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM etqprincmd'
      'WHERE Tipo = 2'
      'AND Codigo=:P0'
      'ORDER BY Tipo, Ordem')
    Left = 180
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEtqPrinCmd_2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEtqPrinCmd_2Tipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEtqPrinCmd_2Ordem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrEtqPrinCmd_2Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEtqPrinCmd_2Config: TWideStringField
      FieldName = 'Config'
      Size = 255
    end
  end
  object QrEtqPrinCmd_1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM etqprincmd'
      'WHERE Tipo = 1'
      'AND Codigo=:P0'
      'AND Coluna=:P1'
      'ORDER BY Tipo, Ordem')
    Left = 152
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrEtqPrinCmd_1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEtqPrinCmd_1Tipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEtqPrinCmd_1Ordem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrEtqPrinCmd_1Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEtqPrinCmd_1Config: TWideStringField
      FieldName = 'Config'
      Size = 255
    end
  end
end
