object FmTabePrcGrGMul1: TFmTabePrcGrGMul1
  Left = 339
  Top = 185
  Caption = 'TAB-PRECO-004 :: Pre'#231'o por Grupo de Produtos (m'#250'ltiplo)'
  ClientHeight = 384
  ClientWidth = 530
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 336
    Width = 530
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      TabOrder = 0
      OnClick = BtOKClick
      NumGlyphs = 2
    end
    object Panel2: TPanel
      Left = 418
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 15
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Desiste'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 530
    Height = 48
    Align = alTop
    Caption = 'Pre'#231'o por Grupo de Produtos (m'#250'ltiplo)'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 528
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 530
    Height = 288
    Align = alClient
    TabOrder = 0
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 528
      Height = 64
      Align = alTop
      TabOrder = 0
      object Label1: TLabel
        Left = 16
        Top = 4
        Width = 129
        Height = 13
        Caption = 'Informe parte da descri'#231#227'o:'
      end
      object Label11: TLabel
        Left = 16
        Top = 44
        Width = 287
        Height = 13
        Caption = 'Pesquisas com muitos itens podem ser demoradas!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object EdPesq: TEdit
        Left = 16
        Top = 20
        Width = 369
        Height = 21
        TabOrder = 0
      end
      object BtPesquisa: TBitBtn
        Tag = 22
        Left = 392
        Top = 8
        Width = 90
        Height = 40
        Caption = '&Pesquisar'
        TabOrder = 1
        OnClick = BtPesquisaClick
        NumGlyphs = 2
      end
    end
    object DBGrid1: TdmkDBGrid
      Left = 1
      Top = 65
      Width = 528
      Height = 205
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'Ativo'
          Title.Caption = 'OK'
          Width = 20
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CodUsu'
          Title.Caption = 'C'#243'digo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Title.Caption = 'Descri'#231#227'o'
          Width = 323
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Preco'
          Title.Caption = 'Pre'#231'o'
          Width = 80
          Visible = True
        end>
      Color = clWindow
      DataSource = DataSource1
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnCellClick = DBGrid1CellClick
      OnColEnter = DBGrid1ColEnter
      OnColExit = DBGrid1ColExit
      Columns = <
        item
          Expanded = False
          FieldName = 'Ativo'
          Title.Caption = 'OK'
          Width = 20
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CodUsu'
          Title.Caption = 'C'#243'digo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Title.Caption = 'Descri'#231#227'o'
          Width = 323
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Preco'
          Title.Caption = 'Pre'#231'o'
          Width = 80
          Visible = True
        end>
    end
    object StaticText1: TStaticText
      Left = 1
      Top = 270
      Width = 321
      Height = 17
      Align = alBottom
      Caption = 
        '  Produtos j'#225' adicionados anteriormente ter'#227'o seu pre'#231'o atualiza' +
        'do!'
      TabOrder = 2
    end
  end
  object QrPesq: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.Nivel1, gg1.CodUsu, gg1.Nome, tpg.Preco'
      'FROM gragru1 gg1'
      
        'LEFT JOIN tabeprcgrg tpg ON tpg.Nivel1=gg1.Nivel1 AND tpg.Codigo' +
        '=:P0'
      'WHERE gg1.Nome LIKE :P1'
      'AND gg1.PrdGrupTip=1')
    Left = 68
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPesqNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrPesqCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPesqNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrPesqPreco: TFloatField
      FieldName = 'Preco'
    end
  end
  object DataSource1: TDataSource
    DataSet = Query
    Left = 40
    Top = 12
  end
  object Query: TABSQuery
    CurrentVersion = '6.01 '
    DatabaseName = 'MEMORY'
    InMemory = True
    ReadOnly = False
    RequestLive = True
    Left = 12
    Top = 12
    object QueryNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QueryCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QueryNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QueryPreco: TFloatField
      FieldName = 'Preco'
    end
    object QueryAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
  end
end
