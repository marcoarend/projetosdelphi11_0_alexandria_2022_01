unit PlanCutImpAtrPrd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DB, DBGrids,
  dmkDBGrid, DBCtrls, mySQLDbTables, Menus, ComCtrls, dmkEdit, dmkEditCB,
  dmkDBLookupComboBox, Variants, UnDmkProcFunc;

type
  THackDBGrid = class(TDBGrid);
  TFmPlanCutImpAtrPrd = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    BtNenhum: TBitBtn;
    BtTodos: TBitBtn;
    BtPesq: TBitBtn;
    PMPesq: TPopupMenu;
    Ativa1: TMenuItem;
    Desativa1: TMenuItem;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel3: TPanel;
    Label1: TLabel;
    EdPesq: TEdit;
    DBGrid1: TdmkDBGrid;
    Panel4: TPanel;
    DBGrid2: TdmkDBGrid;
    RGSelecao: TRadioGroup;
    Panel5: TPanel;
    LaPCIAtrCli: TLabel;
    CBPCIAtrPrd: TdmkDBLookupComboBox;
    EdPCIAtrPrd: TdmkEditCB;
    procedure FormCreate(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure FormStartDock(Sender: TObject; var DragObject: TDragDockObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure EdPesqChange(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtPesqClick(Sender: TObject);
    procedure Ativa1Click(Sender: TObject);
    procedure Desativa1Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure RGSelecaoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EdPCIAtrPrdChange(Sender: TObject);
    procedure EdPCIAtrPrdExit(Sender: TObject);
    procedure EdPCIAtrPrdEnter(Sender: TObject);
  private
    { Private declarations }
    FItem: Integer;
    procedure RecarregaItens();
    procedure Pesquisar();
    procedure AtivaItens(Status: Integer);
    procedure AtivaSelecionados(Status: Integer);
  public
    { Public declarations }
    class function CreateDockForm(const aColor: TColor): TCustomForm;
    //class procedure FiltaAtivos();
    procedure LimpaPesquisa();
  end;

var
  FmPlanCutImpAtrPrd: TFmPlanCutImpAtrPrd;

implementation

uses UnMyObjects, Module, UMySQLModule, TabePrcCab, PlanCutImp, ModPlanCut, MyGlyfs;

{$R *.DFM}

///// PROCEDURES E FUNCTIONS DO DOCKFORM

procedure TFmPlanCutImpAtrPrd.Ativa1Click(Sender: TObject);
begin
  AtivaSelecionados(1);
end;

procedure TFmPlanCutImpAtrPrd.AtivaItens(Status: Integer);
begin
  Screen.Cursor := crHourGlass;
  DmPlanCut.QrPCIAtrPrdIts.Close;
  DmPlanCut.QrPCIAtrPrdIts.SQL.Clear;
  DmPlanCut.QrPCIAtrPrdIts.SQL.Add('UPDATE pciatrprd SET Ativo=' + dmkPF.FFP(Status, 0) + ';');
  DmPlanCut.QrPCIAtrPrdIts.SQL.Add('SELECT * FROM pciatrprd;');
  DmPlanCut.QrPCIAtrPrdIts.Open;
  Screen.Cursor := crDefault;
end;

procedure TFmPlanCutImpAtrPrd.AtivaSelecionados(Status: Integer);
begin
  Screen.Cursor := crHourGlass;
  DmPlanCut.QrPCIAtrPrdIts.First;
  while not DmPlanCut.QrPCIAtrPrdIts.Eof do
  begin
    if DmPlanCut.QrPCIAtrPrdItsAtivo.Value <> Status then
    begin
      DmPlanCut.QrPCIAtrPrdIts.Edit;
      DmPlanCut.QrPCIAtrPrdItsAtivo.Value := Status;
      DmPlanCut.QrPCIAtrPrdIts.Post;
    end;
    DmPlanCut.QrPCIAtrPrdIts.Next;
  end;
  Screen.Cursor := crDefault;
end;

procedure TFmPlanCutImpAtrPrd.BtPesqClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPesq, BtPesq);
end;

procedure TFmPlanCutImpAtrPrd.BtNenhumClick(Sender: TObject);
begin
  AtivaItens(0);
end;

procedure TFmPlanCutImpAtrPrd.BtTodosClick(Sender: TObject);
begin
  AtivaItens(1);
end;

procedure TFmPlanCutImpAtrPrd.RecarregaItens();
const
  Txt1 = 'INSERT INTO pciatrprd (Codigo,CodUsu,Nome,Ativo) VALUES(';
var
  Txt2: String;
begin
  DmPlanCut.QrPCIAtrPrdIts.Close;
  DmPlanCut.QrPCIAtrPrdIts.SQL.Clear;
  //
  DmPlanCut.QrPCIAtrPrdIts.SQL.Add('DELETE FROM pciatrprd; ');
  {
  DmPlanCut.QrPCIAtrPrdIts.SQL.Add('DROP TABLE PCIAtrPrd; ');
  DmPlanCut.QrPCIAtrPrdIts.SQL.Add('CREATE TABLE PCIAtrPrd (');
  DmPlanCut.QrPCIAtrPrdIts.SQL.Add('  Codigo  integer      ,');
  DmPlanCut.QrPCIAtrPrdIts.SQL.Add('  CodUsu  integer      ,');
  DmPlanCut.QrPCIAtrPrdIts.SQL.Add('  Nome    varchar(50)  ,');
  DmPlanCut.QrPCIAtrPrdIts.SQL.Add('  Ativo   smallint      ');
  DmPlanCut.QrPCIAtrPrdIts.SQL.Add(');');
  //
  DmPlanCut.QrPCIAtrPrdCad.Close;
  DmPlanCut.QrPCIAtrPrdCad.Open;
  }
  DmPlanCut.QrAtrPrdSubIts.Close;
  DmPlanCut.QrAtrPrdSubIts.Params[0].AsInteger := DmPlanCut.QrPCIAtrPrdCadCodigo.Value;
  DmPlanCut.QrAtrPrdSubIts.Open;
  DmPlanCut.QrAtrPrdSubIts.First;
  while not DmPlanCut.QrAtrPrdSubIts.Eof do
  begin
    Txt2 :=
      dmkPF.FFP(DmPlanCut.QrAtrPrdSubItsCodigo.Value, 0) + ',' +
      dmkPF.FFP(DmPlanCut.QrAtrPrdSubItsCodUsu.Value, 0) + ',' +
      '"' + DmPlanCut.QrAtrPrdSubItsNome.Value + '",' +
      '0);';
    DmPlanCut.QrPCIAtrPrdIts.SQL.Add(Txt1 + Txt2);
    DmPlanCut.QrAtrPrdSubIts.Next;
  end;
  //
  DmPlanCut.QrPCIAtrPrdIts.SQL.Add('SELECT * FROM pciatrprd;');
  DmPlanCut.QrPCIAtrPrdIts.Open;
  try
    PageControl1.ActivePageIndex := 0;
  except
    ;
  end;
  if DmPlanCut.QrPCIAtrPrdCadCodigo.Value <> 0 then
  begin
    FmPlanCutImp.FPCIAtrPrd_Cod := DmPlanCut.QrPCIAtrPrdCadCodigo.Value;
    FmPlanCutImp.FPCIAtrPrd_Txt := DmPlanCut.QrPCIAtrPrdCadNome.Value;
  end else begin
    FmPlanCutImp.FPCIAtrPrd_Txt := '';
    FmPlanCutImp.FPCIAtrPrd_Cod := 0;
  end;
end;

class function TFmPlanCutImpAtrPrd.CreateDockForm(const aColor: TColor): TCustomForm;
begin
  Result := TFmPlanCutImpAtrPrd.Create(Application);
  //result.Color := aColor;
  //result.Caption := ColorToString(aColor);
  FmMyGlyfs.ConfiguraFormDock(result);
  result.Show;
end;

procedure TFmPlanCutImpAtrPrd.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  DmPlanCut.QrPCIAtrPrdIts.Close;
  DmPlanCut.QrPCIAtrPrdIts.SQL.Clear;
  DmPlanCut.QrPCIAtrPrdIts.SQL.Add('DROP TABLE PCIAtrPrd; ');
  DmPlanCut.QrPCIAtrPrdIts.ExecSQL;
  //
  try
    ManualFloat(Rect(0, 0, 0, 0));
  except

  end;
  Action := caFree;
end;

procedure TFmPlanCutImpAtrPrd.FormShow(Sender: TObject);
begin
  FmPlanCutImp.FechaPesquisa();
end;

procedure TFmPlanCutImpAtrPrd.FormStartDock(Sender: TObject;
  var DragObject: TDragDockObject);
begin
  DragObject := TDragDockObjectEx.Create(Self);
  DragObject.Brush.Color := clAqua; // this will display a red outline
end;


procedure TFmPlanCutImpAtrPrd.LimpaPesquisa;
var
  K, I: Integer;
  //Form: TForm;
begin
  for K := 0 to Screen.FormCount - 1 do
  begin
    if Screen.Forms[K] is TFmPlanCutImpAtrPrd then
    begin
      for I := 0 to Screen.Forms[K].ComponentCount -1 do
      begin
        if Screen.Forms[K].Components[I] is TdmkEditCB then
          TdmkEditCB(Screen.Forms[K].Components[I]).Text := '';
        if Screen.Forms[K].Components[I] is TEdit then
          TEdit(Screen.Forms[K].Components[I]).Text := '';
      end;    
    end;
  end;
  AtivaItens(0);
end;

///// FIM DAS PROCEDURES E FUNCTIONS DO DOCKFORM

procedure TFmPlanCutImpAtrPrd.PageControl1Change(Sender: TObject);
begin
  BtPesq.Enabled := PageControl1.ActivePageIndex = 0;
  case PageControl1.ActivePageIndex of
    0: Pesquisar();
    1: RGSelecaoClick(Self);
  end;
end;

procedure TFmPlanCutImpAtrPrd.Pesquisar();
begin
  Screen.Cursor := crHourGlass;
  //
  DmPlanCut.QrPCIAtrPrdIts.Filter   := 'Nome LIKE "%' + EdPesq.Text + '%"';
  DmPlanCut.QrPCIAtrPrdIts.Filtered := True;
  //
  Screen.Cursor := crDefault;
end;

procedure TFmPlanCutImpAtrPrd.RGSelecaoClick(Sender: TObject);
begin
  DmPlanCut.QrPCIAtrPrdIts.Filtered := False;
  case RGSelecao.ItemIndex of
    0: DmPlanCut.QrPCIAtrPrdIts.Filter := 'Ativo=0';
    1: DmPlanCut.QrPCIAtrPrdIts.Filter := 'Ativo=1';
    2: DmPlanCut.QrPCIAtrPrdIts.Filter := '';
  end;
  if RGSelecao.ItemIndex < 2 then
    DmPlanCut.QrPCIAtrPrdIts.Filtered := True;
end;

procedure TFmPlanCutImpAtrPrd.DBGrid1CellClick(Column: TColumn);
var
  Status: Byte;
begin
  if Column.FieldName = 'Ativo' then
  begin
    if DmPlanCut.QrPCIAtrPrdIts.FieldByName('Ativo').Value = 1 then
      Status := 0
    else
      Status := 1;
    //
    DmPlanCut.QrPCIAtrPrdIts.Edit;
    DmPlanCut.QrPCIAtrPrdIts.FieldByName('Ativo').Value := Status;
    DmPlanCut.QrPCIAtrPrdIts.Post;
  end;
  DmPlanCut.QrPCIAtrPrdAti.Close;
  DmPlanCut.QrPCIAtrPrdAti.Open;
  {
  Habilita :=  DmPlanCut.QrPCIAtrPrdAtiItens.Value = 0;
  FmPlanCutImp.LaPCIAtrPrd.Enabled := Habilita;
  FmPlanCutImp.EdPCIAtrPrd.Enabled := Habilita;
  FmPlanCutImp.CBPCIAtrPrd.Enabled := Habilita;
  if not Habilita then
  begin
    FmPlanCutImp.EdPCIAtrPrd.ValueVariant := 0;
    FmPlanCutImp.CBPCIAtrPrd.KeyValue     := 0;
  end;
  }
end;

procedure TFmPlanCutImpAtrPrd.Desativa1Click(Sender: TObject);
begin
  AtivaSelecionados(0);
end;

procedure TFmPlanCutImpAtrPrd.EdPesqChange(Sender: TObject);
begin
  Pesquisar();
end;

procedure TFmPlanCutImpAtrPrd.EdPCIAtrPrdChange(Sender: TObject);
begin
  if not EdPCIAtrPrd.Focused then
    RecarregaItens();
end;

procedure TFmPlanCutImpAtrPrd.EdPCIAtrPrdEnter(Sender: TObject);
begin
  FItem := EdPCIAtrPrd.ValueVariant;
end;

procedure TFmPlanCutImpAtrPrd.EdPCIAtrPrdExit(Sender: TObject);
begin
  if FItem <> EdPCIAtrPrd.ValueVariant then
  begin
    FItem := EdPCIAtrPrd.ValueVariant;
    RecarregaItens();
  end;
end;

procedure TFmPlanCutImpAtrPrd.FormCreate(Sender: TObject);
const
  Txt1 = 'INSERT INTO pciatrprd (Codigo,CodUsu,Nome,Ativo) VALUES(';
begin
  DmPlanCut.QrPCIAtrPrdIts.Close;
  DmPlanCut.QrPCIAtrPrdIts.SQL.Clear;
  DmPlanCut.QrPCIAtrPrdIts.SQL.Add('DROP TABLE PCIAtrPrd; ');
  DmPlanCut.QrPCIAtrPrdIts.SQL.Add('CREATE TABLE PCIAtrPrd (');
  DmPlanCut.QrPCIAtrPrdIts.SQL.Add('  Codigo  integer      ,');
  DmPlanCut.QrPCIAtrPrdIts.SQL.Add('  CodUsu  integer      ,');
  DmPlanCut.QrPCIAtrPrdIts.SQL.Add('  Nome    varchar(50)  ,');
  DmPlanCut.QrPCIAtrPrdIts.SQL.Add('  Ativo   smallint      ');
  DmPlanCut.QrPCIAtrPrdIts.SQL.Add(');');
  //
  DmPlanCut.QrPCIAtrPrdCad.Close;
  DmPlanCut.QrPCIAtrPrdCad.Open;
  {
  while not DmPlanCut.QrPCIAtrPrdCad.Eof do
  begin
    Txt2 :=
      dmkPF.FFP(DmPlanCut.QrPCIAtrPrdCadCodigo.Value, 0) + ',' +
      dmkPF.FFP(DmPlanCut.QrPCIAtrPrdCadCodUsu.Value, 0) + ',' +
      '"' + DmPlanCut.QrPCIAtrPrdCadNome.Value + '",' +
      '0);';
  //for i  := 1 to 10 do
    DmPlanCut.QrPCIAtrPrdIts.SQL.Add(Txt1 + Txt2);
    DmPlanCut.QrPCIAtrPrdCad.Next;
  end;
  //
  }
  DmPlanCut.QrPCIAtrPrdIts.SQL.Add('SELECT * FROM pciatrprd;');
  DmPlanCut.QrPCIAtrPrdIts.Open;
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmPlanCutImpAtrPrd.FormDeactivate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 1;
  RGSelecao.ItemIndex := 1;
  //
  if FindWindow('TFmPlanCutImp', nil) > 0 then
    FmPlanCutImp.AtivaBtConfirma();
end;

end.

