unit PlanCutImp;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Tabs, DockTabSet, ExtCtrls, StdCtrls, Buttons, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, ComCtrls, dmkEditDateTimePicker, DB,
  mySQLDbTables, Grids, DBGrids, frxClass, frxDBSet, dmkDBGrid,
  UnDmkProcFunc, dmkGeral;

type
  TFmPlanCutImp = class(TForm)
    //DockTabSet1: TDockTabSet;
    DockTabSet1: TTabSet;
    pDockLeft: TPanel;
    Splitter1: TSplitter;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    BtConfirma: TBitBtn;
    Panel4: TPanel;
    BtDesiste: TBitBtn;
    frxPED_INPRI_000_01: TfrxReport;
    BtImprime: TBitBtn;
    Panel9: TPanel;
    DBGrid1: TDBGrid;
    BtLimpa: TBitBtn;
    QrGgx: TmySQLQuery;
    DsGgx: TDataSource;
    Panel5: TPanel;
    Panel10: TPanel;
    LaPCIEmp: TLabel;
    LaPCICen: TLabel;
    LaPCIPrd: TLabel;
    EdPCIEmp: TdmkEditCB;
    EdPCICen: TdmkEditCB;
    EdPCIPrd: TdmkEditCB;
    CBPCIPrd: TdmkDBLookupComboBox;
    CBPCICen: TdmkDBLookupComboBox;
    CBPCIEmp: TdmkDBLookupComboBox;
    Panel7: TPanel;
    RGOrdemGru: TRadioGroup;
    RGOrdemCor: TRadioGroup;
    GroupBox5: TGroupBox;
    GroupBox1: TGroupBox;
    TPIncluIni: TdmkEditDateTimePicker;
    TPIncluFim: TdmkEditDateTimePicker;
    CkIncluIni: TCheckBox;
    CkIncluFim: TCheckBox;
    GroupBox2: TGroupBox;
    TPEmissIni: TdmkEditDateTimePicker;
    TPEmissFim: TdmkEditDateTimePicker;
    CkEmissIni: TCheckBox;
    CkEmissFim: TCheckBox;
    GroupBox3: TGroupBox;
    TPEntraIni: TdmkEditDateTimePicker;
    TPEntraFim: TdmkEditDateTimePicker;
    CkEntraIni: TCheckBox;
    CkEntraFim: TCheckBox;
    GroupBox4: TGroupBox;
    TPPreviIni: TdmkEditDateTimePicker;
    TPPreviFim: TdmkEditDateTimePicker;
    CkPreviIni: TCheckBox;
    CkPreviFim: TCheckBox;
    Panel8: TPanel;
    LaAviso1: TLabel;
    Panel11: TPanel;
    CkSemNegativos: TCheckBox;
    frxDsGgx: TfrxDBDataset;
    QrGgxCU_NIVEL1: TIntegerField;
    QrGgxNO_NIVEL1: TWideStringField;
    QrGgxCO_TAM: TIntegerField;
    QrGgxNO_TAM: TWideStringField;
    QrGgxCU_COR: TIntegerField;
    QrGgxNO_COR: TWideStringField;
    QrGgxGraGruC: TIntegerField;
    QrGgxGraGru1: TIntegerField;
    QrGgxGraTamI: TIntegerField;
    QrGgxControle: TIntegerField;
    QrGgxEstq: TFloatField;
    QrGgxPedv: TFloatField;
    QrGgxNec1: TFloatField;
    QrGgxComp: TFloatField;
    QrGgxPrdz: TFloatField;
    QrGgxNec2: TFloatField;
    QrGgxSobr: TFloatField;
    QrGgxAtivo: TIntegerField;
    RGMostrar: TRadioGroup;
    CkRolar: TCheckBox;
    EdTempo: TdmkEdit;
    Label1: TLabel;
    CkPrdGrupo: TCheckBox;
    frxPED_INPRI_000_02: TfrxReport;
    procedure DockTabSet1TabRemoved(Sender: TObject);
    procedure DockTabSet1DockDrop(Sender: TObject; Source: TDragDockObject; X, Y: Integer);
    procedure pDockLeftDockOver(Sender: TObject; Source: TDragDockObject; X, Y: Integer; State: TDragState;
      var Accept: Boolean);
    procedure pDockLeftUnDock(Sender: TObject; Client: TControl; NewTarget: TWinControl; var Allow: Boolean);
    procedure pDockLeftDockDrop(Sender: TObject; Source: TDragDockObject; X, Y: Integer);
    procedure FormCreate(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure RGOrdemGruClick(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure frxPED_INPRI_000_01GetValue(const VarName: string;
      var Value: Variant);
    procedure FormActivate(Sender: TObject);
    procedure DockTabSet1TabAdded(Sender: TObject);
    procedure BtLimpaClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure QrGgxAfterOpen(DataSet: TDataSet);
    procedure QrGgxBeforeClose(DataSet: TDataSet);
    procedure EdPCIEmpChange(Sender: TObject);
    procedure EdPCICenChange(Sender: TObject);
    procedure EdPCIPrdChange(Sender: TObject);
    procedure RGOrdemCorClick(Sender: TObject);
    procedure CkSemNegativosClick(Sender: TObject);
    procedure CkHideSemValClick(Sender: TObject);
    procedure CkIncluIniClick(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure RGMostrarClick(Sender: TObject);
    procedure CkPrdGrupoClick(Sender: TObject);
  private
    { Private declarations }
    FInRevise: Boolean;
    FLcoTb001: String;
    //
    procedure RecriaDockForms();
    procedure RevisaDockForms();
    procedure CriaQrPCI();
    procedure ListaQrPCI();
    //
    procedure VerificaSQLPrd(Campo: String; var SQL: String);
    procedure VerificaSQLCen(Liga, Campo: String; var SQL: String);
    procedure VerificaSQLEmp(Liga, Campo: String; var SQL: String);
    procedure VerificaSQLAtr(var SQL: String);
    procedure MostraAviso(LaTipo: TLabel; var Seq: Integer; Msg: String);
    //
    procedure ConfiguraGrade(PrdGrupo: Boolean);
  public
    { Public declarations }
    //FPCIAtrCli_Txt,
    //FPCIAtrRep_Txt,
    FPCIAtrPrd_Txt: String;
    //FPCIAtrCli_Cod,
    //FPCIAtrRep_Cod,
    FPCIAtrPrd_Cod: Integer;
    //
    procedure AtivaBtConfirma();
    procedure FechaPesquisa();
  end;

var
  FmPlanCutImp: TFmPlanCutImp;

implementation

uses UnMyObjects, UMySQLModule, UnMLAGeral,
// DockForms
PlanCutImpEmp, PlanCutImpCen, PlanCutImpPrd, PlanCutImpAtrPrd,
// FIM DockForms
Module, ModPlanCut, ModuleGeral, UCreate, UnInternalConsts;

{$R *.dfm}

procedure TFmPlanCutImp.AtivaBtConfirma();
begin
  BtConfirma.Enabled := True;
end;

procedure TFmPlanCutImp.BtImprimeClick(Sender: TObject);
var
  Tempo: TDateTime;
begin
  Tempo            := Now();
  LaAviso1.Caption := 'AGUARDE. Gerando visualiza��o...';
  //
  if CkRolar.Checked = False then
    QrGgx.DisableControls;
  //
  if CkPrdGrupo.Checked = False then
  begin
    MyObjects.frxDefineDataSets(frxPED_INPRI_000_01, [
      DModG.frxDsDono,
      frxDsGgx
      ]);
    //
    MyObjects.frxMostra(frxPED_INPRI_000_01, Caption);
  end else
  begin
    MyObjects.frxDefineDataSets(frxPED_INPRI_000_01, [
      DModG.frxDsDono,
      frxDsGgx
      ]);
    //
    MyObjects.frxMostra(frxPED_INPRI_000_02, Caption);
  end;
  //
  QrGgx.EnableControls;
  LaAviso1.Caption := '...';
  EdTempo.ValueVariant := FormatDateTime('hh:nn:ss:zzz',Now() - Tempo);
end;

procedure TFmPlanCutImp.BtLimpaClick(Sender: TObject);
begin
  RecriaDockForms();
  //
  EdPCIEmp.ValueVariant := 0;
  CBPCIEmp.KeyValue     := Null;
  EdPCICen.ValueVariant := 0;
  CBPCICen.KeyValue     := Null;
  EdPCIPrd.ValueVariant := 0;
  CBPCIPrd.KeyValue     := Null;
  //
  CkEmissFim.Checked    := True;
  CkEmissIni.Checked    := True;
  CkEntraFim.Checked    := True;
  CkEntraIni.Checked    := True;
  CkIncluFim.Checked    := True;
  CkIncluIni.Checked    := True;
  CkPreviFim.Checked    := True;
  CkPreviIni.Checked    := True;
  //
  TPIncluIni.Date := 0;
  TPIncluFim.Date := Date;
  TPEmissIni.Date := 0;
  TPEmissFim.Date := Date;
  TPEntraIni.Date := 0;
  TPEntraFim.Date := Date;
  TPPreviIni.Date := 0;
  TPPreviFim.Date := Date + 180;
  //
  FmPlanCutImpEmp.LimpaPesquisa();
  FmPlanCutImpCen.LimpaPesquisa();
  FmPlanCutImpPrd.LimpaPesquisa();
  FmPlanCutImpAtrPrd.LimpaPesquisa();
  //
end;

procedure TFmPlanCutImp.BtConfirmaClick(Sender: TObject);
var
  SQL1, SQL2, SQL2a, SQL2b, SQL3, SQL4, SQL_Temp, Ordem: String;
  Seq: Integer;
  Tempo: TDateTime;
begin
  Screen.Cursor := crHourGlass;
  //
  Tempo := Now();
  Seq := 0;
  MostraAviso(LaAviso1, Seq, 'Pesquisando reduzidos conforme filtros da pesquisa...');
  CriaQrPCI();
  ListaQrPCI();
  UMyMod.ExecutaDB(DmodG.MyPID_DB, 'DELETE FROM ' + FLcoTb001);
  //
  SQL1 := SQL1 + 'INSERT INTO ' + FLcoTb001 + sLineBreak;
  SQL1 := SQL1 + 'SELECT ggx.GraGruC, ggx.GraGru1, ggx.GraTamI,' + sLineBreak;
  SQL1 := SQL1 + 'ggx.Controle, 0 Estq, 0 Comp, 0 Pedv, 0 Prdz,' + sLineBreak;
  SQL1 := SQL1 + '0 Nec1, 0 Nec2, 0 Sobr, 1 Ativo' + sLineBreak;
  SQL1 := SQL1 + 'FROM ' + TMeuDB + '.gragrux ggx' + sLineBreak;
  if (DmPlanCut.QrPCIAtrPrdIts.RecordCount > 0) and (FPCIAtrPrd_Cod <> 0) then
  begin
    SQL1 := SQL1 + 'LEFT JOIN ' + TMeuDB + '.gragruatr gga ' +
      'ON gga.Nivel1=ggx.GraGru1 AND gga.GraAtrCad=' +
      FormatFloat('0', FPCIAtrPrd_Cod) + sLineBreak;
    // n�o precisa
    //SQL1 := SQL1 +'LEFT JOIN graatrits gai ON gai.Controle=gga.GraAtrIts ');
  end;
  SQL1 := SQL1 +'WHERE ggx.Controle > -1000000' + sLineBreak;
  //
  VerificaSQLPrd('ggx.GraGru1', SQL1);
  VerificaSQLAtr(SQL1);
  //
  UMyMod.ExecutaDB(DmodG.MyPID_DB, SQL1);
  //
  MostraAviso(LaAviso1, Seq, 'Verificando estoques conforme filtros da pesquisa...');
  DmodG.QrUpdPID1.SQL.Clear;
  SQL2a := '';
  VerificaSQLEmp('  WHERE', 'Empresa', SQL2a);
  SQL2b := '';
  if Trim(SQL2a) <> '' then
    VerificaSQLCen('  AND', 'StqCenCad', SQL2b)
  else
    VerificaSQLCen('  WHERE', 'StqCenCad', SQL2b);
  SQL2 :=
    'UPDATE ' + FLcoTb001 + ',' + sLineBreak+
    '(' + sLineBreak +
    '  SELECT GraGruX, SUM(Qtde * Baixa) AS QtdeSum' + sLineBreak +
    '  FROM ' + TMeuDB + '.stqmovitsa' + sLineBreak +
    SQL2a +
    SQL2b +
    ' AND Ativo = 1 ' +
    '  GROUP BY GraGruX' + sLineBreak +
    ') AS T' + sLineBreak +
    'SET ' + FLcoTb001 + '.Estq=QtdeSum WHERE ' + FLcoTb001 + '.Controle=T.GraGrux';
  //
  UMyMod.ExecutaDB(DmodG.MyPID_DB, SQL2);
  //
  if CkSemNegativos.Checked then
  begin
    MostraAviso(LaAviso1, Seq, 'Zerando estoques negativos na pesquisa...');
    UMyMod.ExecutaDB(DmodG.MyPID_DB, 'UPDATE ' + FLcoTb001 + ' SET Estq=0 WHERE Estq<0');
  end;
  //
  MostraAviso(LaAviso1, Seq, 'Verificando pedidos conforme filtros da pesquisa...');
  SQL3 :=
    'UPDATE ' + FLcoTb001 + ',' + sLineBreak +
    '(' + sLineBreak +
    '  SELECT pvi.GraGruX, SUM(pvi.QuantP-pvi.QuantC-pvi.QuantV) AS QtdeSum' + sLineBreak+
    '  FROM ' + TMeuDB + '.pedivdaits pvi' + sLineBreak +
    '  LEFT JOIN ' + TMeuDB + '.pedivda pvd ON pvd.Codigo=pvi.Codigo' + sLineBreak +
    '  WHERE pvi.QuantP > pvi.QuantC+pvi.QuantV' + sLineBreak;
    VerificaSQLEmp('  AND', 'Empresa', SQL3);


    // Data da inclus�o
    SQL_Temp := dmkPF.SQL_Periodo('  AND pvd.DtaInclu ',
      TPIncluIni.Date, TPIncluFim.Date, CkIncluIni.Checked, CkIncluFim.Checked);
    if SQL_Temp <> '' then
      SQL3 := SQL3 + SQL_Temp + sLineBreak;
    //
    // Data da emiss�o
    SQL_Temp := dmkPF.SQL_Periodo('  AND pvd.DtaEmiss ',
      TPEmissIni.Date, TPEmissFim.Date, CkEmissIni.Checked, CkEmissFim.Checked);
    if SQL_Temp <> '' then
      SQL3 := SQL3 + SQL_Temp + sLineBreak;
    //
    // Data da entrada
    SQL_Temp := dmkPF.SQL_Periodo('  AND pvd.DtaEntra ',
      TPEntraIni.Date, TPEntraFim.Date, CkEntraIni.Checked, CkEntraFim.Checked);
    if SQL_Temp <> '' then
      SQL3 := SQL3 + SQL_Temp + sLineBreak;
    //
    // Data da previs�o
    SQL_Temp := dmkPF.SQL_Periodo('  AND pvd.DtaPrevi ',
      TPPreviIni.Date, TPPreviFim.Date, CkPreviIni.Checked, CkPreviFim.Checked);
    if SQL_Temp <> '' then
      SQL3 := SQL3 + SQL_Temp + sLineBreak;
    //
    SQL3 := SQL3 +
    '  GROUP BY pvi.GraGruX' + sLineBreak +
    ') AS T' + sLineBreak +
    'SET ' + FLcoTb001 + '.Pedv=QtdeSum WHERE ' + FLcoTb001 + '.Controle=T.GraGrux' + sLineBreak;
  //
  UMyMod.ExecutaDB(DmodG.MyPID_DB, SQL3);


  //
  MostraAviso(LaAviso1, Seq, 'Definindo necessidades...');
  SQL4 :=
    'UPDATE ' + FLcoTb001 + ' SET Nec1=Estq-Pedv,' +
    'Nec2=IF(Estq-Pedv+Comp+Prdz<0,Estq-Pedv+Comp+Prdz,0),' +
    'Sobr=IF(Estq-Pedv+Comp+Prdz>0,Estq-Pedv+Comp+Prdz,0)';
  UMyMod.ExecutaDB(DmodG.MyPID_DB, SQL4);



  //
  MostraAviso(LaAviso1, Seq, 'Abrindo tabela com resultados da pesquisa...');
  QrGgx.Close;
  QrGgx.SQL.Clear;
  (*
  //QrGgx.SQL.Add('SELECT * FROM ' + FLcoTb001);
  QrGgx.SQL.Add('SELECT gg1.CodUsu CU_NIVEL1, gg1.Nome NO_NIVEL1,');
  QrGgx.SQL.Add('gti.Controle CO_TAM, gti.Nome NO_TAM,');
  //QrGgx.SQL.Add('gtc.CodUsu CU_GRADE, gtc.Nome NO_GRADE,');
  QrGgx.SQL.Add('gcc.CodUsu CU_COR, gcc.Nome NO_COR, pci.*');
  *)
  if CkPrdGrupo.Checked = False then
  begin
    QrGgx.SQL.Add('SELECT gg1.CodUsu CU_NIVEL1, gg1.Nome NO_NIVEL1, ');
    QrGgx.SQL.Add('gti.Controle CO_TAM, gti.Nome NO_TAM, ');
    QrGgx.SQL.Add('gcc.CodUsu CU_COR, gcc.Nome NO_COR, ');
    QrGgx.SQL.Add('pci.GraGruC, pci.GraGru1, pci.GraTamI, pci.Controle, ');
    QrGgx.SQL.Add('Estq, Pedv, Nec1, Comp, Prdz, Nec2, Sobr, pci.Ativo ');
  end else
  begin
    QrGgx.SQL.Add('SELECT gg1.CodUsu CU_NIVEL1, gg1.Nome NO_NIVEL1, ');
    QrGgx.SQL.Add('gti.Controle CO_TAM, gti.Nome NO_TAM, ');
    QrGgx.SQL.Add('gcc.CodUsu CU_COR, gcc.Nome NO_COR, ');
    QrGgx.SQL.Add('pci.GraGruC, pci.GraGru1, pci.GraTamI, pci.Controle, ');
    QrGgx.SQL.Add('SUM(Estq) Estq, SUM(Pedv) Pedv, SUM(Nec1) Nec1, ');
    QrGgx.SQL.Add('SUM(Comp) Comp, SUM(Prdz) Prdz, SUM(Nec2) Nec2, ');
    QrGgx.SQL.Add('SUM(Sobr) Sobr, pci.Ativo ');
  end;
  QrGgx.SQL.Add('FROM ' + FLcoTb001 + ' pci');
  QrGgx.SQL.Add('LEFT JOIN ' + TMeuDB + '.gragru1 gg1 ON gg1.Nivel1=pci.GraGru1');
  QrGgx.SQL.Add('LEFT JOIN ' + TMeuDB + '.gratamits gti ON gti.Controle=pci.GraTamI');
  QrGgx.SQL.Add('LEFT JOIN ' + TMeuDB + '.gratamcad gtc ON gtc.Codigo=gti.Codigo');
  QrGgx.SQL.Add('LEFT JOIN ' + TMeuDB + '.gragruc   ggc ON ggc.Controle=pci.GraGruC');
  QrGgx.SQL.Add('LEFT JOIN ' + TMeuDB + '.gracorcad gcc ON gcc.Codigo=ggc.GraCorCad');
  case RGMostrar.ItemIndex of
    0: ;//nada
    1: QrGgx.SQL.Add('WHERE pci.Estq <> 0 OR pci.Comp <> 0 OR pci.Pedv <> 0 OR pci.Prdz <> 0');
    2: QrGgx.SQL.Add('WHERE pci.Nec2 < 0');
  end;
  if CkPrdGrupo.Checked = True then
    QrGgx.SQL.Add('GROUP BY gg1.Nivel1 ');
  //
  QrGgx.SQL.Add('');
  //
  Ordem := 'ORDER BY ';
  case RGOrdemGru.ItemIndex of
    0: Ordem := Ordem + 'CU_NIVEL1, ';
    1: Ordem := Ordem + 'NO_NIVEL1, ';
  end;
  case RGOrdemCor.ItemIndex of
    0: Ordem := Ordem + 'CU_COR, ';
    1: Ordem := Ordem + 'NO_COR, ';
  end;
  Ordem := Ordem + 'CO_TAM';
  QrGgx.SQL.Add(Ordem);
  //
  UMyMod.AbreQuery(QrGgx, DModG.MyPID_DB, 'TFmPlanCutImp.BtConfirmaClick()');
  LaAviso1.Caption := 'Pesquisa conclu�da!';
  //
  EdTempo.ValueVariant := FormatDateTime('hh:nn:ss:zzz',Now() - Tempo);
  Screen.Cursor := crDefault;
end;

procedure TFmPlanCutImp.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPlanCutImp.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  CorTexto, CorFundo: TColor;  
begin
  if QrGgxNec2.Value < 0 then
  begin
    CorTexto := clRed;
    CorFundo := $00E4E4E4;
  end else begin
    CorTexto := clNavy;
    CorFundo := clWhite;
  end;
  MyObjects.DesenhaTextoEmDBGrid(DBGrid1, Rect, CorTexto, CorFundo,
    Column.Alignment, Column.Field.DisplayText);
end;

procedure TFmPlanCutImp.DockTabSet1DockDrop(Sender: TObject; Source: TDragDockObject; X, Y: Integer);
begin
  DockTabSet1.Visible := True;
end;

procedure TFmPlanCutImp.DockTabSet1TabAdded(Sender: TObject);
begin
  DockTabSet1.Visible := DockTabSet1.Tabs.Count > 0;
end;

procedure TFmPlanCutImp.DockTabSet1TabRemoved(Sender: TObject);
begin
  DockTabSet1.Visible := DockTabSet1.Tabs.Count > 0;
  if FInRevise then Exit;
  RevisaDockForms();
end;

procedure TFmPlanCutImp.EdPCICenChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmPlanCutImp.EdPCIEmpChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmPlanCutImp.EdPCIPrdChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmPlanCutImp.FechaPesquisa;
begin
  LaAviso1.Caption := '...';
  QrGgx.Close;
end;

procedure TFmPlanCutImp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPlanCutImp.FormCreate(Sender: TObject);
var
  FmtQtde: String;
begin
  FmtQtde   := MLAGeral.FormataCasas(Dmod.QrControleCasasProd.Value);
  FLcoTb001 := UCriar.RecriaTempTable('pci1', DmodG.QrUpdPID1, False);
  //
  TFmPlanCutImpEmp.CreateDockForm(clWhite).ManualDock(DockTabSet1);
{
  TFmPlanCutImpCen.CreateDockForm(clWhite).ManualDock(DockTabSet1);
  TFmPlanCutImpPrd.CreateDockForm(clWhite).ManualDock(DockTabSet1);
  TFmPlanCutImpAtrPrd.CreateDockForm(clWhite).ManualDock(DockTabSet1);
  //
}
  TPIncluIni.Date := 0;
  TPIncluFim.Date := Date;
  TPEmissIni.Date := 0;
  TPEmissFim.Date := Date;
  TPEntraIni.Date := 0;
  TPEntraFim.Date := Date;
  TPPreviIni.Date := 0;
  TPPreviFim.Date := Date + 180;
  //
  QrGgxEstq.DisplayFormat := FmtQtde;
  QrGgxComp.DisplayFormat := FmtQtde;
  QrGgxNec1.DisplayFormat := FmtQtde;
  QrGgxPedv.DisplayFormat := FmtQtde;
  QrGgxPrdz.DisplayFormat := FmtQtde;
  QrGgxNec2.DisplayFormat := FmtQtde;
  QrGgxSobr.DisplayFormat := FmtQtde;
  //
  EdPCIEmp.ValueVariant := DModG.QrFiliLogFilial.Value;
  CBPCIEmp.KeyValue     := DModG.QrFiliLogFilial.Value;
  //
  CkPrdGrupo.Checked := False;
  //
  ConfiguraGrade(False);
end;

procedure TFmPlanCutImp.FormDestroy(Sender: TObject);
begin
  //DModG.ExcluiTempTable(FLcoTb001);
end;

procedure TFmPlanCutImp.frxPED_INPRI_000_01GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_SUB_TITULO' then
    Value := '? ? ? ? ? '
  else
  if VarName = 'VARF_PERIODO_INCLU' then
    Value := dmkPF.PeriodoImp2(TPIncluIni.Date, TPIncluFim.Date,
      CkIncluIni.Checked, CkIncluFim.Checked, '', '', '')
  else
  if VarName = 'VARF_PERIODO_EMISS' then
    Value := dmkPF.PeriodoImp2(TPEmissIni.Date, TPEmissFim.Date,
      CkEmissIni.Checked, CkEmissFim.Checked, '', '', '')
  else
  if VarName = 'VARF_PERIODO_ENTRA' then
    Value := dmkPF.PeriodoImp2(TPEntraIni.Date, TPEntraFim.Date,
      CkEntraIni.Checked, CkEntraFim.Checked, '', '', '')
  else
  if VarName = 'VARF_PERIODO_PREVI' then
    Value := dmkPF.PeriodoImp2(TPPreviIni.Date, TPPreviFim.Date,
      CkPreviIni.Checked, CkPreviFim.Checked, '', '', '')
  else
  if VarName = 'VARF_FMT' then
    Value := MLAGeral.FormataCasas(Dmod.QrControleCasasProd.Value)
end;

procedure TFmPlanCutImp.pDockLeftDockDrop(Sender: TObject; Source: TDragDockObject; X, Y: Integer);
begin
  if pDockLeft.Width = 0 then
    pDockLeft.Width := 150;
  Splitter1.Visible := True;
  Splitter1.Left := pDockLeft.Width;
end;

procedure TFmPlanCutImp.pDockLeftDockOver(Sender: TObject; Source: TDragDockObject; X, Y: Integer; State: TDragState;
  var Accept: Boolean);
var
  lRect: TRect;
begin
  Accept := Source.Control is TFmPlanCutImpEmp;
  if Accept then
  begin
    lRect.TopLeft := pDockLeft.ClientToScreen(Point(0, 0));
    lRect.BottomRight := pDockLeft.ClientToScreen(Point(150, pDockLeft.Height));
    Source.DockRect := lRect;
  end;
end;

procedure TFmPlanCutImp.pDockLeftUnDock(Sender: TObject; Client: TControl; NewTarget: TWinControl; var Allow: Boolean);
begin
  if pDockLeft.DockClientCount = 1 then
  begin
    pDockLeft.Width := 0;
    Splitter1.Visible := False;
  end;
end;

procedure TFmPlanCutImp.QrGgxAfterOpen(DataSet: TDataSet);
begin
  BtImprime.Enabled := QrGgx.RecordCount > 0;
end;

procedure TFmPlanCutImp.QrGgxBeforeClose(DataSet: TDataSet);
begin
  BtImprime.Enabled := False;
end;

procedure TFmPlanCutImp.RecriaDockForms();
begin
  {
  for i := 0 to Screen.FormCount - 1 do
  begin
    ShowMessage(Screen.Forms[i].Name + sLineBreak + Screen.Forms[i].Caption);
    if Screen.Forms[i] is TFmPlanCutImpEmp then
    begin
      Screen.Forms[i].Close;
      TFmPlanCutImpEmp.CreateDockForm(clWhite).ManualDock(DockTabSet1);
    end;
    if Screen.Forms[i] is TFmPlanCutImpCen then
    begin
      Screen.Forms[i].Close;
      TFmPlanCutImpCen.CreateDockForm(clWhite).ManualDock(DockTabSet1);
    end;
    if Screen.Forms[i] is TFmPlanCutImpPrd then
    begin
      Screen.Forms[i].Close;
      TFmPlanCutImpPrd.CreateDockForm(clWhite).ManualDock(DockTabSet1);
    end;
    if Screen.Forms[i] is TFmPlanCutImpAtrPrd then
    begin
      Screen.Forms[i].Close;
      TFmPlanCutImpAtrPrd.CreateDockForm(clWhite).ManualDock(DockTabSet1);
    end;
  end;
  }
end;

procedure TFmPlanCutImp.RevisaDockForms();
const
  Janelas: array [0..3] of String = ('Empresas', 'Centros de Estoque',
  'Grupos de Produtos', 'Atributos de Produtos');
var
  I, J, K: Integer;
  Achou: Boolean;
  //CustomForm: TCustomForm;
begin
  FInRevise := True;
  //for J := Low(Janelas) to High(Janelas) do
  for J := 0 to 6 do
  begin
    Achou := False;
    for I  := 0 to DockTabSet1.Tabs.Count - 1 do
    begin
      if Uppercase(Janelas[J]) = Uppercase(DockTabSet1.Tabs[I]) then
      begin
        Achou := True;
        Break;
      end;
    end;
    if not Achou then
    begin
      case J of
        0:
        begin
          for K := 0 to Screen.FormCount - 1 do
          begin
            if Screen.Forms[K] is TFmPlanCutImpEmp then
            begin
              Screen.Forms[K].Close;
              TFmPlanCutImpEmp.CreateDockForm(clWhite).ManualDock(DockTabSet1);
            end;
          end;
        end;
        1:
        begin
          for K := 0 to Screen.FormCount - 1 do
          begin
            if Screen.Forms[K] is TFmPlanCutImpCen then
            begin
              Screen.Forms[K].Close;
              TFmPlanCutImpCen.CreateDockForm(clWhite).ManualDock(DockTabSet1);
            end;
          end;
        end;
        2:
        begin
          for K := 0 to Screen.FormCount - 1 do
          begin
            if Screen.Forms[K] is TFmPlanCutImpPrd then
            begin
              Screen.Forms[K].Close;
              TFmPlanCutImpPrd.CreateDockForm(clWhite).ManualDock(DockTabSet1);
            end;
          end;
        end;
        3:
        begin
          for K := 0 to Screen.FormCount - 1 do
          begin
            if Screen.Forms[K] is TFmPlanCutImpAtrPrd then
            begin
              Screen.Forms[K].Close;
              TFmPlanCutImpAtrPrd.CreateDockForm(clWhite).ManualDock(DockTabSet1);
            end;
          end;
        end;
        //
      end;
    end;
  end;
  FInRevise := False;
end;

procedure TFmPlanCutImp.RGMostrarClick(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmPlanCutImp.RGOrdemCorClick(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmPlanCutImp.RGOrdemGruClick(Sender: TObject);
begin
  AtivaBtConfirma();
  FechaPesquisa();
end;

procedure TFmPlanCutImp.VerificaSQLPrd(Campo: String; var SQL: String);
var
  Aux, Sep: String;
begin
  if EdPCIPrd.Enabled  then
  begin
    if EdPCIPrd.ValueVariant <> 0 then
    begin
      SQL := SQL + 'AND ' + Campo + ' = ' +
        FormatFloat('0', DmPlanCut.QrPCIPrdCadNivel1.Value) + sLineBreak;
    end;
  end else begin
    if DmPlanCut.QrPCIPrdIts.RecordCount > 0 then
    begin
      Sep := '';
      Aux := 'AND ' + Campo + ' IN (';
      DmPlanCut.QrPCIPrdIts.First;
      while not DmPlanCut.QrPCIPrdIts.Eof do
      begin
        if DmPlanCut.QrPCIPrdItsAtivo.Value = 1 then
        begin
          Aux := Aux + Sep + FormatFloat('0', DmPlanCut.QrPCIPrdItsCodigo.Value);
          Sep := ',';
        end;
        //
        DmPlanCut.QrPCIPrdIts.Next;
      end;
      SQL := SQL + Aux + ')' + sLineBreak;
    end;
  end;
end;

procedure TFmPlanCutImp.VerificaSQLCen(Liga, Campo: String; var SQL: String);
var
  Sep, Aux: String;
begin
  if EdPCICen.Enabled  then
  begin
    if EdPCICen.ValueVariant <> 0 then
      SQL := SQL + Liga + ' ' + Campo + ' = ' + FormatFloat('0',
      DmPlanCut.QrPCICenCadCodigo.Value) + sLineBreak;
  end else begin
    if DmPlanCut.QrPCICenIts.RecordCount > 0 then
    begin
      Sep := '';
      Aux := Liga + ' ' + Campo + ' IN (';
      DmPlanCut.QrPCICenIts.First;
      while not DmPlanCut.QrPCICenIts.Eof do
      begin
        if DmPlanCut.QrPCICenItsAtivo.Value = 1 then
        begin
          Aux := Aux + Sep + FormatFloat('0', DmPlanCut.QrPCICenItsCodigo.Value);
          Sep := ',';
        end;
        //
        DmPlanCut.QrPCICenIts.Next;
      end;
      SQL := SQL + Aux + ')' + sLineBreak;
    end;
  end;
end;

procedure TFmPlanCutImp.VerificaSQLEmp(Liga, Campo: String; var SQL: String);
var
  Sep, Aux: String;
begin
  if EdPCIEmp.Enabled  then
  begin
    if EdPCIEmp.ValueVariant <> 0 then
    begin
      SQL := SQL + Liga + ' ' + Campo + ' = ' + FormatFloat('0',
        DmPlanCut.QrPCIEmpCadCodigo.Value) + sLineBreak
    end;
  end else begin
    if DmPlanCut.QrPCIEmpIts.RecordCount > 0 then
    begin
      Sep := '';
      Aux := Liga + ' ' + Campo + ' IN (';
      DmPlanCut.QrPCIEmpIts.First;
      while not DmPlanCut.QrPCIEmpIts.Eof do
      begin
        if DmPlanCut.QrPCIEmpItsAtivo.Value = 1 then
        begin
          Aux := Aux + Sep + FormatFloat('0', DmPlanCut.QrPCIEmpItsCodigo.Value);
          Sep := ',';
        end;
        DmPlanCut.QrPCIEmpIts.Next;
      end;
      SQL := SQL + Aux + ')' + sLineBreak;
    end;
  end;
end;

procedure TFmPlanCutImp.VerificaSQLAtr(var SQL: String);
{ Ver no Tokyo!
var
  Sep, Aux: String;
  QrABS: TABSQuery;
}
begin
{ Ver no Tokyo!
  QrABS := DmPlanCut.QrPCIAtrPrdIts;
  if QrABS.RecordCount > 0 then
  begin
    Sep := '';
    Aux := 'AND gga.GraAtrIts IN (';
    //
    QrABS.First;
    while not QrABS.Eof do
    begin
      if QrABS.FieldByName('Ativo').AsInteger = 1 then
      begin
        Aux := Aux + Sep + FormatFloat('0', QrABS.FieldByName('Codigo').AsInteger);
        Sep := ',';
      end;
      //
      QrABS.Next;
    end;
    SQL := SQL + Aux + ')' + sLineBreak;
  end;
}
end;

procedure TFmPlanCutImp.CkHideSemValClick(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmPlanCutImp.CkIncluIniClick(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmPlanCutImp.CkPrdGrupoClick(Sender: TObject);
begin
  ConfiguraGrade(CkPrdGrupo.Checked);
  FechaPesquisa();
end;

procedure TFmPlanCutImp.CkSemNegativosClick(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmPlanCutImp.ConfiguraGrade(PrdGrupo: Boolean);
begin
  DBGrid1.Columns[2].Visible := not PrdGrupo;
  DBGrid1.Columns[3].Visible := not PrdGrupo;
  DBGrid1.Columns[4].Visible := not PrdGrupo;
end;

procedure TFmPlanCutImp.CriaQrPCI();
const
  Txt1 = 'INSERT INTO pci (Codigo,CodUsu,Nome,Ativo) VALUES(';
begin
  {
  QrPCI.Close;
  QrPCI.SQL.Clear;
  QrPCI.SQL.Add('DROP TABLE PCI;         ');
  QrPCI.SQL.Add('CREATE TABLE PCI (      ');
  QrPCI.SQL.Add('  Tabela  varchar(30)  ,');
  QrPCI.SQL.Add('  CodUsu1 integer      ,');
  QrPCI.SQL.Add('  Nome1   varchar(50)  ,');
  QrPCI.SQL.Add('  CodUsu2 integer      ,');
  QrPCI.SQL.Add('  Nome2   varchar(50)  ,');
  QrPCI.SQL.Add('  CodUsu3 integer      ,');
  QrPCI.SQL.Add('  Nome3   varchar(50)  ,');
  QrPCI.SQL.Add('  Ativo   smallint      ');
  QrPCI.SQL.Add(');                      ');
  //
  QrPCI.SQL.Add('SELECT * FROM pci;');
  QrPCI.Open;
  //
  FCriouPCI := True;
  }
end;

procedure TFmPlanCutImp.ListaQrPCI();
{
Ver no Tokyo!
  procedure AdicionaItensTabela(Tabela: String; Qry: TABSQuery);
  const
    Txt1 = 'INSERT INTO pci (Tabela,' +
    'CodUsu1,Nome1,CodUsu2,Nome2,CodUsu3,Nome3,Ativo) VALUES(';
  var
    Txt2: String;
    j: Integer;
    Str1, Str2, Str3: String;
  begin
    j := 0;
    Qry.First;
    while not Qry.Eof do
    begin
      if Qry.FieldByName('Ativo').AsInteger = 1 then
      begin
        if j = 0 then
        begin
          Txt2 := '"' + Tabela + '",';
          Str1 := '0," ",';
          Str2 := '0," ",';
          Str3 := '0," ",1);';
          j := 1;
        end;
        case j of
          1:
          begin
            Str1 :=
            dmkPF.FFP(Qry.FieldByName('CodUsu').AsInteger, 0) + ',' +
            '"' + Qry.FieldByName('Nome').AsString + '",';
            j := 2;
          end;
          2:
          begin
            Str2 :=
            dmkPF.FFP(Qry.FieldByName('CodUsu').AsInteger, 0) + ',' +
            '"' + Qry.FieldByName('Nome').AsString + '",';
            j := 3;
          end;
          3:
          begin
            Str3 :=
            dmkPF.FFP(Qry.FieldByName('CodUsu').AsInteger, 0) + ',' +
            '"' + Qry.FieldByName('Nome').AsString + '",1);';
            //
            // ? QrPCI.SQL.Add(Txt1 + Txt2 + Str1 + Str2 + Str3);
            j := 0;
          end;
        end;
      end;
      Qry.Next;
    end;
    if j > 0 then
      // ?QrPCI.SQL.Add(Txt1 + Txt2 + Str1 + Str2 + Str3);
  end;
}
begin
  {
  QrPCI.Close;
  QrPCI.SQL.Clear;
  QrPCI.SQL.Add('DELETE FROM pci; ');
  //
  AtribPrd := 'Atributo de produto: ' + FPCIAtrPrd_Txt;
  //
  AdicionaItensTabela('Filial',                    DmPlanCut.QrPCIEmpIts);
  AdicionaItensTabela('Centro de estoque',         DmPlanCut.QrPCICenIts);
  AdicionaItensTabela('Grupo de produto',          DmPlanCut.QrPCIPrdIts);
  AdicionaItensTabela(AtribPrd,                    DmPlanCut.QrPCIAtrPrdIts);
  //
  QrPCI.SQL.Add('SELECT * FROM pci;');
  UMyMod.AbreABSQuery(QrPCI);
  }
end;

procedure TFmPlanCutImp.MostraAviso(LaTipo: TLabel; var Seq: Integer;
  Msg: String);
begin
  Seq := Seq + 1;
  if LaTipo <> nil then
  begin
    LaTipo.Caption := FormatFloat('000', Seq) + '. ' + Msg;
    LaTipo.Update;
    Application.ProcessMessages;
  end;
end;

(*
object DockTabSet1: TDockTabSet
  Left = 1292
  Top = 63
  Width = 26
  Height = 608
  Margins.Left = 4
  Margins.Top = 4
  Margins.Right = 4
  Margins.Bottom = 4
  Align = alRight
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -17
  Font.Name = 'Tahoma'
  Font.Style = [fsBold]
  ShrinkToFit = True
  Style = tsModernPopout
  TabPosition = tpLeft
  DockSite = False
  DestinationDockSite = pDockLeft
  OnDockDrop = DockTabSet1DockDrop
  OnTabAdded = DockTabSet1TabAdded
  OnTabRemoved = DockTabSet1TabRemoved
end
*)

end.

