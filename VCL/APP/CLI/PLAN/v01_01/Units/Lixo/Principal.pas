unit Principal;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  UnMLAGeral, UMySQLModule, ExtCtrls, Menus, Grids, DBGrids, Db, (*DBTables,*)
  TypInfo, StdCtrls, ZCF2, ToolWin, ComCtrls, UnInternalConsts, ExtDlgs, jpeg,
  mySQLDbTables, Buttons, WinSkinStore, WinSkinData, Mask, DBCtrls, Tabs,
  DockTabSet, ButtonGroup, ActnPopup, AdvGlowButton, AdvShapeButton, AdvToolBar,
  AdvToolBarStylers, dmkValUsu, dmkEdit, dmkEditCB, AdvPreviewMenu, DmkDAC_PF,
  AdvPreviewMenuStylers, AdvMenus, dmkGeral, DCPcrypt2, DCPblockciphers,
  DCPtwofish, dmkCheckGroup, frxClass, UrlMon, Vcl.Imaging.pngimage, UnDmkEnums,
  dmkPageControl, UnXXe_PF, UnitNotificacoes, UnitNotificacoesEdit,
  UnEfdIcmsIpi_Jan, UnEfdIcmsIpi_PF, UnTX_Jan;

type
  (*
  TTipoMaterialNF = (tnfMateriaPrima, tnfUsoEConsumo);
  TcpCalc = (cpJurosMes, cpMulta);
  *)

  TFmPrincipal = class(TForm)
    Timer1: TTimer;
    PMProdImp: TPopupMenu;
    Estoque1: TMenuItem;
    Histrico1: TMenuItem;
    Vendas1: TMenuItem;
    StatusBar: TStatusBar;
    PMGeral: TPopupMenu;
    Entidades2: TMenuItem;
    N1: TMenuItem;
    Reabrirtabelas1: TMenuItem;
    PMPlaCtas: TPopupMenu;
    Plano3: TMenuItem;
    Conjutos2: TMenuItem;
    Grupos3: TMenuItem;
    Subgrupos3: TMenuItem;
    Contas3: TMenuItem;
    Todosnveis2: TMenuItem;
    N12: TMenuItem;
    Listaplanos2: TMenuItem;
    N11: TMenuItem;
    Saldoinicialdeconta1: TMenuItem;
    Listagem2: TMenuItem;
    Simples2: TMenuItem;
    Transfernciaentrecontas1: TMenuItem;
    QrCarteiras_: TmySQLQuery;
    QrCarteiras_Codigo: TIntegerField;
    QrCarteiras_Tipo: TIntegerField;
    QrCarteiras_Nome: TWideStringField;
    QrCarteiras_Inicial: TFloatField;
    QrCarteiras_Banco: TIntegerField;
    QrCarteiras_ID: TWideStringField;
    QrCarteiras_Fatura: TWideStringField;
    QrCarteiras_ID_Fat: TWideStringField;
    QrCarteiras_Saldo: TFloatField;
    QrCarteiras_Lk: TIntegerField;
    QrCarteiras_EmCaixa: TFloatField;
    QrCarteiras_DIFERENCA: TFloatField;
    QrCarteiras_Fechamento: TIntegerField;
    QrCarteiras_Prazo: TSmallintField;
    QrCarteiras_TIPOPRAZO: TWideStringField;
    QrCarteiras_DataCad: TDateField;
    QrCarteiras_DataAlt: TDateField;
    QrCarteiras_UserCad: TSmallintField;
    QrCarteiras_UserAlt: TSmallintField;
    QrCarteiras_NOMEPAGREC: TWideStringField;
    QrCarteiras_PagRec: TSmallintField;
    QrCarteiras_FuturoC: TFloatField;
    QrCarteiras_FuturoD: TFloatField;
    QrCarteiras_FuturoS: TFloatField;
    DsCarteiras: TDataSource;
    QrCartSum_: TmySQLQuery;
    QrCartSum_SALDO: TFloatField;
    QrCartSum_FuturoC: TFloatField;
    QrCartSum_FuturoD: TFloatField;
    QrCartSum_FuturoS: TFloatField;
    QrCartSum_EmCaixa: TFloatField;
    QrCartSum_Difere: TFloatField;
    QrCartSum_SDO_FUT: TFloatField;
    DsCartSum: TDataSource;
    TimerIdle: TTimer;
    OpenDialog1: TOpenDialog;
    dmkEditCB1: TdmkEditCB;
    AdvPreviewMenuOfficeStyler1: TAdvPreviewMenuOfficeStyler;
    AdvPMImagem: TAdvPopupMenu;
    Escolher1: TMenuItem;
    AdvPreviewMenu2: TAdvPreviewMenu;
    AdvPMMenuCor: TAdvPopupMenu;
    Office20071: TMenuItem;
    Padro3: TMenuItem;
    Dermatek1: TMenuItem;
    Preto1: TMenuItem;
    Azul1: TMenuItem;
    Cinza1: TMenuItem;
    Verde1: TMenuItem;
    Prscia1: TMenuItem;
    AdvToolBarOfficeStyler1: TAdvToolBarOfficeStyler;
    SkinStore1: TSkinStore;
    sd1: TSkinData;
    WhidbeyStyle1: TMenuItem;
    WindowsXP1: TMenuItem;
    QrXXX1: TmySQLQuery;
    QrXXX1Nivel1: TIntegerField;
    QrXXX1Nivel3: TIntegerField;
    QrXXX1PrdGrupTip: TIntegerField;
    Memo3: TMemo;
    PMZStepCod: TPopupMenu;
    Gerenciaintervalosdecdigod1: TMenuItem;
    QrPrc: TmySQLQuery;
    QrPrcCodigo: TIntegerField;
    QrPrcControle: TIntegerField;
    QrPrcGraGruX: TIntegerField;
    QrPrcPrecoO: TFloatField;
    QrPrcPrecoR: TFloatField;
    QrPrcQuantP: TFloatField;
    QrPrcQuantC: TFloatField;
    QrPrcQuantV: TFloatField;
    QrPrcValBru: TFloatField;
    QrPrcDescoP: TFloatField;
    QrPrcDescoV: TFloatField;
    QrPrcValLiq: TFloatField;
    QrPrcLk: TIntegerField;
    QrPrcDataCad: TDateField;
    QrPrcDataAlt: TDateField;
    QrPrcUserCad: TIntegerField;
    QrPrcUserAlt: TIntegerField;
    QrPrcAlterWeb: TSmallintField;
    QrPrcAtivo: TSmallintField;
    QrPrcPrecoF: TFloatField;
    PB1: TProgressBar;
    Panel1: TPanel;
    dmkEdit1: TdmkEdit;
    Label2: TLabel;
    dmkEdit2: TdmkEdit;
    Label1: TLabel;
    QrAjusVol: TmySQLQuery;
    QrAjusVolIDCtrl: TIntegerField;
    QrPediVdaIts: TmySQLQuery;
    QrPediVdaItsControle: TIntegerField;
    QrStqManIts: TmySQLQuery;
    QrStqManItsDataHora: TDateTimeField;
    QrStqManItsTipo: TSmallintField;
    QrStqManItsOriCodi: TIntegerField;
    QrStqManItsOriCtrl: TIntegerField;
    QrStqManItsEmpresa: TIntegerField;
    QrStqManItsStqCenCad: TIntegerField;
    QrStqManItsGraGruX: TIntegerField;
    QrStqManItsQtde: TFloatField;
    QrStqManItsAlterWeb: TSmallintField;
    QrStqManItsAtivo: TSmallintField;
    QrStqManItsIDCtrl: TIntegerField;
    QrStqManItsOriCnta: TIntegerField;
    QrStqManItsCustoPreco: TFloatField;
    QrPart01: TmySQLQuery;
    QrPart99: TmySQLQuery;
    QrCarteiras_NOMEDOBANCO: TWideStringField;
    QrCarteiras_ForneceI: TIntegerField;
    Limpar1: TMenuItem;
    TmSuporte: TTimer;
    TySuporte: TTrayIcon;
    AdvPMVerificaBD: TAdvPopupMenu;
    VerificaBDServidor1: TMenuItem;
    VerificaTabelasterceiros1: TMenuItem;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    ProgressBar1: TProgressBar;
    AdvToolBarPager1: TAdvToolBarPager;
    AdvToolBarPager21: TAdvPage;
    AdvToolBar5: TAdvToolBar;
    AdvGlowButton10: TAdvGlowButton;
    AdvGlowButton11: TAdvGlowButton;
    AdvToolBar4: TAdvToolBar;
    AdvGlowButton8: TAdvGlowButton;
    AdvGlowButton9: TAdvGlowButton;
    AdvGlowButton12: TAdvGlowButton;
    AdvToolBar11: TAdvToolBar;
    AdvGlowButton5: TAdvGlowButton;
    AdvGlowButton32: TAdvGlowButton;
    AdvGlowButton93: TAdvGlowButton;
    AdvToolBar12: TAdvToolBar;
    AdvGlowButton26: TAdvGlowButton;
    AdvGlowButton27: TAdvGlowButton;
    AdvGlowButton28: TAdvGlowButton;
    AdvToolBar17: TAdvToolBar;
    AdvGlowButton7: TAdvGlowButton;
    AdvGlowButton49: TAdvGlowButton;
    AdvGlowButton103: TAdvGlowButton;
    AdvToolBarPager22: TAdvPage;
    AdvToolBar13: TAdvToolBar;
    AdvGlowButton29: TAdvGlowButton;
    AdvGlowButton30: TAdvGlowButton;
    AGBPediVdaImp: TAdvGlowButton;
    AdvGlowButton55: TAdvGlowButton;
    AdvToolBar14: TAdvToolBar;
    AdvGlowButton34: TAdvGlowButton;
    AdvGlowButton35: TAdvGlowButton;
    AdvGlowButton36: TAdvGlowButton;
    AdvGlowButton37: TAdvGlowButton;
    AdvToolBarPager23: TAdvPage;
    AdvToolBar19: TAdvToolBar;
    AdvGlowButton1: TAdvGlowButton;
    AdvGlowButton53: TAdvGlowButton;
    AdvGlowButton54: TAdvGlowButton;
    AdvGlowButton56: TAdvGlowButton;
    AdvGlowButton97: TAdvGlowButton;
    AdvGlowButton104: TAdvGlowButton;
    AdvToolBar31: TAdvToolBar;
    AdvGlowButton76: TAdvGlowButton;
    AdvGlowButton77: TAdvGlowButton;
    AdvPage9: TAdvPage;
    AdvToolBar16: TAdvToolBar;
    AdvGlowButton38: TAdvGlowButton;
    AdvGlowButton13: TAdvGlowButton;
    AdvGlowButton102: TAdvGlowButton;
    AdvToolBar1: TAdvToolBar;
    AdvGlowButton43: TAdvGlowButton;
    AdvGlowButton44: TAdvGlowButton;
    AdvGlowButton2: TAdvGlowButton;
    AdvPage11: TAdvPage;
    AdvToolBar18: TAdvToolBar;
    AdvGlowButton42: TAdvGlowButton;
    AdvGlowButton45: TAdvGlowButton;
    AdvGlowButton92: TAdvGlowButton;
    AdvPage12: TAdvPage;
    AdvToolBar21: TAdvToolBar;
    AdvGlowButton60: TAdvGlowButton;
    AdvToolBar7: TAdvToolBar;
    AdvGlowButton20: TAdvGlowButton;
    AdvGlowButton21: TAdvGlowButton;
    AdvGlowButton22: TAdvGlowButton;
    AdvGlowButton23: TAdvGlowButton;
    AdvGlowButton24: TAdvGlowButton;
    AdvGlowButton25: TAdvGlowButton;
    AdvToolBar2: TAdvToolBar;
    AdvGlowButton18: TAdvGlowButton;
    AdvGlowButton31: TAdvGlowButton;
    AdvGlowButton33: TAdvGlowButton;
    AdvToolBar34: TAdvToolBar;
    AdvGlowButton98: TAdvGlowButton;
    AdvGlowButton99: TAdvGlowButton;
    AdvPage13: TAdvPage;
    AdvToolBar32: TAdvToolBar;
    AdvGlowButton84: TAdvGlowButton;
    AdvGlowButton88: TAdvGlowButton;
    AdvGlowButton90: TAdvGlowButton;
    AdvGlowButton95: TAdvGlowButton;
    AdvGlowButton96: TAdvGlowButton;
    AdvGlowButton106: TAdvGlowButton;
    AdvGlowButton107: TAdvGlowButton;
    AdvPage15: TAdvPage;
    AdvPage17: TAdvPage;
    AdvShapeButton1: TAdvShapeButton;
    AdvQuickAccessToolBar2: TAdvQuickAccessToolBar;
    AdvToolBarButton7: TAdvToolBarButton;
    AdvToolBarButton8: TAdvToolBarButton;
    AdvToolBarButton10: TAdvToolBarButton;
    AdvToolBarButton11: TAdvToolBarButton;
    AdvToolBarButton13: TAdvToolBarButton;
    AdvGlowButton16: TAdvGlowButton;
    AdvGlowButton14: TAdvGlowButton;
    AdvGlowButton19: TAdvGlowButton;
    PageControl1: TdmkPageControl;
    AGBNewFinMigra: TAdvGlowButton;
    AdvToolBarButton12: TAdvToolBarButton;
    BalloonHint1: TBalloonHint;
    AdvToolBar29: TAdvToolBar;
    AdvGlowButton74: TAdvGlowButton;
    AdvGlowButton91: TAdvGlowButton;
    AdvPage1: TAdvPage;
    AdvPage2: TAdvPage;
    AdvToolBar8: TAdvToolBar;
    AdvGlowButton3: TAdvGlowButton;
    AdvToolBar33: TAdvToolBar;
    AdvGlowButton94: TAdvGlowButton;
    AdvToolBar30: TAdvToolBar;
    AdvGlowButton39: TAdvGlowButton;
    AdvGlowButton46: TAdvGlowButton;
    AdvGlowButton62: TAdvGlowButton;
    AdvGlowButton75: TAdvGlowButton;
    AdvGlowButton79: TAdvGlowButton;
    AdvGlowButton81: TAdvGlowButton;
    AdvGlowButton82: TAdvGlowButton;
    AdvGlowButton86: TAdvGlowButton;
    AdvGlowButton108: TAdvGlowButton;
    AdvToolBar15: TAdvToolBar;
    BtDTB: TAdvGlowButton;
    AdvGlowButton83: TAdvGlowButton;
    AdvToolBar9: TAdvToolBar;
    AdvGlowButton4: TAdvGlowButton;
    AdvGlowButton6: TAdvGlowButton;
    AdvToolBar24: TAdvToolBar;
    AdvGlowButton68: TAdvGlowButton;
    AdvGlowButton69: TAdvGlowButton;
    AdvToolBar25: TAdvToolBar;
    AdvGlowButton70: TAdvGlowButton;
    AdvToolBar26: TAdvToolBar;
    AdvGlowButton71: TAdvGlowButton;
    AdvToolBar27: TAdvToolBar;
    AdvGlowButton72: TAdvGlowButton;
    AdvToolBar20: TAdvToolBar;
    AdvGlowButton50: TAdvGlowButton;
    AdvGlowButton51: TAdvGlowButton;
    AdvGlowButton52: TAdvGlowButton;
    AdvGlowButton59: TAdvGlowButton;
    AdvGlowButton78: TAdvGlowButton;
    AdvToolBar6: TAdvToolBar;
    AdvGlowButton89: TAdvGlowButton;
    AdvGlowButton40: TAdvGlowButton;
    AGBConsultaNFe: TAdvGlowButton;
    AdvToolBar22: TAdvToolBar;
    AdvGlowButton100: TAdvGlowButton;
    AdvGlowButton101: TAdvGlowButton;
    GBValidaXML: TAdvGlowButton;
    AGBNFeDest: TAdvGlowButton;
    AGBNFeLoad_Inn: TAdvGlowButton;
    AGBNFeDesDowC: TAdvGlowButton;
    AdvToolBar10: TAdvToolBar;
    AdvGlowButton17: TAdvGlowButton;
    AdvGlowButton41: TAdvGlowButton;
    AdvGlowButton58: TAdvGlowButton;
    AdvGlowButton80: TAdvGlowButton;
    AdvToolBar3: TAdvToolBar;
    AdvGlowMenuButton3: TAdvGlowMenuButton;
    AdvGlowMenuButton4: TAdvGlowMenuButton;
    AdvGlowButton105: TAdvGlowButton;
    AdvGlowButton48: TAdvGlowButton;
    AGBNFeExportaXML_B: TAdvGlowButton;
    TmVersao: TTimer;
    AdvGlowButton57: TAdvGlowButton;
    Button1: TButton;
    AdvGlowButton61: TAdvGlowButton;
    AdvGlowButton15: TAdvGlowButton;
    AdvGlowButton167: TAdvGlowButton;
    AdvToolBar23: TAdvToolBar;
    AdvGlowButton63: TAdvGlowButton;
    AdvGlowButton64: TAdvGlowButton;
    AdvGlowButton65: TAdvGlowButton;
    AGBLaySPEDEFD: TAdvGlowButton;
    AdvPage3: TAdvPage;
    AtbProducao_Cadastros: TAdvToolBar;
    AGBGraGruY: TAdvGlowButton;
    AGBTXCadNat: TAdvGlowButton;
    AGBTXCadInd: TAdvGlowButton;
    AGBTXCadFcc: TAdvGlowButton;
    AtbProducao_Movimentacao: TAdvToolBar;
    AGBTXInnCab: TAdvGlowButton;
    AGBTXIndCab: TAdvGlowButton;
    AGBOperacoes: TAdvGlowButton;
    AdvPMFluxos: TAdvPopupMenu;
    Fluxos3: TMenuItem;
    Operaes1: TMenuItem;
    AGBFluxoCab: TAdvGlowButton;
    AGBSetorCad: TAdvGlowButton;
    AGBTXPallet: TAdvGlowButton;
    AGBTXPalSta: TAdvGlowButton;
    AGBTXCadInt: TAdvGlowButton;
    AGBTXOutIts: TAdvGlowButton;
    AGBTXExBCab: TAdvGlowButton;
    AtbProducao_Miscelanea: TAdvToolBar;
    AGBVSMovImp: TAdvGlowMenuButton;
    ApmProducao_Relatorios: TAdvPopupMenu;
    EstoquAtual1: TMenuItem;
    N02EstoqueEm1: TMenuItem;
    AGBTXSerTal: TAdvGlowButton;
    AGBTXAjsCab: TAdvGlowButton;
    AGBTXTrfLocCab: TAdvGlowButton;
    AGBVSMovIts: TAdvGlowButton;
    AGBVSPlCCab: TAdvGlowButton;
    AGBVSRRMCab: TAdvGlowButton;
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SBCadEntidadesClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure Entidades2Click(Sender: TObject);
    procedure Reabrirtabelas1Click(Sender: TObject);
    procedure Plano3Click(Sender: TObject);
    procedure Conjutos2Click(Sender: TObject);
    procedure Grupos3Click(Sender: TObject);
    procedure Subgrupos3Click(Sender: TObject);
    procedure Contas3Click(Sender: TObject);
    procedure Todosnveis2Click(Sender: TObject);
    procedure Listaplanos2Click(Sender: TObject);
    procedure Verificanovaverso1Click(Sender: TObject);
    procedure TimerIdleTimer(Sender: TObject);
    procedure Planodecontas2Click(Sender: TObject);
    procedure AdvGlowButton11Click(Sender: TObject);
    procedure AdvGlowButton10Click(Sender: TObject);
    procedure AdvGlowButton9Click(Sender: TObject);
    procedure AdvGlowButton12Click(Sender: TObject);
    procedure AdvGlowButton14Click(Sender: TObject);
    procedure AdvGlowButton3Click(Sender: TObject);
    procedure AdvGlowButton17Click(Sender: TObject);
    procedure AdvGlowButton19Click(Sender: TObject);
    procedure AdvGlowButton25Click(Sender: TObject);
    procedure AdvGlowButton18Click(Sender: TObject);
    procedure AdvGlowButton20Click(Sender: TObject);
    procedure AdvGlowButton21Click(Sender: TObject);
    procedure AdvGlowButton22Click(Sender: TObject);
    procedure AdvGlowButton4Click(Sender: TObject);
    procedure Escolher1Click(Sender: TObject);
    procedure AdvGlowMenuButton4Click(Sender: TObject);
    procedure AdvPreviewMenu2Buttons0Click(Sender: TObject;
      Button: TButtonCollectionItem);
    procedure AdvGlowButton24Click(Sender: TObject);
    procedure AdvGlowButton23Click(Sender: TObject);
    procedure AdvGlowButton7Click(Sender: TObject);
    procedure AdvGlowButton26Click(Sender: TObject);
    procedure AdvGlowButton27Click(Sender: TObject);
    procedure AdvGlowButton28Click(Sender: TObject);
    procedure AdvGlowButton29Click(Sender: TObject);
    procedure AdvGlowButton30Click(Sender: TObject);
    procedure AdvGlowButton31Click(Sender: TObject);
    procedure AdvGlowButton32Click(Sender: TObject);
    procedure AdvGlowButton33Click(Sender: TObject);
    procedure AdvGlowButton34Click(Sender: TObject);
    procedure AdvGlowButton35Click(Sender: TObject);
    procedure BtDTBClick(Sender: TObject);
    procedure AdvGlowButton36Click(Sender: TObject);
    procedure AdvGlowButton37Click(Sender: TObject);
    procedure AdvGlowButton38Click(Sender: TObject);
    procedure AdvPreviewMenu1Buttons1Click(Sender: TObject;
      Button: TButtonCollectionItem);
    procedure AdvGlowButton8Click(Sender: TObject);
    procedure AdvPreviewMenu1Buttons2Click(Sender: TObject;
      Button: TButtonCollectionItem);
    procedure AdvPreviewMenu1Buttons0Click(Sender: TObject;
      Button: TButtonCollectionItem);
    procedure AdvPreviewMenu1Buttons3Click(Sender: TObject;
      Button: TButtonCollectionItem);
    procedure AdvGlowMenuButton3Click(Sender: TObject);
    procedure Padro3Click(Sender: TObject);
    procedure AdvGlowButton39Click(Sender: TObject);
    procedure AdvGlowButton41Click(Sender: TObject);
    procedure AdvGlowButton43Click(Sender: TObject);
    procedure AdvGlowButton44Click(Sender: TObject);
    procedure AdvGlowButton42Click(Sender: TObject);
    procedure AdvGlowButton45Click(Sender: TObject);
    procedure AdvGlowButton46Click(Sender: TObject);
    procedure AdvGlowButton13Click(Sender: TObject);
    procedure AdvGlowButton49Click(Sender: TObject);
    procedure AdvGlowButton50Click(Sender: TObject);
    procedure AdvGlowButton51Click(Sender: TObject);
    procedure AdvGlowButton52Clic(Sender: TObject);
    procedure AdvGlowButton2Click(Sender: TObject);
    procedure AdvGlowButton1Click(Sender: TObject);
    procedure AdvGlowButton58Click(Sender: TObject);
    procedure AdvGlowButton59Click(Sender: TObject);
    procedure AdvGlowButton60Click(Sender: TObject);
    procedure AdvGlowButton62Click(Sender: TObject);
    procedure AdvGlowButton56Click(Sender: TObject);
    procedure AdvGlowButton72Click(Sender: TObject);
    procedure AdvGlowButton74Click(Sender: TObject);
    procedure AdvGlowButton75Click(Sender: TObject);
    procedure AdvGlowButton78Click(Sender: TObject);
    procedure AdvGlowButton76Click(Sender: TObject);
    procedure AdvGlowButton79Click(Sender: TObject);
    procedure AdvGlowButton80Click(Sender: TObject);
    procedure AdvGlowButton81Click(Sender: TObject);
    procedure AdvGlowButton82Click(Sender: TObject);
    procedure AdvGlowButton83Click(Sender: TObject);
    procedure AdvGlowButton84Click(Sender: TObject);
    procedure GBValidaXMLClick(Sender: TObject);
    procedure AdvGlowButton88Click(Sender: TObject);
    procedure AdvGlowButton89Click(Sender: TObject);
    procedure AdvGlowButton90Click(Sender: TObject);
    procedure AdvGlowButton91Click(Sender: TObject);
    procedure AdvGlowButton92Click(Sender: TObject);
    procedure AdvGlowButton95Click(Sender: TObject);
    procedure AdvGlowButton96Click(Sender: TObject);
    procedure AdvGlowButton55Click(Sender: TObject);
    procedure AdvGlowButton97Click(Sender: TObject);
    procedure AdvGlowButton98Click(Sender: TObject);
    procedure AdvGlowButton99Click(Sender: TObject);
    procedure AdvGlowButton100Click(Sender: TObject);
    procedure AdvGlowButton101Click(Sender: TObject);
    procedure AdvGlowButton102Click(Sender: TObject);
    procedure AdvGlowButton103Click(Sender: TObject);
    procedure AdvGlowButton104Click(Sender: TObject);
    procedure AdvGlowButton86Click(Sender: TObject);
    procedure Limpar1Click(Sender: TObject);
    procedure AdvGlowButton105Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure AdvGlowButton93Click(Sender: TObject);
    procedure AdvGlowButton106Click(Sender: TObject);
    procedure AdvGlowButton107Click(Sender: TObject);
    procedure TmSuporteTimer(Sender: TObject);
    procedure VerificaTabelasterceiros1Click(Sender: TObject);
    procedure VerificaBDServidor1Click(Sender: TObject);
    procedure AdvGlowButton108Click(Sender: TObject);
    procedure AdvToolBarButton7Click(Sender: TObject);
    procedure AdvToolBarButton8Click(Sender: TObject);
    procedure AdvToolBarButton10Click(Sender: TObject);
    procedure AdvToolBarButton13Click(Sender: TObject);
    procedure AdvGlowButton5Click(Sender: TObject);
    procedure AdvGlowButton16Click(Sender: TObject);
    procedure AGBNewFinMigraClick(Sender: TObject);
    procedure AdvToolBarButton12Click(Sender: TObject);
    procedure AdvGlowButton15Click(Sender: TObject);
    procedure AdvGlowButton40Click(Sender: TObject);
    procedure AGBConsultaNFeClick(Sender: TObject);
    procedure AdvGlowButton48Click(Sender: TObject);
    procedure AGBNFeDestClick(Sender: TObject);
    procedure AGBNFeLoad_InnClick(Sender: TObject);
    procedure AGBNFeDesDowCClick(Sender: TObject);
    procedure AGBNFeExportaXML_BClick(Sender: TObject);
    procedure TmVersaoTimer(Sender: TObject);
    procedure AdvGlowButton57Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure sd1FormSkin(Sender: TObject; aName: string; var DoSkin: Boolean);
    procedure AdvGlowButton61Click(Sender: TObject);
    procedure AdvGlowButton167Click(Sender: TObject);
    procedure AdvGlowButton65Click(Sender: TObject);
    procedure AdvGlowButton63Click(Sender: TObject);
    procedure AdvGlowButton64Click(Sender: TObject);
    procedure AGBLaySPEDEFDClick(Sender: TObject);
    procedure AGBGraGruYClick(Sender: TObject);
    procedure AGBTXCadNatClick(Sender: TObject);
    procedure AGBPediVdaImpClick(Sender: TObject);
    procedure AGBTXCadIndClick(Sender: TObject);
    procedure AGBTXCadFccClick(Sender: TObject);
    procedure AGBTXInnCabClick(Sender: TObject);
    procedure AGBTXIndCabClick(Sender: TObject);
    procedure AGBOperacoesClick(Sender: TObject);
    procedure AGBFluxoCabClick(Sender: TObject);
    procedure AGBSetorCadClick(Sender: TObject);
    procedure AGBTXPalletClick(Sender: TObject);
    procedure AGBTXPalStaClick(Sender: TObject);
    procedure AGBTXCadIntClick(Sender: TObject);
    procedure AGBTXOutItsClick(Sender: TObject);
    procedure AGBTXExBCabClick(Sender: TObject);
    procedure EstoquAtual1Click(Sender: TObject);
    procedure N02EstoqueEm1Click(Sender: TObject);
    procedure AGBTXSerTalClick(Sender: TObject);
    procedure AGBTXTrfLocCabClick(Sender: TObject);
    procedure AGBTXAjsCabClick(Sender: TObject);
    procedure AGBVSMovItsClick(Sender: TObject);
    procedure AGBVSPlCCabClick(Sender: TObject);
    procedure AGBVSRRMCabClick(Sender: TObject);
  private
    { Private declarations }
    FALiberar: Boolean;
    procedure AtzPed();
    //
    procedure SkinMenu(Index: Integer);
    procedure MostraFormDescanso;
  public
    { Public declarations }
    FEntInt: Integer;
    FTipoEntradTitu: String;
    FTipoEntradaDig, FTipoEntradaNFe, FTipoEntradaEFD, FDuplicata, FCheque,
      FVerArqSCX, FTipoNovoEnti: Integer;
    FLDataIni, FLDataFim: TDateTime;
    FImportPath: String;

    procedure ShowHint(Sender: TObject);
    procedure RetornoCNAB;
    function PreparaMinutosSQL: Boolean;
    function AdicionaMinutosSQL(HI, HF: TTime): Boolean;

    procedure CadastroEntidades();
    procedure CriaPages();
    procedure CriaMinhasEtiquetas();

    procedure SelecionaImagemdefundo;
    procedure AcoesExtrasDeCadastroDeEntidades(Grade1: TStringGrid;
      Entidade: Integer; Aba: Boolean = False);
    procedure AcoesIniciaisDeCadastroDeEntidades(Reference: TComponent;
      Codigo: Integer; Grade1: TStringGrid);
    function RecriaTabelaLocal(Tabela: String; Acao: Integer): Boolean;
    procedure SalvaArquivo(EdNomCli, EdCPFCli: TEdit; Grade: TStringGrid;
      Data: TDateTime; FileName: String; ChangeData: Boolean);
    function AbreArquivoINI(Grade: TStringGrid; EdNomCli, EdCPFCli: TEdit;
      Data: TDateTime; Arquivo: String): Boolean;
    procedure CadastroDeEntidades(Entidade: Integer);
    procedure MostraCarteiras(Carteira: Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    // Cashier
    function CartaoDeFatura: Integer;
    function CompensacaoDeFatura: String;
    procedure CriaCalcPercent(Valor, Porcentagem: String; Calc: TcpCalc);
    function VerificaNovasVersoes(ApenasVerifica: Boolean = False): Boolean;
    procedure CadastroDeContasNiv();
    procedure AcoesIniciaisDoAplicativo();

    procedure MostraBackup3;
    procedure MostraCambioCot;
    procedure MostraCambioMda;
    procedure MostraEntiTipCto();
    procedure MostraEntiCargos();
    procedure MostraEntiContat();
    procedure MostraGeosite;
    procedure MostraGraGruN;
    procedure MostraMotivos;
    procedure MostraOpcoes;
    procedure MostraParamsEmp();
    procedure MostraPediAcc;
    procedure MostraPediVda;
    procedure MostraRegioes;
    procedure MostraTabePrcCab;
    procedure MostraVerifiDB;
    //
    //Notifica��es
    function  NotificacoesVerifica(QueryNotifi: TmySQLQuery; DataHora: TDateTime): Boolean;
    procedure NotificacoesJanelas(TipoNotifi: TNotifi; DataHora: TDateTime);
    //
    procedure ReCaptionComponentesDeForm(Form: TForm (* ; Memo: TMemo *) );
    function CriaFormEntradaCab(MaterialNF: TTipoMaterialNF; ShowForm: Boolean;
      IDCtrl: Integer): Boolean;
    function  FixBugAtualizacoesApp(FixBug: Integer; FixBugStr: String): Boolean;
  end;

var
  FmPrincipal: TFmPrincipal;

implementation

uses UnMyObjects, Module, Entidades, VerifiDB, MyGlyfs, MyDBCheck, Planning_Dmk,
  Opcoes, UnLock_MLA, MyListas, MyVCLSkin, ChConfigCab, UCreate, Backup3,
  CalcPercent, NovaVersao, Bancos, ContasNiv, GraGruN, PediVda, CambioMda,
  ModuleGeral, CambioCot, Geosite, Regioes, IBGE_DTB, PediAcc, TabePrcCab,
  Motivos, ParamsEmp, Entidade2, TabePrcImp, EntiTipCto, EntiCargos, EntiContat,
  EtqPrinCad, EtqGeraLot, ModPediVda, ModProd, PediVdaImp, Matriz, UnGrl_Vars,
  ModuleFin, StqManCad, PlanCutImp, MatPartCad, SugVPedCab, ModuleNFe_0000,
  NFeLayout_0200, MedOrdem, Maladireta, MultiEtiq, Pages, FatConCad, FatConRet,
  PreEmail, UnGFat_Jan, EntradaCab, LinkRankSkin, UnDmkWeb, VerifiDBTerceiros,
  FatPedImp2, UnFinanceiroJan, ModuleLct2, NFe_PF, UnGrade_Jan, About,
  UnEntities, UnUMedi_PF, UnPraz_PF, Descanso, CorrigeCidadeEntidade, UnFixBugs,
  UnitOrdProd, SPED_EFD_Importa;

const
  FAltLin = CO_POLEGADA / 2.16;

{$R *.DFM}

function TFmPrincipal.FixBugAtualizacoesApp(FixBug: Integer;
  FixBugStr: String): Boolean;
begin
  Result := True;
  (* Marcar quando for criar uma fun��o de atualiza��o
  try
    if FixBug = 0 then
      Atualiza��o1()
    else if FixBug = 1 then
      Atualiza��o2()
    else if FixBug = 2 then
      Atualiza��o3()
    else
      Result := False;
  except
    Result := False;
  end;
  *)
end;

procedure TFmPrincipal.FormActivate(Sender: TObject);
begin
  APP_LIBERADO := True;
  MyObjects.CorIniComponente();
  VAR_ATUALIZANDO := False;
  VAR_APPNAME := Application.Title;
  if Geral.VersaoTxt2006(CO_VERSAO) <> Geral.FileVerInfo(Application.ExeName,
    3 (* Versao *) ) then
    ShowMessage('Vers�o difere do arquivo');
  if not FALiberar then
    Timer1.Enabled := True;
end;

procedure TFmPrincipal.FormCreate(Sender: TObject);
var
  MenuStyle: Integer;
  Imagem (* , Avisa *) : String;
begin
  VAR_MULTIPLAS_TAB_LCT := True;
  VAR_TYPE_LOG := ttlFiliLog;
  VAR_ADMIN := 'DS143SCH77';
  FEntInt := -11;
  AdvToolBarPager1.ActivePageIndex := 0;
  //
  VAR_USA_TAG_BITBTN := True;
  FTipoNovoEnti := 0;
  VAR_STLOGIN := StatusBar.Panels[01];
  StatusBar.Panels[3].Text := Geral.VersaoTxt2006(CO_VERSAO);
  // VAR_STTERMINAL    := StatusBar.Panels[05];
  VAR_STEMPRESA := StatusBar.Panels[05];
  VAR_STDATALICENCA := StatusBar.Panels[07];
  // VAR_STAVISOS      := StatusBar.Panels[09];
  VAR_SKINUSANDO := StatusBar.Panels[09];
  VAR_STDATABASES := StatusBar.Panels[11];
  VAR_TIPOSPRODM_TXT := '0,1,2,3,4,5,6,7,8,9,10,11,12,13';
  VAR_APP := ExtractFilePath(Application.ExeName);
  VAR_VENDEOQUE := 1;

  Application.OnMessage := MyObjects.FormMsg;

  //
  MenuStyle := Geral.ReadAppKey('MenuStyle', Application.Title, ktInteger, 0,
    HKEY_LOCAL_MACHINE);
  SkinMenu(MenuStyle);
  //
  VAR_CAD_POPUP := PMGeral;
  // MLAGeral.CopiaItensDeMenu(PMGeral, Cadastros1, FmPrincipal);
  /// ///////////////////////////////////////////////////////////////////////////
  FLDataIni := Date - Geral.ReadAppKey('Dias', Application.Title, ktInteger, 60,
    HKEY_LOCAL_MACHINE);
  FLDataFim := Date;
  /// ///////////////////////////////////////////////////////////////////////////
  //
  Application.OnHint := ShowHint;
  Application.OnException := MyObjects.MostraErro;
  Application.OnMessage   := MyObjects.FormMsg;
  //2017-07-19 => N�o ativar por causa da c�mera Application.OnIdle := AppIdle;
  TimerIdle.Interval := VAR_TIMERIDLEITERVAL;
  TimerIdle.Enabled  := True;
  //
  VAR_CAMINHOSKINPADRAO := 'C:\Dermatek\Skins\VCLSkin\Office 2007.skn';
  //
  TmSuporte.Enabled := False;
  //
  MyObjects.MaximizaAdvToolBarPager(AdvToolBarPager1, deftfTrue);
end;

procedure TFmPrincipal.FormDestroy(Sender: TObject);
begin
  if VAR_WEB_CONECTADO = 100 then
    DmkWeb.DesconectarUsuarioWEB;
end;

procedure TFmPrincipal.SelecionaImagemdefundo;
begin
  MyObjects.SelecionaLimpaImagemdefundoFormDescanso(FmPrincipal, TFmDescanso, PageControl1);
end;

procedure TFmPrincipal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  (* DmDados.TbMaster.Open;
    DmDados.TbMaster.Edit;
    if Trunc(Date) > DmDados.TbMasterLA.Value then
    DmDados.TbMasterLD.Value := DmDados.TbMasterLD.Value +
    (Trunc(Date) - Trunc(DmDados.TbMasterLA.Value));
    if Trunc(Date) < DmDados.TbMasterLA.Value then
    DmDados.TbMasterLD.Value := DmDados.TbMasterLD.Value + 3;
    if (Trunc(Date) = DmDados.TbMasterLA.Value) and
    (Time < DmDados.TbMasterLH.Value)  then
    DmDados.TbMasterLD.Value := DmDados.TbMasterLD.Value + 1;
    DmDados.TbMasterLA.Value := Trunc(Date);
    DmDados.TbMasterLH.Value := Time;
    DmDados.TbMaster.Post;
    DmDados.TbMaster.Close;
    FmSkin.Close;
    FmSkin.Destroy; *)
  Application.Terminate;
end;

function TFmPrincipal.PreparaMinutosSQL: Boolean;
// var
// i: Integer;
begin
  Result := True;
  try
    Dmod.QrUpdL.SQL.Clear;
    Dmod.QrUpdL.SQL.Add('DELETE FROM ocupacao');
    Dmod.QrUpdL.ExecSQL;
    ///
    (* Dmod.QrUpdL.SQL.Clear;
      Dmod.QrUpdL.SQL.Add('INSERT INTO ocupacao SET Min=:P0');
      for i := 1 to 1440 do
      begin
      Dmod.QrUpdL.Params[0].AsInteger := I;
      Dmod.QrUpdL.ExecSQL;
      end; *)
  except
    Result := False
  end;
end;

function TFmPrincipal.AdicionaMinutosSQL(HI, HF: TTime): Boolean;
var
  Hour, Min, Sec, MSec: Word;
  i, Ini, Fim: Integer;
begin
  Result := True;
  if (HI = 0) and (HF = 0) then
    Exit;
  DecodeTime(HI, Hour, Min, Sec, MSec);
  Ini := (Hour * 60) + Min;
  DecodeTime(HF, Hour, Min, Sec, MSec);
  Fim := (Hour * 60) + Min - 1;
  if Fim < Ini then
    Fim := Fim + 1440;
  Dmod.QrUpdL.SQL.Clear;
  Dmod.QrUpdL.SQL.Add('INSERT INTO ocupacao SET Qtd=1, Min=:P0');
  for i := Ini to Fim do
  begin
    Dmod.QrUpdL.Params[0].AsInteger := i;
    Dmod.QrUpdL.ExecSQL;
  end;
end;

procedure TFmPrincipal.AdvGlowButton100Click(Sender: TObject);
begin
  DmNFe_0000.AtualizaXML_No_BD_Tudo(True);
end;

procedure TFmPrincipal.AdvGlowButton101Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeExportaXML();
end;

procedure TFmPrincipal.AdvGlowButton102Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPreEmail, FmPreEmail, afmoNegarComAviso) then
  begin
    FmPreEmail.ShowModal;
    FmPreEmail.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton103Click(Sender: TObject);
begin
  Grade_Jan.MostraFormStqInnCad(0);
end;

procedure TFmPrincipal.AdvGlowButton104Click(Sender: TObject);
begin
  GFat_Jan.MostraFormFatDivGer();
end;

procedure TFmPrincipal.AdvGlowButton105Click(Sender: TObject);
begin
  FmLinkRankSkin.Show;
end;

procedure TFmPrincipal.AdvGlowButton106Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeEveRLoE(0);
end;

procedure TFmPrincipal.AdvGlowButton107Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeCntngnc();
end;

procedure TFmPrincipal.AdvGlowButton108Click(Sender: TObject);
var
  FatNum, Empresa, IDCtrl: Integer;
  SQL: String;
begin
  if not DBCheck.LiberaPelaSenhaBoss then
    Exit;
  //
  // Atualiza XML DB com �spas triplas
  try
    Screen.Cursor := crHourGlass;
    //
    SQL := '';
    //
    DmodG.QrAux.Close;
    DmodG.QrAux.SQL.Clear;
    DmodG.QrAux.SQL.Add('SELECT FatNum, Empresa, IDCtrl, XML_NFe');
    DmodG.QrAux.SQL.Add('FROM nfearq');
    DmodG.QrAux.SQL.Add('WHERE XML_NFe LIKE ''%' + '"""' + '%''');
    DmodG.QrAux.SQL.Add('OR XML_NFe LIKE ''%' + '"""' + '%''');
    DmodG.QrAux.SQL.Add('OR XML_NFe LIKE ''%' + '""' + '%''');
    DmodG.QrAux.Open;
    if DmodG.QrAux.RecordCount > 0 then
    begin
      DmodG.QrAux.First;
      while not DmodG.QrAux.Eof do
      begin
        FatNum := DmodG.QrAux.FieldByName('FatNum').AsInteger;
        Empresa := DmodG.QrAux.FieldByName('Empresa').AsInteger;
        IDCtrl := DmodG.QrAux.FieldByName('IDCtrl').AsInteger;
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfearq', False, ['XML_NFe'],
          ['FatNum', 'Empresa', 'IDCtrl'],
          [Geral.WideStringToSQLString(DmodG.QrAux.FieldByName('XML_NFe')
          .AsWideString)], [FatNum, Empresa, IDCtrl], True) then
        begin
        end;
        DmodG.QrAux.Next;
      end;
    end;
    DmodG.QrAux.Close;
    DmodG.QrAux.SQL.Clear;
    DmodG.QrAux.SQL.Add('SELECT FatNum, Empresa, IDCtrl, XML_Aut');
    DmodG.QrAux.SQL.Add('FROM nfearq');
    DmodG.QrAux.SQL.Add('WHERE XML_Aut LIKE ''%' + '"""' + '%''');
    DmodG.QrAux.SQL.Add('OR XML_Aut LIKE ''%' + '"""' + '%''');
    DmodG.QrAux.SQL.Add('OR XML_Aut LIKE ''%' + '""' + '%''');
    DmodG.QrAux.Open;
    if DmodG.QrAux.RecordCount > 0 then
    begin
      DmodG.QrAux.First;
      while not DmodG.QrAux.Eof do
      begin
        FatNum := DmodG.QrAux.FieldByName('FatNum').AsInteger;
        Empresa := DmodG.QrAux.FieldByName('Empresa').AsInteger;
        IDCtrl := DmodG.QrAux.FieldByName('IDCtrl').AsInteger;
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfearq', False, ['XML_Aut'],
          ['FatNum', 'Empresa', 'IDCtrl'],
          [Geral.WideStringToSQLString(DmodG.QrAux.FieldByName('XML_Aut')
          .AsWideString)], [FatNum, Empresa, IDCtrl], True) then
        begin
        end;
        DmodG.QrAux.Next;
      end;
    end;
    DmodG.QrAux.Close;
    DmodG.QrAux.SQL.Clear;
    DmodG.QrAux.SQL.Add('SELECT FatNum, Empresa, IDCtrl, XML_Can');
    DmodG.QrAux.SQL.Add('FROM nfearq');
    DmodG.QrAux.SQL.Add('WHERE XML_Can LIKE ''%' + '"""' + '%''');
    DmodG.QrAux.SQL.Add('OR XML_Can LIKE ''%' + '"""' + '%''');
    DmodG.QrAux.SQL.Add('OR XML_Can LIKE ''%' + '""' + '%''');
    DmodG.QrAux.Open;
    if DmodG.QrAux.RecordCount > 0 then
    begin
      DmodG.QrAux.First;
      while not DmodG.QrAux.Eof do
      begin
        FatNum := DmodG.QrAux.FieldByName('FatNum').AsInteger;
        Empresa := DmodG.QrAux.FieldByName('Empresa').AsInteger;
        IDCtrl := DmodG.QrAux.FieldByName('IDCtrl').AsInteger;
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfearq', False, ['XML_Can'],
          ['FatNum', 'Empresa', 'IDCtrl'],
          [Geral.WideStringToSQLString(DmodG.QrAux.FieldByName('XML_Can')
          .AsWideString)], [FatNum, Empresa, IDCtrl], True) then
        begin
        end;
        DmodG.QrAux.Next;
      end;
    end;
    DmodG.QrAux.Close;
    DmodG.QrAux.SQL.Clear;
    DmodG.QrAux.SQL.Add('SELECT FatNum, Empresa, IDCtrl, XML_Den');
    DmodG.QrAux.SQL.Add('FROM nfearq');
    DmodG.QrAux.SQL.Add('WHERE XML_Den LIKE ''%' + '"""' + '%''');
    DmodG.QrAux.SQL.Add('OR XML_Den LIKE ''%' + '"""' + '%''');
    DmodG.QrAux.SQL.Add('OR XML_Den LIKE ''%' + '""' + '%''');
    DmodG.QrAux.Open;
    if DmodG.QrAux.RecordCount > 0 then
    begin
      DmodG.QrAux.First;
      while not DmodG.QrAux.Eof do
      begin
        FatNum := DmodG.QrAux.FieldByName('FatNum').AsInteger;
        Empresa := DmodG.QrAux.FieldByName('Empresa').AsInteger;
        IDCtrl := DmodG.QrAux.FieldByName('IDCtrl').AsInteger;
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfearq', False, ['XML_Den'],
          ['FatNum', 'Empresa', 'IDCtrl'],
          [Geral.WideStringToSQLString(DmodG.QrAux.FieldByName('XML_Den')
          .AsWideString)], [FatNum, Empresa, IDCtrl], True) then
        begin
        end;
        DmodG.QrAux.Next;
      end;
    end;
  finally
    Screen.Cursor := crDefault;
    Geral.MB_Info('Atualiza��o finalizada!');
  end;
end;

procedure TFmPrincipal.AdvGlowButton10Click(Sender: TObject);
begin
  Grade_Jan.MostraFormPrdGrupTip(0);
end;

procedure TFmPrincipal.AdvGlowButton11Click(Sender: TObject);
begin
  MostraGraGruN;
end;

procedure TFmPrincipal.AdvGlowButton12Click(Sender: TObject);
begin
  Grade_Jan.MostraFormGraCorFam(0);
end;

procedure TFmPrincipal.AdvGlowButton13Click(Sender: TObject);
begin
  UMedi_PF.MostraUnidMed(0);
end;

procedure TFmPrincipal.AdvGlowButton14Click(Sender: TObject);
begin
  FinanceiroJan.SaldoDeContas(0);
end;

procedure TFmPrincipal.AdvGlowButton15Click(Sender: TObject);
begin
  Application.CreateForm(TFmAbout, FmAbout);
  FmAbout.ShowModal;
  FmAbout.Destroy;
end;

procedure TFmPrincipal.AdvGlowButton167Click(Sender: TObject);
begin
  dmkWeb.AbrirAppAcessoRemoto;
end;

procedure TFmPrincipal.AdvGlowButton16Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmFatPedImp2, FmFatPedImp2, afmoNegarComAviso) then
  begin
    FmFatPedImp2.ShowModal;
    FmFatPedImp2.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton17Click(Sender: TObject);
begin
  MostraOpcoes;
end;

procedure TFmPrincipal.AdvGlowButton18Click(Sender: TObject);
begin
  FinanceiroJan.CadastroDeCarteiras(0);
end;

procedure TFmPrincipal.AdvGlowButton19Click(Sender: TObject);
begin
  FinanceiroJan.ImpressaoDoPlanoDeContas;
end;

procedure TFmPrincipal.AdvGlowButton1Click(Sender: TObject);
begin
  GFat_Jan.MostraFormFatPedCab();
end;

procedure TFmPrincipal.AdvGlowButton20Click(Sender: TObject);
begin
  FinanceiroJan.CadastroDePlano(0);
end;

procedure TFmPrincipal.AdvGlowButton21Click(Sender: TObject);
begin
  FinanceiroJan.CadastroDeNiveisPlano;
end;

procedure TFmPrincipal.AdvGlowButton22Click(Sender: TObject);
begin
  FinanceiroJan.CadastroDeConjutos(0);
end;

procedure TFmPrincipal.AdvGlowButton23Click(Sender: TObject);
begin
  FinanceiroJan.CadastroDeGrupos(0);
end;

procedure TFmPrincipal.AdvGlowButton24Click(Sender: TObject);
begin
  FinanceiroJan.CadastroDeSubGrupos(0);
end;

procedure TFmPrincipal.AdvGlowButton25Click(Sender: TObject);
begin
  FinanceiroJan.CadastroDeContas(0);
end;

procedure TFmPrincipal.AdvGlowButton26Click(Sender: TObject);
begin
  Grade_Jan.MostraGraAtrCad(0);
end;

procedure TFmPrincipal.AdvGlowButton27Click(Sender: TObject);
begin
  Grade_Jan.MostraGraFibCad(0);
end;

procedure TFmPrincipal.AdvGlowButton28Click(Sender: TObject);
begin
  Grade_Jan.MostraGraTecCad(0);
end;

procedure TFmPrincipal.AdvGlowButton29Click(Sender: TObject);
begin
  MostraPediVda;
end;

procedure TFmPrincipal.AdvGlowButton2Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEntidade2, FmEntidade2, afmoNegarComAviso) then
  begin
    FmEntidade2.ShowModal;
    FmEntidade2.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton30Click(Sender: TObject);
begin
  Praz_PF.MostraFormPediPrzCab1(0);
end;

procedure TFmPrincipal.AdvGlowButton31Click(Sender: TObject);
begin
  MostraCambioMda;
end;

procedure TFmPrincipal.AdvGlowButton32Click(Sender: TObject);
begin
  Grade_Jan.MostraFormGraCusPrc(0);
end;

procedure TFmPrincipal.AdvGlowButton33Click(Sender: TObject);
begin
  MostraCambioCot;
end;

procedure TFmPrincipal.AdvGlowButton34Click(Sender: TObject);
begin
  MostraGeosite;
end;

procedure TFmPrincipal.AdvGlowButton35Click(Sender: TObject);
begin
  MostraRegioes;
end;

procedure TFmPrincipal.AdvGlowButton36Click(Sender: TObject);
begin
  MostraPediAcc;
end;

procedure TFmPrincipal.AdvGlowButton37Click(Sender: TObject);
begin
  MostraTabePrcCab;
end;

procedure TFmPrincipal.AdvGlowButton38Click(Sender: TObject);
begin
  MostraMotivos;
end;

procedure TFmPrincipal.AdvGlowButton39Click(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  QrXXX1.Close;
  QrXXX1.Open;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE gragru3 SET PrdGrupTip=:P0 WHERE Nivel3=:P1');
  Dmod.QrUpd.SQL.Add('');
  while not QrXXX1.Eof do
  begin
    Dmod.QrUpd.Params[00].AsInteger := QrXXX1PrdGrupTip.Value;
    Dmod.QrUpd.Params[01].AsInteger := QrXXX1Nivel3.Value;
    Dmod.QrUpd.ExecSQL;
    //
    QrXXX1.Next;
  end;
  Screen.Cursor := crDefault;
end;

procedure TFmPrincipal.AdvGlowButton3Click(Sender: TObject);
begin
  VerificaNovasVersoes();
end;

procedure TFmPrincipal.AdvGlowButton40Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeInfCpl();
end;

procedure TFmPrincipal.AdvGlowButton41Click(Sender: TObject);
begin
  MostraParamsEmp();
end;

procedure TFmPrincipal.MostraParamsEmp();
begin
  if DBCheck.CriaFm(TFmParamsEmp, FmParamsEmp, afmoSoBoss) then
  begin
    FmParamsEmp.ShowModal;
    FmParamsEmp.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton42Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEtqPrinCad, FmEtqPrinCad, afmoSoAdmin) then
  begin
    FmEtqPrinCad.ShowModal;
    FmEtqPrinCad.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton43Click(Sender: TObject);
begin
  MostraEntiTipCto();
end;

procedure TFmPrincipal.AdvGlowButton44Click(Sender: TObject);
begin
  MostraEntiCargos();
end;

procedure TFmPrincipal.AdvGlowButton45Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEtqGeraLot, FmEtqGeraLot, afmoNegarComAviso) then
  begin
    FmEtqGeraLot.ShowModal;
    FmEtqGeraLot.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton46Click(Sender: TObject);
begin
  UMyMod.AtualizaEntidadesCodigoToCodUsu('entidades');
end;

procedure TFmPrincipal.AdvGlowButton48Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormLayoutNFe();
end;

procedure TFmPrincipal.AdvGlowButton49Click(Sender: TObject);
begin
  Grade_Jan.MostraFormStqBalCad(0);
end;

procedure TFmPrincipal.AdvGlowButton4Click(Sender: TObject);
begin
  MostraBackup3;
end;

procedure TFmPrincipal.AdvGlowButton50Click(Sender: TObject);
begin
  Grade_Jan.MostraFormFisRegCad(0);
end;

procedure TFmPrincipal.AdvGlowButton51Click(Sender: TObject);
begin
  Grade_Jan.MostraFormCFOP2003();
end;

procedure TFmPrincipal.AdvGlowButton52Clic(Sender: TObject);
begin
  Grade_Jan.MostraFormModelosImp();
end;

procedure TFmPrincipal.AdvGlowButton55Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmFatConCad, FmFatConCad, afmoNegarComAviso) then
  begin
    FmFatConCad.ShowModal;
    FmFatConCad.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton56Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmStqManCad, FmStqManCad, afmoNegarComAviso) then
  begin
    FmStqManCad.ShowModal;
    FmStqManCad.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton57Click(Sender: TObject);
begin
  UnNotificacoes.MostraFmNotificacoes(PageControl1, AdvToolBarPager1);
end;

procedure TFmPrincipal.AdvGlowButton58Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMatriz, FmMatriz, afmoSoBoss) then
  begin
    FmMatriz.ShowModal;
    FmMatriz.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton59Click(Sender: TObject);
begin
  Grade_Jan.MostraFormNCMs();
end;

procedure TFmPrincipal.AdvGlowButton5Click(Sender: TObject);
begin
  Grade_Jan.MostraFormGraTamCad(0);
end;

procedure TFmPrincipal.AdvGlowButton60Click(Sender: TObject);
begin
  FinanceiroJan.MostraFinancas(PageControl1, AdvToolBarPager1, True);
end;

procedure TFmPrincipal.AdvGlowButton61Click(Sender: TObject);
begin
  UnOrdProd.MostraFormOrdProdCab();
end;

procedure TFmPrincipal.AdvGlowButton62Click(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  QrAjusVol.Close;
  QrAjusVol.Open;
  PB1.Position := 0;
  PB1.Visible := True;
  PB1.Max := QrAjusVol.RecordCount;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE stqmovvala SET OriCnta=:P0');
  Dmod.QrUpd.SQL.Add('WHERE Tipo=1 AND IDCtrl=:P1 AND OriCnta=0');
  Dmod.QrUpd.SQL.Add('');
  //
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT OriCnta');
  Dmod.QrAux.SQL.Add('FROM stqmovvala');
  Dmod.QrAux.SQL.Add('WHERE Tipo=1');
  Dmod.QrAux.SQL.Add('AND IDCtrl=:P0');
  Dmod.QrAux.SQL.Add('AND OriCnta <> 0');
  //
  while not QrAjusVol.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    PB1.Update;
    Application.ProcessMessages;
    //
    Dmod.QrAux.Close;
    Dmod.QrAux.Params[0].AsInteger := QrAjusVolIDCtrl.Value;
    Dmod.QrAux.Open;
    //
    Dmod.QrUpd.Params[00].AsInteger := Dmod.QrAux.FieldByName('OriCnta')
      .AsInteger;
    Dmod.QrUpd.Params[01].AsInteger := QrAjusVolIDCtrl.Value;
    Dmod.QrUpd.ExecSQL;
    //
    QrAjusVol.Next;
  end;
  Screen.Cursor := crDefault;
  Geral.MensagemBox(IntToStr(QrAjusVol.RecordCount) + ' registros ' +
    'foram ajustados!', 'Mensagem', MB_OK + MB_ICONINFORMATION);
end;

procedure TFmPrincipal.AdvGlowButton63Click(Sender: TObject);
begin
  EfdIcmsIpi_Jan.MostraFormEfdIcmsIpiE001();
end;

procedure TFmPrincipal.AdvGlowButton64Click(Sender: TObject);
begin
  EfdIcmsIpi_Jan.MostraFormEfdIcmsIpiExporta();
  //EfdIcmsIpi_Jan.MostraFormSPED_EFD_Exporta();
end;

procedure TFmPrincipal.AdvGlowButton65Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmSPED_EFD_Importa, FmSPED_EFD_Importa, afmoNegarComAviso) then
  begin
    FmSPED_EFD_Importa.ShowModal;
    FmSPED_EFD_Importa.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton72Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPlanCutImp, FmPlanCutImp, afmoNegarComAviso) then
  begin
    FmPlanCutImp.ShowModal;
    FmPlanCutImp.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton74Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMatPartCad, FmMatPartCad, afmoNegarComAviso) then
  begin
    FmMatPartCad.ShowModal;
    FmMatPartCad.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton75Click(Sender: TObject);
{ var
  Sequenzia: Integer;
}
begin
  { QrPediVdaIts.Close;
    QrPediVdaIts.Open;
    if QrPediVdaIts.RecordCount > 0 then
    begin
    Screen.Cursor := crHourGlass;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE pedivdaits SET ');
    Dmod.QrUpd.SQL.Add('Sequenzia=-Controle WHERE Sequenzia=0');
    Dmod.QrUpd.ExecSQL;
    Screen.Cursor := crDefault;
    //
    Geral.MensagemBox(IntToStr(QrPediVdaIts.RecordCount) +
    ' itens foram ajustados!', 'Aviso', MB_OK+MB_ICONWARNING);
    end;
  }
end;

procedure TFmPrincipal.AdvGlowButton76Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmSugVPedCab, FmSugVPedCab, afmoNegarComAviso) then
  begin
    FmSugVPedCab.ShowModal;
    FmSugVPedCab.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton78Click(Sender: TObject);
const
  Cliente       = 0;
  CU_PediVda    = 0;
  ForcaCriarXML = False;
var
  EMP_FILIAL: Integer;
begin
  EMP_FILIAL := DmodG.QrFiliLogFilial.Value;
  //
  UnNFe_PF.MostraFormFatPedNFs(EMP_FILIAL, Cliente, CU_PediVda, ForcaCriarXML);
end;

procedure TFmPrincipal.AdvGlowButton79Click(Sender: TObject);
var
  Qtde, Preco, Valor: Double;
  Antes, Depois: Integer;
begin
  Screen.Cursor := crHourGlass;
  //
  QrStqManIts.Close;
  QrStqManIts.Open;
  //
  Antes := QrStqManIts.RecordCount;
  Depois := 0;
  if QrStqManIts.RecordCount > 0 then
  begin
    while not QrStqManIts.Eof do
    begin
      Qtde := QrStqManItsQtde.Value;
      Preco := QrStqManItsCustoPreco.Value;
      Valor := Trunc(Preco * Qtde * 100) / 100;
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'stqmanits', False,
        ['Codigo', 'GraGruX', 'Qtde', 'Valor', 'Preco'], ['Controle'],
        [QrStqManItsOriCodi.Value, QrStqManItsGraGruX.Value, Qtde, Valor,
        Preco], [QrStqManItsOriCtrl.Value], True);
      //
      QrStqManIts.Next;
    end;
    QrStqManIts.Close;
    QrStqManIts.Open;
    Depois := QrStqManIts.RecordCount;
  end;
  Screen.Cursor := crDefault;
  //
  Geral.MensagemBox('Foram localizados ' + IntToStr(Antes) + ' itens orf�os. ' +
    #13 + #10 + FormatFloat('0', Depois) + ' itens n�o foram modificados!',
    'Informa��o', MB_OK + MB_ICONWARNING);
end;

procedure TFmPrincipal.AdvGlowButton7Click(Sender: TObject);
begin
  Grade_Jan.MostraFormStqCenCad(0);
end;

procedure TFmPrincipal.AdvGlowButton80Click(Sender: TObject);
begin
  Geral.MensagemBox('As op��es s�o setadas diretamente no cadastro da filial!',
    'Mensagem', MB_OK + MB_ICONINFORMATION);
  {
    DmNFe_0000.ReopenOpcoesNFe(-11, True);
    UMyMod.FormInsUpd_Show(TFmNFeOpcoes, FmNFeOpcoes, afmoSoAdmin,
    DmNFe_0000.QrOpcoesNFe, stUpd);
    DmNFe_0000.ReopenOpcoesNFe(-11, True);
  }
end;

procedure TFmPrincipal.AdvGlowButton81Click(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE stqmovnfsa SET DtEmissNF=DataCad');
  Dmod.QrUpd.SQL.Add('WHERE DtEmissNF=0');
  Dmod.QrUpd.ExecSQL;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE stqmovnfsa SET DtEntraSai=DataCad');
  Dmod.QrUpd.SQL.Add('WHERE DtEntraSai=0');
  Dmod.QrUpd.ExecSQL;
  //
  Screen.Cursor := crDefault;
end;

procedure TFmPrincipal.AdvGlowButton82Click(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DROP TABLE ufs');
  Dmod.QrUpd.ExecSQL;
  //
  Screen.Cursor := crDefault;
  Application.CreateForm(TFmVerifiDB, FmVerifiDB);
  with FmVerifiDB do
  begin
    BtSair.Enabled := False;
    FVerifi := True;
    ShowModal;
    FVerifi := False;
    Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton83Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmNFeLayout_0200, FmNFeLayout_0200, afmoSoAdmin) then
  begin
    FmNFeLayout_0200.ShowModal;
    FmNFeLayout_0200.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton84Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeLEnc(0);
end;

procedure TFmPrincipal.AdvGlowButton86Click(Sender: TObject);
var
  Vis: Boolean;
begin
  try
    Screen.Cursor := crHourGlass;
    GBAvisos1.Visible := True;
    Vis := PB1.Visible;
    PB1.Visible := True;

    DmProd.CorrigeFatID_Venda(PB1, LaAviso1, LaAviso2);

    PB1.Visible := Vis;
  finally
    GBAvisos1.Visible := False;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPrincipal.GBValidaXMLClick(Sender: TObject);
begin
  UnNFe_PF.ValidaXML_NFe('');
end;

procedure TFmPrincipal.AdvGlowButton88Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFePesq(False, PageControl1, AdvToolBarPager1, 0);
end;

procedure TFmPrincipal.AdvGlowButton89Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeJust();
end;

procedure TFmPrincipal.AdvGlowButton8Click(Sender: TObject);
begin
  Grade_Jan.MostraFormGraCorPan(0);
end;

procedure TFmPrincipal.AdvGlowButton90Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeInut();
end;

procedure TFmPrincipal.AdvGlowButton91Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMedOrdem, FmMedOrdem, afmoNegarComAviso) then
  begin
    FmMedOrdem.ShowModal;
    FmMedOrdem.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton92Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMalaDireta, FmMalaDireta, afmoNegarComAviso) then
  begin
    FmMalaDireta.ShowModal;
    FmMalaDireta.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton93Click(Sender: TObject);
begin
  Grade_Jan.MostraFormOpcoesGrad();
end;

procedure TFmPrincipal.AdvGlowButton95Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeCabA();
end;

procedure TFmPrincipal.AdvGlowButton96Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormStepsNFe_StatusServico();
end;

procedure TFmPrincipal.AdvGlowButton97Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmFatConRet, FmFatConRet, afmoNegarComAviso) then
  begin
    FmFatConRet.ShowModal;
    FmFatConRet.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton98Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmBancos, FmBancos, afmoNegarComAviso) then
  begin
    FmBancos.ShowModal;
    FmBancos.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton99Click(Sender: TObject);
begin
  FinanceiroJan.CadastroContasLnk;
end;

procedure TFmPrincipal.AdvGlowButton9Click(Sender: TObject);
begin
  Grade_Jan.MostraFormGraCorCad(0);
end;

procedure TFmPrincipal.AdvGlowMenuButton3Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(TPopupMenu(AdvPMImagem),
    TButton(AdvGlowMenuButton3));
end;

procedure TFmPrincipal.AdvGlowMenuButton4Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(TPopupMenu(AdvPMMenuCor),
    TButton(AdvGlowMenuButton4));
end;

procedure TFmPrincipal.AdvPreviewMenu1Buttons0Click(Sender: TObject;
  Button: TButtonCollectionItem);
begin
  Close;
end;

procedure TFmPrincipal.AdvPreviewMenu1Buttons1Click(Sender: TObject;
  Button: TButtonCollectionItem);
begin
  MostraBackup3;
end;

procedure TFmPrincipal.AdvPreviewMenu1Buttons2Click(Sender: TObject;
  Button: TButtonCollectionItem);
begin
  VerificaNovasVersoes();
end;

procedure TFmPrincipal.AdvPreviewMenu1Buttons3Click(Sender: TObject;
  Button: TButtonCollectionItem);
begin
  MostraVerifiDB;
end;

procedure TFmPrincipal.AdvPreviewMenu2Buttons0Click(Sender: TObject;
  Button: TButtonCollectionItem);
begin
  ShowMessage('Teste');
end;

procedure TFmPrincipal.AdvToolBarButton10Click(Sender: TObject);
begin
  MostraBackup3;
end;

procedure TFmPrincipal.AdvToolBarButton12Click(Sender: TObject);
begin
  MyObjects.MaximizaAdvToolBarPager(AdvToolBarPager1, deftfInverse);
end;

procedure TFmPrincipal.AdvToolBarButton13Click(Sender: TObject);
begin
{$IFDEF UsaWSuport}
  DmkWeb.MostraWSuporte(False, 2, BalloonHint1);
{$ENDIF}
end;

procedure TFmPrincipal.AdvToolBarButton7Click(Sender: TObject);
begin
  FmPrincipal.Enabled := False;
  //
  FmPlanning_Dmk.Show;
  FmPlanning_Dmk.EdLogin.Text := '';
  FmPlanning_Dmk.EdSenha.Text := '';
  FmPlanning_Dmk.EdEmpresa.Text := '';
  FmPlanning_Dmk.EdLogin.SetFocus;
end;

procedure TFmPrincipal.AdvToolBarButton8Click(Sender: TObject);
begin
  VerificaNovasVersoes();
end;

procedure TFmPrincipal.AGBConsultaNFeClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeConsulta();
end;

procedure TFmPrincipal.AGBFluxoCabClick(Sender: TObject);
begin
  TX_Jan.MostraFormFluxoCab(0);
end;

procedure TFmPrincipal.AGBGraGruYClick(Sender: TObject);
begin
  Grade_Jan.MostraFormGraGruY(0, 0, '');
end;

procedure TFmPrincipal.AGBLaySPEDEFDClick(Sender: TObject);
begin
  if Geral.MB_Pergunta(
  'Ser�o baixadas e atualizadas as tabelas do layout do SPED EFD.' +
  'Tem certeza que deseja continuar?') =
  ID_YES then
  begin
    Memo3.Visible := True;
    Memo3.Lines.Clear;
    //EfdIcmsIpi_Jan.BaixaLayoutSPED(Memo3);
    EfdIcmsIpi_PF.BaixaLayoutSPED(Memo3);
    Memo3.Visible := False;
  end;
end;

procedure TFmPrincipal.AGBNewFinMigraClick(Sender: TObject);
begin
  DmLct2.MigraLctsParaTabLct();
end;

procedure TFmPrincipal.AGBNFeDesDowCClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeCnfDowC_0100();
end;

procedure TFmPrincipal.AGBNFeDestClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeDistDFeInt();
end;

procedure TFmPrincipal.AGBNFeExportaXML_BClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeExportaXML_B();
end;

procedure TFmPrincipal.AGBNFeLoad_InnClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeLoad_Inn();
end;

procedure TFmPrincipal.AGBOperacoesClick(Sender: TObject);
begin
  TX_Jan.MostraFormOperacoes(0);
end;

procedure TFmPrincipal.AGBPediVdaImpClick(Sender: TObject);
begin
  GFat_Jan.MostraFormPediVdaImp();
end;

procedure TFmPrincipal.AGBSetorCadClick(Sender: TObject);
begin
  TX_Jan.MostraFormSetorCad(0);
end;

procedure TFmPrincipal.AGBTXAjsCabClick(Sender: TObject);
begin
  TX_Jan.MostraFormTXAjsCab(0,0);
end;

procedure TFmPrincipal.AGBTXCadFccClick(Sender: TObject);
begin
  TX_Jan.MostraFormTXCadFcc(0, False, nil);
end;

procedure TFmPrincipal.AGBTXCadNatClick(Sender: TObject);
begin
  TX_Jan.MostraFormTXCadNat(0, False, nil);
end;

procedure TFmPrincipal.AGBTXExBCabClick(Sender: TObject);
begin
  TX_Jan.MostraFormTXExBCab(0, 0);
end;

procedure TFmPrincipal.AGBTXCadIndClick(Sender: TObject);
begin
  TX_Jan.MostraFormTXCadInd(0, False, nil);
end;

procedure TFmPrincipal.AGBTXCadIntClick(Sender: TObject);
begin
  TX_Jan.MostraFormTXCadInt(0, False, nil);
end;

procedure TFmPrincipal.AGBTXIndCabClick(Sender: TObject);
begin
  TX_Jan.MostraFormTXIndCab(0, 0, 0, 0, 0);
end;

procedure TFmPrincipal.AGBTXInnCabClick(Sender: TObject);
begin
  TX_Jan.MostraFormTXInnCab(0, 0, 0);
end;

procedure TFmPrincipal.AGBTXOutItsClick(Sender: TObject);
begin
  TX_JAN.MostraFormTXBxaCab(0, 0);
end;

procedure TFmPrincipal.AGBTXPalletClick(Sender: TObject);
begin
  TX_Jan.MostraFormTXPallet(0);
end;

procedure TFmPrincipal.AGBTXPalStaClick(Sender: TObject);
begin
  TX_Jan.MostraFormTXPalSta();
end;

procedure TFmPrincipal.AGBTXSerTalClick(Sender: TObject);
begin
  TX_Jan.MostraFormTXSerTal();
end;

procedure TFmPrincipal.AGBTXTrfLocCabClick(Sender: TObject);
begin
  TX_Jan.MostraFormTXTrfLocCab(0);
end;

procedure TFmPrincipal.AGBVSMovItsClick(Sender: TObject);
begin
  TX_Jan.MostraFormTXMovIts(0);
end;

procedure TFmPrincipal.AGBVSPlCCabClick(Sender: TObject);
const
  Codigo   = 0;
  Controle = 0;
begin
  TX_Jan.MostraFormTXPlCCab(Codigo, Controle);
end;

procedure TFmPrincipal.AGBVSRRMCabClick(Sender: TObject);
begin
  TX_Jan.MostraFormTXRRMCab(0);
end;

procedure TFmPrincipal.SBCadEntidadesClick(Sender: TObject);
begin
  CadastroEntidades;
end;

procedure TFmPrincipal.sd1FormSkin(Sender: TObject; aName: string;
  var DoSkin: Boolean);
begin
  if aName = VAR_FORMTDI_NAME then
    DoSkin := False;
end;

procedure TFmPrincipal.CadastroEntidades;
begin
  if DBCheck.CriaFm(TFmEntidades, FmEntidades, afmoNegarComAviso) then
  begin
    FmEntidades.ShowModal;
    FmEntidades.Destroy;
  end;
end;

procedure TFmPrincipal.MostraGeosite;
begin
  if DBCheck.CriaFm(TFmGeosite, FmGeosite, afmoNegarComAviso) then
  begin
    FmGeosite.ShowModal;
    FmGeosite.Destroy;
  end;
end;

procedure TFmPrincipal.MostraGraGruN;
begin
  if DBCheck.CriaFm(TFmGraGruN, FmGraGruN, afmoNegarComAviso) then
  begin
    FmGraGruN.ShowModal;
    FmGraGruN.Destroy;
  end;
end;

procedure TFmPrincipal.MostraMotivos;
begin
  if DBCheck.CriaFm(TFmMotivos, FmMotivos, afmoNegarComAviso) then
  begin
    FmMotivos.ShowModal;
    FmMotivos.Destroy;
  end;
end;

procedure TFmPrincipal.MostraOpcoes;
begin
  if DBCheck.CriaFm(TFmOpcoes, FmOpcoes, afmoNegarComAviso) then
  begin
    FmOpcoes.ShowModal;
    FmOpcoes.Destroy;
  end;
end;

procedure TFmPrincipal.MostraPediVda;
begin
  Screen.Cursor := crHourGlass;
  DmodG.ReopenEmpresas(VAR_USUARIO, 0);
  DmPediVda.ReopenTabelasPedido();
  Screen.Cursor := crDefault;
  //
  if DBCheck.CriaFm(TFmPediVda, FmPediVda, afmoNegarComAviso) then
  begin
    FmPediVda.ShowModal;
    FmPediVda.Destroy;
  end;
end;

procedure TFmPrincipal.MostraRegioes;
begin
  if DBCheck.CriaFm(TFmRegioes, FmRegioes, afmoNegarComAviso) then
  begin
    FmRegioes.ShowModal;
    FmRegioes.Destroy;
  end;
end;

procedure TFmPrincipal.MostraTabePrcCab;
begin
  if DBCheck.CriaFm(TFmTabePrcCab, FmTabePrcCab, afmoNegarComAviso) then
  begin
    FmTabePrcCab.ShowModal;
    FmTabePrcCab.Destroy;
  end;
end;

procedure TFmPrincipal.MostraEntiCargos;
begin
  if DBCheck.CriaFm(TFmEntiCargos, FmEntiCargos, afmoNegarComAviso) then
  begin
    FmEntiCargos.ShowModal;
    FmEntiCargos.Destroy;
  end;
end;

procedure TFmPrincipal.MostraEntiContat;
begin
  if DBCheck.CriaFm(TFmEntiContat, FmEntiContat, afmoNegarComAviso) then
  begin
    FmEntiContat.ShowModal;
    FmEntiContat.Destroy;
  end;
end;

procedure TFmPrincipal.MostraEntiTipCto;
begin
  if DBCheck.CriaFm(TFmEntiTipCto, FmEntiTipCto, afmoNegarComAviso) then
  begin
    FmEntiTipCto.ShowModal;
    FmEntiTipCto.Destroy;
  end;
end;

procedure TFmPrincipal.MostraVerifiDB;
begin
  if DBCheck.CriaFm(TFmVerifiDB, FmVerifiDB, afmoNegarComAviso) then
  begin
    FmVerifiDB.ShowModal;
    FmVerifiDB.Destroy;
  end;
end;

procedure TFmPrincipal.N02EstoqueEm1Click(Sender: TObject);
begin
  TX_Jan.MostraFormTXMovImpEstoque(True);
end;

procedure TFmPrincipal.NotificacoesJanelas(TipoNotifi: TNotifi;
  DataHora: TDateTime);
begin
  //Colocar notifica��es espec�ficas aqui
end;

function TFmPrincipal.NotificacoesVerifica(QueryNotifi: TmySQLQuery;
  DataHora: TDateTime): Boolean;
begin
  Result := True;
  //Fazer
end;

procedure TFmPrincipal.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  FALiberar := True;
  FmPlanning_Dmk.Show;
  Enabled := False;
  FmPlanning_Dmk.Refresh;
  // FmPlanning_Dmk.EdSenha.Text := FmPlanning_Dmk.EdSenha.Text+'*';
  // FmPlanning_Dmk.EdSenha.Refresh;
  FmPlanning_Dmk.Refresh;
  try
    Application.CreateForm(TDmod, Dmod);
  except
    Geral.MensagemBox('Imposs�vel criar Modulo de dados', 'Erro',
      MB_OK + MB_ICONERROR);
    Application.Terminate;
    Exit;
  end;
  try
    Application.CreateForm(TDmPediVda, DmPediVda);
  except
    Geral.MensagemBox('Imposs�vel criar Modulo de vendas', 'Erro',
      MB_OK + MB_ICONERROR);
    Application.Terminate;
    Exit;
  end;
  // FmPlanning_Dmk.EdSenha.Text := FmPlanning_Dmk.EdSenha.Text+'*';
  // FmPlanning_Dmk.EdSenha.Refresh;
  FmPlanning_Dmk.ReloadSkin;
  FmPlanning_Dmk.EdLogin.Text := '';
  FmPlanning_Dmk.EdLogin.PasswordChar := 'l';
  // FmPlanning_Dmk.EdSenha.Text := '';
  // FmPlanning_Dmk.EdSenha.Refresh;
  FmPlanning_Dmk.EdLogin.ReadOnly := False;
  FmPlanning_Dmk.EdSenha.ReadOnly := False;
  FmPlanning_Dmk.EdLogin.SetFocus;
  // FmPlanning_Dmk.ReloadSkin;
  FmPlanning_Dmk.Refresh;
  //
end;

procedure TFmPrincipal.AtzPed();
var
  Casas, Controle, Codigo, CodigoAnt: Integer;
  PrecoO, PrecoR, PrecoF, DescoP, DescoI, DescoV, ValBru, ValLiq,
    QuantP: Double;
begin
  Codigo := 0;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPrc, Dmod.MyDB, [
    'SELECT * ',
    'FROM pedivdaits ',
    'WHERE PrecoF=0 ',
    'AND Customizad=0 ',
    'AND DescoP <> 100 ',
    'AND PrecoR <> 0 ',
    '']);
  if QrPrc.RecordCount > 0 then
  begin
    Panel1.Visible := True;
    PB1.Visible := True;
    if Geral.MensagemBox('� necessario corrigir dados de pedidos. ' +
      'Deseja faz�-lo agora?', 'Pergunta', MB_YESNOCANCEL + MB_ICONQUESTION) = ID_YES
    then
    begin
      PB1.Position := 0;
      PB1.Max := QrPrc.RecordCount;
      CodigoAnt := 0;
      QrPrc.First;
      while not QrPrc.Eof do
      begin
        PB1.Position := PB1.Position + 1;
        Casas := Dmod.QrControleCasasProd.Value;
        PrecoO := QrPrcPrecoO.Value;
        PrecoR := QrPrcPrecoR.Value;
        QuantP := QrPrcQuantP.Value;
        // ValCal := PrecoR;// * QuantP;
        DescoP := QrPrcDescoP.Value;
        DescoI := MLAGeral.FFF(PrecoR * DescoP / 100, Casas, siPositivo);
        PrecoF := PrecoR - DescoI;
        DescoV := DescoI * QuantP;
        ValBru := PrecoR * QuantP;
        ValLiq := ValBru - DescoV;
        //
        Controle := QrPrcControle.Value;
        Codigo := QrPrcCodigo.Value;
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'pedivdaits', False,
          ['PrecoO', 'PrecoR', 'QuantP', 'ValBru', 'DescoP', 'DescoV', 'ValLiq',
          'PrecoF'], ['Controle'], [PrecoO, PrecoR, QuantP, ValBru, DescoP,
          DescoV, ValLiq, PrecoF], [Controle], True) then
        begin
          dmkEdit1.ValueVariant := dmkEdit1.ValueVariant + 1;
          Update;
          Application.ProcessMessages;
          if (CodigoAnt <> Codigo) then
          begin
            if CodigoAnt > 0 then
              DmPediVda.AtzSdosPedido(CodigoAnt);
            CodigoAnt := Codigo;
            dmkEdit2.ValueVariant := dmkEdit2.ValueVariant + 1;
          end;
        end;
        QrPrc.Next;
      end;
      // if (CodigoAnt <> Codigo) and (Codigo > 0) then
      // begin
      DmPediVda.AtzSdosPedido(Codigo);
      dmkEdit2.ValueVariant := dmkEdit2.ValueVariant + 1;
      // end;
    end;
    PB1.Position := 0;
  end;

  //

  QrPart01.Close;
  QrPart01.Open;
  if QrPart01.RecordCount > 0 then
  begin
    if Geral.MensagemBox
      ('� necessario corrigir dados de faturamento por pedido. ' +
      'Deseja faz�-lo agora?', 'Pergunta', MB_YESNOCANCEL + MB_ICONQUESTION) = ID_YES
    then
    begin
      Screen.Cursor := crHourGlass;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE stqmovitsa sma,');
      Dmod.QrUpd.SQL.Add('(');
      Dmod.QrUpd.SQL.Add('SELECT smia.IDCtrl,  pvi.Controle Parte');
      Dmod.QrUpd.SQL.Add('FROM stqmovitsa smia');
      Dmod.QrUpd.SQL.Add('LEFT JOIN fatpedcab fpc ON fpc.Codigo=smia.OriCodi');
      Dmod.QrUpd.SQL.Add
        ('LEFT JOIN pedivdaits pvi ON pvi.Codigo=fpc.Pedido AND pvi.GraGruX=smia.GraGruX');
      Dmod.QrUpd.SQL.Add('WHERE smia.Tipo=1');
      Dmod.QrUpd.SQL.Add('AND smia.OriPart=0)  AS T');
      Dmod.QrUpd.SQL.Add('SET sma.OriPart=T.Parte WHERE sma.IDCtrl=T.IDCtrl');
      Dmod.QrUpd.ExecSQL;
      //
      QrPart01.Close;
      QrPart01.Open;
      Screen.Cursor := crDefault;
      if QrPart01.RecordCount = 0 then
        Geral.MensagemBox('Corre��o efetuada com sucesso!', 'Mensagem',
          MB_OK + MB_ICONINFORMATION)
      else
        Geral.MensagemBox('N�o foi poss�vel corrigir ' + FormatFloat('0',
          QrPart01.RecordCount) + ' itens!', 'Erro', MB_OK + MB_ICONERROR);
    end;
  end;

  //

  QrPart99.Close;
  QrPart99.Open;
  if QrPart99.RecordCount > 0 then
  begin
    if Geral.MensagemBox('� necessario corrigir dados de faturamento avulso. ' +
      'Deseja faz�-lo agora?', 'Pergunta', MB_YESNOCANCEL + MB_ICONQUESTION) = ID_YES
    then
    begin
      Screen.Cursor := crHourGlass;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE stqmovitsa');
      Dmod.QrUpd.SQL.Add('SET OriPart=OriCtrl');
      Dmod.QrUpd.SQL.Add('WHERE Tipo=99');
      Dmod.QrUpd.SQL.Add('AND OriPart=0');
      Dmod.QrUpd.SQL.Add('');
      Dmod.QrUpd.ExecSQL;
      //
      QrPart99.Close;
      QrPart99.Open;
      Screen.Cursor := crDefault;
      if QrPart99.RecordCount = 0 then
        Geral.MensagemBox('Corre��o efetuada com sucesso!', 'Mensagem',
          MB_OK + MB_ICONINFORMATION)
      else
        Geral.MensagemBox('N�o foi poss�vel corrigir ' +
          IntToStr(QrPart99.RecordCount) + ' itens!', 'Erro',
          MB_OK + MB_ICONERROR);
    end;
  end;
end;

procedure TFmPrincipal.Entidades2Click(Sender: TObject);
begin
  CadastroEntidades;
end;

procedure TFmPrincipal.AcoesExtrasDeCadastroDeEntidades(Grade1: TStringGrid;
  Entidade: Integer; Aba: Boolean = False);
begin
  // Nada
end;

procedure TFmPrincipal.AcoesIniciaisDeCadastroDeEntidades(Reference: TComponent;
  Codigo: Integer; Grade1: TStringGrid);
begin
  // Compatibilidade
end;

function TFmPrincipal.RecriaTabelaLocal(Tabela: String; Acao: Integer): Boolean;
begin
  Result := Ucriar.RecriaTabelaLocal(Tabela, Acao);
end;

procedure TFmPrincipal.Reabrirtabelas1Click(Sender: TObject);
begin
  // MLAGeral.ReabrirtabelasFormAtivo;
end;

procedure TFmPrincipal.ReCaptionComponentesDeForm(Form: TForm);
begin
  // Apenas compatibilidade usado no Syndi2
end;

procedure TFmPrincipal.SalvaArquivo(EdNomCli, EdCPFCli: TEdit;
  Grade: TStringGrid; Data: TDateTime; FileName: String; ChangeData: Boolean);
begin
  // MyCBase.SalvaArquivo(EdNomCli, EdCPFCli, Grade, Data, VAR_DBPATH+FileName, ChangeData);
end;

function TFmPrincipal.AbreArquivoINI(Grade: TStringGrid;
  EdNomCli, EdCPFCli: TEdit; Data: TDateTime; Arquivo: String): Boolean;
begin
  Result := False;
  // MyCBase.AbreArquivoINI(Grade, EdNomCli, EdCPFCli, Data, Arquivo);
end;

procedure TFmPrincipal.ShowHint(Sender: TObject);
begin
  if Length(Application.Hint) > 0 then
  begin
    StatusBar.SimplePanel := True;
    StatusBar.SimpleText := Application.Hint;
  end
  else
    StatusBar.SimplePanel := False;
end;

procedure TFmPrincipal.SkinMenu(Index: Integer);
{ var
  Indice : Integer;
} begin
  { if Index >= 0 then Indice := Index else
    Indice := Geral.ReadAppKey('MenuStyle', Application.Title,
    ktInteger, 1, HKEY_LOCAL_MACHINE);
  } case Index of
    0:
      begin
        AdvToolBarOfficeStyler1.Style := bsOffice2003Blue;
        AdvPreviewMenuOfficeStyler1.Style := psOffice2003Blue;
      end;
    1:
      begin
        AdvToolBarOfficeStyler1.Style := bsOffice2003Classic;
        AdvPreviewMenuOfficeStyler1.Style := psOffice2003Classic;
      end;
    2:
      begin
        AdvToolBarOfficeStyler1.Style := bsOffice2003Olive;
        AdvPreviewMenuOfficeStyler1.Style := psOffice2003Olive;
      end;
    3:
      begin
        AdvToolBarOfficeStyler1.Style := bsOffice2003Silver;
        AdvPreviewMenuOfficeStyler1.Style := psOffice2003Silver;
      end;
    4:
      begin
        AdvToolBarOfficeStyler1.Style := bsOffice2007Luna;
        AdvPreviewMenuOfficeStyler1.Style := psOffice2007Luna;
      end;
    5:
      begin
        AdvToolBarOfficeStyler1.Style := bsOffice2007Obsidian;
        AdvPreviewMenuOfficeStyler1.Style := psOffice2007Obsidian;
      end;
    6:
      begin
        AdvToolBarOfficeStyler1.Style := bsOffice2007Silver;
        AdvPreviewMenuOfficeStyler1.Style := psOffice2007Silver;
      end;
    7:
      begin
        AdvToolBarOfficeStyler1.Style := bsOfficeXP;
        AdvPreviewMenuOfficeStyler1.Style := psOfficeXP;
      end;
    8:
      begin
        AdvToolBarOfficeStyler1.Style := bsWhidbeyStyle;
        AdvPreviewMenuOfficeStyler1.Style := psWhidbeyStyle;
      end;
    9:
      begin
        AdvToolBarOfficeStyler1.Style := bsWindowsXP;
        AdvPreviewMenuOfficeStyler1.Style := psWindowsXP;
      end;
  end;
end;

procedure TFmPrincipal.Plano3Click(Sender: TObject);
begin
  FinanceiroJan.CadastroDePlano(0);
end;

procedure TFmPrincipal.Conjutos2Click(Sender: TObject);
begin
  FinanceiroJan.CadastroDeConjutos(0);
end;

procedure TFmPrincipal.Grupos3Click(Sender: TObject);
begin
  FinanceiroJan.CadastroDeGrupos(0);
end;

procedure TFmPrincipal.Subgrupos3Click(Sender: TObject);
begin
  FinanceiroJan.CadastroDeSubGrupos(0);
end;

procedure TFmPrincipal.Contas3Click(Sender: TObject);
begin
  FinanceiroJan.CadastroDeContas(0);
end;

procedure TFmPrincipal.Todosnveis2Click(Sender: TObject);
begin
  FinanceiroJan.CadastroDeNiveisPlano;
end;

procedure TFmPrincipal.MostraFormDescanso;
begin
  MyObjects.FormTDICria(TFmDescanso, PageControl1, AdvToolBarPager1, False, True, afmoLiberado);
end;

procedure TFmPrincipal.Limpar1Click(Sender: TObject);
begin
  MyObjects.SelecionaLimpaImagemdefundoFormDescanso(FmPrincipal, TFmDescanso,
    PageControl1, True);
end;

procedure TFmPrincipal.Listaplanos2Click(Sender: TObject);
begin
  FinanceiroJan.ImpressaoDoPlanoDeContas;
end;

procedure TFmPrincipal.MostraPediAcc;
begin
  if DBCheck.CriaFm(TFmPediAcc, FmPediAcc, afmoNegarComAviso) then
  begin
    FmPediAcc.ShowModal;
    FmPediAcc.Destroy;
  end;
end;

procedure TFmPrincipal.MostraBackup3;
begin
  DmodG.MostraBackup3();
end;

procedure TFmPrincipal.MostraCambioCot;
begin
  if DBCheck.CriaFm(TFmCambioCot, FmCambioCot, afmoNegarComAviso) then
  begin
    FmCambioCot.ShowModal;
    FmCambioCot.Destroy;
  end;
end;

procedure TFmPrincipal.MostraCambioMda;
begin
  if DBCheck.CriaFm(TFmCambioMda, FmCambioMda, afmoNegarComAviso) then
  begin
    FmCambioMda.ShowModal;
    FmCambioMda.Destroy;
  end;
end;

procedure TFmPrincipal.MostraCarteiras(Carteira: Integer);
begin
  // n�o � necess�rio
end;

procedure TFmPrincipal.DefParams;
{
  var
  Carteira: Integer;
}
begin
  {
    if QrCarteiras_.State <> dsBrowse then Carteira := 0 else Carteira := QrCarteiras_Codigo.Value;
    //ReabreCarteiras(FEntInt, Carteira);
  }
end;

procedure TFmPrincipal.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  if not QrCarteiras_.Locate('Codigo', Codigo, []) then
    QrCarteiras_.Locate('Codigo', Atual, []);
end;

procedure TFmPrincipal.BtDTBClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmIBGE_DTB, FmIBGE_DTB, afmoSoAdmin) then
  begin
    FmIBGE_DTB.ShowModal;
    FmIBGE_DTB.Destroy;
  end;
end;

procedure TFmPrincipal.Button1Click(Sender: TObject);
begin
  Application.CreateForm(TFmCorrigeCidadeEntidade, FmCorrigeCidadeEntidade);
  FmCorrigeCidadeEntidade.ShowModal;
  FmCorrigeCidadeEntidade.Destroy;
end;

function TFmPrincipal.CartaoDeFatura: Integer;
begin
  Result := 0; // FmFaturas.QrFaturasCodigo.Value;
end;

function TFmPrincipal.CompensacaoDeFatura: String;
begin
  Result := '';
  // FormatDateTime(VAR_FORMATDATE, FmFaturas.QrFaturasData.Value);
end;

procedure TFmPrincipal.CriaCalcPercent(Valor, Porcentagem: String;
  Calc: TcpCalc);
begin
  Application.CreateForm(TFmCalcPercent, FmCalcPercent);
  with FmCalcPercent do
  begin
    if Calc = cpMulta then
      LaPorcent.Caption := '% Multa';
    if Calc = cpJurosMes then
      LaPorcent.Caption := '% Juros/m�s';
    EdValor.Text := Valor;
    EdPercent.Text := Porcentagem;
    ShowModal;
    Destroy;
  end;
end;

function TFmPrincipal.CriaFormEntradaCab(MaterialNF: TTipoMaterialNF;
  ShowForm: Boolean; IDCtrl: Integer): Boolean;
begin
  DmodG.ReopenEmpresas(VAR_USUARIO, 0);
  case MaterialNF of
    tnfMateriaPrima:
      begin
        FTipoEntradaDig := VAR_FATID_0113;
        FTipoEntradaNFe := VAR_FATID_0013;
        FTipoEntradaEFD := VAR_FATID_0213;
        FTipoEntradTitu := 'Mat�ria-prima';
      end;
    tnfUsoEConsumo:
      begin
        FTipoEntradaDig := VAR_FATID_0151;
        FTipoEntradaNFe := VAR_FATID_0051;
        FTipoEntradaEFD := VAR_FATID_0251;
        FTipoEntradTitu := 'Uso e Consumo';
      end;
  end;
  Result := DBCheck.CriaFm(TFmEntradaCab, FmEntradaCab, afmoNegarComAviso);
  if Result and ShowForm then
  begin
    if IDCtrl <> 0 then
    begin
      FmEntradaCab.LocCod(IDCtrl, IDCtrl);
      if FmEntradaCab.QrNFeCabAIDCtrl.Value <> IDCtrl then
        Geral.MensagemBox('N�o foi poss�vel localizar o lan�amento solicitado!',
          'Aviso', MB_OK + MB_ICONINFORMATION);
    end;
    FmEntradaCab.ShowModal;
    FmEntradaCab.Destroy;
  end;
end;

procedure TFmPrincipal.VerificaBDServidor1Click(Sender: TObject);
begin
  MostraVerifiDB;
end;

function TFmPrincipal.VerificaNovasVersoes(ApenasVerifica: Boolean = False): Boolean;
var
  Versao: Integer;
  ArqNome: String;
begin
  Result := DmkWeb.VerificaAtualizacaoVersao2(True, True, 'Planning',
    'Planning', Geral.SoNumero_TT(DmodG.QrMasterCNPJ.Value), CO_VERSAO,
    CO_DMKID_APP, DmodG.ObtemAgora(), Memo3, dtExec, Versao, ArqNome, False,
    ApenasVerifica, BalloonHint1);
end;

procedure TFmPrincipal.Verificanovaverso1Click(Sender: TObject);
begin
  VerificaNovasVersoes();
end;

procedure TFmPrincipal.VerificaTabelasterceiros1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmVerifiDBTerceiros, FmVerifiDBTerceiros, afmoNegarComAviso)
  then
  begin
    FmVerifiDBTerceiros.ShowModal;
    FmVerifiDBTerceiros.Destroy;
  end;
end;

procedure TFmPrincipal.TimerIdleTimer(Sender: TObject);
(*
var
  Dia: Integer;
*)
begin
  if DModG <> nil then
    DmodG.ExecutaPing(FmPlanning_Dmk, [Dmod.MyDB, DModG.MyPID_DB, DModG.AllID_DB]);
  (* 2017-05-24 => Movido para o evento onIdle
  TimerIdle.Enabled := False;
  Dia := Geral.ReadAppKey('VeriNetVersao', Application.Title, ktInteger, 0,
    HKEY_LOCAL_MACHINE);
  if (Dia > 2) and (Dia < Int(Date)) then
  begin
    if VerificaNovasVersoes then
      Application.Terminate;
  end
  else
    Application.Terminate;
  *)
end;

procedure TFmPrincipal.TmSuporteTimer(Sender: TObject);
begin
  {$IFDEF UsaWSuport}
  DmkWeb.AtualizaSolicitApl2(Dmod.QrUpd, Dmod.MyDB, TmSuporte, TySuporte,
    AdvToolBarButton13, BalloonHint1);
  {$ENDIF}
end;

procedure TFmPrincipal.TmVersaoTimer(Sender: TObject);
begin
  TmVersao.Enabled := False;
  //
  if DmkWeb.RemoteConnection then
  begin
    if VerificaNovasVersoes(True) then
      DmkWeb.MostraBalloonHintMenuTopo(AdvToolBarButton8, BalloonHint1,
        'H� uma nova vers�o!', 'Clique aqui para atualizar.');
  end;
end;

procedure TFmPrincipal.Planodecontas2Click(Sender: TObject);
begin
  FinanceiroJan.ImpressaoDoPlanoDeContas;
end;

procedure TFmPrincipal.CadastroDeContasNiv();
begin
  if DBCheck.CriaFm(TFmContasNiv, FmContasNiv, afmoNegarComAviso) then
  begin
    FmContasNiv.ShowModal;
    FmContasNiv.Destroy;
  end;
end;

procedure TFmPrincipal.CadastroDeEntidades(Entidade: Integer);
begin
  if DBCheck.CriaFm(TFmEntidade2, FmEntidade2, afmoNegarComAviso) then
  begin
    if Entidade <> 0 then
      FmEntidade2.LocCod(Entidade, Entidade);
    FmEntidade2.ShowModal;
    FmEntidade2.Destroy;
  end;
end;

procedure TFmPrincipal.RetornoCNAB;
begin
  // Compatibilidade
end;

procedure TFmPrincipal.Escolher1Click(Sender: TObject);
begin
  SelecionaImagemdefundo;
end;

procedure TFmPrincipal.EstoquAtual1Click(Sender: TObject);
begin
  TX_Jan.MostraFormTXMovImpEstoque(False);
end;

procedure TFmPrincipal.Padro3Click(Sender: TObject);
begin
  Geral.WriteAppKey('MenuStyle', Application.Title, TMenuItem(Sender).Tag,
    ktInteger, HKEY_LOCAL_MACHINE);
  SkinMenu(TMenuItem(Sender).Tag);
end;

procedure TFmPrincipal.AcoesIniciaisDoAplicativo();
begin
  try
    Screen.Cursor := crHourGlass;
    //
    if DmodG <> nil then
    begin
      DmodG.MyPID_DB_Cria();
      DmodG.ReopenEmpresas(VAR_USUARIO, 0);
      DmodG.AtualizaCotacoes();
      if (VAR_USUARIO = -1) or (VAR_USUARIO = -3) then
      begin
        DmodG.QrFiliaisSP.Database := Dmod.MyDB;
        DmodG.QrFiliaisSP.Close;
        DmodG.QrFiliaisSP.Open;
        if DmodG.QrFiliaisSP.RecordCount > 0 then
          MostraParamsEmp();
      end;
      AtzPed();
      DmNFe_0000.VerificaNFeCabA();
      //
      //  Descanso
      MostraFormDescanso();
      //
      UFixBugs.MostraFixBugs(['']);
      //
      DmkWeb.ConfiguraAlertaWOrdSerApp(TmSuporte, TySuporte, BalloonHint1);
      //
      TmVersao.Enabled := True;
      //
      DmodG.VerificaHorVerao();
      //
      DmkWeb.HistoricoDeAlteracoes(CO_DMKID_APP, CO_VERSAO, dtMostra);
    end;
  finally
    if VAR_USUARIO = -1 then
      Geral.MB_Info(
      '[ ] Entrada PlC' + sLineBreak +
      '[ ] Reprocessamento' + sLineBreak +
      '');
    TmSuporte.Enabled := True;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPrincipal.CriaPages;
begin
  if DBCheck.CriaFm(TFmPages, FmPages, afmoNegarComAviso) then
  begin
    FmPages.ShowModal;
    FmPages.Destroy;
  end;
end;

procedure TFmPrincipal.CriaMinhasEtiquetas;
begin
  if DBCheck.CriaFm(TFmMultiEtiq, FmMultiEtiq, afmoNegarComAviso) then
  begin
    FmMultiEtiq.ShowModal;
    FmMultiEtiq.Destroy;
  end;
end;

end.
