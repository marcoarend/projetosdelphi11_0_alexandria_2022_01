unit EtqGeraIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DB, mySQLDbTables, Grids, ComCtrls,
  DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkGeral, Variants,
  UnDmkProcFunc, UnDmkEnums;

type
  TFmEtqGeraIts = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    QrGraGru1: TmySQLQuery;
    DsGraGru1: TDataSource;
    QrGraGru1Codusu: TIntegerField;
    QrGraGru1Nivel3: TIntegerField;
    QrGraGru1Nivel2: TIntegerField;
    QrGraGru1Nivel1: TIntegerField;
    QrGraGru1Nome: TWideStringField;
    QrGraGru1PrdGrupTip: TIntegerField;
    QrGraGru1GraTamCad: TIntegerField;
    QrGraGru1NOMEGRATAMCAD: TWideStringField;
    QrGraGru1CODUSUGRATAMCAD: TIntegerField;
    QrGraGru1CST_A: TSmallintField;
    QrGraGru1CST_B: TSmallintField;
    QrGraGru1UnidMed: TIntegerField;
    QrGraGru1NCM: TWideStringField;
    QrGraGru1Peso: TFloatField;
    QrGraGru1SIGLAUNIDMED: TWideStringField;
    QrGraGru1CODUSUUNIDMED: TIntegerField;
    QrGraGru1NOMEUNIDMED: TWideStringField;
    PnSeleciona: TPanel;
    EdGraGru1: TdmkEditCB;
    CBGraGru1: TdmkDBLookupComboBox;
    Label1: TLabel;
    PageControl1: TPageControl;
    TabSheet3: TTabSheet;
    GradeA: TStringGrid;
    StaticText2: TStaticText;
    TabSheet5: TTabSheet;
    GradeQ: TStringGrid;
    StaticText1: TStaticText;
    TabSheet6: TTabSheet;
    GradeC: TStringGrid;
    StaticText6: TStaticText;
    TabSheet9: TTabSheet;
    GradeX: TStringGrid;
    QrLoc: TmySQLQuery;
    QrLocUltimo: TIntegerField;
    PB1: TProgressBar;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdGraGru1Change(Sender: TObject);
    procedure EdGraGru1Exit(Sender: TObject);
    procedure GradeQDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeADrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeCDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure BtOKClick(Sender: TObject);
    procedure GradeQDblClick(Sender: TObject);
    procedure GradeQKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
    procedure ReconfiguraGradeQ();
    // Qtde de etiquetas
    function QtdeItemCorTam(GridP, GridC, GridX: TStringGrid): Boolean;
    function QtdeVariosItensCorTam(GridP, GridA, GridC, GridX: TStringGrid): Boolean;
    function ObtemQtde(var Qtde: Integer): Boolean;
  public
    { Public declarations }
  end;

  var
  FmEtqGeraIts: TFmEtqGeraIts;

implementation

uses UnMyObjects, Module, ModProd, MyVCLSkin, GetValor, UMySQLModule, EtqGeraLot,
UnInternalConsts;

{$R *.DFM}

procedure TFmEtqGeraIts.BtOKClick(Sender: TObject);
var
  Col, Row, Qtd, Tot, i: Integer;
  Codigo, GraGruX, Sequencia: Integer;
  Txt1, Txt2: String;
begin
  Screen.Cursor := crHourGlass;
  try
    Codigo := 0;
    Tot := 0;
    for Col := 1 to GradeQ.ColCount -1 do
    begin
      if Geral.IMV(GradeX.Cells[Col,0]) > 0 then
      begin
        for Row := 1 to GradeQ.RowCount - 1 do
        begin
          if Geral.IMV(GradeX.Cells[0,Row]) > 0 then
            Tot := Tot + Geral.IMV(GradeQ.Cells[Col, Row]);
        end;
      end;
    end;
    if Tot = 0 then
    begin
      Screen.Cursor := crDefault;
      Geral.MensagemBox(
        'N�o h� sele��o de etiquetas!', 'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
    PB1.Position := 0;
    PB1.Max := Tot;
    for Col := 1 to GradeQ.ColCount -1 do
    begin
      if Geral.IMV(GradeX.Cells[Col,0]) > 0 then
      begin
        for Row := 1 to GradeQ.RowCount - 1 do
        begin
          if Geral.IMV(GradeX.Cells[0,Row]) > 0 then
          begin
            Qtd := Geral.IMV(GradeQ.Cells[Col, Row]);
            if Qtd > 0 then
            begin
              if Codigo = 0 then
              begin
                Codigo := UMyMod.BuscaEmLivreY_Def('etqgeralot', 'Codigo', stIns, 0);
                if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'etqgeralot', False, [
                'Impresso', 'Nivel1'], ['Codigo'], [0, QrGraGru1Nivel1.Value],
                [Codigo], True) then
                begin
                  Txt1 := 'INSERT INTO etqgeraits SET Codigo=' +
                    dmkPF.FFP(Codigo, 0) + ', GraGruX=';
                end;
              end;
              GraGruX := Geral.IMV(GradeC.Cells[Col,Row]);
              if GraGruX > 0 then
              begin
                Txt2 := dmkPF.FFP(GraGruX, 0) + ', Sequencia=';
                QrLoc.Close;
                QrLoc.Params[0].AsInteger := GraGruX;
                QrLoc.Open;
                Sequencia := QrLocUltimo.Value;
                for i := 1 to Qtd do
                begin
                  PB1.Position := PB1.Position + 1;
                  Update;
                  Application.ProcessMessages;
                  Sequencia := Sequencia + 1;
                  Dmod.MyDB.Execute(Txt1 + Txt2 + dmkPF.FFP(Sequencia, 0));
                end;
              end;
            end;
          end;
        end;
      end;
    end;
    FmEtqGeraLot.ReopenLastTenLot();
  finally
    Screen.Cursor := crDefault;
  end;
  Close;
end;

procedure TFmEtqGeraIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEtqGeraIts.EdGraGru1Change(Sender: TObject);
begin
  if EdGraGru1.Focused = False then
    ReconfiguraGradeQ();
end;

procedure TFmEtqGeraIts.EdGraGru1Exit(Sender: TObject);
begin
  ReconfiguraGradeQ();
end;

procedure TFmEtqGeraIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEtqGeraIts.FormCreate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  GradeA.ColWidths[0] := 128;
  GradeC.ColWidths[0] := 128;
  GradeX.ColWidths[0] := 128;
  GradeQ.ColWidths[0] := 128;
  QrGraGru1.Open;
end;

procedure TFmEtqGeraIts.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmEtqGeraIts.GradeADrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  Ativo: Integer;
begin
  if (ACol = 0) or (ARow = 0) then
  begin
    with GradeA.Canvas do
    begin
      if ACol = 0 then
        MyObjects.DesenhaTextoEmStringGrid(GradeA, Rect, clBlack,
          PnSeleciona.Color(*FmPrincipal.sd1.Colors[csButtonFace]*), taLeftJustify,
          GradeA.Cells[Acol, ARow], 0, 0, False)
      else
        MyObjects.DesenhaTextoEmStringGrid(GradeA, Rect, clBlack,
          PnSeleciona.Color(*FmPrincipal.sd1.Colors[csButtonFace]*), taCenter,
          GradeA.Cells[Acol, ARow], 0, 0, False);
    end;
  end else begin
    if GradeA.Cells[Acol, ARow] = '' then
      Ativo := 0
    else
      Ativo := Geral.IMV(GradeA.Cells[ACol, ARow]) + 1;
    //
    MeuVCLSkin.DrawGrid4(GradeA, Rect, 0, Ativo);
  end;
end;

procedure TFmEtqGeraIts.GradeCDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  Cor: Integer;
begin
  if (ACol = 0) or (ARow = 0) then
    Cor := PnSeleciona.Color
  else
    Cor := clWindow;
  if ACol = 0 then
    MyObjects.DesenhaTextoEmStringGrid(GradeC, Rect, clBlack,
      Cor(*FmPrincipal.sd1.Colors[csButtonFace]*), taLeftJustify,
      GradeC.Cells[Acol, ARow], 0, 0, False)
  else
    MyObjects.DesenhaTextoEmStringGrid(GradeC, Rect, clBlack,
      Cor(*FmPrincipal.sd1.Colors[csButtonFace]*), taCenter,
      GradeC.Cells[Acol, ARow], 0, 0, False);
end;

procedure TFmEtqGeraIts.GradeQDblClick(Sender: TObject);
begin
  if (GradeQ.Col = 0) or (GradeQ.Row = 0) then
    QtdeVariosItensCorTam(GradeQ, GradeA, GradeC, GradeX)
  else
    QtdeItemCorTam(GradeQ, GradeC, GradeX);
end;

procedure TFmEtqGeraIts.GradeQDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  Cor: Integer;
  Texto: String;
begin
  if (ACol = 0) or (ARow = 0) then
    Cor := PnSeleciona.Color
  else if GradeA.Cells[ACol, ARow] <> '' then
  begin
    Texto := GradeQ.Cells[ACol, ARow];
    Cor := clWindow;
  end else begin
    if (GradeC.Cells[ACol,ARow] = '')
    and (GradeQ.Cells[ACol,ARow] <> '')
    then  GradeQ.Cells[ACol,ARow] := '';
    Texto := '';
    Cor := clMenu;
  end;
  //


  if (ACol = 0) or (ARow = 0) then
    MyObjects.DesenhaTextoEmStringGrid(GradeQ, Rect, clBlack,
      Cor(*FmPrincipal.sd1.Colors[csButtonFace]*), taLeftJustify,
      GradeQ.Cells[Acol, ARow], 1, 1, False)
  else
    MyObjects.DesenhaTextoEmStringGrid(GradeQ, Rect, clBlack,
      Cor(*FmPrincipal.sd1.Colors[csButtonFace]*), taRightJustify,
      (*GradeQ.Cells[AcolARow]*)Texto, 1, 1, False);
end;

procedure TFmEtqGeraIts.GradeQKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  MyObjects.PulaCelulaGradeInput(GradeQ, GradeC, Key, Shift);
end;

procedure TFmEtqGeraIts.ReconfiguraGradeQ;
begin
  DmProd.ConfigGrades3(QrGraGru1GraTamCad.Value, QrGraGru1Nivel1.Value,
    GradeA, GradeX, GradeC, GradeQ);
end;

function TFmEtqGeraIts.ObtemQtde(var Qtde: Integer): Boolean;
var
  ResVar: Variant;
begin
  Qtde := 0;
  Result := False;
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfInteger,
  0, 0, 0, '', '', True, 'Etiquetas', 'Informe a quantidade de etiquetas: ',
  0, ResVar) then
  begin
    Qtde := Geral.IMV(ResVar);
    Result := True;
  end;
end;

function TFmEtqGeraIts.QtdeItemCorTam(GridP, GridC, GridX: TStringGrid): Boolean;
var
  Qtde, c, l, GraGruX, Coluna, Linha: Integer;
begin
  Result := False;
  Screen.Cursor := crHourGlass;
  Coluna := GridP.Col;
  Linha  := GridP.Row;
  c := Geral.IMV(GridX.Cells[Coluna, 0]);
  l := Geral.IMV(GridX.Cells[0, Linha]);
  GraGruX := Geral.IMV(GridC.Cells[Coluna, Linha]);
  if c = 0 then
  begin
    Geral.MensagemBox('Tamanho n�o definido!', 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  if l = 0 then
  begin
    Geral.MensagemBox('Cor n�o definida!', 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  if GraGruX = 0 then
  begin
    Geral.MensagemBox('O item nunca foi ativado!', 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (c = 0) or (l = 0) or (GraGruX = 0) then Exit;
  //
  //Nivel1 := QrGraGru1Nivel1.Value;
  Qtde  := Geral.IMV(GridP.Cells[Coluna, Linha]);
  if ObtemQtde(Qtde) then
    GridP.Cells[Coluna, Linha] := FormatFloat('0', Qtde);
  Result := True;
  Screen.Cursor := crDefault;
end;

function TFmEtqGeraIts.QtdeVariosItensCorTam(GridP, GridA, GridC, GridX:
TStringGrid): Boolean;
var
  Coluna, Linha, c, l, Ativos, ColI, ColF, RowI, RowF,
  CountC, CountL, GraGruX, Qtde: Integer;
  QtdeTxt: String;
begin
  Result := False;
  Screen.Cursor := crHourGlass;
  Ativos := 0;
  RowI := 0;
  RowF := 0;
  ColI := 0;
  ColF := 0;
  Coluna := GridP.Col;
  Linha  := GridP.Row;
  if (Coluna = 0) and (Linha = 0) then
  begin
    ColI := 1;
    ColF := GridP.ColCount - 1;
    RowI := 1;
    RowF := GridP.RowCount - 1;
  end else if Coluna = 0 then
  begin
    ColI := 1;
    ColF := GridP.ColCount - 1;
    RowI := Linha;
    RowF := Linha;
  end else if Linha = 0 then
  begin
    ColI := Coluna;
    ColF := Coluna;
    RowI := 1;
    RowF := GridP.RowCount - 1;
  end;
  //
  CountC := 0;
  CountL := 0;
  for c := ColI to ColF do
    for l := RowI to RowF do
  begin
    CountC := CountC + Geral.IMV(GridX.Cells[c, 0]);
    CountL := CountL + Geral.IMV(GridX.Cells[0, l]);
  end;
  if CountC = 0 then
  begin
    Geral.MensagemBox('Tamanho n�o definido!', 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  if CountL = 0 then
  begin
    Geral.MensagemBox('Cor n�o definida!', 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
  //Nivel1 := QrGraGru1Nivel1.Value;
  Qtde   := Geral.IMV(GridP.Cells[ColI, RowI]);
  for c := ColI to ColF do
    for l := RowI to RowF do
      Ativos := Ativos + Geral.IMV(GridA.Cells[c, l]);
  if Ativos = 0 then
  begin
    Geral.MensagemBox('N�o h� nenhum item com c�digo na sele��o ' +
    'para que se possa incluir / alterar o pre�o!', 'Aviso',
    MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
  if ObtemQtde(Qtde) then
  begin
    QtdeTxt := Geral.FFT(Qtde, 0, siPositivo);
    for c := ColI to ColF do
      for l := RowI to RowF do
    begin
      GraGruX := Geral.IMV(GridC.Cells[c, l]);
      //
      if GraGruX > 0 then
        GradeQ.Cells[c,l] := QtdeTxt;
    end;
    Result := True;
  end;
  Screen.Cursor := crDefault;
end;

end.
