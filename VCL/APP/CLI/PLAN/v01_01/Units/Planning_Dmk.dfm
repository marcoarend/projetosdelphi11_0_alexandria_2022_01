object FmPlanning_Dmk: TFmPlanning_Dmk
  Left = 399
  Top = 261
  Caption = 'Planning'
  ClientHeight = 222
  ClientWidth = 437
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnHide = FormHide
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 437
    Height = 93
    Align = alClient
    Caption = ' Defini'#231#245'es do perfil: '
    TabOrder = 0
    object Label3: TLabel
      Left = 14
      Top = 24
      Width = 43
      Height = 13
      Caption = 'Terminal:'
    end
    object Label1: TLabel
      Left = 84
      Top = 24
      Width = 29
      Height = 13
      Caption = 'Login:'
    end
    object Label2: TLabel
      Left = 229
      Top = 24
      Width = 34
      Height = 13
      Caption = 'Senha:'
    end
    object Label4: TLabel
      Left = 375
      Top = 24
      Width = 44
      Height = 13
      Caption = 'Empresa:'
    end
    object LaSenhas: TLabel
      Left = 84
      Top = 64
      Width = 80
      Height = 13
      Cursor = crHandPoint
      Caption = 'Gerenciar Senha'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      OnClick = LaSenhasClick
      OnMouseEnter = LaSenhasMouseEnter
      OnMouseLeave = LaSenhasMouseLeave
    end
    object LaConexao: TLabel
      Left = 326
      Top = 64
      Width = 96
      Height = 13
      Cursor = crHandPoint
      Caption = 'Gerenciar Conex'#245'es'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      OnClick = LaConexaoClick
      OnMouseEnter = LaConexaoMouseEnter
      OnMouseLeave = LaConexaoMouseLeave
    end
    object EdTerminal: TEdit
      Left = 14
      Top = 39
      Width = 64
      Height = 21
      TabStop = False
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 0
      OnKeyDown = EdLoginKeyDown
    end
    object EdLogin: TEdit
      Left = 84
      Top = 39
      Width = 138
      Height = 22
      Font.Charset = SYMBOL_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      PasswordChar = 'l'
      TabOrder = 1
      OnKeyDown = EdLoginKeyDown
    end
    object EdSenha: TEdit
      Left = 229
      Top = 39
      Width = 137
      Height = 22
      Font.Charset = SYMBOL_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      PasswordChar = 'l'
      TabOrder = 2
      OnExit = EdSenhaExit
      OnKeyDown = EdSenhaKeyDown
    end
    object EdEmpresa: TEdit
      Left = 375
      Top = 39
      Width = 46
      Height = 21
      AutoSize = False
      Font.Charset = SYMBOL_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Wingdings'
      Font.Style = []
      ParentFont = False
      PasswordChar = 'l'
      TabOrder = 3
      OnKeyDown = EdSenhaKeyDown
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 93
    Width = 437
    Height = 60
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 14
      Width = 433
      Height = 44
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object ProgressBar1: TProgressBar
        Left = 12
        Top = 24
        Width = 406
        Height = 16
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 153
    Width = 437
    Height = 69
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 294
      Top = 14
      Width = 141
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtDesiste: TBitBtn
        Tag = 15
        Left = 12
        Top = 3
        Width = 118
        Height = 39
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtDesisteClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 14
      Width = 292
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtEntra: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 118
        Height = 39
        Caption = '&Confirma'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtEntraClick
      end
    end
  end
end
