unit EtqPrinCmd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DB, mySQLDbTables, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkLabel, Mask, dmkDBEdit, dmkGeral,
  Variants, dmkRadioGroup, ComCtrls, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkEnums, ABSMain;

type
  TFmEtqPrinCmd = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    LaTipo: TdmkLabel;
    CkContinuar: TCheckBox;
    BitBtn1: TBitBtn;
    Panel3: TPanel;
    GroupBox3: TGroupBox;
    EdConfig: TdmkEdit;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    BitBtn3: TBitBtn;
    BtImprime: TBitBtn;
    CkLinhaALinha: TCheckBox;
    CkDOS: TCheckBox;
    Panel9: TPanel;
    DBEdit12: TDBEdit;
    Label18: TLabel;
    DBEdit13: TDBEdit;
    DBEdit11: TDBEdit;
    Label17: TLabel;
    DBEdit8: TDBEdit;
    DBEdit7: TDBEdit;
    Label13: TLabel;
    DBEdit14: TDBEdit;
    Label19: TLabel;
    DBEdit15: TDBEdit;
    Label20: TLabel;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    Label21: TLabel;
    DBEdit18: TDBEdit;
    DBEdit19: TDBEdit;
    Label22: TLabel;
    GroupBox2: TGroupBox;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    GroupBox7: TGroupBox;
    DBCheckBox3: TDBCheckBox;
    DBCheckBox4: TDBCheckBox;
    GroupBox8: TGroupBox;
    GBMedidasLabel: TGroupBox;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label24: TLabel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    GroupBox10: TGroupBox;
    LaFormLargura: TLabel;
    Label23: TLabel;
    Label3: TLabel;
    DBEdit20: TDBEdit;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Panel10: TPanel;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label7: TLabel;
    EdOrdem: TdmkEdit;
    EdControle: TdmkEdit;
    RGTipo: TdmkRadioGroup;
    EdColuna: TdmkEdit;
    CkTodoLabel: TCheckBox;
    DBRadioGroup1: TDBRadioGroup;
    Panel6: TPanel;
    MeResult: TMemo;
    Query: TABSQuery;
    QueryColuna: TIntegerField;
    QueryLinha: TIntegerField;
    QueryTexto: TWideStringField;
    QueryAtivo: TSmallintField;
    QueryTxtBar: TWideStringField;
    DataSource1: TDataSource;
    Panel4: TPanel;
    MeLinesCmd: TMemo;
    QueryDataHora: TFloatField;
    TCLinhas: TTabControl;
    DBGTxt: TDBGrid;
    QueryAutoSeq: TIntegerField;
    Quer2: TABSQuery;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    StringField1: TWideStringField;
    SmallintField1: TSmallintField;
    StringField2: TWideStringField;
    ListBox1: TListBox;
    QrAllLin: TmySQLQuery;
    QrAllLinCodigo: TIntegerField;
    QrAllLinTipo: TSmallintField;
    QrAllLinOrdem: TIntegerField;
    QrAllLinControle: TIntegerField;
    QrAllLinConfig: TWideStringField;
    QrAllLinColuna: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure RGTipoClick(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CkDOSClick(Sender: TObject);
    procedure DBRadioGroup1Change(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure TCLinhasChange(Sender: TObject);
    procedure QueryBeforePost(DataSet: TDataSet);
    procedure DBGTxtKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
    function OrdemInvalida(Tipo: Integer): Boolean;
    procedure ImprimeViaWin();
    procedure ImprimeViaDOS();
  public
    { Public declarations }
    FAutoSeq: Integer;
    procedure ReopenQuery(Linha: Integer);
    procedure ReordenaQuery(Linha: Integer);
  end;

  var
  FmEtqPrinCmd: TFmEtqPrinCmd;

implementation

uses UnMyObjects, Module, EtqPrinCad, UMySQLModule, EtqPrinVar, MyDBCheck, UnInternalConsts,
  EPC_Argox_PPLA, BarComands;

{$R *.DFM}

procedure TFmEtqPrinCmd.BitBtn1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEtqPrinVar, FmEtqPrinVar, afmoNegarComAviso) then
  begin
    FmEtqPrinVar.CGGrupo.Value := FmEtqPrinCad.QrEtqPrinCadTypeUso.Value + 1;
    FmEtqPrinVar.FTexto := EdConfig.Texto;
    FmEtqPrinVar.FSelStart := EdConfig.SelStart;
    FmEtqPrinVar.ShowModal;
    EdConfig.Text := FmEtqPrinVar.FTexto;
    FmEtqPrinVar.Destroy;
  end;
end;

procedure TFmEtqPrinCmd.BitBtn3Click(Sender: TObject);
begin
  case FmEtqPrinCad.QrEtqPrinCadLinguagem.Value of
    0: Geral.MensagemBox('Linguagem n�o definida na configura��o!',
       'Aviso', MB_OK+MB_ICONWARNING);
    1:
    begin
      if DBCheck.CriaFm(TFmEPC_Argox_PPLA, FmEPC_Argox_PPLA, afmoNegarComAviso) then
      begin
        case FmEtqPrinCad.QrEtqPrinCadMeasure.Value of
          0:
          begin
            FmEPC_Argox_PPLA.LaMedida_Txt.Caption := 'cm: ';
            FmEPC_Argox_PPLA.LaMedida_Bar.Caption := 'cm: ';
          end;
          1:
          begin
            FmEPC_Argox_PPLA.LaMedida_Txt.Caption := 'pol.: ';
            FmEPC_Argox_PPLA.LaMedida_Bar.Caption := 'pol.: ';
          end;
        end;
        FmEPC_Argox_PPLA.EdLinha_Bar.ValueVariant := Query.RecordCount + 1;
        FmEPC_Argox_PPLA.EdLinha_Txt.ValueVariant := Query.RecordCount + 1;
        FmEPC_Argox_PPLA.ShowModal;
        FmEPC_Argox_PPLA.Destroy;
      end;
    end;
    else Geral.MensagemBox('Linguagem n�o implemetada! AVISE A DERMATEK',
       'ERRO', MB_OK+MB_ICONERROR);
  end;
end;

procedure TFmEtqPrinCmd.BtImprimeClick(Sender: TObject);
var
  i: Integer;
  pm: TPrnMeasure;
begin
  case FmEtqPrinCad.QrEtqPrinCadMeasure.Value of
    0: pm := pmMetric;
    1: pm := pmInches;
    else pm := pmNone;
  end;
  MeResult.Lines.Clear;
  if FmEtqPrinCad.QrEtqPrinCadDefaultIni.Value = 1 then
    UBarComands.ComandosAutomaticos(bctIni, pplArgoxA, pm, MeLinesCmd)
  else begin
    FmEtqPrinCad.QrEtqPrinCmd_0.First;
    while not FmEtqPrinCad.QrEtqPrinCmd_0.Eof do
    begin
      MeResult.Lines.Add(FmEtqPrinCad.QrEtqPrinCmd_0Config.Value);
         //
      FmEtqPrinCad.QrEtqPrinCmd_0.Next;
    end;
  end;
  //
  if CkTodoLabel.Checked then
  begin
    FmEtqPrinCad.QrEtqPrinCmd_1.First;
    while not FmEtqPrinCad.QrEtqPrinCmd_1.Eof do
    begin
      if FmEtqPrinCad.QrEtqPrinCmd_1Ordem.Value <> EdOrdem.ValueVariant then
        MeResult.Lines.Add(FmEtqPrinCad.QrEtqPrinCmd_1Config.Value);
         //
      FmEtqPrinCad.QrEtqPrinCmd_1.Next;
    end;
  end;
  for i := 0 to MeLinesCmd.Lines.Count - 1 do
    MeResult.Lines.Add(MeLinesCmd.Lines[i]);
  //
  if FmEtqPrinCad.QrEtqPrinCadDefaultFim.Value = 1 then
    UBarComands.ComandosAutomaticos(bctFim, pplArgoxA, pm, MeLinesCmd)
  else begin
    FmEtqPrinCad.QrEtqPrinCmd_2.First;
    while not FmEtqPrinCad.QrEtqPrinCmd_2.Eof do
    begin
      MeResult.Lines.Add(FmEtqPrinCad.QrEtqPrinCmd_2Config.Value);
         //
      FmEtqPrinCad.QrEtqPrinCmd_2.Next;
    end;
  end;
  //
  if CkDOS.Checked then
    ImprimeViaDOS()
  else
    ImprimeViaWin();
end;

procedure TFmEtqPrinCmd.BtOKClick(Sender: TObject);
var
  i, Controle, Codigo, Tipo, Ordem, Coluna: Integer;
  Config: String;
begin
  if MyObjects.FIC(RGTipo.ItemIndex < 0, RGTipo,
    'Informe o tipo de sequencial de instru��o') then Exit;
  if OrdemInvalida(RGTipo.ItemIndex) then Exit;
  Screen.Cursor := crHourGlass;
  try
  {if RGTipo.ItemIndex <> 1 then
  begin
    Controle := UMyMod.BuscaEmLivreY_Def('EtqPrinCmd', 'Controle', LaTipo.SQLType,
      EdControle.ValueVariant);
    //
    if UMyMod.ExecSQLInsUpdFm(FmEtqPrinCmd, LaTipo.SQLType, 'EtqPrinCmd', Controle,
    Dmod.QrUpd) then
    begin
      FmEtqPrinCad.ReordenaLinhas(RGTipo.ItemIndex, Controle, EdOrdem.ValueVariant);
      (* N�o precisa! J� � feito no "ReordenaLinhas"
      case RGTipo.ItemIndex of
        0: FmEtqPrinCad.ReopenEtqPrinCmd_0(Controle);
        1: FmEtqPrinCad.ReopenEtqPrinCmd_1(Controle);
        2: FmEtqPrinCad.ReopenEtqPrinCmd_2(Controle);
      end;
      *)
      //
      if CkContinuar.Checked then
      begin
        Latipo.SQLType           := stIns;
        EdControle.ValueVariant  := 0;
        EdOrdem.ValueVariant     := EdOrdem.ValueVariant + 1;
        EdConfig.ValueVariant    := '';
        EdOrdem.SetFocus;
      end else Close;
    end;
  end else begin}
    // Tipo 1 = Dados
    QrAllLin.Close;
    QrAllLin.Params[00].AsInteger := FmEtqPrinCad.QrEtqPrinCadCodigo.Value;
    QrAllLin.Params[01].AsInteger := RGTipo.ItemIndex;
    QrAllLin.Open;
    QrAllLin.First;
    while not QrAllLin.Eof do
    begin
      UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'EtqPrinCmd', QrAllLinControle.Value);
      UMyMod.ExcluiRegistroInt1('', 'EtqPrinCmd', 'Controle', QrAllLinControle.Value, Dmod.MyDB);
      //
      QrAllLin.Next;
    end;
    for i := 0 to TCLinhas.Tabs.Count - 1 do
    begin
      TCLinhas.TabIndex := i;
      ReopenQuery(0);
      //
      Codigo   := FmEtqPrinCad.QrEtqPrinCadCodigo.Value;
      Tipo     := RGTipo.ItemIndex;
      Query.First;
      while not Query.Eof do
      begin
        Ordem    := QueryAutoSeq.Value;
        Coluna   := QueryColuna.Value;
        Config   := QueryTexto.Value;
        Controle := UMyMod.BuscaEmLivreY_Def('etqprincmd', 'Controle', stIns, 0);
        UMyMod.SQLInsUpd(Dmod.QrUpd, LaTipo.SQLType, 'etqprincmd', False, [
        'Codigo', 'Tipo', 'Ordem', 'Config', 'Coluna'], ['Controle'], [
        Codigo, Tipo, Ordem, Config, Coluna], [Controle], True);
        //
        Query.Next;
      end;
    end;
  //end;
  finally
    Screen.Cursor := crDefault;
  end;
  case RGTipo.ItemIndex of
    0: FmEtqPrinCad.ReopenPrinCmd_0(0);
    1: FmEtqPrinCad.ReopenPrinCmd_1(0);
    2: FmEtqPrinCad.ReopenPrinCmd_2(0);
  end;
  Close;
end;

procedure TFmEtqPrinCmd.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEtqPrinCmd.CkDOSClick(Sender: TObject);
begin
  CkLinhaALinha.Enabled := not CkDOS.Checked;
end;

function EnumWindowsProc(Wnd : HWnd;Form : TFmEtqPrinCmd) : Boolean; Export;
          {$ifdef Win32} StdCall; {$endif}
var
  Buffer : Array[0..99] of char;
begin
  GetWindowText(Wnd,Buffer,100);
  if StrLen(Buffer) <> 0 then
    Form.ListBox1.Items.Add(StrPas(Buffer));
  Result := True;
end;

procedure TFmEtqPrinCmd.DBGTxtKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  h: THandle;
  n: Integer;
begin
(*
void __fastcall TForm1::StringGrid1KeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
if(Shift.Contains(ssShift))
{
  HWND h = FindWindowEx(StringGrid1->Handle,NULL,"TInplaceEdit",NULL);
  if(h)
  {
    char selstr[100];
    int n = SendMessage(h,WM_GETTEXTLENGTH,0L,0L);
    if(n != -1)
    {
      n = SendMessage(h,WM_GETTEXT,n+1,(LPARAM)selstr);
      if(n != -1)
      {
        selstr[n] = '\0';
        DWORD i,j;
        SendMessage(h,EM_GETSEL,(WPARAM)&i,(LPARAM)&j);
        ShowMessage(AnsiString(selstr).SubString(i+1,j-i));
      }
    }
  }
}
*)
  if Key=VK_F4 then
  begin
    EnumWindows(@EnumWindowsProc,Integer(Self));
    h := FindWindowEx(DBGTxt.Handle, 0, 'TInplaceEdit', nil);
    if h <> 0 then
    begin
      n := SendMessage(h, WM_GETTEXTLENGTH, 0, 0);
      if n <> -1 then
      begin
        //
        ShowMessage(IntToStr(n));
      end;
    end;
    {
    if DBGTxt.EditorMode then
    begin
      if DBCheck.CriaFm(TFmEtqPrinVar, FmEtqPrinVar, afmoNegarComAviso) then
      begin
        FmEtqPrinVar.CGGrupo.Value := FmEtqPrinCad.QrEtqPrinCadTypeUso.Value + 1;
        FmEtqPrinVar.FTexto := QueryTexto.Value;
        FmEtqPrinVar.FSelStart := Length(QueryTexto.Value);
        FmEtqPrinVar.ShowModal;
        QueryTexto.Value := FmEtqPrinVar.FTexto;
        FmEtqPrinVar.Destroy;
      end;
    end;
    }
  end;
end;

procedure TFmEtqPrinCmd.DBRadioGroup1Change(Sender: TObject);
begin
  case FmEtqPrinCad.QrEtqPrinCadMeasure.Value of
    0:
    begin
      GBMedidasLabel.Caption := 'Label (em cent�mentros): ';
      LaFormLargura.Caption := 'Largura (cm):';
    end;
    1:
    begin
      GBMedidasLabel.Caption := 'Label (em polegadas): ';
      LaFormLargura.Caption := 'Largura (pol):';
    end;
  end;
end;

procedure TFmEtqPrinCmd.FormActivate(Sender: TObject);
begin
  if LaTipo.SQLType <> stIns then
    RGTipo.Enabled := False;
  MyObjects.CorIniComponente();
end;

procedure TFmEtqPrinCmd.FormCreate(Sender: TObject);
begin
  MeResult.Lines.Clear;
  //
  Query.SQL.Add('DROP TABLE E_P_C;          ');
  Query.SQL.Add('CREATE TABLE E_P_C        (');
  Query.SQL.Add('  Coluna  integer         ,');
  Query.SQL.Add('  Linha   integer         ,');
  Query.SQL.Add('  Texto   varchar(255)    ,');
  Query.SQL.Add('  TxtBar  varchar(3)      ,');
  Query.SQL.Add('  DataHora float          ,');
  Query.SQL.Add('  AutoSeq integer         ,');
  Query.SQL.Add('  Ativo smallint          ');
  //Query.SQL.Add('PRIMARY KEY Idx1 (AutoSeq) ');
  Query.SQL.Add(');                         ');
  //
  Query.SQL.Add('SELECT * FROM e_p_c;       ');
  Query.Open;
end;

procedure TFmEtqPrinCmd.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

function TFmEtqPrinCmd.OrdemInvalida(Tipo: Integer): Boolean;
var
  Max, o: Integer;
begin
  Result := False;
  case Tipo of
    0: Max := FmEtqPrinCad.QrEtqPrinCmd_0.RecordCount;
    1: Max := FmEtqPrinCad.QrEtqPrinCmd_1.RecordCount;
    2: Max := FmEtqPrinCad.QrEtqPrinCmd_2.RecordCount;
    else Max := 0;
  end;
  if LaTipo.SQLType = stIns then Max := Max + 1;
  //
  o := EdOrdem.ValueVariant;
  if o > Max then
  begin
    Geral.MensagemBox('A ordem informada (' + IntToStr(o) +
    ') � maior que a maior ordem poss�vel (' + IntToStr(Max) + ')!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Result := True;
    Screen.Cursor := crDefault;
    Exit;
  end;
end;

procedure TFmEtqPrinCmd.QueryBeforePost(DataSet: TDataSet);
begin
  if QueryAutoSeq.Value = 0 then
  begin
    inc(FAutoSeq, 1);
    QueryAutoSeq.Value := FAutoSeq;
    QueryDataHora.Value := Now();
  end;
  if QueryColuna.Value = 0 then
    QueryColuna.Value := TCLinhas.TabIndex + 1;
  if QueryLinha.Value = 0 then
    QueryLinha.Value := Query.RecordCount + 1;
end;

procedure TFmEtqPrinCmd.ReopenQuery(Linha: Integer);
begin
  Query.Close;
  Query.SQL.Clear;
  Query.SQL.Add('SELECT * FROM e_p_c ');
  Query.SQL.Add('WHERE Coluna=' + dmkPF.FFP(TCLinhas.TabIndex + 1, 0));
  Query.SQL.Add('ORDER BY Linha');
  Query.Open;
  Query.Locate('Linha', Linha, []);
end;

procedure TFmEtqPrinCmd.ReordenaQuery(Linha: Integer);
var
  Col, Row: Integer;
begin
  Col := -1000;
  Row := 0;
  Query.Close;
  Query.SQL.Clear;
  Query.SQL.Add('SELECT * FROM e_p_c ');
  Query.SQL.Add('ORDER BY Coluna, Linha, DataHora');
  Query.Open;
  //
  Quer2.Close;
  Quer2.SQL.Clear;
  Quer2.SQL.Add('UPDATE e_p_c ');
  Quer2.SQL.Add('SET Linha=:P0 WHERE AutoSeq=:P1');
  while not Query.Eof do
  begin
    if QueryColuna.Value <> Col then
    begin
      Col := QueryColuna.Value;
      Row := 1;
    end else
      Row := Row + 1;
    //
    {
    Query.Edit;
    QueryLinha.Value := Row;
    Query.Post;
    }
    Quer2.Params[00].AsInteger := Row;
    Quer2.Params[01].AsInteger := QueryAutoSeq.Value;
    Quer2.ExecSQL;
    //
    Query.Next;
  end;
  ReopenQuery(Linha);
end;

procedure TFmEtqPrinCmd.RGTipoClick(Sender: TObject);
begin
  //PageControl1.Visible := RGTipo.ItemIndex = 1;
  if LaTipo.SQLType = stIns then
  begin
    case RGTipo.ItemIndex of
      0: EdOrdem.ValueVariant := FmEtqPrinCad.QrEtqPrinCmd_0.RecNo + 1;
      1: EdOrdem.ValueVariant := FmEtqPrinCad.QrEtqPrinCmd_1.RecNo + 1;
      2: EdOrdem.ValueVariant := FmEtqPrinCad.QrEtqPrinCmd_2.RecNo + 1;
    end;
  end;
end;

procedure TFmEtqPrinCmd.TCLinhasChange(Sender: TObject);
var
  Linha: Integer;
begin
  if Query.State <> dsInactive then
    Linha := QueryLinha.Value
  else
    Linha := 0;
  ReopenQuery(Linha);
end;

procedure TFmEtqPrinCmd.ImprimeViaDOS();
const
  Arq = 'C:\Dermatek\Etiketa.txt';
var
  Porta: String;
begin
  //via dos
  MLAGeral.ExportaMemoToFile(MeResult, Arq, True, True, False);
  Porta := FmEtqPrinCad.QrEtqPrinCadNOMEPORTAIMP.Value +
    dmkPF.FFP(FmEtqPrinCad.QrEtqPrinCadPortaIdx.Value, 0);
  MLAGeral.ExecutaCmd('type ' + Arq + ' > ' + Porta, MeResult, False);
end;

procedure TFmEtqPrinCmd.ImprimeViaWin();
var
  F: TextFile;
  i: Integer;
  Porta: String;
begin
  // Via windows
  Porta := FmEtqPrinCad.QrEtqPrinCadNOMEPORTAIMP.Value +
    dmkPF.FFP(FmEtqPrinCad.QrEtqPrinCadPortaIdx.Value, 0);
  AssignFile(F, Porta);
  ReWrite(F);
  if CkLinhaAlinha.Checked then
  begin
    for i := 0 to MeResult.Lines.Count - 1 do
      Writeln(F, MeLinesCmd.Lines[i] + Char(13));
  end else Writeln(F, MeResult.Text);
  CloseFile(F);
end;

end.

