unit EtqGeraLot;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, dmkGeral, dmkEdit, Grids, DBGrids,
  DB, mySQLDbTables, dmkDBGrid, Menus, ComCtrls, Mask, DBCtrls, UnDmkEnums;

type
  TFmEtqGeraLot = class(TForm)
    PainelConfirma: TPanel;
    BtLotes: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Panel3: TPanel;
    Panel4: TPanel;
    DBGCab: TdmkDBGrid;
    StaticText1: TStaticText;
    Label1: TLabel;
    EdLoteIni: TdmkEdit;
    EdLoteQtd: TdmkEdit;
    Label2: TLabel;
    QrEtqGeraLot: TmySQLQuery;
    DsEtqGeraLot: TDataSource;
    QrEtqGeraLotImpresso: TSmallintField;
    QrEtqGeraLotCodigo: TIntegerField;
    QrEtqGeraLotDataCad: TDateField;
    PMLotes: TPopupMenu;
    Crianovolote1: TMenuItem;
    Marcalote1: TMenuItem;
    QrEtqGeraLotNivel1: TIntegerField;
    QrEtqGeraLotGraTamCad: TIntegerField;
    BitBtn1: TBitBtn;
    QrEtqGeraLotImprimir: TSmallintField;
    Panel5: TPanel;
    PageControl1: TPageControl;
    TabSheet5: TTabSheet;
    GradeQ: TStringGrid;
    TabSheet6: TTabSheet;
    GradeC: TStringGrid;
    TabSheet3: TTabSheet;
    GradeA: TStringGrid;
    TabSheet9: TTabSheet;
    GradeX: TStringGrid;
    Panel6: TPanel;
    DBEdit1: TDBEdit;
    QrEtqGeraLotCodUsu: TIntegerField;
    QrEtqGeraLotNome: TWideStringField;
    DBEdit2: TDBEdit;
    CkNaoImpresso: TCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdLoteIniChange(Sender: TObject);
    procedure EdLoteQtdChange(Sender: TObject);
    procedure BtLotesClick(Sender: TObject);
    procedure Crianovolote1Click(Sender: TObject);
    procedure QrEtqGeraLotAfterScroll(DataSet: TDataSet);
    procedure GradeQDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeCDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeADrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure DBGCabCellClick(Column: TColumn);
    procedure DBGCabEnter(Sender: TObject);
    procedure DBGCabExit(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure CkNaoImpressoClick(Sender: TObject);
  private
    { Private declarations }
    FNaoReabreLot: Boolean;
    procedure ReopenEtqGeraLot(Codigo: Integer);
  public
    { Public declarations }
    procedure ReopenLastTenLot();
  end;

  var
  FmEtqGeraLot: TFmEtqGeraLot;

implementation

uses UnMyObjects, Module, EtqGeraIts, MyDBCheck, MyVCLSkin, ModProd, EtqGeraImp,
UnInternalConsts;

{$R *.DFM}

procedure TFmEtqGeraLot.BitBtn1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEtqGeraImp, FmEtqGeraImp, afmoNegarComAviso) then
  begin
    FmEtqGeraImp.ShowModal;
    FmEtqGeraImp.Destroy;
  end;
end;

procedure TFmEtqGeraLot.BtLotesClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMLotes, BtLotes);
end;

procedure TFmEtqGeraLot.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEtqGeraLot.CkNaoImpressoClick(Sender: TObject);
begin
  ReopenEtqGeraLot(0);
end;

procedure TFmEtqGeraLot.Crianovolote1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEtqGeraIts, FmEtqGeraIts, afmoNegarComAviso) then
  begin
    FmEtqGeraIts.ShowModal;
    FmEtqGeraIts.Destroy;
  end;
end;

procedure TFmEtqGeraLot.DBGCabCellClick(Column: TColumn);
var
  Status, Lote: Integer;
begin
  if Column.FieldName = 'Imprimir' then
  begin
    Screen.Cursor := crHourGlass;
    try
      Status := QrEtqGeraLotImprimir.Value;
      if Status = 0 then Status := 1 else Status := 0;
      Lote := QrEtqGeraLotCodigo.Value;
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE etqgeralot SET Imprimir=:P0');
      Dmod.QrUpd.SQL.Add('WHERE Codigo=:P1');
      Dmod.QrUpd.Params[00].AsInteger := Status;
      Dmod.QrUpd.Params[01].AsInteger := Lote;
      Dmod.QrUpd.ExecSQL;
      //
      ReopenEtqGeraLot(Lote);
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmEtqGeraLot.DBGCabEnter(Sender: TObject);
begin
  DBGCab.Options := DBGCab.Options - [dgRowSelect];
end;

procedure TFmEtqGeraLot.DBGCabExit(Sender: TObject);
begin
  DBGCab.Options := DBGCab.Options + [dgRowSelect];
end;

procedure TFmEtqGeraLot.EdLoteIniChange(Sender: TObject);
begin
  ReopenEtqGeraLot(0);
end;

procedure TFmEtqGeraLot.EdLoteQtdChange(Sender: TObject);
begin
  ReopenEtqGeraLot(0);
end;

procedure TFmEtqGeraLot.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEtqGeraLot.FormCreate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  GradeA.ColWidths[0] := 128;
  GradeC.ColWidths[0] := 128;
  GradeX.ColWidths[0] := 128;
  GradeQ.ColWidths[0] := 128;
  ReopenLastTenLot();
end;

procedure TFmEtqGeraLot.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmEtqGeraLot.GradeADrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  Ativo: Integer;
begin
  if (ACol = 0) or (ARow = 0) then
  begin
    with GradeA.Canvas do
    begin
      if ACol = 0 then
        MyObjects.DesenhaTextoEmStringGrid(GradeA, Rect, clBlack,
          Panel4.Color(*FmPrincipal.sd1.Colors[csButtonFace]*), taLeftJustify,
          GradeA.Cells[Acol, ARow], 0, 0, False)
      else
        MyObjects.DesenhaTextoEmStringGrid(GradeA, Rect, clBlack,
          Panel4.Color(*FmPrincipal.sd1.Colors[csButtonFace]*), taCenter,
          GradeA.Cells[Acol, ARow], 0, 0, False);
    end;
  end else begin
    if GradeA.Cells[Acol, ARow] = '' then
      Ativo := 0
    else
      Ativo := Geral.IMV(GradeA.Cells[ACol, ARow]) + 1;
    //
    MeuVCLSkin.DrawGrid4(GradeA, Rect, 0, Ativo);
  end;
end;

procedure TFmEtqGeraLot.GradeCDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  Cor: Integer;
begin
  if (ACol = 0) or (ARow = 0) then
    Cor := Panel4.Color
  else
    Cor := clWindow;
  if ACol = 0 then
    MyObjects.DesenhaTextoEmStringGrid(GradeC, Rect, clBlack,
      Cor(*FmPrincipal.sd1.Colors[csButtonFace]*), taLeftJustify,
      GradeC.Cells[Acol, ARow], 0, 0, False)
  else
    MyObjects.DesenhaTextoEmStringGrid(GradeC, Rect, clBlack,
      Cor(*FmPrincipal.sd1.Colors[csButtonFace]*), taCenter,
      GradeC.Cells[Acol, ARow], 0, 0, False);
end;

procedure TFmEtqGeraLot.GradeQDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  Cor: Integer;
begin
  if (ACol = 0) or (ARow = 0) then
    Cor := Panel4.Color
  else if GradeA.Cells[ACol, ARow] <> '' then
    Cor := clWindow
  else
    Cor := clMenu;

  //

  if (ACol = 0) or (ARow = 0) then
    MyObjects.DesenhaTextoEmStringGrid(GradeQ, Rect, clBlack,
      Cor(*FmPrincipal.sd1.Colors[csButtonFace]*), taLeftJustify,
      GradeQ.Cells[Acol, ARow], 0, 0, False)
  else
    MyObjects.DesenhaTextoEmStringGrid(GradeQ, Rect, clBlack,
      Cor(*FmPrincipal.sd1.Colors[csButtonFace]*), taRightJustify,
      GradeQ.Cells[Acol, ARow], 0, 0, False);
end;

procedure TFmEtqGeraLot.QrEtqGeraLotAfterScroll(DataSet: TDataSet);
begin
  DmProd.ConfigGrades4(QrEtqGeraLotGraTamCad.Value, QrEtqGeraLotNivel1.Value,
    QrEtqGeraLotCodigo.Value, GradeA, GradeX, GradeC, GradeQ);
end;

procedure TFmEtqGeraLot.ReopenEtqGeraLot(Codigo: Integer);
var
  Cod: Integer;
begin
  if FNaoReabreLot then
    Exit;
  //
  Cod := Codigo;
  if (Cod = 0) and (QrEtqGeraLot.State <> dsInactive) then
    Cod := QrEtqGeraLotCodigo.Value;
  QrEtqGeraLot.Close;

  QrEtqGeraLot.SQL.Clear;
  QrEtqGeraLot.SQL.Add('SELECT egl.Imprimir, egl.Impresso, egl.Codigo,');
  QrEtqGeraLot.SQL.Add('egl.DataCad, egl.Nivel1, gg1.GraTamCad,');
  QrEtqGeraLot.SQL.Add('gg1.CodUsu, gg1.Nome');
  QrEtqGeraLot.SQL.Add('FROM etqgeralot egl');
  QrEtqGeraLot.SQL.Add('LEFT JOIN gragru1 gg1 ON gg1.Nivel1=egl.Nivel1');
  if CkNaoImpresso.Checked then
    QrEtqGeraLot.SQL.Add('WHERE egl.Impresso=0');
  QrEtqGeraLot.SQL.Add('ORDER BY Codigo DESC');
  QrEtqGeraLot.SQL.Add('LIMIT :P0, :P1');
  QrEtqGeraLot.SQL.Add('');
  QrEtqGeraLot.Params[00].AsInteger := EdLoteIni.ValueVariant;
  QrEtqGeraLot.Params[01].AsInteger := EdLoteQtd.ValueVariant;
  QrEtqGeraLot.Open;
  //
  QrEtqGeraLot.Locate('Codigo', Cod, []);
end;

procedure TFmEtqGeraLot.ReopenLastTenLot();
begin
  FNaoReabreLot := True;
  EdLoteIni.ValueVariant := 0;
  EdLoteQtd.ValueVariant := 10;
  FNaoReabreLot := False;
  ReopenEtqGeraLot(0);
end;

end.

