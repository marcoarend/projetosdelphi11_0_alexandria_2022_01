unit Operacoes;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings,  UnGOTOy, UnInternalConsts, UnMsgInt, UnInternalConsts2,
  UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral, dmkPermissoes,
  dmkEdit, UnDmkProcFunc, dmkLabel, dmkImage, UnDmkEnums, AppListas,
  UnMyVCLref, dmkRadioGroup, dmkDBLookupComboBox, dmkEditCB;

type
  TFmOperacoes = class(TForm)
    PainelDados: TPanel;
    DsOperacoes: TDataSource;
    QrOperacoes: TmySQLQuery;
    QrOperacoesLk: TIntegerField;
    QrOperacoesDataCad: TDateField;
    QrOperacoesDataAlt: TDateField;
    QrOperacoesUserCad: TIntegerField;
    QrOperacoesUserAlt: TIntegerField;
    QrOperacoesCodigo: TSmallintField;
    QrOperacoesNome: TWideStringField;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    Label10: TLabel;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel1: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    QrTXOpeSeq: TmySQLQuery;
    DsTXOpeSeq: TDataSource;
    QrTXOpeSeqCodigo: TIntegerField;
    QrTXOpeSeqNome: TWideStringField;
    QrOperacoesTXOpeSeq: TIntegerField;
    QrOperacoesNO_TXOpeSeq: TWideStringField;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label5: TLabel;
    EdTXOpeSeq: TdmkEditCB;
    CBTXOpeSeq: TdmkDBLookupComboBox;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrOperacoesAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrOperacoesAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrOperacoesBeforeOpen(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    FSeq: Integer;
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmOperacoes: TFmOperacoes;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, DmkDAC_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmOperacoes.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmOperacoes.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrOperacoesCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmOperacoes.DefParams;
begin
  VAR_GOTOTABELA := 'Operacoes';
  VAR_GOTOMYSQLTABLE := QrOperacoes;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT ope.*, vos.Nome NO_TXOpeSeq ');
  VAR_SQLx.Add('FROM operacoes ope');
  VAR_SQLx.Add('LEFT JOIN txopeseq vos ON vos.Codigo=ope.TXOpeSeq ');
  VAR_SQLx.Add('WHERE ope.Codigo > -1000');
  //
  VAR_SQL1.Add('AND ope.Codigo=:P0');
  //
  VAR_SQLa.Add('AND ope.Nome Like :P0');
  //
end;

procedure TFmOperacoes.MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      EdNome.Text := CO_VAZIO;
      EdNome.Visible := True;
      if SQLType = stIns then
      begin
        EdCodigo.Text := '';
        EdNome.Text   := '';
      end else begin
        EdCodigo.Text           := IntToStr(QrOperacoesCodigo.Value);
        EdNome.Text             := QrOperacoesNome.Value;
        EdTXOpeSeq.ValueVariant := QrOperacoesTXOpeSeq.Value;
        CBTXOpeSeq.KeyValue     := QrOperacoesTXOpeSeq.Value;
      end;
      EdNome.SetFocus;
    end;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
  if Codigo <> 0 then LocCod(Codigo, Codigo);
end;

procedure TFmOperacoes.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmOperacoes.QueryPrincipalAfterOpen;
begin
end;

procedure TFmOperacoes.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmOperacoes.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmOperacoes.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmOperacoes.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmOperacoes.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmOperacoes.BtIncluiClick(Sender: TObject);
begin
  MostraEdicao(1, stIns, 0);
end;

procedure TFmOperacoes.BtAlteraClick(Sender: TObject);
var
  Operacoes : Integer;
begin
  Operacoes := QrOperacoesCodigo.Value;
  if not UMyMod.SelLockY(Operacoes, Dmod.MyDB, 'Operacoes', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(Operacoes, Dmod.MyDB, 'Operacoes', 'Codigo');
      MostraEdicao(1, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmOperacoes.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrOperacoesCodigo.Value;
  //VAR_OPERACAO := QrOperacoesCodigo.Value;
  Close;
end;

procedure TFmOperacoes.BtConfirmaClick(Sender: TObject);
var
  Nome: String;
  Codigo, TXOpeSeq: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.Text;
  TXOpeSeq       := EdTXOpeSeq.ValueVariant;
  //
  if MyObjects.FIC(Trim(Nome) = '', EdNome, 'Defina uma descri��o!') then Exit;
  if MyVCLRef.NomeMuitoGrande(Nome, 30, EdNome) then exit;
  //if MyObjects.FIC(TXOpeSeq = 0, EdTXOpeSeq, 'Defina o ID sequencial da opera��o!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('operacoes', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'operacoes', False, [
  'Nome', 'TXOpeSeq'], [
  'Codigo'], [
  Nome, TXOpeSeq], [
  Codigo], True) then
  begin
    UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Operacoes', 'Codigo');
    MostraEdicao(0, stLok, 0);
    LocCod(Codigo,Codigo);
    //
    if FSeq = 1 then Close;
  end;
end;

procedure TFmOperacoes.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'Operacoes', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Operacoes', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Operacoes', 'Codigo');
end;

procedure TFmOperacoes.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FSeq := 0;
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  PainelEdit.Align  := alClient;
  PainelData.Align  := alClient;
  UnDmkDAC_PF.AbreQuery(QrTXOpeSeq, Dmod.MyDB);
  CriaOForm;
end;

procedure TFmOperacoes.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrOperacoesCodigo.Value,LaRegistro.Caption);
end;

procedure TFmOperacoes.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmOperacoes.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmOperacoes.SbNovoClick(Sender: TObject);
begin
//
end;

procedure TFmOperacoes.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  VAR_MARCA := QrOperacoesCodigo.Value;
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmOperacoes.QrOperacoesAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmOperacoes.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  if FSeq = 1 then MostraEdicao(1, stIns, 0);
end;

procedure TFmOperacoes.QrOperacoesAfterScroll(DataSet: TDataSet);
begin
  BtAltera.Enabled := GOTOy.BtEnabled(QrOperacoesCodigo.Value, False);
  BtExclui.Enabled := GOTOy.BtEnabled(QrOperacoesCodigo.Value, False);
end;

procedure TFmOperacoes.SbQueryClick(Sender: TObject);
begin
  LocCod(QrOperacoesCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'Operacoes', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmOperacoes.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOperacoes.QrOperacoesBeforeOpen(DataSet: TDataSet);
begin
  QrOperacoesCodigo.DisplayFormat := FFormatFloat;
end;

end.

