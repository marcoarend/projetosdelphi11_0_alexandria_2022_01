unit SetorCad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, UnDmkEnums;

type
  TFmSetorCad = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrSetorCad: TmySQLQuery;
    QrSetorCadNome: TWideStringField;
    QrSetorCadSigla: TWideStringField;
    DsSetorCad: TDataSource;
    DBEdNome: TdmkDBEdit;
    DBEdCodigo: TdmkDBEdit;
    Label1: TLabel;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    Label3: TLabel;
    Label6: TLabel;
    DBEdit2: TDBEdit;
    EdSigla: TdmkEdit;
    Label4: TLabel;
    Label5: TLabel;
    Edm2Dia: TdmkEdit;
    QrSetorCadLk: TIntegerField;
    QrSetorCadDataCad: TDateField;
    QrSetorCadDataAlt: TDateField;
    QrSetorCadUserCad: TIntegerField;
    QrSetorCadUserAlt: TIntegerField;
    QrSetorCadCodigo: TSmallintField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrSetorCadBeforeOpen(DataSet: TDataSet);
    procedure QrSetorCadAfterOpen(DataSet: TDataSet);
    procedure QrSetorCadAfterScroll(DataSet: TDataSet);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmSetorCad: TFmSetorCad;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, DmkDAC_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmSetorCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmSetorCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrSetorCadCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmSetorCad.DefParams;
begin
  VAR_GOTOTABELA := 'setorcad';
  VAR_GOTOMYSQLTABLE := QrSetorCad;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM setorcad');
  VAR_SQLx.Add('WHERE Codigo > -1000');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQLa.Add('AND Nome LIKE :P0');
  //
end;

procedure TFmSetorCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmSetorCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmSetorCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmSetorCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmSetorCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmSetorCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmSetorCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmSetorCad.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSetorCad.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrSetorCad, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'setorcad');
end;

procedure TFmSetorCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrSetorCadCodigo.Value;
  Close;
end;

procedure TFmSetorCad.BtConfirmaClick(Sender: TObject);
var
  Codigo, Loc: Integer;
  Nome, Sigla: String;
  SQLType: TSQLType;
  //
  Qry: TmySQLQuery;
begin
  SQLType := ImgTipo.SQLType;
  Codigo  := EdCodigo.ValueVariant;
  Nome    := Trim(EdNome.Text);
  //
  if MyObjects.FIC(Nome = '', EdNome, 'Defina uma descri��o.') then
    Exit;
  Sigla := Trim(EdSigla.Text);
  if MyObjects.FIC(Sigla = '', EdSigla, 'Defina uma sigla.') then
    Exit;
  //
  if SQLType = stIns then
    Loc := -1000
  else
    Loc := QrSetorCadCodigo.Value;
  //
  Qry := TmySQLQuery.Create(Dmod);
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT * ',
  'FROM setorcad ',
  'WHERE Sigla="' + Sigla + '" ',
  'AND Codigo<>' + Geral.FF0(Loc),
  '']);
  if MyObjects.FIC(Qry.RecordCount > 0, EdSigla,
  'Inclus�o cancelada, a sigla informada j� est� cadastrada para o sertor n� ' +
  Geral.FF0(Qry.FieldByName('Codigo').AsInteger) + ' - "' +
  Qry.FieldByName('Nome').AsString + '"') then
    Exit;
  Codigo := UMyMod.BPGS1I32('setorcad', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'setorcad', False, [
  'Nome', 'Sigla'], [
  'Codigo'], [
  Nome, Sigla], [
  Codigo], True) then
  begin
    UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'setorcad', 'Codigo');
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    LocCod(Codigo,Codigo);
  end;
end;

procedure TFmSetorCad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'setorcad', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmSetorCad.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrSetorCad, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'setorcad');
end;

procedure TFmSetorCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
  CriaOForm;
end;

procedure TFmSetorCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrSetorCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmSetorCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmSetorCad.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrSetorCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmSetorCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmSetorCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSetorCad.SbQueryClick(Sender: TObject);
begin
  LocCod(QrSetorCadCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'setorcad', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmSetorCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSetorCad.QrSetorCadAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmSetorCad.QrSetorCadAfterScroll(DataSet: TDataSet);
begin
  BtAltera.Enabled := QrSetorCadCodigo.Value > 0;
end;

procedure TFmSetorCad.QrSetorCadBeforeOpen(DataSet: TDataSet);
begin
  QrSetorCadCodigo.DisplayFormat := FFormatFloat;
end;

end.

