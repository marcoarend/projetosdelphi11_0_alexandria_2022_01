unit PlanCutImpCen;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DB, DBGrids,
  dmkDBGrid, DBCtrls, mySQLDbTables, Menus, ComCtrls, UnDmkProcFunc;

type
  THackDBGrid = class(TDBGrid);
  TFmPlanCutImpCen = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    BtNenhum: TBitBtn;
    BtTodos: TBitBtn;
    BtPesq: TBitBtn;
    PMPesq: TPopupMenu;
    Ativa1: TMenuItem;
    Desativa1: TMenuItem;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel3: TPanel;
    Label1: TLabel;
    EdPesq: TEdit;
    DBGrid1: TdmkDBGrid;
    Panel4: TPanel;
    DBGrid2: TdmkDBGrid;
    RGSelecao: TRadioGroup;
    procedure FormCreate(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure FormStartDock(Sender: TObject; var DragObject: TDragDockObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure EdPesqChange(Sender: TObject);
    procedure FormUnDock(Sender: TObject; Client: TControl;
      NewTarget: TWinControl; var Allow: Boolean);
    procedure BtNenhumClick(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtPesqClick(Sender: TObject);
    procedure Ativa1Click(Sender: TObject);
    procedure Desativa1Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure RGSelecaoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure Pesquisar();
    procedure AtivaSelecionados(Status: Integer);
    procedure HabilitaItemUnico();
    procedure AtivaItens(Status: Integer);
  public
    { Public declarations }
    class function CreateDockForm(const aColor: TColor): TCustomForm;
    //class procedure FiltaAtivos();
    procedure LimpaPesquisa();
  end;

var
  FmPlanCutImpCen: TFmPlanCutImpCen;

implementation

uses UnMyObjects, Module, UMySQLModule, TabePrcCab, PlanCutImp, ModPlanCut, MyGlyfs;

{$R *.DFM}

///// PROCEDURES E FUNCTIONS DO DOCKFORM

procedure TFmPlanCutImpCen.Ativa1Click(Sender: TObject);
begin
  AtivaSelecionados(1);
end;

procedure TFmPlanCutImpCen.AtivaItens(Status: Integer);
begin
  Screen.Cursor := crHourGlass;
  DmPlanCut.QrPCICenIts.Close;
  DmPlanCut.QrPCICenIts.SQL.Clear;
  DmPlanCut.QrPCICenIts.SQL.Add('UPDATE pcicen SET Ativo=' + dmkPF.FFP(Status, 0) + ';');
  DmPlanCut.QrPCICenIts.SQL.Add('SELECT * FROM pcicen;');
  DmPlanCut.QrPCICenIts.Open;
  HabilitaItemUnico();
  Screen.Cursor := crDefault;
end;

procedure TFmPlanCutImpCen.AtivaSelecionados(Status: Integer);
begin
  Screen.Cursor := crHourGlass;
  DmPlanCut.QrPCICenIts.First;
  while not DmPlanCut.QrPCICenIts.Eof do
  begin
    if DmPlanCut.QrPCICenItsAtivo.Value <> Status then
    begin
      DmPlanCut.QrPCICenIts.Edit;
      DmPlanCut.QrPCICenItsAtivo.Value := Status;
      DmPlanCut.QrPCICenIts.Post;
    end;
    DmPlanCut.QrPCICenIts.Next;
  end;
  HabilitaItemUnico();
  Screen.Cursor := crDefault;
end;

procedure TFmPlanCutImpCen.BtPesqClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPesq, BtPesq);
end;

procedure TFmPlanCutImpCen.BtNenhumClick(Sender: TObject);
begin
  AtivaItens(0);
end;

procedure TFmPlanCutImpCen.BtTodosClick(Sender: TObject);
begin
  AtivaItens(1);
end;

class function TFmPlanCutImpCen.CreateDockForm(const aColor: TColor): TCustomForm;
begin
  result := TFmPlanCutImpCen.Create(Application);
  //result.Color := aColor;
  //result.Caption := ColorToString(aColor);
  FmMyGlyfs.ConfiguraFormDock(result);
  result.Show;
end;

procedure TFmPlanCutImpCen.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  DmPlanCut.QrPCICenIts.Close;
  DmPlanCut.QrPCICenIts.SQL.Clear;
  DmPlanCut.QrPCICenIts.SQL.Add('DROP TABLE PCICen; ');
  DmPlanCut.QrPCICenIts.ExecSQL;
  //
  ManualFloat(Rect(0, 0, 0, 0));
  Action := caFree;
end;

procedure TFmPlanCutImpCen.FormShow(Sender: TObject);
begin
  FmPlanCutImp.FechaPesquisa();
end;

procedure TFmPlanCutImpCen.FormStartDock(Sender: TObject;
  var DragObject: TDragDockObject);
begin
  DragObject := TDragDockObjectEx.Create(Self);
  DragObject.Brush.Color := clAqua; // this will display a red outline
end;


procedure TFmPlanCutImpCen.FormUnDock(Sender: TObject; Client: TControl;
  NewTarget: TWinControl; var Allow: Boolean);
begin
end;

///// FIM DAS PROCEDURES E FUNCTIONS DO DOCKFORM

procedure TFmPlanCutImpCen.PageControl1Change(Sender: TObject);
begin
  BtPesq.Enabled := PageControl1.ActivePageIndex = 0;
  case PageControl1.ActivePageIndex of
    0: Pesquisar();
    1: RGSelecaoClick(Self);
  end;
end;

procedure TFmPlanCutImpCen.Pesquisar();
begin
  Screen.Cursor := crHourGlass;
  //
  DmPlanCut.QrPCICenIts.Filter   := 'Nome LIKE "%' + EdPesq.Text + '%"';
  DmPlanCut.QrPCICenIts.Filtered := True;
  //
  Screen.Cursor := crDefault;
end;

procedure TFmPlanCutImpCen.RGSelecaoClick(Sender: TObject);
begin
  DmPlanCut.QrPCICenIts.Filtered := False;
  case RGSelecao.ItemIndex of
    0: DmPlanCut.QrPCICenIts.Filter := 'Ativo=0';
    1: DmPlanCut.QrPCICenIts.Filter := 'Ativo=1';
    2: DmPlanCut.QrPCICenIts.Filter := '';
  end;
  if RGSelecao.ItemIndex < 2 then
    DmPlanCut.QrPCICenIts.Filtered := True;
end;

procedure TFmPlanCutImpCen.DBGrid1CellClick(Column: TColumn);
var
  Status: Integer;
begin
  if Column.FieldName = 'Ativo' then
  begin
    if DmPlanCut.QrPCICenIts.FieldByName('Ativo').Value = 1 then
      Status := 0
    else
      Status := 1;
    //
    DmPlanCut.QrPCICenIts.Edit;
    DmPlanCut.QrPCICenIts.FieldByName('Ativo').Value := Status;
    DmPlanCut.QrPCICenIts.Post;
  end;
  HabilitaItemUnico();
end;

procedure TFmPlanCutImpCen.HabilitaItemUnico();
var
  Habilita: Boolean;
begin
  DmPlanCut.QrPCICenAti.Close;
  DmPlanCut.QrPCICenAti.Open;
  Habilita :=  DmPlanCut.QrPCICenAtiItens.Value = 0;
  FmPlanCutImp.LaPCICen.Enabled := Habilita;
  FmPlanCutImp.EdPCICen.Enabled := Habilita;
  FmPlanCutImp.CBPCICen.Enabled := Habilita;
  if not Habilita then
  begin
    FmPlanCutImp.EdPCICen.ValueVariant := 0;
    FmPlanCutImp.CBPCICen.KeyValue     := 0;
  end;
end;

procedure TFmPlanCutImpCen.LimpaPesquisa();
var
  K, I: Integer;
  //Form: TForm;
begin
  for K := 0 to Screen.FormCount - 1 do
  begin
    if Screen.Forms[K] is TFmPlanCutImpCen then
    begin
      for I := 0 to Screen.Forms[K].ComponentCount -1 do
      if Screen.Forms[K].Components[I] is TEdit then
        TEdit(Screen.Forms[K].Components[I]).Text := '';
    end;
  end;
  AtivaItens(0);
end;

procedure TFmPlanCutImpCen.Desativa1Click(Sender: TObject);
begin
  AtivaSelecionados(0);
end;

procedure TFmPlanCutImpCen.EdPesqChange(Sender: TObject);
begin
  Pesquisar();
end;

procedure TFmPlanCutImpCen.FormCreate(Sender: TObject);
const
  Txt1 = 'INSERT INTO pcicen (Codigo,CodUsu,Nome,Ativo) VALUES(';
var
  Txt2: String;
begin
  DmPlanCut.QrPCICenIts.Close;
  DmPlanCut.QrPCICenIts.SQL.Clear;
  DmPlanCut.QrPCICenIts.SQL.Add('DROP TABLE PCICen; ');
  DmPlanCut.QrPCICenIts.SQL.Add('CREATE TABLE PCICen (');
  DmPlanCut.QrPCICenIts.SQL.Add('  Codigo  integer      ,');
  DmPlanCut.QrPCICenIts.SQL.Add('  CodUsu  integer      ,');
  DmPlanCut.QrPCICenIts.SQL.Add('  Nome    varchar(50)  ,');
  DmPlanCut.QrPCICenIts.SQL.Add('  Ativo   smallint      ');
  DmPlanCut.QrPCICenIts.SQL.Add(');');
  //
  DmPlanCut.QrPCICenCad.Close;
  DmPlanCut.QrPCICenCad.Open;
  while not DmPlanCut.QrPCICenCad.Eof do
  begin
    Txt2 :=
      dmkPF.FFP(DmPlanCut.QrPCICenCadCodigo.Value, 0) + ',' +
      dmkPF.FFP(DmPlanCut.QrPCICenCadCodUsu.Value, 0) + ',' +
      '"' + DmPlanCut.QrPCICenCadNome.Value + '",' +
      '0);';
  //for i  := 1 to 10 do
    DmPlanCut.QrPCICenIts.SQL.Add(Txt1 + Txt2);
    DmPlanCut.QrPCICenCad.Next;
  end;
  //
  DmPlanCut.QrPCICenIts.SQL.Add('SELECT * FROM pcicen;');
  DmPlanCut.QrPCICenIts.Open;
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmPlanCutImpCen.FormDeactivate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 1;
  RGSelecao.ItemIndex := 1;
  //
  if FindWindow('TFmPlanCutImp', nil) > 0 then
    FmPlanCutImp.AtivaBtConfirma();
end;

end.

