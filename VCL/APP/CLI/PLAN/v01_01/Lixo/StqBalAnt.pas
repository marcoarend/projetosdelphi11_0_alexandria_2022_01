unit StqBalAnt;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DB, mySQLDbTables, ExtCtrls, ComCtrls;

type
  TFmStqBalAnt = class(TForm)
    QrItens: TmySQLQuery;
    PainelTitulo: TPanel;
    Image1: TImage;
    PB1: TProgressBar;
    Panel1: TPanel;
    StaticText1: TStaticText;
    QrItensGraGruX: TIntegerField;
    QrSoma: TmySQLQuery;
    QrSomaQtde: TFloatField;
    QrItensStqCenCad: TIntegerField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  //protected
    //procedure CreateParams(var Params: TCreateParams); override;
  public
    { Public declarations }
    procedure AtualizaSaldosAnteriores(Codigo: Integer);
  end;

var
  FmStqBalAnt: TFmStqBalAnt;

implementation

uses UnMyObjects, UnMLAGeral, Module, UMySQLModule, dmkGeral;

{$R *.dfm}

procedure TFmStqBalAnt.AtualizaSaldosAnteriores(Codigo: Integer);
begin
  Screen.Cursor := crHourGlass;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM stqbalant WHERE Codigo=:P0');
  Dmod.QrUpd.Params[0].AsInteger := Codigo;
  Dmod.QrUpd.ExecSQL;
  //
  QrItens.Close;
  QrItens.Params[0].AsInteger := Codigo;
  QrItens.Open;
  PB1.Max := QrItens.RecordCount;
  PB1.Position := 0;
  while not QrItens.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    PB1.Update;
    Application.ProcessMessages;
    //
    QrSoma.Close;
    QrSoma.Params[00].AsInteger := QrItensStqCenCad.Value;
    QrSoma.Params[01].AsInteger := QrItensGraGruX.Value;
    QrSoma.Open;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'stqbalant', False, [
    'Qtde'], [
    'Codigo', 'StqCenCad', 'GraGruX'], [
    QrSomaQtde.Value], [
    Codigo, QrItensStqCenCad.Value, QrItensGraGruX.Value], False);
    //
    QrItens.Next;
  end;
  Screen.Cursor := crDefault;
  Close;
  //
end;

{
Procedure TFmStqBalAnt.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
  with Params do
  begin
    Style := Style or WS_CHILD;
    WndParent := Application.MainForm.Handle;
  end;
end;
}

procedure TFmStqBalAnt.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmStqBalAnt.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  //Action := caFree;
end;

end.

