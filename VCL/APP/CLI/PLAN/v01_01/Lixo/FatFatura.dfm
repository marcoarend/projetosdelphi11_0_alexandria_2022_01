object FmFatFatura: TFmFatFatura
  Left = 339
  Top = 185
  Caption = 'FAT-FATUR-001 :: Faturamento de Pedido'
  ClientHeight = 604
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    Caption = 'Faturamento de Pedido'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 1006
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 48
    Width = 1008
    Height = 556
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = ' Pedido '
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object PnPed: TPanel
        Left = 0
        Top = 0
        Width = 1000
        Height = 528
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Panel1: TPanel
          Left = 0
          Top = 480
          Width = 1000
          Height = 48
          Align = alBottom
          TabOrder = 1
          object Panel3: TPanel
            Left = 888
            Top = 1
            Width = 111
            Height = 46
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 0
            object BitBtn2: TBitBtn
              Tag = 15
              Left = 2
              Top = 3
              Width = 90
              Height = 40
              Cursor = crHandPoint
              Hint = 'Sai da janela atual'
              Caption = '&Desiste'
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BitBtn2Click
              NumGlyphs = 2
            end
          end
        end
        object GroupBox9: TGroupBox
          Left = 0
          Top = 0
          Width = 1000
          Height = 58
          Align = alTop
          TabOrder = 0
          object Label56: TLabel
            Left = 8
            Top = 12
            Width = 44
            Height = 13
            Caption = 'Empresa:'
          end
          object Label1: TLabel
            Left = 8
            Top = 36
            Width = 36
            Height = 13
            Caption = 'Pedido:'
          end
          object Label64: TLabel
            Left = 458
            Top = 36
            Width = 50
            Height = 13
            Caption = 'Prioridade:'
          end
          object Label65: TLabel
            Left = 574
            Top = 36
            Width = 101
            Height = 13
            Caption = 'Previs'#227'o entrega:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label7: TLabel
            Left = 812
            Top = 36
            Width = 14
            Height = 13
            Caption = 'ID:'
          end
          object Label4: TLabel
            Left = 892
            Top = 36
            Width = 36
            Height = 13
            Caption = 'C'#243'digo:'
          end
          object DBEdit3: TDBEdit
            Left = 56
            Top = 8
            Width = 53
            Height = 21
            DataField = 'Filial'
            DataSource = DsPediVda
            Enabled = False
            TabOrder = 1
          end
          object DBEdit2: TDBEdit
            Left = 114
            Top = 8
            Width = 874
            Height = 21
            DataField = 'NOMEEMP'
            DataSource = DsPediVda
            Enabled = False
            TabOrder = 2
          end
          object EdPedido: TdmkEdit
            Left = 56
            Top = 32
            Width = 93
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            OnExit = EdPedidoExit
          end
          object DBEdit12: TDBEdit
            Left = 512
            Top = 32
            Width = 53
            Height = 21
            DataField = 'Prioridade'
            DataSource = DsPediVda
            Enabled = False
            TabOrder = 3
          end
          object DBEdit11: TDBEdit
            Left = 679
            Top = 32
            Width = 129
            Height = 21
            DataField = 'DtaPrevi'
            DataSource = DsPediVda
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 4
          end
          object EdCodigo: TdmkEdit
            Left = 832
            Top = 32
            Width = 56
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 5
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Codigo'
            UpdCampo = 'Codigo'
            UpdType = utInc
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object EdCodUsu: TdmkEdit
            Left = 932
            Top = 32
            Width = 56
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 6
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'CodUsu'
            UpdCampo = 'CodUsu'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
        end
        object GroupBox1: TGroupBox
          Left = 0
          Top = 58
          Width = 1000
          Height = 136
          Align = alTop
          TabOrder = 2
          object Label8: TLabel
            Left = 8
            Top = 12
            Width = 35
            Height = 13
            Caption = 'Cliente:'
          end
          object DBText1: TDBText
            Left = 832
            Top = 12
            Width = 45
            Height = 17
            DataField = 'NOME_TIPO_DOC'
            DataSource = DsCli
          end
          object Label2: TLabel
            Left = 8
            Top = 84
            Width = 40
            Height = 13
            Caption = 'Entrega:'
          end
          object DBEdit1: TDBEdit
            Left = 56
            Top = 8
            Width = 72
            Height = 21
            DataField = 'CodUsu'
            DataSource = DsCli
            Enabled = False
            TabOrder = 0
          end
          object DBEdit6: TDBEdit
            Left = 132
            Top = 8
            Width = 693
            Height = 21
            DataField = 'NOME_ENT'
            DataSource = DsCli
            Enabled = False
            TabOrder = 1
          end
          object DBMemo1: TDBMemo
            Left = 56
            Top = 32
            Width = 932
            Height = 47
            DataField = 'E_ALL'
            DataSource = DsCli
            Enabled = False
            TabOrder = 2
          end
          object DBEdit4: TDBEdit
            Left = 876
            Top = 8
            Width = 112
            Height = 21
            DataField = 'CNPJ_TXT'
            DataSource = DsCli
            Enabled = False
            TabOrder = 3
          end
          object DBMemo2: TDBMemo
            Left = 56
            Top = 84
            Width = 932
            Height = 47
            DataField = 'E_ALL'
            DataSource = DsEntrega
            Enabled = False
            TabOrder = 4
          end
        end
        object GroupBox12: TGroupBox
          Left = 0
          Top = 194
          Width = 1000
          Height = 34
          Align = alTop
          TabOrder = 3
          object Label16: TLabel
            Left = 8
            Top = 12
            Width = 73
            Height = 13
            Caption = 'Representante:'
          end
          object Label17: TLabel
            Left = 640
            Top = 11
            Width = 117
            Height = 13
            Caption = '% comiss'#227'o faturamento:'
          end
          object Label18: TLabel
            Left = 819
            Top = 11
            Width = 119
            Height = 13
            Caption = '% comiss'#227'o recebimento:'
          end
          object DBEdit44: TDBEdit
            Left = 84
            Top = 8
            Width = 57
            Height = 21
            DataField = 'CODUSU_ACC'
            DataSource = DsPediVda
            Enabled = False
            TabOrder = 0
          end
          object DBEdit45: TDBEdit
            Left = 144
            Top = 8
            Width = 489
            Height = 21
            DataField = 'NOMEACC'
            DataSource = DsPediVda
            Enabled = False
            TabOrder = 1
          end
          object DBEdit46: TDBEdit
            Left = 760
            Top = 8
            Width = 48
            Height = 21
            DataField = 'ComisFat'
            DataSource = DsPediVda
            Enabled = False
            TabOrder = 2
          end
          object DBEdit47: TDBEdit
            Left = 939
            Top = 8
            Width = 48
            Height = 21
            DataField = 'ComisRec'
            DataSource = DsPediVda
            Enabled = False
            TabOrder = 3
          end
        end
        object GroupBox15: TGroupBox
          Left = 0
          Top = 228
          Width = 1000
          Height = 43
          Align = alTop
          Caption = ' Fiscal:'
          TabOrder = 4
          object Label34: TLabel
            Left = 8
            Top = 20
            Width = 73
            Height = 13
            Caption = 'Movimenta'#231#227'o:'
          end
          object Label35: TLabel
            Left = 640
            Top = 20
            Width = 55
            Height = 13
            Caption = 'Modelo NF:'
          end
          object DBEdit48: TDBEdit
            Left = 84
            Top = 16
            Width = 57
            Height = 21
            DataField = 'CODUSU_FRC'
            DataSource = DsPediVda
            Enabled = False
            TabOrder = 0
          end
          object DBEdit49: TDBEdit
            Left = 144
            Top = 16
            Width = 489
            Height = 21
            DataField = 'NOMEFISREGCAD'
            DataSource = DsPediVda
            Enabled = False
            TabOrder = 1
          end
          object DBEdit50: TDBEdit
            Left = 700
            Top = 16
            Width = 287
            Height = 21
            DataField = 'NOMEMODELONF'
            DataSource = DsPediVda
            Enabled = False
            TabOrder = 2
          end
        end
        object GroupBox14: TGroupBox
          Left = 0
          Top = 271
          Width = 1000
          Height = 82
          Align = alTop
          TabOrder = 5
          object Label51: TLabel
            Left = 8
            Top = 13
            Width = 45
            Height = 13
            Caption = 'Frete por:'
          end
          object Label52: TLabel
            Left = 8
            Top = 36
            Width = 75
            Height = 13
            Caption = 'Transportadora:'
          end
          object Label53: TLabel
            Left = 8
            Top = 61
            Width = 64
            Height = 13
            Caption = 'Redespacho:'
          end
          object Label66: TLabel
            Left = 172
            Top = 12
            Width = 117
            Height = 13
            Caption = '% Despesas acess'#243'rias: '
            Visible = False
          end
          object Label67: TLabel
            Left = 340
            Top = 12
            Width = 115
            Height = 13
            Caption = '$ Despesas acess'#243'rias: '
            Visible = False
          end
          object Label68: TLabel
            Left = 532
            Top = 12
            Width = 38
            Height = 13
            Caption = '% Frete:'
            Visible = False
          end
          object Label70: TLabel
            Left = 628
            Top = 12
            Width = 36
            Height = 13
            Caption = '$ Frete:'
            Visible = False
          end
          object Label71: TLabel
            Left = 752
            Top = 12
            Width = 48
            Height = 13
            Caption = '% Seguro:'
            Visible = False
          end
          object Label72: TLabel
            Left = 860
            Top = 12
            Width = 46
            Height = 13
            Caption = '$ Seguro:'
            Visible = False
          end
          object DBEdit16: TDBEdit
            Left = 56
            Top = 8
            Width = 21
            Height = 21
            DataField = 'FretePor'
            DataSource = DsPediVda
            Enabled = False
            TabOrder = 0
          end
          object DBEdit23: TDBEdit
            Left = 80
            Top = 8
            Width = 88
            Height = 21
            DataField = 'NOMEFRETEPOR'
            DataSource = DsPediVda
            Enabled = False
            TabOrder = 1
          end
          object DBEdit34: TDBEdit
            Left = 88
            Top = 32
            Width = 56
            Height = 21
            DataField = 'CODUSU_TRA'
            DataSource = DsPediVda
            Enabled = False
            TabOrder = 2
          end
          object DBEdit35: TDBEdit
            Left = 88
            Top = 56
            Width = 56
            Height = 21
            DataField = 'CODUSU_RED'
            DataSource = DsPediVda
            Enabled = False
            TabOrder = 3
          end
          object DBEdit36: TDBEdit
            Left = 148
            Top = 32
            Width = 839
            Height = 21
            DataField = 'NOMETRANSP'
            DataSource = DsPediVda
            Enabled = False
            TabOrder = 4
          end
          object DBEdit37: TDBEdit
            Left = 148
            Top = 56
            Width = 839
            Height = 21
            DataField = 'NOMEREDESP'
            DataSource = DsPediVda
            Enabled = False
            TabOrder = 5
          end
          object DBEdit38: TDBEdit
            Left = 464
            Top = 8
            Width = 64
            Height = 21
            DataField = 'DesoAces_V'
            DataSource = DsPediVda
            Enabled = False
            TabOrder = 6
            Visible = False
          end
          object DBEdit39: TDBEdit
            Left = 288
            Top = 8
            Width = 48
            Height = 21
            DataField = 'DesoAces_P'
            DataSource = DsPediVda
            Enabled = False
            TabOrder = 7
            Visible = False
          end
          object DBEdit40: TDBEdit
            Left = 676
            Top = 8
            Width = 72
            Height = 21
            DataField = 'Frete_V'
            DataSource = DsPediVda
            Enabled = False
            TabOrder = 8
            Visible = False
          end
          object DBEdit41: TDBEdit
            Left = 576
            Top = 8
            Width = 48
            Height = 21
            DataField = 'Frete_P'
            DataSource = DsPediVda
            Enabled = False
            TabOrder = 9
            Visible = False
          end
          object DBEdit42: TDBEdit
            Left = 916
            Top = 8
            Width = 71
            Height = 21
            DataField = 'Seguro_V'
            DataSource = DsPediVda
            Enabled = False
            TabOrder = 10
            Visible = False
          end
          object DBEdit43: TDBEdit
            Left = 804
            Top = 8
            Width = 48
            Height = 21
            DataField = 'Seguro_P'
            DataSource = DsPediVda
            Enabled = False
            TabOrder = 11
            Visible = False
          end
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = ' Confer'#234'ncia '
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object PnCfe: TPanel
        Left = 0
        Top = 0
        Width = 1000
        Height = 528
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Panel4: TPanel
          Left = 0
          Top = 480
          Width = 1000
          Height = 48
          Align = alBottom
          TabOrder = 0
          object Panel5: TPanel
            Left = 888
            Top = 1
            Width = 111
            Height = 46
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 0
            object BitBtn3: TBitBtn
              Tag = 15
              Left = 2
              Top = 3
              Width = 90
              Height = 40
              Cursor = crHandPoint
              Hint = 'Sai da janela atual'
              Caption = '&Desiste'
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BitBtn2Click
              NumGlyphs = 2
            end
          end
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = ' Emiss'#227'o '
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object PnEmi: TPanel
        Left = 0
        Top = 0
        Width = 1000
        Height = 528
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object PainelConfirma: TPanel
          Left = 0
          Top = 480
          Width = 1000
          Height = 48
          Align = alBottom
          TabOrder = 0
          object BtOK: TBitBtn
            Tag = 14
            Left = 20
            Top = 4
            Width = 90
            Height = 40
            Caption = '&OK'
            TabOrder = 0
            NumGlyphs = 2
          end
          object Panel2: TPanel
            Left = 888
            Top = 1
            Width = 111
            Height = 46
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 1
            object BitBtn1: TBitBtn
              Tag = 15
              Left = 2
              Top = 3
              Width = 90
              Height = 40
              Cursor = crHandPoint
              Hint = 'Sai da janela atual'
              Caption = '&Desiste'
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BitBtn2Click
              NumGlyphs = 2
            end
          end
        end
      end
    end
  end
  object QrPediVda: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPediVdaAfterOpen
    BeforeClose = QrPediVdaBeforeClose
    OnCalcFields = QrPediVdaCalcFields
    SQL.Strings = (
      'SELECT pvd.Codigo, pvd.CodUsu, pvd.Empresa, pvd.Cliente,'
      'pvd.DtaEmiss, pvd.DtaEntra, pvd.DtaInclu, pvd.DtaPrevi, '
      'pvd.Prioridade, pvd.CondicaoPG, pvd.Moeda, '
      'pvd.Situacao, pvd.TabelaPrc, pvd.MotivoSit, pvd.LoteProd,'
      'pvd.PedidoCli, pvd.FretePor, pvd.Transporta, pvd.Redespacho,'
      'pvd.RegrFiscal, pvd.DesoAces_V, pvd.DesoAces_P,'
      'pvd.Frete_V, pvd.Frete_P, pvd.Seguro_V, pvd.Seguro_P, '
      'pvd.TotalQtd, pvd.Total_Vlr, pvd.Total_Des, pvd.Total_Tot,'
      'pvd.Observa, tpc.Nome NOMETABEPRCCAD,mda.Nome NOMEMOEDA,'
      'pvd.Represen, pvd.ComisFat, pvd.ComisRec, pvd.CartEmis,'#13
      'pvd.AFP_Sit, pvd.AFP_Per, ppc.MedDDSimpl, MedDDReal,'
      'pvd.ValLiq, pvd.QuantP, frc.Nome NOMEFISREGCAD,'
      'imp.Nome NOMEMODELONF,'
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NOMEEMP,'
      'IF(ven.Tipo=0, ven.RazaoSocial, ven.NOME) NOMEACC,'
      'IF(tra.Tipo=0, tra.RazaoSocial, tra.NOME) NOMETRANSP,'
      'IF(red.Tipo=0, red.RazaoSocial, red.NOME) NOMEREDESP,'
      'emp.Filial, car.Nome NOMECARTEMIS,'
      'ppc.Nome NOMECONDICAOPG, mot.Nome NOMEMOTIVO,'
      'ven.CodUsu CODUSU_ACC, tra.CodUsu CODUSU_TRA, '
      'red.CodUsu CODUSU_RED, mot.CodUsu CODUSU_MOT, '
      'tpc.CodUsu CODUSU_TPC, mda.CodUsu CODUSU_MDA, '
      'ppc.CodUsu CODUSU_PPC,'
      'frc.CodUsu CODUSU_FRC, imp.Codigo MODELO_NF'#13
      'FROM pedivda pvd'
      'LEFT JOIN entidades  emp ON emp.Codigo=pvd.Empresa'
      'LEFT JOIN entidades  tra ON tra.Codigo=pvd.Transporta'
      'LEFT JOIN entidades  red ON red.Codigo=pvd.Redespacho'
      'LEFT JOIN tabeprccab tpc ON tpc.Codigo=pvd.TabelaPrc'
      'LEFT JOIN cambiomda  mda ON mda.Codigo=pvd.Moeda'
      'LEFT JOIN pediprzcab ppc ON ppc.Codigo=pvd.CondicaoPG'
      'LEFT JOIN motivos    mot ON mot.Codigo=pvd.MotivoSit'
      'LEFT JOIN pediacc    acc ON acc.Codigo=pvd.Represen'
      'LEFT JOIN entidades  ven ON ven.Codigo=acc.Codigo'
      'LEFT JOIN carteiras  car ON car.Codigo=pvd.CartEmis'
      'LEFT JOIN fisregcad  frc ON frc.Codigo=pvd.RegrFiscal'
      'LEFT JOIN imprime    imp ON imp.Codigo=frc.ModeloNF'
      'WHERE pvd.CodUsu=:P0')
    Left = 4
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPediVdaNOMEFRETEPOR: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEFRETEPOR'
      Calculated = True
    end
    object QrPediVdaCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPediVdaCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrPediVdaEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrPediVdaCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrPediVdaDtaEmiss: TDateField
      FieldName = 'DtaEmiss'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPediVdaDtaEntra: TDateField
      FieldName = 'DtaEntra'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPediVdaDtaInclu: TDateField
      FieldName = 'DtaInclu'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPediVdaDtaPrevi: TDateField
      FieldName = 'DtaPrevi'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPediVdaPrioridade: TSmallintField
      FieldName = 'Prioridade'
      Required = True
    end
    object QrPediVdaCondicaoPG: TIntegerField
      FieldName = 'CondicaoPG'
      Required = True
    end
    object QrPediVdaMoeda: TIntegerField
      FieldName = 'Moeda'
      Required = True
    end
    object QrPediVdaSituacao: TIntegerField
      FieldName = 'Situacao'
      Required = True
    end
    object QrPediVdaTabelaPrc: TIntegerField
      FieldName = 'TabelaPrc'
      Required = True
    end
    object QrPediVdaMotivoSit: TIntegerField
      FieldName = 'MotivoSit'
      Required = True
    end
    object QrPediVdaLoteProd: TIntegerField
      FieldName = 'LoteProd'
      Required = True
    end
    object QrPediVdaPedidoCli: TWideStringField
      FieldName = 'PedidoCli'
    end
    object QrPediVdaFretePor: TSmallintField
      FieldName = 'FretePor'
      Required = True
    end
    object QrPediVdaTransporta: TIntegerField
      FieldName = 'Transporta'
      Required = True
    end
    object QrPediVdaRedespacho: TIntegerField
      FieldName = 'Redespacho'
      Required = True
    end
    object QrPediVdaRegrFiscal: TIntegerField
      FieldName = 'RegrFiscal'
      Required = True
    end
    object QrPediVdaDesoAces_V: TFloatField
      FieldName = 'DesoAces_V'
      Required = True
    end
    object QrPediVdaDesoAces_P: TFloatField
      FieldName = 'DesoAces_P'
      Required = True
    end
    object QrPediVdaFrete_V: TFloatField
      FieldName = 'Frete_V'
      Required = True
    end
    object QrPediVdaFrete_P: TFloatField
      FieldName = 'Frete_P'
      Required = True
    end
    object QrPediVdaSeguro_V: TFloatField
      FieldName = 'Seguro_V'
      Required = True
    end
    object QrPediVdaSeguro_P: TFloatField
      FieldName = 'Seguro_P'
      Required = True
    end
    object QrPediVdaTotalQtd: TFloatField
      FieldName = 'TotalQtd'
      Required = True
    end
    object QrPediVdaTotal_Vlr: TFloatField
      FieldName = 'Total_Vlr'
      Required = True
    end
    object QrPediVdaTotal_Des: TFloatField
      FieldName = 'Total_Des'
      Required = True
    end
    object QrPediVdaTotal_Tot: TFloatField
      FieldName = 'Total_Tot'
      Required = True
    end
    object QrPediVdaObserva: TWideMemoField
      FieldName = 'Observa'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrPediVdaRepresen: TIntegerField
      FieldName = 'Represen'
      Required = True
    end
    object QrPediVdaComisFat: TFloatField
      FieldName = 'ComisFat'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediVdaComisRec: TFloatField
      FieldName = 'ComisRec'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediVdaCartEmis: TIntegerField
      FieldName = 'CartEmis'
      Required = True
    end
    object QrPediVdaAFP_Sit: TSmallintField
      FieldName = 'AFP_Sit'
      Required = True
    end
    object QrPediVdaAFP_Per: TFloatField
      FieldName = 'AFP_Per'
      Required = True
    end
    object QrPediVdaMedDDSimpl: TFloatField
      FieldName = 'MedDDSimpl'
    end
    object QrPediVdaMedDDReal: TFloatField
      FieldName = 'MedDDReal'
    end
    object QrPediVdaValLiq: TFloatField
      FieldName = 'ValLiq'
      Required = True
    end
    object QrPediVdaQuantP: TFloatField
      FieldName = 'QuantP'
      Required = True
    end
    object QrPediVdaFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrPediVdaNOMETABEPRCCAD: TWideStringField
      FieldName = 'NOMETABEPRCCAD'
      Size = 50
    end
    object QrPediVdaNOMEMOEDA: TWideStringField
      FieldName = 'NOMEMOEDA'
      Size = 30
    end
    object QrPediVdaNOMEEMP: TWideStringField
      FieldName = 'NOMEEMP'
      Size = 100
    end
    object QrPediVdaNOMEACC: TWideStringField
      FieldName = 'NOMEACC'
      Size = 100
    end
    object QrPediVdaNOMETRANSP: TWideStringField
      FieldName = 'NOMETRANSP'
      Size = 100
    end
    object QrPediVdaNOMEREDESP: TWideStringField
      FieldName = 'NOMEREDESP'
      Size = 100
    end
    object QrPediVdaNOMEFISREGCAD: TWideStringField
      FieldName = 'NOMEFISREGCAD'
      Size = 50
    end
    object QrPediVdaNOMEMODELONF: TWideStringField
      FieldName = 'NOMEMODELONF'
      Size = 100
    end
    object QrPediVdaNOMECARTEMIS: TWideStringField
      FieldName = 'NOMECARTEMIS'
      Size = 100
    end
    object QrPediVdaNOMECONDICAOPG: TWideStringField
      FieldName = 'NOMECONDICAOPG'
      Size = 50
    end
    object QrPediVdaNOMEMOTIVO: TWideStringField
      FieldName = 'NOMEMOTIVO'
      Size = 50
    end
    object QrPediVdaCODUSU_ACC: TIntegerField
      FieldName = 'CODUSU_ACC'
      Required = True
    end
    object QrPediVdaCODUSU_TRA: TIntegerField
      FieldName = 'CODUSU_TRA'
      Required = True
    end
    object QrPediVdaCODUSU_RED: TIntegerField
      FieldName = 'CODUSU_RED'
      Required = True
    end
    object QrPediVdaCODUSU_MOT: TIntegerField
      FieldName = 'CODUSU_MOT'
      Required = True
    end
    object QrPediVdaCODUSU_TPC: TIntegerField
      FieldName = 'CODUSU_TPC'
      Required = True
    end
    object QrPediVdaCODUSU_MDA: TIntegerField
      FieldName = 'CODUSU_MDA'
      Required = True
    end
    object QrPediVdaCODUSU_PPC: TIntegerField
      FieldName = 'CODUSU_PPC'
      Required = True
    end
    object QrPediVdaCODUSU_FRC: TIntegerField
      FieldName = 'CODUSU_FRC'
      Required = True
    end
    object QrPediVdaMODELO_NF: TIntegerField
      FieldName = 'MODELO_NF'
      Required = True
    end
  end
  object DsPediVda: TDataSource
    DataSet = QrPediVda
    Left = 32
    Top = 4
  end
  object QrCli: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrCliAfterOpen
    BeforeClose = QrCliBeforeClose
    OnCalcFields = QrCliCalcFields
    SQL.Strings = (
      'SELECT en.Codigo, Tipo, CodUsu,'
      
        'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome     END NOM' +
        'E_ENT, '
      
        'CASE WHEN en.Tipo=0 THEN en.CNPJ        ELSE en.CPF      END CNP' +
        'J_CPF, '
      
        'CASE WHEN en.Tipo=0 THEN en.IE          ELSE en.RG       END IE_' +
        'RG, '
      
        'CASE WHEN en.Tipo=0 THEN en.ERua        ELSE en.PRua     END RUA' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.ENumero     ELSE en.PNumero  END NUM' +
        'ERO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECompl      ELSE en.PCompl   END COM' +
        'PL, '
      
        'CASE WHEN en.Tipo=0 THEN en.EBairro     ELSE en.PBairro  END BAI' +
        'RRO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECidade     ELSE en.PCidade  END CID' +
        'ADE, '
      
        'CASE WHEN en.Tipo=0 THEN lle.Nome       ELSE llp.Nome    END NOM' +
        'ELOGRAD, '
      
        'CASE WHEN en.Tipo=0 THEN ufe.Nome       ELSE ufp.Nome    END NOM' +
        'EUF, '
      
        'CASE WHEN en.Tipo=0 THEN en.EPais       ELSE en.PPais    END Pai' +
        's, '
      
        'CASE WHEN en.Tipo=0 THEN en.ELograd     ELSE en.PLograd  END Log' +
        'rad, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECEP        ELSE en.PCEP     END CEP' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.EEndeRef    ELSE en.PEndeRef END END' +
        'EREF, '
      
        'CASE WHEN en.Tipo=0 THEN en.ETe1        ELSE en.PTe1     END TE1' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.EFax        ELSE en.PFax     END FAX' +
        ' '
      'FROM entidades en '
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF'
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF'
      'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd'
      'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd'
      'WHERE en.Codigo=:P0')
    Left = 64
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCliE_ALL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_ALL'
      Size = 256
      Calculated = True
    end
    object QrCliCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrCliNOME_TIPO_DOC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_TIPO_DOC'
      Size = 10
      Calculated = True
    end
    object QrCliTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrCliFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Size = 40
      Calculated = True
    end
    object QrCliNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 30
      Calculated = True
    end
    object QrCliCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Calculated = True
    end
    object QrCliCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCliTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrCliCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrCliNOME_ENT: TWideStringField
      FieldName = 'NOME_ENT'
      Size = 100
    end
    object QrCliCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrCliIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrCliRUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrCliNUMERO: TLargeintField
      FieldName = 'NUMERO'
    end
    object QrCliCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrCliBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrCliCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrCliNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrCliNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrCliPais: TWideStringField
      FieldName = 'Pais'
    end
    object QrCliLograd: TLargeintField
      FieldName = 'Lograd'
    end
    object QrCliCEP: TLargeintField
      FieldName = 'CEP'
    end
    object QrCliENDEREF: TWideStringField
      FieldName = 'ENDEREF'
      Size = 100
    end
    object QrCliTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrCliFAX: TWideStringField
      FieldName = 'FAX'
    end
  end
  object DsCli: TDataSource
    DataSet = QrCli
    Left = 92
    Top = 4
  end
  object QrEntrega: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEntregaCalcFields
    SQL.Strings = (
      'SELECT'
      'en.LRua     RUA,'
      'en.LNumero  NUMERO,'
      'en.LCompl   COMPL,'
      'en.LBairro  BAIRRO,'
      'en.LCidade  CIDADE,'
      'lll.Nome    NOMELOGRAD,'
      'ufl.Nome    NOMEUF,'
      'en.LPais    Pais,'
      'en.LLograd  Lograd,'
      'en.LCEP     CEP,'
      'en.LEndeRef ENDEREF,'
      'en.LTel     TE1,'
      'en.LFax     FAX'
      'FROM entidades en'
      'LEFT JOIN ufs ufl ON ufl.Codigo=en.LUF'
      'LEFT JOIN listalograd lll ON lll.Codigo=en.LLograd'
      'WHERE en.Codigo=:P0'
      ''
      '')
    Left = 124
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntregaE_ALL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_ALL'
      Size = 256
      Calculated = True
    end
    object QrEntregaCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntregaNOME_TIPO_DOC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_TIPO_DOC'
      Size = 10
      Calculated = True
    end
    object QrEntregaTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrEntregaFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Size = 40
      Calculated = True
    end
    object QrEntregaNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntregaCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Calculated = True
    end
    object QrEntregaRUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrEntregaNUMERO: TIntegerField
      FieldName = 'NUMERO'
    end
    object QrEntregaCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrEntregaBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrEntregaCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrEntregaNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrEntregaNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Required = True
      Size = 2
    end
    object QrEntregaPais: TWideStringField
      FieldName = 'Pais'
    end
    object QrEntregaLograd: TSmallintField
      FieldName = 'Lograd'
      Required = True
    end
    object QrEntregaCEP: TIntegerField
      FieldName = 'CEP'
    end
    object QrEntregaENDEREF: TWideStringField
      FieldName = 'ENDEREF'
      Size = 100
    end
    object QrEntregaTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrEntregaFAX: TWideStringField
      FieldName = 'FAX'
    end
  end
  object DsEntrega: TDataSource
    DataSet = QrEntrega
    Left = 152
    Top = 4
  end
end
