object FmStqBalAnt: TFmStqBalAnt
  Left = 0
  Top = 0
  Caption = 'Aguarde...'
  ClientHeight = 124
  ClientWidth = 389
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnActivate = FormActivate
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 389
    Height = 48
    Align = alTop
    Caption = 'Aguarde...'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 0
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 387
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object PB1: TProgressBar
    Left = 0
    Top = 48
    Width = 389
    Height = 17
    Align = alTop
    Step = 1
    TabOrder = 1
  end
  object Panel1: TPanel
    Left = 0
    Top = 65
    Width = 389
    Height = 59
    Align = alClient
    TabOrder = 2
    object StaticText1: TStaticText
      Left = 1
      Top = 1
      Width = 387
      Height = 57
      Align = alClient
      Alignment = taCenter
      BevelInner = bvNone
      Caption = 'Verificando saldos anteriores...'
      TabOrder = 0
    end
  end
  object QrItens: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT sbc.StqCenCad, sbi.GraGruX'
      'FROM stqbalits sbi'
      'LEFT JOIN stqbalcad sbc ON sbc.Codigo=sbi.Codigo'
      'WHERE sbi.Codigo=:P0'
      '')
    Left = 4
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrItensGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrItensStqCenCad: TIntegerField
      FieldName = 'StqCenCad'
    end
  end
  object QrSoma: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Qtde) Qtde'
      'FROM stqmovitsa'
      'WHERE StqCenCad=:P0'
      'AND GraGruX=:P1'
      '')
    Left = 32
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSomaQtde: TFloatField
      FieldName = 'Qtde'
    end
  end
end
