object FmPediVda: TFmPediVda
  Left = 368
  Top = 194
  Caption = 'PED-VENDA-001 :: Pedidos de Venda'
  ClientHeight = 614
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 566
    Align = alClient
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object PainelControle: TPanel
      Left = 1
      Top = 517
      Width = 1006
      Height = 48
      Align = alBottom
      TabOrder = 0
      object LaRegistro: TdmkLabel
        Left = 173
        Top = 1
        Width = 26
        Height = 13
        Align = alClient
        Caption = '[N]: 0'
        UpdType = utYes
        SQLType = stNil
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 172
        Height = 46
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
          NumGlyphs = 2
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
          NumGlyphs = 2
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
          NumGlyphs = 2
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
          NumGlyphs = 2
        end
      end
      object Panel3: TPanel
        Left = 324
        Top = 1
        Width = 681
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object BtItens: TBitBtn
          Tag = 10054
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Itens'
          Enabled = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtItensClick
          NumGlyphs = 2
        end
        object BtPedidos: TBitBtn
          Tag = 303
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Pedido'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtPedidosClick
          NumGlyphs = 2
        end
        object Panel2: TPanel
          Left = 572
          Top = 0
          Width = 109
          Height = 46
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 2
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
            NumGlyphs = 2
          end
        end
        object BtVisual: TBitBtn
          Tag = 338
          Left = 280
          Top = 4
          Width = 90
          Height = 40
          Caption = '&Visual'
          TabOrder = 3
          Visible = False
          OnClick = BtVisualClick
        end
        object BitBtn1: TBitBtn
          Tag = 5
          Left = 372
          Top = 4
          Width = 90
          Height = 40
          Caption = '&Pesquisa'
          TabOrder = 4
          Visible = False
          OnClick = BitBtn1Click
        end
        object BtRecalcula: TBitBtn
          Left = 464
          Top = 4
          Width = 90
          Height = 40
          Caption = '&Gravar'
          TabOrder = 5
          OnClick = BtRecalculaClick
        end
        object BtCustom: TBitBtn
          Tag = 10055
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Customiz.'
          Enabled = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 6
          OnClick = BtCustomClick
          NumGlyphs = 2
        end
      end
    end
    object GroupBox9: TGroupBox
      Left = 1
      Top = 1
      Width = 1006
      Height = 96
      Align = alTop
      TabOrder = 1
      object Label56: TLabel
        Left = 8
        Top = 20
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label57: TLabel
        Left = 8
        Top = 44
        Width = 35
        Height = 13
        Caption = 'Cliente:'
      end
      object Label58: TLabel
        Left = 8
        Top = 72
        Width = 95
        Height = 13
        Caption = 'Situa'#231#227'o do pedido:'
      end
      object Label59: TLabel
        Left = 280
        Top = 72
        Width = 93
        Height = 13
        Caption = 'Motivo da situa'#231#227'o:'
      end
      object Label60: TLabel
        Left = 736
        Top = 72
        Width = 68
        Height = 13
        Caption = 'Data inclus'#227'o:'
        Enabled = False
      end
      object Label61: TLabel
        Left = 920
        Top = 72
        Width = 14
        Height = 13
        Caption = 'ID:'
        Enabled = False
      end
      object Label62: TLabel
        Left = 852
        Top = 20
        Width = 43
        Height = 13
        Caption = 'N'#250'mero: '
      end
      object DBEdit1: TDBEdit
        Left = 916
        Top = 12
        Width = 80
        Height = 27
        DataField = 'CodUsu'
        DataSource = DsPediVda
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
      end
      object DBEdit3: TDBEdit
        Left = 56
        Top = 16
        Width = 53
        Height = 21
        DataField = 'Filial'
        DataSource = DsPediVda
        TabOrder = 1
      end
      object DBEdit2: TDBEdit
        Left = 112
        Top = 16
        Width = 737
        Height = 21
        DataField = 'NOMEEMP'
        DataSource = DsPediVda
        TabOrder = 2
      end
      object DBEdit4: TDBEdit
        Left = 56
        Top = 40
        Width = 53
        Height = 21
        DataField = 'CODUSU_CLI'
        DataSource = DsPediVda
        TabOrder = 3
      end
      object DBEdit5: TDBEdit
        Left = 112
        Top = 40
        Width = 533
        Height = 21
        DataField = 'NOMECLI'
        DataSource = DsPediVda
        TabOrder = 4
      end
      object DBEdit30: TDBEdit
        Left = 648
        Top = 40
        Width = 313
        Height = 21
        DataField = 'CIDADE'
        DataSource = DmPediVda.DsClientes
        Enabled = False
        TabOrder = 5
      end
      object DBEdit31: TDBEdit
        Left = 964
        Top = 40
        Width = 32
        Height = 21
        DataField = 'NOMEUF'
        DataSource = DmPediVda.DsClientes
        Enabled = False
        TabOrder = 6
      end
      object DBEdit20: TDBEdit
        Left = 108
        Top = 68
        Width = 21
        Height = 21
        DataField = 'Situacao'
        DataSource = DsPediVda
        TabOrder = 7
      end
      object DBEdit29: TDBEdit
        Left = 132
        Top = 68
        Width = 145
        Height = 21
        DataField = 'NOMESITUACAO'
        DataSource = DsPediVda
        TabOrder = 8
      end
      object DBEdit8: TDBEdit
        Left = 808
        Top = 68
        Width = 105
        Height = 21
        DataField = 'DtaInclu'
        DataSource = DsPediVda
        TabOrder = 9
      end
      object DBEdit22: TDBEdit
        Left = 936
        Top = 68
        Width = 60
        Height = 21
        DataField = 'Codigo'
        DataSource = DsPediVda
        TabOrder = 10
      end
      object DBEdit27: TDBEdit
        Left = 380
        Top = 68
        Width = 56
        Height = 21
        DataField = 'CODUSU_MOT'
        DataSource = DsPediVda
        TabOrder = 11
      end
      object DBEdit32: TDBEdit
        Left = 440
        Top = 68
        Width = 293
        Height = 21
        DataField = 'NOMEMOTIVO'
        DataSource = DsPediVda
        TabOrder = 12
      end
    end
    object PageControl1: TPageControl
      Left = 1
      Top = 133
      Width = 1006
      Height = 364
      ActivePage = TabSheet11
      Align = alTop
      TabOrder = 2
      OnChange = PageControl1Change
      object TabSheet1: TTabSheet
        Caption = ' Dados do pedido '
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel4: TPanel
          Left = 0
          Top = 0
          Width = 998
          Height = 336
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object GroupBox10: TGroupBox
            Left = 0
            Top = 0
            Width = 998
            Height = 36
            Align = alTop
            TabOrder = 0
            object Label54: TLabel
              Left = 8
              Top = 12
              Width = 67
              Height = 13
              Caption = 'Data emiss'#227'o:'
            end
            object Label55: TLabel
              Left = 188
              Top = 12
              Width = 71
              Height = 13
              Caption = 'Data chegada:'
            end
            object Label63: TLabel
              Left = 404
              Top = 12
              Width = 70
              Height = 13
              Caption = 'Pedido cliente:'
            end
            object Label64: TLabel
              Left = 636
              Top = 12
              Width = 50
              Height = 13
              Caption = 'Prioridade:'
            end
            object Label65: TLabel
              Left = 752
              Top = 12
              Width = 101
              Height = 13
              Caption = 'Previs'#227'o entrega:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object DBEdit9: TDBEdit
              Left = 80
              Top = 7
              Width = 105
              Height = 21
              DataField = 'DtaEmiss'
              DataSource = DsPediVda
              TabOrder = 0
            end
            object DBEdit10: TDBEdit
              Left = 264
              Top = 7
              Width = 105
              Height = 21
              DataField = 'DtaEntra'
              DataSource = DsPediVda
              TabOrder = 1
            end
            object DBEdit12: TDBEdit
              Left = 690
              Top = 7
              Width = 53
              Height = 21
              DataField = 'Prioridade'
              DataSource = DsPediVda
              TabOrder = 2
            end
            object DBEdit11: TDBEdit
              Left = 856
              Top = 7
              Width = 129
              Height = 21
              DataField = 'DtaPrevi'
              DataSource = DsPediVda
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 3
            end
            object DBEdit19: TDBEdit
              Left = 480
              Top = 7
              Width = 145
              Height = 21
              DataField = 'PedidoCli'
              DataSource = DsPediVda
              TabOrder = 4
            end
          end
          object GroupBox11: TGroupBox
            Left = 0
            Top = 36
            Width = 998
            Height = 60
            Align = alTop
            TabOrder = 1
            object Label1: TLabel
              Left = 8
              Top = 13
              Width = 81
              Height = 13
              Caption = 'Tabela de pre'#231'o:'
            end
            object Label3: TLabel
              Left = 8
              Top = 37
              Width = 119
              Height = 13
              Caption = 'Condi'#231#227'o de pagamento:'
            end
            object Label4: TLabel
              Left = 530
              Top = 13
              Width = 36
              Height = 13
              Caption = 'Moeda:'
            end
            object Label5: TLabel
              Left = 530
              Top = 36
              Width = 39
              Height = 13
              Caption = 'Carteira:'
            end
            object Label10: TLabel
              Left = 848
              Top = 13
              Width = 78
              Height = 13
              Caption = '% Fatura parcial:'
            end
            object DBEdit21: TDBEdit
              Left = 128
              Top = 8
              Width = 56
              Height = 21
              DataField = 'CODUSU_TPC'
              DataSource = DsPediVda
              TabOrder = 0
            end
            object DBEdit24: TDBEdit
              Left = 188
              Top = 8
              Width = 333
              Height = 21
              DataField = 'NOMETABEPRCCAD'
              DataSource = DsPediVda
              TabOrder = 1
            end
            object DBEdit18: TDBEdit
              Left = 574
              Top = 8
              Width = 44
              Height = 21
              DataField = 'CODUSU_MDA'
              DataSource = DsPediVda
              TabOrder = 2
            end
            object DBEdit25: TDBEdit
              Left = 620
              Top = 8
              Width = 130
              Height = 21
              DataField = 'NOMEMOEDA'
              DataSource = DsPediVda
              TabOrder = 3
            end
            object DBEdit17: TDBEdit
              Left = 128
              Top = 32
              Width = 56
              Height = 21
              DataField = 'CODUSU_PPC'
              DataSource = DsPediVda
              TabOrder = 4
            end
            object DBEdit26: TDBEdit
              Left = 188
              Top = 32
              Width = 333
              Height = 21
              DataField = 'NOMECONDICAOPG'
              DataSource = DsPediVda
              TabOrder = 5
            end
            object DBEdit6: TDBEdit
              Left = 574
              Top = 32
              Width = 44
              Height = 21
              DataField = 'CartEmis'
              DataSource = DsPediVda
              TabOrder = 6
            end
            object DBEdit7: TDBEdit
              Left = 620
              Top = 32
              Width = 365
              Height = 21
              DataField = 'NOMECARTEMIS'
              DataSource = DsPediVda
              TabOrder = 7
            end
            object DBEdit28: TDBEdit
              Left = 928
              Top = 8
              Width = 56
              Height = 21
              DataField = 'AFP_Per'
              DataSource = DsPediVda
              TabOrder = 8
            end
            object DBCheckBox1: TDBCheckBox
              Left = 756
              Top = 11
              Width = 97
              Height = 17
              Caption = 'Fatura parcial?'
              DataField = 'AFP_Sit'
              DataSource = DsPediVda
              TabOrder = 9
              ValueChecked = '1'
              ValueUnchecked = '0'
            end
          end
          object GroupBox8: TGroupBox
            Left = 0
            Top = 96
            Width = 998
            Height = 36
            Align = alTop
            TabOrder = 2
            object Label11: TLabel
              Left = 8
              Top = 12
              Width = 87
              Height = 13
              Caption = 'Lote de produ'#231#227'o:'
              FocusControl = DBEdit33
            end
            object DBEdit33: TDBEdit
              Left = 100
              Top = 8
              Width = 92
              Height = 21
              TabStop = False
              Color = clGradientInactiveCaption
              DataField = 'LoteProd'
              DataSource = DsPediVda
              TabOrder = 0
            end
          end
          object GroupBox14: TGroupBox
            Left = 0
            Top = 132
            Width = 998
            Height = 106
            Align = alTop
            TabOrder = 3
            object Label51: TLabel
              Left = 8
              Top = 13
              Width = 45
              Height = 13
              Caption = 'Frete por:'
            end
            object Label52: TLabel
              Left = 8
              Top = 36
              Width = 75
              Height = 13
              Caption = 'Transportadora:'
            end
            object Label53: TLabel
              Left = 8
              Top = 61
              Width = 64
              Height = 13
              Caption = 'Redespacho:'
            end
            object Label66: TLabel
              Left = 8
              Top = 84
              Width = 117
              Height = 13
              Caption = '% Despesas acess'#243'rias: '
            end
            object Label67: TLabel
              Left = 176
              Top = 84
              Width = 115
              Height = 13
              Caption = '$ Despesas acess'#243'rias: '
            end
            object Label68: TLabel
              Left = 452
              Top = 84
              Width = 38
              Height = 13
              Caption = '% Frete:'
            end
            object Label69: TLabel
              Left = 456
              Top = 13
              Width = 92
              Height = 13
              Caption = 'Local de entrega: >'
            end
            object Label70: TLabel
              Left = 552
              Top = 84
              Width = 36
              Height = 13
              Caption = '$ Frete:'
            end
            object Label71: TLabel
              Left = 736
              Top = 84
              Width = 48
              Height = 13
              Caption = '% Seguro:'
            end
            object Label72: TLabel
              Left = 844
              Top = 84
              Width = 46
              Height = 13
              Caption = '$ Seguro:'
            end
            object DBEdit16: TDBEdit
              Left = 56
              Top = 8
              Width = 21
              Height = 21
              DataField = 'FretePor'
              DataSource = DsPediVda
              TabOrder = 0
            end
            object DBEdit23: TDBEdit
              Left = 80
              Top = 8
              Width = 373
              Height = 21
              DataField = 'NOMEFRETEPOR'
              DataSource = DsPediVda
              TabOrder = 1
            end
            object Memo2: TMemo
              Left = 552
              Top = 8
              Width = 433
              Height = 69
              TabStop = False
              ReadOnly = True
              TabOrder = 2
            end
            object DBEdit34: TDBEdit
              Left = 88
              Top = 32
              Width = 56
              Height = 21
              DataField = 'CODUSU_TRA'
              DataSource = DsPediVda
              TabOrder = 3
            end
            object DBEdit35: TDBEdit
              Left = 88
              Top = 56
              Width = 56
              Height = 21
              DataField = 'CODUSU_RED'
              DataSource = DsPediVda
              TabOrder = 4
            end
            object DBEdit36: TDBEdit
              Left = 148
              Top = 32
              Width = 397
              Height = 21
              DataField = 'NOMETRANSP'
              DataSource = DsPediVda
              TabOrder = 5
            end
            object DBEdit37: TDBEdit
              Left = 148
              Top = 56
              Width = 397
              Height = 21
              DataField = 'NOMEREDESP'
              DataSource = DsPediVda
              TabOrder = 6
            end
            object DBEdit38: TDBEdit
              Left = 300
              Top = 80
              Width = 80
              Height = 21
              DataField = 'DesoAces_V'
              DataSource = DsPediVda
              TabOrder = 7
            end
            object DBEdit39: TDBEdit
              Left = 124
              Top = 80
              Width = 48
              Height = 21
              DataField = 'DesoAces_P'
              DataSource = DsPediVda
              TabOrder = 8
            end
            object DBEdit40: TDBEdit
              Left = 600
              Top = 80
              Width = 80
              Height = 21
              DataField = 'Frete_V'
              DataSource = DsPediVda
              TabOrder = 9
            end
            object DBEdit41: TDBEdit
              Left = 496
              Top = 80
              Width = 48
              Height = 21
              DataField = 'Frete_P'
              DataSource = DsPediVda
              TabOrder = 10
            end
            object DBEdit42: TDBEdit
              Left = 904
              Top = 80
              Width = 80
              Height = 21
              DataField = 'Seguro_V'
              DataSource = DsPediVda
              TabOrder = 11
            end
            object DBEdit43: TDBEdit
              Left = 788
              Top = 80
              Width = 48
              Height = 21
              DataField = 'Seguro_P'
              DataSource = DsPediVda
              TabOrder = 12
            end
          end
          object GroupBox12: TGroupBox
            Left = 0
            Top = 238
            Width = 998
            Height = 40
            Align = alTop
            TabOrder = 4
            object Label16: TLabel
              Left = 12
              Top = 16
              Width = 73
              Height = 13
              Caption = 'Representante:'
            end
            object Label17: TLabel
              Left = 640
              Top = 15
              Width = 117
              Height = 13
              Caption = '% comiss'#227'o faturamento:'
            end
            object Label18: TLabel
              Left = 816
              Top = 15
              Width = 119
              Height = 13
              Caption = '% comiss'#227'o recebimento:'
            end
            object DBEdit44: TDBEdit
              Left = 88
              Top = 12
              Width = 57
              Height = 21
              DataField = 'CODUSU_ACC'
              DataSource = DsPediVda
              TabOrder = 0
            end
            object DBEdit45: TDBEdit
              Left = 148
              Top = 12
              Width = 489
              Height = 21
              DataField = 'NOMEACC'
              DataSource = DsPediVda
              TabOrder = 1
            end
            object DBEdit46: TDBEdit
              Left = 760
              Top = 12
              Width = 48
              Height = 21
              DataField = 'ComisFat'
              DataSource = DsPediVda
              TabOrder = 2
            end
            object DBEdit47: TDBEdit
              Left = 936
              Top = 12
              Width = 48
              Height = 21
              DataField = 'ComisRec'
              DataSource = DsPediVda
              TabOrder = 3
            end
          end
          object GroupBox15: TGroupBox
            Left = 0
            Top = 278
            Width = 998
            Height = 43
            Align = alTop
            Caption = ' Fiscal:'
            TabOrder = 5
            object Label34: TLabel
              Left = 12
              Top = 20
              Width = 73
              Height = 13
              Caption = 'Movimenta'#231#227'o:'
            end
            object Label35: TLabel
              Left = 640
              Top = 20
              Width = 55
              Height = 13
              Caption = 'Modelo NF:'
            end
            object DBEdit48: TDBEdit
              Left = 88
              Top = 16
              Width = 57
              Height = 21
              DataField = 'CODUSU_FRC'
              DataSource = DsPediVda
              TabOrder = 0
            end
            object DBEdit49: TDBEdit
              Left = 148
              Top = 16
              Width = 489
              Height = 21
              DataField = 'NOMEFISREGCAD'
              DataSource = DsPediVda
              TabOrder = 1
            end
            object DBEdit50: TDBEdit
              Left = 700
              Top = 16
              Width = 283
              Height = 21
              DataField = 'NOMEMODELONF'
              DataSource = DsPediVda
              TabOrder = 2
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' Itens do pedido '
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGGru: TdmkDBGrid
          Left = 0
          Top = 0
          Width = 445
          Height = 336
          Align = alLeft
          Columns = <
            item
              Expanded = False
              FieldName = 'CodUsu'
              Title.Caption = 'C'#243'digo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Descri'#231#227'o'
              Width = 220
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QuantP'
              Title.Caption = 'Qtd pedido'
              Width = 58
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValLiq'
              Title.Caption = 'Val. liq.'
              Visible = True
            end>
          Color = clWindow
          DataSource = DsPediVdaGru
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'CodUsu'
              Title.Caption = 'C'#243'digo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Descri'#231#227'o'
              Width = 220
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QuantP'
              Title.Caption = 'Qtd pedido'
              Width = 58
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValLiq'
              Title.Caption = 'Val. liq.'
              Visible = True
            end>
        end
        object PageControl3: TPageControl
          Left = 445
          Top = 0
          Width = 553
          Height = 336
          ActivePage = TabSheet4
          Align = alClient
          MultiLine = True
          TabOrder = 1
          object TabSheet5: TTabSheet
            Caption = ' Quantidade '
            ImageIndex = 2
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object GradeQ: TStringGrid
              Left = 0
              Top = 0
              Width = 545
              Height = 291
              Align = alClient
              ColCount = 2
              DefaultColWidth = 65
              DefaultRowHeight = 18
              FixedCols = 0
              RowCount = 2
              FixedRows = 0
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goTabs]
              ParentFont = False
              TabOrder = 0
              OnDblClick = GradeQDblClick
              OnDrawCell = GradeQDrawCell
            end
            object StaticText1: TStaticText
              Left = 0
              Top = 291
              Width = 507
              Height = 17
              Align = alBottom
              Alignment = taCenter
              BorderStyle = sbsSunken
              Caption = 
                'D'#234' um duplo clique na c'#233'lula, coluna ou linha correspondente par' +
                'a alterar a quantidade.'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 1
              Visible = False
            end
          end
          object TabSheet4: TTabSheet
            Caption = ' Valor unit'#225'rio '
            ImageIndex = 5
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object GradeF: TStringGrid
              Left = 0
              Top = 0
              Width = 545
              Height = 308
              Align = alClient
              ColCount = 2
              DefaultColWidth = 65
              DefaultRowHeight = 18
              FixedCols = 0
              RowCount = 2
              FixedRows = 0
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goTabs]
              ParentFont = False
              TabOrder = 0
              OnDrawCell = GradeFDrawCell
            end
          end
          object TabSheet6: TTabSheet
            Caption = ' Desconto '
            ImageIndex = 6
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object GradeD: TStringGrid
              Left = 0
              Top = 0
              Width = 545
              Height = 291
              Align = alClient
              ColCount = 2
              DefaultColWidth = 65
              DefaultRowHeight = 18
              FixedCols = 0
              RowCount = 2
              FixedRows = 0
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goTabs]
              ParentFont = False
              TabOrder = 0
              OnDrawCell = GradeDDrawCell
            end
            object StaticText3: TStaticText
              Left = 0
              Top = 291
              Width = 544
              Height = 17
              Align = alBottom
              Alignment = taCenter
              BorderStyle = sbsSunken
              Caption = 
                'D'#234' um duplo clique na c'#233'lula, coluna ou linha correspondente par' +
                'a incluir / alterar o desconto.'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 1
            end
          end
          object TabSheet7: TTabSheet
            Caption = ' Valor l'#237'quido'
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object GradeV: TStringGrid
              Left = 0
              Top = 0
              Width = 545
              Height = 308
              Align = alClient
              ColCount = 2
              DefaultColWidth = 65
              DefaultRowHeight = 18
              FixedCols = 0
              RowCount = 2
              FixedRows = 0
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goTabs]
              ParentFont = False
              TabOrder = 0
              OnDrawCell = GradeVDrawCell
            end
          end
          object TabSheet8: TTabSheet
            Caption = ' C'#243'digos '
            ImageIndex = 3
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object GradeC: TStringGrid
              Left = 0
              Top = 0
              Width = 545
              Height = 291
              Align = alClient
              ColCount = 1
              DefaultColWidth = 65
              DefaultRowHeight = 18
              FixedCols = 0
              RowCount = 1
              FixedRows = 0
              TabOrder = 0
              OnDrawCell = GradeCDrawCell
              RowHeights = (
                18)
            end
            object StaticText6: TStaticText
              Left = 0
              Top = 291
              Width = 502
              Height = 17
              Align = alBottom
              Alignment = taCenter
              BorderStyle = sbsSunken
              Caption = 
                'Para gerar o c'#243'digo, clique na c'#233'lula, coluna ou linha correspon' +
                'dente na guia "Ativos".'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 1
              Visible = False
            end
          end
          object TabSheet9: TTabSheet
            Caption = ' Ativos '
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object GradeA: TStringGrid
              Left = 0
              Top = 0
              Width = 545
              Height = 291
              Align = alClient
              ColCount = 2
              DefaultColWidth = 65
              DefaultRowHeight = 18
              FixedCols = 0
              RowCount = 2
              FixedRows = 0
              TabOrder = 0
              OnDrawCell = GradeADrawCell
              RowHeights = (
                18
                18)
            end
            object StaticText2: TStaticText
              Left = 0
              Top = 291
              Width = 475
              Height = 17
              Align = alBottom
              Alignment = taCenter
              BorderStyle = sbsSunken
              Caption = 
                'Clique na c'#233'lula, coluna ou linha correspondente para ativar / d' +
                'esativar o produto.'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 1
              Visible = False
            end
          end
          object TabSheet10: TTabSheet
            Caption = ' X '
            ImageIndex = 6
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object GradeX: TStringGrid
              Left = 0
              Top = 0
              Width = 545
              Height = 308
              Align = alClient
              ColCount = 2
              DefaultColWidth = 65
              DefaultRowHeight = 18
              FixedCols = 0
              RowCount = 2
              FixedRows = 0
              TabOrder = 0
              OnDrawCell = GradeXDrawCell
              RowHeights = (
                18
                18)
            end
          end
        end
      end
      object TabSheet11: TTabSheet
        Caption = ' Dados dos itens customizados '
        ImageIndex = 2
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 998
          Height = 336
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Panel8: TPanel
            Left = 0
            Top = 0
            Width = 998
            Height = 173
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object Splitter1: TSplitter
              Left = 417
              Top = 0
              Width = 5
              Height = 173
              ExplicitLeft = 453
            end
            object dmkDBGrid1: TdmkDBGrid
              Left = 0
              Top = 0
              Width = 417
              Height = 173
              Align = alLeft
              Columns = <
                item
                  Expanded = False
                  FieldName = 'CodUsu'
                  Title.Caption = 'C'#243'digo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Title.Caption = 'Descri'#231#227'o'
                  Width = 191
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'QuantP'
                  Title.Caption = 'Qtd pedido'
                  Width = 58
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValLiq'
                  Title.Caption = 'Val. liq.'
                  Visible = True
                end>
              Color = clWindow
              DataSource = DsPediVdaGru
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'CodUsu'
                  Title.Caption = 'C'#243'digo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Title.Caption = 'Descri'#231#227'o'
                  Width = 191
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'QuantP'
                  Title.Caption = 'Qtd pedido'
                  Width = 58
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValLiq'
                  Title.Caption = 'Val. liq.'
                  Visible = True
                end>
            end
            object Panel10: TPanel
              Left = 422
              Top = 0
              Width = 576
              Height = 173
              Align = alClient
              TabOrder = 1
              object DBGrid1: TDBGrid
                Left = 1
                Top = 1
                Width = 574
                Height = 171
                Align = alClient
                DataSource = DsCustomizados
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'NO_COR'
                    Title.Caption = 'Cor'
                    Width = 162
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_TAM'
                    Title.Caption = 'Tamanho'
                    Width = 56
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'MedidaC'
                    Title.Caption = 'Medida 1'
                    Width = 60
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'MedidaL'
                    Title.Caption = 'Medida 2'
                    Width = 60
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'MedidaA'
                    Title.Caption = 'Medida 3'
                    Width = 60
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'MedidaE'
                    Title.Caption = 'Medida 4'
                    Width = 60
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValLiq'
                    Title.Caption = 'Valor Liq.'
                    Width = 72
                    Visible = True
                  end>
              end
            end
          end
          object Panel9: TPanel
            Left = 0
            Top = 173
            Width = 998
            Height = 163
            Align = alClient
            TabOrder = 1
            object DBGrid2: TDBGrid
              Left = 1
              Top = 1
              Width = 996
              Height = 161
              Align = alClient
              DataSource = DsPediVdaCuz
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'NO_PARTE'
                  Title.Caption = 'Parte'
                  Width = 150
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_GRUPO'
                  Title.Caption = 'Produto'
                  Width = 180
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_COR'
                  Title.Caption = 'Cor'
                  Width = 140
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_TAM'
                  Title.Caption = 'Tamanho'
                  Width = 52
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'GraGruX'
                  Title.Caption = 'Reduzido'
                  Width = 52
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'QuantP'
                  Title.Caption = 'Quantidade'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'MedidaC_TXT'
                  Title.Caption = 'Medida 1'
                  Width = 68
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'MedidaL_TXT'
                  Title.Caption = 'Medida 2'
                  Width = 68
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'MedidaA_TXT'
                  Title.Caption = 'Medida 3'
                  Width = 68
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'MedidaE_TXT'
                  Title.Caption = 'Medida 4'
                  Width = 68
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'PrecoR'
                  Title.Caption = 'Pre'#231'o'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DescoP'
                  Title.Caption = '% Desco.'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DescoV'
                  Title.Caption = '$ Desco.'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValBru'
                  Title.Caption = 'Val. Bruto'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValLiq'
                  Title.Caption = 'Valor Liq.'
                  Visible = True
                end>
            end
          end
        end
      end
    end
    object GroupBox13: TGroupBox
      Left = 1
      Top = 97
      Width = 1006
      Height = 36
      Align = alTop
      TabOrder = 3
      object Label19: TLabel
        Left = 616
        Top = 12
        Width = 93
        Height = 13
        Caption = 'Quantidade pedida:'
        FocusControl = DBEdit13
      end
      object Label21: TLabel
        Left = 796
        Top = 12
        Width = 112
        Height = 13
        Caption = 'Valor l'#237'quido do pedido:'
        FocusControl = DBEdit14
      end
      object DBEdit13: TDBEdit
        Left = 712
        Top = 8
        Width = 77
        Height = 21
        DataField = 'QuantP'
        DataSource = DsPediVda
        TabOrder = 0
      end
      object DBEdit14: TDBEdit
        Left = 916
        Top = 8
        Width = 80
        Height = 21
        DataField = 'ValLiq'
        DataSource = DsPediVda
        TabOrder = 1
      end
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 566
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Visible = False
    object PainelConfirma: TPanel
      Left = 1
      Top = 517
      Width = 1006
      Height = 48
      Align = alBottom
      TabOrder = 2
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 8
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
        NumGlyphs = 2
      end
      object Panel1: TPanel
        Left = 897
        Top = 1
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
          NumGlyphs = 2
        end
      end
    end
    object GroupBox1: TGroupBox
      Left = 1
      Top = 1
      Width = 1006
      Height = 96
      Align = alTop
      TabOrder = 0
      object Label9: TLabel
        Left = 8
        Top = 20
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label2: TLabel
        Left = 8
        Top = 44
        Width = 35
        Height = 13
        Caption = 'Cliente:'
      end
      object Label8: TLabel
        Left = 852
        Top = 20
        Width = 61
        Height = 13
        Caption = 'N'#250'mero: [F4]'
      end
      object SpeedButton5: TSpeedButton
        Left = 622
        Top = 40
        Width = 21
        Height = 21
        OnClick = SpeedButton5Click
      end
      object Label23: TLabel
        Left = 8
        Top = 72
        Width = 95
        Height = 13
        Caption = 'Situa'#231#227'o do pedido:'
      end
      object Label26: TLabel
        Left = 280
        Top = 72
        Width = 93
        Height = 13
        Caption = 'Motivo da situa'#231#227'o:'
      end
      object Label7: TLabel
        Left = 920
        Top = 72
        Width = 14
        Height = 13
        Caption = 'ID:'
        Enabled = False
      end
      object Label6: TLabel
        Left = 736
        Top = 72
        Width = 68
        Height = 13
        Caption = 'Data inclus'#227'o:'
        Enabled = False
      end
      object SpeedButton7: TSpeedButton
        Left = 712
        Top = 67
        Width = 21
        Height = 21
        OnClick = SpeedButton7Click
      end
      object EdCliente: TdmkEditCB
        Left = 56
        Top = 40
        Width = 52
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cliente'
        UpdCampo = 'Cliente'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnChange = EdClienteChange
        DBLookupComboBox = CBCliente
      end
      object EdEmpresa: TdmkEditCB
        Left = 56
        Top = 16
        Width = 53
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBEmpresa
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 112
        Top = 16
        Width = 737
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 1
        dmkEditCB = EdEmpresa
        UpdType = utYes
      end
      object CBCliente: TdmkDBLookupComboBox
        Left = 112
        Top = 40
        Width = 509
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENT'
        ListSource = DmPediVda.DsClientes
        TabOrder = 4
        dmkEditCB = EdCliente
        QryCampo = 'Cliente'
        UpdType = utNil
      end
      object DBEdCidade: TDBEdit
        Left = 648
        Top = 40
        Width = 313
        Height = 21
        DataField = 'CIDADE'
        DataSource = DmPediVda.DsClientes
        Enabled = False
        TabOrder = 5
      end
      object EdCodUsu: TdmkEdit
        Left = 916
        Top = 12
        Width = 80
        Height = 25
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 6
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '000000'
        QryCampo = 'CodUsu'
        UpdCampo = 'CodUsu'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnKeyDown = EdCodUsuKeyDown
      end
      object DBEdNOMEUF: TDBEdit
        Left = 964
        Top = 40
        Width = 32
        Height = 21
        DataField = 'NOMEUF'
        DataSource = DmPediVda.DsClientes
        Enabled = False
        TabOrder = 6
      end
      object EdSituacao: TdmkEditCB
        Left = 112
        Top = 67
        Width = 21
        Height = 21
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Situacao'
        UpdCampo = 'Situacao'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBSituacao
      end
      object CBSituacao: TdmkDBLookupComboBox
        Left = 136
        Top = 67
        Width = 141
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DModG.DsSituacao
        TabOrder = 8
        dmkEditCB = EdSituacao
        QryCampo = 'Situacao'
        UpdType = utNil
      end
      object EdMotivoSit: TdmkEditCB
        Left = 380
        Top = 67
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBMotivoSit
      end
      object CBMotivoSit: TdmkDBLookupComboBox
        Left = 440
        Top = 67
        Width = 269
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DmPediVda.DsMotivos
        TabOrder = 10
        OnExit = CBMotivoSitExit
        dmkEditCB = EdMotivoSit
        UpdType = utNil
      end
      object EdCodigo: TdmkEdit
        Left = 940
        Top = 68
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 12
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object TPDtaInclu: TdmkEditDateTimePicker
        Left = 808
        Top = 68
        Width = 105
        Height = 21
        Date = 39789.688972615740000000
        Time = 39789.688972615740000000
        Enabled = False
        TabOrder = 11
        TabStop = False
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtaInclu'
        UpdCampo = 'DtaInclu'
        UpdType = utYes
      end
    end
    object PageControl2: TPageControl
      Left = 1
      Top = 97
      Width = 1006
      Height = 372
      ActivePage = TabSheet3
      Align = alTop
      TabOrder = 1
      object TabSheet3: TTabSheet
        Caption = ' Dados do Pedido: '
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 998
          Height = 344
          Align = alClient
          ParentBackground = False
          TabOrder = 0
          object GroupBox2: TGroupBox
            Left = 1
            Top = 1
            Width = 996
            Height = 36
            Align = alTop
            TabOrder = 0
            object Label12: TLabel
              Left = 8
              Top = 12
              Width = 67
              Height = 13
              Caption = 'Data emiss'#227'o:'
            end
            object Label13: TLabel
              Left = 188
              Top = 12
              Width = 71
              Height = 13
              Caption = 'Data chegada:'
            end
            object Label15: TLabel
              Left = 636
              Top = 12
              Width = 50
              Height = 13
              Caption = 'Prioridade:'
            end
            object Label14: TLabel
              Left = 752
              Top = 12
              Width = 101
              Height = 13
              Caption = 'Previs'#227'o entrega:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label27: TLabel
              Left = 392
              Top = 12
              Width = 70
              Height = 13
              Caption = 'Pedido cliente:'
            end
            object TPDtaEmiss: TdmkEditDateTimePicker
              Left = 80
              Top = 8
              Width = 105
              Height = 21
              Date = 39789.688972615740000000
              Time = 39789.688972615740000000
              TabOrder = 0
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              QryCampo = 'DtaEmiss'
              UpdCampo = 'DtaEmiss'
              UpdType = utYes
            end
            object TPDtaEntra: TdmkEditDateTimePicker
              Left = 264
              Top = 8
              Width = 105
              Height = 21
              Date = 39789.688972615740000000
              Time = 39789.688972615740000000
              TabOrder = 1
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              QryCampo = 'DtaEntra'
              UpdCampo = 'DtaEntra'
              UpdType = utYes
            end
            object EdPrioridade: TdmkEdit
              Left = 688
              Top = 8
              Width = 53
              Height = 21
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 2
              NoEnterToTab = False
              ValMin = '0'
              ValMax = '99'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '00'
              QryCampo = 'Prioridade'
              UpdCampo = 'Prioridade'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
            end
            object TPDtaPrevi: TdmkEditDateTimePicker
              Left = 856
              Top = 8
              Width = 129
              Height = 21
              Date = 39789.688972615740000000
              Time = 39789.688972615740000000
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 4
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              QryCampo = 'DtaPrevi'
              UpdCampo = 'DtaPrevi'
              UpdType = utYes
            end
            object EdPedidoCli: TdmkEdit
              Left = 468
              Top = 8
              Width = 145
              Height = 21
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 2
              NoEnterToTab = False
              ValMin = '0'
              ValMax = '99'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'PedidoCli'
              UpdCampo = 'PedidoCli'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
            end
          end
          object GroupBox3: TGroupBox
            Left = 1
            Top = 37
            Width = 996
            Height = 60
            Align = alTop
            TabOrder = 1
            object BtTabelaPrc: TSpeedButton
              Left = 504
              Top = 8
              Width = 21
              Height = 21
              OnClick = BtTabelaPrcClick
            end
            object LaTabelaPrc: TLabel
              Left = 8
              Top = 13
              Width = 81
              Height = 13
              Caption = 'Tabela de pre'#231'o:'
            end
            object SpeedButton9: TSpeedButton
              Left = 728
              Top = 8
              Width = 21
              Height = 21
              OnClick = SpeedButton9Click
            end
            object Label22: TLabel
              Left = 530
              Top = 13
              Width = 36
              Height = 13
              Caption = 'Moeda:'
            end
            object BtCondicaoPG: TSpeedButton
              Left = 504
              Top = 32
              Width = 21
              Height = 21
              OnClick = BtCondicaoPGClick
            end
            object Label24: TLabel
              Left = 530
              Top = 36
              Width = 39
              Height = 13
              Caption = 'Carteira:'
            end
            object LaCondicaoPG: TLabel
              Left = 8
              Top = 37
              Width = 119
              Height = 13
              Caption = 'Condi'#231#227'o de pagamento:'
            end
            object SpeedButton13: TSpeedButton
              Left = 964
              Top = 31
              Width = 21
              Height = 21
              OnClick = SpeedButton13Click
            end
            object Label49: TLabel
              Left = 848
              Top = 13
              Width = 78
              Height = 13
              Caption = '% Fatura parcial:'
            end
            object CBTabelaPrc: TdmkDBLookupComboBox
              Left = 188
              Top = 8
              Width = 313
              Height = 21
              KeyField = 'CodUsu'
              ListField = 'Nome'
              ListSource = DmPediVda.DsTabePrcCab
              TabOrder = 1
              dmkEditCB = EdTabelaPrc
              QryCampo = 'TabelaPrc'
              UpdType = utNil
            end
            object EdTabelaPrc: TdmkEditCB
              Left = 128
              Top = 8
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              OnChange = EdTabelaPrcChange
              DBLookupComboBox = CBTabelaPrc
            end
            object CBCartEmis: TdmkDBLookupComboBox
              Left = 620
              Top = 31
              Width = 341
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DmPediVda.DsCartEmis
              TabOrder = 9
              dmkEditCB = EdCartEmis
              QryCampo = 'CartEmis'
              UpdType = utNil
            end
            object CBMoeda: TdmkDBLookupComboBox
              Left = 620
              Top = 8
              Width = 105
              Height = 21
              KeyField = 'CodUsu'
              ListField = 'Nome'
              ListSource = DModG.DsCambioMda
              TabOrder = 3
              dmkEditCB = EdMoeda
              UpdType = utNil
            end
            object EdMoeda: TdmkEditCB
              Left = 574
              Top = 8
              Width = 44
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              DBLookupComboBox = CBMoeda
            end
            object EdCartEmis: TdmkEditCB
              Left = 574
              Top = 31
              Width = 44
              Height = 21
              Alignment = taRightJustify
              TabOrder = 8
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'CartEmis'
              UpdCampo = 'CartEmis'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              DBLookupComboBox = CBCartEmis
            end
            object CBCondicaoPG: TdmkDBLookupComboBox
              Left = 188
              Top = 32
              Width = 313
              Height = 21
              KeyField = 'CodUsu'
              ListField = 'Nome'
              ListSource = DmPediVda.DsPediPrzCab
              TabOrder = 7
              dmkEditCB = EdCondicaoPG
              UpdType = utNil
            end
            object EdCondicaoPG: TdmkEditCB
              Left = 128
              Top = 32
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 6
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              DBLookupComboBox = CBCondicaoPG
            end
            object CkAFP_Sit: TdmkCheckBox
              Left = 756
              Top = 11
              Width = 89
              Height = 17
              Caption = 'Fatura parcial?'
              TabOrder = 4
              QryCampo = 'AFP_Sit'
              UpdCampo = 'AFP_Sit'
              UpdType = utYes
              ValCheck = #0
              ValUncheck = #0
              OldValor = #0
            end
            object EdAFP_Per: TdmkEdit
              Left = 928
              Top = 8
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 5
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 4
              LeftZeros = 0
              NoEnterToTab = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,0000'
              QryCampo = 'AFP_Per'
              UpdCampo = 'AFP_Per'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
            end
          end
          object GroupBox4: TGroupBox
            Left = 1
            Top = 97
            Width = 996
            Height = 36
            Align = alTop
            TabOrder = 2
            object Label28: TLabel
              Left = 8
              Top = 12
              Width = 87
              Height = 13
              Caption = 'Lote de produ'#231#227'o:'
              FocusControl = DBEdit15
            end
            object DBEdit15: TDBEdit
              Left = 100
              Top = 8
              Width = 92
              Height = 21
              TabStop = False
              Color = clGradientInactiveCaption
              DataField = 'LoteProd'
              DataSource = DsPediVda
              TabOrder = 0
            end
          end
          object GroupBox5: TGroupBox
            Left = 1
            Top = 133
            Width = 996
            Height = 106
            Align = alTop
            TabOrder = 3
            object Label29: TLabel
              Left = 8
              Top = 13
              Width = 45
              Height = 13
              Caption = 'Frete por:'
            end
            object Label30: TLabel
              Left = 8
              Top = 36
              Width = 75
              Height = 13
              Caption = 'Transportadora:'
            end
            object Label31: TLabel
              Left = 8
              Top = 61
              Width = 64
              Height = 13
              Caption = 'Redespacho:'
            end
            object SpeedButton6: TSpeedButton
              Left = 524
              Top = 32
              Width = 21
              Height = 21
              OnClick = SpeedButton6Click
            end
            object SpeedButton11: TSpeedButton
              Left = 524
              Top = 56
              Width = 21
              Height = 21
              OnClick = SpeedButton11Click
            end
            object Label20: TLabel
              Left = 456
              Top = 13
              Width = 92
              Height = 13
              Caption = 'Local de entrega: >'
            end
            object Label32: TLabel
              Left = 176
              Top = 84
              Width = 115
              Height = 13
              Caption = '$ Despesas acess'#243'rias: '
            end
            object Label41: TLabel
              Left = 552
              Top = 84
              Width = 36
              Height = 13
              Caption = '$ Frete:'
            end
            object Label42: TLabel
              Left = 736
              Top = 84
              Width = 48
              Height = 13
              Caption = '% Seguro:'
            end
            object Label43: TLabel
              Left = 8
              Top = 84
              Width = 117
              Height = 13
              Caption = '% Despesas acess'#243'rias: '
            end
            object Label44: TLabel
              Left = 452
              Top = 84
              Width = 38
              Height = 13
              Caption = '% Frete:'
            end
            object Label45: TLabel
              Left = 844
              Top = 84
              Width = 46
              Height = 13
              Caption = '$ Seguro:'
            end
            object EdFretePor: TdmkEditCB
              Left = 56
              Top = 8
              Width = 21
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'FretePor'
              UpdCampo = 'FretePor'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              DBLookupComboBox = CBFretePor
            end
            object CBFretePor: TdmkDBLookupComboBox
              Left = 80
              Top = 8
              Width = 369
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DmPediVda.DsFretePor
              TabOrder = 1
              dmkEditCB = EdFretePor
              QryCampo = 'FretePor'
              UpdType = utNil
            end
            object EdTransporta: TdmkEditCB
              Left = 88
              Top = 32
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Transporta'
              UpdCampo = 'Transporta'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              DBLookupComboBox = CBTransporta
            end
            object CBTransporta: TdmkDBLookupComboBox
              Left = 148
              Top = 32
              Width = 373
              Height = 21
              KeyField = 'Codigo'
              ListField = 'NOMEENT'
              ListSource = DmPediVda.DsTransportas
              TabOrder = 3
              dmkEditCB = EdTransporta
              QryCampo = 'Transporta'
              UpdType = utNil
            end
            object EdRedespacho: TdmkEditCB
              Left = 88
              Top = 56
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Redespacho'
              UpdCampo = 'Redespacho'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              DBLookupComboBox = CBRedespacho
            end
            object CBRedespacho: TdmkDBLookupComboBox
              Left = 148
              Top = 56
              Width = 373
              Height = 21
              KeyField = 'Codigo'
              ListField = 'NOMEENT'
              ListSource = DmPediVda.DsRedespachos
              TabOrder = 5
              dmkEditCB = EdRedespacho
              QryCampo = 'Redespacho'
              UpdType = utNil
            end
            object MeEnderecoEntrega1: TMemo
              Left = 552
              Top = 8
              Width = 433
              Height = 69
              TabStop = False
              ReadOnly = True
              TabOrder = 6
            end
            object EdDesoAces_V: TdmkEdit
              Left = 300
              Top = 80
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 8
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryCampo = 'DesoAces_V'
              UpdCampo = 'DesoAces_V'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
            end
            object EdFrete_V: TdmkEdit
              Left = 600
              Top = 80
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 10
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryCampo = 'Frete_V'
              UpdCampo = 'Frete_V'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
            end
            object EdSeguro_P: TdmkEdit
              Left = 788
              Top = 80
              Width = 48
              Height = 21
              Alignment = taRightJustify
              TabOrder = 11
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryCampo = 'Seguro_P'
              UpdCampo = 'Seguro_P'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              OnChange = EdSeguro_PChange
            end
            object EdDesoAces_P: TdmkEdit
              Left = 124
              Top = 80
              Width = 48
              Height = 21
              Alignment = taRightJustify
              TabOrder = 7
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryCampo = 'DesoAces_P'
              UpdCampo = 'DesoAces_P'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              OnChange = EdDesoAces_PChange
            end
            object EdFrete_P: TdmkEdit
              Left = 496
              Top = 80
              Width = 48
              Height = 21
              Alignment = taRightJustify
              TabOrder = 9
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryCampo = 'Frete_P'
              UpdCampo = 'Frete_P'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              OnChange = EdFrete_PChange
            end
            object EdSeguro_V: TdmkEdit
              Left = 904
              Top = 80
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 12
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryCampo = 'Seguro_V'
              UpdCampo = 'Seguro_V'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
            end
          end
          object GroupBox7: TGroupBox
            Left = 1
            Top = 239
            Width = 996
            Height = 40
            Align = alTop
            TabOrder = 4
            object Label46: TLabel
              Left = 12
              Top = 16
              Width = 73
              Height = 13
              Caption = 'Representante:'
            end
            object SpeedButton12: TSpeedButton
              Left = 616
              Top = 12
              Width = 21
              Height = 21
              OnClick = SpeedButton12Click
            end
            object Label47: TLabel
              Left = 640
              Top = 15
              Width = 117
              Height = 13
              Caption = '% comiss'#227'o faturamento:'
            end
            object Label48: TLabel
              Left = 816
              Top = 15
              Width = 119
              Height = 13
              Caption = '% comiss'#227'o recebimento:'
            end
            object EdRepresen: TdmkEditCB
              Left = 88
              Top = 12
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Represen'
              UpdCampo = 'Represen'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              DBLookupComboBox = CBRepresen
            end
            object CBRepresen: TdmkDBLookupComboBox
              Left = 148
              Top = 12
              Width = 465
              Height = 21
              KeyField = 'Codigo'
              ListField = 'NOMEACC'
              ListSource = DmPediVda.DsPediAcc
              TabOrder = 1
              dmkEditCB = EdRepresen
              QryCampo = 'Represen'
              UpdType = utNil
            end
            object EdComisFat: TdmkEdit
              Left = 760
              Top = 11
              Width = 48
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryCampo = 'ComisFat'
              UpdCampo = 'ComisFat'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
            end
            object EdComisRec: TdmkEdit
              Left = 936
              Top = 11
              Width = 48
              Height = 21
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryCampo = 'ComisRec'
              UpdCampo = 'ComisRec'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
            end
          end
          object GroupBox6: TGroupBox
            Left = 1
            Top = 279
            Width = 996
            Height = 43
            Align = alTop
            Caption = ' Fiscal:'
            TabOrder = 5
            object Label25: TLabel
              Left = 12
              Top = 20
              Width = 73
              Height = 13
              Caption = 'Movimenta'#231#227'o:'
            end
            object SbRegrFiscal: TSpeedButton
              Left = 616
              Top = 16
              Width = 21
              Height = 21
              OnClick = SbRegrFiscalClick
            end
            object Label33: TLabel
              Left = 640
              Top = 20
              Width = 55
              Height = 13
              Caption = 'Modelo NF:'
            end
            object EdRegrFiscal: TdmkEditCB
              Left = 88
              Top = 16
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              OnChange = EdRegrFiscalChange
              OnExit = EdRegrFiscalExit
              DBLookupComboBox = CBRegrFiscal
            end
            object CBRegrFiscal: TdmkDBLookupComboBox
              Left = 148
              Top = 16
              Width = 465
              Height = 21
              KeyField = 'CodUsu'
              ListField = 'Nome'
              ListSource = DmPediVda.DsFisRegCad
              TabOrder = 1
              dmkEditCB = EdRegrFiscal
              UpdType = utNil
            end
            object EdModeloNF: TdmkEdit
              Left = 696
              Top = 16
              Width = 287
              Height = 21
              ReadOnly = True
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
            end
          end
        end
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    Caption = '                              Pedidos de Venda'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object LaTipo: TdmkLabel
      Left = 925
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 603
      ExplicitTop = 2
      ExplicitHeight = 44
    end
    object Image1: TImage
      Left = 226
      Top = 1
      Width = 699
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 227
      ExplicitTop = 2
      ExplicitWidth = 376
      ExplicitHeight = 44
    end
    object PanelFill2: TPanel
      Left = 1
      Top = 1
      Width = 225
      Height = 46
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 0
        OnClick = SbImprimeClick
        NumGlyphs = 2
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 1
        OnClick = SbNovoClick
        NumGlyphs = 2
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 2
        OnClick = SbNumeroClick
        NumGlyphs = 2
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 3
        OnClick = SbNomeClick
        NumGlyphs = 2
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 4
        OnClick = SbQueryClick
        NumGlyphs = 2
      end
    end
  end
  object DsPediVda: TDataSource
    DataSet = QrPediVda
    Left = 32
    Top = 65524
  end
  object QrPediVda: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrPediVdaBeforeOpen
    AfterOpen = QrPediVdaAfterOpen
    BeforeClose = QrPediVdaBeforeClose
    AfterScroll = QrPediVdaAfterScroll
    OnCalcFields = QrPediVdaCalcFields
    SQL.Strings = (
      'SELECT pvd.Codigo, pvd.CodUsu, pvd.Empresa, pvd.Cliente,'
      'pvd.DtaEmiss, pvd.DtaEntra, pvd.DtaInclu, pvd.DtaPrevi,'
      'pvd.Prioridade, pvd.CondicaoPG, pvd.Moeda,'
      'pvd.Situacao, pvd.TabelaPrc, pvd.MotivoSit, pvd.LoteProd,'
      'pvd.PedidoCli, pvd.FretePor, pvd.Transporta, pvd.Redespacho,'
      'pvd.RegrFiscal, pvd.DesoAces_V, pvd.DesoAces_P,'
      'pvd.Frete_V, pvd.Frete_P, pvd.Seguro_V, pvd.Seguro_P,'
      'pvd.TotalQtd, pvd.Total_Vlr, pvd.Total_Des, pvd.Total_Tot,'
      'pvd.Observa, tpc.Nome NOMETABEPRCCAD, tpc.DescoMax,'
      'mda.Nome NOMEMOEDA, pvd.Represen, pvd.ComisFat,'
      'pvd.ComisRec, pvd.CartEmis, pvd.AFP_Sit, pvd.AFP_Per,'
      'ppc.MedDDSimpl, ppc.MedDDReal, ppc.MaxDesco, ppc.JurosMes,'
      'pvd.ValLiq, pvd.QuantP, frc.Nome NOMEFISREGCAD,'
      'imp.Nome NOMEMODELONF,'
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NOMEEMP,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NOMECLI,'
      'IF(cli.Tipo=0, cli.ECidade, cli.PCidade) CIDADECLI,'
      'IF(ven.Tipo=0, ven.RazaoSocial, ven.NOME) NOMEACC,'
      'IF(tra.Tipo=0, tra.RazaoSocial, tra.NOME) NOMETRANSP,'
      'IF(red.Tipo=0, red.RazaoSocial, red.NOME) NOMEREDESP,'
      'uf1.Nome NOMEUF, emp.Filial, car.Nome NOMECARTEMIS,'
      'ppc.Nome NOMECONDICAOPG, mot.Nome NOMEMOTIVO,'
      'cli.CodUsu CODUSU_CLI, ven.CodUsu CODUSU_ACC,'
      'tra.CodUsu CODUSU_TRA, red.CodUsu CODUSU_RED,'
      'mot.CodUsu CODUSU_MOT, tpc.CodUsu CODUSU_TPC,'
      'mda.CodUsu CODUSU_MDA, ppc.CodUsu CODUSU_PPC,'
      'frc.CodUsu CODUSU_FRC, imp.Codigo MODELO_NF'
      'FROM pedivda pvd'
      'LEFT JOIN entidades  emp ON emp.Codigo=pvd.Empresa'
      'LEFT JOIN entidades  cli ON cli.Codigo=pvd.Cliente'
      'LEFT JOIN entidades  tra ON tra.Codigo=pvd.Transporta'
      'LEFT JOIN entidades  red ON red.Codigo=pvd.Redespacho'
      
        'LEFT JOIN ufs        uf1 ON uf1.Codigo=IF(cli.Tipo=0, cli.EUF, c' +
        'li.PUF)'
      'LEFT JOIN tabeprccab tpc ON tpc.Codigo=pvd.TabelaPrc'
      'LEFT JOIN cambiomda  mda ON mda.Codigo=pvd.Moeda'
      'LEFT JOIN pediprzcab ppc ON ppc.Codigo=pvd.CondicaoPG'
      'LEFT JOIN motivos    mot ON mot.Codigo=pvd.MotivoSit'
      'LEFT JOIN pediacc    acc ON acc.Codigo=pvd.Represen'
      'LEFT JOIN entidades  ven ON ven.Codigo=acc.Codigo'
      'LEFT JOIN carteiras  car ON car.Codigo=pvd.CartEmis'
      'LEFT JOIN fisregcad  frc ON frc.Codigo=pvd.RegrFiscal'
      'LEFT JOIN imprime    imp ON imp.Codigo=frc.ModeloNF'
      '')
    Left = 4
    Top = 65524
    object QrPediVdaCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPediVdaCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
      DisplayFormat = '000000'
    end
    object QrPediVdaEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrPediVdaCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrPediVdaDtaEmiss: TDateField
      FieldName = 'DtaEmiss'
      Required = True
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrPediVdaDtaEntra: TDateField
      FieldName = 'DtaEntra'
      Required = True
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrPediVdaDtaInclu: TDateField
      FieldName = 'DtaInclu'
      Required = True
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrPediVdaDtaPrevi: TDateField
      FieldName = 'DtaPrevi'
      Required = True
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrPediVdaNOMEEMP: TWideStringField
      FieldName = 'NOMEEMP'
      Size = 100
    end
    object QrPediVdaNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrPediVdaCIDADECLI: TWideStringField
      FieldName = 'CIDADECLI'
      Size = 25
    end
    object QrPediVdaNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Required = True
      Size = 2
    end
    object QrPediVdaFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrPediVdaPrioridade: TSmallintField
      FieldName = 'Prioridade'
      Required = True
      DisplayFormat = '00'
    end
    object QrPediVdaCondicaoPG: TIntegerField
      FieldName = 'CondicaoPG'
      Required = True
    end
    object QrPediVdaMoeda: TIntegerField
      FieldName = 'Moeda'
      Required = True
    end
    object QrPediVdaSituacao: TIntegerField
      FieldName = 'Situacao'
      Required = True
    end
    object QrPediVdaTabelaPrc: TIntegerField
      FieldName = 'TabelaPrc'
      Required = True
    end
    object QrPediVdaMotivoSit: TIntegerField
      FieldName = 'MotivoSit'
      Required = True
    end
    object QrPediVdaLoteProd: TIntegerField
      FieldName = 'LoteProd'
      Required = True
    end
    object QrPediVdaPedidoCli: TWideStringField
      FieldName = 'PedidoCli'
    end
    object QrPediVdaFretePor: TSmallintField
      FieldName = 'FretePor'
      Required = True
    end
    object QrPediVdaTransporta: TIntegerField
      FieldName = 'Transporta'
      Required = True
    end
    object QrPediVdaRedespacho: TIntegerField
      FieldName = 'Redespacho'
      Required = True
    end
    object QrPediVdaRegrFiscal: TIntegerField
      FieldName = 'RegrFiscal'
      Required = True
    end
    object QrPediVdaDesoAces_V: TFloatField
      FieldName = 'DesoAces_V'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediVdaDesoAces_P: TFloatField
      FieldName = 'DesoAces_P'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediVdaFrete_V: TFloatField
      FieldName = 'Frete_V'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediVdaFrete_P: TFloatField
      FieldName = 'Frete_P'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediVdaSeguro_V: TFloatField
      FieldName = 'Seguro_V'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediVdaSeguro_P: TFloatField
      FieldName = 'Seguro_P'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediVdaTotalQtd: TFloatField
      FieldName = 'TotalQtd'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediVdaTotal_Vlr: TFloatField
      FieldName = 'Total_Vlr'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediVdaTotal_Des: TFloatField
      FieldName = 'Total_Des'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediVdaTotal_Tot: TFloatField
      FieldName = 'Total_Tot'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediVdaObserva: TWideMemoField
      FieldName = 'Observa'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrPediVdaNOMETABEPRCCAD: TWideStringField
      FieldName = 'NOMETABEPRCCAD'
      Size = 50
    end
    object QrPediVdaNOMEMOEDA: TWideStringField
      FieldName = 'NOMEMOEDA'
      Size = 30
    end
    object QrPediVdaNOMECONDICAOPG: TWideStringField
      FieldName = 'NOMECONDICAOPG'
      Size = 50
    end
    object QrPediVdaNOMEMOTIVO: TWideStringField
      FieldName = 'NOMEMOTIVO'
      Size = 50
    end
    object QrPediVdaNOMESITUACAO: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NOMESITUACAO'
      LookupDataSet = DModG.QrSituacao
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'Situacao'
      Size = 50
      Lookup = True
    end
    object QrPediVdaNOMECARTEMIS: TWideStringField
      FieldName = 'NOMECARTEMIS'
      Size = 100
    end
    object QrPediVdaNOMEFRETEPOR: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEFRETEPOR'
      Calculated = True
    end
    object QrPediVdaNOMETRANSP: TWideStringField
      FieldName = 'NOMETRANSP'
      Size = 100
    end
    object QrPediVdaNOMEREDESP: TWideStringField
      FieldName = 'NOMEREDESP'
      Size = 100
    end
    object QrPediVdaNOMEACC: TWideStringField
      FieldName = 'NOMEACC'
      Size = 100
    end
    object QrPediVdaNOMEFISREGCAD: TWideStringField
      FieldName = 'NOMEFISREGCAD'
      Size = 50
    end
    object QrPediVdaNOMEMODELONF: TWideStringField
      FieldName = 'NOMEMODELONF'
      Size = 100
    end
    object QrPediVdaRepresen: TIntegerField
      FieldName = 'Represen'
      Required = True
    end
    object QrPediVdaComisFat: TFloatField
      FieldName = 'ComisFat'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediVdaComisRec: TFloatField
      FieldName = 'ComisRec'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediVdaCartEmis: TIntegerField
      FieldName = 'CartEmis'
      Required = True
    end
    object QrPediVdaAFP_Sit: TSmallintField
      FieldName = 'AFP_Sit'
      Required = True
    end
    object QrPediVdaAFP_Per: TFloatField
      FieldName = 'AFP_Per'
      Required = True
      DisplayFormat = '#,###,##0.0000'
    end
    object QrPediVdaCODUSU_CLI: TIntegerField
      FieldName = 'CODUSU_CLI'
      Required = True
    end
    object QrPediVdaCODUSU_ACC: TIntegerField
      FieldName = 'CODUSU_ACC'
      Required = True
    end
    object QrPediVdaCODUSU_TRA: TIntegerField
      FieldName = 'CODUSU_TRA'
      Required = True
    end
    object QrPediVdaCODUSU_RED: TIntegerField
      FieldName = 'CODUSU_RED'
      Required = True
    end
    object QrPediVdaCODUSU_MOT: TIntegerField
      FieldName = 'CODUSU_MOT'
      Required = True
    end
    object QrPediVdaCODUSU_TPC: TIntegerField
      FieldName = 'CODUSU_TPC'
      Required = True
    end
    object QrPediVdaCODUSU_MDA: TIntegerField
      FieldName = 'CODUSU_MDA'
      Required = True
    end
    object QrPediVdaCODUSU_PPC: TIntegerField
      FieldName = 'CODUSU_PPC'
      Required = True
    end
    object QrPediVdaCODUSU_FRC: TIntegerField
      FieldName = 'CODUSU_FRC'
      Required = True
    end
    object QrPediVdaMODELO_NF: TIntegerField
      FieldName = 'MODELO_NF'
      Required = True
    end
    object QrPediVdaMedDDSimpl: TFloatField
      FieldName = 'MedDDSimpl'
    end
    object QrPediVdaMedDDReal: TFloatField
      FieldName = 'MedDDReal'
    end
    object QrPediVdaValLiq: TFloatField
      FieldName = 'ValLiq'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediVdaQuantP: TFloatField
      FieldName = 'QuantP'
      Required = True
    end
    object QrPediVdaMaxDesco: TFloatField
      FieldName = 'MaxDesco'
    end
    object QrPediVdaJurosMes: TFloatField
      FieldName = 'JurosMes'
      DisplayFormat = '0.000000'
    end
    object QrPediVdaDescoMax: TFloatField
      FieldName = 'DescoMax'
      DisplayFormat = '0.00'
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtPedidos
    CanUpd01 = BtItens
    Left = 116
    Top = 65524
  end
  object PMPedidos: TPopupMenu
    OnPopup = PMPedidosPopup
    Left = 356
    Top = 552
    object Incluinovopedido1: TMenuItem
      Caption = '&Inclui novo pedido'
      OnClick = Incluinovopedido1Click
    end
    object Alterapedidoatual1: TMenuItem
      Caption = '&Altera pedido atual'
      OnClick = Alterapedidoatual1Click
    end
    object Excluipedidoatual1: TMenuItem
      Caption = '&Exclui pedido atual'
      Enabled = False
    end
  end
  object PMItens: TPopupMenu
    Left = 452
    Top = 552
    object Incluinovositensdegrupo1: TMenuItem
      Caption = '&Inclui novos itens por &Grade'
      OnClick = Incluinovositensdegrupo1Click
    end
    object IncluinovositensporLeitura1: TMenuItem
      Caption = 'Inclui novos itens por &Leitura'
      OnClick = IncluinovositensporLeitura1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object AlteraExcluiIncluiitemselecionado1: TMenuItem
      Caption = '&Altera / Exclui / Inclui item selecionado'
      OnClick = AlteraExcluiIncluiitemselecionado1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
  end
  object VuEmpresa: TdmkValUsu
    dmkEditCB = EdEmpresa
    Panel = PainelEdita
    QryCampo = 'Empresa'
    UpdCampo = 'Empresa'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 144
    Top = 65532
  end
  object VuCondicaoPG: TdmkValUsu
    dmkEditCB = EdCondicaoPG
    Panel = PainelEdita
    QryCampo = 'CondicaoPG'
    UpdCampo = 'CondicaoPG'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 172
    Top = 65532
  end
  object VuCambioMda: TdmkValUsu
    dmkEditCB = EdMoeda
    Panel = PainelEdita
    QryCampo = 'Moeda'
    UpdCampo = 'Moeda'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 200
    Top = 65524
  end
  object VuTabelaPrc: TdmkValUsu
    dmkEditCB = EdTabelaPrc
    Panel = PainelEdita
    QryCampo = 'TabelaPrc'
    UpdCampo = 'TabelaPrc'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 228
    Top = 65532
  end
  object VuMotivoSit: TdmkValUsu
    dmkEditCB = EdMotivoSit
    Panel = PainelEdita
    QryCampo = 'MotivoSit'
    UpdCampo = 'MotivoSit'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 256
    Top = 65524
  end
  object QrPediVdaGru: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPediVdaGruAfterOpen
    BeforeClose = QrPediVdaGruBeforeClose
    AfterScroll = QrPediVdaGruAfterScroll
    SQL.Strings = (
      'SELECT SUM(QuantP) QuantP, SUM(ValLiq) ValLiq, '
      'SUM(Customizad) ItensCustomizados,'
      'gti.Codigo GRATAMCAD, '
      'gg1.CodUsu, gg1.Nome, gg1.Nivel1, pgt.Fracio'
      'FROM pedivdaits pvi'
      'LEFT JOIN gragrux   ggx ON ggx.Controle=pvi.GraGruX'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN prdgruptip   pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'WHERE pvi.Codigo=:P0'
      'GROUP BY gg1.Nivel1')
    Left = 60
    Top = 65524
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPediVdaGruCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPediVdaGruNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrPediVdaGruNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrPediVdaGruGRATAMCAD: TIntegerField
      FieldName = 'GRATAMCAD'
    end
    object QrPediVdaGruQuantP: TFloatField
      FieldName = 'QuantP'
    end
    object QrPediVdaGruValLiq: TFloatField
      FieldName = 'ValLiq'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediVdaGruItensCustomizados: TFloatField
      FieldName = 'ItensCustomizados'
    end
    object QrPediVdaGruFracio: TSmallintField
      FieldName = 'Fracio'
    end
  end
  object DsPediVdaGru: TDataSource
    DataSet = QrPediVdaGru
    Left = 88
    Top = 65524
  end
  object VuFisRegCad: TdmkValUsu
    dmkEditCB = EdRegrFiscal
    Panel = PainelEdita
    QryCampo = 'RegrFiscal'
    UpdCampo = 'RegrFiscal'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 284
    Top = 65524
  end
  object PMCustom: TPopupMenu
    OnPopup = PMCustomPopup
    Left = 536
    Top = 548
    object ItemprodutoCustomizvel1: TMenuItem
      Caption = '&Item (produto) Customiz'#225'vel'
      object IncluinovoitemprodutoCustomizvel1: TMenuItem
        Caption = 'Inclui novo item (produto) &Customiz'#225'vel'
        OnClick = IncluinovoitemprodutoCustomizvel1Click
      end
      object Alteraitemprodutoselecionado1: TMenuItem
        Caption = '&Altera item (produto) selecionado'
        OnClick = Alteraitemprodutoselecionado1Click
      end
      object Excluiitemprodutoselecionado1: TMenuItem
        Caption = '&Exclui item (produto) selecionado'
        OnClick = Excluiitemprodutoselecionado1Click
      end
      object N3: TMenuItem
        Caption = '-'
      end
      object AtualizaValoresdoitem1: TMenuItem
        Caption = '&Atualiza &Valores do item'
        OnClick = AtualizaValoresdoitem1Click
      end
    end
    object Partedoitemprodutoselecionado1: TMenuItem
      Caption = '&Parte do item (produto) selecionado'
      object Adicionapartesaoitemselecionado1: TMenuItem
        Caption = '&Adiciona partes ao produto selecionado'
        Enabled = False
        OnClick = Adicionapartesaoitemselecionado1Click
      end
      object Excluipartedoprodutoselecionado1: TMenuItem
        Caption = '&Exclui parte do produto selecionado'
        Enabled = False
        OnClick = Excluipartedoprodutoselecionado1Click
      end
    end
  end
  object QrCustomizados: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrCustomizadosBeforeClose
    AfterScroll = QrCustomizadosAfterScroll
    SQL.Strings = (
      'SELECT pvi.Controle, pvi.GraGruX, pvi.ValLiq,'
      'pvi.MedidaC, pvi.MedidaL, pvi.MedidaA, pvi.MedidaE,'
      'gti.Codigo GRATAMCAD, gti.Controle GRATAMITS, '
      'gti.Nome NO_TAM, gcc.Nome NO_COR'
      'FROM pedivdaits pvi'
      'LEFT JOIN gragrux   ggx ON ggx.Controle=pvi.GraGruX'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'WHERE pvi.Customizad=1'
      'AND pvi.Codigo=:P0'
      'AND gg1.Nivel1=:P1')
    Left = 476
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrCustomizadosControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCustomizadosGRATAMCAD: TIntegerField
      FieldName = 'GRATAMCAD'
    end
    object QrCustomizadosGRATAMITS: TAutoIncField
      FieldName = 'GRATAMITS'
    end
    object QrCustomizadosNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object QrCustomizadosNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrCustomizadosGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrCustomizadosMedidaC: TFloatField
      FieldName = 'MedidaC'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrCustomizadosMedidaL: TFloatField
      FieldName = 'MedidaL'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrCustomizadosMedidaA: TFloatField
      FieldName = 'MedidaA'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrCustomizadosMedidaE: TFloatField
      FieldName = 'MedidaE'
      DisplayFormat = '#,###,###,##0.000000;-#,###,###,##0.000000; '
    end
    object QrCustomizadosValLiq: TFloatField
      FieldName = 'ValLiq'
      DisplayFormat = '#,###,###,##0.00'
    end
  end
  object DsCustomizados: TDataSource
    DataSet = QrCustomizados
    Left = 504
    Top = 8
  end
  object QrPediVdaCuz: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrPediVdaCuzCalcFields
    SQL.Strings = (
      'SELECT gti.Nome NO_TAM, gcc.Nome NO_COR, '
      'mpc.Nome NO_PARTE, gg1.Nome NO_GRUPO, '
      'gg1.SiglaCustm, pvc.* '
      'FROM pedivdacuz pvc'
      'LEFT JOIN gragrux   ggx ON ggx.Controle=pvc.GraGruX'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN matpartcad mpc ON mpc.Codigo=pvc.MatPartCad'
      'WHERE pvc.Controle=:P0')
    Left = 536
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPediVdaCuzNO_PARTE: TWideStringField
      FieldName = 'NO_PARTE'
      Size = 50
    end
    object QrPediVdaCuzNO_GRUPO: TWideStringField
      FieldName = 'NO_GRUPO'
      Size = 30
    end
    object QrPediVdaCuzNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object QrPediVdaCuzNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrPediVdaCuzControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPediVdaCuzConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrPediVdaCuzMatPartCad: TIntegerField
      FieldName = 'MatPartCad'
    end
    object QrPediVdaCuzGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrPediVdaCuzMedidaC: TFloatField
      FieldName = 'MedidaC'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrPediVdaCuzMedidaL: TFloatField
      FieldName = 'MedidaL'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrPediVdaCuzMedidaA: TFloatField
      FieldName = 'MedidaA'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrPediVdaCuzMedidaE: TFloatField
      FieldName = 'MedidaE'
      DisplayFormat = '#,###,###,##0.000000;-#,###,###,##0.000000; '
    end
    object QrPediVdaCuzQuantP: TFloatField
      FieldName = 'QuantP'
    end
    object QrPediVdaCuzQuantX: TFloatField
      FieldName = 'QuantX'
    end
    object QrPediVdaCuzPrecoO: TFloatField
      FieldName = 'PrecoO'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrPediVdaCuzPrecoR: TFloatField
      FieldName = 'PrecoR'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrPediVdaCuzPrecoF: TFloatField
      FieldName = 'PrecoF'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrPediVdaCuzValBru: TFloatField
      FieldName = 'ValBru'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrPediVdaCuzDescoP: TFloatField
      FieldName = 'DescoP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPediVdaCuzDescoV: TFloatField
      FieldName = 'DescoV'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPediVdaCuzPerCustom: TFloatField
      FieldName = 'PerCustom'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPediVdaCuzValLiq: TFloatField
      FieldName = 'ValLiq'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPediVdaCuzTipDimens: TSmallintField
      FieldName = 'TipDimens'
    end
    object QrPediVdaCuzMedidaC_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MedidaC_TXT'
      Calculated = True
    end
    object QrPediVdaCuzMedidaL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MedidaL_TXT'
      Calculated = True
    end
    object QrPediVdaCuzMedidaA_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MedidaA_TXT'
      Calculated = True
    end
    object QrPediVdaCuzMedidaE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MedidaE_TXT'
      Calculated = True
    end
    object QrPediVdaCuzSiglaCustm: TWideStringField
      FieldName = 'SiglaCustm'
      Size = 15
    end
  end
  object DsPediVdaCuz: TDataSource
    DataSet = QrPediVdaCuz
    Left = 564
    Top = 8
  end
  object frxDsCli: TfrxDBDataset
    UserName = 'frxDsCli'
    CloseDataSource = False
    DataSet = QrCli
    BCDToCurrency = False
    Left = 776
    Top = 4
  end
  object QrCli: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrCliCalcFields
    SQL.Strings = (
      'SELECT en.Codigo, Tipo, CodUsu, IE, '
      
        'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome     END NOM' +
        'E_ENT, '
      
        'CASE WHEN en.Tipo=0 THEN en.CNPJ        ELSE en.CPF      END CNP' +
        'J_CPF, '
      
        'CASE WHEN en.Tipo=0 THEN en.IE          ELSE en.RG       END IE_' +
        'RG, '
      
        'CASE WHEN en.Tipo=0 THEN en.ERua        ELSE en.PRua     END RUA' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.ENumero     ELSE en.PNumero  END NUM' +
        'ERO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECompl      ELSE en.PCompl   END COM' +
        'PL, '
      
        'CASE WHEN en.Tipo=0 THEN en.EBairro     ELSE en.PBairro  END BAI' +
        'RRO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECidade     ELSE en.PCidade  END CID' +
        'ADE, '
      'CASE WHEN en.Tipo=0 THEN en.EUF     ELSE en.PUF  END UF, '
      
        'CASE WHEN en.Tipo=0 THEN lle.Nome       ELSE llp.Nome    END NOM' +
        'ELOGRAD, '
      
        'CASE WHEN en.Tipo=0 THEN ufe.Nome       ELSE ufp.Nome    END NOM' +
        'EUF, '
      
        'CASE WHEN en.Tipo=0 THEN en.EPais       ELSE en.PPais    END Pai' +
        's, '
      
        'CASE WHEN en.Tipo=0 THEN en.ELograd     ELSE en.PLograd  END Log' +
        'rad, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECEP        ELSE en.PCEP     END CEP' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.EEndeRef    ELSE en.PEndeRef END END' +
        'EREF, '
      
        'CASE WHEN en.Tipo=0 THEN en.ETe1        ELSE en.PTe1     END TE1' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.EFax        ELSE en.PFax     END FAX' +
        ', '
      
        'IF(en.Tipo=0, "CNPJ", "CPF") CAD_FEDERAL, IF(en.Tipo=0, "I.E.", ' +
        '"RG") CAD_ESTADUAL'
      'FROM entidades en '
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF'
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF'
      'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd'
      'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd'
      'WHERE en.Codigo=:P0')
    Left = 748
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCliE_ALL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_ALL'
      Size = 256
      Calculated = True
    end
    object QrCliCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrCliNOME_TIPO_DOC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_TIPO_DOC'
      Size = 10
      Calculated = True
    end
    object QrCliTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrCliFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Size = 40
      Calculated = True
    end
    object QrCliNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 30
      Calculated = True
    end
    object QrCliCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Calculated = True
    end
    object QrCliCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCliTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrCliCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrCliNOME_ENT: TWideStringField
      FieldName = 'NOME_ENT'
      Size = 100
    end
    object QrCliCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrCliIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrCliRUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrCliNUMERO: TLargeintField
      FieldName = 'NUMERO'
    end
    object QrCliCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrCliBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrCliCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrCliNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrCliNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrCliPais: TWideStringField
      FieldName = 'Pais'
    end
    object QrCliLograd: TLargeintField
      FieldName = 'Lograd'
    end
    object QrCliCEP: TLargeintField
      FieldName = 'CEP'
    end
    object QrCliENDEREF: TWideStringField
      FieldName = 'ENDEREF'
      Size = 100
    end
    object QrCliTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrCliFAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrCliIE: TWideStringField
      FieldName = 'IE'
    end
    object QrCliUF: TLargeintField
      FieldName = 'UF'
    end
    object QrCliCAD_FEDERAL: TWideStringField
      FieldName = 'CAD_FEDERAL'
      Required = True
      Size = 4
    end
    object QrCliCAD_ESTADUAL: TWideStringField
      FieldName = 'CAD_ESTADUAL'
      Required = True
      Size = 4
    end
    object QrCliIE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'IE_TXT'
      Size = 50
      Calculated = True
    end
  end
  object frxPED_VENDA_001_01: TfrxReport
    Version = '4.10.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39956.739524675900000000
    ReportOptions.LastChange = 39956.739524675900000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      '  if <MeuLogo3x1Existe> = True then '
      '    Picture1.LoadFromFile(<MeuLogo3x1Caminho>);'
      'end.')
    OnGetValue = frxPED_VENDA_001_01GetValue
    Left = 720
    Top = 4
    Datasets = <
      item
        DataSet = frxDsCli
        DataSetName = 'frxDsCli'
      end
      item
        DataSet = DModG.frxDsEndereco
        DataSetName = 'frxDsEndereco'
      end
      item
        DataSet = frxDsItsC
        DataSetName = 'frxDsItsC'
      end
      item
        DataSet = frxDsItsN
        DataSetName = 'frxDsItsN'
      end
      item
        DataSet = frxDsItsZ
        DataSetName = 'frxDsItsZ'
      end
      item
        DataSet = frxDsPediVda
        DataSetName = 'frxDsPediVda'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        Height = 192.756030000000000000
        Top = 18.897650000000000000
        Width = 699.213050000000000000
        object Shape1: TfrxShapeView
          Width = 699.213050000000000000
          Height = 56.692950000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          Left = 154.960730000000000000
          Width = 393.071120000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[frxDsEndereco."NO_2_ENT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Left = 551.811380000000000000
          Top = 18.897650000000000000
          Width = 147.401670000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          Left = 154.960730000000000000
          Top = 18.897650000000000000
          Width = 393.071120000000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[frxDsEndereco."E_ALL"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 548.031850000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 548.031850000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            'P'#195#161'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Picture1: TfrxPictureView
          Left = 3.779530000000000000
          Top = 3.779530000000000000
          Width = 147.401574800000000000
          Height = 49.133890000000000000
          ShowHint = False
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Shape2: TfrxShapeView
          Top = 68.031540000000000000
          Width = 699.213050000000000000
          Height = 75.590600000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object frxMemoView1: TfrxMemoView
          Left = 75.590600000000000000
          Top = 68.031540000000000000
          Width = 616.063390000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[frxDsCli."CodUsu"] - [frxDsCli."NOME_ENT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 75.590600000000000000
          Top = 86.929190000000000000
          Width = 616.063390000000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[frxDsCli."E_ALL"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 3.779530000000000000
          Top = 68.031540000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            'Destinat'#195#161'rio:')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 3.779530000000000000
          Top = 151.181200000000000000
          Width = 687.874460000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            
              'INFORME DO PEDIDO N'#194#186' [FormatFloat('#39'000000'#39', <frxDsPediVda."CodU' +
              'su">)]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 132.283550000000000000
          Top = 124.724490000000000000
          Width = 170.078850000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[frxDsCli."CNPJ_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Left = 75.590600000000000000
          Top = 124.724490000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDsCli."CAD_FEDERAL"]:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Left = 359.055350000000000000
          Top = 124.724490000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[frxDsCli."IE_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Left = 302.362400000000000000
          Top = 124.724490000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDsCli."CAD_ESTADUAL"]:')
          ParentFont = False
          VAlign = vaCenter
        end
        object frxDsEnderecoTE1: TfrxMemoView
          Left = 548.031850000000000000
          Top = 37.795300000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = DModG.frxDsEndereco
          DataSetName = 'frxDsEndereco'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsEndereco."TE1_TXT"]')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 566.929500000000000000
          Top = 124.724490000000000000
          Width = 124.724490000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[frxDsCli."TE1_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Left = 510.236550000000000000
          Top = 124.724490000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            'Telefone:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo64: TfrxMemoView
          Left = 3.779530000000000000
          Top = 173.858380000000000000
          Width = 687.874460000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            'N'#194#186' DO PEDIDO DO CLIENTE: [frxDsPediVda."PedidoCli"]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 13.228346460000000000
        Top = 695.433520000000000000
        Width = 699.213050000000000000
        object Memo1: TfrxMemoView
          Left = 7.559060000000000000
          Width = 362.834880000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            'www.dermatek.com.br - software customizado')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 370.393940000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupHeader1: TfrxGroupHeader
        Height = 15.118110240000000000
        Top = 272.126160000000000000
        Width = 699.213050000000000000
        Condition = 'frxDsItsN."KGT"'
        object Memo40: TfrxMemoView
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            'C'#195#179'digo')
          ParentFont = False
          WordWrap = False
        end
        object MeTitD: TfrxMemoView
          Left = 52.913420000000000000
          Width = 249.448857950000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            'Descri'#195#167#195#163'o')
          ParentFont = False
        end
        object MeTitB: TfrxMemoView
          Left = 498.897960000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            'Pre'#195#167'o')
          ParentFont = False
          WordWrap = False
        end
        object MeTitA: TfrxMemoView
          Left = 445.984540000000000000
          Width = 52.913390710000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            'Qtde.')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          Left = 302.362400000000000000
          Width = 102.047244090000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            'Cor')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 404.409710000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            'Tamanho')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 638.740570000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            'Vlr. Total')
          ParentFont = False
          WordWrap = False
        end
        object Memo20: TfrxMemoView
          Left = 578.268090000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            'Vlr. Unit.')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          Left = 544.252320000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '% Desc.')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData1: TfrxMasterData
        Height = 15.118110240000000000
        Top = 309.921460000000000000
        Width = 699.213050000000000000
        DataSet = frxDsItsN
        DataSetName = 'frxDsItsN'
        RowCount = 0
        object Memo2: TfrxMemoView
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'CU_NIVEL1'
          DataSet = frxDsItsN
          DataSetName = 'frxDsItsN'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            '[frxDsItsN."CU_NIVEL1"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          Left = 52.913420000000000000
          Width = 249.448857950000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'NO_NIVEL1'
          DataSet = frxDsItsN
          DataSetName = 'frxDsItsN'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            '[frxDsItsN."NO_NIVEL1"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 498.897960000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'PrecoR'
          DataSet = frxDsItsN
          DataSetName = 'frxDsItsN'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsItsN."PrecoR"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo11: TfrxMemoView
          Left = 445.984540000000000000
          Width = 52.913390710000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'QuantP'
          DataSet = frxDsItsN
          DataSetName = 'frxDsItsN'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsItsN."QuantP"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          Left = 302.362400000000000000
          Width = 102.047244090000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'NO_COR'
          DataSet = frxDsItsN
          DataSetName = 'frxDsItsN'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            '[frxDsItsN."NO_COR"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 404.409710000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'NO_TAM'
          DataSet = frxDsItsN
          DataSetName = 'frxDsItsN'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            '[frxDsItsN."NO_TAM"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 638.740570000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'ValLiq'
          DataSet = frxDsItsN
          DataSetName = 'frxDsItsN'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsItsN."ValLiq"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          Left = 578.268090000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'PrecoF'
          DataSet = frxDsItsN
          DataSetName = 'frxDsItsN'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsItsN."PrecoF"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo23: TfrxMemoView
          Left = 544.252320000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'DescoP'
          DataSet = frxDsItsN
          DataSetName = 'frxDsItsN'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsItsN."DescoP"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        Height = 3.779530000000000000
        Top = 347.716760000000000000
        Width = 699.213050000000000000
      end
      object GroupHeader2: TfrxGroupHeader
        Height = 34.015760240000000000
        Top = 374.173470000000000000
        Width = 699.213050000000000000
        Condition = 'frxDsItsC."KGT"'
        object Memo8: TfrxMemoView
          Width = 249.448857950000000000
          Height = 18.897640240000000000
          ShowHint = False
          DataSet = frxDsItsN
          DataSetName = 'frxDsItsN'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            'Itens customiz'#195#161'veis:')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Top = 18.897650000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsItsN
          DataSetName = 'frxDsItsN'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            'C'#195#179'digo')
          ParentFont = False
          WordWrap = False
        end
        object Memo27: TfrxMemoView
          Left = 52.913420000000000000
          Top = 18.897650000000000000
          Width = 238.110267950000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsItsN
          DataSetName = 'frxDsItsN'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            'Descri'#195#167#195#163'o')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 464.882190000000000000
          Top = 18.897650000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsItsN
          DataSetName = 'frxDsItsN'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            'Pre'#195#167'o')
          ParentFont = False
          WordWrap = False
        end
        object Memo31: TfrxMemoView
          Left = 411.968770000000000000
          Top = 18.897650000000000000
          Width = 52.913390710000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsItsN
          DataSetName = 'frxDsItsN'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            'Qtde.')
          ParentFont = False
          WordWrap = False
        end
        object Memo32: TfrxMemoView
          Left = 291.023810000000000000
          Top = 18.897650000000000000
          Width = 79.370064090000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsItsN
          DataSetName = 'frxDsItsN'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            'Cor')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 370.393940000000000000
          Top = 18.897650000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsItsN
          DataSetName = 'frxDsItsN'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            'Tamanho')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Left = 638.740570000000000000
          Top = 18.897650000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsItsN
          DataSetName = 'frxDsItsN'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            'Vlr. Total')
          ParentFont = False
          WordWrap = False
        end
        object Memo59: TfrxMemoView
          Left = 578.268090000000000000
          Top = 18.897650000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsItsN
          DataSetName = 'frxDsItsN'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            'Vlr. Unit.')
          ParentFont = False
          WordWrap = False
        end
        object Memo60: TfrxMemoView
          Left = 544.252320000000000000
          Top = 18.897650000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsItsN
          DataSetName = 'frxDsItsN'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '% Desc.')
          ParentFont = False
          WordWrap = False
        end
        object Memo61: TfrxMemoView
          Left = 510.236550000000000000
          Top = 18.897650000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsItsC
          DataSetName = 'frxDsItsC'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '% Pers.')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData2: TfrxMasterData
        Height = 30.236230240000000000
        Top = 430.866420000000000000
        Width = 699.213050000000000000
        DataSet = frxDsItsC
        DataSetName = 'frxDsItsC'
        RowCount = 0
        object Memo34: TfrxMemoView
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'CU_NIVEL1'
          DataSet = frxDsItsC
          DataSetName = 'frxDsItsC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            '[frxDsItsC."CU_NIVEL1"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo35: TfrxMemoView
          Left = 52.913420000000000000
          Width = 238.110267950000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'NO_NIVEL1'
          DataSet = frxDsItsC
          DataSetName = 'frxDsItsC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            '[frxDsItsC."NO_NIVEL1"]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 464.882190000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'PrecoR'
          DataSet = frxDsItsC
          DataSetName = 'frxDsItsC'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsItsC."PrecoR"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo37: TfrxMemoView
          Left = 411.968770000000000000
          Width = 52.913390710000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'QuantP'
          DataSet = frxDsItsC
          DataSetName = 'frxDsItsC'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsItsC."QuantP"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo43: TfrxMemoView
          Left = 291.023810000000000000
          Width = 79.370064090000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'NO_COR'
          DataSet = frxDsItsC
          DataSetName = 'frxDsItsC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            '[frxDsItsC."NO_COR"]')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 370.393940000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'NO_TAM'
          DataSet = frxDsItsC
          DataSetName = 'frxDsItsC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            '[frxDsItsC."NO_TAM"]')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 638.740570000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'ValLiq'
          DataSet = frxDsItsC
          DataSetName = 'frxDsItsC'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsItsC."ValLiq"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo46: TfrxMemoView
          Left = 578.268090000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'PrecoF'
          DataSet = frxDsItsC
          DataSetName = 'frxDsItsC'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsItsC."PrecoF"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo47: TfrxMemoView
          Left = 544.252320000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'DescoP'
          DataSet = frxDsItsC
          DataSetName = 'frxDsItsC'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsItsC."DescoP"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo48: TfrxMemoView
          Top = 15.118120000000000000
          Width = 699.212927950000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'DESCRICAO'
          DataSet = frxDsItsC
          DataSetName = 'frxDsItsC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            '[frxDsItsC."DESCRICAO"]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 510.236550000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'PercCustom'
          DataSet = frxDsItsC
          DataSetName = 'frxDsItsC'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsItsC."PercCustom"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter2: TfrxGroupFooter
        Height = 1.889763779527559000
        Top = 585.827150000000000000
        Width = 699.213050000000000000
      end
      object GroupHeader3: TfrxGroupHeader
        Top = 483.779840000000000000
        Width = 699.213050000000000000
        Condition = 'frxDsItsZ."KGT"'
      end
      object GroupFooter3: TfrxGroupFooter
        Height = 1.889763780000000000
        Top = 559.370440000000000000
        Width = 699.213050000000000000
      end
      object DetailData1: TfrxDetailData
        Height = 30.236230240000000000
        Top = 506.457020000000000000
        Width = 699.213050000000000000
        DataSet = frxDsItsZ
        DataSetName = 'frxDsItsZ'
        RowCount = 0
        object Memo50: TfrxMemoView
          Left = 52.913420000000000000
          Width = 238.110267950000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'NO_NIVEL1'
          DataSet = frxDsItsZ
          DataSetName = 'frxDsItsZ'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            '[frxDsItsZ."NO_NIVEL1"]')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Left = 464.882190000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'PrecoR'
          DataSet = frxDsItsZ
          DataSetName = 'frxDsItsZ'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsItsZ."PrecoR"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo52: TfrxMemoView
          Left = 411.968770000000000000
          Width = 52.913390710000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'QuantP'
          DataSet = frxDsItsZ
          DataSetName = 'frxDsItsZ'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsItsZ."QuantP"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo53: TfrxMemoView
          Left = 291.023810000000000000
          Width = 79.370064090000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'NO_COR'
          DataSet = frxDsItsZ
          DataSetName = 'frxDsItsZ'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            '[frxDsItsZ."NO_COR"]')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Left = 370.393940000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'NO_TAM'
          DataSet = frxDsItsZ
          DataSetName = 'frxDsItsZ'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            '[frxDsItsZ."NO_TAM"]')
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          Left = 638.740570000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'ValLiq'
          DataSet = frxDsItsZ
          DataSetName = 'frxDsItsZ'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsItsZ."ValLiq"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo56: TfrxMemoView
          Left = 578.268090000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'PrecoF'
          DataSet = frxDsItsZ
          DataSetName = 'frxDsItsZ'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsItsZ."PrecoF"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo57: TfrxMemoView
          Left = 544.252320000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'DescoP'
          DataSet = frxDsItsZ
          DataSetName = 'frxDsItsZ'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsItsZ."DescoP"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo58: TfrxMemoView
          Left = 52.913420000000000000
          Top = 15.118120000000000000
          Width = 646.299507950000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'DESCRICAO'
          DataSet = frxDsItsZ
          DataSetName = 'frxDsItsZ'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            '[frxDsItsZ."DESCRICAO"]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 510.236550000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'PerCustom'
          DataSet = frxDsItsZ
          DataSetName = 'frxDsItsZ'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsItsZ."PerCustom"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        Height = 22.677180000000000000
        Top = 650.079160000000000000
        Width = 699.213050000000000000
        object Memo62: TfrxMemoView
          Left = 578.268090000000000000
          Width = 120.944920940000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsItsZ
          DataSetName = 'frxDsItsZ'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsPediVda."ValLiq"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo63: TfrxMemoView
          Left = 340.157700000000000000
          Width = 238.110267950000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsItsZ
          DataSetName = 'frxDsItsZ'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            'TOTAL ')
          ParentFont = False
        end
      end
    end
  end
  object QrItsN: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.CodUsu CU_NIVEL1, gg1.Nome NO_NIVEL1,'
      'gti.Nome NO_TAM, gcc.Nome NO_COR,   '
      'pvi.QuantP, pvi.PrecoR, pvi.DescoP, '
      'pvi.PrecoF, pvi.ValBru, pvi.ValLiq, 0 KGT'
      'FROM pedivdaits pvi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=pvi.GraGrux'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE pvi.Codigo=:P0'
      'AND pvi.Customizad=0'
      '')
    Left = 748
    Top = 32
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrItsNCU_NIVEL1: TIntegerField
      FieldName = 'CU_NIVEL1'
      Required = True
    end
    object QrItsNNO_NIVEL1: TWideStringField
      FieldName = 'NO_NIVEL1'
      Size = 30
    end
    object QrItsNNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object QrItsNNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrItsNQuantP: TFloatField
      FieldName = 'QuantP'
    end
    object QrItsNPrecoR: TFloatField
      FieldName = 'PrecoR'
    end
    object QrItsNDescoP: TFloatField
      FieldName = 'DescoP'
    end
    object QrItsNPrecoF: TFloatField
      FieldName = 'PrecoF'
    end
    object QrItsNValBru: TFloatField
      FieldName = 'ValBru'
    end
    object QrItsNValLiq: TFloatField
      FieldName = 'ValLiq'
    end
    object QrItsNKGT: TLargeintField
      FieldName = 'KGT'
      Required = True
    end
  end
  object QrItsC: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrItsCAfterScroll
    OnCalcFields = QrItsCCalcFields
    SQL.Strings = (
      'SELECT gg1.CodUsu CU_NIVEL1, gg1.Nome NO_NIVEL1,'
      'gti.Nome NO_TAM, gcc.Nome NO_COR,  pvi.QuantP, '
      'pvi.PrecoR, pvi.DescoP, pvi.PrecoF, pvi.ValBru, pvi.ValLiq, '
      'pvi.Controle, pvi.MedidaC, pvi.MedidaL, pvi.MedidaA, '
      'pvi.MedidaE, PercCustom, pvi.InfAdCuztm, gg1.TipDimens,'
      'med.Medida1, med.Medida2, med.Medida3, med.Medida4, '
      'med.Sigla1, med.Sigla2, med.Sigla3, med.Sigla4, 0 KGT'
      'FROM pedivdaits pvi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=pvi.GraGrux'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN medordem med ON med.Codigo=gg1.MedOrdem'
      'WHERE pvi.Codigo=:P0'
      'AND pvi.Customizad=1'
      '')
    Left = 748
    Top = 60
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrItsCCU_NIVEL1: TIntegerField
      FieldName = 'CU_NIVEL1'
      Required = True
    end
    object QrItsCNO_NIVEL1: TWideStringField
      FieldName = 'NO_NIVEL1'
      Size = 30
    end
    object QrItsCNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object QrItsCNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrItsCQuantP: TFloatField
      FieldName = 'QuantP'
    end
    object QrItsCPrecoR: TFloatField
      FieldName = 'PrecoR'
    end
    object QrItsCDescoP: TFloatField
      FieldName = 'DescoP'
    end
    object QrItsCPrecoF: TFloatField
      FieldName = 'PrecoF'
    end
    object QrItsCValBru: TFloatField
      FieldName = 'ValBru'
    end
    object QrItsCValLiq: TFloatField
      FieldName = 'ValLiq'
    end
    object QrItsCMedidaC: TFloatField
      FieldName = 'MedidaC'
    end
    object QrItsCMedidaL: TFloatField
      FieldName = 'MedidaL'
    end
    object QrItsCMedidaA: TFloatField
      FieldName = 'MedidaA'
    end
    object QrItsCMedidaE: TFloatField
      FieldName = 'MedidaE'
    end
    object QrItsCPercCustom: TFloatField
      FieldName = 'PercCustom'
    end
    object QrItsCInfAdCuztm: TIntegerField
      FieldName = 'InfAdCuztm'
    end
    object QrItsCMedida1: TWideStringField
      FieldName = 'Medida1'
    end
    object QrItsCMedida2: TWideStringField
      FieldName = 'Medida2'
    end
    object QrItsCMedida3: TWideStringField
      FieldName = 'Medida3'
    end
    object QrItsCMedida4: TWideStringField
      FieldName = 'Medida4'
    end
    object QrItsCSigla1: TWideStringField
      FieldName = 'Sigla1'
      Size = 6
    end
    object QrItsCSigla2: TWideStringField
      FieldName = 'Sigla2'
      Size = 6
    end
    object QrItsCSigla3: TWideStringField
      FieldName = 'Sigla3'
      Size = 6
    end
    object QrItsCSigla4: TWideStringField
      FieldName = 'Sigla4'
      Size = 6
    end
    object QrItsCDESCRICAO: TWideStringField
      DisplayWidth = 255
      FieldKind = fkCalculated
      FieldName = 'DESCRICAO'
      Size = 255
      Calculated = True
    end
    object QrItsCControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrItsCKGT: TLargeintField
      FieldName = 'KGT'
      Required = True
    end
    object QrItsCTipDimens: TSmallintField
      FieldName = 'TipDimens'
    end
  end
  object frxDsItsN: TfrxDBDataset
    UserName = 'frxDsItsN'
    CloseDataSource = False
    DataSet = QrItsN
    BCDToCurrency = False
    Left = 776
    Top = 32
  end
  object frxDsPediVda: TfrxDBDataset
    UserName = 'frxDsPediVda'
    CloseDataSource = False
    DataSet = QrPediVda
    BCDToCurrency = False
    Left = 4
    Top = 16
  end
  object frxDsItsC: TfrxDBDataset
    UserName = 'frxDsItsC'
    CloseDataSource = False
    DataSet = QrItsC
    BCDToCurrency = False
    Left = 776
    Top = 60
  end
  object QrItsZ: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrItsZCalcFields
    SQL.Strings = (
      'SELECT gg1.CodUsu CU_NIVEL1, gg1.Nome NO_NIVEL1,'
      'gg1.TipDimens, gti.Nome NO_TAM, gcc.Nome NO_COR,   '
      'pvi.QuantP, pvi.PrecoR, pvi.DescoP, pvi.PrecoF, pvi.ValBru, '
      'pvi.ValLiq, pvi.MedidaC, pvi.MedidaL, pvi.MedidaA, '
      'pvi.MedidaE, pvi.PerCustom, mpc.Nome NO_MatPartCad,'
      'med.Medida1, med.Medida2, med.Medida3, med.Medida4, '
      'med.Sigla1, med.Sigla2, med.Sigla3, med.Sigla4, 0 KGT'
      'FROM pedivdacuz pvi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=pvi.GraGrux'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN medordem med ON med.Codigo=gg1.MedOrdem'
      'LEFT JOIN matpartcad mpc ON mpc.Codigo=pvi.MatPartCad'
      'WHERE pvi.Controle=:P0'
      '')
    Left = 748
    Top = 88
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrItsZCU_NIVEL1: TIntegerField
      FieldName = 'CU_NIVEL1'
      Required = True
    end
    object QrItsZNO_NIVEL1: TWideStringField
      FieldName = 'NO_NIVEL1'
      Size = 30
    end
    object QrItsZNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object QrItsZNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrItsZQuantP: TFloatField
      FieldName = 'QuantP'
    end
    object QrItsZPrecoR: TFloatField
      FieldName = 'PrecoR'
    end
    object QrItsZDescoP: TFloatField
      FieldName = 'DescoP'
    end
    object QrItsZPrecoF: TFloatField
      FieldName = 'PrecoF'
    end
    object QrItsZValBru: TFloatField
      FieldName = 'ValBru'
    end
    object QrItsZValLiq: TFloatField
      FieldName = 'ValLiq'
    end
    object QrItsZMedidaC: TFloatField
      FieldName = 'MedidaC'
    end
    object QrItsZMedidaL: TFloatField
      FieldName = 'MedidaL'
    end
    object QrItsZMedidaA: TFloatField
      FieldName = 'MedidaA'
    end
    object QrItsZMedidaE: TFloatField
      FieldName = 'MedidaE'
    end
    object QrItsZPerCustom: TFloatField
      FieldName = 'PerCustom'
    end
    object QrItsZNO_MatPartCad: TWideStringField
      FieldName = 'NO_MatPartCad'
      Size = 50
    end
    object QrItsZMedida1: TWideStringField
      FieldName = 'Medida1'
    end
    object QrItsZMedida2: TWideStringField
      FieldName = 'Medida2'
    end
    object QrItsZMedida3: TWideStringField
      FieldName = 'Medida3'
    end
    object QrItsZMedida4: TWideStringField
      FieldName = 'Medida4'
    end
    object QrItsZSigla1: TWideStringField
      FieldName = 'Sigla1'
      Size = 6
    end
    object QrItsZSigla2: TWideStringField
      FieldName = 'Sigla2'
      Size = 6
    end
    object QrItsZSigla3: TWideStringField
      FieldName = 'Sigla3'
      Size = 6
    end
    object QrItsZDESCRICAO: TWideStringField
      DisplayWidth = 255
      FieldKind = fkCalculated
      FieldName = 'DESCRICAO'
      Size = 255
      Calculated = True
    end
    object QrItsZSigla4: TWideStringField
      FieldName = 'Sigla4'
      Size = 6
    end
    object QrItsZKGT: TLargeintField
      FieldName = 'KGT'
      Required = True
    end
    object QrItsZTipDimens: TSmallintField
      FieldName = 'TipDimens'
    end
  end
  object frxDsItsZ: TfrxDBDataset
    UserName = 'frxDsItsZ'
    CloseDataSource = False
    DataSet = QrItsZ
    BCDToCurrency = False
    Left = 776
    Top = 88
  end
end
