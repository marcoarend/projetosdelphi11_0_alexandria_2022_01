object frmMain: TfrmMain
  Left = 0
  Top = 0
  Caption = 'TDockTabSetDemo Application'
  ClientHeight = 341
  ClientWidth = 643
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 25
    Top = 49
    Height = 292
    ExplicitLeft = 272
    ExplicitTop = 160
    ExplicitHeight = 100
  end
  object DockTabSet1: TDockTabSet
    Left = 0
    Top = 49
    Width = 25
    Height = 292
    Align = alLeft
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ShrinkToFit = True
    Style = tsModernPopout
    TabPosition = tpLeft
    DockSite = False
    DestinationDockSite = pDockLeft
    OnDockDrop = DockTabSet1DockDrop
    OnTabRemoved = DockTabSet1TabRemoved
    ExplicitLeft = -3
    ExplicitTop = 41
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 643
    Height = 49
    Align = alTop
    BevelKind = bkTile
    BevelOuter = bvNone
    TabOrder = 1
    object Button1: TButton
      Left = 8
      Top = 10
      Width = 75
      Height = 25
      Caption = 'Show Form'
      TabOrder = 0
      OnClick = Button1Click
    end
  end
  object pDockLeft: TPanel
    Left = 28
    Top = 49
    Width = 0
    Height = 292
    Align = alLeft
    BevelOuter = bvNone
    DockSite = True
    TabOrder = 2
    OnDockDrop = pDockLeftDockDrop
    OnDockOver = pDockLeftDockOver
    OnUnDock = pDockLeftUnDock
    ExplicitLeft = 25
    ExplicitTop = 41
    ExplicitHeight = 300
  end
  object Memo1: TMemo
    Left = 28
    Top = 49
    Width = 615
    Height = 292
    Align = alClient
    Lines.Strings = (
      'Memo1')
    TabOrder = 3
    ExplicitLeft = 31
    ExplicitTop = 41
  end
end
