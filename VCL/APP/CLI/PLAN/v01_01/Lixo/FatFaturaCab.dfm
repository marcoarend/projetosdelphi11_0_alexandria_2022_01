object FmFatPedCab: TFmFatPedCab
  Left = 368
  Top = 194
  Caption = 'FAT-PEDID-001 :: Faturamento de Pedido'
  ClientHeight = 456
  ClientWidth = 792
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 408
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Visible = False
    object PainelConfirma: TPanel
      Left = 1
      Top = 359
      Width = 790
      Height = 48
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 8
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
        NumGlyphs = 2
      end
      object Panel1: TPanel
        Left = 681
        Top = 1
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
          NumGlyphs = 2
        end
      end
    end
    object PainelEdit: TPanel
      Left = 1
      Top = 1
      Width = 790
      Height = 92
      Align = alTop
      TabOrder = 0
      object Label7: TLabel
        Left = 4
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label8: TLabel
        Left = 64
        Top = 4
        Width = 57
        Height = 13
        Caption = 'C'#243'digo: [F4]'
      end
      object Label9: TLabel
        Left = 148
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object Label15: TLabel
        Left = 580
        Top = 4
        Width = 66
        Height = 13
        Caption = '% juros / m'#234's:'
      end
      object Label16: TLabel
        Left = 684
        Top = 4
        Width = 83
        Height = 13
        Caption = '% desconto m'#225'x.:'
      end
      object EdCodigo: TdmkEdit
        Left = 4
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdNome: TdmkEdit
        Left = 148
        Top = 20
        Width = 429
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdCodUsu: TdmkEdit
        Left = 64
        Top = 20
        Width = 80
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CodUsu'
        UpdCampo = 'CodUsu'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnKeyDown = EdCodUsuKeyDown
      end
      object EdJurosMes: TdmkEdit
        Left = 580
        Top = 20
        Width = 100
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        QryCampo = 'JurosMes'
        UpdCampo = 'JurosMes'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
      object EdMaxDesco: TdmkEdit
        Left = 684
        Top = 20
        Width = 100
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        QryCampo = 'MaxDesco'
        UpdCampo = 'MaxDesco'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
      object CGAplicacao: TdmkCheckGroup
        Left = 4
        Top = 44
        Width = 781
        Height = 41
        Caption = ' Aplica'#231#227'o: '
        Columns = 3
        Items.Strings = (
          'Pedidos de venda'
          'Pedidos de compra'
          'Venda balc'#227'o')
        TabOrder = 5
        QryCampo = 'Aplicacao'
        UpdCampo = 'Aplicacao'
        UpdType = utYes
        Value = 0
        OldValor = 0
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 408
    Align = alClient
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object PnCabeca: TPanel
      Left = 1
      Top = 1
      Width = 790
      Height = 48
      Align = alTop
      Enabled = False
      TabOrder = 1
      object Label1: TLabel
        Left = 4
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object DBEdCodigo: TDBEdit
        Left = 4
        Top = 20
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsPediPrzCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
    end
    object PainelControle: TPanel
      Left = 1
      Top = 359
      Width = 790
      Height = 48
      Align = alBottom
      TabOrder = 0
      object LaRegistro: TdmkLabel
        Left = 173
        Top = 1
        Width = 26
        Height = 13
        Align = alClient
        Caption = '[N]: 0'
        UpdType = utYes
        SQLType = stNil
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 172
        Height = 46
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
          NumGlyphs = 2
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
          NumGlyphs = 2
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
          NumGlyphs = 2
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
          NumGlyphs = 2
        end
      end
      object Panel3: TPanel
        Left = 320
        Top = 1
        Width = 469
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object BtItens: TBitBtn
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Itens'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtItensClick
          NumGlyphs = 2
        end
        object BtFatura: TBitBtn
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Fatura'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtFaturaClick
          NumGlyphs = 2
        end
        object Panel2: TPanel
          Left = 360
          Top = 0
          Width = 109
          Height = 46
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 2
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
            NumGlyphs = 2
          end
        end
        object BtFiliais: TBitBtn
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Filiais'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtFiliaisClick
          NumGlyphs = 2
        end
      end
    end
    object Panel4: TPanel
      Left = 1
      Top = 236
      Width = 790
      Height = 123
      Align = alBottom
      TabOrder = 2
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 48
    Align = alTop
    Caption = '                              Faturamento de Pedido'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object LaTipo: TdmkLabel
      Left = 709
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 603
      ExplicitTop = 2
      ExplicitHeight = 44
    end
    object Image1: TImage
      Left = 226
      Top = 1
      Width = 483
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 227
      ExplicitTop = 2
      ExplicitWidth = 376
      ExplicitHeight = 44
    end
    object PanelFill2: TPanel
      Left = 1
      Top = 1
      Width = 225
      Height = 46
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 0
        NumGlyphs = 2
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 1
        OnClick = SbNovoClick
        NumGlyphs = 2
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 2
        OnClick = SbNumeroClick
        NumGlyphs = 2
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 3
        OnClick = SbNomeClick
        NumGlyphs = 2
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 4
        OnClick = SbQueryClick
        NumGlyphs = 2
      end
    end
  end
  object DsPediPrzCab: TDataSource
    DataSet = QrPediPrzCab
    Left = 40
    Top = 12
  end
  object QrPediPrzCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrPediPrzCabBeforeOpen
    AfterOpen = QrPediPrzCabAfterOpen
    BeforeClose = QrPediPrzCabBeforeClose
    AfterScroll = QrPediPrzCabAfterScroll
    SQL.Strings = (
      'SELECT ppc.Codigo, ppc.CodUsu, ppc.Nome,'
      'ppc.MaxDesco, ppc.JurosMes, ppc.Parcelas,'
      'ppc.MedDDSimpl, ppc.MedDDReal, ppc.MedDDPerc1,'
      'ppc.MedDDPerc2, ppc.PercentT, ppc.Percent1,'
      'ppc.Percent2, ppc.Aplicacao'
      'FROM pediprzcab ppc')
    Left = 12
    Top = 12
    object QrPediPrzCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPediPrzCabCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrPediPrzCabNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
    object QrPediPrzCabMaxDesco: TFloatField
      FieldName = 'MaxDesco'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediPrzCabJurosMes: TFloatField
      FieldName = 'JurosMes'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediPrzCabParcelas: TIntegerField
      FieldName = 'Parcelas'
      Required = True
      DisplayFormat = '0'
    end
    object QrPediPrzCabMedDDSimpl: TFloatField
      FieldName = 'MedDDSimpl'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediPrzCabMedDDReal: TFloatField
      FieldName = 'MedDDReal'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediPrzCabMedDDPerc1: TFloatField
      FieldName = 'MedDDPerc1'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediPrzCabMedDDPerc2: TFloatField
      FieldName = 'MedDDPerc2'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediPrzCabPercentT: TFloatField
      FieldName = 'PercentT'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediPrzCabPercent1: TFloatField
      FieldName = 'Percent1'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediPrzCabPercent2: TFloatField
      FieldName = 'Percent2'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediPrzCabAplicacao: TSmallintField
      FieldName = 'Aplicacao'
      Required = True
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtFatura
    CanUpd01 = BtItens
    Left = 68
    Top = 12
  end
  object PMFatura: TPopupMenu
    OnPopup = PMFaturaPopup
    Left = 344
    Top = 376
    object Incluinovofaturamento1: TMenuItem
      Caption = '&Inclui novo faturamento'
      OnClick = Incluinovofaturamento1Click
    end
    object Alterafaturamentoatual1: TMenuItem
      Caption = '&Altera faturamento atual'
      OnClick = Alterafaturamentoatual1Click
    end
    object Excluifaturamentoatual1: TMenuItem
      Caption = '&Exclui faturamento atual'
      Enabled = False
    end
  end
  object QrPediPrzIts: TmySQLQuery
    Database = Dmod.MyDB
    DataSource = DsPediPrzCab
    SQL.Strings = (
      'SELECT ppi.Controle, ppi.Dias, '
      'ppi.Percent1, ppi.Percent2,'
      'ppi.Percent1 + ppi.Percent2 PERCENTT'
      'FROM pediprzits ppi'
      'WHERE ppi.Codigo=:P0'
      'ORDER BY Dias')
    Left = 524
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPediPrzItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPediPrzItsDias: TIntegerField
      FieldName = 'Dias'
      Required = True
    end
    object QrPediPrzItsPercent1: TFloatField
      FieldName = 'Percent1'
      Required = True
      DisplayFormat = '0.00;-0.00; '
    end
    object QrPediPrzItsPercent2: TFloatField
      FieldName = 'Percent2'
      Required = True
      DisplayFormat = '0.00;-0.00; '
    end
    object QrPediPrzItsPERCENTT: TFloatField
      FieldName = 'PERCENTT'
      Required = True
      DisplayFormat = '0.00;-0.00; '
    end
  end
  object DsPediPrzIts: TDataSource
    DataSet = QrPediPrzIts
    Left = 552
    Top = 8
  end
  object QrPediPrzEmp: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ppe.Controle, ppe.Empresa, ent.Filial,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEFILIAL'
      'FROM pediprzemp ppe'
      'LEFT JOIN entidades ent ON ent.Codigo=ppe.Empresa'
      'WHERE ppe.Codigo=:P0'
      'ORDER BY NOMEFILIAL'
      '')
    Left = 584
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPediPrzEmpControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPediPrzEmpEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrPediPrzEmpFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrPediPrzEmpNOMEFILIAL: TWideStringField
      FieldName = 'NOMEFILIAL'
      Size = 100
    end
  end
  object DsPediPrzEmp: TDataSource
    DataSet = QrPediPrzEmp
    Left = 612
    Top = 8
  end
end
