unit DockForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs;

type
  TfrmDock = class(TForm)
    procedure FormStartDock(Sender: TObject; var DragObject: TDragDockObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
    class function CreateDockForm(const aColor: TColor): TCustomForm;
  end;

implementation

{$R *.dfm}

class function TfrmDock.CreateDockForm(const aColor: TColor): TCustomForm;
begin
  result := TfrmDock.Create(Application);
  result.Color := aColor;
  result.Caption := ColorToString(aColor);
  result.Show;
end;

procedure TfrmDock.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ManualFloat(Rect(0, 0, 0, 0));
  Action := caFree;
end;

procedure TfrmDock.FormStartDock(Sender: TObject; var DragObject: TDragDockObject);
begin
  DragObject := TDragDockObjectEx.Create(Self);
  DragObject.Brush.Color := clAqua; // this will display a red outline
end;

end.
