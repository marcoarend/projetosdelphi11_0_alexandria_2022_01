unit PrinCad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, Grids, DBGrids, dmkDBGrid,
  Menus, dmkCheckGroup, UnDmkProcFunc;

type
  TFmPrinCad = class(TForm)
    PainelDados: TPanel;
    DsPrinCad: TDataSource;
    QrPrinCad: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TdmkLabel;
    Image1: TImage;
    PanelFill2: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TdmkLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtLinha: TBitBtn;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    Label8: TLabel;
    EdCodUsu: TdmkEdit;
    PMConfigura: TPopupMenu;
    QrPrinCmd_0: TmySQLQuery;
    DsPrinCmd_0: TDataSource;
    BtConfigura: TBitBtn;
    Panel4: TPanel;
    QrPrinCadCodigo: TIntegerField;
    QrPrinCadCodUsu: TIntegerField;
    QrPrinCadNome: TWideStringField;
    QrPrinCadTypeUso: TSmallintField;
    QrPrinCadNOMETYPEUSO: TWideStringField;
    QrPrinCadTypePrn: TSmallintField;
    QrPrinCadNOMETYPEPRN: TWideStringField;
    QrPrinCadPortaImp: TSmallintField;
    QrPrinCadNOMEPORTAIMP: TWideStringField;
    QrPrinCadPortaIdx: TSmallintField;
    QrPrinCadModelo: TWideStringField;
    QrPrinCadColunas: TSmallintField;
    SpeedButton5: TSpeedButton;
    RGTypeUso: TdmkRadioGroup;
    RGTypePrn: TdmkRadioGroup;
    EdModelo: TdmkEdit;
    Label2: TLabel;
    RGPortaImp: TdmkRadioGroup;
    EdPortaIdx: TdmkEdit;
    Label9: TLabel;
    Label14: TLabel;
    EdColunas: TdmkEdit;
    PnCabeca: TPanel;
    Label13: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    Incluinovaconfigurao1: TMenuItem;
    Alteraconfiguraoatual1: TMenuItem;
    Excluiconfiguraoatual1: TMenuItem;
    QrPrinCmd_0Codigo: TIntegerField;
    QrPrinCmd_0Tipo: TSmallintField;
    QrPrinCmd_0Ordem: TIntegerField;
    QrPrinCmd_0Controle: TIntegerField;
    QrPrinCmd_0Config: TWideStringField;
    QrPrinCmd_1: TmySQLQuery;
    DsPrinCmd_1: TDataSource;
    QrPrinCmd_2: TmySQLQuery;
    DsPrinCmd_2: TDataSource;
    QrPrinCmd_1Codigo: TIntegerField;
    QrPrinCmd_1Tipo: TSmallintField;
    QrPrinCmd_1Ordem: TIntegerField;
    QrPrinCmd_1Controle: TIntegerField;
    QrPrinCmd_1Config: TWideStringField;
    QrPrinCmd_2Codigo: TIntegerField;
    QrPrinCmd_2Tipo: TSmallintField;
    QrPrinCmd_2Ordem: TIntegerField;
    QrPrinCmd_2Controle: TIntegerField;
    QrPrinCmd_2Config: TWideStringField;
    PMLinha: TPopupMenu;
    Incluinovalinhadecomando1: TMenuItem;
    Alteralinhadecomandoatual1: TMenuItem;
    Excluilinhadecomandoatual1: TMenuItem;
    QrOrd: TmySQLQuery;
    QrOrdOrdem: TIntegerField;
    QrOrdControle: TIntegerField;
    Incio1: TMenuItem;
    Dados1: TMenuItem;
    Final1: TMenuItem;
    Panel_1: TPanel;
    Panel7: TPanel;
    DBGCmd_1: TDBGrid;
    Panel_0: TPanel;
    Panel9: TPanel;
    DBGCmd_0: TDBGrid;
    Panel_2: TPanel;
    Panel11: TPanel;
    DBGCmd_2: TDBGrid;
    BtAcima_0: TBitBtn;
    BtAbaixo_0: TBitBtn;
    BtAcima_1: TBitBtn;
    BtAbaixo_1: TBitBtn;
    BtAcima_2: TBitBtn;
    BtAbaixo_2: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrPrinCadAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrPrinCadBeforeOpen(DataSet: TDataSet);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure BtConfiguraClick(Sender: TObject);
    procedure PMConfiguraPopup(Sender: TObject);
    procedure QrPrinCadBeforeClose(DataSet: TDataSet);
    procedure QrPrinCadAfterScroll(DataSet: TDataSet);
    procedure SpeedButton5Click(Sender: TObject);
    procedure Incluinovaconfigurao1Click(Sender: TObject);
    procedure Alteraconfiguraoatual1Click(Sender: TObject);
    procedure QrPrinCmd_0AfterOpen(DataSet: TDataSet);
    procedure QrPrinCmd_2AfterOpen(DataSet: TDataSet);
    procedure Incluinovalinhadecomando1Click(Sender: TObject);
    procedure Alteralinhadecomandoatual1Click(Sender: TObject);
    procedure Excluilinhadecomandoatual1Click(Sender: TObject);
    procedure BtLinhaClick(Sender: TObject);
    procedure Incio1Click(Sender: TObject);
    procedure Dados1Click(Sender: TObject);
    procedure Final1Click(Sender: TObject);
    procedure BtAcima_0Click(Sender: TObject);
    procedure BtAbaixo_0Click(Sender: TObject);
    procedure BtAcima_1Click(Sender: TObject);
    procedure BtAbaixo_1Click(Sender: TObject);
    procedure BtAcima_2Click(Sender: TObject);
    procedure BtAbaixo_2Click(Sender: TObject);
    procedure QrPrinCmd_0BeforeClose(DataSet: TDataSet);
    procedure QrPrinCmd_1BeforeClose(DataSet: TDataSet);
    procedure QrPrinCmd_2BeforeClose(DataSet: TDataSet);
    procedure QrPrinCmd_0AfterScroll(DataSet: TDataSet);
    procedure QrPrinCmd_1AfterScroll(DataSet: TDataSet);
    procedure QrPrinCmd_2AfterScroll(DataSet: TDataSet);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; Status: String; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraEdicaoCmd(TipoLinha: Integer);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenPrinCmd_0(Controle: Integer);
    procedure ReopenPrinCmd_1(Controle: Integer);
    procedure ReopenPrinCmd_2(Controle: Integer);
    procedure ReordenaLinhas(Tipo, Controle, Ordem: Integer);
  end;

var
  FmPrinCad: TFmPrinCad;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, PediPrzIts, MyDBCheck, MasterSelFilial, PrinCmd;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmPrinCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmPrinCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrPrinCadCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmPrinCad.DefParams;
begin
  VAR_GOTOTABELA := 'PrinCad';
  VAR_GOTOMYSQLTABLE := QrPrinCad;
  VAR_GOTONEG := True;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT prc.Codigo, prc.CodUsu, prc.Nome, prc.TypeUso,');
  VAR_SQLx.Add('ELT(prc.TypeUso + 1, "Produto", "Pessoa", "Expedi��o")');
  VAR_SQLx.Add('NOMETYPEUSO,  prc.TypePrn, ELT(prc.TypePrn + 1, "T�rmica",');
  VAR_SQLx.Add('"Matricial") NOMETYPEPRN, prc.PortaImp, ELT(prc.PortaImp+1,');
  VAR_SQLx.Add('"COM", "LPT") NOMEPORTAIMP, prc.PortaIdx, prc.Modelo,');
  VAR_SQLx.Add('prc.Colunas');
  VAR_SQLx.Add('FROM princad prc');
  VAR_SQLx.Add('WHERE prc.Codigo > -1000');
  //
  VAR_SQL1.Add('AND prc.Codigo=:P0');
  //
  VAR_SQL2.Add('AND prc.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND prc.Nome Like :P0');
  //
end;

procedure TFmPrinCad.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (LaTipo.SQLType = stIns) then
    EdCodUsu.ValueVariant :=
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'PrinCad', 'CodUsu', [], []);
end;

procedure TFmPrinCad.Excluilinhadecomandoatual1Click(Sender: TObject);
begin
//  Parei Aqui! Fazer
end;

procedure TFmPrinCad.MostraEdicao(Mostra: Integer; Status: String; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelControle.Visible:=True;
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    else Geral.MensagemBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  LaTipo.Caption := Status;
  GOTOy.BotoesSb(LaTipo.Caption);
end;

procedure TFmPrinCad.MostraEdicaoCmd(TipoLinha: Integer);
var
  Query: TmySQLQuery;
begin
  case TipoLinha of
    0: Query := QrPrinCmd_0;
    1: Query := QrPrinCmd_1;
    2: Query := QrPrinCmd_2;
  end;
  UmyMod.CriaFormInsUpd(TFmPrinCmd, FmPrinCmd, afmoNegarComAviso, Query, stUpd);
end;

procedure TFmPrinCad.PMConfiguraPopup(Sender: TObject);
begin
  Alteraconfiguraoatual1.Enabled :=
    (QrPrinCad.State <> dsInactive) and (QrPrinCad.RecordCount > 0);
end;

procedure TFmPrinCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmPrinCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmPrinCad.Dados1Click(Sender: TObject);
begin
  MostraEdicaoCmd(1);
end;

procedure TFmPrinCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmPrinCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmPrinCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmPrinCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmPrinCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmPrinCad.SpeedButton5Click(Sender: TObject);
begin
  EdCodUsu.ValueVariant := DBCheck.ObtemCodUsuZStepCodUni(tscPrintCad);
end;

procedure TFmPrinCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrPrinCadCodigo.Value;
  Close;
end;

procedure TFmPrinCad.Alteraconfiguraoatual1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrPrinCad, [PainelDados],
  [PainelEdita], EdCodUsu, LaTipo, 'PrinCad');
end;

procedure TFmPrinCad.Alteralinhadecomandoatual1Click(Sender: TObject);
begin
//  Parei Aqui! Fazer
end;

procedure TFmPrinCad.BtAcima_0Click(Sender: TObject);
begin
  ReordenaLinhas(0, QrPrinCmd_0Controle.Value, QrPrinCmd_0Ordem.Value - 1);
end;

procedure TFmPrinCad.BtAbaixo_0Click(Sender: TObject);
begin
  ReordenaLinhas(0, QrPrinCmd_0Controle.Value, QrPrinCmd_0Ordem.Value + 1);
end;

procedure TFmPrinCad.BtAcima_1Click(Sender: TObject);
begin
  ReordenaLinhas(1, QrPrinCmd_1Controle.Value, QrPrinCmd_1Ordem.Value - 1);
end;

procedure TFmPrinCad.BtAbaixo_1Click(Sender: TObject);
begin
  ReordenaLinhas(1, QrPrinCmd_1Controle.Value, QrPrinCmd_1Ordem.Value + 1);
end;

procedure TFmPrinCad.BtAcima_2Click(Sender: TObject);
begin
  ReordenaLinhas(2, QrPrinCmd_2Controle.Value, QrPrinCmd_2Ordem.Value - 1);
end;

procedure TFmPrinCad.BtAbaixo_2Click(Sender: TObject);
begin
  ReordenaLinhas(2, QrPrinCmd_2Controle.Value, QrPrinCmd_2Ordem.Value + 1);
end;

procedure TFmPrinCad.BtConfiguraClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMConfigura, BtConfigura);
end;

procedure TFmPrinCad.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Geral.MensagemBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Codigo := UMyMod.BuscaEmLivreY_Def('PrinCad', 'Codigo', LaTipo.SQLType,
    QrPrinCadCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(LaTipo.SQLType, FmPrinCad, PainelEdit,
    'PrinCad', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], LaTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmPrinCad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if LaTipo.Caption = CO_INCLUSAO then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'PrinCad', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'PrinCad', 'Codigo');
  MostraEdicao(0, CO_TRAVADO, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'PrinCad', 'Codigo');
end;

procedure TFmPrinCad.BtLinhaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMLinha, BtLinha);
end;

procedure TFmPrinCad.FormCreate(Sender: TObject);
begin
  PainelEdit.Align  := alClient;
  Panel4.Align      := alClient;
  CriaOForm;
end;

procedure TFmPrinCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrPrinCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmPrinCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmPrinCad.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrPrinCadCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmPrinCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmPrinCad.QrPrinCadAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmPrinCad.QrPrinCadAfterScroll(DataSet: TDataSet);
begin
  ReopenPrinCmd_0(0);
  ReopenPrinCmd_1(0);
  ReopenPrinCmd_2(0);
end;

procedure TFmPrinCad.Final1Click(Sender: TObject);
begin
  MostraEdicaoCmd(2);
end;

procedure TFmPrinCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPrinCad.SbQueryClick(Sender: TObject);
begin
  LocCod(QrPrinCadCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'PrinCad', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmPrinCad.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmPrinCad.Incio1Click(Sender: TObject);
begin
  MostraEdicaoCmd(0);
end;

procedure TFmPrinCad.Incluinovaconfigurao1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrPrinCad, [PainelDados],
  [PainelEdita], EdCodUsu, LaTipo, 'PrinCad');
end;

procedure TFmPrinCad.Incluinovalinhadecomando1Click(Sender: TObject);
begin
  UmyMod.CriaFormInsUpd(TFmPrinCmd, FmPrinCmd, afmoNegarComAviso,
  nil, stIns);
end;

procedure TFmPrinCad.QrPrinCadBeforeClose(DataSet: TDataSet);
begin
  QrPrinCmd_0.Close;
  QrPrinCmd_1.Close;
  QrPrinCmd_2.Close;
end;

procedure TFmPrinCad.QrPrinCadBeforeOpen(DataSet: TDataSet);
begin
  QrPrinCadCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmPrinCad.QrPrinCmd_0AfterOpen(DataSet: TDataSet);
var
  Tam: Integer;
begin
  Tam := 57 + ((QrPrinCmd_0.RecordCount -1) * 18);
  if Tam < 88 then Tam := 88;
  DBGCmd_0.Height := Tam;
end;

procedure TFmPrinCad.QrPrinCmd_0AfterScroll(DataSet: TDataSet);
begin
  BtAcima_0.Enabled := QrPrinCmd_0.RecNo > 1;
  BtAbaixo_0.Enabled := QrPrinCmd_0.RecNo < QrPrinCmd_0.RecordCount;
end;

procedure TFmPrinCad.QrPrinCmd_0BeforeClose(DataSet: TDataSet);
begin
  BtAcima_0.Enabled := False;
  BtAbaixo_0.Enabled := False;
end;

procedure TFmPrinCad.QrPrinCmd_1AfterScroll(DataSet: TDataSet);
begin
  BtAcima_1.Enabled := QrPrinCmd_1.RecNo > 1;
  BtAbaixo_1.Enabled := QrPrinCmd_1.RecNo < QrPrinCmd_1.RecordCount;
end;

procedure TFmPrinCad.QrPrinCmd_1BeforeClose(DataSet: TDataSet);
begin
  BtAcima_1.Enabled := False;
  BtAbaixo_1.Enabled := False;
end;

procedure TFmPrinCad.QrPrinCmd_2AfterOpen(DataSet: TDataSet);
var
  Tam: Integer;
begin
  Tam := 57 + ((QrPrinCmd_2.RecordCount -1) * 18);
  if Tam < 88 then Tam := 88;
  DBGCmd_2.Height := Tam;
end;

procedure TFmPrinCad.QrPrinCmd_2AfterScroll(DataSet: TDataSet);
begin
  BtAcima_2.Enabled := QrPrinCmd_2.RecNo > 1;
  BtAbaixo_2.Enabled := QrPrinCmd_2.RecNo < QrPrinCmd_2.RecordCount;
end;

procedure TFmPrinCad.QrPrinCmd_2BeforeClose(DataSet: TDataSet);
begin
  BtAcima_2.Enabled := False;
  BtAbaixo_2.Enabled := False;
end;

procedure TFmPrinCad.ReopenPrinCmd_0(Controle: Integer);
begin
  QrPrinCmd_0.Close;
  QrPrinCmd_0.Params[0].AsInteger := QrPrinCadCodigo.Value;
  QrPrinCmd_0.Open;
  //
  if Controle <> 0 then
    QrPrinCmd_0.Locate('Controle', Controle, []);
end;

procedure TFmPrinCad.ReopenPrinCmd_1(Controle: Integer);
begin
  QrPrinCmd_1.Close;
  QrPrinCmd_1.Params[0].AsInteger := QrPrinCadCodigo.Value;
  QrPrinCmd_1.Open;
  //
  if Controle <> 0 then
    QrPrinCmd_1.Locate('Controle', Controle, []);
end;

procedure TFmPrinCad.ReopenPrinCmd_2(Controle: Integer);
begin
  QrPrinCmd_2.Close;
  QrPrinCmd_2.Params[0].AsInteger := QrPrinCadCodigo.Value;
  QrPrinCmd_2.Open;
  //
  if Controle <> 0 then
    QrPrinCmd_2.Locate('Controle', Controle, []);
end;

procedure TFmPrinCad.ReordenaLinhas(Tipo, Controle, Ordem: Integer);
var
  n: Integer;
begin
  QrOrd.Close;
  QrOrd.Params[00].AsInteger := Tipo;
  QrOrd.Params[01].AsInteger := QrPrinCadCodigo.Value;
  QrOrd.Open;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE princmd SET Ordem=:P0 ');
  Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
  n := 0;
  while not QrOrd.Eof do
  begin
    if QrOrdControle.Value <> Controle then
    begin
      n := n + 1;
      if n = Ordem then
        n := n + 1;
      Dmod.QrUpd.Params[00].AsInteger := n;
      Dmod.QrUpd.Params[01].AsInteger := QrOrdControle.Value;
      Dmod.QrUpd.ExecSQL;
    end else begin
      Dmod.QrUpd.Params[00].AsInteger := Ordem;
      Dmod.QrUpd.Params[01].AsInteger := QrOrdControle.Value;
      Dmod.QrUpd.ExecSQL;
    end;
    QrOrd.Next;
  end;
  case Tipo of
    0: ReopenPrinCmd_0(Controle);
    1: ReopenPrinCmd_1(Controle);
    2: ReopenPrinCmd_2(Controle);
  end;
end;

end.

