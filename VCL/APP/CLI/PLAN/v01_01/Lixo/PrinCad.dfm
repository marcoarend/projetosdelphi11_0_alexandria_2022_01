object FmPrinCad: TFmPrinCad
  Left = 368
  Top = 194
  Caption = 'ETQ-PRINT-001 :: Configura'#231#227'o de Impressoras'
  ClientHeight = 482
  ClientWidth = 792
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 434
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Visible = False
    object PainelConfirma: TPanel
      Left = 1
      Top = 385
      Width = 790
      Height = 48
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 8
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
        NumGlyphs = 2
      end
      object Panel1: TPanel
        Left = 681
        Top = 1
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
          NumGlyphs = 2
        end
      end
    end
    object PainelEdit: TPanel
      Left = 1
      Top = 1
      Width = 790
      Height = 92
      Align = alTop
      TabOrder = 0
      object Label7: TLabel
        Left = 720
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
      end
      object Label8: TLabel
        Left = 224
        Top = 4
        Width = 38
        Height = 13
        Caption = 'Modelo:'
      end
      object SpeedButton5: TSpeedButton
        Left = 280
        Top = 20
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton5Click
      end
      object Label2: TLabel
        Left = 224
        Top = 44
        Width = 106
        Height = 13
        Caption = 'Modelo da impressora:'
      end
      object Label9: TLabel
        Left = 684
        Top = 44
        Width = 42
        Height = 13
        Caption = 'N'#186' porta:'
      end
      object Label14: TLabel
        Left = 732
        Top = 44
        Width = 41
        Height = 13
        Caption = 'Colunas:'
      end
      object EdCodigo: TdmkEdit
        Left = 720
        Top = 20
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdNome: TdmkEdit
        Left = 304
        Top = 20
        Width = 412
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdCodUsu: TdmkEdit
        Left = 224
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CodUsu'
        UpdCampo = 'CodUsu'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnKeyDown = EdCodUsuKeyDown
      end
      object RGTypeUso: TdmkRadioGroup
        Left = 8
        Top = 4
        Width = 213
        Height = 38
        Caption = ' Grupo: '
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          'Produto'
          'Pessoa'
          'Expedi'#231#227'o')
        TabOrder = 0
        QryCampo = 'TypeUso'
        UpdCampo = 'TypeUso'
        UpdType = utYes
        OldValor = 0
      end
      object RGTypePrn: TdmkRadioGroup
        Left = 8
        Top = 44
        Width = 213
        Height = 38
        Caption = ' Tipo de impressora: '
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'T'#233'rmica'
          'Matricial')
        TabOrder = 4
        QryCampo = 'TypePrn'
        UpdCampo = 'TypePrn'
        UpdType = utYes
        OldValor = 0
      end
      object EdModelo: TdmkEdit
        Left = 224
        Top = 60
        Width = 340
        Height = 21
        TabOrder = 5
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Modelo'
        UpdCampo = 'Modelo'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object RGPortaImp: TdmkRadioGroup
        Left = 568
        Top = 44
        Width = 113
        Height = 38
        Caption = ' Porta de impress'#227'o: '
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'COM'
          'LPT')
        TabOrder = 6
        QryCampo = 'PortaImp'
        UpdCampo = 'PortaImp'
        UpdType = utYes
        OldValor = 0
      end
      object EdPortaIdx: TdmkEdit
        Left = 684
        Top = 60
        Width = 44
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 7
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'PortaIdx'
        UpdCampo = 'PortaIdx'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdColunas: TdmkEdit
        Left = 732
        Top = 60
        Width = 44
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Colunas'
        UpdCampo = 'Colunas'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 434
    Align = alClient
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object PainelControle: TPanel
      Left = 1
      Top = 385
      Width = 790
      Height = 48
      Align = alBottom
      TabOrder = 0
      object LaRegistro: TdmkLabel
        Left = 173
        Top = 1
        Width = 26
        Height = 13
        Align = alClient
        Caption = '[N]: 0'
        UpdType = utYes
        SQLType = stNil
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 172
        Height = 46
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
          NumGlyphs = 2
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
          NumGlyphs = 2
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
          NumGlyphs = 2
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
          NumGlyphs = 2
        end
      end
      object Panel3: TPanel
        Left = 320
        Top = 1
        Width = 469
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object BtLinha: TBitBtn
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Linhas'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtLinhaClick
          NumGlyphs = 2
        end
        object BtConfigura: TBitBtn
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Configura'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtConfiguraClick
          NumGlyphs = 2
        end
        object Panel2: TPanel
          Left = 360
          Top = 0
          Width = 109
          Height = 46
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 2
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
            NumGlyphs = 2
          end
        end
      end
    end
    object Panel4: TPanel
      Left = 1
      Top = 121
      Width = 790
      Height = 264
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 1
      object Panel_1: TPanel
        Left = 0
        Top = 88
        Width = 790
        Height = 88
        Align = alClient
        TabOrder = 0
        object Panel7: TPanel
          Left = 1
          Top = 1
          Width = 48
          Height = 86
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object BtAcima_1: TBitBtn
            Tag = 32
            Left = 4
            Top = 3
            Width = 40
            Height = 40
            Cursor = crHandPoint
            Enabled = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtAcima_1Click
            NumGlyphs = 2
          end
          object BtAbaixo_1: TBitBtn
            Tag = 31
            Left = 4
            Top = 43
            Width = 40
            Height = 40
            Cursor = crHandPoint
            Enabled = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BtAbaixo_1Click
            NumGlyphs = 2
          end
        end
        object DBGCmd_1: TDBGrid
          Left = 49
          Top = 1
          Width = 740
          Height = 86
          Align = alClient
          DataSource = DsPrinCmd_1
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Ordem'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Config'
              Width = 717
              Visible = True
            end>
        end
      end
      object Panel_0: TPanel
        Left = 0
        Top = 0
        Width = 790
        Height = 88
        Align = alTop
        TabOrder = 1
        object Panel9: TPanel
          Left = 1
          Top = 1
          Width = 48
          Height = 86
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object BtAcima_0: TBitBtn
            Tag = 32
            Left = 4
            Top = 3
            Width = 40
            Height = 40
            Cursor = crHandPoint
            Enabled = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtAcima_0Click
            NumGlyphs = 2
          end
          object BtAbaixo_0: TBitBtn
            Tag = 31
            Left = 4
            Top = 43
            Width = 40
            Height = 40
            Cursor = crHandPoint
            Enabled = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BtAbaixo_0Click
            NumGlyphs = 2
          end
        end
        object DBGCmd_0: TDBGrid
          Left = 49
          Top = 1
          Width = 740
          Height = 86
          Align = alClient
          DataSource = DsPrinCmd_0
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Ordem'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Config'
              Width = 666
              Visible = True
            end>
        end
      end
      object Panel_2: TPanel
        Left = 0
        Top = 176
        Width = 790
        Height = 88
        Align = alBottom
        TabOrder = 2
        object Panel11: TPanel
          Left = 1
          Top = 1
          Width = 48
          Height = 86
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object BtAcima_2: TBitBtn
            Tag = 32
            Left = 4
            Top = 3
            Width = 40
            Height = 40
            Cursor = crHandPoint
            Enabled = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtAcima_2Click
            NumGlyphs = 2
          end
          object BtAbaixo_2: TBitBtn
            Tag = 31
            Left = 4
            Top = 43
            Width = 40
            Height = 40
            Cursor = crHandPoint
            Enabled = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BtAbaixo_2Click
            NumGlyphs = 2
          end
        end
        object DBGCmd_2: TDBGrid
          Left = 49
          Top = 1
          Width = 740
          Height = 86
          Align = alClient
          DataSource = DsPrinCmd_2
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Ordem'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Config'
              Width = 717
              Visible = True
            end>
        end
      end
    end
    object PnCabeca: TPanel
      Left = 1
      Top = 1
      Width = 790
      Height = 88
      Align = alTop
      Enabled = False
      TabOrder = 2
      object Label13: TLabel
        Left = 720
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdit1
      end
      object Label17: TLabel
        Left = 224
        Top = 4
        Width = 38
        Height = 13
        Caption = 'Modelo:'
        FocusControl = DBEdit11
      end
      object Label18: TLabel
        Left = 8
        Top = 4
        Width = 32
        Height = 13
        Caption = 'Grupo:'
        FocusControl = DBEdit12
      end
      object Label19: TLabel
        Left = 8
        Top = 44
        Width = 89
        Height = 13
        Caption = 'Tipo de impress'#227'o:'
        FocusControl = DBEdit14
      end
      object Label20: TLabel
        Left = 224
        Top = 44
        Width = 106
        Height = 13
        Caption = 'Modelo da impressora:'
        FocusControl = DBEdit16
      end
      object Label21: TLabel
        Left = 568
        Top = 44
        Width = 93
        Height = 13
        Caption = 'Porta de impress'#227'o:'
        FocusControl = DBEdit17
      end
      object Label22: TLabel
        Left = 684
        Top = 44
        Width = 42
        Height = 13
        Caption = 'N'#186' porta:'
        FocusControl = DBEdit19
      end
      object Label23: TLabel
        Left = 732
        Top = 44
        Width = 41
        Height = 13
        Caption = 'Colunas:'
        FocusControl = DBEdit20
      end
      object DBEdit1: TDBEdit
        Left = 720
        Top = 20
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsPrinCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdit2: TDBEdit
        Left = 284
        Top = 20
        Width = 432
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsPrinCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object DBEdit11: TDBEdit
        Left = 224
        Top = 20
        Width = 56
        Height = 21
        DataField = 'CodUsu'
        DataSource = DsPrinCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
      end
      object DBEdit12: TDBEdit
        Left = 8
        Top = 20
        Width = 36
        Height = 21
        DataField = 'TypeUso'
        DataSource = DsPrinCad
        TabOrder = 3
      end
      object DBEdit13: TDBEdit
        Left = 48
        Top = 20
        Width = 173
        Height = 21
        DataField = 'NOMETYPEUSO'
        DataSource = DsPrinCad
        TabOrder = 4
      end
      object DBEdit14: TDBEdit
        Left = 8
        Top = 60
        Width = 36
        Height = 21
        DataField = 'TypePrn'
        DataSource = DsPrinCad
        TabOrder = 5
      end
      object DBEdit15: TDBEdit
        Left = 48
        Top = 60
        Width = 173
        Height = 21
        DataField = 'NOMETYPEPRN'
        DataSource = DsPrinCad
        TabOrder = 6
      end
      object DBEdit16: TDBEdit
        Left = 224
        Top = 60
        Width = 340
        Height = 21
        DataField = 'Modelo'
        DataSource = DsPrinCad
        TabOrder = 7
      end
      object DBEdit17: TDBEdit
        Left = 568
        Top = 60
        Width = 36
        Height = 21
        DataField = 'PortaImp'
        DataSource = DsPrinCad
        TabOrder = 8
      end
      object DBEdit18: TDBEdit
        Left = 608
        Top = 60
        Width = 72
        Height = 21
        DataField = 'NOMEPORTAIMP'
        DataSource = DsPrinCad
        TabOrder = 9
      end
      object DBEdit19: TDBEdit
        Left = 684
        Top = 60
        Width = 45
        Height = 21
        DataField = 'PortaIdx'
        DataSource = DsPrinCad
        TabOrder = 10
      end
      object DBEdit20: TDBEdit
        Left = 732
        Top = 60
        Width = 45
        Height = 21
        DataField = 'Colunas'
        DataSource = DsPrinCad
        TabOrder = 11
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 48
    Align = alTop
    Caption = '                              Configura'#231#227'o de Impressoras'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object LaTipo: TdmkLabel
      Left = 709
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 603
      ExplicitTop = 2
      ExplicitHeight = 44
    end
    object Image1: TImage
      Left = 226
      Top = 1
      Width = 483
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 227
      ExplicitTop = 2
      ExplicitWidth = 376
      ExplicitHeight = 44
    end
    object PanelFill2: TPanel
      Left = 1
      Top = 1
      Width = 225
      Height = 46
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 0
        NumGlyphs = 2
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 1
        OnClick = SbNovoClick
        NumGlyphs = 2
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 2
        OnClick = SbNumeroClick
        NumGlyphs = 2
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 3
        OnClick = SbNomeClick
        NumGlyphs = 2
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 4
        OnClick = SbQueryClick
        NumGlyphs = 2
      end
    end
  end
  object DsPrinCad: TDataSource
    DataSet = QrPrinCad
    Left = 40
    Top = 12
  end
  object QrPrinCad: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrPrinCadBeforeOpen
    AfterOpen = QrPrinCadAfterOpen
    BeforeClose = QrPrinCadBeforeClose
    AfterScroll = QrPrinCadAfterScroll
    SQL.Strings = (
      'SELECT prc.Codigo, prc.CodUsu, prc.Nome, prc.TypeUso,'
      'ELT(prc.TypeUso + 1, "Produto", "Pessoa", "Expedi'#231#227'o") '
      'NOMETYPEUSO,  prc.TypePrn, ELT(prc.TypePrn + 1, "T'#233'rmica", '
      '"Matricial") NOMETYPEPRN, prc.PortaImp, ELT(prc.PortaImp+1, '
      '"COM", "LPT") NOMEPORTAIMP, prc.PortaIdx, prc.Modelo, '
      'prc.Colunas'
      'FROM princad prc'
      '')
    Left = 12
    Top = 12
    object QrPrinCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPrinCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrPrinCadNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
    object QrPrinCadTypeUso: TSmallintField
      FieldName = 'TypeUso'
      Required = True
    end
    object QrPrinCadNOMETYPEUSO: TWideStringField
      FieldName = 'NOMETYPEUSO'
      Size = 9
    end
    object QrPrinCadTypePrn: TSmallintField
      FieldName = 'TypePrn'
      Required = True
    end
    object QrPrinCadNOMETYPEPRN: TWideStringField
      FieldName = 'NOMETYPEPRN'
      Size = 9
    end
    object QrPrinCadPortaImp: TSmallintField
      FieldName = 'PortaImp'
      Required = True
    end
    object QrPrinCadNOMEPORTAIMP: TWideStringField
      FieldName = 'NOMEPORTAIMP'
      Size = 3
    end
    object QrPrinCadPortaIdx: TSmallintField
      FieldName = 'PortaIdx'
      Required = True
    end
    object QrPrinCadModelo: TWideStringField
      FieldName = 'Modelo'
      Size = 30
    end
    object QrPrinCadColunas: TSmallintField
      FieldName = 'Colunas'
      Required = True
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtConfigura
    CanUpd01 = BtLinha
    Left = 68
    Top = 12
  end
  object PMConfigura: TPopupMenu
    OnPopup = PMConfiguraPopup
    Left = 472
    Top = 52
    object Incluinovaconfigurao1: TMenuItem
      Caption = 'Inclui nova configura'#231#227'o'
      OnClick = Incluinovaconfigurao1Click
    end
    object Alteraconfiguraoatual1: TMenuItem
      Caption = '&Altera configura'#231#227'o atual'
      OnClick = Alteraconfiguraoatual1Click
    end
    object Excluiconfiguraoatual1: TMenuItem
      Caption = '&Exclui configura'#231#227'o atual'
      Enabled = False
    end
  end
  object QrPrinCmd_0: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPrinCmd_0AfterOpen
    BeforeClose = QrPrinCmd_0BeforeClose
    AfterScroll = QrPrinCmd_0AfterScroll
    SQL.Strings = (
      'SELECT * '
      'FROM princmd'
      'WHERE Tipo=0'
      'AND Codigo=:P0'
      'ORDER BY Ordem')
    Left = 292
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPrinCmd_0Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrinCmd_0Tipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrPrinCmd_0Ordem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrPrinCmd_0Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPrinCmd_0Config: TWideStringField
      FieldName = 'Config'
      Size = 255
    end
  end
  object DsPrinCmd_0: TDataSource
    DataSet = QrPrinCmd_0
    Left = 320
    Top = 52
  end
  object QrPrinCmd_1: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrPrinCmd_1BeforeClose
    AfterScroll = QrPrinCmd_1AfterScroll
    SQL.Strings = (
      'SELECT * '
      'FROM princmd'
      'WHERE Tipo=1'
      'AND Codigo=:P0'
      'ORDER BY Ordem')
    Left = 352
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPrinCmd_1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrinCmd_1Tipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrPrinCmd_1Ordem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrPrinCmd_1Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPrinCmd_1Config: TWideStringField
      FieldName = 'Config'
      Size = 255
    end
  end
  object DsPrinCmd_1: TDataSource
    DataSet = QrPrinCmd_1
    Left = 380
    Top = 52
  end
  object QrPrinCmd_2: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPrinCmd_2AfterOpen
    BeforeClose = QrPrinCmd_2BeforeClose
    AfterScroll = QrPrinCmd_2AfterScroll
    SQL.Strings = (
      'SELECT * '
      'FROM princmd'
      'WHERE Tipo=2'
      'AND Codigo=:P0'
      'ORDER BY Ordem')
    Left = 412
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPrinCmd_2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrinCmd_2Tipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrPrinCmd_2Ordem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrPrinCmd_2Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPrinCmd_2Config: TWideStringField
      FieldName = 'Config'
      Size = 255
    end
  end
  object DsPrinCmd_2: TDataSource
    DataSet = QrPrinCmd_2
    Left = 440
    Top = 52
  end
  object PMLinha: TPopupMenu
    Left = 500
    Top = 52
    object Incluinovalinhadecomando1: TMenuItem
      Caption = '&Inclui nova linha de comando'
      OnClick = Incluinovalinhadecomando1Click
    end
    object Alteralinhadecomandoatual1: TMenuItem
      Caption = '&Altera linha de comando atual'
      OnClick = Alteralinhadecomandoatual1Click
      object Incio1: TMenuItem
        Caption = '&In'#237'cio'
        OnClick = Incio1Click
      end
      object Dados1: TMenuItem
        Caption = '&Dados'
        OnClick = Dados1Click
      end
      object Final1: TMenuItem
        Caption = '&Final'
        OnClick = Final1Click
      end
    end
    object Excluilinhadecomandoatual1: TMenuItem
      Caption = '&Exclui linha de comando atual'
      OnClick = Excluilinhadecomandoatual1Click
    end
  end
  object QrOrd: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM princmd'
      'WHERE Tipo=:P0'
      'AND Codigo=:P1'
      'ORDER BY Ordem')
    Left = 532
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrOrdOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrOrdControle: TIntegerField
      FieldName = 'Controle'
    end
  end
end
