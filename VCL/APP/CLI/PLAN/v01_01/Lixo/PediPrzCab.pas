unit PediPrzCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, Grids, DBGrids, dmkDBGrid,
  Menus, dmkCheckGroup, UnDmkProcFunc;

type
  TFmPediPrzCab = class(TForm)
    PainelDados: TPanel;
    DsPediPrzCab: TDataSource;
    QrPediPrzCab: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TdmkLabel;
    Image1: TImage;
    PanelFill2: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TdmkLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtItens: TBitBtn;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    PnCabeca: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdCodUsu: TdmkEdit;
    PMCondicoes: TPopupMenu;
    QrPediPrzIts: TmySQLQuery;
    DsPediPrzIts: TDataSource;
    Incluinovacondio1: TMenuItem;
    Alteracondioatual1: TMenuItem;
    Excluicondioatual1: TMenuItem;
    BtCondicoes: TBitBtn;
    QrPediPrzItsControle: TIntegerField;
    QrPediPrzItsDias: TIntegerField;
    QrPediPrzItsPercent1: TFloatField;
    QrPediPrzItsPercent2: TFloatField;
    Panel4: TPanel;
    DBGrid1: TDBGrid;
    DBGrid2: TDBGrid;
    QrPediPrzItsPERCENTT: TFloatField;
    QrPediPrzCabCodigo: TIntegerField;
    QrPediPrzCabCodUsu: TIntegerField;
    QrPediPrzCabNome: TWideStringField;
    QrPediPrzCabMaxDesco: TFloatField;
    QrPediPrzCabJurosMes: TFloatField;
    QrPediPrzCabParcelas: TIntegerField;
    QrPediPrzCabMedDDSimpl: TFloatField;
    QrPediPrzCabMedDDReal: TFloatField;
    QrPediPrzCabMedDDPerc1: TFloatField;
    QrPediPrzCabMedDDPerc2: TFloatField;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    DBEdit3: TDBEdit;
    Label6: TLabel;
    DBEdit4: TDBEdit;
    Label10: TLabel;
    DBEdit5: TDBEdit;
    Label11: TLabel;
    DBEdit6: TDBEdit;
    Label4: TLabel;
    DBEdit2: TDBEdit;
    Label12: TLabel;
    DBEdit7: TDBEdit;
    Label13: TLabel;
    DBEdit8: TDBEdit;
    Label14: TLabel;
    DBEdit9: TDBEdit;
    EdJurosMes: TdmkEdit;
    EdMaxDesco: TdmkEdit;
    Label15: TLabel;
    Label16: TLabel;
    QrPediPrzCabPercentT: TFloatField;
    QrPediPrzCabPercent1: TFloatField;
    QrPediPrzCabPercent2: TFloatField;
    DBEdit10: TDBEdit;
    Label17: TLabel;
    DBEdit11: TDBEdit;
    Label18: TLabel;
    BtFiliais: TBitBtn;
    QrPediPrzEmp: TmySQLQuery;
    DsPediPrzEmp: TDataSource;
    QrPediPrzEmpEmpresa: TIntegerField;
    QrPediPrzEmpFilial: TIntegerField;
    QrPediPrzEmpNOMEFILIAL: TWideStringField;
    QrPediPrzEmpControle: TIntegerField;
    QrPediPrzCabAplicacao: TSmallintField;
    dmkDBCheckGroup1: TdmkDBCheckGroup;
    CGAplicacao: TdmkCheckGroup;
    PMFiliais: TPopupMenu;
    AdicionaFilial1: TMenuItem;
    RetiraFilial1: TMenuItem;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrPediPrzCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrPediPrzCabBeforeOpen(DataSet: TDataSet);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure BtCondicoesClick(Sender: TObject);
    procedure PMCondicoesPopup(Sender: TObject);
    procedure QrPediPrzCabBeforeClose(DataSet: TDataSet);
    procedure QrPediPrzCabAfterScroll(DataSet: TDataSet);
    procedure BtItensClick(Sender: TObject);
    procedure Incluinovacondio1Click(Sender: TObject);
    procedure Alteracondioatual1Click(Sender: TObject);
    procedure BtFiliaisClick(Sender: TObject);
    procedure AdicionaFilial1Click(Sender: TObject);
    procedure RetiraFilial1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; Status: String; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenPediPrzIts(Controle: Integer);
    procedure ReopenPediPrzEmp(Controle: Integer);
  end;

var
  FmPediPrzCab: TFmPediPrzCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, PediPrzIts, MyDBCheck, MasterSelFilial;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmPediPrzCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmPediPrzCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrPediPrzCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmPediPrzCab.DefParams;
begin
  VAR_GOTOTABELA := 'PediPrzCab';
  VAR_GOTOMYSQLTABLE := QrPediPrzCab;
  VAR_GOTONEG := True;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT ppc.Codigo, ppc.CodUsu, ppc.Nome,');
  VAR_SQLx.Add('ppc.MaxDesco, ppc.JurosMes, ppc.Parcelas,');
  VAR_SQLx.Add('ppc.MedDDSimpl, ppc.MedDDReal, ppc.MedDDPerc1,');
  VAR_SQLx.Add('ppc.MedDDPerc2, ppc.PercentT, ppc.Percent1,');
  VAR_SQLx.Add('ppc.Percent2, ppc.Aplicacao');
  VAR_SQLx.Add('FROM pediprzcab ppc');
  VAR_SQLx.Add('WHERE ppc.Codigo > -1000');
  //
  VAR_SQL1.Add('AND ppc.Codigo=:P0');
  //
  VAR_SQL2.Add('AND ppc.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND ppc.Nome Like :P0');
  //
end;

procedure TFmPediPrzCab.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (LaTipo.SQLType = stIns) then
    EdCodUsu.ValueVariant :=
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'PediPrzCab', 'CodUsu', [], []);
end;

procedure TFmPediPrzCab.MostraEdicao(Mostra: Integer; Status: String; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelControle.Visible:=True;
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    else Geral.MensagemBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  LaTipo.Caption := Status;
  GOTOy.BotoesSb(LaTipo.Caption);
end;

procedure TFmPediPrzCab.PMCondicoesPopup(Sender: TObject);
begin
  Alteracondioatual1.Enabled :=
    (QrPediPrzCab.State <> dsInactive) and (QrPediPrzCab.RecordCount > 0);
end;

procedure TFmPediPrzCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmPediPrzCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmPediPrzCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmPediPrzCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmPediPrzCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmPediPrzCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmPediPrzCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmPediPrzCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrPediPrzCabCodigo.Value;
  Close;
end;

procedure TFmPediPrzCab.AdicionaFilial1Click(Sender: TObject);
var
  Confirmou: Boolean;
  Empresa, Controle, Codigo: Integer;
begin
  Confirmou := False;
  Empresa   := 0;
  if DBCheck.CriaFm(TFmMasterSelFilial, FmMasterSelFilial, afmoNegarComAviso) then
  begin
    with FmMasterSelFilial do
    begin
      QrFiliais.Close;
      QrFiliais.SQL.Clear;
      QrFiliais.SQL.Add('SELECT Filial, Codigo,');
      QrFiliais.SQL.Add('IF(Tipo=0, RazaoSocial, Nome) NOMEFILIAL');
      QrFiliais.SQL.Add('FROM entidades');
      QrFiliais.SQL.Add('WHERE Codigo<-10');
      QrFiliais.SQL.Add('AND NOT (Codigo IN (');
      QrFiliais.SQL.Add('  SELECT Empresa');
      QrFiliais.SQL.Add('  FROM pediprzemp');
      QrFiliais.SQL.Add('  WHERE Codigo=' + FormatFloat('0', QrPediPrzCabCodigo.Value));
      QrFiliais.SQL.Add('))');
      QrFiliais.SQL.Add('ORDER BY NOMEFILIAL');
      QrFiliais.Open;
      //
      ShowModal;
      Confirmou := FSelecionou;
      Empresa   := QrFiliaisCodigo.Value;//Geral.IMV(EdFilial.Text);
      Destroy;
      //
    end;
  end;
  if Confirmou then
  begin
    Codigo   := QrPediPrzCabCodigo.Value;
    Controle := UMyMod.BuscaEmLivreY_Def('pediprzemp', 'Controle', stIns, 0);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'pediprzemp', False, [
    'Codigo', 'Empresa'], ['Controle'], [Codigo, Empresa], [Controle], True) then
      ReopenPediPrzEmp(Controle);
  end;
end;

procedure TFmPediPrzCab.Alteracondioatual1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrPediPrzCab, [PainelDados],
  [PainelEdita], EdCodUsu, LaTipo, 'pediprzcab');
end;

procedure TFmPediPrzCab.BtCondicoesClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCondicoes, BtCondicoes);
end;

procedure TFmPediPrzCab.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Geral.MensagemBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Codigo := UMyMod.BuscaEmLivreY_Def('PediPrzCab', 'Codigo', LaTipo.SQLType,
    QrPediPrzCabCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(LaTipo.SQLType, FmPediPrzCab, PainelEdit,
    'PediPrzCab', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], LaTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmPediPrzCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if LaTipo.Caption = CO_INCLUSAO then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'PediPrzCab', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'PediPrzCab', 'Codigo');
  MostraEdicao(0, CO_TRAVADO, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'PediPrzCab', 'Codigo');
end;

procedure TFmPediPrzCab.BtFiliaisClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMFiliais, BtFiliais);
end;

procedure TFmPediPrzCab.BtItensClick(Sender: TObject);
begin
  UmyMod.FormInsUpd_Show(TFmPediPrzIts, FmPediPrzIts, afmoNegarComAviso,
    QrPediPrzIts, stIns);
end;

procedure TFmPediPrzCab.FormCreate(Sender: TObject);
begin
  PainelEdit.Align  := alClient;
  Panel4.Align      := alClient;
  CriaOForm;
end;

procedure TFmPediPrzCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrPediPrzCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmPediPrzCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmPediPrzCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrPediPrzCabCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmPediPrzCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmPediPrzCab.QrPediPrzCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmPediPrzCab.QrPediPrzCabAfterScroll(DataSet: TDataSet);
begin
  ReopenPediPrzIts(0);
  ReopenPediPrzEmp(0);
end;

procedure TFmPediPrzCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPediPrzCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrPediPrzCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'PediPrzCab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmPediPrzCab.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmPediPrzCab.Incluinovacondio1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrPediPrzCab, [PainelDados],
  [PainelEdita], EdCodUsu, LaTipo, 'pediprzcab');
end;

procedure TFmPediPrzCab.QrPediPrzCabBeforeClose(DataSet: TDataSet);
begin
  QrPediPrzIts.Close;
  QrPediPrzEmp.Close;
end;

procedure TFmPediPrzCab.QrPediPrzCabBeforeOpen(DataSet: TDataSet);
begin
  QrPediPrzCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmPediPrzCab.ReopenPediPrzIts(Controle: Integer);
begin
  QrPediPrzIts.Close;
  QrPediPrzIts.Params[0].AsInteger :=   QrPediPrzCabCodigo.Value;
  QrPediPrzIts.Open;
  //
  if Controle <> 0 then
    QrPediPrzIts.Locate('Controle', Controle, []);
end;

procedure TFmPediPrzCab.RetiraFilial1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrPediPrzEmp, DBGrid2,
  'pediprzemp',  ['Controle'], ['Controle'], istPergunta, '');
  ReopenPediPrzEmp(0);
end;

procedure TFmPediPrzCab.ReopenPediPrzEmp(Controle: Integer);
begin
  QrPediPrzEmp.Close;
  QrPediPrzEmp.Params[0].AsInteger :=   QrPediPrzCabCodigo.Value;
  QrPediPrzEmp.Open;
  //
  if Controle <> 0 then
    QrPediPrzEmp.Locate('Controle', Controle, []);
end;

end.

