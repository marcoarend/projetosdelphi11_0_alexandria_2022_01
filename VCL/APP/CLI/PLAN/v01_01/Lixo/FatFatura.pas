unit FatFatura;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, ComCtrls, DB, mySQLDbTables, dmkEdit,
  Mask, DBCtrls;

type
  TFmFatFatura = class(TForm)
    PainelTitulo: TPanel;
    Image1: TImage;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    PnPed: TPanel;
    PnCfe: TPanel;
    PnEmi: TPanel;
    Panel1: TPanel;
    Panel3: TPanel;
    BitBtn2: TBitBtn;
    QrPediVda: TmySQLQuery;
    QrPediVdaNOMEFRETEPOR: TWideStringField;
    DsPediVda: TDataSource;
    GroupBox9: TGroupBox;
    Label56: TLabel;
    DBEdit3: TDBEdit;
    DBEdit2: TDBEdit;
    EdPedido: TdmkEdit;
    Label1: TLabel;
    Label64: TLabel;
    DBEdit12: TDBEdit;
    Label65: TLabel;
    DBEdit11: TDBEdit;
    GroupBox1: TGroupBox;
    Label8: TLabel;
    DBEdit1: TDBEdit;
    DBEdit6: TDBEdit;
    QrPediVdaCodigo: TIntegerField;
    QrPediVdaCodUsu: TIntegerField;
    QrPediVdaEmpresa: TIntegerField;
    QrPediVdaCliente: TIntegerField;
    QrPediVdaDtaEmiss: TDateField;
    QrPediVdaDtaEntra: TDateField;
    QrPediVdaDtaInclu: TDateField;
    QrPediVdaDtaPrevi: TDateField;
    QrPediVdaPrioridade: TSmallintField;
    QrPediVdaCondicaoPG: TIntegerField;
    QrPediVdaMoeda: TIntegerField;
    QrPediVdaSituacao: TIntegerField;
    QrPediVdaTabelaPrc: TIntegerField;
    QrPediVdaMotivoSit: TIntegerField;
    QrPediVdaLoteProd: TIntegerField;
    QrPediVdaPedidoCli: TWideStringField;
    QrPediVdaFretePor: TSmallintField;
    QrPediVdaTransporta: TIntegerField;
    QrPediVdaRedespacho: TIntegerField;
    QrPediVdaRegrFiscal: TIntegerField;
    QrPediVdaDesoAces_V: TFloatField;
    QrPediVdaDesoAces_P: TFloatField;
    QrPediVdaFrete_V: TFloatField;
    QrPediVdaFrete_P: TFloatField;
    QrPediVdaSeguro_V: TFloatField;
    QrPediVdaSeguro_P: TFloatField;
    QrPediVdaTotalQtd: TFloatField;
    QrPediVdaTotal_Vlr: TFloatField;
    QrPediVdaTotal_Des: TFloatField;
    QrPediVdaTotal_Tot: TFloatField;
    QrPediVdaObserva: TWideMemoField;
    QrPediVdaNOMETABEPRCCAD: TWideStringField;
    QrPediVdaNOMEMOEDA: TWideStringField;
    QrPediVdaRepresen: TIntegerField;
    QrPediVdaComisFat: TFloatField;
    QrPediVdaComisRec: TFloatField;
    QrPediVdaCartEmis: TIntegerField;
    QrPediVdaAFP_Sit: TSmallintField;
    QrPediVdaAFP_Per: TFloatField;
    QrPediVdaMedDDSimpl: TFloatField;
    QrPediVdaMedDDReal: TFloatField;
    QrPediVdaValLiq: TFloatField;
    QrPediVdaQuantP: TFloatField;
    QrPediVdaNOMEEMP: TWideStringField;
    QrPediVdaNOMEACC: TWideStringField;
    QrPediVdaNOMETRANSP: TWideStringField;
    QrPediVdaNOMEREDESP: TWideStringField;
    QrPediVdaFilial: TIntegerField;
    QrPediVdaNOMECARTEMIS: TWideStringField;
    QrPediVdaNOMECONDICAOPG: TWideStringField;
    QrPediVdaNOMEMOTIVO: TWideStringField;
    QrPediVdaCODUSU_ACC: TIntegerField;
    QrPediVdaCODUSU_TRA: TIntegerField;
    QrPediVdaCODUSU_RED: TIntegerField;
    QrPediVdaCODUSU_MOT: TIntegerField;
    QrPediVdaCODUSU_TPC: TIntegerField;
    QrPediVdaCODUSU_MDA: TIntegerField;
    QrPediVdaCODUSU_PPC: TIntegerField;
    QrCli: TmySQLQuery;
    DsCli: TDataSource;
    QrCliCodigo: TIntegerField;
    QrCliTipo: TSmallintField;
    QrCliCodUsu: TIntegerField;
    QrCliNOME_ENT: TWideStringField;
    QrCliCNPJ_CPF: TWideStringField;
    QrCliIE_RG: TWideStringField;
    QrCliRUA: TWideStringField;
    QrCliNUMERO: TLargeintField;
    QrCliCOMPL: TWideStringField;
    QrCliBAIRRO: TWideStringField;
    QrCliCIDADE: TWideStringField;
    QrCliNOMELOGRAD: TWideStringField;
    QrCliNOMEUF: TWideStringField;
    QrCliPais: TWideStringField;
    QrCliLograd: TLargeintField;
    QrCliCEP: TLargeintField;
    QrCliENDEREF: TWideStringField;
    QrCliTE1: TWideStringField;
    QrCliFAX: TWideStringField;
    QrCliCNPJ_TXT: TWideStringField;
    DBMemo1: TDBMemo;
    DBEdit4: TDBEdit;
    DBText1: TDBText;
    QrCliNOME_TIPO_DOC: TWideStringField;
    QrCliTE1_TXT: TWideStringField;
    QrCliFAX_TXT: TWideStringField;
    QrCliNUMERO_TXT: TWideStringField;
    QrCliCEP_TXT: TWideStringField;
    QrCliE_ALL: TWideStringField;
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    Panel2: TPanel;
    BitBtn1: TBitBtn;
    Panel4: TPanel;
    Panel5: TPanel;
    BitBtn3: TBitBtn;
    DBMemo2: TDBMemo;
    Label2: TLabel;
    QrEntrega: TmySQLQuery;
    DsEntrega: TDataSource;
    QrEntregaE_ALL: TWideStringField;
    QrEntregaCNPJ_TXT: TWideStringField;
    QrEntregaNOME_TIPO_DOC: TWideStringField;
    QrEntregaTE1_TXT: TWideStringField;
    QrEntregaFAX_TXT: TWideStringField;
    QrEntregaNUMERO_TXT: TWideStringField;
    QrEntregaCEP_TXT: TWideStringField;
    QrEntregaRUA: TWideStringField;
    QrEntregaNUMERO: TIntegerField;
    QrEntregaCOMPL: TWideStringField;
    QrEntregaBAIRRO: TWideStringField;
    QrEntregaCIDADE: TWideStringField;
    QrEntregaNOMELOGRAD: TWideStringField;
    QrEntregaNOMEUF: TWideStringField;
    QrEntregaPais: TWideStringField;
    QrEntregaLograd: TSmallintField;
    QrEntregaCEP: TIntegerField;
    QrEntregaENDEREF: TWideStringField;
    QrEntregaTE1: TWideStringField;
    QrEntregaFAX: TWideStringField;
    GroupBox12: TGroupBox;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    DBEdit44: TDBEdit;
    DBEdit45: TDBEdit;
    DBEdit46: TDBEdit;
    DBEdit47: TDBEdit;
    GroupBox15: TGroupBox;
    Label34: TLabel;
    Label35: TLabel;
    DBEdit48: TDBEdit;
    DBEdit49: TDBEdit;
    DBEdit50: TDBEdit;
    QrPediVdaNOMEFISREGCAD: TWideStringField;
    QrPediVdaNOMEMODELONF: TWideStringField;
    QrPediVdaCODUSU_FRC: TIntegerField;
    QrPediVdaMODELO_NF: TIntegerField;
    GroupBox14: TGroupBox;
    Label51: TLabel;
    Label52: TLabel;
    Label53: TLabel;
    Label66: TLabel;
    Label67: TLabel;
    Label68: TLabel;
    Label70: TLabel;
    Label71: TLabel;
    Label72: TLabel;
    DBEdit16: TDBEdit;
    DBEdit23: TDBEdit;
    DBEdit34: TDBEdit;
    DBEdit35: TDBEdit;
    DBEdit36: TDBEdit;
    DBEdit37: TDBEdit;
    DBEdit38: TDBEdit;
    DBEdit39: TDBEdit;
    DBEdit40: TDBEdit;
    DBEdit41: TDBEdit;
    DBEdit42: TDBEdit;
    DBEdit43: TDBEdit;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label4: TLabel;
    EdCodUsu: TdmkEdit;
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure QrPediVdaCalcFields(DataSet: TDataSet);
    procedure QrPediVdaAfterOpen(DataSet: TDataSet);
    procedure QrPediVdaBeforeClose(DataSet: TDataSet);
    procedure QrCliCalcFields(DataSet: TDataSet);
    procedure EdPedidoExit(Sender: TObject);
    procedure QrEntregaCalcFields(DataSet: TDataSet);
    procedure QrCliBeforeClose(DataSet: TDataSet);
    procedure QrCliAfterOpen(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmFatFatura: TFmFatFatura;

implementation

uses UnMyObjects, Module;

{$R *.DFM}

procedure TFmFatFatura.BitBtn2Click(Sender: TObject);
begin
  Close;
end;

procedure TFmFatFatura.EdPedidoExit(Sender: TObject);
begin
  QrPediVda.Close;
  QrPediVda.Params[0].AsInteger := EdPedido.ValueVariant;
  QrPediVda.Open;
  if EdPedido.ValueVariant <> 0 then
  begin
    if QrPediVda.RecordCount = 0 then
    begin
      Geral.MensagemBox('Pedido n�o localizado!',
      'Aviso', MB_OK+MB_ICONWARNING);
      EdPedido.ValueVariant := 0;
    end;
  end;
end;

procedure TFmFatFatura.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  if PageControl1.ActivePageIndex = 0 then
    EdPedido.SetFocus;
end;

procedure TFmFatFatura.FormCreate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmFatFatura.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmFatFatura.QrCliAfterOpen(DataSet: TDataSet);
begin
  QrEntrega.Close;
  QrEntrega.Params[00].AsInteger := QrPediVdaCliente.Value;
  QrEntrega.Open;
end;

procedure TFmFatFatura.QrCliBeforeClose(DataSet: TDataSet);
begin
  QrEntrega.Close;
end;

procedure TFmFatFatura.QrCliCalcFields(DataSet: TDataSet);
begin
  QrCliTE1_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrCliTe1.Value);
  QrCliFAX_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrCliFax.Value);
  QrCliCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrCliCNPJ_CPF.Value);
  QrCliNOME_TIPO_DOC.Value :=
    MLAGeral.ObtemTipoDocTxtDeCPFCNPJ(QrCliCNPJ_CPF.Value) + ':';
  QrCliNUMERO_TXT.Value :=
    MLAGeral.FormataNumeroDeRua(QrCliNumero.Value, False);
  //
  QrCliE_ALL.Value := Uppercase(QrCliNOMELOGRAD.Value);
  if Trim(QrCliE_ALL.Value) <> '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' ';
  QrCliE_ALL.Value := QrCliE_ALL.Value + Uppercase(QrCliRua.Value);
  if Trim(QrCliRua.Value) <> '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ', ' + QrCliNUMERO_TXT.Value;
  if Trim(QrCliCompl.Value) <>  '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' ' + Uppercase(QrCliCompl.Value);
  if Trim(QrCliBairro.Value) <>  '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' - ' + Uppercase(QrCliBairro.Value);
  if QrCliCEP.Value > 0 then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' CEP ' +Geral.FormataCEP_NT(QrCliCEP.Value);
  if Trim(QrCliCidade.Value) <>  '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' - ' + Uppercase(QrCliCidade.Value);
  if Trim(QrCliNOMEUF.Value) <>  '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ', ' + QrCliNOMEUF.Value;
  if Trim(QrCliPais.Value) <>  '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' - ' + QrCliPais.Value;
  //
  //QrCliCEP_TXT.Value :=Geral.FormataCEP_NT(QrCliCEP.Value);
  //
end;

procedure TFmFatFatura.QrEntregaCalcFields(DataSet: TDataSet);
begin
  QrEntregaTE1_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrEntregaTe1.Value);
  QrEntregaFAX_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrEntregaFax.Value);
  {
  QrEntregaCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrEntregaCNPJ_CPF.Value);
  QrEntregaNOME_TIPO_DOC.Value :=
    MLAGeral.ObtemTipoDocTxtDeCPFCNPJ(QrEntregaCNPJ_CPF.Value) + ':';
  }
  QrEntregaNUMERO_TXT.Value :=
    MLAGeral.FormataNumeroDeRua(QrEntregaNumero.Value, False);
  //
  QrEntregaE_ALL.Value := Uppercase(QrEntregaNOMELOGRAD.Value);
  if Trim(QrEntregaE_ALL.Value) <> '' then QrEntregaE_ALL.Value :=
    QrEntregaE_ALL.Value + ' ';
  QrEntregaE_ALL.Value := QrEntregaE_ALL.Value + Uppercase(QrEntregaRua.Value);
  if Trim(QrEntregaRua.Value) <> '' then QrEntregaE_ALL.Value :=
    QrEntregaE_ALL.Value + ', ' + QrEntregaNUMERO_TXT.Value;
  if Trim(QrEntregaCompl.Value) <>  '' then QrEntregaE_ALL.Value :=
    QrEntregaE_ALL.Value + ' ' + Uppercase(QrEntregaCompl.Value);
  if Trim(QrEntregaBairro.Value) <>  '' then QrEntregaE_ALL.Value :=
    QrEntregaE_ALL.Value + ' - ' + Uppercase(QrEntregaBairro.Value);
  if QrEntregaCEP.Value > 0 then QrEntregaE_ALL.Value :=
    QrEntregaE_ALL.Value + ' CEP ' +Geral.FormataCEP_NT(QrEntregaCEP.Value);
  if Trim(QrEntregaCidade.Value) <>  '' then QrEntregaE_ALL.Value :=
    QrEntregaE_ALL.Value + ' - ' + Uppercase(QrEntregaCidade.Value);
  if Trim(QrEntregaNOMEUF.Value) <>  '' then QrEntregaE_ALL.Value :=
    QrEntregaE_ALL.Value + ', ' + QrEntregaNOMEUF.Value;
  if Trim(QrEntregaPais.Value) <>  '' then QrEntregaE_ALL.Value :=
    QrEntregaE_ALL.Value + ' - ' + QrEntregaPais.Value;
  //
  //QrEntregaCEP_TXT.Value :=Geral.FormataCEP_NT(QrEntregaCEP.Value);
  //
  if Trim(QrEntregaE_ALL.Value) = '' then
    QrEntregaE_ALL.Value := QrCliE_ALL.Value;
end;

procedure TFmFatFatura.QrPediVdaAfterOpen(DataSet: TDataSet);
begin
  QrCli.Close;
  QrCli.Params[00].AsInteger := QrPediVdaCliente.Value;
  QrCli.Open;
end;

procedure TFmFatFatura.QrPediVdaBeforeClose(DataSet: TDataSet);
begin
  QrCli.Close;
end;

procedure TFmFatFatura.QrPediVdaCalcFields(DataSet: TDataSet);
begin
  QrPediVdaNOMEFRETEPOR.Value := MLAGeral.FretePor_Txt(QrPediVdaFretePor.Value);
end;

end.
