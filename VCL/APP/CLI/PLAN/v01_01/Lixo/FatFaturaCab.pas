unit FatFaturaCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, Grids, DBGrids, dmkDBGrid,
  Menus, dmkCheckGroup, UnDmkProcFunc;

type
  TFmFatPedCab = class(TForm)
    PainelDados: TPanel;
    DsPediPrzCab: TDataSource;
    QrPediPrzCab: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TdmkLabel;
    Image1: TImage;
    PanelFill2: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TdmkLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtItens: TBitBtn;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    PnCabeca: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdCodUsu: TdmkEdit;
    PMFatura: TPopupMenu;
    QrPediPrzIts: TmySQLQuery;
    DsPediPrzIts: TDataSource;
    BtFatura: TBitBtn;
    QrPediPrzItsControle: TIntegerField;
    QrPediPrzItsDias: TIntegerField;
    QrPediPrzItsPercent1: TFloatField;
    QrPediPrzItsPercent2: TFloatField;
    Panel4: TPanel;
    QrPediPrzItsPERCENTT: TFloatField;
    QrPediPrzCabCodigo: TIntegerField;
    QrPediPrzCabCodUsu: TIntegerField;
    QrPediPrzCabNome: TWideStringField;
    QrPediPrzCabMaxDesco: TFloatField;
    QrPediPrzCabJurosMes: TFloatField;
    QrPediPrzCabParcelas: TIntegerField;
    QrPediPrzCabMedDDSimpl: TFloatField;
    QrPediPrzCabMedDDReal: TFloatField;
    QrPediPrzCabMedDDPerc1: TFloatField;
    QrPediPrzCabMedDDPerc2: TFloatField;
    EdJurosMes: TdmkEdit;
    EdMaxDesco: TdmkEdit;
    Label15: TLabel;
    Label16: TLabel;
    QrPediPrzCabPercentT: TFloatField;
    QrPediPrzCabPercent1: TFloatField;
    QrPediPrzCabPercent2: TFloatField;
    BtFiliais: TBitBtn;
    QrPediPrzEmp: TmySQLQuery;
    DsPediPrzEmp: TDataSource;
    QrPediPrzEmpEmpresa: TIntegerField;
    QrPediPrzEmpFilial: TIntegerField;
    QrPediPrzEmpNOMEFILIAL: TWideStringField;
    QrPediPrzEmpControle: TIntegerField;
    QrPediPrzCabAplicacao: TSmallintField;
    CGAplicacao: TdmkCheckGroup;
    Incluinovofaturamento1: TMenuItem;
    Alterafaturamentoatual1: TMenuItem;
    Excluifaturamentoatual1: TMenuItem;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrPediPrzCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrPediPrzCabBeforeOpen(DataSet: TDataSet);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure BtFaturaClick(Sender: TObject);
    procedure PMFaturaPopup(Sender: TObject);
    procedure QrPediPrzCabBeforeClose(DataSet: TDataSet);
    procedure QrPediPrzCabAfterScroll(DataSet: TDataSet);
    procedure BtItensClick(Sender: TObject);
    procedure BtFiliaisClick(Sender: TObject);
    procedure Incluinovofaturamento1Click(Sender: TObject);
    procedure Alterafaturamentoatual1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; Status: String; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenPediPrzIts(Controle: Integer);
    procedure ReopenPediPrzEmp(Controle: Integer);
  end;

var
  FmFatPedCab: TFmFatPedCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, PediPrzIts, MyDBCheck, MasterSelFilial;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmFatPedCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmFatPedCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrPediPrzCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmFatPedCab.DefParams;
begin
  VAR_GOTOTABELA := 'PediPrzCab';
  VAR_GOTOMYSQLTABLE := QrPediPrzCab;
  VAR_GOTONEG := True;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT ppc.Codigo, ppc.CodUsu, ppc.Nome,');
  VAR_SQLx.Add('ppc.MaxDesco, ppc.JurosMes, ppc.Parcelas,');
  VAR_SQLx.Add('ppc.MedDDSimpl, ppc.MedDDReal, ppc.MedDDPerc1,');
  VAR_SQLx.Add('ppc.MedDDPerc2, ppc.PercentT, ppc.Percent1,');
  VAR_SQLx.Add('ppc.Percent2, ppc.Aplicacao');
  VAR_SQLx.Add('FROM pediprzcab ppc');
  VAR_SQLx.Add('WHERE ppc.Codigo > -1000');
  //
  VAR_SQL1.Add('AND ppc.Codigo=:P0');
  //
  VAR_SQL2.Add('AND ppc.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND ppc.Nome Like :P0');
  //
end;

procedure TFmFatPedCab.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (LaTipo.SQLType = stIns) then
    EdCodUsu.ValueVariant :=
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'PediPrzCab', 'CodUsu', [], [], stIns, 0, True);
end;

procedure TFmFatPedCab.MostraEdicao(Mostra: Integer; Status: String; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelControle.Visible:=True;
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    else Geral.MensagemBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  LaTipo.Caption := Status;
  GOTOy.BotoesSb(LaTipo.Caption);
end;

procedure TFmFatPedCab.PMFaturaPopup(Sender: TObject);
begin
  Alteracondioatual1.Enabled :=
    (QrPediPrzCab.State <> dsInactive) and (QrPediPrzCab.RecordCount > 0);
end;

procedure TFmFatPedCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmFatPedCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmFatPedCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmFatPedCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmFatPedCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmFatPedCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmFatPedCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmFatPedCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrPediPrzCabCodigo.Value;
  Close;
end;

procedure TFmFatPedCab.BtFaturaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCondicoes, BtCondicoes);
end;

procedure TFmFatPedCab.Alterafaturamentoatual1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrPediPrzCab, [PainelDados],
  [PainelEdita], EdCodUsu, LaTipo, 'pediprzcab');
end;

procedure TFmFatPedCab.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Geral.MensagemBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Codigo := UMyMod.BuscaEmLivreY_Def('PediPrzCab', 'Codigo', LaTipo.SQLType,
    QrPediPrzCabCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(LaTipo.SQLType, FmPediPrzCab, PainelEdit,
    'PediPrzCab', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], LaTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmFatPedCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if LaTipo.Caption = CO_INCLUSAO then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'PediPrzCab', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'PediPrzCab', 'Codigo');
  MostraEdicao(0, CO_TRAVADO, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'PediPrzCab', 'Codigo');
end;

procedure TFmFatPedCab.BtFiliaisClick(Sender: TObject);
var
  Confirmou: Boolean;
  Empresa, Controle, Codigo: Integer;
begin
  Confirmou := False;
  Empresa   := 0;
  if DBCheck.CriaFm(TFmMasterSelFilial, FmMasterSelFilial, afmoNegarComAviso) then
  begin
    with FmMasterSelFilial do
    begin
      QrFiliais.Close;
      QrFiliais.SQL.Clear;
      QrFiliais.SQL.Add('SELECT Filial, Codigo,');
      QrFiliais.SQL.Add('IF(Tipo=0, RazaoSocial, Nome) NOMEFILIAL');
      QrFiliais.SQL.Add('FROM entidades');
      QrFiliais.SQL.Add('WHERE Codigo<-10');
      QrFiliais.SQL.Add('AND NOT (Codigo IN (');
      QrFiliais.SQL.Add('  SELECT Empresa');
      QrFiliais.SQL.Add('  FROM pediprzemp');
      QrFiliais.SQL.Add('  WHERE Codigo=1');
      QrFiliais.SQL.Add('))');
      QrFiliais.SQL.Add('ORDER BY NOMEFILIAL');
      QrFiliais.Open;
      //
      ShowModal;
      Confirmou := FSelecionou;
      Empresa   := QrFiliaisCodigo.Value;//Geral.IMV(EdFilial.Text);
      Destroy;
      //
    end;
  end;
  if Confirmou then
  begin
    Codigo   := QrPediPrzCabCodigo.Value;
    Controle := UMyMod.BuscaEmLivreY_Def('pediprzemp', 'Controle', stIns, 0);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'pediprzemp', False, [
    'Codigo', 'Empresa'], ['Controle'], [Codigo, Empresa], [Controle], True) then
      ReopenPediPrzEmp(Controle);
  end;
end;

procedure TFmFatPedCab.BtItensClick(Sender: TObject);
begin
  UmyMod.CriaFormInsUpd(TFmPediPrzIts, FmPediPrzIts, afmoNegarComAviso,
    QrPediPrzIts, stIns);
end;

procedure TFmFatPedCab.FormCreate(Sender: TObject);
begin
  PainelEdit.Align  := alClient;
  Panel4.Align      := alClient;
  CriaOForm;
end;

procedure TFmFatPedCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrPediPrzCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmFatPedCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmFatPedCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrPediPrzCabCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmFatPedCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmFatPedCab.QrPediPrzCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmFatPedCab.QrPediPrzCabAfterScroll(DataSet: TDataSet);
begin
  ReopenPediPrzIts(0);
  ReopenPediPrzEmp(0);
end;

procedure TFmFatPedCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFatPedCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrPediPrzCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'PediPrzCab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmFatPedCab.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmFatPedCab.Incluinovofaturamento1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrPediPrzCab, [PainelDados],
  [PainelEdita], EdCodUsu, LaTipo, 'pediprzcab');
end;

procedure TFmFatPedCab.QrPediPrzCabBeforeClose(DataSet: TDataSet);
begin
  QrPediPrzIts.Close;
  QrPediPrzEmp.Close;
end;

procedure TFmFatPedCab.QrPediPrzCabBeforeOpen(DataSet: TDataSet);
begin
  QrPediPrzCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmFatPedCab.ReopenPediPrzIts(Controle: Integer);
begin
  QrPediPrzIts.Close;
  QrPediPrzIts.Params[0].AsInteger :=   QrPediPrzCabCodigo.Value;
  QrPediPrzIts.Open;
  //
  if Controle <> 0 then
    QrPediPrzIts.Locate('Controle', Controle, []);
end;

procedure TFmFatPedCab.ReopenPediPrzEmp(Controle: Integer);
begin
  QrPediPrzEmp.Close;
  QrPediPrzEmp.Params[0].AsInteger :=   QrPediPrzCabCodigo.Value;
  QrPediPrzEmp.Open;
  //
  if Controle <> 0 then
    QrPediPrzEmp.Locate('Controle', Controle, []);
end;

end.

