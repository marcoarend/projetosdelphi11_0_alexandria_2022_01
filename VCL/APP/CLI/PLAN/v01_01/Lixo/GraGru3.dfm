object FmGraGru3: TFmGraGru3
  Left = 368
  Top = 194
  Caption = 'PRD-GRUPO-001 :: Cadastro de Grupos de Produtos'
  ClientHeight = 446
  ClientWidth = 990
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 48
    Width = 990
    Height = 398
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Visible = False
    object PainelConfirma: TPanel
      Left = 1
      Top = 349
      Width = 988
      Height = 48
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 8
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
        NumGlyphs = 2
      end
      object Panel1: TPanel
        Left = 879
        Top = 1
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
          NumGlyphs = 2
        end
      end
    end
    object PainelEdit: TPanel
      Left = 1
      Top = 1
      Width = 988
      Height = 104
      Align = alTop
      TabOrder = 0
      object Label7: TLabel
        Left = 4
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label8: TLabel
        Left = 64
        Top = 4
        Width = 76
        Height = 13
        Caption = 'Refer'#234'ncia: [F4]'
        FocusControl = DBEdit1
      end
      object Label9: TLabel
        Left = 148
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object EdCodigo: TdmkEdit
        Left = 4
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdNome: TdmkEdit
        Left = 148
        Top = 20
        Width = 633
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdCodUsu: TdmkEdit
        Left = 64
        Top = 20
        Width = 80
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CodUsu'
        UpdCampo = 'CodUsu'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnKeyDown = EdCodUsuKeyDown
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 990
    Height = 398
    Align = alClient
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object PainelData: TPanel
      Left = 1
      Top = 1
      Width = 988
      Height = 96
      Align = alTop
      Enabled = False
      TabOrder = 1
      object Label1: TLabel
        Left = 4
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 148
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label3: TLabel
        Left = 64
        Top = 4
        Width = 55
        Height = 13
        Caption = 'Refer'#234'ncia:'
        FocusControl = DBEdit1
      end
      object DBEdCodigo: TDBEdit
        Left = 4
        Top = 20
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Nivel3'
        DataSource = DsGraGru3
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxLength = 1
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdNome: TDBEdit
        Left = 148
        Top = 20
        Width = 633
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsGraGru3
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object DBEdit1: TDBEdit
        Left = 64
        Top = 20
        Width = 80
        Height = 21
        DataField = 'CodUsu'
        DataSource = DsGraGru3
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
      end
    end
    object PainelControle: TPanel
      Left = 1
      Top = 349
      Width = 988
      Height = 48
      Align = alBottom
      TabOrder = 0
      object LaRegistro: TdmkLabel
        Left = 173
        Top = 1
        Width = 26
        Height = 13
        Align = alClient
        Caption = '[N]: 0'
        UpdType = utYes
        SQLType = stNil
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 172
        Height = 46
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
          NumGlyphs = 2
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
          NumGlyphs = 2
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
          NumGlyphs = 2
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
          NumGlyphs = 2
        end
      end
      object Panel3: TPanel
        Left = 518
        Top = 1
        Width = 469
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object BtExclui: TBitBtn
          Tag = 12
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          NumGlyphs = 2
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
          NumGlyphs = 2
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          NumGlyphs = 2
        end
        object Panel2: TPanel
          Left = 360
          Top = 0
          Width = 109
          Height = 46
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
            NumGlyphs = 2
          end
        end
      end
    end
    object Panel4: TPanel
      Left = 1
      Top = 97
      Width = 516
      Height = 252
      Align = alLeft
      TabOrder = 2
      object Splitter1: TSplitter
        Left = 237
        Top = 1
        Width = 5
        Height = 250
        ExplicitLeft = 269
      end
      object Panel6: TPanel
        Left = 1
        Top = 1
        Width = 236
        Height = 250
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 236
          Height = 21
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object EdPesqN: TEdit
            Left = 0
            Top = 0
            Width = 236
            Height = 21
            Align = alTop
            TabOrder = 0
          end
        end
        object DBGrid1: TDBGrid
          Left = 0
          Top = 21
          Width = 236
          Height = 229
          Align = alClient
          DataSource = DsGraGruN
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'CodUsu'
              Title.Caption = 'Refer'#234'ncia'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Descri'#231#227'o'
              Width = 140
              Visible = True
            end>
        end
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 990
    Height = 48
    Align = alTop
    Caption = '                              Cadastro de Grupos de Produtos'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object LaTipo: TdmkLabel
      Left = 907
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Inclus'#227'o'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stIns
      ExplicitLeft = 603
      ExplicitTop = 2
      ExplicitHeight = 44
    end
    object Image1: TImage
      Left = 226
      Top = 1
      Width = 681
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 227
      ExplicitTop = 2
      ExplicitWidth = 376
      ExplicitHeight = 44
    end
    object PanelFill2: TPanel
      Left = 1
      Top = 1
      Width = 225
      Height = 46
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 0
        NumGlyphs = 2
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 1
        OnClick = SbNovoClick
        NumGlyphs = 2
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 2
        OnClick = SbNumeroClick
        NumGlyphs = 2
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 3
        OnClick = SbNomeClick
        NumGlyphs = 2
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 4
        OnClick = SbQueryClick
        NumGlyphs = 2
      end
    end
  end
  object DsGraGru3: TDataSource
    DataSet = QrGraGru3
    Left = 40
    Top = 12
  end
  object QrGraGru3: TmySQLQuery
    Database = Dmod.MyLocDatabase
    BeforeOpen = QrGraGru3BeforeOpen
    AfterOpen = QrGraGru3AfterOpen
    SQL.Strings = (
      'SELECT Nivel3, CodUsu, Nome'
      'FROM gragru3')
    Left = 12
    Top = 12
    object QrGraGru3Nivel3: TIntegerField
      FieldName = 'Nivel3'
      Origin = 'gragru3.Nivel3'
    end
    object QrGraGru3CodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'gragru3.CodUsu'
    end
    object QrGraGru3Nome: TWideStringField
      FieldName = 'Nome'
      Origin = 'gragru3.Nome'
      Size = 30
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtAltera
    CanDel01 = BtExclui
    Left = 68
    Top = 12
  end
  object QrGraGruN: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pgt.Codigo, pgt.CodUsu, pgt.Nome, pgt.MadeBy, '
      'pgt.Fracio, pgt.TipPrd, pgt.NivCad, pgt.FaixaIni, pgt.FaixaFim,'
      'ELT(pgt.MadeBy, "Pr'#243'prio","Terceiros") NOME_MADEBY,'
      'ELT(pgt.Fracio+1, '#39'N'#227'o'#39', '#39'Sim'#39') NOME_FRACIO,'
      
        'ELT(pgt.TipPrd, '#39'Produto'#39', '#39'Mat'#233'ria-prima'#39', '#39'Uso e consumo'#39') NOM' +
        'E_TIPPRD'
      'FROM prdgruptip pgt'
      'WHERE Codigo>0'
      'ORDER BY pgt.Nome'
      '')
    Left = 156
    Top = 56
    object QrGraGruNCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrGraGruNCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrGraGruNNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 30
    end
    object QrGraGruNMadeBy: TSmallintField
      FieldName = 'MadeBy'
      Required = True
    end
    object QrGraGruNFracio: TSmallintField
      FieldName = 'Fracio'
      Required = True
    end
    object QrGraGruNTipPrd: TSmallintField
      FieldName = 'TipPrd'
      Required = True
    end
    object QrGraGruNNivCad: TSmallintField
      FieldName = 'NivCad'
      Required = True
    end
    object QrGraGruNFaixaIni: TIntegerField
      FieldName = 'FaixaIni'
      Required = True
    end
    object QrGraGruNFaixaFim: TIntegerField
      FieldName = 'FaixaFim'
      Required = True
    end
    object QrGraGruNNOME_MADEBY: TWideStringField
      FieldName = 'NOME_MADEBY'
      Size = 9
    end
    object QrGraGruNNOME_FRACIO: TWideStringField
      FieldName = 'NOME_FRACIO'
      Size = 3
    end
    object QrGraGruNNOME_TIPPRD: TWideStringField
      FieldName = 'NOME_TIPPRD'
      Size = 13
    end
  end
  object DsGraGruN: TDataSource
    DataSet = QrGraGruN
    Left = 184
    Top = 56
  end
end
