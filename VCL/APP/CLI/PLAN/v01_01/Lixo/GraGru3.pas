unit GraGru3;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, MyDBCheck, DBCGrids,
  IWVCLBaseControl, IWBaseControl, IWBaseWAPControl, IWControlWAP,
  IWCompLabelWAP, IWDBStdCtrlsWAP, Menus, Grids, DBGrids, Tabs, DockTabSet,
  UnDmkProcFunc;

type
  TFmGraGru3 = class(TForm)
    PainelDados: TPanel;
    DsGraGru3: TDataSource;
    QrGraGru3: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TdmkLabel;
    Image1: TImage;
    PanelFill2: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TdmkLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdCodUsu: TdmkEdit;
    QrGraGru3CodUsu: TIntegerField;
    QrGraGru3Nome: TWideStringField;
    QrGraGru3Nivel3: TIntegerField;
    QrGraGruN: TmySQLQuery;
    QrGraGruNCodigo: TIntegerField;
    QrGraGruNCodUsu: TIntegerField;
    QrGraGruNNome: TWideStringField;
    QrGraGruNMadeBy: TSmallintField;
    QrGraGruNFracio: TSmallintField;
    QrGraGruNTipPrd: TSmallintField;
    QrGraGruNNivCad: TSmallintField;
    QrGraGruNFaixaIni: TIntegerField;
    QrGraGruNFaixaFim: TIntegerField;
    QrGraGruNNOME_MADEBY: TWideStringField;
    QrGraGruNNOME_FRACIO: TWideStringField;
    QrGraGruNNOME_TIPPRD: TWideStringField;
    DsGraGruN: TDataSource;
    Panel4: TPanel;
    Panel6: TPanel;
    Panel7: TPanel;
    DBGrid1: TDBGrid;
    Splitter1: TSplitter;
    EdPesqN: TEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrGraGru3AfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrGraGru3BeforeOpen(DataSet: TDataSet);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; Status: String; Codigo: Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure ReopenGraGruN(GraGruTip: Integer);
    procedure ReopenGraGru3(Nivel3: Integer);
    procedure ReopenGraGru2(Nivel2: Integer);
    procedure ReopenGraGru1(Nivel1: Integer);
  end;

var
  FmGraGru3: TFmGraGru3;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, PrdGruNew;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmGraGru3.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmGraGru3.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrGraGru3Nivel3.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmGraGru3.DefParams;
begin
  VAR_GOTOTABELA := 'GraGru3';
  VAR_GOTOMYSQLTABLE := QrGraGru3;
  VAR_GOTONEG := True;
  VAR_GOTOCAMPO := 'Nivel3';
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT Nivel3, CodUsu, Nome');
  VAR_SQLx.Add('FROM gragru3');
  VAR_SQLx.Add('WHERE Nivel3 > -1000');
  //
  VAR_SQL1.Add('AND Nivel3=:P0');
  //
  VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmGraGru3.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (LaTipo.SQLType = stIns) then
    EdCodUsu.ValueVariant :=
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'GraGru3', 'CodUsu');
end;

procedure TFmGraGru3.MostraEdicao(Mostra: Integer; Status: String; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelControle.Visible:=True;
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      PainelControle.Visible:=False;
      if Status = CO_INCLUSAO then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Geral.MensagemBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  LaTipo.Caption := Status;
  GOTOy.BotoesSb(LaTipo.Caption);
end;

procedure TFmGraGru3.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmGraGru3.AlteraRegistro;
var
  GraGru3 : Integer;
begin
  GraGru3 := QrGraGru3Nivel3.Value;
  if QrGraGru3Nivel3.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(GraGru3, Dmod.MyDB, 'GraGru3', 'Nivel3') then
  begin
    try
      UMyMod.UpdLockY(GraGru3, Dmod.MyDB, 'GraGru3', 'Nivel3');
      MostraEdicao(1, CO_ALTERACAO, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmGraGru3.IncluiRegistro;
var
  Cursor : TCursor;
  GraGru3 : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    GraGru3 := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'GraGru3', 'GraGru3', 'Nivel3');
    if Length(FormatFloat(FFormatFloat, GraGru3))>Length(FFormatFloat) then
    begin
      Geral.MensagemBox(
      'Inclus�o cancelada. Limite de cadastros extrapolado', 'Erro',
      MB_OK+MB_ICONERROR);
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(1, CO_INCLUSAO, GraGru3);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmGraGru3.QueryPrincipalAfterOpen;
begin
end;

procedure TFmGraGru3.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmGraGru3.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmGraGru3.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmGraGru3.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmGraGru3.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmGraGru3.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrGraGru3, [PainelDados],
  [PainelEdita], EdCodUsu, LaTipo);
end;

procedure TFmGraGru3.BtSaidaClick(Sender: TObject);
begin
  VAR_COD := QrGraGru3Nivel3.Value;
  Close;
end;

procedure TFmGraGru3.BtConfirmaClick(Sender: TObject);
var
  Nivel3: Integer;
  Nome: String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Geral.MensagemBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Nivel3 := UMyMod.BuscaEmLivreY_Def('GraGru3', 'Nivel3', LaTipo.SQLType,
    QrGraGru3Nivel3.Value);
  if UMyMod.ExecSQLInsUpdPanel(LaTipo.SQLType, FmGraGru3, PainelEdit,
    'GraGru3', Nivel3, Dmod.QrUpd, [PainelEdita], [PainelDados], LaTipo) then
  begin
    LocCod(Nivel3, Nivel3);
  end;
end;

procedure TFmGraGru3.BtDesisteClick(Sender: TObject);
var
  Nivel3 : Integer;
begin
  Nivel3 := Geral.IMV(EdCodigo.Text);
  if LaTipo.Caption = CO_INCLUSAO then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'GraGru3', Nivel3);
  UMyMod.UpdUnlockY(Nivel3, Dmod.MyDB, 'GraGru3', 'Nivel3');
  MostraEdicao(0, CO_TRAVADO, 0);
  UMyMod.UpdUnlockY(Nivel3, Dmod.MyDB, 'GraGru3', 'Nivel3');
end;

procedure TFmGraGru3.FormCreate(Sender: TObject);
begin
  PainelEdit.Align  := alClient;
  PainelData.Align  := alClient;
  CriaOForm;
  //
  ReopenGraGruN(0);
end;

procedure TFmGraGru3.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrGraGru3Nivel3.Value, LaRegistro.Caption);
end;

procedure TFmGraGru3.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmGraGru3.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrGraGru3CodUsu.Value, LaRegistro.Caption);
end;

procedure TFmGraGru3.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmGraGru3.QrGraGru3AfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmGraGru3.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraGru3.SbQueryClick(Sender: TObject);
begin
  LocCod(QrGraGru3Nivel3.Value,
  CuringaLoc.CriaForm('Nivel3', CO_NOME, 'GraGru3', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmGraGru3.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmGraGru3.QrGraGru3BeforeOpen(DataSet: TDataSet);
begin
  QrGraGru3Nivel3.DisplayFormat := FFormatFloat;
end;

procedure TFmGraGru3.ReopenGraGruN(GraGruTip: Integer);
begin
  QrGraGruN.Close;
  QrGraGruN.Open;
end;

procedure TFmGraGru3.ReopenGraGru3(Nivel3: Integer);
begin
  QrGraGru3.Close;
  //QrGraGru3.Params[0].AsInteger
  QrGraGru3.Open;
end;

procedure TFmGraGru3.ReopenGraGru2(Nivel2: Integer);
begin
  //
end;

procedure TFmGraGru3.ReopenGraGru1(Nivel1: Integer);
begin
  //
end;

end.

