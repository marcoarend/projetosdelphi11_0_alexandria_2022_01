unit PediVda;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, Grids, DBGrids, dmkDBGrid,
  Menus, dmkValUsu, dmkDBLookupComboBox, dmkEditCB, ComCtrls,
  dmkEditDateTimePicker, dmkCheckBox, frxClass, frxDBSet, UnDmkProcFunc;

type
  TFmPediVda = class(TForm)
    PainelDados: TPanel;
    DsPediVda: TDataSource;
    QrPediVda: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TdmkLabel;
    Image1: TImage;
    PanelFill2: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TdmkLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtItens: TBitBtn;
    BtPedidos: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    dmkPermissoes1: TdmkPermissoes;
    PMPedidos: TPopupMenu;
    PMItens: TPopupMenu;
    Incluinovopedido1: TMenuItem;
    Alterapedidoatual1: TMenuItem;
    Excluipedidoatual1: TMenuItem;
    VuEmpresa: TdmkValUsu;
    QrPediVdaCodigo: TIntegerField;
    QrPediVdaCodUsu: TIntegerField;
    QrPediVdaEmpresa: TIntegerField;
    QrPediVdaCliente: TIntegerField;
    QrPediVdaDtaEmiss: TDateField;
    QrPediVdaDtaEntra: TDateField;
    QrPediVdaDtaInclu: TDateField;
    QrPediVdaDtaPrevi: TDateField;
    QrPediVdaNOMEEMP: TWideStringField;
    QrPediVdaNOMECLI: TWideStringField;
    QrPediVdaCIDADECLI: TWideStringField;
    QrPediVdaNOMEUF: TWideStringField;
    QrPediVdaFilial: TIntegerField;
    QrPediVdaPrioridade: TSmallintField;
    QrPediVdaCondicaoPG: TIntegerField;
    QrPediVdaMoeda: TIntegerField;
    QrPediVdaSituacao: TIntegerField;
    VuCondicaoPG: TdmkValUsu;
    VuCambioMda: TdmkValUsu;
    VuTabelaPrc: TdmkValUsu;
    QrPediVdaTabelaPrc: TIntegerField;
    VuMotivoSit: TdmkValUsu;
    QrPediVdaMotivoSit: TIntegerField;
    QrPediVdaLoteProd: TIntegerField;
    QrPediVdaPedidoCli: TWideStringField;
    QrPediVdaFretePor: TSmallintField;
    QrPediVdaTransporta: TIntegerField;
    QrPediVdaRedespacho: TIntegerField;
    QrPediVdaRegrFiscal: TIntegerField;
    QrPediVdaDesoAces_V: TFloatField;
    QrPediVdaDesoAces_P: TFloatField;
    QrPediVdaFrete_V: TFloatField;
    QrPediVdaFrete_P: TFloatField;
    QrPediVdaSeguro_V: TFloatField;
    QrPediVdaSeguro_P: TFloatField;
    QrPediVdaTotalQtd: TFloatField;
    QrPediVdaTotal_Vlr: TFloatField;
    QrPediVdaTotal_Des: TFloatField;
    QrPediVdaTotal_Tot: TFloatField;
    QrPediVdaObserva: TWideMemoField;
    QrPediVdaNOMETABEPRCCAD: TWideStringField;
    QrPediVdaNOMEMOEDA: TWideStringField;
    QrPediVdaNOMECONDICAOPG: TWideStringField;
    QrPediVdaNOMEMOTIVO: TWideStringField;
    QrPediVdaNOMESITUACAO: TWideStringField;
    GroupBox1: TGroupBox;
    Label9: TLabel;
    Label2: TLabel;
    EdCliente: TdmkEditCB;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    CBCliente: TdmkDBLookupComboBox;
    DBEdCidade: TDBEdit;
    EdCodUsu: TdmkEdit;
    Label8: TLabel;
    DBEdNOMEUF: TDBEdit;
    SpeedButton5: TSpeedButton;
    Label23: TLabel;
    EdSituacao: TdmkEditCB;
    CBSituacao: TdmkDBLookupComboBox;
    Label26: TLabel;
    EdMotivoSit: TdmkEditCB;
    CBMotivoSit: TdmkDBLookupComboBox;
    EdCodigo: TdmkEdit;
    Label7: TLabel;
    TPDtaInclu: TdmkEditDateTimePicker;
    Label6: TLabel;
    SpeedButton7: TSpeedButton;
    QrPediVdaRepresen: TIntegerField;
    QrPediVdaComisFat: TFloatField;
    QrPediVdaComisRec: TFloatField;
    QrPediVdaNOMEACC: TWideStringField;
    QrPediVdaCartEmis: TIntegerField;
    QrPediVdaAFP_Sit: TSmallintField;
    QrPediVdaAFP_Per: TFloatField;
    QrPediVdaNOMECARTEMIS: TWideStringField;
    GroupBox9: TGroupBox;
    Label56: TLabel;
    Label57: TLabel;
    Label58: TLabel;
    Label59: TLabel;
    Label60: TLabel;
    Label61: TLabel;
    Label62: TLabel;
    DBEdit1: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit30: TDBEdit;
    DBEdit31: TDBEdit;
    DBEdit20: TDBEdit;
    DBEdit29: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit22: TDBEdit;
    DBEdit27: TDBEdit;
    DBEdit32: TDBEdit;
    QrPediVdaNOMEFRETEPOR: TWideStringField;
    QrPediVdaNOMETRANSP: TWideStringField;
    QrPediVdaNOMEREDESP: TWideStringField;
    QrPediVdaCODUSU_CLI: TIntegerField;
    QrPediVdaCODUSU_ACC: TIntegerField;
    QrPediVdaCODUSU_TRA: TIntegerField;
    QrPediVdaCODUSU_RED: TIntegerField;
    QrPediVdaCODUSU_MOT: TIntegerField;
    QrPediVdaCODUSU_TPC: TIntegerField;
    QrPediVdaCODUSU_MDA: TIntegerField;
    QrPediVdaCODUSU_PPC: TIntegerField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel4: TPanel;
    GroupBox10: TGroupBox;
    Label54: TLabel;
    Label55: TLabel;
    Label63: TLabel;
    Label64: TLabel;
    Label65: TLabel;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit19: TDBEdit;
    GroupBox11: TGroupBox;
    Label1: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label10: TLabel;
    DBEdit21: TDBEdit;
    DBEdit24: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit25: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit26: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit28: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    GroupBox8: TGroupBox;
    Label11: TLabel;
    DBEdit33: TDBEdit;
    GroupBox14: TGroupBox;
    Label51: TLabel;
    Label52: TLabel;
    Label53: TLabel;
    Label66: TLabel;
    Label67: TLabel;
    Label68: TLabel;
    Label69: TLabel;
    Label70: TLabel;
    Label71: TLabel;
    Label72: TLabel;
    DBEdit16: TDBEdit;
    DBEdit23: TDBEdit;
    Memo2: TMemo;
    DBEdit34: TDBEdit;
    DBEdit35: TDBEdit;
    DBEdit36: TDBEdit;
    DBEdit37: TDBEdit;
    DBEdit38: TDBEdit;
    DBEdit39: TDBEdit;
    DBEdit40: TDBEdit;
    DBEdit41: TDBEdit;
    DBEdit42: TDBEdit;
    DBEdit43: TDBEdit;
    GroupBox12: TGroupBox;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    DBEdit44: TDBEdit;
    DBEdit45: TDBEdit;
    DBEdit46: TDBEdit;
    DBEdit47: TDBEdit;
    PageControl2: TPageControl;
    TabSheet3: TTabSheet;
    Panel6: TPanel;
    GroupBox2: TGroupBox;
    Label12: TLabel;
    Label13: TLabel;
    Label15: TLabel;
    Label14: TLabel;
    Label27: TLabel;
    TPDtaEmiss: TdmkEditDateTimePicker;
    TPDtaEntra: TdmkEditDateTimePicker;
    EdPrioridade: TdmkEdit;
    TPDtaPrevi: TdmkEditDateTimePicker;
    EdPedidoCli: TdmkEdit;
    GroupBox3: TGroupBox;
    BtTabelaPrc: TSpeedButton;
    LaTabelaPrc: TLabel;
    SpeedButton9: TSpeedButton;
    Label22: TLabel;
    BtCondicaoPG: TSpeedButton;
    Label24: TLabel;
    LaCondicaoPG: TLabel;
    SpeedButton13: TSpeedButton;
    Label49: TLabel;
    CBTabelaPrc: TdmkDBLookupComboBox;
    EdTabelaPrc: TdmkEditCB;
    CBCartEmis: TdmkDBLookupComboBox;
    CBMoeda: TdmkDBLookupComboBox;
    EdMoeda: TdmkEditCB;
    EdCartEmis: TdmkEditCB;
    CBCondicaoPG: TdmkDBLookupComboBox;
    EdCondicaoPG: TdmkEditCB;
    CkAFP_Sit: TdmkCheckBox;
    EdAFP_Per: TdmkEdit;
    GroupBox4: TGroupBox;
    Label28: TLabel;
    DBEdit15: TDBEdit;
    GroupBox5: TGroupBox;
    Label29: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    SpeedButton6: TSpeedButton;
    SpeedButton11: TSpeedButton;
    Label20: TLabel;
    Label32: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    EdFretePor: TdmkEditCB;
    CBFretePor: TdmkDBLookupComboBox;
    EdTransporta: TdmkEditCB;
    CBTransporta: TdmkDBLookupComboBox;
    EdRedespacho: TdmkEditCB;
    CBRedespacho: TdmkDBLookupComboBox;
    MeEnderecoEntrega1: TMemo;
    EdDesoAces_V: TdmkEdit;
    EdFrete_V: TdmkEdit;
    EdSeguro_P: TdmkEdit;
    EdDesoAces_P: TdmkEdit;
    EdFrete_P: TdmkEdit;
    EdSeguro_V: TdmkEdit;
    GroupBox7: TGroupBox;
    Label46: TLabel;
    SpeedButton12: TSpeedButton;
    Label47: TLabel;
    Label48: TLabel;
    EdRepresen: TdmkEditCB;
    CBRepresen: TdmkDBLookupComboBox;
    EdComisFat: TdmkEdit;
    EdComisRec: TdmkEdit;
    GroupBox6: TGroupBox;
    Incluinovositensdegrupo1: TMenuItem;
    QrPediVdaGru: TmySQLQuery;
    DsPediVdaGru: TDataSource;
    QrPediVdaGruCodUsu: TIntegerField;
    QrPediVdaGruNome: TWideStringField;
    QrPediVdaGruNivel1: TIntegerField;
    QrPediVdaMedDDSimpl: TFloatField;
    QrPediVdaMedDDReal: TFloatField;
    DBGGru: TdmkDBGrid;
    PageControl3: TPageControl;
    TabSheet5: TTabSheet;
    GradeQ: TStringGrid;
    StaticText1: TStaticText;
    TabSheet4: TTabSheet;
    GradeF: TStringGrid;
    TabSheet6: TTabSheet;
    GradeD: TStringGrid;
    StaticText3: TStaticText;
    TabSheet7: TTabSheet;
    GradeV: TStringGrid;
    TabSheet8: TTabSheet;
    GradeC: TStringGrid;
    StaticText6: TStaticText;
    TabSheet9: TTabSheet;
    GradeA: TStringGrid;
    StaticText2: TStaticText;
    TabSheet10: TTabSheet;
    GradeX: TStringGrid;
    QrPediVdaGruGRATAMCAD: TIntegerField;
    BtVisual: TBitBtn;
    BitBtn1: TBitBtn;
    QrPediVdaGruQuantP: TFloatField;
    QrPediVdaGruValLiq: TFloatField;
    GroupBox13: TGroupBox;
    QrPediVdaValLiq: TFloatField;
    QrPediVdaQuantP: TFloatField;
    Label19: TLabel;
    DBEdit13: TDBEdit;
    Label21: TLabel;
    DBEdit14: TDBEdit;
    BtRecalcula: TBitBtn;
    AlteraExcluiIncluiitemselecionado1: TMenuItem;
    Label25: TLabel;
    EdRegrFiscal: TdmkEditCB;
    CBRegrFiscal: TdmkDBLookupComboBox;
    SbRegrFiscal: TSpeedButton;
    VuFisRegCad: TdmkValUsu;
    EdModeloNF: TdmkEdit;
    Label33: TLabel;
    GroupBox15: TGroupBox;
    Label34: TLabel;
    Label35: TLabel;
    QrPediVdaNOMEFISREGCAD: TWideStringField;
    QrPediVdaNOMEMODELONF: TWideStringField;
    QrPediVdaCODUSU_FRC: TIntegerField;
    QrPediVdaMODELO_NF: TIntegerField;
    DBEdit48: TDBEdit;
    DBEdit49: TDBEdit;
    DBEdit50: TDBEdit;
    QrPediVdaMaxDesco: TFloatField;
    QrPediVdaJurosMes: TFloatField;
    N1: TMenuItem;
    IncluinovositensporLeitura1: TMenuItem;
    N2: TMenuItem;
    QrPediVdaDescoMax: TFloatField;
    TabSheet11: TTabSheet;
    Panel7: TPanel;
    Panel8: TPanel;
    dmkDBGrid1: TdmkDBGrid;
    Panel9: TPanel;
    Panel10: TPanel;
    BtCustom: TBitBtn;
    PMCustom: TPopupMenu;
    IncluinovoitemprodutoCustomizvel1: TMenuItem;
    Adicionapartesaoitemselecionado1: TMenuItem;
    QrCustomizados: TmySQLQuery;
    QrPediVdaGruItensCustomizados: TFloatField;
    DsCustomizados: TDataSource;
    QrCustomizadosGRATAMCAD: TIntegerField;
    QrCustomizadosGRATAMITS: TAutoIncField;
    QrCustomizadosNO_TAM: TWideStringField;
    QrCustomizadosNO_COR: TWideStringField;
    DBGrid2: TDBGrid;
    QrCustomizadosGraGruX: TIntegerField;
    QrCustomizadosControle: TIntegerField;
    QrPediVdaCuz: TmySQLQuery;
    DsPediVdaCuz: TDataSource;
    Splitter1: TSplitter;
    QrPediVdaCuzNO_TAM: TWideStringField;
    QrPediVdaCuzNO_COR: TWideStringField;
    QrPediVdaCuzControle: TIntegerField;
    QrPediVdaCuzConta: TIntegerField;
    QrPediVdaCuzMatPartCad: TIntegerField;
    QrPediVdaCuzGraGruX: TIntegerField;
    QrPediVdaCuzMedidaC: TFloatField;
    QrPediVdaCuzMedidaL: TFloatField;
    QrPediVdaCuzMedidaA: TFloatField;
    QrPediVdaCuzMedidaE: TFloatField;
    QrPediVdaCuzQuantP: TFloatField;
    QrPediVdaCuzQuantX: TFloatField;
    QrPediVdaCuzPrecoO: TFloatField;
    QrPediVdaCuzPrecoR: TFloatField;
    QrPediVdaCuzPrecoF: TFloatField;
    QrPediVdaCuzValBru: TFloatField;
    QrPediVdaCuzDescoP: TFloatField;
    QrPediVdaCuzDescoV: TFloatField;
    QrPediVdaCuzPerCustom: TFloatField;
    QrPediVdaCuzValLiq: TFloatField;
    QrPediVdaCuzTipDimens: TSmallintField;
    QrPediVdaCuzNO_PARTE: TWideStringField;
    QrPediVdaCuzNO_GRUPO: TWideStringField;
    N3: TMenuItem;
    AtualizaValoresdoitem1: TMenuItem;
    ItemprodutoCustomizvel1: TMenuItem;
    Partedoitemprodutoselecionado1: TMenuItem;
    Alteraitemprodutoselecionado1: TMenuItem;
    QrPediVdaCuzMedidaC_TXT: TWideStringField;
    QrPediVdaCuzMedidaL_TXT: TWideStringField;
    QrPediVdaCuzMedidaA_TXT: TWideStringField;
    QrPediVdaCuzMedidaE_TXT: TWideStringField;
    QrCustomizadosMedidaC: TFloatField;
    QrCustomizadosMedidaL: TFloatField;
    QrCustomizadosMedidaA: TFloatField;
    QrCustomizadosMedidaE: TFloatField;
    QrCustomizadosValLiq: TFloatField;
    Excluipartedoprodutoselecionado1: TMenuItem;
    Excluiitemprodutoselecionado1: TMenuItem;
    DBGrid1: TDBGrid;
    QrPediVdaCuzSiglaCustm: TWideStringField;
    frxDsCli: TfrxDBDataset;
    QrCli: TmySQLQuery;
    QrCliE_ALL: TWideStringField;
    QrCliCNPJ_TXT: TWideStringField;
    QrCliNOME_TIPO_DOC: TWideStringField;
    QrCliTE1_TXT: TWideStringField;
    QrCliFAX_TXT: TWideStringField;
    QrCliNUMERO_TXT: TWideStringField;
    QrCliCEP_TXT: TWideStringField;
    QrCliCodigo: TIntegerField;
    QrCliTipo: TSmallintField;
    QrCliCodUsu: TIntegerField;
    QrCliNOME_ENT: TWideStringField;
    QrCliCNPJ_CPF: TWideStringField;
    QrCliIE_RG: TWideStringField;
    QrCliRUA: TWideStringField;
    QrCliNUMERO: TLargeintField;
    QrCliCOMPL: TWideStringField;
    QrCliBAIRRO: TWideStringField;
    QrCliCIDADE: TWideStringField;
    QrCliNOMELOGRAD: TWideStringField;
    QrCliNOMEUF: TWideStringField;
    QrCliPais: TWideStringField;
    QrCliLograd: TLargeintField;
    QrCliCEP: TLargeintField;
    QrCliENDEREF: TWideStringField;
    QrCliTE1: TWideStringField;
    QrCliFAX: TWideStringField;
    QrCliIE: TWideStringField;
    QrCliUF: TLargeintField;
    QrCliCAD_FEDERAL: TWideStringField;
    QrCliCAD_ESTADUAL: TWideStringField;
    QrCliIE_TXT: TWideStringField;
    frxPED_VENDA_001_01: TfrxReport;
    QrItsN: TmySQLQuery;
    QrItsNCU_NIVEL1: TIntegerField;
    QrItsNNO_NIVEL1: TWideStringField;
    QrItsNNO_TAM: TWideStringField;
    QrItsNNO_COR: TWideStringField;
    QrItsNQuantP: TFloatField;
    QrItsNPrecoR: TFloatField;
    QrItsNDescoP: TFloatField;
    QrItsNPrecoF: TFloatField;
    QrItsNValBru: TFloatField;
    QrItsNValLiq: TFloatField;
    QrItsC: TmySQLQuery;
    frxDsItsN: TfrxDBDataset;
    QrItsNKGT: TLargeintField;
    frxDsPediVda: TfrxDBDataset;
    QrItsCCU_NIVEL1: TIntegerField;
    QrItsCNO_NIVEL1: TWideStringField;
    QrItsCNO_TAM: TWideStringField;
    QrItsCNO_COR: TWideStringField;
    QrItsCQuantP: TFloatField;
    QrItsCPrecoR: TFloatField;
    QrItsCDescoP: TFloatField;
    QrItsCPrecoF: TFloatField;
    QrItsCValBru: TFloatField;
    QrItsCValLiq: TFloatField;
    QrItsCMedidaC: TFloatField;
    QrItsCMedidaL: TFloatField;
    QrItsCMedidaA: TFloatField;
    QrItsCMedidaE: TFloatField;
    QrItsCPercCustom: TFloatField;
    QrItsCInfAdCuztm: TIntegerField;
    QrItsCMedida1: TWideStringField;
    QrItsCMedida2: TWideStringField;
    QrItsCMedida3: TWideStringField;
    QrItsCMedida4: TWideStringField;
    QrItsCSigla1: TWideStringField;
    QrItsCSigla2: TWideStringField;
    QrItsCSigla3: TWideStringField;
    QrItsCSigla4: TWideStringField;
    frxDsItsC: TfrxDBDataset;
    QrItsCDESCRICAO: TWideStringField;
    QrItsZ: TmySQLQuery;
    frxDsItsZ: TfrxDBDataset;
    QrItsZCU_NIVEL1: TIntegerField;
    QrItsZNO_NIVEL1: TWideStringField;
    QrItsZNO_TAM: TWideStringField;
    QrItsZNO_COR: TWideStringField;
    QrItsZQuantP: TFloatField;
    QrItsZPrecoR: TFloatField;
    QrItsZDescoP: TFloatField;
    QrItsZPrecoF: TFloatField;
    QrItsZValBru: TFloatField;
    QrItsZValLiq: TFloatField;
    QrItsZMedidaC: TFloatField;
    QrItsZMedidaL: TFloatField;
    QrItsZMedidaA: TFloatField;
    QrItsZMedidaE: TFloatField;
    QrItsZPerCustom: TFloatField;
    QrItsZNO_MatPartCad: TWideStringField;
    QrItsZMedida1: TWideStringField;
    QrItsZMedida2: TWideStringField;
    QrItsZMedida3: TWideStringField;
    QrItsZMedida4: TWideStringField;
    QrItsZSigla1: TWideStringField;
    QrItsZSigla2: TWideStringField;
    QrItsZSigla3: TWideStringField;
    QrItsZSigla4: TWideStringField;
    QrItsZDESCRICAO: TWideStringField;
    QrItsCControle: TIntegerField;
    QrItsCKGT: TLargeintField;
    QrItsZKGT: TLargeintField;
    QrItsCTipDimens: TSmallintField;
    QrItsZTipDimens: TSmallintField;
    QrPediVdaGruFracio: TSmallintField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrPediVdaAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrPediVdaBeforeOpen(DataSet: TDataSet);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure BtPedidosClick(Sender: TObject);
    procedure PMPedidosPopup(Sender: TObject);
    procedure QrPediVdaBeforeClose(DataSet: TDataSet);
    procedure QrPediVdaAfterScroll(DataSet: TDataSet);
    procedure BtItensClick(Sender: TObject);
    procedure Incluinovopedido1Click(Sender: TObject);
    procedure Alterapedidoatual1Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
    procedure BtTabelaPrcClick(Sender: TObject);
    procedure SpeedButton9Click(Sender: TObject);
    procedure BtCondicaoPGClick(Sender: TObject);
    procedure EdTabelaPrcChange(Sender: TObject);
    procedure SpeedButton11Click(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure SpeedButton12Click(Sender: TObject);
    procedure SpeedButton13Click(Sender: TObject);
    procedure EdDesoAces_PChange(Sender: TObject);
    procedure EdFrete_PChange(Sender: TObject);
    procedure EdSeguro_PChange(Sender: TObject);
    procedure QrPediVdaCalcFields(DataSet: TDataSet);
    procedure Incluinovositensdegrupo1Click(Sender: TObject);
    procedure QrPediVdaGruAfterScroll(DataSet: TDataSet);
    procedure BtVisualClick(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure GradeADrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeCDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeQDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeFDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeDDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeVDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeXDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure BitBtn1Click(Sender: TObject);
    procedure QrPediVdaGruAfterOpen(DataSet: TDataSet);
    procedure GradeQDblClick(Sender: TObject);
    procedure BtRecalculaClick(Sender: TObject);
    procedure AlteraExcluiIncluiitemselecionado1Click(Sender: TObject);
    procedure SbRegrFiscalClick(Sender: TObject);
    procedure EdRegrFiscalChange(Sender: TObject);
    procedure EdRegrFiscalExit(Sender: TObject);
    procedure IncluinovositensporLeitura1Click(Sender: TObject);
    procedure CBMotivoSitExit(Sender: TObject);
    procedure Adicionapartesaoitemselecionado1Click(Sender: TObject);
    procedure Custumizao1Click(Sender: TObject);
    procedure BtCustomClick(Sender: TObject);
    procedure IncluinovoitemprodutoCustomizvel1Click(Sender: TObject);
    procedure PMCustomPopup(Sender: TObject);
    procedure QrPediVdaGruBeforeClose(DataSet: TDataSet);
    procedure QrCustomizadosAfterScroll(DataSet: TDataSet);
    procedure QrCustomizadosBeforeClose(DataSet: TDataSet);
    procedure AtualizaValoresdoitem1Click(Sender: TObject);
    procedure Alteraitemprodutoselecionado1Click(Sender: TObject);
    procedure QrPediVdaCuzCalcFields(DataSet: TDataSet);
    procedure Excluipartedoprodutoselecionado1Click(Sender: TObject);
    procedure Excluiitemprodutoselecionado1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure QrCliCalcFields(DataSet: TDataSet);
    procedure QrItsCCalcFields(DataSet: TDataSet);
    procedure QrItsZCalcFields(DataSet: TDataSet);
    procedure QrItsCAfterScroll(DataSet: TDataSet);
    procedure frxPED_VENDA_001_01GetValue(const VarName: string;
      var Value: Variant);
  private
    { Private declarations }
    FDBGWidth: Integer;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; Status: String; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure CalculaDesoAces_V();
    procedure CalculaFrete_V();
    procedure CalculaSeguro_V();
    procedure DesabilitaComponentes();
    procedure MostraPediVdaGru(SQLType: TSQLType);
    procedure MostraPediVdaLei(SQLType: TSQLType);
    procedure GradeQDblClick2();

    //function ObtemQtde(var Qtde: Double): Boolean;
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenPediVdaGru(Nivel1: Integer);
    procedure ReopenPediVdaCuz(Conta: Integer);
    procedure MostraFmPediVdaCuzParIns(CodUsu, PrdGrupTip, Nivel1, GraGruX, Controle:
              Integer; SQLType: TSQLType;
              QuantP, MedidaC, MedidaL, MedidaA, MedidaE: Double);
    procedure AtualizaItemCustomizado(Controle: Integer);
  end;

var
  FmPediVda: TFmPediVda;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, GraAtrIts, MyDBCheck, ModuleGeral, Entidade2, ModPediVda, Motivos,
  TabePrcCab, CambioMda, PediPrzCab, PediAcc, Carteiras, PediVdaGru, ModProd,
  UnMyObjects, PediVdaImp, GetValor, PediVdaAlt, Principal, PediVdaLei,
  PediVdaCuzIns, PediVdaCuzUpd, PediVdaCuzParIns;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmPediVda.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmPediVda.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrPediVdaCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmPediVda.DefParams;
begin
  VAR_GOTOTABELA := 'PediVda';
  VAR_GOTOMYSQLTABLE := QrPediVda;
  VAR_GOTONEG := True;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT pvd.Codigo, pvd.CodUsu, pvd.Empresa, pvd.Cliente,');
  VAR_SQLx.Add('pvd.DtaEmiss, pvd.DtaEntra, pvd.DtaInclu, pvd.DtaPrevi,');
  VAR_SQLx.Add('pvd.Prioridade, pvd.CondicaoPG, pvd.Moeda,');
  VAR_SQLx.Add('pvd.Situacao, pvd.TabelaPrc, pvd.MotivoSit, pvd.LoteProd,');
  VAR_SQLx.Add('pvd.PedidoCli, pvd.FretePor, pvd.Transporta, pvd.Redespacho,');
  VAR_SQLx.Add('pvd.RegrFiscal, pvd.DesoAces_V, pvd.DesoAces_P,');
  VAR_SQLx.Add('pvd.Frete_V, pvd.Frete_P, pvd.Seguro_V, pvd.Seguro_P,');
  VAR_SQLx.Add('pvd.TotalQtd, pvd.Total_Vlr, pvd.Total_Des, pvd.Total_Tot,');

  VAR_SQLx.Add('pvd.Observa, tpc.Nome NOMETABEPRCCAD, tpc.DescoMax,');
  VAR_SQLx.Add('mda.Nome NOMEMOEDA, pvd.Represen, pvd.ComisFat,');
  VAR_SQLx.Add('pvd.ComisRec, pvd.CartEmis, pvd.AFP_Sit, pvd.AFP_Per,');
  VAR_SQLx.Add('ppc.MedDDSimpl, ppc.MedDDReal, ppc.MaxDesco, ppc.JurosMes,');
  VAR_SQLx.Add('pvd.ValLiq, pvd.QuantP, frc.Nome NOMEFISREGCAD,');
  VAR_SQLx.Add('imp.Nome NOMEMODELONF,');

  VAR_SQLx.Add('IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NOMEEMP,');
  VAR_SQLx.Add('IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NOMECLI,');
  VAR_SQLx.Add('IF(cli.Tipo=0, cli.ECidade, cli.PCidade) CIDADECLI,');
  VAR_SQLx.Add('IF(ven.Tipo=0, ven.RazaoSocial, ven.NOME) NOMEACC,');
  VAR_SQLx.Add('IF(tra.Tipo=0, tra.RazaoSocial, tra.NOME) NOMETRANSP,');
  VAR_SQLx.Add('IF(red.Tipo=0, red.RazaoSocial, red.NOME) NOMEREDESP,');
  VAR_SQLx.Add('uf1.Nome NOMEUF, emp.Filial, car.Nome NOMECARTEMIS,');
  VAR_SQLx.Add('ppc.Nome NOMECONDICAOPG, mot.Nome NOMEMOTIVO,');
  VAR_SQLx.Add('cli.CodUsu CODUSU_CLI, ven.CodUsu CODUSU_ACC,');
  VAR_SQLx.Add('tra.CodUsu CODUSU_TRA, red.CodUsu CODUSU_RED,');
  VAR_SQLx.Add('mot.CodUsu CODUSU_MOT, tpc.CodUsu CODUSU_TPC,');
  VAR_SQLx.Add('mda.CodUsu CODUSU_MDA, ppc.CodUsu CODUSU_PPC,');
  VAR_SQLx.Add('frc.CodUsu CODUSU_FRC, imp.Codigo MODELO_NF');
  VAR_SQLx.Add('FROM pedivda pvd');
  VAR_SQLx.Add('LEFT JOIN entidades  emp ON emp.Codigo=pvd.Empresa');
  VAR_SQLx.Add('LEFT JOIN entidades  cli ON cli.Codigo=pvd.Cliente');
  VAR_SQLx.Add('LEFT JOIN entidades  tra ON tra.Codigo=pvd.Transporta');
  VAR_SQLx.Add('LEFT JOIN entidades  red ON red.Codigo=pvd.Redespacho');
  VAR_SQLx.Add('LEFT JOIN ufs        uf1 ON uf1.Codigo=IF(cli.Tipo=0, cli.EUF, cli.PUF)');
  VAR_SQLx.Add('LEFT JOIN tabeprccab tpc ON tpc.Codigo=pvd.TabelaPrc');
  VAR_SQLx.Add('LEFT JOIN cambiomda  mda ON mda.Codigo=pvd.Moeda');
  VAR_SQLx.Add('LEFT JOIN pediprzcab ppc ON ppc.Codigo=pvd.CondicaoPG');
  VAR_SQLx.Add('LEFT JOIN motivos    mot ON mot.Codigo=pvd.MotivoSit');
  VAR_SQLx.Add('LEFT JOIN pediacc    acc ON acc.Codigo=pvd.Represen');
  VAR_SQLx.Add('LEFT JOIN entidades  ven ON ven.Codigo=acc.Codigo');
  VAR_SQLx.Add('LEFT JOIN carteiras  car ON car.Codigo=pvd.CartEmis');
  VAR_SQLx.Add('LEFT JOIN fisregcad  frc ON frc.Codigo=pvd.RegrFiscal');
  VAR_SQLx.Add('LEFT JOIN imprime    imp ON imp.Codigo=frc.ModeloNF');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE pvd.Codigo > -1000');
  //
  VAR_SQL1.Add('AND pvd.Codigo=:P0');
  //
  VAR_SQL2.Add('AND pvd.CodUsu=:P0');
  //
  //VAR_SQLa.Add('AND pvd.Nome Like :P0');
  //
end;

procedure TFmPediVda.DesabilitaComponentes();
var
  Habilita1, Habilita2: Boolean;
begin
  Habilita1 := not ((LaTipo.SQLType = stUpd) and (QrPediVdaGru.RecordCount > 0));
  //
  Habilita2 := Habilita1 or (QrPediVdaCondicaoPG.Value = 0);
  LaCondicaoPG.Enabled := Habilita2;
  EdCondicaoPG.Enabled := Habilita2;
  CBCondicaoPG.Enabled := Habilita2;
  BtCondicaoPG.Enabled := Habilita2;
  //
  Habilita2 := Habilita1 or (QrPediVdaTabelaPrc.Value = 0);
  LaTabelaPrc.Enabled := Habilita2;
  EdTabelaPrc.Enabled := Habilita2;
  CBTabelaPrc.Enabled := Habilita2;
  BtTabelaPrc.Enabled := Habilita2;
  //
end;

procedure TFmPediVda.EdClienteChange(Sender: TObject);
begin
  MeEnderecoEntrega1.Text :=
    DModG.ObtemEnderecoEntrega3Linhas(EdCliente.ValueVariant);
  if EdCliente.ValueVariant = 0 then
  begin
    DBEdCidade.DataField := '';
    DBEdNOMEUF.DataField := '';
  end else begin
    DBEdCidade.DataField := 'CIDADE';
    DBEdNOMEUF.DataField := 'NOMEUF';
  end;
end;

procedure TFmPediVda.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (LaTipo.SQLType = stIns) then
    EdCodUsu.ValueVariant :=
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'PediVda', 'CodUsu', [], []);
end;

procedure TFmPediVda.EdDesoAces_PChange(Sender: TObject);
begin
  if EdDesoAces_P.ValueVariant <> 0 then
  begin
    EdDesoAces_V.Enabled := False;
    CalculaDesoAces_V();
  end else EdDesoAces_V.Enabled := True;
end;

procedure TFmPediVda.EdFrete_PChange(Sender: TObject);
begin
  if EdFrete_P.ValueVariant <> 0 then
  begin
    EdFrete_V.Enabled := False;
    CalculaFrete_V();
  end else EdFrete_V.Enabled := True;
end;

procedure TFmPediVda.EdRegrFiscalChange(Sender: TObject);
begin
  EdModeloNF.Text := '';
  if not EdRegrFiscal.Focused then
    EdModeloNF.Text := DmPediVda.QrFisRegCadNO_MODELO_NF.Value;
end;

procedure TFmPediVda.EdRegrFiscalExit(Sender: TObject);
begin
  EdModeloNF.Text := DmPediVda.QrFisRegCadNO_MODELO_NF.Value;
end;

procedure TFmPediVda.EdSeguro_PChange(Sender: TObject);
begin
  if EdSeguro_P.ValueVariant <> 0 then
  begin
    EdSeguro_V.Enabled := False;
    CalculaSeguro_V();
  end else EdSeguro_V.Enabled := True;
end;

procedure TFmPediVda.EdTabelaPrcChange(Sender: TObject);
begin
  if LaTipo.SQLType = stIns then
  begin
    EdMoeda.ValueVariant := DmPediVda.QrTabePrcCabMoeda.Value;
    CBMoeda.KeyValue     := DmPediVda.QrTabePrcCabMoeda.Value;
  end;
end;

procedure TFmPediVda.Excluiitemprodutoselecionado1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrCustomizados, DBGrid1, 'pedivdaits',
   ['Controle'], ['Controle'], istPergunta, '');
  DmPediVda.AtzSdosPedido(QrPediVdaCodigo.Value);
  ReopenPediVdaGru(QrPediVdaGruNivel1.Value);
end;

procedure TFmPediVda.Excluipartedoprodutoselecionado1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrPediVdaCuz, DBGrid2, 'pedivdacuz',
   ['Conta'], ['Conta'], istPergunta, '');
  AtualizaItemCustomizado(QrCustomizadosControle.Value);
  DmPediVda.AtzSdosPedido(QrPediVdaCodigo.Value);
end;

procedure TFmPediVda.MostraEdicao(Mostra: Integer; Status: String; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelControle.Visible:=True;
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    else Geral.MensagemBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  LaTipo.Caption := Status;
  GOTOy.BotoesSb(LaTipo.Caption);
end;

procedure TFmPediVda.MostraFmPediVdaCuzParIns(CodUsu, PrdGrupTip, Nivel1, GraGruX, Controle:
  Integer; SQLType: TSQLType;
  QuantP, MedidaC, MedidaL, MedidaA, MedidaE: Double);
var
  Continuar: Boolean;
  Num: String;
begin
  if DBCheck.CriaFm(TFmPediVdaCuzParIns, FmPediVdaCuzParIns, afmoNegarComAviso) then
  begin
    FmPediVdaCuzParIns.LaTipo.SQLType := SQLType;
    //FmPediVdaCuzParIns.FGraGruX := GraGruX;
    FmPediVdaCuzParIns.ReopenGraGruX(GraGruX);
    FmPediVdaCuzParIns.FControle := Controle;
    if CodUsu > 0 then
    begin
      DmPediVda.QrPP.Close;
      DmPediVda.QrPP.Params[0].AsInteger := Nivel1;
      DmPediVda.QrPP.Open;
      //
      FmPediVdaCuzParIns.RGGrupTip.ItemIndex       := 3;
      FmPediVdaCuzParIns.EdMatPartCad.ValueVariant := DmPediVda.QrPPPartePrinc.Value;
      FmPediVdaCuzParIns.CBMatPartCad.KeyValue     := DmPediVda.QrPPPartePrinc.Value;
      FmPediVdaCuzParIns.EdGraGru1.ValueVariant    := CodUsu;
      FmPediVdaCuzParIns.CBGraGru1.KeyValue        := CodUsu;
      FmPediVdaCuzParIns.EdMedidaC.ValueVariant    := MedidaC;
      FmPediVdaCuzParIns.EdMedidaL.ValueVariant    := MedidaL;
      FmPediVdaCuzParIns.EdMedidaA.ValueVariant    := MedidaA;
      FmPediVdaCuzParIns.EdMedidaE.ValueVariant    := MedidaE;
      FmPediVdaCuzParIns.EdQuantP.ValueVariant     := 1; //QuantP;  Deve ser s� um !
      //
      Num := FormatFloat('0', GraGruX);
      {
      for C := 1 to GradeC.ColCount - 1 do
        for R := 1 to GradeC.RowCount do
        begin
          Txt := Geral.SoNumero_TT(GradeC.Cells[C,R]);
          if Txt = Num then
          begin
            FmPediVdaCuzParIns.EdGraGruX.Text := Num;
            Break;
          end;
        end;
      }
      FmPediVdaCuzParIns.EdGraGruX.Text := Num;
      FmPediVdaCuzParIns.FLimitaCorTam := False;
    end else begin
      {
      }
    end;
    DmProd.QrNeed1.Close;
    DmProd.QrNeed1.Params[00].AsInteger := GraGruX;
    DmProd.QrNeed1.Open;
    FmPediVdaCuzParIns.ShowModal;
    Continuar :=
      FmPediVdaCuzParIns.CkContinuar.Checked;
    FmPediVdaCuzParIns.Destroy;
    //
    if Continuar then
      MostraFmPediVdaCuzParIns(0, 0, 0, GraGruX, Controle, SQLType, 0, 0, 0, 0, 0);
  end;
end;

procedure TFmPediVda.MostraPediVdaGru(SQLType: TSQLType);
begin
  DmPediVda.ReopenParamsEmp(QrPediVdaEmpresa.Value, True);
  if DBCheck.CriaFm(TFmPediVdaGru, FmPediVdaGru, afmoNegarComAviso) then
  begin
    FmPediVdaGru.LaTipo.SQLType := SQLType;
    if SQLType = stUpd then
    begin
      FmPediVdaGru.PnSeleciona.Enabled    := False;
      FmPediVdaGru.EdGraGru1.ValueVariant := QrPediVdaGruNivel1.Value;
      FmPediVdaGru.CBGraGru1.KeyValue     := QrPediVdaGruNivel1.Value;
    end;
    FmPediVdaGru.ShowModal;
    FmPediVdaGru.Destroy;
  end;
end;

procedure TFmPediVda.MostraPediVdaLei(SQLType: TSQLType);
begin
  DmPediVda.ReopenParamsEmp(QrPediVdaEmpresa.Value, True);
  if DBCheck.CriaFm(TFmPediVdaLei, FmPediVdaLei, afmoNegarComAviso) then
  begin
    FmPediVdaLei.LaTipo.SQLType := SQLType;
    FmPediVdaLei.ShowModal;
    FmPediVdaLei.Destroy;
    ReopenPediVdaGru(QrPediVdaGruNivel1.Value);
  end;
end;

{
function TFmPediVda.ObtemQtde(var Qtde: Double): Boolean;
var
  ResVar: Variant;
begin
  // N�o pode, j� vem definido
  //Qtde := 0;
  Result := False;
  if MLAGeral.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble,
  0, 3, 0, '', '', True, 'Itens', 'Informe a quantidade de itens: ',
  0, ResVar) then
  begin
    Qtde := Geral.IMV(ResVar);
    Result := True;
  end;
end;
}

procedure TFmPediVda.PMCustomPopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita :=
    (QrCustomizados.State <> dsInactive)
    and
    (QrCustomizados.RecordCount > 0);
  Adicionapartesaoitemselecionado1.Enabled := Habilita;

  //

  Habilita := Habilita and
    (QrCustomizados.State <> dsInactive)
    and
    (QrCustomizados.RecordCount > 0)
    and
    (QrPediVdaCuz.State <> dsInactive)
    and
    (QrPediVdaCuz.RecordCount = 0);
  Excluiitemprodutoselecionado1.Enabled := Habilita;

  //

  Habilita :=
    (QrPediVdaCuz.State <> dsInactive)
    and
    (QrPediVdaCuz.RecordCount > 0);
  Excluipartedoprodutoselecionado1.Enabled := Habilita;
end;

procedure TFmPediVda.PMPedidosPopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := (QrPediVda.State <> dsInactive) and (QrPediVda.RecordCount > 0);
  Alterapedidoatual1.Enabled := Habilita;
  Excluipedidoatual1.Enabled := Habilita;
end;

procedure TFmPediVda.PageControl1Change(Sender: TObject);
begin
  case PageControl1.ActivePageIndex of
    0: BtVisual.Visible := False;
    1: BtVisual.Visible := True;
  end;
end;

procedure TFmPediVda.CalculaDesoAces_V;
begin
  // Parei aqui!!! Falta fazer
end;

procedure TFmPediVda.CalculaFrete_V;
begin
  // Parei aqui!!! Falta fazer
end;

procedure TFmPediVda.CalculaSeguro_V;
begin
  // Parei aqui!!! Falta fazer
end;

procedure TFmPediVda.CBMotivoSitExit(Sender: TObject);
begin
  TPDtaEmiss.SetFocus;
end;

procedure TFmPediVda.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  //Va(vpLast);
end;

procedure TFmPediVda.Custumizao1Click(Sender: TObject);
begin

end;

{
procedure TFmPediVda.AlteraRegistro;
var
  PediVda : Integer;
begin
  PediVda := QrPediVdaCodigo.Value;
  if QrPediVdaCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(PediVda, Dmod.MyDB, 'PediVda', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(PediVda, Dmod.MyDB, 'PediVda', 'Codigo');
      MostraEdicao(1, CO_ALTERACAO, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmPediVda.IncluiRegistro;
var
  Cursor : TCursor;
  PediVda : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    PediVda := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'PediVda', 'PediVda', 'Codigo');
    if Length(FormatFloat(FFormatFloat, PediVda))>Length(FFormatFloat) then
    begin
      Geral.MensagemBox(
      'Inclus�o cancelada. Limite de cadastros extrapolado', 'Erro',
      MB_OK+MB_ICONERROR);
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(1, CO_INCLUSAO, PediVda);
  finally
    Screen.Cursor := Cursor;
  end;
end;
}

procedure TFmPediVda.QueryPrincipalAfterOpen;
begin
end;

procedure TFmPediVda.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmPediVda.BtVisualClick(Sender: TObject);
begin
  if DBGGru.Align = alLeft then
  begin
    FDBGWidth := DBGGru.Width;
    DBGGru.Align := alTop;
    DBGGru.Height := 68; // 20 + 18 + 20
  end else begin
    DBGGru.Align := alLeft;
    DBGGru.Width := FDBGWidth;
  end;
end;

procedure TFmPediVda.BitBtn1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPediVdaImp, FmPediVdaImp, afmoNegarComAviso) then
  begin
    FmPediVdaImp.ShowModal;
    FmPediVdaImp.Destroy;
  end;
end;

procedure TFmPediVda.BtCondicaoPGClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmPediPrzCab, FmPediPrzCab, afmoNegarComAviso) then
  begin
    FmPediPrzCab.ShowModal;
    FmPediPrzCab.Destroy;
    if VAR_CADASTRO <> 0 then
    begin
      DmPediVda.QrPediPrzCab.Close;
      DmPediVda.QrPediPrzCab.Open;
      if DmPediVda.QrPediPrzCab.Locate('Codigo', VAR_CADASTRO, []) then
      begin
        EdCondicaoPg.ValueVariant := VAR_CADASTRO;
        CBCondicaoPg.KeyValue     := VAR_CADASTRO;
      end;
    end;
  end;
end;

procedure TFmPediVda.SpeedButton11Click(Sender: TObject);
begin
  VAR_ENTIDADE := 0;
  if DBCheck.CriaFm(TFmEntidade2, FmEntidade2, afmoNegarComAviso) then
  begin
    FmEntidade2.ShowModal;
    FmEntidade2.Destroy;
    if VAR_ENTIDADE <> 0 then
    begin
      DmPediVda.QrTransportas.Close;
      DmPediVda.QrTransportas.Open;
      if DmPediVda.QrTransportas.Locate('Codigo', VAR_ENTIDADE, []) then
      begin
        EdRedespacho.ValueVariant := VAR_ENTIDADE;
        CBRedespacho.KeyValue     := VAR_ENTIDADE;
      end;
    end;
  end;
end;

procedure TFmPediVda.SpeedButton12Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmPediAcc, FmPediAcc, afmoNegarComAviso) then
  begin
    FmPediAcc.ShowModal;
    FmPediAcc.Destroy;
    if VAR_CADASTRO <> 0 then
    begin
      DmPediVda.QrPediAcc.Close;
      DmPediVda.QrPediAcc.Open;
      //if DmPediVda.QrPediAcc.Locate('Codigo', VAR_CADASTRO, []) then
      //begin
        EdMoeda.ValueVariant := VAR_CADASTRO;
        CBMoeda.KeyValue     := VAR_CADASTRO;
      //end;
    end;
  end;
end;

procedure TFmPediVda.SpeedButton13Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmCarteiras, FmCarteiras, afmoNegarComAviso) then
  begin
    FmCarteiras.ShowModal;
    FmCarteiras.Destroy;
    if VAR_CADASTRO <> 0 then
    begin
      DmPediVda.QrCartEmis.Close;
      DmPediVda.QrCartEmis.Open;
      EdMoeda.ValueVariant := VAR_CADASTRO;
      CBMoeda.KeyValue     := VAR_CADASTRO;
    end;
  end;
end;

procedure TFmPediVda.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmPediVda.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmPediVda.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmPediVda.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmPediVda.SpeedButton5Click(Sender: TObject);
begin
  VAR_ENTIDADE := 0;
  if DBCheck.CriaFm(TFmEntidade2, FmEntidade2, afmoNegarComAviso) then
  begin
    FmEntidade2.ShowModal;
    FmEntidade2.Destroy;
    if VAR_ENTIDADE <> 0 then
    begin
      DmPediVda.QrClientes.Close;
      DmPediVda.QrClientes.Open;
      if DmPediVda.QrClientes.Locate('Codigo', VAR_ENTIDADE, []) then
      begin
        EdCliente.ValueVariant := VAR_ENTIDADE;
        CBCliente.KeyValue     := VAR_ENTIDADE;
      end;
    end;
  end;
end;

procedure TFmPediVda.SpeedButton6Click(Sender: TObject);
begin
  VAR_ENTIDADE := 0;
  if DBCheck.CriaFm(TFmEntidade2, FmEntidade2, afmoNegarComAviso) then
  begin
    FmEntidade2.ShowModal;
    FmEntidade2.Destroy;
    if VAR_ENTIDADE <> 0 then
    begin
      DmPediVda.QrTransportas.Close;
      DmPediVda.QrTransportas.Open;
      if DmPediVda.QrTransportas.Locate('Codigo', VAR_ENTIDADE, []) then
      begin
        EdTransporta.ValueVariant := VAR_ENTIDADE;
        CBTransporta.KeyValue     := VAR_ENTIDADE;
      end;
    end;
  end;
end;

procedure TFmPediVda.SpeedButton7Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmMotivos, FmMotivos, afmoNegarComAviso) then
  begin
    FmMotivos.ShowModal;
    FmMotivos.Destroy;
    if VAR_CADASTRO <> 0 then
    begin
      DmPediVda.QrMotivos.Close;
      DmPediVda.QrMotivos.Open;
      if DmPediVda.QrMotivos.Locate('Codigo', VAR_CADASTRO, []) then
      begin
        EdMotivoSit.ValueVariant := VAR_CADASTRO;
        CBMotivoSit.KeyValue     := VAR_CADASTRO;
      end;
    end;
  end;
end;

procedure TFmPediVda.BtTabelaPrcClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmTabePrcCab, FmTabePrcCab, afmoNegarComAviso) then
  begin
    FmTabePrcCab.ShowModal;
    FmTabePrcCab.Destroy;
    if VAR_CADASTRO <> 0 then
    begin
      DmPediVda.QrTabePrcCab.Close;
      DmPediVda.QrTabePrcCab.Open;
      if DmPediVda.QrTabePrcCab.Locate('Codigo', VAR_CADASTRO, []) then
      begin
        EdTabelaPrc.ValueVariant := VAR_CADASTRO;
        CBTabelaPrc.KeyValue     := VAR_CADASTRO;
      end;
    end;
  end;
end;

procedure TFmPediVda.SpeedButton9Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmCambioMda, FmCambioMda, afmoNegarComAviso) then
  begin
    FmCambioMda.ShowModal;
    FmCambioMda.Destroy;
    if VAR_CADASTRO <> 0 then
    begin
      DModG.QrCambioMda.Close;
      DModG.QrCambioMda.Open;
      if DModG.QrCambioMda.Locate('Codigo', VAR_CADASTRO, []) then
      begin
        EdMoeda.ValueVariant := VAR_CADASTRO;
        CBMoeda.KeyValue     := VAR_CADASTRO;
      end;
    end;
  end;
end;

procedure TFmPediVda.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrPediVdaCodigo.Value;
  Close;
end;

procedure TFmPediVda.Adicionapartesaoitemselecionado1Click(Sender: TObject);
begin
  MostraFmPediVdaCuzParIns(0, 0, 0, QrCustomizadosGraGruX.Value,
    QrCustomizadosControle.Value, stIns, 0, 0, 0, 0, 0);
end;

procedure TFmPediVda.AlteraExcluiIncluiitemselecionado1Click(Sender: TObject);
begin
  //n�o alterar / excluir quando tiver customiza��o
  GradeQDblClick2();
end;

procedure TFmPediVda.Alteraitemprodutoselecionado1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPediVdaCuzUpd, FmPediVdaCuzUpd, afmoNegarComAviso) then
  begin
    FmPediVdaCuzUpd.ReopenPediVdaIts(QrCustomizadosControle.Value);
    FmPediVdaCuzUpd.ShowModal;
    FmPediVdaCuzUpd.Destroy;
  end;
end;

procedure TFmPediVda.Alterapedidoatual1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrPediVda, [PainelDados],
  [PainelEdita], EdEmpresa, LaTipo, 'pedivda');
  DesabilitaComponentes();
end;

procedure TFmPediVda.AtualizaItemCustomizado(Controle: Integer);
var
  PrecoO, PrecoR, PrecoF, ValBru, ValLiq, DescoP, DescoV, PercCustom, QuantP: Double;
  InfAdCuztm, Codigo: Integer;
  Nome: String;
begin
  InfAdCuztm := 0;
  DmPediVda.QrIts.Close;
  DmPediVda.QrIts.Params[0].AsInteger := Controle;
  DmPediVda.QrIts.Open;
  //
  DmPediVda.QrSumCuz.Close;
  DmPediVda.QrSumCuz.Params[0].AsInteger := Controle;
  DmPediVda.QrSumCuz.Open;
  //
  DmPediVda.QrCustomiz.Close;
  DmPediVda.QrCustomiz.Params[0].AsInteger := Controle;
  DmPediVda.QrCustomiz.Open;
  //
  Nome := '';
  while not DmPediVda.QrCustomiz.Eof do
  begin
    Nome := Nome + ' ' + DmPediVda.QrCustomizSiglaCustm.Value;
    DmPediVda.QrCustomiz.Next;
  end;
  Nome := Copy(Trim(Nome), 1, 255);
  if Nome <> '' then
  begin
    DmPediVda.QrPesInfCuz.Close;
    DmPediVda.QrPesInfCuz.Params[0].AsString := Nome;
    DmPediVda.QrPesInfCuz.Open;
    if DmPediVda.QrPesInfCuz.RecordCount > 0 then
      InfAdCuztm := DmPediVda.QrPesInfCuzCodigo.Value
    else begin
      InfAdCuztm := DModG.BuscaProximoCodigoInt('nfectrl', 'nfeinfcuz', '', 0);
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeinfcuz', False, [
      'Nome'], ['Codigo'], [Nome], [InfAdCuztm], True);
    end;
  end;
  {
  ValLiq := QrSumCuzValLiq.Value;
  DescoV := QrSumCuzDescoV.Value;
  ValBru := ValLiq + DescoV;
  //
  }
  QuantP := DmPediVda.QrItsQuantP.Value;
  ValLiq := DmPediVda.QrSumCuzValLiq.Value * QuantP;
  DescoV := DmPediVda.QrSumCuzDescoV.Value * QuantP;
  ValBru := ValLiq + DescoV;
  if DmPediVda.QrItsQuantP.Value = 0 then
    PrecoO := 0
  else
    PrecoO := Round(ValBru / QuantP * 100) / 100;
  PrecoR := PrecoO;
  //
  if ValBru = 0 then
    DescoP := 0
  else
    DescoP := DescoV / ValBru * 100;
  //
  if QuantP > 0 then
    PrecoF := ValLiq / QuantP// * ((100 - DescoP)/100)
  else
    PrecoF := 0;
  PercCustom :=   DmPediVda.QrSumCuzPercCustom.Value;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'pedivdaits', False, [
    'PrecoO', 'PrecoR', 'ValBru', 'DescoP', 'DescoV',
    'ValLiq', 'PrecoF', 'InfAdCuztm', 'PercCustom'
  ], ['Controle'], [
    PrecoO, PrecoR, ValBru, DescoP, DescoV,
    ValLiq, PrecoF, InfAdCuztm, PercCustom
  ], [Controle], True) then
  begin
    Codigo := QrPediVdaCodigo.Value;
    DmPediVda.AtzSdosPedido(Codigo);
    LocCod(Codigo, Codigo);
    QrPediVdaGru.Locate('Nivel1', DmPediVda.QrItsGraGru1.Value, []);
    if QrCustomizados.State <> dsInactive then
      QrCustomizados.Locate('Controle', Controle, []);
  end;
end;

procedure TFmPediVda.AtualizaValoresdoitem1Click(Sender: TObject);
begin
  AtualizaItemCustomizado(QrCustomizadosControle.Value);
end;

procedure TFmPediVda.BtPedidosClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  MyObjects.MostraPopUpDeBotao(PMPedidos, BtPedidos);
end;

procedure TFmPediVda.BtRecalculaClick(Sender: TObject);
begin
  DmPediVda.AtualizaTodosItensPediVda_(QrPediVdaCodigo.Value);
  DmPediVda.AtzSdosPedido(QrPediVdaCodigo.Value);
  LocCod(QrPediVdaCodigo.Value, QrPediVdaCodigo.Value);
end;

procedure TFmPediVda.BtConfirmaClick(Sender: TObject);
  function FaturaParcialIncorreta(var Texto: String): Boolean;
  var
    Valor: Double;
  begin
    if CkAFP_Sit.Checked then
      Valor := EdAFP_Per.ValueVariant
    else
      Valor := 0;
    //
    Result := int(Valor * 10000) <> int(DmPediVda.QrPediPrzCabPercent2.Value * 10000);
    if Result then Texto := FloatToStr(Valor) + ' � diferente de ' +
      FloatToStr(DmPediVda.QrPediPrzCabPercent2.Value)
  end;
var
  Codigo: Integer;
  //CartEmis, RegrFiscal, CondicaoPG, TabelaPrc, Empresa, Cliente: Integer;
  ErroFatura: Boolean;
  Txt: String;
begin
  if MyObjects.FIC(EdEmpresa.ValueVariant = 0, EdEmpresa,
    'Informe a empresa!') then Exit;
  if MyObjects.FIC(EdCliente.ValueVariant = 0, EdCliente,
    'Informe o cliente!') then Exit;
  if MyObjects.FIC(TPDtaPrevi.Date < 2, TPDtaPrevi,
    'Informe a data de previs�o de entrega!') then Exit;
  //if MyObjects.FIC(EdTabelaPrc.ValueVariant = 0, EdTabelaPrc,
    //'Informe a tabela de pre�os!') then Exit;
  if MyObjects.FIC(EdCondicaoPG.ValueVariant = 0, EdCondicaoPG,
    'Informe a condi��o de pagamento!') then Exit;
  if MyObjects.FIC(EdCartEmis.ValueVariant = 0, EdCartEmis,
    'Informe a carteira!') then Exit;
  if MyObjects.FIC(EdRegrFiscal.ValueVariant = 0, EdRegrFiscal,
    'Informe a movimenta��o (fiscal)!') then Exit;
  ErroFatura := FaturaParcialIncorreta(Txt);
  if MyObjects.FIC(ErroFatura, CkAFP_Sit,
    'Fatura parcial n�o permitida!' + #13#10 + Txt) then Exit;
  {
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Geral.MensagemBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  }
  Codigo := UMyMod.BuscaEmLivreY_Def('PediVda', 'Codigo', LaTipo.SQLType,
    QrPediVdaCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(LaTipo.SQLType, FmPediVda, PainelEdita,
    'PediVda', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], LaTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmPediVda.BtCustomClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 2;
  MyObjects.MostraPopUpDeBotao(PMCustom, BtCustom);
end;

procedure TFmPediVda.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if LaTipo.Caption = CO_INCLUSAO then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'PediVda', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'PediVda', 'Codigo');
  MostraEdicao(0, CO_TRAVADO, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'PediVda', 'Codigo');
end;

procedure TFmPediVda.BtItensClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 1;
  MyObjects.MostraPopUpDeBotao(PMItens, BtItens);
end;

procedure TFmPediVda.FormCreate(Sender: TObject);// Adicionado por .DFM > .PAS
CB?.ListSource = DModG.DsEmpresas;

begin
  QrPediVdaNOMESITUACAO.LookupDataSet := DModG.QrSituacao;
  DBEdCidade.DataField := '';
  DBEdNOMEUF.DataField := '';
  //
  PageControl1.ActivePageIndex := 0;
  PageControl2.ActivePageIndex := 0;
  PageControl3.ActivePageIndex := 0;
  PageControl1.Align := alClient;
  PageControl2.Align := alClient;
  //
  TPDtaEmiss.Date := Date;
  TPDtaEntra.Date := Date;
  TPDtaInclu.Date := Date;
  TPDtaPrevi.Date := Date;
  CriaOForm;
  GradeA.ColWidths[0] := 128;
  GradeC.ColWidths[0] := 128;
  GradeX.ColWidths[0] := 128;
  GradeQ.ColWidths[0] := 128;
  GradeF.ColWidths[0] := 128;
  GradeD.ColWidths[0] := 128;
  GradeV.ColWidths[0] := 128;
end;


procedure TFmPediVda.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrPediVdaCodigo.Value, LaRegistro.Caption);
end;

procedure TFmPediVda.SbImprimeClick(Sender: TObject);
begin
  if QrCli.State = dsInactive then
  begin
    QrCli.Close;     
    QrCli.Params[0].AsInteger := QrPediVdaCliente.Value;
    UMyMod.AbreQuery(QrCli, 'TFmPediVda.SbImprimeClick()');
  end;
  DModG.ReopenParamsEmp(QrPediVdaEmpresa.Value);
  DModG.ReopenEndereco(QrPediVdaEmpresa.Value);
  //
  QrItsN.Close;
  QrItsN.Params[0].AsInteger := QrPediVdaCodigo.Value;
  QrItsN.Open;
  //
  QrItsC.Close;
  QrItsC.Params[0].AsInteger := QrPediVdaCodigo.Value;
  QrItsC.Open;
  //
  MyObjects.frxMostra(frxPED_VENDA_001_01, 'Informe do Pedido n� ' + FormatFloat('000000', QrPediVdaCodUsu.Value));
end;

procedure TFmPediVda.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmPediVda.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrPediVdaCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmPediVda.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmPediVda.QrPediVdaAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
  BtItens.Enabled := QrPediVda.RecordCount > 0;
  BtCustom.Enabled := QrPediVda.RecordCount > 0;
end;

procedure TFmPediVda.QrPediVdaAfterScroll(DataSet: TDataSet);
begin
  ReopenPediVdaGru(0);
  DmPediVda.ReopenParamsEmp(QrPediVdaEmpresa.Value, True);
end;

procedure TFmPediVda.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPediVda.SbQueryClick(Sender: TObject);
begin
  LocCod(QrPediVdaCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'PediVda', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmPediVda.SbRegrFiscalClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  FmPrincipal.MostraFisRegCad;
  if VAR_CADASTRO <> 0 then
  begin
    DmPediVda.QrFisRegCad.Close;
    DmPediVda.QrFisRegCad.Open;
    if DmPediVda.QrFisRegCad.Locate('Codigo', VAR_CADASTRO, []) then
    begin
      EdRegrFiscal.ValueVariant := DmPediVda.QrFisRegCadCodUsu.Value;
      CBRegrFiscal.KeyValue     := DmPediVda.QrFisRegCadCodUsu.Value;
    end;
  end;
end;

procedure TFmPediVda.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmPediVda.frxPED_VENDA_001_01GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'MeuLogo3x1Existe' then
    Value := FileExists(DModG.QrParamsEmpLogo3x1.Value)
  else
  if VarName = 'MeuLogo3x1Caminho' then
    Value := DModG.QrParamsEmpLogo3x1.Value
end;

procedure TFmPediVda.GradeADrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeA(GradeA, ACol, ARow, Rect, State, True);
end;

procedure TFmPediVda.GradeCDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeC, GradeA, nil, ACol, ARow, Rect, State,
  '0', 0, 0, siNegativo);
end;

procedure TFmPediVda.GradeDDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeD, GradeA, nil, ACol, ARow, Rect, State,
  Dmod.FStrFmtPrc, 0, 0, siPositivo);
end;

procedure TFmPediVda.GradeFDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeF, GradeA, nil, ACol, ARow, Rect, State,
  Dmod.FStrFmtPrc, 0, 0, siPositivo);
end;

procedure TFmPediVda.GradeQDblClick(Sender: TObject);
begin
  GradeQDblClick2();
end;

procedure TFmPediVda.GradeQDblClick2;
var
  GraGruX: Integer;
begin
  // somente item n�o customizado
  if QrPediVdaGruItensCustomizados.Value = 0 then
  begin
    GraGruX := Geral.IMV(GradeC.Cells[GradeQ.Col, GradeQ.Row]);
    if (QrPediVdaGru.RecordCount > 0) and (GraGruX > 0) and
    (GradeQ.Col > 0) and (GradeQ.Row > 0) then
    begin
      DmPediVda.QrItemPVI.Close;
      DmPediVda.QrItemPVI.Params[00].AsInteger := QrPediVdaCodigo.Value;
      DmPediVda.QrItemPVI.Params[01].AsInteger := GraGruX;
      DmPediVda.QrItemPVI.Open;
      //
      if DmPediVda.QrItemPVI.RecordCount > 0 then
      begin
        UMyMod.FormInsUpd_Show(TFmPediVdaAlt, FmPediVdaAlt, afmoNegarComAviso,
        DmPediVda.QrItemPVI, stUpd);
      end else begin
        UMyMod.FormInsUpd_Show(TFmPediVdaAlt, FmPediVdaAlt, afmoNegarComAviso,
        DmPediVda.QrItemPVI, stIns);
      end;
    end else Geral.MensagemBox('O item n�o pode ser editado!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end else Geral.MensagemBox('Este item � customizado! ' +
   'Para edit�-lo, clique no bot�o pr�prio!', 'Aviso', MB_OK+MB_ICONWARNING);
end;

procedure TFmPediVda.GradeQDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeQ, GradeA, nil, ACol, ARow, Rect, State,
  MLAGeral.FormataCasas(QrPediVdaGruFracio.Value), 0, 0, siPositivo);
end;

procedure TFmPediVda.GradeVDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeV, GradeA, nil, ACol, ARow, Rect, State,
  Dmod.FStrFmtPrc, 0, 0, true);
end;

procedure TFmPediVda.GradeXDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeX, GradeA, nil, ACol, ARow, Rect, State,
  '0', 0, 0, siNegativo);
end;

procedure TFmPediVda.IncluinovoitemprodutoCustomizvel1Click(Sender: TObject);
begin
  if DmProd.ImpedePeloPrazoMedio(QrPediVdaTabelaPrc.Value,
    QrPediVdaMedDDSimpl.Value, QrPediVdaMedDDReal.Value) then Exit;
  if DBCheck.CriaFm(TFmPediVdaCuzIns, FmPediVdaCuzIns, afmoNegarComAviso) then
  begin
    FmPediVdaCuzIns.ShowModal;
    FmPediVdaCuzIns.Destroy;
  end;
end;

procedure TFmPediVda.Incluinovopedido1Click(Sender: TObject);
begin
  if not DModG.SoUmaEmpresaLogada(True) then Exit;
  //
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdita, QrPediVda, [PainelDados],
  [PainelEdita], EdEmpresa, LaTipo, 'pedivda');
  DmPediVda.ReopenParamsEmp(DmodG.QrFiliLogFilial.Value, True);
  EdSituacao.ValueVariant := DmPediVda.QrParamsEmpSituacao.Value;//2; // Liberado
  CBSituacao.KeyValue     := DmPediVda.QrParamsEmpSituacao.Value; // Liberado
  EdEmpresa.ValueVariant  := DmodG.QrFiliLogFilial.Value;
  CBEmpresa.KeyValue      := DmodG.QrFiliLogFilial.Value;
  DesabilitaComponentes();
  TPDtaEmiss.Date := 0;
  TPDtaEntra.Date := 0;
  TPDtaInclu.Date := Date;
  TPDtaPrevi.Date := 0;
  EdCodUsu.SetFocus;
end;

procedure TFmPediVda.Incluinovositensdegrupo1Click(Sender: TObject);
begin
  MostraPediVdaGru(stIns);
end;

procedure TFmPediVda.IncluinovositensporLeitura1Click(Sender: TObject);
begin
  MostraPediVdaLei(stIns);
end;

procedure TFmPediVda.QrPediVdaBeforeClose(DataSet: TDataSet);
begin
  BtItens.Enabled := False;
  BtCustom.Enabled := False;
  QrPediVdaGru.Close;
end;

procedure TFmPediVda.QrPediVdaBeforeOpen(DataSet: TDataSet);
begin
  QrPediVdaCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmPediVda.QrPediVdaCalcFields(DataSet: TDataSet);
begin
  QrPediVdaNOMEFRETEPOR.Value := MLAGeral.FretePor_Txt(QrPediVdaFretePor.Value);
end;

procedure TFmPediVda.QrPediVdaCuzCalcFields(DataSet: TDataSet);
begin
  if QrPediVdaCuzTipDimens.Value < 0 then
    QrPediVdaCuzMedidaC_TXT.Value := ''
  else
    QrPediVdaCuzMedidaC_TXT.Value := Geral.FFT(QrPediVdaCuzMedidaC.Value, 2, siPositivo);

  //

  if QrPediVdaCuzTipDimens.Value < 1 then
    QrPediVdaCuzMedidaL_TXT.Value := ''
  else
    QrPediVdaCuzMedidaL_TXT.Value := Geral.FFT(QrPediVdaCuzMedidaL.Value, 2, siPositivo);

  //

  if QrPediVdaCuzTipDimens.Value < 2 then
    QrPediVdaCuzMedidaA_TXT.Value := ''
  else
    QrPediVdaCuzMedidaA_TXT.Value := Geral.FFT(QrPediVdaCuzMedidaA.Value, 2, siPositivo);

  //

  if QrPediVdaCuzTipDimens.Value < 3 then
    QrPediVdaCuzMedidaE_TXT.Value := ''
  else
    QrPediVdaCuzMedidaE_TXT.Value := Geral.FFT(QrPediVdaCuzMedidaE.Value, 2, siPositivo);

  //

end;

procedure TFmPediVda.QrCliCalcFields(DataSet: TDataSet);
begin
  QrCliTE1_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrCliTe1.Value);
  QrCliFAX_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrCliFax.Value);
  QrCliCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrCliCNPJ_CPF.Value);
  QrCliIE_TXT.Value :=
    Geral.Formata_IE(QrCliIE_RG.Value, QrCliUF.Value, '??', QrCliTipo.Value);
  QrCliNUMERO_TXT.Value :=
    MLAGeral.FormataNumeroDeRua(QrCliNumero.Value, False);
  //
  QrCliE_ALL.Value := Uppercase(QrCliNOMELOGRAD.Value);
  if Trim(QrCliE_ALL.Value) <> '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' ';
  QrCliE_ALL.Value := QrCliE_ALL.Value + Uppercase(QrCliRua.Value);
  if Trim(QrCliRua.Value) <> '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ', ' + QrCliNUMERO_TXT.Value;
  if Trim(QrCliCompl.Value) <>  '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' ' + Uppercase(QrCliCompl.Value);
  if Trim(QrCliBairro.Value) <>  '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' - ' + Uppercase(QrCliBairro.Value);
  if QrCliCEP.Value > 0 then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' CEP ' +Geral.FormataCEP_NT(QrCliCEP.Value);
  if Trim(QrCliCidade.Value) <>  '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' - ' + Uppercase(QrCliCidade.Value);
  if Trim(QrCliNOMEUF.Value) <>  '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ', ' + QrCliNOMEUF.Value;
  if Trim(QrCliPais.Value) <>  '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' - ' + QrCliPais.Value;
  //
  //QrCliCEP_TXT.Value :=Geral.FormataCEP_NT(QrCliCEP.Value);
  //
end;

procedure TFmPediVda.QrCustomizadosAfterScroll(DataSet: TDataSet);
begin
  ReopenPediVdaCuz(0);
end;

procedure TFmPediVda.QrCustomizadosBeforeClose(DataSet: TDataSet);
begin
  QrPediVdaCuz.Close;
end;

procedure TFmPediVda.QrItsCAfterScroll(DataSet: TDataSet);
begin
  QrItsZ.Close;
  QrItsZ.Params[0].AsInteger := QrItsCControle.Value;
  QrItsZ.Open;
end;

procedure TFmPediVda.QrItsCCalcFields(DataSet: TDataSet);
var
  Txt: String;
begin
  Txt := '';
  if QrItsCTipDimens.Value >= 0 then
  begin
    if QrItsCMedida1.Value <> '' then
      Txt := Txt + QrItsCMedida1.Value + ' ';
    Txt := Txt + Geral.FFT_MinMaxCasas(QrItsCMedidaC.Value, 2, 4) + ' ';
    if QrItsCSigla1.Value <> '' then
      Txt := Txt + QrItsCSigla1.Value + ' ';
  end;
  //
  if QrItsCTipDimens.Value >= 1 then
  begin
    if QrItsCMedida2.Value <> '' then
      Txt := Txt + QrItsCMedida2.Value + ' ';
    Txt := Txt + Geral.FFT_MinMaxCasas(QrItsCMedidaL.Value, 2, 4) + ' ';
    if QrItsCSigla2.Value <> '' then
      Txt := Txt + QrItsCSigla2.Value + ' ';
  end;
  //
  if QrItsCTipDimens.Value >= 2 then
  begin
    if QrItsCMedida3.Value <> '' then
      Txt := Txt + QrItsCMedida3.Value + ' ';
    Txt := Txt + Geral.FFT_MinMaxCasas(QrItsCMedidaA.Value, 2, 4) + ' ';
    if QrItsCSigla3.Value <> '' then
      Txt := Txt + QrItsCSigla3.Value + ' ';
  end;
  //
  if QrItsCTipDimens.Value >= 3 then
  begin
    if QrItsCMedida4.Value <> '' then
      Txt := Txt + QrItsCMedida4.Value + ' ';
    Txt := Txt + Geral.FFT_MinMaxCasas(QrItsCMedidaE.Value, 3, 6) + ' ';
    if QrItsCSigla4.Value <> '' then
      Txt := Txt + QrItsCSigla4.Value + ' ';
  end;
  //
  QrItsCDESCRICAO.Value := Txt;
end;

procedure TFmPediVda.QrItsZCalcFields(DataSet: TDataSet);
var
  Txt: String;
begin
  Txt := '';
  if QrItsZTipDimens.Value >= 0 then
  begin
    if QrItsZMedida1.Value <> '' then
      Txt := Txt + QrItsZMedida1.Value + ' ';
    Txt := Txt + Geral.FFT_MinMaxCasas(QrItsZMedidaC.Value, 2, 4) + ' ';
    if QrItsZSigla1.Value <> '' then
      Txt := Txt + QrItsZSigla1.Value + ' ';
  end;
  //
  if QrItsZTipDimens.Value >= 1 then
  begin
    if QrItsZMedida2.Value <> '' then
      Txt := Txt + QrItsZMedida2.Value + ' ';
    Txt := Txt + Geral.FFT_MinMaxCasas(QrItsZMedidaL.Value, 2, 4) + ' ';
    if QrItsZSigla2.Value <> '' then
      Txt := Txt + QrItsZSigla2.Value + ' ';
  end;
  //
  if QrItsZTipDimens.Value >= 2 then
  begin
    if QrItsZMedida3.Value <> '' then
      Txt := Txt + QrItsZMedida3.Value + ' ';
    Txt := Txt + Geral.FFT_MinMaxCasas(QrItsZMedidaA.Value, 2, 4) + ' ';
    if QrItsZSigla3.Value <> '' then
      Txt := Txt + QrItsZSigla3.Value + ' ';
  end;
  //
  if QrItsZTipDimens.Value >= 3 then
  begin
    if QrItsZMedida4.Value <> '' then
      Txt := Txt + QrItsZMedida4.Value + ' ';
    Txt := Txt + Geral.FFT_MinMaxCasas(QrItsZMedidaE.Value, 3, 6) + ' ';
    if QrItsZSigla4.Value <> '' then
      Txt := Txt + QrItsZSigla4.Value + ' ';
  end;
  //
  QrItsZDESCRICAO.Value := Txt;
end;

procedure TFmPediVda.QrPediVdaGruAfterOpen(DataSet: TDataSet);
begin
  StaticText1.Visible := QrPediVdaGru.RecordCount > 0;
end;

procedure TFmPediVda.QrPediVdaGruAfterScroll(DataSet: TDataSet);
var
  Grade, Nivel1, Pedido: Integer;
begin
  Grade      := QrPediVdaGruGRATAMCAD.Value;
  Nivel1     := QrPediVdaGruNivel1.Value;
  Pedido     := QrPediVdaCodigo.Value;
  //
  DmProd.ConfigGrades6(Grade, Nivel1, Pedido,
  GradeA, GradeX, GradeC, GradeQ, GradeF, GradeD, GradeV);
  //
  QrCustomizados.Close;
  // S� se tiver itens customizados para n�o perder tempo abrindo tabela a toa
  if QrPediVdaGruItensCustomizados.Value > 0 then
  begin
    QrCustomizados.Params[00].AsInteger := Pedido;
    QrCustomizados.Params[01].AsInteger := Nivel1;
    QrCustomizados.Open;
  end;
end;

procedure TFmPediVda.QrPediVdaGruBeforeClose(DataSet: TDataSet);
begin
  MLAGeral.LimpaGrades([GradeQ, GradeF, GradeD, GradeV, GradeC, GradeA, GradeX],
    0, 0, siNegativo);
  QrCustomizados.Close;
end;

procedure TFmPediVda.ReopenPediVdaCuz(Conta: Integer);
begin
  QrPediVdaCuz.Close;
  QrPediVdaCuz.Params[0].AsInteger := QrCustomizadosControle.Value;
  QrPediVdaCuz.Open;
  //
  if Conta <> 0 then
    QrPediVdaCuz.Locate('Conta', Conta, []);
end;

procedure TFmPediVda.ReopenPediVdaGru(Nivel1: Integer);
begin
  QrPediVdaGru.Close;
  QrPediVdaGru.Params[0].AsInteger := QrPediVdaCodigo.Value;
  QrPediVdaGru.Open;
  //
  if Nivel1 <> 0 then
  begin
    DmPediVda.QrLocNiv1.Close;
    DmPediVda.QrLocNiv1.Params[0].AsInteger := Nivel1;
    DmPediVda.QrLocNiv1.Open;
    //
    QrPediVdaGru.Locate('Nivel1', DmPediVda.QrLocNiv1GraGru1.Value, []);
  end;
end;

{
configurar DBGrid2
Exlui parte
altera parte
altera item customizado
}
end.

