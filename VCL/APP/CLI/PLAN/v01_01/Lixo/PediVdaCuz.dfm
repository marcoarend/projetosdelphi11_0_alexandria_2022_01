object FmPediVdaCuz: TFmPediVdaCuz
  Left = 339
  Top = 185
  Caption = 'PED-VENDA-007 :: Inclus'#227'o de Parte de Item Customizado'
  ClientHeight = 559
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 511
    Width = 1008
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      Enabled = False
      TabOrder = 0
      OnClick = BtOKClick
      NumGlyphs = 2
    end
    object Panel2: TPanel
      Left = 896
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    Caption = 'Inclus'#227'o de Parte de Item Customizado'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 924
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 58
      ExplicitTop = 34
      ExplicitWidth = 1004
      ExplicitHeight = 44
    end
    object LaTipo: TdmkLabel
      Left = 925
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 603
      ExplicitTop = 2
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 463
    Align = alClient
    TabOrder = 0
    object Panel3: TPanel
      Left = 1
      Top = 25
      Width = 1006
      Height = 64
      Align = alTop
      TabOrder = 0
      object GroupBox2: TGroupBox
        Left = 285
        Top = 1
        Width = 539
        Height = 62
        Align = alClient
        Caption = ' Dados de pesquisa: '
        TabOrder = 1
        object Label2: TLabel
          Left = 12
          Top = 16
          Width = 88
          Height = 13
          Caption = 'Parte (Obrigat'#243'rio):'
        end
        object SpeedButton1: TSpeedButton
          Left = 512
          Top = 32
          Width = 21
          Height = 21
          Caption = '...'
        end
        object EdMatPartCad: TdmkEditCB
          Left = 12
          Top = 32
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          OnChange = EdMatPartCadChange
          OnEnter = EdMatPartCadEnter
          OnExit = EdMatPartCadExit
          DBLookupComboBox = CBMatPartCad
        end
        object CBMatPartCad: TdmkDBLookupComboBox
          Left = 68
          Top = 32
          Width = 445
          Height = 21
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DsMatPartCad
          TabOrder = 1
          dmkEditCB = EdMatPartCad
          UpdType = utYes
        end
      end
      object RGGrupTip: TRadioGroup
        Left = 824
        Top = 1
        Width = 181
        Height = 62
        Align = alRight
        Caption = ' Tipo de grupo de produtos: '
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'Nenhum'
          'Produto'
          'Mat'#233'ria-prima'
          'Ambos')
        TabOrder = 2
        OnClick = RGGrupTipClick
      end
      object GroupBox7: TGroupBox
        Left = 1
        Top = 1
        Width = 284
        Height = 62
        Align = alLeft
        Caption = ' Dados do produto endo customizado: '
        TabOrder = 0
        object Label11: TLabel
          Left = 8
          Top = 16
          Width = 56
          Height = 13
          Caption = 'Grupo prod:'
        end
        object Label12: TLabel
          Left = 76
          Top = 16
          Width = 52
          Height = 13
          Caption = 'Item gama:'
        end
        object Label10: TLabel
          Left = 144
          Top = 16
          Width = 53
          Height = 13
          Caption = 'Item grade:'
        end
        object Label1: TLabel
          Left = 212
          Top = 16
          Width = 48
          Height = 13
          Caption = 'Reduzido:'
        end
        object DBEdit2: TDBEdit
          Left = 8
          Top = 32
          Width = 64
          Height = 21
          TabStop = False
          DataField = 'GraGru1'
          DataSource = DsGraGrux
          TabOrder = 0
        end
        object DBEdit1: TDBEdit
          Left = 76
          Top = 32
          Width = 64
          Height = 21
          TabStop = False
          DataField = 'GraGruC'
          DataSource = DsGraGrux
          TabOrder = 1
        end
        object DBEdit3: TDBEdit
          Left = 144
          Top = 32
          Width = 64
          Height = 21
          TabStop = False
          DataField = 'GraTamI'
          DataSource = DsGraGrux
          TabOrder = 2
        end
        object DBEdit4: TDBEdit
          Left = 212
          Top = 32
          Width = 64
          Height = 21
          TabStop = False
          DataField = 'Controle'
          DataSource = DsGraGrux
          TabOrder = 3
        end
      end
    end
    object PnDados: TPanel
      Left = 1
      Top = 89
      Width = 1006
      Height = 373
      Align = alClient
      TabOrder = 1
      Visible = False
      object GroupBox3: TGroupBox
        Left = 1
        Top = 1
        Width = 1004
        Height = 45
        Align = alTop
        Caption = ' Grupo de produtos: '
        TabOrder = 0
        object Label4: TLabel
          Left = 12
          Top = 20
          Width = 23
          Height = 13
          Caption = 'Fltro:'
        end
        object Label5: TLabel
          Left = 348
          Top = 20
          Width = 32
          Height = 13
          Caption = 'Grupo:'
        end
        object EdPesqGraGru1: TEdit
          Left = 40
          Top = 16
          Width = 305
          Height = 21
          TabOrder = 0
          OnExit = EdPesqGraGru1Exit
        end
        object CBGraGru1: TdmkDBLookupComboBox
          Left = 440
          Top = 16
          Width = 557
          Height = 21
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DsGraGru1
          TabOrder = 2
          dmkEditCB = EdGraGru1
          UpdType = utYes
        end
        object EdGraGru1: TdmkEditCB
          Left = 384
          Top = 16
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          OnChange = EdGraGru1Change
          DBLookupComboBox = CBGraGru1
        end
      end
      object GroupBox4: TGroupBox
        Left = 1
        Top = 89
        Width = 1004
        Height = 24
        Align = alTop
        Caption = ' Atributos de produtos: '
        TabOrder = 2
      end
      object GradeC: TStringGrid
        Left = 1
        Top = 113
        Width = 1004
        Height = 135
        Align = alClient
        ColCount = 2
        DefaultColWidth = 65
        DefaultRowHeight = 18
        FixedCols = 0
        RowCount = 2
        FixedRows = 0
        TabOrder = 3
        OnClick = GradeCClick
        OnDrawCell = GradeCDrawCell
        RowHeights = (
          18
          18)
      end
      object Panel4: TPanel
        Left = 1
        Top = 248
        Width = 1004
        Height = 60
        Align = alBottom
        TabOrder = 4
        object GroupBox1: TGroupBox
          Left = 269
          Top = 1
          Width = 428
          Height = 58
          Align = alLeft
          Caption = ' Medidas do produto (pe'#231'as, kg, litros, metros, etc.): '
          TabOrder = 1
          object DBText1: TDBText
            Left = 8
            Top = 16
            Width = 100
            Height = 13
            DataField = 'Medida1'
            DataSource = DsGraGru1
          end
          object DBText2: TDBText
            Left = 112
            Top = 16
            Width = 100
            Height = 13
            DataField = 'Medida2'
            DataSource = DsGraGru1
          end
          object DBText3: TDBText
            Left = 216
            Top = 16
            Width = 100
            Height = 13
            DataField = 'Medida3'
            DataSource = DsGraGru1
          end
          object DBText4: TDBText
            Left = 320
            Top = 16
            Width = 42
            Height = 13
            AutoSize = True
            DataField = 'Medida4'
            DataSource = DsGraGru1
          end
          object EdMedidaC: TdmkEdit
            Left = 8
            Top = 32
            Width = 100
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            OnChange = EdMedidaCChange
          end
          object EdMedidaL: TdmkEdit
            Left = 112
            Top = 32
            Width = 100
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            OnChange = EdMedidaLChange
          end
          object EdMedidaA: TdmkEdit
            Left = 216
            Top = 32
            Width = 100
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            OnChange = EdMedidaAChange
          end
          object EdMedidaE: TdmkEdit
            Left = 320
            Top = 32
            Width = 100
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 6
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            OnChange = EdMedidaEChange
          end
        end
        object GroupBox8: TGroupBox
          Left = 1
          Top = 1
          Width = 268
          Height = 58
          Align = alLeft
          Caption = ' Dados do item (parte) selecionado: '
          TabOrder = 0
          object Label14: TLabel
            Left = 8
            Top = 16
            Width = 48
            Height = 13
            Caption = 'Reduzido:'
          end
          object Label15: TLabel
            Left = 68
            Top = 16
            Width = 58
            Height = 13
            Caption = 'Quantidade:'
          end
          object Label16: TLabel
            Left = 132
            Top = 16
            Width = 59
            Height = 13
            Caption = '% Customiz.:'
          end
          object Label18: TLabel
            Left = 196
            Top = 16
            Width = 60
            Height = 13
            Caption = '% Desconto:'
          end
          object EdGraGruX: TdmkEdit
            Left = 8
            Top = 32
            Width = 56
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            OnChange = EdGraGruXChange
          end
          object EdQuantP: TdmkEdit
            Left = 68
            Top = 32
            Width = 61
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            OnChange = EdQuantPChange
          end
          object EdPerCustom: TdmkEdit
            Left = 132
            Top = 32
            Width = 61
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            OnChange = EdPerCustomChange
            OnEnter = EdPerCustomEnter
          end
          object EdDescoP: TdmkEdit
            Left = 196
            Top = 32
            Width = 61
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            OnChange = EdDescoPChange
          end
        end
      end
      object GradeA: TStringGrid
        Left = 168
        Top = 120
        Width = 225
        Height = 137
        ColCount = 2
        DefaultColWidth = 65
        DefaultRowHeight = 18
        FixedCols = 0
        RowCount = 2
        FixedRows = 0
        TabOrder = 5
        Visible = False
        RowHeights = (
          18
          18)
      end
      object GradeX: TStringGrid
        Left = 396
        Top = 120
        Width = 205
        Height = 137
        ColCount = 2
        DefaultRowHeight = 18
        FixedCols = 0
        RowCount = 2
        FixedRows = 0
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goEditing]
        TabOrder = 6
        Visible = False
        RowHeights = (
          18
          18)
      end
      object RGTipDimens: TDBRadioGroup
        Left = 1
        Top = 46
        Width = 1004
        Height = 43
        Align = alTop
        Caption = ' Mensuramento: '
        Columns = 4
        DataField = 'TipDimens'
        DataSource = DsGraGru1
        Enabled = False
        Items.Strings = (
          'Unimensur'#225'vel  -  pe'#231'a, kg, metros, litros, etc.'
          'Bimensur'#225'vell  -  '#193'rea (m'#178')'
          'Trimensur'#225'vel - Vol. (Compr. x largura x altura)'
          'Quadrimensur'#225'vel - Volume x peso espec'#237'fico')
        ParentBackground = True
        TabOrder = 1
        Values.Strings = (
          '0'
          '1'
          '2'
          '3')
        OnChange = RGTipDimensChange
      end
      object Panel5: TPanel
        Left = 1
        Top = 308
        Width = 1004
        Height = 64
        Align = alBottom
        TabOrder = 7
        object GroupBox6: TGroupBox
          Left = 1
          Top = 1
          Width = 240
          Height = 62
          Align = alLeft
          Caption = ' M'#237'nimos e m'#225'ximos: '
          TabOrder = 0
          object Label3: TLabel
            Left = 156
            Top = 16
            Width = 69
            Height = 13
            Caption = 'Desc. m'#225'ximo:'
            FocusControl = DBEdit7
          end
          object Label9: TLabel
            Left = 9
            Top = 16
            Width = 137
            Height = 13
            Caption = '% m'#237'n. e m'#225'x. customiza'#231#227'o:'
            FocusControl = DBEdit5
          end
          object DBEdit5: TDBEdit
            Left = 9
            Top = 32
            Width = 72
            Height = 21
            TabStop = False
            DataField = 'PerCuztMin'
            DataSource = DsGraGru1
            TabOrder = 0
            OnChange = DBEdit5Change
          end
          object DBEdit6: TDBEdit
            Left = 81
            Top = 32
            Width = 72
            Height = 21
            TabStop = False
            DataField = 'PerCuztMax'
            DataSource = DsGraGru1
            TabOrder = 1
            OnChange = DBEdit6Change
          end
          object DBEdit7: TDBEdit
            Left = 156
            Top = 32
            Width = 72
            Height = 21
            TabStop = False
            DataField = 'DescoMax'
            DataSource = FmPediVda.DsPediVda
            TabOrder = 2
            OnChange = DBEdit7Change
          end
        end
        object GroupBox9: TGroupBox
          Left = 241
          Top = 1
          Width = 752
          Height = 62
          Align = alLeft
          Caption = ' C'#225'lculos: '
          TabOrder = 1
          object Label8: TLabel
            Left = 404
            Top = 16
            Width = 58
            Height = 13
            Caption = '$ total geral:'
          end
          object Label7: TLabel
            Left = 304
            Top = 16
            Width = 83
            Height = 13
            Caption = '$ total deste item:'
          end
          object Label6: TLabel
            Left = 108
            Top = 16
            Width = 77
            Height = 13
            Caption = '$ Pre'#231'o unit'#225'rio:'
          end
          object Label13: TLabel
            Left = 8
            Top = 16
            Width = 25
            Height = 13
            Caption = #193'rea:'
          end
          object Label17: TLabel
            Left = 204
            Top = 16
            Width = 73
            Height = 13
            Caption = '$ Valor unit'#225'rio:'
          end
          object EdPrecoF: TdmkEdit
            Left = 208
            Top = 32
            Width = 92
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object dmkEdit3: TdmkEdit
            Left = 404
            Top = 32
            Width = 96
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 4
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object EdValLiq: TdmkEdit
            Left = 304
            Top = 32
            Width = 96
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            OnChange = EdValLiqChange
          end
          object EdQuantX: TdmkEdit
            Left = 8
            Top = 32
            Width = 96
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object EdPrecoO: TdmkEdit
            Left = 108
            Top = 32
            Width = 96
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            OnChange = EdPrecoOChange
          end
        end
      end
    end
    object Panel6: TPanel
      Left = 1
      Top = 1
      Width = 1006
      Height = 24
      Align = alTop
      Caption = 
        'ATEN'#199#195'O: Os dados aqui informados ser'#227'o multiplicados pelo quant' +
        'idade de pe'#231'as informadas no pedido do item.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
    end
  end
  object QrMatPartCad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM matpartcad'
      'ORDER BY Nome')
    Left = 4
    Top = 4
    object QrMatPartCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMatPartCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrMatPartCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsMatPartCad: TDataSource
    DataSet = QrMatPartCad
    Left = 32
    Top = 4
  end
  object QrGraGru1: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrGraGru1AfterOpen
    BeforeClose = QrGraGru1BeforeClose
    SQL.Strings = (
      'SELECT gg1.Nivel1, gg1.CodUsu, gg1.Nome, gg1.GraTamCad, '
      'gg1.TipDimens, gg1.PerCuztMin, gg1.PerCuztMax,'
      'mor.Medida1, mor.Medida2, mor.Medida3, mor.Medida4'
      'FROM gragru1 gg1'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'LEFT JOIN medordem mor ON mor.Codigo=gg1.MedOrdem'
      'WHERE pgt.TipPrd in (:P0,:P1)'
      'AND gg1.Nome LIKE :P2'
      'ORDER BY gg1.Nome')
    Left = 64
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrGraGru1Nivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrGraGru1CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraGru1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrGraGru1GraTamCad: TIntegerField
      FieldName = 'GraTamCad'
    end
    object QrGraGru1TipDimens: TSmallintField
      FieldName = 'TipDimens'
    end
    object QrGraGru1Medida1: TWideStringField
      FieldName = 'Medida1'
    end
    object QrGraGru1Medida2: TWideStringField
      FieldName = 'Medida2'
    end
    object QrGraGru1Medida3: TWideStringField
      FieldName = 'Medida3'
    end
    object QrGraGru1Medida4: TWideStringField
      FieldName = 'Medida4'
    end
    object QrGraGru1PerCuztMin: TFloatField
      FieldName = 'PerCuztMin'
    end
    object QrGraGru1PerCuztMax: TFloatField
      FieldName = 'PerCuztMax'
    end
  end
  object DsGraGru1: TDataSource
    DataSet = QrGraGru1
    Left = 92
    Top = 4
  end
  object QrGraGruX: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM gragrux'
      'WHERE Controle=:P0')
    Left = 124
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraGruXGraGruC: TIntegerField
      FieldName = 'GraGruC'
    end
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXGraTamI: TIntegerField
      FieldName = 'GraTamI'
    end
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object DsGraGrux: TDataSource
    DataSet = QrGraGruX
    Left = 152
    Top = 4
  end
end
