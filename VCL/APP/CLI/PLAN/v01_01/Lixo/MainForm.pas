unit MainForm;

{
  This code accompanies the BDN Article:

      Using the TDockTabSet component by Jeremy North


}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Tabs, DockTabSet, ExtCtrls, StdCtrls;

type
  TfrmMain = class(TForm)
    DockTabSet1: TDockTabSet;
    Panel1: TPanel;
    pDockLeft: TPanel;
    Splitter1: TSplitter;
    Memo1: TMemo;
    Button1: TButton;
    procedure DockTabSet1TabRemoved(Sender: TObject);
    procedure DockTabSet1DockDrop(Sender: TObject; Source: TDragDockObject; X, Y: Integer);
    procedure pDockLeftDockOver(Sender: TObject; Source: TDragDockObject; X, Y: Integer; State: TDragState;
      var Accept: Boolean);
    procedure pDockLeftUnDock(Sender: TObject; Client: TControl; NewTarget: TWinControl; var Allow: Boolean);
    procedure pDockLeftDockDrop(Sender: TObject; Source: TDragDockObject; X, Y: Integer);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

uses DockForm;

{$R *.dfm}

procedure TfrmMain.Button1Click(Sender: TObject);
var
  i: Integer;
begin
  // close all previously dockable forms before creating
  for i := 0 to Screen.FormCount - 1 do
    if Screen.Forms[i] is TfrmDock then
      Screen.Forms[i].Close;
  // dock to the component called pDockLeft
  TfrmDock.CreateDockForm(clBlue).ManualDock(pDockLeft);
  // dock to the top on the pDockLeft panel
  TfrmDock.CreateDockForm(clGreen).ManualDock(pDockLeft, nil, alTop);
  // dock to the right on the pDockLeft panel
  TfrmDock.CreateDockForm(clRed).ManualDock(pDockLeft, nil, alRight);
  // dock directly to the DockTabSet
  TfrmDock.CreateDockForm(clWhite).ManualDock(DockTabSet1);
end;

procedure TfrmMain.DockTabSet1DockDrop(Sender: TObject; Source: TDragDockObject; X, Y: Integer);
begin
  DockTabSet1.Visible := True;
end;

procedure TfrmMain.DockTabSet1TabRemoved(Sender: TObject);
begin
  DockTabSet1.Visible := DockTabSet1.Tabs.Count > 0;
end;

procedure TfrmMain.pDockLeftDockDrop(Sender: TObject; Source: TDragDockObject; X, Y: Integer);
begin
  if pDockLeft.Width = 0 then
    pDockLeft.Width := 150;
  Splitter1.Visible := True;
  Splitter1.Left := pDockLeft.Width;
end;

procedure TfrmMain.pDockLeftDockOver(Sender: TObject; Source: TDragDockObject; X, Y: Integer; State: TDragState;
  var Accept: Boolean);
var
  lRect: TRect;
begin
  Accept := Source.Control is TfrmDock;
  if Accept then
  begin
    lRect.TopLeft := pDockLeft.ClientToScreen(Point(0, 0));
    lRect.BottomRight := pDockLeft.ClientToScreen(Point(150, pDockLeft.Height));
    Source.DockRect := lRect;
  end;
end;

procedure TfrmMain.pDockLeftUnDock(Sender: TObject; Client: TControl; NewTarget: TWinControl; var Allow: Boolean);
begin
  if pDockLeft.DockClientCount = 1 then
  begin
    pDockLeft.Width := 0;
    Splitter1.Visible := False;
  end;
end;

end.
