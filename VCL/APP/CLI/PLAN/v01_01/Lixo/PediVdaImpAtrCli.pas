unit PediVdaImpAtrCli;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DB, ABSMain, DBGrids,
  dmkDBGrid, DBCtrls, mySQLDbTables, Menus, ComCtrls, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB;

type
  THackDBGrid = class(TDBGrid);
  TFmPediVdaImpAtrCli = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    BtNenhum: TBitBtn;
    BtTodos: TBitBtn;
    BtPesq: TBitBtn;
    PMPesq: TPopupMenu;
    Ativa1: TMenuItem;
    Desativa1: TMenuItem;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel3: TPanel;
    Label1: TLabel;
    EdPesq: TEdit;
    DBGrid1: TdmkDBGrid;
    Panel4: TPanel;
    DBGrid2: TdmkDBGrid;
    RGSelecao: TRadioGroup;
    Panel5: TPanel;
    LaPVIAtrCli: TLabel;
    EdPVIAtrCli: TdmkEditCB;
    CBPVIAtrCli: TdmkDBLookupComboBox;
    procedure FormCreate(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure FormStartDock(Sender: TObject; var DragObject: TDragDockObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure EdPesqChange(Sender: TObject);
    procedure FormUnDock(Sender: TObject; Client: TControl;
      NewTarget: TWinControl; var Allow: Boolean);
    procedure BtNenhumClick(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtPesqClick(Sender: TObject);
    procedure Ativa1Click(Sender: TObject);
    procedure Desativa1Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure RGSelecaoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EdPVIAtrCliEnter(Sender: TObject);
    procedure EdPVIAtrCliExit(Sender: TObject);
    procedure EdPVIAtrCliChange(Sender: TObject);
  private
    { Private declarations }
    FItem: Integer;
    procedure RecarregaItens();
    procedure Pesquisar();
    procedure AtivaItens(Status: Integer);
    procedure AtivaSelecionados(Status: Integer);
  public
    { Public declarations }
    class function CreateDockForm(const aColor: TColor): TCustomForm;
    //class procedure FiltaAtivos();
  end;

var
  FmPediVdaImpAtrCli: TFmPediVdaImpAtrCli;

implementation

uses UnMyObjects, Module, UMySQLModule, TabePrcCab, PediVdaImp, ModPediVda;

{$R *.DFM}

///// PROCEDURES E FUNCTIONS DO DOCKFORM

procedure TFmPediVdaImpAtrCli.Ativa1Click(Sender: TObject);
begin
  AtivaSelecionados(1);
end;

procedure TFmPediVdaImpAtrCli.AtivaItens(Status: Integer);
begin
  Screen.Cursor := crHourGlass;
  DmPediVda.QrPVIAtrCliIts.Close;
  DmPediVda.QrPVIAtrCliIts.SQL.Clear;
  DmPediVda.QrPVIAtrCliIts.SQL.Add('UPDATE pviatrcli SET Ativo=' + dmkPF.FFP(Status, 0) + ';');
  DmPediVda.QrPVIAtrCliIts.SQL.Add('SELECT * FROM pviatrcli;');
  DmPediVda.QrPVIAtrCliIts.Open;
  Screen.Cursor := crDefault;
end;

procedure TFmPediVdaImpAtrCli.AtivaSelecionados(Status: Integer);
begin
  Screen.Cursor := crHourGlass;
  DmPediVda.QrPVIAtrCliIts.First;
  while not DmPediVda.QrPVIAtrCliIts.Eof do
  begin
    if DmPediVda.QrPVIAtrCliItsAtivo.Value <> Status then
    begin
      DmPediVda.QrPVIAtrCliIts.Edit;
      DmPediVda.QrPVIAtrCliItsAtivo.Value := Status;
      DmPediVda.QrPVIAtrCliIts.Post;
    end;
    DmPediVda.QrPVIAtrCliIts.Next;
  end;
  Screen.Cursor := crDefault;
end;

procedure TFmPediVdaImpAtrCli.BtPesqClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPesq, BtPesq);
end;

procedure TFmPediVdaImpAtrCli.BtNenhumClick(Sender: TObject);
begin
  AtivaItens(0);
end;

procedure TFmPediVdaImpAtrCli.BtTodosClick(Sender: TObject);
begin
  AtivaItens(1);
end;

class function TFmPediVdaImpAtrCli.CreateDockForm(const aColor: TColor): TCustomForm;
begin
  result := TFmPediVdaImpAtrCli.Create(Application);
  //result.Color := aColor;
  //result.Caption := ColorToString(aColor);
  MLAGeral.ConfiguraFormDock(result);
  result.Show;
end;

procedure TFmPediVdaImpAtrCli.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  DmPediVda.QrPVIAtrCliIts.Close;
  DmPediVda.QrPVIAtrCliIts.SQL.Clear;
  DmPediVda.QrPVIAtrCliIts.SQL.Add('DROP TABLE PVIAtrCli; ');
  DmPediVda.QrPVIAtrCliIts.ExecSQL;
  //
  ManualFloat(Rect(0, 0, 0, 0));
  Action := caFree;
end;

procedure TFmPediVdaImpAtrCli.FormShow(Sender: TObject);
begin
  FmPediVdaImp.BtConfirma.Enabled := False;
end;

procedure TFmPediVdaImpAtrCli.FormStartDock(Sender: TObject;
  var DragObject: TDragDockObject);
begin
  DragObject := TDragDockObjectEx.Create(Self);
  DragObject.Brush.Color := clAqua; // this will display a red outline
end;


procedure TFmPediVdaImpAtrCli.FormUnDock(Sender: TObject; Client: TControl;
  NewTarget: TWinControl; var Allow: Boolean);
begin
end;

///// FIM DAS PROCEDURES E FUNCTIONS DO DOCKFORM

procedure TFmPediVdaImpAtrCli.PageControl1Change(Sender: TObject);
begin
  BtPesq.Enabled := PageControl1.ActivePageIndex = 0;
  case PageControl1.ActivePageIndex of
    0: Pesquisar();
    1: RGSelecaoClick(Self);
  end;
end;

procedure TFmPediVdaImpAtrCli.Pesquisar();
begin
  Screen.Cursor := crHourGlass;
  //
  DmPediVda.QrPVIAtrCliIts.Filter   := 'Nome LIKE "%' + EdPesq.Text + '%"';
  DmPediVda.QrPVIAtrCliIts.Filtered := True;
  //
  Screen.Cursor := crDefault;
end;

procedure TFmPediVdaImpAtrCli.RecarregaItens();
const
  Txt1 = 'INSERT INTO pviatrcli (Codigo,CodUsu,Nome,Ativo) VALUES(';
var
  Txt2: String;
begin
  DmPediVda.QrPVIAtrCliIts.Close;
  DmPediVda.QrPVIAtrCliIts.SQL.Clear;
  DmPediVda.QrPVIAtrCliIts.SQL.Add('DELETE FROM pviatrcli; ');
  {
  DmPediVda.QrPVIAtrCliIts.SQL.Add('DROP TABLE PVIAtrCli; ');
  DmPediVda.QrPVIAtrCliIts.SQL.Add('CREATE TABLE PVIAtrCli (');
  DmPediVda.QrPVIAtrCliIts.SQL.Add('  Codigo  integer      ,');
  DmPediVda.QrPVIAtrCliIts.SQL.Add('  CodUsu  integer      ,');
  DmPediVda.QrPVIAtrCliIts.SQL.Add('  Nome    varchar(50)  ,');
  DmPediVda.QrPVIAtrCliIts.SQL.Add('  Ativo   smallint      ');
  DmPediVda.QrPVIAtrCliIts.SQL.Add(');');
  }
  //
  DmPediVda.QrAtrCliSubIts.Close;
  DmPediVda.QrAtrCliSubIts.Params[0].AsInteger := DmPediVda.QrPVIAtrCliCadCodigo.Value;
  DmPediVda.QrAtrCliSubIts.Open;
  DmPediVda.QrAtrCliSubIts.First;
  while not DmPediVda.QrAtrCliSubIts.Eof do
  begin
    Txt2 :=
      dmkPF.FFP(DmPediVda.QrAtrCliSubItsCodigo.Value, 0) + ',' +
      dmkPF.FFP(DmPediVda.QrAtrCliSubItsCodUsu.Value, 0) + ',' +
      '"' + DmPediVda.QrAtrCliSubItsNome.Value + '",' +
      '0);';
    DmPediVda.QrPVIAtrCliIts.SQL.Add(Txt1 + Txt2);
    DmPediVda.QrAtrCliSubIts.Next;
  end;
  //
  DmPediVda.QrPVIAtrCliIts.SQL.Add('SELECT * FROM pviatrcli;');
  DmPediVda.QrPVIAtrCliIts.Open;
  PageControl1.ActivePageIndex := 0;

  //
  if DmPediVda.QrPVIAtrCliCadCodigo.Value <> 0 then
  begin
    FmPediVdaImp.FPVIAtrCli_Cod := DmPediVda.QrPVIAtrCliCadCodigo.Value;
    FmPediVdaImp.FPVIAtrCli_Txt := DmPediVda.QrPVIAtrCliCadNome.Value;
  end else begin
    FmPediVdaImp.FPVIAtrCli_Txt := '';
    FmPediVdaImp.FPVIAtrCli_Cod := 0;
  end;
end;

procedure TFmPediVdaImpAtrCli.RGSelecaoClick(Sender: TObject);
begin
  DmPediVda.QrPVIAtrCliIts.Filtered := False;
  case RGSelecao.ItemIndex of
    0: DmPediVda.QrPVIAtrCliIts.Filter := 'Ativo=0';
    1: DmPediVda.QrPVIAtrCliIts.Filter := 'Ativo=1';
    2: DmPediVda.QrPVIAtrCliIts.Filter := '';
  end;
  if RGSelecao.ItemIndex < 2 then
    DmPediVda.QrPVIAtrCliIts.Filtered := True;
end;

procedure TFmPediVdaImpAtrCli.DBGrid1CellClick(Column: TColumn);
var
  Status: Byte;
begin
  if Column.FieldName = 'Ativo' then
  begin
    if DmPediVda.QrPVIAtrCliIts.FieldByName('Ativo').Value = 1 then
      Status := 0
    else
      Status := 1;
    //
    DmPediVda.QrPVIAtrCliIts.Edit;
    DmPediVda.QrPVIAtrCliIts.FieldByName('Ativo').Value := Status;
    DmPediVda.QrPVIAtrCliIts.Post;
  end;
  DmPediVda.QrPVIAtrCliAti.Close;
  DmPediVda.QrPVIAtrCliAti.Open;
  {
  Habilita :=  DmPediVda.QrPVIAtrCliAtiItens.Value = 0;
  FmPediVdaImp.LaPVIAtrCli.Enabled := Habilita;
  FmPediVdaImp.EdPVIAtrCli.Enabled := Habilita;
  FmPediVdaImp.CBPVIAtrCli.Enabled := Habilita;
  if not Habilita then
  begin
    FmPediVdaImp.EdPVIAtrCli.ValueVariant := 0;
    FmPediVdaImp.CBPVIAtrCli.KeyValue     := 0;
  end;
  }
end;

procedure TFmPediVdaImpAtrCli.Desativa1Click(Sender: TObject);
begin
  AtivaSelecionados(0);
end;

procedure TFmPediVdaImpAtrCli.EdPesqChange(Sender: TObject);
begin
  Pesquisar();
end;

procedure TFmPediVdaImpAtrCli.EdPVIAtrCliChange(Sender: TObject);
begin
  if not EdPVIAtrCli.Focused then
    RecarregaItens();
end;

procedure TFmPediVdaImpAtrCli.EdPVIAtrCliEnter(Sender: TObject);
begin
  FItem := EdPVIAtrCli.ValueVariant;
end;

procedure TFmPediVdaImpAtrCli.EdPVIAtrCliExit(Sender: TObject);
begin
  if FItem <> EdPVIAtrCli.ValueVariant then
  begin
    FItem := EdPVIAtrCli.ValueVariant;
    RecarregaItens();
  end;
end;

procedure TFmPediVdaImpAtrCli.FormCreate(Sender: TObject);
const
  Txt1 = 'INSERT INTO pviatrcli (Codigo,CodUsu,Nome,Ativo) VALUES(';
begin
  DmPediVda.QrPVIAtrCliIts.Close;
  DmPediVda.QrPVIAtrCliIts.SQL.Clear;
  DmPediVda.QrPVIAtrCliIts.SQL.Add('DROP TABLE PVIAtrCli; ');
  DmPediVda.QrPVIAtrCliIts.SQL.Add('CREATE TABLE PVIAtrCli (');
  DmPediVda.QrPVIAtrCliIts.SQL.Add('  Codigo  integer      ,');
  DmPediVda.QrPVIAtrCliIts.SQL.Add('  CodUsu  integer      ,');
  DmPediVda.QrPVIAtrCliIts.SQL.Add('  Nome    varchar(50)  ,');
  DmPediVda.QrPVIAtrCliIts.SQL.Add('  Ativo   smallint      ');
  DmPediVda.QrPVIAtrCliIts.SQL.Add(');');
  //
  DmPediVda.QrPVIAtrCliCad.Close;
  DmPediVda.QrPVIAtrCliCad.Open;
  {
  while not DmPediVda.QrPVIAtrCliCad.Eof do
  begin
    Txt2 :=
      dmkPF.FFP(DmPediVda.QrPVIAtrCliCadCodigo.Value, 0) + ',' +
      dmkPF.FFP(DmPediVda.QrPVIAtrCliCadCodUsu.Value, 0) + ',' +
      '"' + DmPediVda.QrPVIAtrCliCadNome.Value + '",' +
      '0);';
  //for i  := 1 to 10 do
    DmPediVda.QrPVIAtrCliIts.SQL.Add(Txt1 + Txt2);
    DmPediVda.QrPVIAtrCliCad.Next;
  end;
  }
  //
  DmPediVda.QrPVIAtrCliIts.SQL.Add('SELECT * FROM pviatrcli;');
  DmPediVda.QrPVIAtrCliIts.Open;
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmPediVdaImpAtrCli.FormDeactivate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 1;
  RGSelecao.ItemIndex := 1;
  //
  if FindWindow('TFmPediVdaImp', nil) > 0 then
    FmPediVdaImp.AtivaBtConfirma();
end;

end.

