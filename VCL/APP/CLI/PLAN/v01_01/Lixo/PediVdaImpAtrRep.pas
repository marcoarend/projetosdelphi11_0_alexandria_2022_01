unit PediVdaImpAtrRep;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DB, ABSMain, DBGrids,
  dmkDBGrid, DBCtrls, mySQLDbTables, Menus, ComCtrls, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB;

type
  THackDBGrid = class(TDBGrid);
  TFmPediVdaImpAtrRep = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    BtNenhum: TBitBtn;
    BtTodos: TBitBtn;
    BtPesq: TBitBtn;
    PMPesq: TPopupMenu;
    Ativa1: TMenuItem;
    Desativa1: TMenuItem;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel3: TPanel;
    Label1: TLabel;
    EdPesq: TEdit;
    DBGrid1: TdmkDBGrid;
    Panel4: TPanel;
    DBGrid2: TdmkDBGrid;
    RGSelecao: TRadioGroup;
    Panel5: TPanel;
    LaPVIAtrRep: TLabel;
    EdPVIAtrRep: TdmkEditCB;
    CBPVIAtrRep: TdmkDBLookupComboBox;
    Button1: TButton;
    procedure FormCreate(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure FormStartDock(Sender: TObject; var DragObject: TDragDockObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure EdPesqChange(Sender: TObject);
    procedure FormUnDock(Sender: TObject; Client: TControl;
      NewTarget: TWinControl; var Allow: Boolean);
    procedure BtNenhumClick(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtPesqClick(Sender: TObject);
    procedure Ativa1Click(Sender: TObject);
    procedure Desativa1Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure RGSelecaoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EdPVIAtrRepEnter(Sender: TObject);
    procedure EdPVIAtrRepExit(Sender: TObject);
    procedure EdPVIAtrRepChange(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormCanResize(Sender: TObject; var NewWidth, NewHeight: Integer;
      var Resize: Boolean);
    procedure FormActivate(Sender: TObject);
    function FormAlignInsertBefore(Sender: TWinControl; C1,
      C2: TControl): Boolean;
    procedure FormAlignPosition(Sender: TWinControl; Control: TControl;
      var NewLeft, NewTop, NewWidth, NewHeight: Integer; var AlignRect: TRect;
      AlignInfo: TAlignInfo);
    procedure FormConstrainedResize(Sender: TObject; var MinWidth, MinHeight,
      MaxWidth, MaxHeight: Integer);
    procedure FormContextPopup(Sender: TObject; MousePos: TPoint;
      var Handled: Boolean);
    procedure FormDestroy(Sender: TObject);
    procedure FormDockDrop(Sender: TObject; Source: TDragDockObject; X,
      Y: Integer);
    procedure FormDockOver(Sender: TObject; Source: TDragDockObject; X,
      Y: Integer; State: TDragState; var Accept: Boolean);
    procedure FormDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure FormDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure FormEndDock(Sender, Target: TObject; X, Y: Integer);
    procedure FormGetSiteInfo(Sender: TObject; DockClient: TControl;
      var InfluenceRect: TRect; MousePos: TPoint; var CanDock: Boolean);
    procedure FormHide(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }
    FItem: Integer;
    procedure RecarregaItens();
    procedure Pesquisar();
    procedure AtivaItens(Status: Integer);
    procedure AtivaSelecionados(Status: Integer);
    procedure InformaStatusJanela(Mensagem: String);
  public
    { Public declarations }
    class function CreateDockForm(const aColor: TColor): TCustomForm;
    //class procedure FiltaAtivos();
  end;

var
  FmPediVdaImpAtrRep: TFmPediVdaImpAtrRep;

implementation

uses UnMyObjects, Module, UMySQLModule, TabePrcCab, PediVdaImp, ModPediVda;

{$R *.DFM}

///// PROCEDURES E FUNCTIONS DO DOCKFORM

procedure TFmPediVdaImpAtrRep.Ativa1Click(Sender: TObject);
begin
  AtivaSelecionados(1);
end;

procedure TFmPediVdaImpAtrRep.AtivaItens(Status: Integer);
begin
  Screen.Cursor := crHourGlass;
  DmPediVda.QrPVIAtrRepIts.Close;
  DmPediVda.QrPVIAtrRepIts.SQL.Clear;
  DmPediVda.QrPVIAtrRepIts.SQL.Add('UPDATE pviatrrep SET Ativo=' + dmkPF.FFP(Status, 0) + ';');
  DmPediVda.QrPVIAtrRepIts.SQL.Add('SELECT * FROM pviatrrep;');
  DmPediVda.QrPVIAtrRepIts.Open;
  Screen.Cursor := crDefault;
end;

procedure TFmPediVdaImpAtrRep.AtivaSelecionados(Status: Integer);
begin
  Screen.Cursor := crHourGlass;
  DmPediVda.QrPVIAtrRepIts.First;
  while not DmPediVda.QrPVIAtrRepIts.Eof do
  begin
    if DmPediVda.QrPVIAtrRepItsAtivo.Value <> Status then
    begin
      DmPediVda.QrPVIAtrRepIts.Edit;
      DmPediVda.QrPVIAtrRepItsAtivo.Value := Status;
      DmPediVda.QrPVIAtrRepIts.Post;
    end;
    DmPediVda.QrPVIAtrRepIts.Next;
  end;
  Screen.Cursor := crDefault;
end;

procedure TFmPediVdaImpAtrRep.BtPesqClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPesq, BtPesq);
end;

procedure TFmPediVdaImpAtrRep.BtNenhumClick(Sender: TObject);
begin
  AtivaItens(0);
end;

procedure TFmPediVdaImpAtrRep.BtTodosClick(Sender: TObject);
begin
  AtivaItens(1);
end;

procedure TFmPediVdaImpAtrRep.Button1Click(Sender: TObject);
begin
  {
  case BorderStyle of
    bsNone       : ShowMessage('bsNone       ');
    bsSingle     : ShowMessage('bsSingle     ');
    bsSizeable   : ShowMessage('bsSizeable   ');
    bsDialog     : ShowMessage('bsDialog     ');
    bsToolWindow : ShowMessage('bsToolWindow ');
    bsSizeToolWin: ShowMessage('bsSizeToolWin');
    //else ShowMessage(IntToStr(BorderStyle));
  end;
  }
end;

class function TFmPediVdaImpAtrRep.CreateDockForm(const aColor: TColor): TCustomForm;
var
  I: Integer;
begin
  result := TFmPediVdaImpAtrRep.Create(Application);
  //result.Color := aColor;
  //result.Caption := ColorToString(aColor);
  MLAGeral.ConfiguraFormDock(result);
  result.Show;
end;

procedure TFmPediVdaImpAtrRep.FormActivate(Sender: TObject);
begin
  InformaStatusJanela('Activate');
end;

function TFmPediVdaImpAtrRep.FormAlignInsertBefore(Sender: TWinControl; C1,
  C2: TControl): Boolean;
begin
  InformaStatusJanela('AlignInsertBefore');
end;

procedure TFmPediVdaImpAtrRep.FormAlignPosition(Sender: TWinControl;
  Control: TControl; var NewLeft, NewTop, NewWidth, NewHeight: Integer;
  var AlignRect: TRect; AlignInfo: TAlignInfo);
begin
  InformaStatusJanela('AlignPosition');
end;

procedure TFmPediVdaImpAtrRep.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
  //InformaStatusJanela('CanResize');
end;

procedure TFmPediVdaImpAtrRep.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  InformaStatusJanela('Close');
  DmPediVda.QrPVIAtrRepIts.Close;
  DmPediVda.QrPVIAtrRepIts.SQL.Clear;
  DmPediVda.QrPVIAtrRepIts.SQL.Add('DROP TABLE PVIAtrRep; ');
  DmPediVda.QrPVIAtrRepIts.ExecSQL;
  //
  ManualFloat(Rect(0, 0, 0, 0));
  Action := caFree;
end;

procedure TFmPediVdaImpAtrRep.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  InformaStatusJanela('CloseQuery');
end;

procedure TFmPediVdaImpAtrRep.FormConstrainedResize(Sender: TObject;
  var MinWidth, MinHeight, MaxWidth, MaxHeight: Integer);
begin
  //InformaStatusJanela('ConstrainedResize');
end;

procedure TFmPediVdaImpAtrRep.FormContextPopup(Sender: TObject;
  MousePos: TPoint; var Handled: Boolean);
begin
  InformaStatusJanela('ContextPopup');
end;

procedure TFmPediVdaImpAtrRep.FormShow(Sender: TObject);
begin
  InformaStatusJanela('Show');
  FmPediVdaImp.BtConfirma.Enabled := False;
end;

procedure TFmPediVdaImpAtrRep.FormStartDock(Sender: TObject;
  var DragObject: TDragDockObject);
begin
  InformaStatusJanela('StartDock');
  DragObject := TDragDockObjectEx.Create(Self);
  DragObject.Brush.Color := clAqua; // this will display a red outline
end;


procedure TFmPediVdaImpAtrRep.FormUnDock(Sender: TObject; Client: TControl;
  NewTarget: TWinControl; var Allow: Boolean);
begin
  InformaStatusJanela('UnDock');
end;

procedure TFmPediVdaImpAtrRep.InformaStatusJanela(Mensagem: String);
begin
  //if FindWindow('TFmPediVdaImp', nil) > 0 then
    //FmPediVdaImp.Memo1.Lines.Add(Mensagem);
end;

///// FIM DAS PROCEDURES E FUNCTIONS DO DOCKFORM

procedure TFmPediVdaImpAtrRep.PageControl1Change(Sender: TObject);
begin
  BtPesq.Enabled := PageControl1.ActivePageIndex = 0;
  case PageControl1.ActivePageIndex of
    0: Pesquisar();
    1: RGSelecaoClick(Self);
  end;
end;

procedure TFmPediVdaImpAtrRep.Pesquisar();
begin
  Screen.Cursor := crHourGlass;
  //
  DmPediVda.QrPVIAtrRepIts.Filter   := 'Nome LIKE "%' + EdPesq.Text + '%"';
  DmPediVda.QrPVIAtrRepIts.Filtered := True;
  //
  Screen.Cursor := crDefault;
end;

procedure TFmPediVdaImpAtrRep.RecarregaItens();
const
  Txt1 = 'INSERT INTO pviatrrep (Codigo,CodUsu,Nome,Ativo) VALUES(';
var
  Txt2: String;
begin
  DmPediVda.QrPVIAtrRepIts.Close;
  DmPediVda.QrPVIAtrRepIts.SQL.Clear;
  DmPediVda.QrPVIAtrRepIts.SQL.Add('DELETE FROM pviatrrep; ');
  {
  DmPediVda.QrPVIAtrRepIts.SQL.Add('DROP TABLE PVIAtrRep; ');
  DmPediVda.QrPVIAtrRepIts.SQL.Add('CREATE TABLE PVIAtrRep (');
  DmPediVda.QrPVIAtrRepIts.SQL.Add('  Codigo  integer      ,');
  DmPediVda.QrPVIAtrRepIts.SQL.Add('  CodUsu  integer      ,');
  DmPediVda.QrPVIAtrRepIts.SQL.Add('  Nome    varchar(50)  ,');
  DmPediVda.QrPVIAtrRepIts.SQL.Add('  Ativo   smallint      ');
  DmPediVda.QrPVIAtrRepIts.SQL.Add(');');
  }
  //
  DmPediVda.QrAtrRepSubIts.Close;
  DmPediVda.QrAtrRepSubIts.Params[0].AsInteger := DmPediVda.QrPVIAtrRepCadCodigo.Value;
  DmPediVda.QrAtrRepSubIts.Open;
  DmPediVda.QrAtrRepSubIts.First;
  while not DmPediVda.QrAtrRepSubIts.Eof do
  begin
    Txt2 :=
      dmkPF.FFP(DmPediVda.QrAtrRepSubItsCodigo.Value, 0) + ',' +
      dmkPF.FFP(DmPediVda.QrAtrRepSubItsCodUsu.Value, 0) + ',' +
      '"' + DmPediVda.QrAtrRepSubItsNome.Value + '",' +
      '0);';
    DmPediVda.QrPVIAtrRepIts.SQL.Add(Txt1 + Txt2);
    DmPediVda.QrAtrRepSubIts.Next;
  end;
  //
  DmPediVda.QrPVIAtrRepIts.SQL.Add('SELECT * FROM pviatrrep;');
  DmPediVda.QrPVIAtrRepIts.Open;
  PageControl1.ActivePageIndex := 0;

  //
  if DmPediVda.QrPVIAtrRepCadCodigo.Value <> 0 then
  begin
    FmPediVdaImp.FPVIAtrRep_Cod := DmPediVda.QrPVIAtrRepCadCodigo.Value;
    FmPediVdaImp.FPVIAtrRep_Txt := DmPediVda.QrPVIAtrRepCadNome.Value;
  end else begin
    FmPediVdaImp.FPVIAtrRep_Txt := '';
    FmPediVdaImp.FPVIAtrRep_Cod := 0;
  end;
end;

procedure TFmPediVdaImpAtrRep.RGSelecaoClick(Sender: TObject);
begin
  DmPediVda.QrPVIAtrRepIts.Filtered := False;
  case RGSelecao.ItemIndex of
    0: DmPediVda.QrPVIAtrRepIts.Filter := 'Ativo=0';
    1: DmPediVda.QrPVIAtrRepIts.Filter := 'Ativo=1';
    2: DmPediVda.QrPVIAtrRepIts.Filter := '';
  end;
  if RGSelecao.ItemIndex < 2 then
    DmPediVda.QrPVIAtrRepIts.Filtered := True;
end;

procedure TFmPediVdaImpAtrRep.DBGrid1CellClick(Column: TColumn);
var
  Status: Byte;
begin
  if Column.FieldName = 'Ativo' then
  begin
    if DmPediVda.QrPVIAtrRepIts.FieldByName('Ativo').Value = 1 then
      Status := 0
    else
      Status := 1;
    //
    DmPediVda.QrPVIAtrRepIts.Edit;
    DmPediVda.QrPVIAtrRepIts.FieldByName('Ativo').Value := Status;
    DmPediVda.QrPVIAtrRepIts.Post;
  end;
  DmPediVda.QrPVIAtrRepAti.Close;
  DmPediVda.QrPVIAtrRepAti.Open;
  {
  Habilita :=  DmPediVda.QrPVIAtrRepAtiItens.Value = 0;
  FmPediVdaImp.LaPVIAtrRep.Enabled := Habilita;
  FmPediVdaImp.EdPVIAtrRep.Enabled := Habilita;
  FmPediVdaImp.CBPVIAtrRep.Enabled := Habilita;
  if not Habilita then
  begin
    FmPediVdaImp.EdPVIAtrRep.ValueVariant := 0;
    FmPediVdaImp.CBPVIAtrRep.KeyValue     := 0;
  end;
  }
end;

procedure TFmPediVdaImpAtrRep.Desativa1Click(Sender: TObject);
begin
  AtivaSelecionados(0);
end;

procedure TFmPediVdaImpAtrRep.EdPesqChange(Sender: TObject);
begin
  Pesquisar();
end;

procedure TFmPediVdaImpAtrRep.EdPVIAtrRepChange(Sender: TObject);
begin
  if not EdPVIAtrRep.Focused then
    RecarregaItens();
end;

procedure TFmPediVdaImpAtrRep.EdPVIAtrRepEnter(Sender: TObject);
begin
  FItem := EdPVIAtrRep.ValueVariant;
end;

procedure TFmPediVdaImpAtrRep.EdPVIAtrRepExit(Sender: TObject);
begin
  if FItem <> EdPVIAtrRep.ValueVariant then
  begin
    FItem := EdPVIAtrRep.ValueVariant;
    RecarregaItens();
  end;
end;

procedure TFmPediVdaImpAtrRep.FormCreate(Sender: TObject);
const
  Txt1 = 'INSERT INTO pviatrrep (Codigo,CodUsu,Nome,Ativo) VALUES(';
begin
  DmPediVda.QrPVIAtrRepIts.Close;
  DmPediVda.QrPVIAtrRepIts.SQL.Clear;
  DmPediVda.QrPVIAtrRepIts.SQL.Add('DROP TABLE PVIAtrRep; ');
  DmPediVda.QrPVIAtrRepIts.SQL.Add('CREATE TABLE PVIAtrRep (');
  DmPediVda.QrPVIAtrRepIts.SQL.Add('  Codigo  integer      ,');
  DmPediVda.QrPVIAtrRepIts.SQL.Add('  CodUsu  integer      ,');
  DmPediVda.QrPVIAtrRepIts.SQL.Add('  Nome    varchar(50)  ,');
  DmPediVda.QrPVIAtrRepIts.SQL.Add('  Ativo   smallint      ');
  DmPediVda.QrPVIAtrRepIts.SQL.Add(');');
  //
  DmPediVda.QrPVIAtrRepCad.Close;
  DmPediVda.QrPVIAtrRepCad.Open;
  {
  while not DmPediVda.QrPVIAtrRepCad.Eof do
  begin
    Txt2 :=
      dmkPF.FFP(DmPediVda.QrPVIAtrRepCadCodigo.Value, 0) + ',' +
      dmkPF.FFP(DmPediVda.QrPVIAtrRepCadCodUsu.Value, 0) + ',' +
      '"' + DmPediVda.QrPVIAtrRepCadNome.Value + '",' +
      '0);';
  //for i  := 1 to 10 do
    DmPediVda.QrPVIAtrRepIts.SQL.Add(Txt1 + Txt2);
    DmPediVda.QrPVIAtrRepCad.Next;
  end;
  }
  //
  DmPediVda.QrPVIAtrRepIts.SQL.Add('SELECT * FROM pviatrrep;');
  DmPediVda.QrPVIAtrRepIts.Open;
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmPediVdaImpAtrRep.FormDeactivate(Sender: TObject);
begin
  InformaStatusJanela('Deactivate');
  PageControl1.ActivePageIndex := 1;
  RGSelecao.ItemIndex := 1;
  //
  if FindWindow('TFmPediVdaImp', nil) > 0 then
    FmPediVdaImp.AtivaBtConfirma();
end;

procedure TFmPediVdaImpAtrRep.FormDestroy(Sender: TObject);
begin
  InformaStatusJanela('Destroy');
end;

procedure TFmPediVdaImpAtrRep.FormDockDrop(Sender: TObject;
  Source: TDragDockObject; X, Y: Integer);
begin
  InformaStatusJanela('DockDrop');
end;

procedure TFmPediVdaImpAtrRep.FormDockOver(Sender: TObject;
  Source: TDragDockObject; X, Y: Integer; State: TDragState;
  var Accept: Boolean);
begin
  InformaStatusJanela('DockOver');
end;

procedure TFmPediVdaImpAtrRep.FormDragDrop(Sender, Source: TObject; X,
  Y: Integer);
begin
  InformaStatusJanela('DragDrop');
end;

procedure TFmPediVdaImpAtrRep.FormDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  InformaStatusJanela('DragOver');
end;

procedure TFmPediVdaImpAtrRep.FormEndDock(Sender, Target: TObject; X,
  Y: Integer);
begin
  InformaStatusJanela('EndDock');
end;

procedure TFmPediVdaImpAtrRep.FormGetSiteInfo(Sender: TObject;
  DockClient: TControl; var InfluenceRect: TRect; MousePos: TPoint;
  var CanDock: Boolean);
begin
  InformaStatusJanela('GetSiteInfo');
end;

procedure TFmPediVdaImpAtrRep.FormHide(Sender: TObject);
begin
  InformaStatusJanela('Hide');
end;

procedure TFmPediVdaImpAtrRep.FormResize(Sender: TObject);
begin
  //InformaStatusJanela('Resize');
end;

end.

