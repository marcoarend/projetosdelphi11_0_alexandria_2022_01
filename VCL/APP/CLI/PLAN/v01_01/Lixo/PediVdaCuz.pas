unit PediVdaCuz;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DBCtrls, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, DB, mySQLDbTables, Grids, dmkRadioGroup, Mask, Variants,
  dmkLabel;

type
  TFmPediVdaCuz = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Panel3: TPanel;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    SpeedButton1: TSpeedButton;
    EdMatPartCad: TdmkEditCB;
    CBMatPartCad: TdmkDBLookupComboBox;
    RGGrupTip: TRadioGroup;
    PnDados: TPanel;
    QrMatPartCad: TmySQLQuery;
    QrMatPartCadCodigo: TIntegerField;
    QrMatPartCadCodUsu: TIntegerField;
    QrMatPartCadNome: TWideStringField;
    DsMatPartCad: TDataSource;
    QrGraGru1: TmySQLQuery;
    QrGraGru1Nivel1: TIntegerField;
    QrGraGru1CodUsu: TIntegerField;
    QrGraGru1Nome: TWideStringField;
    QrGraGru1GraTamCad: TIntegerField;
    DsGraGru1: TDataSource;
    GroupBox3: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    EdPesqGraGru1: TEdit;
    CBGraGru1: TdmkDBLookupComboBox;
    EdGraGru1: TdmkEditCB;
    GroupBox4: TGroupBox;
    GradeC: TStringGrid;
    Panel4: TPanel;
    GradeA: TStringGrid;
    GradeX: TStringGrid;
    GroupBox1: TGroupBox;
    EdMedidaC: TdmkEdit;
    EdMedidaL: TdmkEdit;
    EdMedidaA: TdmkEdit;
    QrGraGruX: TmySQLQuery;
    DsGraGrux: TDataSource;
    QrGraGruXGraGruC: TIntegerField;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXGraTamI: TIntegerField;
    QrGraGruXControle: TIntegerField;
    RGTipDimens: TDBRadioGroup;
    QrGraGru1TipDimens: TSmallintField;
    GroupBox7: TGroupBox;
    Label11: TLabel;
    DBEdit2: TDBEdit;
    Label12: TLabel;
    DBEdit1: TDBEdit;
    Label10: TLabel;
    DBEdit3: TDBEdit;
    Label1: TLabel;
    DBEdit4: TDBEdit;
    GroupBox8: TGroupBox;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label18: TLabel;
    EdGraGruX: TdmkEdit;
    EdQuantP: TdmkEdit;
    EdPerCustom: TdmkEdit;
    EdDescoP: TdmkEdit;
    QrGraGru1Medida1: TWideStringField;
    QrGraGru1Medida2: TWideStringField;
    QrGraGru1Medida3: TWideStringField;
    QrGraGru1Medida4: TWideStringField;
    DBText1: TDBText;
    DBText2: TDBText;
    DBText3: TDBText;
    QrGraGru1PerCuztMin: TFloatField;
    QrGraGru1PerCuztMax: TFloatField;
    Panel5: TPanel;
    GroupBox6: TGroupBox;
    Label3: TLabel;
    Label9: TLabel;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    GroupBox9: TGroupBox;
    EdPrecoF: TdmkEdit;
    dmkEdit3: TdmkEdit;
    Label8: TLabel;
    EdValLiq: TdmkEdit;
    Label7: TLabel;
    Label6: TLabel;
    DBText4: TDBText;
    EdMedidaE: TdmkEdit;
    Label13: TLabel;
    EdQuantX: TdmkEdit;
    Label17: TLabel;
    EdPrecoO: TdmkEdit;
    LaTipo: TdmkLabel;
    Panel6: TPanel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrGraGru1BeforeClose(DataSet: TDataSet);
    procedure GradeCClick(Sender: TObject);
    procedure GradeCDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure EdPesqGraGru1Exit(Sender: TObject);
    procedure RGGrupTipClick(Sender: TObject);
    procedure EdGraGru1Change(Sender: TObject);
    procedure EdGgxItemChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrGraGru1AfterOpen(DataSet: TDataSet);
    procedure EdMatPartCadChange(Sender: TObject);
    procedure EdMatPartCadEnter(Sender: TObject);
    procedure EdMatPartCadExit(Sender: TObject);
    procedure RGTipDimensChange(Sender: TObject);
    procedure DBEdit5Change(Sender: TObject);
    procedure DBEdit6Change(Sender: TObject);
    procedure DBEdit7Change(Sender: TObject);
    procedure EdGraGruXChange(Sender: TObject);
    procedure EdPrecoOChange(Sender: TObject);
    procedure EdQuantPChange(Sender: TObject);
    procedure EdPerCustomChange(Sender: TObject);
    procedure EdDescoPChange(Sender: TObject);
    procedure EdMedidaCChange(Sender: TObject);
    procedure EdMedidaLChange(Sender: TObject);
    procedure EdMedidaAChange(Sender: TObject);
    procedure EdMedidaEChange(Sender: TObject);
    procedure EdValLiqChange(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdPerCustomEnter(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenGraGruX();
    procedure ReopenGraGru1(Nivel1: Integer);
    procedure HabilitaOK();
    procedure CalculaCusto();
    procedure CalculaVars(
              var GraGruX: Integer;
              var PrecoO, QuantP, DescoP, MedidaC, MedidaL, MedidaA, MedidaE,
              QuantX, ValTot, PerCustom, DescoI, PrecoR, PrecoF, DescoV,
              ValBru, ValLiq, Fator: Double);
  public
    { Public declarations }
    FGraGruX, FControle, FXsel: Integer;
  end;

  var
  FmPediVdaCuz: TFmPediVdaCuz;

implementation

uses ModProd, UnMyObjects, UMySQLModule, PediVda, Module;

{$R *.DFM}

procedure TFmPediVdaCuz.BtOKClick(Sender: TObject);
var
  MatPartCad,
  GraGruX, Conta: Integer;
  PrecoO, QuantP, DescoP, MedidaC, MedidaL, MedidaA, MedidaE,
  QuantX, ValTot, PerCustom, DescoI, PrecoR, PrecoF, DescoV,
  ValBru, ValLiq, Fator: Double;
begin
  if not UMyMod.ObtemCodigoDeCodUsu(EdMatPartCad, MatPartCad,
    'Informe a parte!') then Exit;
  CalculaVars(
    GraGruX,
    PrecoO, QuantP, DescoP, MedidaC, MedidaL, MedidaA, MedidaE, QuantX, ValTot,
    PerCustom, DescoI, PrecoR, PrecoF, DescoV, ValBru, ValLiq, Fator);
  //
  Conta := UMyMod.BuscaEmLivreY_Def('pedivdacuz', 'Conta', LaTipo.SQLType, 0);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, LaTipo.SQLType, 'pedivdacuz', False, [
    'Controle', 'MatPartCad', 'GraGruX',
    'MedidaC', 'MedidaL', 'MedidaA',
    'MedidaE', 'QuantP', 'QuantX',
    'PrecoO', 'PrecoR', 'PrecoF',
    'ValBru', 'DescoP', 'DescoV',
    'PerCustom', 'ValLiq'
  ], ['Conta'], [
    FControle, MatPartCad, GraGruX,
    MedidaC, MedidaL, MedidaA,
    MedidaE, QuantP, QuantX,
    PrecoO, PrecoR, PrecoF,
    ValBru, DescoP, DescoV,
    PerCustom, ValLiq
  ], [Conta], True) then
  begin
    // Atualizar PediVdaIts pai
    //
    Close;
  end;
end;

procedure TFmPediVdaCuz.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPediVdaCuz.CalculaVars(
  var GraGruX: Integer;
  var PrecoO, QuantP, DescoP, MedidaC, MedidaL, MedidaA, MedidaE, QuantX, ValTot,
  PerCustom, DescoI, PrecoR, PrecoF, DescoV, ValBru, ValLiq, Fator: Double);
var
  Casas: Integer;
begin
  //Codigo     := FmPediVda.QrPediVdaCodigo.Value;
  GraGruX    := EdGraGruX.ValueVariant;
  PrecoO     := EdPrecoO.ValueVariant;
  QuantP     := EdQuantP.ValueVariant;
  DescoP     := EdDescoP.ValueVariant;
  MedidaC    := EdMedidaC.ValueVariant;
  MedidaL    := EdMedidaL.ValueVariant;
  MedidaA    := EdMedidaA.ValueVariant;
  MedidaE    := EdMedidaE.ValueVariant;
  PerCustom := EdPerCustom.ValueVariant;
  Fator      := (PerCustom / 100) + 1;
  //
  if (RGTipDimens.ItemIndex < 0) and (MedidaC = 0) then MedidaC := 1;
  if (RGTipDimens.ItemIndex < 1) and (MedidaL = 0) then MedidaL := 1;
  if (RGTipDimens.ItemIndex < 2) and (MedidaA = 0) then MedidaA := 1;
  if (RGTipDimens.ItemIndex < 3) and (MedidaE = 0) then MedidaE := 1;
  //
  QuantX := MedidaC * MedidaL * MedidaA * MedidaE;
  PrecoR := QuantX * PrecoO * Fator;
  //
  Casas  := Dmod.QrControleCasasProd.Value;
  DescoI := MLAGeral.FFF(PrecoR * DescoP / 100, Casas, siPositivo);
  PrecoF := PrecoR - DescoI;
  DescoV := DescoI * QuantP;
  ValBru := PrecoR * QuantP;
  ValLiq := ValBru - DescoV;
  ValTot := ValLiq + 0;// Parei aqui ver total das soma dos itens!
end;

procedure TFmPediVdaCuz.CalculaCusto();
var
  GraGruX: Integer;
  PrecoO, QuantP, DescoP, MedidaC, MedidaL, MedidaA, MedidaE, QuantX, ValTot,
  PerCustom, DescoI, PrecoR, PrecoF, DescoV, ValBru, ValLiq, Fator: Double;
begin
  CalculaVars(GraGruX,
    PrecoO, QuantP, DescoP, MedidaC, MedidaL, MedidaA, MedidaE, QuantX, ValTot,
    PerCustom, DescoI, PrecoR, PrecoF, DescoV, ValBru, ValLiq, Fator);
  EdQuantX.ValueVariant := QuantX;
  EdPrecoF.ValueVariant := PrecoF;
  EdValLiq.ValueVariant := ValLiq;

end;

procedure TFmPediVdaCuz.DBEdit5Change(Sender: TObject);
begin
  EdPerCustom.ValMin := DBEdit5.Text;
end;

procedure TFmPediVdaCuz.DBEdit6Change(Sender: TObject);
begin
  EdPerCustom.ValMax := DBEdit6.Text;
end;

procedure TFmPediVdaCuz.DBEdit7Change(Sender: TObject);
begin
  EdDescoP.ValMax := DBEdit7.Text;
end;

procedure TFmPediVdaCuz.EdDescoPChange(Sender: TObject);
begin
  CalculaCusto();
end;

procedure TFmPediVdaCuz.EdGgxItemChange(Sender: TObject);
begin
  HabilitaOK();
end;

procedure TFmPediVdaCuz.EdGraGru1Change(Sender: TObject);
begin
  if EdGraGru1.ValueVariant > 0 then
    DmProd.ConfigGrades0(QrGraGru1GraTamCad.Value, QrGraGru1Nivel1.Value,
    GradeA, GradeX, GradeC)
  else MLAGeral.LimpaGrades([GradeA, GradeC, GradeX], 0, 0, True);

end;

procedure TFmPediVdaCuz.EdGraGruXChange(Sender: TObject);
var
  GraGruX: Integer;
begin
  HabilitaOK();
  GraGruX := Geral.IMV(EdGraGruX.Text);
  EdPrecoO.ValueVariant := DmProd.ObtemPrecoGraGruX_GraCusPrc(
    GraGruX, FmPediVda.QrPediVdaTabelaPrc.Value);
end;

procedure TFmPediVdaCuz.EdMatPartCadChange(Sender: TObject);
begin
  if not EdMatPartCad.Focused then
    ReopenGraGru1(0);
end;

procedure TFmPediVdaCuz.EdMatPartCadEnter(Sender: TObject);
begin
  FXsel := EdMatPartCad.ValueVariant;
end;

procedure TFmPediVdaCuz.EdMatPartCadExit(Sender: TObject);
begin
  if FXsel <> EdMatPartCad.ValueVariant then
    ReopenGraGru1(0);
end;

procedure TFmPediVdaCuz.EdMedidaAChange(Sender: TObject);
begin
  CalculaCusto();
end;

procedure TFmPediVdaCuz.EdMedidaCChange(Sender: TObject);
begin
  CalculaCusto();
end;

procedure TFmPediVdaCuz.EdMedidaEChange(Sender: TObject);
begin
  CalculaCusto();
end;

procedure TFmPediVdaCuz.EdMedidaLChange(Sender: TObject);
begin
  CalculaCusto();
end;

procedure TFmPediVdaCuz.EdPerCustomChange(Sender: TObject);
begin
  CalculaCusto();
end;

procedure TFmPediVdaCuz.EdPerCustomEnter(Sender: TObject);
begin
  //ShowMessage('M�nimo: ' + EdPerCustom.ValMin);
  //ShowMessage('M�ximo: ' + EdPerCustom.ValMax);
end;

procedure TFmPediVdaCuz.EdPesqGraGru1Exit(Sender: TObject);
begin
  ReopenGraGru1(0);
end;

procedure TFmPediVdaCuz.EdPrecoOChange(Sender: TObject);
begin
  CalculaCusto();
end;

procedure TFmPediVdaCuz.EdQuantPChange(Sender: TObject);
begin
  CalculaCusto();
end;

procedure TFmPediVdaCuz.EdValLiqChange(Sender: TObject);
begin
  HabilitaOK();
end;

procedure TFmPediVdaCuz.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  ReopenGraGruX();
end;

procedure TFmPediVdaCuz.FormCreate(Sender: TObject);
begin
  QrMatPartCad.Open;
  QrGraGruX.Close;
end;

procedure TFmPediVdaCuz.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmPediVdaCuz.GradeCClick(Sender: TObject);
begin
  if (GradeC.Col > 0) and (GradeC.Row > 0) then
  begin
    EdGraGruX.Text := GradeC.Cells[GradeC.Col, GradeC.Row];
    EdQuantP.SetFocus;
  end;
end;

procedure TFmPediVdaCuz.GradeCDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeC, GradeA, nil, ACol, ARow, Rect, State,
  '0', 0, 0, False);
end;

procedure TFmPediVdaCuz.HabilitaOK();
var
  Produto, Reduzido: Integer;
  ValLiq: Double;
begin
  Produto  := Geral.IMV(EdGraGru1.Text);
  Reduzido := Geral.IMV(EdGraGruX.Text);
  ValLiq   := Geral.DMV(EdValLiq.Text);
  BtOK.Enabled := (Produto > 0) and (Reduzido > 0) and (ValLiq > 0);
end;

procedure TFmPediVdaCuz.QrGraGru1AfterOpen(DataSet: TDataSet);
begin
  PnDados.Visible := QrGraGru1.RecordCount > 0;
  EdGraGru1.ValueVariant := 0;
  CBGraGru1.KeyValue := Null;
end;

procedure TFmPediVdaCuz.QrGraGru1BeforeClose(DataSet: TDataSet);
begin
  MLAGeral.LimpaGrades([GradeA, GradeC, GradeX], 0, 0, True);
  PnDados.Visible := False;
end;

procedure TFmPediVdaCuz.ReopenGraGru1(Nivel1: Integer);
  function CancelaZero(Campo: String): String;
  begin
    if Campo = '0' then
      Result := '-1000'
    else
      Result := Campo;
  end;
var
  Grupo, GamaT, Grade, Reduz: String;
  Parte: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    QrGraGru1.Close;
    if RGGrupTip.ItemIndex = 0 then Exit;
    if not UmyMod.ObtemCodigoDeCodUsu(EdMatPartCad, Parte,
      'Informe a parte do produto!') then Exit;
    Grupo := CancelaZero(FormatFloat('0', QrGraGruXGraGru1.Value));
    GamaT := CancelaZero(FormatFloat('0', QrGraGruXGraGruC.Value));
    Grade := CancelaZero(FormatFloat('0', QrGraGruXGraTamI.Value));
    Reduz := CancelaZero(FormatFloat('0', QrGraGruXControle.Value));
    //
    QrGraGru1.SQL.Clear;
    QrGraGru1.SQL.Add('SELECT gg1.Nivel1, gg1.CodUsu, gg1.Nome, gg1.GraTamCad,');
    QrGraGru1.SQL.Add('gg1.TipDimens, gg1.PerCuztMin, gg1.PerCuztMax,');
    QrGraGru1.SQL.Add('mor.Medida1, mor.Medida2, mor.Medida3, mor.Medida4');
    QrGraGru1.SQL.Add('');
    QrGraGru1.SQL.Add('FROM gragru1 gg1');
    QrGraGru1.SQL.Add('LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip');
    QrGraGru1.SQL.Add('LEFT JOIN gragrux ggx ON ggx.GraGru1=gg1.Nivel1 ');
    QrGraGru1.SQL.Add('LEFT JOIN medordem mor ON mor.Codigo=gg1.MedOrdem ');

    // C�digos de Nivel1, Grade, Gama e reduzido
    // vai gerar erro caso n�o haja nenhum
    QrGraGru1.SQL.Add('WHERE ggx.Controle in (');
    QrGraGru1.SQL.Add('     SELECT GgxItem ');
    QrGraGru1.SQL.Add('     FROM gramatits ');
    QrGraGru1.SQL.Add('     WHERE MatPartCad=' + FormatFloat('0', Parte));
    QrGraGru1.SQL.Add('     AND ');
    QrGraGru1.SQL.Add('     ( ');
    QrGraGru1.SQL.Add('          (GraGru1=' + Grupo + ') OR ');
    QrGraGru1.SQL.Add('          (GraTamI=' + Grade + ') OR ');
    QrGraGru1.SQL.Add('          (GraGruC=' + GamaT + ') OR ');
    QrGraGru1.SQL.Add('          (GraGruX=' + Reduz + ') ');
    QrGraGru1.SQL.Add('     ) ');
    QrGraGru1.SQL.Add(') ');

    // Tipo de grupo de produto
    case RGGrupTip.ItemIndex of
      3: QrGraGru1.SQL.Add('AND pgt.TipPrd in (1,2)');
      else QrGraGru1.SQL.Add('AND pgt.TipPrd=' + FormatFloat('0', RGGrupTip.ItemIndex));
    end;

    // Filtro por nome do grupo
    QrGraGru1.SQL.Add('AND gg1.Nome LIKE "%' + EdPesqGraGru1.Text + '%"');

    QrGraGru1.SQL.Add('ORDER BY gg1.Nome');
    UMyMod.AbreQuery(QrGraGru1);

    if Nivel1 <> 0 then
      QrGraGru1.Locate('Nivel1', Nivel1, []);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPediVdaCuz.ReopenGraGruX();
begin
  QrGraGruX.Close;
  QrGraGruX.Params[0].AsInteger := FGraGruX;
  QrGraGruX.Open;
end;

procedure TFmPediVdaCuz.RGGrupTipClick(Sender: TObject);
begin
  ReopenGraGru1(0);
end;

procedure TFmPediVdaCuz.RGTipDimensChange(Sender: TObject);
begin
  EdMedidaC.Enabled := RGTipDimens.ItemIndex >= 0;
  EdMedidaL.Enabled := RGTipDimens.ItemIndex >= 1;
  EdMedidaA.Enabled := RGTipDimens.ItemIndex >= 2;
  EdMedidaE.Enabled := RGTipDimens.ItemIndex >= 3;
  //
  if EdMedidaC.Enabled then EdMedidaC.ValueVariant := 0;
  if EdMedidaL.Enabled then EdMedidaL.ValueVariant := 0;
  if EdMedidaA.Enabled then EdMedidaA.ValueVariant := 0;
  if EdMedidaE.Enabled then EdMedidaE.ValueVariant := 0;
end;

end.
