unit EstqCen;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, UnDmkProcFunc;

type
  TFmEstqCen = class(TForm)
    PainelDados: TPanel;
    DsEstqCen: TDataSource;
    QrEstqCen: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TdmkLabel;
    Image1: TImage;
    PanelFill2: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TdmkLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdCodUsu: TdmkEdit;
    QrEstqCenCodigo: TIntegerField;
    QrEstqCenCodUsu: TIntegerField;
    QrEstqCenNome: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrEstqCenAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrEstqCenBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    //procedure IncluiRegistro;
    //procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; Status: String; Codigo: Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
  end;

var
  FmEstqCen: TFmEstqCen;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmEstqCen.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmEstqCen.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrEstqCenCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmEstqCen.DefParams;
begin
  VAR_GOTOTABELA := 'EstqCen';
  VAR_GOTOMYSQLTABLE := QrEstqCen;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT Codigo, CodUsu, Nome');
  VAR_SQLx.Add('FROM estqcen');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmEstqCen.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (LaTipo.SQLType = stIns) then
    EdCodUsu.ValueVariant :=
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'EstqCen', 'CodUsu', [], [], stIns, 0, True);
end;

procedure TFmEstqCen.MostraEdicao(Mostra: Integer; Status: String; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelControle.Visible:=True;
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      PainelControle.Visible:=False;
      if Status = CO_INCLUSAO then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Geral.MensagemBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  LaTipo.Caption := Status;
  GOTOy.BotoesSb(LaTipo.Caption);
end;

procedure TFmEstqCen.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

{
procedure TFmEstqCen.AlteraRegistro;
var
  EstqCen : Integer;
begin
  EstqCen := QrEstqCenCodigo.Value;
  if QrEstqCenCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(EstqCen, Dmod.MyDB, 'EstqCen', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(EstqCen, Dmod.MyDB, 'EstqCen', 'Codigo');
      MostraEdicao(1, CO_ALTERACAO, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmEstqCen.IncluiRegistro;
var
  Cursor : TCursor;
  EstqCen : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    EstqCen := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'EstqCen', 'EstqCen', 'Codigo');
    if Length(FormatFloat(FFormatFloat, EstqCen))>Length(FFormatFloat) then
    begin
      Geral.MensagemBox(
      'Inclus�o cancelada. Limite de cadastros extrapolado', 'Erro',
      MB_OK+MB_ICONERROR);
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(1, CO_INCLUSAO, EstqCen);
  finally
    Screen.Cursor := Cursor;
  end;
end;
}

procedure TFmEstqCen.QueryPrincipalAfterOpen;
begin
end;

procedure TFmEstqCen.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmEstqCen.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmEstqCen.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmEstqCen.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmEstqCen.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmEstqCen.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrEstqCen, [PainelDados],
  [PainelEdita], EdCodUsu, LaTipo, 'EstqCen');
end;

procedure TFmEstqCen.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrEstqCenCodigo.Value;
  Close;
end;

procedure TFmEstqCen.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Geral.MensagemBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Codigo := UMyMod.BuscaEmLivreY_Def('EstqCen', 'Codigo', LaTipo.SQLType,
    QrEstqCenCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(LaTipo.SQLType, FmEstqCen, PainelEdit,
    'EstqCen', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], LaTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmEstqCen.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if LaTipo.Caption = CO_INCLUSAO then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'EstqCen', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'EstqCen', 'Codigo');
  MostraEdicao(0, CO_TRAVADO, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'EstqCen', 'Codigo');
end;

procedure TFmEstqCen.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrEstqCen, [PainelDados],
  [PainelEdita], EdCodUsu, LaTipo, 'EstqCen');
end;

procedure TFmEstqCen.FormCreate(Sender: TObject);
begin
  PainelEdit.Align  := alClient;
  PainelData.Align  := alClient;
  CriaOForm;
end;

procedure TFmEstqCen.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrEstqCenCodigo.Value, LaRegistro.Caption);
end;

procedure TFmEstqCen.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmEstqCen.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrEstqCenCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmEstqCen.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmEstqCen.QrEstqCenAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmEstqCen.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEstqCen.SbQueryClick(Sender: TObject);
begin
  LocCod(QrEstqCenCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'EstqCen', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmEstqCen.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmEstqCen.QrEstqCenBeforeOpen(DataSet: TDataSet);
begin
  QrEstqCenCodigo.DisplayFormat := FFormatFloat;
end;

end.

