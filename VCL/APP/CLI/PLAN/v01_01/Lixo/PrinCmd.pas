unit PrinCmd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DB, mySQLDbTables, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkLabel, Mask, dmkDBEdit, dmkGeral,
  Variants, dmkRadioGroup;

type
  TFmPrinCmd = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    LaTipo: TdmkLabel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdit1: TDBEdit;
    Label4: TLabel;
    Label3: TLabel;
    DBEdNome: TDBEdit;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    EdOrdem: TdmkEdit;
    EdConfig: TdmkEdit;
    Label6: TLabel;
    EdControle: TdmkEdit;
    Label2: TLabel;
    RGTipo: TdmkRadioGroup;
    BitBtn1: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
    function OrdemInvalida(Tipo: Integer): Boolean;
  public
    { Public declarations }
  end;

  var
  FmPrinCmd: TFmPrinCmd;

implementation

uses UnMyObjects, Module, PrinCad, UMySQLModule;

{$R *.DFM}

procedure TFmPrinCmd.BitBtn1Click(Sender: TObject);
begin
  case FmPrinCad.QrPrinCadTypeUso.Value of
    0: // Produtos
    begin
      //
    end;
  end;
end;

procedure TFmPrinCmd.BtOKClick(Sender: TObject);
var
  Controle: Integer;
begin
  if OrdemInvalida(RGTipo.ItemIndex) then Exit;
  Screen.Cursor := crHourGlass;
  try
    Controle := UMyMod.BuscaEmLivreY_Def('PrinCmd', 'Controle', LaTipo.SQLType,
      EdControle.ValueVariant);
    //
    if UMyMod.ExecSQLInsUpdFm(FmPrinCmd, LaTipo.SQLType, 'PrinCmd', Controle,
    Dmod.QrUpd) then
    begin
      FmPrinCad.ReordenaLinhas(RGTipo.ItemIndex, Controle, EdOrdem.ValueVariant);
      { N�o precisa! J� � feito no "ReordenaLinhas"
      case RGTipo.ItemIndex of
        0: FmPrinCad.ReopenPrinCmd_0(Controle);
        1: FmPrinCad.ReopenPrinCmd_1(Controle);
        2: FmPrinCad.ReopenPrinCmd_2(Controle);
      end;
      }
      //
      if CkContinuar.Checked then
      begin
        Latipo.SQLType           := stIns;
        EdControle.ValueVariant  := 0;
        EdOrdem.ValueVariant     := EdOrdem.ValueVariant + 1;
        EdConfig.ValueVariant    := '';
        EdOrdem.SetFocus;
      end else Close;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPrinCmd.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPrinCmd.FormActivate(Sender: TObject);
begin
  if LaTipo.SQLType <> stIns then
    RGTipo.Enabled := False;
  MyObjects.CorIniComponente();
end;

procedure TFmPrinCmd.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

function TFmPrinCmd.OrdemInvalida(Tipo: Integer): Boolean;
var
  Max, o: Integer;
begin
  Result := False;
  case Tipo of
    0: Max := FmPrinCad.QrPrinCmd_0.RecordCount;
    1: Max := FmPrinCad.QrPrinCmd_1.RecordCount;
    2: Max := FmPrinCad.QrPrinCmd_2.RecordCount;
  end;
  if LaTipo.SQLType = stIns then Max := Max + 1;
  //
  o := EdOrdem.ValueVariant;
  if o > Max then
  begin
    Geral.MensagemBox('A ordem informada (' + IntToStr(o) +
    ') � maior que a maior ordem poss�vel (' + IntToStr(Max) + ')!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Result := True;
    Screen.Cursor := crDefault;
    Exit;
  end;
end;

end.
