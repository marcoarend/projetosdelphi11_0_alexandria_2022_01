﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Schema;
using System.Security.Cryptography;
using System.Security.Cryptography.Xml;
using System.Security.Cryptography.X509Certificates;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Web.Services.Protocols;
using System.Web.Services.Discovery;


// -----------------------------------------------------------------------------------------------------------------
//                                         Biblioteca NFE_dll.dll
// -----------------------------------------------------------------------------------------------------------------
//
//   Nota: Para que o código funcione é necessário adicionar a referência “System.Security.Criptography.Xml” ao 
//   projeto do Visual Studio, da seguinte forma:
//
//     1. selecione “References” no Solution Explorer eclique com o botão direito; 
//
//     2. selecione “Add References…”; 
//
//     3. procure o componente “System.Security” na aba .NET e confirme; 
//
//     4. no menu  “System.Security” na aba .NET e confirme; 
//


namespace NFE_dll
{   
    [ClassInterface(ClassInterfaceType.AutoDual)]

    [ProgId("NFE_dll.AssinarXML")]

    [ComVisible(true)]


    public class AssinarXML
    {
        public int vResultado { get; private set; }
        public string vResultadoString { get; private set; }
        public string vXMLStringAssinado { get; private set; }
        private XmlDocument XMLDoc;

        /*
         * ==============================================================================
         * AssinarXML - NFe [ Nota Fiscal Eletrônica ]
         * ==============================================================================
         * Descrição..: O método assina digitalmente o arquivo XML passado por 
         *              parâmetro
         *              O método grava o XML assinado com o mesmo nome, sobreponto o 
         *              XML informado por parâmetro
         *              O método também disponibiliza uma propriedade com uma string
         *              do xml assinado (vXmlStringAssinado)
         *              
         * ------------------------------------------------------------------------------
         * Definição..: Assinar( string, string, string, int, string )
         * Parâmetros.: pArqXMLAssinar - Nome do arquivo XML a ser assinado
         *              pUri           - URI (TAG) a ser assinada
         *              oCertificado   - Titular [Subject] do Certificado a ser utilizado na assinatura
         *
         * ------------------------------------------------------------------------------
         * Retorno....: - Atualiza a propriedade vXMLStringAssinado com a string de
         *                xml já assinada
         *              - Grava o XML sobreponto o informado para o método com o conteúdo
         *                já assinado
         *                
         *              - Atualiza as propriedades vResultado e 
         *                vResultadoString com os seguintes valores:
         *                
         *                0 - Assinatura realizada com sucesso
         *                1 - Erro: Problema ao acessar o certificado digital - %exceção%
         *                2 - Problemas no certificado digital
         *                3 - XML mal formado + %exceção%
         *                4 - A tag de assinatura %pUri% não existe 
         *                5 - A tag de assinatura %pUri% não é unica
         *                6 - Erro ao assinar o documento - %exceção%
         *              
         * ------------------------------------------------------------------------------
         * Exemplos...:
         * 
         * ------------------------------------------------------------------------------
         * Notas......: 
         *              
         * ==============================================================================         
         */
        public void Assinar(string pArqXMLAssinar, string pUri, string oCertificado, out int vResultado, out string vResultadoString)
        {

            //Abrir o arquivo XML a ser assinado e ler o seu conteúdo
            StreamReader SR = File.OpenText(pArqXMLAssinar);
            string vXMLString = SR.ReadToEnd();
            SR.Close();

            //Atualizar atributos de retorno com conteúdo padrão
            vResultado = 0;
            vResultadoString = "Assinatura realizada com sucesso";

            try
            {
                // Verifica o certificado a ser utilizado na assinatura
                string _xnome = "";
                if (oCertificado != "")
                {
                    _xnome = oCertificado;
                }

                X509Certificate2 _X509Cert = new X509Certificate2();
                X509Store store = new X509Store("MY", StoreLocation.CurrentUser);
                store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);
                X509Certificate2Collection collection = (X509Certificate2Collection)store.Certificates;
                X509Certificate2Collection collection1 = (X509Certificate2Collection)collection.Find(X509FindType.FindBySubjectDistinguishedName, _xnome, false);

                if (collection1.Count == 0)
                {
                    vResultado = 2;
                    vResultadoString = "Problemas no certificado digital";
                }
                else
                {
                   
                    // certificado ok
                    _X509Cert = collection1[0];

                    if (!_X509Cert.HasPrivateKey){
                        throw new Exception("Certificado digital deve possuir chave privada");
                    }


                    string x;
                    x = _X509Cert.GetKeyAlgorithm().ToString();

                    // Create a new XML document.
                    XmlDocument doc = new XmlDocument();

                    // Format the document to ignore white spaces.
                    doc.PreserveWhitespace = false;

                    // Load the passed XML file using it’s name.
                    try
                    {
                        doc.LoadXml(vXMLString);

                        // Verifica se a tag a ser assinada existe é única
                        int qtdeRefUri = doc.GetElementsByTagName(pUri).Count;

                        if (qtdeRefUri == 0)
                        {
                            // a URI indicada não existe
                            vResultado = 4;
                            vResultadoString = "A tag de assinatura " + pUri.Trim() + " não existe";
                        }
                        // Exsiste mais de uma tag a ser assinada
                        else
                        {
                            if (qtdeRefUri > 1)
                            {
                                // existe mais de uma URI indicada
                                vResultado = 5;
                                vResultadoString = "A tag de assinatura " + pUri.Trim() + " não é unica";
                            }
                            else
                            {
                                try
                                {
                                    // Create a SignedXml object.
                                    SignedXml signedXml = new SignedXml(doc);

                                    // Add the key to the SignedXml document
                                    signedXml.SigningKey = _X509Cert.PrivateKey;

                                    // Create a reference to be signed
                                    Reference reference = new Reference();

                                    // pega o uri que deve ser assinada
                                    XmlAttributeCollection _Uri = doc.GetElementsByTagName(pUri).Item(0).Attributes;
                                    foreach (XmlAttribute _atributo in _Uri)
                                    {
                                        if (_atributo.Name == "Id")
                                        {
                                            reference.Uri = "#" + _atributo.InnerText;
                                        }
                                    }

                                    // Add an enveloped transformation to the reference.
                                    XmlDsigEnvelopedSignatureTransform env = new XmlDsigEnvelopedSignatureTransform();
                                    reference.AddTransform(env);

                                    XmlDsigC14NTransform c14 = new XmlDsigC14NTransform();
                                    reference.AddTransform(c14);

                                    // Add the reference to the SignedXml object.
                                    signedXml.AddReference(reference);

                                    signedXml.ComputeSignature();
                                    // Create a new KeyInfo object
                                    KeyInfo keyInfo = new KeyInfo();

                                    // Load the certificate into a KeyInfoX509Data object
                                    // and add it to the KeyInfo object.
                                    keyInfo.AddClause(new KeyInfoX509Data(_X509Cert));

                                    // Add the KeyInfo object to the SignedXml object.
                                    signedXml.KeyInfo = keyInfo;
                                    

                                    // Get the XML representation of the signature and save
                                    // it to an XmlElement object.
                                    XmlElement xmlDigitalSignature = signedXml.GetXml();

                                    // Gravar o elemento no documento XML
                                    doc.DocumentElement.AppendChild(doc.ImportNode(xmlDigitalSignature, true));
                                    XMLDoc = new XmlDocument();
                                    XMLDoc.PreserveWhitespace = false;
                                    XMLDoc = doc;

                                    // Atualizar a string do XML já assinada
                                    vXMLStringAssinado = XMLDoc.OuterXml;

                                    // Gravar o XML no HD
                                    StreamWriter SW_2 = File.CreateText(pArqXMLAssinar);
                                    SW_2.Write(vXMLStringAssinado);
                                    SW_2.Close();
                                }
                                catch (Exception caught)
                                {
                                    vResultado = 6;
                                    vResultadoString = "Erro ao assinar o documento - " + caught.Message;
                                }
                            }
                        }
                    }
                    catch (Exception caught)
                    {
                        vResultado = 3;
                        vResultadoString = "XML mal formado - " + caught.Message;
                    }
                }
            }
            catch (Exception caught)
            {
                vResultado = 1;
                vResultadoString = "Problema ao acessar o certificado digital" + caught.Message;
            }
        }
    }



    [ClassInterface(ClassInterfaceType.AutoDual)]

    [ProgId("NFE_dll.BuscarCertificado")]

    [ComVisible(true)]

    public class BuscarCertificado
    {

        public string vResultadoString { get; private set; }
        public X509Certificate2 oCertificado { get; set; }
 
        /*
        * ==============================================================================
        * BuscarCertificado - NFe [ NOTA FISCAL ELETRÔNICA ]
        * ==============================================================================
        * Descrição..: Método responsável por buscar um certificado válido
        *              
        * ------------------------------------------------------------------------------
        * Definição..: BuscaNome(  )
        * Parâmetros.: 
        *                        
        * ------------------------------------------------------------------------------
        * Retorno....: Atualiza uma propriedade da classe:
        *              vResultadoString - em caso de algum erro, esta propriedade vai
        *                                 receber um texto contendo o erro para ser
        *                                 analisado, em caso de sucesso essa propriedade
        *                                 vai receber o Titular do Certificado. 
        * ------------------------------------------------------------------------------
        * Exemplos...:
        * 
        * 
        * ------------------------------------------------------------------------------
        * Notas......:
        * 
        * ==============================================================================         
        */

        public bool BuscaNome(out string vResultadoString)
        {
            bool vRetorna;
            vResultadoString = "";

            X509Certificate2 oX509Cert = new X509Certificate2();
            X509Store store = new X509Store("MY", StoreLocation.CurrentUser);
            store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);
            X509Certificate2Collection collection = (X509Certificate2Collection)store.Certificates;
            X509Certificate2Collection collection1 = (X509Certificate2Collection)collection.Find(X509FindType.FindByTimeValid, DateTime.Now, false);
            X509Certificate2Collection collection2 = (X509Certificate2Collection)collection.Find(X509FindType.FindByKeyUsage, X509KeyUsageFlags.DigitalSignature, false);
            X509Certificate2Collection scollection = X509Certificate2UI.SelectFromCollection(collection2, "Certificado(s) Digital(is) disponível(is)", "Selecione o certificado digital para uso no aplicativo", X509SelectionFlag.SingleSelection);

            if (scollection.Count == 0)
            {
                vResultadoString = "Nenhum certificado digital foi selecionado ou o certificado selecionado está com problemas.";
                vRetorna = false;
            }
            else
            {
                oX509Cert = scollection[0];
                oCertificado = oX509Cert;
                vResultadoString = oX509Cert.Subject.ToString();
                vRetorna = true;
            }

            return vRetorna;
        }

    }



    [ClassInterface(ClassInterfaceType.AutoDual)]

    [ProgId("NFE_dll.ValidaXML")]

    [ComVisible(true)]

    public class ValidarXML
    {

        public int nRetornoTipoArq { get; private set; }
        public string cRetornoTipoArq { get; private set; }
        public string cArquivoSchema { get; private set; }


        /*
         * ==============================================================================
         * ValidarXML - NFe [ NOTA FISCAL ELETRÔNICA ]
         * ==============================================================================
         * Descrição..: Classe responsável por validar os arquivos XML de acordo com a 
         *              estrutura determinada pelos schemas (XSD)
         *              
         * ------------------------------------------------------------------------------
         * Definição..: ValidarXML( string, string )
         * Parâmetros.: cRotaArqXML    - Rota e nome do arquivo XML que é
         *                               para ser validado. Exemplo:
         *                               c:\ERES NFe\NFe\teste-nfe.xml
         * 
         *              cRotaArqSchema - Rota e nome do arquivo XSD que é para ser 
         *                               utilizado para validar o XML
         *                               c:\ERES NFe\schemas\nfe_v1.10.xsd
         *                        
         * ------------------------------------------------------------------------------
         * Retorno....: Atualiza duas propriedades da classe:
         *              oRetorno       - 0=Sucesso na validação
         *                               1=O XML não está em conformidade com o schema
         *                               2=Arquivo XML não foi encontrato
         *                               3=Arquivo XSD (schema) não foi encontrato
         *              oRetornoString - em caso de algum erro, esta propriedade vai
         *                               receber um texto contendo o erro para ser
         *                               analisado. 
         * ------------------------------------------------------------------------------
         * Exemplos...:
         * 
         * 
         * ------------------------------------------------------------------------------
         * Notas......:
         * 
         * ==============================================================================         
         */
        public void ValidaXML(string cRotaArqXML, string cRotaArqSchema, out int oRetorno, out string oRetornoString)
        {
            bool lArqXML = File.Exists(cRotaArqXML);
            bool lArqXSD = File.Exists(cRotaArqSchema);

            oRetorno = 0;
            oRetornoString = "";

            if (lArqXML && lArqXSD)
            {
                StreamReader cStreamReader = new StreamReader(cRotaArqXML);
                XmlTextReader cXmlTextReader = new XmlTextReader(cStreamReader);
                XmlValidatingReader reader = new XmlValidatingReader(cXmlTextReader);

                // Criar um coleção de schema, adicionar o XSD para ela
                XmlSchemaCollection schemaCollection = new XmlSchemaCollection();
                schemaCollection.Add("http://www.portalfiscal.inf.br/nfe", cRotaArqSchema);

                // Adicionar a coleção de schema para o XmlValidatingReader
                reader.Schemas.Add(schemaCollection);

                // O ValidationEvent é disparado quando o
                // XmlValidatingReader passa pelos setores do XML
                reader.ValidationEventHandler += new ValidationEventHandler(reader_ValidationEventHandler);

                // Iteração através do XML document
                cErro = "";
                while (reader.Read()) { }

                reader.Close();

                oRetorno = 0;
                oRetornoString = "";
                if (cErro != "")
                {
                    oRetorno = 1;
                    oRetornoString += cErro;
                }
            }
            else
            {
                if (lArqXML == false)
                {
                    oRetorno = 2;
                    oRetornoString = "Arquivo XML não foi encontrato";
                }
                else if (lArqXSD == false)
                {
                    oRetorno = 3;
                    oRetornoString = "Arquivo XSD (schema) não foi encontrato";
                }
            }  
            
        }

        public static int Retorno;
        public static string RetornoString;
        private string cErro;

        private void reader_ValidationEventHandler(object sender, ValidationEventArgs e)
        {
            cErro = "Linha: " + e.Exception.LineNumber + " Coluna: " + e.Exception.LinePosition + " Erro: " + e.Exception.Message + "\r\n";
        }


    }



    [ClassInterface(ClassInterfaceType.AutoDual)]

    [ProgId("NFE_dll.Transmissao XML")]

    [ComVisible(true)]

    public class TransmissaoXML
    {
       public string MsgRetWS { get; private set; }
       /*
      * ==============================================================================
      * Transmissao XML - NFe [ NOTA FISCAL ELETRÔNICA ]
      * ==============================================================================
      * Descrição..: Classe responsável por transmitir as mensagem aos WebServices
      *              - Cancelamento;
      *              - Consulta;
      *              - Inutilização;  
      *              - Recepcao;  
      *              - RetRecepcao;  
      *              - Status do Serviço;
      * 
      * ------------------------------------------------------------------------------
      * Definição..: NfeConsulta( siglaUF, TipoAmb, TitularCert, MsgCabec, MsgDados, 
      *              MsgRetWS ) 
      * 
      * Definição..: NfeCancelamento( siglaUF, TipoAmb, TitularCert, MsgCabec, MsgDados, 
      *              MsgRetWS ) 
      * 
      * Definição..: NfeInutilizacao( siglaUF, TipoAmb, TitularCert, MsgCabec, MsgDados, 
      *              MsgRetWS ) 
      * 
      * Definição..: NfeRecepcao( siglaUF, TipoAmb, TitularCert, MsgCabec, MsgDados, 
      *              MsgRetWS ) 
      *              
      * Definição..: NfeRetRecepcao( siglaUF, TipoAmb, TitularCert, MsgCabec, MsgDados, 
      *              MsgRetWS ) 
      *
      * Definição..: NfeStatusServico( siglaUF, TipoAmb, TitularCert, MsgCabec, MsgDados, 
      *              MsgRetWS )               
      * 
      * ------------------------------------------------------------------------------
      * Retorno....: Atualiza uma propriedade da classe:
      *              MsgRetWS - em caso de algum erro, esta propriedade vai
      *                                 receber um texto contendo o erro para ser
      *                                 analisado, em caso de sucesso essa propriedade
      *                                 vai receber a Resposta do WebService. 
      * ------------------------------------------------------------------------------
      * Exemplos...:
      * 
      * 
      * ------------------------------------------------------------------------------
      * Notas......:
      * 
      * ==============================================================================         
      */

       public bool NfeConsulta(string siglaUF, int TipoAmb, string TitularCert, string MsgCabec, string MsgDados, out string MsgRetWS)
        {
          bool Retorno;
          string xTitular;

          MsgRetWS = "";
          
          // Declara o Objeto principal do WebService via Classe Proxy NFeConsulta... 
          object oServico = null;

          switch (TipoAmb) {
              case 1: {
                      switch (siglaUF)
                      {
                          case "SP":{oServico = new wsConsultaProdSP.NfeConsulta();} break;
                          case "CE":{oServico = new wsConsultaProdCE.NfeConsulta();} break;
                          case "MG":{oServico = new wsConsultaProdMG.NfeConsulta();} break;
                          case "PR": { oServico = new wsConsultaProdRS.NfeConsulta();} break;
                          case "PE": { oServico = new wsConsultaProdPE.NfeConsulta(); } break;
                          case "ES": { oServico = new wsConsultaProdES.NfeConsulta(); } break;
                          case "SV":{oServico = new wsConsultaProdSV.NfeConsulta();} break;
                          case "SCAN":{oServico = new wsConsultaProdSCAN.NfeConsulta();} break;
                          case "GO":{oServico = new wsConsultaProdGO.NfeConsulta();} break;
                          case "RS":{oServico = new wsConsultaProdRS.NfeConsulta();} break;
                          case "SC": { oServico = new wsConsultaProdSVRS.NfeConsulta(); } break;
                      }
                  }
              break;

              case 2: {
                      switch (siglaUF)
                      {
                          case "SP":{oServico = new wsConsultaHomSP.NfeConsulta();} break;
                          case "CE":{oServico = new wsConsultaHomCE.NfeConsulta();} break;
                          case "MG":{oServico = new wsConsultaHomMG.NfeConsulta();} break;
                          case "PR": { oServico = new wsConsultaHomRS.NfeConsulta();} break;
                          case "MT": { oServico = new wsConsultaHomMT.NfeConsulta(); } break;
                          case "PE": { oServico = new wsConsultaHomPE.NfeConsulta(); } break;
                          case "SV":{oServico = new wsConsultaHomSV.NfeConsulta();} break;
                          case "SCAN":{oServico = new wsConsultaHomSCAN.NfeConsulta();} break;                           
                          case "RS":{oServico = new wsConsultaHomRS.NfeConsulta();} break;
                          case "SC": { oServico = new wsConsultaHomRS.NfeConsulta(); } break;
                      }
                  }
              break;
          }

          Type oWS_NfeConsulta = oServico.GetType();

          // se a variável do TitularCert não estiver vazia então usa o Certificado em questão senão abre 
          // repositório de Certificados...
          X509Certificate oX509Cert = new X509Certificate();

          xTitular = "";
          if (TitularCert != "")
          {
                xTitular = TitularCert;
          }
          
          X509Certificate2 _X509Cert = new X509Certificate2();
          X509Store store = new X509Store("MY", StoreLocation.CurrentUser);
          store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);
          X509Certificate2Collection collection = (X509Certificate2Collection)store.Certificates;
          X509Certificate2Collection collection1 = (X509Certificate2Collection)collection.Find(X509FindType.FindBySubjectDistinguishedName, xTitular, false);

          if (collection1.Count == 0)
          {
              MsgRetWS = "Problemas no certificado digital";
              Retorno = false;
              return Retorno;
          }
          else
          {
              oX509Cert = collection1[0];
          }

            //  oWS_NfeConsulta.ClientCertificates.Add(oX509Cert);
           object oClientCertificates;
           Type tipoClientCertificates;

           oClientCertificates = oWS_NfeConsulta.InvokeMember("ClientCertificates", System.Reflection.BindingFlags.GetProperty, null, oServico, new Object[] { });
           tipoClientCertificates = oClientCertificates.GetType();
           tipoClientCertificates.InvokeMember("Add", System.Reflection.BindingFlags.InvokeMethod, null, oClientCertificates, new Object[] { oX509Cert });
                     
          
          //
          try
          {
             // MsgRetWS = oWS_NfeConsulta.nfeConsultaNF(MsgCabec, MsgDados);
              MsgRetWS = (string)(oWS_NfeConsulta.InvokeMember("nfeConsultaNF", System.Reflection.BindingFlags.InvokeMethod, null, oServico, new Object[] { MsgCabec, MsgDados }));
              Retorno = true;
              return Retorno;
          }
          catch (Exception ex)
          {
              MsgRetWS = ex.ToString();
              Retorno = false;
              return Retorno;
          }

        }

       public bool NfeCancelamento(string siglaUF, int TipoAmb, string TitularCert, string MsgCabec, string MsgDados, out string MsgRetWS)
       {
           bool Retorno;
           string xTitular;

           MsgRetWS = "";

           // Declara o Objeto principal do WebService via Classe Proxy NFeConsulta...
           object oServico = null;
           switch (TipoAmb) {
               case 1: {
                       switch (siglaUF)
                       {
                           case "SP":{oServico = new wsCancelamentoProdSP.NfeCancelamento();} break;
                           case "CE":{oServico = new wsCancelamentoProdCE.NfeCancelamento();} break;
                           case "MG":{oServico = new wsCancelamentoProdMG.NfeCancelamento();} break;
                           case "PR": { oServico = new wsCancelamentoProdRS.NfeCancelamento(); } break;
                           case "PE": { oServico = new wsCancelamentoProdPE.NfeCancelamento(); } break;
                           case "ES": { oServico = new wsCancelamentoProdES.NfeCancelamento(); } break;
                           case "SV":{oServico = new wsCancelamentoProdSV.NfeCancelamento();} break;
                           case "SCAN":{oServico = new wsCancelamentoProdSCAN.NfeCancelamento();} break;
                           case "GO":{oServico = new wsCancelamentoProdGO.NfeCancelamento();} break;
                           case "RS":{oServico = new wsCancelamentoProdRS.NfeCancelamento();} break;
                           case "SC": { oServico = new wsCancelamentoProdSVRS.NfeCancelamento(); } break;
                       }
                   }
                   break;

               case 2: {
                       switch (siglaUF)
                       {
                           case "SP":{oServico = new wsCancelamentoHomSP.NfeCancelamento();} break;
                           case "CE":{oServico = new wsCancelamentoHomCE.NfeCancelamento();} break;
                           case "MG":{oServico = new wsCancelamentoHomMG.NfeCancelamento();} break;
                           case "PR": { oServico = new wsCancelamentoHomRS.NfeCancelamento(); } break;
                           case "MT": { oServico = new wsCancelamentoHomMT.NfeCancelamento(); } break;
                           case "PE": { oServico = new wsCancelamentoHomPE.NfeCancelamento(); } break;
                           case "SV":{oServico = new wsCancelamentoHomSV.NfeCancelamento();} break;
                           case "SCAN":{oServico = new wsCancelamentoHomSCAN.NfeCancelamento();} break;
                           case "RS":{oServico = new wsCancelamentoHomRS.NfeCancelamento();} break;
                           case "SC": { oServico = new wsCancelamentoHomRS.NfeCancelamento(); } break;
                       }
                   }
                   break;

           }

           Type oWS_NfeCancelamento = oServico.GetType();

           // se a variável do TitularCert não estiver vazia então usa o Certificado em questão senão abre 
           // repositório de Certificados...
           X509Certificate oX509Cert = new X509Certificate();

           xTitular = "";
           if (TitularCert != "")
           {
               xTitular = TitularCert;
           }

           X509Certificate2 _X509Cert = new X509Certificate2();
           X509Store store = new X509Store("MY", StoreLocation.CurrentUser);
           store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);
           X509Certificate2Collection collection = (X509Certificate2Collection)store.Certificates;
           X509Certificate2Collection collection1 = (X509Certificate2Collection)collection.Find(X509FindType.FindBySubjectDistinguishedName, xTitular, false);

           if (collection1.Count == 0)
           {
               MsgRetWS = "Problemas no certificado digital";
               Retorno = false;
               return Retorno;
           }
           else
           {
               oX509Cert = collection1[0];
           }

          // oWS_NfeCancelamento.ClientCertificates.Add(oX509Cert);
           object oClientCertificates;
           Type tipoClientCertificates;

           oClientCertificates = oWS_NfeCancelamento.InvokeMember("ClientCertificates", System.Reflection.BindingFlags.GetProperty, null, oServico, new Object[] { });
           tipoClientCertificates = oClientCertificates.GetType();
           tipoClientCertificates.InvokeMember("Add", System.Reflection.BindingFlags.InvokeMethod, null, oClientCertificates, new Object[] { oX509Cert });

           //
           try
           {
               //MsgRetWS = oWS_NfeCancelamento.nfeCancelamentoNF(MsgCabec, MsgDados);
               MsgRetWS = (string)(oWS_NfeCancelamento.InvokeMember("nfeCancelamentoNF", System.Reflection.BindingFlags.InvokeMethod, null, oServico, new Object[] { MsgCabec, MsgDados }));
               Retorno = true;
               return Retorno;
           }
           catch (Exception ex)
           {
               MsgRetWS = ex.ToString();
               Retorno = false;
               return Retorno;
           }

       }

       public bool NfeInutilizacao(string siglaUF, int TipoAmb, string TitularCert, string MsgCabec, string MsgDados, out string MsgRetWS)
       {
           bool Retorno;
           string xTitular;

           MsgRetWS = "";

           // Declara o Objeto principal do WebService via Classe Proxy NFeConsulta...
           object oServico = null;
           switch (TipoAmb) {
               case 1: {
                       switch (siglaUF)
                       {
                           case "SP":{oServico = new wsInutilizacaoProdSP.NfeInutilizacao();} break;
                           case "CE":{oServico = new wsInutilizacaoProdCE.NfeInutilizacao();} break;
                           case "MG":{oServico = new wsInutilizacaoProdMG.NfeInutilizacao();} break;
                           case "PR": { oServico = new wsInutilizacaoProdPE.NfeInutilizacao(); } break;
                           case "PE": { oServico = new wsInutilizacaoProdPE.NfeInutilizacao(); } break;
                           case "ES": { oServico = new wsInutilizacaoProdES.NfeInutilizacao(); } break;
                           case "SV":{oServico = new wsInutilizacaoProdSV.NfeInutilizacao();} break;
                           case "SCAN":{oServico = new wsInutilizacaoProdSCAN.NfeInutilizacao();} break;
                           case "GO":{oServico = new wsInutilizacaoProdGO.NfeInutilizacao();} break;
                           case "RS":{oServico = new wsInutilizacaoProdRS.NfeInutilizacao();} break;
                           case "SC": { oServico = new wsInutilizacaoProdSVRS.NfeInutilizacao(); } break;
                       }
                   }
                   break;

               case 2: {
                       switch (siglaUF)
                       {
                           case "SP":{oServico = new wsInutilizacaoHomSP.NfeInutilizacao();} break;
                           case "CE":{oServico = new wsInutilizacaoHomCE.NfeInutilizacao();} break;
                           case "MG":{oServico = new wsInutilizacaoHomMG.NfeInutilizacao();} break;
                           case "PR": { oServico = new wsInutilizacaoHomPE.NfeInutilizacao(); } break;
                           case "MT": { oServico = new wsInutilizacaoHomMT.NfeInutilizacao(); } break;
                           case "PE": { oServico = new wsInutilizacaoHomPE.NfeInutilizacao(); } break;
                           case "SV": {oServico = new wsInutilizacaoHomSV.NfeInutilizacao();} break;
                           case "SCAN": {oServico = new wsInutilizacaoHomSCAN.NfeInutilizacao();} break;
                           case "RS":{oServico = new wsInutilizacaoHomRS.NfeInutilizacao();} break;
                           case "SC": { oServico = new wsInutilizacaoHomRS.NfeInutilizacao(); } break;
                       }
                   }
                   break;
           }

           Type oWS_NfeInutilizacao = oServico.GetType();

           // se a variável do TitularCert não estiver vazia então usa o Certificado em questão senão abre 
           // repositório de Certificados...
           X509Certificate oX509Cert = new X509Certificate();

           xTitular = "";
           if (TitularCert != "")
           {
               xTitular = TitularCert;
           }

           X509Certificate2 _X509Cert = new X509Certificate2();
           X509Store store = new X509Store("MY", StoreLocation.CurrentUser);
           store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);
           X509Certificate2Collection collection = (X509Certificate2Collection)store.Certificates;
           X509Certificate2Collection collection1 = (X509Certificate2Collection)collection.Find(X509FindType.FindBySubjectDistinguishedName, xTitular, false);

           if (collection1.Count == 0)
           {
               MsgRetWS = "Problemas no certificado digital";
               Retorno = false;
               return Retorno;
           }
           else
           {
               oX509Cert = collection1[0];
           }

          // oWS_NfeInutilizacao.ClientCertificates.Add(oX509Cert);
           object oClientCertificates;
           Type tipoClientCertificates;

           oClientCertificates = oWS_NfeInutilizacao.InvokeMember("ClientCertificates", System.Reflection.BindingFlags.GetProperty, null, oServico, new Object[] { });
           tipoClientCertificates = oClientCertificates.GetType();
           tipoClientCertificates.InvokeMember("Add", System.Reflection.BindingFlags.InvokeMethod, null, oClientCertificates, new Object[] { oX509Cert });

           try
           {
          //     MsgRetWS = oWS_NfeInutilizacao.nfeInutilizacaoNF(MsgCabec, MsgDados);
               MsgRetWS = (string)(oWS_NfeInutilizacao.InvokeMember("nfeInutilizacaoNF", System.Reflection.BindingFlags.InvokeMethod, null, oServico, new Object[] { MsgCabec, MsgDados }));
               Retorno = true;
               return Retorno;
           }
           catch (Exception ex)
           {
               MsgRetWS = ex.ToString();
               Retorno = false;
               return Retorno;
           }

       }

       public bool NfeRecepcao(string siglaUF, int TipoAmb, string TitularCert, string MsgCabec, string MsgDados, out string MsgRetWS)
       {
           /*
           bool Retorno;
           string xTitular;

           MsgRetWS = "";

           // Declara o Objeto principal do WebService via Classe Proxy NFeConsulta...
           wsRecepcaoHomSP.NfeRecepcao oWS_NfeRecepcao = new wsRecepcaoHomSP.NfeRecepcao();

           // se a variável do TitularCert não estiver vazia então usa o Certificado em questão senão abre 
           // repositório de Certificados...
           X509Certificate oX509Cert = new X509Certificate();

           xTitular = "";
           if (TitularCert != "")
           {
               xTitular = TitularCert;
           }

           X509Certificate2 _X509Cert = new X509Certificate2();
           X509Store store = new X509Store("MY", StoreLocation.CurrentUser);
           store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);
           X509Certificate2Collection collection = (X509Certificate2Collection)store.Certificates;
           X509Certificate2Collection collection1 = (X509Certificate2Collection)collection.Find(X509FindType.FindBySubjectDistinguishedName, xTitular, false);

           if (collection1.Count == 0)
           {
               MsgRetWS = "Problemas no certificado digital";
               Retorno = false;
               return Retorno;
           }
           else
           {
               oX509Cert = collection1[0];
           }

           oWS_NfeRecepcao.ClientCertificates.Add(oX509Cert);

           //
           try
           {
               MsgRetWS = oWS_NfeRecepcao.nfeRecepcaoLote(MsgCabec, MsgDados);
               Retorno = true;
               return Retorno;
           }
           catch (Exception ex)
           {
               MsgRetWS = ex.ToString();
               Retorno = false;
               return Retorno;
           }*/

           bool Retorno;
           string xTitular;

           MsgRetWS = "";

           // Declara o Objeto principal do WebService via Classe Proxy NFeConsulta... 
           object oServico = null;

           switch (TipoAmb)
           {
               case 1:
                   {
                       switch (siglaUF)
                       {
                           case "SP": { oServico = new wsRecepcaoProdSP.NfeRecepcao(); } break;
                           case "CE": { oServico = new wsRecepcaoProdCE.NfeRecepcao(); } break;
                           case "MG": { oServico = new wsRecepcaoProdMG.NfeRecepcao(); } break;
                           case "PR": { oServico = new wsRecepcaoProdRS.NfeRecepcao(); } break;
                           case "SV": { oServico = new wsRecepcaoProdSV.NfeRecepcao(); } break;
                           case "PE": { oServico = new wsRecepcaoProdPE.NfeRecepcao(); } break;
                           case "ES": { oServico = new wsRecepcaoProdES.NfeRecepcao(); } break;
                           case "SCAN": { oServico = new wsRecepcaoProdSCAN.NfeRecepcao(); } break;
                           case "GO": { oServico = new wsRecepcaoProdGO.NfeRecepcao(); } break;
                           case "RS": { oServico = new wsRecepcaoProdRS.NfeRecepcao(); } break;
                           case "SC": { oServico = new wsRecepcaoProdSVRS.NfeRecepcao(); } break;
                       }
                   }
                   break;

               case 2:
                   {
                       switch (siglaUF)
                       {
                           case "SP": { oServico = new wsRecepcaoHomSP.NfeRecepcao(); } break;
                           case "CE": { oServico = new wsRecepcaoHomCE.NfeRecepcao(); } break;
                           case "MG": { oServico = new wsRecepcaoHomMG.NfeRecepcao(); } break;
                           case "MT": { oServico = new wsRecepcaoHomMT.NfeRecepcao(); } break;
                           case "PR": { oServico = new wsRecepcaoHomRS.NfeRecepcao(); } break;
                           case "SV": { oServico = new wsRecepcaoHomSV.NfeRecepcao(); } break;
                           case "PE": { oServico = new wsRecepcaoHomPE.NfeRecepcao(); } break;
                           case "SCAN": { oServico = new wsRecepcaoHomSCAN.NfeRecepcao(); } break;
                           case "RS": { oServico = new wsRecepcaoHomRS.NfeRecepcao(); } break;
                       }
                   }
                   break;
           }

           Type oWS_NfeRecepcao = oServico.GetType();

           // se a variável do TitularCert não estiver vazia então usa o Certificado em questão senão abre 
           // repositório de Certificados...
           X509Certificate oX509Cert = new X509Certificate();

           xTitular = "";
           if (TitularCert != "")
           {
               xTitular = TitularCert;
           }

           X509Certificate2 _X509Cert = new X509Certificate2();
           X509Store store = new X509Store("MY", StoreLocation.CurrentUser);
           store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);
           X509Certificate2Collection collection = (X509Certificate2Collection)store.Certificates;
           X509Certificate2Collection collection1 = (X509Certificate2Collection)collection.Find(X509FindType.FindBySubjectDistinguishedName, xTitular, false);

           if (collection1.Count == 0)
           {
               MsgRetWS = "Problemas no certificado digital";
               Retorno = false;
               return Retorno;
           }
           else
           {
               oX509Cert = collection1[0];
           }

           //  oWS_NfeConsulta.ClientCertificates.Add(oX509Cert);
           object oClientCertificates;
           Type tipoClientCertificates;

           oClientCertificates = oWS_NfeRecepcao.InvokeMember("ClientCertificates", System.Reflection.BindingFlags.GetProperty, null, oServico, new Object[] { });
           tipoClientCertificates = oClientCertificates.GetType();
           tipoClientCertificates.InvokeMember("Add", System.Reflection.BindingFlags.InvokeMethod, null, oClientCertificates, new Object[] { oX509Cert });


           //
           try
           {
               // MsgRetWS = oWS_NfeConsulta.nfeConsultaNF(MsgCabec, MsgDados);
               MsgRetWS = (string)(oWS_NfeRecepcao.InvokeMember("nfeRecepcaoLote", System.Reflection.BindingFlags.InvokeMethod, null, oServico, new Object[] { MsgCabec, MsgDados }));
               Retorno = true;
               return Retorno;
           }
           catch (Exception ex)
           {
               MsgRetWS = ex.ToString();
               Retorno = false;
               return Retorno;
           }
        
       }

       public bool NfeRetRecepcao(string siglaUF, int TipoAmb, string TitularCert, string MsgCabec, string MsgDados, out string MsgRetWS)
       {
           bool Retorno;
           string xTitular;

           MsgRetWS = "";

           // Declara o Objeto principal do WebService via Classe Proxy NFeConsulta...
           object oServico = null;
           switch (TipoAmb){
               case 1: {
                       switch (siglaUF)
                       {
                           case "SP":{oServico = new wsRetRecepcaoProdSP.NfeRetRecepcao();} break;
                           case "CE":{oServico = new wsRetRecepcaoProdCE.NfeRetRecepcao();} break;
                           case "MG":{oServico = new wsRetRecepcaoProdMG.NfeRetRecepcao();} break;
                           case "PR": { oServico = new wsRetRecepcaoProdRS.NfeRetRecepcao(); } break;
                           case "ES": { oServico = new wsRetRecepcaoProdES.NfeRetRecepcao(); } break;
                           case "PE": { oServico = new wsRetRecepcaoProdPE.NfeRetRecepcao(); } break;
                           case "SV":{oServico = new wsRetRecepcaoProdSV.NfeRetRecepcao();} break;
                           case "SCAN":{oServico = new wsRetRecepcaoProdSCAN.NfeRetRecepcao();} break;
                           case "GO":{oServico = new wsRetRecepcaoProdGO.NfeRetRecepcao();} break;
                           case "RS":{oServico = new wsRetRecepcaoProdRS.NfeRetRecepcao();} break;
                           case "SC": { oServico = new wsRetRecepcaoProdSVRS.NfeRetRecepcao(); } break;

                       }
                   }
                   break;

               case 2: {
                       switch (siglaUF)
                       {
                           case "SP":{oServico = new wsRetRecepcaoHomSP.NfeRetRecepcao();} break;
                           case "CE":{oServico = new wsRetRecepcaoHomCE.NfeRetRecepcao();} break;
                           case "MG":{oServico = new wsRetRecepcaoHomMG.NfeRetRecepcao();} break;
                           case "PR": { oServico = new wsRetRecepcaoHomRS.NfeRetRecepcao(); } break;
                           case "MT": { oServico = new wsRetRecepcaoHomMT.NfeRetRecepcao(); } break;
                           case "PE": { oServico = new wsRetRecepcaoHomPE.NfeRetRecepcao(); } break;
                           case "SV":{oServico = new wsRetRecepcaoHomSV.NfeRetRecepcao();} break;
                           case "SCAN":{oServico = new wsRetRecepcaoHomSCAN.NfeRetRecepcao();} break;
                           case "RS":{oServico = new wsRetRecepcaoHomRS.NfeRetRecepcao();} break;
                           case "SC": { oServico = new wsRetRecepcaoHomRS.NfeRetRecepcao(); } break;
                       }
                   }
                   break;
           }

           Type oWS_NfeRetRecepcao = oServico.GetType();

           // se a variável do TitularCert não estiver vazia então usa o Certificado em questão senão abre 
           // repositório de Certificados...
           X509Certificate oX509Cert = new X509Certificate();

           xTitular = "";
           if (TitularCert != "")
           {
               xTitular = TitularCert;
           }

           X509Certificate2 _X509Cert = new X509Certificate2();
           X509Store store = new X509Store("MY", StoreLocation.CurrentUser);
           store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);
           X509Certificate2Collection collection = (X509Certificate2Collection)store.Certificates;
           X509Certificate2Collection collection1 = (X509Certificate2Collection)collection.Find(X509FindType.FindBySubjectDistinguishedName, xTitular, false);

           if (collection1.Count == 0)
           {
               MsgRetWS = "Problemas no certificado digital";
               Retorno = false;
               return Retorno;
           }
           else
           {
               oX509Cert = collection1[0];
           }

          // oWS_NfeRetRecepcao.ClientCertificates.Add(oX509Cert);
           object oClientCertificates;
           Type tipoClientCertificates;

           oClientCertificates = oWS_NfeRetRecepcao.InvokeMember("ClientCertificates", System.Reflection.BindingFlags.GetProperty, null, oServico, new Object[] { });
           tipoClientCertificates = oClientCertificates.GetType();
           tipoClientCertificates.InvokeMember("Add", System.Reflection.BindingFlags.InvokeMethod, null, oClientCertificates, new Object[] { oX509Cert });

           //
           try
           {
           //  MsgRetWS = oWS_NfeRetRecepcao.nfeRetRecepcao(MsgCabec, MsgDados);
               MsgRetWS = (string)(oWS_NfeRetRecepcao.InvokeMember("nfeRetRecepcao", System.Reflection.BindingFlags.InvokeMethod, null, oServico, new Object[] { MsgCabec, MsgDados }));
               Retorno = true;
               return Retorno;
           }
           catch (Exception ex)
           {
               MsgRetWS = ex.ToString();
               Retorno = false;
               return Retorno;
           }

       }

       public bool NfeStatusServico(string siglaUF, int TipoAmb, string TitularCert, string MsgCabec, string MsgDados, out string MsgRetWS)
       {
           bool Retorno;
           string xTitular;

           MsgRetWS = "";

           // Declara o Objeto principal do WebService via Classe Proxy NFeConsulta...
           object oServico = null;
           switch (TipoAmb){
               case 1:{
                       switch (siglaUF)
                       {
                           case "SP":{oServico = new wsStatusServicoProdSP.NfeStatusServico();} break;
                           case "CE":{oServico = new wsStatusServicoProdCE.NfeStatusServico();} break;
                           case "MG":{oServico = new wsStatusServicoProdMG.NfeStatusServico();} break;
                           case "PR": { oServico = new wsStatusServicoProdRS.NfeStatusServico(); } break;
                           case "ES": { oServico = new wsStatusServicoProdES.NfeStatusServico(); } break;
                           case "SV":{oServico = new wsStatusServicoProdSV.NfeStatusServico();} break;
                           case "PE": { oServico = new wsStatusServicoProdPE.NfeStatusServico(); } break;
                           case "SCAN":{oServico = new wsStatusServicoProdSCAN.NfeStatusServico();} break;
                           case "GO":{oServico = new wsStatusServicoProdGO.NfeStatusServico();} break;
                           case "RS":{oServico = new wsStatusServicoProdRS.NfeStatusServico();} break;
                           case "SC": { oServico = new wsStatusServicoProdSVRS.NfeStatusServico(); } break;

                       }
                   }
                   break;
               case 2:{
                       switch (siglaUF)
                       {
                           case "SP":{oServico = new wsStatusServicoHomSP.NfeStatusServico();} break;
                           case "CE":{oServico = new wsStatusServicoHomCE.NfeStatusServico();} break;
                           case "MG":{oServico = new wsStatusServicoHomMG.NfeStatusServico();} break;
                           case "PR": { oServico = new wsStatusServicoHomRS.NfeStatusServico(); } break;
                           case "MT": { oServico = new wsStatusServicoHomMT.NfeStatusServico(); } break;
                           case "SV":{oServico = new wsStatusServicoHomSV.NfeStatusServico();} break;
                           case "PE": { oServico = new wsStatusServicoHomPE.NfeStatusServico(); } break;
                           case "SCAN":{oServico = new wsStatusServicoHomSCAN.NfeStatusServico();} break;
                           case "RS":{oServico = new wsStatusServicoHomRS.NfeStatusServico();} break;
                           case "SC": { oServico = new wsStatusServicoHomRS.NfeStatusServico(); } break;
                       }
                   }
                   break;
           }

           Type oWS_NfeStatusServico = oServico.GetType();

           // se a variável do TitularCert não estiver vazia então usa o Certificado em questão senão abre 
           // repositório de Certificados...
           X509Certificate oX509Cert = new X509Certificate();

           xTitular = "";
           if (TitularCert != "")
           {
               xTitular = TitularCert;
           }

           X509Certificate2 _X509Cert = new X509Certificate2();
           X509Store store = new X509Store("MY", StoreLocation.CurrentUser);
           store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);
           X509Certificate2Collection collection = (X509Certificate2Collection)store.Certificates;
           X509Certificate2Collection collection1 = (X509Certificate2Collection)collection.Find(X509FindType.FindBySubjectDistinguishedName, xTitular, false);

           if (collection1.Count == 0)
           {
               MsgRetWS = "Problemas no certificado digital";
               Retorno = false;
               return Retorno;
           }
           else
           {
               oX509Cert = collection1[0];
           }

       //    oWS_NfeStatusServico.ClientCertificates.Add(oX509Cert);
           object oClientCertificates;
           Type tipoClientCertificates;

           oClientCertificates = oWS_NfeStatusServico.InvokeMember("ClientCertificates", System.Reflection.BindingFlags.GetProperty, null, oServico, new Object[] { });
           tipoClientCertificates = oClientCertificates.GetType();
           tipoClientCertificates.InvokeMember("Add", System.Reflection.BindingFlags.InvokeMethod, null, oClientCertificates, new Object[] { oX509Cert });

           try
           {
         //      MsgRetWS = oWS_NfeStatusServico.nfeStatusServicoNF(MsgCabec, MsgDados);
               MsgRetWS = (string)(oWS_NfeStatusServico.InvokeMember("nfeStatusServicoNF", System.Reflection.BindingFlags.InvokeMethod, null, oServico, new Object[] { MsgCabec, MsgDados }));
               Retorno = true;
               return Retorno;
           }
           catch (Exception ex)
           {
               MsgRetWS = ex.ToString();
               Retorno = false;
               return Retorno;
           }

       }

    }

}

