object FmFatPedNFsEdit: TFmFatPedNFsEdit
  Left = 339
  Top = 185
  Caption = 'FAT-PEDID-006 :: Edi'#231#227'o de Nota Fiscal'
  ClientHeight = 579
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 483
    Align = alClient
    TabOrder = 0
    object GroupBox1: TGroupBox
      Left = 1
      Top = 1
      Width = 782
      Height = 359
      Align = alClient
      TabOrder = 0
      object Splitter1: TSplitter
        Left = 2
        Top = 273
        Width = 778
        Height = 5
        Cursor = crVSplit
        Align = alTop
        ExplicitTop = 289
      end
      object DBMemo1: TDBMemo
        Left = 2
        Top = 141
        Width = 778
        Height = 40
        TabStop = False
        Align = alTop
        DataField = 'Descricao'
        DataSource = DsCFOP
        TabOrder = 0
      end
      object DBMemo2: TDBMemo
        Left = 2
        Top = 181
        Width = 778
        Height = 92
        TabStop = False
        Align = alTop
        DataField = 'Complementacao'
        DataSource = DsCFOP
        TabOrder = 1
      end
      object Panel3: TPanel
        Left = 2
        Top = 15
        Width = 778
        Height = 126
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 2
        object Label14: TLabel
          Left = 8
          Top = -1
          Width = 44
          Height = 13
          Caption = 'S'#233'rie NF:'
          FocusControl = DBEdit1
        end
        object Label15: TLabel
          Left = 60
          Top = -1
          Width = 61
          Height = 13
          Caption = #218'lt. NF emit.:'
          FocusControl = DBEdit2
        end
        object Label16: TLabel
          Left = 128
          Top = -1
          Width = 45
          Height = 13
          Caption = 'N'#186' Limite:'
          FocusControl = DBEdit3
        end
        object Label3: TLabel
          Left = 196
          Top = -1
          Width = 71
          Height = 13
          Caption = 'N'#186' Nota Fiscal:'
        end
        object Label1: TLabel
          Left = 272
          Top = 0
          Width = 36
          Height = 13
          Caption = '$ Frete:'
        end
        object Label2: TLabel
          Left = 352
          Top = 0
          Width = 46
          Height = 13
          Caption = '$ Seguro:'
        end
        object Label10: TLabel
          Left = 432
          Top = 0
          Width = 74
          Height = 13
          Caption = '$ Desp. acess.:'
        end
        object Label4: TLabel
          Left = 8
          Top = 44
          Width = 58
          Height = 13
          Caption = 'Quantidade:'
        end
        object Label5: TLabel
          Left = 98
          Top = 44
          Width = 41
          Height = 13
          Caption = 'Esp'#233'cie:'
        end
        object Label6: TLabel
          Left = 276
          Top = 44
          Width = 33
          Height = 13
          Caption = 'Marca:'
        end
        object Label7: TLabel
          Left = 454
          Top = 44
          Width = 40
          Height = 13
          Caption = 'N'#250'mero:'
        end
        object Label8: TLabel
          Left = 632
          Top = 44
          Width = 51
          Height = 13
          Caption = 'Peso buto:'
        end
        object Label9: TLabel
          Left = 704
          Top = 44
          Width = 62
          Height = 13
          Caption = 'Peso l'#237'quido:'
        end
        object Label17: TLabel
          Left = 8
          Top = 84
          Width = 161
          Height = 13
          Caption = 'CFOP Geral 1:  (obsoleto na NF-e)'
        end
        object Label18: TLabel
          Left = 512
          Top = -1
          Width = 56
          Height = 13
          Caption = 'Emiss'#227'o NF'
        end
        object Label19: TLabel
          Left = 644
          Top = -1
          Width = 79
          Height = 13
          Caption = 'Sa'#237'da / entrada:'
        end
        object DBEdit1: TDBEdit
          Left = 8
          Top = 15
          Width = 48
          Height = 21
          TabStop = False
          DataField = 'NO_SerieNF'
          DataSource = DsImprime
          TabOrder = 0
        end
        object DBEdit2: TDBEdit
          Left = 60
          Top = 15
          Width = 64
          Height = 21
          TabStop = False
          DataField = 'Sequencial'
          DataSource = DsImprime
          TabOrder = 1
        end
        object DBEdit3: TDBEdit
          Left = 128
          Top = 15
          Width = 64
          Height = 21
          TabStop = False
          DataField = 'MaxSeqLib'
          DataSource = DsImprime
          TabOrder = 2
        end
        object EdNumeroNF: TdmkEdit
          Left = 196
          Top = 15
          Width = 72
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
        object EdFreteVal: TdmkEdit
          Left = 272
          Top = 15
          Width = 76
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
        end
        object EdSeguro: TdmkEdit
          Left = 352
          Top = 15
          Width = 76
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
        end
        object EdOutros: TdmkEdit
          Left = 432
          Top = 15
          Width = 76
          Height = 21
          Alignment = taRightJustify
          TabOrder = 6
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
        end
        object EdQuantidade: TdmkEdit
          Left = 8
          Top = 60
          Width = 86
          Height = 21
          Alignment = taRightJustify
          TabOrder = 9
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
        object EdEspecie: TdmkEdit
          Left = 98
          Top = 60
          Width = 172
          Height = 21
          MaxLength = 60
          TabOrder = 10
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
        object EdMarca: TdmkEdit
          Left = 276
          Top = 60
          Width = 172
          Height = 21
          MaxLength = 60
          TabOrder = 11
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
        object EdNumero: TdmkEdit
          Left = 454
          Top = 60
          Width = 172
          Height = 21
          MaxLength = 60
          TabOrder = 12
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
        object EdkgBruto: TdmkEdit
          Left = 632
          Top = 60
          Width = 68
          Height = 21
          Alignment = taRightJustify
          TabOrder = 13
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
        end
        object EdkgLiqui: TdmkEdit
          Left = 704
          Top = 60
          Width = 68
          Height = 21
          Alignment = taRightJustify
          TabOrder = 14
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
        end
        object EdCFOP1: TdmkEditCB
          Left = 8
          Top = 100
          Width = 56
          Height = 21
          TabOrder = 15
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'CFOP1'
          UpdCampo = 'CFOP1'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = '0'
          DBLookupComboBox = CBCFOP1
        end
        object CBCFOP1: TdmkDBLookupComboBox
          Left = 65
          Top = 100
          Width = 708
          Height = 21
          KeyField = 'CFOP'
          ListField = 'Nome'
          ListSource = DsCFOP
          TabOrder = 16
          dmkEditCB = EdCFOP1
          QryCampo = 'CFOP1'
          UpdType = utYes
        end
        object TPDtEmissNF: TdmkEditDateTimePicker
          Left = 512
          Top = 15
          Width = 130
          Height = 21
          Date = 40002.587303645840000000
          Time = 40002.587303645840000000
          TabOrder = 7
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
        object TPDtEntraSai: TdmkEditDateTimePicker
          Left = 644
          Top = 15
          Width = 130
          Height = 21
          Date = 40002.587380277780000000
          Time = 40002.587380277780000000
          TabOrder = 8
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
      end
      object MeinfAdic_infCpl: TdmkMemo
        Left = 2
        Top = 295
        Width = 778
        Height = 62
        Align = alClient
        TabOrder = 3
        UpdType = utYes
      end
      object StaticText1: TStaticText
        Left = 2
        Top = 278
        Width = 778
        Height = 17
        Align = alTop
        Alignment = taCenter
        Caption = 'Observa'#231#245'es adicionais NF-e '
        TabOrder = 4
        ExplicitWidth = 146
      end
    end
    object GroupBox2: TGroupBox
      Left = 1
      Top = 360
      Width = 782
      Height = 60
      Align = alBottom
      Caption = ' Ve'#237'culo: '
      TabOrder = 1
      object Label11: TLabel
        Left = 12
        Top = 16
        Width = 30
        Height = 13
        Caption = 'Placa:'
      end
      object Label12: TLabel
        Left = 96
        Top = 16
        Width = 17
        Height = 13
        Caption = 'UF:'
      end
      object Label13: TLabel
        Left = 272
        Top = 16
        Width = 358
        Height = 13
        Caption = 
          'Observa'#231#245'es: (Nome motorista, tel. doc., etc.) N'#227'o '#233' impresso na' +
          ' NF/NF-e!!'
      end
      object RNTC: TLabel
        Left = 128
        Top = 16
        Width = 71
        Height = 13
        Caption = 'RNTC (ANTT):'
      end
      object EdPlacaNr: TdmkEdit
        Left = 12
        Top = 32
        Width = 80
        Height = 21
        CharCase = ecUpperCase
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdPlacaUF: TdmkEdit
        Left = 96
        Top = 32
        Width = 28
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdObservacao: TdmkEdit
        Left = 272
        Top = 32
        Width = 381
        Height = 21
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdRNTC: TdmkEdit
        Left = 128
        Top = 32
        Width = 140
        Height = 21
        CharCase = ecUpperCase
        MaxLength = 20
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
    end
    object GroupBox3: TGroupBox
      Left = 1
      Top = 420
      Width = 782
      Height = 62
      Align = alBottom
      Caption = ' Exporta'#231#227'o: (NF-e: ZA - Informa'#231#245'es de com'#233'rcio exterior) '
      TabOrder = 2
      object Label20: TLabel
        Left = 12
        Top = 16
        Width = 17
        Height = 13
        Caption = 'UF:'
      end
      object Label21: TLabel
        Left = 44
        Top = 16
        Width = 224
        Height = 13
        Caption = 'Local onde ocorrer'#225' o embarque dos produtos: '
      end
      object EdUFEmbarq: TdmkEdit
        Left = 12
        Top = 32
        Width = 28
        Height = 21
        MaxLength = 2
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdxLocEmbarq: TdmkEdit
        Left = 44
        Top = 32
        Width = 480
        Height = 21
        MaxLength = 60
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
    end
  end
  object PainelConfirma: TPanel
    Left = 0
    Top = 531
    Width = 784
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      TabOrder = 0
      OnClick = BtOKClick
      NumGlyphs = 2
    end
    object Panel2: TPanel
      Left = 672
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Fechar'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    Caption = 'Edi'#231#227'o de Nota Fiscal'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 700
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
    object LaTipo: TdmkLabel
      Left = 701
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 620
      ExplicitTop = 2
      ExplicitHeight = 44
    end
  end
  object QrPesqPrc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Preco '
      'FROM tabeprcgrg'
      'WHERE Nivel1=:P0')
    Left = 548
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesqPrcPreco: TFloatField
      FieldName = 'Preco'
    end
  end
  object DsCFOP: TDataSource
    DataSet = QrCFOP
    Left = 464
    Top = 24
  end
  object QrCFOP: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COUNT(DISTINCT smva.ID) Itens, smva.CFOP, '
      'frc.OrdCFOPGer, cfp.Nome, Descricao, Complementacao'
      'FROM stqmovvala smva'
      'LEFT JOIN fisregcfop frc ON frc.Codigo=1 AND frc.CFOP=smva.CFOP'
      'LEFT JOIN cfop2003 cfp ON cfp.Codigo=smva.CFOP'
      'WHERE smva.Tipo=1'
      'AND smva.OriCodi=:P0'
      'AND smva.Empresa=:P1'
      'GROUP BY smva.CFOP'
      'ORDER BY frc.OrdCFOPGer DESC')
    Left = 436
    Top = 24
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrCFOPItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
    object QrCFOPCFOP: TWideStringField
      FieldName = 'CFOP'
      Required = True
      Size = 6
    end
    object QrCFOPOrdCFOPGer: TIntegerField
      FieldName = 'OrdCFOPGer'
    end
    object QrCFOPNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrCFOPDescricao: TWideMemoField
      FieldName = 'Descricao'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrCFOPComplementacao: TWideMemoField
      FieldName = 'Complementacao'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object QrFatPedNFs: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrFatPedNFsAfterScroll
    SQL.Strings = (
      'SELECT DISTINCT ent.Filial, ent.Codigo CO_ENT_EMP, '
      'IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NO_EMP, smna.*'
      'FROM stqmovvala smva'
      'LEFT JOIN entidades ent ON ent.Codigo=smva.Empresa'
      'LEFT JOIN stqmovnfsa smna ON smna.Empresa=smva.Empresa '
      '          AND smva.Tipo=1 '
      '          AND smna.OriCodi=:P0'
      'WHERE smva.Tipo=1'
      'AND smva.OriCodi=:P1'
      'AND ent.Codigo<>:P2'
      'ORDER BY ent.Filial')
    Left = 8
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrFatPedNFsFilial: TIntegerField
      FieldName = 'Filial'
      Origin = 'entidades.Filial'
      DisplayFormat = '000'
    end
    object QrFatPedNFsIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
      Origin = 'stqmovnfsa.IDCtrl'
      Required = True
    end
    object QrFatPedNFsTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'stqmovnfsa.Tipo'
    end
    object QrFatPedNFsOriCodi: TIntegerField
      FieldName = 'OriCodi'
      Origin = 'stqmovnfsa.OriCodi'
    end
    object QrFatPedNFsEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'stqmovnfsa.Empresa'
    end
    object QrFatPedNFsNumeroNF: TIntegerField
      FieldName = 'NumeroNF'
      Origin = 'stqmovnfsa.NumeroNF'
      DisplayFormat = '000000;-000000; '
    end
    object QrFatPedNFsIncSeqAuto: TSmallintField
      FieldName = 'IncSeqAuto'
      Origin = 'stqmovnfsa.IncSeqAuto'
    end
    object QrFatPedNFsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'stqmovnfsa.AlterWeb'
    end
    object QrFatPedNFsAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'stqmovnfsa.Ativo'
    end
    object QrFatPedNFsSerieNFCod: TIntegerField
      FieldName = 'SerieNFCod'
      Origin = 'stqmovnfsa.SerieNFCod'
    end
    object QrFatPedNFsSerieNFTxt: TWideStringField
      FieldName = 'SerieNFTxt'
      Origin = 'stqmovnfsa.SerieNFTxt'
      Size = 5
    end
    object QrFatPedNFsCO_ENT_EMP: TIntegerField
      FieldName = 'CO_ENT_EMP'
      Origin = 'entidades.Codigo'
    end
    object QrFatPedNFsDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'stqmovnfsa.DataCad'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrFatPedNFsDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'stqmovnfsa.DataAlt'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrFatPedNFsDataAlt_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DataAlt_TXT'
      Size = 10
      Calculated = True
    end
    object QrFatPedNFsFreteVal: TFloatField
      FieldName = 'FreteVal'
      Origin = 'stqmovnfsa.FreteVal'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrFatPedNFsSeguro: TFloatField
      FieldName = 'Seguro'
      Origin = 'stqmovnfsa.Seguro'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrFatPedNFsOutros: TFloatField
      FieldName = 'Outros'
      Origin = 'stqmovnfsa.Outros'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrFatPedNFsPlacaUF: TWideStringField
      FieldName = 'PlacaUF'
      Origin = 'stqmovnfsa.PlacaUF'
      Size = 2
    end
    object QrFatPedNFsPlacaNr: TWideStringField
      FieldName = 'PlacaNr'
      Origin = 'stqmovnfsa.PlacaNr'
      Size = 8
    end
    object QrFatPedNFsEspecie: TWideStringField
      FieldName = 'Especie'
      Origin = 'stqmovnfsa.Especie'
      Size = 30
    end
    object QrFatPedNFsMarca: TWideStringField
      FieldName = 'Marca'
      Origin = 'stqmovnfsa.Marca'
      Size = 30
    end
    object QrFatPedNFsNumero: TWideStringField
      FieldName = 'Numero'
      Origin = 'stqmovnfsa.Numero'
      Size = 30
    end
    object QrFatPedNFskgBruto: TFloatField
      FieldName = 'kgBruto'
      Origin = 'stqmovnfsa.kgBruto'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrFatPedNFskgLiqui: TFloatField
      FieldName = 'kgLiqui'
      Origin = 'stqmovnfsa.kgLiqui'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrFatPedNFsQuantidade: TWideStringField
      FieldName = 'Quantidade'
      Origin = 'stqmovnfsa.Quantidade'
      Size = 30
    end
    object QrFatPedNFsObservacao: TWideStringField
      FieldName = 'Observacao'
      Origin = 'stqmovnfsa.Observacao'
      Size = 255
    end
    object QrFatPedNFsCFOP1: TWideStringField
      FieldName = 'CFOP1'
      Origin = 'stqmovnfsa.CFOP1'
      Size = 6
    end
    object QrFatPedNFsNO_EMP: TWideStringField
      FieldName = 'NO_EMP'
      Size = 100
    end
  end
  object QrVolumes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COUNT(fpv.Ativo) Volumes, med.Nome NO_UnidMed'
      'FROM fatpedvol fpv'
      'LEFT JOIN unidmed med ON med.Codigo=fpv.UnidMed'
      'WHERE fpv.Codigo=:P0'
      'AND fpv.Ativo=1'
      'GROUP BY med.Codigo')
    Left = 36
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrVolumesVolumes: TLargeintField
      FieldName = 'Volumes'
      Required = True
    end
    object QrVolumesNO_UnidMed: TWideStringField
      FieldName = 'NO_UnidMed'
      Size = 30
    end
  end
  object QrImprime: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT imp.SerieNF CO_SerieNF, imp.TipoImpressao,'
      'nfs.SerieNF NO_SerieNF, nfs.Sequencial, nfs.IncSeqAuto, '
      'nfs.Controle  Ctrl_nfs, nfs.MaxSeqLib'
      'FROM imprime imp'
      'LEFT JOIN paramsnfs nfs ON nfs.Controle=imp.SerieNF'
      'WHERE imp.Codigo=:P0')
    Left = 64
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrImprimeCO_SerieNF: TIntegerField
      FieldName = 'CO_SerieNF'
      Origin = 'imprime.SerieNF'
      Required = True
    end
    object QrImprimeSequencial: TIntegerField
      FieldName = 'Sequencial'
      Origin = 'paramsnfs.Sequencial'
      DisplayFormat = '000000;-000000; '
    end
    object QrImprimeIncSeqAuto: TSmallintField
      FieldName = 'IncSeqAuto'
      Origin = 'paramsnfs.IncSeqAuto'
    end
    object QrImprimeCtrl_nfs: TIntegerField
      FieldName = 'Ctrl_nfs'
      Origin = 'paramsnfs.Controle'
      Required = True
    end
    object QrImprimeMaxSeqLib: TIntegerField
      FieldName = 'MaxSeqLib'
      Origin = 'paramsnfs.MaxSeqLib'
      DisplayFormat = '000000;-000000; '
    end
    object QrImprimeTipoImpressao: TIntegerField
      FieldName = 'TipoImpressao'
      Origin = 'imprime.TipoImpressao'
      Required = True
    end
    object QrImprimeNO_SerieNF: TIntegerField
      FieldName = 'NO_SerieNF'
      Origin = 'paramsnfs.SerieNF'
    end
  end
  object QrParamsEmp: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT AssocModNF '
      'FROM paramsemp'
      'WHERE Codigo=:P0')
    Left = 92
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrParamsEmpAssocModNF: TIntegerField
      FieldName = 'AssocModNF'
    end
  end
  object DsImprime: TDataSource
    DataSet = QrImprime
    Left = 64
    Top = 40
  end
  object QrFatPedCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT fpc.Codigo,fpc.Abertura, fpc.CodUsu,'
      'pvd.Empresa, pvd.Cliente, pvd.Transporta, '
      'pvd.FretePor, pvd.CartEmis, pvd.CondicaoPg, '
      'pvd.AFP_Sit, pvd.AFP_Per, pvd.Represen,'
      'frc.ModeloNF, '
      'car.Tipo TIPOCART,'
      ''
      'par.Associada, ase.Filial ASS_FILIAL,'
      ''
      'par.CtaProdVen EMP_CtaProdVen, '
      'par.FaturaSeq EMP_FaturaSeq,'
      'par.FaturaSep EMP_FaturaSep,'
      'par.FaturaDta EMP_FaturaDta,'
      'par.TxtProdVen EMP_TxtProdVen,'
      'emp.Filial EMP_FILIAL,'
      ''
      'ass.CtaProdVen ASS_CtaProdVen, '
      'ass.FaturaSeq ASS_FaturaSeq,'
      'ass.FaturaSep ASS_FaturaSep,'
      'ass.FaturaDta ASS_FaturaDta,'
      'ass.TxtProdVen ASS_TxtProdVen'
      ''
      'FROM fatpedcab fpc'
      'LEFT JOIN pedivda pvd ON pvd.Codigo=fpc.Pedido'
      'LEFT JOIN fisregcad  frc ON frc.Codigo=pvd.RegrFiscal'
      'LEFT JOIN entidades emp ON emp.Codigo=pvd.Empresa'
      'LEFT JOIN paramsemp  par ON par.Codigo=pvd.Empresa'
      'LEFT JOIN paramsemp  ass ON ass.Codigo=par.Associada'
      'LEFT JOIN entidades ase ON ase.Codigo=ass.Codigo'
      'LEFT JOIN carteiras  car ON car.Codigo=pvd.CartEmis'
      ''
      'WHERE fpc.Codigo=:P0')
    Left = 120
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFatPedCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrFatPedCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrFatPedCabCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrFatPedCabTransporta: TIntegerField
      FieldName = 'Transporta'
    end
    object QrFatPedCabFretePor: TSmallintField
      FieldName = 'FretePor'
    end
    object QrFatPedCabModeloNF: TIntegerField
      FieldName = 'ModeloNF'
    end
    object QrFatPedCabEMP_FILIAL: TIntegerField
      FieldName = 'EMP_FILIAL'
    end
    object QrFatPedCabAFP_Sit: TSmallintField
      FieldName = 'AFP_Sit'
    end
    object QrFatPedCabAFP_Per: TFloatField
      FieldName = 'AFP_Per'
    end
    object QrFatPedCabCartEmis: TIntegerField
      FieldName = 'CartEmis'
    end
    object QrFatPedCabAssociada: TIntegerField
      FieldName = 'Associada'
    end
    object QrFatPedCabCondicaoPg: TIntegerField
      FieldName = 'CondicaoPg'
    end
    object QrFatPedCabAbertura: TDateTimeField
      FieldName = 'Abertura'
      Required = True
    end
    object QrFatPedCabCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrFatPedCabTIPOCART: TIntegerField
      FieldName = 'TIPOCART'
      Required = True
    end
    object QrFatPedCabRepresen: TIntegerField
      FieldName = 'Represen'
    end
    object QrFatPedCabEMP_CtaProdVen: TIntegerField
      FieldName = 'EMP_CtaProdVen'
    end
    object QrFatPedCabEMP_FaturaSep: TWideStringField
      FieldName = 'EMP_FaturaSep'
      Size = 1
    end
    object QrFatPedCabEMP_FaturaDta: TSmallintField
      FieldName = 'EMP_FaturaDta'
    end
    object QrFatPedCabEMP_FaturaSeq: TSmallintField
      FieldName = 'EMP_FaturaSeq'
    end
    object QrFatPedCabEMP_TxtProdVen: TWideStringField
      FieldName = 'EMP_TxtProdVen'
      Size = 100
    end
    object QrFatPedCabASS_CtaProdVen: TIntegerField
      FieldName = 'ASS_CtaProdVen'
    end
    object QrFatPedCabASS_FILIAL: TIntegerField
      FieldName = 'ASS_FILIAL'
    end
    object QrFatPedCabASS_FaturaDta: TSmallintField
      FieldName = 'ASS_FaturaDta'
    end
    object QrFatPedCabASS_FaturaSeq: TSmallintField
      FieldName = 'ASS_FaturaSeq'
    end
    object QrFatPedCabASS_FaturaSep: TWideStringField
      FieldName = 'ASS_FaturaSep'
      Size = 1
    end
    object QrFatPedCabASS_TxtProdVen: TWideStringField
      FieldName = 'ASS_TxtProdVen'
      Size = 100
    end
  end
  object QrNF_X: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SerieNFTxt, NumeroNF'
      'FROM stqmovnfsa'
      'WHERE Tipo=1'
      'AND OriCodi=:P0'
      'AND Empresa=:P1')
    Left = 4
    Top = 260
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNF_XSerieNFTxt: TWideStringField
      FieldName = 'SerieNFTxt'
      Size = 5
    end
    object QrNF_XNumeroNF: TIntegerField
      FieldName = 'NumeroNF'
    end
  end
  object QrPrzT: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Dias, Percent1, Percent2'
      'FROM pediprzits'
      'WHERE Codigo=:P0'
      'ORDER BY Dias')
    Left = 32
    Top = 260
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPrzTDias: TIntegerField
      FieldName = 'Dias'
    end
    object QrPrzTPercent1: TFloatField
      FieldName = 'Percent1'
    end
    object QrPrzTPercent2: TFloatField
      FieldName = 'Percent2'
    end
    object QrPrzTControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrPrzX: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Dias, Percent1 Percent'
      'FROM pediprzits'
      'WHERE Codigo=:P0'
      'AND Percent1 > 0'
      'ORDER BY Dias')
    Left = 60
    Top = 260
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPrzXDias: TIntegerField
      FieldName = 'Dias'
    end
    object QrPrzXPercent: TFloatField
      FieldName = 'Percent'
      Required = True
    end
    object QrPrzXControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrSumT: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(ppi.Percent1) Percent1, SUM(ppi.Percent2) Percent2 ,'
      'ppc.JurosMes'
      'FROM pediprzits ppi'
      'LEFT JOIN pediprzcab ppc ON ppc.Codigo=ppi.Codigo'
      'WHERE ppi.Codigo=:P0'
      'GROUP BY ppi.Codigo')
    Left = 88
    Top = 260
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumTPercent1: TFloatField
      FieldName = 'Percent1'
    end
    object QrSumTPercent2: TFloatField
      FieldName = 'Percent2'
    end
    object QrSumTJurosMes: TFloatField
      FieldName = 'JurosMes'
    end
  end
  object QrSumX: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Total) Total '
      'FROM stqmovvala'
      'WHERE Tipo=1'
      'AND OriCodi=:P0 '
      'AND Empresa=:P1')
    Left = 116
    Top = 260
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSumXTotal: TFloatField
      FieldName = 'Total'
    end
  end
end
