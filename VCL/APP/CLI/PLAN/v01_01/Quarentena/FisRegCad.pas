unit FisRegCad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, Grids, DBGrids, dmkDBGrid,
  Menus, dmkCheckGroup, dmkDBLookupComboBox, dmkEditCB, dmkCheckBox, dmkMemo,
  dmkValUsu, ComCtrls, UnDmkProcFunc;

type
  THackCustomGrid = class(TCustomGrid);
  TFmFisRegCad = class(TForm)
    PainelDados: TPanel;
    DsFisRegCad: TDataSource;
    QrFisRegCad: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TdmkLabel;
    Image1: TImage;
    PanelFill2: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TdmkLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    dmkPermissoes1: TdmkPermissoes;
    PMCondicoes: TPopupMenu;
    DsFisRegMvt: TDataSource;
    Incluinovacondio1: TMenuItem;
    Alteracondioatual1: TMenuItem;
    Excluicondioatual1: TMenuItem;
    BtCondicoes: TBitBtn;
    BtMovimentacao: TBitBtn;
    QrFisRegCadCodigo: TIntegerField;
    QrFisRegCadCodUsu: TIntegerField;
    QrFisRegCadNome: TWideStringField;
    QrFisRegCadTipoMov: TSmallintField;
    QrFisRegCadICMS_Usa: TSmallintField;
    QrFisRegCadICMS_Alq: TFloatField;
    QrFisRegCadICMS_Frm: TWideMemoField;
    QrFisRegCadIPI_Usa: TSmallintField;
    QrFisRegCadIPI_Alq: TFloatField;
    QrFisRegCadIPI_Frm: TWideMemoField;
    QrFisRegCadPIS_Usa: TSmallintField;
    QrFisRegCadPIS_Alq: TFloatField;
    QrFisRegCadPIS_Frm: TWideMemoField;
    QrFisRegCadCOFINS_Usa: TSmallintField;
    QrFisRegCadCOFINS_Alq: TFloatField;
    QrFisRegCadCOFINS_Frm: TWideMemoField;
    QrFisRegCadIR_Usa: TSmallintField;
    QrFisRegCadIR_Alq: TFloatField;
    QrFisRegCadIR_Frm: TWideMemoField;
    QrFisRegCadCS_Usa: TSmallintField;
    QrFisRegCadCS_Alq: TFloatField;
    QrFisRegCadCS_Frm: TWideMemoField;
    QrFisRegCadISS_Usa: TSmallintField;
    QrFisRegCadISS_Alq: TFloatField;
    QrFisRegCadISS_Frm: TWideMemoField;
    QrFisRegCadAplicacao: TSmallintField;
    QrFisRegCadFinanceiro: TSmallintField;
    QrFisRegCadTipoEmiss: TSmallintField;
    QrFisRegCadModeloNF: TIntegerField;
    QrFisRegCadAgrupador: TIntegerField;
    QrFisRegCadObserva: TWideStringField;
    QrFisRegMvt: TmySQLQuery;
    QrFisRegMvtCodigo: TIntegerField;
    QrFisRegMvtControle: TIntegerField;
    QrFisRegMvtEmpresa: TIntegerField;
    QrFisRegMvtStqCenCad: TIntegerField;
    QrFisRegMvtTipoMov: TSmallintField;
    QrFisRegCadNOMETIPOMOV: TWideStringField;
    GroupBox1: TGroupBox;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label8: TLabel;
    EdCodUsu: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label4: TLabel;
    EdTipoMov: TdmkEditCB;
    CBTipoMov: TdmkDBLookupComboBox;
    GroupBox2: TGroupBox;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdit1: TDBEdit;
    DBEdNome: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    CkICMS_Usa: TdmkCheckBox;
    CkIPI_Usa: TdmkCheckBox;
    CkPIS_Usa: TdmkCheckBox;
    CkCOFINS_Usa: TdmkCheckBox;
    CkIR_Usa: TdmkCheckBox;
    CkCS_Usa: TdmkCheckBox;
    CkISS_Usa: TdmkCheckBox;
    GroupBox5: TGroupBox;
    EdICMS_Alq: TdmkEdit;
    EdIPI_Alq: TdmkEdit;
    EdPIS_Alq: TdmkEdit;
    EdCOFINS_Alq: TdmkEdit;
    EdIR_Alq: TdmkEdit;
    EdCS_Alq: TdmkEdit;
    EdISS_Alq: TdmkEdit;
    RGAplicacao: TdmkRadioGroup;
    Panel6: TPanel;
    RGFinanceiro: TdmkRadioGroup;
    RGTipoEmiss: TdmkRadioGroup;
    Panel7: TPanel;
    Label1: TLabel;
    EdModeloNF: TdmkEditCB;
    CBModeloNF: TdmkDBLookupComboBox;
    Label2: TLabel;
    EdAgrupador: TdmkEditCB;
    CBAgrupador: TdmkDBLookupComboBox;
    SbAgrupador: TSpeedButton;
    SBModeloNF: TSpeedButton;
    StaticText1: TStaticText;
    MeInfAdFisco: TdmkMemo;
    QrImprime: TmySQLQuery;
    DsImprime: TDataSource;
    QrImprimeCodigo: TIntegerField;
    QrImprimeNome: TWideStringField;
    QrFisAgrCad: TmySQLQuery;
    QrFisAgrCadCodigo: TIntegerField;
    QrFisAgrCadCodUsu: TIntegerField;
    QrFisAgrCadNome: TWideStringField;
    dmkValUsu1: TdmkValUsu;
    DsFisAgrCad: TDataSource;
    GroupBox7: TGroupBox;
    GroupBox8: TGroupBox;
    dmkCheckBox1: TDBCheckBox;
    dmkCheckBox2: TDBCheckBox;
    dmkCheckBox3: TDBCheckBox;
    dmkCheckBox4: TDBCheckBox;
    dmkCheckBox5: TDBCheckBox;
    dmkCheckBox6: TDBCheckBox;
    dmkCheckBox7: TDBCheckBox;
    GroupBox9: TGroupBox;
    DBEdit_01: TDBEdit;
    DBEdit_02: TDBEdit;
    DBEdit_03: TDBEdit;
    DBEdit_04: TDBEdit;
    DBEdit_05: TDBEdit;
    DBEdit_06: TDBEdit;
    DBEdit_07: TDBEdit;
    Panel4: TPanel;
    DBRadioGroup1: TDBRadioGroup;
    DBRadioGroup2: TDBRadioGroup;
    DBRadioGroup3: TDBRadioGroup;
    Panel8: TPanel;
    Label3: TLabel;
    Label16: TLabel;
    DBEdit100: TDBEdit;
    DBEdit101: TDBEdit;
    DBEdit102: TDBEdit;
    DBEdit103: TDBEdit;
    StaticText2: TStaticText;
    QrFisRegCadNOMEMODELONF: TWideStringField;
    QrFisRegCadNOMEAGRUPADOR: TWideStringField;
    QrFisRegCadCODUSU_AGRUPADOR: TIntegerField;
    DBMemo1: TDBMemo;
    DBGFisRegMvt: TDBGrid;
    QrFisRegMvtTipoCalc: TSmallintField;
    QrFisRegMvtNOME_EMP: TWideStringField;
    QrFisRegMvtNOME_StqCenCad: TWideStringField;
    QrFisRegMvtNOME_TIPOCALC: TWideStringField;
    QrFisRegMvtNOME_TIPOMOV: TWideStringField;
    DBGTitle1: TDBGrid;
    QrFisRegMvtSEQ: TIntegerField;
    QrFisRegMvtFILIAL: TIntegerField;
    QrFisRegMvtCODUSU_STQCENCAD: TIntegerField;
    PMMovimentacao: TPopupMenu;
    Incluiitemdemovimentao1: TMenuItem;
    Alteraitemdemovimentaoatual1: TMenuItem;
    Excluiitemnsdemovimentao1: TMenuItem;
    QrFisRegCFOP: TmySQLQuery;
    DsFisRegCFOP: TDataSource;
    QrFisRegCFOPCodigo: TIntegerField;
    QrFisRegCFOPInterno: TSmallintField;
    QrFisRegCFOPContribui: TSmallintField;
    QrFisRegCFOPProprio: TSmallintField;
    QrFisRegCFOPCFOP: TWideStringField;
    QrFisRegCFOPNO_CFOP: TWideStringField;
    BtCFOP: TBitBtn;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DBGFisRegCFOP: TdmkDBGrid;
    DBMemo_01: TDBMemo;
    DBMemo_02: TDBMemo;
    DBMemo_03: TDBMemo;
    DBMemo_04: TDBMemo;
    DBMemo_05: TDBMemo;
    DBMemo_06: TDBMemo;
    DBMemo_07: TDBMemo;
    QrFisRegCFOPOrdCFOPGer: TIntegerField;
    QrFisRegMvtCU_GraCusPrc: TIntegerField;
    QrFisRegMvtNO_GraCusPrc: TWideStringField;
    QrFisRegMvtGraCusApl: TSmallintField;
    QrFisRegMvtGraCusPrc: TIntegerField;
    QrFisRegMvtNO_GraCusApl: TWideStringField;
    PageControl2: TPageControl;
    TabSheet4: TTabSheet;
    Panel10: TPanel;
    PMCFOP: TPopupMenu;
    IncluinovoCFOP1: TMenuItem;
    AlteraCFOPatual1: TMenuItem;
    RetiraCFOPAtual1: TMenuItem;
    EdICMS_Frm: TdmkEdit;
    EdIPI_Frm: TdmkEdit;
    EdPIS_Frm: TdmkEdit;
    EdCOFINS_Frm: TdmkEdit;
    EdIR_Frm: TdmkEdit;
    EdCS_Frm: TdmkEdit;
    EdISS_Frm: TdmkEdit;
    SpeedButton11: TSpeedButton;
    SpeedButton10: TSpeedButton;
    SpeedButton9: TSpeedButton;
    SpeedButton8: TSpeedButton;
    SpeedButton7: TSpeedButton;
    SpeedButton5: TSpeedButton;
    SpeedButton6: TSpeedButton;
    StaticText3: TStaticText;
    dmkMemo1: TdmkMemo;
    StaticText4: TStaticText;
    DBMemo2: TDBMemo;
    QrFisRegCadInfAdFisco: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrFisRegCadAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrFisRegCadBeforeOpen(DataSet: TDataSet);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure BtCondicoesClick(Sender: TObject);
    procedure PMCondicoesPopup(Sender: TObject);
    procedure QrFisRegCadBeforeClose(DataSet: TDataSet);
    procedure QrFisRegCadAfterScroll(DataSet: TDataSet);
    procedure BtMovimentacaoClick(Sender: TObject);
    procedure Incluinovacondio1Click(Sender: TObject);
    procedure Alteracondioatual1Click(Sender: TObject);
    procedure QrFisRegCadCalcFields(DataSet: TDataSet);
    procedure SBModeloNFClick(Sender: TObject);
    procedure SbAgrupadorClick(Sender: TObject);
    procedure QrFisRegMvtCalcFields(DataSet: TDataSet);
    procedure DBGFisRegMvtMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure Incluiitemdemovimentao1Click(Sender: TObject);
    procedure Alteraitemdemovimentaoatual1Click(Sender: TObject);
    procedure Excluiitemnsdemovimentao1Click(Sender: TObject);
    procedure BtCFOPClick(Sender: TObject);
    procedure IncluinovoCFOP1Click(Sender: TObject);
    procedure AlteraCFOPatual1Click(Sender: TObject);
    procedure RetiraCFOPAtual1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; Status: String; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenFisRegMvt(Controle: Integer);
    procedure ReopenFisRegCFOP(CFOP: String);
  end;

var
  FmFisRegCad: TFmFisRegCad;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, FisRegMvt, MyDBCheck, ModPediVda, CFOP2003, Imprime, FisAgrCad,
  FisRegCFOP;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmFisRegCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmFisRegCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrFisRegCadCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmFisRegCad.DefParams;
begin
  VAR_GOTOTABELA := 'FisRegCad';
  VAR_GOTOMYSQLTABLE := QrFisRegCad;
  VAR_GOTONEG := True;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT frc.*, mnf.Nome NOMEMODELONF,');
  VAR_SQLx.Add('fac.Nome NOMEAGRUPADOR, fac.CodUsu CODUSU_AGRUPADOR');
  VAR_SQLx.Add('FROM fisregcad frc');
  VAR_SQLx.Add('LEFT JOIN imprime   mnf ON mnf.Codigo=frc.ModeloNF');
  VAR_SQLx.Add('LEFT JOIN fisagrcad fac ON fac.Codigo=frc.Agrupador');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE frc.Codigo > -1000');
  //
  VAR_SQL1.Add('AND frc.Codigo=:P0');
  //
  VAR_SQL2.Add('AND frc.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND frc.Nome Like :P0');
  //
end;

procedure TFmFisRegCad.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (LaTipo.SQLType = stIns) then
    EdCodUsu.ValueVariant :=
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'FisRegCad', 'CodUsu', [], []);
end;

procedure TFmFisRegCad.Excluiitemnsdemovimentao1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrFisRegMvt, DBGFisRegMvt,
  'FisRegMvt', ['Controle'], ['Controle'], istPergunta, '');
end;

procedure TFmFisRegCad.MostraEdicao(Mostra: Integer; Status: String; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelControle.Visible:=True;
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    else Geral.MensagemBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  LaTipo.Caption := Status;
  GOTOy.BotoesSb(LaTipo.Caption);
end;

procedure TFmFisRegCad.PMCondicoesPopup(Sender: TObject);
begin
  Alteracondioatual1.Enabled :=
    (QrFisRegCad.State <> dsInactive) and (QrFisRegCad.RecordCount > 0);
end;

procedure TFmFisRegCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmFisRegCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmFisRegCad.DBGFisRegMvtMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
var
  K: Integer;
begin
  with DBGFisRegMvt do
  begin
    K := Columns[0].Width;
    if DBGTitle1.Columns[0].Width <> K then
      DBGTitle1.Columns[0].Width := K;
    //
    K := Columns[1].Width + Columns[2].Width + 1;
    if DBGTitle1.Columns[1].Width <> K then
      DBGTitle1.Columns[1].Width := K;
    //
    K := Columns[3].Width;
    if DBGTitle1.Columns[2].Width <> K then
      DBGTitle1.Columns[2].Width := K;
    //
    K := Columns[4].Width;
    if DBGTitle1.Columns[3].Width <> K then
      DBGTitle1.Columns[3].Width := K;
    //
    K := Columns[5].Width + Columns[6].Width + 1;
    if DBGTitle1.Columns[4].Width <> K then
      DBGTitle1.Columns[4].Width := K;
    //
    K := Columns[7].Width + Columns[8].Width + Columns[9].Width + 1;
    if DBGTitle1.Columns[5].Width <> K then
      DBGTitle1.Columns[5].Width := K;
    //
  end;
end;

procedure TFmFisRegCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmFisRegCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmFisRegCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmFisRegCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmFisRegCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmFisRegCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrFisRegCadCodigo.Value;
  Close;
end;

procedure TFmFisRegCad.AlteraCFOPatual1Click(Sender: TObject);
begin
  UmyMod.FormInsUpd_Show(TFmFisRegCFOP, FmFisRegCFOP, afmoNegarComAviso,
    QrFisRegCFOP, stUpd);
end;

procedure TFmFisRegCad.Alteracondioatual1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrFisRegCad, [PainelDados],
  [PainelEdita], EdCodUsu, LaTipo, 'FisRegCad');
end;

procedure TFmFisRegCad.Alteraitemdemovimentaoatual1Click(Sender: TObject);
begin
  UmyMod.FormInsUpd_Show(TFmFisRegMvt, FmFisRegMvt, afmoNegarComAviso,
    QrFisRegMvt, stUpd);
end;

procedure TFmFisRegCad.BtCFOPClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCFOP, BtCFOP);
end;

procedure TFmFisRegCad.BtCondicoesClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCondicoes, BtCondicoes);
end;

procedure TFmFisRegCad.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Geral.MensagemBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Codigo := UMyMod.BuscaEmLivreY_Def('FisRegCad', 'Codigo', LaTipo.SQLType,
    QrFisRegCadCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(LaTipo.SQLType, FmFisRegCad, PainelEdita,
    'FisRegCad', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], LaTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmFisRegCad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if LaTipo.Caption = CO_INCLUSAO then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'FisRegCad', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'FisRegCad', 'Codigo');
  MostraEdicao(0, CO_TRAVADO, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'FisRegCad', 'Codigo');
end;

procedure TFmFisRegCad.BtMovimentacaoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMMovimentacao, BtMovimentacao);
end;

procedure TFmFisRegCad.FormCreate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  with THackCustomGrid(DBGFisRegMvt) do
     Options := Options - [goColMoving];
  DBGFisRegMvt.Align := alClient;
  QrFisAgrCad.Open;
  QrImprime.Open;
  CriaOForm;
end;

procedure TFmFisRegCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrFisRegCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmFisRegCad.SbAgrupadorClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmFisAgrCad, FmFisAgrCad, afmoNegarComAviso) then
  begin
    FmFisAgrCad.ShowModal;
    FmFisAgrCad.Destroy;
    if (VAR_CADASTRO > 0) then
    begin
      QrFisAgrCad.Close;
      QrFisAgrCad.Open;
      //
      EdModeloNF.ValueVariant := VAR_CADASTRO;
      CBModeloNF.KeyValue := VAR_CADASTRO;
    end;
  end;
end;

procedure TFmFisRegCad.SBModeloNFClick(Sender: TObject);
begin
  VAR_IMPRIMEFMT := 0;
  if DBCheck.CriaFm(TFmImprime, FmImprime, afmoNegarComAviso) then
  begin
    FmImprime.ShowModal;
    FmImprime.Destroy;
    if (VAR_IMPRIMEFMT <> 0) and (VAR_IMPRIMETYP = 1) then
    begin
      QrImprime.Close;
      QrImprime.Open;
      //
      EdModeloNF.ValueVariant := VAR_IMPRIMEFMT;
      CBModeloNF.KeyValue := VAR_IMPRIMEFMT;
    end;
  end;
end;

procedure TFmFisRegCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmFisRegCad.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrFisRegCadCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmFisRegCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmFisRegCad.QrFisRegCadAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmFisRegCad.QrFisRegCadAfterScroll(DataSet: TDataSet);
begin
  ReopenFisRegMvt(0);
  ReopenFisRegCFOP('');
end;

procedure TFmFisRegCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFisRegCad.SbQueryClick(Sender: TObject);
begin
  LocCod(QrFisRegCadCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'FisRegCad', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmFisRegCad.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmFisRegCad.Incluiitemdemovimentao1Click(Sender: TObject);
begin
  UmyMod.FormInsUpd_Show(TFmFisRegMvt, FmFisRegMvt, afmoNegarComAviso,
    QrFisRegMvt, stIns);
end;

procedure TFmFisRegCad.Incluinovacondio1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdita, QrFisRegCad, [PainelDados],
  [PainelEdita], EdCodUsu, LaTipo, 'FisRegCad');
end;

procedure TFmFisRegCad.IncluinovoCFOP1Click(Sender: TObject);
begin
  UmyMod.FormInsUpd_Show(TFmFisRegCFOP, FmFisRegCFOP, afmoNegarComAviso,
    QrFisRegCFOP, stIns);
end;

procedure TFmFisRegCad.QrFisRegCadBeforeClose(DataSet: TDataSet);
begin
  QrFisRegMvt.Close;
  QrFisRegCFOP.Close;
end;

procedure TFmFisRegCad.QrFisRegCadBeforeOpen(DataSet: TDataSet);
begin
  QrFisRegCadCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmFisRegCad.QrFisRegCadCalcFields(DataSet: TDataSet);
begin
  case QrFisRegCadTipoMov.Value of
    1: QrFisRegCadNOMETIPOMOV.Value := 'Entrada';
    5: QrFisRegCadNOMETIPOMOV.Value := 'Sa�da';
    else QrFisRegCadNOMETIPOMOV.Value := '? ? ?';
  end;
end;

procedure TFmFisRegCad.QrFisRegMvtCalcFields(DataSet: TDataSet);
begin
  QrFisRegMvtSEQ.Value := QrFisRegMvt.RecNo;
end;

procedure TFmFisRegCad.ReopenFisRegCFOP(CFOP: String);
begin
  QrFisRegCFOP.Close;
  QrFisRegCFOP.Params[0].AsInteger := QrFisRegCadCodigo.Value;
  QrFisRegCFOP.Open;
  //
  QrFisRegCFOP.Locate('CFOP', CFOP, []);
end;

procedure TFmFisRegCad.ReopenFisRegMvt(Controle: Integer);
begin
  QrFisRegMvt.Close;
  QrFisRegMvt.Params[0].AsInteger :=   QrFisRegCadCodigo.Value;
  QrFisRegMvt.Open;
  //
  if Controle <> 0 then
    QrFisRegMvt.Locate('Controle', Controle, []);
end;

procedure TFmFisRegCad.RetiraCFOPAtual1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrFisRegCFOP, TDBGrid(DBGFisRegCFOP),
  'FisRegCFOP', ['Interno','Contribui','Proprio'],
  ['Interno','Contribui','Proprio'], istPergunta, '');
end;

end.

