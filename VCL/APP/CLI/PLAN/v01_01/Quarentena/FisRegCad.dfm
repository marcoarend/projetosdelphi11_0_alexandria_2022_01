object FmFisRegCad: TFmFisRegCad
  Left = 368
  Top = 194
  Caption = 'FIS-REGRA-001 :: Regras Fiscais'
  ClientHeight = 694
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 646
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Visible = False
    object PainelConfirma: TPanel
      Left = 1
      Top = 597
      Width = 1006
      Height = 48
      Align = alBottom
      TabOrder = 0
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 8
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
        NumGlyphs = 2
      end
      object Panel1: TPanel
        Left = 897
        Top = 1
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
          NumGlyphs = 2
        end
      end
    end
    object GroupBox1: TGroupBox
      Left = 1
      Top = 1
      Width = 1006
      Height = 44
      Align = alTop
      Caption = ' Regra de Movimenta'#231#227'o: '
      TabOrder = 1
      object Label7: TLabel
        Left = 8
        Top = 20
        Width = 14
        Height = 13
        Caption = 'ID:'
      end
      object Label8: TLabel
        Left = 88
        Top = 20
        Width = 57
        Height = 13
        Caption = 'C'#243'digo: [F4]'
      end
      object Label9: TLabel
        Left = 232
        Top = 20
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object Label4: TLabel
        Left = 800
        Top = 21
        Width = 93
        Height = 13
        Caption = 'Tipo de movimento:'
      end
      object EdCodigo: TdmkEdit
        Left = 28
        Top = 16
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdCodUsu: TdmkEdit
        Left = 148
        Top = 16
        Width = 80
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CodUsu'
        UpdCampo = 'CodUsu'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnKeyDown = EdCodUsuKeyDown
      end
      object EdNome: TdmkEdit
        Left = 288
        Top = 16
        Width = 509
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdTipoMov: TdmkEditCB
        Left = 900
        Top = 16
        Width = 21
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'TipoMov'
        UpdCampo = 'TipoMov'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBTipoMov
      end
      object CBTipoMov: TdmkDBLookupComboBox
        Left = 924
        Top = 16
        Width = 73
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DmPediVda.DsFRC_TMov
        TabOrder = 4
        dmkEditCB = EdTipoMov
        QryCampo = 'TipoMov'
        UpdType = utNil
      end
    end
    object GroupBox3: TGroupBox
      Left = 1
      Top = 45
      Width = 1006
      Height = 196
      Align = alTop
      Caption = ' Tributa'#231#227'o: '
      TabOrder = 2
      object GroupBox4: TGroupBox
        Left = 2
        Top = 15
        Width = 75
        Height = 179
        Align = alLeft
        Caption = ' Tributo: '
        TabOrder = 0
        object CkICMS_Usa: TdmkCheckBox
          Left = 8
          Top = 32
          Width = 60
          Height = 17
          Caption = 'ICMS'
          TabOrder = 0
          QryCampo = 'ICMS_Usa'
          UpdCampo = 'ICMS_Usa'
          UpdType = utYes
          ValCheck = '1'
          ValUncheck = '0'
          OldValor = #0
        end
        object CkIPI_Usa: TdmkCheckBox
          Left = 8
          Top = 52
          Width = 60
          Height = 17
          Caption = 'IPI'
          TabOrder = 1
          QryCampo = 'IPI_Usa'
          UpdCampo = 'IPI_Usa'
          UpdType = utYes
          ValCheck = '1'
          ValUncheck = '0'
          OldValor = #0
        end
        object CkPIS_Usa: TdmkCheckBox
          Left = 8
          Top = 72
          Width = 60
          Height = 17
          Caption = 'PIS'
          TabOrder = 2
          QryCampo = 'PIS_Usa'
          UpdCampo = 'PIS_Usa'
          UpdType = utYes
          ValCheck = '1'
          ValUncheck = '0'
          OldValor = #0
        end
        object CkCOFINS_Usa: TdmkCheckBox
          Left = 8
          Top = 92
          Width = 60
          Height = 17
          Caption = 'COFINS'
          TabOrder = 3
          QryCampo = 'COFINS_Usa'
          UpdCampo = 'COFINS_Usa'
          UpdType = utYes
          ValCheck = '1'
          ValUncheck = '0'
          OldValor = #0
        end
        object CkIR_Usa: TdmkCheckBox
          Left = 8
          Top = 112
          Width = 60
          Height = 17
          Caption = 'IR'
          TabOrder = 4
          QryCampo = 'IR_Usa'
          UpdCampo = 'IR_Usa'
          UpdType = utYes
          ValCheck = '1'
          ValUncheck = '0'
          OldValor = #0
        end
        object CkCS_Usa: TdmkCheckBox
          Left = 8
          Top = 132
          Width = 60
          Height = 17
          Caption = 'CS'
          TabOrder = 5
          QryCampo = 'CS_Usa'
          UpdCampo = 'CS_Usa'
          UpdType = utYes
          ValCheck = '1'
          ValUncheck = '0'
          OldValor = #0
        end
        object CkISS_Usa: TdmkCheckBox
          Left = 8
          Top = 152
          Width = 60
          Height = 17
          Caption = 'ISS'
          TabOrder = 6
          QryCampo = 'ISS_Usa'
          UpdCampo = 'ISS_Usa'
          UpdType = utYes
          ValCheck = '1'
          ValUncheck = '0'
          OldValor = #0
        end
      end
      object GroupBox5: TGroupBox
        Left = 77
        Top = 15
        Width = 84
        Height = 179
        Align = alLeft
        Caption = ' Al'#237'quota (%): '
        TabOrder = 1
        object EdICMS_Alq: TdmkEdit
          Left = 8
          Top = 28
          Width = 68
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          ValMax = '99,99'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ICMS_Alq'
          UpdCampo = 'ICMS_Alq'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
        end
        object EdIPI_Alq: TdmkEdit
          Left = 8
          Top = 48
          Width = 68
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          ValMax = '99,99'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'IPI_Alq'
          UpdCampo = 'IPI_Alq'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
        end
        object EdPIS_Alq: TdmkEdit
          Left = 8
          Top = 68
          Width = 68
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          ValMax = '99,99'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'PIS_Alq'
          UpdCampo = 'PIS_Alq'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
        end
        object EdCOFINS_Alq: TdmkEdit
          Left = 8
          Top = 88
          Width = 68
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          ValMax = '99,99'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'COFINS_Alq'
          UpdCampo = 'COFINS_Alq'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
        end
        object EdIR_Alq: TdmkEdit
          Left = 8
          Top = 108
          Width = 68
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          ValMax = '99,99'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'IR_Alq'
          UpdCampo = 'IR_Alq'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
        end
        object EdCS_Alq: TdmkEdit
          Left = 8
          Top = 128
          Width = 68
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          ValMax = '99,99'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'CS_Alq'
          UpdCampo = 'CS_Alq'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
        end
        object EdISS_Alq: TdmkEdit
          Left = 8
          Top = 148
          Width = 68
          Height = 21
          Alignment = taRightJustify
          TabOrder = 6
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          ValMax = '99,99'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ISS_Alq'
          UpdCampo = 'ISS_Alq'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
        end
      end
      object PageControl2: TPageControl
        Left = 161
        Top = 15
        Width = 843
        Height = 179
        ActivePage = TabSheet4
        Align = alClient
        TabOrder = 2
        object TabSheet4: TTabSheet
          Caption = ' F'#243'rmulas '
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel10: TPanel
            Left = 0
            Top = 0
            Width = 835
            Height = 151
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object SpeedButton11: TSpeedButton
              Left = 814
              Top = 4
              Width = 21
              Height = 21
              Caption = '...'
            end
            object SpeedButton10: TSpeedButton
              Left = 814
              Top = 24
              Width = 21
              Height = 21
              Caption = '...'
            end
            object SpeedButton9: TSpeedButton
              Left = 814
              Top = 44
              Width = 21
              Height = 21
              Caption = '...'
            end
            object SpeedButton8: TSpeedButton
              Left = 814
              Top = 64
              Width = 21
              Height = 21
              Caption = '...'
            end
            object SpeedButton7: TSpeedButton
              Left = 814
              Top = 84
              Width = 21
              Height = 21
              Caption = '...'
            end
            object SpeedButton5: TSpeedButton
              Left = 814
              Top = 104
              Width = 21
              Height = 21
              Caption = '...'
            end
            object SpeedButton6: TSpeedButton
              Left = 814
              Top = 124
              Width = 21
              Height = 21
              Caption = '...'
            end
            object EdICMS_Frm: TdmkEdit
              Left = 8
              Top = 4
              Width = 805
              Height = 21
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'ICMS_Frm'
              UpdCampo = 'ICMS_Frm'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
            end
            object EdIPI_Frm: TdmkEdit
              Left = 8
              Top = 24
              Width = 805
              Height = 21
              TabOrder = 1
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'IPI_Frm'
              UpdCampo = 'IPI_Frm'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
            end
            object EdPIS_Frm: TdmkEdit
              Left = 8
              Top = 44
              Width = 805
              Height = 21
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'PIS_Frm'
              UpdCampo = 'PIS_Frm'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
            end
            object EdCOFINS_Frm: TdmkEdit
              Left = 8
              Top = 64
              Width = 805
              Height = 21
              TabOrder = 3
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'COFINS_Frm'
              UpdCampo = 'COFINS_Frm'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
            end
            object EdIR_Frm: TdmkEdit
              Left = 8
              Top = 84
              Width = 805
              Height = 21
              TabOrder = 4
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'IR_Frm'
              UpdCampo = 'IR_Frm'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
            end
            object EdCS_Frm: TdmkEdit
              Left = 8
              Top = 104
              Width = 805
              Height = 21
              TabOrder = 5
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'CS_Frm'
              UpdCampo = 'CS_Frm'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
            end
            object EdISS_Frm: TdmkEdit
              Left = 8
              Top = 124
              Width = 805
              Height = 21
              TabOrder = 6
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'ISS_Frm'
              UpdCampo = 'ISS_Frm'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
            end
          end
        end
      end
    end
    object RGAplicacao: TdmkRadioGroup
      Left = 1
      Top = 241
      Width = 1006
      Height = 64
      Align = alTop
      Caption = ' Aplica'#231#227'o: '
      Columns = 5
      ItemIndex = 0
      Items.Strings = (
        'Nenhuma aplica'#231#227'o'
        'Ped. Venda'
        'Condicional'
        'Transfer'#234'ncia'
        'Movimento Avulso'
        'Venda Balc'#227'o'
        'Ped. Compra'
        'Consigna'#231#227'o'
        'Produ'#231#227'o'
        'Remessa p/ Indust.'
        'Devolu'#231#227'o'
        'Troca'
        'Servi'#231'o'
        'Retorno Rem. p/ Indust.'
        'Balan'#231'o (uso restrito do adm. do sist.)')
      TabOrder = 3
      QryCampo = 'Aplicacao'
      UpdCampo = 'Aplicacao'
      UpdType = utYes
      OldValor = 0
    end
    object Panel6: TPanel
      Left = 1
      Top = 305
      Width = 1006
      Height = 36
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 4
      object RGFinanceiro: TdmkRadioGroup
        Left = 0
        Top = 0
        Width = 361
        Height = 36
        Align = alLeft
        Caption = ' Financeiro: '
        Columns = 4
        ItemIndex = 0
        Items.Strings = (
          'Nenhum'
          #192' receber'#9
          #192' pagar'#9
          'Conta Corrente'#9)
        TabOrder = 0
        QryCampo = 'Financeiro'
        UpdCampo = 'Financeiro'
        UpdType = utYes
        OldValor = 0
      end
      object RGTipoEmiss: TdmkRadioGroup
        Left = 361
        Top = 0
        Width = 645
        Height = 36
        Align = alClient
        Caption = 'Emiss'#227'o: '
        Columns = 5
        ItemIndex = 0
        Items.Strings = (
          'Indefinido'
          'Nota Fiscal'#9
          'Romaneio'#9
          'ECF n'#227'o concomit.'
          'ECF concomitante')
        TabOrder = 1
        QryCampo = 'TipoEmiss'
        UpdCampo = 'TipoEmiss'
        UpdType = utYes
        OldValor = 0
      end
    end
    object Panel7: TPanel
      Left = 1
      Top = 341
      Width = 1006
      Height = 32
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 5
      object Label1: TLabel
        Left = 8
        Top = 9
        Width = 70
        Height = 13
        Caption = 'Modelo de NF:'
      end
      object Label2: TLabel
        Left = 512
        Top = 9
        Width = 52
        Height = 13
        Caption = 'Agrupador:'
      end
      object SbAgrupador: TSpeedButton
        Left = 977
        Top = 4
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbAgrupadorClick
      end
      object SBModeloNF: TSpeedButton
        Left = 486
        Top = 4
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SBModeloNFClick
      end
      object EdModeloNF: TdmkEditCB
        Left = 84
        Top = 4
        Width = 52
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ModeloNF'
        UpdCampo = 'ModeloNF'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBModeloNF
      end
      object CBModeloNF: TdmkDBLookupComboBox
        Left = 140
        Top = 4
        Width = 345
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsImprime
        TabOrder = 1
        dmkEditCB = EdModeloNF
        QryCampo = 'ModeloNF'
        UpdType = utNil
      end
      object EdAgrupador: TdmkEditCB
        Left = 568
        Top = 4
        Width = 52
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBAgrupador
      end
      object CBAgrupador: TdmkDBLookupComboBox
        Left = 624
        Top = 4
        Width = 353
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsFisAgrCad
        TabOrder = 3
        dmkEditCB = EdAgrupador
        UpdType = utNil
      end
    end
    object StaticText1: TStaticText
      Left = 1
      Top = 426
      Width = 1006
      Height = 17
      Align = alTop
      Alignment = taCenter
      Caption = 
        'Informa'#231#245'es adicionais de interesse do fisco (na NF-e) m'#225'x. 255 ' +
        'caracteres:'
      TabOrder = 6
      ExplicitWidth = 361
    end
    object MeInfAdFisco: TdmkMemo
      Left = 1
      Top = 443
      Width = 1006
      Height = 36
      Align = alTop
      MaxLength = 255
      TabOrder = 7
      QryCampo = 'InfAdFisco'
      UpdCampo = 'InfAdFisco'
      UpdType = utYes
    end
    object StaticText3: TStaticText
      Left = 1
      Top = 373
      Width = 1006
      Height = 17
      Align = alTop
      Alignment = taCenter
      Caption = 'Observa'#231#245'es:'
      TabOrder = 8
      ExplicitWidth = 70
    end
    object dmkMemo1: TdmkMemo
      Left = 1
      Top = 390
      Width = 1006
      Height = 36
      Align = alTop
      TabOrder = 9
      QryCampo = 'Observa'
      UpdCampo = 'Observa'
      UpdType = utYes
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 646
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object PainelControle: TPanel
      Left = 1
      Top = 597
      Width = 1006
      Height = 48
      Align = alBottom
      TabOrder = 0
      object LaRegistro: TdmkLabel
        Left = 173
        Top = 1
        Width = 26
        Height = 13
        Align = alClient
        Caption = '[N]: 0'
        UpdType = utYes
        SQLType = stNil
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 172
        Height = 46
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
          NumGlyphs = 2
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
          NumGlyphs = 2
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
          NumGlyphs = 2
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
          NumGlyphs = 2
        end
      end
      object Panel3: TPanel
        Left = 536
        Top = 1
        Width = 469
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object BtCondicoes: TBitBtn
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Condi'#231#227'o'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtCondicoesClick
          NumGlyphs = 2
        end
        object Panel2: TPanel
          Left = 360
          Top = 0
          Width = 109
          Height = 46
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 1
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
            NumGlyphs = 2
          end
        end
        object BtMovimentacao: TBitBtn
          Left = 96
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Movimenta'#231#227'o'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtMovimentacaoClick
          NumGlyphs = 2
        end
        object BtCFOP: TBitBtn
          Left = 218
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = 'CF&OP'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtCFOPClick
          NumGlyphs = 2
        end
      end
    end
    object GroupBox2: TGroupBox
      Left = 1
      Top = 1
      Width = 1006
      Height = 44
      Align = alTop
      Caption = ' Regra de Movimenta'#231#227'o: '
      TabOrder = 1
      object Label10: TLabel
        Left = 8
        Top = 20
        Width = 14
        Height = 13
        Caption = 'ID:'
      end
      object Label11: TLabel
        Left = 88
        Top = 20
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label12: TLabel
        Left = 232
        Top = 20
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object Label13: TLabel
        Left = 800
        Top = 21
        Width = 93
        Height = 13
        Caption = 'Tipo de movimento:'
      end
      object DBEdCodigo: TDBEdit
        Left = 28
        Top = 16
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsFisRegCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdit1: TDBEdit
        Left = 148
        Top = 16
        Width = 80
        Height = 21
        DataField = 'CodUsu'
        DataSource = DsFisRegCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
      end
      object DBEdNome: TDBEdit
        Left = 284
        Top = 16
        Width = 513
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsFisRegCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
      end
      object DBEdit2: TDBEdit
        Left = 900
        Top = 16
        Width = 21
        Height = 21
        DataField = 'TipoMov'
        DataSource = DsFisRegCad
        TabOrder = 3
      end
      object DBEdit3: TDBEdit
        Left = 924
        Top = 16
        Width = 72
        Height = 21
        DataField = 'NOMETIPOMOV'
        DataSource = DsFisRegCad
        TabOrder = 4
      end
    end
    object GroupBox7: TGroupBox
      Left = 1
      Top = 45
      Width = 1006
      Height = 188
      Align = alTop
      Caption = ' Tributa'#231#227'o: '
      TabOrder = 2
      object GroupBox8: TGroupBox
        Left = 2
        Top = 15
        Width = 75
        Height = 171
        Align = alLeft
        Caption = ' Tributo: '
        TabOrder = 0
        object dmkCheckBox1: TDBCheckBox
          Left = 8
          Top = 24
          Width = 60
          Height = 17
          Caption = 'ICMS'
          DataField = 'ICMS_Usa'
          DataSource = DsFisRegCad
          TabOrder = 0
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
        object dmkCheckBox2: TDBCheckBox
          Left = 8
          Top = 44
          Width = 60
          Height = 17
          Caption = 'IPI'
          DataField = 'IPI_Usa'
          DataSource = DsFisRegCad
          TabOrder = 1
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
        object dmkCheckBox3: TDBCheckBox
          Left = 8
          Top = 64
          Width = 60
          Height = 17
          Caption = 'PIS'
          DataField = 'PIS_Usa'
          DataSource = DsFisRegCad
          TabOrder = 2
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
        object dmkCheckBox4: TDBCheckBox
          Left = 8
          Top = 84
          Width = 60
          Height = 17
          Caption = 'COFINS'
          DataField = 'COFINS_Usa'
          DataSource = DsFisRegCad
          TabOrder = 3
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
        object dmkCheckBox5: TDBCheckBox
          Left = 8
          Top = 104
          Width = 60
          Height = 17
          Caption = 'IR'
          DataField = 'IR_Usa'
          DataSource = DsFisRegCad
          TabOrder = 4
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
        object dmkCheckBox6: TDBCheckBox
          Left = 8
          Top = 124
          Width = 60
          Height = 17
          Caption = 'CS'
          DataField = 'CS_Usa'
          DataSource = DsFisRegCad
          TabOrder = 5
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
        object dmkCheckBox7: TDBCheckBox
          Left = 8
          Top = 144
          Width = 60
          Height = 17
          Caption = 'ISS'
          DataField = 'ISS_Usa'
          DataSource = DsFisRegCad
          TabOrder = 6
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
      end
      object GroupBox9: TGroupBox
        Left = 77
        Top = 15
        Width = 84
        Height = 171
        Align = alLeft
        Caption = ' Al'#237'quota (%): '
        TabOrder = 1
        object DBEdit_01: TDBEdit
          Left = 8
          Top = 24
          Width = 68
          Height = 21
          DataField = 'ICMS_Alq'
          DataSource = DsFisRegCad
          TabOrder = 0
        end
        object DBEdit_02: TDBEdit
          Left = 8
          Top = 44
          Width = 68
          Height = 21
          DataField = 'IPI_Alq'
          DataSource = DsFisRegCad
          TabOrder = 1
        end
        object DBEdit_03: TDBEdit
          Left = 8
          Top = 64
          Width = 68
          Height = 21
          DataField = 'PIS_Alq'
          DataSource = DsFisRegCad
          TabOrder = 2
        end
        object DBEdit_04: TDBEdit
          Left = 8
          Top = 84
          Width = 68
          Height = 21
          DataField = 'COFINS_Alq'
          DataSource = DsFisRegCad
          TabOrder = 3
        end
        object DBEdit_05: TDBEdit
          Left = 8
          Top = 104
          Width = 68
          Height = 21
          DataField = 'IR_Alq'
          DataSource = DsFisRegCad
          TabOrder = 4
        end
        object DBEdit_06: TDBEdit
          Left = 8
          Top = 124
          Width = 68
          Height = 21
          DataField = 'CS_Alq'
          DataSource = DsFisRegCad
          TabOrder = 5
        end
        object DBEdit_07: TDBEdit
          Left = 8
          Top = 144
          Width = 68
          Height = 21
          DataField = 'ISS_Alq'
          DataSource = DsFisRegCad
          TabOrder = 6
        end
      end
      object PageControl1: TPageControl
        Left = 161
        Top = 15
        Width = 843
        Height = 171
        ActivePage = TabSheet1
        Align = alClient
        TabOrder = 2
        object TabSheet1: TTabSheet
          Caption = ' Regras CFOP '
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object DBGFisRegCFOP: TdmkDBGrid
            Left = 0
            Top = 0
            Width = 835
            Height = 143
            Align = alClient
            Columns = <
              item
                Expanded = False
                FieldName = 'CFOP'
                Width = 44
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'OrdCFOPGer'
                Title.Alignment = taCenter
                Title.Caption = 'Prefer'#234'ncia'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Contribui'
                Title.Caption = 'Contribuinte UF (I.E.)'
                Width = 108
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Interno'
                Title.Caption = 'Venda p/ mesma UF'
                Width = 108
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Proprio'
                Title.Caption = 'Fabrica'#231#227'o pr'#243'pria'
                Width = 108
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_CFOP'
                Title.Caption = 'Descri'#231#227'o CFOP'
                Visible = True
              end>
            Color = clWindow
            DataSource = DsFisRegCFOP
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'CFOP'
                Width = 44
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'OrdCFOPGer'
                Title.Alignment = taCenter
                Title.Caption = 'Prefer'#234'ncia'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Contribui'
                Title.Caption = 'Contribuinte UF (I.E.)'
                Width = 108
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Interno'
                Title.Caption = 'Venda p/ mesma UF'
                Width = 108
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Proprio'
                Title.Caption = 'Fabrica'#231#227'o pr'#243'pria'
                Width = 108
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_CFOP'
                Title.Caption = 'Descri'#231#227'o CFOP'
                Visible = True
              end>
          end
        end
        object TabSheet2: TTabSheet
          Caption = ' F'#243'rmulas '
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object DBMemo_01: TDBMemo
            Left = 4
            Top = 0
            Width = 826
            Height = 21
            DataField = 'ICMS_Frm'
            DataSource = DsFisRegCad
            TabOrder = 0
          end
          object DBMemo_02: TDBMemo
            Left = 4
            Top = 20
            Width = 826
            Height = 21
            DataField = 'IPI_Frm'
            DataSource = DsFisRegCad
            TabOrder = 1
          end
          object DBMemo_03: TDBMemo
            Left = 4
            Top = 40
            Width = 826
            Height = 21
            DataField = 'PIS_Frm'
            DataSource = DsFisRegCad
            TabOrder = 2
          end
          object DBMemo_04: TDBMemo
            Left = 4
            Top = 60
            Width = 826
            Height = 21
            DataField = 'COFINS_Frm'
            DataSource = DsFisRegCad
            TabOrder = 3
          end
          object DBMemo_05: TDBMemo
            Left = 4
            Top = 80
            Width = 826
            Height = 21
            DataField = 'IR_Frm'
            DataSource = DsFisRegCad
            TabOrder = 4
          end
          object DBMemo_06: TDBMemo
            Left = 4
            Top = 100
            Width = 826
            Height = 21
            DataField = 'CS_Frm'
            DataSource = DsFisRegCad
            TabOrder = 5
          end
          object DBMemo_07: TDBMemo
            Left = 4
            Top = 120
            Width = 826
            Height = 21
            DataField = 'ISS_Frm'
            DataSource = DsFisRegCad
            TabOrder = 6
          end
        end
      end
    end
    object Panel4: TPanel
      Left = 1
      Top = 423
      Width = 1006
      Height = 36
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 3
      object DBRadioGroup1: TDBRadioGroup
        Left = 0
        Top = 0
        Width = 361
        Height = 36
        Align = alLeft
        Caption = ' Financeiro: '
        Columns = 4
        DataField = 'Financeiro'
        DataSource = DsFisRegCad
        Items.Strings = (
          'Nenhum'
          #192' receber'#9
          #192' pagar'#9
          'Conta Corrente'#9)
        ParentBackground = True
        TabOrder = 0
        Values.Strings = (
          '0'
          '1'
          '2'
          '3')
      end
      object DBRadioGroup2: TDBRadioGroup
        Left = 361
        Top = 0
        Width = 645
        Height = 36
        Align = alClient
        Caption = 'Emiss'#227'o: '
        Columns = 5
        DataField = 'TipoEmiss'
        DataSource = DsFisRegCad
        Items.Strings = (
          'Indefinido'
          'Nota Fiscal'#9
          'Romaneio'#9
          'ECF n'#227'o concomit.'
          'ECF concomitante')
        ParentBackground = True
        TabOrder = 1
        Values.Strings = (
          '0'
          '1'
          '2'
          '3'
          '4')
      end
    end
    object DBRadioGroup3: TDBRadioGroup
      Left = 1
      Top = 233
      Width = 1006
      Height = 64
      Align = alTop
      Caption = ' Aplica'#231#227'o: '
      Columns = 5
      DataField = 'Aplicacao'
      DataSource = DsFisRegCad
      Items.Strings = (
        'Nenhuma aplica'#231#227'o'
        'Ped. Venda'
        'Condicional'
        'Transfer'#234'ncia'
        'Movimento Avulso'
        'Venda Balc'#227'o'
        'Ped. Compra'
        'Consigna'#231#227'o'
        'Produ'#231#227'o'
        'Remessa p/ Indust.'
        'Devolu'#231#227'o'
        'Troca'
        'Servi'#231'o'
        'Retorno Rem. p/ Indust.'
        'Balan'#231'o (uso restrito do adm. do sist.)')
      ParentBackground = True
      TabOrder = 4
      Values.Strings = (
        '0'
        '1'
        '2'
        '3'
        '4'
        '5'
        '6'
        '7'
        '8'
        '9'
        '10'
        '11'
        '12'
        '13'
        '14')
    end
    object Panel8: TPanel
      Left = 1
      Top = 459
      Width = 1006
      Height = 32
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 5
      object Label3: TLabel
        Left = 8
        Top = 9
        Width = 70
        Height = 13
        Caption = 'Modelo de NF:'
      end
      object Label16: TLabel
        Left = 512
        Top = 9
        Width = 52
        Height = 13
        Caption = 'Agrupador:'
      end
      object DBEdit100: TDBEdit
        Left = 84
        Top = 4
        Width = 52
        Height = 21
        DataField = 'ModeloNF'
        DataSource = DsFisRegCad
        TabOrder = 0
      end
      object DBEdit101: TDBEdit
        Left = 140
        Top = 4
        Width = 366
        Height = 21
        DataField = 'NOMEMODELONF'
        DataSource = DsFisRegCad
        TabOrder = 1
      end
      object DBEdit102: TDBEdit
        Left = 568
        Top = 4
        Width = 52
        Height = 21
        DataField = 'CODUSU_AGRUPADOR'
        DataSource = DsFisRegCad
        TabOrder = 2
      end
      object DBEdit103: TDBEdit
        Left = 624
        Top = 4
        Width = 374
        Height = 21
        DataField = 'NOMEAGRUPADOR'
        DataSource = DsFisRegCad
        TabOrder = 3
      end
    end
    object StaticText2: TStaticText
      Left = 1
      Top = 491
      Width = 1006
      Height = 17
      Align = alBottom
      Alignment = taCenter
      Caption = 'Observa'#231#245'es:'
      TabOrder = 6
      ExplicitWidth = 70
    end
    object DBMemo1: TDBMemo
      Left = 1
      Top = 508
      Width = 1006
      Height = 36
      Align = alBottom
      DataField = 'Observa'
      DataSource = DsFisRegCad
      TabOrder = 7
    end
    object DBGFisRegMvt: TDBGrid
      Left = 1
      Top = 316
      Width = 1006
      Height = 52
      Align = alTop
      BorderStyle = bsNone
      DataSource = DsFisRegMvt
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      TabOrder = 8
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnMouseMove = DBGFisRegMvtMouseMove
      Columns = <
        item
          Expanded = False
          FieldName = 'SEQ'
          Width = 32
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CODUSU_STQCENCAD'
          Title.Caption = 'C'#243'digo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOME_StqCenCad'
          Title.Caption = 'Descri'#231#227'o'
          Width = 221
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOME_TIPOMOV'
          Title.Caption = 'Movimento'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOME_TIPOCALC'
          Title.Caption = 'C'#225'lculo'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FILIAL'
          Title.Caption = 'Filial'
          Width = 32
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOME_EMP'
          Title.Caption = 'Descri'#231#227'o'
          Width = 200
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_GraCusApl'
          Title.Caption = 'Aplica'#231#227'o'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CU_GraCusPrc'
          Title.Caption = 'C'#243'digo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_GraCusPrc'
          Title.Caption = 'Descri'#231#227'o'
          Width = 180
          Visible = True
        end>
    end
    object DBGTitle1: TDBGrid
      Left = 1
      Top = 297
      Width = 1006
      Height = 19
      Align = alTop
      BorderStyle = bsNone
      Enabled = False
      TabOrder = 9
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          Width = 32
          Visible = True
        end
        item
          Expanded = False
          Title.Alignment = taCenter
          Title.Caption = 'Centro de estoque'
          Width = 286
          Visible = True
        end
        item
          Expanded = False
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          Title.Alignment = taCenter
          Title.Caption = 'Empresa'
          Width = 233
          Visible = True
        end
        item
          Expanded = False
          Title.Alignment = taCenter
          Title.Caption = 'Lista de pre'#231'os'
          Width = 298
          Visible = True
        end>
    end
    object StaticText4: TStaticText
      Left = 1
      Top = 544
      Width = 1006
      Height = 17
      Align = alBottom
      Alignment = taCenter
      Caption = 
        'Informa'#231#245'es adicionais de interesse do fisco (na NF-e) m'#225'x. 255 ' +
        'caracteres:'
      TabOrder = 10
      ExplicitWidth = 361
    end
    object DBMemo2: TDBMemo
      Left = 1
      Top = 561
      Width = 1006
      Height = 36
      Align = alBottom
      DataField = 'InfAdFisco'
      DataSource = DsFisRegCad
      TabOrder = 11
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    Caption = '                              Regras Fiscais'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object LaTipo: TdmkLabel
      Left = 925
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 603
      ExplicitTop = 2
      ExplicitHeight = 44
    end
    object Image1: TImage
      Left = 226
      Top = 1
      Width = 699
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 227
      ExplicitTop = 2
      ExplicitWidth = 376
      ExplicitHeight = 44
    end
    object PanelFill2: TPanel
      Left = 1
      Top = 1
      Width = 225
      Height = 46
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 0
        NumGlyphs = 2
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 1
        OnClick = SbNovoClick
        NumGlyphs = 2
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 2
        OnClick = SbNumeroClick
        NumGlyphs = 2
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 3
        OnClick = SbNomeClick
        NumGlyphs = 2
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 4
        OnClick = SbQueryClick
        NumGlyphs = 2
      end
    end
  end
  object DsFisRegCad: TDataSource
    DataSet = QrFisRegCad
    Left = 40
    Top = 12
  end
  object QrFisRegCad: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrFisRegCadBeforeOpen
    AfterOpen = QrFisRegCadAfterOpen
    BeforeClose = QrFisRegCadBeforeClose
    AfterScroll = QrFisRegCadAfterScroll
    OnCalcFields = QrFisRegCadCalcFields
    SQL.Strings = (
      'SELECT frc.Codigo, mnf.Nome NOMEMODELONF, '
      'fac.Nome NOMEAGRUPADOR, fac.CodUsu CODUSU_AGRUPADOR'
      'FROM fisregcad frc'
      'LEFT JOIN imprime   mnf ON mnf.Codigo=frc.ModeloNF'
      'LEFT JOIN fisagrcad fac ON fac.Codigo=frc.Agrupador')
    Left = 12
    Top = 12
    object QrFisRegCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrFisRegCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrFisRegCadNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
    object QrFisRegCadTipoMov: TSmallintField
      FieldName = 'TipoMov'
      Required = True
    end
    object QrFisRegCadICMS_Usa: TSmallintField
      FieldName = 'ICMS_Usa'
      Required = True
    end
    object QrFisRegCadICMS_Alq: TFloatField
      FieldName = 'ICMS_Alq'
      Required = True
    end
    object QrFisRegCadICMS_Frm: TWideMemoField
      FieldName = 'ICMS_Frm'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrFisRegCadIPI_Usa: TSmallintField
      FieldName = 'IPI_Usa'
      Required = True
    end
    object QrFisRegCadIPI_Alq: TFloatField
      FieldName = 'IPI_Alq'
      Required = True
    end
    object QrFisRegCadIPI_Frm: TWideMemoField
      FieldName = 'IPI_Frm'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrFisRegCadPIS_Usa: TSmallintField
      FieldName = 'PIS_Usa'
      Required = True
    end
    object QrFisRegCadPIS_Alq: TFloatField
      FieldName = 'PIS_Alq'
      Required = True
    end
    object QrFisRegCadPIS_Frm: TWideMemoField
      FieldName = 'PIS_Frm'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrFisRegCadCOFINS_Usa: TSmallintField
      FieldName = 'COFINS_Usa'
      Required = True
    end
    object QrFisRegCadCOFINS_Alq: TFloatField
      FieldName = 'COFINS_Alq'
      Required = True
    end
    object QrFisRegCadCOFINS_Frm: TWideMemoField
      FieldName = 'COFINS_Frm'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrFisRegCadIR_Usa: TSmallintField
      FieldName = 'IR_Usa'
      Required = True
    end
    object QrFisRegCadIR_Alq: TFloatField
      FieldName = 'IR_Alq'
      Required = True
    end
    object QrFisRegCadIR_Frm: TWideMemoField
      FieldName = 'IR_Frm'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrFisRegCadCS_Usa: TSmallintField
      FieldName = 'CS_Usa'
      Required = True
    end
    object QrFisRegCadCS_Alq: TFloatField
      FieldName = 'CS_Alq'
      Required = True
    end
    object QrFisRegCadCS_Frm: TWideMemoField
      FieldName = 'CS_Frm'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrFisRegCadISS_Usa: TSmallintField
      FieldName = 'ISS_Usa'
      Required = True
    end
    object QrFisRegCadISS_Alq: TFloatField
      FieldName = 'ISS_Alq'
      Required = True
    end
    object QrFisRegCadISS_Frm: TWideMemoField
      FieldName = 'ISS_Frm'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrFisRegCadAplicacao: TSmallintField
      FieldName = 'Aplicacao'
      Required = True
    end
    object QrFisRegCadFinanceiro: TSmallintField
      FieldName = 'Financeiro'
      Required = True
    end
    object QrFisRegCadTipoEmiss: TSmallintField
      FieldName = 'TipoEmiss'
      Required = True
    end
    object QrFisRegCadModeloNF: TIntegerField
      FieldName = 'ModeloNF'
      Required = True
    end
    object QrFisRegCadAgrupador: TIntegerField
      FieldName = 'Agrupador'
      Required = True
    end
    object QrFisRegCadObserva: TWideStringField
      FieldName = 'Observa'
      Size = 255
    end
    object QrFisRegCadNOMETIPOMOV: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMETIPOMOV'
      Size = 30
      Calculated = True
    end
    object QrFisRegCadNOMEMODELONF: TWideStringField
      FieldName = 'NOMEMODELONF'
      Size = 100
    end
    object QrFisRegCadNOMEAGRUPADOR: TWideStringField
      FieldName = 'NOMEAGRUPADOR'
      Size = 50
    end
    object QrFisRegCadCODUSU_AGRUPADOR: TIntegerField
      FieldName = 'CODUSU_AGRUPADOR'
      Required = True
    end
    object QrFisRegCadInfAdFisco: TWideStringField
      FieldName = 'InfAdFisco'
      Size = 255
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCondicoes
    Left = 68
    Top = 12
  end
  object PMCondicoes: TPopupMenu
    OnPopup = PMCondicoesPopup
    Left = 552
    Top = 8
    object Incluinovacondio1: TMenuItem
      Caption = '&Inclui nova condi'#231#227'o'
      OnClick = Incluinovacondio1Click
    end
    object Alteracondioatual1: TMenuItem
      Caption = '&Altera condi'#231#227'o atual'
      OnClick = Alteracondioatual1Click
    end
    object Excluicondioatual1: TMenuItem
      Caption = '&Exclui condi'#231#227'o atual'
      Enabled = False
    end
  end
  object DsFisRegMvt: TDataSource
    DataSet = QrFisRegMvt
    Left = 524
    Top = 8
  end
  object QrFisRegMvt: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrFisRegMvtCalcFields
    SQL.Strings = (
      'SELECT IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NOME_EMP,'
      'emp.FILIAL, gcp.CodUsu CU_GraCusPrc, gcp.Nome NO_GraCusPrc, '
      'ELT(frm.GraCusApl+1, "Custo", "Pre'#231'o", "? ? ?") NO_GraCusApl,'
      'scc.CodUsu CODUSU_STQCENCAD, '
      'scc.Nome NOME_StqCenCad, '
      
        'ELT(frm.TipoCalc+1, "NULO", "ADICIONA", "SUBTRAI") NOME_TIPOCALC' +
        ', '
      'ELT(frm.TipoMov+1, "ENTRADA", "SA'#205'DA") NOME_TIPOMOV, frm.*'
      'FROM fisregmvt frm'
      'LEFT JOIN entidades emp ON emp.Codigo=frm.Empresa'
      'LEFT JOIN gracusprc gcp ON gcp.Codigo=frm.GraCusPrc'
      'LEFT JOIN stqcencad scc ON scc.Codigo=frm. StqCenCad'
      'WHERE frm.Codigo=:P0'
      ''
      ''
      ''
      '')
    Left = 496
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFisRegMvtCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrFisRegMvtControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrFisRegMvtEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrFisRegMvtStqCenCad: TIntegerField
      FieldName = 'StqCenCad'
      Required = True
    end
    object QrFisRegMvtTipoMov: TSmallintField
      FieldName = 'TipoMov'
      Required = True
    end
    object QrFisRegMvtTipoCalc: TSmallintField
      FieldName = 'TipoCalc'
      Required = True
    end
    object QrFisRegMvtNOME_EMP: TWideStringField
      FieldName = 'NOME_EMP'
      Size = 100
    end
    object QrFisRegMvtNOME_StqCenCad: TWideStringField
      FieldName = 'NOME_StqCenCad'
      Size = 50
    end
    object QrFisRegMvtNOME_TIPOCALC: TWideStringField
      FieldName = 'NOME_TIPOCALC'
      Size = 8
    end
    object QrFisRegMvtNOME_TIPOMOV: TWideStringField
      FieldName = 'NOME_TIPOMOV'
      Size = 7
    end
    object QrFisRegMvtSEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
    object QrFisRegMvtFILIAL: TIntegerField
      FieldName = 'FILIAL'
    end
    object QrFisRegMvtCODUSU_STQCENCAD: TIntegerField
      FieldName = 'CODUSU_STQCENCAD'
      Required = True
    end
    object QrFisRegMvtCU_GraCusPrc: TIntegerField
      FieldName = 'CU_GraCusPrc'
      Required = True
    end
    object QrFisRegMvtNO_GraCusPrc: TWideStringField
      FieldName = 'NO_GraCusPrc'
      Size = 30
    end
    object QrFisRegMvtGraCusApl: TSmallintField
      FieldName = 'GraCusApl'
      Required = True
    end
    object QrFisRegMvtGraCusPrc: TIntegerField
      FieldName = 'GraCusPrc'
      Required = True
    end
    object QrFisRegMvtNO_GraCusApl: TWideStringField
      FieldName = 'NO_GraCusApl'
      Size = 5
    end
  end
  object QrImprime: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM imprime'
      'ORDER BY Nome')
    Left = 640
    Top = 8
    object QrImprimeCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrImprimeNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsImprime: TDataSource
    DataSet = QrImprime
    Left = 668
    Top = 8
  end
  object QrFisAgrCad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM fisagrcad'
      'ORDER BY Nome')
    Left = 700
    Top = 8
    object QrFisAgrCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFisAgrCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrFisAgrCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object dmkValUsu1: TdmkValUsu
    dmkEditCB = EdAgrupador
    Panel = PainelEdita
    QryCampo = 'Agrupador'
    UpdCampo = 'Agrupador'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 756
    Top = 8
  end
  object DsFisAgrCad: TDataSource
    DataSet = QrFisAgrCad
    Left = 728
    Top = 8
  end
  object PMMovimentacao: TPopupMenu
    Left = 580
    Top = 8
    object Incluiitemdemovimentao1: TMenuItem
      Caption = '&Inclui item de movimenta'#231#227'o'
      OnClick = Incluiitemdemovimentao1Click
    end
    object Alteraitemdemovimentaoatual1: TMenuItem
      Caption = '&Altera item de movimenta'#231#227'o atual'
      OnClick = Alteraitemdemovimentaoatual1Click
    end
    object Excluiitemnsdemovimentao1: TMenuItem
      Caption = '&Exclui item(ns) de movimenta'#231#227'o'
      OnClick = Excluiitemnsdemovimentao1Click
    end
  end
  object QrFisRegCFOP: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT fgc.*, cfp.Nome NO_CFOP'
      'FROM fisregcfop fgc'
      'LEFT JOIN cfop2003 cfp ON cfp.Codigo=fgc.CFOP'
      ''
      'WHERE fgc.Codigo=:P0')
    Left = 100
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFisRegCFOPCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrFisRegCFOPInterno: TSmallintField
      FieldName = 'Interno'
      Required = True
      MaxValue = 1
    end
    object QrFisRegCFOPContribui: TSmallintField
      FieldName = 'Contribui'
      Required = True
      MaxValue = 1
    end
    object QrFisRegCFOPProprio: TSmallintField
      FieldName = 'Proprio'
      Required = True
      MaxValue = 1
    end
    object QrFisRegCFOPCFOP: TWideStringField
      FieldName = 'CFOP'
      Required = True
      Size = 5
    end
    object QrFisRegCFOPNO_CFOP: TWideStringField
      FieldName = 'NO_CFOP'
      Size = 255
    end
    object QrFisRegCFOPOrdCFOPGer: TIntegerField
      FieldName = 'OrdCFOPGer'
      Required = True
    end
  end
  object DsFisRegCFOP: TDataSource
    DataSet = QrFisRegCFOP
    Left = 128
    Top = 12
  end
  object PMCFOP: TPopupMenu
    Left = 608
    Top = 8
    object IncluinovoCFOP1: TMenuItem
      Caption = '&Inclui novo CFOP'
      OnClick = IncluinovoCFOP1Click
    end
    object AlteraCFOPatual1: TMenuItem
      Caption = '&Altera CFOP atual'
      OnClick = AlteraCFOPatual1Click
    end
    object RetiraCFOPAtual1: TMenuItem
      Caption = '&Retira CFOP(s)'
      OnClick = RetiraCFOPAtual1Click
    end
  end
end
