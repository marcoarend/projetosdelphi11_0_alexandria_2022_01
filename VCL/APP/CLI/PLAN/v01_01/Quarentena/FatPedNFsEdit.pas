unit FatPedNFsEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DB, mySQLDbTables, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkLabel, Mask, dmkDBEdit, dmkGeral,
  Variants, dmkValUsu, ComCtrls, dmkEditDateTimePicker, dmkMemo;

type
  TFormChamou = (fcFmFatPedCab, fcFmFatPedNFs);
  TFmFatPedNFsEdit = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    QrPesqPrc: TmySQLQuery;
    QrPesqPrcPreco: TFloatField;
    LaTipo: TdmkLabel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    EdPlacaNr: TdmkEdit;
    Label11: TLabel;
    Label12: TLabel;
    EdPlacaUF: TdmkEdit;
    EdObservacao: TdmkEdit;
    Label13: TLabel;
    DsCFOP: TDataSource;
    DBMemo1: TDBMemo;
    DBMemo2: TDBMemo;
    Panel3: TPanel;
    Label14: TLabel;
    DBEdit1: TDBEdit;
    Label15: TLabel;
    DBEdit2: TDBEdit;
    Label16: TLabel;
    DBEdit3: TDBEdit;
    Label3: TLabel;
    EdNumeroNF: TdmkEdit;
    Label1: TLabel;
    EdFreteVal: TdmkEdit;
    Label2: TLabel;
    EdSeguro: TdmkEdit;
    Label10: TLabel;
    EdOutros: TdmkEdit;
    Label4: TLabel;
    EdQuantidade: TdmkEdit;
    Label5: TLabel;
    EdEspecie: TdmkEdit;
    Label6: TLabel;
    EdMarca: TdmkEdit;
    Label7: TLabel;
    EdNumero: TdmkEdit;
    Label8: TLabel;
    EdkgBruto: TdmkEdit;
    Label9: TLabel;
    EdkgLiqui: TdmkEdit;
    Label17: TLabel;
    EdCFOP1: TdmkEditCB;
    CBCFOP1: TdmkDBLookupComboBox;
    QrCFOP: TmySQLQuery;
    QrCFOPItens: TLargeintField;
    QrCFOPCFOP: TWideStringField;
    QrCFOPOrdCFOPGer: TIntegerField;
    QrCFOPNome: TWideStringField;
    QrCFOPDescricao: TWideMemoField;
    QrCFOPComplementacao: TWideMemoField;
    QrFatPedNFs: TmySQLQuery;
    QrFatPedNFsFilial: TIntegerField;
    QrFatPedNFsIDCtrl: TIntegerField;
    QrFatPedNFsTipo: TSmallintField;
    QrFatPedNFsOriCodi: TIntegerField;
    QrFatPedNFsEmpresa: TIntegerField;
    QrFatPedNFsNumeroNF: TIntegerField;
    QrFatPedNFsIncSeqAuto: TSmallintField;
    QrFatPedNFsAlterWeb: TSmallintField;
    QrFatPedNFsAtivo: TSmallintField;
    QrFatPedNFsSerieNFCod: TIntegerField;
    QrFatPedNFsSerieNFTxt: TWideStringField;
    QrFatPedNFsCO_ENT_EMP: TIntegerField;
    QrFatPedNFsDataCad: TDateField;
    QrFatPedNFsDataAlt: TDateField;
    QrFatPedNFsDataAlt_TXT: TWideStringField;
    QrFatPedNFsFreteVal: TFloatField;
    QrFatPedNFsSeguro: TFloatField;
    QrFatPedNFsOutros: TFloatField;
    QrFatPedNFsPlacaUF: TWideStringField;
    QrFatPedNFsPlacaNr: TWideStringField;
    QrFatPedNFsEspecie: TWideStringField;
    QrFatPedNFsMarca: TWideStringField;
    QrFatPedNFsNumero: TWideStringField;
    QrFatPedNFskgBruto: TFloatField;
    QrFatPedNFskgLiqui: TFloatField;
    QrFatPedNFsQuantidade: TWideStringField;
    QrFatPedNFsObservacao: TWideStringField;
    QrFatPedNFsCFOP1: TWideStringField;
    QrVolumes: TmySQLQuery;
    QrImprime: TmySQLQuery;
    QrImprimeCO_SerieNF: TIntegerField;
    QrImprimeSequencial: TIntegerField;
    QrImprimeIncSeqAuto: TSmallintField;
    QrImprimeCtrl_nfs: TIntegerField;
    QrImprimeMaxSeqLib: TIntegerField;
    QrImprimeTipoImpressao: TIntegerField;
    QrParamsEmp: TmySQLQuery;
    QrParamsEmpAssocModNF: TIntegerField;
    QrFatPedNFsNO_EMP: TWideStringField;
    DsImprime: TDataSource;
    QrFatPedCab: TmySQLQuery;
    QrFatPedCabCodigo: TIntegerField;
    QrFatPedCabEmpresa: TIntegerField;
    QrFatPedCabCliente: TIntegerField;
    QrFatPedCabTransporta: TIntegerField;
    QrFatPedCabFretePor: TSmallintField;
    QrFatPedCabModeloNF: TIntegerField;
    QrNF_X: TmySQLQuery;
    QrNF_XSerieNFTxt: TWideStringField;
    QrNF_XNumeroNF: TIntegerField;
    QrPrzT: TmySQLQuery;
    QrPrzTDias: TIntegerField;
    QrPrzTPercent1: TFloatField;
    QrPrzTPercent2: TFloatField;
    QrPrzTControle: TIntegerField;
    QrPrzX: TmySQLQuery;
    QrPrzXDias: TIntegerField;
    QrPrzXPercent: TFloatField;
    QrPrzXControle: TIntegerField;
    QrSumT: TmySQLQuery;
    QrSumTPercent1: TFloatField;
    QrSumTPercent2: TFloatField;
    QrSumTJurosMes: TFloatField;
    QrSumX: TmySQLQuery;
    QrSumXTotal: TFloatField;
    QrFatPedCabEMP_FILIAL: TIntegerField;
    QrFatPedCabAFP_Sit: TSmallintField;
    QrFatPedCabAFP_Per: TFloatField;
    QrFatPedCabAssociada: TIntegerField;
    QrFatPedCabASS_FILIAL: TIntegerField;
    QrFatPedCabEMP_CtaProdVen: TIntegerField;
    QrFatPedCabEMP_FaturaSep: TWideStringField;
    QrFatPedCabASS_CtaProdVen: TIntegerField;
    QrFatPedCabASS_FaturaDta: TSmallintField;
    QrFatPedCabCartEmis: TIntegerField;
    QrFatPedCabCondicaoPg: TIntegerField;
    QrFatPedCabEMP_FaturaDta: TSmallintField;
    QrFatPedCabAbertura: TDateTimeField;
    QrFatPedCabCodUsu: TIntegerField;
    QrFatPedCabEMP_FaturaSeq: TSmallintField;
    QrFatPedCabEMP_TxtProdVen: TWideStringField;
    QrFatPedCabASS_FaturaSeq: TSmallintField;
    QrFatPedCabASS_FaturaSep: TWideStringField;
    QrFatPedCabASS_TxtProdVen: TWideStringField;
    QrFatPedCabTIPOCART: TIntegerField;
    QrFatPedCabRepresen: TIntegerField;
    TPDtEmissNF: TdmkEditDateTimePicker;
    TPDtEntraSai: TdmkEditDateTimePicker;
    Label18: TLabel;
    Label19: TLabel;
    QrImprimeNO_SerieNF: TIntegerField;
    Splitter1: TSplitter;
    MeinfAdic_infCpl: TdmkMemo;
    EdRNTC: TdmkEdit;
    RNTC: TLabel;
    GroupBox3: TGroupBox;
    EdUFEmbarq: TdmkEdit;
    Label20: TLabel;
    Label21: TLabel;
    EdxLocEmbarq: TdmkEdit;
    QrVolumesVolumes: TLargeintField;
    QrVolumesNO_UnidMed: TWideStringField;
    StaticText1: TStaticText;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QrFatPedNFsAfterScroll(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenQrImprime();
    procedure IncluiNFs();
    procedure AlteraNFAtual();
  public
    { Public declarations }
    FFormChamou: TFormChamou;
    FThisFatID: Integer;
    FIDCtrl: Integer;

    procedure ReopenFatPedNFs(QuaisFiliais, FilialLoc: Integer);
    procedure ReopenFatPedCab(FatPedCab: Integer);
    function Encerra(): Boolean;
    function  ImprimeNF(): Boolean;

  end;

  var
  FmFatPedNFsEdit: TFmFatPedNFsEdit;

implementation

uses UnMyObjects, Module, UMySQLModule, ModuleGeral, NF1b, MyDBCheck, UnFinanceiro, GetData,
UnInternalConsts;

{$R *.DFM}

procedure TFmFatPedNFsEdit.AlteraNFAtual();
var
  Tipo, OriCodi, Empresa, SerieNFCod, NumeroNF, IncSeqAuto,
  UF: Integer;
  FreteVal, Seguro, Outros, kgBruto, kgLiqui: Double;
  SerieNFTxt, PlacaUF, PlacaNr, Especie, Marca, Numero, Observacao, Quantidade,
  CFOP1, DtEmissNF, DtEntraSai: String;
begin
  //usar QrFatPedNFs deste form!!!
  ReopenFatPedNFs(1,0);
  Tipo        := 1;
  OriCodi     := QrFatPedCabCodigo.Value;
  Empresa     := QrFatPedNFsCO_ENT_EMP.Value;
  SerieNFCod  := QrImprimeCO_SerieNF.Value;
  SerieNFTxt  := IntToStr(QrImprimeNO_SerieNF.Value);
  IncSeqAuto  := QrImprimeIncSeqAuto.Value;
  NumeroNF    := EdNumeroNF.ValueVariant;
  FreteVal    := EdFreteVal.ValueVariant;
  Seguro      := EdSeguro.ValueVariant;
  Outros      := EdOutros.ValueVariant;
  kgBruto     := EdkgBruto.ValueVariant;
  kgLiqui     := EdkgLiqui.ValueVariant;
  PlacaUF     := EdPlacaUF.ValueVariant;
  PlacaNr     := EdPlacaNr.ValueVariant;
  Especie     := EdEspecie.ValueVariant;
  Marca       := EdMarca.ValueVariant;
  Numero      := EdNumero.ValueVariant;
  Quantidade  := EdQuantidade.ValueVariant;
  Observacao  := EdObservacao.ValueVariant;
  CFOP1       := EdCFOP1.Text;
  DtEmissNF   := Geral.FDT(TPDtEmissNF.Date, 1);
  DtEntraSai  := Geral.FDT(TPDtEntraSai.Date, 1);
  if Trim(PlacaUF) <> '' then
  begin
    UF := MLAGeral.GetCodigoUF_da_SiglaUF(Geral.SoLetra_TT(PlacaUF));
    if UF = 0 then
    begin
      if Geral.MensagemBox('A UF ' + PlacaUF + ' n�o � reconhecida '+
      'pelo aplicativo como uma UF v�lida! Deseja continuar assim mesmo?',
      'Pergunta', MB_YESNOCANCEL+MB_ICONWARNING) <> ID_YES then Exit;
    end;
  end;
  PlacaNr := Trim(PlacaNr);
  if PlacaNr <> '' then
  begin
    if not MLAGeral.PlacaDetranValida(PlacaNr, False) then
    begin
      if Geral.MensagemBox('A placa ' + PlacaNr + ' n�o � reconhecida ' +
      'pelo aplicativo como uma placa v�lida! Deseja continuar assim mesmo?',
      'Pergunta', MB_YESNOCANCEL+MB_ICONWARNING) <> ID_YES then Exit;
    end;
  end;
  if IncSeqAuto = 0 then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE paramsnfs SET Sequencial=:P0');
    Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
    Dmod.QrUpd.SQL.Add('AND Sequencial<:P2');
    Dmod.QrUpd.Params[00].AsInteger := NumeroNF;
    Dmod.QrUpd.Params[01].AsInteger := QrImprimeCtrl_nfs.Value;
    Dmod.QrUpd.Params[02].AsInteger := NumeroNF;
    Dmod.QrUpd.ExecSQL;
  end;
  if NumeroNF < 1 then
  begin
    Geral.MensagemBox('N�mero inv�lido para a Nota Fiscal:' + #13#10 +
    FormatFloat('000000', NumeroNF), 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  //ReopenQrImprime;
  if NumeroNF > QrImprimeMaxSeqLib.Value then
  begin
    Geral.MensagemBox('N�mero inv�lido para a Nota Fiscal:' + #13#10 +
    FormatFloat('000000', NumeroNF), 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'stqmovnfsa', False, [
    'Tipo', 'OriCodi', 'Empresa', 'NumeroNF', 'IncSeqAuto', 'SerieNFCod',
    'SerieNFTxt', 'FreteVal', 'Seguro', 'Outros', 'PlacaUF', 'PlacaNr',
    'Especie', 'Marca', 'Numero', 'kgBruto', 'kgLiqui', 'Observacao',
    'Quantidade', 'CFOP1', 'DtEmissNF', 'DtEntraSai', 'infAdic_infCpl',
    'RNTC', 'UFEmbarq', 'xLocEmbarq'
  ], ['IDCtrl'], [
    Tipo, OriCodi, Empresa, NumeroNF, IncSeqAuto, SerieNFCod,
    SerieNFTxt, FreteVal, Seguro, Outros, PlacaUF, PlacaNr,
    Especie, Marca, Numero, kgBruto, kgLiqui, Observacao,
    Quantidade, CFOP1, DtEmissNF, DtEntraSai, MeinfAdic_infCpl.Text,
    EdRNTC.Text, EdUFEmbarq.Text, EdxLocEmbarq.Text
  ], [FIDCtrl], True) then
  begin
    //dmkPF.LeMeuTexto(Dmod.QrUpd.SQL.Text);
    Close;
  end;
end;

procedure TFmFatPedNFsEdit.BtOKClick(Sender: TObject);
begin
  if LaTipo.SQLType = stIns then
    IncluiNFs()
  else
    AlteraNFAtual();
end;

procedure TFmFatPedNFsEdit.IncluiNFs();
  procedure ExcluiNF(Tipo, OriCodi: Integer);
  begin
    // excluir nfe stqmovnfsa para n�o gerar erro quando tentar de novo
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM stqmovnfsa ');
    Dmod.QrUpd.SQL.Add('WHERE Tipo=:P0 AND OriCodi=:P1');
    Dmod.QrUpd.Params[00].AsInteger := Tipo;
    Dmod.QrUpd.Params[01].AsInteger := OriCodi;
    Dmod.QrUpd.ExecSQL;
    //
    Geral.MensagemBox('Erro no encerramento! Feche a janela e tente ' +
    'encerrar novamente! Caso n�o consiga, AVISE A DERMATEK.',
    'Aviso', MB_OK+MB_ICONWARNING);
    Close;
  end;
var
  IDCtrl, Tipo, OriCodi, Empresa, SerieNFCod, NumeroNF, IncSeqAuto, UF: Integer;
  FreteVal, Seguro, Outros, kgBruto, kgLiqui: Double;
  SerieNFTxt, PlacaUF, PlacaNr, Especie, Marca, Numero, Observacao, Quantidade,
  CFOP1, DtEmissNF, DtEntraSai: String;
  Incluir, Encerrou: Boolean;
begin
  //usar QrFatPedNFs deste form!!!
  Encerrou    := False;
  Tipo        := 1;
  OriCodi     := QrFatPedCabCodigo.Value;
  Empresa     := QrFatPedNFsCO_ENT_EMP.Value;
  SerieNFCod  := QrImprimeCO_SerieNF.Value;
  SerieNFTxt  := IntToStr(QrImprimeNO_SerieNF.Value);
  IncSeqAuto  := QrImprimeIncSeqAuto.Value;
  NumeroNF    := EdNumeroNF.ValueVariant;
  FreteVal    := EdFreteVal.ValueVariant;
  Seguro      := EdSeguro.ValueVariant;
  Outros      := EdOutros.ValueVariant;
  kgBruto     := EdkgBruto.ValueVariant;
  kgLiqui     := EdkgLiqui.ValueVariant;
  PlacaUF     := EdPlacaUF.ValueVariant;
  PlacaNr     := EdPlacaNr.ValueVariant;
  Especie     := EdEspecie.ValueVariant;
  Marca       := EdMarca.ValueVariant;
  Numero      := EdNumero.ValueVariant;
  Quantidade  := EdQuantidade.ValueVariant;
  Observacao  := EdObservacao.ValueVariant;
  CFOP1       := EdCFOP1.Text;
  DtEmissNF   := Geral.FDT(TPDtEmissNF.Date, 1);
  DtEntraSai  := Geral.FDT(TPDtEntraSai.Date, 1);
  if Trim(PlacaUF) <> '' then
  begin
    UF := MLAGeral.GetCodigoUF_da_SiglaUF(Geral.SoLetra_TT(PlacaUF));
    if UF = 0 then
    begin
      if Geral.MensagemBox('A UF ' + PlacaUF + ' n�o � reconhecida '+
      'pelo aplicativo como uma UF v�lida! Deseja continuar assim mesmo?',
      'Pergunta', MB_YESNOCANCEL+MB_ICONWARNING) <> ID_YES then Exit;
    end;
  end;
  PlacaNr := Trim(PlacaNr);
  if PlacaNr <> '' then
  begin
    if not MLAGeral.PlacaDetranValida(PlacaNr, False) then
    begin
      if Geral.MensagemBox('A placa ' + PlacaNr + ' n�o � reconhecida ' +
      'pelo aplicativo como uma placa v�lida! Deseja continuar assim mesmo?',
      'Pergunta', MB_YESNOCANCEL+MB_ICONWARNING) <> ID_YES then Exit;
    end;
  end;
  if IncSeqAuto = 0 then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE paramsnfs SET Sequencial=:P0');
    Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
    Dmod.QrUpd.SQL.Add('AND Sequencial<:P2');
    Dmod.QrUpd.Params[00].AsInteger := NumeroNF;
    Dmod.QrUpd.Params[01].AsInteger := QrImprimeCtrl_nfs.Value;
    Dmod.QrUpd.Params[02].AsInteger := NumeroNF;
    Dmod.QrUpd.ExecSQL;
  end;
  if NumeroNF < 1 then
  begin
    Geral.MensagemBox('N�mero inv�lido para a Nota Fiscal:' + #13#10 +
    FormatFloat('000000', NumeroNF), 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  if NumeroNF > QrImprimeMaxSeqLib.Value then
  begin
    Geral.MensagemBox('N�mero inv�lido para a Nota Fiscal:' + #13#10 +
    FormatFloat('000000', NumeroNF), 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  IDCtrl := UMyMod.BuscaEmLivreY_Def('stqmovnfsa', 'IDCtrl', stIns, 0);
  try
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'stqmovnfsa', False, [
      'Tipo', 'OriCodi', 'Empresa', 'NumeroNF', 'IncSeqAuto', 'SerieNFCod',
      'SerieNFTxt', 'FreteVal', 'Seguro', 'Outros', 'PlacaUF', 'PlacaNr',
      'Especie', 'Marca', 'Numero', 'kgBruto', 'kgLiqui', 'Observacao',
      'Quantidade', 'CFOP1', 'DtEmissNF', 'DtEntraSai', 'infAdic_infCpl',
      'RNTC', 'UFEmbarq', 'xLocEmbarq'
    ], ['IDCtrl'], [
      Tipo, OriCodi, Empresa, NumeroNF, IncSeqAuto, SerieNFCod,
      SerieNFTxt, FreteVal, Seguro, Outros, PlacaUF, PlacaNr,
      Especie, Marca, Numero, kgBruto, kgLiqui, Observacao,
      Quantidade, CFOP1, DtEmissNF, DtEntraSai, MeinfAdic_infCpl.Text,
      EdRNTC.Text, EdUFEmbarq.Text, EdxLocEmbarq.Text
    ], [IDCtrl], True) then
    begin
      ReopenFatPedNFs(2,0);
      if QrFatPedNFs.RecordCount > 0 then
      begin
        while not QrFatPedNFs.Eof do
        begin
          Incluir := True;
          //
          SerieNFTxt := IntToStr(QrImprimeNO_SerieNF.Value);
          NumeroNF   := DModG.BuscaProximoCodigoInt('paramsnfs', 'Sequencial',
          'WHERE Controle=' + dmkPF.FFP(QrImprimeCtrl_nfs.Value, 0), 0,
          QrImprimeMaxSeqLib.Value, 'S�rie: ' + SerieNFTxt + #13#10 +
          'Filial: ' + dmkPF.FFP(QrFatPedNFsFilial.Value, 0));
          //
          Tipo        := 1;
          OriCodi     := QrFatPedCabCodigo.Value;
          Empresa     := QrFatPedNFsCO_ENT_EMP.Value;
          SerieNFCod  := QrImprimeCO_SerieNF.Value;
          SerieNFTxt  := IntToStr(QrImprimeNO_SerieNF.Value);
          IncSeqAuto  := QrImprimeIncSeqAuto.Value;
          //NumeroNF    := EdNumeroNF.ValueVariant;
          FreteVal    := 0;
          Seguro      := 0;
          Outros      := 0;
          kgBruto     := 0;
          kgLiqui     := 0;
          PlacaUF     := '';
          PlacaNr     := '';
          Especie     := '';
          Marca       := '';
          Numero      := '';
          Quantidade  := '';
          Observacao  := '';
          CFOP1       := '';
          //
          if IncSeqAuto = 0 then
          begin
            Dmod.QrUpd.SQL.Clear;
            Dmod.QrUpd.SQL.Add('UPDATE paramsnfs SET Sequencial=:P0');
            Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
            Dmod.QrUpd.SQL.Add('AND Sequencial<:P2');
            Dmod.QrUpd.Params[00].AsInteger := NumeroNF;
            Dmod.QrUpd.Params[01].AsInteger := QrImprimeCtrl_nfs.Value;
            Dmod.QrUpd.Params[02].AsInteger := NumeroNF;
            Dmod.QrUpd.ExecSQL;
          end;
          if NumeroNF < 1 then
          begin
            Geral.MensagemBox('N�mero inv�lido para a Nota Fiscal:' + #13#10 +
            FormatFloat('000000', NumeroNF), 'Aviso', MB_OK+MB_ICONWARNING);
            Incluir := False;
          end;
          if Incluir and (NumeroNF > QrImprimeMaxSeqLib.Value) then
          begin
            Geral.MensagemBox('N�mero inv�lido para a Nota Fiscal:' + #13#10 +
            FormatFloat('000000', NumeroNF), 'Aviso', MB_OK+MB_ICONWARNING);
            Incluir := False;
          end;
          if Incluir then
          begin
            try
              IDCtrl := UMyMod.BuscaEmLivreY_Def('stqmovnfsa', 'IDCtrl', stIns, 0);
              if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'stqmovnfsa', False, [
              'Tipo', 'OriCodi', 'Empresa', 'NumeroNF', 'IncSeqAuto', 'SerieNFCod',
              'SerieNFTxt', 'FreteVal', 'Seguro', 'Outros', 'PlacaUF', 'PlacaNr',
              'Especie', 'Marca', 'Numero', 'kgBruto', 'kgLiqui', 'Observacao',
              'Quantidade', 'CFOP1', 'infAdic_infCpl', 'RNTC',
              'UFEmbarq', 'xLocEmbarq'], ['IDCtrl'], [
              Tipo, OriCodi, Empresa, NumeroNF, IncSeqAuto, SerieNFCod,
              SerieNFTxt, FreteVal, Seguro, Outros, PlacaUF, PlacaNr,
              Especie, Marca, Numero, kgBruto, kgLiqui, Observacao,
              Quantidade, CFOP1, MeinfAdic_infCpl.Text, EdRNTC.Text,
              EdUFEmbarq.Text, EdxLocEmbarq.Text], [IDCtrl], True) then
              begin
              // 
              end;
            except
              ExcluiNF(Tipo, OriCodi);
              raise;
            end;
            //
          end else
            ExcluiNF(Tipo, OriCodi);
          QrFatPedNFs.Next;
        end;
      end;
      //
      if FFormChamou = fcFmFatPedCab then
      begin
        Encerrou := Encerra();
      end;
      if Encerrou then
      begin
        ReopenFatPedNFs(1,0);
        ImprimeNF();
        Close;
      end;
    end;
  except
    ExcluiNF(Tipo, OriCodi);
    raise;
  end;
end;

procedure TFmFatPedNFsEdit.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFatPedNFsEdit.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFatPedNFsEdit.FormCreate(Sender: TObject);
begin
  FThisFatID := 1;
  // N�o pode ser aqui precisa criar primeiro!!!
  // Chamar na cria��o antes do ShowModal
  //ReopenFatPedNFs(1,0);
  TPDtEmissNF.Date := Date;
  TPDtEntraSai.Date := Date;
end;

procedure TFmFatPedNFsEdit.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

function TFmFatPedNFsEdit.ImprimeNF: Boolean;
var
  SQL_NCMs, SQL_ITS, SQL_Fat, SQL_TOT: String;
  CodTXT, EmpTXT: String;
begin
  Result := False;
  {
  if QrImprimeTipoImpressao.Value = 3 then // NF1
  begin
    //colocar valor de despesas acess�rias etc.
    //
    if DBCheck.CriaFm(TFmNF1b, FmNF1b, afmoNegarComAviso) then
    begin
      FmNF1b.LimpaVars;
      //
      CodTXT := dmkPF.FFP(QrFatPedCabCodigo.Value, 0);
      EmpTXT := dmkPF.FFP(QrFatPedCabEmpresa.Value, 0);
      SQL_ITS :=
      'SELECT med.Sigla NO_UNIDADE, smva.Empresa, smva.Preco,          '#13#10 +
      'smva.ICMS_Per, smva.IPI_Per, smva.GraGruX CU_PRODUTO,           '#13#10 +
      'smva.ID CONTROLE,                                               '#13#10 +
      'SUM(smva.Total) Total, SUM(smva.ICMS_Val) ICMS_Val,             '#13#10 +
      'SUM(smva.IPI_Val) IPI_Val, SUM(smva.Qtde) Qtde,                 '#13#10 +
      'gg1.CodUsu CU_GRUPO, gg1.Nome NO_GRUPO,                         '#13#10 +
      'gg1.CST_A, gg1.CST_B,                                           '#13#10 +
      'CONCAT(gg1.CST_A, LPAD(gg1.CST_B, 2, "0"))  CST_T,              '#13#10 +
      'gg1.NCM, ncm.Letras,                                            '#13#10 +
      'ggc.GraCorCad, gcc.CodUsu CU_COR, gcc.Nome NO_COR,              '#13#10 +
      'gti.Nome NO_TAM, ggx.GraGru1 CO_GRUPO, 0 Desc_Per               '#13#10 +
      'FROM stqmovvala smva                                            '#13#10 +
      'LEFT JOIN gragrux ggx ON ggx.Controle=smva.GraGruX              '#13#10 +
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1               '#13#10 +
      'LEFT JOIN gragruc   ggc ON ggc.Controle=ggx.GraGruC             '#13#10 +
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad             '#13#10 +
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI             '#13#10 +
      'LEFT JOIN ncms ncm ON ncm.ncm=gg1.ncm                           '#13#10 +
      'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed                 '#13#10 +
      'WHERE smva.Tipo=1                                               '#13#10 +
      'AND smva.OriCodi=' + CodTXT +                                    #13#10 +
      'AND smva.Empresa=' + EmpTXT +                                    #13#10 +
      'GROUP BY smva.Preco, smva.ICMS_Per, smva.IPI_Per, smva.GraGruX  ';
      //
      SQL_TOT :=
      'SELECT SUM(smva.Total) Total, SUM(smva.ICMS_Val) ICMS_Val,      '#13#10 +
      'SUM(smva.IPI_Val) IPI_Val, SUM(smva.Qtde) Qtde,                 '#13#10 +
      'COUNT(DISTINCT OriCnta) Volumes                                 '#13#10 +
      'FROM stqmovvala smva                                            '#13#10 +
      'WHERE smva.Tipo=1                                               '#13#10 +
      'AND smva.OriCodi=' + CodTXT +                                    #13#10 +
      'AND smva.Empresa=' + EmpTXT;
      //
      FmNF1b.CarregaNF1Pro(SQL_ITS, SQL_TOT);
      //

      SQL_Fat :=
      'SELECT Vencimento, Credito, Duplicata                           '#13#10 +
      'FROM lanctos                                                    '#13#10 +
      'WHERE FatID=1                                                   '#13#10 +
      'AND FatNum=' + CodTXT +                                          #13#10 +
      'AND CliInt=' + EmpTXT;
      //
      FmNF1b.CarregaNF1Fat(SQL_Fat, QrFatPedCabModeloNF.Value);
      //

      FmNF1b.EdNFSerie.Text         := IntToStr(QrImprimeNO_SerieNF.Value);
      FmNF1b.EdNFNumero.Text        := Geral.FFT(QrImprimeSequencial.Value, 0, siNegativo);
      FmNF1b.EdCliente.Text         := IntToStr(QrFatPedCabCliente.Value);
      FmNF1b.CBCliente.KeyValue     := QrFatPedCabCliente.Value;
      FmNF1b.EdBaseICMS.Text        := Geral.FFT(FmNF1b.QrTotalTotal.Value, 2, siNegativo);
      FmNF1b.EdValorICMS.Text       := Geral.FFT(FmNF1b.QrTotalICMS_Val.Value, 2, siNegativo);
      FmNF1b.EdBaseICMS_Sub.Text    := '0,00';
      FmNF1b.EdValorICMS_Sub.Text   := '0,00';
      FmNF1b.EdTotProd.Text         := Geral.FFT(FmNF1b.QrTotalTotal.Value, 2, siNegativo);
      FmNF1b.EdValorIPI.Text        := Geral.FFT(FmNF1b.QrTotalIPI_Val.Value, 2, siNegativo);
      FmNF1b.EdTotalNota.Text       := Geral.FFT(FmNF1b.QrTotalTotal.Value, 2, siNegativo);
      FmNF1b.EdTransp.Text          := IntToStr(QrFatPedCabTransporta.Value);
      FmNF1b.CBTransp.KeyValue      := QrFatPedCabTransporta.Value;
      FmNF1b.EdFPC.Text             := IntToStr(QrFatPedCabFretePor.Value);
      FmNF1b.EdFrete.Text           := Geral.FFT(QrFatPedNFsFreteVal.Value, 2, siNegativo);
      FmNF1b.EdSeguro.Text          := Geral.FFT(QrFatPedNFsSeguro.Value, 2, siNegativo);
      FmNF1b.EdOutros.Text          := Geral.FFT(QrFatPedNFsOutros.Value, 2, siNegativo);
      FmNF1b.EdUFPlaca.Text         := QrFatPedNFsPlacaUF.Value;
      FmNF1b.EdPlaca.Text           := QrFatPedNFsPlacaNr.Value;
      FmNF1b.EdQuantidade.Text      := QrFatPedNFsQuantidade.Value;
      FmNF1b.EdEspecie.Text         := QrFatPedNFsEspecie.Value;
      FmNF1b.EdMarca.Text           := QrFatPedNFsMarca.Value;
      FmNF1b.EdNumero.Text          := QrFatPedNFsNumero.Value;
      FmNF1b.EdkgB.Text             := Geral.FFT(QrFatPedNFskgBruto.Value, 3, siNegativo);
      FmNF1b.EdkgL.Text             := Geral.FFT(QrFatPedNFskgLiqui.Value, 3, siNegativo);
      FmNF1b.EdImprime.Text         := IntToStr(QrFatPedCabModeloNF.Value);
      FmNF1b.CBImprime.KeyValue     := QrFatPedCabModeloNF.Value;
      FmNF1b.EDCFOP1.Text           := QrFatPedNFsCFOP1.Value;
      FmNF1b.CBCFOP1.KeyValue       := QrFatPedNFsCFOP1.Value;
      //
      SQL_NCMs :=
      'SELECT DISTINCT gg1.NCM, ncm.Letras,                  '#13#10 +
      'ncm.ImpNaLista, ncm.Codigo                            '#13#10 +
      'FROM stqmovvala smva                                  '#13#10 +
      'LEFT JOIN gragrux ggx ON ggx.Controle=smva.GraGruX    '#13#10 +
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1     '#13#10 +
      'LEFT JOIN ncms ncm ON ncm.NCM=gg1.NCM                 '#13#10 +
      'WHERE smva.Tipo=1                                     '#13#10 +
      'AND smva.OriCodi=' +
      dmkPF.FFP(QrFatPedCabCodigo.Value,0)    +#13#10 +
      'AND smva.Empresa=' +
      dmkPF.FFP(QrFatPedCabEmpresa.Value,0)   +#13#10 +
      'AND gg1.NCM <> ""                                     '#13#10 +
      'ORDER BY ncm.Letras                                   ';
      FmNF1b.CarregaNCMs(QrFatPedCabModeloNF.Value, SQL_NCMs);
      //

      FmNF1b.ShowModal;
      FmNF1b.Destroy;
    end;
  end else if QrImprimeTipoImpressao.Value = 4 then // NF-e
  begin
    ??? Nada, � preciso abrir o form pai: FatPedNFs !!!
  end else Geral.MensagemBox('Tipo de impress�o n�o implementado! ' +
  'Verifique se o tipo de impress�o cadastrado no modelo de impress�o � para ' +
  'Nota Fiscal. Caso for informe a DERMATEK para implementa��o!', 'Aviso',
  MB_OK+MB_ICONWARNING);
  }
end;

procedure TFmFatPedNFsEdit.QrFatPedNFsAfterScroll(DataSet: TDataSet);
begin
  QrVolumes.Close;
  QrVolumes.Params[00].AsInteger := QrFatPedCabCodigo.Value;
  QrVolumes.Open;
  ReopenQrImprime();
end;

procedure TFmFatPedNFsEdit.ReopenFatPedCab(FatPedCab: Integer);
begin
  QrFatPedCab.Close;
  QrFatPedCab.Params[0].AsInteger := FatPedCab;
  QrFatPedCab.Open;
end;

procedure TFmFatPedNFsEdit.ReopenFatPedNFs(QuaisFiliais, FilialLoc: Integer);
var
  Txt: String;
begin
  if QuaisFiliais = 1 then
    Txt := '='
  else
    Txt := '<>';
  //
  QrFatPedNFs.Close;
  QrFatPedNFs.SQL.Clear;
  QrFatPedNFs.SQL.Add('SELECT DISTINCT ent.Filial, ent.Codigo CO_ENT_EMP,');
  QrFatPedNFs.SQL.Add('IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NO_EMP, smna.*');
  QrFatPedNFs.SQL.Add('FROM stqmovvala smva');
  QrFatPedNFs.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=smva.Empresa');
  QrFatPedNFs.SQL.Add('LEFT JOIN stqmovnfsa smna ON smna.Empresa=smva.Empresa');
  QrFatPedNFs.SQL.Add('          AND smva.Tipo=1');
  QrFatPedNFs.SQL.Add('          AND smna.OriCodi=:P0');
  QrFatPedNFs.SQL.Add('WHERE smva.Tipo=1');
  QrFatPedNFs.SQL.Add('AND smva.OriCodi=:P1');
  QrFatPedNFs.SQL.Add('AND ent.Codigo' + Txt + ':P2');
  QrFatPedNFs.SQL.Add('ORDER BY ent.Filial');
  QrFatPedNFs.SQL.Add('');
  QrFatPedNFs.SQL.Add('');

  QrFatPedNFs.Params[00].AsInteger := QrFatPedCabCodigo.Value;
  QrFatPedNFs.Params[01].AsInteger := QrFatPedCabCodigo.Value;
  QrFatPedNFs.Params[02].AsInteger := QrFatPedCabEmpresa.Value;
  QrFatPedNFs.Open;
  //
  QrFatPedNFs.Locate('Filial', FilialLoc, []);
end;

procedure TFmFatPedNFsEdit.ReopenQrImprime;
begin
  QrImprime.Close;
  if QrFatPedNFsCO_ENT_EMP.Value = QrFatPedCabEmpresa.Value then
  begin
    QrImprime.Params[0].AsInteger := QrFatPedCabModeloNF.Value;
    QrImprime.Open;
  end else begin
    QrParamsEmp.Close;
    QrParamsEmp.Params[0].AsInteger := QrFatPedCabEmpresa.Value;
    QrParamsEmp.Open;
    //
    if QrParamsEmpAssocModNF.Value > 0 then
    begin
      QrImprime.Params[0].AsInteger := QrParamsEmpAssocModNF.Value;
      QrImprime.Open;
    end;
  end;
end;

function TFmFatPedNFsEdit.Encerra(): Boolean;
  procedure IncluiLancto(Valor: Double; Data, Vencto: TDateTime; Duplicata,
  Descricao: String; TipoCart, Carteira, Genero, CliInt, Parcela,
  NotaFiscal, Account: Integer; SerieNF: String; VerificaCliInt: Boolean);
  begin
    UFinanceiro.LancamentoDefaultVARS;
    //
    FLAN_VERIFICA_CLIINT := VerificaCliInt;
    FLAN_Data       := Geral.FDT(Data, 1);
    FLAN_Tipo       := TipoCart;
    FLAN_Documento  := 0;
    FLAN_Credito    := Valor;
    FLAN_MoraDia    := QrSumTJurosMes.Value;
    FLAN_Multa      := 0;//
    FLAN_Carteira   := Carteira;
    FLAN_Genero     := Genero;
    FLAN_Cliente    := QrFatPedCabCliente.Value;
    FLAN_CliInt     := CliInt;
    //FLAN_Depto      := QrBolacarAApto.Value;
    //FLAN_ForneceI   := QrBolacarAPropriet.Value;
    FLAN_Vencimento := Geral.FDT(Vencto, 1);
    //FLAN_Mez        := MLAGeral.ITS_Null(QrPrevPeriodo.Value);
    FLAN_FatID      := FThisFatID;
    FLAN_FatNum     := QrFatPedCabCodigo.Value;
    FLAN_FatParcela := Parcela;
    FLAN_Descricao  := Descricao;
    FLAN_Duplicata  := Duplicata;
    FLAN_SerieNF    := SerieNF;
    FLAN_NotaFiscal := NotaFiscal;
    FLAN_Account    := Account;
    FLAN_Controle   := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres',
      'Controle', 'Lanctos', 'Lanctos', 'Controle');
    if UFinanceiro.InsereLancamento() then
    begin
      // nada
    end;
  end;
var
  DataFat, DataEnc: TDateTime;
  Agora, Duplicata, NF_Emp_Serie, NF_Ass_Serie: String;
  Codigo, FatPedCab, Parcela, NF_Emp_Numer, NF_Ass_Numer: Integer;
  T1, T2, F1, F2, V1, V2, P1, P2: Double;
  TemAssociada: Boolean;
  FreteVal, Seguro, Outros: Double;
begin
  Result := False;
  {
  Geral.MensagemBox('Em desenvolvimento',
  'Mensagem', MB_OK+MB_ICONINFORMATION);
  Exit;
  }
  // Encerra Faturamento
  if Geral.MensagemBox('Confirma o encerramento do faturamento?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONWARNING) = ID_YES then
  begin
    try
      FatPedCab := QrFatPedCabCodigo.Value;
      // Exclui lan�amentos para evitar duplica��o
      if (FThisFatID <> 0) and (FatPedCab <> 0) then
      begin
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('DELETE FROM lanctos ');
        Dmod.QrUpd.SQL.Add('WHERE FatID=:P0');
        Dmod.QrUpd.SQL.Add('AND FatNum=:P1');
        //
        Dmod.QrUpd.Params[00].AsInteger := FThisFatID;
        Dmod.QrUpd.Params[01].AsInteger := FatPedCab;
        Dmod.QrUpd.ExecSQL;
      end;
      //
      QrNF_X.Close;
      QrNF_X.Params[00].AsInteger := FatPedCab;
      QrNF_X.Params[01].AsInteger := QrFatPedCabEmpresa.Value;
      QrNF_X.Open;
      NF_Emp_Serie := QrNF_XSerieNFTxt.Value;
      NF_Emp_Numer := QrNF_XNumeroNF.Value;
      if (Trim(NF_Emp_Serie) = '') or (NF_Emp_Numer = 0) then
      begin
        Geral.MensagemBox('Encerramento n�o realizado!'#13#10 +
        'O n�mero de nota fiscal n�o foi definida para a empresa ' +
        FormatFloat('000', QrFatPedCabEMP_FILIAL.Value) + ' !'#13#10 +
        'Para definir o n�mero de nota fiscal clique no bot�o "Fatura" > "'+
        'Nota Fiscal"', 'Aviso', MB_OK+MB_ICONWARNING);
        Exit;
      end;
      //
      TemAssociada := (QrFatPedCabAFP_Sit.Value = 1) and (QrFatPedCabAFP_Per.Value > 0);
      if TemAssociada then
      begin
        QrNF_X.Close;
        QrNF_X.Params[00].AsInteger := FatPedCab;
        QrNF_X.Params[01].AsInteger := QrFatPedCabAssociada.Value;
        QrNF_X.Open;
        NF_Ass_Serie := QrNF_XSerieNFTxt.Value;
        NF_Ass_Numer := QrNF_XNumeroNF.Value;
        if (Trim(NF_Ass_Serie) = '') or (NF_Ass_Numer = 0) then
        begin
          Geral.MensagemBox('Encerramento n�o realizado!'#13#10 +
          'O n�mero de nota fiscal n�o foi definida para a empresa ' +
          FormatFloat('000', QrFatPedCabASS_FILIAL.Value) + ' !'#13#10 +
          'Para definir o n�mero de nota fiscal clique no bot�o "Fatura" > "'+
          'Nota Fiscal"', 'Aviso', MB_OK+MB_ICONWARNING);
          Exit;
        end;
        //
        P2 := QrFatPedCabAFP_Per.Value;
        P1 := 100 - P2;
      end else begin
        P1 := 100;
        P2 := 0;
        NF_Ass_Serie := '';
        NF_Ass_Numer := 0;
      end;
      //
      if QrFatPedCabEMP_CtaProdVen.Value = 0 then
      begin
        Geral.MensagemBox('Encerramento n�o realizado!'#13#10 +
        'A conta de venda de produtos n�o foi definida para a empresa ID = ' +
        IntToStr(QrFatPedCabEMP_FILIAL.Value) + '!', 'Aviso',
        MB_OK+MB_ICONWARNING);
        Exit;
      end;
      if (QrFatPedCabAssociada.Value <> 0) and
      (QrFatPedCabASS_CtaProdVen.Value = 0) and TemAssociada then
      begin
        Geral.MensagemBox('Encerramento n�o realizado!'#13#10 +
        'A conta de venda de produtos n�o foi definida para a empresa ID = ' +
        IntToStr(QrFatPedCabASS_FILIAL.Value) + '!', 'Aviso',
        MB_OK+MB_ICONWARNING);
        Exit;
      end;
      if QrFatPedCabCartEmis.Value = 0 then
      begin
        Geral.MensagemBox('Encerramento n�o realizado!'#13#10 +
        'A carteira n�o foi definida no pedido selecionado!', 'Aviso',
        MB_OK+MB_ICONWARNING);
        Exit;
      end;
      QrPrzT.Close;
      QrPrzT.Params[0].AsInteger := QrFatPedCabCondicaoPG.Value;
      QrPrzT.Open;
      if QrPrzT.RecordCount = 0 then
      begin
        Geral.MensagemBox('Encerramento n�o realizado!'#13#10 +
        'N�o h� parcela(s) definida(s) para a empresa ' +
        IntToStr(QrFatPedCabEMP_FILIAL.Value) + ' na condi��o de pagamento ' +
        'cadastrada no pedido selecionado!', 'Aviso',
        MB_OK+MB_ICONWARNING);
        Exit;
      end;
      if (QrFatPedCabAFP_Sit.Value = 1) and
      (QrFatPedCabAFP_Per.Value > 0) then
      begin
        QrSumT.Close;
        QrSumT.Params[0].AsInteger := QrFatPedCabCondicaoPG.Value;
        QrSumT.Open;
        if (QrSumTPercent2.Value <> QrFatPedCabAFP_Per.Value) then
        begin
          Geral.MensagemBox('Encerramento n�o realizado!'#13#10 +
          'Percentual da fatura parcial no pedido n�o confere com percentual ' +
          'nos prazos da condi��o de pagamento!', 'Aviso',
          MB_OK+MB_ICONWARNING);
          Exit;
        end;
        if QrSumT.RecordCount = 0 then
        begin
          Geral.MensagemBox('Encerramento n�o realizado!'#13#10 +
          'Percentual da fatura parcial no pedido n�o confere com percentual ' +
          'nos prazos da condi��o de pagamento!', 'Aviso',
          MB_OK+MB_ICONWARNING);
          Exit;
        end;
      end;
      Screen.Cursor := crHourGlass;
      DataEnc := DModG.ObtemAgora();
      Agora := Geral.FDT(DataEnc, 105);
      case QrFatPedCabEMP_FaturaDta.Value of
        0:
        begin
          MyObjects.CriaForm_AcessoTotal(TFmGetData, FmGetData);
          FmGetData.MCData.Date := Date;
          FmGetData.OcultaJurosEMulta();
          FmGetData.ShowModal;
          FmGetData.Destroy;
          DataFat := VAR_GETDATA;
        end;
        1: DataFat := QrFatPedCabAbertura.Value;
        2: DataFat := DataEnc;
        else DataFat := 0;
      end;
      if DataFat = 0 then
      begin
        Geral.MensagemBox('Encerramento n�o realizado!'#13#10 +
        'Data de faturamento n�o definida!'), 'Aviso',
        MB_OK+MB_ICONWARNING);
        Exit;
      end;
      //
      // Faturamento empresa principal
      FreteVal := EdFreteVal.ValueVariant;
      Seguro   := EdSeguro.ValueVariant;
      Outros   := EdOutros.ValueVariant;
      //
      QrSumX.Close;
      QrSumX.Params[00].AsInteger := QrFatPedCabCodigo.Value;
      QrSumX.Params[01].AsInteger := QrFatPedCabEmpresa.Value;
      QrSumX.Open;
      T1 := QrSumXTotal.Value + FreteVal + + Seguro + Outros;
      F1 := T1;
      QrPrzX.Close;
      QrPrzX.SQL.Clear;
      QrPrzX.SQL.Add('SELECT Controle, Dias, Percent1 Percent ');
      QrPrzX.SQL.Add('FROM pediprzits');
      QrPrzX.SQL.Add('WHERE Percent1 > 0');
      QrPrzX.SQL.Add('AND Codigo=:P0');
      QrPrzX.SQL.Add('ORDER BY Dias');
      QrPrzX.Params[00].AsInteger := QrFatPedCabCondicaoPG.Value;
      QrPrzX.Open;
      QrPrzX.First;
      while not QrPrzX.Eof do
      begin
        if QrPrzX.RecordCount = QrPrzX.RecNo then
          V1 := F1
        else begin
          if P1 = 0 then V1 := 0 else
            V1 := (Round(T1 * (QrPrzXPercent.Value / P1 * 100))) / 100;

          F1 := F1 - V1;
        end;
        QrPrzT.Locate('Controle', QrPrzXControle.Value, []);
        Parcela := QrPrzT.RecNo;
        Duplicata := FormatFloat('000000', QrFatPedCabCodUsu.Value) +
          QrFatPedCabEMP_FaturaSep.Value + MLAGeral.ParcelaFatura(
          QrPrzX.RecNo, QrFatPedCabEMP_FaturaSeq.Value);
        IncluiLancto(V1, DataFat, DataFat + QrPrzXDias.Value, Duplicata,
          QrFatPedCabEMP_TxtProdVen.Value, QrFatPedCabTIPOCART.Value,
          QrFatPedCabCartEmis.Value, QrFatPedCabEMP_CtaProdVen.Value,
          QrFatPedCabEmpresa.Value, Parcela, NF_Emp_Numer,
          QrFatPedCabRepresen.Value, NF_Emp_Serie, True);
        //
        QrPrzX.Next;
      end;


      // Faturamento associada
      if TemAssociada then
      begin
        QrSumX.Close;
        QrSumX.Params[00].AsInteger := QrFatPedCabCodigo.Value;
        QrSumX.Params[01].AsInteger := QrFatPedCabAssociada.Value;
        QrSumX.Open;
        T2 := QrSumXTotal.Value;
        F2 := T2;
        //
        QrPrzX.Close;
        QrPrzX.SQL.Clear;
        QrPrzX.SQL.Add('SELECT Controle, Dias, Percent2 Percent ');
        QrPrzX.SQL.Add('FROM pediprzits');
        QrPrzX.SQL.Add('WHERE Percent2 > 0');
        QrPrzX.SQL.Add('AND Codigo=:P0');
        QrPrzX.SQL.Add('ORDER BY Dias');
        QrPrzX.Params[00].AsInteger := QrFatPedCabCondicaoPG.Value;
        QrPrzX.Open;
        QrPrzX.First;
        while not QrPrzX.Eof do
        begin
          if QrPrzX.RecordCount = QrPrzX.RecNo then
            V2 := F2
          else begin
            if P2 = 0 then V2 := 0 else
              V2 := (Round(T2 * (QrPrzXPercent.Value / P2 * 100))) / 100;
            F2 := F2 - V2;
          end;
          QrPrzT.Locate('Controle', QrPrzXControle.Value, []);
          Parcela := QrPrzT.RecNo;
          Duplicata := FormatFloat('000000', QrFatPedCabCodUsu.Value) +
            QrFatPedCabASS_FaturaSep.Value + MLAGeral.ParcelaFatura(
            QrPrzX.RecNo, QrFatPedCabASS_FaturaSeq.Value);
          IncluiLancto(V2, DataFat, DataFat + QrPrzXDias.Value, Duplicata,
            QrFatPedCabASS_TxtProdVen.Value, QrFatPedCabTIPOCART.Value,
            QrFatPedCabCartEmis.Value, QrFatPedCabASS_CtaProdVen.Value,
            QrFatPedCabAssociada.Value, Parcela, NF_Ass_Numer,
            QrFatPedCabRepresen.Value, NF_Ass_Serie, False);
          //
          QrPrzX.Next;
        end;
      end;
      //
      // Ativa itens no movimento
      Codigo := QrFatPedCabCodigo.Value;
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE stqmovitsa SET Ativo=1');
      Dmod.QrUpd.SQL.Add('WHERE Tipo=1 AND OriCodi=:P0 ');
      Dmod.QrUpd.Params[00].AsInteger := Codigo;
      Dmod.QrUpd.ExecSQL;
      //
      // Encerra definivamente faturamento
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE fatpedcab SET ');
      Dmod.QrUpd.SQL.Add('Status=9, Encerrou=:P0, DataFat=:P1 ');
      Dmod.QrUpd.SQL.Add('WHERE Codigo=:P2 ');
      Dmod.QrUpd.Params[00].AsString  := Agora;
      Dmod.QrUpd.Params[01].AsString  := Geral.FDT(DataFat, 105);
      Dmod.QrUpd.Params[02].AsInteger := Codigo;
      Dmod.QrUpd.ExecSQL;
      //
      //
      Result := True;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

end.

