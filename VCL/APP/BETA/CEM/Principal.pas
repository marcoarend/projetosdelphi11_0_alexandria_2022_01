unit Principal;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Dialogs, FMXTee.Engine, FMXTee.Procs,
  FMXTee.Chart, FMX.Types3D, FMXTee.Chart3D, Data.DB, Data.SqlExpr, FMX.Layouts,
  FMX.Memo, Data.FMTBcd, Data.Win.ADODB, ZAbstractConnection, ZConnection;

type
  TForm1 = class(TForm)
    outputMemo: TMemo;
    connectButton: TButton;
    executeButton: TButton;
    SQLConnection1: TSQLConnection;
    SQLQuery1: TSQLQuery;
    ADOConn: TADOConnection;
    ADOTable1: TADOTable;
    ZConnection1: TZConnection;
    procedure connectButtonClick(Sender: TObject);
    procedure executeButtonClick(Sender: TObject);
  private
    { Private declarations }
    procedure ShowSelectResults();
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

procedure TForm1.connectButtonClick(Sender: TObject);
begin
{
  SQLConnection1.Params.Add('Database=full_path_to_your_database_file');
  try
    // Establish the connection.
    SQLConnection1.Connected := true;
    executeButton.Enabled := true;
    outputMemo.Text := 'Connection established!';
  except
    on E: EDatabaseError do
      ShowMessage('Exception raised with message' + E.Message);
  end;
}
  ADOConn.LoginPrompt := False;

  try
    ADOConn.Connected := True;
  except
    on e: EADOError do
    begin
      ShowMessage('Error while connecting');

      Exit;
    end;
  end;
end;

procedure TForm1.executeButtonClick(Sender: TObject);
var
  query: String;
begin
  outputMemo.ClearSelection;
  // A random query
  query := 'SELECT * FROM Employee;';
  try
  // Assign the query to the object SQLQuery1.
    SQLQuery1.SQL.Text := query;
    SQLQuery1.Active := true;
  except
    on E: Exception do
      outputMemo.Text := 'Exception raised with message: ' + E.Message;
  end;
  // Show the results of the query in a TMemo control.
  ShowSelectResults();
end;

procedure TForm1.ShowSelectResults();
var
  names: TStringList;
  i: Integer;
  currentField: TField;
  currentLine: string;
begin
  if not SQLQuery1.IsEmpty then
  begin
    SQLQuery1.First;
    names := TStringList.Create;
    try
      SQLQuery1.GetFieldNames(names);
      while not SQLQuery1.Eof do
      begin
        currentLine := '';
        for i := 0 to names.Count - 1 do
        begin
          currentField := SQLQuery1.FieldByName(names[i]);
          currentLine := currentLine + ' ' + currentField.AsString;
        end;
        outputMemo.Lines.Add(currentLine);
        SQLQuery1.Next;
      end;
    finally
      names.Free;
    end;
  end;
end;

end.
