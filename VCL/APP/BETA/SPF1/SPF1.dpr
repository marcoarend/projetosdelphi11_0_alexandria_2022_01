program SPF1;

uses
  Vcl.Forms,
  Principal in 'Units\Principal.pas' {FmPrincipal},
  UnAppCreate in 'Units\UnAppCreate.pas',
  UMemModule in '..\..\..\UTL\MemoryTable\v01_01\UMemModule.pas',
  Sorteio in 'Units\Sorteio.pas' {FmSorteio},
  Module in 'Units\Module.pas' {Dmod: TDataModule},
  Configs in 'Units\Configs.pas' {FmConfigs},
  Configs_v001 in 'Schemas\Configs_v001.pas',
  EntCliCad in 'Units\EntCliCad.pas' {FmEntCliCad},
  UnApp_Vars in 'Units\UnApp_Vars.pas',
  UnApp_Consts in 'Units\UnApp_Consts.pas',
  MyListas in 'Units\MyListas.pas' {$R *.res},
  UnInternalConsts in '..\..\..\UTL\_UNT\v01_01\UnInternalConsts.pas',
  UnDmkEnums in '..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnDmkEnums.pas',
  UnMyObjects in '..\..\..\UTL\_UNT\v01_01\UnMyObjects.pas',
  ZCF2 in '..\..\..\UTL\_UNT\v01_01\ZCF2.pas',
  UnDmkProcFunc in '..\..\..\UTL\_UNT\v01_01\UnDmkProcFunc.pas',
  UnitMD5 in '..\..\..\..\Outros\Encrypt\UnitMD5.pas',
  MemDBCheck in '..\..\..\UTL\MemoryTable\v01_01\MemDBCheck.pas',
  NomeX in '..\..\..\UTL\_FRM\v01_01\NomeX.pas' {FmNomeX},
  UnGOTOm in '..\..\..\..\Outros\MemoryTable\UnGOTOm.pas',
  GModule in '..\..\..\UTL\MemoryTable\v01_01\GModule.pas' {GMod: TDataModule},
  UnMyVCLref in '..\..\..\UTL\_UNT\v01_01\UnMyVCLref.pas',
  TesteXML in '..\..\..\UTL\XML\v01_01\TesteXML.pas' {FmTesteXML},
  MyGlyfs in '..\..\..\UTL\_FRM\v01_01\MyGlyfs.pas' {FmMyGlyfs},
  AppListas in 'Units\AppListas.pas',
  SorteioCab_v001 in 'Schemas\SorteioCab_v001.pas',
  EntCliCad_v001 in 'Schemas\EntCliCad_v001.pas',
  MeuDBUses in '..\..\..\UTL\MemoryTable\v01_01\MeuDBUses.pas' {FmMeuDBUses},
  SelRadioGroup in '..\..\..\UTL\_FRM\v01_01\SelRadioGroup.pas' {FmSelRadioGroup},
  MeuFrx in '..\..\..\UTL\Print\v01_01\MeuFrx.pas' {FmMeuFrx},
  CustomFR3Imp in '..\..\..\UTL\Print\v01_01\CustomFR3Imp.pas' {FmCustomFR3Imp};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFmPrincipal, FmPrincipal);
  Application.CreateForm(TGMod, GMod);
  Application.CreateForm(TDmod, Dmod);
  Application.CreateForm(TFmMyGlyfs, FmMyGlyfs);
  Application.CreateForm(TFmMyGlyfs, FmMyGlyfs);
  Application.CreateForm(TFmNomeX, FmNomeX);
  Application.CreateForm(TGMod, GMod);
  Application.CreateForm(TFmNomeX, FmNomeX);
  Application.CreateForm(TGMod, GMod);
  Application.CreateForm(TFmTesteXML, FmTesteXML);
  Application.CreateForm(TFmMyGlyfs, FmMyGlyfs);
  Application.CreateForm(TFmMeuDBUses, FmMeuDBUses);
  Application.Run;
end.
