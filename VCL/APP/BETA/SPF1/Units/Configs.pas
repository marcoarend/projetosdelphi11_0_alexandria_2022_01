unit Configs;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkDBGridZTO,
  ABSMain, dmkRadioGroup;

type
  TFmConfigs = class(TForm)
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    Panel5: TPanel;
    Label1: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    EdVersao: TdmkEdit;
    EdAccNome: TdmkEdit;
    EdAccID: TdmkEdit;
    EdUserID: TdmkEdit;
    EdDefLstPrc: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdVersaoChange(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmConfigs: TFmConfigs;

implementation

uses UnMyObjects, Module, UMemModule, UnApp_Consts;

{$R *.DFM}

procedure TFmConfigs.BtOKClick(Sender: TObject);
const
  Ativo = 1;
var
  RecipPosTp, DiluProdu: Integer;
  Produto, Origem, Embalagem, MyID: String;
begin
  Dmod.TbConfigs.Edit;
  //
  Dmod.TbConfigsversao.Value    := EdVersao.ValueVariant;
  Dmod.TbConfigsAccNome.Value   := EdAccNome.Text;
  Dmod.TbConfigsAccID.Value     := EdAccID.Text;
  Dmod.TbConfigsUserID.Value    := EdUserID.Text;
  Dmod.TbConfigsDefLstPrc.Value := EdDefLstPrc.Text;
  Dmod.TbConfigsAtivo.Value     := Ativo;
  //
  Dmod.TbConfigs.Post;
  //
  Dmod.SalvaXML_Configs(True);
  //
  Close;
end;

procedure TFmConfigs.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmConfigs.EdVersaoChange(Sender: TObject);
begin
(*
  QrPrdLstIts.Close;
  UMemMod.AbreABSQuery1(QrPrdLstIts, [
  'SELECT * FROM prdlstits ',
  'WHERE Produto LIKE "%' + EdProduto.Text + '%"',
  '']);
*)
end;

procedure TFmConfigs.FormActivate(Sender: TObject);
begin
 // MyObjects.CorIniComponente();
end;

procedure TFmConfigs.FormCreate(Sender: TObject);
begin
  //ImgTipo.SQLType := stLok;
  EdVersao.ValueVariant := Geral.DMV_Dot(versaoConfigs);
end;

procedure TFmConfigs.FormResize(Sender: TObject);
begin
  //MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  //[LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmConfigs.SpeedButton1Click(Sender: TObject);
const
  Titulo = 'Defini��o de User ID';
  ID_Ini  = '<created_by>';
  //'e45251346aeac1a13d1ff9ce83a05db0'
  ID_Fim = '</created_by>';
var
  IniDir, Arquivo, Filtro, Texto, UserID: String;
  Tam, K, T: Integer;
begin
  Tam := Length(ID_Ini);
  IniDir  := CO_DB_APP_PATH;
  Arquivo := '';
  Filtro  := 'Arquivos FAST|*.spf1;Todos Arquivos|*.*';
  if MyObjects.FileOpenDialog(Self, IniDir, Arquivo, Titulo, Filtro, [], Arquivo) then
  begin
    if FileExists(Arquivo) then
    begin
      Screen.Cursor := crHourGlass;
      Geral.LeArquivoToString(Arquivo, Texto);
      UserID := '';
      K := pos(ID_Ini, Texto);
      while (K > 0) and (UserID = '') do
      begin
        K := K + Tam;
        Texto := Copy(Texto, K);
        T := pos(ID_Fim, Texto) - 1;
        UserID := Copy(Texto, 1, T);
      end;
      if UserID <> '' then
        EdUserID.Text := UserID
      else
        Geral.MB_Aviso('N�o foi poss�vel localizar o ID do usu�rio no arquivo: ' + Arquivo);
      //
      Screen.Cursor := crDefault;
    end else
      Geral.MB_Aviso('Arquivo n�o localizado: ' + Arquivo);
  end;
end;

procedure TFmConfigs.SpeedButton2Click(Sender: TObject);
const
  Titulo = 'Sele��o de Lista de Pre�os Base';
var
  IniDir, Arquivo, Filtro, Texto: String;
begin
  IniDir  := CO_DB_APP_PATH + CO_PrdPrc_NSubDir;
  Arquivo := '';
  Filtro  := 'Arquivos de pre�os|*.' + CO_PrdPrc_NomeExt +';Todos Arquivos|*.*';
  if MyObjects.FileOpenDialog(Self, IniDir, Arquivo, Titulo, Filtro, [], Arquivo) then
  begin
    if FileExists(Arquivo) then
      EdDefLstPrc.Text := Arquivo
  end else
    Geral.MB_Aviso('Arquivo n�o localizado: ' + Arquivo);
end;

end.
