unit UnApp_Consts;

interface

uses UnDmkEnums;

const
  CO_DB_APP_MAIN_DB = 'MEMORY';
  CO_DB_APP_PATH = 'C:\Dermatek\SPF1\';

  CO_Configs_Arquivo = 'Configs';
  CO_Configs_NSubDir = '';
  CO_Configs_NomeExt = 'stngs';

  CO_EntCliCad_Arquivo = 'EntCliCad';
  CO_EntCliCad_NSubDir = '';
  CO_EntCliCad_NomeExt = 'entcli';

  CO_PrdPrc_NSubDir = 'Precos';
  CO_PrdPrc_NomeExt = 'lstprc';

  CO_PrdLst_NSubDir = 'Produtos';
  CO_PrdLst_NomeExt = 'prdlst';


implementation

end.
