object FmSorteio: TFmSorteio
  Left = 0
  Top = 0
  Caption = 'TDI-SORT1-001 :: Sorteio'
  ClientHeight = 461
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 784
    Height = 347
    ActivePage = TSReceita
    Align = alClient
    TabOrder = 0
    ExplicitWidth = 1008
    object TSReceita: TTabSheet
      Caption = 'Pilotos'
      ImageIndex = 1
      object dmkDBGridZTO1: TdmkDBGridZTO
        Left = 0
        Top = 0
        Width = 776
        Height = 319
        Align = alClient
        DataSource = DsPilotos
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        RowColors = <>
        SortArrow.Column = 'Pais'
        Columns = <
          item
            Expanded = False
            FieldName = 'CliCodi'
            Title.Caption = 'C'#243'digo'
            Width = 76
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CliNome'
            Title.Caption = 'Nome'
            Width = 281
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Ordem'
            Width = 38
            Visible = True
          end>
      end
    end
    object TabSheet1: TTabSheet
      Caption = 'S'#243'cios'
      ImageIndex = 1
      object dmkDBGridZTO2: TdmkDBGridZTO
        Left = 0
        Top = 0
        Width = 776
        Height = 319
        Align = alClient
        DataSource = DsSocios
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        RowColors = <>
        SortArrow.Column = 'Pais'
        Columns = <
          item
            Expanded = False
            FieldName = 'CliCodi'
            Title.Caption = 'C'#243'digo'
            Width = 76
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CliNome'
            Title.Caption = 'Nome'
            Width = 281
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Ordem'
            Width = 38
            Visible = True
          end>
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Sorteio'
      ImageIndex = 2
      ExplicitWidth = 1000
      object SG1: TStringGrid
        Left = 0
        Top = 0
        Width = 776
        Height = 319
        Align = alClient
        ColCount = 4
        DefaultColWidth = 40
        DefaultRowHeight = 21
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
        TabOrder = 0
        ExplicitWidth = 1000
        ColWidths = (
          40
          160
          254
          69)
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 347
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    ExplicitWidth = 1008
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 1004
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 391
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 2
    ExplicitWidth = 1008
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 862
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = 'Sair'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        Visible = False
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 860
      object BtRefresh: TBitBtn
        Tag = 18
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Reabre'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtRefreshClick
      end
      object BitBtn1: TBitBtn
        Tag = 20
        Left = 240
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Sorteia'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BitBtn1Click
      end
      object BtImprime: TBitBtn
        Tag = 20
        Left = 464
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Imprime'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtImprimeClick
      end
    end
  end
  object QrPilotos: TABSQuery
    CurrentVersion = '7.20 '
    DatabaseName = 'MEMORY'
    InMemory = True
    ReadOnly = False
    Left = 136
    Top = 252
    object QrPilotosversao: TFloatField
      FieldName = 'versao'
    end
    object QrPilotosCliCodi: TWideStringField
      FieldName = 'CliCodi'
      Size = 60
    end
    object QrPilotosCliNome: TWideStringField
      FieldName = 'CliNome'
      Size = 255
    end
    object QrPilotosNomeUTF: TWideStringField
      FieldName = 'NomeUTF'
      Size = 255
    end
    object QrPilotosCidade: TWideStringField
      FieldName = 'Cidade'
      Size = 255
    end
    object QrPilotosUF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrPilotosPais: TWideStringField
      FieldName = 'Pais'
      Size = 255
    end
    object QrPilotosAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPilotosOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrPilotosTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrPilotosNO_Tipo: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_Tipo'
      Size = 512
      Calculated = True
    end
    object QrPilotosAleatorio: TIntegerField
      FieldName = 'Aleatorio'
    end
  end
  object DsPilotos: TDataSource
    DataSet = QrPilotos
    Left = 136
    Top = 300
  end
  object QrSocios: TABSQuery
    CurrentVersion = '7.20 '
    DatabaseName = 'MEMORY'
    InMemory = True
    ReadOnly = False
    Left = 220
    Top = 252
    object QrSociosversao: TFloatField
      FieldName = 'versao'
    end
    object QrSociosCliCodi: TWideStringField
      FieldName = 'CliCodi'
      Size = 60
    end
    object QrSociosCliNome: TWideStringField
      FieldName = 'CliNome'
      Size = 255
    end
    object QrSociosNomeUTF: TWideStringField
      FieldName = 'NomeUTF'
      Size = 255
    end
    object QrSociosCidade: TWideStringField
      FieldName = 'Cidade'
      Size = 255
    end
    object QrSociosUF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrSociosPais: TWideStringField
      FieldName = 'Pais'
      Size = 255
    end
    object QrSociosAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrSociosOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrSociosTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrSociosNO_Tipo: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_Tipo'
      Size = 512
      Calculated = True
    end
    object QrSociosAleatorio: TIntegerField
      FieldName = 'Aleatorio'
    end
  end
  object DsSocios: TDataSource
    DataSet = QrSocios
    Left = 220
    Top = 300
  end
end
