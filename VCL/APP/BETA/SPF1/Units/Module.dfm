object Dmod: TDmod
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 460
  Width = 737
  object QrUpd: TABSQuery
    CurrentVersion = '7.20 '
    DatabaseName = 'MEMORY'
    InMemory = True
    ReadOnly = False
    Left = 60
    Top = 4
  end
  object TbConfigs: TABSTable
    CurrentVersion = '7.20 '
    DatabaseName = 'MEMORY'
    InMemory = True
    ReadOnly = False
    TableName = 'configs'
    Exclusive = False
    Left = 136
    Top = 4
    object TbConfigsversao: TFloatField
      FieldName = 'versao'
    end
    object TbConfigsAccNome: TWideStringField
      FieldName = 'AccNome'
      Size = 255
    end
    object TbConfigsAccID: TWideStringField
      FieldName = 'AccID'
      Size = 60
    end
    object TbConfigsUserID: TWideStringField
      FieldName = 'UserID'
      Size = 60
    end
    object TbConfigsDefLstPrc: TWideStringField
      FieldName = 'DefLstPrc'
      Size = 255
    end
    object TbConfigsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsConfigs: TDataSource
    DataSet = TbConfigs
    Left = 136
    Top = 52
  end
  object QrPesqPrd: TABSQuery
    CurrentVersion = '7.20 '
    DatabaseName = 'MEMORY'
    InMemory = True
    ReadOnly = False
    Left = 60
    Top = 52
  end
  object TbEntCliCad: TABSTable
    CurrentVersion = '7.20 '
    DatabaseName = 'MEMORY'
    InMemory = True
    ReadOnly = False
    OnCalcFields = TbEntCliCadCalcFields
    TableName = 'entclicad'
    Exclusive = False
    Left = 212
    Top = 4
    object TbEntCliCadversao: TFloatField
      FieldName = 'versao'
    end
    object TbEntCliCadCliCodi: TWideStringField
      FieldName = 'CliCodi'
      Size = 60
    end
    object TbEntCliCadCliNome: TWideStringField
      FieldName = 'CliNome'
      Size = 255
    end
    object TbEntCliCadNomeUTF: TWideStringField
      FieldName = 'NomeUTF'
      Size = 255
    end
    object TbEntCliCadCidade: TWideStringField
      FieldName = 'Cidade'
      Size = 255
    end
    object TbEntCliCadUF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object TbEntCliCadPais: TWideStringField
      FieldName = 'Pais'
      Size = 255
    end
    object TbEntCliCadOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object TbEntCliCadTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object TbEntCliCadAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object TbEntCliCadNO_Tipo: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_Tipo'
      Size = 512
      Calculated = True
    end
  end
  object DsEntCliCad: TDataSource
    DataSet = TbEntCliCad
    Left = 212
    Top = 52
  end
end
