unit Principal;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Menus, Grids, DBGrids, Db, (*DBTables,*) TypInfo, StdCtrls, ToolWin,
  ComCtrls, ExtDlgs, jpeg, mySQLDbTables, Buttons, WinSkinStore, WinSkinData,
  Mask, DBCtrls, Tabs, DockTabSet, AdvToolBar, UnInternalConsts,
  AdvGlowButton, AdvMenus, dmkGeral, AdvToolBarStylers, IdHashMessageDigest,
  AdvShapeButton, AdvPreviewMenu, AdvOfficeHint, dmkDBGrid, dmkEdit,
  Vcl.Imaging.pngimage,
  UnMyObjects, dmkPageControl;

type
  TFmPrincipal = class(TForm)
    OpenPictureDialog1: TOpenPictureDialog;
    Timer1: TTimer;
    sd1: TSkinData;
    SkinStore1: TSkinStore;
    StatusBar: TStatusBar;
    TimerIdle: TTimer;
    AdvToolBarPager1: TAdvToolBarPager;
    AdvToolBarPager12: TAdvPage;
    AdvToolBarPager13: TAdvPage;
    AdvToolBar2: TAdvToolBar;
    AdvGlowButton1: TAdvGlowButton;
    AdvToolBar8: TAdvToolBar;
    AdvGlowButton6: TAdvGlowButton;
    AdvToolBar9: TAdvToolBar;
    AdvPMImagem: TAdvPopupMenu;
    MenuItem1: TMenuItem;
    AdvPMMenuCor: TAdvPopupMenu;
    Padro3: TMenuItem;
    Office20071: TMenuItem;
    Dermatek1: TMenuItem;
    Preto1: TMenuItem;
    Azul1: TMenuItem;
    Cinza1: TMenuItem;
    Verde1: TMenuItem;
    Prscia1: TMenuItem;
    WhidbeyStyle1: TMenuItem;
    WindowsXP1: TMenuItem;
    AdvToolBarOfficeStyler1: TAdvToolBarOfficeStyler;
    AdvShapeButton1: TAdvShapeButton;
    AdvQuickAccessToolBar1: TAdvQuickAccessToolBar;
    AdvPreviewMenu1: TAdvPreviewMenu;
    AdvToolBar10: TAdvToolBar;
    AdvGlowButton17: TAdvGlowButton;
    AdvToolBar11: TAdvToolBar;
    AdvGlowMenuButton3: TAdvGlowMenuButton;
    AdvGlowMenuButton4: TAdvGlowMenuButton;
    AdvOfficeHint1: TAdvOfficeHint;
    AdvGlowButton58: TAdvGlowButton;
    Memo3: TMemo;
    AGMBBackup: TAdvGlowButton;
    AGMBVerifiBD: TAdvGlowButton;
    AdvToolBar3: TAdvToolBar;
    AdvGlowButton3: TAdvGlowButton;
    AdvGlowButton7: TAdvGlowButton;
    Limpar1: TMenuItem;
    PMGeral: TPopupMenu;
    AdvToolBar1: TAdvToolBar;
    AGBLogins: TAdvGlowButton;
    AGBPerfis: TAdvGlowButton;
    AdvPage1: TAdvPage;
    AdvToolBar5: TAdvToolBar;
    AGBDistrCli: TAdvGlowButton;
    AdvGlowButton4: TAdvGlowButton;
    AdvToolBar6: TAdvToolBar;
    AGBSNAccCad: TAdvGlowButton;
    AGBDCArtCad: TAdvGlowButton;
    AdvToolBar7: TAdvToolBar;
    AGBWUser: TAdvGlowButton;
    AGBWPerfis: TAdvGlowButton;
    AGBWPerfJan: TAdvGlowButton;
    AdvGlowButton2: TAdvGlowButton;
    AdvGlowButton8: TAdvGlowButton;
    AdvPage3: TAdvPage;
    AdvToolBar4: TAdvToolBar;
    AGBSalesInn: TAdvGlowButton;
    AGBImportaCli: TAdvGlowButton;
    AGBSNEntInn: TAdvGlowButton;
    AGBAccCli: TAdvGlowButton;
    Timer2: TTimer;
    AdvToolBarButton4: TAdvToolBarButton;
    AdvToolBarButton3: TAdvToolBarButton;
    ATBBFavoritos: TAdvToolBarButton;
    ATBBATBAutoExp: TAdvToolBarButton;
    AdvToolBarButton5: TAdvToolBarButton;
    PageControl1: TdmkPageControl;
    ImgPrincipal: TImage;
    ATBNovo: TAdvToolBarButton;
    ATBConfigs: TAdvToolBarButton;
    APMPrcPrd: TAdvPopupMenu;
    Cadastro1: TMenuItem;
    Importao1: TMenuItem;
    AdvToolBarButton2: TAdvToolBarButton;
    APMPrdLst: TAdvPopupMenu;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    esteformulas1: TMenuItem;
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SBCadEntidadesClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure Verificanovaverso1Click(Sender: TObject);
    procedure TimerIdleTimer(Sender: TObject);
    procedure AdvGlowButton6Click(Sender: TObject);
    procedure AdvGlowButton17Click(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure Padro3Click(Sender: TObject);
    procedure AdvToolBarButton4Click(Sender: TObject);
    procedure AdvToolBarButton3Click(Sender: TObject);
    procedure AGMBBackupClick(Sender: TObject);
    procedure AGMBVerifiBDClick(Sender: TObject);
    procedure AdvGlowButton7Click(Sender: TObject);
    procedure Limpar1Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure AGBLoginsClick(Sender: TObject);
    procedure AGBPerfisClick(Sender: TObject);
    procedure AGBWUserClick(Sender: TObject);
    procedure AGBWPerfisClick(Sender: TObject);
    procedure AdvToolBarPager1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure Timer2Timer(Sender: TObject);
    procedure ATBBATBAutoExpClick(Sender: TObject);
    procedure PageControl1Enter(Sender: TObject);
    procedure PageControl1MouseEnter(Sender: TObject);
    procedure PageControl1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure AdvToolBarButton5Click(Sender: TObject);
    procedure ATBNovoClick(Sender: TObject);
    procedure ATBBFavoritosClick(Sender: TObject);
    procedure ATBConfigsClick(Sender: TObject);
    procedure Importao1Click(Sender: TObject);
    procedure Cadastro1Click(Sender: TObject);
    procedure AdvToolBarButton2Click(Sender: TObject);
    procedure ATBImportaListaPrecosClick(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure MenuItem3Click(Sender: TObject);
    procedure esteformulas1Click(Sender: TObject);
    procedure AdvGlowButton1Click(Sender: TObject);
  private
    { Private declarations }
    FATBAutoExp, FALiberar: Boolean;
    FBorda, FCursorPosX, FCursorPosY: Integer;
    //
    procedure MostraBackup3;
    procedure MostraVerifiDB;
    procedure MostraOpcoes;
    procedure MostraLogoff;
    procedure SkinMenu(Index: integer);
    //
  public
    { Public declarations }
    FEntInt, FTipoNovoEnti: Integer;
    FLDataIni, FLDataFim: TDateTime;
{
    FDuplicata, FCheque, FVerArqSCX, FCliInt: Integer;
    FImportPath: String;
}

    procedure AcoesIniciaisDoAplicativo;
    procedure AppIdle(Sender: TObject; var Done: Boolean);
    procedure ShowHint(Sender: TObject);

    procedure CadastroEntidades;

    procedure SelecionaImagemdefundo;
    procedure AcoesExtrasDeCadastroDeEntidades(Grade1: TStringGrid; Entidade: Integer);
    procedure AcoesIniciaisDeCadastroDeEntidades(Reference: TComponent;
              Codigo: Integer; Grade1: TStringGrid);
    function VerificaNovasVersoes(): Integer;
    procedure ReCaptionComponentesDeForm(Form: TForm(*; Memo: TMemo*));
    //
    function  MD5(Texto: String): String;
  end;

var
  FmPrincipal: TFmPrincipal;
const
  CO_DIRETORIO = 'C:\Dermatek';

implementation

uses ZCF2, Sorteio, (*TesteMD5,*) (*RecAnyOpn,*) TesteXML, (*PrdNomLst, PrdPrcXLS,*)
Module, EntCliCad(*, PrdLstXLS*);

{$R *.DFM}

var
  FAdvToolBarPager_Hei_Max: Integer;


procedure TFmPrincipal.FormActivate(Sender: TObject);
(***
const
  Versao = 3;
*)
begin
(***
  APP_LIBERADO := True;
  MyObjects.CorIniComponente();
  VAR_ATUALIZANDO := False;
  VAR_APPNAME := Application.Title;
  if Geral.VersaoTxt2006(CO_VERSAO) <>
    Geral.FileVerInfo(Application.ExeName, Versao) then
    ShowMessage('Vers�o difere do arquivo');
  if not FALiberar then Timer1.Enabled := True;
*)
  MyObjects.FormTDICria(TFmSorteio, PageControl1, AdvToolBarPager1, True);

end;

procedure TFmPrincipal.FormCreate(Sender: TObject);
(***
var
  MenuStyle: Integer;
  //Avisa,
  Imagem: String;
*)
begin
  VAR_USA_TAG_BITBTN := True;
  //
  Geral.DefineFormatacoes();
  FATBAutoExp := True;
  FAdvToolBarPager_Hei_Max := AdvToolBarPager1.Height; // 145
  PageControl1.Align := alClient;
(***
  Caption := Application.Title + ' :: Aguardando login...';
  //
  VAR_CAMINHOSKINPADRAO := 'C:\Dermatek\Skins\VCLSkin\Office 2007.skn';
  AdvToolBarPager1.ActivePageIndex := 0;
  //
  VAR_TYPE_LOG := ttlFiliLog;
  FEntInt := -1;
  VAR_USA_TAG_BITBTN := True;
  FTipoNovoEnti := 0;
  VAR_STLOGIN       := StatusBar.Panels[01];
  StatusBar.Panels[3].Text := Geral.VersaoTxt2006(CO_VERSAO);
  VAR_STTERMINAL    := StatusBar.Panels[05];
  VAR_STDATALICENCA := StatusBar.Panels[07];
  //VAR_STAVISOS      := StatusBar.Panels[09];
  VAR_SKINUSANDO    := StatusBar.Panels[09];
  VAR_STDATABASES   := StatusBar.Panels[11];
  VAR_TIPOSPRODM_TXT := '0,1,2,3,4,5,6,7,8,9,10,11,12,13';
  VAR_APP := ExtractFilePath(Application.ExeName);
  VAR_VENDEOQUE := 1;
  Imagem := Geral.ReadAppKey(
    'ImagemFundo', Application.Title, ktString, VAR_APP+'Fundo.jpg', HKEY_LOCAL_MACHINE);
  if FileExists(Imagem) then ImgPrincipal.Picture.LoadFromFile(Imagem);
  //
  MenuStyle := Geral.ReadAppKey('MenuStyle', Application.Title,
    ktInteger, 0, HKEY_LOCAL_MACHINE);
  SkinMenu(MenuStyle);
  //
  VAR_CAD_POPUP := PMGeral;
  //////////////////////////////////////////////////////////////////////////////
  FLDataIni := Date - Geral.ReadAppKey('Dias', Application.Title,
    ktInteger, 60, HKEY_LOCAL_MACHINE);
  FLDataFim := Date;
  //////////////////////////////////////////////////////////////////////////////
  //
*)
  Application.OnHint      := ShowHint;
  Application.OnException := MyObjects.MostraErro;
  Application.OnMessage   := MyObjects.FormMsg;
  Application.OnIdle      := AppIdle;
  //////////////////////////////////////////////////////////////////////////////

end;

procedure TFmPrincipal.FormDestroy(Sender: TObject);
begin
(***
  if VAR_WEB_CONECTADO = 100 then
    DmkWeb.DesconectarUsuarioWEB;
*)
end;

procedure TFmPrincipal.Importao1Click(Sender: TObject);
begin
  //Dmod.MostraFormPrdPrcXLS();
end;

procedure TFmPrincipal.SelecionaImagemdefundo;
begin
(***
  if OpenPictureDialog1.Execute then
  begin
    ImgPrincipal.Picture.LoadFromFile(OpenpictureDialog1.FileName);
    Geral.WriteAppKey('ImagemFundo', Application.Title, OpenPictureDialog1.FileName, ktString, HKEY_LOCAL_MACHINE);
  end;
*)
end;

procedure TFmPrincipal.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Application.Terminate;
end;

procedure TFmPrincipal.AdvGlowButton17Click(Sender: TObject);
begin
  MostraOpcoes;
end;

procedure TFmPrincipal.AdvGlowButton1Click(Sender: TObject);
begin
  Dmod.MostraFormEntiCliCad();
end;

procedure TFmPrincipal.AdvGlowButton6Click(Sender: TObject);
begin
  VerificaNovasVersoes();
end;

procedure TFmPrincipal.AdvGlowButton7Click(Sender: TObject);
begin
(***
  FmLinkRankSkin.Show;
*)
end;

procedure TFmPrincipal.AdvToolBarButton2Click(Sender: TObject);
begin
(*
  Application.CreateForm(TFmEntCliCad, FmEntCliCad);
  FmEntCliCad.ShowModal;
  FmEntCliCad.Destroy;
*)
  Dmod.MostraFormEntiCliCad();
end;

procedure TFmPrincipal.AdvToolBarButton3Click(Sender: TObject);
begin
{
  Application.CreateForm(TFmRecAnyOpn, FmRecAnyOpn);
  FmRecAnyOpn.ShowModal;
  FmRecAnyOpn.Destroy;
  //VerificaNovasVersoes();
}
end;

procedure TFmPrincipal.AdvToolBarButton4Click(Sender: TObject);
begin
  MostraLogoff;
end;

procedure TFmPrincipal.AdvToolBarButton5Click(Sender: TObject);
begin
  MostraBackup3;
end;

procedure TFmPrincipal.AdvToolBarPager1MouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
(*
  //if Dmod.QrOpcoesBugsATBAutoExp.Value = 1 then
  if FATBAutoExp then
  begin
    if AdvToolBarPager1.Expanded then
    begin
      if AdvToolBarPager1.Height < FAdvToolBarPager_Hei_Max then
      begin
        AdvToolBarPager1.Collaps;
        AdvToolBarPager1.Expand;
        AdvToolBarPager1.Height := FAdvToolBarPager_Hei_Max;
      end;
      if Y > FAdvToolBarPager_Hei_Max - 5 then
        AdvToolBarPager1.Collaps;
    end else
      AdvToolBarPager1.Expand;
  end;
*)
end;

procedure TFmPrincipal.AGBLoginsClick(Sender: TObject);
begin
(***
  if DBCheck.CriaFm(TFmUsuarios, FmUsuarios, afmoNegarComAviso) then
  begin
    FmUsuarios.ShowModal;
    FmUsuarios.Destroy;
  end;
*)
end;

procedure TFmPrincipal.AGBPerfisClick(Sender: TObject);
begin
(***
  VAR_FSENHA := 5;

  if DBCheck.CriaFm(TFmSenha, FmSenha, afmoNegarComAviso) then
  begin
    FmSenha.ShowModal;
    FmSenha.Destroy;
  end;
  if VAR_SENHARESULT = 2 then
  begin
    if DBCheck.CriaFm(TFmPerfis, FmPerfis, afmoNegarComAviso) then
    begin
      FmPerfis.ShowModal;
      FmPerfis.Destroy;
    end;
  end;
*)
end;

procedure TFmPrincipal.AGBWPerfisClick(Sender: TObject);
begin
(***
  if DBCheck.CriaFm(TFmWPerfis, FmWPerfis, afmoSoBoss) then
  begin
    FmWPerfis.ShowModal;
    FmWPerfis.Destroy;
  end;
*)
end;

procedure TFmPrincipal.AGBWUserClick(Sender: TObject);
begin
(***
  if DBCheck.CriaFm(TFmWUsers, FmWUsers, afmoNegarComAviso) then
  begin
    FmWUsers.ShowModal;
    FmWUsers.Destroy;
  end;
*)
end;

procedure TFmPrincipal.AGMBBackupClick(Sender: TObject);
begin
  MostraBackup3;
end;

procedure TFmPrincipal.AGMBVerifiBDClick(Sender: TObject);
begin
  MostraVerifiDB;
end;

procedure TFmPrincipal.SBCadEntidadesClick(Sender: TObject);
begin
  CadastroEntidades;
end;

procedure TFmPrincipal.Cadastro1Click(Sender: TObject);
begin
  //Dmod.MostraFormPrdPrcCab();
end;

procedure TFmPrincipal.CadastroEntidades;
begin
{
  DModG.CadastroDeEntidade(0,
    fmcadEntidade2, fmcadEntidade2, False);
}
end;

procedure TFmPrincipal.esteformulas1Click(Sender: TObject);
begin
{
  Application.CreateForm(TFmPrdNomLst, FmPrdNomLst);
  FmPrdNomLst.ShowModal;
  FmPrdNomLst.Destroy;
//  MostraVerifiDB;
(*  Application.CreateForm(TFmTesteMD5, FmTesteMD5);
  FmTesteMD5.ShowModal;
  FmTesteMD5.Destroy;
*)
}
end;

procedure TFmPrincipal.Timer1Timer(Sender: TObject);
begin
(***
  Timer1.Enabled := False;
  FALiberar := True;
  FmXipmert_Dmk.Show;
  Enabled := False;
  FmXipmert_Dmk.Refresh;
  try
    Application.CreateForm(TDmod, Dmod);
  except
    Geral.MensagemBox('Imposs�vel criar Modulo de dados', 'Erro', MB_OK+MB_ICONERROR);
    Application.Terminate;
    Exit;
  end;
  FmXipmert_Dmk.Info('M�dulo de dados criado');
  FmXipmert_Dmk.ReloadSkin;
  FmXipmert_Dmk.EdLogin.Text := '';
  FmXipmert_Dmk.EdLogin.PasswordChar := 'l';
  FmXipmert_Dmk.EdSenha.Text := '';
  FmXipmert_Dmk.EdSenha.Refresh;
  FmXipmert_Dmk.EdLogin.ReadOnly := False;
  FmXipmert_Dmk.EdSenha.ReadOnly := False;
  FmXipmert_Dmk.EdLogin.SetFocus;
  FmXipmert_Dmk.Refresh;
  //
  MyObjects.Informa2(FmXipmert_Dmk.LaAviso1, FmXipmert_Dmk.LaAviso2, False, '...');
*)
end;

procedure TFmPrincipal.Timer2Timer(Sender: TObject);
var
  ptCursor: TPoint;
  Fim: Integer;
begin
(*
  // Caso esteja finalizando o aplicativo...
  if ZZTerminate then
    Exit;
  // .. caso nao esteja, continua.
  try
    //if Dmod.QrOpcoesBugsATBAutoExp.Value = 1 then
    if FATBAutoExp then
    begin
      GetCursorPos(ptCursor);
      //
      if (ptCursor.X <> FCursorPosX)
      or (ptCursor.Y <> FCursorPosY) then
      begin
        Fim := AdvToolBarPager1.Top + FAdvToolBarPager_Hei_Max +
        FmPrincipal.Top + FmPrincipal.Height - FmPrincipal.ClientHeight + FBorda;
        FCursorPosX := ptCursor.X;
        FCursorPosY := ptCursor.Y;
        //
        if AdvToolBarPager1.Expanded then
        begin
          if FCursorPosY > Fim then
            AdvToolBarPager1.Collaps;
        end;
      end;
    end else
    begin
      Timer2.Enabled := False;
      MyObjects.MaximizaAdvToolBarPager(AdvToolBarPager1, deftfInverse);
    end;
  except
    Timer2.Enabled := False;
    // TODO > Implementar erro no idle
    raise Exception.Create('N�o foi poss�vel executar "FmPrincipal.Timer2()"');
  end;
*)
end;

procedure TFmPrincipal.MostraLogoff;
begin
(***
  FmPrincipal.Enabled := False;
  //
  FmXipmert_Dmk.Show;
  FmXipmert_Dmk.EdLogin.Text   := '';
  FmXipmert_Dmk.EdSenha.Text   := '';
  FmXipmert_Dmk.EdLogin.SetFocus;
*)
end;

procedure TFmPrincipal.MostraOpcoes;
begin
{
  if DBCheck.CriaFm(TFmOpcoes, FmOpcoes, afmoNegarComAviso) then
  begin
    FmOpcoes.ShowModal;
    FmOpcoes.Destroy;
  end;
}
end;

procedure TFmPrincipal.MostraVerifiDB;
begin
(***
  if DBCheck.CriaFm(TFmVerifiDB, FmVerifiDB, afmoNegarComAviso) then
  begin
    FmVerifiDB.ShowModal;
    FmVerifiDB.Destroy;
  end;
*)
end;

procedure TFmPrincipal.AcoesExtrasDeCadastroDeEntidades(Grade1: TStringGrid; Entidade: Integer);
begin
 // Nada
end;

procedure TFmPrincipal.AcoesIniciaisDeCadastroDeEntidades(Reference: TComponent;
Codigo: Integer; Grade1: TStringGrid);
begin
   // Compatibilidade
end;

procedure TFmPrincipal.Padro3Click(Sender: TObject);
begin
(***
  Geral.WriteAppKey('MenuStyle', Application.Title,
    TMenuItem(Sender).Tag, ktInteger, HKEY_LOCAL_MACHINE);
  SkinMenu(TMenuItem(Sender).Tag);
*)
end;

procedure TFmPrincipal.PageControl1Enter(Sender: TObject);
begin
  //if Dmod.QrOpcoesBugsATBAutoExp.Value = 1 then
  if FATBAutoExp then
  begin
    AdvToolBarPager1.Collaps;
  end;
end;

procedure TFmPrincipal.PageControl1MouseEnter(Sender: TObject);
begin
  //if Dmod.QrOpcoesBugsATBAutoExp.Value = 1 then
  if FATBAutoExp then
  begin
    AdvToolBarPager1.Collaps;
  end;
end;

procedure TFmPrincipal.PageControl1MouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if (Button = mbRight) then
  begin
    if PageControl1.ActivePage.ComponentCount = 0 then
    begin
      if Geral.MB_Pergunta('Deseja excluir a aba "' +
      PageControl1.ActivePage.Caption + '"?') = ID_YES then
        PageControl1.ActivePage.Free;
    end;
  end;
end;

procedure TFmPrincipal.ReCaptionComponentesDeForm(Form: TForm);
begin
  //Apenas compatibilidade usado no Syndi2
end;

procedure TFmPrincipal.AppIdle(Sender: TObject; var Done: Boolean);
begin
end;

procedure TFmPrincipal.ATBBATBAutoExpClick(Sender: TObject);
begin
  MyObjects.MaximizaAdvToolBarPager(AdvToolBarPager1, deftfInverse);
end;

procedure TFmPrincipal.ATBBFavoritosClick(Sender: TObject);
begin
  Application.CreateForm(TFmTesteXML, FmTesteXML);
  FmTesteXML.ShowModal;
  FmTesteXML.Destroy;
(*
C:\_Compilers\Delphi_XE2\XML
TesteXML

C:\_Compilers\Delphi_XE2\NFe\Teste
LoadXML.pas
*)
end;

procedure TFmPrincipal.ATBConfigsClick(Sender: TObject);
begin
  Dmod.MostraFormConfig();
end;

procedure TFmPrincipal.ATBImportaListaPrecosClick(Sender: TObject);
(*
var
  CliCodi, CliNome: String;
*)
begin
(*
  if Dmod.SelecionaEntCliCad(CliCodi, CliNome) then
  begin
    Dmod.MostraFormCliPrcCab(CliCodi, CliNome);
  end;
*)
end;

procedure TFmPrincipal.ATBNovoClick(Sender: TObject);
begin
  MyObjects.FormTDICria(TFmSorteio, PageControl1, AdvToolBarPager1, True);
end;

procedure TFmPrincipal.ShowHint(Sender: TObject);
begin
  if Length(Application.Hint) > 0 then
  begin
    StatusBar.SimplePanel := True;
    StatusBar.SimpleText := Application.Hint;
  end
  else StatusBar.SimplePanel := False;
end;

function TFmPrincipal.MD5(Texto: String): String;
var
  IDmd5 : TIdHashMessageDigest5;
begin
  IDmd5 := TIdHashMessageDigest5.Create;
  try
    Result := IDmd5.HashStringAsHex(texto);
  finally
    IDmd5.Free;
  end;
end;

procedure TFmPrincipal.MenuItem1Click(Sender: TObject);
begin
  SelecionaImagemdefundo;
end;

procedure TFmPrincipal.MenuItem2Click(Sender: TObject);
const
  TxtProd = '';
  TxtEmb = '';
var
  ProdCod: Integer;
  ProdNom, Embalagem, Diluicao, Temperatura: String;
begin
  Dmod.GerenciaProduto(TxtProd, TxtEmb, ProdCod, ProdNom, Embalagem, Diluicao,
  Temperatura);
end;

procedure TFmPrincipal.MenuItem3Click(Sender: TObject);
begin
{
  Application.CreateForm(TFmPrdLstXLS, FmPrdLstXLS);
  FmPrdLstXLS.ShowModal;
  FmPrdLstXLS.Destroy;
}
end;

procedure TFmPrincipal.MostraBackup3;
begin
{
  DModG.MostraBackup3();
}
end;

procedure TFmPrincipal.Limpar1Click(Sender: TObject);
begin
(***
  ImgPrincipal.Picture := nil;
  Geral.WriteAppKey('ImagemFundo', Application.Title, '', ktString, HKEY_LOCAL_MACHINE);
*)
end;

procedure TFmPrincipal.SkinMenu(Index: integer);
{var
  Indice : Integer;}
begin
  {if Index >= 0 then
    Indice := Index
  else
    Indice := Geral.ReadAppKey('MenuStyle', Application.Title,
      ktInteger, 1, HKEY_LOCAL_MACHINE);}
  case Index of
    0: AdvToolBarOfficeStyler1.Style := bsOffice2003Blue;
    1: AdvToolBarOfficeStyler1.Style := bsOffice2003Classic;
    2: AdvToolBarOfficeStyler1.Style := bsOffice2003Olive;
    3: AdvToolBarOfficeStyler1.Style := bsOffice2003Silver;
    4: AdvToolBarOfficeStyler1.Style := bsOffice2007Luna;
    5: AdvToolBarOfficeStyler1.Style := bsOffice2007Obsidian;
    6: AdvToolBarOfficeStyler1.Style := bsOffice2007Silver;
    7: AdvToolBarOfficeStyler1.Style := bsOfficeXP;
    8: AdvToolBarOfficeStyler1.Style := bsWhidbeyStyle;
    9: AdvToolBarOfficeStyler1.Style := bsWindowsXP;
  end;
end;

procedure TFmPrincipal.AcoesIniciaisDoAplicativo;
begin
  // N�o pode! N�o tem permiss�o na web!
  //DModG.MyPID_DB_Cria();
end;

function TFmPrincipal.VerificaNovasVersoes(): Integer;
begin
{
  Result := DmkWeb.VerificaAtualizacaoVersao2(TFmNovaVersao, FmNovaVersao, True, True,
    'Xipmert', 'Xipmert', CO_VERSAO, CO_DMKID_APP, Memo3, dtExec);
}
end;

procedure TFmPrincipal.Verificanovaverso1Click(Sender: TObject);
begin
  VerificaNovasVersoes();
end;

procedure TFmPrincipal.TimerIdleTimer(Sender: TObject);
(***
var
  Dia: Integer;
*)
begin
(***
  TimerIdle.Enabled := False;
  Dia := Geral.ReadAppKey('VeriNetVersao', Application.Title, ktInteger, 0,
    HKEY_LOCAL_MACHINE);
  if (Dia > 2) and (Dia < Int(Date)) then
  begin
    if VerificaNovasVersoes < 1 then Application.Terminate;
  end else Application.Terminate;
*)
end;

end.
