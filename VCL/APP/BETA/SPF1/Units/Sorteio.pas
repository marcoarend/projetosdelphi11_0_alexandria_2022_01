unit Sorteio;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.Buttons, Vcl.ExtCtrls, Vcl.ComCtrls, Vcl.StdCtrls, dmkEdit, Vcl.Grids,
  dmkGeral, UnDmkEnums, UnDmkProcFunc, ABSMain, Data.DB, Vcl.DBGrids,
  dmkDBGridZTO, UnApp_Consts, AppListas,
  // XML
  XMLDoc, xmldom, XMLIntf, msxmldom, MSXML2_TLB,
  dmkRadioGroup;

type
  TFmSorteio = class(TForm)
    PageControl1: TPageControl;
    TSReceita: TTabSheet;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    QrPilotos: TABSQuery;
    QrPilotosversao: TFloatField;
    QrPilotosCliCodi: TWideStringField;
    QrPilotosCliNome: TWideStringField;
    QrPilotosNomeUTF: TWideStringField;
    QrPilotosCidade: TWideStringField;
    QrPilotosUF: TWideStringField;
    QrPilotosPais: TWideStringField;
    QrPilotosAtivo: TSmallintField;
    QrPilotosOrdem: TIntegerField;
    QrPilotosTipo: TIntegerField;
    QrPilotosNO_Tipo: TWideStringField;
    DsPilotos: TDataSource;
    dmkDBGridZTO1: TdmkDBGridZTO;
    QrSocios: TABSQuery;
    QrSociosversao: TFloatField;
    QrSociosCliCodi: TWideStringField;
    QrSociosCliNome: TWideStringField;
    QrSociosNomeUTF: TWideStringField;
    QrSociosCidade: TWideStringField;
    QrSociosUF: TWideStringField;
    QrSociosPais: TWideStringField;
    QrSociosAtivo: TSmallintField;
    QrSociosOrdem: TIntegerField;
    QrSociosTipo: TIntegerField;
    QrSociosNO_Tipo: TWideStringField;
    DsSocios: TDataSource;
    dmkDBGridZTO2: TdmkDBGridZTO;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtRefresh: TBitBtn;
    BtSaida: TBitBtn;
    BitBtn1: TBitBtn;
    QrPilotosAleatorio: TIntegerField;
    QrSociosAleatorio: TIntegerField;
    SG1: TStringGrid;
    BtImprime: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure BtRefreshClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
  private
    { Private declarations }
    //
    procedure ReopenPilotos(Codigo: String);
    procedure ReopenSocios(Codigo: String);
    procedure ReopenTabelas();
  public
    { Public declarations }
  end;

var
  FmSorteio: TFmSorteio;

implementation

uses UnMyObjects, UMemModule, Principal, GModule, MeuDBUses;

{$R *.dfm}

procedure TFmSorteio.BitBtn1Click(Sender: TObject);
  procedure SortGrid(Grid: TStringGrid; Col: integer);
  var
    i, j: integer;
    prev: string;
  begin
    //in worst-case scenario we'll need RowCount-1 iterations to fully sort the grid
    for j:= 0 to Grid.RowCount- 1 do
      for i:= 2 to Grid.RowCount- 1 do
      begin
        if strtoint(Grid.Cells[Col, i])> strtoint(Grid.Cells[Col, i-1]) then
        begin // < if you want to reverse the sort order
          prev:= Grid.Rows[i- 1].CommaText;
          Grid.Rows[i- 1].CommaText:= Grid.Rows[i].CommaText;
          Grid.Rows[i].CommaText:= prev;
        end;
      end;
  end;
var
  Aleatorio, Lin, n: Integer;
  PilotosNom: array of String;
  PilotosRnd: array of String;
  Texto: String;
begin
  Screen.Cursor := crHourGlass;
  try
    ReopenTabelas();
    SetLength(PilotosNom, QrPilotos.RecordCount);
    SetLength(PilotosRnd, QrPilotos.RecordCount);
    n := 0;
    QrPilotos.First;
    while not QrPilotos.Eof do
    begin
      PilotosNom[QrPilotos.RecNo -1] := QrPilotosNomeUTF.Value;
      PilotosRnd[QrPilotos.RecNo -1] := Geral.FF0(QrPilotosAleatorio.Value);
      //
      QrPilotos.Next;
    end;
    //
    n := 0;
    Lin := 0;
    QrSocios.First;
    while not QrSocios.Eof do
    begin
      Lin := Lin + 1;
      SG1.RowCount := Lin + 1;
      SG1.Cells[0, Lin] := Geral.FF0(Lin);
      SG1.Cells[1, Lin] := PilotosNom[n];
      SG1.Cells[2, Lin] := QrSociosNomeUTF.Value;
(*
      Texto :=
        Geral.CompletaString(Pilotos[n], ' ', 60, taRightJustify, False)
        +
        QrSociosNomeUTF.Value;
*)
      Texto := PilotosRnd[n] + Geral.CompletaString(
        Geral.FF0(QrSociosAleatorio.Value), '0', 5, taLeftJustify, False);
      SG1.Cells[3, Lin] := Texto;
      //
      n := n + 1;
      if n = Length(PilotosNom) then
        n := 0;
      QrSocios.Next;
    end;
    //
  SortGrid(SG1, 3);
  PageControl1.ActivePageIndex := 2;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmSorteio.BtImprimeClick(Sender: TObject);
begin
  FmMeuDBUses.ImprimeStringGrid(SG1, 'Sorteio');
end;

procedure TFmSorteio.BtRefreshClick(Sender: TObject);
begin
  ReopenTabelas();
end;

procedure TFmSorteio.FormCreate(Sender: TObject);
begin
{
  FNomeExt := 'recrib';
  FNSubDir := 'RecRib';
  FMudouXML := False;
}
  PageControl1.ActivePageIndex := 0;
  ReopenTabelas();
  SG1.Cells[0,0] := 'Seq.';
  SG1.Cells[1,0] := 'Piloto';
  SG1.Cells[2,0] := 'S�cio';
  SG1.Cells[3,0] := 'Sorteio';
end;

procedure TFmSorteio.ReopenPilotos(Codigo: String);
begin
  UMemMod.AbreABSQuery1(QrPilotos, [
  'SELECT ecc.*, RAND(9999) Aleatorio ',
  'FROM entclicad ecc ',
  'WHERE (ecc.Tipo IN (' + CO_CJT_01 + '))',
  'AND Ativo=1 ',
  'ORDER BY Aleatorio, ecc.NomeUTF ',
  '']);
  if Codigo <> '' then
    QrPilotos.Locate('CliCodi', Codigo, []);
end;

procedure TFmSorteio.ReopenSocios(Codigo: String);
begin
  UMemMod.AbreABSQuery1(QrSocios, [
  'SELECT ecc.*, RAND(99999) Aleatorio ',
  'FROM entclicad ecc ',
  'WHERE (ecc.Tipo IN (' + CO_CJT_02 + '))',
  'AND Ativo=1 ',
  'ORDER BY Aleatorio, ecc.NomeUTF ',
  '']);
  if Codigo <> '' then
    QrSocios.Locate('CliCodi', Codigo, []);
end;

procedure TFmSorteio.ReopenTabelas;
begin
  ReopenPilotos('');
  ReopenSocios('');
end;

end.
