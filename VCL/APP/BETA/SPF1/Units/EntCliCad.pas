unit EntCliCad;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkDBGridZTO,
  ABSMain, dmkRadioGroup, dmkCheckGroup, dmkCheckBox, AppListas, UnDmkProcFunc;

type
  TFmEntCliCad = class(TForm)
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    Panel5: TPanel;
    Label1: TLabel;
    EdCliNome: TdmkEdit;
    DBGCli: TdmkDBGridZTO;
    QrEntCliCad: TABSQuery;
    DsEntCliCad: TDataSource;
    BtInsUpd: TBitBtn;
    Label4: TLabel;
    EdCliCodi: TdmkEdit;
    QrEntCliCadversao: TFloatField;
    QrEntCliCadCliCodi: TWideStringField;
    QrEntCliCadCliNome: TWideStringField;
    QrEntCliCadAtivo: TSmallintField;
    QrEntCliCadNomeUTF: TWideStringField;
    Label2: TLabel;
    EdCidade: TdmkEdit;
    Label3: TLabel;
    EdUF: TdmkEdit;
    Label5: TLabel;
    EdPais: TdmkEdit;
    QrEntCliCadCidade: TWideStringField;
    QrEntCliCadUF: TWideStringField;
    QrEntCliCadPais: TWideStringField;
    CGTipo: TdmkCheckGroup;
    CkAtivo: TdmkCheckBox;
    EdOrdem: TdmkEdit;
    Label6: TLabel;
    QrEntCliCadOrdem: TIntegerField;
    QrEntCliCadTipo: TIntegerField;
    QrEntCliCadNO_Tipo: TWideStringField;
    QrEntCliCadNO_Ativo: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtInsUpdClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure DBGCliDblClick(Sender: TObject);
    procedure EdCliNomeChange(Sender: TObject);
    procedure EdCliCodiChange(Sender: TObject);
    procedure QrEntCliCadCalcFields(DataSet: TDataSet);
    procedure DBGCliDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure CkAtivoClick(Sender: TObject);
    procedure CGTipoClick(Sender: TObject);
  private
    { Private declarations }
    procedure DefinedadosPorSelecao();
    procedure ReabrePesquisa();
  public
    { Public declarations }
    //
    FConfirmado: Boolean;
    FCliCodi, FCliNome: String;
    //
  end;

  var
  FmEntCliCad: TFmEntCliCad;

implementation

uses UnMyObjects, Module, UMemModule;

{$R *.DFM}

procedure TFmEntCliCad.BtInsUpdClick(Sender: TObject);
var
  CliCodi, CliNome, Cidade, UF, Pais, Nomes : String;
  Ordem, Tipo, Ativo: Integer;
begin
  Nomes   := '';
  CliCodi := EdCliCodi.Text;
  CliNome := EdCliNome.Text;
  Cidade  := EdCidade.Text;
  UF      := EdUF.Text;
  Pais    := EdPais.Text;
  Ordem   := EdOrdem.ValueVariant;
  Tipo    := CGTipo.Value;
  Ativo   := Geral.BoolToInt(CkATivo.Checked);
  //
  if MyObjects.FIC(Trim(CliCodi) = '', EdCliCodi, 'Defina o c�digo do cliente!') then
    Exit;
  if MyObjects.FIC(Trim(CliCodi) = '0', EdCliCodi, 'Defina o c�digo do cliente!') then
    Exit;
  if MyObjects.FIC(Trim(CliNome) = '', EdCliNome, 'Defina o nome do cliente!') then
    Exit;
  if Dmod.InserirRegistroEntCliCad(CliCodi, CliNome, Cidade, UF, Pais, Ordem,
  Tipo, Ativo) then
  begin
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, MSG_SAVE_XML);
    Dmod.SalvaXML_EntCliCod(False);
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    //
    FConfirmado := True;
    FCliCodi    := CliCodi;
    FCliNome    := CliNome;
    //
    Dmod.AbreEntCliCad(Dmod.TbEntCliCad);
    UMemMod.AbreABSTable1(Dmod.TbEntCliCad);
    //
    EdCliCodi.Text := '';
    EdCliNome.Text := '';
    EdCidade.Text  := '';
    EdUF.Text      := '';
    EdOrdem.ValueVariant := 0;
    //EdPais.Text    := '';
    //
    ReabrePesquisa();
    QrEntCliCad.Locate('CliCodi', CliCodi, []);
  end;
end;

procedure TFmEntCliCad.BtOKClick(Sender: TObject);
begin
  DefinedadosPorSelecao();
end;

procedure TFmEntCliCad.BtSaidaClick(Sender: TObject);
begin
  FConfirmado := True;
  FCliCodi    := EdCliCodi.Text;
  FCliNome    := EdCliNome.Text;
  //
  Close;
end;

procedure TFmEntCliCad.CGTipoClick(Sender: TObject);
begin
  ReabrePesquisa();
end;

procedure TFmEntCliCad.CkAtivoClick(Sender: TObject);
begin
  ReabrePesquisa();
end;

procedure TFmEntCliCad.DefinedadosPorSelecao();
begin
  FConfirmado := QrEntCliCad.RecordCount > 0;
  FCliCodi    := QrEntCliCadCliCodi.Value;
  FCliNome    := QrEntCliCadCliNome.Value;
  //
  Close;
end;

procedure TFmEntCliCad.DBGCliDblClick(Sender: TObject);
begin
  //DefinedadosPorSelecao();
  EdCliCodi.Text := QrEntCliCadCliCodi.Value;
  EdCliNome.Text := QrEntCliCadCliNome.Value;
  EdCidade.Text  := QrEntCliCadCidade.Value;
  EdUF.Text      := QrEntCliCadUF.Value;
  EdPais.Text    := QrEntCliCadPais.Value;
  EdOrdem.ValueVariant := QrEntCliCadOrdem.Value;
  CGTipo.Value    := QrEntCliCadTipo.Value;
  CkAtivo.Checked := Geral.IntToBool(QrEntCliCadAtivo.Value);
  //
end;

procedure TFmEntCliCad.DBGCliDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
const
  CorFundo = clWhite;
var
  CorTexto: TColor;
  Texto: String;
begin

  if Column.FieldName = 'NO_Ativo' then
  begin
    if QrEntCliCadAtivo.Value = 0 then
      CorTexto := clRed
    else
      CorTexto := clBlue;
    //
    MyObjects.DesenhaTextoEmDBGrid(TDBGrid(DBGCli), Rect, CorTexto, CorFundo,
      Column.Alignment, Column.Field.DisplayText);
  end;
end;

procedure TFmEntCliCad.EdCliCodiChange(Sender: TObject);
begin
  MyObjects.AlteraCaptionBtInsUpd(BtInsUpd,
    Dmod.LocalizaEntCliCad(EdCliCodi.Text));
end;

procedure TFmEntCliCad.EdCliNomeChange(Sender: TObject);
begin
  ReabrePesquisa();
end;

procedure TFmEntCliCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEntCliCad.FormCreate(Sender: TObject);
begin
  FConfirmado := False;
  FCliCodi    := '';
  FCliNome    := '';
  //
  MyObjects.ConfiguraCheckGroup(CGTipo, sListaCadEnt, 3, 0, False);
  ReabrePesquisa();
end;

procedure TFmEntCliCad.QrEntCliCadCalcFields(DataSet: TDataSet);
begin
  QrEntCliCadNO_Tipo.Value :=
    DmkPF.ConcatTxtChkGroup(QrEntCliCadTipo.Value, MaxListaCadEnt, sListaCadEnt);
end;

procedure TFmEntCliCad.ReabrePesquisa();
var
  Texto, Ativo, Tipo: String;
begin
  Texto := Geral.MyUTF(EdCliNome.Text);
  Ativo := Geral.FF0(Geral.BoolToInt(CkAtivo.Checked));
  if CGTipo.Value in ([1,2]) then
    Tipo := 'AND Tipo=' + Geral.FF0(CGTipo.Value)
  else
    Tipo := '';
  QrEntCliCad.Close;
  UMemMod.AbreABSQuery1(QrEntCliCad, [
  'SELECT CASE ecc."Ativo" ',
  'WHEN 0 THEN "N�o" ',
  'WHEN 1 THEN "Sim" ',
  'ELSE "???" ',
  'END AS No_Ativo, ecc.* ',
  'FROM entclicad ecc ',
  'WHERE ecc.NomeUTF LIKE "%' + Texto + '%"',
  'AND Ativo=' + Ativo,
  Tipo,
  'ORDER BY ecc.NomeUTF ',
  '']);
end;

end.
