object FmEntCliCad: TFmEntCliCad
  Left = 339
  Top = 185
  Caption = 'XXX-XXXXX-999 :: ????????????? ??????????? ????????????'
  ClientHeight = 553
  ClientWidth = 624
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 624
    Height = 439
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 624
      Height = 439
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 624
        Height = 439
        Align = alClient
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 620
          Height = 114
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Label1: TLabel
            Left = 92
            Top = 8
            Width = 31
            Height = 13
            Caption = 'Nome:'
          end
          object Label4: TLabel
            Left = 8
            Top = 8
            Width = 36
            Height = 13
            Caption = 'C'#243'digo:'
          end
          object Label2: TLabel
            Left = 8
            Top = 104
            Width = 36
            Height = 13
            Caption = 'Cidade:'
            Visible = False
          end
          object Label3: TLabel
            Left = 240
            Top = 104
            Width = 17
            Height = 13
            Caption = 'UF:'
            Visible = False
          end
          object Label5: TLabel
            Left = 272
            Top = 104
            Width = 25
            Height = 13
            Caption = 'Pa'#237's:'
            Visible = False
          end
          object Label6: TLabel
            Left = 408
            Top = 8
            Width = 34
            Height = 13
            Caption = 'Ordem:'
          end
          object EdCliNome: TdmkEdit
            Left = 92
            Top = 24
            Width = 313
            Height = 21
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdCliNomeChange
          end
          object EdCliCodi: TdmkEdit
            Left = 8
            Top = 24
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdCliCodiChange
          end
          object EdCidade: TdmkEdit
            Left = 8
            Top = 120
            Width = 229
            Height = 21
            TabOrder = 2
            Visible = False
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdUF: TdmkEdit
            Left = 240
            Top = 120
            Width = 28
            Height = 21
            CharCase = ecUpperCase
            MaxLength = 2
            TabOrder = 3
            Visible = False
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdPais: TdmkEdit
            Left = 272
            Top = 120
            Width = 133
            Height = 21
            TabOrder = 4
            Visible = False
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = 'Brasil'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 'Brasil'
            ValWarn = False
          end
          object CGTipo: TdmkCheckGroup
            Left = 8
            Top = 48
            Width = 397
            Height = 52
            Caption = ' Tipo de cadastro : '
            Columns = 2
            Items.Strings = (
              'S'#243'cio'
              'Piloto')
            TabOrder = 5
            OnClick = CGTipoClick
            UpdType = utYes
            Value = 0
            OldValor = 0
          end
          object CkAtivo: TdmkCheckBox
            Left = 420
            Top = 68
            Width = 97
            Height = 17
            Caption = 'Ativo'
            Checked = True
            State = cbChecked
            TabOrder = 6
            OnClick = CkAtivoClick
            UpdType = utYes
            ValCheck = #0
            ValUncheck = #0
            OldValor = #0
          end
          object EdOrdem: TdmkEdit
            Left = 408
            Top = 24
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 7
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdCliCodiChange
          end
        end
        object DBGCli: TdmkDBGridZTO
          Left = 2
          Top = 129
          Width = 620
          Height = 308
          Align = alClient
          DataSource = DsEntCliCad
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          SortArrow.Column = 'Pais'
          OnDrawColumnCell = DBGCliDrawColumnCell
          OnDblClick = DBGCliDblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'NO_Ativo'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              Title.Caption = 'OK'
              Width = 32
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CliCodi'
              Title.Caption = 'C'#243'digo'
              Width = 76
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CliNome'
              Title.Caption = 'Nome'
              Width = 281
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ordem'
              Width = 38
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Tipo'
              Width = 29
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_Tipo'
              Title.Caption = 'Tipo de cadastro'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Cidade'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'UF'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pais'
              Title.Caption = 'Pa'#237's'
              Width = 90
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 439
    Width = 624
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 620
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 483
    Width = 624
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 478
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = 'I&gnora'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 476
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Seleciona'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtInsUpd: TBitBtn
        Tag = 10
        Left = 136
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Insere'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtInsUpdClick
      end
    end
  end
  object QrEntCliCad: TABSQuery
    CurrentVersion = '7.20 '
    DatabaseName = 'MEMORY'
    InMemory = True
    ReadOnly = False
    OnCalcFields = QrEntCliCadCalcFields
    Left = 136
    Top = 252
    object QrEntCliCadversao: TFloatField
      FieldName = 'versao'
    end
    object QrEntCliCadCliCodi: TWideStringField
      FieldName = 'CliCodi'
      Size = 60
    end
    object QrEntCliCadCliNome: TWideStringField
      FieldName = 'CliNome'
      Size = 255
    end
    object QrEntCliCadNomeUTF: TWideStringField
      FieldName = 'NomeUTF'
      Size = 255
    end
    object QrEntCliCadCidade: TWideStringField
      FieldName = 'Cidade'
      Size = 255
    end
    object QrEntCliCadUF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrEntCliCadPais: TWideStringField
      FieldName = 'Pais'
      Size = 255
    end
    object QrEntCliCadAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrEntCliCadOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrEntCliCadTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrEntCliCadNO_Tipo: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_Tipo'
      Size = 512
      Calculated = True
    end
    object QrEntCliCadNO_Ativo: TWideStringField
      FieldName = 'NO_Ativo'
      Size = 3
    end
  end
  object DsEntCliCad: TDataSource
    DataSet = QrEntCliCad
    Left = 136
    Top = 300
  end
end
