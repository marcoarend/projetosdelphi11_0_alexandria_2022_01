unit UnAppCreate;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db, DbCtrls, UnApp_Consts,
  UnInternalConsts2, ComCtrls, Registry, ABSMain,(* DbTables,*) undmkProcFunc,
  dmkGeral;

const
  TMax_SWMS = 2;

type
  TNomeTabCria_TFC = ( ntcAInformar, (*ntcDatabases,*)
    ntcConfigs,
    ntcCliPrcCab, ntcCliPrcIts,
    ntcEntCliCad,
    ntcPrdLstCab, ntcPrdLstIts,
    ntcPrdPrcCab, ntcPrdPrcIts,
    ntcRecRibCab, ntcRecRibIts,

    //..
    ntcPrintGrid,
    //
    ntcFimLista);
  TLista_SWMS = set of ntcAInformar .. ntcFimLista;
  //
  TAppCreate = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    procedure ConfiguracoesIniciais(UsaCoresRel: integer; AppIDtxt: String);
    function RecriaMemTable(Tabela: TNomeTabCria_TFC; UsaNomeTab: String;
             Qry: TABSQuery; Executa: Boolean; UniqueTableName: Boolean;
             Repeticoes: Integer = 1; NomeTab: String = ''): String;
    (*procedure Cria_ntcDatabases(Qry: TABSQuery);*)
    //
    procedure Cria_ntcConfigs(Qry: TABSQuery);
    procedure Cria_ntcCliPrcCab(Qry: TABSQuery);
    procedure Cria_ntcCliPrcIts(Qry: TABSQuery);
    procedure Cria_ntcEntCliCad(Qry: TABSQuery);
    procedure Cria_ntcPrdLstCab(Qry: TABSQuery);
    procedure Cria_ntcPrdLstIts(Qry: TABSQuery);
    procedure Cria_ntcPrdPrcCab(Qry: TABSQuery);
    procedure Cria_ntcPrdPrcIts(Qry: TABSQuery);
    procedure Cria_ntcRecRibCab(Qry: TABSQuery);
    procedure Cria_ntcRecRibIts(Qry: TABSQuery);
    //...
    procedure Cria_ntcPrintGrid(Qry: TABSQuery; Repeticoes: Integer);
    //
  end;

var
  AppCreate: TAppCreate;

  TIndex_TFC: array[0..Integer(ntcFimLista)] of TNomeTabCria_TFC;

implementation

uses UnMyObjects(*, Module*);

procedure TAppCreate.ConfiguracoesIniciais(UsaCoresRel: integer;
  AppIDtxt: String);
begin
  dmkPF.ConfigIniApp(UsaCoresRel);
{
  if Uppercase(AppIDtxt) = 'TFC' then
  begin
    VAR_CLIENTE1 := 'Cliente';
    VAR_CLIENTE2 := '';
    VAR_CLIENTE3 := '';
    VAR_CLIENTE4 := '';
    VAR_FORNECE1 := 'Fornecedor';
    VAR_FORNECE2 := 'Funcion�rio';
    VAR_FORNECE3 := '';
    VAR_FORNECE4 := '';
    VAR_FORNECE5 := '';
    VAR_FORNECE6 := '';
    VAR_FORNECE7 := '';
    VAR_FORNECE8 := '';
    VAR_QUANTI1NOME := '?????????';
    VAR_CAMPOTRANSPORTADORA := '?????????';
  end else
    Geral.MensagemBox(
    'Database para configura��es de "CheckBox" n�o definidos!', 'Aviso!',
    MB_OK+MB_ICONWARNING)
}
end;

procedure TAppCreate.Cria_ntcCliPrcCab(Qry: TABSQuery);
begin
  Qry.SQL.Add('  Versao              float               ,');
  Qry.SQL.Add('  VerLst              float               ,');
  Qry.SQL.Add('  CliCodi             varchar(60)         ,');
  Qry.SQL.Add('  CliNome             varchar(255)        ,');
  Qry.SQL.Add('  Account             varchar(60)         ,');
  Qry.SQL.Add('  Dta_Ini             date                ,');
  Qry.SQL.Add('  Dta_Fim             date                ,');
  Qry.SQL.Add('  Cambio              float               ,');
  //
  Qry.SQL.Add('  Ativo                smallint           ,');
  Qry.SQL.Add('  PRIMARY KEY (CliCodi)                    ');
  Qry.SQL.Add(');                                         ');
end;

procedure TAppCreate.Cria_ntcCliPrcIts(Qry: TABSQuery);
begin
  Qry.SQL.Add('  ProdCod             integer             ,');
  Qry.SQL.Add('  ProdNom             varchar(255)        ,');
  Qry.SQL.Add('  ProdUTF             varchar(255)        ,');
  Qry.SQL.Add('  Embalagem           varchar(60)         ,');
  Qry.SQL.Add('  OrigemID            varchar(60)         ,');
  Qry.SQL.Add('  IPI                 float               ,');
  Qry.SQL.Add('  ICMS_RS             float               ,');
  Qry.SQL.Add('  ICMS_SP             float               ,');
  Qry.SQL.Add('  MoedaMI             varchar(10)         ,');
  Qry.SQL.Add('  MoedaID             varchar(10)         ,');
  Qry.SQL.Add('  PrecoMI_RS          float               ,');
  Qry.SQL.Add('  PrecoMI_SP          float               ,');
  Qry.SQL.Add('  PrecoID             float               ,');
  //
  Qry.SQL.Add('  Ativo               smallint            ,');
  Qry.SQL.Add('  PRIMARY KEY (ProdCod,ProdNom,OrigemID,Embalagem)');
  Qry.SQL.Add(');                                         ');
end;

procedure TAppCreate.Cria_ntcConfigs(Qry: TABSQuery);
begin
  Qry.SQL.Add('  Versao              float               ,');
  Qry.SQL.Add('  AccNome             varchar(255)        ,');
  Qry.SQL.Add('  AccID               varchar(60)         ,');
  Qry.SQL.Add('  UserID              varchar(60)         ,');
  Qry.SQL.Add('  DefLstPrc           varchar(255)        ,');
  //
  Qry.SQL.Add('  Ativo               smallint             ');
  // AccID nao pode ser null
  //Qry.SQL.Add('  PRIMARY KEY (AccID)                      ');
  Qry.SQL.Add(');                                         ');
end;

procedure TAppCreate.Cria_ntcEntCliCad(Qry: TABSQuery);
begin
  Qry.SQL.Add('  Versao              float               ,');
  Qry.SQL.Add('  CliCodi             varchar(60)         ,');
  Qry.SQL.Add('  CliNome             varchar(255)        ,');
  Qry.SQL.Add('  NomeUTF             varchar(255)        ,');
  Qry.SQL.Add('  Cidade              varchar(255)        ,');
  Qry.SQL.Add('  UF                  varchar(2)          ,');
  Qry.SQL.Add('  Pais                varchar(255)        ,');
  Qry.SQL.Add('  Ordem               integer             ,');
  Qry.SQL.Add('  Tipo                integer             ,');
  //
  Qry.SQL.Add('  Ativo                smallint           ,');
  Qry.SQL.Add('  PRIMARY KEY (CliCodi)                    ');
  Qry.SQL.Add(');                                         ');
end;

procedure TAppCreate.Cria_ntcPrdLstCab(Qry: TABSQuery);
begin
  Qry.SQL.Add('  Versao              float               ,');
  Qry.SQL.Add('  LstCodi             varchar(60)         ,');
  Qry.SQL.Add('  LstNome             varchar(255)        ,');
  Qry.SQL.Add('  Account             varchar(60)         ,');
  Qry.SQL.Add('  Dta_Ini             date                ,');
  Qry.SQL.Add('  Dta_Fim             date                ,');
  //
  Qry.SQL.Add('  Ativo                smallint           ,');
  Qry.SQL.Add('  PRIMARY KEY (LstCodi)                    ');
  Qry.SQL.Add(');                                         ');
end;

procedure TAppCreate.Cria_ntcPrdLstIts(Qry: TABSQuery);
begin
  Qry.SQL.Add('  IDSeqTb             integer             ,');
  Qry.SQL.Add('  ProdCod             integer             ,');
  Qry.SQL.Add('  ProdNom             varchar(255)        ,');
  Qry.SQL.Add('  ProdUTF             varchar(255)        ,');
  Qry.SQL.Add('  ProdNPS             varchar(255)        ,');
  Qry.SQL.Add('  Origem              varchar(60)         ,');
  Qry.SQL.Add('  Embalagem           varchar(60)         ,');
  Qry.SQL.Add('  RecipPosTp          integer             ,');
  Qry.SQL.Add('  MyID                varchar(60)         ,');
  //
  Qry.SQL.Add('  DiluProdu           integer             ,');
  //Qry.SQL.Add('  Temperatu           integer             ,');
  //
  Qry.SQL.Add('  Ativo               smallint            ,');
  Qry.SQL.Add('  PRIMARY KEY (ProdCod,Origem,Embalagem,MyID)');
  Qry.SQL.Add(');                                         ');
end;

procedure TAppCreate.Cria_ntcPrdPrcCab(Qry: TABSQuery);
begin
  Qry.SQL.Add('  Versao              float               ,');
  Qry.SQL.Add('  CliCodi             varchar(60)         ,');
  Qry.SQL.Add('  CliNome             varchar(255)        ,');
  Qry.SQL.Add('  Account             varchar(60)         ,');
  Qry.SQL.Add('  Dta_Ini             date                ,');
  Qry.SQL.Add('  Dta_Fim             date                ,');
  Qry.SQL.Add('  Cambio              float               ,');
  //
  Qry.SQL.Add('  Ativo                smallint           ,');
  Qry.SQL.Add('  PRIMARY KEY (CliCodi)                    ');
  Qry.SQL.Add(');                                         ');
end;

procedure TAppCreate.Cria_ntcPrdPrcIts(Qry: TABSQuery);
begin
  Qry.SQL.Add('  Seq                 integer             ,');
  Qry.SQL.Add('  Linha               varchar(255)        ,');
  Qry.SQL.Add('  ProdUTF             varchar(255)        ,');
  Qry.SQL.Add('  Produto             varchar(255)        ,');
  Qry.SQL.Add('  Origem              varchar(60)         ,');
  Qry.SQL.Add('  Embalagem           varchar(60)         ,');
  Qry.SQL.Add('  RecipPosTp          integer             ,');
  Qry.SQL.Add('  MyID                varchar(60)         ,');
  Qry.SQL.Add('  IPI_Txa             float               ,');
  Qry.SQL.Add('  ICMS_Dif            integer             ,');
  Qry.SQL.Add('  MoedaMI             varchar(10)         ,');
  Qry.SQL.Add('  MoedaID             varchar(10)         ,');
  Qry.SQL.Add('  MoedaEX             varchar(10)         ,');
  Qry.SQL.Add('  PrecoMI_PL          float               ,');
  Qry.SQL.Add('  PrecoMI_PR          float               ,');
  Qry.SQL.Add('  PrecoID             float               ,');
  Qry.SQL.Add('  PrecoEX             float               ,');
  Qry.SQL.Add('  VariaMI             float               ,');
  //
  Qry.SQL.Add('  Ativo               smallint            ,');
  Qry.SQL.Add('  PRIMARY KEY (Produto,Origem,Embalagem,MyID)');
  Qry.SQL.Add(');                                         ');
end;

procedure TAppCreate.Cria_ntcRecRibCab(Qry: TABSQuery);
begin
(*
  Qry.SQL.Add('  Versao              float               ,');
  Qry.SQL.Add('  CliCodi             varchar(60)         ,');
  Qry.SQL.Add('  CliNome             varchar(255)        ,');
  Qry.SQL.Add('  Account             varchar(60)         ,');
  Qry.SQL.Add('  Dta             date                ,');
  Qry.SQL.Add('  Dta_Fim             date                ,');
  /
    function Get_Setor: UnicodeString;
    function Get_CodTxt: UnicodeString;
    function Get_NomeArq: UnicodeString;
    function Get_VersaoRec: UnicodeString;
    function Get_Martelo: UnicodeString;
    function Get_Status: IXMLStatus;
    function Get_CliCodi: UnicodeString;
    function Get_CliNome: UnicodeString;
    function Get_Tecnico: UnicodeString;
    function Get_Account: UnicodeString;
    function Get_Origem: UnicodeString;
    function Get_Rendiment: UnicodeString;
    function Get_Artigo: UnicodeString;
    function Get_Cor: UnicodeString;
    function Get_EspReb: IXMLEspReb;
    function Get_EspFim: IXMLEspFim;
    function Get_ClasseIni: UnicodeString;
    function Get_ClasseFim: UnicodeString;
    function Get_RelatA: UnicodeString;
    function Get_PesoCouro: UnicodeString;
    function Get_Cambio: UnicodeString;
    function Get_Quantia: UnicodeString;
    function Get_UnidCouro: UnicodeString;
    function Get_Observ: UnicodeString;
    function Get_Data: UnicodeString;
  Qry.SQL.Add('  Ativo                smallint           ,');
  Qry.SQL.Add('  PRIMARY KEY (LstCodi)                    ');
  Qry.SQL.Add(');                                          ');
*)
end;

procedure TAppCreate.Cria_ntcRecRibIts(Qry: TABSQuery);
begin
  Qry.SQL.Add('  Seq                 integer             ,');
  Qry.SQL.Add('  Processo            varchar(255)        ,');
  Qry.SQL.Add('  NomeItem            varchar(255)        ,');
  Qry.SQL.Add('  Percentual          float               ,');
  Qry.SQL.Add('  Quantidade          float               ,');
  Qry.SQL.Add('  NomProduto          varchar(255)        ,');
  Qry.SQL.Add('  DiluiProdu          integer             ,');
  Qry.SQL.Add('  Temperatur          float               ,');
  Qry.SQL.Add('  RodarTempo          time                ,');
  Qry.SQL.Add('  PararTempo          time                ,');
  Qry.SQL.Add('  pHDesejado          float               ,');
  Qry.SQL.Add('  Observacao          varchar(255)        ,');
  Qry.SQL.Add('  RecipPosTp          smallint            ,');
  Qry.SQL.Add('  Instrucoes          varchar(255)        ,');
  Qry.SQL.Add('  PrecoProdu          float               ,');
  //
  Qry.SQL.Add('  Ativo               smallint            ,');
  Qry.SQL.Add('  PRIMARY KEY (Seq)                         ');
  Qry.SQL.Add(');                                          ');
end;

procedure TAppCreate.Cria_ntcPrintGrid(Qry: TABSQuery; Repeticoes: Integer);
var
  I: Integer;
begin
  for I := 0 to Repeticoes -1 do
    Qry.SQL.Add('  Col' + FormatFloat('000', I) + ' varchar(255),');
  //
  Qry.SQL.Add('  Ativo               smallint             ');
  // AccID nao pode ser null
  //Qry.SQL.Add('  PRIMARY KEY (AccID)                      ');
  Qry.SQL.Add(');                                         ');
  Qry.ExecSQL;
end;

(*
procedure TAppCreate.Cria_ntcCompetiInscr(Qry: TABSQuery);
begin
  Qry.SQL.Add('  Codigo               integer             ,');
  Qry.SQL.Add('  Controle             integer             ,');
  Qry.SQL.Add('  Data                 date                ,');
  Qry.SQL.Add('  Hora                 time                ,');
  Qry.SQL.Add('  Status               integer             ,');
  Qry.SQL.Add('  Mensagem             varchar(255)        ,');
  Qry.SQL.Add('  NomeEve              varchar(255)        ,');
  Qry.SQL.Add('  Recibo               varchar(20)         ,');
  Qry.SQL.Add('  IP                   varchar(255)        ,');
  Qry.SQL.Add('  Ativo                smallint            ,');
  Qry.SQL.Add('  PRIMARY KEY (Controle)                    ');
  Qry.SQL.Add(');                                          ');
end;

procedure TAppCreate.Cria_ntcCompetiPart(Qry: TABSQuery);
begin
  Qry.SQL.Add('  Codigo               integer             ,');
  Qry.SQL.Add('  Controle             integer             ,');
  Qry.SQL.Add('  Estilo               integer             ,');
  Qry.SQL.Add('  Metros               integer             ,');
  Qry.SQL.Add('  Nadador              integer             ,');
  Qry.SQL.Add('  Categoria            integer             ,');
  Qry.SQL.Add('  Tempo                time                ,');
  Qry.SQL.Add('  Ativo                smallint            ,');
  Qry.SQL.Add('  PRIMARY KEY (Controle)                   ,');
  Qry.SQL.Add('  UNIQUE INDEX UNIQUE1                      ');
  Qry.SQL.Add('  (Codigo,Estilo,Metros,Nadador)            ');
  Qry.SQL.Add(');                                          ');
end;

procedure TAppCreate.Cria_ntcNadadores(Qry: TABSQuery);
begin
  Qry.SQL.Add('  Codigo               integer             ,');
  Qry.SQL.Add('  CodTxt               varchar(60)         ,');
  Qry.SQL.Add('  Nome                 varchar(100)        ,');
  Qry.SQL.Add('  Nascim               date                ,');
  Qry.SQL.Add('  Sexo                 char(1)             ,');
  Qry.SQL.Add('  CPF                  varchar(11)         ,');
  Qry.SQL.Add('  Fone                 varchar(20)         ,');
  Qry.SQL.Add('  Ativo                smallint            ,');
  Qry.SQL.Add('  PRIMARY KEY (Codigo)                      ');
  Qry.SQL.Add(');                                          ');
end;

procedure TAppCreate.Cria_ntcEmpresa(Qry: TABSQuery);
begin
  Qry.SQL.Add('  Nome                 varchar(100)        ,');
  Qry.SQL.Add('  CNPJ                 varchar(14)         ,');
  Qry.SQL.Add('  xLgr                 varchar(60)         ,');
  Qry.SQL.Add('  xRua                 varchar(60)         ,');
  Qry.SQL.Add('  nro                  varchar(60)         ,');
  Qry.SQL.Add('  xCpl                 varchar(60)         ,');
  Qry.SQL.Add('  xBairro              varchar(60)         ,');
  Qry.SQL.Add('  cMun                 integer             ,');
  Qry.SQL.Add('  xMun                 varchar(60)         ,');
  Qry.SQL.Add('  UF                   char(2)             ,');
  Qry.SQL.Add('  CEP                  integer             ,');
  Qry.SQL.Add('  cPais                integer             ,');
  Qry.SQL.Add('  xPais                varchar(60)         ,');
  Qry.SQL.Add('  Fone                 varchar(20)         ,');
  Qry.SQL.Add('  Ativo                smallint            ,');
  Qry.SQL.Add('  PRIMARY KEY (CNPJ)                        ');
  Qry.SQL.Add(');                                          ');
end;
*)

function TAppCreate.RecriaMemTable(Tabela: TNomeTabCria_TFC; UsaNomeTab: String;
Qry: TABSQuery; Executa: Boolean; UniqueTableName: Boolean;
Repeticoes: Integer = 1; NomeTab: String = ''): String;
  function ObtemTabela(const TabIdx: Integer; var NomeIdx: String): Boolean;
  begin
    Result := True;
    case TabIdx of
      Integer(ntcAInformar):         NomeIdx := Lowercase(UsaNomeTab);
      (*Integer(ntcDatabases):         NomeIdx := Lowercase('databases');*)
      Integer(ntcConfigs):         NomeIdx := Lowercase(CO_Configs_Arquivo);
      Integer(ntcCliPrcCab):       NomeIdx := Lowercase('CliPrcCab');
      Integer(ntcCliPrcIts):       NomeIdx := Lowercase('CliPrcIts');
      Integer(ntcEntCliCad):       NomeIdx := Lowercase('EntCliCad');
      Integer(ntcPrdLstCab):       NomeIdx := Lowercase('PrdLstCab');
      Integer(ntcPrdLstIts):       NomeIdx := Lowercase('PrdLstIts');
      Integer(ntcPrdPrcCab):       NomeIdx := Lowercase('PrdPrcCab');
      Integer(ntcPrdPrcIts):       NomeIdx := Lowercase('PrdPrcIts');
      Integer(ntcRecRibCab):       NomeIdx := Lowercase('RecRibCab');
      Integer(ntcRecRibIts):       NomeIdx := Lowercase('RecRibIts');
      //
      Integer(ntcPrintGrid):       NomeIdx := Lowercase('_PrintGrid_');
      // ...
      else
      begin
        Result := False;
        NomeIdx := '';
      end;
    end;
  end;
var
  Nome, TabNo: String;
  P, I: Integer;
  Continua: Boolean;
  TabACriar: TNomeTabCria_TFC;
begin
  TabNo := '';
  if NomeTab = '' then
  begin
    ObtemTabela(Integer(Tabela), Nome);
  end else
    Nome := Lowercase(NomeTab);
  //
  if Nome = '' then
  begin
    Geral.MensagemBox(
    'Tabela em mem�ria sem nome definido! (RecriaMemTable)',
    'Erro', MB_OK+MB_ICONERROR);
    Result := '';
    Exit;
  end;
  if UniqueTableName then
  begin
    TabNo := '_' + FormatFloat('0', VAR_USUARIO) + '_' + Nome;
    //  caso for Master ou Admin (n�meros negativos)
    P := pos('-', TabNo);
    if P > 0 then
      TabNo[P] := '_';
  end else TabNo := Nome;
  Qry.Close;
  Qry.SQL.Clear;
  Qry.SQL.Add('DROP TABLE IF EXISTS ' + TabNo);
  Qry.ExecSQL;
  //
  Qry.Close;
  Qry.SQL.Clear;
  Qry.SQL.Add('CREATE TABLE ' + TabNo +' (');
  //
  TabACriar := Tabela;
  if TabACriar = ntcAInformar then
  begin
    // I n�o pode ser 0 por que o ntcAInformar � a pr�pria tabela
    for I := 1 to Integer(ntcFimLista) do
    begin
      if ObtemTabela(I, Nome) then
      begin
        if Nome = UsaNomeTab then
        begin
          TabACriar := TNomeTabCria_TFC(I);
          Break;
        end;  
      end;
    end;
  end;
  Continua := True;
  case TabACriar of
    //
    (*ntcDatabases                 : Cria_ntcDatabases(Qry);*)
    //
    ntcConfigs                 : Cria_ntcConfigs(Qry);
    ntcCliPrcCab               : Cria_ntcCliPrcCab(Qry);
    ntcCliPrcIts               : Cria_ntcCliPrcIts(Qry);
    ntcEntCliCad               : Cria_ntcEntCliCad(Qry);
    ntcPrdLstCab               : Cria_ntcPrdLstCab(Qry);
    ntcPrdLstIts               : Cria_ntcPrdLstIts(Qry);
    ntcPrdPrcCab               : Cria_ntcPrdPrcCab(Qry);
    ntcPrdPrcIts               : Cria_ntcPrdPrcIts(Qry);
    ntcRecRibCab               : Cria_ntcRecRibCab(Qry);
    ntcRecRibIts               : Cria_ntcRecRibIts(Qry);
    ntcPrintGrid               : Cria_ntcPrintGrid(Qry, Repeticoes);
    //
    //
    else
    begin
      Continua := False;
      Geral.MensagemBox('N�o foi poss�vel criar a tabela em mem�ria "' +
        Nome + '" por falta de implementa��o!', 'Erro', MB_OK+MB_ICONERROR);
    end;
  end;
  if Continua and Executa then
    Qry.ExecSQL;
  Result := TabNo;
end;

end.
