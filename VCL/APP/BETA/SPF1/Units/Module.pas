unit Module;

interface

uses
  Vcl.Forms, Winapi.Windows, System.SysUtils, System.Classes, Data.DB, ABSMain,
  Variants, Winapi.Messages, Vcl.Graphics, Vcl.Controls, Vcl.Dialogs,
  Vcl.ComCtrls, Vcl.StdCtrls, dmkEdit, Vcl.Buttons, Vcl.ExtCtrls, Vcl.Grids,
  UnDmkEnums, UnApp_Consts, dmkGeral, UnDmkProcFunc, UnMyObjects, UnApp_Vars,
  // XML
  XMLDoc, xmldom, XMLIntf, msxmldom, MSXML2_TLB, AppListas,
  Configs_v001, (*PrdLst_v001, PrdPrc_v001,*) EntCliCad_v001;

type
  TDmod = class(TDataModule)
    QrUpd: TABSQuery;
    TbConfigs: TABSTable;
    DsConfigs: TDataSource;
    TbConfigsAccNome: TWideStringField;
    TbConfigsAccID: TWideStringField;
    TbConfigsUserID: TWideStringField;
    TbConfigsDefLstPrc: TWideStringField;
    TbConfigsversao: TFloatField;
    TbConfigsAtivo: TSmallintField;
    QrPesqPrd: TABSQuery;
    TbEntCliCad: TABSTable;
    DsEntCliCad: TDataSource;
    TbEntCliCadversao: TFloatField;
    TbEntCliCadCliCodi: TWideStringField;
    TbEntCliCadCliNome: TWideStringField;
    TbEntCliCadNomeUTF: TWideStringField;
    TbEntCliCadCidade: TWideStringField;
    TbEntCliCadUF: TWideStringField;
    TbEntCliCadPais: TWideStringField;
    TbEntCliCadOrdem: TIntegerField;
    TbEntCliCadTipo: TIntegerField;
    TbEntCliCadAtivo: TSmallintField;
    TbEntCliCadNO_Tipo: TWideStringField;
    procedure DataModuleCreate(Sender: TObject);
    procedure TbEntCliCadCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    function  UTF8(Texto: String): String;
  public
    { Public declarations }
    function  AbreTabelaConfigs(TbCab: TABSTable; MostrarForm: Boolean): Boolean;
    function  AbreEntCliCad(TbCab: TABSTable): Boolean;
    //function  AbreListaDeProdutos(Arquivo: String; TbCab, TbIts: TABSTable): Boolean;
{
    function  AbreListaDePrecos(Arquivo: String; TbCab, TbIts: TABSTable): Boolean;
    function  AbreTabelaPrdLstCad(): Boolean;
}
    function  AbreTabelasLifeTime(): Boolean;

    function  InserirRegistroEntCliCad(CliCodi, CliNome, Cidade, UF, Pais:
              String; Ordem, Tipo, Ativo: Integer): Boolean;
{
    function  InsUpdRegistroProduto(var IDSeqTb: Integer; const ProdCod:
              Integer; const ProdNom, Embalagem, Origem: String; const RecipPosTp: Integer;
              const MyID: String; const DiluProdu, Ativo: Integer): Boolean;
    function  InserirRegistroPrecoDeProduto(Seq, Linha, Produto, Origem,
              Embalagem, MyID, RecipPosTp, IPI_Txa, ICMS_Dif, MoedaEX, PrecoEX,
              MoedaID, PrecoID, MoedaMI, PrecoMI_PL, PrecoMI_PR, VariaMI, Ativo:
              String): Boolean;
}

    function  LocalizaEntCliCad(CliCodi: String): Boolean;
{
    function  LocalizaPrdLstIts(ProdCod: Integer; ProdUTF, Origem, Embalagem, MyID: String): Boolean;
    function  LocalizaProdutoSohNome(Produto: String): Boolean;
    function  LocalizaProdutoListaPrecos(ProdUTF, Origem, Embalagem, MyID: String): Boolean;
}

    procedure MostraFormConfig();
    procedure MostraFormEntiCliCad();
{
    procedure MostraFormPrdPrcCab();
    procedure MostraFormPrdPrcXLS();
}

    function  SalvaXML_Configs(Avisa: Boolean): Boolean;
    function  SalvaXML_EntCliCod(Avisa: Boolean): Boolean;
{
    function  SalvaXML_PrdLst(Avisa: Boolean): Boolean;
    function  SalvaXML_PrdPrc(Arquivo: String; Avisa: Boolean): Boolean;
}

    function  SelecionaEntCliCad(var CliCodi, CliNome: String): Boolean;

    function  TraduzProduto(const Texto: String;
              var Produto, Diluicao, Temperatura: String): Boolean;
    function  GerenciaProduto(const TxtPrd, TxtEmb: String; var ProdCod:
              Integer; var ProdNom, Embalagem, Diluicao, Temperatura: String):
              Boolean;

  end;

const
  sListaPadrao    = 'ListaPadrao';
  versaoConfigs   = '0.01';
  versaoEntCliCad = '0.01';
  versaoPrdLst    = '0.01';
  versaoPrdPrc    = '0.01';
  versaoCliPrc    = '0.01';

var
  Dmod: TDmod;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}


{$R *.dfm}

uses
  GModule, UnAppCreate, UMemModule, Configs,
  //PrdLstCad, PrdPrcXLS, PrdPrcCab,
  EntCliCad(*, CliPrcCab*);

var
  Configs_XML_STR: IXMLTConfigs;
  Configs_XML_DOC: TXMLDocument;

  EntCliCad_XML_STR: IXMLTEntCliCad;
  EntCliCad_XML_DOC: TXMLDocument;
  EntCliCad: IXMLEntCliCadCab;

{
  PrdLst_XML_STR: IXMLTPrdLst;
  PrdLst_XML_DOC: TXMLDocument;
  //PrdLstItsList: IXMLPrdLstItsList;
  PrdLstIts: IXMLPrdLstIts;
  FIDSeqTb_PrdLstIts: Integer;

  PrdPrc_XML_STR: IXMLTPrdPrc;
  PrdPrc_XML_DOC: TXMLDocument;
  PrdPrcIts: IXMLPrdPrcIts;
}


{ TDataModule1 }

function TDmod.AbreEntCliCad(TbCab: TABSTable): Boolean;
var
  Caminho: String;
  Continua: Boolean;
  Versao, CliCodi, CliNome, Cidade, UF, Pais: String;
  Itens, I, Tipo, Ordem, Ativo: Integer;
var
  Node, Attr: IXMLNode;
  Found: Boolean;
var
  MostraForm: Boolean;

begin
  Result := False;
  MostraForm := False;
  //
  if TbEntCliCad.Exists then
    TbEntCliCad.DeleteTable;
  AppCreate.RecriaMemTable(ntcEntCliCad, 'EntCliCad', QrUpd, True, False);
  UMemMod.AbreABSTable1(TbEntCliCad);
  //
  Caminho := GMod.NomeCompletoArquivo(CO_EntCliCad_Arquivo, CO_EntCliCad_NSubDir, CO_EntCliCad_NomeExt);
  if not FileExists(Caminho) then
  begin
    if Geral.MB_Pergunta('O arquivo de cadastros de clientes n�o foi localizado!' +
    sLineBreak + 'Deseja cri�-lo vazio?') = ID_YES then
    begin
      //XML_Vazio := '';
      EntCliCad_XML_DOC := TXMLDocument.Create(nil);
      EntCliCad_XML_DOC.Active := False;
      EntCliCad_XML_DOC.FileName := '';
      EntCliCad_XML_STR := GetEntCliCad(EntCliCad_XML_DOC);
      EntCliCad_XML_DOC.Version := '1.0';
      EntCliCad_XML_DOC.Encoding := 'UTF-8';
      //
      Gmod.SalvaXML_De_Tabela(CO_EntCliCad_Arquivo, CO_EntCliCad_NSubDir,
        CO_EntCliCad_NomeExt, EntCliCad_XML_DOC.XML.Text(*XMLVazio*));
      EntCliCad_XML_DOC := nil;
      //
      MostraForm := True;
    end;
  end;
  if not FileExists(Caminho) then
    Exit;
  //
(* Criando o Documento XML ... *)
  EntCliCad_XML_DOC := TXMLDocument.Create(nil);
  EntCliCad_XML_DOC.FileName := Caminho;
  EntCliCad_XML_DOC.Active := True;
  EntCliCad_XML_STR := GetEntCliCad(EntCliCad_XML_DOC);
  EntCliCad_XML_DOC.Active := True;
  //
  Versao    := EntCliCad_XML_STR.Versao;
  for I := 0 to EntCliCad_XML_STR.Clientes.Count - 1 do
  begin
    EntCliCad := EntCliCad_XML_STR.Clientes.EntCliCadCab[I];
    //
    CliCodi   := EntCliCad.Item.CliCodi;
    CliNome   := EntCliCad.Item.CliNome;
    Cidade    := EntCliCad.Item.Cidade;
    UF        := EntCliCad.Item.UF;
    Pais      := EntCliCad.Item.Pais;
    //
    Tipo      := Geral.IMV(EntCliCad.Item.Tipo);
    Ordem     := Geral.IMV(EntCliCad.Item.Ordem);
    Ativo     := Geral.IMV(EntCliCad.Item.Ativo);

    TbCab.Insert;
    TbCab.FieldByName('versao').AsFloat     := Geral.DMV_Dot(Versao);
    TbCab.FieldByName('CliCodi').AsString   := CliCodi;
    TbCab.FieldByName('CliNome').AsString   := CliNome;
    TbCab.FieldByName('NomeUTF').AsString   := Geral.MyUTF(CliNome);
    TbCab.FieldByName('Cidade').AsString    := Cidade;
    TbCab.FieldByName('UF').AsString        := UF;
    TbCab.FieldByName('Pais').AsString      := Pais;
    //
    TbCab.FieldByName('Tipo').AsInteger     := Tipo;
    TbCab.FieldByName('Ordem').AsInteger    := Ordem;
    //
    TbCab.FieldByName('Ativo').AsInteger    := Ativo;
    GMod.PostTable(TbCab, CO_EntCliCad_Arquivo, 'AbreEntCliCad');
  end;
  //
  EntCliCad_XML_DOC := nil;
  //
end;

function TDmod.AbreTabelaConfigs(TbCab: TABSTable; MostrarForm: Boolean): Boolean;
const
  Ativo = 1;
var
  Caminho: String;
  Continua: Boolean;
  Versao, AccID, AccNome, DefLstPrc, UserID: String;
  Itens, I: Integer;
var
  Node, Attr: IXMLNode;
  Found: Boolean;
var
  MostraForm: Boolean;

begin
  Result := False;
  MostraForm := False;
  //
  Caminho := GMod.NomeCompletoArquivo(CO_Configs_Arquivo, CO_Configs_NSubDir, CO_Configs_NomeExt);
  if not FileExists(Caminho) then
  begin
    if Geral.MB_Pergunta('O arquivo de configura��o n�o foi localizado!' +
    sLineBreak + 'Deseja cri�-lo vazio?') = ID_YES then
    begin
      //XML_Vazio := '';
      Configs_XML_DOC := TXMLDocument.Create(nil);
      Configs_XML_DOC.Active := False;
      Configs_XML_DOC.FileName := '';
      Configs_XML_STR := GetConfigs(Configs_XML_DOC);
      Configs_XML_DOC.Version := '1.0';
      Configs_XML_DOC.Encoding := 'UTF-8';
      //
      Gmod.SalvaXML_De_Tabela(CO_Configs_Arquivo, CO_Configs_NSubDir,
        CO_Configs_NomeExt, Configs_XML_DOC.XML.Text(*XMLVazio*));
      Configs_XML_DOC := nil;
      //
      MostraForm := True;
    end;
  end;
  if not FileExists(Caminho) then
    Exit;
  //
(* Criando o Documento XML ... *)
  Configs_XML_DOC := TXMLDocument.Create(nil);
  Configs_XML_DOC.FileName := Caminho;
  Configs_XML_DOC.Active := True;
  Configs_XML_STR := GetConfigs(Configs_XML_DOC);
  Configs_XML_DOC.Active := True;
  //
  Versao    := Configs_XML_STR.Versao;
  AccID     := Configs_XML_STR.Configuracoes.Cabecalho.AccID;
  AccNome   := Configs_XML_STR.Configuracoes.Cabecalho.AccNome;
  DefLstPrc := Configs_XML_STR.Configuracoes.Cabecalho.DefLstPrc;
  UserID    := Configs_XML_STR.Configuracoes.Cabecalho.UserID;

  if TbCab.RecordCount = 0 then
    TbCab.Insert
  else
    TbCab.Edit;
  //
  TbCab.FieldByName('versao').AsFloat     := Geral.DMV_Dot(Versao);
  TbCab.FieldByName('AccID').AsString     := AccID;
  TbCab.FieldByName('AccNome').AsString   := AccNome;
  TbCab.FieldByName('DefLstPrc').AsString := DefLstPrc;
  TbCab.FieldByName('UserID').AsString    := UserID;
  //
  TbCab.FieldByName('Ativo').AsInteger    := Ativo;
  GMod.PostTable(TbCab, CO_Configs_Arquivo, 'AbreTabelaConfigs');
  //
  VAR_ACCOUNT_ID := AccID;
  Configs_XML_DOC := nil;
  //
  if MostrarForm and MostraForm then
    MostraFormConfig;
end;

{
function TDmod.AbreListaDePrecos(Arquivo: String; TbCab,
  TbIts: TABSTable): Boolean;
const
  Ativo = 1;
var
  Caminho: String;
  Continua: Boolean;
  Versao, Account, Dta_Ini, Dta_Fim, LstCodi, LstNome, Cambio: String;
  Itens, I: Integer;
var
  Node, Attr: IXMLNode;
  Found: Boolean;
  Produto, Origem, Embalagem, RecipPosTp, MyID, Seq, Linha, IPI_Txa, DiluProdu,
  ICMS_Dif, MoedaMI, MOedaID, MoedaEX, PrecoMI_PL, PrecoMI_PR, PrecoID,
  PrecoEX, VariaMI: String;
begin
  Result := False;
  //
  Caminho := GMod.NomeCompletoArquivo(Arquivo, CO_PrdPrc_NSubDir, CO_PrdPrc_NomeExt);
  if not FileExists(Caminho) then
  begin
    if Geral.MB_Pergunta('A lista de pre�os de produtos "' + Arquivo +
    '" n�o foi localizada!' + sLineBreak + 'Deseja cri�-la vazia?') = ID_YES then
    begin
      //XML_Vazio := '';
      PrdPrc_XML_DOC := TXMLDocument.Create(nil);
      PrdPrc_XML_DOC.Active := False;
      PrdPrc_XML_DOC.FileName := '';
      PrdPrc_XML_STR := GetPrdPrc(PrdPrc_XML_DOC);
      PrdPrc_XML_DOC.Version := '1.0';
      PrdPrc_XML_DOC.Encoding := 'UTF-8';
      //
      Gmod.SalvaXML_De_Tabela(Arquivo, CO_PrdPrc_NSubDir, CO_PrdPrc_NomeExt, PrdPrc_XML_DOC.XML.Text(*XMLVazio*));
      PrdPrc_XML_DOC := nil;
    end;
  end;
  if not FileExists(Caminho) then
    Exit;
  //
(* Criando o Documento XML ... *)
  PrdPrc_XML_DOC := TXMLDocument.Create(nil);
  PrdPrc_XML_DOC.FileName := Caminho;
  PrdPrc_XML_DOC.Active := True;
  PrdPrc_XML_STR := GetPrdPrc(PrdPrc_XML_DOC);
  PrdPrc_XML_DOC.Active := True;
  //
  Versao  := PrdPrc_XML_STR.Versao;
  LstCodi := PrdPrc_XML_STR.ListaDePrecos.Cabecalho.CliCodi;
  LstNome := PrdPrc_XML_STR.ListaDePrecos.Cabecalho.CliNome;
  Account := PrdPrc_XML_STR.ListaDePrecos.Cabecalho.Account;
  Dta_Ini := PrdPrc_XML_STR.ListaDePrecos.Cabecalho.Dta.Ini;
  Dta_Fim := PrdPrc_XML_STR.ListaDePrecos.Cabecalho.Dta.Fim;
  Cambio  := PrdPrc_XML_STR.ListaDePrecos.Cabecalho.Cambio;
  if Dta_Ini = '' then
    Dta_Ini := '1899-12-30';
  if Dta_Fim = '' then
    Dta_Fim := '1899-12-30';
  if LstCodi = '' then
  begin
    LstCodi := '0';
    LstNome := sListaPadrao;
  end;
  TbCab.Insert;
  TbCab.FieldByName('versao').AsFloat     := Geral.DMV_Dot(Versao);
  TbCab.FieldByName('CliCodi').AsString   := LstCodi;
  TbCab.FieldByName('CliNome').AsString   := LstNome;
  TbCab.FieldByName('Account').AsString   := Account;
  TbCab.FieldByName('Dta_Ini').AsDateTime := Geral.STD_001(Dta_Ini);
  TbCab.FieldByName('Dta_Fim').AsDateTime := Geral.STD_001(Dta_Fim);
  TbCab.FieldByName('Cambio').AsFloat     := Geral.DMV_Dot(Cambio);
  //
  TbCab.FieldByName('Ativo').AsInteger    := Ativo;
  GMod.PostTable(TbCab, Arquivo, 'AbreListaDePrecosDeProdutos');
  //
  for I := 0 to PrdPrc_XML_STR.ListaDePrecos.PrdPrcIts.Count - 1 do
  begin
    PrdPrcIts := PrdPrc_XML_STR.ListaDePrecos.PrdPrcIts.Items[I];
    //
    Seq        := PrdPrcIts.Item.Seq;
    Linha      := PrdPrcIts.Item.Linha;
    Produto    := PrdPrcIts.Item.Produto;
    Origem     := PrdPrcIts.Item.Origem;
    Embalagem  := PrdPrcIts.Item.Embalagem;
    MyID       := PrdPrcIts.Item.MyID;
    RecipPosTp := PrdPrcIts.Item.RecipPosTp;
    IPI_Txa    := PrdPrcIts.Item.IPI_Txa;
    ICMS_Dif   := PrdPrcIts.Item.ICMS_Dif;
    MoedaMI    := PrdPrcIts.Item.MoedaMI;
    MOedaID    := PrdPrcIts.Item.MOedaID;
    MoedaEX    := PrdPrcIts.Item.MoedaEX;
    PrecoMI_PL := PrdPrcIts.Item.PrecoMI_PL;
    PrecoMI_PR := PrdPrcIts.Item.PrecoMI_PR;
    PrecoID    := PrdPrcIts.Item.PrecoID;
    PrecoEX    := PrdPrcIts.Item.PrecoEX;
    VariaMI    := PrdPrcIts.Item.VariaMI;

    if (Produto = '') and (Origem = '') and (Embalagem = '') and (MyID = '') then
    begin
      // nada!
    end else
    begin
      TbIts.Insert;
      TbIts.FieldByName('Seq').AsInteger        := Geral.IMV(Seq);
      TbIts.FieldByName('Linha').AsString       := Linha;
      TbIts.FieldByName('ProdUTF').AsString     := Geral.MyUTF(Produto);
      TbIts.FieldByName('Produto').AsString     := Produto;
      TbIts.FieldByName('Origem').AsString      := Origem;
      TbIts.FieldByName('Embalagem').AsString   := Embalagem;
      TbIts.FieldByName('MyID').AsString        := MyID;
      TbIts.FieldByName('RecipPosTp').AsInteger := Geral.IMV(RecipPosTp);
      TbIts.FieldByName('IPI_Txa').AsFloat      := Geral.DMV_Dot(IPI_Txa);
      TbIts.FieldByName('ICMS_Dif').AsInteger   := Geral.IMV(ICMS_Dif);
      TbIts.FieldByName('MoedaMI').AsString     := MoedaMI;
      TbIts.FieldByName('MoedaID').AsString     := MoedaID;
      TbIts.FieldByName('MoedaEX').AsString     := MoedaEX;
      TbIts.FieldByName('PrecoMI_PL').AsFloat   := Geral.DMV_Dot(PrecoMI_PL);
      TbIts.FieldByName('PrecoMI_PR').AsFloat   := Geral.DMV_Dot(PrecoMI_PR);
      TbIts.FieldByName('PrecoID').AsFloat      := Geral.DMV_Dot(PrecoID);
      TbIts.FieldByName('PrecoEX').AsFloat      := Geral.DMV_Dot(PrecoEX);
      TbIts.FieldByName('VariaMI').AsFloat      := Geral.DMV_Dot(VariaMI);
      //
      TbIts.FieldByName('Ativo').AsInteger      := Ativo;
      GMod.PostTable(TbIts, Arquivo, 'AbreListaDePrecosDeProdutos');
    end;
  end;
  //
  PrdPrc_XML_DOC := nil;
end;
}

{
function TDmod.AbreTabelaPrdLstCad(): Boolean;
const
  Ativo = 1;
var
  Caminho: String;
  Continua: Boolean;
  Versao, Account, Dta_Ini, Dta_Fim, LstCodi, LstNome: String;
  Itens, I: Integer;
var
  Node, Attr: IXMLNode;
  Found: Boolean;
  ProdCod, ProdNom, Origem, Embalagem, RecipPosTp, MyID, DiluProdu: String;
var
  IDSeqTb: Integer;
begin
  Result := False;
  //
  TbPrdLstCab.Close;
  AppCreate.RecriaMemTable(ntcPrdLstCab, 'prdlstcab', QrUpd, True, False);
  UMemMod.AbreABSTable1(TbPrdLstCab);
  //
  TbPrdLstIts.Close;
  AppCreate.RecriaMemTable(ntcPrdLstIts, 'prdlstits', QrUpd, True, False);
  UMemMod.AbreABSTable1(TbPrdLstIts);
  //
  //
  //
  FIDSeqTb_PrdLstIts := 0;
  //
  Caminho := GMod.NomeCompletoArquivo(sListaPadrao, CO_PrdLst_NSubDir, CO_PrdLst_NomeExt);
  if not FileExists(Caminho) then
  begin
    if Geral.MB_Pergunta('A lista de produtos "' + sListaPadrao +
    '" n�o foi localizada!' + sLineBreak + 'Deseja cri�-la vazia?') = ID_YES then
    begin
      //XML_Vazio := '';
      PrdLst_XML_DOC := TXMLDocument.Create(nil);
      PrdLst_XML_DOC.Active := False;
      PrdLst_XML_DOC.FileName := '';
      PrdLst_XML_STR := GetPrdLst(PrdLst_XML_DOC);
      PrdLst_XML_DOC.Version := '1.0';
      PrdLst_XML_DOC.Encoding := 'UTF-8';
      //
      Gmod.SalvaXML_De_Tabela(sListaPadrao, CO_PrdLst_NSubDir, CO_PrdLst_NomeExt, PrdLst_XML_DOC.XML.Text(*XMLVazio*));
      PrdLst_XML_DOC := nil;
    end;
  end;
  if not FileExists(Caminho) then
    Exit;
  //
(* Criando o Documento XML ... *)
  PrdLst_XML_DOC := TXMLDocument.Create(nil);
  PrdLst_XML_DOC.FileName := Caminho;
  PrdLst_XML_DOC.Active := True;
  PrdLst_XML_STR := GetPrdLst(PrdLst_XML_DOC);
  PrdLst_XML_DOC.Active := True;
  //
  Versao  := PrdLst_XML_STR.Versao;
  Account := PrdLst_XML_STR.ListaDeProdutos.Cabecalho.Account;
  Dta_Ini := PrdLst_XML_STR.ListaDeProdutos.Cabecalho.Dta.Ini;
  Dta_Fim := PrdLst_XML_STR.ListaDeProdutos.Cabecalho.Dta.Fim;
  LstCodi := PrdLst_XML_STR.ListaDeProdutos.Cabecalho.LstCodi;
  LstNome := PrdLst_XML_STR.ListaDeProdutos.Cabecalho.LstNome;
  if Dta_Ini = '' then
    Dta_Ini := '1899-12-30';
  if Dta_Fim = '' then
    Dta_Fim := '1899-12-30';
  if LstCodi = '' then
  begin
    LstCodi := '0';
    LstNome := sListaPadrao;
  end;
  TbPrdLstCab.Insert;
  TbPrdLstCab.FieldByName('versao').AsFloat     := Geral.DMV_Dot(Versao);
  TbPrdLstCab.FieldByName('LstCodi').AsString   := LstCodi;
  TbPrdLstCab.FieldByName('LstNome').AsString   := LstNome;
  TbPrdLstCab.FieldByName('Account').AsString   := Account;
  TbPrdLstCab.FieldByName('Dta_Ini').AsDateTime := Geral.STD_001(Dta_Ini);
  TbPrdLstCab.FieldByName('Dta_Fim').AsDateTime := Geral.STD_001(Dta_Fim);
  //
  TbPrdLstCab.FieldByName('Ativo').AsInteger    := Ativo;
  GMod.PostTable(TbPrdLstCab, sListaPadrao, 'AbreTabelaPrdLstCad');
  //
  for I := 0 to PrdLst_XML_STR.ListaDeProdutos.PrdLstIts.Count - 1 do
  begin
    PrdLstIts := PrdLst_XML_STR.ListaDeProdutos.PrdLstIts.Items[I];
    //
    IDSeqTb    := Geral.IMV(PrdLstIts.Item.IDSeqTb);
    if IDSeqTb = 0 then
    begin
      FIDSeqTb_PrdLstIts := FIDSeqTb_PrdLstIts + 1;
      IDSeqTb := FIDSeqTb_PrdLstIts;
    end;
    ProdCod    := PrdLstIts.Item.ProdCod;
    ProdNom    := PrdLstIts.Item.ProdNom;
    Origem     := PrdLstIts.Item.Origem;
    Embalagem  := PrdLstIts.Item.Embalagem;
    RecipPosTp := PrdLstIts.Item.RecipPosTp;
    DiluProdu  := PrdLstIts.Item.DiluProdu;
    MyID       := PrdLstIts.Item.MyID;

    if IDSeqTb > FIDSeqTb_PrdLstIts then
      FIDSeqTb_PrdLstIts := IDSeqTb;
    //
    if (ProdNom = '') and (Origem = '') and (Embalagem = '') and (MyID = '') then
    begin
      // nada!
    end else
    begin
      TbPrdLstIts.Insert;
      TbPrdLstIts.FieldByName('IDSeqTb').AsInteger    := IDSeqTb;
      TbPrdLstIts.FieldByName('ProdCod').AsInteger    := Geral.IMV(ProdCod);
      TbPrdLstIts.FieldByName('ProdUTF').AsString     := Geral.MyUTF(ProdNom);
      TbPrdLstIts.FieldByName('ProdNPS').AsString     := Geral.MyNPS(ProdNom);
      TbPrdLstIts.FieldByName('ProdNom').AsString     := ProdNom;
      TbPrdLstIts.FieldByName('Origem').AsString      := Origem;
      TbPrdLstIts.FieldByName('Embalagem').AsString   := Embalagem;
      TbPrdLstIts.FieldByName('RecipPosTp').AsInteger := Geral.IMV(RecipPosTp);
      TbPrdLstIts.FieldByName('DiluProdu').AsInteger  := Geral.IMV(DiluProdu);
      TbPrdLstIts.FieldByName('MyID').AsString        := MyID;
      //
      TbPrdLstIts.FieldByName('Ativo').AsInteger      := Ativo;
      GMod.PostTable(TbPrdLstIts, sListaPadrao, 'AbreTabelaPrdLstCad');
    end;
  end;
  //
  PrdLst_XML_DOC := nil;
end;
}

function TDmod.AbreTabelasLifeTime: Boolean;
begin
  TbConfigs.Close;
  AppCreate.RecriaMemTable(ntcConfigs, CO_Configs_Arquivo, QrUpd, True, False);
  UMemMod.AbreABSTable1(TbConfigs);
  AbreTabelaConfigs(TbConfigs, True);
  //
  //
  //
  //
  //
  //
(*
  TbPrdPrcCab.Close;
  AppCreate.RecriaMemTable(ntcPrdPrcCab, 'prdprccab', QrUpd, True, False);
  UMemMod.AbreABSTable1(TbPrdPrcCab);
  //
  TbPrdPrcIts.Close;
  AppCreate.RecriaMemTable(ntcPrdPrcIts, 'prdprcits', QrUpd, True, False);
  UMemMod.AbreABSTable1(TbPrdPrcIts);
  //
  //
  //
  TbCliPrcCab.Close;
  AppCreate.RecriaMemTable(ntcCliPrcCab, 'cliprccab', QrUpd, True, False);
  UMemMod.AbreABSTable1(TbCliPrcCab);
  //
  TbCliPrcIts.Close;
  AppCreate.RecriaMemTable(ntcCliPrcIts, 'cliprcits', QrUpd, True, False);
  UMemMod.AbreABSTable1(TbCliPrcIts);
*)
  //
  //
  //
  AbreEntCliCad(TbEntCliCad);
(*
  AbreTabelaPrdLstCad();
  AbreListaDePrecos(sListaPadrao, TbPrdPrcCab, TbPrdPrcIts);
*)
end;

procedure TDmod.DataModuleCreate(Sender: TObject);
begin
  AbreTabelasLifeTime();
  AppCreate.ConfiguracoesIniciais(1, 'MEMORY');
end;

function TDmod.GerenciaProduto(const TxtPrd, TxtEmb: String; var ProdCod:
Integer; var ProdNom, Embalagem, Diluicao, Temperatura: String): Boolean;
begin
{
  TraduzProduto(TxtPrd, ProdNom, Diluicao, Temperatura);
  if not LocalizaProdutoSohNome(ProdNom) then
  begin
    Application.CreateForm(TFmPrdLstCad, FmPrdLstCad);
    //
    FmPrdLstCad.LaProdNom.Caption      := ProdNom;
    FmPrdLstCad.LaEmbalagem.Caption    := ProdNom;
    FmPrdLstCad.EdProdCod.ValueVariant := ProdCod;
    FmPrdLstCad.EdProdNom.Text         := ProdNom;
    FmPrdLstCad.EdOrigem.Text          := '';
    FmPrdLstCad.EdEmbalagem.Text       := Embalagem;
    FmPrdLstCad.EdDiluProdu.Text       := Diluicao;
    //
    FmPrdLstCad.ShowModal;
    //
    Result    := FmPrdLstCad.FConfirmado;
    ProdNom   := FmPrdLstCad.FProdNom;
    Embalagem := FmPrdLstCad.FEmbalagem;
    Diluicao  := IntToStr(FmPrdLstCad.FDiluicao);
    //
    FmPrdLstCad.Destroy;
  end;
}
end;

function TDmod.InserirRegistroEntCliCad(CliCodi, CliNome, Cidade, UF, Pais:
  String; Ordem, Tipo, Ativo: Integer): Boolean;
var
  NomeUTF: String;
  //SQLType: TSQLType;
begin
  Result := False;
  Screen.Cursor := crHourGlass;
  try
    if LocalizaEntCliCad(CliCodi) then
      //SQLType := stUpd
      TbEntCliCad.Edit
    else
      //SQLType := stIns;
      TbEntCliCad.Insert;
    //
    try
      NomeUTF := Geral.MyUTF(CliNome);
      //
      TbEntCliCadCliCodi.Value    := CliCodi;
      TbEntCliCadCliNome.Value    := CliNome;
      TbEntCliCadCidade.Value     := Cidade;
      TbEntCliCadUF.Value         := UF;
      TbEntCliCadPais.Value       := Pais;
      TbEntCliCadOrdem.Value      := Ordem;
      TbEntCliCadTipo.Value       := Tipo;
      TbEntCliCadAtivo.Value      := Ativo;

      //
      TbEntCliCad.Post;
      Result := True;
    except
      on E: Exception do
      begin
        Geral.MB_Erro('Erro ao cadastrar o cliente:' + sLineBreak +
        CliCodi + sLineBreak +
        CliNome + sLineBreak +
        sLineBreak + E.Message);
      end;
    end;
    //
  finally
    Screen.Cursor := crDefault;
  end;
end;

{
function TDmod.InserirRegistroPrecoDeProduto(Seq, Linha, Produto, Origem,
  Embalagem, MyID, RecipPosTp, IPI_Txa, ICMS_Dif, MoedaEX, PrecoEX, MoedaID,
  PrecoID, MoedaMI, PrecoMI_PL, PrecoMI_PR, VariaMI, Ativo: String): Boolean;
var
  ProdUTF: String;
begin
  Result := False;
  Screen.Cursor := crHourGlass;
  try
    ProdUTF := Geral.MyUTF(Produto);
    if not LocalizaProdutoListaPrecos(ProdUTF, Origem, Embalagem, MyID) then
    begin
      if (Produto <> '') or (Origem <> '') or (Embalagem <> '') or (MyID <> '') then
      begin
        try
          TbPrdPrcIts.Insert;
          //
          TbPrdPrcItsSeq.Value        := Geral.IMV(Seq);
          TbPrdPrcItsLinha.Value      := Linha;
          TbPrdPrcItsProdUTF.Value    := ProdUTF;
          TbPrdPrcItsProduto.Value    := Produto;
          TbPrdPrcItsOrigem.Value     := Origem;
          TbPrdPrcItsEmbalagem.Value  := Embalagem;
          TbPrdPrcItsMyID.Value       := MyID;
          TbPrdPrcItsRecipPosTp.Value := Geral.IMV(RecipPosTp);
          TbPrdPrcItsIPI_Txa.Value    := Geral.DMV(IPI_Txa);
          TbPrdPrcItsICMS_Dif.Value   := Geral.IMV(ICMS_Dif);
          TbPrdPrcItsMoedaEX.Value    := MoedaEX;
          TbPrdPrcItsPrecoEX.Value    := Geral.DMV(PrecoEX);
          TbPrdPrcItsMoedaID.Value    := MoedaID;
          TbPrdPrcItsPrecoID.Value    := Geral.DMV(PrecoID);
          TbPrdPrcItsMoedaMI.Value    := MoedaMI;
          TbPrdPrcItsPrecoMI_PL.Value := Geral.DMV(PrecoMI_PL);
          TbPrdPrcItsPrecoMI_PR.Value := Geral.DMV(PrecoMI_PR);
          TbPrdPrcItsVariaMI.Value    := Geral.DMV(VariaMI);
          TbPrdPrcItsAtivo.Value      := Geral.IMV(Ativo);
          //
          TbPrdPrcIts.Post;
        except
          on E: Exception do
          begin
            Geral.MB_Erro('Erro ao cadastrar o pre�o do produto:' + sLineBreak +
            Produto + sLineBreak +
            'Origem: ' + Origem + sLineBreak +
            'Embalagem: ' + Embalagem + sLineBreak +
            sLineBreak + E.Message);
          end;
        end;
      end;
      //
      Result := True;
    end;
    //
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TDmod.InsUpdRegistroProduto(var IDSeqTb: Integer; const ProdCod:
  Integer; const ProdNom, Embalagem, Origem: String; const RecipPosTp: Integer;
  const MyID: String; const DiluProdu, Ativo: Integer): Boolean;
var
  ProdUTF: String;
begin
  Result := False;
  Screen.Cursor := crHourGlass;
  try
    if IDSeqTb <> 0 then
    begin
      if not TbPrdLstIts.Locate('IDSeqTb', IDSeqTb, []) then
      begin
        Geral.MB_Erro('N�o foi poss�vel localizar o registro a ser alterado!' +
        sLineBreak + IntToStr(IDSeqTb));
        //
        Exit;
      end;
      // Parei aqui! 2014-05-31
      //TbPrdLstIts.Edit; <- nao esta funcionando
    end else
    begin
      FIDSeqTb_PrdLstIts := FIDSeqTb_PrdLstIts + 1;
      IDSeqTb := FIDSeqTb_PrdLstIts;
      TbPrdLstIts.Insert;
    end;
    ProdUTF := Geral.MyUTF(ProdNom);
    try
      TbPrdLstItsIDSeqTb.Value    := IDSeqTb;
      TbPrdLstItsProdCod.Value    := ProdCod;
      TbPrdLstItsProdNom.Value    := ProdNom;
      TbPrdLstItsProdUTF.Value    := ProdUTF;
      TbPrdLstItsOrigem.Value     := Origem;
      TbPrdLstItsEmbalagem.Value  := Embalagem;
      TbPrdLstItsRecipPosTp.Value := RecipPosTp;
      TbPrdLstItsMyID.Value       := MyID;
      TbPrdLstItsDiluProdu.Value  := DiluProdu;
      //
      TbPrdLstItsAtivo.Value      := Ativo;
      TbPrdLstIts.Post;
    except
      on E: Exception do
      begin
        Geral.MB_Erro('Erro ao cadastrar o produto:' + sLineBreak +
        ProdNom + sLineBreak +
        'Codigo: ' + Geral.FF0(ProdCod) + sLineBreak +
        'Origem: ' + Origem + sLineBreak +
        'Embalagem: ' + Embalagem + sLineBreak +
        sLineBreak + E.Message);
      end;
    end;
    Result := True;
  finally
    Screen.Cursor := crDefault;
  end;
end;
}

function TDmod.LocalizaEntCliCad(CliCodi: String): Boolean;
begin
  Result := TbEntCliCad.Locate('CliCodi', CliCodi, []);
end;

{
function TDmod.LocalizaPrdLstIts(ProdCod: Integer;
ProdUTF, Origem, Embalagem, MyID: String): Boolean;
begin
  Result := TbPrdLstIts.Locate('ProdCod;ProdUTF;Origem;Embalagem;MyID',
  VarArrayof([ProdCod, ProdUTF, Origem, Embalagem, MyID]), [loCaseInsensitive]);
end;

function TDmod.LocalizaProdutoListaPrecos(ProdUTF, Origem, Embalagem,
  MyID: String): Boolean;
begin
  Result := TbPrdPrcIts.Locate('ProdUTF;Origem;Embalagem;MyID',
  VarArrayof([ProdUTF, Origem, Embalagem, MyID]), [loCaseInsensitive]);
end;
}

{
function TDmod.LocalizaProdutoSohNome(Produto: String): Boolean;
var
  Nome: String;
begin
  Nome := Geral.MyUTF(Produto);
  UMemMod.AbreABSQuery1(QrPesqPrd, [
  'SELECT pli.*',
  'FROM prdlstits pli ',
  'WHERE pli.ProdUTF = "' + Nome + '"',
  '']);
  Result := QrPesqPrd.RecordCount > 0;
end;
}

(*
procedure TDmod.MostraFormCliPrcCab(CliCodi, CliNome: String);
begin
  Application.CreateForm(TFmCliPrcCab, FmCliPrcCab);
  FmCliPrcCab.EdCliCodi.ValueVariant   := CliCodi;
  FmCliPrcCab.EdCliNome.ValueVariant   := CliNome;
  FmCliPrcCab.EdAccount.ValueVariant   := VAR_ACCOUNT_ID;
  FmCliPrcCab.EdVersao.ValueVariant    := versaoCliPrc;
  FmCliPrcCab.EdVerLst.ValueVariant    := 0.0001;
  FmCliPrcCab.EdDtaIni.ValueVariant    := Int(Date);
  FmCliPrcCab.EdDtaFim.ValueVariant    := EncodeDate(2999, 12, 31);
  //
  FmCliPrcCab.ShowModal;
  FmCliPrcCab.Destroy;
end;
*)

procedure TDmod.MostraFormConfig;
begin
  Application.CreateForm(TFmConfigs, FmConfigs);
  FmConfigs.EdVersao.ValueVariant    := TbConfigsVersao.Value;
  FmConfigs.EdAccID.ValueVariant     := TbConfigsAccID.Value;
  FmConfigs.EdAccNome.ValueVariant   := TbConfigsAccNome.Value;
  FmConfigs.EdDefLstPrc.ValueVariant := TbConfigsDefLstPrc.Value;
  FmConfigs.EdUserID.ValueVariant    := TbConfigsUserID.Value;
  //
  FmConfigs.ShowModal;
  FmConfigs.Destroy;
end;

procedure TDmod.MostraFormEntiCliCad();
begin
  MyObjects.CriaForm_AcessoTotal(TFmEntCliCad, FmEntCliCad);
  FmEntCliCad.ShowModal;
  FmEntCliCad.Destroy;
end;

{
procedure TDmod.MostraFormPrdPrcCab;
begin
  Application.CreateForm(TFmPrdPrcCab, FmPrdPrcCab);
  //FmPrdPrcCab.EdAccount.Text := TbConfigsAccID.Value;
  //
  FmPrdPrcCab.ShowModal;
  FmPrdPrcCab.Destroy;
end;

procedure TDmod.MostraFormPrdPrcXLS();
begin
  Application.CreateForm(TFmPrdPrcXLS, FmPrdPrcXLS);
  FmPrdPrcXLS.EdAccount.Text := TbConfigsAccID.Value;
  //
  FmPrdPrcXLS.ShowModal;
  FmPrdPrcXLS.Destroy;
end;
}

function TDmod.SalvaXML_Configs(Avisa: Boolean): Boolean;
var
  I, Itens: Integer;
  AccNome, AccID, UserID, DefLstPrc: String;
  //
  RecipPosTp, DiluProdu, Ativo: Integer;
begin
  Itens := 0;
  Result := False;
  //

  AccNome   := TbConfigsAccNome.Value;
  AccID     := TbConfigsAccID.Value;
  UserID    := TbConfigsUserID.Value;
  DefLstPrc := TbConfigsDefLstPrc.Value;


  (* Criando o Documento XML e Gravando cabe�alho... *)
  Configs_XML_DOC := TXMLDocument.Create(nil);
  Configs_XML_DOC.Active := False;
  Configs_XML_DOC.FileName := '';
  Configs_XML_STR := GetConfigs(Configs_XML_DOC);
  Configs_XML_DOC.Version := '1.0';
  Configs_XML_DOC.Encoding := 'UTF-8';
  //
  Configs_XML_STR.Versao := versaoConfigs;
  //
  Configs_XML_STR.Configuracoes.Cabecalho.AccNome   := UTF8(AccNome);
  Configs_XML_STR.Configuracoes.Cabecalho.AccID     := UTF8(AccID);
  Configs_XML_STR.Configuracoes.Cabecalho.UserID    := UTF8(UserID);
  Configs_XML_STR.Configuracoes.Cabecalho.DefLstPrc := UTF8(DefLstPrc);
  //
  Result := True;
  //
  Gmod.SalvaXML_De_Tabela(CO_Configs_Arquivo,
    CO_Configs_NSubDir, CO_Configs_NomeExt, Configs_XML_DOC.XML.Text);
  //
  Configs_XML_DOC := nil;
  //
  if Avisa then
    Geral.MB_Info('Configura��es atualizadas com sucesso!');
end;

function TDmod.SalvaXML_EntCliCod(Avisa: Boolean): Boolean;
var
  I, Itens, Ordem, Tipo, Ativo: Integer;
  CliCodi, CliNome, Cidade, UF, Pais: String;
begin
  Itens := 0;
  Result := False;
  //

  (* Criando o Documento XML e Gravando cabe�alho... *)
  EntCliCad_XML_DOC := TXMLDocument.Create(nil);
  EntCliCad_XML_DOC.Active := False;
  EntCliCad_XML_DOC.FileName := '';
  EntCliCad_XML_STR := GetEntCliCad(EntCliCad_XML_DOC);
  EntCliCad_XML_DOC.Version := '1.0';
  EntCliCad_XML_DOC.Encoding := 'UTF-8';
  //
  EntCliCad_XML_STR.Versao := versaoEntCliCad;
  //
  TbEntCliCad.First;
  while not TbEntCliCad.Eof do
  begin
    CliCodi   := TbEntCliCadCliCodi.Value;
    CliNome   := TbEntCliCadCliNome.Value;
    Cidade    := TbEntCliCadCidade.Value;
    UF        := TbEntCliCadUF.Value;
    Pais      := TbEntCliCadPais.Value;
    Ordem     := TbEntCliCadOrdem.Value;
    Tipo      := TbEntCliCadTipo.Value;
    Ativo     := TbEntCliCadAtivo.Value;
    //
    EntCliCad :=  EntCliCad_XML_STR.Clientes.Add;
    //
    EntCliCad.Item.CliCodi     := UTF8(CliCodi);
    EntCliCad.Item.CliNome     := UTF8(CliNome);
    EntCliCad.Item.Cidade      := UTF8(Cidade);
    EntCliCad.Item.UF          := UTF8(UF);
    EntCliCad.Item.Pais        := UTF8(Pais);
    //
    EntCliCad.Item.Ordem       := UTF8(Geral.FF0(Ordem));
    EntCliCad.Item.Tipo        := UTF8(Geral.FF0(Tipo));
    EntCliCad.Item.Ativo       := UTF8(Geral.FF0(Ativo));
    //
    TbEntCliCad.Next;
  end;

  //
  Result := True;
  //
  Gmod.SalvaXML_De_Tabela(CO_EntCliCad_Arquivo,
    CO_EntCliCad_NSubDir, CO_EntCliCad_NomeExt, EntCliCad_XML_DOC.XML.Text);
  //
  EntCliCad_XML_DOC := nil;
  //
  if Avisa then
    Geral.MB_Info('Clientes atualizados com sucesso!');
end;

{
function TDmod.SalvaXML_PrdLst(Avisa: Boolean): Boolean;
var
  I, Itens, IDSeqTb, ProdCod: Integer;
  CliCod, CliNom: String;
  LstCodi, LstNome, Account, Dta_Ini, Dta_Fim,
  ProdNom, Origem, Embalagem, MyID: String;
  //
  RecipPosTp, DiluProdu, Ativo: Integer;
begin
  UMemMod.AbreABSTable1(TbPrdLstCab);
  UMemMod.AbreABSTable1(TbPrdLstIts);
  Itens := 0;
  Result := False;
  if LstCodi = '0' then
  begin
    if LstNome <> sListaPadrao then
    begin
      Geral.MB_Aviso('Salvamento cancelado!' + sLineBreak +
      'O c�digo "' + CliCod + '" s� pode serusado para "' + sListaPadrao + '"');
      Exit;
    end;
  end;
  //

  LstCodi := TbPrdLstCabLstCodi.Value;
  LstNome := TbPrdLstCabLstNome.Value;
  Account := UTF8(TbPrdLstCabAccount.Value);
  Dta_Ini := Geral.FDT(TbPrdLstCabDta_Ini.Value, 1);
  Dta_Fim := Geral.FDT(TbPrdLstCabDta_fim.Value, 1);

  (* Criando o Documento XML e Gravando cabe�alho... *)
  PrdLst_XML_DOC := TXMLDocument.Create(nil);
  PrdLst_XML_DOC.Active := False;
  PrdLst_XML_DOC.FileName := '';
  PrdLst_XML_STR := GetPrdLst(PrdLst_XML_DOC);
  PrdLst_XML_DOC.Version := '1.0';
  PrdLst_XML_DOC.Encoding := 'UTF-8';
  //
  PrdLst_XML_STR.Versao := versaoPrdLst;
  //
  PrdLst_XML_STR.ListaDeProdutos.Cabecalho.LstCodi      := UTF8(LstCodi);
  PrdLst_XML_STR.ListaDeProdutos.Cabecalho.LstNome      := UTF8(LstNome);
  PrdLst_XML_STR.ListaDeProdutos.Cabecalho.Account      := UTF8(Account);
  PrdLst_XML_STR.ListaDeProdutos.Cabecalho.Dta.Ini      := UTF8(Dta_Ini);
  PrdLst_XML_STR.ListaDeProdutos.Cabecalho.Dta.Fim      := UTF8(Dta_Fim);
  //
  Result := True;

  TbPrdLstIts.First;
  while not TbPrdLstIts.Eof do
  begin
    IDSeqTb    := TbPrdLstItsIDSeqTb.Value;
    ProdCod    := TbPrdLstItsProdCod.Value;
    ProdNom    := TbPrdLstItsProdNom.Value;
    Origem     := TbPrdLstItsOrigem.Value;
    Embalagem  := TbPrdLstItsEmbalagem.Value;
    MyID       := TbPrdLstItsMyID.Value;
    RecipPosTp := TbPrdLstItsRecipPosTp.Value;
    DiluProdu  := TbPrdLstItsDiluProdu.Value;
    Ativo      := TbPrdLstItsAtivo.Value;
    //
    PrdLstIts := PrdLst_XML_STR.ListaDeProdutos.PrdLstIts.Add;
    //
    PrdLstIts.Item.IDSeqTb     := UTF8(Geral.FF0(IDSeqTb));
    PrdLstIts.Item.ProdCod     := UTF8(Geral.FF0(ProdCod));
    PrdLstIts.Item.ProdNom     := UTF8(ProdNom);
    PrdLstIts.Item.Origem      := UTF8(Origem);
    PrdLstIts.Item.Embalagem   := UTF8(Embalagem);
    PrdLstIts.Item.RecipPosTp  := UTF8(Geral.FF0(RecipPosTp));  // TFL=1
    PrdLstIts.Item.DiluProdu   := UTF8(Geral.FF0(DiluProdu));
    PrdLstIts.Item.MyID        := UTF8(MyID);
    //
    TbPrdLstIts.Next;
  end;
  //
  Gmod.SalvaXML_De_Tabela(sListaPadrao,
    CO_PrdLst_NSubDir, CO_PrdLst_NomeExt, PrdLst_XML_DOC.XML.Text);
  //
  PrdLst_XML_DOC := nil;
  //
  if Avisa then
    Geral.MB_Info('Lista de produtos atualizadacom sucesso!');
end;

function TDmod.SalvaXML_PrdPrc(Arquivo: String; Avisa: Boolean): Boolean;
var
  I, Itens: Integer;
  CliCodi, CliNome, Seq, Linha, ProdUTF: String;
  LstCodi, LstNome, Account, Dta_Ini, Dta_Fim,
  Produto, Origem, Embalagem, MyID, Cambio,
  IPI_Txa, ICMS_Dif, MoedaMI, PrecoMI_PL, MoedaEX, PrecoEX, MoedaID, PrecoID,
  PrecoMI_PR, VariaMI: String;
  //
  RecipPosTp, Ativo: Integer;
begin
  UMemMod.AbreABSTable1(TbPrdPrcCab);
  UMemMod.AbreABSTable1(TbPrdPrcIts);
  Itens := 0;
  Result := False;
  CliCodi := TbPrdPrcCabCliCodi.Value;
  CliNome := TbPrdPrcCabCliNome.Value;
  Account := TbPrdPrcCabAccount.Value;
  Cambio  := Geral.FFT_Dot(TbPrdPrcCabCambio.Value, 4, siPositivo);
  Dta_Ini := Geral.FDT(TbPrdPrcCabDta_Ini.Value, 1);
  Dta_Fim := Geral.FDT(TbPrdPrcCabDta_fim.Value, 1);

  (* Criando o Documento XML e Gravando cabe�alho... *)
  PrdPrc_XML_DOC := TXMLDocument.Create(nil);
  PrdPrc_XML_DOC.Active := False;
  PrdPrc_XML_DOC.FileName := '';
  PrdPrc_XML_STR := GetPrdPrc(PrdPrc_XML_DOC);
  PrdPrc_XML_DOC.Version := '1.0';
  PrdPrc_XML_DOC.Encoding := 'UTF-8';
  //
  PrdPrc_XML_STR.Versao := versaoPrdPrc;
  //
  PrdPrc_XML_STR.ListadePrecos.Cabecalho.CliCodi      := UTF8(CliCodi);
  PrdPrc_XML_STR.ListadePrecos.Cabecalho.CLiNome      := UTF8(CliNome);
  PrdPrc_XML_STR.ListadePrecos.Cabecalho.Account      := UTF8(Account);
  PrdPrc_XML_STR.ListadePrecos.Cabecalho.Cambio       := UTF8(Cambio);
  PrdPrc_XML_STR.ListadePrecos.Cabecalho.Dta.Ini      := UTF8(Dta_Ini);
  PrdPrc_XML_STR.ListadePrecos.Cabecalho.Dta.Fim      := UTF8(Dta_Fim);

  //
  Result := True;

  TbPrdPrcIts.First;
  while not TbPrdPrcIts.Eof do
  begin
    Seq        := Geral.FF0(TbPrdPrcItsSeq.Value);
    Linha      := TbPrdPrcItsLinha.Value;
    ProdUTF    := TbPrdPrcItsProdUTF.Value;
    Produto    := TbPrdPrcItsProduto.Value;
    Origem     := TbPrdPrcItsOrigem.Value;
    Embalagem  := TbPrdPrcItsEmbalagem.Value;
    MyID       := TbPrdPrcItsMyID.Value;
    RecipPosTp := TbPrdPrcItsRecipPosTp.Value;
    IPI_Txa    := Geral.FFT_Dot(TbPrdPrcItsIPI_Txa.Value, 2, siPositivo);
    ICMS_Dif   := Geral.FFT_Dot(TbPrdPrcItsICMS_Dif.Value, 2, siPositivo);
    MoedaEX    := TbPrdPrcItsMoedaEX.Value;
    PrecoEX    := Geral.FFT_Dot(TbPrdPrcItsPrecoEX.Value, 4, siPositivo);
    MoedaID    := TbPrdPrcItsMoedaID.Value;
    PrecoID    := Geral.FFT_Dot(TbPrdPrcItsPrecoID.Value, 4, siPositivo);
    MoedaMI    := TbPrdPrcItsMoedaMI.Value;
    PrecoMI_PL := Geral.FFT_Dot(TbPrdPrcItsPrecoMI_PL.Value, 4, siPositivo);
    PrecoMI_PR := Geral.FFT_Dot(TbPrdPrcItsPrecoMI_PR.Value, 4, siPositivo);
    VariaMI    := Geral.FFT_Dot(TbPrdPrcItsVariaMI.Value, 4, siPositivo);
    Ativo      := TbPrdPrcItsAtivo.Value;
    //
    PrdPrcIts := PrdPrc_XML_STR.ListaDePrecos.PrdPrcIts.Add;
    //
    PrdPrcIts.Item.Seq         := UTF8(Seq);
    PrdPrcIts.Item.Linha       := UTF8(Linha);
    //PrdPrcIts.Item.ProdUTF     := UTF8(ProdUTF);
    PrdPrcIts.Item.Produto     := UTF8(Produto);
    PrdPrcIts.Item.Origem      := UTF8(Origem);
    PrdPrcIts.Item.Embalagem   := UTF8(Embalagem);
    PrdPrcIts.Item.IPI_Txa     := UTF8(IPI_Txa);
    PrdPrcIts.Item.ICMS_Dif    := UTF8(ICMS_Dif);
    PrdPrcIts.Item.MoedaMI     := UTF8(MoedaMI);
    PrdPrcIts.Item.PrecoMI_PL  := UTF8(PrecoMI_PL);
    PrdPrcIts.Item.MoedaEX     := UTF8(MoedaEX);
    PrdPrcIts.Item.PrecoEX     := UTF8(PrecoEX);
    PrdPrcIts.Item.MoedaID     := UTF8(MoedaID);
    PrdPrcIts.Item.PrecoID     := UTF8(PrecoID);
    PrdPrcIts.Item.PrecoMI_PR  := UTF8(PrecoMI_PR);
    PrdPrcIts.Item.VariaMI     := UTF8(VariaMI);
    //
    TbPrdPrcIts.Next;
  end;
  //
  Gmod.SalvaXML_De_Tabela(Arquivo, CO_PrdPrc_NSubDir, CO_PrdPrc_NomeExt, PrdPrc_XML_DOC.XML.Text);
  //
  PrdPrc_XML_DOC := nil;
  //
  if Avisa then
    Geral.MB_Info('Lista de pre�os de produtos atualizada com sucesso!');
end;
}

function TDmod.SelecionaEntCliCad(var CliCodi, CliNome: String): Boolean;
begin
  MyObjects.CriaForm_AcessoTotal(TFmEntCliCad, FmEntCliCad);
  FmEntCliCad.ShowModal;
  Result  := FmEntCliCad.FConfirmado;
  CliCodi := FmEntCliCad.FCliCodi;
  CliNome := FmEntCliCad.FCliNome;
  //
  FmEntCliCad.Destroy;
end;

procedure TDmod.TbEntCliCadCalcFields(DataSet: TDataSet);
begin
  TbEntCliCadNO_Tipo.Value :=
    DmkPF.ConcatTxtChkGroup(TbEntCliCadTipo.Value, MaxListaCadEnt, sListaCadEnt);
end;

function TDmod.TraduzProduto(const Texto: String; var Produto, Diluicao,
  Temperatura: String): Boolean;
var
  I, J, K, Dil: Integer;
  Txt, Tmp, GC: String;
  Achou: Boolean;
  Sai: Boolean;
  //
  function Tem(Palavra: String): Boolean;
  begin
    Result := pos(Palavra, Tmp) > 0;
  end;

begin
  Txt := Texto;
  Txt := Geral.Substitui(Txt, '�', ' �');
  Txt := Geral.Substitui(Txt, '�', ' �');
  while pos('  ', Txt) > 0 do
    Txt := Geral.Substitui(Txt, '  ', ' ');
{
  while pos('oC', Txt) > 0 do
    Txt := Geral.Substitui(Txt, 'oC', '�C');
}
  //

  //
  Dil := 0;
  GC := '';
  Tmp := Geral.Maiusculas(Geral.SemAcento(Txt), False);
  //
  if Tem('ACIDO') then
  begin
    if Tem('FORM') then
    begin
      Tmp := '�cido F�rmico';
      Dil := 5;
    end else
    if Tem('ACET') then
    begin
      Tmp := '�cido Ac�tico';
      Dil := 5;
    end else
    if Tem('OXAL') then
    begin
      Tmp := '�cido Ox�lico';
      Dil := 5;
    end else
    if Tem('SULF') then
    begin
      Tmp := '�cido Sulf�rico';
      Dil := 10;
    end else
    if Tem('MURI') then
    begin
      Tmp := '�cido Clor�drico';
      Dil := 10;
    end else
    if Tem('CLOR') then
    begin
      Tmp := '�cido Clor�drico';
      Dil := 10;
    end else
      Tmp := '';
  end else
  if Tem('SODIO') then
  begin
    if Tem('FORM') then
      Tmp := 'Formiato de S�dio'
    else
    if Tem('ACET') then
      Tmp := 'Acetato de S�dio'
    else
    if Tem('OXAL') then
      Tmp := 'Oxalato de S�dio'
    else
    if Tem('SULF') then
      Tmp := 'Sulfato de S�dio'
    else
    if Tem('MURI') then
      Tmp := 'Cloreto de S�dio'
    else
    if Tem('CLOR') then
      Tmp := 'Cloreto de S�dio'
    else
    if Tem('BICARB') then
      Tmp := 'Bicarbonato de S�dio'
    else
    if Tem('CARBON') then
      Tmp := 'Carbonato de S�dio'
    else
      Tmp := '';
  end else
  if Tem('AMONI') then
  begin
    if Tem('SULF') then
      Tmp := 'Sulfato de Am�nio'
    else
    if Tem('CLOR') then
      Tmp := 'Cloreto de Am�nio'
    else
    if Tem('BICARB') then
      Tmp := 'Bicarbonato de Am�nio'
    else
    if Tem('ACO') then
      Tmp := 'Amon�aco'
    else
      Tmp := '';
  end else
  if Tem('AGUA') then
  begin
    Tmp := '�gua';
  end else
  if Tem('ACACIA') then
  begin
    Tmp := 'Tanino de Ac�cia';
    Dil := 10;
  end else
    Tmp := '';
  //

(*
A SECO
Ac�cia
Acetato de s�dio
Acid Black 210
�cido Ac�tico (1:5)
�cido ac�tico ( 1:5 )
�cido Ac�tico (1 : 5)
�CIDO FORMICO
�cido F�rmico
�cido F�rmico (1 : 5 / 28 oC)
�cido F�rmico (1 : 5)
�cido F�rmico (1:10)
Acido Ox�lico
�cido Ox�lico
�cido Sulf�rico (1 : 10) EM
�cido Sulf�rico (1:10)
ACTICE WB 420
ACTICIDE WB 420
ACTIVE WB 420
Agua
�gua
�gua 25 �C
�gua 28 �C
�gua 30 �C
�gua 32 �C
�gua 35 �C
�gua 35 �C
�gua 40 �C
�gua 43 �C
�gua 45 �C
�gua 48 �C
�gua 50 �C
�gua 55 �C
�gua 60 �C
�gua 65 �C
�gua 65 �C
�gua 70 �C
�gua 27 �C
�gua 27 �C (cobrir couros)
�gua 28 �C
�gua 28 �C (cobrir couros)
�gua Ambiente
�gua Ambiente/Reciclo de cromo
�gua 25 �C
�gua 25 �C
�gua 27 �C
�gua 35 �C
�gua 35 �C
�gua 40 �C
�gua 42 �C
�gua 43 �C
�gua 50 �C
�gua 55 �C
�gua 60 �C
Agua a 30 �C
�gua a 40 �C
�gua Amb. Cobrindo os couros
�gua ambiente
�gua Fria
ALSOFT MLB
Alvox MK
AMARELO S.SO. 4GL
Amoniaco
amon�aco
AQUECER
AQUECER A 45 oC EM
ARACIT DA
ARACIT RM
Autom�tico
Autom�tico - Rodar 5' / Parar 25' - Por
AUTOM�TICO 5/55 - 8 HORAS
AUTOM�TICO A NOITE 8H/10H
automatico anoite
AUTOM�TICO DURANTE
AZUL S.SO. A
Bactericida
Basificante
BEGE S.SO.E
Bemanol CR (Sthal)
Bicarbonato de Am�nia
Bicarbonato de Am�nio
Bicarbonato de S�dio
Biocid A-8
Bissulfito de s�dio
BORRON ANV
BORRON LFG
BORRON TE
BORRON 6113
BORRON ANV
BORRON DL-LA
BORRON DNC
BORRON LFG
BORRON SAF
BORRON ST
BORRON WP
Busan 144
Busan 1444
BUSAN 1451
Cal
Cal Hidratada
Carbonato de s�dio
Castanheira
Castanheiro
CASTANHO A3R
CASTANHO ADF
CASTANHO RA
CASTANHO S.SO. CG
CASTANHO S.SO. DS
CASTANHO S.SO. GLN
CASTANHO S.SO. H
CASTANHO S.SO. RE
CASTANHO S.SO. RLN
CASTANHO SELLASET M
CHROMOSAL B
Clarotan
CLORITO DE S�DIO (1 : 10)
Colocar 10% de reciclo se necess�rio
cor:
Corante
CORIPOL A
CORIPOL BR-SU
CORIPOL EFA
CORIPOL GA
CORIPOL OPS
CORIPOL A
CORIPOL A TESTE
CORIPOL ALF
CORIPOL BLV
CORIPOL BR CT
CORIPOL BR SL
CORIPOL BR SU
CORIPOL BR-CT
CORIPOL BR-OP
CORIPOL BR-SU
CORIPOL CLD
CORIPOL HDS
CORIPOL IRH
CORIPOL MK
CORIPOL MLA
CORIPOL OPS
CORIPOL RB
CORIPOL RB (sozinho)
CORIPOL SG
CORIPOL SG BR
CORIPOL SG LA
CORIPOL SG-BR
CORIPOL SG-LA
CORIPOL SLG
couros no ful�o
CROMENO FB
CROMENO 6213 New
CROMENO XP
Cromo 26/33
Cromo 33/26
Cromosal B
Delitan K1
DERMASCAL 6140 (1:5)
DERMASCAL 6140 (1:5)
DERMASCAL DU LA
DERMASCAL 6140 (1:5)
DERMASCAL F LIQ.(1 : 5)
DERMASCAL HLA
DERMASCAL U LIQ.(1 : 5)
Descal SD
Descarregar
Deslizante
Deslizante p�
Di�xido de tit�nio
DRY WALK WAS 1
Dybest
ERHA GM 3034
ERHAVIT MC
ERHAVIT SRC
ERHAVIT 6211
ERHAVIT MC
ERHAVIT MC-F
ERHAVIT SR
ERHAVIT SRC
ESCORRER
EUPILON WAS 1
Feliderm MGO
Feliderm SAN
Feliderm SAN (deslizante)
FERVURA 3 MIN. 100%
FILLER WE
Formiato de S�dio
Formiato de S�dio
Fungicida
Fungicida (1 : 20)
Fungicida a base de OIT
GIRANDO
Glicerina
HAVANA E
HAVANA SELLA STAR
INVADERM LU
INVADERM LU
INVADERM SN
LARANJA S.SO 2 GC
LARANJA S.SO. A
LF Alvejante HO
LF Desencalante P�
LF Moc Cr�
LF Remolhante SE
LS Tan BA (RODACRYL I 113)
Mabratan TA
MAGNOPAL HBR
MAGNOPAL TGR
MAGNOPAL BC3
MAGNOPAL PGN
MAGNOPAL RHN
MAGNOPAL SFT
MAGNOPAL D LA
MAGNOPAL DLA
MAGNOPAL D-LA
MAGNOPAL FTP
MAGNOPAL HBR
MAGNOPAL RHN
MAGNOPAL SFT
MARINHO S.SO. A
Metabissulfito de S�dio
Mimosa
Mimosa (Clarotan)
Mirecide - 50 / TS
MK ECT 005
MK ER 130
MK KROMIUM PP
NO FINAL
Nokotan 1016
NOKOTAN AP 810
Nutratan CR
OLIVA S.SO. 2G
OLIVA SELLA STAR
OLIVA SELLASTAR
PELLVIT B
PELLVIT IS-B
PELLVIT KAB P
PELVIT B-LA
PR�-DESCARNE
PRETO SSO IG CONC.
PRETO HS
PRETO IG conc (1:10)
PRETO S.SO BG
PRETO S.SO. BA
PRETO S.SO. BG
PRETO S.SO. IG
PRETO S.SO. WB
PRETO SELLA S�LIDO HS
PRETO SSO FT
PRODUTO L 6211
PRODUTO VL 6213
Quebracho
Reciclo
RECICLO DE CALEIRO
Remonte
RODA FEEL CLD
RODACOR AS BRANCO AZL
RODANDO
Rodar
Rodar total de 2 horas de neutraliza��o
RODOU MAIS
ROHAPON NPB
ROHAPON NPB
Royaltan
Sal
Sal de Cromo - 33% BAS/26% CONC.
Sal Fino
Sal limpo
SELLA SOLIDO PRETO BA
SELLAFIX FRD
SELLAFIX FRD
SELLASOL NG Liq.
SELLASOL BR SF
SELLASOL BR-SF
SELLASOL FT new
SELLASOL HF N
SELLASOL NG
SELLASOL NG GRAN
SELLASOL NG gran.
SELLASOL NG Liq
SELLASOL NG LIQ.
SELLASOL NG p�
SELLASOL TN GRAN
SELLASOL TN Gran.
SELLASOL TN PO
SELLASOL TN p�
SELLASOL TN-GRAN
SELLASOL TN-P�
SELLASOLIDO PRETO BA
SELLATAM FB LIQ.
SELLATAN CF NEW Liq.
SELLATAN RL LIQ.
SELLATAN AR
SELLATAN CF NEW
SELLATAN CF-NEW
SELLATAN D LA
SELLATAN D-LA
SELLATAN FB
SELLATAN FB liq.
SELLATAN FL
SELLATAN GS-B
SELLATAN KL
SELLATAN RBL
SELLATAN RL
SELLATAN RL Liq.
SELLATAN TA-LA
SEM BANHO
SERVOIL CB
SOBRA DE BANHO
SOBRA DE �GUA LIMPA
Sobra de lavagem conforme escala
Soda Barrilha
Soda C�ustca Liq.
Soda L�quida
Sorbitol
Sulfato b�sico de cromo
Sulfato de Am�nea
Sulfato de Am�nio
Sulfeto de S�dio
Sulfeto de S�dio 50%
Sulfeto de S�dio 41%
Sulfeto de S�dio 60%
TABACO SELLA STAR
Tan PVU
TANIGAN PR
Tanigan PC
Tanigan PR
TANNESCO HN Liq.
TANNESCO HN
TANNESCO HN liq.
TANNESCO HN L�Q.
Tara
Tensoativo
TURQUESA S.SO. 2BL
Ultrader DGW 710
Ultrader DWG 710
Ultraprime WAX
V�LVULAS ABERTAS
VELOCIDADE 02
VELOCIDADE 04
VER COR BANHO E CHEIRO
VERMELHO S.SO. E
Weibull Extra Light
*)

  Txt := Geral.Maiusculas(Geral.SemAcento(Txt), False);
  K := pos('�C', Txt);
  if K > 0 then
  begin
    Sai := False;
    for I := K - 1 downto 1 do
    begin
      if not Sai then
      begin
        if (CharInSet(Txt[I], (['0'..'9', ' ']))) then
          GC := Txt[I] + GC
        else
          Sai := True;
      end;
    end;
    GC := Trim(GC);
  end;

  //

  if Tmp <> '' then
  begin
    Produto := Tmp;
    Diluicao := IntToStr(Dil);
    Temperatura := GC;
  end else begin
    Produto := Texto;
    Diluicao := '';
    Temperatura := '';
  end;
end;

function TDmod.UTF8(Texto: String): String;
begin
  Result := DmkPF.ConverteANSItoUTF8(Texto);
end;


end.

