object FmConfigs: TFmConfigs
  Left = 339
  Top = 185
  Caption = 'XXX-XXXXX-999 :: ????????????? ??????????? ????????????'
  ClientHeight = 207
  ClientWidth = 421
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 93
    Width = 421
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 0
    ExplicitTop = 231
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 417
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 137
    Width = 421
    Height = 70
    Align = alBottom
    TabOrder = 1
    ExplicitTop = 275
    object PnSaiDesis: TPanel
      Left = 275
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 273
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 421
    Height = 93
    Align = alClient
    TabOrder = 2
    ExplicitHeight = 231
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 417
      Height = 158
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitLeft = 202
      ExplicitTop = 3
      object Label1: TLabel
        Left = 8
        Top = 8
        Width = 36
        Height = 13
        Caption = 'Vers'#227'o:'
      end
      object Label2: TLabel
        Left = 8
        Top = 88
        Width = 111
        Height = 13
        Caption = 'Lista de pre'#231'os padr'#227'o:'
        Visible = False
      end
      object Label4: TLabel
        Left = 48
        Top = 8
        Width = 89
        Height = 13
        Caption = 'Nome do Account:'
      end
      object Label5: TLabel
        Left = 8
        Top = 48
        Width = 84
        Height = 13
        Caption = 'Sigla do Account:'
        Visible = False
      end
      object Label6: TLabel
        Left = 100
        Top = 48
        Width = 136
        Height = 13
        Caption = 'ID MD5 FAST'#174' do Account:'
        Visible = False
      end
      object SpeedButton1: TSpeedButton
        Left = 384
        Top = 64
        Width = 21
        Height = 21
        Caption = '...'
        Visible = False
        OnClick = SpeedButton1Click
      end
      object SpeedButton2: TSpeedButton
        Left = 384
        Top = 104
        Width = 21
        Height = 21
        Caption = '...'
        Visible = False
        OnClick = SpeedButton2Click
      end
      object EdVersao: TdmkEdit
        Left = 8
        Top = 24
        Width = 37
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdVersaoChange
      end
      object EdAccNome: TdmkEdit
        Left = 48
        Top = 24
        Width = 357
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnChange = EdVersaoChange
      end
      object EdAccID: TdmkEdit
        Left = 8
        Top = 64
        Width = 85
        Height = 21
        TabOrder = 2
        Visible = False
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnChange = EdVersaoChange
      end
      object EdUserID: TdmkEdit
        Left = 96
        Top = 64
        Width = 286
        Height = 21
        TabOrder = 3
        Visible = False
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnChange = EdVersaoChange
      end
      object EdDefLstPrc: TdmkEdit
        Left = 8
        Top = 104
        Width = 374
        Height = 21
        TabOrder = 4
        Visible = False
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnChange = EdVersaoChange
      end
    end
  end
end
