
{************************************************************************************************}
{                                                                                                }
{                                        XML Data Binding                                        }
{                                                                                                }
{         Generated on: 30/03/2014 19:25:44                                                      }
{       Generated from: C:\_Compilers\Delphi_XE2\Aplicativos\TFL\TFC\Schemas\Configs_v0.01.xsd   }
{   Settings stored in: C:\_Compilers\Delphi_XE2\Aplicativos\TFL\TFC\Schemas\Configs_v0.01.xdb   }
{                                                                                                }
{************************************************************************************************}

unit Configs_v001;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLTConfigs = interface;
  IXMLConfiguracoes = interface;
  IXMLTConfigsCab = interface;

{ IXMLTConfigs }

  IXMLTConfigs = interface(IXMLNode)
    ['{EEBBF0A8-AF09-41C2-BBEE-F200E3328037}']
    { Property Accessors }
    function Get_Versao: UnicodeString;
    function Get_Configuracoes: IXMLConfiguracoes;
    procedure Set_Versao(Value: UnicodeString);
    { Methods & Properties }
    property Versao: UnicodeString read Get_Versao write Set_Versao;
    property Configuracoes: IXMLConfiguracoes read Get_Configuracoes;
  end;

{ IXMLConfiguracoes }

  IXMLConfiguracoes = interface(IXMLNode)
    ['{BE72C9AE-3E3D-4B64-9505-880F8B60D5DB}']
    { Property Accessors }
    function Get_Cabecalho: IXMLTConfigsCab;
    { Methods & Properties }
    property Cabecalho: IXMLTConfigsCab read Get_Cabecalho;
  end;

{ IXMLTConfigsCab }

  IXMLTConfigsCab = interface(IXMLNode)
    ['{811D7F24-7405-4805-8203-2CEA5CC8C0A8}']
    { Property Accessors }
    function Get_AccNome: UnicodeString;
    function Get_AccID: UnicodeString;
    function Get_UserID: UnicodeString;
    function Get_DefLstPrc: UnicodeString;
    procedure Set_AccNome(Value: UnicodeString);
    procedure Set_AccID(Value: UnicodeString);
    procedure Set_UserID(Value: UnicodeString);
    procedure Set_DefLstPrc(Value: UnicodeString);
    { Methods & Properties }
    property AccNome: UnicodeString read Get_AccNome write Set_AccNome;
    property AccID: UnicodeString read Get_AccID write Set_AccID;
    property UserID: UnicodeString read Get_UserID write Set_UserID;
    property DefLstPrc: UnicodeString read Get_DefLstPrc write Set_DefLstPrc;
  end;

{ Forward Decls }

  TXMLTConfigs = class;
  TXMLConfiguracoes = class;
  TXMLTConfigsCab = class;

{ TXMLTConfigs }

  TXMLTConfigs = class(TXMLNode, IXMLTConfigs)
  protected
    { IXMLTConfigs }
    function Get_Versao: UnicodeString;
    function Get_Configuracoes: IXMLConfiguracoes;
    procedure Set_Versao(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLConfiguracoes }

  TXMLConfiguracoes = class(TXMLNode, IXMLConfiguracoes)
  protected
    { IXMLConfiguracoes }
    function Get_Cabecalho: IXMLTConfigsCab;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTConfigsCab }

  TXMLTConfigsCab = class(TXMLNode, IXMLTConfigsCab)
  protected
    { IXMLTConfigsCab }
    function Get_AccNome: UnicodeString;
    function Get_AccID: UnicodeString;
    function Get_UserID: UnicodeString;
    function Get_DefLstPrc: UnicodeString;
    procedure Set_AccNome(Value: UnicodeString);
    procedure Set_AccID(Value: UnicodeString);
    procedure Set_UserID(Value: UnicodeString);
    procedure Set_DefLstPrc(Value: UnicodeString);
  end;

{ Global Functions }

function GetConfigs(Doc: IXMLDocument): IXMLTConfigs;
function LoadConfigs(const FileName: string): IXMLTConfigs;
function NewConfigs: IXMLTConfigs;

const
  TargetNamespace = 'http://www.dermatek.com.br/www/apps/TFC';

implementation

{ Global Functions }

function GetConfigs(Doc: IXMLDocument): IXMLTConfigs;
begin
  Result := Doc.GetDocBinding('Configs', TXMLTConfigs, TargetNamespace) as IXMLTConfigs;
end;

function LoadConfigs(const FileName: string): IXMLTConfigs;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('Configs', TXMLTConfigs, TargetNamespace) as IXMLTConfigs;
end;

function NewConfigs: IXMLTConfigs;
begin
  Result := NewXMLDocument.GetDocBinding('Configs', TXMLTConfigs, TargetNamespace) as IXMLTConfigs;
end;

{ TXMLTConfigs }

procedure TXMLTConfigs.AfterConstruction;
begin
  RegisterChildNode('Configuracoes', TXMLConfiguracoes);
  inherited;
end;

function TXMLTConfigs.Get_Versao: UnicodeString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTConfigs.Set_Versao(Value: UnicodeString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTConfigs.Get_Configuracoes: IXMLConfiguracoes;
begin
  Result := ChildNodes['Configuracoes'] as IXMLConfiguracoes;
end;

{ TXMLConfiguracoes }

procedure TXMLConfiguracoes.AfterConstruction;
begin
  RegisterChildNode('Cabecalho', TXMLTConfigsCab);
  inherited;
end;

function TXMLConfiguracoes.Get_Cabecalho: IXMLTConfigsCab;
begin
  Result := ChildNodes['Cabecalho'] as IXMLTConfigsCab;
end;

{ TXMLTConfigsCab }

function TXMLTConfigsCab.Get_AccNome: UnicodeString;
begin
  Result := ChildNodes['AccNome'].Text;
end;

procedure TXMLTConfigsCab.Set_AccNome(Value: UnicodeString);
begin
  ChildNodes['AccNome'].NodeValue := Value;
end;

function TXMLTConfigsCab.Get_AccID: UnicodeString;
begin
  Result := ChildNodes['AccID'].Text;
end;

procedure TXMLTConfigsCab.Set_AccID(Value: UnicodeString);
begin
  ChildNodes['AccID'].NodeValue := Value;
end;

function TXMLTConfigsCab.Get_UserID: UnicodeString;
begin
  Result := ChildNodes['UserID'].Text;
end;

procedure TXMLTConfigsCab.Set_UserID(Value: UnicodeString);
begin
  ChildNodes['UserID'].NodeValue := Value;
end;

function TXMLTConfigsCab.Get_DefLstPrc: UnicodeString;
begin
  Result := ChildNodes['DefLstPrc'].Text;
end;

procedure TXMLTConfigsCab.Set_DefLstPrc(Value: UnicodeString);
begin
  ChildNodes['DefLstPrc'].NodeValue := Value;
end;

end.