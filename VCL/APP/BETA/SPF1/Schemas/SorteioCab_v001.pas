
{*************************************************************************************************}
{                                                                                                 }
{                                        XML Data Binding                                         }
{                                                                                                 }
{         Generated on: 30/01/2017 13:22:17                                                       }
{       Generated from: C:\_Compilers\DELPHI_XE2\VCL\APP\BETA\SPF1\Schemas\SorteioCab_v0.01.xsd   }
{   Settings stored in: C:\_Compilers\DELPHI_XE2\VCL\APP\BETA\SPF1\Schemas\SorteioCab_v0.01.xdb   }
{                                                                                                 }
{*************************************************************************************************}

unit SorteioCab_v001;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLTSorteioCab = interface;
  IXMLSorteios = interface;
  IXMLFldsSorteioCab = interface;
  IXMLTFldsSorteioCab = interface;

{ IXMLTSorteioCab }

  IXMLTSorteioCab = interface(IXMLNode)
    ['{586BFEFD-98B6-4079-8883-CE93627CACDA}']
    { Property Accessors }
    function Get_Versao: UnicodeString;
    function Get_Sorteios: IXMLSorteios;
    procedure Set_Versao(Value: UnicodeString);
    { Methods & Properties }
    property Versao: UnicodeString read Get_Versao write Set_Versao;
    property Sorteios: IXMLSorteios read Get_Sorteios;
  end;

{ IXMLSorteios }

  IXMLSorteios = interface(IXMLNodeCollection)
    ['{5956B854-4A04-4ABB-A848-90EC756CE764}']
    { Property Accessors }
    function Get_FldsSorteioCab(Index: Integer): IXMLFldsSorteioCab;
    { Methods & Properties }
    function Add: IXMLFldsSorteioCab;
    function Insert(const Index: Integer): IXMLFldsSorteioCab;
    property FldsSorteioCab[Index: Integer]: IXMLFldsSorteioCab read Get_FldsSorteioCab; default;
  end;

{ IXMLFldsSorteioCab }

  IXMLFldsSorteioCab = interface(IXMLNode)
    ['{69A57E25-2D2A-468B-9A33-547973BFFA09}']
    { Property Accessors }
    function Get_Item: IXMLTFldsSorteioCab;
    { Methods & Properties }
    property Item: IXMLTFldsSorteioCab read Get_Item;
  end;

{ IXMLTFldsSorteioCab }

  IXMLTFldsSorteioCab = interface(IXMLNode)
    ['{9ECE6548-FD06-4ECB-AA45-6B7EE052ED41}']
    { Property Accessors }
    function Get_Codigo: UnicodeString;
    procedure Set_Codigo(Value: UnicodeString);
    { Methods & Properties }
    property Codigo: UnicodeString read Get_Codigo write Set_Codigo;
  end;

{ Forward Decls }

  TXMLTSorteioCab = class;
  TXMLSorteios = class;
  TXMLFldsSorteioCab = class;
  TXMLTFldsSorteioCab = class;

{ TXMLTSorteioCab }

  TXMLTSorteioCab = class(TXMLNode, IXMLTSorteioCab)
  protected
    { IXMLTSorteioCab }
    function Get_Versao: UnicodeString;
    function Get_Sorteios: IXMLSorteios;
    procedure Set_Versao(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSorteios }

  TXMLSorteios = class(TXMLNodeCollection, IXMLSorteios)
  protected
    { IXMLSorteios }
    function Get_FldsSorteioCab(Index: Integer): IXMLFldsSorteioCab;
    function Add: IXMLFldsSorteioCab;
    function Insert(const Index: Integer): IXMLFldsSorteioCab;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLFldsSorteioCab }

  TXMLFldsSorteioCab = class(TXMLNode, IXMLFldsSorteioCab)
  protected
    { IXMLFldsSorteioCab }
    function Get_Item: IXMLTFldsSorteioCab;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTFldsSorteioCab }

  TXMLTFldsSorteioCab = class(TXMLNode, IXMLTFldsSorteioCab)
  protected
    { IXMLTFldsSorteioCab }
    function Get_Codigo: UnicodeString;
    procedure Set_Codigo(Value: UnicodeString);
  end;

{ Global Functions }

function GetSorteioCab(Doc: IXMLDocument): IXMLTSorteioCab;
function LoadSorteioCab(const FileName: string): IXMLTSorteioCab;
function NewSorteioCab: IXMLTSorteioCab;

const
  TargetNamespace = 'http://www.dermatek.com.br/www/apps/TFC';

implementation

{ Global Functions }

function GetSorteioCab(Doc: IXMLDocument): IXMLTSorteioCab;
begin
  Result := Doc.GetDocBinding('SorteioCab', TXMLTSorteioCab, TargetNamespace) as IXMLTSorteioCab;
end;

function LoadSorteioCab(const FileName: string): IXMLTSorteioCab;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('SorteioCab', TXMLTSorteioCab, TargetNamespace) as IXMLTSorteioCab;
end;

function NewSorteioCab: IXMLTSorteioCab;
begin
  Result := NewXMLDocument.GetDocBinding('SorteioCab', TXMLTSorteioCab, TargetNamespace) as IXMLTSorteioCab;
end;

{ TXMLTSorteioCab }

procedure TXMLTSorteioCab.AfterConstruction;
begin
  RegisterChildNode('Sorteios', TXMLSorteios);
  inherited;
end;

function TXMLTSorteioCab.Get_Versao: UnicodeString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTSorteioCab.Set_Versao(Value: UnicodeString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTSorteioCab.Get_Sorteios: IXMLSorteios;
begin
  Result := ChildNodes['Sorteios'] as IXMLSorteios;
end;

{ TXMLSorteios }

procedure TXMLSorteios.AfterConstruction;
begin
  RegisterChildNode('FldsSorteioCab', TXMLFldsSorteioCab);
  ItemTag := 'FldsSorteioCab';
  ItemInterface := IXMLFldsSorteioCab;
  inherited;
end;

function TXMLSorteios.Get_FldsSorteioCab(Index: Integer): IXMLFldsSorteioCab;
begin
  Result := List[Index] as IXMLFldsSorteioCab;
end;

function TXMLSorteios.Add: IXMLFldsSorteioCab;
begin
  Result := AddItem(-1) as IXMLFldsSorteioCab;
end;

function TXMLSorteios.Insert(const Index: Integer): IXMLFldsSorteioCab;
begin
  Result := AddItem(Index) as IXMLFldsSorteioCab;
end;

{ TXMLFldsSorteioCab }

procedure TXMLFldsSorteioCab.AfterConstruction;
begin
  RegisterChildNode('Item', TXMLTFldsSorteioCab);
  inherited;
end;

function TXMLFldsSorteioCab.Get_Item: IXMLTFldsSorteioCab;
begin
  Result := ChildNodes['Item'] as IXMLTFldsSorteioCab;
end;

{ TXMLTFldsSorteioCab }

function TXMLTFldsSorteioCab.Get_Codigo: UnicodeString;
begin
  Result := ChildNodes['Codigo'].Text;
end;

procedure TXMLTFldsSorteioCab.Set_Codigo(Value: UnicodeString);
begin
  ChildNodes['Codigo'].NodeValue := Value;
end;

end.