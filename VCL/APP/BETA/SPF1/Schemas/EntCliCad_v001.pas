
{************************************************************************************************}
{                                                                                                }
{                                        XML Data Binding                                        }
{                                                                                                }
{         Generated on: 30/01/2017 14:41:13                                                      }
{       Generated from: C:\_Compilers\DELPHI_XE2\VCL\APP\BETA\SPF1\Schemas\EntCliCad_v0.01.xsd   }
{   Settings stored in: C:\_Compilers\DELPHI_XE2\VCL\APP\BETA\SPF1\Schemas\EntCliCad_v0.01.xdb   }
{                                                                                                }
{************************************************************************************************}

unit EntCliCad_v001;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLTEntCliCad = interface;
  IXMLClientes = interface;
  IXMLEntCliCadCab = interface;
  IXMLTEntCliCadCab = interface;

{ IXMLTEntCliCad }

  IXMLTEntCliCad = interface(IXMLNode)
    ['{6ACA7895-0C62-4E0C-9712-E9C51DA53129}']
    { Property Accessors }
    function Get_Versao: UnicodeString;
    function Get_Clientes: IXMLClientes;
    procedure Set_Versao(Value: UnicodeString);
    { Methods & Properties }
    property Versao: UnicodeString read Get_Versao write Set_Versao;
    property Clientes: IXMLClientes read Get_Clientes;
  end;

{ IXMLClientes }

  IXMLClientes = interface(IXMLNodeCollection)
    ['{FF2EDC28-C519-4E97-9561-1EF91FFC6314}']
    { Property Accessors }
    function Get_EntCliCadCab(Index: Integer): IXMLEntCliCadCab;
    { Methods & Properties }
    function Add: IXMLEntCliCadCab;
    function Insert(const Index: Integer): IXMLEntCliCadCab;
    property EntCliCadCab[Index: Integer]: IXMLEntCliCadCab read Get_EntCliCadCab; default;
  end;

{ IXMLEntCliCadCab }

  IXMLEntCliCadCab = interface(IXMLNode)
    ['{5E832191-C382-45D5-8E45-75F40EB5632E}']
    { Property Accessors }
    function Get_Item: IXMLTEntCliCadCab;
    { Methods & Properties }
    property Item: IXMLTEntCliCadCab read Get_Item;
  end;

{ IXMLTEntCliCadCab }

  IXMLTEntCliCadCab = interface(IXMLNode)
    ['{68006255-4C3A-4244-951E-0730333F9CCF}']
    { Property Accessors }
    function Get_CliCodi: UnicodeString;
    function Get_CliNome: UnicodeString;
    function Get_Cidade: UnicodeString;
    function Get_UF: UnicodeString;
    function Get_Pais: UnicodeString;
    function Get_Tipo: UnicodeString;
    function Get_Ordem: UnicodeString;
    function Get_Aleatorio: UnicodeString;
    function Get_Ativo: UnicodeString;
    procedure Set_CliCodi(Value: UnicodeString);
    procedure Set_CliNome(Value: UnicodeString);
    procedure Set_Cidade(Value: UnicodeString);
    procedure Set_UF(Value: UnicodeString);
    procedure Set_Pais(Value: UnicodeString);
    procedure Set_Tipo(Value: UnicodeString);
    procedure Set_Ordem(Value: UnicodeString);
    procedure Set_Aleatorio(Value: UnicodeString);
    procedure Set_Ativo(Value: UnicodeString);
    { Methods & Properties }
    property CliCodi: UnicodeString read Get_CliCodi write Set_CliCodi;
    property CliNome: UnicodeString read Get_CliNome write Set_CliNome;
    property Cidade: UnicodeString read Get_Cidade write Set_Cidade;
    property UF: UnicodeString read Get_UF write Set_UF;
    property Pais: UnicodeString read Get_Pais write Set_Pais;
    property Tipo: UnicodeString read Get_Tipo write Set_Tipo;
    property Ordem: UnicodeString read Get_Ordem write Set_Ordem;
    property Aleatorio: UnicodeString read Get_Aleatorio write Set_Aleatorio;
    property Ativo: UnicodeString read Get_Ativo write Set_Ativo;
  end;

{ Forward Decls }

  TXMLTEntCliCad = class;
  TXMLClientes = class;
  TXMLEntCliCadCab = class;
  TXMLTEntCliCadCab = class;

{ TXMLTEntCliCad }

  TXMLTEntCliCad = class(TXMLNode, IXMLTEntCliCad)
  protected
    { IXMLTEntCliCad }
    function Get_Versao: UnicodeString;
    function Get_Clientes: IXMLClientes;
    procedure Set_Versao(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLClientes }

  TXMLClientes = class(TXMLNodeCollection, IXMLClientes)
  protected
    { IXMLClientes }
    function Get_EntCliCadCab(Index: Integer): IXMLEntCliCadCab;
    function Add: IXMLEntCliCadCab;
    function Insert(const Index: Integer): IXMLEntCliCadCab;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLEntCliCadCab }

  TXMLEntCliCadCab = class(TXMLNode, IXMLEntCliCadCab)
  protected
    { IXMLEntCliCadCab }
    function Get_Item: IXMLTEntCliCadCab;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTEntCliCadCab }

  TXMLTEntCliCadCab = class(TXMLNode, IXMLTEntCliCadCab)
  protected
    { IXMLTEntCliCadCab }
    function Get_CliCodi: UnicodeString;
    function Get_CliNome: UnicodeString;
    function Get_Cidade: UnicodeString;
    function Get_UF: UnicodeString;
    function Get_Pais: UnicodeString;
    function Get_Tipo: UnicodeString;
    function Get_Ordem: UnicodeString;
    function Get_Aleatorio: UnicodeString;
    function Get_Ativo: UnicodeString;
    procedure Set_CliCodi(Value: UnicodeString);
    procedure Set_CliNome(Value: UnicodeString);
    procedure Set_Cidade(Value: UnicodeString);
    procedure Set_UF(Value: UnicodeString);
    procedure Set_Pais(Value: UnicodeString);
    procedure Set_Tipo(Value: UnicodeString);
    procedure Set_Ordem(Value: UnicodeString);
    procedure Set_Aleatorio(Value: UnicodeString);
    procedure Set_Ativo(Value: UnicodeString);
  end;

{ Global Functions }

function GetEntCliCad(Doc: IXMLDocument): IXMLTEntCliCad;
function LoadEntCliCad(const FileName: string): IXMLTEntCliCad;
function NewEntCliCad: IXMLTEntCliCad;

const
  TargetNamespace = 'http://www.dermatek.com.br/www/apps/TFC';

implementation

{ Global Functions }

function GetEntCliCad(Doc: IXMLDocument): IXMLTEntCliCad;
begin
  Result := Doc.GetDocBinding('EntCliCad', TXMLTEntCliCad, TargetNamespace) as IXMLTEntCliCad;
end;

function LoadEntCliCad(const FileName: string): IXMLTEntCliCad;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('EntCliCad', TXMLTEntCliCad, TargetNamespace) as IXMLTEntCliCad;
end;

function NewEntCliCad: IXMLTEntCliCad;
begin
  Result := NewXMLDocument.GetDocBinding('EntCliCad', TXMLTEntCliCad, TargetNamespace) as IXMLTEntCliCad;
end;

{ TXMLTEntCliCad }

procedure TXMLTEntCliCad.AfterConstruction;
begin
  RegisterChildNode('Clientes', TXMLClientes);
  inherited;
end;

function TXMLTEntCliCad.Get_Versao: UnicodeString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTEntCliCad.Set_Versao(Value: UnicodeString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTEntCliCad.Get_Clientes: IXMLClientes;
begin
  Result := ChildNodes['Clientes'] as IXMLClientes;
end;

{ TXMLClientes }

procedure TXMLClientes.AfterConstruction;
begin
  RegisterChildNode('EntCliCadCab', TXMLEntCliCadCab);
  ItemTag := 'EntCliCadCab';
  ItemInterface := IXMLEntCliCadCab;
  inherited;
end;

function TXMLClientes.Get_EntCliCadCab(Index: Integer): IXMLEntCliCadCab;
begin
  Result := List[Index] as IXMLEntCliCadCab;
end;

function TXMLClientes.Add: IXMLEntCliCadCab;
begin
  Result := AddItem(-1) as IXMLEntCliCadCab;
end;

function TXMLClientes.Insert(const Index: Integer): IXMLEntCliCadCab;
begin
  Result := AddItem(Index) as IXMLEntCliCadCab;
end;

{ TXMLEntCliCadCab }

procedure TXMLEntCliCadCab.AfterConstruction;
begin
  RegisterChildNode('Item', TXMLTEntCliCadCab);
  inherited;
end;

function TXMLEntCliCadCab.Get_Item: IXMLTEntCliCadCab;
begin
  Result := ChildNodes['Item'] as IXMLTEntCliCadCab;
end;

{ TXMLTEntCliCadCab }

function TXMLTEntCliCadCab.Get_CliCodi: UnicodeString;
begin
  Result := ChildNodes['CliCodi'].Text;
end;

procedure TXMLTEntCliCadCab.Set_CliCodi(Value: UnicodeString);
begin
  ChildNodes['CliCodi'].NodeValue := Value;
end;

function TXMLTEntCliCadCab.Get_CliNome: UnicodeString;
begin
  Result := ChildNodes['CliNome'].Text;
end;

procedure TXMLTEntCliCadCab.Set_CliNome(Value: UnicodeString);
begin
  ChildNodes['CliNome'].NodeValue := Value;
end;

function TXMLTEntCliCadCab.Get_Cidade: UnicodeString;
begin
  Result := ChildNodes['Cidade'].Text;
end;

procedure TXMLTEntCliCadCab.Set_Cidade(Value: UnicodeString);
begin
  ChildNodes['Cidade'].NodeValue := Value;
end;

function TXMLTEntCliCadCab.Get_UF: UnicodeString;
begin
  Result := ChildNodes['UF'].Text;
end;

procedure TXMLTEntCliCadCab.Set_UF(Value: UnicodeString);
begin
  ChildNodes['UF'].NodeValue := Value;
end;

function TXMLTEntCliCadCab.Get_Pais: UnicodeString;
begin
  Result := ChildNodes['Pais'].Text;
end;

procedure TXMLTEntCliCadCab.Set_Pais(Value: UnicodeString);
begin
  ChildNodes['Pais'].NodeValue := Value;
end;

function TXMLTEntCliCadCab.Get_Tipo: UnicodeString;
begin
  Result := ChildNodes['Tipo'].Text;
end;

procedure TXMLTEntCliCadCab.Set_Tipo(Value: UnicodeString);
begin
  ChildNodes['Tipo'].NodeValue := Value;
end;

function TXMLTEntCliCadCab.Get_Ordem: UnicodeString;
begin
  Result := ChildNodes['Ordem'].Text;
end;

procedure TXMLTEntCliCadCab.Set_Ordem(Value: UnicodeString);
begin
  ChildNodes['Ordem'].NodeValue := Value;
end;

function TXMLTEntCliCadCab.Get_Aleatorio: UnicodeString;
begin
  Result := ChildNodes['Aleatorio'].Text;
end;

procedure TXMLTEntCliCadCab.Set_Aleatorio(Value: UnicodeString);
begin
  ChildNodes['Aleatorio'].NodeValue := Value;
end;

function TXMLTEntCliCadCab.Get_Ativo: UnicodeString;
begin
  Result := ChildNodes['Ativo'].Text;
end;

procedure TXMLTEntCliCadCab.Set_Ativo(Value: UnicodeString);
begin
  ChildNodes['Ativo'].NodeValue := Value;
end;

end.