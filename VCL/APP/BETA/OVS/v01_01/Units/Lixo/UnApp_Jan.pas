unit UnApp_Jan;

interface

uses
  Windows, SysUtils, Classes, DB, Menus, UnDmkEnums, mySQLDbTables, Forms,
  Grids, DBGrids, UnInternalConsts;

type
  TUnApp_Jan = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    procedure MostraFormOpcoesApp();
  end;

var
  App_Jan: TUnApp_Jan;


implementation

uses
  MyDBCheck, Module, OpcoesApp;

{ TUnApp_Jan }

procedure TUnApp_Jan.MostraFormOpcoesApp();
begin
  if DBCheck.CriaFm(TFmOpcoesApp, FmOpcoesApp, afmoSoBoss) then
  begin
    FmOpcoesApp.ShowModal;
    FmOpcoesApp.Destroy;
  end;
end;

end.
