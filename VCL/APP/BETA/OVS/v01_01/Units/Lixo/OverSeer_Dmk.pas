unit OverSeer_Dmk;

interface

uses
  Forms, Db, Menus, Controls, Classes, ExtCtrls,
  UnInternalConsts, Messages, Dialogs, Windows, SysUtils, Graphics, StdCtrls,
  Mask, DBCtrls, UnInternalConsts2, jpeg, mySQLDbTables, UnGOTOy,
  UnMyLinguas, Buttons, ComCtrls, dmkGeral;

type
  TFmOverSeer_Dmk = class(TForm)
    GroupBox1: TGroupBox;
    Label3: TLabel;
    EdTerminal: TEdit;
    Label1: TLabel;
    EdLogin: TEdit;
    Label2: TLabel;
    EdSenha: TEdit;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    ProgressBar1: TProgressBar;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtDesiste: TBitBtn;
    Panel1: TPanel;
    BtEntra: TBitBtn;
    EdEmpresa: TEdit;
    Label4: TLabel;
    LaSenhas: TLabel;
    LaConexao: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure Close1Click(Sender: TObject);
    procedure EdSenhaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdLoginKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure BtSairClick(Sender: TObject);
    procedure EdSenhaExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtEntraClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure LaSenhasClick(Sender: TObject);
    procedure LaSenhasMouseLeave(Sender: TObject);
    procedure LaSenhasMouseEnter(Sender: TObject);
    procedure LaConexaoClick(Sender: TObject);
    procedure LaConexaoMouseEnter(Sender: TObject);
    procedure LaConexaoMouseLeave(Sender: TObject);

  private
    { Private declarations }
    procedure ControleMudou(Sender: TObject);
    procedure BtInstallVisible;

  public
    { Public declarations }
    procedure DefineCores;
    procedure ReloadSkin;

  end;

var
  FmOverSeer_Dmk: TFmOverSeer_Dmk;
  SkinConta : Integer;

implementation

uses UnMyObjects, Principal, Module, UnLic_Dmk, VerificaConexoes,
  {$IFDEF DEFINE_VARLCT}ModuleFin, {$ENDIF}
  UMySQLModule;

{$R *.DFM}

procedure TFmOverSeer_Dmk.ControleMudou(Sender: TObject);
begin
  if APP_LIBERADO then
  begin
    MyObjects.ControleCor(Self);
    //M L A Geral.SistemaMetrico;
    //M L A Geral.CustoSincronizado;
  end;
end;

procedure TFmOverSeer_Dmk.FormCreate(Sender: TObject);
var
  WinPath : Array[0..144] of Char;
  Cam: String;
begin
  Cam := Application.Title+'\Opcoes';
  VAR_CAMINHOTXTBMP := 'C:\Dermatek\Skins\VCLSkin\mxskin33.skn';
  MyLinguas.GetInternationalStrings(myliPortuguesBR);
  TMeuDB := 'overseer';
  Geral.WriteAppKeyCU('Cashier_Database', 'Dermatek', TMeuDB, ktString);
{$IfNDef SemDBLocal}
  TLocDB := 'locoverseer';
  Geral.WriteAppKeyCU('CashLoc_Database', 'Dermatek', TLocDB, ktString);
{$EndIf}
  UMyMod.DefineDatabase(TMeuDB, Application.ExeName);
  //
  VAR_SQLUSER := 'root';
  DefineCores;
  Screen.OnActiveControlChange := ControleMudou;
  IC2_LOpcoesTeclaEnter := Geral.ReadAppKey('TeclaEnter', Application.Title, ktInteger,-1, HKEY_LOCAL_MACHINE);
  if IC2_LOpcoesTeclaEnter = -1 then
  begin
    Geral.WriteAppKey('TeclaEnter', Application.Title, 13, ktInteger, HKEY_LOCAL_MACHINE);
    IC2_LOpcoesTeclaEnter := 13;
  end;
  IC2_LOpcoesTeclaTab := Geral.ReadAppKey('TeclaTAB', Application.Title, ktInteger,-1, HKEY_LOCAL_MACHINE);
  if IC2_LOpcoesTeclaTab = -1 then
  begin
    Geral.WriteAppKey('TeclaTAB', Application.Title, 9, ktInteger, HKEY_LOCAL_MACHINE);
    IC2_LOpcoesTeclaTab := 9;
  end;
  GetWindowsDirectory(WinPath,144);
  ReloadSkin;
  IC2_COMPANT := nil;
  VAR_BD := 0;
end;

procedure TFmOverSeer_Dmk.Close1Click(Sender: TObject);
begin
  Close;
end;

procedure TFmOverSeer_Dmk.EdSenhaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_RETURN) then
  GOTOy.AnaliseSenhaEmpresa(EdLogin, EdSenha, EdEmpresa,
  FmOverSeer_Dmk, FmPrincipal, BtEntra,
  False(*ForcaSenha*), LaAviso1, LaAviso2, nil, nil);
  if (Key = VK_ESCAPE) then Close;
end;

procedure TFmOverSeer_Dmk.EdLoginKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_RETURN) then
  GOTOy.AnaliseSenhaEmpresa(EdLogin, EdSenha, EdEmpresa,
  FmOverSeer_Dmk, FmPrincipal, BtEntra,
  False(*ForcaSenha*), LaAviso1, LaAviso2, nil, nil);
  if (Key = VK_ESCAPE) then Close;
end;

procedure TFmOverSeer_Dmk.DefineCores;
//var
  //Aparencia: Integer;
begin
  IC2_AparenciasTituloPainelItem   := clBtnFace;
  IC2_AparenciasTituloPainelTexto  := clWindowText;
  IC2_AparenciasDadosPainel        := clBtnFace;
  IC2_AparenciasDadosTexto         := clWindowText;
  IC2_AparenciasGradeAItem         := clWindow;
  IC2_AparenciasGradeATitulo       := clWindowText;
  IC2_AparenciasGradeATexto        := clWindowText;
  IC2_AparenciasGradeAGrade        := clBtnFace;
  IC2_AparenciasGradeBItem         := clWindow;
  IC2_AparenciasGradeBTitulo       := clWindowText;
  IC2_AparenciasGradeBTexto        := clWindowText;
  IC2_AparenciasGradeBGrade        := clBtnFace;
  IC2_AparenciasEditAItem          := clWindow;
  IC2_AparenciasEditATexto         := clWindowText;
  IC2_AparenciasEditBItem          := clBtnFace;
  IC2_AparenciasEditBTexto         := clWindowText;
  IC2_AparenciasConfirmaPainelItem := clBtnFace;
  IC2_AparenciasControlePainelItem := clBtnFace;
  IC2_AparenciasFormPesquisa       := clBtnFace;
  IC2_AparenciasLabelPesquisa      := clWindowText;
  IC2_AparenciasLabelDigite        := clWindowText;
  IC2_AparenciasEditPesquisaItem   := clWindow;
  IC2_AparenciasEditPesquisaTexto  := clWindowText;
  IC2_AparenciasEditItemIn         := $00FF8000;
  IC2_AparenciasEditItemOut        := clWindow;
  IC2_AparenciasEditTextIn         := $00D5FAFF;
  IC2_AparenciasEditTextOut        := clWindowText;
(*  if Geral.ReadAppKey('Versao', Application.Title, ktInteger, 0, HKEY_LOCAL_MACHINE) <> 0 then
  begin
    Aparencia := Geral.IMV(Geral.ReadAppKey('Aparencia',
    Application.Title, ktString, '-1000', HKEY_LOCAL_MACHINE));
    if Aparencia = -1000 then
       Geral.WriteAppKey('Aparencia', Application.Title, '0', ktString,
    HKEY_LOCAL_MACHINE);
    QrAparencias.Close;
    QrAparencias.Params[0].AsInteger := Aparencia;
    UnDmkDAC_PF.AbreQuery(QrAparencias, ?
    if GOTOy.Registros(QrAparencias)>0 then
    begin
      IC2_AparenciasTituloPainelItem := QrAparenciasTituloPainelItem.Value;
      IC2_AparenciasTituloPainelTexto := QrAparenciasTituloPainelTexto.Value;
      IC2_AparenciasDadosPainel := QrAparenciasDadosPainel.Value;
      IC2_AparenciasDadosTexto := QrAparenciasDadosTexto.Value;
      IC2_AparenciasGradeAItem := QrAparenciasGradeAItem.Value;
      IC2_AparenciasGradeATitulo := QrAparenciasGradeATitulo.Value;
      IC2_AparenciasGradeATexto := QrAparenciasGradeATexto.Value;
      IC2_AparenciasGradeAGrade := QrAparenciasGradeAGrade.Value;
      IC2_AparenciasGradeBItem := QrAparenciasGradeBItem.Value;
      IC2_AparenciasGradeBTitulo := QrAparenciasGradeBTitulo.Value;
      IC2_AparenciasGradeBTexto := QrAparenciasGradeBTexto.Value;
      IC2_AparenciasGradeBGrade := QrAparenciasGradeBGrade.Value;
      IC2_AparenciasEditAItem := QrAparenciasEditAItem.Value;
      IC2_AparenciasEditATexto := QrAparenciasEditATexto.Value;
      IC2_AparenciasEditBItem := QrAparenciasEditBItem.Value;
      IC2_AparenciasEditBTexto := QrAparenciasEditBTexto.Value;
      IC2_AparenciasConfirmaPainelItem := QrAparenciasConfirmaPainelItem.Value;
      IC2_AparenciasControlePainelItem := QrAparenciasControlePainelItem.Value;
      IC2_AparenciasFormPesquisa := QrAparenciasFormPesquisa.Value;
      IC2_AparenciasLabelPesquisa := QrAparenciasLabelPesquisa.Value;
      IC2_AparenciasLabelDigite := QrAparenciasLabelDigite.Value;
      IC2_AparenciasEditPesquisaItem := QrAparenciasEditPesquisaItem.Value;
      IC2_AparenciasEditPesquisaTexto := QrAparenciasEditPesquisaTexto.Value;
      IC2_AparenciasEditItemIn         := $00D9FFFF;
      IC2_AparenciasEditItemOut        := QrAparenciasEditAItem.Value;
      IC2_AparenciasEditTextIn         := $00A00000;
      IC2_AparenciasEditTextOut        := QrAparenciasEditATexto.Value;
      IC2_AparenciasEditItemIn         := QrAparenciasEditATexto.Value;
      IC2_AparenciasEditItemOut        := QrAparenciasEditAItem.Value;
      IC2_AparenciasEditTextIn         := QrAparenciasEditAItem.Value;
      IC2_AparenciasEditTextOut        := QrAparenciasEditATexto.Value;
    QrAparencias.Close;
  end;*)
end;

procedure TFmOverSeer_Dmk.ReloadSkin;
begin
end;

procedure TFmOverSeer_Dmk.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  FmPrincipal.Close;
end;

procedure TFmOverSeer_Dmk.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOverSeer_Dmk.FormHide(Sender: TObject);
begin
  FmPrincipal.BringToFront;
  VAR_SHOW_MASTER_POPUP := True;
  FmPrincipal.AcoesIniciaisDoAplicativo();
end;

procedure TFmOverSeer_Dmk.BtSairClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOverSeer_Dmk.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOverSeer_Dmk.BtEntraClick(Sender: TObject);
begin
  GOTOy.AnaliseSenhaEmpresa(EdLogin, EdSenha, EdEmpresa,
    FmOverSeer_Dmk, FmPrincipal, BtEntra,
    False(*ForcaSenha*), LaAviso1, LaAviso2, nil, nil);
end;

procedure TFmOverSeer_Dmk.BtInstallVisible;
begin
end;

procedure TFmOverSeer_Dmk.EdSenhaExit(Sender: TObject);
begin
  BtInstallVisible;
end;

procedure TFmOverSeer_Dmk.FormShow(Sender: TObject);
begin
  UnLic_Dmk.FLaAvisa1 := LaAviso1;
  UnLic_Dmk.FLaAvisa2 := LaAviso2;
  VAR_SHOW_MASTER_POPUP := False;
end;

procedure TFmOverSeer_Dmk.LaConexaoClick(Sender: TObject);
begin
  Application.CreateForm(TFmVerificaConexoes,  FmVerificaConexoes);
  FmVerificaConexoes.ShowModal;
  FmVerificaConexoes.Destroy;
end;

procedure TFmOverSeer_Dmk.LaConexaoMouseEnter(Sender: TObject);
begin
  LaConexao.Font.Color := clBlue;
  LaConexao.Font.Style := [fsUnderline];
end;

procedure TFmOverSeer_Dmk.LaConexaoMouseLeave(Sender: TObject);
begin
  LaConexao.Font.Color := clNavy;
  LaConexao.Font.Style := [];
end;

procedure TFmOverSeer_Dmk.LaSenhasClick(Sender: TObject);
begin
  //Geral.GerenciarSenhas;
end;

procedure TFmOverSeer_Dmk.LaSenhasMouseEnter(Sender: TObject);
begin
  LaSenhas.Font.Color := clBlue;
  LaSenhas.Font.Style := [fsUnderline];
end;

procedure TFmOverSeer_Dmk.LaSenhasMouseLeave(Sender: TObject);
begin
  LaSenhas.Font.Color := clNavy;
  LaSenhas.Font.Style := [];
end;

end.

