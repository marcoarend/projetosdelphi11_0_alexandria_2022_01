unit OpcoesApp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, ComCtrls, ExtDlgs, DBCtrls, Db,
  mySQLDbTables, dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkRadioGroup,
  dmkGeral, UnDmkProcFunc, dmkImage, UnDmkEnums, dmkCheckBox,
  UnGrl_Consts, dmkCheckGroup, Vcl.Menus;

type
  TFmOpcoesApp = class(TForm)
    Panel1: TPanel;
    OpenPictureDialog1: TOpenPictureDialog;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel8: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel9: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    PCGeral: TPageControl;
    TabSheet2: TTabSheet;
    Panel6: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    SpeedButton2: TSpeedButton;
    EdLoadCSVOthIP: TEdit;
    EdLoadCSVOthDir: TEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
  private
    { Private declarations }
    //
  public
    { Public declarations }
  end;

  var
  FmOpcoesApp: TFmOpcoesApp;

implementation

uses UnMyObjects, Module, Principal, UMySQLModule, UnInternalConsts, MyDBCheck,
  DmkDAC_PF;

{$R *.DFM}

procedure TFmOpcoesApp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOpcoesApp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOpcoesApp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOpcoesApp.BtOKClick(Sender: TObject);
var
  //Codigo: Integer;
  LoadCSVOthIP, LoadCSVOthDir: String;
begin
  LoadCSVOthIP     := EdLoadCSVOthIP.Text;
  LoadCSVOthDir    := EdLoadCSVOthDir.Text;
  //
   UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'opcoesapp', False, [
    'LoadCSVOthIP', 'LoadCSVOthDir'
    ], ['Codigo'], [
    LoadCSVOthIP, LoadCSVOthDir
    ], [1], True);
  //
  UnDmkDAC_PF.AbreQuery(Dmod.QrOpcoesApp, Dmod.MyDB);
  Close;
end;

procedure TFmOpcoesApp.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  Dmod.ReopenOpcoesApp();
  EdLoadCSVOthIP.Text     := Dmod.QrOpcoesAppLoadCSVOthIP.Value;
  EdLoadCSVOthDir.Text    := Dmod.QrOpcoesAppLoadCSVOthDir.Value;
  //
end;

procedure TFmOpcoesApp.SpeedButton2Click(Sender: TObject);
begin
  OpenPictureDialog1.InitialDir := ExtractFilePath(EdLoadCSVOthDir.Text);
  if OpenPictureDialog1.Execute then
    EdLoadCSVOthDir.Text := OpenPictureDialog1.FileName;
end;

end.
