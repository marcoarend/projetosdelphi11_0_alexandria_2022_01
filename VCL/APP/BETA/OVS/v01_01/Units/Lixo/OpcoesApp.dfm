object FmOpcoesApp: TFmOpcoesApp
  Left = 339
  Top = 185
  Caption = 'APP-OPCAO-000 :: Op'#231#245'es Espec'#237'ficas do Sistema'
  ClientHeight = 657
  ClientWidth = 767
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 767
    Height = 501
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 792
    object PCGeral: TPageControl
      Left = 0
      Top = 0
      Width = 767
      Height = 501
      ActivePage = TabSheet2
      Align = alClient
      TabOrder = 0
      ExplicitWidth = 954
      object TabSheet2: TTabSheet
        Caption = 'Miscel'#226'nea'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 759
          Height = 473
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          ExplicitWidth = 784
          object Label1: TLabel
            Left = 4
            Top = 4
            Width = 80
            Height = 13
            Caption = 'IP arquivos CSV:'
          end
          object Label2: TLabel
            Left = 188
            Top = 4
            Width = 175
            Height = 13
            Caption = 'Caminho da pasta dos arquivos CSV:'
          end
          object SpeedButton2: TSpeedButton
            Left = 728
            Top = 20
            Width = 23
            Height = 22
            OnClick = SpeedButton2Click
          end
          object EdLoadCSVOthIP: TEdit
            Left = 4
            Top = 20
            Width = 181
            Height = 21
            TabOrder = 0
          end
          object EdLoadCSVOthDir: TEdit
            Left = 188
            Top = 20
            Width = 537
            Height = 21
            TabOrder = 1
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 767
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 792
    object GB_R: TGroupBox
      Left = 719
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 744
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 671
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 696
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 384
        Height = 32
        Caption = 'Op'#231#245'es Espec'#237'ficas do Sistema'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 384
        Height = 32
        Caption = 'Op'#231#245'es Espec'#237'ficas do Sistema'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 384
        Height = 32
        Caption = 'Op'#231#245'es Espec'#237'ficas do Sistema'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 549
    Width = 767
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitWidth = 792
    object Panel8: TPanel
      Left = 2
      Top = 15
      Width = 763
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 788
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 593
    Width = 767
    Height = 64
    Align = alBottom
    TabOrder = 3
    ExplicitWidth = 792
    object Panel9: TPanel
      Left = 2
      Top = 15
      Width = 763
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 788
      object PnSaiDesis: TPanel
        Left = 619
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        ExplicitLeft = 644
        object BtSaida: TBitBtn
          Tag = 15
          Left = 6
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 14
        Top = 3
        Width = 119
        Height = 40
        Caption = '&Salva'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object OpenPictureDialog1: TOpenPictureDialog
    Filter = 
      'All (*.gif;*.png;*.jpg;*.jpeg;*.bmp;*.tif;*.tiff;*.ico;*.emf;*.w' +
      'mf)|*.gif;*.png;*.jpg;*.jpeg;*.bmp;*.tif;*.tiff;*.ico;*.emf;*.wm' +
      'f|GIF Image (*.gif)|*.gif|Portable Network Graphics (*.png)|*.pn' +
      'g|JPEG Image File (*.jpg)|*.jpg|JPEG Image File (*.jpeg)|*.jpeg|' +
      'Bitmaps (*.bmp)|*.bmp|TIFF Images (*.tif)|*.tif|TIFF Images (*.t' +
      'iff)|*.tiff|Icons (*.ico)|*.ico|Enhanced Metafiles (*.emf)|*.emf' +
      '|Metafiles (*.wmf)|*.wmf'
    Left = 681
    Top = 17
  end
end
