object Dmod: TDmod
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 530
  Width = 785
  object MyDB: TMySQLDatabase
    ConnectOptions = [coCompress]
    KeepConnection = False
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    DatasetOptions = []
    Left = 52
    Top = 16
  end
  object QrUpd: TMySQLQuery
    Database = MyDB
    Left = 52
    Top = 68
  end
  object QrAux: TMySQLQuery
    Database = MyDB
    Left = 52
    Top = 120
  end
  object QrMas: TMySQLQuery
    Database = MyDB
    Left = 148
    Top = 320
  end
  object QrSQL: TMySQLQuery
    Database = MyDB
    Left = 120
    Top = 364
  end
  object QrIdx: TMySQLQuery
    Database = MyDB
    Left = 120
    Top = 320
  end
  object QrNTV: TMySQLQuery
    Database = MyDB
    Left = 160
    Top = 52
  end
  object MyDBn: TMySQLDatabase
    ConnectOptions = [coCompress]
    KeepConnection = False
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    DatasetOptions = []
    Left = 400
    Top = 8
  end
  object QrControle: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM Controle')
    Left = 44
    Top = 288
    object QrControleSoMaiusculas: TStringField
      FieldName = 'SoMaiusculas'
      Origin = 'controle.SoMaiusculas'
      Size = 1
    end
    object QrControlePaperLef: TIntegerField
      FieldName = 'PaperLef'
      Origin = 'controle.PaperLef'
    end
    object QrControlePaperTop: TIntegerField
      FieldName = 'PaperTop'
      Origin = 'controle.PaperTop'
    end
    object QrControlePaperHei: TIntegerField
      FieldName = 'PaperHei'
      Origin = 'controle.PaperHei'
    end
    object QrControlePaperWid: TIntegerField
      FieldName = 'PaperWid'
      Origin = 'controle.PaperWid'
    end
    object QrControlePaperFcl: TIntegerField
      FieldName = 'PaperFcl'
      Origin = 'controle.PaperFcl'
    end
    object QrControleMoeda: TStringField
      FieldName = 'Moeda'
      Size = 4
    end
    object QrControleCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrControleVersao: TIntegerField
      FieldName = 'Versao'
    end
    object QrControleDono: TIntegerField
      FieldName = 'Dono'
    end
    object QrControleUFPadrao: TIntegerField
      FieldName = 'UFPadrao'
    end
    object QrControleTravaCidade: TSmallintField
      FieldName = 'TravaCidade'
    end
    object QrControleCNPJ: TStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrControleCidade: TStringField
      FieldName = 'Cidade'
      Size = 100
    end
    object QrControleVerBcoTabs: TIntegerField
      FieldName = 'VerBcoTabs'
    end
  end
  object QrNTI: TMySQLQuery
    Database = MyDBn
    Left = 160
    Top = 100
  end
  object QrPriorNext: TMySQLQuery
    Database = MyDB
    Left = 148
    Top = 364
  end
  object QrAgora: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT YEAR(NOW()) ANO, MONTH(NOW()) MES,'
      'DAYOFMONTH(NOW()) DIA,'
      'HOUR(NOW()) HORA, MINUTE(NOW()) MINUTO,'
      'SECOND(NOW()) SEGUNDO, NOW() AGORA')
    Left = 48
    Top = 168
    object QrAgoraANO: TLargeintField
      FieldName = 'ANO'
    end
    object QrAgoraMES: TLargeintField
      FieldName = 'MES'
    end
    object QrAgoraDIA: TLargeintField
      FieldName = 'DIA'
    end
    object QrAgoraHORA: TLargeintField
      FieldName = 'HORA'
    end
    object QrAgoraMINUTO: TLargeintField
      FieldName = 'MINUTO'
    end
    object QrAgoraSEGUNDO: TLargeintField
      FieldName = 'SEGUNDO'
    end
    object QrAgoraAGORA: TDateTimeField
      FieldName = 'AGORA'
      Required = True
    end
  end
  object QrTerminal: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT * FROM Terminais'
      'WHERE IP=:P0')
    Left = 516
    Top = 160
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTerminalIP: TStringField
      FieldName = 'IP'
      Size = 15
    end
    object QrTerminalTerminal: TIntegerField
      FieldName = 'Terminal'
    end
  end
  object QrUpdU: TMySQLQuery
    Database = MyDB
    Left = 104
    Top = 68
  end
  object MyLocDatabase: TMySQLDatabase
    Host = '127.0.0.1'
    ConnectOptions = []
    KeepConnection = False
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'Host=127.0.0.1')
    DatasetOptions = []
    Left = 264
    Top = 7
  end
  object ZZDB: TMySQLDatabase
    Host = '127.0.0.1'
    ConnectOptions = []
    KeepConnection = False
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'Host=127.0.0.1')
    DatasetOptions = []
    Left = 96
    Top = 16
  end
  object QrUpdM: TMySQLQuery
    Database = MyDB
    Left = 108
    Top = 116
  end
  object QrAuxL: TMySQLQuery
    Database = MyDB
    Left = 264
    Top = 52
  end
  object QrMaster: TMySQLQuery
    Database = ZZDB
    AfterOpen = QrMasterAfterOpen
    OnCalcFields = QrMasterCalcFields
    SQL.Strings = (
      'SELECT ma.Em, te.Tipo, te.Logo,  te.Logo2,cm.Dono, cm.Versao, '
      'cm.CNPJ, te.IE, te.ECidade, uf.Nome NOMEUF, te.EFax,'
      
        'te.ERua, (te.ENumero+0.000) ENumero, te.EBairro, te.ECEP, te.ECo' +
        'mpl,'
      'te.EContato, te.ECel, te.ETe1, te.ETe2, te.ETe3, te.EPais,'
      'te.Respons1, te.Respons2, ma.Limite, ma.SolicitaSenha,'
      'ma.UsaAccMngr'
      'FROM Entidades te, Controle cm, Ufs uf, Master ma'
      'WHERE te.Codigo=cm.Dono'
      
        'AND ((te.CNPJ=cm.CNPJ AND te.Tipo=0) OR (te.CPF=cm.CNPJ AND te.T' +
        'ipo=1))'
      'AND uf.Codigo=te.EUF'
      '')
    Left = 248
    Top = 172
    object QrMasterCNPJ_TXT: TStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrMasterTE1_TXT: TStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 30
      Calculated = True
    end
    object QrMasterCEP_TXT: TStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Size = 15
      Calculated = True
    end
    object QrMasterEm: TStringField
      FieldName = 'Em'
      Origin = 'master.Em'
      Required = True
      Size = 100
    end
    object QrMasterTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'entidades.Tipo'
    end
    object QrMasterLogo: TBlobField
      FieldName = 'Logo'
      Origin = 'entidades.Logo'
      Size = 4
    end
    object QrMasterDono: TIntegerField
      FieldName = 'Dono'
      Origin = 'controle.Dono'
      Required = True
    end
    object QrMasterVersao: TIntegerField
      FieldName = 'Versao'
      Origin = 'controle.Versao'
      Required = True
    end
    object QrMasterCNPJ: TStringField
      FieldName = 'CNPJ'
      Origin = 'controle.CNPJ'
      Required = True
      Size = 18
    end
    object QrMasterIE: TStringField
      FieldName = 'IE'
      Origin = 'entidades.IE'
      Size = 15
    end
    object QrMasterECidade: TStringField
      FieldName = 'ECidade'
      Origin = 'entidades.ECidade'
      Size = 15
    end
    object QrMasterNOMEUF: TStringField
      FieldName = 'NOMEUF'
      Origin = 'ufs.Nome'
      Required = True
      Size = 2
    end
    object QrMasterEFax: TStringField
      FieldName = 'EFax'
      Origin = 'entidades.EFax'
    end
    object QrMasterERua: TStringField
      FieldName = 'ERua'
      Origin = 'entidades.ERua'
      Size = 30
    end
    object QrMasterEBairro: TStringField
      FieldName = 'EBairro'
      Origin = 'entidades.EBairro'
      Size = 30
    end
    object QrMasterECompl: TStringField
      FieldName = 'ECompl'
      Origin = 'entidades.ECompl'
      Size = 30
    end
    object QrMasterEContato: TStringField
      FieldName = 'EContato'
      Origin = 'entidades.EContato'
      Size = 60
    end
    object QrMasterECel: TStringField
      FieldName = 'ECel'
      Origin = 'entidades.ECel'
    end
    object QrMasterETe1: TStringField
      FieldName = 'ETe1'
      Origin = 'entidades.ETe1'
    end
    object QrMasterETe2: TStringField
      FieldName = 'ETe2'
      Origin = 'entidades.Ete2'
    end
    object QrMasterETe3: TStringField
      FieldName = 'ETe3'
      Origin = 'entidades.Ete3'
    end
    object QrMasterEPais: TStringField
      FieldName = 'EPais'
      Origin = 'entidades.EPais'
    end
    object QrMasterRespons1: TStringField
      FieldName = 'Respons1'
      Origin = 'entidades.Respons1'
      Required = True
      Size = 60
    end
    object QrMasterRespons2: TStringField
      FieldName = 'Respons2'
      Origin = 'entidades.Respons2'
      Required = True
      Size = 60
    end
    object QrMasterECEP: TIntegerField
      FieldName = 'ECEP'
      Origin = 'entidades.ECEP'
    end
    object QrMasterLogo2: TBlobField
      FieldName = 'Logo2'
      Origin = 'entidades.Logo2'
      Size = 4
    end
    object QrMasterLimite: TSmallintField
      FieldName = 'Limite'
      Origin = 'master.Limite'
      Required = True
    end
    object QrMasterSolicitaSenha: TIntegerField
      FieldName = 'SolicitaSenha'
      Origin = 'master.SolicitaSenha'
    end
    object QrMasterENumero: TFloatField
      FieldName = 'ENumero'
    end
    object QrMasterUsaAccMngr: TSmallintField
      FieldName = 'UsaAccMngr'
    end
  end
  object QrOpcoesApp: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT * FROM opcoesapp'
      'WHERE Codigo=1')
    Left = 316
    Top = 328
    object QrOpcoesAppLoadCSVOthIP: TStringField
      FieldName = 'LoadCSVOthIP'
      Size = 255
    end
    object QrOpcoesAppLoadCSVOthDir: TStringField
      FieldName = 'LoadCSVOthDir'
      Size = 255
    end
  end
end
