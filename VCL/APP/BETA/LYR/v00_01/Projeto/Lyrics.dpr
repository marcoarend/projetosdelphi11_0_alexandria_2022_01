program Lyrics;

uses
  Vcl.Forms,
  Principal in '..\Units\Principal.pas' {FmPrincipal},
  uOnScreenDisplay in '..\..\..\..\..\UTL\Lyrics\uOnScreenDisplay.pas',
  dmkGeral in '..\..\..\..\..\..\dmkComp\dmkGeral.pas',
  UnDmkEnums in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnDmkEnums.pas',
  UnDmkProcFunc in '..\..\..\..\..\UTL\_UNT\v01_01\UnDmkProcFunc.pas',
  SelMusica in '..\Units\SelMusica.pas' {FmSelMusica},
  UnMyObjects in '..\..\..\..\..\UTL\_UNT\v01_01\UnMyObjects.pas',
  dmkPopOutFntCBox in '..\..\..\..\..\..\dmkComp\dmkFontCB\dmkPopOutFntCBox.pas',
  UnApp_PF in '..\Units\UnApp_PF.pas',
  EditEstrofe in '..\Units\EditEstrofe.pas' {FmEditEstrofe},
  dmkEdit in '..\..\..\..\..\..\dmkComp\dmkEdit.pas',
  UnInternalConsts in '..\..\..\..\..\UTL\_UNT\v01_01\UnInternalConsts.pas',
  UnMyLinguas in '..\..\..\..\..\..\MultiOS\AllOS\Listas\UnMyLinguas.pas',
  Lyrics_v001 in '..\Schemas\Lyrics_v001.pas',
  EditMusica in '..\Units\EditMusica.pas' {FmEditMusica},
  EditSom in '..\Units\EditSom.pas' {FmEditSom},
  UnArr_PF in '..\Units\UnArr_PF.pas',
  SelPaleta in '..\Units\SelPaleta.pas' {FmSelPaleta},
  Sound in '..\Units\Sound.pas' {FmSound},
  _midi in '..\Units\_midi.pas',
  CatchSound in '..\Units\CatchSound.pas' {FmCatchSound},
  ZCF2 in '..\..\..\..\..\UTL\_UNT\v01_01\ZCF2.pas',
  ResIntStrings in '..\..\..\..\..\UTL\_UNT\v01_01\ResIntStrings.pas',
  UnDmkListas in '..\..\..\..\..\..\MultiOS\AllOS\Listas\UnDmkListas.pas',
  UnProjGroup_Consts in '..\Units\UnProjGroup_Consts.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFmPrincipal, FmPrincipal);
  Application.CreateForm(TFmSelMusica, FmSelMusica);
  Application.CreateForm(TFmEditEstrofe, FmEditEstrofe);
  Application.CreateForm(TFmEditMusica, FmEditMusica);
  Application.CreateForm(TFmEditSom, FmEditSom);
  Application.CreateForm(TFmSelPaleta, FmSelPaleta);
  Application.CreateForm(TFmSound, FmSound);
  Application.CreateForm(TFmCatchSound, FmCatchSound);
  Application.Run;
end.
