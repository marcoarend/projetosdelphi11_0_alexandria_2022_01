unit UnArr_PF;
(*
Move() to Insert/Delete item(s) from a dynamic array of string

up vote
5
down vote
favorite
4
Using System.Move() to insert/delete item(s) from an array of string is not as easy as insert/delete it from other array of simple data types. The problem is ... string is reference counted in Delphi. Using Move() on reference-counted data types needs deeper knowledge on internal compiler behaviour.

Can someone here explain the needed steps for me to achieve that, or better with some snippet codes, or direct me to a good reference on the internet?

Oh, Please don't tell me to use the "lazy-but-slow way", that is, for loop, I know that.

delphi pascal
shareimprove this question
asked Sep 16 '10 at 20:50

Phantom
68126
add a comment
7 Answers
active oldest votes
up vote
10
down vote
accepted
I've demonstrated how to delete items from a dynamic array before:

Delphi Q&A: How do I delete an element from an array?
In that article, I start with the following code:
*)

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics,
  Controls, Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db, DbCtrls,
  ComCtrls, UnInternalConsts2, dmkGeral, dmkEditCB, mySQLDBTables, UnDmkEnums,
  Variants;

type
  TInteiros = array of Integer;
  TSom = ^TCamposSom;
  TCamposSom = record
    Musica:      Integer;
    Estrofe:     Integer;
    Linha:       Integer;
    //
    Codigo:      Integer;
    Coluna:      Integer;
    SeqInLin:    Integer;
    Texto:       String;
    TextoSom:    String;
    TextoImp:    String;
    CorGray:     TColor;
    CorPrev:     TColor;
    CorShow:     TColor;
    Tempo:       Integer;
    TempoIni:    Integer;
    TempoTam:    Integer;
  end;
  TSons = array of TSom;
  TLinha = ^TCamposLinha;
  TCamposLinha = record
    Musica:      Integer;
    Estrofe:     Integer;
    //
    CodLinha:    Integer;
    CodItens:    TInteiros;
    Sons:        TSons;
  end;
  TLinhas = array of TLinha;
  TEstrofe = ^TCamposEstrofe;
  TCamposEstrofe = record
    Musica:      Integer;
    //
    CodEst:      Integer;
    NoEst:       String;
    TempoShow:   TTime;
    TempoHide:   TTime;
    CodItens:    TInteiros;
    Linhas:      TLinhas;
  end;
  TEstrofes = array of TEstrofe;
  TMusica = ^TCamposMusica;
  TCamposMusica = record
    CodMus:       Integer;
    NomeMusica:   String[255];
    NomeHinario:  String[255];
    NoOriMusica:  String[255];
    NoOriArtista: String[255];
    NoOriMidia:   String[255];
    DataLetra:    TDateTime;
    EstrPorTela:  Integer;
    FontName:     String[255];
    FontSize:     Integer;
    LablAltu:     Integer;
    FontBold:     Boolean;
    CodItens:     TInteiros;
    Estrofes:     TEstrofes;
  end;
  TMusicas = array of TMusica;
  //
  TUnArr_PF = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure DeleteEstrofe(var A: TEstrofes; const Index: Cardinal);
    procedure DeleteLinha(var A: TLinhas; const Index: Cardinal);
    procedure DeleteSom(var A: TSons; const Index: Cardinal);
    procedure InsertEstrofe(var A: TEstrofes; const Index: Cardinal; const Value: TEstrofe);
    procedure InsertLinha(var A: TLinhas; const Index: Cardinal; const Value: TLinha);
    procedure InsertSom(var A: TSons; const Index: Cardinal; const Value: TSom);
  end;
var
  FRSom: TSom;
  FRLinha: TLinha;
  FREstrofe: TEstrofe;
  FRMusica: TMusica;
  //
  Arr_PF: TUnArr_PF;

implementation

//uses

{ TUnArr_PF }

procedure TUnArr_PF.DeleteEstrofe(var A: TEstrofes; const Index: Cardinal);
var
  ALength: Cardinal;
  TailElements: Cardinal;
begin
  ALength := Length(A);
  Assert(ALength > 0);
  Assert(Index < ALength);
  Finalize(A[Index]);
  TailElements := ALength - Index;
  if TailElements > 0 then
    Move(A[Index + 1], A[Index], SizeOf(TEstrofes) * TailElements);
  Initialize(A[ALength - 1]);
  SetLength(A, ALength - 1);
end;

procedure TUnArr_PF.DeleteLinha(var A: TLinhas; const Index: Cardinal);
var
  ALength: Cardinal;
  TailElements: Cardinal;
begin
  ALength := Length(A);
  Assert(ALength > 0);
  Assert(Index < ALength);
  Finalize(A[Index]);
  TailElements := ALength - Index;
  if TailElements > 0 then
    Move(A[Index + 1], A[Index], SizeOf(TLinhas) * TailElements);
  Initialize(A[ALength - 1]);
  SetLength(A, ALength - 1);
end;

procedure TUnArr_PF.DeleteSom(var A: TSons; const Index: Cardinal);
{
procedure DeleteSom(var A: TSom; const Index: Cardinal);
var
  ALength: Cardinal;
  i: Cardinal;
begin
  ALength := Length(A);
  Assert(ALength > 0);
  Assert(Index < ALength);
  for i := Index + 1 to ALength - 1 do
    A[i - 1] := A[i];
  SetLength(A, ALength - 1);
end;
}
var
  ALength: Cardinal;
  TailElements: Cardinal;
begin
//You cannot go wrong with that code. Use whatever value for X you want; in
//your case, replace it with string. If you want to get fancier and use Move,
//then there's way to do that, too.

  ALength := Length(A);
  Assert(ALength > 0);
  Assert(Index < ALength);
  Finalize(A[Index]);
  TailElements := ALength - Index;
  if TailElements > 0 then
    Move(A[Index + 1], A[Index], SizeOf(TSons) * TailElements);
  Initialize(A[ALength - 1]);
  SetLength(A, ALength - 1);
end;


procedure TUnArr_PF.InsertEstrofe(var A: TEstrofes; const Index: Cardinal;
  const Value: TEstrofe);
var
  ALength: Cardinal;
  TailElements: Cardinal;
begin
  ALength := Length(A);
  Assert(Index <= ALength);
  SetLength(A, ALength + 1);
  Finalize(A[ALength]);
  TailElements := ALength - Index;
  if TailElements > 0 then //begin
    Move(A[Index], A[Index + 1], SizeOf(TSons) * TailElements);
  Initialize(A[Index]);
  A[Index] := Value;
end;

procedure TUnArr_PF.InsertLinha(var A: TLinhas; const Index: Cardinal;
  const Value: TLinha);
var
  ALength: Cardinal;
  TailElements: Cardinal;
begin
  ALength := Length(A);
  if ALength = 0 then
  begin
    SetLength(A, 1);
    A[0] := Value;
  end else
  begin
    Assert(Index <= ALength);
    SetLength(A, ALength + 1);
    Finalize(A[ALength]);
    TailElements := ALength - Index;
    if TailElements > 0 then //begin
      Move(A[Index], A[Index + 1], SizeOf(TSons) * TailElements);
    Initialize(A[Index]);
  end;
  A[Index] := Value;
end;

procedure TUnArr_PF.InsertSom(var A: TSons; const Index: Cardinal;
  const Value: TSom);
//Since X is string, the Finalize call is equivalent to assigning the empty
//string to that array element. I use Finalize in this code, though, because
//it will work for all array-element types, even types that include records,
//interfaces, strings, and other arrays.
// For inserting, you just shift things the opposite direction:
var
  ALength: Cardinal;
  TailElements: Cardinal;
begin
  ALength := Length(A);
  Assert(Index <= ALength);
  SetLength(A, ALength + 1);
  Finalize(A[ALength]);
  TailElements := ALength - Index;
  if TailElements > 0 then //begin
    Move(A[Index], A[Index + 1], SizeOf(TSons) * TailElements);
  Initialize(A[Index]);
  A[Index] := Value;
//Use Finalize when you're about to do something that's outside the bounds of the language, such as using the non-type-safe Move procedure to overwrite a variable of a compiler-managed type. Use Initialize when you're re-entering the defined part of the language. (The language defines what happens when an array grows or shrinks with SetLength, but it doesn't define how to copy or delete strings without using a string-assignment statement.)
end;

end.
