unit SelMusica;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.MPlayer,
  dmkGeral, Vcl.Buttons, Vcl.ComCtrls, (*dmkPopOutFntCBox,*) dmkEdit, UnApp_PF,
  UnProjGroup_Consts;

type
  TFmSelMusica = class(TForm)
    PnPrepara: TPanel;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    EdTitle: TEdit;
    EdArtist: TEdit;
    EdAlbum: TEdit;
    EdYear: TEdit;
    EdGenre: TEdit;
    EdComment: TEdit;
    Panel1: TPanel;
    mp3List: TListBox;
    StFolder: TStaticText;
    Panel2: TPanel;
    BtOK: TBitBtn;
    PnAviso: TPanel;
    PB1: TProgressBar;
    GroupBox2: TGroupBox;
    Panel3: TPanel;
    CBFontName: TdmkPopOutFntCBox;
    Label1: TLabel;
    Label8: TLabel;
    CkNegrito: TCheckBox;
    Label9: TLabel;
    EdFontHigh: TdmkEdit;
    CBFontSize: TComboBox;
    RGModoCarga: TRadioGroup;
    Label10: TLabel;
    procedure mp3ListClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FMp3File: String;
    procedure Carrega();
  end;

var
  FmSelMusica: TFmSelMusica;

implementation

uses Principal, UnDmkProcFunc, EditMusica;

{$R *.dfm}

procedure FillID3TagInformation(mp3File:string; Title,Artist,Album,Year,Genre,Comment:TEdit);
var //fMP3: file of Byte;
    ID3 : TID3Rec;
    fmp3: TFileStream;
begin
  fmp3:=TFileStream.Create(mp3File, fmOpenRead);
  try
    fmp3.position:=fmp3.size-128;
    fmp3.Read(ID3,SizeOf(ID3));
  finally
    fmp3.free;
  end;

 { or the non Stream approach - as in ChangeID3Tag procedure
 try
   AssignFile(fMP3, mp3File);
   Reset(fMP3);
   try
     Seek(fMP3, FileSize(fMP3) - 128);
     BlockRead(fMP3, ID3, SizeOf(ID3));
   finally
   end;
 finally
   CloseFile(fMP3);
 end;
 }

 if ID3.Tag = 'TAG' then
 begin
   Title.Text:='Wrong or no ID3 tag information';
   Artist.Text:='Wrong or no ID3 tag information';
   Album.Text:='Wrong or no ID3 tag information';
   Year.Text:='Wrong or no ID3 tag information';
   Genre.Text:='Wrong or no ID3 tag information';
   Comment.Text:='Wrong or no ID3 tag information';
 end else
 begin
   Title.Text:=ID3.Title;
   Artist.Text:=ID3.Artist;
   Album.Text:=ID3.Album;
   Year.Text:=ID3.Year;
   if ID3.Genre in [0..MaxID3Genre] then
     Genre.Text:=ID3Genre[ID3.Genre]
   else
     Genre.Text:=IntToStr(ID3.Genre);
   Comment.Text:=ID3.Comment
 end;
end;

procedure TFmSelMusica.BtOKClick(Sender: TObject);
begin
  Carrega();
end;

procedure TFmSelMusica.Carrega();
begin
  try
    PnAviso.Visible := True;
    PnPrepara.Visible := False;
    Application.ProcessMessages;
    //
    FmPrincipal.FFontName := CBFontName.FonteNome;
    FmPrincipal.FFontSize := Geral.IMV(CBFontSize.Text);
    FmPrincipal.FFontBold := CkNegrito.Checked;
    FmPrincipal.FFontHigh := EdFontHigh.ValueVariant;
    if FmPrincipal.FScrollBox <> nil then
      FmPrincipal.FScrollBox.Visible := False;
(*
    if RGModoCarga.ItemIndex = 0 then
    begin
      FmPrincipal.CarregaArquivoLyrico_Txt(FmPrincipal.FLyrFile, PB1);
      FmPrincipal.ScrollBox1.Visible := True;
    end;
*)
    FmPrincipal.Progress.Max := 0;

    FmPrincipal.mp3player.Close;
    FmPrincipal.mp3player.FileName:= FMp3File;
    FmPrincipal.mp3player.Open;
    FmPrincipal.mp3player.EnabledButtons := [TMPBtnType.btPlay];
    FmPrincipal.mp3player.Enabled := True;

    FmPrincipal.Progress.Max := FmPrincipal.mp3player.Length;

    if App_PF.CarregaMusica(dmkPF.MudaExtensaoDeArquivo(FmPrincipal.FLyrFile,
    'xml'), FmEditMusica.SGMusica, FmPrincipal.FMusicas) then
    begin
    if RGModoCarga.ItemIndex = 1 then
    begin
      FmEditMusica.BtOK.Enabled :=
        App_PF.CarregaPanels(FMusicaSel, FmPrincipal.FStep, FmPrincipal, FmPrincipal.FScrollBox,
          FmPrincipal.FPanels, FmPrincipal.FLabels, FmPrincipal.FMusicas,
          FmPrincipal.FLablCount, FmPrincipal.FMaxSom, FmPrincipal.FMaxTempo,
          FmPrincipal.FSeqI, FmPrincipal.FTempos,
          FmPrincipal.FCoresGray, FmPrincipal.FCoresPrev, FmPrincipal.FCoresShow);
        //FmEditMusica.CriaLabels();
        FmPrincipal.FScrollBox.Visible := True;
      end;
    end;
     FmPrincipal.StatusPlayer();
  finally
    PnPrepara.Visible := True;
    PnAviso.Visible := False;
    //
    Hide;
  end;
end;

procedure TFmSelMusica.FormCreate(Sender: TObject);
begin
  PnPrepara.Align := alClient;
  PnPrepara.Visible := True;
  PnAviso.Visible := False;
  PnAviso.Align := alClient;
  StFolder.Caption := VAR_PASTA_MAE;
  App_PF.FillMP3FileList(StFolder.Caption, mp3List.Items);
  //
  CBFontName.FonteNome := 'Tahoma';
end;

procedure TFmSelMusica.mp3ListClick(Sender: TObject);
var
  I: Integer;
  Txt, Musica: String;
begin
  if mp3List.Items.Count = 0 then
    Exit;
  if mp3List.ItemIndex = -1 then
    Exit;
  Musica := mp3List.Items.Strings[mp3List.ItemIndex];
  FMp3File := Concat(StFolder.Caption, '\', Musica, '\' + Musica + '.mp3');
  if not FileExists(FMp3File) then
  begin
    Geral.MB_Aviso('O arquivo MP3 ' + slineBreak + FMp3File + slineBreak +' n�o existe!');
    Exit;
  end;
  FmPrincipal.FLyrFile := dmkPF.MudaExtensaoDeArquivo(FMp3File, 'txt');
  if not FileExists(FmPrincipal.FLyrFile) then
  begin
    I := pos('.mp3vr', FMp3File);
    if I > 0 then
    begin
      Txt := Copy(FMp3File, 1, I);
      FmPrincipal.FLyrFile := Txt + 'txt';
      if not FileExists(FmPrincipal.FLyrFile) then
      begin
        Geral.MB_Aviso('O arquivo TXT ' + slineBreak + FmPrincipal.FLyrFile + slineBreak +' n�o existe!');
        Exit;
      end;
    end;
  end;

  FillID3TagInformation(FMp3File, edTitle, edArtist, edAlbum, edYear, edGenre, edComment);

  BtOK.Enabled := True;
end;

end.
