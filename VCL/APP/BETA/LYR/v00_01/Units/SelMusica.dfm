object FmSelMusica: TFmSelMusica
  Left = 0
  Top = 0
  Caption = 'FmSelMusica'
  ClientHeight = 510
  ClientWidth = 826
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PnPrepara: TPanel
    Left = 0
    Top = 17
    Width = 817
    Height = 292
    TabOrder = 0
    object GroupBox1: TGroupBox
      Left = 432
      Top = 1
      Width = 384
      Height = 290
      Align = alRight
      Caption = ' Informa'#231#245'es: '
      TabOrder = 0
      object Label2: TLabel
        Left = 8
        Top = 24
        Width = 30
        Height = 13
        Caption = 'T'#237'tulo:'
      end
      object Label3: TLabel
        Left = 8
        Top = 48
        Width = 36
        Height = 13
        Caption = 'Artista:'
      end
      object Label4: TLabel
        Left = 8
        Top = 72
        Width = 33
        Height = 13
        Caption = #193'lbum:'
      end
      object Label5: TLabel
        Left = 8
        Top = 96
        Width = 23
        Height = 13
        Caption = 'Ano:'
      end
      object Label6: TLabel
        Left = 8
        Top = 120
        Width = 39
        Height = 13
        Caption = 'G'#234'nero:'
      end
      object Label7: TLabel
        Left = 8
        Top = 144
        Width = 64
        Height = 13
        Caption = 'Coment'#225'rios:'
      end
      object EdTitle: TEdit
        Left = 76
        Top = 20
        Width = 177
        Height = 21
        TabOrder = 0
        Text = '...'
      end
      object EdArtist: TEdit
        Left = 76
        Top = 44
        Width = 177
        Height = 21
        TabOrder = 1
        Text = '...'
      end
      object EdAlbum: TEdit
        Left = 76
        Top = 68
        Width = 177
        Height = 21
        TabOrder = 2
        Text = '...'
      end
      object EdYear: TEdit
        Left = 76
        Top = 92
        Width = 177
        Height = 21
        TabOrder = 3
        Text = '...'
      end
      object EdGenre: TEdit
        Left = 76
        Top = 116
        Width = 177
        Height = 21
        TabOrder = 4
        Text = '...'
      end
      object EdComment: TEdit
        Left = 76
        Top = 140
        Width = 177
        Height = 21
        TabOrder = 5
        Text = '...'
      end
      object GroupBox2: TGroupBox
        Left = 8
        Top = 168
        Width = 341
        Height = 105
        Caption = ' Configura'#231#227'o do texto:'
        TabOrder = 6
        object Panel3: TPanel
          Left = 2
          Top = 15
          Width = 337
          Height = 88
          Align = alClient
          TabOrder = 0
          object Label1: TLabel
            Left = 8
            Top = 4
            Width = 77
            Height = 13
            Caption = 'Nome da Fonte:'
          end
          object Label8: TLabel
            Left = 8
            Top = 44
            Width = 92
            Height = 13
            Caption = 'Tamanho da fonte:'
          end
          object Label9: TLabel
            Left = 116
            Top = 44
            Width = 73
            Height = 13
            Caption = 'Altura da linha:'
          end
          object CBFontName: TdmkPopOutFntCBox
            Left = 8
            Top = 20
            Width = 321
            Height = 22
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            MoveUsedToTop = True
            PopUpHeight = 75
            FonteNome = 'MS Sans Serif'
            UpdType = utYes
          end
          object CkNegrito: TCheckBox
            Left = 260
            Top = 60
            Width = 65
            Height = 17
            Alignment = taLeftJustify
            Caption = 'Negrito'
            Checked = True
            State = cbChecked
            TabOrder = 1
          end
          object EdFontHigh: TdmkEdit
            Left = 116
            Top = 60
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '30'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 30
            ValWarn = False
          end
          object CBFontSize: TComboBox
            Left = 9
            Top = 60
            Width = 100
            Height = 21
            TabOrder = 3
            Text = '24'
            Items.Strings = (
              '5'
              '6'
              '7'
              '8'
              '9'
              '10'
              '11'
              '12'
              '14'
              '16'
              '18'
              '20'
              '22'
              '24'
              '26'
              '28'
              '36'
              '48'
              '72')
          end
        end
      end
      object RGModoCarga: TRadioGroup
        Left = 260
        Top = 28
        Width = 117
        Height = 105
        Caption = ' Modo de carga: '
        ItemIndex = 1
        Items.Strings = (
          'Labels in Scroll'
          'Labels in Panels')
        TabOrder = 7
      end
    end
    object Panel1: TPanel
      Left = 1
      Top = 1
      Width = 431
      Height = 290
      Align = alClient
      Caption = 'Panel1'
      TabOrder = 1
      object mp3List: TListBox
        Left = 1
        Top = 1
        Width = 429
        Height = 288
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ItemHeight = 13
        ParentFont = False
        TabOrder = 0
        OnClick = mp3ListClick
      end
    end
  end
  object StFolder: TStaticText
    Left = 0
    Top = 0
    Width = 826
    Height = 17
    Align = alTop
    Caption = '.........'
    TabOrder = 1
  end
  object Panel2: TPanel
    Left = 0
    Top = 475
    Width = 826
    Height = 35
    Align = alBottom
    TabOrder = 2
    object BtOK: TBitBtn
      Left = 352
      Top = 4
      Width = 75
      Height = 25
      Caption = '&OK'
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = BtOKClick
    end
  end
  object PnAviso: TPanel
    Left = 0
    Top = 342
    Width = 826
    Height = 133
    Align = alBottom
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -32
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    Visible = False
    object Label10: TLabel
      Left = 1
      Top = 1
      Width = 824
      Height = 39
      Align = alTop
      Alignment = taCenter
      Caption = 'Aguarde... Carregando m'#250'sica e texto....'
      ExplicitWidth = 581
    end
  end
  object PB1: TProgressBar
    Left = 0
    Top = 325
    Width = 826
    Height = 17
    Align = alBottom
    TabOrder = 4
  end
end
