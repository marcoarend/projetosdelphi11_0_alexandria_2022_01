unit UnApp_PF;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics,
  Controls, Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db, DbCtrls,
  ComCtrls, UnInternalConsts2, dmkGeral, dmkEditCB, mySQLDBTables, UnDmkEnums,
  Xml.xmldom, Xml.XMLIntf, Xml.Win.msxmldom, Xml.XMLDoc, Lyrics_v001,
  Grids, Variants, UnArr_PF;

type
  TPanels = array of TPanel;
  TLabels = array of TLabel;
  TUnApp_PF = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function CarregaMusica(Arquivo: String; Grade: TStringGrid; var Musicas:
             TMusicas): Boolean;
    function CarregaPanels(const Idx, Step: Integer; Form: TForm; ScrollBox:
             TScrollBox; var Panels: TPanels; var Labels: TLabels; var Musicas:
             TMusicas; var FLablCount, MaxSom, MaxTempo: Integer; var SeqI,
             Tempos, CoresGray, CoresPrev, CoresShow: TInteiros): Boolean;
    procedure FillMP3FileList(Folder: string; sl: TStrings);
    //function SalvaMusica(Sons: array of TSom; NomeArquivo: String): Boolean;
    function SalvaMusica2(PastaMae, PastaMusica, NomeMusica, NomeHinario, NoOriMusica,
             NoOriArtista, NoOriMidia: String; CodMusica: Integer; DataLetra: TDateTime;
             EstrPorTela: Integer; FontName: String; FontSize, LablAltu: Integer; FontBold:
             Boolean; Grade: TStringGrid): Boolean;
    function SalvaMusica3(SQLType: TSQLType; Musicas: TMusicas; nMusica: Integer;
             PastaMae, PastaMusica, NomeMusica, NomeHinario, NoOriMusica,
             NoOriArtista, NoOriMidia: String; CodMusica: Integer; DataLetra: TDateTime;
             EstrPorTela: Integer; FontName: String; FontSize, LablAltu: Integer; FontBold:
             Boolean; Grade: TStringGrid; FMusica: TMusica): Boolean;
    function SalvaXML(Dir, Arq, Texto: String): String;
  end;
var
  //
  App_PF: TUnApp_PF;

implementation

uses UnMyObjects, UnDmkProcFunc, EditMusica, Principal;

{ TUnApp_PF }

var
  arqXML: TXMLDocument;
  cXML: IXMLTLyrics;
  cEstrofes: IXMLEstrofes;
  cLinhas: IXMLLinhas;
  cSons: IXMLSons;

const
  verLyrics = '1.00';

function TUnApp_PF.CarregaMusica(Arquivo: String; Grade: TStringGrid; var Musicas:
  TMusicas): Boolean;
  procedure MsgNoh(Noh: String);
  begin
    Geral.MB_Aviso('Arquivo n�o possui n� "' + Noh + '"');
  end;
var
  //Texto: WideString;
  //xmlDocA, xmlDocB: IXMLDocument;
  xmlNodeA, xmlNodeB, xmlNodeC, xmlNodeD, xmlNodeE, xmlNodeF, xmlNodeG: IXMLNode;
  //xmlList: IXMLNodeList;
  //
  TV1: TTreeNode;
  j(*, N*): Integer;
  X: IXMLNode;
  XMLDocument1: IXMLDocument;
  Node, NodePlano, NodeConjunto, NodeGrupo, NodeSubgrupo, NodeConta: IXMLNode;
  TVNode: TTreeNode;
  i, k, l, m, Codigo, CtrlaSdo, TipoAgrupa, Plano, Conjunto, Grupo, SubGrupo,
  Conta, CentroRes, PagRec: Integer;
  NodeNome, Nome, Nome2, Nome3, Sigla, Debito, Credito, Mensal: String;
  Noh: String;
  //
  CodItem, LinGrd: Integer;
  CodMus, CodEst, CodLinha: Integer;
  //
  GerouSom, GerouLinha, GerouEstrofe, GerouMusica: Boolean;
  Sons: array of TSom;
  Linhas: array of TLinha;
  Estrofes: array of TEstrofe;
  N, P, Q: Integer;
  //
  AttrNode: IXMLNode;
  nMus, nEst, nLin, nSom: Integer;
  TesteI: TInteiros;
  Estrofez: TEstrofes;
  Linhaz: TLinhas;
  Sonz: TSons;
begin
  FmEditMusica.EdPastaMusica.Text := dmkPF.RemoveExtensaoDeArquivo(ExtractFilePath(Arquivo));
  nMus := 0;
  nEst := 0;
  nLin := 0;
  nSom := 0;
  XMLDocument1 := TXMLDocument.Create(nil);
  //XMLDocument1.LoadFromFile(Arquivo);
  //
  //ClearTree;
  XMLDocument1.FileName := Arquivo;
  XMLDocument1.Active := True;

  xmlNodeA := XMLDocument1.DocumentElement;
  Noh := 'Lyrics';
  if xmlNodeA.NodeName = Noh then
  begin
    xmlNodeB := XMLDocument1.DocumentElement.ChildNodes.First;
    while xmlNodeB <> nil do
    begin
      Noh := 'Musicas';
      if xmlNodeB.NodeName = Noh then
      begin
        FmEditMusica.EdCodMus.ValueVariant        := 0;
        FmEditMusica.EdNomeMusica.ValueVariant    := '[Sem Nome]';
        FmEditMusica.EdNomeHinario.ValueVariant   := '[Sem Nome]';
        FmEditMusica.EdNoOriMusica.ValueVariant   := '[Sem Nome]';
        FmEditMusica.EdNoOriArtista.ValueVariant  := '[Sem Nome]';
        FmEditMusica.EdNoOriMidia.ValueVariant    := '[Sem Nome]';
        FmEditMusica.TPDataLetra.Date             := Date;
        FmEditMusica.RGEstrPorTela.ItemIndex      := 2;
        FmEditMusica.CBFontName.FonteNome         := 'Tahoma';
        FmEditMusica.CBFontSize.Text              := '24';
        FmEditMusica.EdLablAltu.ValueVariant      := 30;
        FmEditMusica.CkNegrito.Checked            := True;
        //
        xmlNodeC := xmlNodeB.AttributeNodes.First;
        while xmlNodeC <> nil do
        begin
          if xmlNodeC.NodeName = 'CodMus' then
          begin
            GerouMusica := True;
            New(FRMusica);
            FRMusica.CodMus := dmkPF.XMLValInt(xmlNodeC);
            FmEditMusica.EdCodMus.ValueVariant := dmkPF.XMLValInt(xmlNodeC);
            FRMusica.NomeMusica   := '';
            FRMusica.NomeHinario  := '';
            FRMusica.NoOriMusica  := '';
            FRMusica.NoOriArtista := '';
            FRMusica.NoOriMidia   := '';
            FRMusica.DataLetra    := 0;
            FRMusica.EstrPorTela  := 0;
            FRMusica.FontName     := '';
            FRMusica.FontSize     := 0;
            FRMusica.LablAltu     := 0;
            FRMusica.FontBold     := True;
          end
          else
            MsgNoh('N� Attr n�o implementado: ' + xmlNodeC.NodeName);
          xmlNodeC := xmlNodeC.NextSibling;
        end;
        //
        xmlNodeC := xmlNodeB.ChildNodes.First;
        while xmlNodeC <> nil do
        begin
          if xmlNodeC.NodeName = 'NomeMusica' then
          begin
            FmEditMusica.EdNomeMusica.ValueVariant := dmkPF.XMLValTxt(xmlNodeC);
            FRMusica.NomeMusica := dmkPF.XMLValTxt(xmlNodeC);
          end else
          if xmlNodeC.NodeName = 'NomeHinario' then
          begin
            FmEditMusica.EdNomeHinario.ValueVariant := dmkPF.XMLValTxt(xmlNodeC);
            FRMusica.NomeHinario := dmkPF.XMLValTxt(xmlNodeC);
          end else
          if xmlNodeC.NodeName = 'NoOriMusica' then
          begin
            FmEditMusica.EdNoOriMusica.ValueVariant := dmkPF.XMLValTxt(xmlNodeC);
            FRMusica.NoOriMusica := dmkPF.XMLValTxt(xmlNodeC);
          end else
          if xmlNodeC.NodeName = 'NoOriArtista' then
          begin
            FmEditMusica.EdNoOriArtista.ValueVariant := dmkPF.XMLValTxt(xmlNodeC);
            FRMusica.NoOriArtista := dmkPF.XMLValTxt(xmlNodeC);
          end else
          if xmlNodeC.NodeName = 'NoOriMidia' then
          begin
            FmEditMusica.EdNoOriMidia.ValueVariant := dmkPF.XMLValTxt(xmlNodeC);
            FRMusica.NoOriMidia := dmkPF.XMLValTxt(xmlNodeC);
          end else
          if xmlNodeC.NodeName = 'DataLetra' then
          begin
            //FmEditMusica.TPDataLetra.Date := Geral.ValidaData8Num(Geral.SoNumero_TT(dmkPF.XMLValTxt(xmlNodeC)), True, False);
            FmEditMusica.TPDataLetra.Date := dmkPF.XMLValTDt(xmlNodeC);
            FRMusica.DataLetra := dmkPF.XMLValTDt(xmlNodeC);
          end else
          if xmlNodeC.NodeName = 'EstrPorTela' then
          begin
            FmEditMusica.RGEstrPorTela.ItemIndex := dmkPF.XMLValInt(xmlNodeC) - 1;
            FRMusica.EstrPorTela := dmkPF.XMLValInt(xmlNodeC) - 1;
          end else
          if xmlNodeC.NodeName = 'FontName' then
          begin
            FmEditMusica.CBFontName.FonteNome := dmkPF.XMLValTxt(xmlNodeC);
            FRMusica.FontName := dmkPF.XMLValTxt(xmlNodeC);
          end else
          if xmlNodeC.NodeName = 'FontSize' then
          begin
            FmEditMusica.CBFontSize.Text := Geral.FF0(dmkPF.XMLValInt(xmlNodeC));
            FRMusica.FontSize := dmkPF.XMLValInt(xmlNodeC);
          end else
          if xmlNodeC.NodeName = 'LablAltu' then
          begin
            FmEditMusica.EdLablAltu.ValueVariant := dmkPF.XMLValInt(xmlNodeC);
            FRMusica.LablAltu := dmkPF.XMLValInt(xmlNodeC);
          end else
          if xmlNodeC.NodeName = 'FontBold' then
          begin
            FmEditMusica.CkNegrito.Checked := dmkPF.XMLValBol(xmlNodeC);
            FRMusica.FontBold := dmkPF.XMLValBol(xmlNodeC);
          end else
          begin
            if xmlNodeC.NodeName = 'Estrofes' then
            begin
              xmlNodeD := xmlNodeC.AttributeNodes.First;
              while xmlNodeD <> nil do
              begin
                if xmlNodeD.NodeName = 'CodEst' then
                begin
                  GerouEstrofe := True;
                  New(FREstrofe);
                  FREstrofe.CodEst     := dmkPF.XMLValInt(xmlNodeD);
                  FREstrofe.Musica     := FRMusica.CodMus;
                  FREstrofe.NoEst      := '';
                  FREstrofe.TempoShow  := 0;
                  FREstrofe.TempoHide  := 0;
                  //
                  SetLength(Linhas, 0);
                end
                else
                  MsgNoh('N� Attr n�o implementado: ' + xmlNodeD.NodeName);
                xmlNodeD := xmlNodeD.NextSibling;
              end;
              //
              xmlNodeD := xmlNodeC.ChildNodes.First;
              while xmlNodeD <> nil do
              begin
                if xmlNodeD.NodeName = 'NoEst' then
                begin
                  FREstrofe.NoEst := dmkPF.XMLValTxt(xmlNodeD);
                end else
                if xmlNodeD.NodeName = 'TempoShow' then
                begin
                  FREstrofe.TempoShow := dmkPF.XMLValZzz(xmlNodeD);
                end else
                if xmlNodeD.NodeName = 'TempoHide' then
                begin
                  FREstrofe.TempoHide := dmkPF.XMLValZzz(xmlNodeD);
                end else
                if xmlNodeD.NodeName = 'Linhas' then
                begin


                  xmlNodeE := xmlNodeD.AttributeNodes.First;
                  while xmlNodeE <> nil do
                  begin
                    if xmlNodeE.NodeName = 'CodLinha' then
                    begin
                      GerouLinha := True;
                      New(FRLinha);
                      FRLinha.CodLinha := dmkPF.XMLValInt(xmlNodeE);
                      //
                      SetLength(Sons, 0);
                    end
                    else
                      MsgNoh('N� Attr n�o implementado: ' + xmlNodeE.NodeName);
                    xmlNodeE := xmlNodeE.NextSibling;
                  end;
                  //


                  xmlNodeE := xmlNodeD.ChildNodes.First;
                  while xmlNodeE <> nil do
                  begin
                    if xmlNodeE.NodeName = 'Sons' then
                    begin
                      xmlNodeF := xmlNodeE.ChildNodes.First;
                      while xmlNodeF <> nil do
                      begin
                        if xmlNodeF.NodeName = 'Item' then
                        begin
                          xmlNodeG := xmlNodeF.ChildNodes.First;
                          while xmlNodeG <> nil do
                          begin
                            //Geral.MB_Info(xmlNodeG.NodeName);
                            if xmlNodeG.NodeValue <> null then
                            begin
                              if xmlNodeG.NodeName = 'Codigo' then
                              begin
                                (*
                                CodItem := Geral.IMV(xmlNodeG.NodeValue);
                                LinGrd := CodItem + 1;
                                Grade.RowCount := LinGrd;
                                Grade.Cells[00, CodItem] := xmlNodeG.NodeValue;
                                //
                                *)
                                GerouSom       := True;
                                New(FRSom);
                                FRSom.Codigo   := Geral.IMV_Var(xmlNodeG.NodeValue);
                                FRSom.Musica   := FRMusica.CodMus;
                                FRSom.Estrofe  := FREstrofe.CodEst;
                                FRSom.Linha    := FRLinha.CodLinha;
                                //
                                FRSom.Coluna   := 0;
                                FRSom.CorGray  := clSilver;
                                FRSom.CorPrev  := clYellow;
                                FRSom.CorShow  := clFuchsia;
                                FRSom.SeqInLin := 0;
                                FRSom.Tempo    := -1;
                                FRSom.TempoIni := -1;
                                FRSom.TempoTam := -1;
                                FRSom.Texto    := '';
                                FRSom.TextoImp := '';
                                FRSom.TextoSom := '';
                                //SetLength(Sons, 0);
                              end else
                              if xmlNodeG.NodeName = 'Coluna' then
                              begin
                                FRSom.Coluna := dmkPF.XMLValInt(xmlNodeG);
                                //Grade.Cells[01, CodItem] := xmlNodeG.NodeValue;
                              end else
                              if xmlNodeG.NodeName = 'Linha' then
                              begin
                                //Grade.Cells[02, CodItem] := xmlNodeG.NodeValue;
                                FRSom.Linha    := dmkPF.XMLValInt(xmlNodeG);
                              end else
                              if xmlNodeG.NodeName = 'SeqInLin' then
                              begin
                                FRSom.SeqInLin := dmkPF.XMLValInt(xmlNodeG);
                                //Grade.Cells[03, CodItem] := xmlNodeG.NodeValue;
                              end else
                              if xmlNodeG.NodeName = 'TempoIni' then
                              begin
                                FRSom.TempoIni :=dmkPF.XMLValInt(xmlNodeG);
                                //Grade.Cells[04, CodItem] := xmlNodeG.NodeValue;
                              end else
                              if xmlNodeG.NodeName = 'TempoTam' then
                              begin
                                FRSom.TempoTam := dmkPF.XMLValInt(xmlNodeG);
                                //Grade.Cells[05, CodItem] := xmlNodeG.NodeValue;
                              end else
                              if xmlNodeG.NodeName = 'TextoSom' then
                              begin
                                FRSom.TextoSom := dmkPF.XMLValTxt(xmlNodeG);
                                //Grade.Cells[06, CodItem] := xmlNodeG.NodeValue;
                              end else
                              if xmlNodeG.NodeName = 'TextoImp' then
                              begin
                                FRSom.TextoImp := dmkPF.XMLValTxt(xmlNodeG);
                                //Grade.Cells[07, CodItem] := xmlNodeG.NodeValue;
                              end else
                              if xmlNodeG.NodeName = 'CorGray' then
                              begin
                                FRSom.CorGray  := dmkPF.XMLValInt(xmlNodeG);
                                //Grade.Cells[08, CodItem] := xmlNodeG.NodeValue;
                              end else
                              if xmlNodeG.NodeName = 'CorPrev' then
                              begin
                                FRSom.CorPrev  := dmkPF.XMLValInt(xmlNodeG);
                                //Grade.Cells[09, CodItem] := xmlNodeG.NodeValue;
                              end else
                              if xmlNodeG.NodeName = 'CorShow' then
                              begin
                                FRSom.CorShow  := dmkPF.XMLValInt(xmlNodeG);
                                //Grade.Cells[10, CodItem] := xmlNodeG.NodeValue;
                              end
                              else
                                MsgNoh('N� n�o implementado: ' + xmlNodeG.NodeName);
                              //
                            end;
                            xmlNodeG := xmlNodeG.NextSibling;
                          end;
                          if GerouSom then
                          begin
                            GerouSom := False;
                            N := Length(Sons);
                            SetLength(Sons, N + 1);
                            Sons[N] := FRSom;
                            //
                            //fazer Labels em panels!
                            //
                             nSom := nSom + 1;
                          end;
                        end;
                        xmlNodeF := xmlNodeF.NextSibling;
                      end;
                    end;
                    xmlNodeE := xmlNodeE.NextSibling;
                  end;
                  if GerouLinha then
                  begin
                    GerouLinha := False;
                    N := Length(Linhas);
                    SetLength(Linhas, N + 1);
                    //
                    P := Length(Sons);
                    SetLength(TesteI, P);
                    SetLength(Sonz, P);
                    for Q := 0 to P - 1 do
                    begin
                      TesteI[Q] := Sons[Q].Codigo;
                      Sonz[Q] := Sons[Q];
                    end;
                    FRLinha.CodItens := TesteI;
                    FRLinha.Sons := Sonz;
                    //
                    Linhas[N] := FRLinha;
                    nLin := nLin + 1;
                  end;
                end;
                xmlNodeD := xmlNodeD.NextSibling;
              end;
              if GerouEstrofe then
              begin
                GerouEstrofe := False;
                N := Length(Estrofes);
                SetLength(Estrofes, N + 1);
                //
                P := Length(Linhas);
                SetLength(TesteI, P);
                SetLength(Linhaz, P);
                for Q := 0 to P - 1 do
                begin
                  TesteI[Q] := Linhas[Q].CodLinha;
                  Linhaz[Q] := Linhas[Q];
                end;
                FREstrofe.CodItens := TesteI;
                FREstrofe.Linhas := Linhaz;
                //
                Estrofes[N] := FREstrofe;
                //
                nEst := nEst + 1;
              end;
            end;
          end;
          xmlNodeC := xmlNodeC.NextSibling;
        end;
        if GerouMusica then
        begin
          GerouMusica := False;
          N := Length(Musicas);
          //if N = 0 then

            SetLength(Musicas, N + 1);
            FMusicaSel := N;
          //
          P := Length(Estrofes);
          SetLength(TesteI, P);
          SetLength(Estrofez, P);
          for Q := 0 to P - 1 do
          begin
            TesteI[Q] := Estrofes[Q].CodEst;
            Estrofez[Q] := Estrofes[Q];
          end;
          FRMusica.CodItens := TesteI;
          FRMusica.Estrofes := Estrofez;
          //
          Musicas[N] := FRMusica;
          nMus := nMus + 1;
        end;
      end else
        MsgNoh(Noh);
      xmlNodeB := xmlNodeB.NextSibling;
    end;
  end else
    MsgNoh(Noh);
(*
  Geral.MB_Aviso(
    'Musicas: ' + Geral.FF0(nMus) + sLineBreak +
    'Estrofes: ' + Geral.FF0(nEst)+ sLineBreak +
    'Linhas: ' + Geral.FF0(nLin)+ sLineBreak +
    'Sons: ' + Geral.FF0(nSom)+ sLineBreak +
    '');
*)
  Result := True;
end;

{
function TUnApp_PF.SalvaMusica(Sons: array of TSom; NomeArquivo: String): Boolean;
var
  Som: TSom;
  I, Linha, SeqInLinha, CountSons: Integer;
  Texto: String;
  Txt: UTF8String;
begin
  Result := False;
  //
(* Criando o Documento XML e Gravando cabe�alho... *)
  arqXML := TXMLDocument.Create(nil);
  arqXML.Active := False;
  arqXML.FileName := '';
  cXML := GetLyrics(arqXML);
  arqXML.Version := '1.0';
  arqXML.Encoding := 'UTF-8';
  (*
  arqXML.DocumentElement.Attributes['xmlns:ds'] := 'http://www.w3.org/2000/09/xmldsig#';
  arqXML.DocumentElement.Attributes['xmlns:xsi'] := 'http://www.w3.org/2001/XMLSchema-instance';
  arqXML.DocumentElement.Attributes['xsi:schemaLocation'] := 'http://www.portalfiscal.inf.br/nfe/enviNFe_v1.12.xsd';
  *)
  cXML.Versao := verLyrics;
  SeqInLinha := -1;
  Linha := -1;
  CountSons := 0;
  for I := 1 to High(Sons) do
  begin
    if Sons[I] <> nil then
    begin
      New(Som);
      Som := Sons[I];
      //
      //
      if Som.Linha <> Linha then
      begin
        Linha := Som.Linha;
        SeqInLinha := 1;
        cEstrofes := cXML.Musicas.Add;
        cEstrofes.Attributes['CodEst'] := Geral.FF0(Linha);
      end else
        SeqInLinha := SeqInLinha + 1;
      //
      //cLinhas.Cod := Geral.FF0(Linha);
      cSons := cLinhas.Add;
      CountSons := CountSons + 1;
      cSons.Attributes['CodSom'] := Geral.FF0(CountSons);
      //Texto := DmkPF.ConverteANSItoUTF8(Som.Texto);
      Texto := Som.Texto;
      Txt := dmkPF.MyAnsiToUtf8(Texto);
      //Txt := System.AnsiToUtf8(Texto);
      cSons.Item.Codigo   := Geral.FF0(I);
      cSons.Item.Coluna   := Geral.FF0(Som.Coluna);
      cSons.Item.Linha    := Geral.FF0(Som.Linha);
      cSons.Item.SeqInLin := Geral.FF0(SeqInLinha);
      cSons.Item.CorGray  := Geral.FF0(clGray);
      cSons.Item.CorPrev  := Geral.FF0(Integer($000080FF));
      cSons.Item.CorShow  := Geral.FF0(Integer(clRed));
      cSons.Item.TempoIni := Geral.FF0(Som.Tempo);
      cSons.Item.TempoTam := '-1';
      cSons.Item.TextoImp := Txt;
      cSons.Item.TextoSom := Txt;
    end;
  end;
  //Geral.MB_Info(arqXML.XML.Text);
  SalvaXML('', NomeArquivo, arqXML.XML.Text);
  Result := True;
  arqXML := nil;
end;
}

function TUnApp_PF.CarregaPanels(const Idx, Step: Integer; Form: TForm;
  ScrollBox: TScrollBox; var Panels: TPanels; var Labels: TLabels;
  var Musicas: TMusicas; var FLablCount, MaxSom, MaxTempo: Integer; var SeqI,
  Tempos, CoresGray, CoresPrev, CoresShow: TInteiros): Boolean;
const
  TCol = 30;
var
  I, J, K, Altura: Integer;
  TesteE: TEstrofes;
  Estrofe: TEstrofe;
  Musica: TMusica;
  //
  Frase, FontName: String;
  nEst, nLin, nSom, LablAltu, EstLin, FontSize: Integer;
  FontBold: Boolean;
  TamLin, Tempo: Integer;
  ProcLin: Integer;
begin
  ProcLin := 0;
  try
    ProcLin := 1;
    for J := 0 to High(Labels) do
    begin
      if Labels[J] <> nil then
      begin
        ProcLin := 2;
        if Assigned(Labels[J]) then
        begin
          ProcLin := 3;
          if Labels[J].Font <> nil then
          begin
             ProcLin := 4;
            Sysutils.FreeAndNil(Labels[J]);
          end;
        end;
      end;
    end;
    ProcLin := 5;
    for I := 0 to High(Panels) do
    begin
      ProcLin := 6;
      if Panels[I] <> nil then
      begin
        ProcLin := 7;
        if Panels[I] is TPanel then
        begin
          ProcLin := 8;
          if Panels[I].Font <> nil then
          begin
            ProcLin := 9;
            Sysutils.FreeAndNil(Panels[I]);
          end;
        end;
      end;
    end;
    ProcLin := 10;
    if FmPrincipal.FScrollBox <> nil then
    begin
      ProcLin := 11;
      FmPrincipal.FScrollBox.Visible := False;
      ProcLin := 12;
      //FmPrincipal.FScrollBox.Free;
      ProcLin := 13;
      Sysutils.FreeAndNil(FmPrincipal.FScrollBox);
    end;
    ProcLin := 14;
    for I := 0 to High(Tempos) do
    begin
      Tempos[I] := 0;
      CoresGray[I] := 0;
      CoresPrev[I] := 0;
      CoresShow[I] := 0;
    end;
    ProcLin := 15;
    for I := 0 to High(SeqI) do
      SeqI[I] := 0;
    //
    FLablCount := 0;
    nEst := 0;
    nLin := 0;
    nSom := 0;
    Tempo := 0;
    LablAltu := Musicas[Idx].LablAltu;
    FontName := Musicas[Idx].FontName;
    FontSize := Musicas[Idx].FontSize;
    FontBold := Musicas[Idx].FontBold;


      ProcLin := 16;
      FmPrincipal.FScrollBox := TScrollBox.Create(Form);
      FmPrincipal.FScrollBox.Parent := FmPrincipal;
      FmPrincipal.FScrollBox.Align := alClient;


      ProcLin := 17;
    for I := 0 to High(Musicas[Idx].Estrofes) do
    begin
      ProcLin := 18;
      EstLin := Length(Musicas[Idx].Estrofes[I].Linhas);
      ProcLin := 19;
      SetLength(Panels, nEst + 1);
      ProcLin := 20;
      Panels[nEst] := TPanel.Create(Form);
      ProcLin := 21;
      //Panels[nEst].Parent := ScrollBox;
      Panels[nEst].Parent := FmPrincipal.FScrollBox;
      ProcLin := 22;
      Panels[nEst].Tag := nEst;
      ProcLin := 23;
      Panels[nEst].Height := Trunc((LablAltu * (EstLin + 1.5))) + 4;
      ProcLin := 24;
      Panels[nEst].Align := alBottom;
      ProcLin := 25;
      Panels[nEst].Align := alTop;
      ProcLin := 26;
      Panels[nEst].OnMouseDown := FmPrincipal.PanelMouseDown;
      Panels[nEst].OnMouseMove := FmPrincipal.PanelMouseMove;
      Panels[nEst].OnMouseUp   := FmPrincipal.PanelMouseUp;
      //Panels[nEst].Caption := 'Painel ' + IntToStr(nEst);
      nLin := 0;
      for J := 0 to High(Musicas[Idx].Estrofes[I].Linhas) do
      begin
        ProcLin := 27;
        nSom := 0;
        TamLin := 0;
        for K := 0 to High(Musicas[Idx].Estrofes[I].Linhas[J].Sons) do
        begin
          ProcLin := 28;
          FLablCount := FLablCount + 1;
      ProcLin := 29;
          Tempo := Musicas[Idx].Estrofes[I].Linhas[J].Sons[K].TempoIni;
      ProcLin := 30;
          SetLength(Tempos, FLablCount + 1);
      ProcLin := 31;
          Tempos[FLablCount] := Tempo;
      ProcLin := 32;
          SetLength(CoresGray, FLablCount + 1);
          SetLength(CoresPrev, FLablCount + 1);
          SetLength(CoresShow, FLablCount + 1);
          CoresGray[FLablCount] := Musicas[Idx].Estrofes[I].Linhas[J].Sons[K].CorGray;
          CoresPrev[FLablCount] := Musicas[Idx].Estrofes[I].Linhas[J].Sons[K].CorPrev;
          CoresShow[FLablCount] := Musicas[Idx].Estrofes[I].Linhas[J].Sons[K].CorShow;
          if MaxTempo < Tempo then
            MaxTempo := Tempo;
          //Frase := Frase + Musicas[Idx].Estrofes[I].Linhas[J].Sons[K].TextoSom + sLineBreak;
          //Geral.MB_Aviso(Musicas[Idx].Estrofes[I].Linhas[J].Sons[K].TextoSom);
      ProcLin := 33;
          SetLength(Labels, FLablCount + 1);
      ProcLin := 34;
          Labels[FLablCount] := TLabel.Create(Form);
      ProcLin := 35;
          Labels[FLablCount].Parent := Panels[nEst];
          Labels[FLablCount].Tag := FLablCount;
          Labels[FLablCount].Left := TamLin + TCol;
          Labels[FLablCount].Height := LablAltu;
          Labels[FLablCount].Top := Trunc(((nLin + 0.5) * LablAltu));
          //Labels[FLablCount].Caption := IntToStr(Caixa);
          Labels[FLablCount].Caption := Musicas[Idx].Estrofes[I].Linhas[J].Sons[K].TextoSom;
          Labels[FLablCount].Font.Color := Musicas[Idx].Estrofes[I].Linhas[J].Sons[K].CorGray;
          Labels[FLablCount].OnDblClick := FmPrincipal.LabelDblClick;
          Labels[FLablCount].OnMouseUp  := FmPrincipal.LabelMouseUp;
          Labels[FLablCount].OnMouseEnter  := FmPrincipal.LabelMouseEnter;

          if FontName <> '' then
            Labels[FLablCount].Font.Name := FontName;
          if FontSize <> 0 then
            Labels[FLablCount].Font.Size := FontSize;
          if FontBold then
            Labels[FLablCount].Font.Style := [fsBold];

          //Labels[FLablCount].Font.Color := clWhite;
          //Labels[FLablCount].Transparent := False;
          TamLin := TamLin + Labels[FLablCount].Width;
          //Labels[FLablCount].OnDblClick := FmPrincipal.LabelDblClick;
          //
          nSom := nSom + 1;
        end;
        nLin := nLin + 1;
      end;
      //Geral.MB_Aviso(Frase);
      Frase := '';
      //
      nEst := nEst + 1;
    end;
      ProcLin := 36;
    MaxSom := MaxTempo div Step;
      ProcLin := 37;
    SetLength(SeqI, MaxSom + 1);
      ProcLin := 38;
    for I := 0 to High(Tempos) do
    begin
      SeqI[Tempos[I] div Step] := I;
    end;
      ProcLin := 39;
    //
    Altura := 0;
    SetLength(FmPrincipal.FAltPan, Length(Panels));
    for I := Low(Panels) to High(Panels) do
    begin
      Altura := Altura + Panels[I].Height;
      FmPrincipal.FAltPan[I] := Altura;
    end;
    Result := True;
    //
(*
    Geral.MB_Info(
    'Panels: ' + Geral.FF0(Length(Panels)) + sLineBreak +
    'Labels: ' + Geral.FF0(Length(Labels)) + sLineBreak +
    'Tempos: ' + Geral.FF0(Length(Tempos)) + sLineBreak +
    'SeqI  : ' + Geral.FF0(Length(SeqI)) + sLineBreak +
    'LablCount: ' + Geral.FF0(FLablCount) + sLineBreak +
    'MaxSom: ' + Geral.FF0(MaxSom) + sLineBreak +
    'MaxTempo: ' + Geral.FF0(MaxTempo) + sLineBreak +
    '');
*)
  except
    on E: Exception do
    begin
      Geral.MB_Erro('Erro no ProgressTimerTimer() ProcLin ' + Geral.FF0(ProcLin));
      raise EAbort.Create(IntToStr(ProcLin));
    end;
  end;
end;

procedure TUnApp_PF.FillMP3FileList(Folder: string; sl: TStrings);
(*
begin
 sl.Clear;
 if System.SysUtils.FindFirst(Folder + '*.mp3', faAnyFile, Rec) = 0 then
  try
    repeat
      sl.Add(Rec.Name);
    until System.SysUtils.FindNext(Rec) > 0;
  finally
    System.SysUtils.FindClose(Rec);
  end;
*)
//procedure AtribuiDiretorios(Diretorio : String);
  function TemAtributo(Attr, Val: Integer): Boolean;
  begin
    Result := Attr and Val = Val;
  end;

var
  F : TSearchRec;
  Retorno : Integer;
begin
  //Inicia Busca
  Retorno := FindFirst( Folder + '\*.*', faAnyFile, F );

  sl.Clear;
  try
    while Retorno = 0 do
    begin
      if  (F.Name <> '.') and (F.Name <> '..') then
        begin
          if  TemAtributo( F.Attr, faDirectory ) then
              //sl.Add( Folder + '\' + F.Name )
              sl.Add(F.Name )
          {endif};
        end;
           Retorno := FindNext( F )
       {endif};
    end;
  finally
    FindClose( F );
  end;
end;

function TUnApp_PF.SalvaMusica2(PastaMae, PastaMusica, NomeMusica, NomeHinario, NoOriMusica,
  NoOriArtista, NoOriMidia: String; CodMusica: Integer; DataLetra: TDateTime;
  EstrPorTela: Integer; FontName: String; FontSize, LablAltu: Integer; FontBold:
  Boolean; Grade: TStringGrid): Boolean;
var
  //Som: TSom;
  I, Estrofe, Linha, SeqInLinha, CountSons: Integer;
  Texto: String;
  Txt: UTF8String;
  Txt_SeqMus, Txt_Estrofe, Txt_Linha, Txt_SeqLin, Txt_TempoI, Txt_Tamt,
  Txt_TextoSom, Txt_TextoImp, Txt_CorGray, Txt_CorPrev, Txt_CorShow: String;
  //
  CodEst_Ant, CosLinAnt: String;
begin
  Result := False;
  //
(* Criando o Documento XML e Gravando cabe�alho... *)
  arqXML := TXMLDocument.Create(nil);
  arqXML.Active := False;
  arqXML.FileName := '';
  cXML := GetLyrics(arqXML);
  arqXML.Version := '1.0';
  arqXML.Encoding := 'UTF-8';
  {
  arqXML.DocumentElement.Attributes['xmlns:ds'] := 'http://www.w3.org/2000/09/xmldsig#';
  arqXML.DocumentElement.Attributes['xmlns:xsi'] := 'http://www.w3.org/2001/XMLSchema-instance';
  arqXML.DocumentElement.Attributes['xsi:schemaLocation'] := 'http://www.portalfiscal.inf.br/nfe/enviNFe_v1.12.xsd';
  }
  cXML.Versao := verLyrics;
  SeqInLinha := -1;
  Linha := 0;
  Estrofe := 0;
  CountSons := 0;
  //
  cXML.Musicas.Attributes['CodMus'] := Geral.FF0(CodMusica);
  cXML.Musicas.NomeMusica    := dmkPF.MyAnsiToUtf8(NomeMusica);
  cXML.Musicas.NomeHinario   := dmkPF.MyAnsiToUtf8(NomeHinario);
  cXML.Musicas.NoOriMusica   := dmkPF.MyAnsiToUtf8(NoOriMusica);
  cXML.Musicas.NoOriArtista  := dmkPF.MyAnsiToUtf8(NoOriArtista);
  cXML.Musicas.NoOriMidia    := dmkPF.MyAnsiToUtf8(NoOriMidia);
  cXML.Musicas.DataLetra     := Geral.FDT(DataLetra, 1);
  cXML.Musicas.EstrPorTela   := Geral.FF0(EstrPorTela);
  cXML.Musicas.FontName      := dmkPF.MyAnsiToUtf8(FontName);
  cXML.Musicas.FontSize      := Geral.FF0(FontSize);
  cXML.Musicas.LablAltu      := Geral.FF0(LablAltu);
  cXML.Musicas.FontBold      := Geral.FF0(dmkPF.EscolhaDe2Int(FontBold, 1, 0));
  //
  for I := 1 to Grade.RowCount do
  begin
    ///if Sons[I] <> nil then
    begin
      Txt_SeqMus    := Grade.Cells[00, I];
      Txt_Estrofe   := Grade.Cells[01, I];
      Txt_Linha     := Grade.Cells[02, I];
      Txt_SeqLin    := Grade.Cells[03, I];
      Txt_TempoI    := Grade.Cells[04, I];
      Txt_Tamt      := Grade.Cells[05, I];
      Txt_TextoSom  := Grade.Cells[06, I];
      Txt_TextoImp  := Grade.Cells[07, I];
      Txt_CorGray   := Grade.Cells[08, I];
      Txt_CorPrev   := Grade.Cells[09, I];
      Txt_CorShow   := Grade.Cells[10, I];

      if Geral.IMV(Txt_Estrofe) <> Estrofe then
      begin
        Estrofe := Estrofe + 1;
        cEstrofes := cXML.Musicas.Estrofes.Add;
        cEstrofes.Attributes['CodEst'] := Geral.FF0(Estrofe);
      end;
      //
      if Geral.IMV(Txt_Linha) <> Linha then
      begin
        Linha := Geral.IMV(Txt_Linha);
        SeqInLinha := 1;
        cLinhas := cEstrofes.Linhas.Add;
        cLinhas.Attributes['CodLin'] := Geral.FF0(Linha);
      end else
        SeqInLinha := SeqInLinha + 1;
      //
      //cLinhas.Cod := Geral.FF0(Linha);
      cSons := cLinhas.Add;
      CountSons := CountSons + 1;
      cSons.Attributes['CodSom'] := Geral.FF0(CountSons);
      //Texto := DmkPF.ConverteANSItoUTF8(Som.Texto);
      //Texto := Som.Texto;
      Txt := dmkPF.MyAnsiToUtf8(Texto);
      //Txt := System.AnsiToUtf8(Texto);
      cSons.Item.Codigo  :=  Txt_SeqMus  ; // := Geral.FF0(I);
      cSons.Item.Coluna  :=  Txt_Estrofe ; // := Geral.FF0(Som.Coluna);
      cSons.Item.Linha   :=  Txt_Linha   ; // := Geral.FF0(Som.Linha);
      cSons.Item.SeqInLin:=  Txt_SeqLin  ; // := Geral.FF0(SeqInLinha);
      cSons.Item.CorGray :=  Txt_CorGray  ; // := Geral.FF0(clGray);
      cSons.Item.CorPrev :=  Txt_CorPrev    ; // := Geral.FF0(Integer($000080FF));
      cSons.Item.CorShow :=  Txt_CorShow; // := Geral.FF0(Integer(clRed));
      cSons.Item.TempoIni:=  Txt_TempoI; // := Geral.FF0(Som.Tempo);
      cSons.Item.TempoTam:=  Txt_Tamt; // := '-1';
      txt := dmkPF.MyAnsiToUtf8(Txt_TextoImp);
      cSons.Item.TextoImp:=  Txt ; // := Txt;
      txt := dmkPF.MyAnsiToUtf8(Txt_TextoSom);
      cSons.Item.TextoSom:=  Txt ; // := Txt;
    end;
  end;
  //Geral.MB_Info(arqXML.XML.Text);
  SalvaXML(PastaMae, PastaMusica, arqXML.XML.Text);
  Result := True;
  arqXML := nil;
end;

function TUnApp_PF.SalvaMusica3(SQLType: TSQLType; Musicas: TMusicas; nMusica: Integer;
PastaMae, PastaMusica, NomeMusica, NomeHinario,
  NoOriMusica, NoOriArtista, NoOriMidia: String; CodMusica: Integer;
  DataLetra: TDateTime; EstrPorTela: Integer; FontName: String; FontSize,
  LablAltu: Integer; FontBold: Boolean; Grade: TStringGrid; FMusica: TMusica): Boolean;
var
(*
  //Som: TSom;
  I, Estrofe, Linha, SeqInLinha, CountSons: Integer;
  Texto: String;
  Txt: UTF8String;
  Txt_SeqMus, Txt_Estrofe, Txt_Linha, Txt_SeqLin, Txt_TempoI, Txt_Tamt,
  Txt_TextoSom, Txt_TextoImp, Txt_CorGray, Txt_CorPrev, Txt_CorShow: String;
  //
  CodEst_Ant, CosLinAnt: String;
*)
  //
  Musica: TMusica;
  Estrofe: TEstrofe;
  Linha: TLinha;
  Som: TSom;
  I, nMus, nEst, nLin, nSom(*, ScanEst, iEst*): Integer;
  AchouEst: Boolean;
  //
  procedure Cabecalho();
  begin
    if FMusica <> nil then
      Musica  := FMusica
    else
      Musica  := Musicas[nMus];
    //
    cXML.Musicas.Attributes['CodMus'] := Geral.FF0(CodMusica);
    cXML.Musicas.NomeMusica    := dmkPF.MyAnsiToUtf8(NomeMusica);
    cXML.Musicas.NomeHinario   := dmkPF.MyAnsiToUtf8(NomeHinario);
    cXML.Musicas.NoOriMusica   := dmkPF.MyAnsiToUtf8(NoOriMusica);
    cXML.Musicas.NoOriArtista  := dmkPF.MyAnsiToUtf8(NoOriArtista);
    cXML.Musicas.NoOriMidia    := dmkPF.MyAnsiToUtf8(NoOriMidia);
    cXML.Musicas.DataLetra     := Geral.FDT(DataLetra, 1);
    cXML.Musicas.EstrPorTela   := Geral.FF0(EstrPorTela);
    cXML.Musicas.FontName      := dmkPF.MyAnsiToUtf8(FontName);
    cXML.Musicas.FontSize      := Geral.FF0(FontSize);
    cXML.Musicas.LablAltu      := Geral.FF0(LablAltu);
    cXML.Musicas.FontBold      := Geral.FF0(dmkPF.EscolhaDe2Int(FontBold, 1, 0));
    //
  end;
begin
  Result := False;
  if MyObjects.FIC(Trim(PastaMae) = '', nil, 'Pasta m�e n�o definida!') then Exit;
  if MyObjects.FIC(Trim(PastaMusica) = '', nil, 'Sub pasta da m�sica n�o definida!') then Exit;
  if MyObjects.FIC(Trim(NomeMusica) = '', nil, 'Nome da m�sica n�o definida!') then Exit;
  //
(* Criando o Documento XML e Gravando cabe�alho... *)
  arqXML := TXMLDocument.Create(nil);
  arqXML.Active := False;
  arqXML.FileName := '';
  cXML := GetLyrics(arqXML);
  arqXML.Version := '1.0';
  arqXML.Encoding := 'UTF-8';
  {
  arqXML.DocumentElement.Attributes['xmlns:ds'] := 'http://www.w3.org/2000/09/xmldsig#';
  arqXML.DocumentElement.Attributes['xmlns:xsi'] := 'http://www.w3.org/2001/XMLSchema-instance';
  arqXML.DocumentElement.Attributes['xsi:schemaLocation'] := 'http://www.portalfiscal.inf.br/nfe/enviNFe_v1.12.xsd';
  }
  cXML.Versao := verLyrics;
  nMus := 0;
  if SQLType = stIns then
  begin
    SetLength(Musicas, 1);
    Cabecalho();
  end else
  begin
    //for nMus := 0 to High(Musicas) do
    begin
      Cabecalho();
(*
      Musica  := Musicas[nMus];
      //
      cXML.Musicas.Attributes['CodMus'] := Geral.FF0(CodMusica);
      cXML.Musicas.NomeMusica    := dmkPF.MyAnsiToUtf8(NomeMusica);
      cXML.Musicas.NomeHinario   := dmkPF.MyAnsiToUtf8(NomeHinario);
      cXML.Musicas.NoOriMusica   := dmkPF.MyAnsiToUtf8(NoOriMusica);
      cXML.Musicas.NoOriArtista  := dmkPF.MyAnsiToUtf8(NoOriArtista);
      cXML.Musicas.NoOriMidia    := dmkPF.MyAnsiToUtf8(NoOriMidia);
      cXML.Musicas.DataLetra     := Geral.FDT(DataLetra, 1);
      cXML.Musicas.EstrPorTela   := Geral.FF0(EstrPorTela);
      cXML.Musicas.FontName      := dmkPF.MyAnsiToUtf8(FontName);
      cXML.Musicas.FontSize      := Geral.FF0(FontSize);
      cXML.Musicas.LablAltu      := Geral.FF0(LablAltu);
      cXML.Musicas.FontBold      := Geral.FF0(dmkPF.EscolhaDe2Int(FontBold, 1, 0));
      //
*)
      //ScanEst  := 0;
      for nEst := 0 to High(Musica.Estrofes) do
      begin
        cEstrofes := cXML.Musicas.Estrofes.Add;
        Estrofe   := Musica.Estrofes[nEst];
        cEstrofes.Attributes['CodEst'] := Geral.FF0(nEst + 1);
        //
        //cEstrofes.Musica      := Estrofe.Musica;
        cEstrofes.NoEst       := dmkPF.MyAnsiToUtf8(Estrofe.NoEst);
        //cEstrofes.            := Estrofe.CodInteiros;
        //cEstrofes.            := Estrofe.Linhas;
        cEstrofes.TempoShow   := Geral.FDT(Estrofe.TempoShow, 111);
        cEstrofes.TempoHide   := Geral.FDT(Estrofe.TempoHide, 111);
        //
        for nLin := 0 to High(Estrofe.Linhas) do
        begin
          cLinhas := cEstrofes.Linhas.Add;
          Linha   := Estrofe.Linhas[nLin];
          cLinhas.Attributes['CodLinha'] := Geral.FF0(nLin + 1);
          //
          //cLinhas.Musica      := Linha.Musica;
          //cLinhas.Estrofe      := Linha.Estrofe;
          //cLinhas.            := Linha.CodItens;
          //cLinhas.            := Linha.Sons;
          for nSom := 0 to High(Linha.Sons) do
          begin
            cSons     := cLinhas.Add;
            Som       := Linha.Sons[nSom];
            cSons.Attributes['CodSom'] := Geral.FF0(nSom + 1);
            //
            //cSons.Musica      := Son.Musica;
            cSons.Item.Versao       := '0.01';
            cSons.Item.Codigo       := Geral.FF0(nSom + 1);
            cSons.Item.Coluna       := Geral.FF0(Som.Coluna);
            cSons.Item.Linha        := Geral.FF0(Som.Linha);
            cSons.Item.SeqinLin     := Geral.FF0(Som.SeqInLin);
            cSons.Item.TempoIni     := Geral.FF0(Som.TempoIni);
            cSons.Item.TempoTam     := Geral.FF0(Som.TempoTam);
            cSons.Item.TextoSom     := dmkPF.MyAnsiToUtf8(Som.TextoSom);
            cSons.Item.TextoImp     := dmkPF.MyAnsiToUtf8(Som.TextoImp);
            cSons.Item.CorGray      := Geral.FF0(Som.CorGray);
            cSons.Item.CorPrev      := Geral.FF0(Som.CorPrev);
            cSons.Item.CorShow      := Geral.FF0(Som.CorShow);
            //cSons.            := Som.CodInteiros;
            //cSons.            := Som.Linhas;
          end;
        end;
      end;
    end;
  end;
  //Geral.MB_Info(arqXML.XML.Text);
  SalvaXML(PastaMae, PastaMusica, arqXML.XML.Text);
  Result := True;
  arqXML := nil;
end;

function TUnApp_PF.SalvaXML(Dir, Arq, Texto: String): String;
var
  Arquivo, xxx: String;
  TxtXML: TextFile;
  P: Integer;
begin
  ForceDirectories(Dir);
  Arquivo := Dir + '\' + ExtractFileName(Arq) + '\' + ExtractFileName(Arq) + '.xml'; // j� tem Ext ?
  //
  AssignFile(TxtXML, Arquivo);
  Rewrite(TxtXML);
  Write(TxtXML, Texto);
  CloseFile(TxtXML);
  //
  Result := Arquivo;
end;

end.
