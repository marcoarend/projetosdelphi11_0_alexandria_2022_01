object FmPrincipal: TFmPrincipal
  Left = 0
  Top = 0
  Caption = 'FmPrincipal'
  ClientHeight = 557
  ClientWidth = 1004
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnMouseWheel = FormMouseWheel
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1004
    Height = 41
    Align = alTop
    TabOrder = 0
    object LaTempo: TLabel
      Left = 252
      Top = 12
      Width = 66
      Height = 13
      Caption = '00:00:00.000'
    end
    object Label8: TLabel
      Left = 700
      Top = 12
      Width = 4
      Height = 13
      Caption = ':'
    end
    object Label9: TLabel
      Left = 736
      Top = 12
      Width = 4
      Height = 13
      Caption = ':'
    end
    object Label10: TLabel
      Left = 772
      Top = 12
      Width = 4
      Height = 13
      Caption = '.'
    end
    object LaPlayerStatus: TLabel
      Left = 444
      Top = 14
      Width = 31
      Height = 13
      Caption = 'Status'
    end
    object LaSomClik: TLabel
      Left = 600
      Top = 16
      Width = 47
      Height = 13
      Caption = 'LaSomClik'
    end
    object BtMenu: TButton
      Left = 8
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Menu'
      DropDownMenu = PMMenu
      PopupMenu = PMMenu
      TabOrder = 0
      OnClick = BtMenuClick
    end
    object Mp3player: TMediaPlayer
      Left = 96
      Top = 4
      Width = 141
      Height = 30
      VisibleButtons = [btPlay, btPause, btStop, btPrev, btEject]
      TabOrder = 1
      OnClick = Mp3playerClick
      OnPostClick = Mp3playerPostClick
    end
    object BtVaiPara: TButton
      Left = 816
      Top = 4
      Width = 75
      Height = 25
      Caption = 'Vai para'
      TabOrder = 2
      OnClick = BtVaiParaClick
    end
    object EdHor: TdmkEdit
      Left = 668
      Top = 8
      Width = 31
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 2
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '0'
      ValMax = '24'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdMin: TdmkEdit
      Left = 704
      Top = 8
      Width = 31
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 2
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '0'
      ValMax = '60'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdSeg: TdmkEdit
      Left = 740
      Top = 8
      Width = 31
      Height = 21
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 2
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '0'
      ValMax = '60'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdMil: TdmkEdit
      Left = 776
      Top = 8
      Width = 37
      Height = 21
      Alignment = taRightJustify
      TabOrder = 6
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 3
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '0'
      ValMax = '999'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object BtRecomeca: TButton
      Left = 892
      Top = 4
      Width = 75
      Height = 25
      Caption = 'Recome'#231'a'
      TabOrder = 7
      OnClick = BtRecomecaClick
    end
    object CkOcultaestLidas: TCheckBox
      Left = 340
      Top = 4
      Width = 97
      Height = 17
      Caption = 'Ocultar estrofes lidas.'
      Checked = True
      State = cbChecked
      TabOrder = 8
    end
    object BitBtn1: TBitBtn
      Left = 976
      Top = 4
      Width = 75
      Height = 25
      Caption = 'BitBtn1'
      TabOrder = 9
      OnClick = BitBtn1Click
    end
    object CkSetGray: TdmkCheckBox
      Left = 340
      Top = 20
      Width = 77
      Height = 17
      Caption = 'Descolorir'
      Checked = True
      State = cbChecked
      TabOrder = 10
      UpdType = utYes
      ValCheck = #0
      ValUncheck = #0
      OldValor = #0
    end
    object EdMouse: TdmkEdit
      Left = 500
      Top = 12
      Width = 80
      Height = 21
      TabOrder = 11
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
  end
  object Progress: TProgressBar
    Left = 0
    Top = 540
    Width = 1004
    Height = 17
    Align = alBottom
    TabOrder = 1
  end
  object OpenDialog1: TOpenDialog
    Filter = 'Arquivos Texto|*.TXT'
    FilterIndex = 0
    Left = 136
    Top = 128
  end
  object TimerLabels: TTimer
    Enabled = False
    Interval = 10
    OnTimer = TimerLabelsTimer
    Left = 60
    Top = 132
  end
  object TimerVoz: TTimer
    Enabled = False
    Interval = 10
    OnTimer = TimerVozTimer
    Left = 344
    Top = 124
  end
  object FolderDialog1: TFolderDialog
    DialogX = 0
    DialogY = 0
    Version = '1.1.0.2'
    Left = 188
    Top = 348
  end
  object PMMenu: TPopupMenu
    Left = 84
    Top = 64
    object Pastadefault1: TMenuItem
      Caption = 'Abrir M'#250'sica'
      OnClick = Pastadefault1Click
    end
    object NovaMsica1: TMenuItem
      Caption = 'Nova M'#250'sica'
      OnClick = NovaMsica1Click
    end
    object Editaraletradamusica1: TMenuItem
      Caption = 'Editar a letra da m'#250'sica'
      OnClick = Editaraletradamusica1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Recarrega1: TMenuItem
      Caption = 'Recarrega?'
      OnClick = Recarrega1Click
    end
    object Habilitartimecatch1: TMenuItem
      Caption = 'Habilitar time catch'
      OnClick = Habilitartimecatch1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Imprimir1: TMenuItem
      Caption = 'Imprimir'
      OnClick = Imprimir1Click
    end
    object Musicascarregadas1: TMenuItem
      Caption = 'Musicas carregadas'
      OnClick = Musicascarregadas1Click
    end
  end
  object XMLDocument1: TXMLDocument
    Left = 536
    Top = 300
    DOMVendorDesc = 'MSXML'
  end
  object TimerCatch: TTimer
    Interval = 10
    Left = 552
    Top = 152
  end
  object TimerPanels: TTimer
    Enabled = False
    OnTimer = TimerPanelsTimer
    Left = 372
    Top = 308
  end
  object TimerScroll: TTimer
    Enabled = False
    Interval = 50
    OnTimer = TimerScrollTimer
    Left = 596
    Top = 424
  end
end
