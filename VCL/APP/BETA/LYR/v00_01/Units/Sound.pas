unit Sound;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEditCB,
  dmkDBLookupComboBox, dmkEdit, MMSystem;

type
  TFmSound = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    BitBtn1: TBitBtn;
    EdFreqHerz: TdmkEdit;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    EdHor: TdmkEdit;
    EdMin: TdmkEdit;
    EdSeg: TdmkEdit;
    EdMil: TdmkEdit;
    RGAjusteTempo: TRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure RGAjusteTempoClick(Sender: TObject);
    procedure EdHorChange(Sender: TObject);
    procedure EdMinChange(Sender: TObject);
    procedure EdSegChange(Sender: TObject);
    procedure EdMilChange(Sender: TObject);
  private
    { Private declarations }
    // BEEP
    procedure CaculaAjusteDeTempo();
    procedure DoBeep(FreqHertz, Duration: LongWord);
    procedure Sound(FreqHertz: Word);
    procedure NoSound();
    // MIDI
    function  MIDIEncodeMessage(Msg, Param1, Param2: byte): integer;
    procedure NoteOn(NewNote, NewIntensity: byte);
    procedure NoteOff(NewNote, NewIntensity: byte);
    procedure SetInstrument(NewInstrument: byte);
    procedure InitMIDI();
  public
    { Public declarations }
    FAjusteTempo: Integer;
  end;

  var
  FmSound: TFmSound;

implementation

uses UnMyObjects;

{$R *.DFM}

var
  mo: HMIDIOUT;

const
  MIDI_NOTE_ON = $90;
  MIDI_NOTE_OFF = $80;
  MIDI_CHANGE_INSTRUMENT = $C0;

procedure TFmSound.BitBtn1Click(Sender: TObject);
begin
  DoBeep(EdFreqHerz.ValueVariant, 1000);
end;

procedure TFmSound.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSound.Button1Click(Sender: TObject);
begin
  InitMIDI();
  NoteOn(50, 127);
  Sleep(500);
  SetInstrument(60);
  NoteOn(60, 127);
  Sleep(500);
  NoteOff(60, 127);
  SetInstrument(80);
  NoteOn(70, 127);
  Sleep(500);
  NoteOff(70, 127);
  SetInstrument(90);
  NoteOn(80, 127);
  Sleep(500);
  NoteOff(80, 127);
  SetInstrument(100);
  NoteOn(90, 127);
  Sleep(500);
  NoteOff(90, 127);
  SetInstrument(12);
  NoteOn(40, 127);
  Sleep(1000);
  NoteOff(40, 127);
  //
     midiOutClose(mo);
     //MidiOpen:=false;
end;

procedure TFmSound.Button2Click(Sender: TObject);
begin
  InitMIDI();

  NoteOn(40, 127);
  Sleep(600);

  NoteOn(41, 127);
  Sleep(600);

  NoteOn(42, 127);
  Sleep(600);

  NoteOn(43, 127);
  Sleep(600);

  NoteOn(44, 127);
  Sleep(600);

  NoteOn(45, 127);
  Sleep(600);

  NoteOn(46, 127);
  Sleep(600);

  NoteOn(47, 127);
  Sleep(600);

  NoteOn(48, 127);
  Sleep(600);

  NoteOn(49, 127);
  Sleep(600);

  NoteOn(50, 127);
  Sleep(600);

  NoteOn(51, 127);
  Sleep(600);

  NoteOn(52, 127);
  Sleep(600);

  NoteOn(53, 127);
  Sleep(600);

  NoteOn(54, 127);
  Sleep(600);

  NoteOn(55, 127);
  Sleep(600);

  NoteOn(56, 127);
  Sleep(600);

  NoteOn(57, 127);
  Sleep(600);

  NoteOn(58, 127);
  Sleep(600);

  NoteOn(59, 127);
  Sleep(600);

  NoteOn(60, 127);
  Sleep(600);

  NoteOn(61, 127);
  Sleep(600);

  NoteOn(62, 127);
  Sleep(600);

  NoteOn(63, 127);
  Sleep(600);

  NoteOff(40, 127);
  //
     midiOutClose(mo);
     //MidiOpen:=false;

end;

procedure TFmSound.Button3Click(Sender: TObject);
begin
  InitMIDI();


  NoteOn(30, 63);
  Sleep(720);
  NoteOn(31, 63);
  Sleep(720);
  NoteOn(32, 63);
  Sleep(720);

     midiOutClose(mo);
     //MidiOpen:=false;


end;

procedure TFmSound.CaculaAjusteDeTempo();
var
  Tempo: Integer;
begin
  Tempo :=
    (EdHor.ValueVariant * 3600000) +
    (EdMin.ValueVariant * 60000) +
    (EdSeg.ValueVariant * 1000) +
    EdMil.ValueVariant;
  case RGAjusteTempo.ItemIndex of
    0: FAjusteTempo := -Tempo;
    1: FAjusteTempo :=  Tempo;
    else FAjusteTempo :=  0;
  end;
end;

procedure TFmSound.DoBeep(FreqHertz, Duration: LongWord);
begin
  if Win32Platform = VER_PLATFORM_WIN32_NT then
    Windows.Beep(FreqHertz, Duration)
  else begin
    Sound(1193181 div FreqHertz);
    Sleep(Duration);
    NoSound;
  end;
end;

procedure TFmSound.EdHorChange(Sender: TObject);
begin
  CaculaAjusteDeTempo();
end;

procedure TFmSound.EdMilChange(Sender: TObject);
begin
  CaculaAjusteDeTempo();
end;

procedure TFmSound.EdMinChange(Sender: TObject);
begin
  CaculaAjusteDeTempo();
end;

procedure TFmSound.EdSegChange(Sender: TObject);
begin
  CaculaAjusteDeTempo();
end;

procedure TFmSound.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSound.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmSound.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSound.InitMIDI();
begin
  midiOutOpen(@mo, 0, 0, 0, CALLBACK_NULL);
  midiOutShortMsg(mo, MIDIEncodeMessage(MIDI_CHANGE_INSTRUMENT, 0, 0));
end;

function TFmSound.MIDIEncodeMessage(Msg, Param1, Param2: byte): integer;
begin
  result := Msg + (Param1 shl 8) + (Param2 shl 16);
end;

procedure TFmSound.NoSound();
asm
  IN AL, $61
  AND AL, $FC
  OUT $61, AL
end;

procedure TFmSound.NoteOff(NewNote, NewIntensity: byte);
begin
  midiOutShortMsg(mo, MIDIEncodeMessage(MIDI_NOTE_OFF, NewNote, NewIntensity));
end;

procedure TFmSound.NoteOn(NewNote, NewIntensity: byte);
begin
  midiOutShortMsg(mo, MIDIEncodeMessage(MIDI_NOTE_ON, NewNote, NewIntensity));
end;

procedure TFmSound.RGAjusteTempoClick(Sender: TObject);
begin
  CaculaAjusteDeTempo();
end;

procedure TFmSound.SetInstrument(NewInstrument: byte);
begin
  midiOutShortMsg(mo, MIDIEncodeMessage(MIDI_CHANGE_INSTRUMENT, NewInstrument, 0));
end;

procedure TFmSound.Sound(FreqHertz: Word);
asm
  MOV DX, AX
  IN AL, $61
  MOV AH, AL
  AND AL, 3
  JNE @@1
  MOV AL, AH
  OR AL, 3
  OUT $61, AL
  MOV AL, $B6
  OUT $43, AL
@@1:
  MOV AX, DX
  OUT $42, AL
  MOV AL, AH
  OUT $42, AL
end;

end.
