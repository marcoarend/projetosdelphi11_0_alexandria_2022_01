unit EditMusica;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEditCB,
  dmkDBLookupComboBox, dmkEdit, dmkStringGrid, dmkRadioGroup, (*dmkPopOutFntCBox,*)
  dmkEditDateTimePicker, Menus, Vcl.MPlayer,
  UnApp_PF, UnDmkProcFunc, UnArr_PF, UnProjGroup_Consts;

type
  TFmEditMusica = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    PnLyrics: TPanel;
    Panel5: TPanel;
    Label11: TLabel;
    Button2: TButton;
    EdLinha: TdmkEdit;
    MeLyrics: TMemo;
    PB1: TProgressBar;
    SGMusica: TStringGrid;
    Panel6: TPanel;
    Panel7: TPanel;
    Label3: TLabel;
    Label2: TLabel;
    Label7: TLabel;
    EdNomeHinario: TdmkEdit;
    EdCodMus: TdmkEdit;
    EdNomeMusica: TdmkEdit;
    TPDataLetra: TdmkEditDateTimePicker;
    Panel8: TPanel;
    Label6: TLabel;
    Label5: TLabel;
    Label4: TLabel;
    EdNoOriMidia: TdmkEdit;
    EdNoOriMusica: TdmkEdit;
    EdNoOriArtista: TdmkEdit;
    GroupBox2: TGroupBox;
    Panel9: TPanel;
    Label1: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    CBFontName: TdmkPopOutFntCBox;
    CkNegrito: TCheckBox;
    EdLablAltu: TdmkEdit;
    CBFontSize: TComboBox;
    RGEstrPorTela: TdmkRadioGroup;
    SGEstrofes: TdmkStringGrid;
    Label10: TLabel;
    EdPastaMusica: TdmkEdit;
    SGLinhas: TdmkStringGrid;
    SGSons: TdmkStringGrid;
    PMSons: TPopupMenu;
    Inserirnovosomacima1: TMenuItem;
    Excluirsomselecionado1: TMenuItem;
    PMLinhas: TPopupMenu;
    Incrementatempoemtodossons1: TMenuItem;
    Acimadalinha1: TMenuItem;
    Abaixodalinha1: TMenuItem;
    NoInicio1: TMenuItem;
    Nofinal1: TMenuItem;
    Excluirlinha1: TMenuItem;
    Inserirnovalinha1: TMenuItem;
    Noincio1: TMenuItem;
    Acimadalinha2: TMenuItem;
    Abaixodalinha2: TMenuItem;
    Nofinal2: TMenuItem;
    Incrementatempoemtodossonsapartirdaqui1: TMenuItem;
    PMEstrofes: TPopupMenu;
    Alteraestrofe1: TMenuItem;
    Excluiestrofe1: TMenuItem;
    Inserenovaestrofe1: TMenuItem;
    Noincio2: TMenuItem;
    Acimadalinha3: TMenuItem;
    Abaixodalinha3: TMenuItem;
    Nofinal3: TMenuItem;
    Duplicarlinha1: TMenuItem;
    Mudapaletadecordetodossons1: TMenuItem;
    Incrementetempoemtodaslinhas1: TMenuItem;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure MeLyricsChange(Sender: TObject);
    procedure MeLyricsClick(Sender: TObject);
    procedure MeLyricsEnter(Sender: TObject);
    procedure MeLyricsKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure SGMusicaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SGMusicaSetEditText(Sender: TObject; ACol, ARow: Integer;
      const Value: string);
    procedure BtOKClick(Sender: TObject);
    procedure SGEstrofesClick(Sender: TObject);
    procedure SGLinhasClick(Sender: TObject);
    procedure SGSonsDblClick(Sender: TObject);
    procedure Excluirsomselecionado1Click(Sender: TObject);
    procedure Incrementatempoemtodossons1Click(Sender: TObject);
    procedure Acimadalinha1Click(Sender: TObject);
    procedure Abaixodalinha1Click(Sender: TObject);
    procedure NoInicio1Click(Sender: TObject);
    procedure Nofinal1Click(Sender: TObject);
    procedure Excluirlinha1Click(Sender: TObject);
    procedure Noincio1Click(Sender: TObject);
    procedure Acimadalinha2Click(Sender: TObject);
    procedure Abaixodalinha2Click(Sender: TObject);
    procedure Nofinal2Click(Sender: TObject);
    procedure Incrementatempoemtodossonsapartirdaqui1Click(Sender: TObject);
    procedure Alteraestrofe1Click(Sender: TObject);
    procedure Excluiestrofe1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SGEstrofesDblClick(Sender: TObject);
    procedure Noincio2Click(Sender: TObject);
    procedure Acimadalinha3Click(Sender: TObject);
    procedure Abaixodalinha3Click(Sender: TObject);
    procedure Nofinal3Click(Sender: TObject);
    procedure Duplicarlinha1Click(Sender: TObject);
    procedure Mudapaletadecordetodossons1Click(Sender: TObject);
    procedure Incrementetempoemtodaslinhas1Click(Sender: TObject);
  private
    { Private declarations }
    //procedure CriaLabels();
    procedure AlteraEstrofe();
    procedure InsereNovaEstrofe(Como: TVaiPara);
    procedure InsereNovaLinha(Como: TVaiPara);
    function  DefineIndice(Como: TVaiPara; Atual, Max: Integer): Integer;
    procedure DefineSelecionados();
    function  ObtemSom(var Som: TSom; var Index: Integer): Boolean;
    function  SelecionaPaleta(): Boolean;
  public
    { Public declarations }
    FnEst, FnLin, FnSom: Integer;
    FContinuar: Boolean;
    //
    procedure MostraEstrofesDaMusica(nEst: Integer);
    procedure MostraLinhasDaEstrofe(nEst, nLin: Integer);
    procedure MostraSonsDaLinha(nEst, nLin, nSom: Integer);
    procedure InsereNovoSom(Como: TVaiPara);
  end;

  var
  FmEditMusica: TFmEditMusica;

implementation

uses UnMyObjects, Principal, EditEstrofe, EditSom, SelMusica, SelPaleta;

{$R *.DFM}

var
  FMusica: TMusica;
  FEstrofe: TEstrofe;
  FLinha: TLinha;
  FSom: TSom;

procedure TFmEditMusica.Abaixodalinha1Click(Sender: TObject);
begin
  InsereNovoSom(vpNext);
end;

procedure TFmEditMusica.Abaixodalinha2Click(Sender: TObject);
begin
  InsereNovaLinha(vpNext);
end;

procedure TFmEditMusica.Abaixodalinha3Click(Sender: TObject);
begin
  InsereNovaEstrofe(vpNext);
end;

procedure TFmEditMusica.Acimadalinha1Click(Sender: TObject);
begin
  InsereNovoSom(vpPrior);
end;

procedure TFmEditMusica.Acimadalinha2Click(Sender: TObject);
begin
  InsereNovaLinha(vpPrior);
end;

procedure TFmEditMusica.Acimadalinha3Click(Sender: TObject);
begin
  InsereNovaEstrofe(vpPrior);
end;

procedure TFmEditMusica.AlteraEstrofe();
var
  I: Integer;
  //FMusica: TMusica;
begin
  FmEditMusica.DefineSelecionados();
  //
  FmEditEstrofe.FMusica                   := FMusica;
  FmEditEstrofe.FEstrofe                  := FEstrofe;
  //
  FmEditEstrofe.EdNoMusica.ValueVariant   := FMusica.NomeMusica;
  FmEditEstrofe.EdCodMus.ValueVariant     := FMusica.CodMus;
  FmEditEstrofe.EdCodEstrofe.ValueVariant := FEstrofe.CodEst;
  FmEditEstrofe.EdTempoHide.ValueVariant  := Geral.FDT(FEstrofe.TempoHide, 111);
  FmEditEstrofe.EdTempoShow.ValueVariant  := Geral.FDT(FEstrofe.TempoShow, 111);
  FmEditEstrofe.EdNoEst.ValueVariant      := FEstrofe.NoEst;
  //
  FmEditEstrofe.Show;
end;

procedure TFmEditMusica.Alteraestrofe1Click(Sender: TObject);
begin
  AlteraEstrofe();
end;

procedure TFmEditMusica.BtOKClick(Sender: TObject);
var
  PastaMae, PastaMusica, NomeMusica, NomeHinario, NoOriMusica, NoOriArtista, NoOriMidia: String;
  DataLetra: TDateTime;
  CodMusica, EstrPorTela: Integer;
  FontName: String; FontSize, LablAltu: Integer;
  FontBold: Boolean;
begin
  Screen.Cursor := crHourGlass;
  try
    try
      if TMPBtnType.btStop in  FmPrincipal.mp3player.EnabledButtons then
        FmPrincipal.Mp3player.Position := 0;
      FmPrincipal.ParaLoop();
    except
      //
    end;
    //PastaMae     := FmPrincipal.FLyrFile + dmkPF.RemoveExtensaoDeArquivo(ExtractFileName(FmPrincipal.FLyrFile));
    NomeMusica   := EdNomeMusica.ValueVariant;
    NoOriMusica  := EdNoOriMusica.ValueVariant;
    NoOriArtista := EdNoOriArtista.ValueVariant;
    if MyObjects.FIC(Trim(NomeMusica) = '', EdNomeMusica, 'Nome da m�sica n�o definida!') then Exit;
    if MyObjects.FIC(Trim(NoOriMusica) = '', EdNoOriMusica, 'Nome do artista da m�sica original n�o definido!') then Exit;
    if MyObjects.FIC(Trim(NoOriArtista) = '', EdNoOriArtista, 'Nome da m�sica n�o definida!') then Exit;
    if ImgTipo.SQLType = stIns then
    begin
      PastaMusica  := NomeMusica + ' - ' + NoOriArtista + ' - ' + NoOriMusica;
      //PastaMae     := ExtractFilePath(Application.ExeName) + PastaMusica;
      PastaMae     := dmkPF.CaminhoArquivo(VAR_PASTA_MAE, PastaMusica, '');
      if MyObjects.FIC(DirectoryExists(PastaMae), nil,
      'J� existe uma pasta com este nome!') then
        Exit;
    end else
    begin
      //PastaMae     := ExtractFileDir(FmPrincipal.FLyrFile);
      PastaMae     := VAR_PASTA_MAE;
      PastaMusica  := dmkPF.RemoveExtensaoDeArquivo(ExtractFileName(FmPrincipal.FLyrFile));
    end;
    CodMusica    := EdCodMus.ValueVariant;
    NomeHinario  := EdNomeHinario.ValueVariant;
    NoOriMidia   := EdNoOriMidia.ValueVariant;
    DataLetra    := TPDataLetra.DateTime;
    EstrPorTela  := Geral.IMV(RGEstrPorTela.Items[RGEstrPorTela.ItemIndex]);
    FontName     := CBFontName.FonteNome;
    FontSize     := Geral.IMV(CBFontSize.Text);
    LablAltu     := EdLablAltu.ValueVariant;
    //App_PF.SalvaMusica(FmPrincipal.FSons, FmPrincipal.FLyrFile);
    if App_PF.SalvaMusica3(ImgTipo.SQLType, FmPrincipal.FMusicas, FMusicaSel,
    PastaMae, PastaMusica, NomeMusica, NomeHinario, NoOriMusica, NoOriArtista,
    NoOriMidia, CodMusica, DataLetra, EstrPorTela, FontName, FontSize, LablAltu,
    CkNegrito.Checked, SGMusica, FMusica) then
    begin
      //Geral.SalvaTextoEmArquivo(FmPrincipal.FLyrFile, MeLyrics.Text, True);
      if (ImgTipo.SQLType = stUpd) and (
      Geral.MB_Pergunta('Deseja recarregar a m�sica selecionada!') = ID_YES) then
        FmSelMusica.Carrega();
      //
      App_PF.FillMP3FileList(FmSelMusica.StFolder.Caption, FmSelMusica.mp3List.Items);
      Hide;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmEditMusica.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEditMusica.Button2Click(Sender: TObject);
var
  sIncr: String;
  I, Ini, Fim, iIncr: Integer;
const
  Separador = '|';
var
  N, Colum, Linha, Conta, Tempo, NewTmp: Integer;
  L_Ini, L_Alt, T_Ini, T_Col: Integer;
  TamLin: array of array of Integer;
  Sons: TStringList;
  Lin, Texto, Parte: String;
  //
begin
(*
  sIncr := '0';
  if InputQuery('Incremento de tempo', 'Informe em milisegundos:', sIncr) then
  begin
    iIncr := Geral.IMV(sIncr);
    if iIncr <> 0 then
    begin
      Ini := EdLinha.ValueVariant;
      Fim := MeLyrics.Lines.Count;
      //
      for I := Ini to Fim do
      begin
        Lin := MeLyrics.Lines[I];
        if Lin <> '' then
        begin
          Colum := 0;
          Linha := 0;
          Tempo := 0;
          Texto := '';
          //
          Conta := 0;
          while Length(Lin) > 0 do
          begin
            if Geral.SeparaPrimeiraOcorrenciaDeTexto(Separador, Lin, Parte, Lin) then
            begin
              Conta := Conta + 1;
              case Conta of
                1: Colum := Geral.IMV(Parte);
                2: Linha := Geral.IMV(Parte);
                3: Tempo := Geral.IMV(Parte);
                4: Texto := Parte;
                else begin
                  if Trim(Parte) <> '' then
                  begin
                    Geral.MB_Erro('N�o foi poss�vel definir a vari�vel pai "' +
                    Geral.FF0(I) + '" de resultado (valor):' + sLineBreak + Parte);
                    Screen.Cursor := crDefault;
                    Exit;
                  end;
                end;
              end;
            end;
          end;
          NewTmp := Tempo + iIncr;
          Lin := Geral.FFF(Colum, 2) + '|' + Geral.FFF(Linha, 3) + '|' + Geral.FFF(NewTmp, 6) + '|' + Texto;
          MeLyrics.Lines[I] := Lin;
        end;
      end;
    end;
  end;
*)
end;

function TFmEditMusica.DefineIndice(Como: TVaiPara; Atual, Max: Integer): Integer;
var
  P: Integer;
begin
  P := -1;
  case Como of
    vpFirst: P := 0;
    vpPrior: P := Atual - 1;
    vpNext : P := Atual;
    vpLast : P := Max + 1;
    //vpThis : P := nSom;
  end;
  if P < 0 then
    P := 0;
  if P > Max then
    P := Max;
  Result := P;
end;

procedure TFmEditMusica.DefineSelecionados();
var
  Txt: String;
  A, B, C, D: Boolean;
begin
  FMusica  := FmPrincipal.FMusicas[FMusicaSel];
  //
  if SGEstrofes.Row > 0 then
  begin
    FnEst    := Geral.IMV(SGEstrofes.Cells[1, SGEstrofes.Row]);
    if Length(FMusica.Estrofes) < FnEst then
      FEstrofe := nil
    else
      FEstrofe := FMusica.Estrofes[FnEst - 1];
    //
    if SGLinhas.Row > 0 then
    begin
      FnLin    := Geral.IMV(SGLinhas.Cells[0, SGLinhas.Row]);
      if (not Assigned(FEstrofe)) or (Length(FEstrofe.Linhas) < FnLin) or (FnLin < 1) then
        //FLinha := nil
      else
        FLinha := FEstrofe.Linhas[FnLin - 1];
      //
      if SGSons.Row > 0 then
      begin
        FnSom    := Geral.IMV(SGSons.Cells[0, SGSons.Row]);
        //if (not Assigned(FLinha)) or (Length(FLinha.Sons) < FnSom) or (FnSom = 0) then
        A := Assigned(FLinha);
        B := Assigned(FSom);
        C := Length(FLinha.Sons) < FnSom;
        D := FnSom = 0;
        if (not A) or (not B) or C or D then
          //FSom := nil
        else
          FSom     := FLinha.Sons[FnSom - 1];
      end;
    end;
  end;
end;

procedure TFmEditMusica.Duplicarlinha1Click(Sender: TObject);
var
  Txt: String;
  Estrofe: TEstrofe;
  Linha: TLinha;
  Som: TSom;
  nEst, nLin, I, nSom: Integer;
  Continua: Boolean;
begin
  Continua := True;
  DefineSelecionados();
  //
  Txt := '0';
  if InputQuery('Sele��o de Estrofe', 'Informe a estrofe de destino:', Txt) then
  begin
    nEst := Geral.IMV(Txt);
    if (nEst > 0) and (nEst - 1 < Length(FMusica.Estrofes)) then
      Continua := True
    else
    begin
      Continua := False;
      Geral.MB_Aviso('Estrofe inv�lida!');
      Exit;
    end;
  end;
  //
  Estrofe := FMusica.Estrofes[nEst - 1];
  Txt := '0';
  if InputQuery('Sele��o de Linha', 'Informe a linha de destino:', Txt) then
  begin
    nLin := Geral.IMV(Txt);
    if (nLin > 0) and (nLin - 2 < Length(Estrofe.Linhas)) then
      Continua := True
    else
    begin
      Continua := False;
      Geral.MB_Aviso('Linha inv�lida!');
    end;
  end;
  //
  if Continua then
  begin
    New(Linha);
    //
    Linha.Musica   := FMusicaSel;
    Linha.Estrofe  := FnEst;
    //
    Linha.CodLinha := FnLin;
    //Linha.CodItens := 1; // criado vazio!?
    //
    for I := 0 to Length(FLinha.Sons) -1 do
    begin
      New(Som);
      //
      Som.Musica   := FLinha.Sons[I].Musica;
      Som.Estrofe  := FLinha.Sons[I].Estrofe;
      Som.Linha    := FnLin;
      //
      Som.SeqInLin := FLinha.Sons[I].SeqInLin;
      //
      Som.TempoIni := FLinha.Sons[I].TempoIni;
      Som.TempoTam := FLinha.Sons[I].TempoTam;
      Som.TextoSom := FLinha.Sons[I].TextoSom;
      Som.TextoImp := FLinha.Sons[I].TextoImp;
      Som.CorGray  := FLinha.Sons[I].CorGray;
      Som.CorPrev  := FLinha.Sons[I].CorPrev;
      Som.CorShow  := FLinha.Sons[I].CorShow;
      //
      //
      Arr_PF.InsertSom(Linha.Sons, I, Som);
    end;
    //
    Arr_PF.InsertLinha(Estrofe.Linhas, nLin, Linha);
    //
    MostraLinhasDaEstrofe(FnEst, FnLin);
    //SGLinhas.Row := FnLin;
    DefineSelecionados();
    MostraSonsDaLinha(FnEst, FnLin, 1);
  end;
end;

{
procedure TFmEditMusica2.CriaLabels();
var
  N, I, Colum, Linha, Conta, Tempo: Integer;
  L_Ini, L_Alt, T_Ini, T_Col: Integer;
  TamLin: array of array of Integer;
  //Lin,
  Texto, Parte: String;
  CorPrev, CorShow: TColor;
  //
  function CriaLabel(Caixa, Coluna, Linha: Integer; Texto: String): Boolean;
  begin
    FmPrincipal.FLabels[Caixa] := TLabel.Create(Self);
    FmPrincipal.FLabels[Caixa].Parent := FmPrincipal.ScrollBox1;
    FmPrincipal.FLabels[Caixa].Tag := Caixa;
    FmPrincipal.FLabels[Caixa].Left := TamLin[Coluna][Linha] + ((Coluna - 1) * T_Col);
    FmPrincipal.FLabels[Caixa].Height := T_Ini + (L_Alt * Caixa);
    FmPrincipal.FLabels[Caixa].Top := T_Ini + (L_Alt * Linha);
    //FmPrincipal.FLabels[Caixa].Caption := IntToStr(Caixa);
    FmPrincipal.FLabels[Caixa].Caption := Texto;
    FmPrincipal.FLabels[Caixa].Font.Color := clGray;

    if FmPrincipal.FFontName <> '' then
      FmPrincipal.FLabels[Caixa].Font.Name := FmPrincipal.FFontName;
    if FmPrincipal.FFontSize <> 0 then
      FmPrincipal.FLabels[Caixa].Font.Size := FmPrincipal.FFontSize;
    if FmPrincipal.FFontBold then
      FmPrincipal.FLabels[Caixa].Font.Style := [fsBold];

    //FmPrincipal.FLabels[Caixa].Font.Color := clWhite;
    //FmPrincipal.FLabels[Caixa].Transparent := False;
    TamLin[Coluna][Linha] := TamLin[Coluna][Linha] + FmPrincipal.FLabels[Caixa].Width;
    FmPrincipal.FLabels[Caixa].OnDblClick := FmPrincipal.LabelDblClick;
  end;
begin
  EXIT;
    FMEditMusica.BtSalva.Enabled := True;
    //
    N := SGMusica.RowCount -1;
    FmPrincipal.FMaxSom := N + 1;
    //
    if FmPrincipal.FFontHigh <> 0 then
      L_Alt :=  FmPrincipal.FFontHigh
    else
      L_Alt := 30;
    L_Ini := 40;
    T_Ini := 40;
    T_Col := 800;
    FmPrincipal.FGapPrev := 250 div FmPrincipal.FStep;
    //
    FmPrincipal.FMaxTempo := 0;
    for I := 1 to High(FmPrincipal.FLabels) do
    begin
      if FmPrincipal.FLabels[I] <> nil then
      begin
        try
          FmPrincipal.FLabels[I].Free;
        except
          //
        end;
      end;
    end;
    SetLength(FmPrincipal.FLabels, FmPrincipal.FMaxSom);
    SetLength(FmPrincipal.FTempos, FmPrincipal.FMaxSom);
    SetLength(TamLin, 4);
    SetLength(TamLin[1], N);
    SetLength(TamLin[2], N);
    SetLength(TamLin[3], N);
    SetLength(TamLin[4], N);
    SetLength(FmPrincipal.FSons, N);
    for I := 0 to N - 1 do
    begin
      TamLin[1][I] := L_Ini;
      TamLin[2][I] := L_Ini;
      TamLin[3][I] := L_Ini;
      TamLin[4][I] := L_Ini;
    end;
    FmSelMusica.PB1.Max := N;
    FmSelMusica.PB1.Position := 0;
    for I := 1 to N - 1 do
    begin
      FmSelMusica.PB1.Position := FmSelMusica.PB1.Position + 1;
      Application.ProcessMessages;
      //
      FmPrincipal.FTempos[I + 1] := 0;
      //Lin := FmPrincipal.FSons[I];
      //if Lin <> '' then
      begin
        Colum := 0;
        Linha := 0;
        Tempo := 0;
        Texto := '';
        CorPrev := $000080FF;
        CorShow := clred;
        //
        Conta := 0;
        FmPrincipal.FTempos[I + 1] := Tempo;
        if FmPrincipal.FMaxTempo < Tempo then
          FmPrincipal.FMaxTempo := Tempo;
        //
        New(FmPrincipal.FRSom);
        FmPrincipal.FRSom.Codigo  := I + 1;
        //SGMusica.Cells[00, 0] := 'SeqMus';
        FmPrincipal.FRSom.Estrofe  := Geral.IMV('0' + SGMusica.Cells[01, 0]);
        FmPrincipal.FRSom.Linha    := Geral.IMV('0' + SGMusica.Cells[02, 0]);
        FmPrincipal.FRSom.SeqInLin := Geral.IMV('0' + SGMusica.Cells[03, 0]);
        FmPrincipal.FRSom.TempoIni := Geral.IMV('0' + SGMusica.Cells[04, 0]);
        FmPrincipal.FRSom.TempoTam := Geral.IMV('0' + SGMusica.Cells[05, 0]);
        FmPrincipal.FRSom.TextoSom := SGMusica.Cells[06, 0];
        FmPrincipal.FRSom.TextoImp := SGMusica.Cells[07, 0];
        FmPrincipal.FRSom.CorGray  := Geral.IMV('0' + SGMusica.Cells[08, 0]);
        FmPrincipal.FRSom.CorPrev  := Geral.IMV('0' + SGMusica.Cells[09, 0]);
        FmPrincipal.FRSom.CorShow  := Geral.IMV('0' + SGMusica.Cells[10, 0]);
        //SGMusica.Cells[11, 0] := '';
        FmPrincipal.FSons[I+1] := FmPrincipal.FRSom;
        //
        CriaLabel(I + 1, Colum, Linha, Texto);
      end;
    end;
    FmPrincipal.FMaxSom := FmPrincipal.FMaxTempo div FmPrincipal.FStep;
    SetLength(FmPrincipal.FSeqI, FmPrincipal.FMaxSom);
    for I := 0 to High(FmPrincipal.FTempos) do
    begin
      FmPrincipal.FSeqI[FmPrincipal.FTempos[I] div FmPrincipal.FStep] := I;
    end;
end;
}

procedure TFmEditMusica.Excluiestrofe1Click(Sender: TObject);
var
  MaxEst: Integer;
begin
  MaxEst := High(FMusica.Estrofes);
  //
  DefineSelecionados();
  //
  Arr_PF.DeleteEstrofe(FMusica.Estrofes, FnEst - 1);
  //
  if MaxEst > FnEst then
    SGEstrofes.Row := FnEst
  else
    SGEstrofes.Row := FnEst - 1;
  //
  DefineSelecionados();
  MostraEstrofesDaMusica(1);
  MostraLinhasDaEstrofe(FnEst, 1);
  MostraSonsDaLinha(FnEst, 1, 1);
end;

procedure TFmEditMusica.Excluirlinha1Click(Sender: TObject);
begin
  DefineSelecionados();
  //
  Arr_PF.DeleteLinha(FEstrofe.Linhas, FnLin - 1);
  //
  DefineSelecionados();
  MostraLinhasDaEstrofe(FnEst, FnLin);
  MostraSonsDaLinha(FnEst, FnLin, 1);
end;

procedure TFmEditMusica.Excluirsomselecionado1Click(Sender: TObject);
begin
  DefineSelecionados();
  //
  Arr_PF.DeleteSom(FLinha.Sons, FnSom - 1);
  //
  MostraSonsDaLinha(FnEst, FnLin, FnSom - 1);
end;

procedure TFmEditMusica.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEditMusica.FormCreate(Sender: TObject);
begin
  FnEst := 1;
  //FnLin := 1; Stack Overflow
  //
  ImgTipo.SQLType := stLok;
  //
  TPDataLetra.Date := Date;
  //
  New(FEstrofe);
  New(FLinha);
  New(FSom);
  //
  SGMusica.Cells[00, 0] := 'SeqMus';
  SGMusica.Cells[01, 0] := 'Estrofe';
  SGMusica.Cells[02, 0] := 'Linha';
  SGMusica.Cells[03, 0] := 'SeqLin';
  SGMusica.Cells[04, 0] := 'Tempo I';
  SGMusica.Cells[05, 0] := 'Tam t';
  SGMusica.Cells[06, 0] := 'TextoSom';
  SGMusica.Cells[07, 0] := 'TextoImp';
  SGMusica.Cells[08, 0] := 'CorGray';
  SGMusica.Cells[09, 0] := 'CorPrev';
  SGMusica.Cells[10, 0] := 'CorShow';
  SGMusica.Cells[11, 0] := '';
  //
  CBFontName.FonteNome := 'Tahoma';

  //

  SGEstrofes.ColWidths[0] := 32;
  SGEstrofes.Cells[0, 0] := 'Seq';
  //
  SGEstrofes.ColWidths[1] := 32;
  SGEstrofes.Cells[1, 0] := 'N�mero';
  //
  SGEstrofes.ColWidths[2] := 72;
  SGEstrofes.Cells[2, 0] := 'In�cio';
  //
  SGEstrofes.ColWidths[3] := 72;
  SGEstrofes.Cells[3, 0] := 'Fim';
  //
  SGEstrofes.ColWidths[4] := 160;
  SGEstrofes.Cells[4, 0] := 'Nome da Estrofe';

  //


  SGLinhas.ColWidths[0] := 32;
  SGLinhas.Cells[0, 0] := 'Seq';
  //
  SGLinhas.ColWidths[1] := 32;
  SGLinhas.Cells[1, 0] := 'Lin';
  //
  SGLinhas.ColWidths[2] := 400;
  SGLinhas.Cells[2, 0] := 'Texto';
  //

  //


  SGSons.ColWidths[0] := 32;
  SGSons.Cells[0, 0] := 'Seq';
  //
  SGSons.ColWidths[1] := 32;
  SGSons.Cells[1, 0] := 'Lin';
  //
  SGSons.ColWidths[2] := 72;
  SGSons.Cells[02, 0] := 'Tempo I';
  //
  SGSons.ColWidths[3] := 72;
  SGSons.Cells[03, 0] := 'Tam t';
  //
  SGSons.ColWidths[4] := 100;
  SGSons.Cells[4, 0] := 'TextoSom';
  //
  SGSons.ColWidths[5] := 100;
  SGSons.Cells[05, 0] := 'TextoImp';
  //
  SGSons.ColWidths[6] := 72;
  SGSons.Cells[06, 0] := 'CorGray';
  //
  SGSons.ColWidths[7] := 72;
  SGSons.Cells[07, 0] := 'CorPrev';
  //
  SGSons.ColWidths[8] := 72;
  SGSons.Cells[08, 0] := 'CorShow';
end;

procedure TFmEditMusica.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEditMusica.FormShow(Sender: TObject);
begin
//  SGLinhas.Visible := False;
//  SGSons.Visible := False;
end;

procedure TFmEditMusica.Incrementatempoemtodossons1Click(Sender: TObject);
var
  Texto: String;
  Inteiro, Incremento, I: Integer;
  Tempo: TTime;
  Som: TSom;
  OK: Boolean;
begin
  OK := False;
  //Tempo   := Geral.StrToTimeZZZ(TdmkEdit(Sender).Text);
  //Inteiro := Round(Tempo * 86400000);
  DefineSelecionados();
  Som := FLinha.Sons[0];
  if Som <> nil then
  begin
    //Inteiro := Som.TempoIni;
    if InputQuery('Incremento em Milisegunos', 'Informe o tempo de incremento em milisegundos',
    Texto) then
    begin
      Incremento := Geral.IMV(Texto);
      if Incremento <> 0 then
      begin
        //Inteiro := Inteiro + Incremento;
        //
        for I := Low(FLinha.Sons) to High(FLinha.Sons) do
        begin
          FLinha.Sons[I].TempoIni := FLinha.Sons[I].TempoIni + Incremento
        end;
        //
        MostraSonsDaLinha(FnEst, FnLin, FnSom);
        OK := True;
      end;
    end;
  end;
  if not OK then
    Geral.MB_Aviso('Incremento n�o realizado!');
end;

procedure TFmEditMusica.Incrementatempoemtodossonsapartirdaqui1Click(
  Sender: TObject);
var
  Texto: String;
  Inteiro, Incremento, I: Integer;
  Tempo: TTime;
  Som: TSom;
  OK: Boolean;
begin
  OK := False;
  //Tempo   := Geral.StrToTimeZZZ(TdmkEdit(Sender).Text);
  //Inteiro := Round(Tempo * 86400000);
  DefineSelecionados();
  Som := FLinha.Sons[FnSom];
  if Som <> nil then
  begin
    //Inteiro := Som.TempoIni;
    if InputQuery('Incremento em Milisegunos', 'Informe o tempo de incremento em milisegundos',
    Texto) then
    begin
      Incremento := Geral.IMV(Texto);
      if Incremento <> 0 then
      begin
        //Inteiro := Inteiro + Incremento;
        //
        for I := FnSom - 1 to High(FLinha.Sons) do
        begin
          FLinha.Sons[I].TempoIni := FLinha.Sons[I].TempoIni + Incremento
        end;
        //
        MostraSonsDaLinha(FnEst, FnLin, FnSom);
        OK := True;
      end;
    end;
  end;
  if not OK then
    Geral.MB_Aviso('Incremento n�o realizado!');
end;

procedure TFmEditMusica.Incrementetempoemtodaslinhas1Click(Sender: TObject);
var
  Musica: TMusica;
  Estrofe: TEstrofe;
  Linha: TLinha;
  Som: TSom;
  I, J(*, nEst*): Integer;
  //
  Texto: String;
  Incremento: Integer;
  OK: Boolean;
begin
  OK := False;
  DefineSelecionados();
  Musica  := FmPrincipal.FMusicas[FMusicaSel];
  if Length(Musica.Estrofes) = 0 then Exit;
  Estrofe := Musica.Estrofes[FnEst - 1];
  //
  if InputQuery('Incremento em Milisegunos', 'Informe o tempo de incremento em milisegundos',
  Texto) then
  begin
    Incremento := Geral.IMV(Texto);
    if Incremento <> 0 then
    begin
      for I := Low(Estrofe.Linhas) to High(Estrofe.Linhas) do
      begin
        Linha := Estrofe.Linhas[I];
          //
        for J := Low(Linha.Sons) to High(Linha.Sons) do
        begin
          Linha.Sons[J].TempoIni := Linha.Sons[J].TempoIni + Incremento
        end;
      end;
    end;
    MostraSonsDaLinha(FnEst, FnLin, FnSom);
    OK := True;
  end;
  if not OK then
    Geral.MB_Aviso('Incremento n�o realizado!');
end;

procedure TFmEditMusica.InsereNovaEstrofe(Como: TVaiPara);
var
  Estrofe: TEstrofe;
  Linha: TLinha;
  Som: TSom;
  I, P, Q, nSom: Integer;
begin
  DefineSelecionados();
  if Como <> vpThis then
  begin
    Q := DefineIndice(Como, FnEst, Length(FMusica.Estrofes));
    //
    New(Estrofe);
    Estrofe.Musica := FMusicaSel;
    Estrofe.NoEst  := '';
    Estrofe.TempoHide := 0;
    Estrofe.TempoShow := 0;
    Estrofe.CodEst := Q + 1;
    //Estrofe.CodItens
    //
    P := 1;
    New(Linha);
    //
    Linha.Musica   := FMusicaSel;
    Linha.Estrofe  := FnEst;
    //
    Linha.CodLinha := FnLin;
    //Linha.CodItens := 1; // criado vazio!?
    //
    New(Som);
    //
    Som.Musica   := FMusicaSel;
    Som.Estrofe  := FnEst;
    Som.Linha    := FnLin;
    //
    Som.SeqInLin := 1;
    //
    Som.TempoIni := 0;
    Som.TempoTam := 0;
    Som.TextoSom := '';
    Som.TextoImp := '';
    Som.CorGray  := 8421504;
    Som.CorPrev  := 33023;
    Som.CorShow  := 255;
    //
    //
    Arr_PF.InsertSom(Linha.Sons, 0, Som);
    //
    Arr_PF.InsertLinha(FEstrofe.Linhas, P, Linha);
    //
    Arr_PF.InsertEstrofe(FMusica.Estrofes, Q, Estrofe);
    //
    MostraEstrofesDaMusica(FnEst);
    MostraLinhasDaEstrofe(FnEst, FnLin);
    //SGLinhas.Row := FnLin;
    DefineSelecionados();
    MostraSonsDaLinha(FnEst, 1, 1);
  end;
end;

procedure TFmEditMusica.InsereNovaLinha(Como: TVaiPara);
var
  Linha: TLinha;
  Som: TSom;
  I, P, nSom: Integer;
begin
  DefineSelecionados();
  if Como <> vpThis then
  begin
    P := DefineIndice(Como, FnLin, Length(FEstrofe.Linhas));
    //
    New(Linha);
    //
    Linha.Musica   := FMusicaSel;
    Linha.Estrofe  := FnEst;
    //
    Linha.CodLinha := FnLin;
    //Linha.CodItens := 1; // criado vazio!?
    //
    New(Som);
    //
    Som.Musica   := FMusicaSel;
    Som.Estrofe  := FnEst;
    Som.Linha    := FnLin;
    //
    Som.SeqInLin := 1;
    //
    Som.TempoIni := 0;
    Som.TempoTam := 0;
    Som.TextoSom := '';
    Som.TextoImp := '';
    Som.CorGray  := 8421504;
    Som.CorPrev  := 33023;
    Som.CorShow  := 255;
    //
    //
    Arr_PF.InsertSom(Linha.Sons, 0, Som);
    //
    Arr_PF.InsertLinha(FEstrofe.Linhas, P, Linha);
    //
    MostraLinhasDaEstrofe(FnEst, FnLin);
    //SGLinhas.Row := FnLin;
    DefineSelecionados();
    MostraSonsDaLinha(FnEst, FnLin, 1);
  end;
end;

procedure TFmEditMusica.InsereNovoSom(Como: TVaiPara);
var
  Som: TSom;
  I, P, nSom: Integer;
begin
  DefineSelecionados();
  if Como <> vpThis then
  begin
    P := DefineIndice(Como, FnSom, Length(FLinha.Sons));
    //
    New(Som);
    //
    Som.Musica   := FMusicaSel;
    Som.Estrofe  := FnEst;
    Som.Linha    := FnLin;
    //
    Som.SeqInLin := P;
    //
    if P > 0 then
      Som.TempoIni := FLinha.Sons[P-1].TempoIni + 300   // milisegundos
    else
      Som.TempoIni := 0;
    Som.TempoTam := 0;
    Som.TextoSom := '';
    Som.TextoImp := '';



    Som.CorGray  := FmEditSom.ShCorGray.Brush.Color;
    Som.CorPrev  := FmEditSom.ShCorPrev.Brush.Color;
    Som.CorShow  := FmEditSom.ShCorShow.Brush.Color;
    //
    Arr_PF.InsertSom(FLinha.Sons, P, Som);
    //
    MostraSonsDaLinha(FnEst, FnLin, FnSom);
    DefineSelecionados();
    //
    nSom := P + 1;
  end else
    nSom := FnSom;

  //

  begin
    FmEditSom.FMusica                 := FMusica;
    FmEditSom.FEstrofe                := FEstrofe;
    FmEditSom.FLinha                  := FLinha;
    FmEditSom.FSom                    := Som;
    //
    FmEditSom.EdControle.ValueVariant := nSom;
    FmEditSom.EdNome.ValueVariant     := Som.TextoSom;
    //
    FmEditSom.EdSeqSom.ValueVariant   := nSom - 1;
    //
    FmEditSom.EdTempoIni.ValueVariant := Geral.FDT(Som.TempoIni / 86400000, 111);
    FmEditSom.EdTempoTam.ValueVariant := Geral.FDT(Som.TempoTam / 86400000, 111);
    FmEditSom.EdTextoSom.ValueVariant := Som.TextoSom;
    FmEditSom.EdTextoImp.ValueVariant := Som.TextoImp;
    FmEditSom.ShCorGray.Brush.Color   := Som.CorGray;
    FmEditSom.ShCorPrev.Brush.Color   := Som.CorPrev;
    FmEditSom.ShCorShow.Brush.Color   := Som.CorShow;
    //
    FmEditSom.Show;
    //
    FmEditSom.EdTempoIni.SetFocus;
  end;
(*
  case Como of
    vpFirst, vpPrior, vpNext, vpLast, vpThis
*)
end;

procedure TFmEditMusica.MeLyricsChange(Sender: TObject);
begin
//  EdLinha.ValueVariant := MeLyrics.Perform(EM_LINEFROMCHAR, MeLYrics.SelStart, 0);
end;

procedure TFmEditMusica.MeLyricsClick(Sender: TObject);
begin
//  EdLinha.ValueVariant := MeLyrics.Perform(EM_LINEFROMCHAR, MeLYrics.SelStart, 0);
end;

procedure TFmEditMusica.MeLyricsEnter(Sender: TObject);
begin
//  EdLinha.ValueVariant := MeLyrics.Perform(EM_LINEFROMCHAR, MeLYrics.SelStart, 0);
end;

procedure TFmEditMusica.MeLyricsKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
//  EdLinha.ValueVariant := MeLyrics.Perform(EM_LINEFROMCHAR, MeLYrics.SelStart, 0);
end;

procedure TFmEditMusica.MostraEstrofesDaMusica(nEst: Integer);
var
  I, J, N: Integer;
  NoEst: String;
  TempoShow, TempoHide: TTime;
begin
  for I := 0 to SGEstrofes.ColCount - 1 do
    for J := 1 to SGEstrofes.RowCount - 1 do
      SGEstrofes.Cells[I, J] := '';
  //
  N := Length(FmPrincipal.FMusicas[FMusicaSel].Estrofes);
  SGEstrofes.ColCount := 5;
  SGEstrofes.RowCount := N + 1;
  for I := 0 to N - 1 do
  begin
    NoEst := FmPrincipal.FMusicas[FMusicaSel].Estrofes[I].NoEst;
    TempoShow := FmPrincipal.FMusicas[FMusicaSel].Estrofes[I].TempoShow;
    TempoHide := FmPrincipal.FMusicas[FMusicaSel].Estrofes[I].TempoHide;
    //
    N := I + 1;
    SGEstrofes.Cells[0, N] := Geral.FF0(I + 1);
    SGEstrofes.Cells[1, N] := Geral.FFN(I + 1, 4);
    SGEstrofes.Cells[2, N] := Geral.FDT(TempoShow, 111);
    SGEstrofes.Cells[3, N] := Geral.FDT(TempoHide, 111);
    SGEstrofes.Cells[4, N] := NoEst;
  end;
  (*
  if SGEstrofes.RowCount = 1 then
  begin
    SGEstrofes.RowCount := 2;
    SGEstrofes.Cells[1,1] := '1';
  end;
  *)if (nEst > 0) and (nEst < SGEstrofes.RowCount)  then
    SGEstrofes.Row := nEst
  else
  if SGEstrofes.RowCount > 1 then
    SGEstrofes.Row := 1;
end;

procedure TFmEditMusica.MostraLinhasDaEstrofe(nEst, nLin: Integer);
var
  Musica: TMusica;
  Estrofe: TEstrofe;
  Linha: TLinha;
  Som: TSom;
  I, J(*, nEst*): Integer;
  TxtLin: String;
begin
  Musica  := FmPrincipal.FMusicas[FMusicaSel];
  //nEst    := Geral.IMV(SGEstrofes.Cells[1, SGEstrofes.Row]);
  if Length(Musica.Estrofes) = 0 then Exit;
  Estrofe := Musica.Estrofes[nEst - 1];
  //
  SGLinhas.RowCount := High(Estrofe.Linhas) + 2;
  for I := Low(Estrofe.Linhas) to High(Estrofe.Linhas) do
  begin
    Linha := Estrofe.Linhas[I];
    //
    TxtLin := '';
    for J := Low(Linha.Sons) to High(Linha.Sons) do
    begin
      Som := Linha.Sons[J];
      TxtLin := TxtLin + Som.TextoImp;
    end;
    SGLinhas.Cells[0, I + 1] := Geral.FF0(I + 1);
    Linha.CodLinha := I + 1;
    SGLinhas.Cells[1, I + 1] := Geral.FF0(Linha.CodLinha);
    SGLinhas.Cells[2, I + 1] := TxtLin;
  end;
  //if SGLinhas.RowCount = 1 then
  if Length(FEstrofe.Linhas) = 0 then
    InsereNovaLinha(vpLast);
  if SGLinhas.RowCount = 1 then
    InsereNovoSom(vpLast);
  if nLin > 0 then
    SGLinhas.Row := nLin
  else
  if SGLinhas.RowCount > 1 then
    SGLinhas.Row := 1;
  SGLinhas.Visible := True;
end;

procedure TFmEditMusica.MostraSonsDaLinha(nEst, nLin, nSom: Integer);
var
  Musica: TMusica;
  Estrofe: TEstrofe;
  Linha: TLinha;
  Som: TSom;
  I, myLin(*, nEst, nLin*): Integer;
begin
  Musica  := FmPrincipal.FMusicas[FMusicaSel];
  //nEst    := Geral.IMV(SGEstrofes.Cells[1, SGEstrofes.Row]);
  if Length(Musica.Estrofes) = 0 then Exit;
  Estrofe := Musica.Estrofes[nEst - 1];
  //nLin    := Geral.IMV(SGLinhas.Cells[1, SGLinhas.Row]);
  if nLin > Length(Estrofe.Linhas) then
    myLin := Length(Estrofe.Linhas)
  else
    myLin := nLin;
  //
  SGSons.Visible := True;
  if nLin > 0 then
  begin
    if Length(Estrofe.Linhas) = 0 then Exit;
    Linha   := Estrofe.Linhas[myLin - 1];
    //
    if Assigned(Linha) then
    begin
      if Assigned(Linha.Sons) then
      begin
        SGSons.RowCount := High(Linha.Sons) + 2;
        if Linha.Sons <> nil then
        begin
          for I := Low(Linha.Sons) to High(Linha.Sons) do
          begin
            Som := Linha.Sons[I];
            SGSons.Cells[0, I + 1] := Geral.FF0(I + 1);
            SGSons.Cells[1, I + 1] := Geral.FF0(I + 1);
            SGSons.Cells[2, I + 1] := Geral.FDT(Som.TempoIni / 86400000, 111);
            SGSons.Cells[3, I + 1] := Geral.FDT(Som.TempoTam / 86400000, 111);
            SGSons.Cells[4, I + 1] := Som.TextoSom;
            SGSons.Cells[5, I + 1] := Som.TextoImp;
            SGSons.Cells[6, I + 1] := Geral.FF0(Som.CorGray);
            SGSons.Cells[7, I + 1] := Geral.FF0(Som.CorPrev);
            SGSons.Cells[8, I + 1] := Geral.FF0(Som.CorShow);
          end;
          SGSons.Row := 1;
        end else
          InsereNovoSom(vpLast);
        //
      end else
        InsereNovoSom(vpLast);
    end;
    if nSom > 0 then
      SGSons.Row := nSom
    else
      SGSons.Row := 1;
    SGSons.Visible := True;
  end;
end;

procedure TFmEditMusica.Mudapaletadecordetodossons1Click(Sender: TObject);
var
  Texto: String;
  Inteiro, Incremento, I: Integer;
  Tempo: TTime;
  Som: TSom;
  OK: Boolean;
begin
  OK := False;
  Som := FLinha.Sons[0];
  if Som <> nil then
  begin
    if not SelecionaPaleta() then
    begin
      for I := Low(FLinha.Sons) to High(FLinha.Sons) do
      begin
        FLinha.Sons[I].CorGray :=  FmSelPaleta.FSelGray;
        FLinha.Sons[I].CorPrev :=  FmSelPaleta.FSelPrev;
        FLinha.Sons[I].CorShow :=  FmSelPaleta.FSelShow;
      end;
      //
      MostraSonsDaLinha(FnEst, FnLin, FnSom);
      OK := True;
    end;
  end;
  if not OK then
    Geral.MB_Aviso('Altera��o n�o realizada!');
end;

procedure TFmEditMusica.Nofinal1Click(Sender: TObject);
begin
  FContinuar := True;
  InsereNovoSom(vpLast);
end;

procedure TFmEditMusica.Nofinal2Click(Sender: TObject);
begin
  InsereNovaLinha(vpLast);
end;

procedure TFmEditMusica.Nofinal3Click(Sender: TObject);
begin
  InsereNovaEstrofe(vpLast);
end;

procedure TFmEditMusica.Noincio1Click(Sender: TObject);
begin
  InsereNovaLinha(vpFirst);
end;

procedure TFmEditMusica.Noincio2Click(Sender: TObject);
begin
  InsereNovaEstrofe(vpFirst);
end;

procedure TFmEditMusica.NoInicio1Click(Sender: TObject);
begin
  InsereNovoSom(vpFirst);
end;

function TFmEditMusica.ObtemSom(var Som: TSom; var Index: Integer): Boolean;
begin
  DefineSelecionados();
  Som   := FSom;
  Index := FnSom;
end;

function TFmEditMusica.SelecionaPaleta(): Boolean;
begin
  Result := False;
  FmSelPaleta.FShemaCores := -1;
  //
  FmSelPaleta.Show;
  if FmSelPaleta.FShemaCores > 0 then
    Result := True;
end;

procedure TFmEditMusica.SGEstrofesClick(Sender: TObject);
begin
  DefineSelecionados();
  MostraLinhasDaEstrofe(FnEst, 1);
  MostraSonsDaLinha(FnEst, 1, 1);
  //
  SGLinhas.Visible := True;
end;

procedure TFmEditMusica.SGEstrofesDblClick(Sender: TObject);
begin
  AlteraEstrofe();
end;

procedure TFmEditMusica.SGLinhasClick(Sender: TObject);
begin
  DefineSelecionados();
  MostraSonsDaLinha(FnEst, FnLin, 1);
  //
  SGSons.Visible := True;
end;

procedure TFmEditMusica.SGMusicaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
(*
  if key = VK_INSERT then
    MyObjects.InsereLinhaStringGrid(SGMusica, True);
  if (key = VK_DELETE) and (ssCtrl in Shift) then
    MyObjects.ExcluiLinhaStringGrid(SGMusica, True);
*)
end;

procedure TFmEditMusica.SGMusicaSetEditText(Sender: TObject; ACol,
  ARow: Integer; const Value: string);
var
  I: Integer;
begin
(*
  for I := ARow + 1 to SGMusica.RowCount do
  begin
    SGMusica.Cells[ACol, I] := Value;
  end;
*)
end;

procedure TFmEditMusica.SGSonsDblClick(Sender: TObject);
var
  I: Integer;
begin
  FmEditMusica.DefineSelecionados();
  //
  FmEditSom.FMusica                 := FMusica;
  FmEditSom.FEstrofe                := FEstrofe;
  FmEditSom.FLinha                  := FLinha;
  FmEditSom.FSom                    := FSom;
  //
  FmEditSom.EdControle.ValueVariant := FnSom;
  FmEditSom.EdNome.ValueVariant     := FSom.TextoSom;
  //
  FmEditSom.EdSeqSom.ValueVariant   := FnSom - 1;
  //
  FmEditSom.EdTempoIni.ValueVariant := Geral.FDT(FSom.TempoIni / 86400000, 111);
  FmEditSom.EdTempoTam.ValueVariant := Geral.FDT(FSom.TempoTam / 86400000, 111);
  FmEditSom.EdTextoSom.ValueVariant := FSom.TextoSom;
  FmEditSom.EdTextoImp.ValueVariant := FSom.TextoImp;
  FmEditSom.ShCorGray.Brush.Color   := FSom.CorGray;
  FmEditSom.ShCorPrev.Brush.Color   := FSom.CorPrev;
  FmEditSom.ShCorShow.Brush.Color   := FSom.CorShow;
  //
  FmEditSom.Show;
  //
  FmEditSom.EdTempoIni.SetFocus;
end;

end.
