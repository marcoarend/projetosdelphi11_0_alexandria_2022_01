object FmEditSom: TFmEditSom
  Left = 339
  Top = 185
  Caption = 'SOM-EDITA-001 :: Edi'#231#227'o de Som'
  ClientHeight = 418
  ClientWidth = 812
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 764
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 716
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 184
        Height = 32
        Caption = 'Edi'#231#227'o de Som'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 184
        Height = 32
        Caption = 'Edi'#231#227'o de Som'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 184
        Height = 32
        Caption = 'Edi'#231#227'o de Som'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 812
    Height = 256
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 812
      Height = 256
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 812
        Height = 256
        Align = alClient
        TabOrder = 0
        object GroupBox2: TGroupBox
          Left = 2
          Top = 15
          Width = 808
          Height = 62
          Align = alTop
          Caption = ' Dados do item: '
          Enabled = False
          TabOrder = 0
          object Label6: TLabel
            Left = 12
            Top = 16
            Width = 14
            Height = 13
            Caption = 'ID:'
          end
          object Label7: TLabel
            Left = 96
            Top = 16
            Width = 28
            Height = 13
            Caption = 'Nome'
          end
          object EdControle: TdmkEdit
            Left = 12
            Top = 32
            Width = 80
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Controle'
            UpdCampo = 'Controle'
            UpdType = utInc
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdNome: TdmkEdit
            Left = 96
            Top = 32
            Width = 509
            Height = 21
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'Nome'
            UpdCampo = 'Nome'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
        object GroupBox3: TGroupBox
          Left = 2
          Top = 77
          Width = 808
          Height = 172
          Align = alTop
          Caption = ' Dados do item: '
          TabOrder = 1
          object Label1: TLabel
            Left = 12
            Top = 16
            Width = 23
            Height = 13
            Caption = 'Item:'
          end
          object Label2: TLabel
            Left = 96
            Top = 16
            Width = 51
            Height = 13
            Caption = 'In'#237'cio [F4]:'
          end
          object Label3: TLabel
            Left = 180
            Top = 16
            Width = 57
            Height = 13
            Caption = 'Tempo: [F4]'
          end
          object Label4: TLabel
            Left = 264
            Top = 16
            Width = 52
            Height = 13
            Caption = 'Texto som:'
          end
          object Label5: TLabel
            Left = 428
            Top = 16
            Width = 101
            Height = 13
            Caption = 'Texto impress'#227'o: [F4]'
          end
          object EdSeqSom: TdmkEdit
            Left = 12
            Top = 32
            Width = 80
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Controle'
            UpdCampo = 'Controle'
            UpdType = utInc
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdTextoSom: TdmkEdit
            Left = 264
            Top = 32
            Width = 161
            Height = 21
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'Nome'
            UpdCampo = 'Nome'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnKeyDown = EdTextoImpKeyDown
            OnRedefinido = EdTextoSomRedefinido
          end
          object EdTempoIni: TdmkEdit
            Left = 96
            Top = 32
            Width = 80
            Height = 21
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfMiliSeconds
            Texto = '0:00:00.000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = '0:00:00.000'
            ValWarn = False
            OnKeyDown = EdTempoIniKeyDown
          end
          object EdTempoTam: TdmkEdit
            Left = 180
            Top = 32
            Width = 80
            Height = 21
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfMiliSeconds
            Texto = '0:00:00.000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = '0:00:00.000'
            ValWarn = False
            OnKeyDown = EdTempoTamKeyDown
          end
          object EdTextoImp: TdmkEdit
            Left = 428
            Top = 32
            Width = 161
            Height = 21
            TabOrder = 4
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'Nome'
            UpdCampo = 'Nome'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnKeyDown = EdTextoImpKeyDown
          end
          object GroupBox5: TGroupBox
            Left = 2
            Top = 59
            Width = 804
            Height = 111
            Align = alBottom
            Caption = ' Cores do som:'
            TabOrder = 5
            ExplicitLeft = 6
            object Label8: TLabel
              Left = 12
              Top = 24
              Width = 30
              Height = 13
              Caption = 'Antes:'
            end
            object Label9: TLabel
              Left = 12
              Top = 50
              Width = 29
              Height = 13
              Caption = 'Aviso:'
            end
            object Label10: TLabel
              Left = 12
              Top = 78
              Width = 31
              Height = 13
              Caption = 'Canto:'
            end
            object SbTitCorC: TSpeedButton
              Left = 84
              Top = 20
              Width = 24
              Height = 24
              Caption = '...'
              OnClick = SbTitCorCClick
            end
            object ShCorGray: TShape
              Left = 112
              Top = 20
              Width = 24
              Height = 24
              Pen.Style = psClear
              Pen.Width = 0
            end
            object LaTitCorC: TLabel
              Left = 140
              Top = 24
              Width = 47
              Height = 13
              AutoSize = False
              Caption = '[Nenhum]'
            end
            object SbTitCorA: TSpeedButton
              Left = 84
              Top = 48
              Width = 24
              Height = 24
              Caption = '...'
              OnClick = SbTitCorAClick
            end
            object ShCorPrev: TShape
              Left = 112
              Top = 48
              Width = 24
              Height = 24
              Pen.Style = psClear
              Pen.Width = 0
            end
            object LaTitCorA: TLabel
              Left = 140
              Top = 50
              Width = 46
              Height = 13
              Caption = '[Nenhum]'
            end
            object SbTitCorB: TSpeedButton
              Left = 84
              Top = 76
              Width = 24
              Height = 24
              Caption = '...'
              OnClick = SbTitCorBClick
            end
            object ShCorShow: TShape
              Left = 112
              Top = 76
              Width = 24
              Height = 24
              Pen.Style = psClear
              Pen.Width = 0
            end
            object LaTitCorB: TLabel
              Left = 140
              Top = 78
              Width = 46
              Height = 13
              Caption = '[Nenhum]'
            end
            object Shape01: TShape
              Left = 224
              Top = 20
              Width = 24
              Height = 24
              Brush.Color = 10461151
              Pen.Style = psClear
              Pen.Width = 0
              OnMouseUp = Shape01MouseUp
            end
            object Shape11: TShape
              Left = 224
              Top = 48
              Width = 24
              Height = 24
              Brush.Color = 33023
              Pen.Style = psClear
              Pen.Width = 0
              OnMouseUp = Shape01MouseUp
            end
            object Shape21: TShape
              Left = 224
              Top = 76
              Width = 24
              Height = 24
              Brush.Color = clRed
              Pen.Style = psClear
              Pen.Width = 0
              OnMouseUp = Shape01MouseUp
            end
            object Shape22: TShape
              Left = 252
              Top = 76
              Width = 24
              Height = 24
              Brush.Color = clGreen
              Pen.Style = psClear
              Pen.Width = 0
              OnMouseUp = Shape01MouseUp
            end
            object Shape12: TShape
              Left = 252
              Top = 48
              Width = 24
              Height = 24
              Brush.Color = 65408
              Pen.Style = psClear
              Pen.Width = 0
              OnMouseUp = Shape01MouseUp
            end
            object Shape02: TShape
              Left = 252
              Top = 20
              Width = 24
              Height = 24
              Brush.Color = 8311721
              Pen.Style = psClear
              Pen.Width = 0
              OnMouseUp = Shape01MouseUp
            end
            object Shape03: TShape
              Left = 280
              Top = 20
              Width = 24
              Height = 24
              Brush.Color = 13860478
              Pen.Style = psClear
              Pen.Width = 0
              OnMouseUp = Shape01MouseUp
            end
            object Shape13: TShape
              Left = 280
              Top = 48
              Width = 24
              Height = 24
              Brush.Color = 16733011
              Pen.Style = psClear
              Pen.Width = 0
              OnMouseUp = Shape01MouseUp
            end
            object Shape23: TShape
              Left = 280
              Top = 76
              Width = 24
              Height = 24
              Brush.Color = clBlue
              Pen.Style = psClear
              Pen.Width = 0
              OnMouseUp = Shape01MouseUp
            end
            object Shape24: TShape
              Left = 308
              Top = 76
              Width = 24
              Height = 24
              Brush.Color = clFuchsia
              Pen.Style = psClear
              Pen.Width = 0
              OnMouseUp = Shape01MouseUp
            end
            object Shape14: TShape
              Left = 308
              Top = 48
              Width = 24
              Height = 24
              Brush.Color = 12615935
              Pen.Style = psClear
              Pen.Width = 0
              OnMouseUp = Shape01MouseUp
            end
            object Shape04: TShape
              Left = 308
              Top = 20
              Width = 24
              Height = 24
              Brush.Color = 13610959
              Pen.Style = psClear
              Pen.Width = 0
              OnMouseUp = Shape01MouseUp
            end
            object Shape05: TShape
              Left = 336
              Top = 20
              Width = 24
              Height = 24
              Brush.Color = 14655391
              Pen.Style = psClear
              Pen.Width = 0
              OnMouseUp = Shape01MouseUp
            end
            object Shape15: TShape
              Left = 336
              Top = 48
              Width = 24
              Height = 24
              Brush.Color = 16744576
              Pen.Style = psClear
              Pen.Width = 0
              OnMouseUp = Shape01MouseUp
            end
            object Shape25: TShape
              Left = 336
              Top = 76
              Width = 24
              Height = 24
              Brush.Color = 16711808
              Pen.Style = psClear
              Pen.Width = 0
              OnMouseUp = Shape01MouseUp
            end
            object Shape26: TShape
              Left = 364
              Top = 76
              Width = 24
              Height = 24
              Brush.Color = 11691520
              Pen.Style = psClear
              Pen.Width = 0
              OnMouseUp = Shape01MouseUp
            end
            object Shape16: TShape
              Left = 364
              Top = 48
              Width = 24
              Height = 24
              Brush.Color = 16752672
              Pen.Style = psClear
              Pen.Width = 0
              OnMouseUp = Shape01MouseUp
            end
            object Shape06: TShape
              Left = 364
              Top = 20
              Width = 24
              Height = 24
              Brush.Color = 13080664
              Pen.Style = psClear
              Pen.Width = 0
              OnMouseUp = Shape01MouseUp
            end
            object Shape07: TShape
              Left = 392
              Top = 20
              Width = 24
              Height = 24
              Brush.Color = clSilver
              Pen.Style = psClear
              Pen.Width = 0
              OnMouseUp = Shape01MouseUp
            end
            object Shape17: TShape
              Left = 392
              Top = 48
              Width = 24
              Height = 24
              Brush.Color = clGray
              Pen.Style = psClear
              Pen.Width = 0
              OnMouseUp = Shape01MouseUp
            end
            object Shape27: TShape
              Left = 392
              Top = 76
              Width = 24
              Height = 24
              Brush.Color = -1
              Pen.Style = psClear
              Pen.Width = 0
              OnMouseUp = Shape01MouseUp
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 304
    Width = 812
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 808
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 348
    Width = 812
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 666
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 15
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 664
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object ColorDialog1: TColorDialog
    Left = 368
    Top = 292
  end
end
