object FmEditMusica: TFmEditMusica
  Left = 339
  Top = 185
  Caption = 'MUS-EDITA-001 :: Edi'#231#227'o de M'#250'sica'
  ClientHeight = 767
  ClientWidth = 977
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 977
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 929
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 881
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 216
        Height = 32
        Caption = 'Edi'#231#227'o de M'#250'sica'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 216
        Height = 32
        Caption = 'Edi'#231#227'o de M'#250'sica'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 216
        Height = 32
        Caption = 'Edi'#231#227'o de M'#250'sica'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 977
    Height = 605
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 977
      Height = 605
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 977
        Height = 605
        Align = alClient
        TabOrder = 0
        object PnLyrics: TPanel
          Left = 2
          Top = 15
          Width = 973
          Height = 588
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Panel5: TPanel
            Left = 0
            Top = 539
            Width = 973
            Height = 32
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 0
            Visible = False
            object Label11: TLabel
              Left = 84
              Top = 12
              Width = 29
              Height = 13
              Caption = 'Linha:'
            end
            object Button2: TButton
              Left = 4
              Top = 4
              Width = 75
              Height = 25
              Caption = 'Incremento'
              TabOrder = 0
              OnClick = Button2Click
            end
            object EdLinha: TdmkEdit
              Left = 116
              Top = 8
              Width = 53
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
          end
          object MeLyrics: TMemo
            Left = 0
            Top = 317
            Width = 973
            Height = 12
            Align = alTop
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Courier New'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            Visible = False
            OnChange = MeLyricsChange
            OnClick = MeLyricsClick
            OnEnter = MeLyricsEnter
            OnKeyUp = MeLyricsKeyUp
          end
          object PB1: TProgressBar
            Left = 0
            Top = 571
            Width = 973
            Height = 17
            Align = alBottom
            TabOrder = 2
          end
          object SGMusica: TStringGrid
            Left = 0
            Top = 329
            Width = 973
            Height = 20
            Align = alTop
            ColCount = 11
            Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
            TabOrder = 3
            Visible = False
            OnKeyDown = SGMusicaKeyDown
            OnSetEditText = SGMusicaSetEditText
          end
          object Panel6: TPanel
            Left = 0
            Top = 0
            Width = 973
            Height = 317
            Align = alTop
            TabOrder = 4
            object Panel7: TPanel
              Left = 1
              Top = 189
              Width = 971
              Height = 127
              Align = alClient
              TabOrder = 1
              object Label3: TLabel
                Left = 8
                Top = 0
                Width = 120
                Height = 13
                Caption = 'Nome do Hin'#225'rio / M'#237'dia:'
              end
              object Label2: TLabel
                Left = 8
                Top = 40
                Width = 129
                Height = 13
                Caption = 'N'#250'mero e nome da m'#250'sica:'
              end
              object Label7: TLabel
                Left = 512
                Top = 40
                Width = 87
                Height = 13
                Caption = 'Data cria'#231#227'o letra:'
              end
              object EdNomeHinario: TdmkEdit
                Left = 8
                Top = 16
                Width = 617
                Height = 21
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdCodMus: TdmkEdit
                Left = 8
                Top = 56
                Width = 80
                Height = 21
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object EdNomeMusica: TdmkEdit
                Left = 92
                Top = 56
                Width = 417
                Height = 21
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object TPDataLetra: TdmkEditDateTimePicker
                Left = 512
                Top = 56
                Width = 112
                Height = 21
                Date = 42551.538749224540000000
                Time = 42551.538749224540000000
                TabOrder = 3
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                UpdType = utYes
              end
            end
            object Panel8: TPanel
              Left = 1
              Top = 105
              Width = 971
              Height = 84
              Align = alTop
              TabOrder = 0
              object Label6: TLabel
                Left = 308
                Top = 40
                Width = 111
                Height = 13
                Caption = 'Nome da m'#237'dia original:'
              end
              object Label5: TLabel
                Left = 8
                Top = 0
                Width = 118
                Height = 13
                Caption = 'Nome da m'#250'sica original:'
              end
              object Label4: TLabel
                Left = 8
                Top = 40
                Width = 176
                Height = 13
                Caption = 'Nome artista dono da musica original:'
              end
              object EdNoOriMidia: TdmkEdit
                Left = 308
                Top = 56
                Width = 317
                Height = 21
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdNoOriMusica: TdmkEdit
                Left = 8
                Top = 16
                Width = 617
                Height = 21
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdNoOriArtista: TdmkEdit
                Left = 8
                Top = 56
                Width = 293
                Height = 21
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
            end
            object GroupBox2: TGroupBox
              Left = 1
              Top = 1
              Width = 971
              Height = 104
              Align = alTop
              Caption = ' Configura'#231#227'o do texto:'
              TabOrder = 2
              object Panel9: TPanel
                Left = 2
                Top = 15
                Width = 507
                Height = 87
                Align = alLeft
                TabOrder = 0
                object Label1: TLabel
                  Left = 8
                  Top = 4
                  Width = 76
                  Height = 13
                  Caption = 'Nome da Fonte:'
                end
                object Label8: TLabel
                  Left = 276
                  Top = 4
                  Width = 90
                  Height = 13
                  Caption = 'Tamanho da fonte:'
                end
                object Label9: TLabel
                  Left = 372
                  Top = 4
                  Width = 70
                  Height = 13
                  Caption = 'Altura da linha:'
                end
                object Label10: TLabel
                  Left = 8
                  Top = 44
                  Width = 224
                  Height = 13
                  Caption = 'Pasta onde se encontra os arquivos da m'#250'sica:'
                end
                object CBFontName: TdmkPopOutFntCBox
                  Left = 8
                  Top = 20
                  Width = 265
                  Height = 22
                  Font.Charset = ANSI_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  TabOrder = 0
                  MoveUsedToTop = True
                  PopUpHeight = 75
                  FonteNome = 'MS Sans Serif'
                  UpdType = utYes
                end
                object CkNegrito: TCheckBox
                  Left = 444
                  Top = 20
                  Width = 57
                  Height = 17
                  Alignment = taLeftJustify
                  Caption = 'Negrito'
                  Checked = True
                  State = cbChecked
                  TabOrder = 3
                end
                object EdLablAltu: TdmkEdit
                  Left = 372
                  Top = 20
                  Width = 69
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 2
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '30'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 30
                  ValWarn = False
                end
                object CBFontSize: TComboBox
                  Left = 277
                  Top = 20
                  Width = 88
                  Height = 21
                  TabOrder = 1
                  Text = '24'
                  Items.Strings = (
                    '5'
                    '6'
                    '7'
                    '8'
                    '9'
                    '10'
                    '11'
                    '12'
                    '14'
                    '16'
                    '18'
                    '20'
                    '22'
                    '24'
                    '26'
                    '28'
                    '36'
                    '48'
                    '72')
                end
                object EdPastaMusica: TdmkEdit
                  Left = 8
                  Top = 60
                  Width = 492
                  Height = 21
                  ReadOnly = True
                  TabOrder = 4
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
              end
              object RGEstrPorTela: TdmkRadioGroup
                Left = 509
                Top = 15
                Width = 460
                Height = 87
                Align = alClient
                Caption = 'Estrofes simull'#226'neas:'
                Columns = 2
                ItemIndex = 1
                Items.Strings = (
                  '1'
                  '2'
                  '3'
                  '4')
                TabOrder = 1
                UpdType = utYes
                OldValor = 0
              end
            end
          end
          object SGEstrofes: TdmkStringGrid
            Left = 0
            Top = 349
            Width = 400
            Height = 190
            Align = alLeft
            Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine]
            PopupMenu = PMEstrofes
            TabOrder = 5
            OnClick = SGEstrofesClick
            OnDblClick = SGEstrofesDblClick
          end
          object SGLinhas: TdmkStringGrid
            Left = 400
            Top = 349
            Width = 488
            Height = 190
            Align = alLeft
            ColCount = 3
            Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine]
            PopupMenu = PMLinhas
            TabOrder = 6
            Visible = False
            OnClick = SGLinhasClick
            RowHeights = (
              24
              24
              24
              24
              24)
          end
          object SGSons: TdmkStringGrid
            Left = 888
            Top = 349
            Width = 85
            Height = 190
            Align = alClient
            ColCount = 9
            Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine]
            PopupMenu = PMSons
            TabOrder = 7
            Visible = False
            OnDblClick = SGSonsDblClick
            RowHeights = (
              24
              24
              24
              24
              24)
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 653
    Width = 977
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 973
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 697
    Width = 977
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 831
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 829
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Salvar'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object PMSons: TPopupMenu
    Left = 900
    Top = 524
    object Inserirnovosomacima1: TMenuItem
      Caption = 'Inserir novo som'
      object NoInicio1: TMenuItem
        Caption = 'No in'#237'cio'
        OnClick = NoInicio1Click
      end
      object Acimadalinha1: TMenuItem
        Caption = 'Acima da linha'
        OnClick = Acimadalinha1Click
      end
      object Abaixodalinha1: TMenuItem
        Caption = 'Abaixo da linha'
        OnClick = Abaixodalinha1Click
      end
      object Nofinal1: TMenuItem
        Caption = 'No final'
        OnClick = Nofinal1Click
      end
    end
    object Excluirsomselecionado1: TMenuItem
      Caption = 'Excluir som selecionado'
      OnClick = Excluirsomselecionado1Click
    end
    object Incrementatempoemtodossonsapartirdaqui1: TMenuItem
      Caption = 'Incrementa tempo em todos sons a partir daqui'
      OnClick = Incrementatempoemtodossonsapartirdaqui1Click
    end
  end
  object PMLinhas: TPopupMenu
    Left = 572
    Top = 532
    object Inserirnovalinha1: TMenuItem
      Caption = '&Inserir nova linha'
      object Noincio1: TMenuItem
        Caption = 'No in'#237'cio'
        OnClick = Noincio1Click
      end
      object Acimadalinha2: TMenuItem
        Caption = 'Acima da linha'
        OnClick = Acimadalinha2Click
      end
      object Abaixodalinha2: TMenuItem
        Caption = 'Abaixo da linha'
        OnClick = Abaixodalinha2Click
      end
      object Nofinal2: TMenuItem
        Caption = 'No final'
        OnClick = Nofinal2Click
      end
    end
    object Incrementatempoemtodossons1: TMenuItem
      Caption = 'Incrementa &tempo em todos sons'
      OnClick = Incrementatempoemtodossons1Click
    end
    object Excluirlinha1: TMenuItem
      Caption = '&Excluir linha'
      OnClick = Excluirlinha1Click
    end
    object Duplicarlinha1: TMenuItem
      Caption = '&Duplicar linha'
      OnClick = Duplicarlinha1Click
    end
    object Mudapaletadecordetodossons1: TMenuItem
      Caption = 'Muda paleta de cor de todos sons'
      OnClick = Mudapaletadecordetodossons1Click
    end
  end
  object PMEstrofes: TPopupMenu
    Left = 180
    Top = 532
    object Inserenovaestrofe1: TMenuItem
      Caption = '&Insere nova estrofe'
      object Noincio2: TMenuItem
        Caption = 'No in'#237'cio'
        OnClick = Noincio2Click
      end
      object Acimadalinha3: TMenuItem
        Caption = 'Acima da linha'
        OnClick = Acimadalinha3Click
      end
      object Abaixodalinha3: TMenuItem
        Caption = 'Abaixo da linha'
        OnClick = Abaixodalinha3Click
      end
      object Nofinal3: TMenuItem
        Caption = 'No final'
        OnClick = Nofinal3Click
      end
    end
    object Alteraestrofe1: TMenuItem
      Caption = 'Altera estrofe'
      OnClick = Alteraestrofe1Click
    end
    object Excluiestrofe1: TMenuItem
      Caption = 'Exclui estrofe'
      OnClick = Excluiestrofe1Click
    end
    object Incrementetempoemtodaslinhas1: TMenuItem
      Caption = '&Incrementa tempo em todas linhas'
      OnClick = Incrementetempoemtodaslinhas1Click
    end
  end
end
