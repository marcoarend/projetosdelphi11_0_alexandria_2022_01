object FmSelPaleta: TFmSelPaleta
  Left = 339
  Top = 185
  Caption = '???-?????-999 :: ??? ??? ???'
  ClientHeight = 306
  ClientWidth = 292
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 292
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 812
    object GB_R: TGroupBox
      Left = 244
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 764
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 196
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 716
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 76
        Height = 32
        Caption = 'Paleta'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 76
        Height = 32
        Caption = 'Paleta'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 76
        Height = 32
        Caption = 'Paleta'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 292
    Height = 144
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 812
    ExplicitHeight = 467
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 292
      Height = 144
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 812
      ExplicitHeight = 467
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 292
        Height = 144
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 812
        ExplicitHeight = 467
        object Shape01: TShape
          Left = 32
          Top = 32
          Width = 24
          Height = 24
          Brush.Color = 10461151
          Pen.Style = psClear
          Pen.Width = 0
          OnMouseUp = Shape01MouseUp
        end
        object Shape11: TShape
          Left = 32
          Top = 60
          Width = 24
          Height = 24
          Brush.Color = 33023
          Pen.Style = psClear
          Pen.Width = 0
          OnMouseUp = Shape01MouseUp
        end
        object Shape21: TShape
          Left = 32
          Top = 88
          Width = 24
          Height = 24
          Brush.Color = clRed
          Pen.Style = psClear
          Pen.Width = 0
          OnMouseUp = Shape01MouseUp
        end
        object Shape22: TShape
          Left = 60
          Top = 88
          Width = 24
          Height = 24
          Brush.Color = clGreen
          Pen.Style = psClear
          Pen.Width = 0
          OnMouseUp = Shape01MouseUp
        end
        object Shape12: TShape
          Left = 60
          Top = 60
          Width = 24
          Height = 24
          Brush.Color = 65408
          Pen.Style = psClear
          Pen.Width = 0
          OnMouseUp = Shape01MouseUp
        end
        object Shape02: TShape
          Left = 60
          Top = 32
          Width = 24
          Height = 24
          Brush.Color = 8311721
          Pen.Style = psClear
          Pen.Width = 0
          OnMouseUp = Shape01MouseUp
        end
        object Shape03: TShape
          Left = 88
          Top = 32
          Width = 24
          Height = 24
          Brush.Color = 13860478
          Pen.Style = psClear
          Pen.Width = 0
          OnMouseUp = Shape01MouseUp
        end
        object Shape13: TShape
          Left = 88
          Top = 60
          Width = 24
          Height = 24
          Brush.Color = 16733011
          Pen.Style = psClear
          Pen.Width = 0
          OnMouseUp = Shape01MouseUp
        end
        object Shape23: TShape
          Left = 88
          Top = 88
          Width = 24
          Height = 24
          Brush.Color = clBlue
          Pen.Style = psClear
          Pen.Width = 0
          OnMouseUp = Shape01MouseUp
        end
        object Shape24: TShape
          Left = 116
          Top = 88
          Width = 24
          Height = 24
          Brush.Color = clFuchsia
          Pen.Style = psClear
          Pen.Width = 0
          OnMouseUp = Shape01MouseUp
        end
        object Shape14: TShape
          Left = 116
          Top = 60
          Width = 24
          Height = 24
          Brush.Color = 12615935
          Pen.Style = psClear
          Pen.Width = 0
          OnMouseUp = Shape01MouseUp
        end
        object Shape04: TShape
          Left = 116
          Top = 32
          Width = 24
          Height = 24
          Brush.Color = 13610959
          Pen.Style = psClear
          Pen.Width = 0
          OnMouseUp = Shape01MouseUp
        end
        object Shape05: TShape
          Left = 144
          Top = 32
          Width = 24
          Height = 24
          Brush.Color = 14655391
          Pen.Style = psClear
          Pen.Width = 0
          OnMouseUp = Shape01MouseUp
        end
        object Shape15: TShape
          Left = 144
          Top = 60
          Width = 24
          Height = 24
          Brush.Color = 16744576
          Pen.Style = psClear
          Pen.Width = 0
          OnMouseUp = Shape01MouseUp
        end
        object Shape25: TShape
          Left = 144
          Top = 88
          Width = 24
          Height = 24
          Brush.Color = 16711808
          Pen.Style = psClear
          Pen.Width = 0
          OnMouseUp = Shape01MouseUp
        end
        object Shape26: TShape
          Left = 172
          Top = 88
          Width = 24
          Height = 24
          Brush.Color = 11691520
          Pen.Style = psClear
          Pen.Width = 0
          OnMouseUp = Shape01MouseUp
        end
        object Shape16: TShape
          Left = 172
          Top = 60
          Width = 24
          Height = 24
          Brush.Color = 16752672
          Pen.Style = psClear
          Pen.Width = 0
          OnMouseUp = Shape01MouseUp
        end
        object Shape06: TShape
          Left = 172
          Top = 32
          Width = 24
          Height = 24
          Brush.Color = 13080664
          Pen.Style = psClear
          Pen.Width = 0
          OnMouseUp = Shape01MouseUp
        end
        object Shape07: TShape
          Left = 200
          Top = 32
          Width = 24
          Height = 24
          Brush.Color = clSilver
          Pen.Style = psClear
          Pen.Width = 0
          OnMouseUp = Shape01MouseUp
        end
        object Shape17: TShape
          Left = 200
          Top = 60
          Width = 24
          Height = 24
          Brush.Color = clGray
          Pen.Style = psClear
          Pen.Width = 0
          OnMouseUp = Shape01MouseUp
        end
        object Shape27: TShape
          Left = 200
          Top = 88
          Width = 24
          Height = 24
          Brush.Color = -1
          Pen.Style = psClear
          Pen.Width = 0
          OnMouseUp = Shape01MouseUp
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 192
    Width = 292
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 515
    ExplicitWidth = 812
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 288
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 808
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 236
    Width = 292
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 559
    ExplicitWidth = 812
    object BtSaida: TBitBtn
      Tag = 13
      Left = 160
      Top = 15
      Width = 120
      Height = 40
      Cursor = crHandPoint
      Caption = '&Desiste'
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtSaidaClick
    end
  end
end
