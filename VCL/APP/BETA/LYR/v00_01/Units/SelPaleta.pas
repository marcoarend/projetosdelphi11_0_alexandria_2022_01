unit SelPaleta;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEditCB,
  dmkDBLookupComboBox, dmkEdit;

type
  TFmSelPaleta = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    LaTitulo1C: TLabel;
    Shape01: TShape;
    Shape11: TShape;
    Shape21: TShape;
    Shape22: TShape;
    Shape12: TShape;
    Shape02: TShape;
    Shape03: TShape;
    Shape13: TShape;
    Shape23: TShape;
    Shape24: TShape;
    Shape14: TShape;
    Shape04: TShape;
    Shape05: TShape;
    Shape15: TShape;
    Shape25: TShape;
    Shape26: TShape;
    Shape16: TShape;
    Shape06: TShape;
    Shape07: TShape;
    Shape17: TShape;
    Shape27: TShape;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Shape01MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
    FShemaCores: Integer;
    FSelGray, FSelPrev, FSelShow: TColor;
  end;

  var
  FmSelPaleta: TFmSelPaleta;

implementation

uses UnMyObjects;

{$R *.DFM}

procedure TFmSelPaleta.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSelPaleta.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSelPaleta.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmSelPaleta.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSelPaleta.Shape01MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  FShemaCores := Geral.IMV(TShape(Sender).Name[7]);
  case FShemaCores of
    1:
    begin
      FSelGray := Shape01.Brush.Color;
      FSelPrev := Shape11.Brush.Color;
      FSelShow := Shape21.Brush.Color;
    end;
    2:
    begin
      FSelGray := Shape02.Brush.Color;
      FSelPrev := Shape12.Brush.Color;
      FSelShow := Shape22.Brush.Color;
    end;
    3:
    begin
      FSelGray := Shape03.Brush.Color;
      FSelPrev := Shape13.Brush.Color;
      FSelShow := Shape23.Brush.Color;
    end;
    4:
    begin
      FSelGray := Shape04.Brush.Color;
      FSelPrev := Shape14.Brush.Color;
      FSelShow := Shape24.Brush.Color;
    end;
    5:
    begin
      FSelGray := Shape05.Brush.Color;
      FSelPrev := Shape15.Brush.Color;
      FSelShow := Shape25.Brush.Color;
    end;
    6:
    begin
      FSelGray := Shape06.Brush.Color;
      FSelPrev := Shape16.Brush.Color;
      FSelShow := Shape26.Brush.Color;
    end;
    7:
    begin
      FSelGray := Shape07.Brush.Color;
      FSelPrev := Shape17.Brush.Color;
      FSelShow := Shape27.Brush.Color;
    end;
  end;
  Close;
end;

end.
