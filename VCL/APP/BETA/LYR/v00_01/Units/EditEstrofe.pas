unit EditEstrofe;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, UnApp_PF, UnArr_PF;

type
  TFmEditEstrofe = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    EdCodEstrofe: TdmkEdit;
    Label6: TLabel;
    EdNoEst: TdmkEdit;
    Label7: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrEntidades: TmySQLQuery;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesCodUsu: TIntegerField;
    QrEntidadesNome: TWideStringField;
    DsEntidades: TDataSource;
    Label2: TLabel;
    EdCodMus: TdmkEdit;
    Label3: TLabel;
    EdNoMusica: TdmkEdit;
    EdTempoShow: TdmkEdit;
    Label1: TLabel;
    EdTempoHide: TdmkEdit;
    Label4: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenCadastro_Com_Itens_ITS(Controle: Integer);
  public
    { Public declarations }
    FMusica: TMusica;
    FEstrofe: TEstrofe;
  end;

  var
  FmEditEstrofe: TFmEditEstrofe;

implementation

uses UnMyObjects, UnInternalConsts, EditMusica;

{$R *.DFM}

procedure TFmEditEstrofe.BtOKClick(Sender: TObject);
var
  nEst: Integer;
begin
  FEstrofe.NoEst     := EdNoEst.ValueVariant;
  FEstrofe.TempoShow := Geral.StrToTimeZZZ(EdTempoShow.ValueVariant);
  FEstrofe.TempoHide := Geral.StrToTimeZZZ(EdTempoHide.ValueVariant);
  //
  nEst := EdCodEstrofe.ValueVariant;
  FmEditMusica.SGEstrofes.Cells[2, nEst] := Geral.FDT(FEstrofe.TempoShow, 111);
  FmEditMusica.SGEstrofes.Cells[3, nEst] := Geral.FDT(FEstrofe.TempoHide, 111);
  FmEditMusica.SGEstrofes.Cells[4, nEst] := FEstrofe.NoEst;
  //
  Hide;
end;

procedure TFmEditEstrofe.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEditEstrofe.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEditEstrofe.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  //UnDmkDAC_PF.AbreQuery(Qr_Sel_, Dmod.MyDB?);
end;

procedure TFmEditEstrofe.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEditEstrofe.ReopenCadastro_Com_Itens_ITS(Controle: Integer);
begin
{
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database?);
    //
    if Controle? <> 0 then
      FQrIts.Locate('Controle?, Controle?, []);
  end;
}
end;

end.
