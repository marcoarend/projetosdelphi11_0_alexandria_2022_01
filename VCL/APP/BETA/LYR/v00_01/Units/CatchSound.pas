unit CatchSound;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit,
  UnArr_PF;

type
  TFmCatchSound = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Grade: TStringGrid;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure GradeSetEditText(Sender: TObject; ACol, ARow: Integer;
      const Value: string);
    procedure GradeSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure InsereSom(nEst, nLin, nSom, Tempo: Integer);
  public
    { Public declarations }
    FMusica: TMusica;
    FLastNum: Integer;
  end;

  var
  FmCatchSound: TFmCatchSound;

implementation

uses EditSom, UnMyObjects, Principal;

{$R *.DFM}

var
  FEstrofe: TEstrofe;
  FLinha: TLinha;
  FSom: TSom;


procedure TFmCatchSound.BtOKClick(Sender: TObject);
var
  I, nEst, qEst, nLin, qLin, nSom, qSom: Integer;
begin
  if Geral.MB_Pergunta('O n�mero da estrofe e linha est�o corretos?') <> IDYES then
    Exit;
  for I := 1 to Grade.RowCount - 2 do
  begin
    nEst := Geral.IMV(Grade.Cells[1, I]) - 1;
    //while not Assigned(FEstrofe) do
    qEst := Length(FMusica.Estrofes);
    while qEst <= nEst do
    begin
      New(FEstrofe);
      FEstrofe.Musica := FMusicaSel;
      FEstrofe.NoEst  := '';
      FEstrofe.TempoHide := 0;
      FEstrofe.TempoShow := 0;
      FEstrofe.CodEst := Length(FMusica.Estrofes) + 1;
      //
      Arr_PF.InsertEstrofe(FMusica.Estrofes, qEst, FEstrofe);
      //
      qEst := Length(FMusica.Estrofes);
    end;
    FEstrofe := FMusica.Estrofes[nEst];
    //
    nLin := Geral.IMV(Grade.Cells[2, I]) - 1;
    //while not Assigned(FLinrofe) do
    qLin := Length(FEstrofe.Linhas);
    while qLin <= nLin do
    begin
      New(FLinha);
      //
      FLinha.Musica   := FMusicaSel;
      FLinha.Estrofe  := nEst;
      //
      FLinha.CodLinha := nLin;
      //
      Arr_PF.InsertLinha(FEstrofe.Linhas, qLin, FLinha);
      //
      qLin := Length(FEstrofe.Linhas);
    end;
    FLinha := FEstrofe.Linhas[nLin];
    //
    nSom := Geral.IMV(Grade.Cells[3, I]) - 1;
    //while not Assigned(FSomrofe) do
    qSom := Length(FLinha.Sons);
    while qSom <= nSom do
    begin
      New(FSom);
      //
      New(FSom);
      //
      FSom.Musica   := FMusicaSel;
      FSom.Estrofe  := nEst;
      FSom.Linha    := nLin;
      //
      FSom.SeqInLin := qSom + 2;
      //
      FSom.TempoIni := 0;
      FSom.TempoTam := 0;
      FSom.TextoSom := '';
      FSom.TextoImp := '';
      FSom.CorGray  := FmEditSom.Shape01.Brush.Color;
      FSom.CorPrev  := FmEditSom.Shape11.Brush.Color;
      FSom.CorShow  := FmEditSom.Shape21.Brush.Color;
      //
      Arr_PF.InsertSom(FLinha.Sons, qSom, FSom);
      //
      qSom := Length(FLinha.Sons);
    end;
    FSom := FLinha.Sons[nSom];
    FSom.TempoIni := Geral.IMV(Grade.Cells[4, I]);
    FSom.TextoSom := Grade.Cells[5, I];
    FSom.TextoImp := Grade.Cells[5, I];
    //
  end;
  Hide;
end;

procedure TFmCatchSound.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCatchSound.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCatchSound.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmCatchSound.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCatchSound.GradeSelectCell(Sender: TObject; ACol, ARow: Integer;
  var CanSelect: Boolean);
begin
  if ACol in ([1,2]) then
    FLastNum:= Geral.IMV(Grade.Cells[ACol, ARow]);
end;

procedure TFmCatchSound.GradeSetEditText(Sender: TObject; ACol, ARow: Integer;
  const Value: string);
var
  I, K, N: Integer;
begin
  if ACol in ([1,2]) then
  begin
    K := Geral.IMV(Value) - FLastNum;
    for I := ARow + 1 to Grade.RowCount do
    begin
      N := Geral.IMV(Grade.Cells[ACol, I]);
      N := N + K;
      Grade.Cells[ACol, I] := Geral.FF0(N);
    end;
  end;
end;

procedure TFmCatchSound.InsereSom(nEst, nLin, nSom, Tempo: Integer);
begin
(*
  Screen.Cursor := crHourGlass;
  try
    FSom.TempoIni      := Tempo;
    FSom.TempoTam      := 0;
    FSom.TextoSom      := EdTextoSom.ValueVariant;
    FSom.TextoImp      := EdTextoImp.ValueVariant;
    FSom.CorGray       := ShCorGray.Brush.Color;
    FSom.CorPrev       := FmEditSom.Shape01.Brush.Color;
    FSom.CorShow       := ShCorShow.Brush.Color;

    //
    FmEditMusica.SGEstrofes.Row := nEst;
    FmEditMusica.MostraLinhasDaEstrofe(nEst, nLin);
    //FmEditMusica.SGLinhas.Row := nLin;
    FmEditMusica.MostraSonsDaLinha(nEst, nLin, nSom);
    //FmEditMusica.SGSons.Row := nSom;
    //

  finally
    Screen.Cursor := crDefault;
  end;
  //
  Hide;
  if FmEditMusica.FContinuar then
    FmEditMusica.InsereNovoSom(vpLast);
*)
end;

end.
