unit EditMusica2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEditCB,
  dmkDBLookupComboBox, dmkEdit, dmkStringGrid, dmkRadioGroup, (*dmkPopOutFntCBox,*)
  dmkEditDateTimePicker, Vcl.Menus,
  UnApp_PF, UnDmkProcFunc;

type
  TFmEditMusica2 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    PnLyrics: TPanel;
    Panel5: TPanel;
    Label11: TLabel;
    BtSalva: TButton;
    Button2: TButton;
    EdLinha: TdmkEdit;
    BtAltera: TButton;
    MeLyrics: TMemo;
    PB1: TProgressBar;
    SGMusica: TStringGrid;
    Panel6: TPanel;
    Panel7: TPanel;
    Label3: TLabel;
    Label2: TLabel;
    Label7: TLabel;
    EdNomeHinario: TdmkEdit;
    EdCodMus: TdmkEdit;
    EdNomeMusica: TdmkEdit;
    TPDataLetra: TdmkEditDateTimePicker;
    Panel8: TPanel;
    Label6: TLabel;
    Label5: TLabel;
    Label4: TLabel;
    EdNoOriMidia: TdmkEdit;
    EdNoOriMusica: TdmkEdit;
    EdNoOriArtista: TdmkEdit;
    GroupBox2: TGroupBox;
    Panel9: TPanel;
    Label1: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    CBFontName: TdmkPopOutFntCBox;
    CkNegrito: TCheckBox;
    EdLablAltu: TdmkEdit;
    CBFontSize: TComboBox;
    RGEstrPorTela: TdmkRadioGroup;
    SGEstrofes: TdmkStringGrid;
    PMAltera: TPopupMenu;
    Estrofe1: TMenuItem;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure Estrofe1Click(Sender: TObject);
    procedure BtSalvaClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure MeLyricsChange(Sender: TObject);
    procedure MeLyricsClick(Sender: TObject);
    procedure MeLyricsEnter(Sender: TObject);
    procedure MeLyricsKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure SGMusicaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SGMusicaSetEditText(Sender: TObject; ACol, ARow: Integer;
      const Value: string);
  private
    { Private declarations }
    //procedure CriaLabels();
  public
    { Public declarations }
  end;

  var
  FmEditMusica2: TFmEditMusica2;

implementation

uses UnMyObjects, Principal, EditEstrofe;

{$R *.DFM}

procedure TFmEditMusica2.BtAlteraClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAltera, BtAltera);
end;

procedure TFmEditMusica2.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEditMusica2.BtSalvaClick(Sender: TObject);
var
  PastaMae, PastaMusica, NomeMusica, NomeHinario, NoOriMusica, NoOriArtista, NoOriMidia: String;
  DataLetra: TDateTime;
  CodMusica, EstrPorTela: Integer;
  FontName: String; FontSize, LablAltu: Integer;
  FontBold: Boolean;
begin
  //PastaMae     := FmPrincipal.FLyrFile + dmkPF.RemoveExtensaoDeArquivo(ExtractFileName(FmPrincipal.FLyrFile));
  PastaMae     := ExtractFileDir(FmPrincipal.FLyrFile);
  PastaMusica  := dmkPF.RemoveExtensaoDeArquivo(ExtractFileName(FmPrincipal.FLyrFile));
  CodMusica    := EdCodMus.ValueVariant;
  NomeMusica   := EdNomeMusica.ValueVariant;
  NomeHinario  := EdNomeHinario.ValueVariant;
  NoOriMusica  := EdNoOriMusica.ValueVariant;
  NoOriArtista := EdNoOriArtista.ValueVariant;
  NoOriMidia   := EdNoOriMidia.ValueVariant;
  DataLetra    := TPDataLetra.DateTime;
  EstrPorTela  := Geral.IMV(RGEstrPorTela.Items[RGEstrPorTela.ItemIndex]);
  FontName     := CBFontName.FonteNome;
  FontSize     := Geral.IMV(CBFontSize.Text);
  LablAltu     := EdLablAltu.ValueVariant;
  //App_PF.SalvaMusica(FmPrincipal.FSons, FmPrincipal.FLyrFile);
  App_PF.SalvaMusica3(FmPrincipal.FMusicas, FMusicaSel,
    PastaMae, PastaMusica, NomeMusica, NomeHinario, NoOriMusica, NoOriArtista,
    NoOriMidia, CodMusica, DataLetra, EstrPorTela, FontName, FontSize, LablAltu,
    CkNegrito.Checked, SGMusica);
  //
  Geral.SalvaTextoEmArquivo(FmPrincipal.FLyrFile, MeLyrics.Text, True);
  FmPrincipal.CarregaArquivoLyrico_XML(FmPrincipal.FLyrFile, PB1);
(*
  GlobalOSD.Show('I love delphigeist Too 0!!', 4000);
  GlobalOSD.OSDText.Caption := 'I love delphigeist Too 1!!';
  GlobalOSD.OSDText.Invalidate;
  Application.ProcessMessages;
  sleep(1000);
  GlobalOSD.OSDText.Caption := 'I love delphigeist Too 2!!';
  GlobalOSD.OSDText.Invalidate;
  Application.ProcessMessages;
  sleep(1000);
  GlobalOSD.OSDText.Caption := 'I love delphigeist Too 3!!';
  GlobalOSD.OSDText.Invalidate;
  Application.ProcessMessages;
  sleep(1000);
  GlobalOSD.OSDText.Caption := 'I love delphigeist Too 4!!';
  GlobalOSD.OSDText.Invalidate;
  Application.ProcessMessages;
  sleep(1000);
*)
  Hide;
end;

procedure TFmEditMusica2.Button2Click(Sender: TObject);
var
  sIncr: String;
  I, Ini, Fim, iIncr: Integer;
const
  Separador = '|';
var
  N, Colum, Linha, Conta, Tempo, NewTmp: Integer;
  L_Ini, L_Alt, T_Ini, T_Col: Integer;
  TamLin: array of array of Integer;
  Sons: TStringList;
  Lin, Texto, Parte: String;
  //
begin
  sIncr := '0';
  if InputQuery('Incremento de tempo', 'Informe em milisegundos:', sIncr) then
  begin
    iIncr := Geral.IMV(sIncr);
    if iIncr <> 0 then
    begin
      Ini := EdLinha.ValueVariant;
      Fim := MeLyrics.Lines.Count;
      //
      for I := Ini to Fim do
      begin
        Lin := MeLyrics.Lines[I];
        if Lin <> '' then
        begin
          Colum := 0;
          Linha := 0;
          Tempo := 0;
          Texto := '';
          //
          Conta := 0;
          while Length(Lin) > 0 do
          begin
            if Geral.SeparaPrimeiraOcorrenciaDeTexto(Separador, Lin, Parte, Lin) then
            begin
              Conta := Conta + 1;
              case Conta of
                1: Colum := Geral.IMV(Parte);
                2: Linha := Geral.IMV(Parte);
                3: Tempo := Geral.IMV(Parte);
                4: Texto := Parte;
                else begin
                  if Trim(Parte) <> '' then
                  begin
                    Geral.MB_Erro('N�o foi poss�vel definir a vari�vel pai "' +
                    Geral.FF0(I) + '" de resultado (valor):' + sLineBreak + Parte);
                    Screen.Cursor := crDefault;
                    Exit;
                  end;
                end;
              end;
            end;
          end;
          NewTmp := Tempo + iIncr;
          Lin := Geral.FFF(Colum, 2) + '|' + Geral.FFF(Linha, 3) + '|' + Geral.FFF(NewTmp, 6) + '|' + Texto;
          MeLyrics.Lines[I] := Lin;
        end;
      end;
    end;
  end;
end;

{
procedure TFmEditMusica2.CriaLabels();
var
  N, I, Colum, Linha, Conta, Tempo: Integer;
  L_Ini, L_Alt, T_Ini, T_Col: Integer;
  TamLin: array of array of Integer;
  //Lin,
  Texto, Parte: String;
  CorPrev, CorShow: TColor;
  //
  function CriaLabel(Caixa, Coluna, Linha: Integer; Texto: String): Boolean;
  begin
    FmPrincipal.FLabels[Caixa] := TLabel.Create(Self);
    FmPrincipal.FLabels[Caixa].Parent := FmPrincipal.ScrollBox1;
    FmPrincipal.FLabels[Caixa].Tag := Caixa;
    FmPrincipal.FLabels[Caixa].Left := TamLin[Coluna][Linha] + ((Coluna - 1) * T_Col);
    FmPrincipal.FLabels[Caixa].Height := T_Ini + (L_Alt * Caixa);
    FmPrincipal.FLabels[Caixa].Top := T_Ini + (L_Alt * Linha);
    //FmPrincipal.FLabels[Caixa].Caption := IntToStr(Caixa);
    FmPrincipal.FLabels[Caixa].Caption := Texto;
    FmPrincipal.FLabels[Caixa].Font.Color := clGray;

    if FmPrincipal.FFontName <> '' then
      FmPrincipal.FLabels[Caixa].Font.Name := FmPrincipal.FFontName;
    if FmPrincipal.FFontSize <> 0 then
      FmPrincipal.FLabels[Caixa].Font.Size := FmPrincipal.FFontSize;
    if FmPrincipal.FFontBold then
      FmPrincipal.FLabels[Caixa].Font.Style := [fsBold];

    //FmPrincipal.FLabels[Caixa].Font.Color := clWhite;
    //FmPrincipal.FLabels[Caixa].Transparent := False;
    TamLin[Coluna][Linha] := TamLin[Coluna][Linha] + FmPrincipal.FLabels[Caixa].Width;
    FmPrincipal.FLabels[Caixa].OnDblClick := FmPrincipal.LabelDblClick;
  end;
begin
  EXIT;
    FMEditMusica.BtSalva.Enabled := True;
    //
    N := SGMusica.RowCount -1;
    FmPrincipal.FMaxSom := N + 1;
    //
    if FmPrincipal.FFontHigh <> 0 then
      L_Alt :=  FmPrincipal.FFontHigh
    else
      L_Alt := 30;
    L_Ini := 40;
    T_Ini := 40;
    T_Col := 800;
    FmPrincipal.FGapPrev := 250 div FmPrincipal.FStep;
    //
    FmPrincipal.FMaxTempo := 0;
    for I := 1 to High(FmPrincipal.FLabels) do
    begin
      if FmPrincipal.FLabels[I] <> nil then
      begin
        try
          FmPrincipal.FLabels[I].Free;
        except
          //
        end;
      end;
    end;
    SetLength(FmPrincipal.FLabels, FmPrincipal.FMaxSom);
    SetLength(FmPrincipal.FTempos, FmPrincipal.FMaxSom);
    SetLength(TamLin, 4);
    SetLength(TamLin[1], N);
    SetLength(TamLin[2], N);
    SetLength(TamLin[3], N);
    SetLength(TamLin[4], N);
    SetLength(FmPrincipal.FSons, N);
    for I := 0 to N - 1 do
    begin
      TamLin[1][I] := L_Ini;
      TamLin[2][I] := L_Ini;
      TamLin[3][I] := L_Ini;
      TamLin[4][I] := L_Ini;
    end;
    FmSelMusica.PB1.Max := N;
    FmSelMusica.PB1.Position := 0;
    for I := 1 to N - 1 do
    begin
      FmSelMusica.PB1.Position := FmSelMusica.PB1.Position + 1;
      Application.ProcessMessages;
      //
      FmPrincipal.FTempos[I + 1] := 0;
      //Lin := FmPrincipal.FSons[I];
      //if Lin <> '' then
      begin
        Colum := 0;
        Linha := 0;
        Tempo := 0;
        Texto := '';
        CorPrev := $000080FF;
        CorShow := clred;
        //
        Conta := 0;
        FmPrincipal.FTempos[I + 1] := Tempo;
        if FmPrincipal.FMaxTempo < Tempo then
          FmPrincipal.FMaxTempo := Tempo;
        //
        New(FmPrincipal.FRSom);
        FmPrincipal.FRSom.Codigo  := I + 1;
        //SGMusica.Cells[00, 0] := 'SeqMus';
        FmPrincipal.FRSom.Estrofe  := Geral.IMV('0' + SGMusica.Cells[01, 0]);
        FmPrincipal.FRSom.Linha    := Geral.IMV('0' + SGMusica.Cells[02, 0]);
        FmPrincipal.FRSom.SeqInLin := Geral.IMV('0' + SGMusica.Cells[03, 0]);
        FmPrincipal.FRSom.TempoIni := Geral.IMV('0' + SGMusica.Cells[04, 0]);
        FmPrincipal.FRSom.TempoTam := Geral.IMV('0' + SGMusica.Cells[05, 0]);
        FmPrincipal.FRSom.TextoSom := SGMusica.Cells[06, 0];
        FmPrincipal.FRSom.TextoImp := SGMusica.Cells[07, 0];
        FmPrincipal.FRSom.CorGray  := Geral.IMV('0' + SGMusica.Cells[08, 0]);
        FmPrincipal.FRSom.CorPrev  := Geral.IMV('0' + SGMusica.Cells[09, 0]);
        FmPrincipal.FRSom.CorShow  := Geral.IMV('0' + SGMusica.Cells[10, 0]);
        //SGMusica.Cells[11, 0] := '';
        FmPrincipal.FSons[I+1] := FmPrincipal.FRSom;
        //
        CriaLabel(I + 1, Colum, Linha, Texto);
      end;
    end;
    FmPrincipal.FMaxSom := FmPrincipal.FMaxTempo div FmPrincipal.FStep;
    SetLength(FmPrincipal.FSeqI, FmPrincipal.FMaxSom);
    for I := 0 to High(FmPrincipal.FTempos) do
    begin
      FmPrincipal.FSeqI[FmPrincipal.FTempos[I] div FmPrincipal.FStep] := I;
    end;
end;
}

procedure TFmEditMusica2.Estrofe1Click(Sender: TObject);
var
  Musica: TMusica;
  Estrofe: TEstrofe;
  I: Integer;
begin
  Musica  := FmPrincipal.FMusicas[FMusicaSel];
  Estrofe := Musica.Estrofes[SGEstrofes.Row - 1];
  //
  FmEditEstrofe.FMusica                   := Musica;
  FmEditEstrofe.FEstrofe                  := Estrofe;
  //
  FmEditEstrofe.EdNoMusica.ValueVariant   := Musica.NomeMusica;
  FmEditEstrofe.EdCodMus.ValueVariant     := Musica.CodMus;
  FmEditEstrofe.EdCodEstrofe.ValueVariant := Estrofe.CodEst;
  FmEditEstrofe.EdTempoHide.ValueVariant  := Geral.FDT(Estrofe.TempoHide, 111);
  FmEditEstrofe.EdTempoShow.ValueVariant  := Geral.FDT(Estrofe.TempoShow, 111);
  FmEditEstrofe.EdNoEst.ValueVariant      := Estrofe.NoEst;
  //
  FmEditEstrofe.Show;
end;

procedure TFmEditMusica2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEditMusica2.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  SGMusica.Cells[00, 0] := 'SeqMus';
  SGMusica.Cells[01, 0] := 'Estrofe';
  SGMusica.Cells[02, 0] := 'Linha';
  SGMusica.Cells[03, 0] := 'SeqLin';
  SGMusica.Cells[04, 0] := 'Tempo I';
  SGMusica.Cells[05, 0] := 'Tam t';
  SGMusica.Cells[06, 0] := 'TextoSom';
  SGMusica.Cells[07, 0] := 'TextoImp';
  SGMusica.Cells[08, 0] := 'CorGray';
  SGMusica.Cells[09, 0] := 'CorPrev';
  SGMusica.Cells[10, 0] := 'CorShow';
  SGMusica.Cells[11, 0] := '';
  //
  CBFontName.FonteNome := 'Tahoma';

  //

  SGEstrofes.ColWidths[0] := 26;
  SGEstrofes.Cells[0, 0] := 'N�mero';
  //
  SGEstrofes.ColWidths[1] := 68;
  SGEstrofes.Cells[1, 0] := 'In�cio';
  //
  SGEstrofes.ColWidths[2] := 68;
  SGEstrofes.Cells[2, 0] := 'Fim';
  //
  SGEstrofes.ColWidths[3] := 200;
  SGEstrofes.Cells[3, 0] := 'Nome da Estrofe';

  //


end;

procedure TFmEditMusica2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEditMusica2.MeLyricsChange(Sender: TObject);
begin
  EdLinha.ValueVariant := MeLyrics.Perform(EM_LINEFROMCHAR, MeLYrics.SelStart, 0);
end;

procedure TFmEditMusica2.MeLyricsClick(Sender: TObject);
begin
  EdLinha.ValueVariant := MeLyrics.Perform(EM_LINEFROMCHAR, MeLYrics.SelStart, 0);
end;

procedure TFmEditMusica2.MeLyricsEnter(Sender: TObject);
begin
  EdLinha.ValueVariant := MeLyrics.Perform(EM_LINEFROMCHAR, MeLYrics.SelStart, 0);
end;

procedure TFmEditMusica2.MeLyricsKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  EdLinha.ValueVariant := MeLyrics.Perform(EM_LINEFROMCHAR, MeLYrics.SelStart, 0);
end;

procedure TFmEditMusica2.SGMusicaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = VK_INSERT then
    MyObjects.InsereLinhaStringGrid(SGMusica, True);
  if (key = VK_DELETE) and (ssCtrl in Shift) then
    MyObjects.ExcluiLinhaStringGrid(SGMusica, True);
end;

procedure TFmEditMusica2.SGMusicaSetEditText(Sender: TObject; ACol,
  ARow: Integer; const Value: string);
var
  I: Integer;
begin
  for I := ARow + 1 to SGMusica.RowCount do
  begin
    SGMusica.Cells[ACol, I] := Value;
  end;
end;

end.
