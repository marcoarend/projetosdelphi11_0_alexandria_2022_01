object FmEditMusica: TFmEditMusica
  Left = 0
  Top = 0
  Caption = 'Edi'#231#227'o da Letra da M'#250'sica'
  ClientHeight = 580
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PnLyrics: TPanel
    Left = 0
    Top = 0
    Width = 635
    Height = 580
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 531
      Width = 635
      Height = 32
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 0
      object Label11: TLabel
        Left = 84
        Top = 12
        Width = 29
        Height = 13
        Caption = 'Linha:'
      end
      object BtSalva: TButton
        Left = 551
        Top = 4
        Width = 75
        Height = 25
        Caption = 'Salva'
        Enabled = False
        TabOrder = 0
        OnClick = BtSalvaClick
      end
      object BtIncremento: TButton
        Left = 4
        Top = 4
        Width = 75
        Height = 25
        Caption = 'Incremento'
        TabOrder = 1
        OnClick = BtIncrementoClick
      end
      object EdLinha: TdmkEdit
        Left = 116
        Top = 8
        Width = 53
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object BtAltera: TButton
        Left = 176
        Top = 3
        Width = 75
        Height = 25
        Caption = 'Altera'
        TabOrder = 3
        OnClick = BtAlteraClick
      end
    end
    object MeLyrics: TMemo
      Left = 0
      Top = 249
      Width = 635
      Height = 12
      Align = alTop
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Courier New'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnChange = MeLyricsChange
      OnClick = MeLyricsClick
      OnEnter = MeLyricsEnter
      OnKeyUp = MeLyricsKeyUp
    end
    object PB1: TProgressBar
      Left = 0
      Top = 563
      Width = 635
      Height = 17
      Align = alBottom
      TabOrder = 2
    end
    object SGMusica: TStringGrid
      Left = 0
      Top = 261
      Width = 635
      Height = 112
      Align = alTop
      ColCount = 11
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
      TabOrder = 3
      OnKeyDown = SGMusicaKeyDown
      OnSetEditText = SGMusicaSetEditText
    end
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 635
      Height = 249
      Align = alTop
      TabOrder = 4
      object GroupBox2: TGroupBox
        Left = 1
        Top = 1
        Width = 633
        Height = 76
        Align = alTop
        Caption = ' Configura'#231#227'o do texto:'
        TabOrder = 0
        object Panel2: TPanel
          Left = 2
          Top = 15
          Width = 507
          Height = 59
          Align = alLeft
          TabOrder = 0
          object Label1: TLabel
            Left = 8
            Top = 4
            Width = 77
            Height = 13
            Caption = 'Nome da Fonte:'
          end
          object Label8: TLabel
            Left = 276
            Top = 4
            Width = 92
            Height = 13
            Caption = 'Tamanho da fonte:'
          end
          object Label9: TLabel
            Left = 372
            Top = 4
            Width = 73
            Height = 13
            Caption = 'Altura da linha:'
          end
          object CBFontName: TdmkPopOutFntCBox
            Left = 8
            Top = 20
            Width = 265
            Height = 22
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            MoveUsedToTop = True
            PopUpHeight = 75
            FonteNome = 'MS Sans Serif'
            UpdType = utYes
          end
          object CkNegrito: TCheckBox
            Left = 444
            Top = 20
            Width = 57
            Height = 17
            Alignment = taLeftJustify
            Caption = 'Negrito'
            Checked = True
            State = cbChecked
            TabOrder = 3
          end
          object EdLablAltu: TdmkEdit
            Left = 372
            Top = 20
            Width = 69
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '30'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 30
            ValWarn = False
          end
          object CBFontSize: TComboBox
            Left = 277
            Top = 20
            Width = 88
            Height = 21
            TabOrder = 1
            Text = '24'
            Items.Strings = (
              '5'
              '6'
              '7'
              '8'
              '9'
              '10'
              '11'
              '12'
              '14'
              '16'
              '18'
              '20'
              '22'
              '24'
              '26'
              '28'
              '36'
              '48'
              '72')
          end
        end
        object RGEstrPorTela: TdmkRadioGroup
          Left = 509
          Top = 15
          Width = 122
          Height = 59
          Align = alClient
          Caption = 'Estrofes simull'#226'neas:'
          Columns = 2
          ItemIndex = 1
          Items.Strings = (
            '1'
            '2'
            '3'
            '4')
          TabOrder = 1
          UpdType = utYes
          OldValor = 0
        end
      end
      object Panel4: TPanel
        Left = 1
        Top = 161
        Width = 633
        Height = 87
        Align = alClient
        TabOrder = 2
        object Label3: TLabel
          Left = 8
          Top = 0
          Width = 116
          Height = 13
          Caption = 'Nome do Hin'#225'rio / M'#237'dia:'
        end
        object Label2: TLabel
          Left = 8
          Top = 40
          Width = 129
          Height = 13
          Caption = 'N'#250'mero e nome da m'#250'sica:'
        end
        object Label7: TLabel
          Left = 512
          Top = 40
          Width = 89
          Height = 13
          Caption = 'Data cria'#231#227'o letra:'
        end
        object EdNomeHinario: TdmkEdit
          Left = 8
          Top = 16
          Width = 617
          Height = 21
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdCodMus: TdmkEdit
          Left = 8
          Top = 56
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdNomeMusica: TdmkEdit
          Left = 92
          Top = 56
          Width = 417
          Height = 21
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object TPDataLetra: TdmkEditDateTimePicker
          Left = 512
          Top = 56
          Width = 112
          Height = 21
          Date = 42551.538749224540000000
          Time = 42551.538749224540000000
          TabOrder = 3
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
      end
      object Panel5: TPanel
        Left = 1
        Top = 77
        Width = 633
        Height = 84
        Align = alTop
        TabOrder = 1
        object Label6: TLabel
          Left = 308
          Top = 40
          Width = 110
          Height = 13
          Caption = 'Nome da m'#237'dia original:'
        end
        object Label5: TLabel
          Left = 8
          Top = 0
          Width = 118
          Height = 13
          Caption = 'Nome da m'#250'sica original:'
        end
        object Label4: TLabel
          Left = 8
          Top = 40
          Width = 179
          Height = 13
          Caption = 'Nome artista dono da musica original:'
        end
        object EdNoOriMidia: TdmkEdit
          Left = 308
          Top = 56
          Width = 317
          Height = 21
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdNoOriMusica: TdmkEdit
          Left = 8
          Top = 16
          Width = 617
          Height = 21
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdNoOriArtista: TdmkEdit
          Left = 8
          Top = 56
          Width = 293
          Height = 21
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
      end
    end
    object SGEstrofes: TdmkStringGrid
      Left = 0
      Top = 373
      Width = 635
      Height = 158
      Align = alClient
      ColCount = 4
      TabOrder = 5
    end
  end
  object PMAltera: TPopupMenu
    Left = 392
    Top = 452
    object Estrofe1: TMenuItem
      Caption = '&Estrofe'
      OnClick = Estrofe1Click
    end
  end
end
