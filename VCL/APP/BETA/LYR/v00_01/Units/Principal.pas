unit Principal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.MPlayer,
  Vcl.ComCtrls, Vcl.Buttons,
  dmkEdit, FolderDialog,
  UnDmkProcFunc, UnInternalConsts, UnInternalConsts2, UnMyLinguas,
  Menus, ClipBrd, ToolWin, ActnList,
  ImgList, Db, mySQLDbTables, DBCtrls, dmkGeral, UnDmkEnums,
  UnApp_PF, UnArr_PF,
  Xml.xmldom, Xml.XMLIntf, Xml.Win.msxmldom, Xml.XMLDoc, dmkCheckBox;

type
  TFmPrincipal = class(TForm)
    OpenDialog1: TOpenDialog;
    TimerLabels: TTimer;
    TimerVoz: TTimer;
    Panel1: TPanel;
    BtMenu: TButton;
    LaTempo: TLabel;
    Mp3player: TMediaPlayer;
    BtVaiPara: TButton;
    EdHor: TdmkEdit;
    Label8: TLabel;
    EdMin: TdmkEdit;
    Label9: TLabel;
    EdSeg: TdmkEdit;
    Label10: TLabel;
    EdMil: TdmkEdit;
    Progress: TProgressBar;
    FolderDialog1: TFolderDialog;
    PMMenu: TPopupMenu;
    Pastadefault1: TMenuItem;
    Recarrega1: TMenuItem;
    Editaraletradamusica1: TMenuItem;
    XMLDocument1: TXMLDocument;
    N1: TMenuItem;
    Habilitartimecatch1: TMenuItem;
    TimerCatch: TTimer;
    NovaMsica1: TMenuItem;
    BtRecomeca: TButton;
    TimerPanels: TTimer;
    CkOcultaestLidas: TCheckBox;
    N2: TMenuItem;
    Imprimir1: TMenuItem;
    TimerScroll: TTimer;
    LaPlayerStatus: TLabel;
    BitBtn1: TBitBtn;
    CkSetGray: TdmkCheckBox;
    EdMouse: TdmkEdit;
    Musicascarregadas1: TMenuItem;
    LaSomClik: TLabel;
    procedure TimerVozTimer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TimerLabelsTimer(Sender: TObject);
    procedure Mp3playerClick(Sender: TObject; Button: TMPBtnType;
      var DoDefault: Boolean);
    procedure Mp3playerPostClick(Sender: TObject; Button: TMPBtnType);
    procedure Pastadefault1Click(Sender: TObject);
    procedure Recarrega1Click(Sender: TObject);
    procedure BtMenuClick(Sender: TObject);
    procedure Editaraletradamusica1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Habilitartimecatch1Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure NovaMsica1Click(Sender: TObject);
    procedure BtVaiParaClick(Sender: TObject);
    procedure BtRecomecaClick(Sender: TObject);
    procedure TimerPanelsTimer(Sender: TObject);
    procedure Imprimir1Click(Sender: TObject);
    procedure TimerScrollTimer(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    //
    procedure LabelDblClick(Sender: TObject);
    procedure LabelMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure PanelMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure PanelMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure PanelMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormMouseWheel(Sender: TObject; Shift: TShiftState;
      WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure LabelMouseEnter(Sender: TObject);
    procedure Musicascarregadas1Click(Sender: TObject);
  private
    { Private declarations }
    FSeqTNow, FSeqTPrv: Integer;
    FlabelPintado: TLabel;
    FScrollRun: Boolean;
    FScrollIni, FScrollFim: Integer;
    //FInicio: TDateTime;
    procedure EditarLetraDaMusica(Estrofe, Linha, Som: Integer);
    procedure MostraOcultaPanels(ByStep: Boolean);
    procedure ObtemSomDeLabel(const Sender: TObject; var nEst, nLin,
              nSom: Integer);
  public
    { Public declarations }
    FMusicas: TMusicas;
    FEstrofes: TEstrofes;
    FLinhas: TLinhas;
    FSons: TSons;
    //
    FSeqI, FTempos, FCoresGray, FCoresPrev, FCoresShow: TInteiros;
    FLabels: TLabels;
    FPanels: TPanels;
    FAltPan: array of Integer;
    //FPnShow: TPanel;
    FScrollBox: TScrollBox;
    FLablCount, FMaxSom, FGapPrev, FMaxTempo, FShemaCores: Integer;
    //
    FLyrFile, FFontName: String;
    FFontSize, FFontHigh, FStep: Integer;
    FFontBold: Boolean;
    FRSom: TSom;
    //
    //function  CarregaArquivoLyrico_Txt(Arquivo: String; PB: TProgressBar): Boolean;
    procedure BtStopClicked();
    procedure Executa();
    procedure VaiPara();
    procedure ParaLoop();
    //
    procedure ControleMudou(Sender: TObject);
    procedure StatusPlayer();
    procedure SetGray();
  end;

var
  FmPrincipal: TFmPrincipal;
  FMouse: TMouse;
  FMusicaSel: Integer = 0;
const
  CO_DMKID_APP = 0;

type
  TID3Rec = packed record
    Tag     : array[0..2] of Char;
    Title,
    Artist,
    Comment,
    Album   : array[0..29] of Char;
    Year    : array[0..3] of Char;
    Genre   : Byte;
  end;

const
  MaxID3Genre=147;
  ID3Genre: array[0..MaxID3Genre] of string = (
    'Blues', 'Classic Rock', 'Country', 'Dance', 'Disco', 'Funk', 'Grunge',
    'Hip-Hop', 'Jazz', 'Metal', 'New Age', 'Oldies', 'Other', 'Pop', 'R&B',
    'Rap', 'Reggae', 'Rock', 'Techno', 'Industrial', 'Alternative', 'Ska',
    'Death Metal', 'Pranks', 'Soundtrack', 'Euro-Techno', 'Ambient',
    'Trip-Hop', 'Vocal', 'Jazz+Funk', 'Fusion', 'Trance', 'Classical',
    'Instrumental', 'Acid', 'House', 'Game', 'Sound Clip', 'Gospel',
    'Noise', 'AlternRock', 'Bass', 'Soul', 'Punk', 'Space', 'Meditative',
    'Instrumental Pop', 'Instrumental Rock', 'Ethnic', 'Gothic',
    'Darkwave', 'Techno-Industrial', 'Electronic', 'Pop-Folk',
    'Eurodance', 'Dream', 'Southern Rock', 'Comedy', 'Cult', 'Gangsta',
    'Top 40', 'Christian Rap', 'Pop/Funk', 'Jungle', 'Native American',
    'Cabaret', 'New Wave', 'Psychadelic', 'Rave', 'Showtunes', 'Trailer',
    'Lo-Fi', 'Tribal', 'Acid Punk', 'Acid Jazz', 'Polka', 'Retro',
    'Musical', 'Rock & Roll', 'Hard Rock', 'Folk', 'Folk-Rock',
    'National Folk', 'Swing', 'Fast Fusion', 'Bebob', 'Latin', 'Revival',
    'Celtic', 'Bluegrass', 'Avantgarde', 'Gothic Rock', 'Progressive Rock',
    'Psychedelic Rock', 'Symphonic Rock', 'Slow Rock', 'Big Band',
    'Chorus', 'Easy Listening', 'Acoustic', 'Humour', 'Speech', 'Chanson',
    'Opera', 'Chamber Music', 'Sonata', 'Symphony', 'Booty Bass', 'Primus',
    'Porn Groove', 'Satire', 'Slow Jam', 'Club', 'Tango', 'Samba',
    'Folklore', 'Ballad', 'Power Ballad', 'Rhythmic Soul', 'Freestyle',
    'Duet', 'Punk Rock', 'Drum Solo', 'Acapella', 'Euro-House', 'Dance Hall',
    'Goa', 'Drum & Bass', 'Club-House', 'Hardcore', 'Terror', 'Indie',
    'BritPop', 'Negerpunk', 'Polsk Punk', 'Beat', 'Christian Gangsta Rap',
    'Heavy Metal', 'Black Metal', 'Crossover', 'Contemporary Christian',
    'Christian Rock', 'Merengue', 'Salsa', 'Trash Metal', 'Anime', 'Jpop',
    'Synthpop'  {and probably more to come}
  );

  implementation

uses
  ShellAPI, ShlObj,  // needed for the BrowseForFolder function;
  Lyrics_v001,
  uOnScreenDisplay, SelMusica, EditMusica, UnMyObjects, CatchSound, Sound;

{$R *.dfm}

const
  MaxCtchSom = 256;
  MaxCtchLin = 128;

var
  FArCtchSom: array [0..MaxCtchLin] of array [0..MaxCtchSom] of Integer;
  FCatchEnabled: Boolean;
  FCtchSomCount, FCtchLinCount: Integer;

procedure FillID3TagInformation(mp3File:string; Title,Artist,Album,Year,Genre,Comment:TEdit);
var //fMP3: file of Byte;
    ID3 : TID3Rec;
    fmp3: TFileStream;
begin
  fmp3:=TFileStream.Create(mp3File, fmOpenRead);
  try
    fmp3.position:=fmp3.size-128;
    fmp3.Read(ID3,SizeOf(ID3));
  finally
    fmp3.free;
  end;

 { or the non Stream approach - as in ChangeID3Tag procedure
 try
   AssignFile(fMP3, mp3File);
   Reset(fMP3);
   try
     Seek(fMP3, FileSize(fMP3) - 128);
     BlockRead(fMP3, ID3, SizeOf(ID3));
   finally
   end;
 finally
   CloseFile(fMP3);
 end;
 }

 if ID3.Tag = 'TAG' then
 begin
   Title.Text:='Wrong or no ID3 tag information';
   Artist.Text:='Wrong or no ID3 tag information';
   Album.Text:='Wrong or no ID3 tag information';
   Year.Text:='Wrong or no ID3 tag information';
   Genre.Text:='Wrong or no ID3 tag information';
   Comment.Text:='Wrong or no ID3 tag information';
 end else
 begin
   Title.Text:=ID3.Title;
   Artist.Text:=ID3.Artist;
   Album.Text:=ID3.Album;
   Year.Text:=ID3.Year;
   if ID3.Genre in [0..MaxID3Genre] then
     Genre.Text:=ID3Genre[ID3.Genre]
   else
     Genre.Text:=IntToStr(ID3.Genre);
   Comment.Text:=ID3.Comment
 end;
end;


procedure ChangeID3Tag(NewID3: TID3Rec; mp3FileName: string);
var
  fMP3: file of Byte;
  OldID3 : TID3Rec;
begin
  try
    AssignFile(fMP3, mp3FileName);
    Reset(fMP3);
    try
      Seek(fMP3, FileSize(fMP3) - 128);
      BlockRead(fMP3, OldID3, SizeOf(OldID3));
      if OldID3.Tag = 'TAG' then
        { Replace old tag }
        Seek(fMP3, FileSize(fMP3) - 128)
      else
        { Append tag to file because it doesn't exist }
        Seek(fMP3, FileSize(fMP3));
      BlockWrite(fMP3, NewID3, SizeOf(NewID3));
    finally
    end;
  finally
    CloseFile(fMP3);
  end;
end;


procedure FillMP3FileList(Folder: string; sl: TStrings);
var Rec : TSearchRec;
begin
 sl.Clear;
 if System.SysUtils.FindFirst(Folder + '*.mp3', faAnyFile, Rec) = 0 then
  try
    repeat
      sl.Add(Rec.Name);
    until System.SysUtils.FindNext(Rec) > 0;
  finally
    System.SysUtils.FindClose(Rec);
  end;
end;

function BrowseDialog(const Title: string; const Flag: integer): string;
var
  lpItemID : PItemIDList;
  BrowseInfo : TBrowseInfo;
  DisplayName : array[0..MAX_PATH] of char;
  TempPath : array[0..MAX_PATH] of char;
begin
  Result:='';
  FillChar(BrowseInfo, sizeof(TBrowseInfo), #0);
  with BrowseInfo do begin
    hwndOwner := Application.Handle;
    pszDisplayName := @DisplayName;
    lpszTitle := PChar(Title);
    ulFlags := Flag;
  end;
  lpItemID := SHBrowseForFolder(BrowseInfo);
  if lpItemId  = nil then begin
    SHGetPathFromIDList(lpItemID, TempPath);
    Result := IncludeTrailingBackslash(TempPath);
    GlobalFreePtr(lpItemID);
  end;
end;

procedure TFmPrincipal.BtRecomecaClick(Sender: TObject);
begin
  VaiPara();
  Executa();
  Mp3player.Play;
end;

procedure TFmPrincipal.BtStopClicked();
begin
  ParaLoop();
  //
  mp3player.Enabled := False;
  mp3player.Stop;
  mp3player.Close;
end;

procedure TFmPrincipal.BtVaiParaClick(Sender: TObject);
begin
  VaiPara();
end;

procedure TFmPrincipal.BitBtn1Click(Sender: TObject);
begin
  FmSound.Show;
end;

procedure TFmPrincipal.BtMenuClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMMenu, BtMenu);
end;

{
function TFmPrincipal.CarregaArquivoLyrico_Txt(Arquivo: String; PB: TProgressBar): Boolean;
const
  Separador = '|';
var
  N, I, Z, Colum, Linha, Conta, Tempo: Integer;
  L_Ini, L_Alt, T_Ini, T_Col: Integer;
  TamLin: array of array of Integer;
  Sons: TStringList;
  Lin, Texto, Parte: String;
  CorPrev, CorShow: TColor;
  //
  function CriaLabel(Caixa, Coluna, Linha: Integer; Texto: String): Boolean;
  begin
    FLabels[Caixa] := TLabel.Create(Self);
    FLabels[Caixa].Parent := ScrollBox1;
    FLabels[Caixa].Tag := Caixa;
    FLabels[Caixa].Left := TamLin[Coluna][Linha] + ((Coluna - 1) * T_Col);
    FLabels[Caixa].Height := T_Ini + (L_Alt * Caixa);
    FLabels[Caixa].Top := T_Ini + (L_Alt * Linha);
    //FLabels[Caixa].Caption := IntToStr(Caixa);
    FLabels[Caixa].Caption := Texto;
    FLabels[Caixa].Font.Color := clGray;

    if FFontName <> '' then
      FLabels[Caixa].Font.Name := FFontName;
    if FFontSize <> 0 then
      FLabels[Caixa].Font.Size := FFontSize;
    if FFontBold then
      FLabels[Caixa].Font.Style := [fsBold];

    //FLabels[Caixa].Font.Color := clWhite;
    //FLabels[Caixa].Transparent := False;
    TamLin[Coluna][Linha] := TamLin[Coluna][Linha] + FLabels[Caixa].Width;
    FLabels[Caixa].OnDblClick := LabelDblClick;
  end;
begin
  Z := 0;
  Sons := TStringList.Create;
  try
    Sons.LoadFromFile(Arquivo);
    FmEditMusica.MeLyrics.Text := Sons.Text;
    FMEditMusica.BtOK.Enabled := True;
    //
    N := Sons.Count;
    FMaxSom := N + 1;
    //
    if FFontHigh <> 0 then
      L_Alt :=  FFontHigh
    else
      L_Alt := 30;
    T_Ini := 40;
    T_Col := 800;
    FGapPrev := 250 div FStep;
    //
    FMaxTempo := 0;
    for I := 1 to High(FLabels) do
    begin
      if FLabels[I] <> nil then
      begin
        try
          FLabels[I].Free;
        except
          //
        end;
      end;
    end;
    SetLength(FLabels, FMaxSom);
    SetLength(FTempos, FMaxSom);
    SetLength(TamLin, 4);
    SetLength(TamLin[1], N);
    SetLength(TamLin[2], N);
    SetLength(TamLin[3], N);
    SetLength(TamLin[4], N);
    SetLength(FSons, N);
    for I := 0 to N - 1 do
    begin
      TamLin[1][I] := L_Ini;
      TamLin[2][I] := L_Ini;
      TamLin[3][I] := L_Ini;
      TamLin[4][I] := L_Ini;
    end;
    PB.Max := N;
    PB.Position := 0;
    for I := 0 to N - 1 do
    begin
      PB.Position := PB.Position + 1;
      Application.ProcessMessages;
      //
      FTempos[I + 1] := 0;
      Lin := Sons[I];
      if Lin <> '' then
      begin
        Colum := 0;
        Linha := 0;
        Tempo := 0;
        Texto := '';
        CorPrev := $000080FF;
        CorShow := clred;
        //
        Conta := 0;
        while Length(Lin) > 0 do
        begin
          if Geral.SeparaPrimeiraOcorrenciaDeTexto(Separador, Lin, Parte, Lin) then
          begin
            Conta := Conta + 1;
            case Conta of
              1: Colum := Geral.IMV(Parte);
              2: Linha := Geral.IMV(Parte);
              3: Tempo := Geral.IMV(Parte);
              4: Texto := Parte;
              5: CorPrev := Geral.IMV(Parte);
              6: CorShow := Geral.IMV(Parte);
              else begin
                if Trim(Parte) <> '' then
                begin
                  Geral.MB_Erro('N�o foi poss�vel definir a vari�vel pai "' +
                  Geral.FF0(I) + '" de resultado (valor):' + sLineBreak + Parte);
                  Screen.Cursor := crDefault;
                  Exit;
                end;
              end;
            end;
          end;
        end;
        FTempos[I + 1] := Tempo;
        if FMaxTempo < Tempo then
          FMaxTempo := Tempo;
        //
        New(FRSom);
        FRSom.Codigo  := I + 1;
        FRSom.Coluna  := 1;
        FRSom.Linha   := Linha;
        FRSom.Tempo   := Tempo;
        FRSom.Texto   := Texto;
        FRSom.CorPrev := CorPrev;
        FRSom.CorShow := CorShow;
        FSons[I+1] := FRSom;
        //
        if Colum <> 0 then
        begin
          Z := Z + 1;
          FmEditMusica.SGMusica.RowCount := Z + 1;
          FmEditMusica.SGMusica.Cells[0, Z] := Geral.FF0(Z);
          FmEditMusica.SGMusica.Cells[1, Z] := Geral.FF0(Colum);
          FmEditMusica.SGMusica.Cells[2, Z] := Geral.FF0(FRSom.Linha);
          FmEditMusica.SGMusica.Cells[3, Z] := Geral.FF0(FRSom.Linha);
          FmEditMusica.SGMusica.Cells[4, Z] := Geral.FF0(FRSom.Tempo);
          FmEditMusica.SGMusica.Cells[5, Z] := Geral.FF0(0);
          FmEditMusica.SGMusica.Cells[6, Z] := FRSom.Texto;
          FmEditMusica.SGMusica.Cells[7, Z] := FRSom.Texto;
          FmEditMusica.SGMusica.Cells[8, Z] := Geral.FF0(clGray);
          FmEditMusica.SGMusica.Cells[9, Z] := Geral.FF0($000080FF);
          FmEditMusica.SGMusica.Cells[10, Z] := Geral.FF0(clRed);
        end;
        CriaLabel(I + 1, FRSom.Coluna, Linha, Texto);
      end;
    end;
    FMaxSom := FMaxTempo div FStep;
    SetLength(FSeqI, FMaxSom);
    for I := 0 to High(FTempos) do
    begin
      FSeqI[FTempos[I] div FStep] := I;
    end;
  finally
    Sons.Free;
  end;
end;
}

procedure TFmPrincipal.ControleMudou(Sender: TObject);
begin
  //if APP_LIBERADO then
  begin
    MyObjects.ControleCor(Self);
    // M L A G e r a l .SistemaMetrico;
    // M L A G e r a l .CustoSincronizado;
  end;
end;

procedure TFmPrincipal.Editaraletradamusica1Click(Sender: TObject);
begin
  EditarLetraDaMusica(1, 1, 1);
end;

procedure TFmPrincipal.EditarLetraDaMusica(Estrofe, Linha, Som: Integer);
var
  DoDefault: Boolean;
begin
  if Trim(FLyrFile) = '' then
  begin
    Geral.MB_Aviso('M�sica n�o definida!');
    Exit;
  end;
  if TMPBtnType.btStop in mp3player.EnabledButtons then
  begin
    //Geral.MB_Aviso('M�sica em execu��o!');
    //Exit;
    DoDefault := False;
    Mp3playerClick(Mp3player, TMPBtnType.btStop, DoDefault);
  end;
  FmEditMusica.ImgTipo.SQLType := stUpd;
  FmEditMusica.MostraEstrofesDaMusica(Estrofe);
  FmEditMusica.MostraLinhasDaEstrofe(Estrofe, Linha);
  FmEditMusica.MostraSonsDaLinha(Estrofe, Linha, Som);
  FmEditMusica.Show;
end;

procedure TFmPrincipal.Executa();
begin
  MostraOcultaPanels(False);
  //
  TimerLabels.Enabled := True;
  TimerPanels.Enabled := True;
  //FInicio := Now();
  FSeqTNow := 0;
  FSeqTPrv := 0;
  { Override automatic button controlling. }
  mp3player.EnabledButtons := [TMPBtnType.btPause, TMPBtnType.btStop,
    TMPBtnType.btPlay, TMPBtnType.btPrev];
end;

procedure TFmPrincipal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FMouse.Free;
end;

procedure TFmPrincipal.FormCreate(Sender: TObject);
  procedure X_Dmk_Create();
    procedure DefineCores();
    //var
      //Aparencia: Integer;
    begin
      IC2_AparenciasTituloPainelItem   := clBtnFace;
      IC2_AparenciasTituloPainelTexto  := clWindowText;
      IC2_AparenciasDadosPainel        := clBtnFace;
      IC2_AparenciasDadosTexto         := clWindowText;
      IC2_AparenciasGradeAItem         := clWindow;
      IC2_AparenciasGradeATitulo       := clWindowText;
      IC2_AparenciasGradeATexto        := clWindowText;
      IC2_AparenciasGradeAGrade        := clBtnFace;
      IC2_AparenciasGradeBItem         := clWindow;
      IC2_AparenciasGradeBTitulo       := clWindowText;
      IC2_AparenciasGradeBTexto        := clWindowText;
      IC2_AparenciasGradeBGrade        := clBtnFace;
      IC2_AparenciasEditAItem          := clWindow;
      IC2_AparenciasEditATexto         := clWindowText;
      IC2_AparenciasEditBItem          := clBtnFace;
      IC2_AparenciasEditBTexto         := clWindowText;
      IC2_AparenciasConfirmaPainelItem := clBtnFace;
      IC2_AparenciasControlePainelItem := clBtnFace;
      IC2_AparenciasFormPesquisa       := clBtnFace;
      IC2_AparenciasLabelPesquisa      := clWindowText;
      IC2_AparenciasLabelDigite        := clWindowText;
      IC2_AparenciasEditPesquisaItem   := clWindow;
      IC2_AparenciasEditPesquisaTexto  := clWindowText;
      IC2_AparenciasEditItemIn         := $00FF8000;
      IC2_AparenciasEditItemOut        := clWindow;
      IC2_AparenciasEditTextIn         := $00D5FAFF;
      IC2_AparenciasEditTextOut        := clWindowText;
    end;
  var
    WinPath : Array[0..144] of Char;
    Cam: String;
  begin
    Cam := Application.Title+'\Opcoes';
    //
    VAR_CAMINHOTXTBMP := 'C:\Dermatek\Skins\VCLSkin\mxskin33.skn';
    MyLinguas.GetInternationalStrings(myliPortuguesBR);
    TMeuDB := 'BlueDerm';
    //UMyMod.DefineDatabase(TMeuDB, Application.ExeName);
    //
    TLocDB := 'LocBDerm';
    VAR_SQLUSER := 'root';
    Geral.WriteAppKeyCU('Cashier_Database', 'Dermatek', TMeuDB, ktString);
    Geral.WriteAppKeyCU('CashLoc_Database', 'Dermatek', TLocDB, ktString);
    Geral.WriteAppKeyCU('Cashier_DmkID_APP', 'Dermatek', CO_DMKID_APP, ktInteger);
    //
    DefineCores;
    Screen.OnActiveControlChange := ControleMudou;
    IC2_LOpcoesTeclaEnter := Geral.ReadAppKey('TeclaEnter', Application.Title, ktInteger,-1, HKEY_LOCAL_MACHINE);
    if IC2_LOpcoesTeclaEnter = -1 then
    begin
      Geral.WriteAppKey('TeclaEnter', Application.Title, 13, ktInteger, HKEY_LOCAL_MACHINE);
      IC2_LOpcoesTeclaEnter := 13;
    end;
    IC2_LOpcoesTeclaTab := Geral.ReadAppKey('TeclaTAB', Application.Title, ktInteger,-1, HKEY_LOCAL_MACHINE);
    if IC2_LOpcoesTeclaTab = -1 then
    begin
      Geral.WriteAppKey('TeclaTAB', Application.Title, 9, ktInteger, HKEY_LOCAL_MACHINE);
      IC2_LOpcoesTeclaTab := 9;
    end;
    GetWindowsDirectory(WinPath,144);
    //ReloadSkin;
    IC2_COMPANT := nil;
    VAR_BD := 0;
  end;

var
  MenuStyle: Integer;
  Imagem: String;
begin
  FStep := 50;
  FGapPrev := 250 div FStep;
  FScrollRun := False;
  FScrollIni := 0;
  FScrollFim := 0;
  //
  FMouse := TMouse.Create;
  //
  Geral.DefineFormatacoes;
  Application.OnException := MyObjects.MostraErro;
  //
  FFontBold := True;
  { Disable all buttons. }
  mp3player.AutoEnable := false;
  mp3player.EnabledButtons := [];
  //
  Progress.Max := 0;
  //
  VAR_CAMBIO_DATA       := 0;
  VAR_MULTIPLAS_TAB_LCT := True;
  VAR_TYPE_LOG          := ttlFiliLog;
  VAR_ADMIN             := 'owemqOQWD';
  //
  VAR_USA_TAG_BITBTN := True;
  VAR_CAMINHOSKINPADRAO := 'C:\Dermatek\Skins\VCLSkin\Office 2007.skn';
  //
{
  VAR_AdvToolBarPagerPrincipal := AdvToolBarPager1;
  AdvToolBarPager1.ActivePageIndex := 0;
  FTipoNovoEnti := 0;
  VAR_STLOGIN       := StatusBar.Panels[01];
  StatusBar.Panels[3].Text := Geral.VersaoTxt2006(CO_VERSAO);
  VAR_STEMPRESA     := StatusBar.Panels[05];
  VAR_STDATALICENCA := StatusBar.Panels[07];
  //VAR_STAVISOS      := StatusBar.Panels[09];
  VAR_SKINUSANDO    := StatusBar.Panels[09];
  VAR_STDATABASES   := StatusBar.Panels[11];
}
  VAR_TIPOSPRODM_TXT := '0,1,2,3,4,5,6,7,8,9,10,11,12,13';
  VAR_APP := ExtractFilePath(Application.ExeName);
  VAR_VENDEOQUE := 1;
  Application.OnMessage := MyObjects.FormMsg;
  Imagem := Geral.ReadAppKey(
    'ImagemFundo', Application.Title, ktString, VAR_APP+'Fundo.jpg', HKEY_LOCAL_MACHINE);
  //
{
  PageControl1.Align := alClient;
  //
  if FileExists(Imagem) then
  begin
    ImgPrincipal.Picture.LoadFromFile(Imagem);
    AjustaEstiloImagem(ImgPrincipal.Tag);
  end;
  AjustaEstiloImagem(-1);
  UnPQx.SetaVariaveisIniciais;
}
  //
  MenuStyle := Geral.ReadAppKey('MenuStyle', Application.Title,
    ktInteger, 0, HKEY_LOCAL_MACHINE);
{
  SkinMenu(MenuStyle);
  VAR_CAD_POPUP := PMGeral;
  AdvToolBarPager1.ActivePageIndex := 0;
  // M L A G e r a l .CopiaItensDeMenu(PMGeral, Cadastros1, FmPrincipal);
  MyObjects.CopiaItensDeMenu(PMGeral, FmPrincipal);
  //
  MyObjects.MaximizaAdvToolBarPager(AdvToolBarPager1, deftfTrue);
}
  X_Dmk_Create();
  //
end;

procedure TFmPrincipal.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  I, J, N: Integer;
begin
  if ssShift in Shift then
  begin
    case ord(Key) of
      Ord('l'), Ord('L'):
      begin
        FCtchLinCount := FCtchLinCount + 1;
        FCtchSomCount := 0;
      end;
      Ord('p'), Ord('P'):
      begin
        FArCtchSom[FCtchLinCount][FCtchSomCount] := Mp3Player.Position;
        FCtchSomCount := FCtchSomCount + 1;
      end;
      Ord('f'), Ord('F'):
      begin
        Self.KeyPreview := False;
        //FmCatchSound.Memo1.Text := 'Linha.Som.Milisegundos';
        N := 0;
        for I := 0 to FCtchLinCount do
        begin
          for J := 0 to MaxCtchSom do
          begin
            if FArCtchSom[I][J] > 0 then
            begin
              //FmCatchSound.Memo1.Lines.Add(Geral.FF0(I) + ' - ' +
              //Geral.FF0(J) + ' - ' + Geral.FF0(FArCtchSom[I][J]));
              N := N + 1;
              FmCatchSound.Grade.RowCount := N + 2;
              FmCatchSound.Grade.Cells[0, N] := '?';
              FmCatchSound.Grade.Cells[1, N] := Geral.FF0(1);
              FmCatchSound.Grade.Cells[2, N] := Geral.FF0(I + 1);
              FmCatchSound.Grade.Cells[3, N] := Geral.FF0(J + 1);
              FmCatchSound.Grade.Cells[4, N] := Geral.FF0(FArCtchSom[I][J]);
            end;
          end;
        end;
        FmCatchSound.FLastNum := Geral.IMV(FmCatchSound.Grade.Cells[1, 1]);
        FmCatchSound.FMusica  := FMusicas[FMusicaSel];
        FmCatchSound.Show;
      end;
    end;
  end;
end;

procedure TFmPrincipal.FormMouseWheel(Sender: TObject; Shift: TShiftState;
  WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
var
  Fator, Increment, Linhas: Integer;
  Mouse: TMouse;
begin
  if FScrollBox <> nil then
  begin
    Linhas := FMouse.WheelScrollLines;
    if WheelDelta < 0 then
      Fator := 1
    else
      Fator := -1;
    Increment := FMusicas[FMusicaSel].LablAltu div Linhas * 2;
    FScrollBox.VertScrollBar.Position :=
        FScrollBox.VertScrollBar.Position + (Increment * Fator);
  end;
end;

procedure TFmPrincipal.FormShow(Sender: TObject);
begin
  FmSelMusica.Show;
end;

procedure TFmPrincipal.Habilitartimecatch1Click(Sender: TObject);
var
  I, J: Integer;
begin
  for I := 0 to MaxCtchLin do
    for J := 0 to MaxCtchSom do
      FArCtchSom[I][J] := 0;
  FCatchEnabled := True;
  FCtchLinCount := 0;
  FCtchSomCount := 0;
  //
  Geral.MB_Info('Catch sound habilitado!' +
    sLineBreak + 'Shift + P = Catch Sound' +
    sLineBreak + 'Shift + L = Mudou a linha' +
    sLineBreak + 'Shift + F = Para e mostra resultados');
  Self.KeyPreview := True;
end;

procedure TFmPrincipal.Imprimir1Click(Sender: TObject);
var
  Som: TSom;
  Texto: String;
  I, J, K,
  N, O, P: Integer;
  //NoEst: String;
  //TempoShow, TempoHide: TTime;
begin
  N := Length(FmPrincipal.FMusicas[FMusicaSel].Estrofes);
  for I := 0 to N - 1 do
  begin
    O := Length(FmPrincipal.FMusicas[FMusicaSel].Estrofes[I].Linhas);
    for J := 0 to O - 1 do
    begin
      P := Length(FmPrincipal.FMusicas[FMusicaSel].Estrofes[I].Linhas[J].Sons);
      for K := 0 to P - 1 do
      begin
        Som := FmPrincipal.FMusicas[FMusicaSel].Estrofes[I].Linhas[J].Sons[K];
        //
        if Som.TextoImp <> '' then
          Texto := Texto + Som.TextoImp;
      end;
      Texto := Texto + sLineBreak;
    end;
    Texto := Texto + sLineBreak;
  end;
  Geral.MB_Info(Texto);
end;

procedure TFmPrincipal.LabelMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  nEst, nLin, nSom: Integer;
  Musica: TMusica;
  Estrofe: TEstrofe;
  Linha: TLinha;
  Som: TSom;
  I, myLin(*, nEst, nLin*): Integer;
  Tempo: TTime;
  PosicaoCorrigida: Integer;
begin
  if Button = TMouseButton.mbRight then
  //if Button = TMouseButton.mbLeft then
  begin
    ObtemSomDeLabel(Sender, nEst, nLin, nSom);
    Musica  := FmPrincipal.FMusicas[FMusicaSel];

    if Length(Musica.Estrofes) = 0 then Exit;
    Estrofe := Musica.Estrofes[nEst - 1];

    if Length(Estrofe.Linhas) = 0 then Exit;
    Linha := Estrofe.Linhas[nLin - 1];

    if Length(Linha.Sons) = 0 then Exit;
    Som := Linha.Sons[nSom - 1];

    //Tempo := Som.TempoIni / 86400000;

    LaSomClik.Caption  := FormatDateTime('nn:ss:zzz', Som.TempoIni / 86400000);
    (*PosicaoCorrigida := Som.TempoIni + FmSound.FAjusteTempo;
    if PosicaoCorrigida < 0 then
      PosicaoCorrigida := 0;
    Mp3player.Position := PosicaoCorrigida;*)
    Mp3player.Position := Som.TempoIni;
    Executa();
    Mp3player.Play;
  end;
end;

procedure TFmPrincipal.LabelMouseEnter(Sender: TObject);
begin
  EdMouse.ValueVariant := TLabel(Sender).Tag;
end;

procedure TFmPrincipal.LabelDblClick(Sender: TObject);
var
  nEst, nLin, nSom: Integer;
begin
  ObtemSomDeLabel(Sender, nEst, nLin, nSom);
  EditarLetraDaMusica(nEst, nLin, nSom);
end;

procedure TFmPrincipal.MostraOcultaPanels(ByStep: Boolean);
var
  I, J: Integer;
  TempoMP3, TempoPan: Double;
  Visivel, Pular: Boolean;
begin
  TempoMP3 := mp3player.Position;
  for I := 0 to High(FMusicas[FMusicaSel].Estrofes) do
  begin
    //TempoPan := FMusicas[FMusicaSel].Estrofes[I].TempoHide;
    TempoPan := FMusicas[FMusicaSel].Estrofes[I].TempoHide * 86400000;
    Visivel := ((TempoPan = 0) or (TempoPan > TempoMP3));
    Pular := (not Visivel) and (CkOcultaestLidas.Checked);
    //if FPanels[I].Visible <> Visivel then
        //FPanels[I].Visible := Visivel;
    (*
    if FAltPan[I] < FScrollBox.VertScrollBar.Position then
    begin
      if I = 0 then
        FScrollBox.VertScrollBar.Position := 0
      else
        FScrollBox.VertScrollBar.Position := FAltPan[I-1];
    end else*)
    if Pular and (FScrollBox.VertScrollBar.Position < FAltPan[I]) then
    begin
      if ByStep then
      begin
        FScrollFim := FAltPan[I];
        FScrollRun := True;
        TimerScroll.Interval := Trunc(1000 / FFontSize);
        TimerScroll.Enabled := True;
      end else
      begin
        FScrollBox.VertScrollBar.Position := FAltPan[I];
      end;
    end;
  end;
end;

procedure TFmPrincipal.Mp3playerClick(Sender: TObject; Button: TMPBtnType;
  var DoDefault: Boolean);
var
  I: Integer;
begin
  case Button of
    TMPBtnType.btPlay: Executa();
    TMPBtnType.btStop:
    begin
      DoDefault := False;
      TimerScroll.Enabled := False;
      mp3player.EnabledButtons := [TMPBtnType.btPlay];
      TimerLabels.Enabled := False;
      TimerPanels.Enabled := False;
      mp3player.Stop;
      mp3player.Position := 0;
      SetGray();
    end;
    TMPBtnType.btEject:
    begin
      DoDefault := False;
      TimerScroll.Enabled := False;
      BtStopClicked();
      SetGray();
    end;
    TMPBtnType.btPause:
    begin
     TimerLabels.Enabled := False;
     TimerPanels.Enabled := False;
     TimerScroll.Enabled := False;
    end;
    TMPBtnType.btPrev:
    begin
      FScrollBox.VertScrollBar.Position := 0;
      //ProgressTimer.Enabled := False;
      //Progress.Max := 0;
      FSeqTNow := 0;
      FSeqTPrv := 0;
      mp3player.Position := 0;
      Progress.Position := 0;
      SetGray();
    end;
  end;
  StatusPlayer();
end;

procedure TFmPrincipal.Mp3playerPostClick(Sender: TObject; Button: TMPBtnType);
begin
  StatusPlayer();
  //if Button = TMPBtnType.btStop then
    //BtStopClicked();
end;

procedure TFmPrincipal.Musicascarregadas1Click(Sender: TObject);
var
  I: Integer;
  Lista: String;
begin
  Lista := '';
  for I := 0 to Length(FMusicas) - 1 do
    Lista := FMusicas[I].NoOriMusica + ' | ' + FMusicas[I].NomeMusica + sLineBreak;
  Geral.MB_Info(Lista);
end;

procedure TFmPrincipal.NovaMsica1Click(Sender: TObject);
begin
(*
  if Trim(FLyrFile) = '' then
  begin
    Geral.MB_Aviso('Pasta default n�o definida!');
    Exit;
  end;
*)
  FmEditMusica.ImgTipo.SQLType := stIns;
  //
  FmEditMusica.BtOK.Enabled := True;
  FmEditMusica.Show;
end;

procedure TFmPrincipal.ObtemSomDeLabel(const Sender: TObject; var nEst, nLin,
  nSom: Integer);
var
  I, J, K, N: Integer;
begin
  nEst := 1;
  nLin := 1;
  nSom := 1;
  N := 0;
  for I := 0 to High(FMusicas[FMusicaSel].Estrofes) do
  begin
    for J := 0 to High(FMusicas[FMusicaSel].Estrofes[I].Linhas) do
    begin
      for K := 0 to High(FMusicas[FMusicaSel].Estrofes[I].Linhas[J].Sons) do
      begin
        N := N + 1;
        if N = TLabel(Sender).Tag then
        begin
          nEst := I + 1;
          nLin := J + 1;
          nSom := K + 1;
        end;
      end;
    end;
  end;
end;

procedure TFmPrincipal.PanelMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
(*
  if Button :=
  FMouseDown := True;
  Geral.MB_Info('Mouse Down');
*)
end;

procedure TFmPrincipal.PanelMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  //Geral.MB_Info('Mouse Move');
end;

procedure TFmPrincipal.PanelMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
(*
  FMouseDown := False;
  Geral.MB_Info('Mouse Up');
*)
end;

procedure TFmPrincipal.ParaLoop();
begin
  mp3player.EnabledButtons := [];
  TimerLabels.Enabled := False;
  TimerPanels.Enabled := False;
end;

procedure TFmPrincipal.Pastadefault1Click(Sender: TObject);
begin
  FmSelMusica.Show;
end;

procedure TFmPrincipal.TimerLabelsTimer(Sender: TObject);
var
  Tempo: TDateTime;
  SeqNow, SeqNex, Item, milisec, I, Labels: Integer;
  ProcLin, Posicao: Integer;
begin
  Posicao := mp3player.Position + FmSound.FAjusteTempo;
  if Posicao < 0 then
    Posicao := 0;
  StatusPlayer();
  ProcLin := 0;
  try
    if (Progress.Max > 0) and (mp3player.Enabled) then
    begin
      ProcLin := 1;
      Labels := High(FLabels);
      ProcLin := 2;
      //Progress.Position := mp3player.Position;
      Progress.Position := Posicao;
      ProcLin := 3;
      //LaTempo.Caption := FormatDateTime('nn:ss:zzz',  mp3player.Position / 86400000);
      LaTempo.Caption := FormatDateTime('nn:ss:zzz',  Posicao / 86400000);
      ProcLin := 4;
      //Milisec := mp3player.Position;
      Milisec := Posicao;
      ProcLin := 5;
      Tempo := Milisec / 86400000;
      ProcLin := 6;
      //
      SeqNow := Milisec div FStep;
      ProcLin := 7;
      if SeqNow < 1 then Exit;
      ProcLin := 8;
      for I := FSeqTNow + 1 to SeqNow do
      begin
      ProcLin := 9;
        if I <= FMaxSom + 1 then
        begin
      ProcLin := 10;
          Item := FSeqI[I - 1];
      ProcLin := 11;
          if (Item > 0) and (Item <= Labels) then
          begin
      ProcLin := 12;
            //if FLabels[Item] <> nil then
            if Assigned(FLabels[Item]) then
              if FLabels[Item].Font <> nil then
                FLabels[Item].Font.Color := FCoresShow[Item];
      ProcLin := 13;
          end;
        end;
      end;
      FSeqTNow := SeqNow;
      ProcLin := 14;
      //
      SeqNex := FSeqTNow + FGapPrev;
      ProcLin := 15;
      if SeqNex < 1 then Exit;
      ProcLin := 16;
      for I := FSeqTPrv + 1 to SeqNex do
      begin
        if I <= FMaxSom + 1 then
        begin
      ProcLin := 17;
          Item := FSeqI[I - 1];
      ProcLin := 18;
          if (Item > 0) and (Item <= Labels) then
            if FLabels[Item] <> nil then
              if FLabels[Item].Font <> nil then
                FLabels[Item].Font.Color := FCoresPrev[Item];
      ProcLin := 19;
        end;
      end;
      FSeqTPrv := SeqNex;
      ProcLin := 20;
    end;
  except
    on E: Exception do
    begin
      ShowMessage('Erro no ProgressTimerTimer() ProcLin ' + Geral.FF0(ProcLin));
      raise EAbort.Create('');
    end;
  end;
end;

procedure TFmPrincipal.Recarrega1Click(Sender: TObject);
begin
//  CarregaArquivoLyrico_Txt(FLyrFile, Progress);
  FmSelMusica.Carrega();
end;

procedure TFmPrincipal.SetGray();
var
  I: Integer;
begin
  if not CkSetGray.Checked then Exit;
  //
  for I := 1 to Length(FLabels) - 1 do
  begin
    FLabels[I].Font.Color := FCoresGray[I];
  end;
end;

procedure TFmPrincipal.StatusPlayer();
begin
  case mp3player.Mode of
    mpNotReady  : LaPlayerStatus.Caption := 'N�o pronto';
    mpStopped   : LaPlayerStatus.Caption := 'Parado';
    mpPlaying   : LaPlayerStatus.Caption := 'Reproduzindo';
    mpRecording : LaPlayerStatus.Caption := 'Gravando';
    mpSeeking   : LaPlayerStatus.Caption := 'Procurando';
    mpPaused    : LaPlayerStatus.Caption := 'Pausado';
    mpOpen      : LaPlayerStatus.Caption := 'Carregado';
    else LaPlayerStatus.Caption := '?????: ' + Geral.FF0(Integer(mp3player.Mode));
  end;
end;

procedure TFmPrincipal.TimerPanelsTimer(Sender: TObject);
begin
  MostraOcultaPanels(True);
end;

procedure TFmPrincipal.TimerScrollTimer(Sender: TObject);
begin
  if FScrollBox.VertScrollBar.Position < FScrollFim then
    FScrollBox.VertScrollBar.Position := FScrollBox.VertScrollBar.Position + 1  // Parei aqui
  else
    TimerScroll.Enabled := False;
end;

procedure TFmPrincipal.TimerVozTimer(Sender: TObject);
var
  Tempo: TDateTime;
  SeqNow, Item, milisec, I: Integer;
begin
{
  Tempo  := (Now() - FInicio);
  SeqNow := Trunc(Tempo * 8640000);
  Milisec := SeqNow * FStep;
  if SeqNow < 1 then Exit;
  for I := FSeqT + 1 to SeqNow do
  begin
    Item := FSeqI[I - 1];
    if Item > 0 then
      FLabels[Item].Font.Color := clRed;
  end;
  FSeqT := SeqNow;
(*
  FSeqT := FSeqT + 1;
  Milisec := FSeqT * FStep;
  Item := FSeqI[FSeqT - 1];
  if Item > 0 then
    FLabels[Item].Font.Color := clRed;
*)
  if Milisec div 100 =  Milisec / 100 then
    Label1.Caption := FormatDateTime('nn:ss:zzz',  Milisec / 86400000);
  //
(*
  if Milisec > FMaxTempo then
    TimerVoz.Enabled := False;
*)
}
end;


procedure TFmPrincipal.VaiPara();
var
  Miliseconds: Integer;
begin
  Miliseconds :=
    (EdHor.ValueVariant * 3600000) +
    (EdMin.ValueVariant * 60000) +
    (EdSeg.ValueVariant * 1000) +
    EdMil.ValueVariant;
  //Position := Trunc(Miliseconds * 86400000);
  mp3player.Position := Miliseconds;
end;

(*

001|01|001700|Tolos
001|01|002100| pen
001|01|002500|sam
001|01|002700| a
001|01|003000|ssim:

001|02|005000|"Deus
001|02|005300| n�o
001|02|005600| tem
001|02|005900| para
001|02|006200| mi
001|02|006800|-im,

001|03|009000|Qual
001|03|009300|quer
001|03|009600| im
001|03|010000|por
001|03|010500|t�n
001|03|011100|cia"

001|04|015400|Po
001|04|015600|r�m
001|04|015800| a
001|04|015900| co
001|04|016200|rrup
001|04|016500|��o

001|05|018800|e as
001|05|019100| injus
001|05|019300|ti�as
001|05|019700| lhes
001|05|020100| s�o
001|05|020400| nor
001|05|020700|mais.

001|06|022800|N�o
001|06|023100| h�
001|06|023400| quem
001|06|023700| fa
001|06|024000|�a
001|06|024300| o
001|06|024600| be
001|06|024900|-em

002|08|028000|L�
002|08|029000| do
002|08|029300| c�u,
002|08|030200| o
002|08|030700|lha
002|08|031400| Deus
002|08|031800| pa
002|08|032100|ra
002|08|032500| os
002|08|032800| ho
002|08|033400|me
002|08|033700|n
002|08|034000|s

002|09|034800|Pra
002|09|035400| ver
002|09|035700| que
002|09|036200|m o
002|09|036500| bus
002|09|037200|ca

002|10|038300|Pra
002|10|038900| ver
002|10|039200| quem
002|10|039500| o
002|10|039600| a
002|10|040500|ma

003|12|043300|Mas
003|12|043900| to
003|12|044200|dos
003|12|044600| sa
003|12|045000|�
003|12|045500|ram
003|12|046000| do
003|12|046500| ca
003|12|047000|mi
003|12|047800|nho

003|13|050000|e
003|13|050400| s�o
003|13|050900| to
003|13|051500|dos
003|13|052000| i
003|13|052500|gual
003|13|053000|men
003|13|053500|te
003|13|053800| ma
003|13|054200|us

003|14|057000|N�o
003|14|057300| h�
003|14|057600| ma
003|14|057900|is
003|14|058200| quem
003|14|058500| fa
003|14|058800|�a
003|14|059100| o
003|14|059300| que
003|14|059600| �
003|14|060000| di
003|14|060700|rei
003|14|061700|to

003|15|064000|N�o
003|15|064500| h�
003|15|064800| me
003|15|065100|smo
003|15|065500| u
003|15|066100|ma
003|15|066600| s�
003|15|067200| pee
003|15|068200|sso
003|15|068600|o
003|15|069000|o
003|15|069500|o
003|15|070200|-a
003|15|070600|a
003|15|071100|a

004|17|079100|E
004|17|079300| assim
004|17|079600| Deus
004|17|079900| em
004|17|080000| grande
004|17|080200| a
004|17|080500|mor

004|18|083000|Pro
004|18|083300|me
004|18|084000|teu e
004|18|084600| cum
004|18|084800|priu

004|19|086400|Ao
004|19|086700| en
004|19|087000|vi
004|19|087300|ar
004|19|087700| seu
004|19|088200| fi
004|19|088900|lho

004|20|091200|Que
004|20|091500| mo
004|20|091900|rreu
004|21|092900|e
004|21|093100| a
004|21|093400|ssi
004|21|093800|-im
004|21|094100| pa
004|21|094400|gou

004|22|096900|Com
004|22|097100| pre
004|22|097400|ci
004|22|097600|o
004|22|097800|so
004|22|098000| san
004|22|098200|gue

004|23|099900|E
004|23|100300| os
004|23|100500| res
004|23|100800|ga
004|23|101200|tou
004|23|101800| da
004|23|102100| mor
004|23|102700|te

005|25|105400|L�
005|25|106500| do
005|25|106700| c�u,
005|25|107800| o
005|25|108300|lha
005|25|109000| Deus
005|25|109400| pa
005|25|109600|ra
005|25|109900| os
005|25|110200| ho
005|25|111000|me
005|25|111300|n
005|25|111600|s

005|26|112400|Pra
005|26|113000| ver
005|26|113300| que
005|26|113800|m o
005|26|114100| bus
005|26|114800|ca

005|27|115900|Pra
005|27|116500| ver
005|27|116800| quem
005|27|117100| o
005|27|117200| a
005|27|118100|ma

006|01|121000|Mas
006|01|121600| to
006|01|121900|dos
006|01|122300| sa
006|01|122700|�
006|01|123200|ram
006|01|123700| do
006|01|124200| ca
006|01|124700|mi
006|01|125500|nho

006|02|127700|e
006|02|128100| s�o
006|02|128600| to
006|02|129200|dos
006|02|129700| i
006|02|130200|gual
006|02|130700|men
006|02|131200|te
006|02|131400|e
006|02|131600|e
006|02|131800| ma
006|02|133100|us

006|03|134700|Mas
006|03|135000| to
006|03|135300|do
006|03|135600| a
006|03|135900|que
006|03|136200|le
006|03|136500| que
006|03|136800| a
006|03|137400|rre
006|03|137700|pen
006|03|138100|der
006|03|139100|-se

006|04|141700|E
006|04|142200| pe
006|04|142500|dir
006|04|142800| per
006|04|143200|d�o
006|04|143800| a
006|04|144300| Deus
006|04|144900| te
006|04|145000|ee
006|04|145600|r�
006|04|147200| te
006|04|147700|r�
006|04|148000|�
006|04|148300|�

007|06|148500|O A amo
007|06|149100|o
007|06|149700|o
007|06|150300|o
007|06|150900|o
007|06|151300|or
007|06|151600| de
007|06|151900| De
007|06|152500|e
007|06|153100|e
007|06|153700|us
007|06|154500| du
007|06|154800|ra
007|06|155100| pra
007|06|155600| sem
007|06|156000|pre

007|07|158100|Du
007|07|158400|ra
007|07|158700| pra
007|07|159000| sem
007|07|159300|pre

007|08|161400|O A amo
007|08|162000|o
007|08|162600|o
007|08|163200|o
007|08|163800|o
007|08|164200|or
007|08|164500| de
007|08|164800| De
007|08|165400|e
007|08|166000|e
007|08|166600|us
007|08|167400| du
007|08|167700|ra
007|08|168000| pra
007|08|168500| sem
007|08|168900|pre

008|010|178000|Mas
008|010|178600| to
008|010|178900|dos
008|010|179300| sa
008|010|179700|�
008|010|180200|ram
008|010|180700| do
008|010|181200| ca
008|010|181700|mi
008|010|182500|nho

008|011|184700|e
008|011|185100| s�o
008|011|185600| to
008|011|186200|dos
008|011|186700| i
008|011|187200|gual
008|011|187700|men
008|011|188200|te
008|011|188400|e
008|011|188600|e
008|011|188800| ma
008|011|190100|us

008|012|191700|Mas
008|012|192000| to
008|012|192300|do
008|012|192600| a
008|012|192900|que
008|012|193200|le
008|012|193500| que
008|012|193800| a
008|012|194400|rre
008|012|194700|pen
008|012|195000|.
008|012|195300|.
008|012|195600|.
008|012|195800|de
008|012|196200|e
008|012|196600|er
008|012|197100|-se

008|013|198700|E
008|013|199200| pe
008|013|199500|dir
008|013|199800| per
008|013|200200|d�o
008|013|200800| a
008|013|201300| Deus
008|013|201900| te
008|013|202000|ee
008|013|202600|r�

008|014|205300|E
008|014|205800| pe
008|014|206100|dir
008|014|206400| per
008|014|206800|d�o
008|014|207400| a
008|014|207900| Deus
008|014|208500| te
008|014|208600|ee
008|014|209200|r�
008|014|209700|�
008|014|210200|�
008|014|210700|�.

008|014|211500| Te
008|014|211800|r�
008|014|212100|�
008|014|212400|�
008|014|212600| Hei!

009|16|213800|Oh
009|16|214100| Oh
009|16|214400| Oh
009|16|214700| Ho
009|16|215300|-o
009|16|215900| ou
009|16|217800| o
009|16|218100| o
009|16|218500|-o
009|16|218700| o
009|16|219000| ou
009|16|219500| I��h

009|17|221100|Hei!
009|17|222200| Hou!
009|17|223100| Hei!
009|17|224000| Hou!
009|17|224700| Hei!

009|18|226300|

009|19|226300|O A amo
009|19|226900|o
009|19|227500|o
009|19|228100|o
009|19|228700|o
009|19|229100|or
009|19|229400| de
009|19|229700| De
009|19|230300|e
009|19|230900|e
009|19|231500|us
009|19|232300| du
009|19|232600|ra
009|19|232900| pra
009|19|233400| sem
009|19|233800|pre

009|20|235900|Du
009|20|236200|ra
009|20|236500| pra
009|20|236800| sem
009|20|237100|pre

009|21|239200|O A amo
009|21|239800|o
009|21|240400|o
009|21|241000|o
009|21|241600|o
009|21|242000|or
009|21|242300| de
009|21|242600| De
009|21|243200|e
009|21|243800|e
009|21|244400|us
009|21|245200| du
009|21|245500|ra
009|21|245800| pra
009|21|246300| sem
009|21|246700|pre

009|22|248800|Du
009|22|249100|ra
009|22|249400| pra
009|22|249700| sem
009|22|250000|pre


009|23|252100|O A amo
009|23|252700|o
009|23|253300|o
009|23|253900|o
009|23|254500|o
009|23|254900|or
009|23|255200| de
009|23|255500| De
009|23|256100|e
009|23|256700|e
009|23|257300|us
009|23|258100| du
009|23|258400|ra
009|23|258700| pra
009|23|259200| sem
009|23|259600|pre

009|24|261700|Du
009|24|262000|ra
009|24|262300| pra
009|24|262600| sem
009|24|262900|pre
*)

(*
object ScrollBox1: TScrollBox
  Left = 0
  Top = 41
  Width = 1004
  Height = 499
  Align = alClient
  TabOrder = 2
  Visible = False
  ExplicitLeft = 44
  ExplicitTop = 73
end
*)

end.
