unit EditSom;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEditCB,
  dmkDBLookupComboBox, dmkEdit, UnApp_PF, UnArr_PF, UnDmkProcFunc;

type
  TFmEditSom = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    GroupBox2: TGroupBox;
    Label6: TLabel;
    Label7: TLabel;
    EdControle: TdmkEdit;
    EdNome: TdmkEdit;
    GroupBox3: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    EdSeqSom: TdmkEdit;
    EdTextoSom: TdmkEdit;
    EdTempoIni: TdmkEdit;
    EdTempoTam: TdmkEdit;
    EdTextoImp: TdmkEdit;
    Label5: TLabel;
    GroupBox5: TGroupBox;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    SbTitCorC: TSpeedButton;
    ShCorGray: TShape;
    LaTitCorC: TLabel;
    SbTitCorA: TSpeedButton;
    ShCorPrev: TShape;
    LaTitCorA: TLabel;
    SbTitCorB: TSpeedButton;
    ShCorShow: TShape;
    LaTitCorB: TLabel;
    ColorDialog1: TColorDialog;
    Shape01: TShape;
    Shape11: TShape;
    Shape21: TShape;
    Shape22: TShape;
    Shape12: TShape;
    Shape02: TShape;
    Shape03: TShape;
    Shape13: TShape;
    Shape23: TShape;
    Shape24: TShape;
    Shape14: TShape;
    Shape04: TShape;
    Shape05: TShape;
    Shape15: TShape;
    Shape25: TShape;
    Shape26: TShape;
    Shape16: TShape;
    Shape06: TShape;
    Shape07: TShape;
    Shape17: TShape;
    Shape27: TShape;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbTitCorCClick(Sender: TObject);
    procedure SbTitCorAClick(Sender: TObject);
    procedure SbTitCorBClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdTempoIniKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdTextoSomRedefinido(Sender: TObject);
    procedure EdTextoImpKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdTempoTamKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Shape01MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    { Private declarations }
    function  SelecionaCor(Shape: TShape): Boolean;
    procedure TempoMilisegundos(Sender: Tobject);
  public
    { Public declarations }
    FMusica  : TMusica;
    FEstrofe : TEstrofe;
    FLinha   : TLinha;
    FSom     : TSom;
    procedure ColoreShapesInsUpd();
  end;

  var
  FmEditSom: TFmEditSom;

implementation

uses UnMyObjects, EditMusica, Principal;

{$R *.DFM}

procedure TFmEditSom.BtOKClick(Sender: TObject);
var
  nEst, nLin, nSom: Integer;
  Ini, Fim: TTime;
begin
  Screen.Cursor := crHourGlass;
  try
    nEst := FmEditMusica.FnEst;
    nLin := FmEditMusica.FnLin;
    nSom := FmEditMusica.FnSom;
    Ini := Geral.StrToTimeZZZ(FmEditSom.EdTempoIni.Text);
    Fim := Geral.StrToTimeZZZ(FmEditSom.EdTempoTam.Text);
    FSom.TempoIni      := Round(Ini * 86400000);
    FSom.TempoTam      := Round(Fim * 86400000);
    FSom.TextoSom      := EdTextoSom.ValueVariant;
    FSom.TextoImp      := EdTextoImp.ValueVariant;
    FSom.CorGray       := ShCorGray.Brush.Color;
    FSom.CorPrev       := ShCorPrev.Brush.Color;
    FSom.CorShow       := ShCorShow.Brush.Color;

    //
  (*
    FmEditSom.EdTextoSom.ValueVariant := Som.TextoSom;
    FmEditSom.EdTextoImp.ValueVariant := Som.TextoImp;
    FmEditSom.ShCorGray.Brush.Color   := Som.CorGray;
    FmEditSom.ShCorPrev.Brush.Color   := Som.CorPrev;
    FmEditSom.ShCorShow.Brush.Color   := Som.CorShow;
  *)



(*
    nSom := EdSeqSom.ValueVariant;
    FmEditMusica.SGSons.Cells[2, nSom + 1] := Geral.FDT(FSom.TempoIni / 86400000, 111);
    FmEditMusica.SGSons.Cells[3, nSom + 1] := Geral.FDT(FSom.TempoTam / 86400000, 111);
    FmEditMusica.SGSons.Cells[4, nSom + 1] := FSom.TextoSom;
    FmEditMusica.SGSons.Cells[5, nSom + 1] := FSom.TextoImp;
    FmEditMusica.SGSons.Cells[6, nSom + 1] := Geral.FF0(FSom.CorGray);
    FmEditMusica.SGSons.Cells[7, nSom + 1] := Geral.FF0(FSom.CorPrev);
    FmEditMusica.SGSons.Cells[8, nSom + 1] := Geral.FF0(FSom.CorShow);
    //
*)
    FmEditMusica.SGEstrofes.Row := nEst;
    FmEditMusica.MostraLinhasDaEstrofe(nEst, nLin);
    //FmEditMusica.SGLinhas.Row := nLin;
    FmEditMusica.MostraSonsDaLinha(nEst, nLin, nSom);
    //FmEditMusica.SGSons.Row := nSom;
    //

  finally
    Screen.Cursor := crDefault;
  end;
  //
  Hide;
  if FmEditMusica.FContinuar then
    FmEditMusica.InsereNovoSom(vpLast);
end;

procedure TFmEditSom.BtSaidaClick(Sender: TObject);
begin
  FmEditMusica.FContinuar := False;
  Close;
end;

procedure TFmEditSom.ColoreShapesInsUpd();
var
  SelGray, SelPrev, SelShow: TColor;
begin
  case FmPrincipal.FShemaCores of
    1:
    begin
      SelGray := Shape01.Brush.Color;
      SelPrev := Shape11.Brush.Color;
      SelShow := Shape21.Brush.Color;
    end;
    2:
    begin
      SelGray := Shape02.Brush.Color;
      SelPrev := Shape12.Brush.Color;
      SelShow := Shape22.Brush.Color;
    end;
    3:
    begin
      SelGray := Shape03.Brush.Color;
      SelPrev := Shape13.Brush.Color;
      SelShow := Shape23.Brush.Color;
    end;
    4:
    begin
      SelGray := Shape04.Brush.Color;
      SelPrev := Shape14.Brush.Color;
      SelShow := Shape24.Brush.Color;
    end;
    5:
    begin
      SelGray := Shape05.Brush.Color;
      SelPrev := Shape15.Brush.Color;
      SelShow := Shape25.Brush.Color;
    end;
    6:
    begin
      SelGray := Shape06.Brush.Color;
      SelPrev := Shape16.Brush.Color;
      SelShow := Shape26.Brush.Color;
    end;
    7:
    begin
      SelGray := Shape07.Brush.Color;
      SelPrev := Shape17.Brush.Color;
      SelShow := Shape27.Brush.Color;
    end;
  end;
  ShCorGray.Brush.Color := SelGray;
  ShCorPrev.Brush.Color := SelPrev;
  ShCorShow.Brush.Color := SelShow;
end;

procedure TFmEditSom.EdTempoIniKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    TempoMilisegundos(Sender);
end;

procedure TFmEditSom.EdTempoTamKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  TempoMilisegundos(Sender);
end;

procedure TFmEditSom.EdTextoImpKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    EdTextoImp.Text := EdTextoSom.Text;
end;

procedure TFmEditSom.EdTextoSomRedefinido(Sender: TObject);
begin
  if EdTextoImp.Text = '' then
    EdTextoImp.Text := EdTextoSom.Text;
end;

procedure TFmEditSom.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEditSom.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmEditSom.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEditSom.SbTitCorAClick(Sender: TObject);
begin
  SelecionaCor(ShCorPrev);
end;

procedure TFmEditSom.SbTitCorBClick(Sender: TObject);
begin
  SelecionaCor(ShCorShow);
end;

procedure TFmEditSom.SbTitCorCClick(Sender: TObject);
begin
  SelecionaCor(ShCorGray);
end;

function TFmEditSom.SelecionaCor(Shape: TShape): Boolean;
begin
  ColorDialog1.Color := Shape.Brush.Color;
  if ColorDialog1.Execute then
    TShape(Shape).Brush.Color := ColorDialog1.Color;
(*
  Result := False;
  if MyObjects.CriaForm_AcessoTotal(TFmCoresVCLSkin, FmCoresVCLSkin) then
  begin
    FmCoresVCLSkin.ShowModal;
    Shape.Color := FmCoresVCLSkin.FCor;
    FmCoresVCLSkin.Destroy;
  end;
*)
end;

procedure TFmEditSom.Shape01MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  FmPrincipal.FShemaCores := Geral.IMV(TShape(Sender).Name[7]);
  ColoreShapesInsUpd();
end;

procedure TFmEditSom.TempoMilisegundos(Sender: Tobject);
var
  Texto: String;
  Inteiro: Integer;
  Tempo: TTime;
begin
  try
    Tempo := Geral.StrToTimeZZZ(TdmkEdit(Sender).Text);
  except
    Tempo := 0;
  end;
  Inteiro := Round(Tempo * 86400000);
  Texto := IntToStr(Inteiro);
  if InputQuery('Tempo em Milisegunos', 'Informe o tempo em milisegundos',
  Texto) then
  begin
    Inteiro := Geral.IMV(Texto);
    Texto   := Geral.FDT(Inteiro / 86400000, 111);
    //
    TdmkEdit(Sender).Text := Texto;
  end;
end;

end.
