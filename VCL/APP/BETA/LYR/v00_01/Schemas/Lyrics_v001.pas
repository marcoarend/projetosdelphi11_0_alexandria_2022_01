
{***************************************************************************************************}
{                                                                                                   }
{                                         XML Data Binding                                          }
{                                                                                                   }
{         Generated on: 03/07/2016 20:47:44                                                         }
{       Generated from: C:\_Compilers\DELPHI_XE2\VCL\APP\BETA\LYR\v00_01\Schemas\Lyrics_v0.01.xsd   }
{   Settings stored in: C:\_Compilers\DELPHI_XE2\VCL\APP\BETA\LYR\v00_01\Schemas\Lyrics_v0.01.xdb   }
{                                                                                                   }
{***************************************************************************************************}

unit Lyrics_v001;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLTLyrics = interface;
  IXMLMusicas = interface;
  IXMLEstrofes = interface;
  IXMLEstrofesList = interface;
  IXMLLinhas = interface;
  IXMLLinhasList = interface;
  IXMLSons = interface;
  IXMLTSom = interface;

{ IXMLTLyrics }

  IXMLTLyrics = interface(IXMLNode)
    ['{47AF94F7-B844-45A8-82BD-1C79B0467503}']
    { Property Accessors }
    function Get_Versao: UnicodeString;
    function Get_Musicas: IXMLMusicas;
    procedure Set_Versao(Value: UnicodeString);
    { Methods & Properties }
    property Versao: UnicodeString read Get_Versao write Set_Versao;
    property Musicas: IXMLMusicas read Get_Musicas;
  end;

{ IXMLMusicas }

  IXMLMusicas = interface(IXMLNode)
    ['{A803CF74-C3D7-4B4C-B308-62FA8648DD5B}']
    { Property Accessors }
    function Get_NomeMusica: UnicodeString;
    function Get_NomeHinario: UnicodeString;
    function Get_NoOriMusica: UnicodeString;
    function Get_NoOriArtista: UnicodeString;
    function Get_NoOriMidia: UnicodeString;
    function Get_DataLetra: UnicodeString;
    function Get_EstrPorTela: UnicodeString;
    function Get_FontName: UnicodeString;
    function Get_FontSize: UnicodeString;
    function Get_LablAltu: UnicodeString;
    function Get_FontBold: UnicodeString;
    function Get_Estrofes: IXMLEstrofesList;
    procedure Set_NomeMusica(Value: UnicodeString);
    procedure Set_NomeHinario(Value: UnicodeString);
    procedure Set_NoOriMusica(Value: UnicodeString);
    procedure Set_NoOriArtista(Value: UnicodeString);
    procedure Set_NoOriMidia(Value: UnicodeString);
    procedure Set_DataLetra(Value: UnicodeString);
    procedure Set_EstrPorTela(Value: UnicodeString);
    procedure Set_FontName(Value: UnicodeString);
    procedure Set_FontSize(Value: UnicodeString);
    procedure Set_LablAltu(Value: UnicodeString);
    procedure Set_FontBold(Value: UnicodeString);
    { Methods & Properties }
    property NomeMusica: UnicodeString read Get_NomeMusica write Set_NomeMusica;
    property NomeHinario: UnicodeString read Get_NomeHinario write Set_NomeHinario;
    property NoOriMusica: UnicodeString read Get_NoOriMusica write Set_NoOriMusica;
    property NoOriArtista: UnicodeString read Get_NoOriArtista write Set_NoOriArtista;
    property NoOriMidia: UnicodeString read Get_NoOriMidia write Set_NoOriMidia;
    property DataLetra: UnicodeString read Get_DataLetra write Set_DataLetra;
    property EstrPorTela: UnicodeString read Get_EstrPorTela write Set_EstrPorTela;
    property FontName: UnicodeString read Get_FontName write Set_FontName;
    property FontSize: UnicodeString read Get_FontSize write Set_FontSize;
    property LablAltu: UnicodeString read Get_LablAltu write Set_LablAltu;
    property FontBold: UnicodeString read Get_FontBold write Set_FontBold;
    property Estrofes: IXMLEstrofesList read Get_Estrofes;
  end;

{ IXMLEstrofes }

  IXMLEstrofes = interface(IXMLNode)
    ['{371A948E-8923-4B98-A5D2-5DD6A1330C2C}']
    { Property Accessors }
    function Get_NoEst: UnicodeString;
    function Get_TempoShow: UnicodeString;
    function Get_TempoHide: UnicodeString;
    function Get_Linhas: IXMLLinhasList;
    procedure Set_NoEst(Value: UnicodeString);
    procedure Set_TempoShow(Value: UnicodeString);
    procedure Set_TempoHide(Value: UnicodeString);
    { Methods & Properties }
    property NoEst: UnicodeString read Get_NoEst write Set_NoEst;
    property TempoShow: UnicodeString read Get_TempoShow write Set_TempoShow;
    property TempoHide: UnicodeString read Get_TempoHide write Set_TempoHide;
    property Linhas: IXMLLinhasList read Get_Linhas;
  end;

{ IXMLEstrofesList }

  IXMLEstrofesList = interface(IXMLNodeCollection)
    ['{E29D4B4B-4D60-4FC7-AE60-432E43503CDE}']
    { Methods & Properties }
    function Add: IXMLEstrofes;
    function Insert(const Index: Integer): IXMLEstrofes;

    function Get_Item(Index: Integer): IXMLEstrofes;
    property Items[Index: Integer]: IXMLEstrofes read Get_Item; default;
  end;

{ IXMLLinhas }

  IXMLLinhas = interface(IXMLNodeCollection)
    ['{4093658C-AC45-4A96-BC0C-1AFA1F742C01}']
    { Property Accessors }
    function Get_Sons(Index: Integer): IXMLSons;
    { Methods & Properties }
    function Add: IXMLSons;
    function Insert(const Index: Integer): IXMLSons;
    property Sons[Index: Integer]: IXMLSons read Get_Sons; default;
  end;

{ IXMLLinhasList }

  IXMLLinhasList = interface(IXMLNodeCollection)
    ['{8E5058C2-E0FB-4E49-977C-983C10F9E27B}']
    { Methods & Properties }
    function Add: IXMLLinhas;
    function Insert(const Index: Integer): IXMLLinhas;

    function Get_Item(Index: Integer): IXMLLinhas;
    property Items[Index: Integer]: IXMLLinhas read Get_Item; default;
  end;

{ IXMLSons }

  IXMLSons = interface(IXMLNode)
    ['{5B855B52-D813-4C35-BC81-13952F9C5C0C}']
    { Property Accessors }
    function Get_Item: IXMLTSom;
    { Methods & Properties }
    property Item: IXMLTSom read Get_Item;
  end;

{ IXMLTSom }

  IXMLTSom = interface(IXMLNode)
    ['{2136E1F7-E5EC-4367-82D6-D1A7262D7CAC}']
    { Property Accessors }
    function Get_Versao: UnicodeString;
    function Get_Codigo: UnicodeString;
    function Get_Coluna: UnicodeString;
    function Get_Linha: UnicodeString;
    function Get_SeqInLin: UnicodeString;
    function Get_TempoIni: UnicodeString;
    function Get_TempoTam: UnicodeString;
    function Get_TextoSom: UnicodeString;
    function Get_TextoImp: UnicodeString;
    function Get_CorGray: UnicodeString;
    function Get_CorPrev: UnicodeString;
    function Get_CorShow: UnicodeString;
    procedure Set_Versao(Value: UnicodeString);
    procedure Set_Codigo(Value: UnicodeString);
    procedure Set_Coluna(Value: UnicodeString);
    procedure Set_Linha(Value: UnicodeString);
    procedure Set_SeqInLin(Value: UnicodeString);
    procedure Set_TempoIni(Value: UnicodeString);
    procedure Set_TempoTam(Value: UnicodeString);
    procedure Set_TextoSom(Value: UnicodeString);
    procedure Set_TextoImp(Value: UnicodeString);
    procedure Set_CorGray(Value: UnicodeString);
    procedure Set_CorPrev(Value: UnicodeString);
    procedure Set_CorShow(Value: UnicodeString);
    { Methods & Properties }
    property Versao: UnicodeString read Get_Versao write Set_Versao;
    property Codigo: UnicodeString read Get_Codigo write Set_Codigo;
    property Coluna: UnicodeString read Get_Coluna write Set_Coluna;
    property Linha: UnicodeString read Get_Linha write Set_Linha;
    property SeqInLin: UnicodeString read Get_SeqInLin write Set_SeqInLin;
    property TempoIni: UnicodeString read Get_TempoIni write Set_TempoIni;
    property TempoTam: UnicodeString read Get_TempoTam write Set_TempoTam;
    property TextoSom: UnicodeString read Get_TextoSom write Set_TextoSom;
    property TextoImp: UnicodeString read Get_TextoImp write Set_TextoImp;
    property CorGray: UnicodeString read Get_CorGray write Set_CorGray;
    property CorPrev: UnicodeString read Get_CorPrev write Set_CorPrev;
    property CorShow: UnicodeString read Get_CorShow write Set_CorShow;
  end;

{ Forward Decls }

  TXMLTLyrics = class;
  TXMLMusicas = class;
  TXMLEstrofes = class;
  TXMLEstrofesList = class;
  TXMLLinhas = class;
  TXMLLinhasList = class;
  TXMLSons = class;
  TXMLTSom = class;

{ TXMLTLyrics }

  TXMLTLyrics = class(TXMLNode, IXMLTLyrics)
  protected
    { IXMLTLyrics }
    function Get_Versao: UnicodeString;
    function Get_Musicas: IXMLMusicas;
    procedure Set_Versao(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLMusicas }

  TXMLMusicas = class(TXMLNode, IXMLMusicas)
  private
    FEstrofes: IXMLEstrofesList;
  protected
    { IXMLMusicas }
    function Get_NomeMusica: UnicodeString;
    function Get_NomeHinario: UnicodeString;
    function Get_NoOriMusica: UnicodeString;
    function Get_NoOriArtista: UnicodeString;
    function Get_NoOriMidia: UnicodeString;
    function Get_DataLetra: UnicodeString;
    function Get_EstrPorTela: UnicodeString;
    function Get_FontName: UnicodeString;
    function Get_FontSize: UnicodeString;
    function Get_LablAltu: UnicodeString;
    function Get_FontBold: UnicodeString;
    function Get_Estrofes: IXMLEstrofesList;
    procedure Set_NomeMusica(Value: UnicodeString);
    procedure Set_NomeHinario(Value: UnicodeString);
    procedure Set_NoOriMusica(Value: UnicodeString);
    procedure Set_NoOriArtista(Value: UnicodeString);
    procedure Set_NoOriMidia(Value: UnicodeString);
    procedure Set_DataLetra(Value: UnicodeString);
    procedure Set_EstrPorTela(Value: UnicodeString);
    procedure Set_FontName(Value: UnicodeString);
    procedure Set_FontSize(Value: UnicodeString);
    procedure Set_LablAltu(Value: UnicodeString);
    procedure Set_FontBold(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLEstrofes }

  TXMLEstrofes = class(TXMLNode, IXMLEstrofes)
  private
    FLinhas: IXMLLinhasList;
  protected
    { IXMLEstrofes }
    function Get_NoEst: UnicodeString;
    function Get_TempoShow: UnicodeString;
    function Get_TempoHide: UnicodeString;
    function Get_Linhas: IXMLLinhasList;
    procedure Set_NoEst(Value: UnicodeString);
    procedure Set_TempoShow(Value: UnicodeString);
    procedure Set_TempoHide(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLEstrofesList }

  TXMLEstrofesList = class(TXMLNodeCollection, IXMLEstrofesList)
  protected
    { IXMLEstrofesList }
    function Add: IXMLEstrofes;
    function Insert(const Index: Integer): IXMLEstrofes;

    function Get_Item(Index: Integer): IXMLEstrofes;
  end;

{ TXMLLinhas }

  TXMLLinhas = class(TXMLNodeCollection, IXMLLinhas)
  protected
    { IXMLLinhas }
    function Get_Sons(Index: Integer): IXMLSons;
    function Add: IXMLSons;
    function Insert(const Index: Integer): IXMLSons;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLLinhasList }

  TXMLLinhasList = class(TXMLNodeCollection, IXMLLinhasList)
  protected
    { IXMLLinhasList }
    function Add: IXMLLinhas;
    function Insert(const Index: Integer): IXMLLinhas;

    function Get_Item(Index: Integer): IXMLLinhas;
  end;

{ TXMLSons }

  TXMLSons = class(TXMLNode, IXMLSons)
  protected
    { IXMLSons }
    function Get_Item: IXMLTSom;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTSom }

  TXMLTSom = class(TXMLNode, IXMLTSom)
  protected
    { IXMLTSom }
    function Get_Versao: UnicodeString;
    function Get_Codigo: UnicodeString;
    function Get_Coluna: UnicodeString;
    function Get_Linha: UnicodeString;
    function Get_SeqInLin: UnicodeString;
    function Get_TempoIni: UnicodeString;
    function Get_TempoTam: UnicodeString;
    function Get_TextoSom: UnicodeString;
    function Get_TextoImp: UnicodeString;
    function Get_CorGray: UnicodeString;
    function Get_CorPrev: UnicodeString;
    function Get_CorShow: UnicodeString;
    procedure Set_Versao(Value: UnicodeString);
    procedure Set_Codigo(Value: UnicodeString);
    procedure Set_Coluna(Value: UnicodeString);
    procedure Set_Linha(Value: UnicodeString);
    procedure Set_SeqInLin(Value: UnicodeString);
    procedure Set_TempoIni(Value: UnicodeString);
    procedure Set_TempoTam(Value: UnicodeString);
    procedure Set_TextoSom(Value: UnicodeString);
    procedure Set_TextoImp(Value: UnicodeString);
    procedure Set_CorGray(Value: UnicodeString);
    procedure Set_CorPrev(Value: UnicodeString);
    procedure Set_CorShow(Value: UnicodeString);
  end;

{ Global Functions }

function GetLyrics(Doc: IXMLDocument): IXMLTLyrics;
function LoadLyrics(const FileName: string): IXMLTLyrics;
function NewLyrics: IXMLTLyrics;

const
  TargetNamespace = 'http://www.dermatek.com.br/www/apps/Lyrics';

implementation

{ Global Functions }

function GetLyrics(Doc: IXMLDocument): IXMLTLyrics;
begin
  Result := Doc.GetDocBinding('Lyrics', TXMLTLyrics, TargetNamespace) as IXMLTLyrics;
end;

function LoadLyrics(const FileName: string): IXMLTLyrics;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('Lyrics', TXMLTLyrics, TargetNamespace) as IXMLTLyrics;
end;

function NewLyrics: IXMLTLyrics;
begin
  Result := NewXMLDocument.GetDocBinding('Lyrics', TXMLTLyrics, TargetNamespace) as IXMLTLyrics;
end;

{ TXMLTLyrics }

procedure TXMLTLyrics.AfterConstruction;
begin
  RegisterChildNode('Musicas', TXMLMusicas);
  inherited;
end;

function TXMLTLyrics.Get_Versao: UnicodeString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTLyrics.Set_Versao(Value: UnicodeString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTLyrics.Get_Musicas: IXMLMusicas;
begin
  Result := ChildNodes['Musicas'] as IXMLMusicas;
end;

{ TXMLMusicas }

procedure TXMLMusicas.AfterConstruction;
begin
  RegisterChildNode('Estrofes', TXMLEstrofes);
  FEstrofes := CreateCollection(TXMLEstrofesList, IXMLEstrofes, 'Estrofes') as IXMLEstrofesList;
  inherited;
end;

function TXMLMusicas.Get_NomeMusica: UnicodeString;
begin
  Result := ChildNodes['NomeMusica'].Text;
end;

procedure TXMLMusicas.Set_NomeMusica(Value: UnicodeString);
begin
  ChildNodes['NomeMusica'].NodeValue := Value;
end;

function TXMLMusicas.Get_NomeHinario: UnicodeString;
begin
  Result := ChildNodes['NomeHinario'].Text;
end;

procedure TXMLMusicas.Set_NomeHinario(Value: UnicodeString);
begin
  ChildNodes['NomeHinario'].NodeValue := Value;
end;

function TXMLMusicas.Get_NoOriMusica: UnicodeString;
begin
  Result := ChildNodes['NoOriMusica'].Text;
end;

procedure TXMLMusicas.Set_NoOriMusica(Value: UnicodeString);
begin
  ChildNodes['NoOriMusica'].NodeValue := Value;
end;

function TXMLMusicas.Get_NoOriArtista: UnicodeString;
begin
  Result := ChildNodes['NoOriArtista'].Text;
end;

procedure TXMLMusicas.Set_NoOriArtista(Value: UnicodeString);
begin
  ChildNodes['NoOriArtista'].NodeValue := Value;
end;

function TXMLMusicas.Get_NoOriMidia: UnicodeString;
begin
  Result := ChildNodes['NoOriMidia'].Text;
end;

procedure TXMLMusicas.Set_NoOriMidia(Value: UnicodeString);
begin
  ChildNodes['NoOriMidia'].NodeValue := Value;
end;

function TXMLMusicas.Get_DataLetra: UnicodeString;
begin
  Result := ChildNodes['DataLetra'].Text;
end;

procedure TXMLMusicas.Set_DataLetra(Value: UnicodeString);
begin
  ChildNodes['DataLetra'].NodeValue := Value;
end;

function TXMLMusicas.Get_EstrPorTela: UnicodeString;
begin
  Result := ChildNodes['EstrPorTela'].Text;
end;

procedure TXMLMusicas.Set_EstrPorTela(Value: UnicodeString);
begin
  ChildNodes['EstrPorTela'].NodeValue := Value;
end;

function TXMLMusicas.Get_FontName: UnicodeString;
begin
  Result := ChildNodes['FontName'].Text;
end;

procedure TXMLMusicas.Set_FontName(Value: UnicodeString);
begin
  ChildNodes['FontName'].NodeValue := Value;
end;

function TXMLMusicas.Get_FontSize: UnicodeString;
begin
  Result := ChildNodes['FontSize'].Text;
end;

procedure TXMLMusicas.Set_FontSize(Value: UnicodeString);
begin
  ChildNodes['FontSize'].NodeValue := Value;
end;

function TXMLMusicas.Get_LablAltu: UnicodeString;
begin
  Result := ChildNodes['LablAltu'].Text;
end;

procedure TXMLMusicas.Set_LablAltu(Value: UnicodeString);
begin
  ChildNodes['LablAltu'].NodeValue := Value;
end;

function TXMLMusicas.Get_FontBold: UnicodeString;
begin
  Result := ChildNodes['FontBold'].Text;
end;

procedure TXMLMusicas.Set_FontBold(Value: UnicodeString);
begin
  ChildNodes['FontBold'].NodeValue := Value;
end;

function TXMLMusicas.Get_Estrofes: IXMLEstrofesList;
begin
  Result := FEstrofes;
end;

{ TXMLEstrofes }

procedure TXMLEstrofes.AfterConstruction;
begin
  RegisterChildNode('Linhas', TXMLLinhas);
  FLinhas := CreateCollection(TXMLLinhasList, IXMLLinhas, 'Linhas') as IXMLLinhasList;
  inherited;
end;

function TXMLEstrofes.Get_NoEst: UnicodeString;
begin
  Result := ChildNodes['NoEst'].Text;
end;

procedure TXMLEstrofes.Set_NoEst(Value: UnicodeString);
begin
  ChildNodes['NoEst'].NodeValue := Value;
end;

function TXMLEstrofes.Get_TempoShow: UnicodeString;
begin
  Result := ChildNodes['TempoShow'].Text;
end;

procedure TXMLEstrofes.Set_TempoShow(Value: UnicodeString);
begin
  ChildNodes['TempoShow'].NodeValue := Value;
end;

function TXMLEstrofes.Get_TempoHide: UnicodeString;
begin
  Result := ChildNodes['TempoHide'].Text;
end;

procedure TXMLEstrofes.Set_TempoHide(Value: UnicodeString);
begin
  ChildNodes['TempoHide'].NodeValue := Value;
end;

function TXMLEstrofes.Get_Linhas: IXMLLinhasList;
begin
  Result := FLinhas;
end;

{ TXMLEstrofesList }

function TXMLEstrofesList.Add: IXMLEstrofes;
begin
  Result := AddItem(-1) as IXMLEstrofes;
end;

function TXMLEstrofesList.Insert(const Index: Integer): IXMLEstrofes;
begin
  Result := AddItem(Index) as IXMLEstrofes;
end;

function TXMLEstrofesList.Get_Item(Index: Integer): IXMLEstrofes;
begin
  Result := List[Index] as IXMLEstrofes;
end;

{ TXMLLinhas }

procedure TXMLLinhas.AfterConstruction;
begin
  RegisterChildNode('Sons', TXMLSons);
  ItemTag := 'Sons';
  ItemInterface := IXMLSons;
  inherited;
end;

function TXMLLinhas.Get_Sons(Index: Integer): IXMLSons;
begin
  Result := List[Index] as IXMLSons;
end;

function TXMLLinhas.Add: IXMLSons;
begin
  Result := AddItem(-1) as IXMLSons;
end;

function TXMLLinhas.Insert(const Index: Integer): IXMLSons;
begin
  Result := AddItem(Index) as IXMLSons;
end;

{ TXMLLinhasList }

function TXMLLinhasList.Add: IXMLLinhas;
begin
  Result := AddItem(-1) as IXMLLinhas;
end;

function TXMLLinhasList.Insert(const Index: Integer): IXMLLinhas;
begin
  Result := AddItem(Index) as IXMLLinhas;
end;

function TXMLLinhasList.Get_Item(Index: Integer): IXMLLinhas;
begin
  Result := List[Index] as IXMLLinhas;
end;

{ TXMLSons }

procedure TXMLSons.AfterConstruction;
begin
  RegisterChildNode('Item', TXMLTSom);
  inherited;
end;

function TXMLSons.Get_Item: IXMLTSom;
begin
  Result := ChildNodes['Item'] as IXMLTSom;
end;

{ TXMLTSom }

function TXMLTSom.Get_Versao: UnicodeString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTSom.Set_Versao(Value: UnicodeString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTSom.Get_Codigo: UnicodeString;
begin
  Result := ChildNodes['Codigo'].Text;
end;

procedure TXMLTSom.Set_Codigo(Value: UnicodeString);
begin
  ChildNodes['Codigo'].NodeValue := Value;
end;

function TXMLTSom.Get_Coluna: UnicodeString;
begin
  Result := ChildNodes['Coluna'].Text;
end;

procedure TXMLTSom.Set_Coluna(Value: UnicodeString);
begin
  ChildNodes['Coluna'].NodeValue := Value;
end;

function TXMLTSom.Get_Linha: UnicodeString;
begin
  Result := ChildNodes['Linha'].Text;
end;

procedure TXMLTSom.Set_Linha(Value: UnicodeString);
begin
  ChildNodes['Linha'].NodeValue := Value;
end;

function TXMLTSom.Get_SeqInLin: UnicodeString;
begin
  Result := ChildNodes['SeqInLin'].Text;
end;

procedure TXMLTSom.Set_SeqInLin(Value: UnicodeString);
begin
  ChildNodes['SeqInLin'].NodeValue := Value;
end;

function TXMLTSom.Get_TempoIni: UnicodeString;
begin
  Result := ChildNodes['TempoIni'].Text;
end;

procedure TXMLTSom.Set_TempoIni(Value: UnicodeString);
begin
  ChildNodes['TempoIni'].NodeValue := Value;
end;

function TXMLTSom.Get_TempoTam: UnicodeString;
begin
  Result := ChildNodes['TempoTam'].Text;
end;

procedure TXMLTSom.Set_TempoTam(Value: UnicodeString);
begin
  ChildNodes['TempoTam'].NodeValue := Value;
end;

function TXMLTSom.Get_TextoSom: UnicodeString;
begin
  Result := ChildNodes['TextoSom'].Text;
end;

procedure TXMLTSom.Set_TextoSom(Value: UnicodeString);
begin
  ChildNodes['TextoSom'].NodeValue := Value;
end;

function TXMLTSom.Get_TextoImp: UnicodeString;
begin
  Result := ChildNodes['TextoImp'].Text;
end;

procedure TXMLTSom.Set_TextoImp(Value: UnicodeString);
begin
  ChildNodes['TextoImp'].NodeValue := Value;
end;

function TXMLTSom.Get_CorGray: UnicodeString;
begin
  Result := ChildNodes['CorGray'].Text;
end;

procedure TXMLTSom.Set_CorGray(Value: UnicodeString);
begin
  ChildNodes['CorGray'].NodeValue := Value;
end;

function TXMLTSom.Get_CorPrev: UnicodeString;
begin
  Result := ChildNodes['CorPrev'].Text;
end;

procedure TXMLTSom.Set_CorPrev(Value: UnicodeString);
begin
  ChildNodes['CorPrev'].NodeValue := Value;
end;

function TXMLTSom.Get_CorShow: UnicodeString;
begin
  Result := ChildNodes['CorShow'].Text;
end;

procedure TXMLTSom.Set_CorShow(Value: UnicodeString);
begin
  ChildNodes['CorShow'].NodeValue := Value;
end;

end.