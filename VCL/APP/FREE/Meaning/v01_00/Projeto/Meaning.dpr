program Meaning;

uses
  Vcl.Forms,
  Principal in '..\Units\Principal.pas' {FmPrincipal},
  xliffcore12strict in '..\Auxiliares\xliffcore12strict.pas',
  UnAppPF in '..\Units\UnAppPF.pas',
  Vcl.Themes,
  Vcl.Styles,
  About in '..\Units\About.pas' {FmAbout};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  TStyleManager.TrySetStyle('Amethyst Kamri');
  Application.CreateForm(TFmPrincipal, FmPrincipal);
  Application.Run;
end.
