object FmPrincipal: TFmPrincipal
  Left = 0
  Top = 0
  Caption = 'Meaning'
  ClientHeight = 587
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object TreeView1: TTreeView
    Left = 0
    Top = 0
    Width = 1008
    Height = 68
    Align = alTop
    Indent = 19
    TabOrder = 0
    Visible = False
    ExplicitTop = 109
  end
  object PnSGs: TPanel
    Left = 0
    Top = 68
    Width = 1008
    Height = 519
    Align = alClient
    TabOrder = 1
    ExplicitTop = 177
    ExplicitHeight = 316
    object Splitter1: TSplitter
      Left = 1
      Top = 1
      Width = 1006
      Height = 5
      Cursor = crVSplit
      Align = alTop
      Visible = False
    end
    object SG1: TStringGrid
      Left = 1
      Top = 6
      Width = 1006
      Height = 512
      Align = alClient
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goDrawFocusSelected, goColSizing]
      TabOrder = 0
      OnSelectCell = SG1SelectCell
      ExplicitTop = 80
      ExplicitHeight = 220
    end
  end
  object MainMenu1: TMainMenu
    Left = 12
    Top = 8
    object File1: TMenuItem
      Caption = '&File'
      OnClick = File1Click
      object Open1: TMenuItem
        Caption = '&Open'
        OnClick = Open1Click
      end
      object Save1: TMenuItem
        Caption = '&Save'
        OnClick = Save1Click
      end
      object SaveAs1: TMenuItem
        Caption = 'Save &As'
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object Exit1: TMenuItem
        Caption = '&Exit'
        OnClick = Exit1Click
      end
    end
    object Help1: TMenuItem
      Caption = '&Help'
      object About1: TMenuItem
        Caption = '&About...'
        OnClick = About1Click
      end
    end
  end
  object XMLDocument1: TXMLDocument
    Left = 288
    Top = 156
  end
end
