unit Principal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Menus, Vcl.StdCtrls,
  xliffcore12strict,  XMLDoc, xmldom, XMLIntf, msxmldom, Vcl.ComCtrls, TypInfo,
  Vcl.ExtCtrls, Vcl.Grids;

type
  TFmPrincipal = class(TForm)
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    Open1: TMenuItem;
    TreeView1: TTreeView;
    PnSGs: TPanel;
    SG1: TStringGrid;
    Save1: TMenuItem;
    SaveAs1: TMenuItem;
    N1: TMenuItem;
    Exit1: TMenuItem;
    XMLDocument1: TXMLDocument;
    Splitter1: TSplitter;
    Help1: TMenuItem;
    About1: TMenuItem;
    procedure Open1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SG1SelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure Exit1Click(Sender: TObject);
    procedure Save1Click(Sender: TObject);
    procedure File1Click(Sender: TObject);
    procedure About1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    TextoEN_US, FileEN_US: String;
    FLinhas: Integer;
    cXML: IXMLXliff;
    //arqXML: TXMLDocument;
    //
    //FLiff: array of array[0..2] of String;
    //
    function  OpenByChose(): Boolean;
    function  CarregaXML(): Boolean;

    procedure DomToTree(XmlNode: IXMLNode; TreeNode: TTreeNode);
    function  HabilitaSalvamento(): Boolean;
    function  ObtemVal(XmlNode: IXMLNode): String;
    function  PreparaDadosParaSalvar2(): Boolean;
    procedure Salvar();

  public
    { Public declarations }
  end;

var
  FmPrincipal: TFmPrincipal;
  CO_DB_APP_PATH: String;

implementation

uses
  UnAppPF, About;

{$R *.dfm}

const
  FCol_ID = 01;
  FCol_Source = 02;
  FCol_Target = 03;
  FCol_Note   = 04;

procedure TFmPrincipal.About1Click(Sender: TObject);
begin
  Application.CreateForm(TFmAbout, FmAbout);
  FmAbout.ShowModal;
  FmAbout.Destroy;
end;

function TFmPrincipal.CarregaXML(): Boolean;
var
  //cXML: IXMLTRetConsStatServ;
  I, J, L: Integer;

  //

var
  Files_, File_:IXmlNode;
  AttrNode: IXMLNode;

begin
  //SetLength(FLiff, 0);
  FLinhas := 0;
  XMLDocument1.LoadFromFile(FileEN_US);
  XMLDocument1.Active := True;
  cXML := Loadxliff(FileEN_US);
  ShowMessage('Lang = ' + cXML.Lang);
  //
  if cXML.Version <> '1.2' then
    ShowMessage('Unsupported version: ' + cXML.Version);
  //
  for j := 0 to cXML.ChildNodes.Count -1 do
  begin
    Files_ := cXML.ChildNodes[j];
    DomToTree (Files_,  nil);
  end;
end;

procedure TFmPrincipal.DomToTree (XmlNode: IXMLNode; TreeNode: TTreeNode);
var
  I: Integer;
  NewTreeNode: TTreeNode;
  NodeText: string;
  AttrNode: IXMLNode;
  //
  sNodeType, Valor: String;
begin
  // skip text nodes and other special cases

  if XmlNode.NodeType <> ntElement then
  begin
{
    sNodeType := GetEnumName(TypeInfo(TNodeType), Integer(XmlNode.NodeType));
    NodeText := Trim(XmlNode.NodeValue);
    NodeText := StringReplace(NodeText, #10, '', []);
    NodeText := StringReplace(NodeText, #13, '', []);
    if NodeText <> EmptyStr then
    begin
      NodeText := XmlNode.NodeName;
      //if XmlNode.IsTextElement then
        NodeText := '[' + sNodeType + '] ' + NodeText + ' = ' + XmlNode.NodeValue;
      NewTreeNode := TreeView1.Items.AddChild(TreeNode, NodeText);
    end;
}
  end else
  begin
    // add the node itself
    NodeText := XmlNode.NodeName;
    if XmlNode.NodeName = 'trans-unit' then
    begin
      //ShowMessage(XmlNode.NodeName);
      FLinhas := FLinhas + 1;
      SG1.RowCount := FLinhas + 1;
      //SetLength(FLiff, FLinhas);
      //FLiff[FLinhas-1][0] := FAtualFile_;
      //
    end else
    if XmlNode.NodeName = 'source' then
    begin
      Valor := ObtemVal(XmlNode);
      SG1.Cells[FCol_Source, FLinhas] := Valor;
      //
      //FLiff[FLinhas-1][2] := Valor;
    end else
    if XmlNode.NodeName = 'target' then
    begin
      SG1.Cells[FCol_Target, FLinhas] := ObtemVal(XmlNode);
    end else
    if XmlNode.NodeName = 'note' then
    begin
      Valor := ObtemVal(XmlNode);
      SG1.Cells[FCol_Note, FLinhas] := Valor;
      //
      //FLiff[FLinhas-1][3] := Valor;
    end;
    if XmlNode.IsTextElement then
      NodeText := NodeText + ' = ' + ObtemVal(XmlNode);
    NewTreeNode := TreeView1.Items.AddChild(TreeNode, NodeText);
    // add attributes
    for I := 0 to xmlNode.AttributeNodes.Count - 1 do
    begin
      AttrNode := xmlNode.AttributeNodes.Nodes[I];
      TreeView1.Items.AddChild(NewTreeNode,
        '[' + AttrNode.NodeName + ' = "' + AttrNode.Text + '"]');
      // ID do texto
      if AttrNode.NodeName = 'id' then
      begin
        Valor := AttrNode.Text;
        SG1.Cells[FCol_ID, FLinhas] := Valor;
        //FLiff[FLinhas-1][1] := Valor;
      end;
    end;
    // add each child node
    if XmlNode.HasChildNodes then
      for I := 0 to xmlNode.ChildNodes.Count - 1 do
        DomToTree (xmlNode.ChildNodes.Nodes [I], NewTreeNode);
  end;
end;

procedure TFmPrincipal.Exit1Click(Sender: TObject);
begin
  Close;
end;

procedure TFmPrincipal.File1Click(Sender: TObject);
begin
  HabilitaSalvamento();
end;

procedure TFmPrincipal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if HabilitaSalvamento() then
    if MessageDlg('Save before close?', mtConfirmation, [mbYes, mbNo], 0, mbYes) = mrYes then
      Salvar();
end;

procedure TFmPrincipal.FormCreate(Sender: TObject);
begin
  CO_DB_APP_PATH := ExtractFilePath(Application.ExeName);
  SG1.RowCount := 2;
  SG1.ColWidths[00]          := 18; // Seq?
  SG1.ColWidths[FCol_ID]     := 120; // trans-unit id
  SG1.ColWidths[FCol_Source] := 400; // source
  SG1.ColWidths[FCol_Target] := 400; // target
  SG1.ColWidths[FCol_Note]   := 200; // note
  //
  SG1.Cells[FCol_ID, 00]     := 'ID';
  SG1.Cells[FCol_Source, 00] := 'Source';
  SG1.Cells[FCol_Target, 00] := 'Target';
  SG1.Cells[FCol_Note, 00]   := 'Note';
  //
end;

function TFmPrincipal.HabilitaSalvamento(): Boolean;
begin
  Result := XMLDocument1.Active = True;
  //
  Save1.Enabled := Result;
  SaveAs1.Enabled := Result;
end;

function TFmPrincipal.ObtemVal(XmlNode: IXMLNode): String;
begin
  if Assigned(XmlNode) then // <> nil then
  begin
    //if not Assigned(XmlNode.NodeValue) then //= Null then
    if XmlNode.NodeValue = Null then
      Result := ''
    else
      Result := String(XmlNode.NodeValue);
  end else
    Result := '';
end;

procedure TFmPrincipal.Open1Click(Sender: TObject);
var
  I: Integer;
begin
  if OpenByChose() then
  begin
    CarregaXML();
  end;
end;

function TFmPrincipal.OpenByChose(): Boolean;
const
  Titulo = 'File with text to translate';
var
  IniDir, Arquivo, Filtro: String;

begin
  TextoEN_US := '';
  IniDir  := CO_DB_APP_PATH;
  Arquivo := '';
  Filtro  := 'xliff File|*.xliff;All files|*.*';
  if AppPF.MyObjects_FileOpenDialog(Self, IniDir, Arquivo, Titulo, Filtro, [], Arquivo) then
  begin
    if FileExists(Arquivo) then
    begin
      FileEN_US := Arquivo;
      //AppPF.dmkPF_CarregaArquivo(Arquivo, TextoEN_US);
      //
      //Memo1.Lines.LoadFromFile(FileEN_US);
      //
      Result := True;
    end;
  end;
end;

function TFmPrincipal.PreparaDadosParaSalvar2(): Boolean;
var
  Linhas, I, J, K, L: Integer;
begin
  Result := False;
  Linhas := 0;
  //
  for J := 0 to XMLDocument1.DocumentElement.ChildNodes.Count -1 do
  begin
    begin
    for I := 0 to XMLDocument1.DocumentElement.ChildNodes[J].ChildNodes.Count -1 do
      begin
        for K := 0 to XMLDocument1.DocumentElement.ChildNodes[J].ChildNodes[I].ChildNodes.Count -1 do
        begin
          for L := 0 to XMLDocument1.DocumentElement.ChildNodes[J].ChildNodes[I].ChildNodes[K].ChildNodes.Count -1 do
          begin
            if XMLDocument1.DocumentElement.ChildNodes[J].ChildNodes[I].ChildNodes[K].ChildNodes[L].NodeName = 'target' then
            begin
              Linhas := Linhas + 1;
              XMLDocument1.DocumentElement.ChildNodes[J].ChildNodes[I].ChildNodes[K].ChildNodes[L].NodeValue :=
                SG1.Cells[FCol_Target, Linhas];
            end;
          end;
        end;
      end;
    end;
  end;
  Result := True;
end;

procedure TFmPrincipal.Salvar();
begin
  if PreparaDadosParaSalvar2() then
    XMLDocument1.SaveToFile(XMLDocument1.FileName);
end;

procedure TFmPrincipal.Save1Click(Sender: TObject);
begin
  Salvar();
end;

procedure TFmPrincipal.SG1SelectCell(Sender: TObject; ACol, ARow: Integer;
  var CanSelect: Boolean);
begin
  if ACol = FCol_Target then
    SG1.Options := SG1.Options + [goEditing]
  else
    SG1.Options := SG1.Options - [goEditing];
end;

{
procedure TForm2.DomToTree (XmlNode: IXMLNode; TreeNode: TTreeNode);
var
  I: Integer;
  NewTreeNode: TTreeNode;
  NodeText: string;
  AttrNode: IXMLNode;
begin
  // skip text nodes and other special cases
  if XmlNode.NodeType <> ntElement then
    Exit;
  // add the node itself
  NodeText := XmlNode.NodeName;
  if XmlNode.IsTextElement then
    NodeText := NodeText + ' = ' + XmlNode.NodeValue;
  NewTreeNode := TreeView1.Items.AddChild(TreeNode, NodeText);
  // add attributes
  for I := 0 to xmlNode.AttributeNodes.Count - 1 do
  begin
    AttrNode := xmlNode.AttributeNodes.Nodes[I];
    TreeView1.Items.AddChild(NewTreeNode,
      '[' + AttrNode.NodeName + ' = "' + AttrNode.Text + '"]');
  end;
  // add each child node
  if XmlNode.HasChildNodes then
    for I := 0 to xmlNode.ChildNodes.Count - 1 do
      DomToTree (xmlNode.ChildNodes.Nodes [I], NewTreeNode);
end;
}

end.
