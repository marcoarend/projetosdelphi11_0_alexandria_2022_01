unit UnAppPF;

interface

uses
  Vcl.Forms, Vcl.Dialogs,
  System.Classes, System.SysUtils;

type
  TUnAppPF = class(TObject)
  private
    { Private declarations }
  public
    { Private declarations }
    function  dmkPF_CarregaArquivo(const Arquivo: String; var Texto: String): Boolean;
    function  Geral_SeparaPrimeiraOcorrenciaDeTexto(const Separador, Texto: String;
              var PrimeiraOcorrencia, NovoTexto: String): Boolean;
    function  MyObjects_FileOpenDialog(Form: TForm; const IniDir, Arquivo, Titulo, Filtro:
              String; Opcoes: TOpenOptions; var Resultado: String): Boolean;
end;

var
  AppPF: TUnAppPF;


implementation

{ TUnAppPF }

function TUnAppPF.dmkPF_CarregaArquivo(const Arquivo: String;
  var Texto: String): Boolean;
var
  Lista: TStringList;
begin
  Result := False;
  Lista := TStringList.Create;
  try
    try
      if FileExists(Arquivo) then
        Lista.LoadFromFile(Arquivo);
      Texto := Lista.Text;
      Result := True;
    except
      on e: exception do
        //Geral.MB_Erro(E.Message);
        ShowMessage(E.Message);
    end;
  finally
    // libera a lista
    Lista.Free;
  end;
end;

function TUnAppPF.Geral_SeparaPrimeiraOcorrenciaDeTexto(const Separador,
  Texto: String; var PrimeiraOcorrencia, NovoTexto: String): Boolean;
var
  P: Integer;
begin
  Result := False;
  if Texto <> '' then
  begin
    P := pos(Separador, Texto);
    if P > 0 then
    begin
      PrimeiraOcorrencia := Copy(Texto, 1, P - 1);
      NovoTexto := Copy(Texto, P + 1);
    end else begin
      PrimeiraOcorrencia := Texto;
      NovoTexto := '';
    end;
    Result := True;
  end;
end;

function TUnAppPF.MyObjects_FileOpenDialog(Form: TForm; const IniDir, Arquivo, Titulo,
  Filtro: String; Opcoes: TOpenOptions; var Resultado: String): Boolean;
const
  TxtPadraoDir = 'Sele��o de Pastas.';
var
  //VerWin: MyArrayI03;
  //OpenDialog: TOpenDialog;
{$WARN UNIT_PLATFORM OFF}
{$WARN SYMBOL_PLATFORM OFF}
  FileOpenDialog: TFileOpenDialog;
{$WARN UNIT_PLATFORM ON}
{$WARN SYMBOL_PLATFORM ON}
  Arq, NovoFiltro, Item, Tipo, Extensao: String;
begin
  Arq := Arquivo;
  Result := false;
  Resultado := '';
  FileOpenDialog := TFileOpenDialog.Create(Form);
  try
    try
      FileOpenDialog.DefaultFolder := IniDir;
      FileOpenDialog.Title      := Titulo;//'Abrir Arquivo de Concilia��o Banc�ria';
      NovoFiltro := Filtro; // Ex.: Filtro := 'Arquivos OFC|*.ofc;Arquivos OFX|*.ofx';
      while NovoFiltro <> '' do
      begin
        if Geral_SeparaPrimeiraOcorrenciaDeTexto(';', NovoFiltro, Item, NovoFiltro) then
        begin
          if Geral_SeparaPrimeiraOcorrenciaDeTexto('|', Item, Tipo, Extensao) then
          begin
            with FileOpenDialog.FileTypes.Add do
            begin
              DisplayName := Tipo;
              FileMask := Extensao;
            end;
          end;
        end;
      end;
      FileOpenDialog.FileName   := Arq;
      //
      if Length(FileOpenDialog.FileName) = 0 then
        FileOpenDialog.FileName := TxtPadraoDir;
      //
      //FileOpenDialog.Options    := FileOpenDialog.Options + Opcoes;
      //
    except
      // nada
    end;
    if FileOpenDialog.Execute then
    begin
      Resultado := FileOpenDialog.FileName;
      Result := True;
    end;
  finally
    FileOpenDialog.free;
  end;
end;

end.
