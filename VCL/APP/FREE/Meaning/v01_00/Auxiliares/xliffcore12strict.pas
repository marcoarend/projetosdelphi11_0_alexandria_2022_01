
{*******************************************************************************************************************************************}
{                                                                                                                                           }
{                                                             XML Data Binding                                                              }
{                                                                                                                                           }
{         Generated on: 29/12/2021 08:19:01                                                                                                 }
{       Generated from: C:\_Compilers\projetosdelphi10_2_2_tokyo_2020_06\VCL\APP\FREE\Meaning\v01_00\Auxiliares\xliff-core-1.2-strict.xsd   }
{   Settings stored in: C:\_Compilers\projetosdelphi10_2_2_tokyo_2020_06\VCL\APP\FREE\Meaning\v01_00\Auxiliares\xliff-core-1.2-strict.xdb   }
{                                                                                                                                           }
{*******************************************************************************************************************************************}

unit xliffcore12strict;

interface

uses Xml.xmldom, Xml.XMLDoc, Xml.XMLIntf;

type

{ Forward Decls }

  IXMLXliff = interface;
  IXMLFile_ = interface;
  IXMLHeader = interface;
  IXMLElemType_ExternalReference = interface;
  IXMLElemType_ExternalReferenceList = interface;
  IXMLInternalfile = interface;
  IXMLExternalfile = interface;
  IXMLPhasegroup = interface;
  IXMLPhase = interface;
  IXMLNote = interface;
  IXMLNoteList = interface;
  IXMLCountgroup = interface;
  IXMLCountgroupList = interface;
  IXMLCount = interface;
  IXMLTool = interface;
  IXMLToolList = interface;
  IXMLBody = interface;
  IXMLGroup = interface;
  IXMLGroupList = interface;
  IXMLContextgroup = interface;
  IXMLContextgroupList = interface;
  IXMLContext = interface;
  IXMLTransunit = interface;
  IXMLTransunitList = interface;
  IXMLSource = interface;
  IXMLG = interface;
  IXMLBpt = interface;
  IXMLSub = interface;
  IXMLEpt = interface;
  IXMLPh = interface;
  IXMLIt = interface;
  IXMLMrk = interface;
  IXMLX = interface;
  IXMLBx = interface;
  IXMLEx = interface;
  IXMLSegsource = interface;
  IXMLTarget = interface;
  IXMLAlttrans = interface;
  IXMLAlttransList = interface;
  IXMLBinunit = interface;
  IXMLBinunitList = interface;
  IXMLBinsource = interface;
  IXMLBintarget = interface;

{ IXMLXliff }

  IXMLXliff = interface(IXMLNodeCollection)
    ['{F4F3533A-6A66-4231-8940-310674921710}']
    { Property Accessors }
    function Get_Version: UnicodeString;
    function Get_Lang: UnicodeString;
    function Get_File_(Index: Integer): IXMLFile_;
    procedure Set_Version(Value: UnicodeString);
    procedure Set_Lang(Value: UnicodeString);
    { Methods & Properties }
    function Add: IXMLFile_;
    function Insert(const Index: Integer): IXMLFile_;
    property Version: UnicodeString read Get_Version write Set_Version;
    property Lang: UnicodeString read Get_Lang write Set_Lang;
    property File_[Index: Integer]: IXMLFile_ read Get_File_; default;
  end;

{ IXMLFile_ }

  IXMLFile_ = interface(IXMLNode)
    ['{E291072D-93AF-4F08-B22C-648C13FC15FC}']
    { Property Accessors }
    function Get_Original: UnicodeString;
    function Get_Sourcelanguage: UnicodeString;
    function Get_Datatype: UnicodeString;
    function Get_Toolid: UnicodeString;
    function Get_Date: UnicodeString;
    function Get_Space: UnicodeString;
    function Get_Category: UnicodeString;
    function Get_Targetlanguage: UnicodeString;
    function Get_Productname: UnicodeString;
    function Get_Productversion: UnicodeString;
    function Get_Buildnum: UnicodeString;
    function Get_Header: IXMLHeader;
    function Get_Body: IXMLBody;
    procedure Set_Original(Value: UnicodeString);
    procedure Set_Sourcelanguage(Value: UnicodeString);
    procedure Set_Datatype(Value: UnicodeString);
    procedure Set_Toolid(Value: UnicodeString);
    procedure Set_Date(Value: UnicodeString);
    procedure Set_Space(Value: UnicodeString);
    procedure Set_Category(Value: UnicodeString);
    procedure Set_Targetlanguage(Value: UnicodeString);
    procedure Set_Productname(Value: UnicodeString);
    procedure Set_Productversion(Value: UnicodeString);
    procedure Set_Buildnum(Value: UnicodeString);
    { Methods & Properties }
    property Original: UnicodeString read Get_Original write Set_Original;
    property Sourcelanguage: UnicodeString read Get_Sourcelanguage write Set_Sourcelanguage;
    property Datatype: UnicodeString read Get_Datatype write Set_Datatype;
    property Toolid: UnicodeString read Get_Toolid write Set_Toolid;
    property Date: UnicodeString read Get_Date write Set_Date;
    property Space: UnicodeString read Get_Space write Set_Space;
    property Category: UnicodeString read Get_Category write Set_Category;
    property Targetlanguage: UnicodeString read Get_Targetlanguage write Set_Targetlanguage;
    property Productname: UnicodeString read Get_Productname write Set_Productname;
    property Productversion: UnicodeString read Get_Productversion write Set_Productversion;
    property Buildnum: UnicodeString read Get_Buildnum write Set_Buildnum;
    property Header: IXMLHeader read Get_Header;
    property Body: IXMLBody read Get_Body;
  end;

{ IXMLHeader }

  IXMLHeader = interface(IXMLNode)
    ['{FE4CFDF5-F2CE-4833-967C-2C74A21B7D13}']
    { Property Accessors }
    function Get_Skl: IXMLElemType_ExternalReference;
    function Get_Phasegroup: IXMLPhasegroup;
    function Get_Glossary: IXMLElemType_ExternalReferenceList;
    function Get_Reference: IXMLElemType_ExternalReferenceList;
    function Get_Countgroup: IXMLCountgroupList;
    function Get_Note: IXMLNoteList;
    function Get_Tool: IXMLToolList;
    { Methods & Properties }
    property Skl: IXMLElemType_ExternalReference read Get_Skl;
    property Phasegroup: IXMLPhasegroup read Get_Phasegroup;
    property Glossary: IXMLElemType_ExternalReferenceList read Get_Glossary;
    property Reference: IXMLElemType_ExternalReferenceList read Get_Reference;
    property Countgroup: IXMLCountgroupList read Get_Countgroup;
    property Note: IXMLNoteList read Get_Note;
    property Tool: IXMLToolList read Get_Tool;
  end;

{ IXMLElemType_ExternalReference }

  IXMLElemType_ExternalReference = interface(IXMLNode)
    ['{CDB66226-98FD-4939-8A71-62BCA94C081B}']
    { Property Accessors }
    function Get_Internalfile: IXMLInternalfile;
    function Get_Externalfile: IXMLExternalfile;
    { Methods & Properties }
    property Internalfile: IXMLInternalfile read Get_Internalfile;
    property Externalfile: IXMLExternalfile read Get_Externalfile;
  end;

{ IXMLElemType_ExternalReferenceList }

  IXMLElemType_ExternalReferenceList = interface(IXMLNodeCollection)
    ['{E50DA255-19DD-4611-BC86-783260ED5ADF}']
    { Methods & Properties }
    function Add: IXMLElemType_ExternalReference;
    function Insert(const Index: Integer): IXMLElemType_ExternalReference;

    function Get_Item(Index: Integer): IXMLElemType_ExternalReference;
    property Items[Index: Integer]: IXMLElemType_ExternalReference read Get_Item; default;
  end;

{ IXMLInternalfile }

  IXMLInternalfile = interface(IXMLNode)
    ['{250E77EA-24FB-4D06-83C3-2C3B48EBAB9F}']
    { Property Accessors }
    function Get_Form: UnicodeString;
    function Get_Crc: UnicodeString;
    procedure Set_Form(Value: UnicodeString);
    procedure Set_Crc(Value: UnicodeString);
    { Methods & Properties }
    property Form: UnicodeString read Get_Form write Set_Form;
    property Crc: UnicodeString read Get_Crc write Set_Crc;
  end;

{ IXMLExternalfile }

  IXMLExternalfile = interface(IXMLNode)
    ['{D2DEA8FD-2A99-4FD5-B8FE-6478DFA37D42}']
    { Property Accessors }
    function Get_Href: UnicodeString;
    function Get_Crc: UnicodeString;
    function Get_Uid: UnicodeString;
    procedure Set_Href(Value: UnicodeString);
    procedure Set_Crc(Value: UnicodeString);
    procedure Set_Uid(Value: UnicodeString);
    { Methods & Properties }
    property Href: UnicodeString read Get_Href write Set_Href;
    property Crc: UnicodeString read Get_Crc write Set_Crc;
    property Uid: UnicodeString read Get_Uid write Set_Uid;
  end;

{ IXMLPhasegroup }

  IXMLPhasegroup = interface(IXMLNodeCollection)
    ['{4F385E9E-6C77-449F-8DBC-F9AABD19B4DC}']
    { Property Accessors }
    function Get_Phase(Index: Integer): IXMLPhase;
    { Methods & Properties }
    function Add: IXMLPhase;
    function Insert(const Index: Integer): IXMLPhase;
    property Phase[Index: Integer]: IXMLPhase read Get_Phase; default;
  end;

{ IXMLPhase }

  IXMLPhase = interface(IXMLNodeCollection)
    ['{F4B929CD-8CF0-4B98-8F92-9277953B8569}']
    { Property Accessors }
    function Get_Phasename: UnicodeString;
    function Get_Processname: UnicodeString;
    function Get_Companyname: UnicodeString;
    function Get_Toolid: UnicodeString;
    function Get_Date: UnicodeString;
    function Get_Jobid: UnicodeString;
    function Get_Contactname: UnicodeString;
    function Get_Contactemail: UnicodeString;
    function Get_Contactphone: UnicodeString;
    function Get_Note(Index: Integer): IXMLNote;
    procedure Set_Phasename(Value: UnicodeString);
    procedure Set_Processname(Value: UnicodeString);
    procedure Set_Companyname(Value: UnicodeString);
    procedure Set_Toolid(Value: UnicodeString);
    procedure Set_Date(Value: UnicodeString);
    procedure Set_Jobid(Value: UnicodeString);
    procedure Set_Contactname(Value: UnicodeString);
    procedure Set_Contactemail(Value: UnicodeString);
    procedure Set_Contactphone(Value: UnicodeString);
    { Methods & Properties }
    function Add: IXMLNote;
    function Insert(const Index: Integer): IXMLNote;
    property Phasename: UnicodeString read Get_Phasename write Set_Phasename;
    property Processname: UnicodeString read Get_Processname write Set_Processname;
    property Companyname: UnicodeString read Get_Companyname write Set_Companyname;
    property Toolid: UnicodeString read Get_Toolid write Set_Toolid;
    property Date: UnicodeString read Get_Date write Set_Date;
    property Jobid: UnicodeString read Get_Jobid write Set_Jobid;
    property Contactname: UnicodeString read Get_Contactname write Set_Contactname;
    property Contactemail: UnicodeString read Get_Contactemail write Set_Contactemail;
    property Contactphone: UnicodeString read Get_Contactphone write Set_Contactphone;
    property Note[Index: Integer]: IXMLNote read Get_Note; default;
  end;

{ IXMLNote }

  IXMLNote = interface(IXMLNode)
    ['{C0FF0DBC-150E-4763-A3EF-C3D37B069FEE}']
    { Property Accessors }
    function Get_Lang: UnicodeString;
    function Get_Priority: UnicodeString;
    function Get_From: UnicodeString;
    function Get_Annotates: UnicodeString;
    procedure Set_Lang(Value: UnicodeString);
    procedure Set_Priority(Value: UnicodeString);
    procedure Set_From(Value: UnicodeString);
    procedure Set_Annotates(Value: UnicodeString);
    { Methods & Properties }
    property Lang: UnicodeString read Get_Lang write Set_Lang;
    property Priority: UnicodeString read Get_Priority write Set_Priority;
    property From: UnicodeString read Get_From write Set_From;
    property Annotates: UnicodeString read Get_Annotates write Set_Annotates;
  end;

{ IXMLNoteList }

  IXMLNoteList = interface(IXMLNodeCollection)
    ['{584C25B0-4E10-48DA-B02B-E7589B2CE644}']
    { Methods & Properties }
    function Add: IXMLNote;
    function Insert(const Index: Integer): IXMLNote;

    function Get_Item(Index: Integer): IXMLNote;
    property Items[Index: Integer]: IXMLNote read Get_Item; default;
  end;

{ IXMLCountgroup }

  IXMLCountgroup = interface(IXMLNodeCollection)
    ['{F0060C9A-09D7-4FA2-9A54-02EDC56DE8C6}']
    { Property Accessors }
    function Get_Name: UnicodeString;
    function Get_Count(Index: Integer): IXMLCount;
    procedure Set_Name(Value: UnicodeString);
    { Methods & Properties }
    function Add: IXMLCount;
    function Insert(const Index: Integer): IXMLCount;
    property Name: UnicodeString read Get_Name write Set_Name;
    property Count[Index: Integer]: IXMLCount read Get_Count; default;
  end;

{ IXMLCountgroupList }

  IXMLCountgroupList = interface(IXMLNodeCollection)
    ['{C14B70EE-B968-477B-BFBA-9C9455F7DB46}']
    { Methods & Properties }
    function Add: IXMLCountgroup;
    function Insert(const Index: Integer): IXMLCountgroup;

    function Get_Item(Index: Integer): IXMLCountgroup;
    property Items[Index: Integer]: IXMLCountgroup read Get_Item; default;
  end;

{ IXMLCount }

  IXMLCount = interface(IXMLNode)
    ['{60214783-ADCC-447A-B7A5-94AD10C5BE0E}']
    { Property Accessors }
    function Get_Counttype: UnicodeString;
    function Get_Phasename: UnicodeString;
    function Get_Unit_: UnicodeString;
    procedure Set_Counttype(Value: UnicodeString);
    procedure Set_Phasename(Value: UnicodeString);
    procedure Set_Unit_(Value: UnicodeString);
    { Methods & Properties }
    property Counttype: UnicodeString read Get_Counttype write Set_Counttype;
    property Phasename: UnicodeString read Get_Phasename write Set_Phasename;
    property Unit_: UnicodeString read Get_Unit_ write Set_Unit_;
  end;

{ IXMLTool }

  IXMLTool = interface(IXMLNode)
    ['{B34348D9-9BDE-4ACB-94DF-558ECC75FEB5}']
    { Property Accessors }
    function Get_Toolid: UnicodeString;
    function Get_Toolname: UnicodeString;
    function Get_Toolversion: UnicodeString;
    function Get_Toolcompany: UnicodeString;
    procedure Set_Toolid(Value: UnicodeString);
    procedure Set_Toolname(Value: UnicodeString);
    procedure Set_Toolversion(Value: UnicodeString);
    procedure Set_Toolcompany(Value: UnicodeString);
    { Methods & Properties }
    property Toolid: UnicodeString read Get_Toolid write Set_Toolid;
    property Toolname: UnicodeString read Get_Toolname write Set_Toolname;
    property Toolversion: UnicodeString read Get_Toolversion write Set_Toolversion;
    property Toolcompany: UnicodeString read Get_Toolcompany write Set_Toolcompany;
  end;

{ IXMLToolList }

  IXMLToolList = interface(IXMLNodeCollection)
    ['{06E3B26B-6268-4682-BF4F-EE52D9E3F801}']
    { Methods & Properties }
    function Add: IXMLTool;
    function Insert(const Index: Integer): IXMLTool;

    function Get_Item(Index: Integer): IXMLTool;
    property Items[Index: Integer]: IXMLTool read Get_Item; default;
  end;

{ IXMLBody }

  IXMLBody = interface(IXMLNode)
    ['{4A129D16-DAAE-48DD-B070-1561D55EB6B5}']
    { Property Accessors }
    function Get_Group: IXMLGroupList;
    function Get_Transunit: IXMLTransunitList;
    function Get_Binunit: IXMLBinunitList;
    { Methods & Properties }
    property Group: IXMLGroupList read Get_Group;
    property Transunit: IXMLTransunitList read Get_Transunit;
    property Binunit: IXMLBinunitList read Get_Binunit;
  end;

{ IXMLGroup }

  IXMLGroup = interface(IXMLNode)
    ['{4FBCB784-FCB5-44DC-BC9D-411FA2C4C9D4}']
    { Property Accessors }
    function Get_Id: UnicodeString;
    function Get_Datatype: UnicodeString;
    function Get_Space: UnicodeString;
    function Get_Restype: UnicodeString;
    function Get_Resname: UnicodeString;
    function Get_Extradata: UnicodeString;
    function Get_Extype: UnicodeString;
    function Get_Helpid: UnicodeString;
    function Get_Menu: UnicodeString;
    function Get_Menuoption: UnicodeString;
    function Get_Menuname: UnicodeString;
    function Get_Coord: UnicodeString;
    function Get_Font: UnicodeString;
    function Get_Cssstyle: UnicodeString;
    function Get_Style: UnicodeString;
    function Get_Exstyle: UnicodeString;
    function Get_Translate: UnicodeString;
    function Get_Reformat: UnicodeString;
    function Get_Sizeunit: UnicodeString;
    function Get_Maxwidth: UnicodeString;
    function Get_Minwidth: UnicodeString;
    function Get_Maxheight: UnicodeString;
    function Get_Minheight: UnicodeString;
    function Get_Maxbytes: UnicodeString;
    function Get_Minbytes: UnicodeString;
    function Get_Charclass: UnicodeString;
    function Get_Mergedtrans: UnicodeString;
    function Get_Contextgroup: IXMLContextgroupList;
    function Get_Countgroup: IXMLCountgroupList;
    function Get_Note: IXMLNoteList;
    function Get_Group: IXMLGroupList;
    function Get_Transunit: IXMLTransunitList;
    function Get_Binunit: IXMLBinunitList;
    procedure Set_Id(Value: UnicodeString);
    procedure Set_Datatype(Value: UnicodeString);
    procedure Set_Space(Value: UnicodeString);
    procedure Set_Restype(Value: UnicodeString);
    procedure Set_Resname(Value: UnicodeString);
    procedure Set_Extradata(Value: UnicodeString);
    procedure Set_Extype(Value: UnicodeString);
    procedure Set_Helpid(Value: UnicodeString);
    procedure Set_Menu(Value: UnicodeString);
    procedure Set_Menuoption(Value: UnicodeString);
    procedure Set_Menuname(Value: UnicodeString);
    procedure Set_Coord(Value: UnicodeString);
    procedure Set_Font(Value: UnicodeString);
    procedure Set_Cssstyle(Value: UnicodeString);
    procedure Set_Style(Value: UnicodeString);
    procedure Set_Exstyle(Value: UnicodeString);
    procedure Set_Translate(Value: UnicodeString);
    procedure Set_Reformat(Value: UnicodeString);
    procedure Set_Sizeunit(Value: UnicodeString);
    procedure Set_Maxwidth(Value: UnicodeString);
    procedure Set_Minwidth(Value: UnicodeString);
    procedure Set_Maxheight(Value: UnicodeString);
    procedure Set_Minheight(Value: UnicodeString);
    procedure Set_Maxbytes(Value: UnicodeString);
    procedure Set_Minbytes(Value: UnicodeString);
    procedure Set_Charclass(Value: UnicodeString);
    procedure Set_Mergedtrans(Value: UnicodeString);
    { Methods & Properties }
    property Id: UnicodeString read Get_Id write Set_Id;
    property Datatype: UnicodeString read Get_Datatype write Set_Datatype;
    property Space: UnicodeString read Get_Space write Set_Space;
    property Restype: UnicodeString read Get_Restype write Set_Restype;
    property Resname: UnicodeString read Get_Resname write Set_Resname;
    property Extradata: UnicodeString read Get_Extradata write Set_Extradata;
    property Extype: UnicodeString read Get_Extype write Set_Extype;
    property Helpid: UnicodeString read Get_Helpid write Set_Helpid;
    property Menu: UnicodeString read Get_Menu write Set_Menu;
    property Menuoption: UnicodeString read Get_Menuoption write Set_Menuoption;
    property Menuname: UnicodeString read Get_Menuname write Set_Menuname;
    property Coord: UnicodeString read Get_Coord write Set_Coord;
    property Font: UnicodeString read Get_Font write Set_Font;
    property Cssstyle: UnicodeString read Get_Cssstyle write Set_Cssstyle;
    property Style: UnicodeString read Get_Style write Set_Style;
    property Exstyle: UnicodeString read Get_Exstyle write Set_Exstyle;
    property Translate: UnicodeString read Get_Translate write Set_Translate;
    property Reformat: UnicodeString read Get_Reformat write Set_Reformat;
    property Sizeunit: UnicodeString read Get_Sizeunit write Set_Sizeunit;
    property Maxwidth: UnicodeString read Get_Maxwidth write Set_Maxwidth;
    property Minwidth: UnicodeString read Get_Minwidth write Set_Minwidth;
    property Maxheight: UnicodeString read Get_Maxheight write Set_Maxheight;
    property Minheight: UnicodeString read Get_Minheight write Set_Minheight;
    property Maxbytes: UnicodeString read Get_Maxbytes write Set_Maxbytes;
    property Minbytes: UnicodeString read Get_Minbytes write Set_Minbytes;
    property Charclass: UnicodeString read Get_Charclass write Set_Charclass;
    property Mergedtrans: UnicodeString read Get_Mergedtrans write Set_Mergedtrans;
    property Contextgroup: IXMLContextgroupList read Get_Contextgroup;
    property Countgroup: IXMLCountgroupList read Get_Countgroup;
    property Note: IXMLNoteList read Get_Note;
    property Group: IXMLGroupList read Get_Group;
    property Transunit: IXMLTransunitList read Get_Transunit;
    property Binunit: IXMLBinunitList read Get_Binunit;
  end;

{ IXMLGroupList }

  IXMLGroupList = interface(IXMLNodeCollection)
    ['{57FF3B90-B500-4731-904B-535B858A5D22}']
    { Methods & Properties }
    function Add: IXMLGroup;
    function Insert(const Index: Integer): IXMLGroup;

    function Get_Item(Index: Integer): IXMLGroup;
    property Items[Index: Integer]: IXMLGroup read Get_Item; default;
  end;

{ IXMLContextgroup }

  IXMLContextgroup = interface(IXMLNodeCollection)
    ['{F697C17F-120B-4594-AE08-400FB3301956}']
    { Property Accessors }
    function Get_Name: UnicodeString;
    function Get_Crc: UnicodeString;
    function Get_Purpose: UnicodeString;
    function Get_Context(Index: Integer): IXMLContext;
    procedure Set_Name(Value: UnicodeString);
    procedure Set_Crc(Value: UnicodeString);
    procedure Set_Purpose(Value: UnicodeString);
    { Methods & Properties }
    function Add: IXMLContext;
    function Insert(const Index: Integer): IXMLContext;
    property Name: UnicodeString read Get_Name write Set_Name;
    property Crc: UnicodeString read Get_Crc write Set_Crc;
    property Purpose: UnicodeString read Get_Purpose write Set_Purpose;
    property Context[Index: Integer]: IXMLContext read Get_Context; default;
  end;

{ IXMLContextgroupList }

  IXMLContextgroupList = interface(IXMLNodeCollection)
    ['{30A03497-CA44-47F6-B6E9-4694D1A4FD51}']
    { Methods & Properties }
    function Add: IXMLContextgroup;
    function Insert(const Index: Integer): IXMLContextgroup;

    function Get_Item(Index: Integer): IXMLContextgroup;
    property Items[Index: Integer]: IXMLContextgroup read Get_Item; default;
  end;

{ IXMLContext }

  IXMLContext = interface(IXMLNode)
    ['{F5C0A4D8-1578-499B-8188-25ED737A0EE2}']
    { Property Accessors }
    function Get_Contexttype: UnicodeString;
    function Get_Matchmandatory: UnicodeString;
    function Get_Crc: UnicodeString;
    procedure Set_Contexttype(Value: UnicodeString);
    procedure Set_Matchmandatory(Value: UnicodeString);
    procedure Set_Crc(Value: UnicodeString);
    { Methods & Properties }
    property Contexttype: UnicodeString read Get_Contexttype write Set_Contexttype;
    property Matchmandatory: UnicodeString read Get_Matchmandatory write Set_Matchmandatory;
    property Crc: UnicodeString read Get_Crc write Set_Crc;
  end;

{ IXMLTransunit }

  IXMLTransunit = interface(IXMLNode)
    ['{ADE2F92C-120B-466C-918C-2675BB2E2BA9}']
    { Property Accessors }
    function Get_Id: UnicodeString;
    function Get_Approved: UnicodeString;
    function Get_Translate: UnicodeString;
    function Get_Reformat: UnicodeString;
    function Get_Space: UnicodeString;
    function Get_Datatype: UnicodeString;
    function Get_Phasename: UnicodeString;
    function Get_Restype: UnicodeString;
    function Get_Resname: UnicodeString;
    function Get_Extradata: UnicodeString;
    function Get_Extype: UnicodeString;
    function Get_Helpid: UnicodeString;
    function Get_Menu: UnicodeString;
    function Get_Menuoption: UnicodeString;
    function Get_Menuname: UnicodeString;
    function Get_Coord: UnicodeString;
    function Get_Font: UnicodeString;
    function Get_Cssstyle: UnicodeString;
    function Get_Style: UnicodeString;
    function Get_Exstyle: UnicodeString;
    function Get_Sizeunit: UnicodeString;
    function Get_Maxwidth: UnicodeString;
    function Get_Minwidth: UnicodeString;
    function Get_Maxheight: UnicodeString;
    function Get_Minheight: UnicodeString;
    function Get_Maxbytes: UnicodeString;
    function Get_Minbytes: UnicodeString;
    function Get_Charclass: UnicodeString;
    function Get_Mergedtrans: UnicodeString;
    function Get_Source: IXMLSource;
    function Get_Segsource: IXMLSegsource;
    function Get_Target: IXMLTarget;
    function Get_Contextgroup: IXMLContextgroupList;
    function Get_Countgroup: IXMLCountgroupList;
    function Get_Note: IXMLNoteList;
    function Get_Alttrans: IXMLAlttransList;
    procedure Set_Id(Value: UnicodeString);
    procedure Set_Approved(Value: UnicodeString);
    procedure Set_Translate(Value: UnicodeString);
    procedure Set_Reformat(Value: UnicodeString);
    procedure Set_Space(Value: UnicodeString);
    procedure Set_Datatype(Value: UnicodeString);
    procedure Set_Phasename(Value: UnicodeString);
    procedure Set_Restype(Value: UnicodeString);
    procedure Set_Resname(Value: UnicodeString);
    procedure Set_Extradata(Value: UnicodeString);
    procedure Set_Extype(Value: UnicodeString);
    procedure Set_Helpid(Value: UnicodeString);
    procedure Set_Menu(Value: UnicodeString);
    procedure Set_Menuoption(Value: UnicodeString);
    procedure Set_Menuname(Value: UnicodeString);
    procedure Set_Coord(Value: UnicodeString);
    procedure Set_Font(Value: UnicodeString);
    procedure Set_Cssstyle(Value: UnicodeString);
    procedure Set_Style(Value: UnicodeString);
    procedure Set_Exstyle(Value: UnicodeString);
    procedure Set_Sizeunit(Value: UnicodeString);
    procedure Set_Maxwidth(Value: UnicodeString);
    procedure Set_Minwidth(Value: UnicodeString);
    procedure Set_Maxheight(Value: UnicodeString);
    procedure Set_Minheight(Value: UnicodeString);
    procedure Set_Maxbytes(Value: UnicodeString);
    procedure Set_Minbytes(Value: UnicodeString);
    procedure Set_Charclass(Value: UnicodeString);
    procedure Set_Mergedtrans(Value: UnicodeString);
    { Methods & Properties }
    property Id: UnicodeString read Get_Id write Set_Id;
    property Approved: UnicodeString read Get_Approved write Set_Approved;
    property Translate: UnicodeString read Get_Translate write Set_Translate;
    property Reformat: UnicodeString read Get_Reformat write Set_Reformat;
    property Space: UnicodeString read Get_Space write Set_Space;
    property Datatype: UnicodeString read Get_Datatype write Set_Datatype;
    property Phasename: UnicodeString read Get_Phasename write Set_Phasename;
    property Restype: UnicodeString read Get_Restype write Set_Restype;
    property Resname: UnicodeString read Get_Resname write Set_Resname;
    property Extradata: UnicodeString read Get_Extradata write Set_Extradata;
    property Extype: UnicodeString read Get_Extype write Set_Extype;
    property Helpid: UnicodeString read Get_Helpid write Set_Helpid;
    property Menu: UnicodeString read Get_Menu write Set_Menu;
    property Menuoption: UnicodeString read Get_Menuoption write Set_Menuoption;
    property Menuname: UnicodeString read Get_Menuname write Set_Menuname;
    property Coord: UnicodeString read Get_Coord write Set_Coord;
    property Font: UnicodeString read Get_Font write Set_Font;
    property Cssstyle: UnicodeString read Get_Cssstyle write Set_Cssstyle;
    property Style: UnicodeString read Get_Style write Set_Style;
    property Exstyle: UnicodeString read Get_Exstyle write Set_Exstyle;
    property Sizeunit: UnicodeString read Get_Sizeunit write Set_Sizeunit;
    property Maxwidth: UnicodeString read Get_Maxwidth write Set_Maxwidth;
    property Minwidth: UnicodeString read Get_Minwidth write Set_Minwidth;
    property Maxheight: UnicodeString read Get_Maxheight write Set_Maxheight;
    property Minheight: UnicodeString read Get_Minheight write Set_Minheight;
    property Maxbytes: UnicodeString read Get_Maxbytes write Set_Maxbytes;
    property Minbytes: UnicodeString read Get_Minbytes write Set_Minbytes;
    property Charclass: UnicodeString read Get_Charclass write Set_Charclass;
    property Mergedtrans: UnicodeString read Get_Mergedtrans write Set_Mergedtrans;
    property Source: IXMLSource read Get_Source;
    property Segsource: IXMLSegsource read Get_Segsource;
    property Target: IXMLTarget read Get_Target;
    property Contextgroup: IXMLContextgroupList read Get_Contextgroup;
    property Countgroup: IXMLCountgroupList read Get_Countgroup;
    property Note: IXMLNoteList read Get_Note;
    property Alttrans: IXMLAlttransList read Get_Alttrans;
  end;

{ IXMLTransunitList }

  IXMLTransunitList = interface(IXMLNodeCollection)
    ['{586D57BA-F0A9-4F10-8738-BBB2A2FF6D35}']
    { Methods & Properties }
    function Add: IXMLTransunit;
    function Insert(const Index: Integer): IXMLTransunit;

    function Get_Item(Index: Integer): IXMLTransunit;
    property Items[Index: Integer]: IXMLTransunit read Get_Item; default;
  end;

{ IXMLSource }

  IXMLSource = interface(IXMLNode)
    ['{EA8DA0D0-9016-4D91-B490-D48FDC795397}']
    { Property Accessors }
    function Get_Lang: UnicodeString;
    function Get_G: IXMLG;
    function Get_Bpt: IXMLBpt;
    function Get_Ept: IXMLEpt;
    function Get_Ph: IXMLPh;
    function Get_It: IXMLIt;
    function Get_Mrk: IXMLMrk;
    function Get_X: IXMLX;
    function Get_Bx: IXMLBx;
    function Get_Ex: IXMLEx;
    procedure Set_Lang(Value: UnicodeString);
    { Methods & Properties }
    property Lang: UnicodeString read Get_Lang write Set_Lang;
    property G: IXMLG read Get_G;
    property Bpt: IXMLBpt read Get_Bpt;
    property Ept: IXMLEpt read Get_Ept;
    property Ph: IXMLPh read Get_Ph;
    property It: IXMLIt read Get_It;
    property Mrk: IXMLMrk read Get_Mrk;
    property X: IXMLX read Get_X;
    property Bx: IXMLBx read Get_Bx;
    property Ex: IXMLEx read Get_Ex;
  end;

{ IXMLG }

  IXMLG = interface(IXMLNode)
    ['{BB795323-453B-44F7-9114-B2EE5B252642}']
    { Property Accessors }
    function Get_Ctype: UnicodeString;
    function Get_Clone: UnicodeString;
    function Get_Id: UnicodeString;
    function Get_Xid: UnicodeString;
    function Get_Equivtext: UnicodeString;
    function Get_G: IXMLG;
    function Get_Bpt: IXMLBpt;
    function Get_Ept: IXMLEpt;
    function Get_Ph: IXMLPh;
    function Get_It: IXMLIt;
    function Get_Mrk: IXMLMrk;
    function Get_X: IXMLX;
    function Get_Bx: IXMLBx;
    function Get_Ex: IXMLEx;
    procedure Set_Ctype(Value: UnicodeString);
    procedure Set_Clone(Value: UnicodeString);
    procedure Set_Id(Value: UnicodeString);
    procedure Set_Xid(Value: UnicodeString);
    procedure Set_Equivtext(Value: UnicodeString);
    { Methods & Properties }
    property Ctype: UnicodeString read Get_Ctype write Set_Ctype;
    property Clone: UnicodeString read Get_Clone write Set_Clone;
    property Id: UnicodeString read Get_Id write Set_Id;
    property Xid: UnicodeString read Get_Xid write Set_Xid;
    property Equivtext: UnicodeString read Get_Equivtext write Set_Equivtext;
    property G: IXMLG read Get_G;
    property Bpt: IXMLBpt read Get_Bpt;
    property Ept: IXMLEpt read Get_Ept;
    property Ph: IXMLPh read Get_Ph;
    property It: IXMLIt read Get_It;
    property Mrk: IXMLMrk read Get_Mrk;
    property X: IXMLX read Get_X;
    property Bx: IXMLBx read Get_Bx;
    property Ex: IXMLEx read Get_Ex;
  end;

{ IXMLBpt }

  IXMLBpt = interface(IXMLNodeCollection)
    ['{A8BCCB3F-B2CB-4586-8AF3-AD14EF051CAE}']
    { Property Accessors }
    function Get_Rid: UnicodeString;
    function Get_Ctype: UnicodeString;
    function Get_Crc: UnicodeString;
    function Get_Id: UnicodeString;
    function Get_Xid: UnicodeString;
    function Get_Equivtext: UnicodeString;
    function Get_Sub(Index: Integer): IXMLSub;
    procedure Set_Rid(Value: UnicodeString);
    procedure Set_Ctype(Value: UnicodeString);
    procedure Set_Crc(Value: UnicodeString);
    procedure Set_Id(Value: UnicodeString);
    procedure Set_Xid(Value: UnicodeString);
    procedure Set_Equivtext(Value: UnicodeString);
    { Methods & Properties }
    function Add: IXMLSub;
    function Insert(const Index: Integer): IXMLSub;
    property Rid: UnicodeString read Get_Rid write Set_Rid;
    property Ctype: UnicodeString read Get_Ctype write Set_Ctype;
    property Crc: UnicodeString read Get_Crc write Set_Crc;
    property Id: UnicodeString read Get_Id write Set_Id;
    property Xid: UnicodeString read Get_Xid write Set_Xid;
    property Equivtext: UnicodeString read Get_Equivtext write Set_Equivtext;
    property Sub[Index: Integer]: IXMLSub read Get_Sub; default;
  end;

{ IXMLSub }

  IXMLSub = interface(IXMLNode)
    ['{398BC8A5-DEBC-4F5B-AE17-A33718D91B57}']
    { Property Accessors }
    function Get_Datatype: UnicodeString;
    function Get_Ctype: UnicodeString;
    function Get_Xid: UnicodeString;
    function Get_G: IXMLG;
    function Get_Bpt: IXMLBpt;
    function Get_Ept: IXMLEpt;
    function Get_Ph: IXMLPh;
    function Get_It: IXMLIt;
    function Get_Mrk: IXMLMrk;
    function Get_X: IXMLX;
    function Get_Bx: IXMLBx;
    function Get_Ex: IXMLEx;
    procedure Set_Datatype(Value: UnicodeString);
    procedure Set_Ctype(Value: UnicodeString);
    procedure Set_Xid(Value: UnicodeString);
    { Methods & Properties }
    property Datatype: UnicodeString read Get_Datatype write Set_Datatype;
    property Ctype: UnicodeString read Get_Ctype write Set_Ctype;
    property Xid: UnicodeString read Get_Xid write Set_Xid;
    property G: IXMLG read Get_G;
    property Bpt: IXMLBpt read Get_Bpt;
    property Ept: IXMLEpt read Get_Ept;
    property Ph: IXMLPh read Get_Ph;
    property It: IXMLIt read Get_It;
    property Mrk: IXMLMrk read Get_Mrk;
    property X: IXMLX read Get_X;
    property Bx: IXMLBx read Get_Bx;
    property Ex: IXMLEx read Get_Ex;
  end;

{ IXMLEpt }

  IXMLEpt = interface(IXMLNodeCollection)
    ['{89DB6E0D-FB54-45AF-8F45-EF3428AB5247}']
    { Property Accessors }
    function Get_Rid: UnicodeString;
    function Get_Crc: UnicodeString;
    function Get_Id: UnicodeString;
    function Get_Xid: UnicodeString;
    function Get_Equivtext: UnicodeString;
    function Get_Sub(Index: Integer): IXMLSub;
    procedure Set_Rid(Value: UnicodeString);
    procedure Set_Crc(Value: UnicodeString);
    procedure Set_Id(Value: UnicodeString);
    procedure Set_Xid(Value: UnicodeString);
    procedure Set_Equivtext(Value: UnicodeString);
    { Methods & Properties }
    function Add: IXMLSub;
    function Insert(const Index: Integer): IXMLSub;
    property Rid: UnicodeString read Get_Rid write Set_Rid;
    property Crc: UnicodeString read Get_Crc write Set_Crc;
    property Id: UnicodeString read Get_Id write Set_Id;
    property Xid: UnicodeString read Get_Xid write Set_Xid;
    property Equivtext: UnicodeString read Get_Equivtext write Set_Equivtext;
    property Sub[Index: Integer]: IXMLSub read Get_Sub; default;
  end;

{ IXMLPh }

  IXMLPh = interface(IXMLNodeCollection)
    ['{9DF6D22F-00A2-481C-8E56-2BA52F5CA457}']
    { Property Accessors }
    function Get_Ctype: UnicodeString;
    function Get_Crc: UnicodeString;
    function Get_Assoc: UnicodeString;
    function Get_Id: UnicodeString;
    function Get_Xid: UnicodeString;
    function Get_Equivtext: UnicodeString;
    function Get_Sub(Index: Integer): IXMLSub;
    procedure Set_Ctype(Value: UnicodeString);
    procedure Set_Crc(Value: UnicodeString);
    procedure Set_Assoc(Value: UnicodeString);
    procedure Set_Id(Value: UnicodeString);
    procedure Set_Xid(Value: UnicodeString);
    procedure Set_Equivtext(Value: UnicodeString);
    { Methods & Properties }
    function Add: IXMLSub;
    function Insert(const Index: Integer): IXMLSub;
    property Ctype: UnicodeString read Get_Ctype write Set_Ctype;
    property Crc: UnicodeString read Get_Crc write Set_Crc;
    property Assoc: UnicodeString read Get_Assoc write Set_Assoc;
    property Id: UnicodeString read Get_Id write Set_Id;
    property Xid: UnicodeString read Get_Xid write Set_Xid;
    property Equivtext: UnicodeString read Get_Equivtext write Set_Equivtext;
    property Sub[Index: Integer]: IXMLSub read Get_Sub; default;
  end;

{ IXMLIt }

  IXMLIt = interface(IXMLNodeCollection)
    ['{7ED03ABA-FFB8-4676-AE43-833180152853}']
    { Property Accessors }
    function Get_Pos: UnicodeString;
    function Get_Rid: UnicodeString;
    function Get_Ctype: UnicodeString;
    function Get_Crc: UnicodeString;
    function Get_Id: UnicodeString;
    function Get_Xid: UnicodeString;
    function Get_Equivtext: UnicodeString;
    function Get_Sub(Index: Integer): IXMLSub;
    procedure Set_Pos(Value: UnicodeString);
    procedure Set_Rid(Value: UnicodeString);
    procedure Set_Ctype(Value: UnicodeString);
    procedure Set_Crc(Value: UnicodeString);
    procedure Set_Id(Value: UnicodeString);
    procedure Set_Xid(Value: UnicodeString);
    procedure Set_Equivtext(Value: UnicodeString);
    { Methods & Properties }
    function Add: IXMLSub;
    function Insert(const Index: Integer): IXMLSub;
    property Pos: UnicodeString read Get_Pos write Set_Pos;
    property Rid: UnicodeString read Get_Rid write Set_Rid;
    property Ctype: UnicodeString read Get_Ctype write Set_Ctype;
    property Crc: UnicodeString read Get_Crc write Set_Crc;
    property Id: UnicodeString read Get_Id write Set_Id;
    property Xid: UnicodeString read Get_Xid write Set_Xid;
    property Equivtext: UnicodeString read Get_Equivtext write Set_Equivtext;
    property Sub[Index: Integer]: IXMLSub read Get_Sub; default;
  end;

{ IXMLMrk }

  IXMLMrk = interface(IXMLNode)
    ['{C27ED293-1691-426D-980C-545FC463E41F}']
    { Property Accessors }
    function Get_Mtype: UnicodeString;
    function Get_Mid: UnicodeString;
    function Get_Comment: UnicodeString;
    function Get_G: IXMLG;
    function Get_Bpt: IXMLBpt;
    function Get_Ept: IXMLEpt;
    function Get_Ph: IXMLPh;
    function Get_It: IXMLIt;
    function Get_Mrk: IXMLMrk;
    function Get_X: IXMLX;
    function Get_Bx: IXMLBx;
    function Get_Ex: IXMLEx;
    procedure Set_Mtype(Value: UnicodeString);
    procedure Set_Mid(Value: UnicodeString);
    procedure Set_Comment(Value: UnicodeString);
    { Methods & Properties }
    property Mtype: UnicodeString read Get_Mtype write Set_Mtype;
    property Mid: UnicodeString read Get_Mid write Set_Mid;
    property Comment: UnicodeString read Get_Comment write Set_Comment;
    property G: IXMLG read Get_G;
    property Bpt: IXMLBpt read Get_Bpt;
    property Ept: IXMLEpt read Get_Ept;
    property Ph: IXMLPh read Get_Ph;
    property It: IXMLIt read Get_It;
    property Mrk: IXMLMrk read Get_Mrk;
    property X: IXMLX read Get_X;
    property Bx: IXMLBx read Get_Bx;
    property Ex: IXMLEx read Get_Ex;
  end;

{ IXMLX }

  IXMLX = interface(IXMLNode)
    ['{E81093BF-4EA5-42C3-8A81-B21C0D705D7B}']
    { Property Accessors }
    function Get_Ctype: UnicodeString;
    function Get_Clone: UnicodeString;
    function Get_Id: UnicodeString;
    function Get_Xid: UnicodeString;
    function Get_Equivtext: UnicodeString;
    procedure Set_Ctype(Value: UnicodeString);
    procedure Set_Clone(Value: UnicodeString);
    procedure Set_Id(Value: UnicodeString);
    procedure Set_Xid(Value: UnicodeString);
    procedure Set_Equivtext(Value: UnicodeString);
    { Methods & Properties }
    property Ctype: UnicodeString read Get_Ctype write Set_Ctype;
    property Clone: UnicodeString read Get_Clone write Set_Clone;
    property Id: UnicodeString read Get_Id write Set_Id;
    property Xid: UnicodeString read Get_Xid write Set_Xid;
    property Equivtext: UnicodeString read Get_Equivtext write Set_Equivtext;
  end;

{ IXMLBx }

  IXMLBx = interface(IXMLNode)
    ['{0151453F-A5B4-4081-B7F0-4692BB5A8545}']
    { Property Accessors }
    function Get_Rid: UnicodeString;
    function Get_Ctype: UnicodeString;
    function Get_Clone: UnicodeString;
    function Get_Id: UnicodeString;
    function Get_Xid: UnicodeString;
    function Get_Equivtext: UnicodeString;
    procedure Set_Rid(Value: UnicodeString);
    procedure Set_Ctype(Value: UnicodeString);
    procedure Set_Clone(Value: UnicodeString);
    procedure Set_Id(Value: UnicodeString);
    procedure Set_Xid(Value: UnicodeString);
    procedure Set_Equivtext(Value: UnicodeString);
    { Methods & Properties }
    property Rid: UnicodeString read Get_Rid write Set_Rid;
    property Ctype: UnicodeString read Get_Ctype write Set_Ctype;
    property Clone: UnicodeString read Get_Clone write Set_Clone;
    property Id: UnicodeString read Get_Id write Set_Id;
    property Xid: UnicodeString read Get_Xid write Set_Xid;
    property Equivtext: UnicodeString read Get_Equivtext write Set_Equivtext;
  end;

{ IXMLEx }

  IXMLEx = interface(IXMLNode)
    ['{A30F718F-FA49-4B57-82D8-C436B7C25080}']
    { Property Accessors }
    function Get_Rid: UnicodeString;
    function Get_Id: UnicodeString;
    function Get_Xid: UnicodeString;
    function Get_Equivtext: UnicodeString;
    procedure Set_Rid(Value: UnicodeString);
    procedure Set_Id(Value: UnicodeString);
    procedure Set_Xid(Value: UnicodeString);
    procedure Set_Equivtext(Value: UnicodeString);
    { Methods & Properties }
    property Rid: UnicodeString read Get_Rid write Set_Rid;
    property Id: UnicodeString read Get_Id write Set_Id;
    property Xid: UnicodeString read Get_Xid write Set_Xid;
    property Equivtext: UnicodeString read Get_Equivtext write Set_Equivtext;
  end;

{ IXMLSegsource }

  IXMLSegsource = interface(IXMLNode)
    ['{8A5892D1-CF94-475D-8784-98A435082159}']
    { Property Accessors }
    function Get_Lang: UnicodeString;
    function Get_G: IXMLG;
    function Get_Bpt: IXMLBpt;
    function Get_Ept: IXMLEpt;
    function Get_Ph: IXMLPh;
    function Get_It: IXMLIt;
    function Get_Mrk: IXMLMrk;
    function Get_X: IXMLX;
    function Get_Bx: IXMLBx;
    function Get_Ex: IXMLEx;
    procedure Set_Lang(Value: UnicodeString);
    { Methods & Properties }
    property Lang: UnicodeString read Get_Lang write Set_Lang;
    property G: IXMLG read Get_G;
    property Bpt: IXMLBpt read Get_Bpt;
    property Ept: IXMLEpt read Get_Ept;
    property Ph: IXMLPh read Get_Ph;
    property It: IXMLIt read Get_It;
    property Mrk: IXMLMrk read Get_Mrk;
    property X: IXMLX read Get_X;
    property Bx: IXMLBx read Get_Bx;
    property Ex: IXMLEx read Get_Ex;
  end;

{ IXMLTarget }

  IXMLTarget = interface(IXMLNode)
    ['{648FE006-AF10-466F-8013-BB229E419455}']
    { Property Accessors }
    function Get_State: UnicodeString;
    function Get_Statequalifier: UnicodeString;
    function Get_Phasename: UnicodeString;
    function Get_Lang: UnicodeString;
    function Get_Resname: UnicodeString;
    function Get_Coord: UnicodeString;
    function Get_Font: UnicodeString;
    function Get_Cssstyle: UnicodeString;
    function Get_Style: UnicodeString;
    function Get_Exstyle: UnicodeString;
    function Get_Equivtrans: UnicodeString;
    function Get_G: IXMLG;
    function Get_Bpt: IXMLBpt;
    function Get_Ept: IXMLEpt;
    function Get_Ph: IXMLPh;
    function Get_It: IXMLIt;
    function Get_Mrk: IXMLMrk;
    function Get_X: IXMLX;
    function Get_Bx: IXMLBx;
    function Get_Ex: IXMLEx;
    procedure Set_State(Value: UnicodeString);
    procedure Set_Statequalifier(Value: UnicodeString);
    procedure Set_Phasename(Value: UnicodeString);
    procedure Set_Lang(Value: UnicodeString);
    procedure Set_Resname(Value: UnicodeString);
    procedure Set_Coord(Value: UnicodeString);
    procedure Set_Font(Value: UnicodeString);
    procedure Set_Cssstyle(Value: UnicodeString);
    procedure Set_Style(Value: UnicodeString);
    procedure Set_Exstyle(Value: UnicodeString);
    procedure Set_Equivtrans(Value: UnicodeString);
    { Methods & Properties }
    property State: UnicodeString read Get_State write Set_State;
    property Statequalifier: UnicodeString read Get_Statequalifier write Set_Statequalifier;
    property Phasename: UnicodeString read Get_Phasename write Set_Phasename;
    property Lang: UnicodeString read Get_Lang write Set_Lang;
    property Resname: UnicodeString read Get_Resname write Set_Resname;
    property Coord: UnicodeString read Get_Coord write Set_Coord;
    property Font: UnicodeString read Get_Font write Set_Font;
    property Cssstyle: UnicodeString read Get_Cssstyle write Set_Cssstyle;
    property Style: UnicodeString read Get_Style write Set_Style;
    property Exstyle: UnicodeString read Get_Exstyle write Set_Exstyle;
    property Equivtrans: UnicodeString read Get_Equivtrans write Set_Equivtrans;
    property G: IXMLG read Get_G;
    property Bpt: IXMLBpt read Get_Bpt;
    property Ept: IXMLEpt read Get_Ept;
    property Ph: IXMLPh read Get_Ph;
    property It: IXMLIt read Get_It;
    property Mrk: IXMLMrk read Get_Mrk;
    property X: IXMLX read Get_X;
    property Bx: IXMLBx read Get_Bx;
    property Ex: IXMLEx read Get_Ex;
  end;

{ IXMLAlttrans }

  IXMLAlttrans = interface(IXMLNode)
    ['{8DEE7F4F-DEB1-490D-A442-954C9F369733}']
    { Property Accessors }
    function Get_Matchquality: UnicodeString;
    function Get_Toolid: UnicodeString;
    function Get_Crc: UnicodeString;
    function Get_Lang: UnicodeString;
    function Get_Origin: UnicodeString;
    function Get_Datatype: UnicodeString;
    function Get_Space: UnicodeString;
    function Get_Restype: UnicodeString;
    function Get_Resname: UnicodeString;
    function Get_Extradata: UnicodeString;
    function Get_Extype: UnicodeString;
    function Get_Helpid: UnicodeString;
    function Get_Menu: UnicodeString;
    function Get_Menuoption: UnicodeString;
    function Get_Menuname: UnicodeString;
    function Get_Mid: UnicodeString;
    function Get_Coord: UnicodeString;
    function Get_Font: UnicodeString;
    function Get_Cssstyle: UnicodeString;
    function Get_Style: UnicodeString;
    function Get_Exstyle: UnicodeString;
    function Get_Phasename: UnicodeString;
    function Get_Alttranstype: UnicodeString;
    function Get_Source: IXMLSource;
    function Get_Segsource: IXMLSegsource;
    function Get_Target: IXMLTarget;
    function Get_Contextgroup: IXMLContextgroupList;
    function Get_Note: IXMLNoteList;
    procedure Set_Matchquality(Value: UnicodeString);
    procedure Set_Toolid(Value: UnicodeString);
    procedure Set_Crc(Value: UnicodeString);
    procedure Set_Lang(Value: UnicodeString);
    procedure Set_Origin(Value: UnicodeString);
    procedure Set_Datatype(Value: UnicodeString);
    procedure Set_Space(Value: UnicodeString);
    procedure Set_Restype(Value: UnicodeString);
    procedure Set_Resname(Value: UnicodeString);
    procedure Set_Extradata(Value: UnicodeString);
    procedure Set_Extype(Value: UnicodeString);
    procedure Set_Helpid(Value: UnicodeString);
    procedure Set_Menu(Value: UnicodeString);
    procedure Set_Menuoption(Value: UnicodeString);
    procedure Set_Menuname(Value: UnicodeString);
    procedure Set_Mid(Value: UnicodeString);
    procedure Set_Coord(Value: UnicodeString);
    procedure Set_Font(Value: UnicodeString);
    procedure Set_Cssstyle(Value: UnicodeString);
    procedure Set_Style(Value: UnicodeString);
    procedure Set_Exstyle(Value: UnicodeString);
    procedure Set_Phasename(Value: UnicodeString);
    procedure Set_Alttranstype(Value: UnicodeString);
    { Methods & Properties }
    property Matchquality: UnicodeString read Get_Matchquality write Set_Matchquality;
    property Toolid: UnicodeString read Get_Toolid write Set_Toolid;
    property Crc: UnicodeString read Get_Crc write Set_Crc;
    property Lang: UnicodeString read Get_Lang write Set_Lang;
    property Origin: UnicodeString read Get_Origin write Set_Origin;
    property Datatype: UnicodeString read Get_Datatype write Set_Datatype;
    property Space: UnicodeString read Get_Space write Set_Space;
    property Restype: UnicodeString read Get_Restype write Set_Restype;
    property Resname: UnicodeString read Get_Resname write Set_Resname;
    property Extradata: UnicodeString read Get_Extradata write Set_Extradata;
    property Extype: UnicodeString read Get_Extype write Set_Extype;
    property Helpid: UnicodeString read Get_Helpid write Set_Helpid;
    property Menu: UnicodeString read Get_Menu write Set_Menu;
    property Menuoption: UnicodeString read Get_Menuoption write Set_Menuoption;
    property Menuname: UnicodeString read Get_Menuname write Set_Menuname;
    property Mid: UnicodeString read Get_Mid write Set_Mid;
    property Coord: UnicodeString read Get_Coord write Set_Coord;
    property Font: UnicodeString read Get_Font write Set_Font;
    property Cssstyle: UnicodeString read Get_Cssstyle write Set_Cssstyle;
    property Style: UnicodeString read Get_Style write Set_Style;
    property Exstyle: UnicodeString read Get_Exstyle write Set_Exstyle;
    property Phasename: UnicodeString read Get_Phasename write Set_Phasename;
    property Alttranstype: UnicodeString read Get_Alttranstype write Set_Alttranstype;
    property Source: IXMLSource read Get_Source;
    property Segsource: IXMLSegsource read Get_Segsource;
    property Target: IXMLTarget read Get_Target;
    property Contextgroup: IXMLContextgroupList read Get_Contextgroup;
    property Note: IXMLNoteList read Get_Note;
  end;

{ IXMLAlttransList }

  IXMLAlttransList = interface(IXMLNodeCollection)
    ['{4748A95C-55F1-41DF-B7C9-12CEE2875BE6}']
    { Methods & Properties }
    function Add: IXMLAlttrans;
    function Insert(const Index: Integer): IXMLAlttrans;

    function Get_Item(Index: Integer): IXMLAlttrans;
    property Items[Index: Integer]: IXMLAlttrans read Get_Item; default;
  end;

{ IXMLBinunit }

  IXMLBinunit = interface(IXMLNode)
    ['{BD22F3DC-3D8E-4996-A81F-F1ADE0CAFF5C}']
    { Property Accessors }
    function Get_Id: UnicodeString;
    function Get_Mimetype: UnicodeString;
    function Get_Approved: UnicodeString;
    function Get_Translate: UnicodeString;
    function Get_Reformat: UnicodeString;
    function Get_Restype: UnicodeString;
    function Get_Resname: UnicodeString;
    function Get_Phasename: UnicodeString;
    function Get_Binsource: IXMLBinsource;
    function Get_Bintarget: IXMLBintarget;
    function Get_Contextgroup: IXMLContextgroupList;
    function Get_Countgroup: IXMLCountgroupList;
    function Get_Note: IXMLNoteList;
    function Get_Transunit: IXMLTransunitList;
    procedure Set_Id(Value: UnicodeString);
    procedure Set_Mimetype(Value: UnicodeString);
    procedure Set_Approved(Value: UnicodeString);
    procedure Set_Translate(Value: UnicodeString);
    procedure Set_Reformat(Value: UnicodeString);
    procedure Set_Restype(Value: UnicodeString);
    procedure Set_Resname(Value: UnicodeString);
    procedure Set_Phasename(Value: UnicodeString);
    { Methods & Properties }
    property Id: UnicodeString read Get_Id write Set_Id;
    property Mimetype: UnicodeString read Get_Mimetype write Set_Mimetype;
    property Approved: UnicodeString read Get_Approved write Set_Approved;
    property Translate: UnicodeString read Get_Translate write Set_Translate;
    property Reformat: UnicodeString read Get_Reformat write Set_Reformat;
    property Restype: UnicodeString read Get_Restype write Set_Restype;
    property Resname: UnicodeString read Get_Resname write Set_Resname;
    property Phasename: UnicodeString read Get_Phasename write Set_Phasename;
    property Binsource: IXMLBinsource read Get_Binsource;
    property Bintarget: IXMLBintarget read Get_Bintarget;
    property Contextgroup: IXMLContextgroupList read Get_Contextgroup;
    property Countgroup: IXMLCountgroupList read Get_Countgroup;
    property Note: IXMLNoteList read Get_Note;
    property Transunit: IXMLTransunitList read Get_Transunit;
  end;

{ IXMLBinunitList }

  IXMLBinunitList = interface(IXMLNodeCollection)
    ['{BF7564E2-8051-4181-AC33-B998C4381FD2}']
    { Methods & Properties }
    function Add: IXMLBinunit;
    function Insert(const Index: Integer): IXMLBinunit;

    function Get_Item(Index: Integer): IXMLBinunit;
    property Items[Index: Integer]: IXMLBinunit read Get_Item; default;
  end;

{ IXMLBinsource }

  IXMLBinsource = interface(IXMLNode)
    ['{46833121-40F7-4637-8A03-2EE7A78C2D65}']
    { Property Accessors }
    function Get_Internalfile: IXMLInternalfile;
    function Get_Externalfile: IXMLExternalfile;
    { Methods & Properties }
    property Internalfile: IXMLInternalfile read Get_Internalfile;
    property Externalfile: IXMLExternalfile read Get_Externalfile;
  end;

{ IXMLBintarget }

  IXMLBintarget = interface(IXMLNode)
    ['{E63CBF8A-0B8D-4815-8296-9F62C4D0F0EF}']
    { Property Accessors }
    function Get_Mimetype: UnicodeString;
    function Get_State: UnicodeString;
    function Get_Statequalifier: UnicodeString;
    function Get_Phasename: UnicodeString;
    function Get_Restype: UnicodeString;
    function Get_Resname: UnicodeString;
    function Get_Internalfile: IXMLInternalfile;
    function Get_Externalfile: IXMLExternalfile;
    procedure Set_Mimetype(Value: UnicodeString);
    procedure Set_State(Value: UnicodeString);
    procedure Set_Statequalifier(Value: UnicodeString);
    procedure Set_Phasename(Value: UnicodeString);
    procedure Set_Restype(Value: UnicodeString);
    procedure Set_Resname(Value: UnicodeString);
    { Methods & Properties }
    property Mimetype: UnicodeString read Get_Mimetype write Set_Mimetype;
    property State: UnicodeString read Get_State write Set_State;
    property Statequalifier: UnicodeString read Get_Statequalifier write Set_Statequalifier;
    property Phasename: UnicodeString read Get_Phasename write Set_Phasename;
    property Restype: UnicodeString read Get_Restype write Set_Restype;
    property Resname: UnicodeString read Get_Resname write Set_Resname;
    property Internalfile: IXMLInternalfile read Get_Internalfile;
    property Externalfile: IXMLExternalfile read Get_Externalfile;
  end;

{ Forward Decls }

  TXMLXliff = class;
  TXMLFile_ = class;
  TXMLHeader = class;
  TXMLElemType_ExternalReference = class;
  TXMLElemType_ExternalReferenceList = class;
  TXMLInternalfile = class;
  TXMLExternalfile = class;
  TXMLPhasegroup = class;
  TXMLPhase = class;
  TXMLNote = class;
  TXMLNoteList = class;
  TXMLCountgroup = class;
  TXMLCountgroupList = class;
  TXMLCount = class;
  TXMLTool = class;
  TXMLToolList = class;
  TXMLBody = class;
  TXMLGroup = class;
  TXMLGroupList = class;
  TXMLContextgroup = class;
  TXMLContextgroupList = class;
  TXMLContext = class;
  TXMLTransunit = class;
  TXMLTransunitList = class;
  TXMLSource = class;
  TXMLG = class;
  TXMLBpt = class;
  TXMLSub = class;
  TXMLEpt = class;
  TXMLPh = class;
  TXMLIt = class;
  TXMLMrk = class;
  TXMLX = class;
  TXMLBx = class;
  TXMLEx = class;
  TXMLSegsource = class;
  TXMLTarget = class;
  TXMLAlttrans = class;
  TXMLAlttransList = class;
  TXMLBinunit = class;
  TXMLBinunitList = class;
  TXMLBinsource = class;
  TXMLBintarget = class;

{ TXMLXliff }

  TXMLXliff = class(TXMLNodeCollection, IXMLXliff)
  protected
    { IXMLXliff }
    function Get_Version: UnicodeString;
    function Get_Lang: UnicodeString;
    function Get_File_(Index: Integer): IXMLFile_;
    procedure Set_Version(Value: UnicodeString);
    procedure Set_Lang(Value: UnicodeString);
    function Add: IXMLFile_;
    function Insert(const Index: Integer): IXMLFile_;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLFile_ }

  TXMLFile_ = class(TXMLNode, IXMLFile_)
  protected
    { IXMLFile_ }
    function Get_Original: UnicodeString;
    function Get_Sourcelanguage: UnicodeString;
    function Get_Datatype: UnicodeString;
    function Get_Toolid: UnicodeString;
    function Get_Date: UnicodeString;
    function Get_Space: UnicodeString;
    function Get_Category: UnicodeString;
    function Get_Targetlanguage: UnicodeString;
    function Get_Productname: UnicodeString;
    function Get_Productversion: UnicodeString;
    function Get_Buildnum: UnicodeString;
    function Get_Header: IXMLHeader;
    function Get_Body: IXMLBody;
    procedure Set_Original(Value: UnicodeString);
    procedure Set_Sourcelanguage(Value: UnicodeString);
    procedure Set_Datatype(Value: UnicodeString);
    procedure Set_Toolid(Value: UnicodeString);
    procedure Set_Date(Value: UnicodeString);
    procedure Set_Space(Value: UnicodeString);
    procedure Set_Category(Value: UnicodeString);
    procedure Set_Targetlanguage(Value: UnicodeString);
    procedure Set_Productname(Value: UnicodeString);
    procedure Set_Productversion(Value: UnicodeString);
    procedure Set_Buildnum(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLHeader }

  TXMLHeader = class(TXMLNode, IXMLHeader)
  private
    FGlossary: IXMLElemType_ExternalReferenceList;
    FReference: IXMLElemType_ExternalReferenceList;
    FCountgroup: IXMLCountgroupList;
    FNote: IXMLNoteList;
    FTool: IXMLToolList;
  protected
    { IXMLHeader }
    function Get_Skl: IXMLElemType_ExternalReference;
    function Get_Phasegroup: IXMLPhasegroup;
    function Get_Glossary: IXMLElemType_ExternalReferenceList;
    function Get_Reference: IXMLElemType_ExternalReferenceList;
    function Get_Countgroup: IXMLCountgroupList;
    function Get_Note: IXMLNoteList;
    function Get_Tool: IXMLToolList;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLElemType_ExternalReference }

  TXMLElemType_ExternalReference = class(TXMLNode, IXMLElemType_ExternalReference)
  protected
    { IXMLElemType_ExternalReference }
    function Get_Internalfile: IXMLInternalfile;
    function Get_Externalfile: IXMLExternalfile;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLElemType_ExternalReferenceList }

  TXMLElemType_ExternalReferenceList = class(TXMLNodeCollection, IXMLElemType_ExternalReferenceList)
  protected
    { IXMLElemType_ExternalReferenceList }
    function Add: IXMLElemType_ExternalReference;
    function Insert(const Index: Integer): IXMLElemType_ExternalReference;

    function Get_Item(Index: Integer): IXMLElemType_ExternalReference;
  end;

{ TXMLInternalfile }

  TXMLInternalfile = class(TXMLNode, IXMLInternalfile)
  protected
    { IXMLInternalfile }
    function Get_Form: UnicodeString;
    function Get_Crc: UnicodeString;
    procedure Set_Form(Value: UnicodeString);
    procedure Set_Crc(Value: UnicodeString);
  end;

{ TXMLExternalfile }

  TXMLExternalfile = class(TXMLNode, IXMLExternalfile)
  protected
    { IXMLExternalfile }
    function Get_Href: UnicodeString;
    function Get_Crc: UnicodeString;
    function Get_Uid: UnicodeString;
    procedure Set_Href(Value: UnicodeString);
    procedure Set_Crc(Value: UnicodeString);
    procedure Set_Uid(Value: UnicodeString);
  end;

{ TXMLPhasegroup }

  TXMLPhasegroup = class(TXMLNodeCollection, IXMLPhasegroup)
  protected
    { IXMLPhasegroup }
    function Get_Phase(Index: Integer): IXMLPhase;
    function Add: IXMLPhase;
    function Insert(const Index: Integer): IXMLPhase;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLPhase }

  TXMLPhase = class(TXMLNodeCollection, IXMLPhase)
  protected
    { IXMLPhase }
    function Get_Phasename: UnicodeString;
    function Get_Processname: UnicodeString;
    function Get_Companyname: UnicodeString;
    function Get_Toolid: UnicodeString;
    function Get_Date: UnicodeString;
    function Get_Jobid: UnicodeString;
    function Get_Contactname: UnicodeString;
    function Get_Contactemail: UnicodeString;
    function Get_Contactphone: UnicodeString;
    function Get_Note(Index: Integer): IXMLNote;
    procedure Set_Phasename(Value: UnicodeString);
    procedure Set_Processname(Value: UnicodeString);
    procedure Set_Companyname(Value: UnicodeString);
    procedure Set_Toolid(Value: UnicodeString);
    procedure Set_Date(Value: UnicodeString);
    procedure Set_Jobid(Value: UnicodeString);
    procedure Set_Contactname(Value: UnicodeString);
    procedure Set_Contactemail(Value: UnicodeString);
    procedure Set_Contactphone(Value: UnicodeString);
    function Add: IXMLNote;
    function Insert(const Index: Integer): IXMLNote;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLNote }

  TXMLNote = class(TXMLNode, IXMLNote)
  protected
    { IXMLNote }
    function Get_Lang: UnicodeString;
    function Get_Priority: UnicodeString;
    function Get_From: UnicodeString;
    function Get_Annotates: UnicodeString;
    procedure Set_Lang(Value: UnicodeString);
    procedure Set_Priority(Value: UnicodeString);
    procedure Set_From(Value: UnicodeString);
    procedure Set_Annotates(Value: UnicodeString);
  end;

{ TXMLNoteList }

  TXMLNoteList = class(TXMLNodeCollection, IXMLNoteList)
  protected
    { IXMLNoteList }
    function Add: IXMLNote;
    function Insert(const Index: Integer): IXMLNote;

    function Get_Item(Index: Integer): IXMLNote;
  end;

{ TXMLCountgroup }

  TXMLCountgroup = class(TXMLNodeCollection, IXMLCountgroup)
  protected
    { IXMLCountgroup }
    function Get_Name: UnicodeString;
    function Get_Count(Index: Integer): IXMLCount;
    procedure Set_Name(Value: UnicodeString);
    function Add: IXMLCount;
    function Insert(const Index: Integer): IXMLCount;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLCountgroupList }

  TXMLCountgroupList = class(TXMLNodeCollection, IXMLCountgroupList)
  protected
    { IXMLCountgroupList }
    function Add: IXMLCountgroup;
    function Insert(const Index: Integer): IXMLCountgroup;

    function Get_Item(Index: Integer): IXMLCountgroup;
  end;

{ TXMLCount }

  TXMLCount = class(TXMLNode, IXMLCount)
  protected
    { IXMLCount }
    function Get_Counttype: UnicodeString;
    function Get_Phasename: UnicodeString;
    function Get_Unit_: UnicodeString;
    procedure Set_Counttype(Value: UnicodeString);
    procedure Set_Phasename(Value: UnicodeString);
    procedure Set_Unit_(Value: UnicodeString);
  end;

{ TXMLTool }

  TXMLTool = class(TXMLNode, IXMLTool)
  protected
    { IXMLTool }
    function Get_Toolid: UnicodeString;
    function Get_Toolname: UnicodeString;
    function Get_Toolversion: UnicodeString;
    function Get_Toolcompany: UnicodeString;
    procedure Set_Toolid(Value: UnicodeString);
    procedure Set_Toolname(Value: UnicodeString);
    procedure Set_Toolversion(Value: UnicodeString);
    procedure Set_Toolcompany(Value: UnicodeString);
  end;

{ TXMLToolList }

  TXMLToolList = class(TXMLNodeCollection, IXMLToolList)
  protected
    { IXMLToolList }
    function Add: IXMLTool;
    function Insert(const Index: Integer): IXMLTool;

    function Get_Item(Index: Integer): IXMLTool;
  end;

{ TXMLBody }

  TXMLBody = class(TXMLNode, IXMLBody)
  private
    FGroup: IXMLGroupList;
    FTransunit: IXMLTransunitList;
    FBinunit: IXMLBinunitList;
  protected
    { IXMLBody }
    function Get_Group: IXMLGroupList;
    function Get_Transunit: IXMLTransunitList;
    function Get_Binunit: IXMLBinunitList;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLGroup }

  TXMLGroup = class(TXMLNode, IXMLGroup)
  private
    FContextgroup: IXMLContextgroupList;
    FCountgroup: IXMLCountgroupList;
    FNote: IXMLNoteList;
    FGroup: IXMLGroupList;
    FTransunit: IXMLTransunitList;
    FBinunit: IXMLBinunitList;
  protected
    { IXMLGroup }
    function Get_Id: UnicodeString;
    function Get_Datatype: UnicodeString;
    function Get_Space: UnicodeString;
    function Get_Restype: UnicodeString;
    function Get_Resname: UnicodeString;
    function Get_Extradata: UnicodeString;
    function Get_Extype: UnicodeString;
    function Get_Helpid: UnicodeString;
    function Get_Menu: UnicodeString;
    function Get_Menuoption: UnicodeString;
    function Get_Menuname: UnicodeString;
    function Get_Coord: UnicodeString;
    function Get_Font: UnicodeString;
    function Get_Cssstyle: UnicodeString;
    function Get_Style: UnicodeString;
    function Get_Exstyle: UnicodeString;
    function Get_Translate: UnicodeString;
    function Get_Reformat: UnicodeString;
    function Get_Sizeunit: UnicodeString;
    function Get_Maxwidth: UnicodeString;
    function Get_Minwidth: UnicodeString;
    function Get_Maxheight: UnicodeString;
    function Get_Minheight: UnicodeString;
    function Get_Maxbytes: UnicodeString;
    function Get_Minbytes: UnicodeString;
    function Get_Charclass: UnicodeString;
    function Get_Mergedtrans: UnicodeString;
    function Get_Contextgroup: IXMLContextgroupList;
    function Get_Countgroup: IXMLCountgroupList;
    function Get_Note: IXMLNoteList;
    function Get_Group: IXMLGroupList;
    function Get_Transunit: IXMLTransunitList;
    function Get_Binunit: IXMLBinunitList;
    procedure Set_Id(Value: UnicodeString);
    procedure Set_Datatype(Value: UnicodeString);
    procedure Set_Space(Value: UnicodeString);
    procedure Set_Restype(Value: UnicodeString);
    procedure Set_Resname(Value: UnicodeString);
    procedure Set_Extradata(Value: UnicodeString);
    procedure Set_Extype(Value: UnicodeString);
    procedure Set_Helpid(Value: UnicodeString);
    procedure Set_Menu(Value: UnicodeString);
    procedure Set_Menuoption(Value: UnicodeString);
    procedure Set_Menuname(Value: UnicodeString);
    procedure Set_Coord(Value: UnicodeString);
    procedure Set_Font(Value: UnicodeString);
    procedure Set_Cssstyle(Value: UnicodeString);
    procedure Set_Style(Value: UnicodeString);
    procedure Set_Exstyle(Value: UnicodeString);
    procedure Set_Translate(Value: UnicodeString);
    procedure Set_Reformat(Value: UnicodeString);
    procedure Set_Sizeunit(Value: UnicodeString);
    procedure Set_Maxwidth(Value: UnicodeString);
    procedure Set_Minwidth(Value: UnicodeString);
    procedure Set_Maxheight(Value: UnicodeString);
    procedure Set_Minheight(Value: UnicodeString);
    procedure Set_Maxbytes(Value: UnicodeString);
    procedure Set_Minbytes(Value: UnicodeString);
    procedure Set_Charclass(Value: UnicodeString);
    procedure Set_Mergedtrans(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLGroupList }

  TXMLGroupList = class(TXMLNodeCollection, IXMLGroupList)
  protected
    { IXMLGroupList }
    function Add: IXMLGroup;
    function Insert(const Index: Integer): IXMLGroup;

    function Get_Item(Index: Integer): IXMLGroup;
  end;

{ TXMLContextgroup }

  TXMLContextgroup = class(TXMLNodeCollection, IXMLContextgroup)
  protected
    { IXMLContextgroup }
    function Get_Name: UnicodeString;
    function Get_Crc: UnicodeString;
    function Get_Purpose: UnicodeString;
    function Get_Context(Index: Integer): IXMLContext;
    procedure Set_Name(Value: UnicodeString);
    procedure Set_Crc(Value: UnicodeString);
    procedure Set_Purpose(Value: UnicodeString);
    function Add: IXMLContext;
    function Insert(const Index: Integer): IXMLContext;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLContextgroupList }

  TXMLContextgroupList = class(TXMLNodeCollection, IXMLContextgroupList)
  protected
    { IXMLContextgroupList }
    function Add: IXMLContextgroup;
    function Insert(const Index: Integer): IXMLContextgroup;

    function Get_Item(Index: Integer): IXMLContextgroup;
  end;

{ TXMLContext }

  TXMLContext = class(TXMLNode, IXMLContext)
  protected
    { IXMLContext }
    function Get_Contexttype: UnicodeString;
    function Get_Matchmandatory: UnicodeString;
    function Get_Crc: UnicodeString;
    procedure Set_Contexttype(Value: UnicodeString);
    procedure Set_Matchmandatory(Value: UnicodeString);
    procedure Set_Crc(Value: UnicodeString);
  end;

{ TXMLTransunit }

  TXMLTransunit = class(TXMLNode, IXMLTransunit)
  private
    FContextgroup: IXMLContextgroupList;
    FCountgroup: IXMLCountgroupList;
    FNote: IXMLNoteList;
    FAlttrans: IXMLAlttransList;
  protected
    { IXMLTransunit }
    function Get_Id: UnicodeString;
    function Get_Approved: UnicodeString;
    function Get_Translate: UnicodeString;
    function Get_Reformat: UnicodeString;
    function Get_Space: UnicodeString;
    function Get_Datatype: UnicodeString;
    function Get_Phasename: UnicodeString;
    function Get_Restype: UnicodeString;
    function Get_Resname: UnicodeString;
    function Get_Extradata: UnicodeString;
    function Get_Extype: UnicodeString;
    function Get_Helpid: UnicodeString;
    function Get_Menu: UnicodeString;
    function Get_Menuoption: UnicodeString;
    function Get_Menuname: UnicodeString;
    function Get_Coord: UnicodeString;
    function Get_Font: UnicodeString;
    function Get_Cssstyle: UnicodeString;
    function Get_Style: UnicodeString;
    function Get_Exstyle: UnicodeString;
    function Get_Sizeunit: UnicodeString;
    function Get_Maxwidth: UnicodeString;
    function Get_Minwidth: UnicodeString;
    function Get_Maxheight: UnicodeString;
    function Get_Minheight: UnicodeString;
    function Get_Maxbytes: UnicodeString;
    function Get_Minbytes: UnicodeString;
    function Get_Charclass: UnicodeString;
    function Get_Mergedtrans: UnicodeString;
    function Get_Source: IXMLSource;
    function Get_Segsource: IXMLSegsource;
    function Get_Target: IXMLTarget;
    function Get_Contextgroup: IXMLContextgroupList;
    function Get_Countgroup: IXMLCountgroupList;
    function Get_Note: IXMLNoteList;
    function Get_Alttrans: IXMLAlttransList;
    procedure Set_Id(Value: UnicodeString);
    procedure Set_Approved(Value: UnicodeString);
    procedure Set_Translate(Value: UnicodeString);
    procedure Set_Reformat(Value: UnicodeString);
    procedure Set_Space(Value: UnicodeString);
    procedure Set_Datatype(Value: UnicodeString);
    procedure Set_Phasename(Value: UnicodeString);
    procedure Set_Restype(Value: UnicodeString);
    procedure Set_Resname(Value: UnicodeString);
    procedure Set_Extradata(Value: UnicodeString);
    procedure Set_Extype(Value: UnicodeString);
    procedure Set_Helpid(Value: UnicodeString);
    procedure Set_Menu(Value: UnicodeString);
    procedure Set_Menuoption(Value: UnicodeString);
    procedure Set_Menuname(Value: UnicodeString);
    procedure Set_Coord(Value: UnicodeString);
    procedure Set_Font(Value: UnicodeString);
    procedure Set_Cssstyle(Value: UnicodeString);
    procedure Set_Style(Value: UnicodeString);
    procedure Set_Exstyle(Value: UnicodeString);
    procedure Set_Sizeunit(Value: UnicodeString);
    procedure Set_Maxwidth(Value: UnicodeString);
    procedure Set_Minwidth(Value: UnicodeString);
    procedure Set_Maxheight(Value: UnicodeString);
    procedure Set_Minheight(Value: UnicodeString);
    procedure Set_Maxbytes(Value: UnicodeString);
    procedure Set_Minbytes(Value: UnicodeString);
    procedure Set_Charclass(Value: UnicodeString);
    procedure Set_Mergedtrans(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTransunitList }

  TXMLTransunitList = class(TXMLNodeCollection, IXMLTransunitList)
  protected
    { IXMLTransunitList }
    function Add: IXMLTransunit;
    function Insert(const Index: Integer): IXMLTransunit;

    function Get_Item(Index: Integer): IXMLTransunit;
  end;

{ TXMLSource }

  TXMLSource = class(TXMLNode, IXMLSource)
  protected
    { IXMLSource }
    function Get_Lang: UnicodeString;
    function Get_G: IXMLG;
    function Get_Bpt: IXMLBpt;
    function Get_Ept: IXMLEpt;
    function Get_Ph: IXMLPh;
    function Get_It: IXMLIt;
    function Get_Mrk: IXMLMrk;
    function Get_X: IXMLX;
    function Get_Bx: IXMLBx;
    function Get_Ex: IXMLEx;
    procedure Set_Lang(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLG }

  TXMLG = class(TXMLNode, IXMLG)
  protected
    { IXMLG }
    function Get_Ctype: UnicodeString;
    function Get_Clone: UnicodeString;
    function Get_Id: UnicodeString;
    function Get_Xid: UnicodeString;
    function Get_Equivtext: UnicodeString;
    function Get_G: IXMLG;
    function Get_Bpt: IXMLBpt;
    function Get_Ept: IXMLEpt;
    function Get_Ph: IXMLPh;
    function Get_It: IXMLIt;
    function Get_Mrk: IXMLMrk;
    function Get_X: IXMLX;
    function Get_Bx: IXMLBx;
    function Get_Ex: IXMLEx;
    procedure Set_Ctype(Value: UnicodeString);
    procedure Set_Clone(Value: UnicodeString);
    procedure Set_Id(Value: UnicodeString);
    procedure Set_Xid(Value: UnicodeString);
    procedure Set_Equivtext(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLBpt }

  TXMLBpt = class(TXMLNodeCollection, IXMLBpt)
  protected
    { IXMLBpt }
    function Get_Rid: UnicodeString;
    function Get_Ctype: UnicodeString;
    function Get_Crc: UnicodeString;
    function Get_Id: UnicodeString;
    function Get_Xid: UnicodeString;
    function Get_Equivtext: UnicodeString;
    function Get_Sub(Index: Integer): IXMLSub;
    procedure Set_Rid(Value: UnicodeString);
    procedure Set_Ctype(Value: UnicodeString);
    procedure Set_Crc(Value: UnicodeString);
    procedure Set_Id(Value: UnicodeString);
    procedure Set_Xid(Value: UnicodeString);
    procedure Set_Equivtext(Value: UnicodeString);
    function Add: IXMLSub;
    function Insert(const Index: Integer): IXMLSub;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSub }

  TXMLSub = class(TXMLNode, IXMLSub)
  protected
    { IXMLSub }
    function Get_Datatype: UnicodeString;
    function Get_Ctype: UnicodeString;
    function Get_Xid: UnicodeString;
    function Get_G: IXMLG;
    function Get_Bpt: IXMLBpt;
    function Get_Ept: IXMLEpt;
    function Get_Ph: IXMLPh;
    function Get_It: IXMLIt;
    function Get_Mrk: IXMLMrk;
    function Get_X: IXMLX;
    function Get_Bx: IXMLBx;
    function Get_Ex: IXMLEx;
    procedure Set_Datatype(Value: UnicodeString);
    procedure Set_Ctype(Value: UnicodeString);
    procedure Set_Xid(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLEpt }

  TXMLEpt = class(TXMLNodeCollection, IXMLEpt)
  protected
    { IXMLEpt }
    function Get_Rid: UnicodeString;
    function Get_Crc: UnicodeString;
    function Get_Id: UnicodeString;
    function Get_Xid: UnicodeString;
    function Get_Equivtext: UnicodeString;
    function Get_Sub(Index: Integer): IXMLSub;
    procedure Set_Rid(Value: UnicodeString);
    procedure Set_Crc(Value: UnicodeString);
    procedure Set_Id(Value: UnicodeString);
    procedure Set_Xid(Value: UnicodeString);
    procedure Set_Equivtext(Value: UnicodeString);
    function Add: IXMLSub;
    function Insert(const Index: Integer): IXMLSub;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLPh }

  TXMLPh = class(TXMLNodeCollection, IXMLPh)
  protected
    { IXMLPh }
    function Get_Ctype: UnicodeString;
    function Get_Crc: UnicodeString;
    function Get_Assoc: UnicodeString;
    function Get_Id: UnicodeString;
    function Get_Xid: UnicodeString;
    function Get_Equivtext: UnicodeString;
    function Get_Sub(Index: Integer): IXMLSub;
    procedure Set_Ctype(Value: UnicodeString);
    procedure Set_Crc(Value: UnicodeString);
    procedure Set_Assoc(Value: UnicodeString);
    procedure Set_Id(Value: UnicodeString);
    procedure Set_Xid(Value: UnicodeString);
    procedure Set_Equivtext(Value: UnicodeString);
    function Add: IXMLSub;
    function Insert(const Index: Integer): IXMLSub;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLIt }

  TXMLIt = class(TXMLNodeCollection, IXMLIt)
  protected
    { IXMLIt }
    function Get_Pos: UnicodeString;
    function Get_Rid: UnicodeString;
    function Get_Ctype: UnicodeString;
    function Get_Crc: UnicodeString;
    function Get_Id: UnicodeString;
    function Get_Xid: UnicodeString;
    function Get_Equivtext: UnicodeString;
    function Get_Sub(Index: Integer): IXMLSub;
    procedure Set_Pos(Value: UnicodeString);
    procedure Set_Rid(Value: UnicodeString);
    procedure Set_Ctype(Value: UnicodeString);
    procedure Set_Crc(Value: UnicodeString);
    procedure Set_Id(Value: UnicodeString);
    procedure Set_Xid(Value: UnicodeString);
    procedure Set_Equivtext(Value: UnicodeString);
    function Add: IXMLSub;
    function Insert(const Index: Integer): IXMLSub;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLMrk }

  TXMLMrk = class(TXMLNode, IXMLMrk)
  protected
    { IXMLMrk }
    function Get_Mtype: UnicodeString;
    function Get_Mid: UnicodeString;
    function Get_Comment: UnicodeString;
    function Get_G: IXMLG;
    function Get_Bpt: IXMLBpt;
    function Get_Ept: IXMLEpt;
    function Get_Ph: IXMLPh;
    function Get_It: IXMLIt;
    function Get_Mrk: IXMLMrk;
    function Get_X: IXMLX;
    function Get_Bx: IXMLBx;
    function Get_Ex: IXMLEx;
    procedure Set_Mtype(Value: UnicodeString);
    procedure Set_Mid(Value: UnicodeString);
    procedure Set_Comment(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLX }

  TXMLX = class(TXMLNode, IXMLX)
  protected
    { IXMLX }
    function Get_Ctype: UnicodeString;
    function Get_Clone: UnicodeString;
    function Get_Id: UnicodeString;
    function Get_Xid: UnicodeString;
    function Get_Equivtext: UnicodeString;
    procedure Set_Ctype(Value: UnicodeString);
    procedure Set_Clone(Value: UnicodeString);
    procedure Set_Id(Value: UnicodeString);
    procedure Set_Xid(Value: UnicodeString);
    procedure Set_Equivtext(Value: UnicodeString);
  end;

{ TXMLBx }

  TXMLBx = class(TXMLNode, IXMLBx)
  protected
    { IXMLBx }
    function Get_Rid: UnicodeString;
    function Get_Ctype: UnicodeString;
    function Get_Clone: UnicodeString;
    function Get_Id: UnicodeString;
    function Get_Xid: UnicodeString;
    function Get_Equivtext: UnicodeString;
    procedure Set_Rid(Value: UnicodeString);
    procedure Set_Ctype(Value: UnicodeString);
    procedure Set_Clone(Value: UnicodeString);
    procedure Set_Id(Value: UnicodeString);
    procedure Set_Xid(Value: UnicodeString);
    procedure Set_Equivtext(Value: UnicodeString);
  end;

{ TXMLEx }

  TXMLEx = class(TXMLNode, IXMLEx)
  protected
    { IXMLEx }
    function Get_Rid: UnicodeString;
    function Get_Id: UnicodeString;
    function Get_Xid: UnicodeString;
    function Get_Equivtext: UnicodeString;
    procedure Set_Rid(Value: UnicodeString);
    procedure Set_Id(Value: UnicodeString);
    procedure Set_Xid(Value: UnicodeString);
    procedure Set_Equivtext(Value: UnicodeString);
  end;

{ TXMLSegsource }

  TXMLSegsource = class(TXMLNode, IXMLSegsource)
  protected
    { IXMLSegsource }
    function Get_Lang: UnicodeString;
    function Get_G: IXMLG;
    function Get_Bpt: IXMLBpt;
    function Get_Ept: IXMLEpt;
    function Get_Ph: IXMLPh;
    function Get_It: IXMLIt;
    function Get_Mrk: IXMLMrk;
    function Get_X: IXMLX;
    function Get_Bx: IXMLBx;
    function Get_Ex: IXMLEx;
    procedure Set_Lang(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTarget }

  TXMLTarget = class(TXMLNode, IXMLTarget)
  protected
    { IXMLTarget }
    function Get_State: UnicodeString;
    function Get_Statequalifier: UnicodeString;
    function Get_Phasename: UnicodeString;
    function Get_Lang: UnicodeString;
    function Get_Resname: UnicodeString;
    function Get_Coord: UnicodeString;
    function Get_Font: UnicodeString;
    function Get_Cssstyle: UnicodeString;
    function Get_Style: UnicodeString;
    function Get_Exstyle: UnicodeString;
    function Get_Equivtrans: UnicodeString;
    function Get_G: IXMLG;
    function Get_Bpt: IXMLBpt;
    function Get_Ept: IXMLEpt;
    function Get_Ph: IXMLPh;
    function Get_It: IXMLIt;
    function Get_Mrk: IXMLMrk;
    function Get_X: IXMLX;
    function Get_Bx: IXMLBx;
    function Get_Ex: IXMLEx;
    procedure Set_State(Value: UnicodeString);
    procedure Set_Statequalifier(Value: UnicodeString);
    procedure Set_Phasename(Value: UnicodeString);
    procedure Set_Lang(Value: UnicodeString);
    procedure Set_Resname(Value: UnicodeString);
    procedure Set_Coord(Value: UnicodeString);
    procedure Set_Font(Value: UnicodeString);
    procedure Set_Cssstyle(Value: UnicodeString);
    procedure Set_Style(Value: UnicodeString);
    procedure Set_Exstyle(Value: UnicodeString);
    procedure Set_Equivtrans(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLAlttrans }

  TXMLAlttrans = class(TXMLNode, IXMLAlttrans)
  private
    FContextgroup: IXMLContextgroupList;
    FNote: IXMLNoteList;
  protected
    { IXMLAlttrans }
    function Get_Matchquality: UnicodeString;
    function Get_Toolid: UnicodeString;
    function Get_Crc: UnicodeString;
    function Get_Lang: UnicodeString;
    function Get_Origin: UnicodeString;
    function Get_Datatype: UnicodeString;
    function Get_Space: UnicodeString;
    function Get_Restype: UnicodeString;
    function Get_Resname: UnicodeString;
    function Get_Extradata: UnicodeString;
    function Get_Extype: UnicodeString;
    function Get_Helpid: UnicodeString;
    function Get_Menu: UnicodeString;
    function Get_Menuoption: UnicodeString;
    function Get_Menuname: UnicodeString;
    function Get_Mid: UnicodeString;
    function Get_Coord: UnicodeString;
    function Get_Font: UnicodeString;
    function Get_Cssstyle: UnicodeString;
    function Get_Style: UnicodeString;
    function Get_Exstyle: UnicodeString;
    function Get_Phasename: UnicodeString;
    function Get_Alttranstype: UnicodeString;
    function Get_Source: IXMLSource;
    function Get_Segsource: IXMLSegsource;
    function Get_Target: IXMLTarget;
    function Get_Contextgroup: IXMLContextgroupList;
    function Get_Note: IXMLNoteList;
    procedure Set_Matchquality(Value: UnicodeString);
    procedure Set_Toolid(Value: UnicodeString);
    procedure Set_Crc(Value: UnicodeString);
    procedure Set_Lang(Value: UnicodeString);
    procedure Set_Origin(Value: UnicodeString);
    procedure Set_Datatype(Value: UnicodeString);
    procedure Set_Space(Value: UnicodeString);
    procedure Set_Restype(Value: UnicodeString);
    procedure Set_Resname(Value: UnicodeString);
    procedure Set_Extradata(Value: UnicodeString);
    procedure Set_Extype(Value: UnicodeString);
    procedure Set_Helpid(Value: UnicodeString);
    procedure Set_Menu(Value: UnicodeString);
    procedure Set_Menuoption(Value: UnicodeString);
    procedure Set_Menuname(Value: UnicodeString);
    procedure Set_Mid(Value: UnicodeString);
    procedure Set_Coord(Value: UnicodeString);
    procedure Set_Font(Value: UnicodeString);
    procedure Set_Cssstyle(Value: UnicodeString);
    procedure Set_Style(Value: UnicodeString);
    procedure Set_Exstyle(Value: UnicodeString);
    procedure Set_Phasename(Value: UnicodeString);
    procedure Set_Alttranstype(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLAlttransList }

  TXMLAlttransList = class(TXMLNodeCollection, IXMLAlttransList)
  protected
    { IXMLAlttransList }
    function Add: IXMLAlttrans;
    function Insert(const Index: Integer): IXMLAlttrans;

    function Get_Item(Index: Integer): IXMLAlttrans;
  end;

{ TXMLBinunit }

  TXMLBinunit = class(TXMLNode, IXMLBinunit)
  private
    FContextgroup: IXMLContextgroupList;
    FCountgroup: IXMLCountgroupList;
    FNote: IXMLNoteList;
    FTransunit: IXMLTransunitList;
  protected
    { IXMLBinunit }
    function Get_Id: UnicodeString;
    function Get_Mimetype: UnicodeString;
    function Get_Approved: UnicodeString;
    function Get_Translate: UnicodeString;
    function Get_Reformat: UnicodeString;
    function Get_Restype: UnicodeString;
    function Get_Resname: UnicodeString;
    function Get_Phasename: UnicodeString;
    function Get_Binsource: IXMLBinsource;
    function Get_Bintarget: IXMLBintarget;
    function Get_Contextgroup: IXMLContextgroupList;
    function Get_Countgroup: IXMLCountgroupList;
    function Get_Note: IXMLNoteList;
    function Get_Transunit: IXMLTransunitList;
    procedure Set_Id(Value: UnicodeString);
    procedure Set_Mimetype(Value: UnicodeString);
    procedure Set_Approved(Value: UnicodeString);
    procedure Set_Translate(Value: UnicodeString);
    procedure Set_Reformat(Value: UnicodeString);
    procedure Set_Restype(Value: UnicodeString);
    procedure Set_Resname(Value: UnicodeString);
    procedure Set_Phasename(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLBinunitList }

  TXMLBinunitList = class(TXMLNodeCollection, IXMLBinunitList)
  protected
    { IXMLBinunitList }
    function Add: IXMLBinunit;
    function Insert(const Index: Integer): IXMLBinunit;

    function Get_Item(Index: Integer): IXMLBinunit;
  end;

{ TXMLBinsource }

  TXMLBinsource = class(TXMLNode, IXMLBinsource)
  protected
    { IXMLBinsource }
    function Get_Internalfile: IXMLInternalfile;
    function Get_Externalfile: IXMLExternalfile;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLBintarget }

  TXMLBintarget = class(TXMLNode, IXMLBintarget)
  protected
    { IXMLBintarget }
    function Get_Mimetype: UnicodeString;
    function Get_State: UnicodeString;
    function Get_Statequalifier: UnicodeString;
    function Get_Phasename: UnicodeString;
    function Get_Restype: UnicodeString;
    function Get_Resname: UnicodeString;
    function Get_Internalfile: IXMLInternalfile;
    function Get_Externalfile: IXMLExternalfile;
    procedure Set_Mimetype(Value: UnicodeString);
    procedure Set_State(Value: UnicodeString);
    procedure Set_Statequalifier(Value: UnicodeString);
    procedure Set_Phasename(Value: UnicodeString);
    procedure Set_Restype(Value: UnicodeString);
    procedure Set_Resname(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ Global Functions }

function Getxliff(Doc: IXMLDocument): IXMLXliff;
function Loadxliff(const FileName: string): IXMLXliff;
function Newxliff: IXMLXliff;

const
  TargetNamespace = 'urn:oasis:names:tc:xliff:document:1.2';

implementation

uses Xml.xmlutil;

{ Global Functions }

function Getxliff(Doc: IXMLDocument): IXMLXliff;
begin
  Result := Doc.GetDocBinding('xliff', TXMLXliff, TargetNamespace) as IXMLXliff;
end;

function Loadxliff(const FileName: string): IXMLXliff;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('xliff', TXMLXliff, TargetNamespace) as IXMLXliff;
end;

function Newxliff: IXMLXliff;
begin
  Result := NewXMLDocument.GetDocBinding('xliff', TXMLXliff, TargetNamespace) as IXMLXliff;
end;

{ TXMLXliff }

procedure TXMLXliff.AfterConstruction;
begin
  RegisterChildNode('file', TXMLFile_);
  ItemTag := 'file';
  ItemInterface := IXMLFile_;
  inherited;
end;

function TXMLXliff.Get_Version: UnicodeString;
begin
  Result := AttributeNodes['version'].Text;
end;

procedure TXMLXliff.Set_Version(Value: UnicodeString);
begin
  SetAttribute('version', Value);
end;

function TXMLXliff.Get_Lang: UnicodeString;
begin
  Result := AttributeNodes['xml:lang'].Text;
end;

procedure TXMLXliff.Set_Lang(Value: UnicodeString);
begin
  SetAttribute('xml:lang', Value);
end;

function TXMLXliff.Get_File_(Index: Integer): IXMLFile_;
begin
  Result := List[Index] as IXMLFile_;
end;

function TXMLXliff.Add: IXMLFile_;
begin
  Result := AddItem(-1) as IXMLFile_;
end;

function TXMLXliff.Insert(const Index: Integer): IXMLFile_;
begin
  Result := AddItem(Index) as IXMLFile_;
end;

{ TXMLFile_ }

procedure TXMLFile_.AfterConstruction;
begin
  RegisterChildNode('header', TXMLHeader);
  RegisterChildNode('body', TXMLBody);
  inherited;
end;

function TXMLFile_.Get_Original: UnicodeString;
begin
  Result := AttributeNodes['original'].Text;
end;

procedure TXMLFile_.Set_Original(Value: UnicodeString);
begin
  SetAttribute('original', Value);
end;

function TXMLFile_.Get_Sourcelanguage: UnicodeString;
begin
  Result := AttributeNodes['source-language'].Text;
end;

procedure TXMLFile_.Set_Sourcelanguage(Value: UnicodeString);
begin
  SetAttribute('source-language', Value);
end;

function TXMLFile_.Get_Datatype: UnicodeString;
begin
  Result := AttributeNodes['datatype'].Text;
end;

procedure TXMLFile_.Set_Datatype(Value: UnicodeString);
begin
  SetAttribute('datatype', Value);
end;

function TXMLFile_.Get_Toolid: UnicodeString;
begin
  Result := AttributeNodes['tool-id'].Text;
end;

procedure TXMLFile_.Set_Toolid(Value: UnicodeString);
begin
  SetAttribute('tool-id', Value);
end;

function TXMLFile_.Get_Date: UnicodeString;
begin
  Result := AttributeNodes['date'].Text;
end;

procedure TXMLFile_.Set_Date(Value: UnicodeString);
begin
  SetAttribute('date', Value);
end;

function TXMLFile_.Get_Space: UnicodeString;
begin
  Result := AttributeNodes['xml:space'].Text;
end;

procedure TXMLFile_.Set_Space(Value: UnicodeString);
begin
  SetAttribute('xml:space', Value);
end;

function TXMLFile_.Get_Category: UnicodeString;
begin
  Result := AttributeNodes['category'].Text;
end;

procedure TXMLFile_.Set_Category(Value: UnicodeString);
begin
  SetAttribute('category', Value);
end;

function TXMLFile_.Get_Targetlanguage: UnicodeString;
begin
  Result := AttributeNodes['target-language'].Text;
end;

procedure TXMLFile_.Set_Targetlanguage(Value: UnicodeString);
begin
  SetAttribute('target-language', Value);
end;

function TXMLFile_.Get_Productname: UnicodeString;
begin
  Result := AttributeNodes['product-name'].Text;
end;

procedure TXMLFile_.Set_Productname(Value: UnicodeString);
begin
  SetAttribute('product-name', Value);
end;

function TXMLFile_.Get_Productversion: UnicodeString;
begin
  Result := AttributeNodes['product-version'].Text;
end;

procedure TXMLFile_.Set_Productversion(Value: UnicodeString);
begin
  SetAttribute('product-version', Value);
end;

function TXMLFile_.Get_Buildnum: UnicodeString;
begin
  Result := AttributeNodes['build-num'].Text;
end;

procedure TXMLFile_.Set_Buildnum(Value: UnicodeString);
begin
  SetAttribute('build-num', Value);
end;

function TXMLFile_.Get_Header: IXMLHeader;
begin
  Result := ChildNodes['header'] as IXMLHeader;
end;

function TXMLFile_.Get_Body: IXMLBody;
begin
  Result := ChildNodes['body'] as IXMLBody;
end;

{ TXMLHeader }

procedure TXMLHeader.AfterConstruction;
begin
  RegisterChildNode('skl', TXMLElemType_ExternalReference);
  RegisterChildNode('phase-group', TXMLPhasegroup);
  RegisterChildNode('glossary', TXMLElemType_ExternalReference);
  RegisterChildNode('reference', TXMLElemType_ExternalReference);
  RegisterChildNode('count-group', TXMLCountgroup);
  RegisterChildNode('note', TXMLNote);
  RegisterChildNode('tool', TXMLTool);
  FGlossary := CreateCollection(TXMLElemType_ExternalReferenceList, IXMLElemType_ExternalReference, 'glossary') as IXMLElemType_ExternalReferenceList;
  FReference := CreateCollection(TXMLElemType_ExternalReferenceList, IXMLElemType_ExternalReference, 'reference') as IXMLElemType_ExternalReferenceList;
  FCountgroup := CreateCollection(TXMLCountgroupList, IXMLCountgroup, 'count-group') as IXMLCountgroupList;
  FNote := CreateCollection(TXMLNoteList, IXMLNote, 'note') as IXMLNoteList;
  FTool := CreateCollection(TXMLToolList, IXMLTool, 'tool') as IXMLToolList;
  inherited;
end;

function TXMLHeader.Get_Skl: IXMLElemType_ExternalReference;
begin
  Result := ChildNodes['skl'] as IXMLElemType_ExternalReference;
end;

function TXMLHeader.Get_Phasegroup: IXMLPhasegroup;
begin
  Result := ChildNodes['phase-group'] as IXMLPhasegroup;
end;

function TXMLHeader.Get_Glossary: IXMLElemType_ExternalReferenceList;
begin
  Result := FGlossary;
end;

function TXMLHeader.Get_Reference: IXMLElemType_ExternalReferenceList;
begin
  Result := FReference;
end;

function TXMLHeader.Get_Countgroup: IXMLCountgroupList;
begin
  Result := FCountgroup;
end;

function TXMLHeader.Get_Note: IXMLNoteList;
begin
  Result := FNote;
end;

function TXMLHeader.Get_Tool: IXMLToolList;
begin
  Result := FTool;
end;

{ TXMLElemType_ExternalReference }

procedure TXMLElemType_ExternalReference.AfterConstruction;
begin
  RegisterChildNode('internal-file', TXMLInternalfile);
  RegisterChildNode('external-file', TXMLExternalfile);
  inherited;
end;

function TXMLElemType_ExternalReference.Get_Internalfile: IXMLInternalfile;
begin
  Result := ChildNodes['internal-file'] as IXMLInternalfile;
end;

function TXMLElemType_ExternalReference.Get_Externalfile: IXMLExternalfile;
begin
  Result := ChildNodes['external-file'] as IXMLExternalfile;
end;

{ TXMLElemType_ExternalReferenceList }

function TXMLElemType_ExternalReferenceList.Add: IXMLElemType_ExternalReference;
begin
  Result := AddItem(-1) as IXMLElemType_ExternalReference;
end;

function TXMLElemType_ExternalReferenceList.Insert(const Index: Integer): IXMLElemType_ExternalReference;
begin
  Result := AddItem(Index) as IXMLElemType_ExternalReference;
end;

function TXMLElemType_ExternalReferenceList.Get_Item(Index: Integer): IXMLElemType_ExternalReference;
begin
  Result := List[Index] as IXMLElemType_ExternalReference;
end;

{ TXMLInternalfile }

function TXMLInternalfile.Get_Form: UnicodeString;
begin
  Result := AttributeNodes['form'].Text;
end;

procedure TXMLInternalfile.Set_Form(Value: UnicodeString);
begin
  SetAttribute('form', Value);
end;

function TXMLInternalfile.Get_Crc: UnicodeString;
begin
  Result := AttributeNodes['crc'].Text;
end;

procedure TXMLInternalfile.Set_Crc(Value: UnicodeString);
begin
  SetAttribute('crc', Value);
end;

{ TXMLExternalfile }

function TXMLExternalfile.Get_Href: UnicodeString;
begin
  Result := AttributeNodes['href'].Text;
end;

procedure TXMLExternalfile.Set_Href(Value: UnicodeString);
begin
  SetAttribute('href', Value);
end;

function TXMLExternalfile.Get_Crc: UnicodeString;
begin
  Result := AttributeNodes['crc'].Text;
end;

procedure TXMLExternalfile.Set_Crc(Value: UnicodeString);
begin
  SetAttribute('crc', Value);
end;

function TXMLExternalfile.Get_Uid: UnicodeString;
begin
  Result := AttributeNodes['uid'].Text;
end;

procedure TXMLExternalfile.Set_Uid(Value: UnicodeString);
begin
  SetAttribute('uid', Value);
end;

{ TXMLPhasegroup }

procedure TXMLPhasegroup.AfterConstruction;
begin
  RegisterChildNode('phase', TXMLPhase);
  ItemTag := 'phase';
  ItemInterface := IXMLPhase;
  inherited;
end;

function TXMLPhasegroup.Get_Phase(Index: Integer): IXMLPhase;
begin
  Result := List[Index] as IXMLPhase;
end;

function TXMLPhasegroup.Add: IXMLPhase;
begin
  Result := AddItem(-1) as IXMLPhase;
end;

function TXMLPhasegroup.Insert(const Index: Integer): IXMLPhase;
begin
  Result := AddItem(Index) as IXMLPhase;
end;

{ TXMLPhase }

procedure TXMLPhase.AfterConstruction;
begin
  RegisterChildNode('note', TXMLNote);
  ItemTag := 'note';
  ItemInterface := IXMLNote;
  inherited;
end;

function TXMLPhase.Get_Phasename: UnicodeString;
begin
  Result := AttributeNodes['phase-name'].Text;
end;

procedure TXMLPhase.Set_Phasename(Value: UnicodeString);
begin
  SetAttribute('phase-name', Value);
end;

function TXMLPhase.Get_Processname: UnicodeString;
begin
  Result := AttributeNodes['process-name'].Text;
end;

procedure TXMLPhase.Set_Processname(Value: UnicodeString);
begin
  SetAttribute('process-name', Value);
end;

function TXMLPhase.Get_Companyname: UnicodeString;
begin
  Result := AttributeNodes['company-name'].Text;
end;

procedure TXMLPhase.Set_Companyname(Value: UnicodeString);
begin
  SetAttribute('company-name', Value);
end;

function TXMLPhase.Get_Toolid: UnicodeString;
begin
  Result := AttributeNodes['tool-id'].Text;
end;

procedure TXMLPhase.Set_Toolid(Value: UnicodeString);
begin
  SetAttribute('tool-id', Value);
end;

function TXMLPhase.Get_Date: UnicodeString;
begin
  Result := AttributeNodes['date'].Text;
end;

procedure TXMLPhase.Set_Date(Value: UnicodeString);
begin
  SetAttribute('date', Value);
end;

function TXMLPhase.Get_Jobid: UnicodeString;
begin
  Result := AttributeNodes['job-id'].Text;
end;

procedure TXMLPhase.Set_Jobid(Value: UnicodeString);
begin
  SetAttribute('job-id', Value);
end;

function TXMLPhase.Get_Contactname: UnicodeString;
begin
  Result := AttributeNodes['contact-name'].Text;
end;

procedure TXMLPhase.Set_Contactname(Value: UnicodeString);
begin
  SetAttribute('contact-name', Value);
end;

function TXMLPhase.Get_Contactemail: UnicodeString;
begin
  Result := AttributeNodes['contact-email'].Text;
end;

procedure TXMLPhase.Set_Contactemail(Value: UnicodeString);
begin
  SetAttribute('contact-email', Value);
end;

function TXMLPhase.Get_Contactphone: UnicodeString;
begin
  Result := AttributeNodes['contact-phone'].Text;
end;

procedure TXMLPhase.Set_Contactphone(Value: UnicodeString);
begin
  SetAttribute('contact-phone', Value);
end;

function TXMLPhase.Get_Note(Index: Integer): IXMLNote;
begin
  Result := List[Index] as IXMLNote;
end;

function TXMLPhase.Add: IXMLNote;
begin
  Result := AddItem(-1) as IXMLNote;
end;

function TXMLPhase.Insert(const Index: Integer): IXMLNote;
begin
  Result := AddItem(Index) as IXMLNote;
end;

{ TXMLNote }

function TXMLNote.Get_Lang: UnicodeString;
begin
  Result := AttributeNodes['xml:lang'].Text;
end;

procedure TXMLNote.Set_Lang(Value: UnicodeString);
begin
  SetAttribute('xml:lang', Value);
end;

function TXMLNote.Get_Priority: UnicodeString;
begin
  Result := AttributeNodes['priority'].Text;
end;

procedure TXMLNote.Set_Priority(Value: UnicodeString);
begin
  SetAttribute('priority', Value);
end;

function TXMLNote.Get_From: UnicodeString;
begin
  Result := AttributeNodes['from'].Text;
end;

procedure TXMLNote.Set_From(Value: UnicodeString);
begin
  SetAttribute('from', Value);
end;

function TXMLNote.Get_Annotates: UnicodeString;
begin
  Result := AttributeNodes['annotates'].Text;
end;

procedure TXMLNote.Set_Annotates(Value: UnicodeString);
begin
  SetAttribute('annotates', Value);
end;

{ TXMLNoteList }

function TXMLNoteList.Add: IXMLNote;
begin
  Result := AddItem(-1) as IXMLNote;
end;

function TXMLNoteList.Insert(const Index: Integer): IXMLNote;
begin
  Result := AddItem(Index) as IXMLNote;
end;

function TXMLNoteList.Get_Item(Index: Integer): IXMLNote;
begin
  Result := List[Index] as IXMLNote;
end;

{ TXMLCountgroup }

procedure TXMLCountgroup.AfterConstruction;
begin
  RegisterChildNode('count', TXMLCount);
  ItemTag := 'count';
  ItemInterface := IXMLCount;
  inherited;
end;

function TXMLCountgroup.Get_Name: UnicodeString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLCountgroup.Set_Name(Value: UnicodeString);
begin
  SetAttribute('name', Value);
end;

function TXMLCountgroup.Get_Count(Index: Integer): IXMLCount;
begin
  Result := List[Index] as IXMLCount;
end;

function TXMLCountgroup.Add: IXMLCount;
begin
  Result := AddItem(-1) as IXMLCount;
end;

function TXMLCountgroup.Insert(const Index: Integer): IXMLCount;
begin
  Result := AddItem(Index) as IXMLCount;
end;

{ TXMLCountgroupList }

function TXMLCountgroupList.Add: IXMLCountgroup;
begin
  Result := AddItem(-1) as IXMLCountgroup;
end;

function TXMLCountgroupList.Insert(const Index: Integer): IXMLCountgroup;
begin
  Result := AddItem(Index) as IXMLCountgroup;
end;

function TXMLCountgroupList.Get_Item(Index: Integer): IXMLCountgroup;
begin
  Result := List[Index] as IXMLCountgroup;
end;

{ TXMLCount }

function TXMLCount.Get_Counttype: UnicodeString;
begin
  Result := AttributeNodes['count-type'].Text;
end;

procedure TXMLCount.Set_Counttype(Value: UnicodeString);
begin
  SetAttribute('count-type', Value);
end;

function TXMLCount.Get_Phasename: UnicodeString;
begin
  Result := AttributeNodes['phase-name'].Text;
end;

procedure TXMLCount.Set_Phasename(Value: UnicodeString);
begin
  SetAttribute('phase-name', Value);
end;

function TXMLCount.Get_Unit_: UnicodeString;
begin
  Result := AttributeNodes['unit'].Text;
end;

procedure TXMLCount.Set_Unit_(Value: UnicodeString);
begin
  SetAttribute('unit', Value);
end;

{ TXMLTool }

function TXMLTool.Get_Toolid: UnicodeString;
begin
  Result := AttributeNodes['tool-id'].Text;
end;

procedure TXMLTool.Set_Toolid(Value: UnicodeString);
begin
  SetAttribute('tool-id', Value);
end;

function TXMLTool.Get_Toolname: UnicodeString;
begin
  Result := AttributeNodes['tool-name'].Text;
end;

procedure TXMLTool.Set_Toolname(Value: UnicodeString);
begin
  SetAttribute('tool-name', Value);
end;

function TXMLTool.Get_Toolversion: UnicodeString;
begin
  Result := AttributeNodes['tool-version'].Text;
end;

procedure TXMLTool.Set_Toolversion(Value: UnicodeString);
begin
  SetAttribute('tool-version', Value);
end;

function TXMLTool.Get_Toolcompany: UnicodeString;
begin
  Result := AttributeNodes['tool-company'].Text;
end;

procedure TXMLTool.Set_Toolcompany(Value: UnicodeString);
begin
  SetAttribute('tool-company', Value);
end;

{ TXMLToolList }

function TXMLToolList.Add: IXMLTool;
begin
  Result := AddItem(-1) as IXMLTool;
end;

function TXMLToolList.Insert(const Index: Integer): IXMLTool;
begin
  Result := AddItem(Index) as IXMLTool;
end;

function TXMLToolList.Get_Item(Index: Integer): IXMLTool;
begin
  Result := List[Index] as IXMLTool;
end;

{ TXMLBody }

procedure TXMLBody.AfterConstruction;
begin
  RegisterChildNode('group', TXMLGroup);
  RegisterChildNode('trans-unit', TXMLTransunit);
  RegisterChildNode('bin-unit', TXMLBinunit);
  FGroup := CreateCollection(TXMLGroupList, IXMLGroup, 'group') as IXMLGroupList;
  FTransunit := CreateCollection(TXMLTransunitList, IXMLTransunit, 'trans-unit') as IXMLTransunitList;
  FBinunit := CreateCollection(TXMLBinunitList, IXMLBinunit, 'bin-unit') as IXMLBinunitList;
  inherited;
end;

function TXMLBody.Get_Group: IXMLGroupList;
begin
  Result := FGroup;
end;

function TXMLBody.Get_Transunit: IXMLTransunitList;
begin
  Result := FTransunit;
end;

function TXMLBody.Get_Binunit: IXMLBinunitList;
begin
  Result := FBinunit;
end;

{ TXMLGroup }

procedure TXMLGroup.AfterConstruction;
begin
  RegisterChildNode('context-group', TXMLContextgroup);
  RegisterChildNode('count-group', TXMLCountgroup);
  RegisterChildNode('note', TXMLNote);
  RegisterChildNode('group', TXMLGroup);
  RegisterChildNode('trans-unit', TXMLTransunit);
  RegisterChildNode('bin-unit', TXMLBinunit);
  FContextgroup := CreateCollection(TXMLContextgroupList, IXMLContextgroup, 'context-group') as IXMLContextgroupList;
  FCountgroup := CreateCollection(TXMLCountgroupList, IXMLCountgroup, 'count-group') as IXMLCountgroupList;
  FNote := CreateCollection(TXMLNoteList, IXMLNote, 'note') as IXMLNoteList;
  FGroup := CreateCollection(TXMLGroupList, IXMLGroup, 'group') as IXMLGroupList;
  FTransunit := CreateCollection(TXMLTransunitList, IXMLTransunit, 'trans-unit') as IXMLTransunitList;
  FBinunit := CreateCollection(TXMLBinunitList, IXMLBinunit, 'bin-unit') as IXMLBinunitList;
  inherited;
end;

function TXMLGroup.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['id'].Text;
end;

procedure TXMLGroup.Set_Id(Value: UnicodeString);
begin
  SetAttribute('id', Value);
end;

function TXMLGroup.Get_Datatype: UnicodeString;
begin
  Result := AttributeNodes['datatype'].Text;
end;

procedure TXMLGroup.Set_Datatype(Value: UnicodeString);
begin
  SetAttribute('datatype', Value);
end;

function TXMLGroup.Get_Space: UnicodeString;
begin
  Result := AttributeNodes['xml:space'].Text;
end;

procedure TXMLGroup.Set_Space(Value: UnicodeString);
begin
  SetAttribute('xml:space', Value);
end;

function TXMLGroup.Get_Restype: UnicodeString;
begin
  Result := AttributeNodes['restype'].Text;
end;

procedure TXMLGroup.Set_Restype(Value: UnicodeString);
begin
  SetAttribute('restype', Value);
end;

function TXMLGroup.Get_Resname: UnicodeString;
begin
  Result := AttributeNodes['resname'].Text;
end;

procedure TXMLGroup.Set_Resname(Value: UnicodeString);
begin
  SetAttribute('resname', Value);
end;

function TXMLGroup.Get_Extradata: UnicodeString;
begin
  Result := AttributeNodes['extradata'].Text;
end;

procedure TXMLGroup.Set_Extradata(Value: UnicodeString);
begin
  SetAttribute('extradata', Value);
end;

function TXMLGroup.Get_Extype: UnicodeString;
begin
  Result := AttributeNodes['extype'].Text;
end;

procedure TXMLGroup.Set_Extype(Value: UnicodeString);
begin
  SetAttribute('extype', Value);
end;

function TXMLGroup.Get_Helpid: UnicodeString;
begin
  Result := AttributeNodes['help-id'].Text;
end;

procedure TXMLGroup.Set_Helpid(Value: UnicodeString);
begin
  SetAttribute('help-id', Value);
end;

function TXMLGroup.Get_Menu: UnicodeString;
begin
  Result := AttributeNodes['menu'].Text;
end;

procedure TXMLGroup.Set_Menu(Value: UnicodeString);
begin
  SetAttribute('menu', Value);
end;

function TXMLGroup.Get_Menuoption: UnicodeString;
begin
  Result := AttributeNodes['menu-option'].Text;
end;

procedure TXMLGroup.Set_Menuoption(Value: UnicodeString);
begin
  SetAttribute('menu-option', Value);
end;

function TXMLGroup.Get_Menuname: UnicodeString;
begin
  Result := AttributeNodes['menu-name'].Text;
end;

procedure TXMLGroup.Set_Menuname(Value: UnicodeString);
begin
  SetAttribute('menu-name', Value);
end;

function TXMLGroup.Get_Coord: UnicodeString;
begin
  Result := AttributeNodes['coord'].Text;
end;

procedure TXMLGroup.Set_Coord(Value: UnicodeString);
begin
  SetAttribute('coord', Value);
end;

function TXMLGroup.Get_Font: UnicodeString;
begin
  Result := AttributeNodes['font'].Text;
end;

procedure TXMLGroup.Set_Font(Value: UnicodeString);
begin
  SetAttribute('font', Value);
end;

function TXMLGroup.Get_Cssstyle: UnicodeString;
begin
  Result := AttributeNodes['css-style'].Text;
end;

procedure TXMLGroup.Set_Cssstyle(Value: UnicodeString);
begin
  SetAttribute('css-style', Value);
end;

function TXMLGroup.Get_Style: UnicodeString;
begin
  Result := AttributeNodes['style'].Text;
end;

procedure TXMLGroup.Set_Style(Value: UnicodeString);
begin
  SetAttribute('style', Value);
end;

function TXMLGroup.Get_Exstyle: UnicodeString;
begin
  Result := AttributeNodes['exstyle'].Text;
end;

procedure TXMLGroup.Set_Exstyle(Value: UnicodeString);
begin
  SetAttribute('exstyle', Value);
end;

function TXMLGroup.Get_Translate: UnicodeString;
begin
  Result := AttributeNodes['translate'].Text;
end;

procedure TXMLGroup.Set_Translate(Value: UnicodeString);
begin
  SetAttribute('translate', Value);
end;

function TXMLGroup.Get_Reformat: UnicodeString;
begin
  Result := AttributeNodes['reformat'].Text;
end;

procedure TXMLGroup.Set_Reformat(Value: UnicodeString);
begin
  SetAttribute('reformat', Value);
end;

function TXMLGroup.Get_Sizeunit: UnicodeString;
begin
  Result := AttributeNodes['size-unit'].Text;
end;

procedure TXMLGroup.Set_Sizeunit(Value: UnicodeString);
begin
  SetAttribute('size-unit', Value);
end;

function TXMLGroup.Get_Maxwidth: UnicodeString;
begin
  Result := AttributeNodes['maxwidth'].Text;
end;

procedure TXMLGroup.Set_Maxwidth(Value: UnicodeString);
begin
  SetAttribute('maxwidth', Value);
end;

function TXMLGroup.Get_Minwidth: UnicodeString;
begin
  Result := AttributeNodes['minwidth'].Text;
end;

procedure TXMLGroup.Set_Minwidth(Value: UnicodeString);
begin
  SetAttribute('minwidth', Value);
end;

function TXMLGroup.Get_Maxheight: UnicodeString;
begin
  Result := AttributeNodes['maxheight'].Text;
end;

procedure TXMLGroup.Set_Maxheight(Value: UnicodeString);
begin
  SetAttribute('maxheight', Value);
end;

function TXMLGroup.Get_Minheight: UnicodeString;
begin
  Result := AttributeNodes['minheight'].Text;
end;

procedure TXMLGroup.Set_Minheight(Value: UnicodeString);
begin
  SetAttribute('minheight', Value);
end;

function TXMLGroup.Get_Maxbytes: UnicodeString;
begin
  Result := AttributeNodes['maxbytes'].Text;
end;

procedure TXMLGroup.Set_Maxbytes(Value: UnicodeString);
begin
  SetAttribute('maxbytes', Value);
end;

function TXMLGroup.Get_Minbytes: UnicodeString;
begin
  Result := AttributeNodes['minbytes'].Text;
end;

procedure TXMLGroup.Set_Minbytes(Value: UnicodeString);
begin
  SetAttribute('minbytes', Value);
end;

function TXMLGroup.Get_Charclass: UnicodeString;
begin
  Result := AttributeNodes['charclass'].Text;
end;

procedure TXMLGroup.Set_Charclass(Value: UnicodeString);
begin
  SetAttribute('charclass', Value);
end;

function TXMLGroup.Get_Mergedtrans: UnicodeString;
begin
  Result := AttributeNodes['merged-trans'].Text;
end;

procedure TXMLGroup.Set_Mergedtrans(Value: UnicodeString);
begin
  SetAttribute('merged-trans', Value);
end;

function TXMLGroup.Get_Contextgroup: IXMLContextgroupList;
begin
  Result := FContextgroup;
end;

function TXMLGroup.Get_Countgroup: IXMLCountgroupList;
begin
  Result := FCountgroup;
end;

function TXMLGroup.Get_Note: IXMLNoteList;
begin
  Result := FNote;
end;

function TXMLGroup.Get_Group: IXMLGroupList;
begin
  Result := FGroup;
end;

function TXMLGroup.Get_Transunit: IXMLTransunitList;
begin
  Result := FTransunit;
end;

function TXMLGroup.Get_Binunit: IXMLBinunitList;
begin
  Result := FBinunit;
end;

{ TXMLGroupList }

function TXMLGroupList.Add: IXMLGroup;
begin
  Result := AddItem(-1) as IXMLGroup;
end;

function TXMLGroupList.Insert(const Index: Integer): IXMLGroup;
begin
  Result := AddItem(Index) as IXMLGroup;
end;

function TXMLGroupList.Get_Item(Index: Integer): IXMLGroup;
begin
  Result := List[Index] as IXMLGroup;
end;

{ TXMLContextgroup }

procedure TXMLContextgroup.AfterConstruction;
begin
  RegisterChildNode('context', TXMLContext);
  ItemTag := 'context';
  ItemInterface := IXMLContext;
  inherited;
end;

function TXMLContextgroup.Get_Name: UnicodeString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLContextgroup.Set_Name(Value: UnicodeString);
begin
  SetAttribute('name', Value);
end;

function TXMLContextgroup.Get_Crc: UnicodeString;
begin
  Result := AttributeNodes['crc'].Text;
end;

procedure TXMLContextgroup.Set_Crc(Value: UnicodeString);
begin
  SetAttribute('crc', Value);
end;

function TXMLContextgroup.Get_Purpose: UnicodeString;
begin
  Result := AttributeNodes['purpose'].Text;
end;

procedure TXMLContextgroup.Set_Purpose(Value: UnicodeString);
begin
  SetAttribute('purpose', Value);
end;

function TXMLContextgroup.Get_Context(Index: Integer): IXMLContext;
begin
  Result := List[Index] as IXMLContext;
end;

function TXMLContextgroup.Add: IXMLContext;
begin
  Result := AddItem(-1) as IXMLContext;
end;

function TXMLContextgroup.Insert(const Index: Integer): IXMLContext;
begin
  Result := AddItem(Index) as IXMLContext;
end;

{ TXMLContextgroupList }

function TXMLContextgroupList.Add: IXMLContextgroup;
begin
  Result := AddItem(-1) as IXMLContextgroup;
end;

function TXMLContextgroupList.Insert(const Index: Integer): IXMLContextgroup;
begin
  Result := AddItem(Index) as IXMLContextgroup;
end;

function TXMLContextgroupList.Get_Item(Index: Integer): IXMLContextgroup;
begin
  Result := List[Index] as IXMLContextgroup;
end;

{ TXMLContext }

function TXMLContext.Get_Contexttype: UnicodeString;
begin
  Result := AttributeNodes['context-type'].Text;
end;

procedure TXMLContext.Set_Contexttype(Value: UnicodeString);
begin
  SetAttribute('context-type', Value);
end;

function TXMLContext.Get_Matchmandatory: UnicodeString;
begin
  Result := AttributeNodes['match-mandatory'].Text;
end;

procedure TXMLContext.Set_Matchmandatory(Value: UnicodeString);
begin
  SetAttribute('match-mandatory', Value);
end;

function TXMLContext.Get_Crc: UnicodeString;
begin
  Result := AttributeNodes['crc'].Text;
end;

procedure TXMLContext.Set_Crc(Value: UnicodeString);
begin
  SetAttribute('crc', Value);
end;

{ TXMLTransunit }

procedure TXMLTransunit.AfterConstruction;
begin
  RegisterChildNode('source', TXMLSource);
  RegisterChildNode('seg-source', TXMLSegsource);
  RegisterChildNode('target', TXMLTarget);
  RegisterChildNode('context-group', TXMLContextgroup);
  RegisterChildNode('count-group', TXMLCountgroup);
  RegisterChildNode('note', TXMLNote);
  RegisterChildNode('alt-trans', TXMLAlttrans);
  FContextgroup := CreateCollection(TXMLContextgroupList, IXMLContextgroup, 'context-group') as IXMLContextgroupList;
  FCountgroup := CreateCollection(TXMLCountgroupList, IXMLCountgroup, 'count-group') as IXMLCountgroupList;
  FNote := CreateCollection(TXMLNoteList, IXMLNote, 'note') as IXMLNoteList;
  FAlttrans := CreateCollection(TXMLAlttransList, IXMLAlttrans, 'alt-trans') as IXMLAlttransList;
  inherited;
end;

function TXMLTransunit.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['id'].Text;
end;

procedure TXMLTransunit.Set_Id(Value: UnicodeString);
begin
  SetAttribute('id', Value);
end;

function TXMLTransunit.Get_Approved: UnicodeString;
begin
  Result := AttributeNodes['approved'].Text;
end;

procedure TXMLTransunit.Set_Approved(Value: UnicodeString);
begin
  SetAttribute('approved', Value);
end;

function TXMLTransunit.Get_Translate: UnicodeString;
begin
  Result := AttributeNodes['translate'].Text;
end;

procedure TXMLTransunit.Set_Translate(Value: UnicodeString);
begin
  SetAttribute('translate', Value);
end;

function TXMLTransunit.Get_Reformat: UnicodeString;
begin
  Result := AttributeNodes['reformat'].Text;
end;

procedure TXMLTransunit.Set_Reformat(Value: UnicodeString);
begin
  SetAttribute('reformat', Value);
end;

function TXMLTransunit.Get_Space: UnicodeString;
begin
  Result := AttributeNodes['xml:space'].Text;
end;

procedure TXMLTransunit.Set_Space(Value: UnicodeString);
begin
  SetAttribute('xml:space', Value);
end;

function TXMLTransunit.Get_Datatype: UnicodeString;
begin
  Result := AttributeNodes['datatype'].Text;
end;

procedure TXMLTransunit.Set_Datatype(Value: UnicodeString);
begin
  SetAttribute('datatype', Value);
end;

function TXMLTransunit.Get_Phasename: UnicodeString;
begin
  Result := AttributeNodes['phase-name'].Text;
end;

procedure TXMLTransunit.Set_Phasename(Value: UnicodeString);
begin
  SetAttribute('phase-name', Value);
end;

function TXMLTransunit.Get_Restype: UnicodeString;
begin
  Result := AttributeNodes['restype'].Text;
end;

procedure TXMLTransunit.Set_Restype(Value: UnicodeString);
begin
  SetAttribute('restype', Value);
end;

function TXMLTransunit.Get_Resname: UnicodeString;
begin
  Result := AttributeNodes['resname'].Text;
end;

procedure TXMLTransunit.Set_Resname(Value: UnicodeString);
begin
  SetAttribute('resname', Value);
end;

function TXMLTransunit.Get_Extradata: UnicodeString;
begin
  Result := AttributeNodes['extradata'].Text;
end;

procedure TXMLTransunit.Set_Extradata(Value: UnicodeString);
begin
  SetAttribute('extradata', Value);
end;

function TXMLTransunit.Get_Extype: UnicodeString;
begin
  Result := AttributeNodes['extype'].Text;
end;

procedure TXMLTransunit.Set_Extype(Value: UnicodeString);
begin
  SetAttribute('extype', Value);
end;

function TXMLTransunit.Get_Helpid: UnicodeString;
begin
  Result := AttributeNodes['help-id'].Text;
end;

procedure TXMLTransunit.Set_Helpid(Value: UnicodeString);
begin
  SetAttribute('help-id', Value);
end;

function TXMLTransunit.Get_Menu: UnicodeString;
begin
  Result := AttributeNodes['menu'].Text;
end;

procedure TXMLTransunit.Set_Menu(Value: UnicodeString);
begin
  SetAttribute('menu', Value);
end;

function TXMLTransunit.Get_Menuoption: UnicodeString;
begin
  Result := AttributeNodes['menu-option'].Text;
end;

procedure TXMLTransunit.Set_Menuoption(Value: UnicodeString);
begin
  SetAttribute('menu-option', Value);
end;

function TXMLTransunit.Get_Menuname: UnicodeString;
begin
  Result := AttributeNodes['menu-name'].Text;
end;

procedure TXMLTransunit.Set_Menuname(Value: UnicodeString);
begin
  SetAttribute('menu-name', Value);
end;

function TXMLTransunit.Get_Coord: UnicodeString;
begin
  Result := AttributeNodes['coord'].Text;
end;

procedure TXMLTransunit.Set_Coord(Value: UnicodeString);
begin
  SetAttribute('coord', Value);
end;

function TXMLTransunit.Get_Font: UnicodeString;
begin
  Result := AttributeNodes['font'].Text;
end;

procedure TXMLTransunit.Set_Font(Value: UnicodeString);
begin
  SetAttribute('font', Value);
end;

function TXMLTransunit.Get_Cssstyle: UnicodeString;
begin
  Result := AttributeNodes['css-style'].Text;
end;

procedure TXMLTransunit.Set_Cssstyle(Value: UnicodeString);
begin
  SetAttribute('css-style', Value);
end;

function TXMLTransunit.Get_Style: UnicodeString;
begin
  Result := AttributeNodes['style'].Text;
end;

procedure TXMLTransunit.Set_Style(Value: UnicodeString);
begin
  SetAttribute('style', Value);
end;

function TXMLTransunit.Get_Exstyle: UnicodeString;
begin
  Result := AttributeNodes['exstyle'].Text;
end;

procedure TXMLTransunit.Set_Exstyle(Value: UnicodeString);
begin
  SetAttribute('exstyle', Value);
end;

function TXMLTransunit.Get_Sizeunit: UnicodeString;
begin
  Result := AttributeNodes['size-unit'].Text;
end;

procedure TXMLTransunit.Set_Sizeunit(Value: UnicodeString);
begin
  SetAttribute('size-unit', Value);
end;

function TXMLTransunit.Get_Maxwidth: UnicodeString;
begin
  Result := AttributeNodes['maxwidth'].Text;
end;

procedure TXMLTransunit.Set_Maxwidth(Value: UnicodeString);
begin
  SetAttribute('maxwidth', Value);
end;

function TXMLTransunit.Get_Minwidth: UnicodeString;
begin
  Result := AttributeNodes['minwidth'].Text;
end;

procedure TXMLTransunit.Set_Minwidth(Value: UnicodeString);
begin
  SetAttribute('minwidth', Value);
end;

function TXMLTransunit.Get_Maxheight: UnicodeString;
begin
  Result := AttributeNodes['maxheight'].Text;
end;

procedure TXMLTransunit.Set_Maxheight(Value: UnicodeString);
begin
  SetAttribute('maxheight', Value);
end;

function TXMLTransunit.Get_Minheight: UnicodeString;
begin
  Result := AttributeNodes['minheight'].Text;
end;

procedure TXMLTransunit.Set_Minheight(Value: UnicodeString);
begin
  SetAttribute('minheight', Value);
end;

function TXMLTransunit.Get_Maxbytes: UnicodeString;
begin
  Result := AttributeNodes['maxbytes'].Text;
end;

procedure TXMLTransunit.Set_Maxbytes(Value: UnicodeString);
begin
  SetAttribute('maxbytes', Value);
end;

function TXMLTransunit.Get_Minbytes: UnicodeString;
begin
  Result := AttributeNodes['minbytes'].Text;
end;

procedure TXMLTransunit.Set_Minbytes(Value: UnicodeString);
begin
  SetAttribute('minbytes', Value);
end;

function TXMLTransunit.Get_Charclass: UnicodeString;
begin
  Result := AttributeNodes['charclass'].Text;
end;

procedure TXMLTransunit.Set_Charclass(Value: UnicodeString);
begin
  SetAttribute('charclass', Value);
end;

function TXMLTransunit.Get_Mergedtrans: UnicodeString;
begin
  Result := AttributeNodes['merged-trans'].Text;
end;

procedure TXMLTransunit.Set_Mergedtrans(Value: UnicodeString);
begin
  SetAttribute('merged-trans', Value);
end;

function TXMLTransunit.Get_Source: IXMLSource;
begin
  Result := ChildNodes['source'] as IXMLSource;
end;

function TXMLTransunit.Get_Segsource: IXMLSegsource;
begin
  Result := ChildNodes['seg-source'] as IXMLSegsource;
end;

function TXMLTransunit.Get_Target: IXMLTarget;
begin
  Result := ChildNodes['target'] as IXMLTarget;
end;

function TXMLTransunit.Get_Contextgroup: IXMLContextgroupList;
begin
  Result := FContextgroup;
end;

function TXMLTransunit.Get_Countgroup: IXMLCountgroupList;
begin
  Result := FCountgroup;
end;

function TXMLTransunit.Get_Note: IXMLNoteList;
begin
  Result := FNote;
end;

function TXMLTransunit.Get_Alttrans: IXMLAlttransList;
begin
  Result := FAlttrans;
end;

{ TXMLTransunitList }

function TXMLTransunitList.Add: IXMLTransunit;
begin
  Result := AddItem(-1) as IXMLTransunit;
end;

function TXMLTransunitList.Insert(const Index: Integer): IXMLTransunit;
begin
  Result := AddItem(Index) as IXMLTransunit;
end;

function TXMLTransunitList.Get_Item(Index: Integer): IXMLTransunit;
begin
  Result := List[Index] as IXMLTransunit;
end;

{ TXMLSource }

procedure TXMLSource.AfterConstruction;
begin
  RegisterChildNode('g', TXMLG);
  RegisterChildNode('bpt', TXMLBpt);
  RegisterChildNode('ept', TXMLEpt);
  RegisterChildNode('ph', TXMLPh);
  RegisterChildNode('it', TXMLIt);
  RegisterChildNode('mrk', TXMLMrk);
  RegisterChildNode('x', TXMLX);
  RegisterChildNode('bx', TXMLBx);
  RegisterChildNode('ex', TXMLEx);
  inherited;
end;

function TXMLSource.Get_Lang: UnicodeString;
begin
  Result := AttributeNodes['xml:lang'].Text;
end;

procedure TXMLSource.Set_Lang(Value: UnicodeString);
begin
  SetAttribute('xml:lang', Value);
end;

function TXMLSource.Get_G: IXMLG;
begin
  Result := ChildNodes[WideString('g')] as IXMLG;
end;

function TXMLSource.Get_Bpt: IXMLBpt;
begin
  Result := ChildNodes['bpt'] as IXMLBpt;
end;

function TXMLSource.Get_Ept: IXMLEpt;
begin
  Result := ChildNodes['ept'] as IXMLEpt;
end;

function TXMLSource.Get_Ph: IXMLPh;
begin
  Result := ChildNodes['ph'] as IXMLPh;
end;

function TXMLSource.Get_It: IXMLIt;
begin
  Result := ChildNodes['it'] as IXMLIt;
end;

function TXMLSource.Get_Mrk: IXMLMrk;
begin
  Result := ChildNodes['mrk'] as IXMLMrk;
end;

function TXMLSource.Get_X: IXMLX;
begin
  Result := ChildNodes[WideString('x')] as IXMLX;
end;

function TXMLSource.Get_Bx: IXMLBx;
begin
  Result := ChildNodes['bx'] as IXMLBx;
end;

function TXMLSource.Get_Ex: IXMLEx;
begin
  Result := ChildNodes['ex'] as IXMLEx;
end;

{ TXMLG }

procedure TXMLG.AfterConstruction;
begin
  RegisterChildNode('g', TXMLG);
  RegisterChildNode('bpt', TXMLBpt);
  RegisterChildNode('ept', TXMLEpt);
  RegisterChildNode('ph', TXMLPh);
  RegisterChildNode('it', TXMLIt);
  RegisterChildNode('mrk', TXMLMrk);
  RegisterChildNode('x', TXMLX);
  RegisterChildNode('bx', TXMLBx);
  RegisterChildNode('ex', TXMLEx);
  inherited;
end;

function TXMLG.Get_Ctype: UnicodeString;
begin
  Result := AttributeNodes['ctype'].Text;
end;

procedure TXMLG.Set_Ctype(Value: UnicodeString);
begin
  SetAttribute('ctype', Value);
end;

function TXMLG.Get_Clone: UnicodeString;
begin
  Result := AttributeNodes['clone'].Text;
end;

procedure TXMLG.Set_Clone(Value: UnicodeString);
begin
  SetAttribute('clone', Value);
end;

function TXMLG.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['id'].Text;
end;

procedure TXMLG.Set_Id(Value: UnicodeString);
begin
  SetAttribute('id', Value);
end;

function TXMLG.Get_Xid: UnicodeString;
begin
  Result := AttributeNodes['xid'].Text;
end;

procedure TXMLG.Set_Xid(Value: UnicodeString);
begin
  SetAttribute('xid', Value);
end;

function TXMLG.Get_Equivtext: UnicodeString;
begin
  Result := AttributeNodes['equiv-text'].Text;
end;

procedure TXMLG.Set_Equivtext(Value: UnicodeString);
begin
  SetAttribute('equiv-text', Value);
end;

function TXMLG.Get_G: IXMLG;
begin
  Result := ChildNodes[WideString('g')] as IXMLG;
end;

function TXMLG.Get_Bpt: IXMLBpt;
begin
  Result := ChildNodes['bpt'] as IXMLBpt;
end;

function TXMLG.Get_Ept: IXMLEpt;
begin
  Result := ChildNodes['ept'] as IXMLEpt;
end;

function TXMLG.Get_Ph: IXMLPh;
begin
  Result := ChildNodes['ph'] as IXMLPh;
end;

function TXMLG.Get_It: IXMLIt;
begin
  Result := ChildNodes['it'] as IXMLIt;
end;

function TXMLG.Get_Mrk: IXMLMrk;
begin
  Result := ChildNodes['mrk'] as IXMLMrk;
end;

function TXMLG.Get_X: IXMLX;
begin
  Result := ChildNodes[WideString('x')] as IXMLX;
end;

function TXMLG.Get_Bx: IXMLBx;
begin
  Result := ChildNodes['bx'] as IXMLBx;
end;

function TXMLG.Get_Ex: IXMLEx;
begin
  Result := ChildNodes['ex'] as IXMLEx;
end;

{ TXMLBpt }

procedure TXMLBpt.AfterConstruction;
begin
  RegisterChildNode('sub', TXMLSub);
  ItemTag := 'sub';
  ItemInterface := IXMLSub;
  inherited;
end;

function TXMLBpt.Get_Rid: UnicodeString;
begin
  Result := AttributeNodes['rid'].Text;
end;

procedure TXMLBpt.Set_Rid(Value: UnicodeString);
begin
  SetAttribute('rid', Value);
end;

function TXMLBpt.Get_Ctype: UnicodeString;
begin
  Result := AttributeNodes['ctype'].Text;
end;

procedure TXMLBpt.Set_Ctype(Value: UnicodeString);
begin
  SetAttribute('ctype', Value);
end;

function TXMLBpt.Get_Crc: UnicodeString;
begin
  Result := AttributeNodes['crc'].Text;
end;

procedure TXMLBpt.Set_Crc(Value: UnicodeString);
begin
  SetAttribute('crc', Value);
end;

function TXMLBpt.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['id'].Text;
end;

procedure TXMLBpt.Set_Id(Value: UnicodeString);
begin
  SetAttribute('id', Value);
end;

function TXMLBpt.Get_Xid: UnicodeString;
begin
  Result := AttributeNodes['xid'].Text;
end;

procedure TXMLBpt.Set_Xid(Value: UnicodeString);
begin
  SetAttribute('xid', Value);
end;

function TXMLBpt.Get_Equivtext: UnicodeString;
begin
  Result := AttributeNodes['equiv-text'].Text;
end;

procedure TXMLBpt.Set_Equivtext(Value: UnicodeString);
begin
  SetAttribute('equiv-text', Value);
end;

function TXMLBpt.Get_Sub(Index: Integer): IXMLSub;
begin
  Result := List[Index] as IXMLSub;
end;

function TXMLBpt.Add: IXMLSub;
begin
  Result := AddItem(-1) as IXMLSub;
end;

function TXMLBpt.Insert(const Index: Integer): IXMLSub;
begin
  Result := AddItem(Index) as IXMLSub;
end;

{ TXMLSub }

procedure TXMLSub.AfterConstruction;
begin
  RegisterChildNode('g', TXMLG);
  RegisterChildNode('bpt', TXMLBpt);
  RegisterChildNode('ept', TXMLEpt);
  RegisterChildNode('ph', TXMLPh);
  RegisterChildNode('it', TXMLIt);
  RegisterChildNode('mrk', TXMLMrk);
  RegisterChildNode('x', TXMLX);
  RegisterChildNode('bx', TXMLBx);
  RegisterChildNode('ex', TXMLEx);
  inherited;
end;

function TXMLSub.Get_Datatype: UnicodeString;
begin
  Result := AttributeNodes['datatype'].Text;
end;

procedure TXMLSub.Set_Datatype(Value: UnicodeString);
begin
  SetAttribute('datatype', Value);
end;

function TXMLSub.Get_Ctype: UnicodeString;
begin
  Result := AttributeNodes['ctype'].Text;
end;

procedure TXMLSub.Set_Ctype(Value: UnicodeString);
begin
  SetAttribute('ctype', Value);
end;

function TXMLSub.Get_Xid: UnicodeString;
begin
  Result := AttributeNodes['xid'].Text;
end;

procedure TXMLSub.Set_Xid(Value: UnicodeString);
begin
  SetAttribute('xid', Value);
end;

function TXMLSub.Get_G: IXMLG;
begin
  Result := ChildNodes[WideString('g')] as IXMLG;
end;

function TXMLSub.Get_Bpt: IXMLBpt;
begin
  Result := ChildNodes['bpt'] as IXMLBpt;
end;

function TXMLSub.Get_Ept: IXMLEpt;
begin
  Result := ChildNodes['ept'] as IXMLEpt;
end;

function TXMLSub.Get_Ph: IXMLPh;
begin
  Result := ChildNodes['ph'] as IXMLPh;
end;

function TXMLSub.Get_It: IXMLIt;
begin
  Result := ChildNodes['it'] as IXMLIt;
end;

function TXMLSub.Get_Mrk: IXMLMrk;
begin
  Result := ChildNodes['mrk'] as IXMLMrk;
end;

function TXMLSub.Get_X: IXMLX;
begin
  Result := ChildNodes[WideString('x')] as IXMLX;
end;

function TXMLSub.Get_Bx: IXMLBx;
begin
  Result := ChildNodes['bx'] as IXMLBx;
end;

function TXMLSub.Get_Ex: IXMLEx;
begin
  Result := ChildNodes['ex'] as IXMLEx;
end;

{ TXMLEpt }

procedure TXMLEpt.AfterConstruction;
begin
  RegisterChildNode('sub', TXMLSub);
  ItemTag := 'sub';
  ItemInterface := IXMLSub;
  inherited;
end;

function TXMLEpt.Get_Rid: UnicodeString;
begin
  Result := AttributeNodes['rid'].Text;
end;

procedure TXMLEpt.Set_Rid(Value: UnicodeString);
begin
  SetAttribute('rid', Value);
end;

function TXMLEpt.Get_Crc: UnicodeString;
begin
  Result := AttributeNodes['crc'].Text;
end;

procedure TXMLEpt.Set_Crc(Value: UnicodeString);
begin
  SetAttribute('crc', Value);
end;

function TXMLEpt.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['id'].Text;
end;

procedure TXMLEpt.Set_Id(Value: UnicodeString);
begin
  SetAttribute('id', Value);
end;

function TXMLEpt.Get_Xid: UnicodeString;
begin
  Result := AttributeNodes['xid'].Text;
end;

procedure TXMLEpt.Set_Xid(Value: UnicodeString);
begin
  SetAttribute('xid', Value);
end;

function TXMLEpt.Get_Equivtext: UnicodeString;
begin
  Result := AttributeNodes['equiv-text'].Text;
end;

procedure TXMLEpt.Set_Equivtext(Value: UnicodeString);
begin
  SetAttribute('equiv-text', Value);
end;

function TXMLEpt.Get_Sub(Index: Integer): IXMLSub;
begin
  Result := List[Index] as IXMLSub;
end;

function TXMLEpt.Add: IXMLSub;
begin
  Result := AddItem(-1) as IXMLSub;
end;

function TXMLEpt.Insert(const Index: Integer): IXMLSub;
begin
  Result := AddItem(Index) as IXMLSub;
end;

{ TXMLPh }

procedure TXMLPh.AfterConstruction;
begin
  RegisterChildNode('sub', TXMLSub);
  ItemTag := 'sub';
  ItemInterface := IXMLSub;
  inherited;
end;

function TXMLPh.Get_Ctype: UnicodeString;
begin
  Result := AttributeNodes['ctype'].Text;
end;

procedure TXMLPh.Set_Ctype(Value: UnicodeString);
begin
  SetAttribute('ctype', Value);
end;

function TXMLPh.Get_Crc: UnicodeString;
begin
  Result := AttributeNodes['crc'].Text;
end;

procedure TXMLPh.Set_Crc(Value: UnicodeString);
begin
  SetAttribute('crc', Value);
end;

function TXMLPh.Get_Assoc: UnicodeString;
begin
  Result := AttributeNodes['assoc'].Text;
end;

procedure TXMLPh.Set_Assoc(Value: UnicodeString);
begin
  SetAttribute('assoc', Value);
end;

function TXMLPh.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['id'].Text;
end;

procedure TXMLPh.Set_Id(Value: UnicodeString);
begin
  SetAttribute('id', Value);
end;

function TXMLPh.Get_Xid: UnicodeString;
begin
  Result := AttributeNodes['xid'].Text;
end;

procedure TXMLPh.Set_Xid(Value: UnicodeString);
begin
  SetAttribute('xid', Value);
end;

function TXMLPh.Get_Equivtext: UnicodeString;
begin
  Result := AttributeNodes['equiv-text'].Text;
end;

procedure TXMLPh.Set_Equivtext(Value: UnicodeString);
begin
  SetAttribute('equiv-text', Value);
end;

function TXMLPh.Get_Sub(Index: Integer): IXMLSub;
begin
  Result := List[Index] as IXMLSub;
end;

function TXMLPh.Add: IXMLSub;
begin
  Result := AddItem(-1) as IXMLSub;
end;

function TXMLPh.Insert(const Index: Integer): IXMLSub;
begin
  Result := AddItem(Index) as IXMLSub;
end;

{ TXMLIt }

procedure TXMLIt.AfterConstruction;
begin
  RegisterChildNode('sub', TXMLSub);
  ItemTag := 'sub';
  ItemInterface := IXMLSub;
  inherited;
end;

function TXMLIt.Get_Pos: UnicodeString;
begin
  Result := AttributeNodes['pos'].Text;
end;

procedure TXMLIt.Set_Pos(Value: UnicodeString);
begin
  SetAttribute('pos', Value);
end;

function TXMLIt.Get_Rid: UnicodeString;
begin
  Result := AttributeNodes['rid'].Text;
end;

procedure TXMLIt.Set_Rid(Value: UnicodeString);
begin
  SetAttribute('rid', Value);
end;

function TXMLIt.Get_Ctype: UnicodeString;
begin
  Result := AttributeNodes['ctype'].Text;
end;

procedure TXMLIt.Set_Ctype(Value: UnicodeString);
begin
  SetAttribute('ctype', Value);
end;

function TXMLIt.Get_Crc: UnicodeString;
begin
  Result := AttributeNodes['crc'].Text;
end;

procedure TXMLIt.Set_Crc(Value: UnicodeString);
begin
  SetAttribute('crc', Value);
end;

function TXMLIt.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['id'].Text;
end;

procedure TXMLIt.Set_Id(Value: UnicodeString);
begin
  SetAttribute('id', Value);
end;

function TXMLIt.Get_Xid: UnicodeString;
begin
  Result := AttributeNodes['xid'].Text;
end;

procedure TXMLIt.Set_Xid(Value: UnicodeString);
begin
  SetAttribute('xid', Value);
end;

function TXMLIt.Get_Equivtext: UnicodeString;
begin
  Result := AttributeNodes['equiv-text'].Text;
end;

procedure TXMLIt.Set_Equivtext(Value: UnicodeString);
begin
  SetAttribute('equiv-text', Value);
end;

function TXMLIt.Get_Sub(Index: Integer): IXMLSub;
begin
  Result := List[Index] as IXMLSub;
end;

function TXMLIt.Add: IXMLSub;
begin
  Result := AddItem(-1) as IXMLSub;
end;

function TXMLIt.Insert(const Index: Integer): IXMLSub;
begin
  Result := AddItem(Index) as IXMLSub;
end;

{ TXMLMrk }

procedure TXMLMrk.AfterConstruction;
begin
  RegisterChildNode('g', TXMLG);
  RegisterChildNode('bpt', TXMLBpt);
  RegisterChildNode('ept', TXMLEpt);
  RegisterChildNode('ph', TXMLPh);
  RegisterChildNode('it', TXMLIt);
  RegisterChildNode('mrk', TXMLMrk);
  RegisterChildNode('x', TXMLX);
  RegisterChildNode('bx', TXMLBx);
  RegisterChildNode('ex', TXMLEx);
  inherited;
end;

function TXMLMrk.Get_Mtype: UnicodeString;
begin
  Result := AttributeNodes['mtype'].Text;
end;

procedure TXMLMrk.Set_Mtype(Value: UnicodeString);
begin
  SetAttribute('mtype', Value);
end;

function TXMLMrk.Get_Mid: UnicodeString;
begin
  Result := AttributeNodes['mid'].Text;
end;

procedure TXMLMrk.Set_Mid(Value: UnicodeString);
begin
  SetAttribute('mid', Value);
end;

function TXMLMrk.Get_Comment: UnicodeString;
begin
  Result := AttributeNodes['comment'].Text;
end;

procedure TXMLMrk.Set_Comment(Value: UnicodeString);
begin
  SetAttribute('comment', Value);
end;

function TXMLMrk.Get_G: IXMLG;
begin
  Result := ChildNodes[WideString('g')] as IXMLG;
end;

function TXMLMrk.Get_Bpt: IXMLBpt;
begin
  Result := ChildNodes['bpt'] as IXMLBpt;
end;

function TXMLMrk.Get_Ept: IXMLEpt;
begin
  Result := ChildNodes['ept'] as IXMLEpt;
end;

function TXMLMrk.Get_Ph: IXMLPh;
begin
  Result := ChildNodes['ph'] as IXMLPh;
end;

function TXMLMrk.Get_It: IXMLIt;
begin
  Result := ChildNodes['it'] as IXMLIt;
end;

function TXMLMrk.Get_Mrk: IXMLMrk;
begin
  Result := ChildNodes['mrk'] as IXMLMrk;
end;

function TXMLMrk.Get_X: IXMLX;
begin
  Result := ChildNodes[WideString('x')] as IXMLX;
end;

function TXMLMrk.Get_Bx: IXMLBx;
begin
  Result := ChildNodes['bx'] as IXMLBx;
end;

function TXMLMrk.Get_Ex: IXMLEx;
begin
  Result := ChildNodes['ex'] as IXMLEx;
end;

{ TXMLX }

function TXMLX.Get_Ctype: UnicodeString;
begin
  Result := AttributeNodes['ctype'].Text;
end;

procedure TXMLX.Set_Ctype(Value: UnicodeString);
begin
  SetAttribute('ctype', Value);
end;

function TXMLX.Get_Clone: UnicodeString;
begin
  Result := AttributeNodes['clone'].Text;
end;

procedure TXMLX.Set_Clone(Value: UnicodeString);
begin
  SetAttribute('clone', Value);
end;

function TXMLX.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['id'].Text;
end;

procedure TXMLX.Set_Id(Value: UnicodeString);
begin
  SetAttribute('id', Value);
end;

function TXMLX.Get_Xid: UnicodeString;
begin
  Result := AttributeNodes['xid'].Text;
end;

procedure TXMLX.Set_Xid(Value: UnicodeString);
begin
  SetAttribute('xid', Value);
end;

function TXMLX.Get_Equivtext: UnicodeString;
begin
  Result := AttributeNodes['equiv-text'].Text;
end;

procedure TXMLX.Set_Equivtext(Value: UnicodeString);
begin
  SetAttribute('equiv-text', Value);
end;

{ TXMLBx }

function TXMLBx.Get_Rid: UnicodeString;
begin
  Result := AttributeNodes['rid'].Text;
end;

procedure TXMLBx.Set_Rid(Value: UnicodeString);
begin
  SetAttribute('rid', Value);
end;

function TXMLBx.Get_Ctype: UnicodeString;
begin
  Result := AttributeNodes['ctype'].Text;
end;

procedure TXMLBx.Set_Ctype(Value: UnicodeString);
begin
  SetAttribute('ctype', Value);
end;

function TXMLBx.Get_Clone: UnicodeString;
begin
  Result := AttributeNodes['clone'].Text;
end;

procedure TXMLBx.Set_Clone(Value: UnicodeString);
begin
  SetAttribute('clone', Value);
end;

function TXMLBx.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['id'].Text;
end;

procedure TXMLBx.Set_Id(Value: UnicodeString);
begin
  SetAttribute('id', Value);
end;

function TXMLBx.Get_Xid: UnicodeString;
begin
  Result := AttributeNodes['xid'].Text;
end;

procedure TXMLBx.Set_Xid(Value: UnicodeString);
begin
  SetAttribute('xid', Value);
end;

function TXMLBx.Get_Equivtext: UnicodeString;
begin
  Result := AttributeNodes['equiv-text'].Text;
end;

procedure TXMLBx.Set_Equivtext(Value: UnicodeString);
begin
  SetAttribute('equiv-text', Value);
end;

{ TXMLEx }

function TXMLEx.Get_Rid: UnicodeString;
begin
  Result := AttributeNodes['rid'].Text;
end;

procedure TXMLEx.Set_Rid(Value: UnicodeString);
begin
  SetAttribute('rid', Value);
end;

function TXMLEx.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['id'].Text;
end;

procedure TXMLEx.Set_Id(Value: UnicodeString);
begin
  SetAttribute('id', Value);
end;

function TXMLEx.Get_Xid: UnicodeString;
begin
  Result := AttributeNodes['xid'].Text;
end;

procedure TXMLEx.Set_Xid(Value: UnicodeString);
begin
  SetAttribute('xid', Value);
end;

function TXMLEx.Get_Equivtext: UnicodeString;
begin
  Result := AttributeNodes['equiv-text'].Text;
end;

procedure TXMLEx.Set_Equivtext(Value: UnicodeString);
begin
  SetAttribute('equiv-text', Value);
end;

{ TXMLSegsource }

procedure TXMLSegsource.AfterConstruction;
begin
  RegisterChildNode('g', TXMLG);
  RegisterChildNode('bpt', TXMLBpt);
  RegisterChildNode('ept', TXMLEpt);
  RegisterChildNode('ph', TXMLPh);
  RegisterChildNode('it', TXMLIt);
  RegisterChildNode('mrk', TXMLMrk);
  RegisterChildNode('x', TXMLX);
  RegisterChildNode('bx', TXMLBx);
  RegisterChildNode('ex', TXMLEx);
  inherited;
end;

function TXMLSegsource.Get_Lang: UnicodeString;
begin
  Result := AttributeNodes['xml:lang'].Text;
end;

procedure TXMLSegsource.Set_Lang(Value: UnicodeString);
begin
  SetAttribute('xml:lang', Value);
end;

function TXMLSegsource.Get_G: IXMLG;
begin
  Result := ChildNodes[WideString('g')] as IXMLG;
end;

function TXMLSegsource.Get_Bpt: IXMLBpt;
begin
  Result := ChildNodes['bpt'] as IXMLBpt;
end;

function TXMLSegsource.Get_Ept: IXMLEpt;
begin
  Result := ChildNodes['ept'] as IXMLEpt;
end;

function TXMLSegsource.Get_Ph: IXMLPh;
begin
  Result := ChildNodes['ph'] as IXMLPh;
end;

function TXMLSegsource.Get_It: IXMLIt;
begin
  Result := ChildNodes['it'] as IXMLIt;
end;

function TXMLSegsource.Get_Mrk: IXMLMrk;
begin
  Result := ChildNodes['mrk'] as IXMLMrk;
end;

function TXMLSegsource.Get_X: IXMLX;
begin
  Result := ChildNodes[WideString('x')] as IXMLX;
end;

function TXMLSegsource.Get_Bx: IXMLBx;
begin
  Result := ChildNodes['bx'] as IXMLBx;
end;

function TXMLSegsource.Get_Ex: IXMLEx;
begin
  Result := ChildNodes['ex'] as IXMLEx;
end;

{ TXMLTarget }

procedure TXMLTarget.AfterConstruction;
begin
  RegisterChildNode('g', TXMLG);
  RegisterChildNode('bpt', TXMLBpt);
  RegisterChildNode('ept', TXMLEpt);
  RegisterChildNode('ph', TXMLPh);
  RegisterChildNode('it', TXMLIt);
  RegisterChildNode('mrk', TXMLMrk);
  RegisterChildNode('x', TXMLX);
  RegisterChildNode('bx', TXMLBx);
  RegisterChildNode('ex', TXMLEx);
  inherited;
end;

function TXMLTarget.Get_State: UnicodeString;
begin
  Result := AttributeNodes['state'].Text;
end;

procedure TXMLTarget.Set_State(Value: UnicodeString);
begin
  SetAttribute('state', Value);
end;

function TXMLTarget.Get_Statequalifier: UnicodeString;
begin
  Result := AttributeNodes['state-qualifier'].Text;
end;

procedure TXMLTarget.Set_Statequalifier(Value: UnicodeString);
begin
  SetAttribute('state-qualifier', Value);
end;

function TXMLTarget.Get_Phasename: UnicodeString;
begin
  Result := AttributeNodes['phase-name'].Text;
end;

procedure TXMLTarget.Set_Phasename(Value: UnicodeString);
begin
  SetAttribute('phase-name', Value);
end;

function TXMLTarget.Get_Lang: UnicodeString;
begin
  Result := AttributeNodes['xml:lang'].Text;
end;

procedure TXMLTarget.Set_Lang(Value: UnicodeString);
begin
  SetAttribute('xml:lang', Value);
end;

function TXMLTarget.Get_Resname: UnicodeString;
begin
  Result := AttributeNodes['resname'].Text;
end;

procedure TXMLTarget.Set_Resname(Value: UnicodeString);
begin
  SetAttribute('resname', Value);
end;

function TXMLTarget.Get_Coord: UnicodeString;
begin
  Result := AttributeNodes['coord'].Text;
end;

procedure TXMLTarget.Set_Coord(Value: UnicodeString);
begin
  SetAttribute('coord', Value);
end;

function TXMLTarget.Get_Font: UnicodeString;
begin
  Result := AttributeNodes['font'].Text;
end;

procedure TXMLTarget.Set_Font(Value: UnicodeString);
begin
  SetAttribute('font', Value);
end;

function TXMLTarget.Get_Cssstyle: UnicodeString;
begin
  Result := AttributeNodes['css-style'].Text;
end;

procedure TXMLTarget.Set_Cssstyle(Value: UnicodeString);
begin
  SetAttribute('css-style', Value);
end;

function TXMLTarget.Get_Style: UnicodeString;
begin
  Result := AttributeNodes['style'].Text;
end;

procedure TXMLTarget.Set_Style(Value: UnicodeString);
begin
  SetAttribute('style', Value);
end;

function TXMLTarget.Get_Exstyle: UnicodeString;
begin
  Result := AttributeNodes['exstyle'].Text;
end;

procedure TXMLTarget.Set_Exstyle(Value: UnicodeString);
begin
  SetAttribute('exstyle', Value);
end;

function TXMLTarget.Get_Equivtrans: UnicodeString;
begin
  Result := AttributeNodes['equiv-trans'].Text;
end;

procedure TXMLTarget.Set_Equivtrans(Value: UnicodeString);
begin
  SetAttribute('equiv-trans', Value);
end;

function TXMLTarget.Get_G: IXMLG;
begin
  Result := ChildNodes[WideString('g')] as IXMLG;
end;

function TXMLTarget.Get_Bpt: IXMLBpt;
begin
  Result := ChildNodes['bpt'] as IXMLBpt;
end;

function TXMLTarget.Get_Ept: IXMLEpt;
begin
  Result := ChildNodes['ept'] as IXMLEpt;
end;

function TXMLTarget.Get_Ph: IXMLPh;
begin
  Result := ChildNodes['ph'] as IXMLPh;
end;

function TXMLTarget.Get_It: IXMLIt;
begin
  Result := ChildNodes['it'] as IXMLIt;
end;

function TXMLTarget.Get_Mrk: IXMLMrk;
begin
  Result := ChildNodes['mrk'] as IXMLMrk;
end;

function TXMLTarget.Get_X: IXMLX;
begin
  Result := ChildNodes[WideString('x')] as IXMLX;
end;

function TXMLTarget.Get_Bx: IXMLBx;
begin
  Result := ChildNodes['bx'] as IXMLBx;
end;

function TXMLTarget.Get_Ex: IXMLEx;
begin
  Result := ChildNodes['ex'] as IXMLEx;
end;

{ TXMLAlttrans }

procedure TXMLAlttrans.AfterConstruction;
begin
  RegisterChildNode('source', TXMLSource);
  RegisterChildNode('seg-source', TXMLSegsource);
  RegisterChildNode('target', TXMLTarget);
  RegisterChildNode('context-group', TXMLContextgroup);
  RegisterChildNode('note', TXMLNote);
  FContextgroup := CreateCollection(TXMLContextgroupList, IXMLContextgroup, 'context-group') as IXMLContextgroupList;
  FNote := CreateCollection(TXMLNoteList, IXMLNote, 'note') as IXMLNoteList;
  inherited;
end;

function TXMLAlttrans.Get_Matchquality: UnicodeString;
begin
  Result := AttributeNodes['match-quality'].Text;
end;

procedure TXMLAlttrans.Set_Matchquality(Value: UnicodeString);
begin
  SetAttribute('match-quality', Value);
end;

function TXMLAlttrans.Get_Toolid: UnicodeString;
begin
  Result := AttributeNodes['tool-id'].Text;
end;

procedure TXMLAlttrans.Set_Toolid(Value: UnicodeString);
begin
  SetAttribute('tool-id', Value);
end;

function TXMLAlttrans.Get_Crc: UnicodeString;
begin
  Result := AttributeNodes['crc'].Text;
end;

procedure TXMLAlttrans.Set_Crc(Value: UnicodeString);
begin
  SetAttribute('crc', Value);
end;

function TXMLAlttrans.Get_Lang: UnicodeString;
begin
  Result := AttributeNodes['xml:lang'].Text;
end;

procedure TXMLAlttrans.Set_Lang(Value: UnicodeString);
begin
  SetAttribute('xml:lang', Value);
end;

function TXMLAlttrans.Get_Origin: UnicodeString;
begin
  Result := AttributeNodes['origin'].Text;
end;

procedure TXMLAlttrans.Set_Origin(Value: UnicodeString);
begin
  SetAttribute('origin', Value);
end;

function TXMLAlttrans.Get_Datatype: UnicodeString;
begin
  Result := AttributeNodes['datatype'].Text;
end;

procedure TXMLAlttrans.Set_Datatype(Value: UnicodeString);
begin
  SetAttribute('datatype', Value);
end;

function TXMLAlttrans.Get_Space: UnicodeString;
begin
  Result := AttributeNodes['xml:space'].Text;
end;

procedure TXMLAlttrans.Set_Space(Value: UnicodeString);
begin
  SetAttribute('xml:space', Value);
end;

function TXMLAlttrans.Get_Restype: UnicodeString;
begin
  Result := AttributeNodes['restype'].Text;
end;

procedure TXMLAlttrans.Set_Restype(Value: UnicodeString);
begin
  SetAttribute('restype', Value);
end;

function TXMLAlttrans.Get_Resname: UnicodeString;
begin
  Result := AttributeNodes['resname'].Text;
end;

procedure TXMLAlttrans.Set_Resname(Value: UnicodeString);
begin
  SetAttribute('resname', Value);
end;

function TXMLAlttrans.Get_Extradata: UnicodeString;
begin
  Result := AttributeNodes['extradata'].Text;
end;

procedure TXMLAlttrans.Set_Extradata(Value: UnicodeString);
begin
  SetAttribute('extradata', Value);
end;

function TXMLAlttrans.Get_Extype: UnicodeString;
begin
  Result := AttributeNodes['extype'].Text;
end;

procedure TXMLAlttrans.Set_Extype(Value: UnicodeString);
begin
  SetAttribute('extype', Value);
end;

function TXMLAlttrans.Get_Helpid: UnicodeString;
begin
  Result := AttributeNodes['help-id'].Text;
end;

procedure TXMLAlttrans.Set_Helpid(Value: UnicodeString);
begin
  SetAttribute('help-id', Value);
end;

function TXMLAlttrans.Get_Menu: UnicodeString;
begin
  Result := AttributeNodes['menu'].Text;
end;

procedure TXMLAlttrans.Set_Menu(Value: UnicodeString);
begin
  SetAttribute('menu', Value);
end;

function TXMLAlttrans.Get_Menuoption: UnicodeString;
begin
  Result := AttributeNodes['menu-option'].Text;
end;

procedure TXMLAlttrans.Set_Menuoption(Value: UnicodeString);
begin
  SetAttribute('menu-option', Value);
end;

function TXMLAlttrans.Get_Menuname: UnicodeString;
begin
  Result := AttributeNodes['menu-name'].Text;
end;

procedure TXMLAlttrans.Set_Menuname(Value: UnicodeString);
begin
  SetAttribute('menu-name', Value);
end;

function TXMLAlttrans.Get_Mid: UnicodeString;
begin
  Result := AttributeNodes['mid'].Text;
end;

procedure TXMLAlttrans.Set_Mid(Value: UnicodeString);
begin
  SetAttribute('mid', Value);
end;

function TXMLAlttrans.Get_Coord: UnicodeString;
begin
  Result := AttributeNodes['coord'].Text;
end;

procedure TXMLAlttrans.Set_Coord(Value: UnicodeString);
begin
  SetAttribute('coord', Value);
end;

function TXMLAlttrans.Get_Font: UnicodeString;
begin
  Result := AttributeNodes['font'].Text;
end;

procedure TXMLAlttrans.Set_Font(Value: UnicodeString);
begin
  SetAttribute('font', Value);
end;

function TXMLAlttrans.Get_Cssstyle: UnicodeString;
begin
  Result := AttributeNodes['css-style'].Text;
end;

procedure TXMLAlttrans.Set_Cssstyle(Value: UnicodeString);
begin
  SetAttribute('css-style', Value);
end;

function TXMLAlttrans.Get_Style: UnicodeString;
begin
  Result := AttributeNodes['style'].Text;
end;

procedure TXMLAlttrans.Set_Style(Value: UnicodeString);
begin
  SetAttribute('style', Value);
end;

function TXMLAlttrans.Get_Exstyle: UnicodeString;
begin
  Result := AttributeNodes['exstyle'].Text;
end;

procedure TXMLAlttrans.Set_Exstyle(Value: UnicodeString);
begin
  SetAttribute('exstyle', Value);
end;

function TXMLAlttrans.Get_Phasename: UnicodeString;
begin
  Result := AttributeNodes['phase-name'].Text;
end;

procedure TXMLAlttrans.Set_Phasename(Value: UnicodeString);
begin
  SetAttribute('phase-name', Value);
end;

function TXMLAlttrans.Get_Alttranstype: UnicodeString;
begin
  Result := AttributeNodes['alttranstype'].Text;
end;

procedure TXMLAlttrans.Set_Alttranstype(Value: UnicodeString);
begin
  SetAttribute('alttranstype', Value);
end;

function TXMLAlttrans.Get_Source: IXMLSource;
begin
  Result := ChildNodes['source'] as IXMLSource;
end;

function TXMLAlttrans.Get_Segsource: IXMLSegsource;
begin
  Result := ChildNodes['seg-source'] as IXMLSegsource;
end;

function TXMLAlttrans.Get_Target: IXMLTarget;
begin
  Result := ChildNodes['target'] as IXMLTarget;
end;

function TXMLAlttrans.Get_Contextgroup: IXMLContextgroupList;
begin
  Result := FContextgroup;
end;

function TXMLAlttrans.Get_Note: IXMLNoteList;
begin
  Result := FNote;
end;

{ TXMLAlttransList }

function TXMLAlttransList.Add: IXMLAlttrans;
begin
  Result := AddItem(-1) as IXMLAlttrans;
end;

function TXMLAlttransList.Insert(const Index: Integer): IXMLAlttrans;
begin
  Result := AddItem(Index) as IXMLAlttrans;
end;

function TXMLAlttransList.Get_Item(Index: Integer): IXMLAlttrans;
begin
  Result := List[Index] as IXMLAlttrans;
end;

{ TXMLBinunit }

procedure TXMLBinunit.AfterConstruction;
begin
  RegisterChildNode('bin-source', TXMLBinsource);
  RegisterChildNode('bin-target', TXMLBintarget);
  RegisterChildNode('context-group', TXMLContextgroup);
  RegisterChildNode('count-group', TXMLCountgroup);
  RegisterChildNode('note', TXMLNote);
  RegisterChildNode('trans-unit', TXMLTransunit);
  FContextgroup := CreateCollection(TXMLContextgroupList, IXMLContextgroup, 'context-group') as IXMLContextgroupList;
  FCountgroup := CreateCollection(TXMLCountgroupList, IXMLCountgroup, 'count-group') as IXMLCountgroupList;
  FNote := CreateCollection(TXMLNoteList, IXMLNote, 'note') as IXMLNoteList;
  FTransunit := CreateCollection(TXMLTransunitList, IXMLTransunit, 'trans-unit') as IXMLTransunitList;
  inherited;
end;

function TXMLBinunit.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['id'].Text;
end;

procedure TXMLBinunit.Set_Id(Value: UnicodeString);
begin
  SetAttribute('id', Value);
end;

function TXMLBinunit.Get_Mimetype: UnicodeString;
begin
  Result := AttributeNodes['mime-type'].Text;
end;

procedure TXMLBinunit.Set_Mimetype(Value: UnicodeString);
begin
  SetAttribute('mime-type', Value);
end;

function TXMLBinunit.Get_Approved: UnicodeString;
begin
  Result := AttributeNodes['approved'].Text;
end;

procedure TXMLBinunit.Set_Approved(Value: UnicodeString);
begin
  SetAttribute('approved', Value);
end;

function TXMLBinunit.Get_Translate: UnicodeString;
begin
  Result := AttributeNodes['translate'].Text;
end;

procedure TXMLBinunit.Set_Translate(Value: UnicodeString);
begin
  SetAttribute('translate', Value);
end;

function TXMLBinunit.Get_Reformat: UnicodeString;
begin
  Result := AttributeNodes['reformat'].Text;
end;

procedure TXMLBinunit.Set_Reformat(Value: UnicodeString);
begin
  SetAttribute('reformat', Value);
end;

function TXMLBinunit.Get_Restype: UnicodeString;
begin
  Result := AttributeNodes['restype'].Text;
end;

procedure TXMLBinunit.Set_Restype(Value: UnicodeString);
begin
  SetAttribute('restype', Value);
end;

function TXMLBinunit.Get_Resname: UnicodeString;
begin
  Result := AttributeNodes['resname'].Text;
end;

procedure TXMLBinunit.Set_Resname(Value: UnicodeString);
begin
  SetAttribute('resname', Value);
end;

function TXMLBinunit.Get_Phasename: UnicodeString;
begin
  Result := AttributeNodes['phase-name'].Text;
end;

procedure TXMLBinunit.Set_Phasename(Value: UnicodeString);
begin
  SetAttribute('phase-name', Value);
end;

function TXMLBinunit.Get_Binsource: IXMLBinsource;
begin
  Result := ChildNodes['bin-source'] as IXMLBinsource;
end;

function TXMLBinunit.Get_Bintarget: IXMLBintarget;
begin
  Result := ChildNodes['bin-target'] as IXMLBintarget;
end;

function TXMLBinunit.Get_Contextgroup: IXMLContextgroupList;
begin
  Result := FContextgroup;
end;

function TXMLBinunit.Get_Countgroup: IXMLCountgroupList;
begin
  Result := FCountgroup;
end;

function TXMLBinunit.Get_Note: IXMLNoteList;
begin
  Result := FNote;
end;

function TXMLBinunit.Get_Transunit: IXMLTransunitList;
begin
  Result := FTransunit;
end;

{ TXMLBinunitList }

function TXMLBinunitList.Add: IXMLBinunit;
begin
  Result := AddItem(-1) as IXMLBinunit;
end;

function TXMLBinunitList.Insert(const Index: Integer): IXMLBinunit;
begin
  Result := AddItem(Index) as IXMLBinunit;
end;

function TXMLBinunitList.Get_Item(Index: Integer): IXMLBinunit;
begin
  Result := List[Index] as IXMLBinunit;
end;

{ TXMLBinsource }

procedure TXMLBinsource.AfterConstruction;
begin
  RegisterChildNode('internal-file', TXMLInternalfile);
  RegisterChildNode('external-file', TXMLExternalfile);
  inherited;
end;

function TXMLBinsource.Get_Internalfile: IXMLInternalfile;
begin
  Result := ChildNodes['internal-file'] as IXMLInternalfile;
end;

function TXMLBinsource.Get_Externalfile: IXMLExternalfile;
begin
  Result := ChildNodes['external-file'] as IXMLExternalfile;
end;

{ TXMLBintarget }

procedure TXMLBintarget.AfterConstruction;
begin
  RegisterChildNode('internal-file', TXMLInternalfile);
  RegisterChildNode('external-file', TXMLExternalfile);
  inherited;
end;

function TXMLBintarget.Get_Mimetype: UnicodeString;
begin
  Result := AttributeNodes['mime-type'].Text;
end;

procedure TXMLBintarget.Set_Mimetype(Value: UnicodeString);
begin
  SetAttribute('mime-type', Value);
end;

function TXMLBintarget.Get_State: UnicodeString;
begin
  Result := AttributeNodes['state'].Text;
end;

procedure TXMLBintarget.Set_State(Value: UnicodeString);
begin
  SetAttribute('state', Value);
end;

function TXMLBintarget.Get_Statequalifier: UnicodeString;
begin
  Result := AttributeNodes['state-qualifier'].Text;
end;

procedure TXMLBintarget.Set_Statequalifier(Value: UnicodeString);
begin
  SetAttribute('state-qualifier', Value);
end;

function TXMLBintarget.Get_Phasename: UnicodeString;
begin
  Result := AttributeNodes['phase-name'].Text;
end;

procedure TXMLBintarget.Set_Phasename(Value: UnicodeString);
begin
  SetAttribute('phase-name', Value);
end;

function TXMLBintarget.Get_Restype: UnicodeString;
begin
  Result := AttributeNodes['restype'].Text;
end;

procedure TXMLBintarget.Set_Restype(Value: UnicodeString);
begin
  SetAttribute('restype', Value);
end;

function TXMLBintarget.Get_Resname: UnicodeString;
begin
  Result := AttributeNodes['resname'].Text;
end;

procedure TXMLBintarget.Set_Resname(Value: UnicodeString);
begin
  SetAttribute('resname', Value);
end;

function TXMLBintarget.Get_Internalfile: IXMLInternalfile;
begin
  Result := ChildNodes['internal-file'] as IXMLInternalfile;
end;

function TXMLBintarget.Get_Externalfile: IXMLExternalfile;
begin
  Result := ChildNodes['external-file'] as IXMLExternalfile;
end;

end.