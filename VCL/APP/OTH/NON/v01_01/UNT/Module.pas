unit Module;

interface

uses
  System.SysUtils, System.Classes, Data.DB, mySQLDbTables;

type
  TDmod = class(TDataModule)
    MyDB: TmySQLDatabase;
    QrUpd: TmySQLQuery;
    QrAux: TmySQLQuery;
    QrMas: TmySQLQuery;
    QrSQL: TmySQLQuery;
    QrIdx: TmySQLQuery;
    QrNTV: TmySQLQuery;
    MyDBn: TmySQLDatabase;
    QrControle: TmySQLQuery;
    QrControleContasU: TIntegerField;
    QrControleEntDefAtr: TIntegerField;
    QrControleEntAtrCad: TIntegerField;
    QrControleEntAtrIts: TIntegerField;
    QrControleBalTopoNom: TIntegerField;
    QrControleBalTopoTit: TIntegerField;
    QrControleBalTopoPer: TIntegerField;
    QrControleCasasProd: TSmallintField;
    QrControleNCMs: TIntegerField;
    QrControleParamsNFs: TIntegerField;
    QrControleCambioCot: TIntegerField;
    QrControleCambioMda: TIntegerField;
    QrControleMoedaBr: TIntegerField;
    QrControleSecuritStr: TWideStringField;
    QrControleLogoBig1: TWideStringField;
    QrControleEquiCom: TIntegerField;
    QrControleContasLnk: TIntegerField;
    QrControleLastBco: TIntegerField;
    QrControleMyPerJuros: TFloatField;
    QrControleMyPerMulta: TFloatField;
    QrControleEquiGru: TIntegerField;
    QrControleCNAB_Rem: TIntegerField;
    QrControleCNAB_Rem_I: TIntegerField;
    QrControlePreEmMsgIm: TIntegerField;
    QrControlePreEmMsg: TIntegerField;
    QrControlePreEmail: TIntegerField;
    QrControleContasMes: TIntegerField;
    QrControleMultiPgto: TIntegerField;
    QrControleImpObs: TIntegerField;
    QrControleProLaINSSr: TFloatField;
    QrControleProLaINSSp: TFloatField;
    QrControleVerSalTabs: TIntegerField;
    QrControleVerBcoTabs: TIntegerField;
    QrControleCNABCtaTar: TIntegerField;
    QrControleCNABCtaJur: TIntegerField;
    QrControleCNABCtaMul: TIntegerField;
    QrControleCNAB_CaD: TIntegerField;
    QrControleCNAB_CaG: TIntegerField;
    QrControleAtzCritic: TIntegerField;
    QrControleContasTrf: TIntegerField;
    QrControleSomaIts: TIntegerField;
    QrControleContasAgr: TIntegerField;
    QrControleVendaCartPg: TIntegerField;
    QrControleVendaParcPg: TIntegerField;
    QrControleVendaPeriPg: TIntegerField;
    QrControleVendaDiasPg: TIntegerField;
    QrControleLogoNF: TWideStringField;
    QrControleConfJanela: TIntegerField;
    QrControleDataPesqAuto: TSmallintField;
    QrControleMeuLogoPath: TWideStringField;
    QrControleAtividad: TSmallintField;
    QrControleCidades: TSmallintField;
    QrControleChequez: TSmallintField;
    QrControlePaises: TSmallintField;
    QrControleMyPgParc: TSmallintField;
    QrControleMyPgQtdP: TSmallintField;
    QrControleMyPgPeri: TSmallintField;
    QrControleMyPgDias: TSmallintField;
    QrControleCorRecibo: TIntegerField;
    QrControleIdleMinutos: TIntegerField;
    QrControleCambiosData: TDateField;
    QrControleCambiosUsuario: TIntegerField;
    QrControlereg10: TSmallintField;
    QrControlereg11: TSmallintField;
    QrControlereg50: TSmallintField;
    QrControlereg51: TSmallintField;
    QrControlereg53: TSmallintField;
    QrControlereg54: TSmallintField;
    QrControlereg56: TSmallintField;
    QrControlereg60: TSmallintField;
    QrControlereg75: TSmallintField;
    QrControlereg88: TSmallintField;
    QrControlereg90: TSmallintField;
    QrControleNumSerieNF: TSmallintField;
    QrControleSerieNF: TIntegerField;
    QrControleModeloNF: TSmallintField;
    QrControleMyPagTip: TSmallintField;
    QrControleMyPagCar: TSmallintField;
    QrControleControlaNeg: TSmallintField;
    QrControleFamilias: TSmallintField;
    QrControleFamiliasIts: TSmallintField;
    QrControleAskNFOrca: TSmallintField;
    QrControlePreviewNF: TSmallintField;
    QrControleOrcaRapido: TSmallintField;
    QrControleDistriDescoItens: TSmallintField;
    QrControleEntraSemValor: TSmallintField;
    QrControleMensalSempre: TSmallintField;
    QrControleBalType: TSmallintField;
    QrControleOrcaOrdem: TSmallintField;
    QrControleOrcaLinhas: TSmallintField;
    QrControleOrcaLFeed: TIntegerField;
    QrControleOrcaModelo: TSmallintField;
    QrControleOrcaRodaPos: TSmallintField;
    QrControleOrcaRodape: TSmallintField;
    QrControleOrcaCabecalho: TSmallintField;
    QrControleCoresRel: TSmallintField;
    QrControleMoraDD: TFloatField;
    QrControleMulta: TFloatField;
    QrControleCFiscalPadr: TWideStringField;
    QrControleSitTribPadr: TWideStringField;
    QrControleCFOPPadr: TWideStringField;
    QrControleAvisosCxaEdit: TSmallintField;
    QrControleTravaCidade: TSmallintField;
    QrControleChConfCab: TIntegerField;
    QrControleImpDOS: TIntegerField;
    QrControleUnidadePadrao: TIntegerField;
    QrControleProdutosV: TIntegerField;
    QrControleCartDespesas: TIntegerField;
    QrControleReserva: TSmallintField;
    QrControleCNPJ: TWideStringField;
    QrControleVersao: TIntegerField;
    QrControleVerWeb: TIntegerField;
    QrControleUFPadrao: TIntegerField;
    QrControleCidade: TWideStringField;
    QrControleDono: TIntegerField;
    QrControleSoMaiusculas: TWideStringField;
    QrControleMoeda: TWideStringField;
    QrControleErroHora: TIntegerField;
    QrControleSenhas: TIntegerField;
    QrControleSenhasIts: TIntegerField;
    QrControleSalarios: TIntegerField;
    QrControleEntidades: TIntegerField;
    QrControleEntiCtas: TIntegerField;
    QrControleUFs: TIntegerField;
    QrControleListaECivil: TIntegerField;
    QrControlePerfis: TIntegerField;
    QrControleUsuario: TIntegerField;
    QrControleContas: TIntegerField;
    QrControleCentroCusto: TIntegerField;
    QrControleDepartamentos: TIntegerField;
    QrControleDividas: TIntegerField;
    QrControleDividasIts: TIntegerField;
    QrControleDividasPgs: TIntegerField;
    QrControleCarteiras: TIntegerField;
    QrControleCarteirasU: TIntegerField;
    QrControleCartaG: TIntegerField;
    QrControleCartas: TIntegerField;
    QrControleConsignacao: TIntegerField;
    QrControleGrupo: TIntegerField;
    QrControleSubGrupo: TIntegerField;
    QrControleConjunto: TIntegerField;
    QrControlePlano: TIntegerField;
    QrControleInflacao: TIntegerField;
    QrControlekm: TIntegerField;
    QrControlekmMedia: TIntegerField;
    QrControlekmIts: TIntegerField;
    QrControleFatura: TIntegerField;
    QrControleLanctos: TLargeintField;
    QrControleLctoEndoss: TIntegerField;
    QrControleEntiGrupos: TIntegerField;
    QrControleEntiContat: TIntegerField;
    QrControleEntiCargos: TIntegerField;
    QrControleEntiMail: TIntegerField;
    QrControleEntiTel: TIntegerField;
    QrControleEntiTipCto: TIntegerField;
    QrControleAparencias: TIntegerField;
    QrControlePages: TIntegerField;
    QrControleMultiEtq: TIntegerField;
    QrControleEntiTransp: TIntegerField;
    QrControleEntiRespon: TIntegerField;
    QrControleEntiCfgRel: TIntegerField;
    QrControleExcelGru: TIntegerField;
    QrControleExcelGruImp: TIntegerField;
    QrControleMediaCH: TIntegerField;
    QrControleContraSenha: TWideStringField;
    QrControleImprime: TIntegerField;
    QrControleImprimeBand: TIntegerField;
    QrControleImprimeView: TIntegerField;
    QrControleComProdPerc: TIntegerField;
    QrControleComProdEdit: TIntegerField;
    QrControleComServPerc: TIntegerField;
    QrControleComServEdit: TIntegerField;
    QrControlePaperLef: TIntegerField;
    QrControlePaperTop: TIntegerField;
    QrControlePaperHei: TIntegerField;
    QrControlePaperWid: TIntegerField;
    QrControlePaperFcl: TIntegerField;
    QrControlePadrPlacaCar: TWideStringField;
    QrControleServSMTP: TWideStringField;
    QrControleNomeMailOC: TWideStringField;
    QrControleDonoMailOC: TWideStringField;
    QrControleMailOC: TWideStringField;
    QrControleCorpoMailOC: TWideMemoField;
    QrControleConexaoDialUp: TWideStringField;
    QrControleMailCCCega: TWideStringField;
    QrControleContVen: TIntegerField;
    QrControleContCom: TIntegerField;
    QrControleCartVen: TIntegerField;
    QrControleCartCom: TIntegerField;
    QrControleCartDeS: TIntegerField;
    QrControleCartReS: TIntegerField;
    QrControleCartDeG: TIntegerField;
    QrControleCartReG: TIntegerField;
    QrControleCartCoE: TIntegerField;
    QrControleCartCoC: TIntegerField;
    QrControleCartEmD: TIntegerField;
    QrControleCartEmA: TIntegerField;
    QrControleMoedaVal: TFloatField;
    QrControleTela1: TIntegerField;
    QrControleChamarPgtoServ: TIntegerField;
    QrControleFormUsaTam: TIntegerField;
    QrControleFormHeight: TIntegerField;
    QrControleFormWidth: TIntegerField;
    QrControleFormPixEsq: TIntegerField;
    QrControleFormPixDir: TIntegerField;
    QrControleFormPixTop: TIntegerField;
    QrControleFormPixBot: TIntegerField;
    QrControleFormFoAlt: TIntegerField;
    QrControleFormFoPro: TFloatField;
    QrControleFormUsaPro: TIntegerField;
    QrControleFormSlides: TIntegerField;
    QrControleFormNeg: TIntegerField;
    QrControleFormIta: TIntegerField;
    QrControleFormSub: TIntegerField;
    QrControleFormExt: TIntegerField;
    QrControleFormFundoTipo: TIntegerField;
    QrControleFormFundoBMP: TWideStringField;
    QrControleServInterv: TIntegerField;
    QrControleServAntecip: TIntegerField;
    QrControleAdiLancto: TIntegerField;
    QrControleContaSal: TIntegerField;
    QrControleContaVal: TIntegerField;
    QrControlePronomeE: TWideStringField;
    QrControlePronomeM: TWideStringField;
    QrControlePronomeF: TWideStringField;
    QrControlePronomeA: TWideStringField;
    QrControleSaudacaoE: TWideStringField;
    QrControleSaudacaoM: TWideStringField;
    QrControleSaudacaoF: TWideStringField;
    QrControleSaudacaoA: TWideStringField;
    QrControleNiver: TSmallintField;
    QrControleNiverddA: TSmallintField;
    QrControleNiverddD: TSmallintField;
    QrControleLastPassD: TDateTimeField;
    QrControleMultiPass: TIntegerField;
    QrControleCodigo: TIntegerField;
    QrControleAlterWeb: TSmallintField;
    QrControleAtivo: TSmallintField;
    QrControleBLQ_TopoAvisoV: TIntegerField;
    QrControleBLQ_MEsqAvisoV: TIntegerField;
    QrControleBLQ_AltuAvisoV: TIntegerField;
    QrControleBLQ_LargAvisoV: TIntegerField;
    QrControleBLQ_TopoDestin: TIntegerField;
    QrControleBLQ_MEsqDestin: TIntegerField;
    QrControleBLQ_AltuDestin: TIntegerField;
    QrControleBLQ_LargDestin: TIntegerField;
    QrNTI: TmySQLQuery;
    QrPriorNext: TmySQLQuery;
    QrAgora: TmySQLQuery;
    QrAgoraANO: TLargeintField;
    QrAgoraMES: TLargeintField;
    QrAgoraDIA: TLargeintField;
    QrAgoraHORA: TLargeintField;
    QrAgoraMINUTO: TLargeintField;
    QrAgoraSEGUNDO: TLargeintField;
    QrAgoraAGORA: TDateTimeField;
    QrTerminal: TmySQLQuery;
    QrTerminalIP: TWideStringField;
    QrTerminalTerminal: TIntegerField;
    QrUpdU: TmySQLQuery;
    MyLocDatabase: TmySQLDatabase;
    ZZDB: TmySQLDatabase;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ReopenControle();
    procedure ReopenParamsEspecificos(Empresa: Integer);
    function  TabelasQueNaoQueroCriar(): String;
  end;

var
  Dmod: TDmod;

implementation

{%CLASSGROUP 'System.Classes.TPersistent'}

{$R *.dfm}

uses
  DmkDAC_PF;

{ TDmod }

procedure TDmod.ReopenControle;
begin
  UnDmkDAC_PF.AbreQuery(QrControle, Dmod.MyDB);
end;


procedure TDmod.ReopenParamsEspecificos(Empresa: Integer);
begin
  // Compatibilidade
end;

function TDmod.TabelasQueNaoQueroCriar: String;
begin
  // Compatibilidade
end;

end.
