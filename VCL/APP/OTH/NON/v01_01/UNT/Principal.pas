unit Principal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.StdCtrls, Vcl.Buttons, AdvToolBar, WinSkinStore, WinSkinData, Vcl.ExtCtrls;

type
  TFmPrincipal = class(TForm)
    sd1: TSkinData;
    SkinStore1: TSkinStore;
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    Image1: TImage;
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ReCaptionComponentesDeForm(Form: TForm);
  end;

var
  FmPrincipal: TFmPrincipal;

implementation

{$R *.dfm}

{ TFmPrincipal }

uses
  TesteCores;

procedure TFmPrincipal.BitBtn1Click(Sender: TObject);
begin
  Application.CreateForm(TFmTesteCores, FmTesteCores);
  FmTesteCores.ShowModal;
  FmTesteCores.Destroy;
end;

procedure TFmPrincipal.ReCaptionComponentesDeForm(Form: TForm);
begin
  // N�o usa ainda!
end;

end.
