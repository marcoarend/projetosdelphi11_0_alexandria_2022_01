unit AppCreate;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db, DbCtrls,
  UnInternalConsts2, ComCtrls, Registry, mySQLDbTables,(* DbTables,*) dmkGeral;

type
  TNomeTabRecriaTempTableGerl = (ntrtt_ReadCor);
  TAcaoCreateGer = (acDrop, acCreate, acFind);
  TAppCreate = class(TObject)

  private
    { Private declarations }
    procedure Cria_ntrttReadCor(Qry: TmySQLQuery);

  public
    { Public declarations }
    function RecriaTempTableNovo(Tabela: TNomeTabRecriaTempTableGerl;
             Qry: TmySQLQuery; UniqueTableName: Boolean; Repeticoes:
             Integer = 1; NomeTab: String = ''): String;
  end;

var
  UnAppCreate: TAppCreate;

implementation

uses UnMyObjects, Module, ModuleGeral;

procedure TAppCreate.Cria_ntrttReadCor(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  PosX                           int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  PosY                           int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  RGBr                           int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  RGBg                           int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  RGBb                           int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  HSLh                           double(15,2) NOT NULL DEFAULT "0.00"          ,');
  Qry.SQL.Add('  HSLs                           double(15,2) NOT NULL DEFAULT "0.00"          ,');
  Qry.SQL.Add('  HSLl                           double(15,2) NOT NULL DEFAULT "0.00"          ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

function TAppCreate.RecriaTempTableNovo(Tabela: TNomeTabRecriaTempTableGerl; Qry: TmySQLQuery;
UniqueTableName: Boolean; Repeticoes: Integer = 1; NomeTab: String = ''): String;
var
  Nome, TabNo: String;
  P: Integer;
begin
  TabNo := '';
  if NomeTab = '' then
  begin
    case Tabela of
      ntrtt_ReadCor:          Nome := Lowercase('_ReadCor_');
      //
      // ...
      else Nome := '';
    end;
  end else
    Nome := Lowercase(NomeTab);
  //
  if Nome = '' then
  begin
    Geral.MB_Erro('Tabela tempor�ria sem nome definido! (RecriaTemTableNovo)');
    Result := '';
    Exit;
  end;
  if UniqueTableName then
  begin
    TabNo := '_' + FormatFloat('0', VAR_USUARIO) + '_' + Nome;
    //  caso for Master ou Admin (n�meros negativos)
    P := pos('-', TabNo);
    if P > 0 then
      TabNo[P] := '_';
  end else TabNo := Nome;
  Qry.Close;
  Qry.SQL.Clear;
  Qry.SQL.Add('DROP TABLE IF EXISTS ' + TabNo);
  Qry.ExecSQL;
  //
  Qry.Close;
  Qry.SQL.Clear;
  Qry.SQL.Add('CREATE TABLE ' + TabNo +' (');
  //
  case Tabela of
    ntrtt_ReadCor:           Cria_ntrttReadCor(Qry);
    //
    else Geral.MB_Erro('N�o foi poss�vel criar a tabela tempor�ria "' +
    Nome + '" por falta de implementa��o!');
  end;
  Result := TabNo;
end;

end.

