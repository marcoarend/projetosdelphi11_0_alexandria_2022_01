object Dmod: TDmod
  OldCreateOrder = False
  Height = 530
  Width = 785
  object MyDB: TmySQLDatabase
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    DatasetOptions = []
    Left = 52
    Top = 16
  end
  object QrUpd: TmySQLQuery
    Database = MyDB
    Left = 52
    Top = 68
  end
  object QrAux: TmySQLQuery
    Database = MyDB
    Left = 52
    Top = 120
  end
  object QrMas: TmySQLQuery
    Database = MyDB
    Left = 148
    Top = 320
  end
  object QrSQL: TmySQLQuery
    Database = MyDB
    Left = 120
    Top = 364
  end
  object QrIdx: TmySQLQuery
    Database = MyDB
    Left = 120
    Top = 320
  end
  object QrNTV: TmySQLQuery
    Database = MyDB
    Left = 160
    Top = 52
  end
  object MyDBn: TmySQLDatabase
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    DatasetOptions = []
    Left = 400
    Top = 8
  end
  object QrControle: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM Controle')
    Left = 44
    Top = 288
    object QrControleContasU: TIntegerField
      FieldName = 'ContasU'
      Origin = 'controle.ContasU'
    end
    object QrControleEntDefAtr: TIntegerField
      FieldName = 'EntDefAtr'
      Origin = 'controle.EntDefAtr'
    end
    object QrControleEntAtrCad: TIntegerField
      FieldName = 'EntAtrCad'
      Origin = 'controle.EntAtrCad'
    end
    object QrControleEntAtrIts: TIntegerField
      FieldName = 'EntAtrIts'
      Origin = 'controle.EntAtrIts'
    end
    object QrControleBalTopoNom: TIntegerField
      FieldName = 'BalTopoNom'
      Origin = 'controle.BalTopoNom'
    end
    object QrControleBalTopoTit: TIntegerField
      FieldName = 'BalTopoTit'
      Origin = 'controle.BalTopoTit'
    end
    object QrControleBalTopoPer: TIntegerField
      FieldName = 'BalTopoPer'
      Origin = 'controle.BalTopoPer'
    end
    object QrControleCasasProd: TSmallintField
      FieldName = 'CasasProd'
      Origin = 'controle.CasasProd'
    end
    object QrControleNCMs: TIntegerField
      FieldName = 'NCMs'
      Origin = 'controle.NCMs'
    end
    object QrControleParamsNFs: TIntegerField
      FieldName = 'ParamsNFs'
      Origin = 'controle.ParamsNFs'
    end
    object QrControleCambioCot: TIntegerField
      FieldName = 'CambioCot'
      Origin = 'controle.CambioCot'
    end
    object QrControleCambioMda: TIntegerField
      FieldName = 'CambioMda'
      Origin = 'controle.CambioMda'
    end
    object QrControleMoedaBr: TIntegerField
      FieldName = 'MoedaBr'
      Origin = 'controle.MoedaBr'
    end
    object QrControleSecuritStr: TWideStringField
      FieldName = 'SecuritStr'
      Origin = 'controle.SecuritStr'
      Size = 32
    end
    object QrControleLogoBig1: TWideStringField
      FieldName = 'LogoBig1'
      Origin = 'controle.LogoBig1'
      Size = 255
    end
    object QrControleEquiCom: TIntegerField
      FieldName = 'EquiCom'
      Origin = 'controle.EquiCom'
    end
    object QrControleContasLnk: TIntegerField
      FieldName = 'ContasLnk'
      Origin = 'controle.ContasLnk'
    end
    object QrControleLastBco: TIntegerField
      FieldName = 'LastBco'
      Origin = 'controle.LastBco'
    end
    object QrControleMyPerJuros: TFloatField
      FieldName = 'MyPerJuros'
      Origin = 'controle.MyPerJuros'
    end
    object QrControleMyPerMulta: TFloatField
      FieldName = 'MyPerMulta'
      Origin = 'controle.MyPerMulta'
    end
    object QrControleEquiGru: TIntegerField
      FieldName = 'EquiGru'
      Origin = 'controle.EquiGru'
    end
    object QrControleCNAB_Rem: TIntegerField
      FieldName = 'CNAB_Rem'
      Origin = 'controle.CNAB_Rem'
    end
    object QrControleCNAB_Rem_I: TIntegerField
      FieldName = 'CNAB_Rem_I'
      Origin = 'controle.CNAB_Rem_I'
    end
    object QrControlePreEmMsgIm: TIntegerField
      FieldName = 'PreEmMsgIm'
      Origin = 'controle.PreEmMsgIm'
    end
    object QrControlePreEmMsg: TIntegerField
      FieldName = 'PreEmMsg'
      Origin = 'controle.PreEmMsg'
    end
    object QrControlePreEmail: TIntegerField
      FieldName = 'PreEmail'
      Origin = 'controle.PreEmail'
    end
    object QrControleContasMes: TIntegerField
      FieldName = 'ContasMes'
      Origin = 'controle.ContasMes'
    end
    object QrControleMultiPgto: TIntegerField
      FieldName = 'MultiPgto'
      Origin = 'controle.MultiPgto'
    end
    object QrControleImpObs: TIntegerField
      FieldName = 'ImpObs'
      Origin = 'controle.ImpObs'
    end
    object QrControleProLaINSSr: TFloatField
      FieldName = 'ProLaINSSr'
      Origin = 'controle.ProLaINSSr'
    end
    object QrControleProLaINSSp: TFloatField
      FieldName = 'ProLaINSSp'
      Origin = 'controle.ProLaINSSp'
    end
    object QrControleVerSalTabs: TIntegerField
      FieldName = 'VerSalTabs'
      Origin = 'controle.VerSalTabs'
    end
    object QrControleVerBcoTabs: TIntegerField
      FieldName = 'VerBcoTabs'
      Origin = 'controle.VerBcoTabs'
    end
    object QrControleCNABCtaTar: TIntegerField
      FieldName = 'CNABCtaTar'
      Origin = 'controle.CNABCtaTar'
    end
    object QrControleCNABCtaJur: TIntegerField
      FieldName = 'CNABCtaJur'
      Origin = 'controle.CNABCtaJur'
    end
    object QrControleCNABCtaMul: TIntegerField
      FieldName = 'CNABCtaMul'
      Origin = 'controle.CNABCtaMul'
    end
    object QrControleCNAB_CaD: TIntegerField
      FieldName = 'CNAB_CaD'
      Origin = 'controle.CNAB_CaD'
    end
    object QrControleCNAB_CaG: TIntegerField
      FieldName = 'CNAB_CaG'
      Origin = 'controle.CNAB_CaG'
    end
    object QrControleAtzCritic: TIntegerField
      FieldName = 'AtzCritic'
      Origin = 'controle.AtzCritic'
    end
    object QrControleContasTrf: TIntegerField
      FieldName = 'ContasTrf'
      Origin = 'controle.ContasTrf'
    end
    object QrControleSomaIts: TIntegerField
      FieldName = 'SomaIts'
      Origin = 'controle.SomaIts'
    end
    object QrControleContasAgr: TIntegerField
      FieldName = 'ContasAgr'
      Origin = 'controle.ContasAgr'
    end
    object QrControleVendaCartPg: TIntegerField
      FieldName = 'VendaCartPg'
      Origin = 'controle.VendaCartPg'
    end
    object QrControleVendaParcPg: TIntegerField
      FieldName = 'VendaParcPg'
      Origin = 'controle.VendaParcPg'
    end
    object QrControleVendaPeriPg: TIntegerField
      FieldName = 'VendaPeriPg'
      Origin = 'controle.VendaPeriPg'
    end
    object QrControleVendaDiasPg: TIntegerField
      FieldName = 'VendaDiasPg'
      Origin = 'controle.VendaDiasPg'
    end
    object QrControleLogoNF: TWideStringField
      FieldName = 'LogoNF'
      Origin = 'controle.LogoNF'
      Size = 255
    end
    object QrControleConfJanela: TIntegerField
      FieldName = 'ConfJanela'
      Origin = 'controle.ConfJanela'
    end
    object QrControleDataPesqAuto: TSmallintField
      FieldName = 'DataPesqAuto'
      Origin = 'controle.DataPesqAuto'
    end
    object QrControleMeuLogoPath: TWideStringField
      FieldName = 'MeuLogoPath'
      Origin = 'controle.MeuLogoPath'
      Size = 255
    end
    object QrControleAtividad: TSmallintField
      FieldName = 'Atividad'
      Origin = 'controle.Atividad'
    end
    object QrControleCidades: TSmallintField
      FieldName = 'Cidades'
      Origin = 'controle.Cidades'
    end
    object QrControleChequez: TSmallintField
      FieldName = 'Chequez'
      Origin = 'controle.Chequez'
    end
    object QrControlePaises: TSmallintField
      FieldName = 'Paises'
      Origin = 'controle.Paises'
    end
    object QrControleMyPgParc: TSmallintField
      FieldName = 'MyPgParc'
      Origin = 'controle.MyPgParc'
    end
    object QrControleMyPgQtdP: TSmallintField
      FieldName = 'MyPgQtdP'
      Origin = 'controle.MyPgQtdP'
    end
    object QrControleMyPgPeri: TSmallintField
      FieldName = 'MyPgPeri'
      Origin = 'controle.MyPgPeri'
    end
    object QrControleMyPgDias: TSmallintField
      FieldName = 'MyPgDias'
      Origin = 'controle.MyPgDias'
    end
    object QrControleCorRecibo: TIntegerField
      FieldName = 'CorRecibo'
      Origin = 'controle.CorRecibo'
    end
    object QrControleIdleMinutos: TIntegerField
      FieldName = 'IdleMinutos'
      Origin = 'controle.IdleMinutos'
    end
    object QrControleCambiosData: TDateField
      FieldName = 'CambiosData'
      Origin = 'controle.CambiosData'
    end
    object QrControleCambiosUsuario: TIntegerField
      FieldName = 'CambiosUsuario'
      Origin = 'controle.CambiosUsuario'
    end
    object QrControlereg10: TSmallintField
      FieldName = 'reg10'
      Origin = 'controle.reg10'
    end
    object QrControlereg11: TSmallintField
      FieldName = 'reg11'
      Origin = 'controle.reg11'
    end
    object QrControlereg50: TSmallintField
      FieldName = 'reg50'
      Origin = 'controle.reg50'
    end
    object QrControlereg51: TSmallintField
      FieldName = 'reg51'
      Origin = 'controle.reg51'
    end
    object QrControlereg53: TSmallintField
      FieldName = 'reg53'
      Origin = 'controle.reg53'
    end
    object QrControlereg54: TSmallintField
      FieldName = 'reg54'
      Origin = 'controle.reg54'
    end
    object QrControlereg56: TSmallintField
      FieldName = 'reg56'
      Origin = 'controle.reg56'
    end
    object QrControlereg60: TSmallintField
      FieldName = 'reg60'
      Origin = 'controle.reg60'
    end
    object QrControlereg75: TSmallintField
      FieldName = 'reg75'
      Origin = 'controle.reg75'
    end
    object QrControlereg88: TSmallintField
      FieldName = 'reg88'
      Origin = 'controle.reg88'
    end
    object QrControlereg90: TSmallintField
      FieldName = 'reg90'
      Origin = 'controle.reg90'
    end
    object QrControleNumSerieNF: TSmallintField
      FieldName = 'NumSerieNF'
      Origin = 'controle.NumSerieNF'
    end
    object QrControleSerieNF: TIntegerField
      FieldName = 'SerieNF'
      Origin = 'controle.SerieNF'
    end
    object QrControleModeloNF: TSmallintField
      FieldName = 'ModeloNF'
      Origin = 'controle.ModeloNF'
    end
    object QrControleMyPagTip: TSmallintField
      FieldName = 'MyPagTip'
      Origin = 'controle.MyPagTip'
    end
    object QrControleMyPagCar: TSmallintField
      FieldName = 'MyPagCar'
      Origin = 'controle.MyPagCar'
    end
    object QrControleControlaNeg: TSmallintField
      FieldName = 'ControlaNeg'
      Origin = 'controle.ControlaNeg'
    end
    object QrControleFamilias: TSmallintField
      FieldName = 'Familias'
      Origin = 'controle.Familias'
    end
    object QrControleFamiliasIts: TSmallintField
      FieldName = 'FamiliasIts'
      Origin = 'controle.FamiliasIts'
    end
    object QrControleAskNFOrca: TSmallintField
      FieldName = 'AskNFOrca'
      Origin = 'controle.AskNFOrca'
    end
    object QrControlePreviewNF: TSmallintField
      FieldName = 'PreviewNF'
      Origin = 'controle.PreviewNF'
    end
    object QrControleOrcaRapido: TSmallintField
      FieldName = 'OrcaRapido'
      Origin = 'controle.OrcaRapido'
    end
    object QrControleDistriDescoItens: TSmallintField
      FieldName = 'DistriDescoItens'
      Origin = 'controle.DistriDescoItens'
    end
    object QrControleEntraSemValor: TSmallintField
      FieldName = 'EntraSemValor'
      Origin = 'controle.EntraSemValor'
    end
    object QrControleMensalSempre: TSmallintField
      FieldName = 'MensalSempre'
      Origin = 'controle.MensalSempre'
    end
    object QrControleBalType: TSmallintField
      FieldName = 'BalType'
      Origin = 'controle.BalType'
    end
    object QrControleOrcaOrdem: TSmallintField
      FieldName = 'OrcaOrdem'
      Origin = 'controle.OrcaOrdem'
    end
    object QrControleOrcaLinhas: TSmallintField
      FieldName = 'OrcaLinhas'
      Origin = 'controle.OrcaLinhas'
    end
    object QrControleOrcaLFeed: TIntegerField
      FieldName = 'OrcaLFeed'
      Origin = 'controle.OrcaLFeed'
    end
    object QrControleOrcaModelo: TSmallintField
      FieldName = 'OrcaModelo'
      Origin = 'controle.OrcaModelo'
    end
    object QrControleOrcaRodaPos: TSmallintField
      FieldName = 'OrcaRodaPos'
      Origin = 'controle.OrcaRodaPos'
    end
    object QrControleOrcaRodape: TSmallintField
      FieldName = 'OrcaRodape'
      Origin = 'controle.OrcaRodape'
    end
    object QrControleOrcaCabecalho: TSmallintField
      FieldName = 'OrcaCabecalho'
      Origin = 'controle.OrcaCabecalho'
    end
    object QrControleCoresRel: TSmallintField
      FieldName = 'CoresRel'
      Origin = 'controle.CoresRel'
    end
    object QrControleMoraDD: TFloatField
      FieldName = 'MoraDD'
      Origin = 'controle.MoraDD'
    end
    object QrControleMulta: TFloatField
      FieldName = 'Multa'
      Origin = 'controle.Multa'
    end
    object QrControleCFiscalPadr: TWideStringField
      FieldName = 'CFiscalPadr'
      Origin = 'controle.CFiscalPadr'
    end
    object QrControleSitTribPadr: TWideStringField
      FieldName = 'SitTribPadr'
      Origin = 'controle.SitTribPadr'
    end
    object QrControleCFOPPadr: TWideStringField
      FieldName = 'CFOPPadr'
      Origin = 'controle.CFOPPadr'
    end
    object QrControleAvisosCxaEdit: TSmallintField
      FieldName = 'AvisosCxaEdit'
      Origin = 'controle.AvisosCxaEdit'
    end
    object QrControleTravaCidade: TSmallintField
      FieldName = 'TravaCidade'
      Origin = 'controle.TravaCidade'
    end
    object QrControleChConfCab: TIntegerField
      FieldName = 'ChConfCab'
      Origin = 'controle.ChConfCab'
    end
    object QrControleImpDOS: TIntegerField
      FieldName = 'ImpDOS'
      Origin = 'controle.ImpDOS'
    end
    object QrControleUnidadePadrao: TIntegerField
      FieldName = 'UnidadePadrao'
      Origin = 'controle.UnidadePadrao'
    end
    object QrControleProdutosV: TIntegerField
      FieldName = 'ProdutosV'
      Origin = 'controle.ProdutosV'
    end
    object QrControleCartDespesas: TIntegerField
      FieldName = 'CartDespesas'
      Origin = 'controle.CartDespesas'
    end
    object QrControleReserva: TSmallintField
      FieldName = 'Reserva'
      Origin = 'controle.Reserva'
    end
    object QrControleCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Origin = 'controle.CNPJ'
      Size = 18
    end
    object QrControleVersao: TIntegerField
      FieldName = 'Versao'
      Origin = 'controle.Versao'
    end
    object QrControleVerWeb: TIntegerField
      FieldName = 'VerWeb'
      Origin = 'controle.VerWeb'
    end
    object QrControleUFPadrao: TIntegerField
      FieldName = 'UFPadrao'
      Origin = 'controle.UFPadrao'
    end
    object QrControleCidade: TWideStringField
      FieldName = 'Cidade'
      Origin = 'controle.Cidade'
      Size = 100
    end
    object QrControleDono: TIntegerField
      FieldName = 'Dono'
      Origin = 'controle.Dono'
    end
    object QrControleSoMaiusculas: TWideStringField
      FieldName = 'SoMaiusculas'
      Origin = 'controle.SoMaiusculas'
      Size = 1
    end
    object QrControleMoeda: TWideStringField
      FieldName = 'Moeda'
      Origin = 'controle.Moeda'
      Size = 4
    end
    object QrControleErroHora: TIntegerField
      FieldName = 'ErroHora'
      Origin = 'controle.ErroHora'
    end
    object QrControleSenhas: TIntegerField
      FieldName = 'Senhas'
      Origin = 'controle.Senhas'
    end
    object QrControleSenhasIts: TIntegerField
      FieldName = 'SenhasIts'
      Origin = 'controle.SenhasIts'
    end
    object QrControleSalarios: TIntegerField
      FieldName = 'Salarios'
      Origin = 'controle.Salarios'
    end
    object QrControleEntidades: TIntegerField
      FieldName = 'Entidades'
      Origin = 'controle.Entidades'
    end
    object QrControleEntiCtas: TIntegerField
      FieldName = 'EntiCtas'
      Origin = 'controle.EntiCtas'
    end
    object QrControleUFs: TIntegerField
      FieldName = 'UFs'
      Origin = 'controle.UFs'
    end
    object QrControleListaECivil: TIntegerField
      FieldName = 'ListaECivil'
      Origin = 'controle.ListaECivil'
    end
    object QrControlePerfis: TIntegerField
      FieldName = 'Perfis'
      Origin = 'controle.Perfis'
    end
    object QrControleUsuario: TIntegerField
      FieldName = 'Usuario'
      Origin = 'controle.Usuario'
    end
    object QrControleContas: TIntegerField
      FieldName = 'Contas'
      Origin = 'controle.Contas'
    end
    object QrControleCentroCusto: TIntegerField
      FieldName = 'CentroCusto'
      Origin = 'controle.CentroCusto'
    end
    object QrControleDepartamentos: TIntegerField
      FieldName = 'Departamentos'
      Origin = 'controle.Departamentos'
    end
    object QrControleDividas: TIntegerField
      FieldName = 'Dividas'
      Origin = 'controle.Dividas'
    end
    object QrControleDividasIts: TIntegerField
      FieldName = 'DividasIts'
      Origin = 'controle.DividasIts'
    end
    object QrControleDividasPgs: TIntegerField
      FieldName = 'DividasPgs'
      Origin = 'controle.DividasPgs'
    end
    object QrControleCarteiras: TIntegerField
      FieldName = 'Carteiras'
      Origin = 'controle.Carteiras'
    end
    object QrControleCarteirasU: TIntegerField
      FieldName = 'CarteirasU'
      Origin = 'controle.CarteirasU'
    end
    object QrControleCartaG: TIntegerField
      FieldName = 'CartaG'
      Origin = 'controle.CartaG'
    end
    object QrControleCartas: TIntegerField
      FieldName = 'Cartas'
      Origin = 'controle.Cartas'
    end
    object QrControleConsignacao: TIntegerField
      FieldName = 'Consignacao'
      Origin = 'controle.Consignacao'
    end
    object QrControleGrupo: TIntegerField
      FieldName = 'Grupo'
      Origin = 'controle.Grupo'
    end
    object QrControleSubGrupo: TIntegerField
      FieldName = 'SubGrupo'
      Origin = 'controle.SubGrupo'
    end
    object QrControleConjunto: TIntegerField
      FieldName = 'Conjunto'
      Origin = 'controle.Conjunto'
    end
    object QrControlePlano: TIntegerField
      FieldName = 'Plano'
      Origin = 'controle.Plano'
    end
    object QrControleInflacao: TIntegerField
      FieldName = 'Inflacao'
      Origin = 'controle.Inflacao'
    end
    object QrControlekm: TIntegerField
      FieldName = 'km'
      Origin = 'controle.km'
    end
    object QrControlekmMedia: TIntegerField
      FieldName = 'kmMedia'
      Origin = 'controle.kmMedia'
    end
    object QrControlekmIts: TIntegerField
      FieldName = 'kmIts'
      Origin = 'controle.kmIts'
    end
    object QrControleFatura: TIntegerField
      FieldName = 'Fatura'
      Origin = 'controle.Fatura'
    end
    object QrControleLanctos: TLargeintField
      FieldName = 'Lanctos'
      Origin = 'controle.Lanctos'
    end
    object QrControleLctoEndoss: TIntegerField
      FieldName = 'LctoEndoss'
      Origin = 'controle.LctoEndoss'
    end
    object QrControleEntiGrupos: TIntegerField
      FieldName = 'EntiGrupos'
      Origin = 'controle.EntiGrupos'
    end
    object QrControleEntiContat: TIntegerField
      FieldName = 'EntiContat'
      Origin = 'controle.EntiContat'
    end
    object QrControleEntiCargos: TIntegerField
      FieldName = 'EntiCargos'
      Origin = 'controle.EntiCargos'
    end
    object QrControleEntiMail: TIntegerField
      FieldName = 'EntiMail'
      Origin = 'controle.EntiMail'
    end
    object QrControleEntiTel: TIntegerField
      FieldName = 'EntiTel'
      Origin = 'controle.EntiTel'
    end
    object QrControleEntiTipCto: TIntegerField
      FieldName = 'EntiTipCto'
      Origin = 'controle.EntiTipCto'
    end
    object QrControleAparencias: TIntegerField
      FieldName = 'Aparencias'
      Origin = 'controle.Aparencias'
    end
    object QrControlePages: TIntegerField
      FieldName = 'Pages'
      Origin = 'controle.Pages'
    end
    object QrControleMultiEtq: TIntegerField
      FieldName = 'MultiEtq'
      Origin = 'controle.MultiEtq'
    end
    object QrControleEntiTransp: TIntegerField
      FieldName = 'EntiTransp'
      Origin = 'controle.EntiTransp'
    end
    object QrControleEntiRespon: TIntegerField
      FieldName = 'EntiRespon'
      Origin = 'controle.EntiRespon'
    end
    object QrControleEntiCfgRel: TIntegerField
      FieldName = 'EntiCfgRel'
      Origin = 'controle.EntiCfgRel'
    end
    object QrControleExcelGru: TIntegerField
      FieldName = 'ExcelGru'
      Origin = 'controle.ExcelGru'
    end
    object QrControleExcelGruImp: TIntegerField
      FieldName = 'ExcelGruImp'
      Origin = 'controle.ExcelGruImp'
    end
    object QrControleMediaCH: TIntegerField
      FieldName = 'MediaCH'
      Origin = 'controle.MediaCH'
    end
    object QrControleContraSenha: TWideStringField
      FieldName = 'ContraSenha'
      Origin = 'controle.ContraSenha'
      Size = 50
    end
    object QrControleImprime: TIntegerField
      FieldName = 'Imprime'
      Origin = 'controle.Imprime'
    end
    object QrControleImprimeBand: TIntegerField
      FieldName = 'ImprimeBand'
      Origin = 'controle.ImprimeBand'
    end
    object QrControleImprimeView: TIntegerField
      FieldName = 'ImprimeView'
      Origin = 'controle.ImprimeView'
    end
    object QrControleComProdPerc: TIntegerField
      FieldName = 'ComProdPerc'
      Origin = 'controle.ComProdPerc'
    end
    object QrControleComProdEdit: TIntegerField
      FieldName = 'ComProdEdit'
      Origin = 'controle.ComProdEdit'
    end
    object QrControleComServPerc: TIntegerField
      FieldName = 'ComServPerc'
      Origin = 'controle.ComServPerc'
    end
    object QrControleComServEdit: TIntegerField
      FieldName = 'ComServEdit'
      Origin = 'controle.ComServEdit'
    end
    object QrControlePaperLef: TIntegerField
      FieldName = 'PaperLef'
      Origin = 'controle.PaperLef'
    end
    object QrControlePaperTop: TIntegerField
      FieldName = 'PaperTop'
      Origin = 'controle.PaperTop'
    end
    object QrControlePaperHei: TIntegerField
      FieldName = 'PaperHei'
      Origin = 'controle.PaperHei'
    end
    object QrControlePaperWid: TIntegerField
      FieldName = 'PaperWid'
      Origin = 'controle.PaperWid'
    end
    object QrControlePaperFcl: TIntegerField
      FieldName = 'PaperFcl'
      Origin = 'controle.PaperFcl'
    end
    object QrControlePadrPlacaCar: TWideStringField
      FieldName = 'PadrPlacaCar'
      Origin = 'controle.PadrPlacaCar'
      Size = 100
    end
    object QrControleServSMTP: TWideStringField
      FieldName = 'ServSMTP'
      Origin = 'controle.ServSMTP'
      Size = 50
    end
    object QrControleNomeMailOC: TWideStringField
      FieldName = 'NomeMailOC'
      Origin = 'controle.NomeMailOC'
      Size = 50
    end
    object QrControleDonoMailOC: TWideStringField
      FieldName = 'DonoMailOC'
      Origin = 'controle.DonoMailOC'
      Size = 50
    end
    object QrControleMailOC: TWideStringField
      FieldName = 'MailOC'
      Origin = 'controle.MailOC'
      Size = 80
    end
    object QrControleCorpoMailOC: TWideMemoField
      FieldName = 'CorpoMailOC'
      Origin = 'controle.CorpoMailOC'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrControleConexaoDialUp: TWideStringField
      FieldName = 'ConexaoDialUp'
      Origin = 'controle.ConexaoDialUp'
      Size = 50
    end
    object QrControleMailCCCega: TWideStringField
      FieldName = 'MailCCCega'
      Origin = 'controle.MailCCCega'
      Size = 80
    end
    object QrControleContVen: TIntegerField
      FieldName = 'ContVen'
      Origin = 'controle.ContVen'
    end
    object QrControleContCom: TIntegerField
      FieldName = 'ContCom'
      Origin = 'controle.ContCom'
    end
    object QrControleCartVen: TIntegerField
      FieldName = 'CartVen'
      Origin = 'controle.CartVen'
    end
    object QrControleCartCom: TIntegerField
      FieldName = 'CartCom'
      Origin = 'controle.CartCom'
    end
    object QrControleCartDeS: TIntegerField
      FieldName = 'CartDeS'
      Origin = 'controle.CartDeS'
    end
    object QrControleCartReS: TIntegerField
      FieldName = 'CartReS'
      Origin = 'controle.CartReS'
    end
    object QrControleCartDeG: TIntegerField
      FieldName = 'CartDeG'
      Origin = 'controle.CartDeG'
    end
    object QrControleCartReG: TIntegerField
      FieldName = 'CartReG'
      Origin = 'controle.CartReG'
    end
    object QrControleCartCoE: TIntegerField
      FieldName = 'CartCoE'
      Origin = 'controle.CartCoE'
    end
    object QrControleCartCoC: TIntegerField
      FieldName = 'CartCoC'
      Origin = 'controle.CartCoC'
    end
    object QrControleCartEmD: TIntegerField
      FieldName = 'CartEmD'
      Origin = 'controle.CartEmD'
    end
    object QrControleCartEmA: TIntegerField
      FieldName = 'CartEmA'
      Origin = 'controle.CartEmA'
    end
    object QrControleMoedaVal: TFloatField
      FieldName = 'MoedaVal'
      Origin = 'controle.MoedaVal'
    end
    object QrControleTela1: TIntegerField
      FieldName = 'Tela1'
      Origin = 'controle.Tela1'
    end
    object QrControleChamarPgtoServ: TIntegerField
      FieldName = 'ChamarPgtoServ'
      Origin = 'controle.ChamarPgtoServ'
    end
    object QrControleFormUsaTam: TIntegerField
      FieldName = 'FormUsaTam'
      Origin = 'controle.FormUsaTam'
    end
    object QrControleFormHeight: TIntegerField
      FieldName = 'FormHeight'
      Origin = 'controle.FormHeight'
    end
    object QrControleFormWidth: TIntegerField
      FieldName = 'FormWidth'
      Origin = 'controle.FormWidth'
    end
    object QrControleFormPixEsq: TIntegerField
      FieldName = 'FormPixEsq'
      Origin = 'controle.FormPixEsq'
    end
    object QrControleFormPixDir: TIntegerField
      FieldName = 'FormPixDir'
      Origin = 'controle.FormPixDir'
    end
    object QrControleFormPixTop: TIntegerField
      FieldName = 'FormPixTop'
      Origin = 'controle.FormPixTop'
    end
    object QrControleFormPixBot: TIntegerField
      FieldName = 'FormPixBot'
      Origin = 'controle.FormPixBot'
    end
    object QrControleFormFoAlt: TIntegerField
      FieldName = 'FormFoAlt'
      Origin = 'controle.FormFoAlt'
    end
    object QrControleFormFoPro: TFloatField
      FieldName = 'FormFoPro'
      Origin = 'controle.FormFoPro'
    end
    object QrControleFormUsaPro: TIntegerField
      FieldName = 'FormUsaPro'
      Origin = 'controle.FormUsaPro'
    end
    object QrControleFormSlides: TIntegerField
      FieldName = 'FormSlides'
      Origin = 'controle.FormSlides'
    end
    object QrControleFormNeg: TIntegerField
      FieldName = 'FormNeg'
      Origin = 'controle.FormNeg'
    end
    object QrControleFormIta: TIntegerField
      FieldName = 'FormIta'
      Origin = 'controle.FormIta'
    end
    object QrControleFormSub: TIntegerField
      FieldName = 'FormSub'
      Origin = 'controle.FormSub'
    end
    object QrControleFormExt: TIntegerField
      FieldName = 'FormExt'
      Origin = 'controle.FormExt'
    end
    object QrControleFormFundoTipo: TIntegerField
      FieldName = 'FormFundoTipo'
      Origin = 'controle.FormFundoTipo'
    end
    object QrControleFormFundoBMP: TWideStringField
      FieldName = 'FormFundoBMP'
      Origin = 'controle.FormFundoBMP'
      Size = 255
    end
    object QrControleServInterv: TIntegerField
      FieldName = 'ServInterv'
      Origin = 'controle.ServInterv'
    end
    object QrControleServAntecip: TIntegerField
      FieldName = 'ServAntecip'
      Origin = 'controle.ServAntecip'
    end
    object QrControleAdiLancto: TIntegerField
      FieldName = 'AdiLancto'
      Origin = 'controle.AdiLancto'
    end
    object QrControleContaSal: TIntegerField
      FieldName = 'ContaSal'
      Origin = 'controle.ContaSal'
    end
    object QrControleContaVal: TIntegerField
      FieldName = 'ContaVal'
      Origin = 'controle.ContaVal'
    end
    object QrControlePronomeE: TWideStringField
      FieldName = 'PronomeE'
      Origin = 'controle.PronomeE'
    end
    object QrControlePronomeM: TWideStringField
      FieldName = 'PronomeM'
      Origin = 'controle.PronomeM'
    end
    object QrControlePronomeF: TWideStringField
      FieldName = 'PronomeF'
      Origin = 'controle.PronomeF'
    end
    object QrControlePronomeA: TWideStringField
      FieldName = 'PronomeA'
      Origin = 'controle.PronomeA'
    end
    object QrControleSaudacaoE: TWideStringField
      FieldName = 'SaudacaoE'
      Origin = 'controle.SaudacaoE'
      Size = 50
    end
    object QrControleSaudacaoM: TWideStringField
      FieldName = 'SaudacaoM'
      Origin = 'controle.SaudacaoM'
      Size = 50
    end
    object QrControleSaudacaoF: TWideStringField
      FieldName = 'SaudacaoF'
      Origin = 'controle.SaudacaoF'
      Size = 50
    end
    object QrControleSaudacaoA: TWideStringField
      FieldName = 'SaudacaoA'
      Origin = 'controle.SaudacaoA'
      Size = 50
    end
    object QrControleNiver: TSmallintField
      FieldName = 'Niver'
      Origin = 'controle.Niver'
    end
    object QrControleNiverddA: TSmallintField
      FieldName = 'NiverddA'
      Origin = 'controle.NiverddA'
    end
    object QrControleNiverddD: TSmallintField
      FieldName = 'NiverddD'
      Origin = 'controle.NiverddD'
    end
    object QrControleLastPassD: TDateTimeField
      FieldName = 'LastPassD'
      Origin = 'controle.LastPassD'
    end
    object QrControleMultiPass: TIntegerField
      FieldName = 'MultiPass'
      Origin = 'controle.MultiPass'
    end
    object QrControleCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'controle.Codigo'
    end
    object QrControleAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'controle.AlterWeb'
    end
    object QrControleAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'controle.Ativo'
    end
    object QrControleBLQ_TopoAvisoV: TIntegerField
      FieldName = 'BLQ_TopoAvisoV'
    end
    object QrControleBLQ_MEsqAvisoV: TIntegerField
      FieldName = 'BLQ_MEsqAvisoV'
    end
    object QrControleBLQ_AltuAvisoV: TIntegerField
      FieldName = 'BLQ_AltuAvisoV'
    end
    object QrControleBLQ_LargAvisoV: TIntegerField
      FieldName = 'BLQ_LargAvisoV'
    end
    object QrControleBLQ_TopoDestin: TIntegerField
      FieldName = 'BLQ_TopoDestin'
    end
    object QrControleBLQ_MEsqDestin: TIntegerField
      FieldName = 'BLQ_MEsqDestin'
    end
    object QrControleBLQ_AltuDestin: TIntegerField
      FieldName = 'BLQ_AltuDestin'
    end
    object QrControleBLQ_LargDestin: TIntegerField
      FieldName = 'BLQ_LargDestin'
    end
  end
  object QrNTI: TmySQLQuery
    Database = MyDBn
    Left = 160
    Top = 100
  end
  object QrPriorNext: TmySQLQuery
    Database = MyDB
    Left = 148
    Top = 364
  end
  object QrAgora: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT YEAR(NOW()) ANO, MONTH(NOW()) MES,'
      'DAYOFMONTH(NOW()) DIA,'
      'HOUR(NOW()) HORA, MINUTE(NOW()) MINUTO,'
      'SECOND(NOW()) SEGUNDO, NOW() AGORA')
    Left = 48
    Top = 168
    object QrAgoraANO: TLargeintField
      FieldName = 'ANO'
    end
    object QrAgoraMES: TLargeintField
      FieldName = 'MES'
    end
    object QrAgoraDIA: TLargeintField
      FieldName = 'DIA'
    end
    object QrAgoraHORA: TLargeintField
      FieldName = 'HORA'
    end
    object QrAgoraMINUTO: TLargeintField
      FieldName = 'MINUTO'
    end
    object QrAgoraSEGUNDO: TLargeintField
      FieldName = 'SEGUNDO'
    end
    object QrAgoraAGORA: TDateTimeField
      FieldName = 'AGORA'
      Required = True
    end
  end
  object QrTerminal: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT * FROM Terminais'
      'WHERE IP=:P0')
    Left = 516
    Top = 160
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTerminalIP: TWideStringField
      FieldName = 'IP'
      Size = 15
    end
    object QrTerminalTerminal: TIntegerField
      FieldName = 'Terminal'
    end
  end
  object QrUpdU: TmySQLQuery
    Database = MyDB
    Left = 104
    Top = 68
  end
  object MyLocDatabase: TmySQLDatabase
    Host = '127.0.0.1'
    ConnectOptions = []
    KeepConnection = False
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'Host=127.0.0.1')
    DatasetOptions = []
    Left = 264
    Top = 7
  end
  object ZZDB: TmySQLDatabase
    Host = '127.0.0.1'
    ConnectOptions = []
    KeepConnection = False
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'Host=127.0.0.1')
    DatasetOptions = []
    Left = 96
    Top = 16
  end
end
