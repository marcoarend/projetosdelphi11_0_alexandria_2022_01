unit Principal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.StdCtrls, Vcl.Buttons, Vcl.ComCtrls, Vcl.Grids, Vcl.ExtCtrls,
  Vcl.Imaging.pngimage, Vcl.Menus,
  // Terceiros
  WinSkinStore, WinSkinData,
  AdvToolBar, AdvShapeButton, AdvGlowButton, AdvToolBarStylers,
  mySQLDbTables,
  // Dermatek
  dmkPageControl, dmkGeral, UnDmkEnums, UnInternalConsts, UnDmkProcFunc, ZCF2,
  MyListas, MyDBCheck, UnDmkWeb, DmkDAC_PF, AdvMenus;

type
  TFmPrincipal = class(TForm)
    sd1: TSkinData;
    SkinStore1: TSkinStore;
    AdvToolBarPager1: TAdvToolBarPager;
    AdvToolBarPager23: TAdvPage;
    AdvToolBar1: TAdvToolBar;
    AGBEntidade: TAdvGlowButton;
    AdvPage3: TAdvPage;
    AdvToolBar7: TAdvToolBar;
    AGBMalaDireta: TAdvGlowButton;
    AGBListaEnti: TAdvGlowButton;
    AdvPage1: TAdvPage;
    AdvToolBar20: TAdvToolBar;
    AGBNovasVersoes: TAdvGlowButton;
    AGBRevertVersao: TAdvGlowButton;
    AdvToolBar8: TAdvToolBar;
    AGBVerifiDB: TAdvGlowButton;
    AGBBackup: TAdvGlowButton;
    AdvToolBar9: TAdvToolBar;
    AGBMatriz: TAdvGlowButton;
    AGBOpcoes: TAdvGlowButton;
    AGBParamsEmp: TAdvGlowButton;
    AdvToolBar25: TAdvToolBar;
    AGBImagem: TAdvGlowMenuButton;
    AGBMenu: TAdvGlowMenuButton;
    AGBTema: TAdvGlowButton;
    AdvPage4: TAdvPage;
    AdvToolBar10: TAdvToolBar;
    AGBSuporte: TAdvGlowButton;
    AGBSobre: TAdvGlowButton;
    AdvShapeButton1: TAdvShapeButton;
    AdvQuickAccessToolBar2: TAdvQuickAccessToolBar;
    ATBLogoff: TAdvToolBarButton;
    ATBVerificaNovaVersao: TAdvToolBarButton;
    ATBFavoritos: TAdvToolBarButton;
    ATBBackup: TAdvToolBarButton;
    ATBVerifi: TAdvToolBarButton;
    ATBMaximizaMenu: TAdvToolBarButton;
    ATBSuporte: TAdvToolBarButton;
    ATBBLastWork: TAdvToolBarButton;
    Memo3: TMemo;
    PageControl1: TdmkPageControl;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    TimerPingServer: TTimer;
    GBAvisos1: TGroupBox;
    Panel14: TPanel;
    LaAvisoA1: TLabel;
    LaAvisoA2: TLabel;
    PB1: TProgressBar;
    TmSuporte: TTimer;
    StatusBar: TStatusBar;
    PMGeral: TPopupMenu;
    Chamadasrecebidas1: TMenuItem;
    Chamadasatendidas1: TMenuItem;
    Entidades2: TMenuItem;
    N1: TMenuItem;
    Reabrirtabelas1: TMenuItem;
    AdvToolBarOfficeStyler1: TAdvToolBarOfficeStyler;
    TySuporte: TTrayIcon;
    BalloonHint1: TBalloonHint;
    Timer1: TTimer;
    TimerAlphaBlend: TTimer;
    TimerIdle: TTimer;
    AdvPMVerifiDB: TAdvPopupMenu;
    MenuItem20: TMenuItem;
    VerificaTabelasPblicas1: TMenuItem;
    AdvPMMenuCor: TAdvPopupMenu;
    Padro3: TMenuItem;
    Office20071: TMenuItem;
    Dermatek1: TMenuItem;
    Preto1: TMenuItem;
    Azul1: TMenuItem;
    Cinza1: TMenuItem;
    Verde1: TMenuItem;
    Prscia1: TMenuItem;
    WhidbeyStyle1: TMenuItem;
    WindowsXP1: TMenuItem;
    AdvPMImagem: TAdvPopupMenu;
    MenuItem1: TMenuItem;
    Limpar1: TMenuItem;
    APGrade: TAdvPage;
    ATBGradeCad: TAdvToolBar;
    AGBGraTamCad: TAdvGlowButton;
    AGBGraCusPrc: TAdvGlowButton;
    GBReduzido: TAdvGlowButton;
    AGBTabePrcCab: TAdvGlowButton;
    ATBGradeCores: TAdvToolBar;
    AGBGraCorPan: TAdvGlowButton;
    AGBGraCorCad: TAdvGlowButton;
    AGBGraCorFam: TAdvGlowButton;
    AGBPrdgrupTip: TAdvGlowButton;
    AGBGraGruN: TAdvGlowButton;
    AdvToolBar33: TAdvToolBar;
    AGBStqInnCad: TAdvGlowButton;
    AGBStqBalCad: TAdvGlowButton;
    AGBStqManCad: TAdvGlowButton;
    AGBGraImpLista: TAdvGlowButton;
    AGBStqCenCab: TAdvGlowButton;
    AGBGraFabCad: TAdvGlowButton;
    AGBStqCenCad: TAdvGlowButton;
    AGBOpcoesGrad: TAdvGlowButton;
    AGBGraOptEnt: TAdvGlowButton;
    AdvPage2: TAdvPage;
    AdvToolBar16: TAdvToolBar;
    AdvGlowButton64: TAdvGlowButton;
    AdvGlowButton65: TAdvGlowButton;
    AdvGlowButton66: TAdvGlowButton;
    AdvGlowButton68: TAdvGlowButton;
    AdvGlowButton69: TAdvGlowButton;
    AdvToolBar17: TAdvToolBar;
    AdvGlowButton70: TAdvGlowButton;
    AdvGlowButton71: TAdvGlowButton;
    AdvGlowButton16: TAdvGlowButton;
    AdvPage5: TAdvPage;
    AdvToolBar2: TAdvToolBar;
    AGBFinancas2: TAdvGlowButton;
    AdvToolBar6: TAdvToolBar;
    AGBPlanoCtaNiveis: TAdvGlowButton;
    AGBPlanoCtaContas: TAdvGlowButton;
    AGBPlanoCtaSubgrupos: TAdvGlowButton;
    AGBPlanoCtaGrupos: TAdvGlowButton;
    AGBPlanoCtaConjuntos: TAdvGlowButton;
    AGBPlanoCtaPlanos: TAdvGlowButton;
    AdvToolBar3: TAdvToolBar;
    AGBPlanoCtaLista: TAdvGlowButton;
    AGBPlanoCtaSaldo: TAdvGlowButton;
    AdvToolBar5: TAdvToolBar;
    AGBBancos: TAdvGlowButton;
    AGBCarteiras: TAdvGlowButton;
    AGBPreLctCab: TAdvGlowButton;
    AGBContasPlanoLista: TAdvGlowButton;
    AGBCentroCustos: TAdvGlowButton;
    AGBCartNiv2: TAdvGlowButton;
    AdvPage6: TAdvPage;
    AdvToolBar13: TAdvToolBar;
    AGBPediVda: TAdvGlowButton;
    AGBPediVdaImp: TAdvGlowButton;
    AGBFatConCad: TAdvGlowButton;
    AdvToolBar4: TAdvToolBar;
    AGBPediAcc: TAdvGlowButton;
    AGBGeoSite: TAdvGlowButton;
    AGBRegioes: TAdvGlowButton;
    AGBTabePrcCab2: TAdvGlowButton;
    AGBPediPrzCab: TAdvGlowButton;
    procedure FormCreate(Sender: TObject);
    procedure AGBEntidadeClick(Sender: TObject);
    procedure TimerPingServerTimer(Sender: TObject);
    procedure TmSuporteTimer(Sender: TObject);
    procedure Chamadasrecebidas1Click(Sender: TObject);
    procedure Chamadasatendidas1Click(Sender: TObject);
    procedure Entidades2Click(Sender: TObject);
    procedure Reabrirtabelas1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure TimerIdleTimer(Sender: TObject);
    procedure TimerAlphaBlendTimer(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);
    procedure AGBListaEntiClick(Sender: TObject);
    procedure AGBMalaDiretaClick(Sender: TObject);
    procedure AGBNovasVersoesClick(Sender: TObject);
    procedure AGBRevertVersaoClick(Sender: TObject);
    procedure AGBBackupClick(Sender: TObject);
    procedure AGBOpcoesClick(Sender: TObject);
    procedure AGBMatrizClick(Sender: TObject);
    procedure AGBTemaClick(Sender: TObject);
    procedure ATBMaximizaMenuClick(Sender: TObject);
    procedure ATBSuporteClick(Sender: TObject);
    procedure ATBBackupClick(Sender: TObject);
    procedure ATBFavoritosClick(Sender: TObject);
    procedure ATBLogoffClick(Sender: TObject);
    procedure MenuItem20Click(Sender: TObject);
    procedure VerificaTabelasPblicas1Click(Sender: TObject);
    procedure AGBSuporteClick(Sender: TObject);
    procedure AGBSobreClick(Sender: TObject);
    procedure GBReduzidoClick(Sender: TObject);
    procedure AGBGraTamCadClick(Sender: TObject);
    procedure AGBGraCorCadClick(Sender: TObject);
    procedure AGBPrdgrupTipClick(Sender: TObject);
    procedure AGBGraCusPrcClick(Sender: TObject);
    procedure AGBTabePrcCabClick(Sender: TObject);
    procedure AGBGraGruNClick(Sender: TObject);
    procedure AGBGraCorFamClick(Sender: TObject);
    procedure AGBGraCorPanClick(Sender: TObject);
    procedure AGBGraImpListaClick(Sender: TObject);
    procedure AGBOpcoesGradClick(Sender: TObject);
    procedure AGBGraFabCadClick(Sender: TObject);
    procedure AGBGraOptEntClick(Sender: TObject);
    procedure AGBStqInnCadClick(Sender: TObject);
    procedure AGBStqCenCabClick(Sender: TObject);
    procedure AGBStqManCadClick(Sender: TObject);
    procedure AGBStqBalCadClick(Sender: TObject);
    procedure AGBStqCenCadClick(Sender: TObject);
    procedure AdvGlowButton69Click(Sender: TObject);
    procedure AdvGlowButton68Click(Sender: TObject);
    procedure AdvGlowButton66Click(Sender: TObject);
    procedure AdvGlowButton70Click(Sender: TObject);
    procedure AdvGlowButton65Click(Sender: TObject);
    procedure AdvGlowButton16Click(Sender: TObject);
    procedure AGBParamsEmpClick(Sender: TObject);
    procedure AGBFinancas2Click(Sender: TObject);
    procedure AGBPlanoCtaNiveisClick(Sender: TObject);
    procedure AGBPlanoCtaContasClick(Sender: TObject);
    procedure AGBPlanoCtaSubgruposClick(Sender: TObject);
    procedure AGBPlanoCtaGruposClick(Sender: TObject);
    procedure AGBPlanoCtaConjuntosClick(Sender: TObject);
    procedure AGBPlanoCtaPlanosClick(Sender: TObject);
    procedure AGBPlanoCtaListaClick(Sender: TObject);
    procedure AGBPlanoCtaSaldoClick(Sender: TObject);
    procedure AGBCarteirasClick(Sender: TObject);
    procedure AGBCartNiv2Click(Sender: TObject);
    procedure AGBBancosClick(Sender: TObject);
    procedure AGBPreLctCabClick(Sender: TObject);
    procedure AGBContasPlanoListaClick(Sender: TObject);
    procedure AGBCentroCustosClick(Sender: TObject);
    procedure AGBGeoSiteClick(Sender: TObject);
    procedure AGBRegioesClick(Sender: TObject);
    procedure AGBTabePrcCab2Click(Sender: TObject);
    procedure AGBPediAccClick(Sender: TObject);
    procedure AGBPediPrzCabClick(Sender: TObject);
    procedure AGBPediVdaClick(Sender: TObject);
    procedure AGBFatConCadClick(Sender: TObject);
    procedure AGBPediVdaImpClick(Sender: TObject);
  private
    { Private declarations }
    FBorda, FCursorPosX, FCursorPosY: Integer;
    FMenuMaximizado, FALiberar: Boolean;
    FAdvToolBarPager_Hei_Max: Integer;
    //
    procedure AppIdle(Sender: TObject; var Done: Boolean);
    procedure MostraLogoff();
  public
    { Public declarations }
    FTipoNovoEnti, FEntInt: Integer;
    FLDataIni, FLDataFim: TDateTime;
    //
    FModBloq_EntCliInt, FModBloq_CliInt, FModBloq_Peri, FModBloq_FatID,
    FModBloq_Lancto: Integer;
    FModBloq_TabLctA: String;
    FModBloq_FatNum: Double;

    procedure AcoesExtrasDeCadastroDeEntidades(Grade: TStringGrid; Codigo:
              Integer);
    procedure AcoesIniciaisDeCadastroDeEntidades(Reference: TComponent;
              Codigo: Integer; Grade: TStringGrid);
    procedure AcoesIniciaisDoAplicativo();
    procedure DefineVarsCliInt(Empresa: Integer);
    procedure MostraFormDescanso();
    procedure ReCaptionComponentesDeForm(Form: TForm);
    function  VerificaNovasVersoes(ApenasVerifica: Boolean): Boolean;
    // Financeiro
    procedure RetornoCNAB();
    procedure ShowHint(Sender: TObject);
    //
  end;

var
  FmPrincipal: TFmPrincipal;

implementation

uses
  ModuleGeral, UnMyObjects, Module, UMySQLModule, Feriados,
{$IfNDef NAO_BINA} UnBina_PF, {$EndIf}
  // , CashTabs
  UnEntities, Descanso, LinkRankSkin, UnLic_Dmk, MalaDireta, FavoritosG, About,
  UnGrade_Jan, UnEmpresas_Jan,
  GPED_Dmk, UnGFat_Jan, ModPediVda,
  UnFinanceiroJan, ModuleLct2, CentroCusto, ContasPlanoLista, PreLctCab,
  UnGPed_Jan;

{$R *.dfm}

{ TFmPrincipal }


procedure TFmPrincipal.AcoesExtrasDeCadastroDeEntidades(Grade: TStringGrid;
  Codigo: Integer);
begin
  //
end;

procedure TFmPrincipal.AcoesIniciaisDeCadastroDeEntidades(Reference: TComponent;
  Codigo: Integer; Grade: TStringGrid);
begin
//
end;

procedure TFmPrincipal.AcoesIniciaisDoAplicativo();
begin
  try
    Screen.Cursor := crHourGlass;
    //
    if DModG <> nil then
    begin
      DModG.ReopenEmpresas(VAR_USUARIO, 0);
      //
      MyObjects.Informa2(LaAviso2, LaAviso1, True, 'Criando Module Geral');
      DModG.MyPID_DB_Cria();
      //
      MyObjects.Informa2(LaAviso2, LaAviso1, True, 'Criando favoritos');
      DModG.CriaFavoritos(AdvToolBarPager1, LaAviso2, LaAviso1, AGBEntidade, FmPrincipal);
      //
  {[***VerSePrecisa***]  Importa��o de dados de outro sistema - Ver B U G S T R O L
      MyObjects.Informa2(LaAviso2, LaAviso1, True, 'Criando Module Anterior');
      DmABD_Mod.MyABD_Cria();
}
      //
      MyObjects.Informa2(LaAviso2, LaAviso1, True, 'Atualizando atrelamentos de contatos');
      DModG.AtualizaEntiConEnt();
      //
      MyObjects.Informa2(LaAviso2, LaAviso1, True, 'Setando ping ao servidor');
      TimerPingServer.Enabled := VAR_SERVIDOR = 2;
      //
      MyObjects.Informa2(LaAviso2, LaAviso1, True, 'Verificando feriados futuros');
      UMyMod.VerificaFeriadosFuturos(TFmFeriados, FmFeriados);
      //
  {[***VerSePrecisa***]  Ver B U G S T R O L
      MyObjects.Informa2(LaAviso2, LaAviso1, True, 'Carregando paletas de cores de listas de status');
      Dmod.PoeEmMemoryCoresStatusAvul();
      Dmod.PoeEmMemoryCoresStatusOS();
      //
      // Deve ser depois da paleta de cores! > Dmod.PoeEmMemoryCoresStatusOS();
      if Dmod.QrOpcoesBugsSWTAgenda.Value = 1 then
      begin
        MyObjects.Informa2(LaAviso2, LaAviso1, True, 'Criando configurando agenda em guia (aba)');
        MyObjects.FormTDICria(TFmAgendaGer, PageControl1, AdvToolBarPager1, False, True);
      end;
}
      //
      if DModG.QrCtrlGeralAtualizouPreEmail.AsInteger = 0 then
        DModG.AtualizaPreEmail;
      if DModG.QrCtrlGeralAtualizouEntidades.AsInteger = 0 then
      begin
        try
          GBAvisos1.Visible := True;
          Entities.AtualizaEntidadesParaEntidade2(PB1, Dmod.MyDB, DModG.AllID_DB);
        finally
          GBAvisos1.Visible := False;
        end;
      end;

      //

  {[***VerSePrecisa***]  Renova��es de Contratos! - Ver B U G S T R O L
      // Deixar mais para o final!!
      MyObjects.Informa2(LaAviso2, LaAviso1, True, 'Verificando a��es e renova��es');
      //DmModOS.VerificaFormulasFilhas(False);
      if DBCheck.CriaFm(TFmAllToRenew, FmAllToRenew, afmoNegarComAviso) then
      begin
        if FmAllToRenew.ItensAbertos() > 0 then
          FmAllToRenew.ShowModal;
        FmAllToRenew.Destroy;
      end;
      //
}
      DefineVarsCliInt(VAR_LIB_EMPRESA_SEL);
      //
  {[***VerSePrecisa***]  Renova��es de Contratos! - Ver B U G S T R O L
      RecriaTiposDeProdutoPadrao;
}
      DmodG.ConfiguraIconeAplicativo;
      //
{$IfDef UsaWSuport}
      DmkWeb.ConfiguraAlertaWOrdSerApp(TmSuporte, TySuporte, BalloonHint1);
      if DmkWeb.RemoteConnection() then
      begin
        if VerificaNovasVersoes(True) then
          DmkWeb.MostraBalloonHintMenuTopo(ATBVerificaNovaVersao,
          BalloonHint1, 'H� uma nova vers�o!', 'Clique aqui para atualizar!');
      end;
{$EndIf}
    end;
  finally
{$IfDef UsaWSuport}
    TmSuporte.Enabled := True;
{$EndIf}
    MyObjects.Informa2(LaAviso2, LaAviso1, False,
      Geral.FF0(VAR_LIB_EMPRESA_SEL) + ' - ' + VAR_LIB_EMPRESA_SEL_TXT);
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPrincipal.AdvGlowButton16Click(Sender: TObject);
begin
  GFat_Jan.MostraFormFatPedImp();
end;

procedure TFmPrincipal.AdvGlowButton65Click(Sender: TObject);
begin
(*&^
  if DBCheck.CriaFm(TFmFatConRet, FmFatConRet, afmoNegarComAviso) then
  begin
    FmFatConRet.ShowModal;
    FmFatConRet.Destroy;
  end;
*)
end;

procedure TFmPrincipal.AdvGlowButton66Click(Sender: TObject);
begin
  Grade_Jan.MostraFormStqManCad();
end;

procedure TFmPrincipal.AdvGlowButton68Click(Sender: TObject);
begin
  GFat_Jan.MostraFormFatDivGer();
end;

procedure TFmPrincipal.AdvGlowButton69Click(Sender: TObject);
begin
  GFat_Jan.MostraFormFatPedCab();
end;

procedure TFmPrincipal.AdvGlowButton70Click(Sender: TObject);
begin
(*&^
  if DBCheck.CriaFm(TFmSugVPedCab, FmSugVPedCab, afmoNegarComAviso) then
  begin
    FmSugVPedCab.ShowModal;
    FmSugVPedCab.Destroy;
  end;
*)
end;

procedure TFmPrincipal.AGBGeoSiteClick(Sender: TObject);
begin
  GPed_Jan.MostraFormGeoSite();
end;

procedure TFmPrincipal.AGBRegioesClick(Sender: TObject);
begin
  GPed_Jan.MostraFormRegioes();
end;

procedure TFmPrincipal.AGBGraCorCadClick(Sender: TObject);
begin
  Grade_Jan.MostraFormGraCorCad();
end;

procedure TFmPrincipal.AGBGraCorFamClick(Sender: TObject);
begin
  Grade_Jan.MostraFormGraCorFam();
end;

procedure TFmPrincipal.AGBGraCorPanClick(Sender: TObject);
begin
  Grade_Jan.MostraFormGraCorPan();
end;

procedure TFmPrincipal.AGBGraCusPrcClick(Sender: TObject);
begin
  Grade_Jan.MostraFormGraCusPrc();
end;

procedure TFmPrincipal.AGBGraFabCadClick(Sender: TObject);
begin
  Grade_Jan.MostraFormGraFabCad();
end;

procedure TFmPrincipal.AGBGraGruNClick(Sender: TObject);
begin
  Grade_Jan.MostraFormGraGruN();
end;

procedure TFmPrincipal.AGBGraImpListaClick(Sender: TObject);
begin
  Grade_Jan.MostraFormGraImpLista();
end;

procedure TFmPrincipal.AGBGraOptEntClick(Sender: TObject);
begin
  Grade_Jan.MostraFormGraOptEnt();
end;

procedure TFmPrincipal.AGBGraTamCadClick(Sender: TObject);
begin
  Grade_Jan.MostraFormGraTamCad();
end;

procedure TFmPrincipal.AGBListaEntiClick(Sender: TObject);
begin
  Entities.MostraFormEntidadesImp();
end;

procedure TFmPrincipal.AGBTabePrcCab2Click(Sender: TObject);
begin
  GFat_Jan.MostraFormTabePrcCab();
end;

procedure TFmPrincipal.AGBTabePrcCabClick(Sender: TObject);
begin
{$IfNDef NAO_GFAT}
  GFat_Jan.MostraFormTabePrcCab();
{$Else}
  dmkPF.InfoSemModulo(TDmkModuloApp.mdlappGFat);
{$EndIf}
end;

procedure TFmPrincipal.AGBTemaClick(Sender: TObject);
begin
  FmLinkRankSkin.Show;
end;

procedure TFmPrincipal.AGBNovasVersoesClick(Sender: TObject);
begin
  VerificaNovasVersoes(False);
end;

procedure TFmPrincipal.AGBBackupClick(Sender: TObject);
begin
  DModG.MostraBackup3();
end;

procedure TFmPrincipal.AGBBancosClick(Sender: TObject);
begin
  //UBancos.MostraFormBancos(0);
end;

procedure TFmPrincipal.AGBCarteirasClick(Sender: TObject);
begin
  FinanceiroJan.CadastroDeCarteiras(0);
end;

procedure TFmPrincipal.AGBCartNiv2Click(Sender: TObject);
begin
  FinanceiroJan.CadastroDeCartNiv2();
end;

procedure TFmPrincipal.AGBCentroCustosClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCentroCusto, FmCentroCusto, afmoNegarComAviso) then
  begin
    FmCentroCusto.ShowModal;
    FmCentroCusto.Destroy;
  end;
end;

procedure TFmPrincipal.AGBContasPlanoListaClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmContasPlanoLista, FmContasPlanoLista, afmoNegarComAviso) then
  begin
    FmContasPlanoLista.ShowModal;
    FmContasPlanoLista.Destroy;
  end;
end;

procedure TFmPrincipal.AGBMatrizClick(Sender: TObject);
begin
  Empresas_Jan.MostraFormMatriz();
end;

procedure TFmPrincipal.AGBOpcoesClick(Sender: TObject);
begin
//
end;

procedure TFmPrincipal.AGBOpcoesGradClick(Sender: TObject);
begin
  Grade_Jan.MostraFormOpcoesGrad();
end;

procedure TFmPrincipal.AGBParamsEmpClick(Sender: TObject);
begin
  Empresas_Jan.MostraFormParamsEmp();
end;

procedure TFmPrincipal.AGBPediAccClick(Sender: TObject);
begin
  GPed_Jan.MostraFormPediAcc();
end;

procedure TFmPrincipal.AGBPediPrzCabClick(Sender: TObject);
begin
  GFat_Jan.MostraFormPediPrzCab(0);
end;

procedure TFmPrincipal.AGBPediVdaClick(Sender: TObject);
begin
  GPed_Jan.MostraFormPediVda();
end;

procedure TFmPrincipal.AGBPediVdaImpClick(Sender: TObject);
begin
  GPed_Jan.MostraFormPediVdaImp();
end;

procedure TFmPrincipal.AGBPlanoCtaConjuntosClick(Sender: TObject);
begin
  FinanceiroJan.CadastroDeConjutos(0);
end;

procedure TFmPrincipal.AGBPlanoCtaContasClick(Sender: TObject);
begin
  FinanceiroJan.CadastroDeContas(0);
end;

procedure TFmPrincipal.AGBPlanoCtaGruposClick(Sender: TObject);
begin
  FinanceiroJan.CadastroDeGrupos(0);
end;

procedure TFmPrincipal.AGBPlanoCtaListaClick(Sender: TObject);
begin
  FinanceiroJan.ImpressaoDoPlanoDeContas();
end;

procedure TFmPrincipal.AGBPlanoCtaNiveisClick(Sender: TObject);
begin
  FinanceiroJan.CadastroDeNiveisPlano();
end;

procedure TFmPrincipal.AGBPlanoCtaPlanosClick(Sender: TObject);
begin
  FinanceiroJan.CadastroDePlano(0);
end;

procedure TFmPrincipal.AGBPlanoCtaSaldoClick(Sender: TObject);
begin
  FinanceiroJan.ImpressaoDoPlanoDeContas();
end;

procedure TFmPrincipal.AGBPlanoCtaSubgruposClick(Sender: TObject);
begin
  FinanceiroJan.CadastroDeSubGrupos(0);
end;

procedure TFmPrincipal.AGBPrdgrupTipClick(Sender: TObject);
begin
  Grade_Jan.MostraFormPrdGrupTip();
end;

procedure TFmPrincipal.AGBPreLctCabClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPreLctCab, FmPreLctCab, afmoNegarComAviso) then
  begin
    FmPreLctCab.ShowModal;
    FmPreLctCab.Destroy;
  end;
end;

procedure TFmPrincipal.AGBRevertVersaoClick(Sender: TObject);
begin
  Lic_Dmk.ReverteVersao('_G_P_E_D_', Handle);
end;

procedure TFmPrincipal.AGBSobreClick(Sender: TObject);
begin
  Application.CreateForm(TFmAbout, FmAbout);
  FmAbout.ShowModal;
  FmAbout.Destroy;
end;

procedure TFmPrincipal.AGBStqBalCadClick(Sender: TObject);
begin
  Grade_Jan.MostraFormStqBalCad(0);
end;

procedure TFmPrincipal.AGBStqCenCabClick(Sender: TObject);
begin
  Grade_Jan.MostraFormStqCenCab;
end;

procedure TFmPrincipal.AGBStqCenCadClick(Sender: TObject);
begin
  Grade_Jan.MostraFormStqCenCad();
end;

procedure TFmPrincipal.AGBStqInnCadClick(Sender: TObject);
begin
  Grade_Jan.MostraFormStqInnCad(0);
end;

procedure TFmPrincipal.AGBStqManCadClick(Sender: TObject);
begin
  Grade_Jan.MostraFormStqManCad();
end;

procedure TFmPrincipal.AGBSuporteClick(Sender: TObject);
begin
  {$IFDEF UsaWSuport}
  DmkWeb.MostraWSuporte(False, 2, BalloonHint1);
  {$ENDIF}
end;

procedure TFmPrincipal.AGBMalaDiretaClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMalaDireta, FmMalaDireta, afmoNegarComAviso) then
  begin
    FmMalaDireta.ShowModal;
    FmMalaDireta.Destroy;
  end;
end;

procedure TFmPrincipal.ATBBackupClick(Sender: TObject);
begin
  DModG.MostraBackup3();
end;

procedure TFmPrincipal.ATBMaximizaMenuClick(Sender: TObject);
begin
  MyObjects.MaximizaAdvToolBarPager(AdvToolBarPager1, deftfInverse);
end;

procedure TFmPrincipal.ATBSuporteClick(Sender: TObject);
begin
  {$IFDEF UsaWSuport}
  DmkWeb.MostraWSuporte(False, 2, BalloonHint1);
  {$ENDIF}
end;

procedure TFmPrincipal.ATBLogoffClick(Sender: TObject);
begin
  MostraLogoff;
end;

procedure TFmPrincipal.ATBFavoritosClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmFavoritosG, FmFavoritosG, afmoLiberado) then
  begin
    FmFavoritosG.ShowModal;
    FmFavoritosG.Destroy;
    DModG.CriaFavoritos(AdvToolBarPager1, LaAviso1, LaAviso2, AGBNovasVersoes, FmPrincipal);
  end;
end;

procedure TFmPrincipal.AGBEntidadeClick(Sender: TObject);
begin
  Entities.CadastroDeEntidade(0, fmCadSelecionar, fmCadSelecionar, False);
end;

procedure TFmPrincipal.AGBFatConCadClick(Sender: TObject);
begin
  //GPed_Jan.MostraFormFatConCad();
end;

procedure TFmPrincipal.AGBFinancas2Click(Sender: TObject);
var
  DmLctX: TDataModule;
begin
  if UnDmkDAC_PF.CriaDataModule(FmPrincipal, TDmLct2, DmLctX) then
    TDmLct2(DmLctX).GerenciaEmpresa(PageControl1, AdvToolBarPager1);
end;

procedure TFmPrincipal.AppIdle(Sender: TObject; var Done: Boolean);
begin
  (*
  TimerIdle.Enabled := False;
  TimerIdle.Enabled := True;
  *)
end;

procedure TFmPrincipal.Chamadasatendidas1Click(Sender: TObject);
begin
{$IfNDef NAO_BINA}
  Bina_PF.MostraFormBinaLigouB();
{$EndIf}
end;

procedure TFmPrincipal.Chamadasrecebidas1Click(Sender: TObject);
begin
{$IfNDef NAO_BINA}
  Bina_PF.MostraFormBinaLigouA();
{$EndIf}
end;

procedure TFmPrincipal.DefineVarsCliInt(Empresa: Integer);
begin
  Geral.MB_Aviso('Falta definir Vars CliInt!');
  {[***Desmarcar***]
  DmodG.QrCliIntUni.Close;
  DmodG.QrCliIntUni.Params[0].AsInteger := Empresa;
  UnDmkDAC_PF.AbreQuery(DmodG.QrCliIntUni, Dmod.MyDB);
  //
  FEntInt := DmodG.QrCliIntUniCodigo.Value;
  VAR_LIB_EMPRESAS := FormatFloat('0', DmodG.QrCliIntUniCodigo.Value);
  VAR_LIB_FILIAIS  := '';
  //
}
  //
  {[***VerSePrecisa***]  No B U G S T R O L n�o precisa!!!
  DmodFin.QrCarts.Close;
  DmodFin.QrLctos.Close;
}
end;

procedure TFmPrincipal.Entidades2Click(Sender: TObject);
begin
  UnDmkDAC_PF.ReabrirtabelasFormAtivo(Sender);
end;

procedure TFmPrincipal.FormActivate(Sender: TObject);
begin
  APP_LIBERADO := True;
  MyObjects.CorIniComponente();
  VAR_ATUALIZANDO := False;
  VAR_APPNAME := Application.Title;
  if Geral.VersaoTxt2006(CO_VERSAO) <>
    Geral.FileVerInfo(Application.ExeName, 3 (*Versao*)) then
    ShowMessage('Vers�o difere do arquivo');
  if not FALiberar then Timer1.Enabled := True;
end;

procedure TFmPrincipal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ZZTerminate := True;
  Application.Terminate;
end;

procedure TFmPrincipal.FormCreate(Sender: TObject);
var
  MenuStyle: Integer;
begin
  //VAR_USA_MODULO_CRO := True;
  //
  VAR_TemContratoMensalidade_FldCodigo := 'Codigo';
  VAR_TemContratoMensalidade_FldNome := 'Nome';
  VAR_TemContratoMensalidade_TabNome := 'contratos';
  //
  dmkPF.AcoesAntesDeIniciarApp_dmk();
  //
  FBorda := (Width - ClientWidth) div 2;
  //
  VAR_TIPO_TAB_LCT := 1;
  VAR_MULTIPLAS_TAB_LCT := True;
  //
  GERAL_MODELO_FORM_ENTIDADES := fmcadEntidade2;
  AdvToolBarPager1.ActivePageIndex := 0;
  //
  VAR_TYPE_LOG := ttlFiliLog;
  FEntInt := -1;
  VAR_USA_TAG_BITBTN := True;
  FTipoNovoEnti := 0;
  VAR_STLOGIN       := StatusBar.Panels[01];
  StatusBar.Panels[3].Text := Geral.VersaoTxt2006(CO_VERSAO);
  VAR_STTERMINAL    := StatusBar.Panels[05];
  VAR_STDATALICENCA := StatusBar.Panels[07];
  //VAR_STAVISOS      := StatusBar.Panels[09];
  VAR_SKINUSANDO    := StatusBar.Panels[09];
  VAR_STDATABASES   := StatusBar.Panels[11];
  VAR_TIPOSPRODM_TXT := '0,1,2,3,4,5,6,7,8,9,10,11,12,13';
  VAR_APP := ExtractFilePath(Application.ExeName);
  VAR_VENDEOQUE := 1;
  VAR_KIND_DEPTO := kdOS1;
  VAR_LA_PRINCIPAL1   := LaAviso1;
  VAR_LA_PRINCIPAL2   := LaAviso1;
  //
  MenuStyle := Geral.ReadAppKey('MenuStyle', Application.Title,
    ktInteger, 0, HKEY_LOCAL_MACHINE);
  MyObjects.SkinMenu(AdvToolBarOfficeStyler1, MenuStyle);
  //
  VAR_CAD_POPUP := PMGeral;
  MyObjects.CopiaItensDeMenu(PMGeral, FmPrincipal);
  //////////////////////////////////////////////////////////////////////////////
  FLDataIni := Date - Geral.ReadAppKey('Dias', Application.Title,
    ktInteger, 60, HKEY_LOCAL_MACHINE);
  FLDataFim := Date;
  //////////////////////////////////////////////////////////////////////////////
  //
  Application.OnHint      := ShowHint;
  Application.OnException := MyObjects.MostraErro;
  Application.OnMessage   := MyObjects.FormMsg;
  Application.OnIdle      := AppIdle;
  // Deixar invis�vel
  AlphaBlendValue := 0;
  AlphaBlend := True;
  //
  PageControl1.Align := alClient;
  Width := 1600;
  Height := 870;
  FAdvToolBarPager_Hei_Max := AdvToolBarPager1.Height; // 225
  //
  //  Descanso
  MostraFormDescanso();
  //  Di�rio
  FmPrincipal.WindowState := wsMaximized;
  //MyObjects.FormTDICria(TFmAgendaGer, PageControl1, AdvToolBarPager1);
  // d� erro!! vou abrir no AcoesIniciaisDoAplicativo();
  //MyObjects.MaximizaAdvToolBarPager(AdvToolBarPager1, deftfTrue);
  AdvToolBarPager1.Collaps;
  //
  FModBloq_EntCliInt := 0;
  FModBloq_CliInt    := 0;
  FModBloq_Peri      := 0;
  FModBloq_FatID     := 0;
  FModBloq_Lancto    := 0;
  FModBloq_TabLctA   := '';
  FModBloq_FatNum    := 0;
end;

procedure TFmPrincipal.FormDestroy(Sender: TObject);
begin
  if VAR_WEB_CONECTADO = 100 then
    DmkWeb.DesconectarUsuarioWEB;
end;

procedure TFmPrincipal.GBReduzidoClick(Sender: TObject);
begin
  Grade_Jan.MostraFormGraGruReduzido();
end;

procedure TFmPrincipal.MenuItem20Click(Sender: TObject);
begin
  UnDmkDAC_PF.MostraFormVerifiDB();
end;

procedure TFmPrincipal.MostraFormDescanso;
begin
  MyObjects.FormTDICria(TFmDescanso, PageControl1, AdvToolBarPager1);
end;

procedure TFmPrincipal.MostraLogoff();
begin
  FmPrincipal.Enabled := False;
  //
  FmGPED_Dmk.Show;
  FmGPED_Dmk.EdLogin.Text   := '';
  FmGPED_Dmk.EdSenha.Text   := '';
  FmGPED_Dmk.EdLogin.SetFocus;
end;

procedure TFmPrincipal.Reabrirtabelas1Click(Sender: TObject);
begin
  UnDmkDAC_PF.ReabrirtabelasFormAtivo(Sender);
end;

procedure TFmPrincipal.ReCaptionComponentesDeForm(Form: TForm);
begin
  // N�o usa ainda!
end;

procedure TFmPrincipal.RetornoCNAB();
begin
  Geral.MB_Aviso('M�dulo n�o implementado!' + sLineBreak +
  'Solicite � Dermatek');
end;

procedure TFmPrincipal.ShowHint(Sender: TObject);
begin
  if Length(Application.Hint) > 0 then
  begin
    StatusBar.SimplePanel := True;
    StatusBar.SimpleText := Application.Hint;
  end
  else StatusBar.SimplePanel := False;
end;

procedure TFmPrincipal.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  FALiberar := True;
  FmGPED_Dmk.Show;
  Enabled := False;
  FmGPED_Dmk.Refresh;
  FmGPED_Dmk.EdSenha.Text := FmGPED_Dmk.EdSenha.Text+'*';
  FmGPED_Dmk.EdSenha.Refresh;
  FmGPED_Dmk.Refresh;
  try
    Application.CreateForm(TDmod, Dmod);
    (* Se precisar mudar caption dos componentes!
    FDmodCriado := True;
    ReCaptionComponentesDeForm(FmPrincipal);
    AdvToolBarPager1.Visible := True;
    *)
    // Tornar vis�vel
    TimerAlphaBlend.Enabled := True;
  except
    Geral.MB_Erro('Imposs�vel criar Modulo de dados');
    Application.Terminate;
    Exit;
  end;
  try
    Application.CreateForm(TDmPediVda, DmPediVda);
  except
    Geral.MB_Erro('Imposs�vel criar M�dulo de vendas');
    Application.Terminate;
    Exit;
  end;
  FmGPED_Dmk.EdSenha.Text := FmGPED_Dmk.EdSenha.Text+'*';
  FmGPED_Dmk.EdSenha.Refresh;
  FmGPED_Dmk.ReloadSkin;
  FmGPED_Dmk.EdLogin.Text := '';
  FmGPED_Dmk.EdLogin.PasswordChar := 'l';
  FmGPED_Dmk.EdSenha.Text := '';
  FmGPED_Dmk.EdSenha.Refresh;
  FmGPED_Dmk.EdLogin.ReadOnly := False;
  FmGPED_Dmk.EdSenha.ReadOnly := False;
  FmGPED_Dmk.EdLogin.SetFocus;
  //FmGPED_Dmk.ReloadSkin;
  FmGPED_Dmk.Refresh;
end;

procedure TFmPrincipal.TimerAlphaBlendTimer(Sender: TObject);
begin
  if AlphaBlendValue < 255 then
    AlphaBlendValue := AlphaBlendValue + 1
  else begin
    TimerAlphaBlend.Enabled := False;
    AlphaBlend := False;
  end;
end;

procedure TFmPrincipal.TimerIdleTimer(Sender: TObject);
var
  Dia: Integer;
begin
  TimerIdle.Enabled := False;
  Dia := Geral.ReadAppKey('VeriNetVersao', Application.Title, ktInteger, 0,
    HKEY_LOCAL_MACHINE);
  if (Dia > 2) and (Dia < Int(Date)) then
  begin
    if not VerificaNovasVersoes(True) then
      Application.Terminate;
  end else
    Application.Terminate;
end;

procedure TFmPrincipal.TimerPingServerTimer(Sender: TObject);
var
  Qry: TmySQLQuery;
begin
  // Ping no mysql a cada 30 minutos (10800000 )
  Qry := TmySQLQuery.Create(Dmod);
  try
    Qry.Database := Dmod.MyDB;
    Qry.SQL.Clear;
    Qry.SQL.Add('SELECT 1 Um');
    UnDmkDAC_PF.AbreQuery(Qry, Dmod.MyDB);
    Qry.Close;
  finally
    Qry.Free;
  end;
end;

procedure TFmPrincipal.TmSuporteTimer(Sender: TObject);
begin
  {$IFDEF UsaWSuport}
  DmkWeb.AtualizaSolicitApl2(Dmod.QrUpd, Dmod.MyDB, TmSuporte, TySuporte,
    ATBSuporte, BalloonHint1);
  {$ENDIF}
end;

function TFmPrincipal.VerificaNovasVersoes(ApenasVerifica: Boolean): Boolean;
var
  Versao: Integer;
begin
  Result := DmkWeb.VerificaAtualizacaoVersao2(True, True, '[***NomeApp***]',
    '[***NomeApp***]', Geral.SoNumero_TT(DModG.QrMasterCNPJ.Value), CO_VERSAO,
    CO_DMKID_APP, DModG.ObtemAgora(), Memo3, dtExec, Versao, False, ApenasVerifica,
    BalloonHint1);
end;

procedure TFmPrincipal.VerificaTabelasPblicas1Click(Sender: TObject);
begin
  UnDmkDAC_PF.MostraFormVerifiDBTerceiros();
end;

end.
