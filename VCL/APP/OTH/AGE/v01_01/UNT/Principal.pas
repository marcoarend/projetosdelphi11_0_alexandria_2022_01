unit Principal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.StdCtrls, Vcl.Buttons, Vcl.ComCtrls, Vcl.Grids, Vcl.ExtCtrls,
  Vcl.Imaging.pngimage, Vcl.Menus,
  // Terceiros
  WinSkinStore, WinSkinData,
  AdvToolBar, AdvShapeButton, AdvGlowButton, AdvToolBarStylers,
  mySQLDbTables,
  // Dermatek
  dmkPageControl, dmkGeral, UnDmkEnums, UnInternalConsts, UnDmkProcFunc, ZCF2,
  MyListas, MyDBCheck, UnDmkWeb, DmkDAC_PF, AdvMenus;

type
  TFmPrincipal = class(TForm)
    sd1: TSkinData;
    SkinStore1: TSkinStore;
    AdvToolBarPager1: TAdvToolBarPager;
    AdvToolBarPager23: TAdvPage;
    AdvToolBar1: TAdvToolBar;
    AGBEntidade: TAdvGlowButton;
    AdvPage3: TAdvPage;
    AdvToolBar7: TAdvToolBar;
    AdvGlowButton9: TAdvGlowButton;
    AGBEntidadesImp: TAdvGlowButton;
    AdvPage1: TAdvPage;
    AdvToolBar20: TAdvToolBar;
    AGBAtualizaVersao: TAdvGlowButton;
    AGBRevertVersao: TAdvGlowButton;
    AdvToolBar8: TAdvToolBar;
    AGBVerifiDB: TAdvGlowButton;
    AGBBackup: TAdvGlowButton;
    AdvToolBar9: TAdvToolBar;
    AGBMatriz: TAdvGlowButton;
    AGBOpcoes: TAdvGlowButton;
    AGBFiliais: TAdvGlowButton;
    AdvToolBar25: TAdvToolBar;
    AGBImagem: TAdvGlowMenuButton;
    AGBMenu: TAdvGlowMenuButton;
    AGBTema: TAdvGlowButton;
    AdvPage4: TAdvPage;
    AdvToolBar10: TAdvToolBar;
    AGBSuporte: TAdvGlowButton;
    AGBSobre: TAdvGlowButton;
    AdvShapeButton1: TAdvShapeButton;
    AdvQuickAccessToolBar2: TAdvQuickAccessToolBar;
    AdvToolBarButton7: TAdvToolBarButton;
    ATBVerificaNovaVersao: TAdvToolBarButton;
    AdvToolBarButton9: TAdvToolBarButton;
    AdvToolBarButton10: TAdvToolBarButton;
    AdvToolBarButton11: TAdvToolBarButton;
    AdvToolBarButton12: TAdvToolBarButton;
    AdvToolBarButton13: TAdvToolBarButton;
    ATBBLastWork: TAdvToolBarButton;
    Memo3: TMemo;
    PageControl1: TdmkPageControl;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    TimerPingServer: TTimer;
    GBAvisos1: TGroupBox;
    Panel14: TPanel;
    LaAvisoA1: TLabel;
    LaAvisoA2: TLabel;
    PB1: TProgressBar;
    TmSuporte: TTimer;
    StatusBar: TStatusBar;
    PMGeral: TPopupMenu;
    Chamadasrecebidas1: TMenuItem;
    Chamadasatendidas1: TMenuItem;
    Entidades2: TMenuItem;
    N1: TMenuItem;
    Reabrirtabelas1: TMenuItem;
    AdvToolBarOfficeStyler1: TAdvToolBarOfficeStyler;
    TySuporte: TTrayIcon;
    BalloonHint1: TBalloonHint;
    Timer1: TTimer;
    TimerAlphaBlend: TTimer;
    TimerIdle: TTimer;
    AdvPMVerifiDB: TAdvPopupMenu;
    MenuItem20: TMenuItem;
    VerificaTabelasPblicas1: TMenuItem;
    AdvPMMenuCor: TAdvPopupMenu;
    Padro3: TMenuItem;
    Office20071: TMenuItem;
    Dermatek1: TMenuItem;
    Preto1: TMenuItem;
    Azul1: TMenuItem;
    Cinza1: TMenuItem;
    Verde1: TMenuItem;
    Prscia1: TMenuItem;
    WhidbeyStyle1: TMenuItem;
    WindowsXP1: TMenuItem;
    AdvPMImagem: TAdvPopupMenu;
    MenuItem1: TMenuItem;
    Limpar1: TMenuItem;
    AdvPage2: TAdvPage;
    ATBAgenda: TAdvToolBar;
    AGBAgendaGer: TAdvGlowButton;
    AGBAgenGrCab: TAdvGlowButton;
    AGBAgendaEve: TAdvGlowButton;
    AdvToolBar2: TAdvToolBar;
    APMListasDiversas: TAdvPopupMenu;
    Atendimento1: TMenuItem;
    EstatusdeOSs1: TMenuItem;
    Formadecontato1: TMenuItem;
    Pragas1: TMenuItem;
    Pragas2: TMenuItem;
    Gruposdepragas1: TMenuItem;
    LocaisdeAplicao1: TMenuItem;
    Atividades1: TMenuItem;
    CaixasDgua1: TMenuItem;
    Materias1: TMenuItem;
    Formas1: TMenuItem;
    Caractersticas1: TMenuItem;
    HowFound1: TMenuItem;
    Cuidados1: TMenuItem;
    Dependncias1: TMenuItem;
    Dependncias2: TMenuItem;
    iposdedependncias1: TMenuItem;
    Finalidades1: TMenuItem;
    Objetos1: TMenuItem;
    Residentes1: TMenuItem;
    iposdeConstruo1: TMenuItem;
    Vistoria1: TMenuItem;
    Abrangncias1: TMenuItem;
    Fatosgeradores1: TMenuItem;
    Frmulas1: TMenuItem;
    Servios1: TMenuItem;
    ipodeAplicaes1: TMenuItem;
    Gruposdeservios1: TMenuItem;
    Gerencia1: TMenuItem;
    Autogera1: TMenuItem;
    Novos1: TMenuItem;
    Regeratodo1: TMenuItem;
    Produtos1: TMenuItem;
    PrincpiosAtivos1: TMenuItem;
    GruposQumicos1: TMenuItem;
    Monitoramento1: TMenuItem;
    Questionrios1: TMenuItem;
    ListasdePerguntas1: TMenuItem;
    Cadastrodeperguntas1: TMenuItem;
    Atributosdeperguntas1: TMenuItem;
    extosbinrios1: TMenuItem;
    MotivosdedestivdePIPs1: TMenuItem;
    Agendaavulsa1: TMenuItem;
    Atividadesaseremexecutadas1: TMenuItem;
    Fatosgeradoresdoscompromissos1: TMenuItem;
    Statusdecompromissos1: TMenuItem;
    Geral1: TMenuItem;
    extosGenricos1: TMenuItem;
    CheckList1: TMenuItem;
    StatusdeProvidncias1: TMenuItem;
    Feriados1: TMenuItem;
    Motivospsgarantia1: TMenuItem;
    Clientes1: TMenuItem;
    Statusdefotosrelativas1: TMenuItem;
    NCTCAC1: TMenuItem;
    GerenciaContatos1: TMenuItem;
    PrCadastros1: TMenuItem;
    PsVenda1: TMenuItem;
    Mobilidade1: TMenuItem;
    Rotas1: TMenuItem;
    Mobilidade2: TMenuItem;
    Automoo1: TMenuItem;
    AdvGlowMenuButton1: TAdvGlowMenuButton;
    procedure FormCreate(Sender: TObject);
    procedure AGBEntidadeClick(Sender: TObject);
    procedure TimerPingServerTimer(Sender: TObject);
    procedure TmSuporteTimer(Sender: TObject);
    procedure Chamadasrecebidas1Click(Sender: TObject);
    procedure Chamadasatendidas1Click(Sender: TObject);
    procedure Entidades2Click(Sender: TObject);
    procedure Reabrirtabelas1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure TimerIdleTimer(Sender: TObject);
    procedure TimerAlphaBlendTimer(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);
    procedure MenuItem20Click(Sender: TObject);
    procedure VerificaTabelasPblicas1Click(Sender: TObject);
    procedure AdvGlowButton9Click(Sender: TObject);
    procedure AGBRevertVersaoClick(Sender: TObject);
    procedure AGBBackupClick(Sender: TObject);
    procedure AGBOpcoesClick(Sender: TObject);
    procedure AGBTemaClick(Sender: TObject);
    procedure Padro3Click(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure Limpar1Click(Sender: TObject);
    procedure AGBEntidadesImpClick(Sender: TObject);
    procedure AGBAtualizaVersaoClick(Sender: TObject);
    procedure AGBSobreClick(Sender: TObject);
    procedure AGBSuporteClick(Sender: TObject);
    procedure AGBAgendaGerClick(Sender: TObject);
    procedure AGBAgenGrCabClick(Sender: TObject);
    procedure AGBAgendaEveClick(Sender: TObject);
    procedure Atividadesaseremexecutadas1Click(Sender: TObject);
    procedure Fatosgeradoresdoscompromissos1Click(Sender: TObject);
    procedure Statusdecompromissos1Click(Sender: TObject);
    procedure AdvToolBarButton12Click(Sender: TObject);
    procedure AdvToolBarButton13Click(Sender: TObject);
    procedure AdvToolBarButton10Click(Sender: TObject);
    procedure AdvToolBarButton9Click(Sender: TObject);
    procedure AdvToolBarButton7Click(Sender: TObject);
  private
    { Private declarations }
    FBorda, FCursorPosX, FCursorPosY: Integer;
    FMenuMaximizado, FALiberar: Boolean;
    FAdvToolBarPager_Hei_Max: Integer;
    //
    procedure AppIdle(Sender: TObject; var Done: Boolean);
    procedure ShowHint(Sender: TObject);
  public
    { Public declarations }
    FTipoNovoEnti, FEntInt: Integer;
    FLDataIni, FLDataFim: TDateTime;
    //
    FModBloq_EntCliInt, FModBloq_CliInt, FModBloq_Peri, FModBloq_FatID,
    FModBloq_Lancto: Integer;
    FModBloq_TabLctA: String;
    FModBloq_FatNum: Double;

    procedure AcoesExtrasDeCadastroDeEntidades(Grade: TStringGrid; Codigo:
              Integer);
    procedure AcoesIniciaisDeCadastroDeEntidades(Reference: TComponent;
              Codigo: Integer; Grade: TStringGrid);
    procedure AcoesIniciaisDoAplicativo();
    procedure DefineVarsCliInt(Empresa: Integer);
    procedure MostraFormDescanso();
    procedure MostraLogoff();
    procedure ReCaptionComponentesDeForm(Form: TForm);
    function  VerificaNovasVersoes(ApenasVerifica: Boolean): Boolean;
  end;

var
  FmPrincipal: TFmPrincipal;

implementation

uses
  AGE_Dmk, // [***App***]
  UnEntities, ModuleGeral, UnMyObjects, Module, UMySQLModule, Feriados,
{$IfNDef NAO_BINA} UnBina_PF, {$EndIf}
  // , CashTabs
  Descanso, LinkRankSkin, UnLic_Dmk, Maladireta, About,
  UnAgendaGerAll, FavoritosG;

{$R *.dfm}

{ TFmPrincipal }


procedure TFmPrincipal.AcoesExtrasDeCadastroDeEntidades(Grade: TStringGrid;
  Codigo: Integer);
begin
  //
end;

procedure TFmPrincipal.AcoesIniciaisDeCadastroDeEntidades(Reference: TComponent;
  Codigo: Integer; Grade: TStringGrid);
begin
//
end;

procedure TFmPrincipal.AcoesIniciaisDoAplicativo();
begin
  try
    Screen.Cursor := crHourGlass;
    //
    if DModG <> nil then
    begin
      DModG.ReopenEmpresas(VAR_USUARIO, 0);
      //
      MyObjects.Informa2(LaAviso2, LaAviso1, True, 'Criando Module Geral');
      DModG.MyPID_DB_Cria();
      //
      MyObjects.Informa2(LaAviso2, LaAviso1, True, 'Criando favoritos');
      DModG.CriaFavoritos(AdvToolBarPager1, LaAviso2, LaAviso1, AGBEntidade, FmPrincipal);
      //
  {[***VerSePrecisa***]  Importa��o de dados de outro sistema - Ver B U G S T R O L
      MyObjects.Informa2(LaAviso2, LaAviso1, True, 'Criando Module Anterior');
      DmABD_Mod.MyABD_Cria();
}
      //
      MyObjects.Informa2(LaAviso2, LaAviso1, True, 'Atualizando atrelamentos de contatos');
      DModG.AtualizaEntiConEnt();
      //
      MyObjects.Informa2(LaAviso2, LaAviso1, True, 'Setando ping ao servidor');
      TimerPingServer.Enabled := VAR_SERVIDOR = 2;
      //
      MyObjects.Informa2(LaAviso2, LaAviso1, True, 'Verificando feriados futuros');
      UMyMod.VerificaFeriadosFuturos(TFmFeriados, FmFeriados);
      //
  {[***VerSePrecisa***]  Ver B U G S T R O L
      MyObjects.Informa2(LaAviso2, LaAviso1, True, 'Carregando paletas de cores de listas de status');
      Dmod.PoeEmMemoryCoresStatusAvul();
      Dmod.PoeEmMemoryCoresStatusOS();
      //
      // Deve ser depois da paleta de cores! > Dmod.PoeEmMemoryCoresStatusOS();
      if Dmod.QrOpcoesBugsSWTAgenda.Value = 1 then
      begin
        MyObjects.Informa2(LaAviso2, LaAviso1, True, 'Criando configurando agenda em guia (aba)');
        MyObjects.FormTDICria(TFmAgendaGer, PageControl1, AdvToolBarPager1, False, True);
      end;
}
      //
(*    // Nao Usa mais
      if DModG.QrCtrlGeralAtualizouPreEmail.AsInteger = 0 then
        DModG.AtualizaPreEmail;
*)
      if DModG.QrCtrlGeralAtualizouEntidades.AsInteger = 0 then
      begin
        try
          GBAvisos1.Visible := True;
          Entities.AtualizaEntidadesParaEntidade2(PB1, Dmod.MyDB, DModG.AllID_DB);
        finally
          GBAvisos1.Visible := False;
        end;
      end;

      //

  {[***VerSePrecisa***]  Renova��es de Contratos! - Ver B U G S T R O L
      // Deixar mais para o final!!
      MyObjects.Informa2(LaAviso2, LaAviso1, True, 'Verificando a��es e renova��es');
      //DmModOS.VerificaFormulasFilhas(False);
      if DBCheck.CriaFm(TFmAllToRenew, FmAllToRenew, afmoNegarComAviso) then
      begin
        if FmAllToRenew.ItensAbertos() > 0 then
          FmAllToRenew.ShowModal;
        FmAllToRenew.Destroy;
      end;
      //
}
      DefineVarsCliInt(VAR_LIB_EMPRESA_SEL);
      //
  {[***VerSePrecisa***]  Renova��es de Contratos! - Ver B U G S T R O L
      RecriaTiposDeProdutoPadrao;
}
      DmodG.ConfiguraIconeAplicativo;
      //
{$IfDef UsaWSuport}
      DmkWeb.ConfiguraAlertaWOrdSerApp(TmSuporte, TySuporte, BalloonHint1);
      if DmkWeb.RemoteConnection() then
      begin
        if VerificaNovasVersoes(True) then
          DmkWeb.MostraBalloonHintMenuTopo(ATBVerificaNovaVersao,
          BalloonHint1, 'H� uma nova vers�o!', 'Clique aqui para atualizar!');
      end;
{$EndIf}
    end;
  finally
{$IfDef UsaWSuport}
    TmSuporte.Enabled := True;
{$EndIf}
    MyObjects.Informa2(LaAviso2, LaAviso1, False,
      Geral.FF0(VAR_LIB_EMPRESA_SEL) + ' - ' + VAR_LIB_EMPRESA_SEL_TXT);
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPrincipal.AGBTemaClick(Sender: TObject);
begin
  FmLinkRankSkin.Show;
end;

procedure TFmPrincipal.AGBAtualizaVersaoClick(Sender: TObject);
begin
  VerificaNovasVersoes(False);
end;

procedure TFmPrincipal.AGBSobreClick(Sender: TObject);
begin
  Application.CreateForm(TFmAbout, FmAbout);
  FmAbout.ShowModal;
  FmAbout.Destroy;
end;

procedure TFmPrincipal.AGBBackupClick(Sender: TObject);
begin
  DModG.MostraBackup3();
end;

procedure TFmPrincipal.AGBOpcoesClick(Sender: TObject);
begin
  Entities.MostraFormOpcoes();
end;

procedure TFmPrincipal.AGBRevertVersaoClick(Sender: TObject);
begin
  Lic_Dmk.ReverteVersao('AGE', Handle);
end;

procedure TFmPrincipal.AGBSuporteClick(Sender: TObject);
begin
  {$IFDEF UsaWSuport}
  DmkWeb.MostraWSuporte(False, 2, BalloonHint1);
  {$ENDIF}
end;

procedure TFmPrincipal.AdvGlowButton9Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMalaDireta, FmMalaDireta, afmoNegarComAviso) then
  begin
    FmMalaDireta.ShowModal;
    FmMalaDireta.Destroy;
  end;
end;

procedure TFmPrincipal.AdvToolBarButton10Click(Sender: TObject);
begin
  DModG.MostraBackup3();
end;

procedure TFmPrincipal.AdvToolBarButton12Click(Sender: TObject);
begin
  MyObjects.MaximizaAdvToolBarPager(AdvToolBarPager1, deftfInverse);
end;

procedure TFmPrincipal.AdvToolBarButton13Click(Sender: TObject);
begin
  {$IFDEF UsaWSuport}
  DmkWeb.MostraWSuporte(False, 2, BalloonHint1);
  {$ENDIF}
end;

procedure TFmPrincipal.AdvToolBarButton7Click(Sender: TObject);
begin
  MostraLogoff();
end;

procedure TFmPrincipal.AdvToolBarButton9Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmFavoritosG, FmFavoritosG, afmoLiberado) then
  begin
    FmFavoritosG.ShowModal;
    FmFavoritosG.Destroy;
    DModG.CriaFavoritos(AdvToolBarPager1, LaAviso1, LaAviso2, AGBAgendaGer, FmPrincipal);
  end;
end;

procedure TFmPrincipal.AGBAgendaEveClick(Sender: TObject);
begin
  AgendaGerAll.MostraFormAgendaEve(0, 0, False, 0, 0, 0, 0, []);
end;

procedure TFmPrincipal.AGBAgendaGerClick(Sender: TObject);
begin
  AgendaGerAll.MostraFormAgendaGer(PageControl1, AdvToolBarPager1);
end;

procedure TFmPrincipal.AGBAgenGrCabClick(Sender: TObject);
begin
  AgendaGerAll.MostraFormAgenGrCab(0);
end;

procedure TFmPrincipal.AGBEntidadeClick(Sender: TObject);
begin
  Entities.CadastroDeEntidade(0, fmCadSelecionar, fmCadSelecionar, False);
end;

procedure TFmPrincipal.AGBEntidadesImpClick(Sender: TObject);
begin
  Entities.MostraFormEntidadesImp();
end;

procedure TFmPrincipal.AppIdle(Sender: TObject; var Done: Boolean);
begin
  (*
  TimerIdle.Enabled := False;
  TimerIdle.Enabled := True;
  *)
end;

procedure TFmPrincipal.Atividadesaseremexecutadas1Click(Sender: TObject);
begin
  AgendaGerAll.MostraFormAgenExCad();
end;

procedure TFmPrincipal.Chamadasatendidas1Click(Sender: TObject);
begin
{$IfNDef NAO_BINA}
  Bina_PF.MostraFormBinaLigouB();
{$EndIf}
end;

procedure TFmPrincipal.Chamadasrecebidas1Click(Sender: TObject);
begin
{$IfNDef NAO_BINA}
  Bina_PF.MostraFormBinaLigouA();
{$EndIf}
end;

procedure TFmPrincipal.DefineVarsCliInt(Empresa: Integer);
begin
  Geral.MB_Aviso('Falta definir Vars CliInt!');
  {[***Desmarcar***]
  DmodG.QrCliIntUni.Close;
  DmodG.QrCliIntUni.Params[0].AsInteger := Empresa;
  UnDmkDAC_PF.AbreQuery(DmodG.QrCliIntUni, Dmod.MyDB);
  //
  FEntInt := DmodG.QrCliIntUniCodigo.Value;
  VAR_LIB_EMPRESAS := FormatFloat('0', DmodG.QrCliIntUniCodigo.Value);
  VAR_LIB_FILIAIS  := '';
  //
}
  //
  {[***VerSePrecisa***]  No B U G S T R O L n�o precisa!!!
  DmodFin.QrCarts.Close;
  DmodFin.QrLctos.Close;
}
end;

procedure TFmPrincipal.Entidades2Click(Sender: TObject);
begin
  UnDmkDAC_PF.ReabrirtabelasFormAtivo(Sender);
end;

procedure TFmPrincipal.Fatosgeradoresdoscompromissos1Click(Sender: TObject);
begin
  AgendaGerAll.MostraFormAgenFgCad();
end;

procedure TFmPrincipal.FormActivate(Sender: TObject);
begin
  APP_LIBERADO := True;
  MyObjects.CorIniComponente();
  VAR_ATUALIZANDO := False;
  VAR_APPNAME := Application.Title;
  if Geral.VersaoTxt2006(CO_VERSAO) <>
    Geral.FileVerInfo(Application.ExeName, 3 (*Versao*)) then
    ShowMessage('Vers�o difere do arquivo');
  if not FALiberar then Timer1.Enabled := True;
end;

procedure TFmPrincipal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ZZTerminate := True;
  Application.Terminate;
end;

procedure TFmPrincipal.FormCreate(Sender: TObject);
var
  MenuStyle: Integer;
begin
  //
  //VAR_USA_MODULO_CRO := True;
  //
  VAR_TemContratoMensalidade_FldCodigo := 'Codigo';
  VAR_TemContratoMensalidade_FldNome := 'Nome';
  VAR_TemContratoMensalidade_TabNome := 'contratos';
  //
  dmkPF.AcoesAntesDeIniciarApp_dmk();
  //
  FBorda := (Width - ClientWidth) div 2;
  //
  VAR_TIPO_TAB_LCT := 1;
{$IfNDef NO_FINANCEIRO}
  VAR_MULTIPLAS_TAB_LCT := True;
{$EndIf}
  //
  GERAL_MODELO_FORM_ENTIDADES := fmcadEntidade2;
  AdvToolBarPager1.ActivePageIndex := 0;
  //
  VAR_TYPE_LOG := ttlFiliLog;
  FEntInt := -1;
  VAR_USA_TAG_BITBTN := True;
  FTipoNovoEnti := 0;
  VAR_STLOGIN       := StatusBar.Panels[01];
  StatusBar.Panels[3].Text := Geral.VersaoTxt2006(CO_VERSAO);
  VAR_STTERMINAL    := StatusBar.Panels[05];
  VAR_STDATALICENCA := StatusBar.Panels[07];
  //VAR_STAVISOS      := StatusBar.Panels[09];
  VAR_SKINUSANDO    := StatusBar.Panels[09];
  VAR_STDATABASES   := StatusBar.Panels[11];
  VAR_TIPOSPRODM_TXT := '0,1,2,3,4,5,6,7,8,9,10,11,12,13';
  VAR_APP := ExtractFilePath(Application.ExeName);
  VAR_VENDEOQUE := 1;
  VAR_KIND_DEPTO := kdOS1;
  VAR_LA_PRINCIPAL1   := LaAviso1;
  VAR_LA_PRINCIPAL2   := LaAviso1;
  //
  MenuStyle := Geral.ReadAppKey('MenuStyle', Application.Title,
    ktInteger, 0, HKEY_LOCAL_MACHINE);
  MyObjects.SkinMenu(AdvToolBarOfficeStyler1, MenuStyle);
  //
  VAR_CAD_POPUP := PMGeral;
  MyObjects.CopiaItensDeMenu(PMGeral, FmPrincipal);
  //////////////////////////////////////////////////////////////////////////////
  FLDataIni := Date - Geral.ReadAppKey('Dias', Application.Title,
    ktInteger, 60, HKEY_LOCAL_MACHINE);
  FLDataFim := Date;
  //////////////////////////////////////////////////////////////////////////////
  //
  Application.OnHint      := ShowHint;
  Application.OnException := MyObjects.MostraErro;
  Application.OnMessage   := MyObjects.FormMsg;
  Application.OnIdle      := AppIdle;
  // Deixar invis�vel
  AlphaBlendValue := 0;
  AlphaBlend := True;
  //
  PageControl1.Align := alClient;
  Width := 1600;
  Height := 870;
  FAdvToolBarPager_Hei_Max := AdvToolBarPager1.Height; // 225
  //
  //  Descanso
  MostraFormDescanso();
  //  Di�rio
  FmPrincipal.WindowState := wsMaximized;
  //MyObjects.FormTDICria(TFmAgendaGer, PageControl1, AdvToolBarPager1);
  // d� erro!! vou abrir no AcoesIniciaisDoAplicativo();
  //MyObjects.MaximizaAdvToolBarPager(AdvToolBarPager1, deftfTrue);
  AdvToolBarPager1.Collaps;
  //
  FModBloq_EntCliInt := 0;
  FModBloq_CliInt    := 0;
  FModBloq_Peri      := 0;
  FModBloq_FatID     := 0;
  FModBloq_Lancto    := 0;
  FModBloq_TabLctA   := '';
  FModBloq_FatNum    := 0;
end;

procedure TFmPrincipal.FormDestroy(Sender: TObject);
begin
  if VAR_WEB_CONECTADO = 100 then
    DmkWeb.DesconectarUsuarioWEB;
end;

procedure TFmPrincipal.Limpar1Click(Sender: TObject);
begin
  // Ver o que fazer
  //ImgPrincipal.Picture := nil;
  Geral.WriteAppKey('ImagemFundo', Application.Title, '', ktString, HKEY_LOCAL_MACHINE);
end;

procedure TFmPrincipal.MenuItem1Click(Sender: TObject);
var
  Imagem: String;
begin
  if MyObjects.DefineArquivoImagem(Imagem, 'Bitmaps (*.bmp)|*.bmp') then
  begin
    // ver o que fazer
    //ImgPrincipal.Picture.LoadFromFile(OpenpictureDialog1.FileName);
    Geral.WriteAppKey('ImagemFundo', Application.Title, Imagem, ktString, HKEY_LOCAL_MACHINE);
  end;
end;

procedure TFmPrincipal.MenuItem20Click(Sender: TObject);
begin
  UnDmkDAC_PF.MostraFormVerifiDB();
end;

procedure TFmPrincipal.MostraFormDescanso;
begin
  MyObjects.FormTDICria(TFmDescanso, PageControl1, AdvToolBarPager1);
end;

procedure TFmPrincipal.MostraLogoff;
begin
  FmPrincipal.Enabled := False;
  //
  FmAGE_Dmk.Show;
  FmAGE_Dmk.EdLogin.Text   := '';
  FmAGE_Dmk.EdSenha.Text   := '';
  FmAGE_Dmk.EdLogin.SetFocus;
end;

procedure TFmPrincipal.Padro3Click(Sender: TObject);
begin
  Geral.WriteAppKey('MenuStyle', Application.Title,
    TMenuItem(Sender).Tag, ktInteger, HKEY_LOCAL_MACHINE);
  MyObjects.SkinMenu(AdvToolBarOfficeStyler1, TMenuItem(Sender).Tag);
end;

procedure TFmPrincipal.Reabrirtabelas1Click(Sender: TObject);
begin
  UnDmkDAC_PF.ReabrirtabelasFormAtivo(Sender);
end;

procedure TFmPrincipal.ReCaptionComponentesDeForm(Form: TForm);
begin
  // N�o usa ainda!
end;

procedure TFmPrincipal.ShowHint(Sender: TObject);
begin
  if Length(Application.Hint) > 0 then
  begin
    StatusBar.SimplePanel := True;
    StatusBar.SimpleText := Application.Hint;
  end
  else StatusBar.SimplePanel := False;
end;

procedure TFmPrincipal.Statusdecompromissos1Click(Sender: TObject);
begin
  AgendaGerAll.MostraFormAgendaStatus();
end;

procedure TFmPrincipal.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  FALiberar := True;
  FmAGE_Dmk.Show;
  Enabled := False;
  FmAGE_Dmk.Refresh;
  FmAGE_Dmk.EdSenha.Text := FmAGE_Dmk.EdSenha.Text+'*';
  FmAGE_Dmk.EdSenha.Refresh;
  FmAGE_Dmk.Refresh;
  try
    Application.CreateForm(TDmod, Dmod);
    (* Se precisar mudar caption dos componentes!
    FDmodCriado := True;
    ReCaptionComponentesDeForm(FmPrincipal);
    AdvToolBarPager1.Visible := True;
    *)
    // Tornar vis�vel
    TimerAlphaBlend.Enabled := True;
  except
    Geral.MB_Erro('Imposs�vel criar Modulo de dados');
    Application.Terminate;
    Exit;
  end;
  {[***Desmarcar***]
  try
    Application.CreateForm(TDmPediVda, DmPediVda);
  except
    Geral.MB_Aviso(PChar('Imposs�vel criar M�dulo de vendas'), 'Erro', MB_OK+MB_ICONERROR);
    Application.Terminate;
    Exit;
  end;
}
  FmAGE_Dmk.EdSenha.Text := FmAGE_Dmk.EdSenha.Text+'*';
  FmAGE_Dmk.EdSenha.Refresh;
  FmAGE_Dmk.ReloadSkin;
  FmAGE_Dmk.EdLogin.Text := '';
  FmAGE_Dmk.EdLogin.PasswordChar := 'l';
  FmAGE_Dmk.EdSenha.Text := '';
  FmAGE_Dmk.EdSenha.Refresh;
  FmAGE_Dmk.EdLogin.ReadOnly := False;
  FmAGE_Dmk.EdSenha.ReadOnly := False;
  FmAGE_Dmk.EdLogin.SetFocus;
  //FmAGE_Dmk.ReloadSkin;
  FmAGE_Dmk.Refresh;
end;

procedure TFmPrincipal.TimerAlphaBlendTimer(Sender: TObject);
begin
  if AlphaBlendValue < 255 then
    AlphaBlendValue := AlphaBlendValue + 1
  else begin
    TimerAlphaBlend.Enabled := False;
    AlphaBlend := False;
  end;
end;

procedure TFmPrincipal.TimerIdleTimer(Sender: TObject);
var
  Dia: Integer;
begin
  TimerIdle.Enabled := False;
  Dia := Geral.ReadAppKey('VeriNetVersao', Application.Title, ktInteger, 0,
    HKEY_LOCAL_MACHINE);
  if (Dia > 2) and (Dia < Int(Date)) then
  begin
    if not VerificaNovasVersoes(True) then
      Application.Terminate;
  end else
    Application.Terminate;
end;

procedure TFmPrincipal.TimerPingServerTimer(Sender: TObject);
var
  Qry: TmySQLQuery;
begin
  // Ping no mysql a cada 30 minutos (10800000 )
  Qry := TmySQLQuery.Create(Dmod);
  try
    Qry.Database := Dmod.MyDB;
    Qry.SQL.Clear;
    Qry.SQL.Add('SELECT 1 Um');
    UnDmkDAC_PF.AbreQuery(Qry, Dmod.MyDB);
    Qry.Close;
  finally
    Qry.Free;
  end;
end;

procedure TFmPrincipal.TmSuporteTimer(Sender: TObject);
begin
  {$IFDEF UsaWSuport}
  DmkWeb.AtualizaSolicitApl2(Dmod.QrUpd, Dmod.MyDB, TmSuporte, TySuporte,
    AdvToolBarButton13, BalloonHint1);
  {$ENDIF}
end;

function TFmPrincipal.VerificaNovasVersoes(ApenasVerifica: Boolean): Boolean;
var
  Versao: Integer;
begin
  Result := DmkWeb.VerificaAtualizacaoVersao2(True, True, '_A_G_E_',
    '_A_G_E_', Geral.SoNumero_TT(DModG.QrMasterCNPJ.Value), CO_VERSAO,
    CO_DMKID_APP, DModG.ObtemAgora(), Memo3, dtExec, Versao, False,
    ApenasVerifica, BalloonHint1);
end;

procedure TFmPrincipal.VerificaTabelasPblicas1Click(Sender: TObject);
begin
  UnDmkDAC_PF.MostraFormVerifiDBTerceiros();
end;

end.
