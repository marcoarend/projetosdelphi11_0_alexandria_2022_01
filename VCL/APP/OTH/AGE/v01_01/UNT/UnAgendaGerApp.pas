unit UnAgendaGerApp;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs, DB,
  ComCtrls, (*DBTables,*) UnMyLinguas, Forms, UnInternalConsts, dmkGeral,
  Variants,
  //
  //AdvToolBar,
  //
  mySQLDbTables,
  //Planner,
  UnDmkEnums, dmkEditCB, dmkDBLookupComboBox;

type
  TUnAgendaGerApp = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    //Confirmados
    function  AgendaEveQuestaoExe_ValueToField(const QuestaoExe: Integer;
              var CampoIni, CampoFim: String): String;
    function  DadosAtividadeApp(QuestaoCod: Integer): String;
    function  IDEntidadePorApp(Entidade, QuestaoCod: Integer): Integer;
    procedure MostraFormItemAgendaApp(FormOwner: TForm; (*Planner: TPlanner;*)
              (*Item: TPlannerItem;*) QuestaoTyp, QuestaoCod, QuestaoExe: Integer);
    function  MostraFormCunsCad(Tabed: Boolean; Codigo, SiapTerCad: Integer;
              Unique: Boolean = False; Aba: Integer = 0;
              AbreSiapTerFlh: Boolean = False; AbreCroqui: Boolean = False): TForm;
    procedure ConfiguraCompsAgenGrCabDia(EdAgenGrCabDia: TdmkEditCB;
              CBAgenGrCabDia: TdmkDBLookupComboBox; QrAgenGrCabDia: TmySQLQuery);
    // A confirmar
{
    function  AgendaEveQuestaoExe_ValueToText(ID, QuestaoExe: Integer): String;
    function  AgendaEveQuestaoExe_ArrayDeAspasDuplas(): String;
    function  AtualizaHorarioCompromisso(Inicio, Termino: String; QuestaoTyp,
              QuestaoCod, QuestaoExe, Agente, ID_OSAge: Integer; MudouAgente:
              Boolean): Boolean;
    function  ItemBandToItemTime(Banda, Unidades: Integer; Data: TDateTime;
              Position: Integer): TDateTime;
    function  MontagemTextoCompromissoApp(QuestaoCod, QuestaoExe,
              Terceiro: Integer; NO_ENT: String; FatoGeradr: Integer;
              NO_FATOGERADR: String): String;
    procedure MostraFormAgenGrCab(Codigo: Integer);
    procedure MostraFormAgendaEve(QuestaoCod, Entidade: Integer; AgendaNovo:
              Boolean; DataInicio: TDateTime; HoraInicio: TTime;
              DataTermino: TDateTime; HoraTermino: TTime;
              Participantes: array of Integer);
    procedure MostraFormAgendaGer(PageControl1: TPageControl;
              AdvToolBarPager1: TAdvToolBarPager);
    procedure MostraFormAgenExCad();
    procedure MostraFormAgenFgCad();
    procedure MostraFormAgendaStatus();
}
    //
  end;

//const

var
  AgendaGerApp: TUnAgendaGerApp;

implementation

uses UnMyObjects, Module, UMySQLModule, DmkDAC_PF, MyDBCheck, AgenGrCab,
(*OSCab2, ModOS, OSUnit, CadAnyStatus, *) // Forms especificos!!!
AgendaEve, AgendaGer, CfgCadLista;

{ TUnAgendaGerApp }

const
  FMultinstancia = True;

{
function TUnAgendaGerApp.AgendaEveQuestaoExe_ArrayDeAspasDuplas(): String;
var
  I: Integer;
begin
  Result := '';
  for I := 0 to  Integer(aagDesconhecido) do
    Result := Result + ', "' + AgendaEveQuestaoExe_ValueToText(0, I) + '"';
  Result := Copy(Result, 3);
end;
}

function TUnAgendaGerApp.AgendaEveQuestaoExe_ValueToField(const QuestaoExe:
Integer; var CampoIni, CampoFim: String): String;
var
 AgendaAfazer: TAgendaAfazer;
 // = (aagIndefinido, aagVistoria, aagExecucao);
begin
  CampoIni := '???';
  CampoFim := '???';
  //
  AgendaAFAzer := TAgendaAfazer(QuestaoExe);
  case AgendaAFazer of
    //(*0*)aagIndefinido : Result := 'Indefinido';
    (*1*)aagVistoria   : begin CampoIni := 'DtaVisPrv'; CampoFim := 'FimVisPrv'; end;
    (*2*)aagExecucao   : begin CampoIni := 'DtaExePrv'; CampoFim := 'FimExePrv'; end;
    //else                 Result := 'Desconhecido';
  end;
end;

{
function TUnAgendaGerApp.AgendaEveQuestaoExe_ValueToText(
  ID, QuestaoExe: Integer): String;
var
 AgendaAfazer: TAgendaAfazer;
 // = (aagIndefinido, aagVistoria, aagExecucao);
begin
  AgendaAFAzer := TAgendaAfazer(QuestaoExe);
  case AgendaAFazer of
    (*0*)aagIndefinido    : Result := 'Indefinido';
    (*1*)aagVistoria      : Result := 'Vistoria';
    (*2*)aagExecucao      : Result := 'Execu��o';
    (*3*)aagEscalonamento : Result := 'Escalonamento';
    else                    Result := 'Desconhecido';
  end;
  if ID <> 0 then
  begin
    case AgendaAFazer of
      (*0*)aagIndefinido    : ; //Result := 'Indefinido';
      (*1*)aagVistoria      : Result := 'OS ' + Geral.FF0(ID) + ' ' + Result;
      (*2*)aagExecucao      : Result := 'OS ' + Geral.FF0(ID) + ' ' + Result;
      (*3*)aagEscalonamento : Result := 'Ctrl ' + Geral.FF0(ID) + ' ' + Result;
      else                    Result := '?? ' + Geral.FF0(ID) + ' ' + Result;
    end;
  end;
end;

function  TUnAgendaGerApp.AtualizaHorarioCompromisso(Inicio, Termino: String;
  QuestaoTyp, QuestaoCod, QuestaoExe, Agente, ID_OSAge: Integer; MudouAgente:
  Boolean): Boolean;
var
  CampoIni, CampoFim: String;
  Codigo, Controle: Integer;
begin
  Result := False;
  case TAgendaQuestao(QuestaoTyp) of
    //(*0*)qagIndefinido : ; // nada
    //(*1*)qagAvulso     : ;  //nada?;
    (*2*)qagOSBgstrl   :
    begin
      Codigo  := QuestaoCod;
      AgendaEveQuestaoExe_ValueToField(QuestaoExe, CampoIni, CampoFim);
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'oscab', False, [
      CampoIni, CampoFim], ['Codigo'], [
      Inicio, Termino], [Codigo], True);
      //
      if MudouAgente then
      begin
        if ID_OSAGe <> 0 then
        begin
          Controle := ID_OSAGe;
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'osage', False, [
          'Agente'], ['Controle'], [
          Agente], [Controle], True);
        end else Geral.MB_ERRO('ID do Agente n�o definido em "AtualizaHorarioCompromisso()"');
      end;
      //
      Result := True;
    end;
    else Geral.MB_Erro(
    '"QuestaoTyp" n�o definido na procedure "AtualizaHorarioCompromisso()"');
  end;
end;
}

procedure TUnAgendaGerApp.ConfiguraCompsAgenGrCabDia(EdAgenGrCabDia: TdmkEditCB;
              CBAgenGrCabDia: TdmkDBLookupComboBox; QrAgenGrCabDia: TmySQLQuery);
begin
  // Compatibilidade!
{
  Dmod.ReopenOpcoesFiliPelaEmpresa(VAR_LIB_EMPRESA_SEL);
  if QrAgenGrCabDia.Locate('Codigo', Dmod.QrOpcoesFiliAgenDGrCab.Value, []) then
  begin
    EdAgenGrCabDia.ValueVariant := Dmod.QrOpcoesFiliAgenDGrCab.Value;
    CBAgenGrCabDia.KeyValue     := Dmod.QrOpcoesFiliAgenDGrCab.Value;
  end;
}
end;

function TUnAgendaGerApp.DadosAtividadeApp(QuestaoCod: Integer): String;
begin
  // ver exemplo no B U G S T R O L >>>> DmModOS.DadosOSBgstrl(QuestaoCod);
  Result := '';
end;

function TUnAgendaGerApp.IDEntidadePorApp(Entidade,
  QuestaoCod: Integer): Integer;
begin
  // ver exemplo no B U G S T R O L >>>> IDAgenteNaOS(Entidade, QuestaoCod: Integer): Integer;
  Result := 0;
end;

{
function TUnAgendaGerApp.ItemBandToItemTime(Banda, Unidades: Integer;
  Data: TDateTime; Position: Integer): TDateTime;
begin
  Result := Int(Data) + Position + (Banda * Unidades / (24 * 60));
end;

function TUnAgendaGerApp.MontagemTextoCompromissoApp(QuestaoCod, QuestaoExe,
Terceiro: Integer; NO_ENT: String; FatoGeradr: Integer; NO_FATOGERADR:
String): String;
var
  TxtOS, TxtCL: String;
begin
  if QuestaoExe <> 0 then
    //TxtOS := 'OS ' + Geral.FF0(QuestaoCod) + ' ' +
    TxtOS := AgendaEveQuestaoExe_ValueToText(QuestaoCod, QuestaoExe)
  else
    TxtOS := '';
  //
  if Terceiro <> 0 then
    TxtCL := 'CL' + Geral.FF0(Terceiro) + '-' + NO_ENT
  else
    TxtCL := '';
  //
  if VAR_ORDEM_TXT_POPUP_AGENDA = 0 then
    Result := Trim(TxtOS + ' ' + TxtCL)
  else
    Result := Trim(TxtCL + ' ' + TxtOS);
  //
  if (FatoGeradr <> 0) and (NO_FATOGERADR <> '') then
    Result := Result + ' FG' + Geral.FF0(FatoGeradr) + '-' + NO_FATOGERADR;
end;

procedure TUnAgendaGerApp.MostraFormAgendaEve(QuestaoCod, Entidade: Integer;
  AgendaNovo: Boolean; DataInicio: TDateTime; HoraInicio: TTime;
  DataTermino: TDateTime; HoraTermino: TTime; Participantes: array of Integer);
var
  I: Integer;
begin
  if DBCheck.CriaFm(TFmAgendaEve, FmAgendaEve, afmoNegarComAviso) then
  begin
    if QuestaoCod <> 0 then
    begin
      FmAgendaEve.LocCod(QuestaoCod, QuestaoCod);
      FmAgendaEve.QrAgendaEnt.Locate('Entidade', Entidade, []);
    end;
    if AgendaNovo then
    begin
      FmAgendaEve.FTerceiro   := Entidade;
      FmAgendaEve.FDataInicio := DataInicio;
      FmAgendaEve.FHoraInicio := HoraInicio;
      FmAgendaEve.FDataTermino := DataTermino;
      FmAgendaEve.FHoraTermino := HoraTermino;
      //
      SetLength(FmAgendaEve.FParticipantes, Length(Participantes));
      for I := 0 to Length(Participantes) - 1 do
        FmAgendaEve.FParticipantes[I] := Participantes[I];
      FmAgendaEve.AgendaCompromissoDeOutroForm();
      //
    end;
    FmAgendaEve.ShowModal;
    FmAgendaEve.Destroy;
  end;
end;

procedure TUnAgendaGerApp.MostraFormAgendaGer(PageControl1: TPageControl;
  AdvToolBarPager1: TAdvToolBarPager);
begin
  Application.MainForm.WindowState := wsMaximized;
  MyObjects.FormTDICria(TFmAgendaGer, PageControl1, AdvToolBarPager1, False);
end;

procedure TUnAgendaGerApp.MostraFormAgendaStatus;
begin
  if DBCheck.CriaFm(TFmCadAnyStatus, FmCadAnyStatus, afmoNegarComAviso,
  'AGE-STATU-001 :: Status de Compromissos') then
  begin
    FmCadAnyStatus.TbCad.TableName := 'agenstats';
    FmCadAnyStatus.ShowModal;
    FmCadAnyStatus.Destroy;
  end;
end;

procedure TUnAgendaGerApp.MostraFormAgenExCad;
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'AgenExCad', 60, ncGerlSeq1,
  'Atividades a serem executadas',
  [], False, Null, [], [], False);
end;

procedure TUnAgendaGerApp.MostraFormAgenFgCad;
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'AgenFgCad', 60, ncGerlSeq1,
  'Fatos geradores dos compromissos',
  [], False, Null, [], [], False);
end;

procedure TUnAgendaGerApp.MostraFormAgenGrCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmAgenGrCab, FmAgenGrCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmAgenGrCab.LocCod(Codigo, Codigo);
    FmAgenGrCab.ShowModal;
    FmAgenGrCab.Destroy;
  end;
end;
}

function TUnAgendaGerApp.MostraFormCunsCad(Tabed: Boolean; Codigo,
  SiapTerCad: Integer; Unique: Boolean = False; Aba: Integer = 0;
  AbreSiapTerFlh: Boolean = False; AbreCroqui: Boolean = False): TForm;
begin
  Geral.MB_Aviso('Aplicativo sem janela de cadastro de cliente!');
end;

procedure TUnAgendaGerApp.MostraFormItemAgendaApp(FormOwner: TForm;(* Planner: TPlanner;*)
  (*Item: TPlannerItem;*) QuestaoTyp, QuestaoCod, QuestaoExe: Integer);
{
var
  Form: TForm;
  TabSheet: TTabSheet;
  PageControl: TPageControl;
  I(*, P*): Integer;
  Achou: Boolean;
  OSCab: Integer;
  //
  Qry: TmySQLQuery;
}
begin
{
  OSCab := 0;
  case TAgendaQuestao(QuestaoTyp) of
    (*2*)qagOSBgstrl:
    begin
      Achou := False;
      case TAgendaAfazer(QuestaoExe) of
        //(*0*)aagIndefinido    : OSCab := 0;
        (*1*)aagVistoria      : OSCab := QuestaoCod;
        (*2*)aagExecucao      : OSCab := QuestaoCod;
        (*3*)aagEscalonamento :
        begin
          Qry := TmySQLQuery.Create(Dmod);
          try
            UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
            'SELECT Codigo ',
            'FROM oscabxtr ',
            'WHERE Controle=' + Geral.FF0(QuestaoCod),
            '']);
            //
            OSCab := Qry.FieldByName('Codigo').AsInteger;
          finally
            Qry.Free;
          end;
        end;
        else OSCab := 0;
      end;
      //
      PageControl := TPageControl(TTabSheet(TForm(FormOwner).Owner).Owner);
      for I := 0 to PageControl.PageCount - 1 do
      begin
        Form := TForm(PageControl.Pages[I].Components[0]);
        //Geral.MB_Info(Form.Name);
        if Form is TFmOSCab2 then
        begin
          if TFmOSCab2(Form).QrOSCabCodigo.Value = OSCab then
          begin
            PageControl.ActivePage := PageControl.Pages[I];
            PageControl.ActivePageIndex := PageControl.Pages[I].PageIndex;
            Achou := PageControl.ActivePageIndex = I;
            Break;
          end;
        end;
      end;
      if not Achou then
      begin
        TabSheet := TTabSheet(FormOwner.Owner);
        Form := MyObjects.FormTDICria(TFmOSCab2, FormOwner, nil);
        UnOSUnit.ReopenLocOS_Idx(DmModOS.QrLocOS, OSCab);
        (*

        TFmOSCab2(Form).LocCod(OSCab, OSCab, dispIndefinido,
          DmModOS.QrLocOSSiapTerCad.Value, DmModOS.QrLocOSOpcao.Value, True,
          DmModOS.QrLocOSCodigo.Value);
        *)
        TFmOSCab2(Form).LocCod(
          DmModOS.QrLocOSGrupo.Value,
          DmModOS.QrLocOSGrupo.Value,
          dispIndefinido,
          DmModOS.QrLocOSSiapTerCad.Value,
          DmModOS.QrLocOSOpcao.Value,
          True,
          DmModOS.QrLocOSCodigo.Value);
        TFmOSCab2(Form).CSTabSheetChamou.Component := TabSheet;
      end;
    end;
    else Geral.MB_Erro(
      '"QuestaoTyp" n�o implementado no procedimento "MostraFormItemAgenda()"');
  end;
}
  Geral.MB_Aviso('Aplicativo sem janela de visualiza��o definida!');
end;

end.
