object Dmod: TDmod
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 530
  Width = 785
  object MyDB: TmySQLDatabase
    DatabaseName = '_f_i_n_'
    UserName = 'root'
    ConnectOptions = []
    ConnectionCharacterSet = 'latin1'
    ConnectionCollation = 'latin1_swedish_ci'
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'DatabaseName=_f_i_n_'
      'UID=root')
    DatasetOptions = []
    Left = 52
    Top = 16
  end
  object QrUpd: TmySQLQuery
    Database = MyDB
    Left = 52
    Top = 68
  end
  object QrAux: TmySQLQuery
    Database = MyDB
    Left = 52
    Top = 120
  end
  object QrMas: TmySQLQuery
    Database = MyDB
    Left = 148
    Top = 320
  end
  object QrSQL: TmySQLQuery
    Database = MyDB
    Left = 120
    Top = 364
  end
  object QrIdx: TmySQLQuery
    Database = MyDB
    Left = 120
    Top = 320
  end
  object QrNTV: TmySQLQuery
    Database = MyDB
    Left = 160
    Top = 52
  end
  object MyDBn: TmySQLDatabase
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    DatasetOptions = []
    Left = 420
    Top = 4
  end
  object QrControle: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM Controle')
    Left = 44
    Top = 288
    object QrControlePaperLef: TIntegerField
      FieldName = 'PaperLef'
      Origin = 'controle.PaperLef'
    end
    object QrControlePaperTop: TIntegerField
      FieldName = 'PaperTop'
      Origin = 'controle.PaperTop'
    end
    object QrControlePaperHei: TIntegerField
      FieldName = 'PaperHei'
      Origin = 'controle.PaperHei'
    end
    object QrControlePaperWid: TIntegerField
      FieldName = 'PaperWid'
      Origin = 'controle.PaperWid'
    end
    object QrControlePaperFcl: TIntegerField
      FieldName = 'PaperFcl'
      Origin = 'controle.PaperFcl'
    end
    object QrControleMoeda: TWideStringField
      FieldName = 'Moeda'
      Size = 4
    end
    object QrControleCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrControleVersao: TIntegerField
      FieldName = 'Versao'
    end
    object QrControleDono: TIntegerField
      FieldName = 'Dono'
    end
    object QrControleUFPadrao: TIntegerField
      FieldName = 'UFPadrao'
    end
    object QrControleTravaCidade: TSmallintField
      FieldName = 'TravaCidade'
    end
    object QrControleCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrControleCidade: TWideStringField
      FieldName = 'Cidade'
      Size = 100
    end
    object QrControleVerBcoTabs: TIntegerField
      FieldName = 'VerBcoTabs'
    end
    object QrControleMyPerJuros: TFloatField
      FieldName = 'MyPerJuros'
    end
    object QrControleMyPerMulta: TFloatField
      FieldName = 'MyPerMulta'
    end
    object QrControleContraSenha: TWideStringField
      FieldName = 'ContraSenha'
      Size = 50
    end
    object QrControleMoraDD: TFloatField
      FieldName = 'MoraDD'
    end
    object QrControleMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrControleEntraSemValor: TSmallintField
      FieldName = 'EntraSemValor'
    end
    object QrControleMensalSempre: TSmallintField
      FieldName = 'MensalSempre'
    end
    object QrControleCorRecibo: TIntegerField
      FieldName = 'CorRecibo'
    end
    object QrControleIdleMinutos: TIntegerField
      FieldName = 'IdleMinutos'
    end
    object QrControleMyPagCar: TSmallintField
      FieldName = 'MyPagCar'
    end
    object QrControleMyPgParc: TSmallintField
      FieldName = 'MyPgParc'
    end
    object QrControleMyPgQtdP: TSmallintField
      FieldName = 'MyPgQtdP'
    end
    object QrControleMyPgPeri: TSmallintField
      FieldName = 'MyPgPeri'
    end
    object QrControleMyPgDias: TSmallintField
      FieldName = 'MyPgDias'
    end
    object QrControleVendaCartPg: TIntegerField
      FieldName = 'VendaCartPg'
    end
    object QrControleVendaParcPg: TIntegerField
      FieldName = 'VendaParcPg'
    end
    object QrControleVendaPeriPg: TIntegerField
      FieldName = 'VendaPeriPg'
    end
    object QrControleVendaDiasPg: TIntegerField
      FieldName = 'VendaDiasPg'
    end
    object QrControleCNABCtaTar: TIntegerField
      FieldName = 'CNABCtaTar'
    end
    object QrControleCNABCtaJur: TIntegerField
      FieldName = 'CNABCtaJur'
    end
    object QrControleCNABCtaMul: TIntegerField
      FieldName = 'CNABCtaMul'
    end
    object QrControleMeuLogoPath: TWideStringField
      FieldName = 'MeuLogoPath'
      Size = 255
    end
    object QrControleLogoNF: TWideStringField
      FieldName = 'LogoNF'
      Size = 255
    end
    object QrControleLogoBig1: TWideStringField
      FieldName = 'LogoBig1'
      Size = 255
    end
    object QrControleMoedaBr: TIntegerField
      FieldName = 'MoedaBr'
    end
    object QrControleSoMaiusculas: TWideStringField
      FieldName = 'SoMaiusculas'
      Origin = 'controle.SoMaiusculas'
      Size = 1
    end
    object QrControleMyPagTip: TSmallintField
      FieldName = 'MyPagTip'
    end
    object QrControleBLQ_TopoAvisoV: TIntegerField
      FieldName = 'BLQ_TopoAvisoV'
    end
    object QrControleBLQ_MEsqAvisoV: TIntegerField
      FieldName = 'BLQ_MEsqAvisoV'
    end
    object QrControleBLQ_AltuAvisoV: TIntegerField
      FieldName = 'BLQ_AltuAvisoV'
    end
    object QrControleBLQ_LargAvisoV: TIntegerField
      FieldName = 'BLQ_LargAvisoV'
    end
    object QrControleBLQ_TopoDestin: TIntegerField
      FieldName = 'BLQ_TopoDestin'
    end
    object QrControleBLQ_MEsqDestin: TIntegerField
      FieldName = 'BLQ_MEsqDestin'
    end
    object QrControleBLQ_AltuDestin: TIntegerField
      FieldName = 'BLQ_AltuDestin'
    end
    object QrControleBLQ_LargDestin: TIntegerField
      FieldName = 'BLQ_LargDestin'
    end
  end
  object QrNTI: TmySQLQuery
    Database = MyDBn
    Left = 160
    Top = 100
  end
  object QrPriorNext: TmySQLQuery
    Database = MyDB
    Left = 148
    Top = 364
  end
  object QrAgora: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT YEAR(NOW()) ANO, MONTH(NOW()) MES,'
      'DAYOFMONTH(NOW()) DIA,'
      'HOUR(NOW()) HORA, MINUTE(NOW()) MINUTO,'
      'SECOND(NOW()) SEGUNDO, NOW() AGORA')
    Left = 48
    Top = 168
    object QrAgoraANO: TLargeintField
      FieldName = 'ANO'
    end
    object QrAgoraMES: TLargeintField
      FieldName = 'MES'
    end
    object QrAgoraDIA: TLargeintField
      FieldName = 'DIA'
    end
    object QrAgoraHORA: TLargeintField
      FieldName = 'HORA'
    end
    object QrAgoraMINUTO: TLargeintField
      FieldName = 'MINUTO'
    end
    object QrAgoraSEGUNDO: TLargeintField
      FieldName = 'SEGUNDO'
    end
    object QrAgoraAGORA: TDateTimeField
      FieldName = 'AGORA'
      Required = True
    end
  end
  object QrTerminal: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT * FROM Terminais'
      'WHERE IP=:P0')
    Left = 516
    Top = 160
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTerminalIP: TWideStringField
      FieldName = 'IP'
      Size = 15
    end
    object QrTerminalTerminal: TIntegerField
      FieldName = 'Terminal'
    end
  end
  object QrUpdU: TmySQLQuery
    Database = MyDB
    Left = 104
    Top = 68
  end
  object MyLocDatabase: TmySQLDatabase
    Host = '127.0.0.1'
    ConnectOptions = []
    KeepConnection = False
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'Host=127.0.0.1')
    DatasetOptions = []
    Left = 264
    Top = 7
  end
  object ZZDB: TmySQLDatabase
    Host = '127.0.0.1'
    ConnectOptions = []
    KeepConnection = False
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'Host=127.0.0.1')
    DatasetOptions = []
    Left = 96
    Top = 16
  end
  object QrUpdM: TmySQLQuery
    Database = MyDB
    Left = 104
    Top = 116
  end
  object QrAuxL: TmySQLQuery
    Database = MyDB
    Left = 264
    Top = 52
  end
  object QrMaster: TmySQLQuery
    Database = ZZDB
    AfterOpen = QrMasterAfterOpen
    OnCalcFields = QrMasterCalcFields
    SQL.Strings = (
      'SELECT ma.Em, te.Tipo, te.Logo,  te.Logo2,cm.Dono, cm.Versao, '
      'cm.CNPJ, te.IE, te.ECidade, uf.Nome NOMEUF, te.EFax,'
      
        'te.ERua, (te.ENumero+0.000) ENumero, te.EBairro, te.ECEP, te.ECo' +
        'mpl,'
      'te.EContato, te.ECel, te.ETe1, te.ETe2, te.ETe3, te.EPais,'
      'te.Respons1, te.Respons2, ma.Limite, ma.SolicitaSenha,'
      'ma.UsaAccMngr'
      'FROM Entidades te, Controle cm, Ufs uf, Master ma'
      'WHERE te.Codigo=cm.Dono'
      
        'AND ((te.CNPJ=cm.CNPJ AND te.Tipo=0) OR (te.CPF=cm.CNPJ AND te.T' +
        'ipo=1))'
      'AND uf.Codigo=te.EUF'
      '')
    Left = 48
    Top = 216
    object QrMasterCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrMasterTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 30
      Calculated = True
    end
    object QrMasterCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Size = 15
      Calculated = True
    end
    object QrMasterEm: TWideStringField
      FieldName = 'Em'
      Origin = 'master.Em'
      Required = True
      Size = 100
    end
    object QrMasterTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'entidades.Tipo'
    end
    object QrMasterLogo: TBlobField
      FieldName = 'Logo'
      Origin = 'entidades.Logo'
      Size = 4
    end
    object QrMasterDono: TIntegerField
      FieldName = 'Dono'
      Origin = 'controle.Dono'
      Required = True
    end
    object QrMasterVersao: TIntegerField
      FieldName = 'Versao'
      Origin = 'controle.Versao'
      Required = True
    end
    object QrMasterCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Origin = 'controle.CNPJ'
      Required = True
      Size = 18
    end
    object QrMasterIE: TWideStringField
      FieldName = 'IE'
      Origin = 'entidades.IE'
      Size = 15
    end
    object QrMasterECidade: TWideStringField
      FieldName = 'ECidade'
      Origin = 'entidades.ECidade'
      Size = 15
    end
    object QrMasterNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Origin = 'ufs.Nome'
      Required = True
      Size = 2
    end
    object QrMasterEFax: TWideStringField
      FieldName = 'EFax'
      Origin = 'entidades.EFax'
    end
    object QrMasterERua: TWideStringField
      FieldName = 'ERua'
      Origin = 'entidades.ERua'
      Size = 30
    end
    object QrMasterEBairro: TWideStringField
      FieldName = 'EBairro'
      Origin = 'entidades.EBairro'
      Size = 30
    end
    object QrMasterECompl: TWideStringField
      FieldName = 'ECompl'
      Origin = 'entidades.ECompl'
      Size = 30
    end
    object QrMasterEContato: TWideStringField
      FieldName = 'EContato'
      Origin = 'entidades.EContato'
      Size = 60
    end
    object QrMasterECel: TWideStringField
      FieldName = 'ECel'
      Origin = 'entidades.ECel'
    end
    object QrMasterETe1: TWideStringField
      FieldName = 'ETe1'
      Origin = 'entidades.ETe1'
    end
    object QrMasterETe2: TWideStringField
      FieldName = 'ETe2'
      Origin = 'entidades.Ete2'
    end
    object QrMasterETe3: TWideStringField
      FieldName = 'ETe3'
      Origin = 'entidades.Ete3'
    end
    object QrMasterEPais: TWideStringField
      FieldName = 'EPais'
      Origin = 'entidades.EPais'
    end
    object QrMasterRespons1: TWideStringField
      FieldName = 'Respons1'
      Origin = 'entidades.Respons1'
      Required = True
      Size = 60
    end
    object QrMasterRespons2: TWideStringField
      FieldName = 'Respons2'
      Origin = 'entidades.Respons2'
      Required = True
      Size = 60
    end
    object QrMasterECEP: TIntegerField
      FieldName = 'ECEP'
      Origin = 'entidades.ECEP'
    end
    object QrMasterLogo2: TBlobField
      FieldName = 'Logo2'
      Origin = 'entidades.Logo2'
      Size = 4
    end
    object QrMasterLimite: TSmallintField
      FieldName = 'Limite'
      Origin = 'master.Limite'
      Required = True
    end
    object QrMasterSolicitaSenha: TIntegerField
      FieldName = 'SolicitaSenha'
      Origin = 'master.SolicitaSenha'
    end
    object QrMasterENumero: TFloatField
      FieldName = 'ENumero'
    end
    object QrMasterUsaAccMngr: TSmallintField
      FieldName = 'UsaAccMngr'
    end
  end
  object QrLocY: TmySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT MIN(Codigo) Record FROM Carteiras'
      '')
    Left = 464
    Top = 203
    object QrLocYRecord: TIntegerField
      FieldName = 'Record'
      Required = True
    end
  end
  object QrCountY: TmySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT COUNT(*) Record From  Entidades')
    Left = 464
    Top = 247
    object QrCountYRecord: TIntegerField
      FieldName = 'Record'
      Required = True
    end
  end
  object QrUpdL: TmySQLQuery
    Database = MyLocDatabase
    Left = 264
    Top = 99
  end
  object QrUpd2: TmySQLQuery
    Database = MyDB
    Left = 104
    Top = 163
  end
  object QrWeb: TmySQLQuery
    Database = MyDBn
    Left = 420
    Top = 48
  end
  object QrUpdN: TmySQLQuery
    Database = MyDBn
    Left = 420
    Top = 95
  end
end
