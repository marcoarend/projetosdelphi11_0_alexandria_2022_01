unit UnAppPF;
interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db, DbCtrls,
  UnInternalConsts2, ComCtrls, dmkGeral,
  mySQLDBTables, UnDmkEnums;

type
  TUnAppPF = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function AcaoEspecificaDeApp(Tabela: String): Boolean;
  end;

var
  AppPF: TUnAppPF;

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, ModuleGeral;


{ TUnAppPF }

function TUnAppPF.AcaoEspecificaDeApp(Tabela: String): Boolean;
begin
  //Compatibilidade
  Result := True;
end;

end.
