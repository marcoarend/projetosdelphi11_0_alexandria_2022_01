unit UnAppListas;

interface

uses UnMyLinguas, UnDmkEnums,
  // Ver como substituir
  mySQLDbTables, Vcl.StdCtrls, dmkGeral, UnDmkProcFunc,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs, Forms,
  ExtCtrls, ComCtrls, UnInternalConsts, DB, Variants;

type
  //TTipoTroca = (trcOA, trcOsAs, trcPara);
  TUnAppListas = class(TObject)
  private
    { Private declarations }
    procedure IIA(var Lista: TAppGrupLst; Valores: array of String);

  public
    { Public declarations }
    function  ObtemGeneroDeUserGenero(UserGenero: Integer): Integer;
    function  ListaNiveisPragas(): TAppGrupLst;
    function  ListaNiveisPrincipiosAtivos(): TAppGrupLst;
    function  ListaNiveisSituacoesFotosClientes(): TAppGrupLst;
    function  ReplaceVarsContratoMSWord_A(var Doc: Variant;
              QrCtr: TmySQLQuery; LaAviso1, LaAviso2: TLabel;
              ListVarsNomes, ListVarsValrs: array of String): Boolean;
    function  ReplaceVarsContratoMSWord_B(var MSWord: OleVariant;
              QrCtr: TmySQLQuery; LaAviso1, LaAviso2: TLabel;
              ListVarsNomes, ListVarsValrs: array of String;
              Extras: Integer): Boolean;
    //
    function  GeraVersaoContrato(Qry: TmySQLQuery): String;
  end;

var
  AppListas: TUnAppListas;

implementation

uses Diario_Tabs, Module, DmkDAC_PF, ModuleGeral, UnMyObjects;

{ TUnAppListas }

function TUnAppListas.GeraVersaoContrato(Qry: TmySQLQuery): String;
begin
  Result := Geral.FFN(Qry.FieldByName('Codigo').AsInteger, 6) + ' v. ' +
    Geral.FFT(Qry.FieldByName('Versao').AsFloat, 2, siPositivo);
end;

procedure TUnAppListas.IIA(var Lista: TAppGrupLst; Valores: array of String);
var
  //I,
  K: Integer;
begin
  K := Length(Lista);
  SetLength(Lista, K + 1);
  SetLength(Lista[K], 3);
  Lista[K][0] := Valores[0];
  Lista[K][1] := Valores[1];
  Lista[K][2] := Valores[2];
end;

function TUnAppListas.ListaNiveisPragas(): TAppGrupLst;
{
var
  I: Integer;
}
begin
  SetLength(Result, 0);
  //
  IIA(Result, ['Praga_A', '60', 'Grupos de Pragas']);
  IIA(Result, ['Praga_Z', '60', 'Pragas']);
end;

function TUnAppListas.ListaNiveisPrincipiosAtivos(): TAppGrupLst;
begin
  SetLength(Result, 0);
  //
  IIA(Result, ['G1PriAtiG', '60', 'Grupos Qu�micos']);
  IIA(Result, ['G1PriAtiI', '60', 'Princ�pios Ativos']);
end;

function TUnAppListas.ListaNiveisSituacoesFotosClientes(): TAppGrupLst;
begin
  SetLength(Result, 0);
  //
  IIA(Result, ['CunsImgSit', '60', 'Status de Fotos Relativas a Entidades']);
  //IIA(Result, ['??', '??', '???']);
end;

function TUnAppListas.ObtemGeneroDeUserGenero(UserGenero: Integer): Integer;
begin
  case UserGenero of
    0: Result := 0;
    1: Result := CO_DIARIO_ADD_GENERO_ORD_SRV;
    else Result := -1;
  end;
end;

function TUnAppListas.ReplaceVarsContratoMSWord_A(var Doc: Variant;
  QrCtr: TmySQLQuery; LaAviso1, LaAviso2: TLabel;
  ListVarsNomes, ListVarsValrs: array of String): Boolean;
begin
  //Compatibilidade
  Result := True;
end;

function TUnAppListas.ReplaceVarsContratoMSWord_B(var MSWord: OleVariant;
  QrCtr: TmySQLQuery; LaAviso1, LaAviso2: TLabel; ListVarsNomes,
  ListVarsValrs: array of String; Extras: Integer): Boolean;
begin
  //Compatibilidade
  Result := True;
end;

end.
