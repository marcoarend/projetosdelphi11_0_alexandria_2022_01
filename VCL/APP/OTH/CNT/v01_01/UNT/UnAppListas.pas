unit UnAppListas;

interface

uses UnMyLinguas, UnDmkEnums,
  // Ver como substituir
  mySQLDbTables, Vcl.StdCtrls, dmkGeral, UnDmkProcFunc,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs, Forms,
  ExtCtrls, ComCtrls, UnInternalConsts, DB, Variants(*, UnProjGroup_Consts*);

type
  TUnAppListas = class(TObject)
  private
    { Private declarations }
    //procedure IIA(var Lista: TAppGrupLst; Valores: array of String);

  public
    { Public declarations }
{
    function  ObtemGeneroDeUserGenero(UserGenero: Integer): Integer;
    function  ListaNiveisPragas(): TAppGrupLst;
    function  ListaNiveisPrincipiosAtivos(): TAppGrupLst;
    function  ListaNiveisSituacoesFotosClientes(): TAppGrupLst;
    function  ReplaceVarsContratoMSWord_A(var Doc: Variant;
              QrCtr: TmySQLQuery; LaAviso1, LaAviso2: TLabel;
              ListVarsNomes, ListVarsValrs: array of String): Boolean;
    function  ReplaceVarsContratoMSWord_B(var MSWord: OleVariant;
              QrCtr: TmySQLQuery; LaAviso1, LaAviso2: TLabel;
              ListVarsNomes, ListVarsValrs: array of String;
              Extras: Integer): Boolean;
    //
    function  GeraVersaoContrato(Qry: TmySQLQuery): String;
}
  end;

var
  AppListas: TUnAppListas;

implementation

//uses UnDiario_Tabs, Module, DmkDAC_PF, ModuleGeral, UnMyObjects;

{ TUnAppListas }

{
function TUnAppListas.GeraVersaoContrato(Qry: TmySQLQuery): String;
begin
  Result := Geral.FFN(Qry.FieldByName('Codigo').AsInteger, 6) + ' v. ' +
    Geral.FFT(Qry.FieldByName('Versao').AsFloat, 2, siPositivo);
end;

procedure TUnAppListas.IIA(var Lista: TAppGrupLst; Valores: array of String);
var
  //I,
  K: Integer;
begin
  K := Length(Lista);
  SetLength(Lista, K + 1);
  SetLength(Lista[K], 3);
  Lista[K][0] := Valores[0];
  Lista[K][1] := Valores[1];
  Lista[K][2] := Valores[2];
end;

function TUnAppListas.ListaNiveisPragas(): TAppGrupLst;
(*
var
  I: Integer;
*)
begin
  SetLength(Result, 0);
  //
  IIA(Result, ['Praga_A', '60', 'Grupos de Pragas']);
  IIA(Result, ['Praga_Z', '60', 'Pragas']);
end;

function TUnAppListas.ListaNiveisPrincipiosAtivos(): TAppGrupLst;
begin
  SetLength(Result, 0);
  //
  IIA(Result, ['G1PriAtiG', '60', 'Grupos Qu�micos']);
  IIA(Result, ['G1PriAtiI', '60', 'Princ�pios Ativos']);
end;

function TUnAppListas.ListaNiveisSituacoesFotosClientes(): TAppGrupLst;
begin
  SetLength(Result, 0);
  //
  IIA(Result, ['CunsImgSit', '60', 'Status de Fotos Relativas a Entidades']);
  //IIA(Result, ['??', '??', '???']);
end;

function TUnAppListas.ObtemGeneroDeUserGenero(UserGenero: Integer): Integer;
begin
  case UserGenero of
    0: Result := 0;
    1: Result := CO_DIARIO_ADD_GENERO_ORD_SRV;
    else Result := -1;
  end;
end;

function TUnAppListas.ReplaceVarsContratoMSWord_A(var Doc: Variant;
QrCtr: TmySQLQuery; LaAviso1, LaAviso2: TLabel;
ListVarsNomes, ListVarsValrs: array of String): Boolean;
  function Troc(Tag, Val: String): Integer;
  var
    //I: Integer;
    Continua: Boolean;
  begin
    Result := 0;
(*
procedure TFrmPrincipal.Button1Click(Sender: TObject);
var
  MSWord: OleVariant;
begin
  if not OpenDialog1.Execute then
    Exit;
  MSWord := CreateOleObject('Word.Application');
  // Ser� aberto seu documento com o parametro a ser substituido
  MSWord.Application.Documents.Open(OpenDialog1.FileName);
  // Deixo o word visivel
  MSWord.Visible := true;
  // Limpo os parametros de busca do word
  MSWord.Application.Selection.Find.ClearFormatting;
  //Adiciono no find o parametro que quero buscar dentro do word,
  //e abaixo esta selecionado as condi��es
  MSWord.Application.Selection.Find.Text := '@parametro1';
  MSWord.Application.Selection.Find.Replacement.Text := '';
  MSWord.Application.Selection.Find.Forward := true;
  MSWord.Application.Selection.Find.Format := False;
  MSWord.Application.Selection.Find.MatchCase := False;
  MSWord.Application.Selection.Find.MatchWholeWord := False;
  MSWord.Application.Selection.Find.MatchWildcards := False;
  MSWord.Application.Selection.Find.MatchSoundsLike := False;
  MSWord.Application.Selection.Find.MatchAllWordForms := False;
  // Executo o pesquisa, o parametro se estiver no texto vai ficar selecionado
  MSWord.Application.Selection.Find.Execute;
  if MSWord.Application.Selection.Find.Found then // verifica se achou
  // Substituo o parametro selecionado pelo texto que esta eu quero
  MSWord.Application.Selection.TypeText(Memo1.Lines.Text);
  end;
end.*)

    Continua := True;
    while Continua do
    begin
      Continua :=
        Doc.Content.Find.Execute(FindText := Tag, ReplaceWith := '');
      if Continua then
        Doc.Content.TypeText(Val);
    end;
  end;
  function ValI(Campo: String): Integer;
  begin
    Result := QrCtr.FieldByName(Campo).AsInteger;
  end;
  function TxtI(Tag: String; Val: Integer; Tipo: TTipoTroca): String;
  begin
    Troc(Tag, dmkPF.FormaDeTratamentoBR(Val, Tipo));
  end;
  procedure CtrI(Tag, Campo: String);
  begin
    Troc(Tag, Geral.FF0(QrCtr.FieldByName(Campo).AsInteger));
  end;
  procedure CtrS(Tag, Campo: String);
  begin
    Troc(Tag, QrCtr.FieldByName(Campo).AsString);
  end;
  function  ValF(Campo: String; Casas: Integer): String;
  begin
    Result :=
      Geral.FFT(QrCtr.FieldByName(Campo).AsFloat, Casas, siPositivo);
  end;
  procedure CtrF(Tag, Campo: String; Casas: Integer);
  begin
    Troc(Tag, ValF(Campo, Casas));
  end;
  procedure CtrD(Tag, Campo: String);
  begin
    Troc(Tag, Geral.FDT(QrCtr.FieldByName(Campo).AsDateTime, 2));
  end;
var
  I, J, K, N, ArtDefCli, ArtDefLug: Integer;
  Tag: String;
  NOME_CLI, CPFJ_EMP, CPFJ_CLI,
  //NOME_REP, CPFJ_REP,
  NOME_EMP, ENDE_EMP, ENDE_CLI, CIDA_EMP,
  //ENDE_REP, CIDA_CLI, CIDA_REP,
  TempoImpl, TempoMoni, TempoTrei, PrazoImpl, VAL_MENS, PER_MULT, PER_MORA,
  NOM_RESP, CID_RESP, CPF_RESP, RGN_RESP, RGO_RESP, RGD_RESP, Trato, TIPO_DOC,
  NOM_TES1, CPF_TES1, EN1_TES1, EN2_TES1, EN3_TES1,
  NOM_TES2, CPF_TES2, EN1_TES2, EN2_TES2, EN3_TES2: String;
  Achou: Boolean;
begin
  Result := False;
  try
    (*if Doc = Unassigned then
    begin
      Geral.MB_ERRO('O documento n�o foi apropriadamente carregado!');
      Exit;
    end;*)
    DmodG.ReopenEndereco(ValI('Contratante'));
    NOME_CLI := DmodG.QrEnderecoNOME_ENT.Value;
    CPFJ_CLI := DmodG.QrEnderecoCNPJ_TXT.Value;
    ENDE_CLI := DmodG.QrEnderecoE_ALL.Value;
    Trato    := DmodG.QrEnderecoTRATO.Value;
    if Length(Geral.SoNumero_TT(CPFJ_CLI)) >= 14 then
      TIPO_DOC := 'CNPJ'
    else
    if Length(Geral.SoNumero_TT(CPFJ_CLI)) >= 11 then
      TIPO_DOC := 'CPF'
    else
      TIPO_DOC := '';
    //
    DmodG.ReopenEndereco(ValI('Contratada'));
    NOME_EMP := DmodG.QrEnderecoNOME_ENT.Value;
    CPFJ_EMP := DmodG.QrEnderecoCNPJ_TXT.Value;
    ENDE_EMP := DmodG.QrEnderecoE_ALL.Value;
    CIDA_EMP := DmodG.QrEnderecoCIDADE.Value;
    //

    DmodG.ReopenEndereco(ValI('RepreLegal'));
    NOM_RESP := DmodG.QrEnderecoNOME_ENT.Value;
    CID_RESP := DmodG.QrEnderecoCIDADE.Value;
    CPF_RESP := DmodG.QrEnderecoCNPJ_TXT.Value;
    RGN_RESP := DmodG.QrEnderecoRG.Value;
    RGO_RESP := Trim(DmodG.QrEnderecoSSP.Value);
    if RGO_RESP <> '' then
      RGO_RESP := ' ' + RGO_RESP;
    RGD_RESP := Geral.FDT(DmodG.QrEnderecoDataRG.Value, 2);
    if RGD_RESP <> '' then
      RGD_RESP := ' emitido em ' + RGD_RESP;
    //

    //
    //DmodG.ReopenEndereco(ValI('Testemun01'));
    DModG.ObtemEnderecoEtiqueta3Linhas(ValI('Testemun01'), EN1_TES1, EN2_TES1, EN3_TES1);
    NOM_TES1 := DmodG.QrEnderecoNOME_ENT.Value;
    CPF_TES1 := DmodG.QrEnderecoCNPJ_TXT.Value;

    DModG.ObtemEnderecoEtiqueta3Linhas(ValI('Testemun02'), EN1_TES2, EN2_TES2, EN3_TES2);
    NOM_TES2 := DmodG.QrEnderecoNOME_ENT.Value;
    CPF_TES2 := DmodG.QrEnderecoCNPJ_TXT.Value;

    //

    ArtDefCli :=  ValI('ArtigoDef');
    ArtDefLug :=  ValI('LugArtDef');
    //
    K := ValI('PerDefImpl');
    N := ValI('PerQtdImpl');
    TempoImpl := Geral.FF0(N) + ' ' + dmkPF.TxtGrandezaPeriodo(K, N > 1);
    //
    K := ValI('PerDefMoni');
    N := ValI('PerQtdMoni');
    TempoMoni := Geral.FF0(N) + ' ' + dmkPF.TxtGrandezaPeriodo(K, N > 1);
    //
    K := ValI('PerDefTrei');
    N := ValI('PerQtdTrei');
    TempoTrei := Geral.FF0(N) + ' ' + dmkPF.TxtGrandezaPeriodo(K, N > 1);
    //
    K := ValI('PerDefIniI');
    N := ValI('PerQtdIniI');
    PrazoImpl := Geral.FF0(N) + ' ' + dmkPF.TxtGrandezaPeriodo(K, N > 1);
    //
    VAL_MENS := ValF('ValorMes', 2);
    VAL_MENS := VAL_MENS + ' (' + dmkPF.ExtensoMoney(VAL_MENS) + ')';
    //
    PER_MULT := ValF('PercMulta', 2);
    PER_MULT := PER_MULT + '% (' + dmkPF.ExtensoFloat(PER_MULT) + ')';
    PER_MORA := ValF('PercJuroM', 2);
    PER_MORA := PER_MORA + '% (' + dmkPF.ExtensoFloat(PER_MORA) + ')';
    //

    for I := 0 to MaxVarsContratos do
    begin
      Tag := Copy(sListaContratos[I], 1, pos(']', sListaContratos[I]));
      if Tag <> '' then
      begin
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Substituindo tag ' + Tag);
        try
          //if Tag = '[NUM_CNTR]' then CtrS(Tag, 'Numero') else
          if Tag = '[NUM_CNTR]' then Troc(Tag, GeraVersaoContrato(QrCtr)) else
          if Tag = '[NOME_CLI]' then Troc(Tag, NOME_CLI) else
          if Tag = '[TRAT_CLI]' then CtrS(Tag, 'Tratamento') else
          if Tag = '[ART_O_A_]' then TxtI(Tag, ArtDefCli, trcOA) else
          if Tag = '[ART_OSAS]' then TxtI(Tag, ArtDefCli, trcOsAs) else
          if Tag = '[PREP_ART]' then TxtI(Tag, ArtDefCli, trcPara) else
          if Tag = '[DOCU_CLI]' then Troc(Tag, CPFJ_CLI) else
          if Tag = '[ENDR_CLI]' then Troc(Tag, ENDE_CLI) else
          if Tag = '[DATA_CON]' then CtrD(Tag, 'DContrato') else
          if Tag = '[DATA_VAL]' then CtrD(Tag, 'DtaPropFim') else
          if Tag = '[TEM_IMPL]' then Troc(Tag, TempoImpl) else
          if Tag = '[TEM_MONI]' then Troc(Tag, TempoMoni) else
          if Tag = '[TEM_TREI]' then Troc(Tag, TempoTrei) else
          if Tag = '[PRZ_IMPL]' then Troc(Tag, PrazoImpl) else
          if Tag = '[VAL_MENS]' then Troc(Tag, VAL_MENS) else
          if Tag = '[DOC_PAGA]' then CtrS(Tag, 'TpPagDocu') else
          if Tag = '[LOC_ASIN]' then Troc(Tag, CIDA_EMP) else
          if Tag = '[DAT_ASIN]' then CtrS(Tag, 'DtaAssina') else
          //if Tag = '[DET_SERV]' then CtrS(Tag, 'Servicos02') else
          if Tag = '[NOM_RESP]' then Troc(Tag, NOM_RESP) else
          if Tag = '[CID_RESP]' then Troc(Tag, CID_RESP) else
          if Tag = '[CPF_RESP]' then Troc(Tag, CPF_RESP) else
          if Tag = '[RGN_RESP]' then Troc(Tag, RGN_RESP) else
          if Tag = '[RGO_RESP]' then Troc(Tag, RGO_RESP) else
          if Tag = '[RGD_RESP]' then Troc(Tag, RGD_RESP) else
          if Tag = '[DATA_INT]' then CtrD(Tag, 'DInstal') else
          if Tag = '[DATA_VEN]' then CtrD(Tag, 'DVencimento') else
          if Tag = '[DIA_PAGT]' then CtrI(Tag, 'ddMesVcto') else
          if Tag = '[PER_MULT]' then Troc(Tag, PER_MULT) else
          if Tag = '[PER_MORA]' then Troc(Tag, PER_MORA) else
          if Tag = '[NOM_TES1]' then Troc(Tag, NOM_TES1) else
          if Tag = '[CPF_TES1]' then Troc(Tag, CPF_TES1) else
          if Tag = '[EN1_TES1]' then Troc(Tag, EN1_TES1) else
          if Tag = '[EN2_TES1]' then Troc(Tag, EN2_TES1) else
          if Tag = '[EN3_TES1]' then Troc(Tag, EN3_TES1) else
          if Tag = '[NOM_TES2]' then Troc(Tag, NOM_TES2) else
          if Tag = '[CPF_TES2]' then Troc(Tag, CPF_TES2) else
          if Tag = '[EN1_TES2]' then Troc(Tag, EN1_TES2) else
          if Tag = '[EN2_TES2]' then Troc(Tag, EN2_TES2) else
          if Tag = '[EN3_TES2]' then Troc(Tag, EN3_TES2) else
          if Tag = '[EST_O_A_]' then TxtI(Tag, ArtDefLug, trcOA) else
          if Tag = '[EST_OSAS]' then TxtI(Tag, ArtDefLug, trcOsAs) else
          if Tag = '[ESTB_ART]' then TxtI(Tag, ArtDefLug, trcPara) else
          if Tag = '[EST_DESC]' then CtrS(Tag, 'LugarNome') else
          if Tag = '[LOGR_PRP]' then Troc(Tag, Trato) else
          if Tag = '[TIPO_DOC]' then Troc(Tag, TIPO_DOC) else
          begin
            //if Tag = '[SERVICOS]' then CtrS(Tag, 'Servicos01') else
            //if Tag = '[DET_SERV]' then CtrS(Tag, 'Servicos02') else
            Achou := False;
            N := High(ListVarsNomes);
            J := High(ListVarsValrs);
            if N <> J then
            begin
              Geral.MB_ERRO('ERRO! Existem ' + IntToStr(N + 1) +
              ' variaveis avulsas e '        + IntToStr(J + 1) +
              ' valores para essas vari�veis em "AppListas.ObtemGeneroDeUserGenero()"!');
              Exit;
            end;
            for N := Low(ListVarsNomes) to J do
            begin
              if Tag = ListVarsNomes[N] then
              begin
                Achou := True;
                Troc(Tag, ListVarsValrs[N]);
              end;
            end;
            if not Achou then
              Geral.MB_Aviso('Tag n�o implementada: ' + Tag);
          end;
        except
          MyObjects.Informa2(LaAviso1, LaAviso2, True, 'ERRO!!! ' + LaAviso1.Caption);
          raise;
        end;
      end;
    end;
    Result := True;
  except
    raise;
  end;
end;

function TUnAppListas.ReplaceVarsContratoMSWord_B(var MSWord: OleVariant;
  QrCtr: TmySQLQuery; LaAviso1, LaAviso2: TLabel; ListVarsNomes,
  ListVarsValrs: array of String; Extras: Integer): Boolean;
var
  Tag: String;
  function Troc(Tag, Val: String): Integer;
const
  wdFindContinue = 1;
  wdReplaceOne = 1;
  wdReplaceAll = 2;
  var
    //I: Integer;
    Continua: Boolean;
    Conta: Integer;
    Texto: String;
  begin
    Texto := Val;
    if Val = '' then
      Texto := ' ';
    //
    Result := 0;
(*
procedure TFrmPrincipal.Button1Click(Sender: TObject);
var
  MSWord: OleVariant;
begin
  if not OpenDialog1.Execute then
    Exit;
  MSWord := CreateOleObject('Word.Application');
  // Ser� aberto seu documento com o parametro a ser substituido
  MSWord.Application.Documents.Open(OpenDialog1.FileName);
  // Deixo o word visivel
  MSWord.Visible := true;
  // Limpo os parametros de busca do word
*)
    Continua := True;
    Conta := 1;
    while Continua do
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Substituindo tag ' + Tag + ' > tentativa ' + Geral.FF0(Conta));
      MSWord.Application.Selection.Find.ClearFormatting;
      //Adiciono no Selection.Find o parametro que quero buscar dentro do word,
      //e abaixo esta selecionado as condi��es
      //MSWord.Application.Selection.Selection.Find.Text := Tag;
      MSWord.Application.Selection.Find.Text := Tag;
      MSWord.Application.Selection.Find.Replacement.Text := '';
      MSWord.Application.Selection.Find.Forward := true;
      MSWord.Application.Selection.Find.Format := False;
      MSWord.Application.Selection.Find.MatchCase := False;


      MSWord.Application.Selection.Find.MatchFuzzy := False;
      MSWord.Application.Selection.Find.Wrap := wdFindContinue;


      MSWord.Application.Selection.Find.MatchWholeWord := False;
      MSWord.Application.Selection.Find.MatchWildcards := False;
      MSWord.Application.Selection.Find.MatchSoundsLike := False;
      MSWord.Application.Selection.Find.MatchAllWordForms := False;
      // Executo o pesquisa, o parametro se estiver no texto vai ficar selecionado
      MSWord.Application.Selection.Find.Execute();
      Continua := MSWord.Application.Selection.Find.Found; // verifica se achou
      if Continua then
      begin
        // Substituo o parametro selecionado pelo texto que esta eu quero
        MSWord.Application.Selection.TypeText(Texto);
        Conta := Conta + 1;
      end;
      if Conta > 100 then
        Continua := False;
    end;
    (*
    Continua := True;
    while Continua do
    begin
      Continua :=
        Doc.Content.Find.Execute(FindText := Tag, ReplaceWith := '');
      if Continua then
        Doc.Content.TypeText(Val);
    end;
    *)
  end;
  function ValI(Campo: String): Integer;
  begin
    Result := QrCtr.FieldByName(Campo).AsInteger;
  end;
  function TxtI(Tag: String; Val: Integer; Tipo: TTipoTroca): String;
  begin
    Troc(Tag, dmkPF.FormaDeTratamentoBR(Val, Tipo));
  end;
  procedure CtrI(Tag, Campo: String);
  begin
    Troc(Tag, Geral.FF0(QrCtr.FieldByName(Campo).AsInteger));
  end;
  procedure CtrS(Tag, Campo: String);
  begin
    Troc(Tag, QrCtr.FieldByName(Campo).AsString);
  end;
  function  ValF(Campo: String; Casas: Integer): String;
  begin
    Result :=
      Geral.FFT(QrCtr.FieldByName(Campo).AsFloat, Casas, siPositivo);
  end;
  procedure CtrF(Tag, Campo: String; Casas: Integer);
  begin
    Troc(Tag, ValF(Campo, Casas));
  end;
  procedure CtrD(Tag, Campo: String);
  begin
    Troc(Tag, Geral.FDT(QrCtr.FieldByName(Campo).AsDateTime, 2));
  end;
var
  I, J, K, N, ArtDefCli, ArtDefLug: Integer;
  NOME_CLI, CPFJ_EMP, CPFJ_CLI,
  //NOME_REP, CPFJ_REP,
  NOME_EMP, ENDE_EMP, ENDE_CLI, CIDA_EMP,
  //ENDE_REP, CIDA_CLI, CIDA_REP,
  TempoImpl, TempoMoni, TempoTrei, PrazoImpl, VAL_MENS, PER_MULT, PER_MORA,
  NOM_RESP, CID_RESP, CPF_RESP, RGN_RESP, RGO_RESP, RGD_RESP, Trato, TIPO_DOC,
  NOM_TES1, CPF_TES1, EN1_TES1, EN2_TES1, EN3_TES1,
  NOM_TES2, CPF_TES2, EN1_TES2, EN2_TES2, EN3_TES2, DATA_COE, LOGR_PPE: String;
  Achou: Boolean;
begin
  Result := False;
  try
    (*if Doc = Unassigned then
    begin
      Geral.MB_ERRO('O documento n�o foi apropriadamente carregado!');
      Exit;
    end;*)
    DmodG.ReopenEndereco(ValI('Contratante'));
    NOME_CLI := DmodG.QrEnderecoNOME_ENT.Value;
    CPFJ_CLI := DmodG.QrEnderecoCNPJ_TXT.Value;
    ENDE_CLI := DmodG.QrEnderecoE_ALL.Value;
    Trato    := DmodG.QrEnderecoTRATO.Value;
    if Length(Geral.SoNumero_TT(CPFJ_CLI)) >= 14 then
      TIPO_DOC := 'CNPJ'
    else
    if Length(Geral.SoNumero_TT(CPFJ_CLI)) >= 11 then
      TIPO_DOC := 'CPF'
    else
      TIPO_DOC := '';
    //
    DmodG.ReopenEndereco(ValI('Contratada'));
    NOME_EMP := DmodG.QrEnderecoNOME_ENT.Value;
    CPFJ_EMP := DmodG.QrEnderecoCNPJ_TXT.Value;
    ENDE_EMP := DmodG.QrEnderecoE_ALL.Value;
    CIDA_EMP := DmodG.QrEnderecoCIDADE.Value;
    ENDE_EMP := DModG.QrEnderecoE_ALL.Value;
    LOGR_PPE := DModG.QrEnderecoTRATO.Value;
    //

    DmodG.ReopenEndereco(ValI('RepreLegal'));
    NOM_RESP := DmodG.QrEnderecoNOME_ENT.Value;
    CID_RESP := DmodG.QrEnderecoCIDADE.Value;
    CPF_RESP := DmodG.QrEnderecoCNPJ_TXT.Value;
    RGN_RESP := DmodG.QrEnderecoRG.Value;
    RGO_RESP := Trim(DmodG.QrEnderecoSSP.Value);
    if RGO_RESP <> '' then
      RGO_RESP := ' ' + RGO_RESP;
    RGD_RESP := Geral.FDT(DmodG.QrEnderecoDataRG.Value, 2);
    if RGD_RESP <> '' then
      RGD_RESP := ' emitido em ' + RGD_RESP;
    //

    //
    //DmodG.ReopenEndereco(ValI('Testemun01'));
    DModG.ObtemEnderecoEtiqueta3Linhas(ValI('Testemun01'), EN1_TES1, EN2_TES1, EN3_TES1);
    NOM_TES1 := DmodG.QrEnderecoNOME_ENT.Value;
    CPF_TES1 := DmodG.QrEnderecoCNPJ_TXT.Value;

    DModG.ObtemEnderecoEtiqueta3Linhas(ValI('Testemun02'), EN1_TES2, EN2_TES2, EN3_TES2);
    NOM_TES2 := DmodG.QrEnderecoNOME_ENT.Value;
    CPF_TES2 := DmodG.QrEnderecoCNPJ_TXT.Value;

    //

    ArtDefCli :=  ValI('ArtigoDef');
    ArtDefLug :=  ValI('LugArtDef');
    //
    K := ValI('PerDefImpl');
    N := ValI('PerQtdImpl');
    TempoImpl := Geral.FF0(N) + ' ' + dmkPF.TxtGrandezaPeriodo(K, N > 1);
    //
    K := ValI('PerDefMoni');
    N := ValI('PerQtdMoni');
    TempoMoni := Geral.FF0(N) + ' ' + dmkPF.TxtGrandezaPeriodo(K, N > 1);
    //
    K := ValI('PerDefTrei');
    N := ValI('PerQtdTrei');
    TempoTrei := Geral.FF0(N) + ' ' + dmkPF.TxtGrandezaPeriodo(K, N > 1);
    //
    K := ValI('PerDefIniI');
    N := ValI('PerQtdIniI');
    PrazoImpl := Geral.FF0(N) + ' ' + dmkPF.TxtGrandezaPeriodo(K, N > 1);
    //
    VAL_MENS := ValF('ValorMes', 2);
    VAL_MENS := VAL_MENS + ' (' + dmkPF.ExtensoMoney(VAL_MENS) + ')';
    //
    PER_MULT := ValF('PercMulta', 2);
    PER_MULT := PER_MULT + '% (' + dmkPF.ExtensoFloat(PER_MULT) + ')';
    PER_MORA := ValF('PercJuroM', 2);
    PER_MORA := PER_MORA + '% (' + dmkPF.ExtensoFloat(PER_MORA) + ')';
    //

    for I := 0 to MaxVarsContratos do
    begin
      Tag := Copy(sListaContratos[I], 1, pos(']', sListaContratos[I]));
      if Tag <> '' then
      begin
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Substituindo tag ' + Tag);
        try
          //if Tag = '[NUM_CNTR]' then CtrS(Tag, 'Numero') else
          if Tag = '[NUM_CNTR]' then Troc(Tag, GeraVersaoContrato(QrCtr)) else
          if Tag = '[NOME_CLI]' then Troc(Tag, NOME_CLI) else
          //
          if Tag = '[NOME_EMP]' then Troc(Tag, NOME_EMP) else
          if Tag = '[DOCU_EMP]' then Troc(Tag, CPFJ_EMP) else
          if Tag = '[ENDR_EMP]' then Troc(Tag, ENDE_EMP) else
          if Tag = '[DATA_COE]' then Troc(Tag, Geral.FDT(
            QrCtr.FieldByName('DContrato').AsDateTime, 16) + ' de ' +
            dmkPF.VerificaMes(Geral.IMV(Geral.FDT(
            QrCtr.FieldByName('DContrato').AsDateTime, 22)), False) + ' de ' +
            Geral.FDT(QrCtr.FieldByName('DContrato').AsDateTime, 25)) else
          if Tag = '[DATA_REC]' then CtrD(Tag, 'DtaCntrFim') else
          if Tag = '[DATA_PRR]' then CtrD(Tag, 'DtaPrxRenw') else
          if Tag = '[DAT_ASEX]' then Troc(Tag, Geral.FDT(
            QrCtr.FieldByName('DtaAssina').AsDateTime, 16) + ' de ' +
            dmkPF.VerificaMes(Geral.IMV(Geral.FDT(
               QrCtr.FieldByName('DtaAssina').AsDateTime, 22)), False) + ' de '
               + Geral.FDT(QrCtr.FieldByName('DtaAssina').AsDateTime, 25)) else
          if Tag = '[LOGR_PPE]' then Troc(Tag, LOGR_PPE) else
          //
          if Tag = '[TRAT_CLI]' then CtrS(Tag, 'Tratamento') else
          if Tag = '[ART_O_A_]' then TxtI(Tag, ArtDefCli, trcOA) else
          if Tag = '[ART_OSAS]' then TxtI(Tag, ArtDefCli, trcOsAs) else
          if Tag = '[PREP_ART]' then TxtI(Tag, ArtDefCli, trcPara) else
          if Tag = '[DOCU_CLI]' then Troc(Tag, CPFJ_CLI) else
          if Tag = '[ENDR_CLI]' then Troc(Tag, ENDE_CLI) else
          if Tag = '[DATA_CON]' then CtrD(Tag, 'DContrato') else
          if Tag = '[DATA_VAL]' then CtrD(Tag, 'DtaPropFim') else
          if Tag = '[TEM_IMPL]' then Troc(Tag, TempoImpl) else
          if Tag = '[TEM_MONI]' then Troc(Tag, TempoMoni) else
          if Tag = '[TEM_TREI]' then Troc(Tag, TempoTrei) else
          if Tag = '[PRZ_IMPL]' then Troc(Tag, PrazoImpl) else
          if Tag = '[VAL_MENS]' then Troc(Tag, VAL_MENS) else
          if Tag = '[DOC_PAGA]' then CtrS(Tag, 'TpPagDocu') else
          if Tag = '[LOC_ASIN]' then Troc(Tag, CIDA_EMP) else
          if Tag = '[DAT_ASIN]' then CtrS(Tag, 'DtaAssina') else
          //if Tag = '[DET_SERV]' then CtrS(Tag, 'Servicos02') else
          if Tag = '[NOM_RESP]' then Troc(Tag, NOM_RESP) else
          if Tag = '[CID_RESP]' then Troc(Tag, CID_RESP) else
          if Tag = '[CPF_RESP]' then Troc(Tag, CPF_RESP) else
          if Tag = '[RGN_RESP]' then Troc(Tag, RGN_RESP) else
          if Tag = '[RGO_RESP]' then Troc(Tag, RGO_RESP) else
          if Tag = '[RGD_RESP]' then Troc(Tag, RGD_RESP) else
          if Tag = '[DATA_INT]' then CtrD(Tag, 'DInstal') else
          if Tag = '[DATA_VEN]' then CtrD(Tag, 'DVencimento') else
          if Tag = '[DIA_PAGT]' then CtrI(Tag, 'ddMesVcto') else
          if Tag = '[PER_MULT]' then Troc(Tag, PER_MULT) else
          if Tag = '[PER_MORA]' then Troc(Tag, PER_MORA) else
          if Tag = '[NOM_TES1]' then Troc(Tag, NOM_TES1) else
          if Tag = '[CPF_TES1]' then Troc(Tag, CPF_TES1) else
          if Tag = '[EN1_TES1]' then Troc(Tag, EN1_TES1) else
          if Tag = '[EN2_TES1]' then Troc(Tag, EN2_TES1) else
          if Tag = '[EN3_TES1]' then Troc(Tag, EN3_TES1) else
          if Tag = '[NOM_TES2]' then Troc(Tag, NOM_TES2) else
          if Tag = '[CPF_TES2]' then Troc(Tag, CPF_TES2) else
          if Tag = '[EN1_TES2]' then Troc(Tag, EN1_TES2) else
          if Tag = '[EN2_TES2]' then Troc(Tag, EN2_TES2) else
          if Tag = '[EN3_TES2]' then Troc(Tag, EN3_TES2) else
          if Tag = '[EST_O_A_]' then TxtI(Tag, ArtDefLug, trcOA) else
          if Tag = '[EST_OSAS]' then TxtI(Tag, ArtDefLug, trcOsAs) else
          if Tag = '[ESTB_ART]' then TxtI(Tag, ArtDefLug, trcPara) else
          if Tag = '[EST_DESC]' then CtrS(Tag, 'LugarNome') else
          if Tag = '[LOGR_PRP]' then Troc(Tag, Trato) else
          if Tag = '[TIPO_DOC]' then Troc(Tag, TIPO_DOC) else
          begin
            //if Tag = '[SERVICOS]' then CtrS(Tag, 'Servicos01') else
            //if Tag = '[DET_SERV]' then CtrS(Tag, 'Servicos02') else
            Achou := False;
            N := High(ListVarsNomes);
            J := High(ListVarsValrs);
            if N <> J then
            begin
              Geral.MB_ERRO('ERRO! Existem ' + IntToStr(N + 1) +
              ' variaveis avulsas e '        + IntToStr(J + 1) +
              ' valores para essas vari�veis em "AppListas.ObtemGeneroDeUserGenero()"!');
              Exit;
            end;
            for N := Low(ListVarsNomes) to J do
            begin
              if Tag = ListVarsNomes[N] then
              begin
                Achou := True;
                Troc(Tag, ListVarsValrs[N]);
              end;
            end;
            if not Achou then
              Geral.MB_Aviso('Tag n�o implementada: ' + Tag);
          end;
        except
          MyObjects.Informa2(LaAviso1, LaAviso2, True, 'ERRO!!! ' + LaAviso1.Caption);
          raise;
        end;
      end;
    end;
    //
    //  Para variaveis extras!
    (*
    N := High(ListVarsNomes);
    J := High(ListVarsValrs);
    if N <> J then
    begin
      Geral.MB_ERRO('ERRO! Existem ' + IntToStr(N + 1) +
      ' variaveis avulsas e '        + IntToStr(J + 1) +
      ' valores para essas vari�veis em "AppListas.ObtemGeneroDeUserGenero()"!');
      Exit;
    end;
    *)
    J := High(ListVarsValrs);
    for N := J - Extras + 1 to J do
      Troc(ListVarsNomes[N], ListVarsValrs[N]);
    //
    Result := True;
  except
    raise;
  end;
end;
 }

end.
