unit UnAppListas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs, Forms,
  ExtCtrls, ComCtrls, StdCtrls, UnMyLinguas, UnInternalConsts, dmkGeral, DB,
  mySQLDbTables, UnDmkProcFunc, UnDmkEnums;

type
  //TTipoTroca = (trcOA, trcOsAs, trcPara);
  TUnAppListas = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    function  ObtemGeneroDeUserGenero(UserGenero: Integer): Integer;
  end;

var
  AppListas: TUnAppListas;

implementation

//uses Module, DmkDAC_PF, ModuleGeral, UnMyObjects;

{ TUnAppListas }

{ TUnAppListas }

function TUnAppListas.ObtemGeneroDeUserGenero(UserGenero: Integer): Integer;
begin
  case UserGenero of
    0: Result := 0;
    //1: Result := CO_DIARIO_ADD_GENERO_ORD_SRV;
    else Result := -1;
  end;

end;

end.
