<html>
<head>
<title>Atlas Refinery</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<script src="includes/js/jquery-1.4.1.min.js"></script>
<script src="includes/js/easytooltip/js/easyTooltip.js"></script>
<script type="text/javascript">
        $(document).ready(function() {
            $("a").easyTooltip();
        });
</script>
<style type="text/css">
	#easyTooltip{
		padding:5px 10px;
		border:1px solid #195fa4;
		background:#195fa4 url(includes/js/easytooltip/bg.gif) repeat-x;
		color:#fff;
		line-height: 1.5em;
		font-size: 110%;
		width: 200px;
		z-index: 1000;
	}
</style>
<!-- Save for Web Styles (index.psd) -->
<style type="text/css">
<!--

#Table_01 {
	position: relative;
	left:0px;
	top:0px;
	width:1000px;
	height:725px;
	background-color: #ffffff;
}

#index-01 {
	position:absolute;
	left:0px;
	top:0px;
	width:1000px;
	height:232px;
}

#index-02 {
	position:absolute;
	left:0px;
	top:232px;
	width:190px;
	height:36px;
}

#index-03 {
	position:absolute;
	left:190px;
	top:232px;
	width:626px;
	height:51px;
}

#index-04 {
	position:absolute;
	left:528px;
	top:172px;
	width:184px;
	height:462px;
	z-index: -1;
}

#index-05 {
	position:absolute;
	left:0px;
	top:268px;
	width:190px;
	height:40px;
}

#index-06 {
	position:absolute;
	left:190px;
	top:283px;
	width:40px;
	height:343px;
}

#index-07 {
	position:absolute;
	left:230px;
	top:283px;
	width:772px;
	height:65px;
	overflow: auto;
	z-index: 1;
}

#index-08 {
	position:absolute;
	left:0px;
	top:308px;
	width:190px;
	height:38px;
}

#index-09 {
	position:absolute;
	left:0px;
	top:346px;
	width:190px;
	height:44px;
}

#index-10 {
	position:absolute;
	left:0px;
	top:390px;
	width:190px;
	height:38px;
}

#index-11 {
	position:absolute;
	left:0px;
	top:428px;
	width:190px;
	height:42px;
}

#index-12 {
	position:absolute;
	left:0px;
	top:470px;
	width:190px;
	height:41px;
}

#index-13 {
	position:absolute;
	left:0px;
	top:511px;
	width:190px;
	height:183px;
}

#index-14 {
	position:absolute;
	left:230px;
	top:571px;
	width:109px;
	height:55px;
}

#index-15 {
	position:absolute;
	left:339px;
	top:571px;
	width:477px;
	height:55px;
}

#index-16 {
	position:absolute;
	left:190px;
	top:626px;
	width:626px;
	height:40px;
}

#index-17 {
	position:absolute;
	left:190px;
	top:666px;
	width:626px;
	height:28px;
	text-align: center;
	color: #adacac;
}
body,td,th {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #666;
}
body {
	background-color: #dadada;
}
table th{
	background-color: #dadada;
	vertical-align: top;
}
.tablecopy {
	font-size: 11px;
}
.tablebodycopy {
	font-size: 11px;
}
.tabletop-white {
	color: #FFF;
}
.darkgrey {
	color: #333;
}
#apDiv1 {
	position:absolute;
	left:229px;
	top:350px;
	width:772px;
	height:330px;
	z-index:1;
	overflow: scroll;
}
#apDiv2 {
	position:absolute;
	left:836px;
	top:238px;
	width:142px;
	height:32px;
	z-index:1;
	text-align: right;
}
-->
</style>
<!-- End Save for Web Styles -->
<script type="text/javascript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-29137145-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body style="background-color:#dadada; margin-top: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px;" onLoad="MM_preloadImages('images/index-over_02.jpg','images/index-over_05.jpg','images/index-over_08.jpg','images/index-over_09.jpg','images/index-over_10.jpg','images/index-over_11.jpg','images/index-over_12.jpg')">
<!-- Save for Web Slices (index.psd) -->


<center><div id="Table_01">
<div id="apDiv2"><a href="products.php">Search Again</a></div>
<div id="apDiv1">
  <table width="100%" border="0" cellpadding="5" cellspacing="0">
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Sulphated Natural & Synthetic Fatliquors</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">EUREKA� 678</td>
      <td width="42" align="center" valign="top"><a href="#" title="" onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Sulphated%20Natural%20&%20Synthetic%20Fatliquors/Eureka%20678-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Spanish/Sulphated%20Natural%20&%20Synthetic%20Fatliquors/Eureka%20678-s.pdf'>Spanish</a></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Sulphated%20Natural%20&%20Synthetic%20Fatliquors/Eureka%20678-c.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Sulphated Natural & Synthetic Fatliquors</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLASOL� 178</td>
      <td width="42" align="center" valign="top"><a href="#" title="A compounded fatliquor of high quality sulphated neatsfoot and synthetic sperm oil. Designed for fatliquoring upper leathers where a greater resistance to discoloration by light is required." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Sulphated%20Natural%20&%20Synthetic%20Fatliquors/Atlasol%20178-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Spanish/Sulphated%20Natural%20&%20Synthetic%20Fatliquors/Atlasol%20178-s.pdf'>Spanish</a></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Sulphated%20Natural%20&%20Synthetic%20Fatliquors/Atlasol%20178-c.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Sulphated Natural & Synthetic Fatliquors</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLASOL� CAM</td>
      <td width="42" align="center" valign="top"><a href="#" title="A semi-synthetic fatliquor developed as a general purpose fatliquor for all types of leathers. Based on sulphated synthetic oils, hydrocarbon and bisulfited marine oil. Also displays very adequate light fastness properties." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Sulphated%20Natural%20&%20Synthetic%20Fatliquors/Atlasol%20cam-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Spanish/Sulphated%20Natural%20&%20Synthetic%20Fatliquors/Atlasol%20cam-s.pdf'>Spanish</a></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Sulphated%20Natural%20&%20Synthetic%20Fatliquors/Atlasol%20cam-c.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Sulphated Natural & Synthetic Fatliquors</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLASOL� 23-KPM</td>
      <td width="42" align="center" valign="top"><a href="#" title="A compounded fatliquor based on sulphated synthetic sperm and neatsfoot oil." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Sulphated Natural & Synthetic Fatliquors/Atlasol 23-KPM-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Sulphated Natural & Synthetic Fatliquors</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLASOL� CAM-SF</td>
      <td width="42" align="center" valign="top"><a href="#" title="A general fatliquor based on sulfonated synthetic oil that's also formulated to be chrome stable." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Sulphated%20Natural%20&%20Synthetic%20Fatliquors/Atlasol%20cam-sf-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Sulphated Natural & Synthetic Fatliquors</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">EUREKA� 1014-M</td>
      <td width="42" align="center" valign="top"><a href="#" title="A specially compounded fatliquor for finer upper leathers. Based on sulphated high grade neatsfoot oil and a sulphated synthetic sperm oil" onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Sulphated%20Natural%20&%20Synthetic%20Fatliquors/Eureka%201014-M-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Sulphated%20Natural%20&%20Synthetic%20Fatliquors/Eureka%201014-M-c.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Sulphated Natural & Synthetic Fatliquors</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">EUREKA� ECO-2</td>
      <td width="42" align="center" valign="top"><a href="#" title="An economical fatliquor blended from synthetic sperm Oil, sulfated Oils and lecithin." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Sulphated%20Natural%20&%20Synthetic%20Fatliquors/Eureka%20ECO-2-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Sulphated%20Natural%20&%20Synthetic%20Fatliquors/Eureka%20ECO-2-c.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Sulphated Natural & Synthetic Fatliquors</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">EUREKA� ECO-4</td>
      <td width="42" align="center" valign="top"><a href="#" title="A general purpose fatliquor based on sulfonated natural oil and sulfated synthetic oil." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Sulphated%20Natural%20&%20Synthetic%20Fatliquors/Eureka%20ECO-4-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Sulphated%20Natural%20&%20Synthetic%20Fatliquors/Eureka%20ECO-4-c.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Sulphated Natural & Synthetic Fatliquors</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">EUREKA� 884</td>
      <td width="42" align="center" valign="top"><a href="#" title="A specially formulated fatliquor based upon sulfated and sulphonated natural oils" onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Sulphated%20Natural%20&%20Synthetic%20Fatliquors/Eureka%20884-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Sulphated Natural & Synthetic Fatliquors</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLASOL� 141</td>
      <td width="42" align="center" valign="top"><a href="#" title="An outstanding synthetic replacement fatliquor for egg yolk. It is based on a synthetic sulphated ester, synthetic neatsfoot oil and lecithin." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Sulphated%20Natural%20&%20Synthetic%20Fatliquors/Atlasol%20141-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Sulphated Natural & Synthetic Fatliquors</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLASOL� 160-SFD</td>
      <td width="42" align="center" valign="top"><a href="#" title="A synthetic fatliquor that can be used as a replacement for highly sulphated sperm oil." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Sulphated%20Natural%20&%20Synthetic%20Fatliquors/Atlasol%20160-SFD-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Sulphated Natural & Synthetic Fatliquors</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLASOL� 170</td>
      <td width="42" align="center" valign="top"><a href="#" title="A specially compounded fatliquor for snow-white alum tanned leather. Based on emulsified neatsfoot oil, refined coconut oil and synthetic oils." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Sulphated%20Natural%20&%20Synthetic%20Fatliquors/Atlasol%20170-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Sulphated Natural & Synthetic Fatliquors</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLASOL� 2000</td>
      <td width="42" align="center" valign="top"><a href="#" title="An electrolyte stable, light-fast synthetic fatliquor. It is especially designed for making soft, light and fluffy leathers and is suitable for all fur skins." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Sulphated%20Natural%20&%20Synthetic%20Fatliquors/Atlasol%202000-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Sulphated%20Natural%20&%20Synthetic%20Fatliquors/Atlasol%202000-c.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Sulphated Natural & Synthetic Fatliquors</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">EUREKA� NLS</td>
      <td width="42" align="center" valign="top"><a href="#" title="A combination natural and synthetic fatliquor, based on lecithin and special anionic emulsifiers. Offers low fog and moderate thermal resistance. " onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Sulphated%20Natural%20&%20Synthetic%20Fatliquors/Eureka%20NLS-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Sulphated%20Natural%20&%20Synthetic%20Fatliquors/Eureka%20NLS-c.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Sulphated Natural & Synthetic Fatliquors</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">EUREKA TR</td>
      <td width="42" align="center" valign="top"><a href="#" title="A natural and synthetic compound, based on lecithin and special anionic emulsifiers. Offers superior softness and designed for very soft leathers." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Sulphated%20Natural%20&%20Synthetic%20Fatliquors/Eureka%20TR-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Sulphated%20Natural%20&%20Synthetic%20Fatliquors/Eureka%20TR-c.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Sulphated Natural & Synthetic Fatliquors</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLASOL RLT</td>
      <td width="42" align="center" valign="top"><a href="#" title="An economical product that promotes tightness with medium to soft temper and gives the same touch as the waterproof emulsions." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Sulphated%20Natural%20&%20Synthetic%20Fatliquors/Atlasol%20RLT-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Sulphated Natural & Synthetic Fatliquors</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">EUREKA� 102</td>
      <td width="42" align="center" valign="top"><a href="#" title="A highly sulphated castor Oil." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Sulphated%20Natural%20&%20Synthetic%20Fatliquors/Eureka%20102-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Sulphated Natural & Synthetic Fatliquors</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">EUREKA� 1040</td>
      <td width="42" align="center" valign="top"><a href="#" title="A blend of sulphated fish oil and straight chain aliphatic hydrocarbons." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Sulphated%20Natural%20&%20Synthetic%20Fatliquors/Eureka%201040-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Sulphated Natural & Synthetic Fatliquors</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">EUREKA� 1084</td>
      <td width="42" align="center" valign="top"><a href="#" title="A fatliquor based on bleached and medium sulphated Neatsfoot Oil." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Sulphated%20Natural%20&%20Synthetic%20Fatliquors/Eureka%201084-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Sulphated Natural & Synthetic Fatliquors</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">EUREKA� 25-A</td>
      <td width="42" align="center" valign="top"><a href="#" title="A highly sulfated tallow containing 17% moisture. It is for use on vegetable tanned leather where fullness and softening is required. " onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Sulphated%20Natural%20&%20Synthetic%20Fatliquors/Eureka%2025-A-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Bisulfited Fatliquors</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">EUREKA� 400-RU</td>
      <td width="42" align="center" valign="top"><a href="#" title="" onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Bisulfited%20Fatliquors/Eureka%20400-RU-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Spanish/Bisulfited%20Fatliquors/Eureka%20400-RU-s.pdf'>Spanish</a></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Bisulfited%20Fatliquors/Eureka%20400-RU-c.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Bisulfited Fatliquors</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">EUREKA� 408</td>
      <td width="42" align="center" valign="top"><a href="#" title="An excellent, versatile leather fatliquor based on sulphonated high quality fish oil.   " onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Bisulfited%20Fatliquors/Eureka%20408-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Spanish/Bisulfited%20Fatliquors/Eureka%20408-s.pdf'>Spanish</a></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Bisulfited%20Fatliquors/Eureka%20408-c.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Bisulfited Fatliquors</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">EUREKA� 950-R</td>
      <td width="42" align="center" valign="top"><a href="#" title="" onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Bisulfited%20Fatliquors/Eureka%20950-R-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Spanish/Bisulfited%20Fatliquors/Eureka%20950-R-s.pdf'>Spanish</a></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Bisulfited Fatliquors</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLASOL� 177-CONCENTRATE</td>
      <td width="42" align="center" valign="top"><a href="#" title="A compounded fatliquor based on bisulfited oil, fatty alcohols and synthetic lubricants. Designed for use on leathers requiring a light and fluffy feel." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Bisulfited%20Fatliquors/Atlasol%20177-concentrate-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Spanish/Bisulfited%20Fatliquors/Atlasol%20177-Concentrate-s.pdf'>Spanish</a></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Bisulfited%20Fatliquors/Atlasol%20177-C-c.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Bisulfited Fatliquors</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLASOL� GRL</td>
      <td width="42" align="center" valign="top"><a href="#" title="A compounded fatliquor based on bisulfited oil, fatty alcohols and synthetic lubricants. Designed for use on leathers and fur skins requiring a light and fluffy feel." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Bisulfited%20Fatliquors/Atlasol%20GRL-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Spanish/Bisulfited%20Fatliquors/Atlasol%20GRL-s.pdf'>Spanish</a></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Bisulfited%20Fatliquors/Atlasol%20GRL-c.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Bisulfited Fatliquors</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">EUREKA� 400-R</td>
      <td width="42" align="center" valign="top"><a href="#" title="A highly penetrating bisulfited fish oil for use as a general-purpose fatliquor. Compatible with other anionic fatliquors and stable to various electrolytes." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Bisulfited%20Fatliquors/Eureka%20400-R-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Bisulfited%20Fatliquors/Eureka%20400-R-c.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Bisulfited Fatliquors</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">EUREKA� 800-R</td>
      <td width="42" align="center" valign="top"><a href="#" title="A leather fatliquor based on bisulfited natural oil that's oxidized at a relatively high temperature." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Bisulfited%20Fatliquors/Eureka%20800-R-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Bisulfited%20Fatliquors/Eureka%20800-R-c.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Bisulfited Fatliquors</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">EUREKA� 808</td>
      <td width="42" align="center" valign="top"><a href="#" title="A fatliquor, based on unique blend of bisulfited natural oils." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Bisulfited%20Fatliquors/Eureka%20808-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Bisulfited%20Fatliquors/Eureka%20808-c.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Bisulfited Fatliquors</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">EUREKA� 960-R</td>
      <td width="42" align="center" valign="top"><a href="#" title="A fatliquor developed as a low fogging oil. Based on oxidized bisulphited natural oil." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Bisulfited%20Fatliquors/Eureka%20960-R-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Bisulfited%20Fatliquors/Eureka%20960-R-c.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Bisulfited Fatliquors</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">EUREKA� 975-ES</td>
      <td width="42" align="center" valign="top"><a href="#" title="A newly developed fatliquor for automotive upholstery leather. Based on the well-proven Eureka 800R, Eureka 950R series with a significant improvement in thermal and light stability. " onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Bisulfited%20Fatliquors/Eureka%20975-ES-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Bisulfited%20Fatliquors/Eureka%20975-ES-c.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Bisulfited Fatliquors</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">EUREKA� 985-ESM</td>
      <td width="42" align="center" valign="top"><a href="#" title="A newly developed fatliquor for all upholstery leather., as well as Garment and Nappa. Based on the Eureka 975-ES series with a and light stability." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Bisulfited%20Fatliquors/Eureka%20985-ESM-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Bisulfited%20Fatliquors/Eureka%20985-ESM-c.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Bisulfited Fatliquors</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">EUREKA� RLC</td>
      <td width="42" align="center" valign="top"><a href="#" title="A concentrated leather fatliquor specially designed for use on all kinds of softee type leathers where rich suface feel is required." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='../database/English/Bisulfited Fatliquors/Eureka RLC-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Bisulfited Fatliquors</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLASOL� 192</td>
      <td width="42" align="center" valign="top"><a href="#" title="A bisulfited, synthetic fatliquor developed to produce a wide variety of leathers. Its use will result in low density, tight leather with a wide range of temper." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Bisulfited%20Fatliquors/Atlasol%20192-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Bisulfited%20Fatliquors/Atlasol%20192-c.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Bisulfited Fatliquors</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">EUREKA� 884</td>
      <td width="42" align="center" valign="top"><a href="#" title="A specially formulated fatliquor based upon sulfated and sulphonated natural oils" onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Bisulfited%20Fatliquors/Eureka%20884-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Bisulfited Fatliquors</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">EUREKA RTW</td>
      <td width="42" align="center" valign="top"><a href="#" title="Based on fully bisulfited fish oil that has been developed for use on softee type leathers, where a high degree of penetration is required." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Bisulfited Fatliquors/Eureka RTW-english.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Bisulfited Fatliquors/Eureka RTW-chinese.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Solvent Fatliquors</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLASOL� 118-U</td>
      <td width="42" align="center" valign="top"><a href="#" title="A general purpose synthetic fatliquor for all types of leather . Especially recommended for white leather. Based on sulphated synthetic neatsfoot oil, hydrocarbon and relatively high boiling hydrocarbon." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Solvent%20Fatliquors/Atlasol%20118-U-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Solvent%20Fatliquors/Atlasol%20118-U-c.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Cationic Fatliquors</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLASOL� ALX</td>
      <td width="42" align="center" valign="top"><a href="#" title="A cationic semi-stuffing compound based on both natural and synthetic lubricants." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Cationic%20Fatliquors/Atlasol%20ALX-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Spanish/Cationic%20Fatliquors/Atlasol%20ALX-s.pdf'>Spanish</a></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Cationic%20Fatliquors/Atlasol%20ALX-c.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Cationic Fatliquors</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLASOL� KLW</td>
      <td width="42" align="center" valign="top"><a href="#" title="A water-emulsifiable hot stuffing compound. Also an excellent replacement product for traditional hot stuffing blends. " onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Cationic%20Fatliquors/Atlasol%20KLW-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Spanish/Cationic%20Fatliquors/Atlasol%20KLW-s.pdf'>Spanish</a></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Cationic%20Fatliquors/Atlasol%20KLW-c.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Cationic Fatliquors</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLASOL� 310-B</td>
      <td width="42" align="center" valign="top"><a href="#" title="A concentrated cationic fatliquor based on highly refined neatsfoot oil and a synthetic substitute for sperm oil." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Cationic%20Fatliquors/Atlasol%20310-B-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Cationic%20Fatliquors/Atlasol%20310-B-c.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Cationic Fatliquors</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLASOL� 310-L</td>
      <td width="42" align="center" valign="top"><a href="#" title="A concentrated mildly cationic fatliquor based on lanolin, highly refined neatsfoot oil and synthetic sperm oil.  " onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Cationic%20Fatliquors/Atlasol%20310-L-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Cationic%20Fatliquors/Atlasol%20310-L-c.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Hydrophobic Products</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLASOL�WDA</td>
      <td width="42" align="center" valign="top"><a href="#" title="A waterproof fatliquor especially designed for the production of tight-grain shoe upper leather. " onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Hydrophobic%20Products/Atlasol WDA-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Hydrophobic%20Products/Atlasol WDA-c.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Hydrophobic Products</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLASOL�WRF</td>
      <td width="42" align="center" valign="top"><a href="#" title="An excellent waterproofing agent based on fluorocarbon additives. Gives excellent stain and water resistance to all types of leathers." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Hydrophobic%20Products/Atlasol WRF-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Hydrophobic%20Products/Atlasol WRF-c.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Hydrophobic Products</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLASOL�WRL</td>
      <td width="42" align="center" valign="top"><a href="#" title="An excellent, economical fatliquoring and waterproofing agent for chrome or combination tanned shoe upper and garment leathers." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Hydrophobic%20Products/Atlasol WRL-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Hydrophobic Products</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLASOL�WRM</td>
      <td width="42" align="center" valign="top"><a href="#" title="An excellent, economical fatliquoring and waterproofing agent for chrome or combination tanned shoe upper and garment leathers." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Hydrophobic%20Products/Atlasol WRM-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Hydrophobic%20Products/Atlasol WRM-c.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Hydrophobic Products</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLASOL�WRS</td>
      <td width="42" align="center" valign="top"><a href="#" title="A synthetic fatliquor used for the production of water-resistant leathers. " onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Hydrophobic%20Products/Atlasol WRS-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Hydrophobic Products</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLASOL�WSP</td>
      <td width="42" align="center" valign="top"><a href="#" title="A high silicone fatliquor that's an economical fatliquoring and waterproofing agent for chrome or combination garment leathers,tanned shoe uppers." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Hydrophobic%20Products/Atlasol WSP-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Hydrophobic%20Products/Atlasol WSP-c.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Raw Natural Oils</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLAS� NEATSFOOT OIL 30 CT</td>
      <td width="42" align="center" valign="top"><a href="#" title="A fatliquor additive, gives leather extra lubricity, a rich feel and improves leather tensile strength. " onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='../database/English/Raw Natural Oils/Atlas Neatfoot Oil 30 CT-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Spanish/Raw%20Natural%20Oils/Atlas%20Neatsfoot%20oil%2030%20CT-s.pdf'>Spanish</a></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='../database/Chinese/Raw Natural Oils/Atlas 30 CT Neatfoot Oil - C.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Finishing Oils & Waxes</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLAS� LEATHER OIL - 88</td>
      <td width="42" align="center" valign="top"><a href="#" title="A synthetic oil-based oiling-off system for crust leather, which provides a rich surface lubrication, pull-up effect and enhanced water resistance." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Finishing%20Oils%20&%20Waxes/Atlas%20leather%20oil-88-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Spanish/Finishing%20Oils%20&%20Waxes/Atlas%20leather%20oil-88-s.pdf'>Spanish</a></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Finishing%20Oils%20&%20Waxes/Atlas%20leather%20oil%20-%2088-c.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Finishing Oils & Waxes</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLAS� LEATHER OIL - 101</td>
      <td width="42" align="center" valign="top"><a href="#" title="A versatile oiling off composition for vegetable or chrome tanned leathers where shade darkening and a two-tone pull-up effect is desired." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Finishing%20Oils%20&%20Waxes/Atlas%20leather%20oil%20-%20101-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Spanish/Finishing%20Oils%20&%20Waxes/Atlas%20leather%20oil%20-%20101-s.pdf'>Spanish</a></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Finishing%20Oils%20&%20Waxes/Atlas%20leather%20oil%20-%20101-c.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Finishing Oils & Waxes</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLASENE� 100</td>
      <td width="42" align="center" valign="top"><a href="#" title="A combination of natural and synthetic waxes. It is of particular value when pull-up effect is desired." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Finishing%20Oils%20&%20Waxes/Atlasene%20100-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Spanish/Finishing%20Oils%20&%20Waxes/Atlasene%20100-s.pdf'>Spanish</a></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Finishing%20Oils%20&%20Waxes/Atlasene%20100-c.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Finishing Oils & Waxes</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLAS� LEATHER OIL-90</td>
      <td width="42" align="center" valign="top"><a href="#" title="An oiling-off compound for chrome and vegetable tanned leathers where two-tone effect for 'pull-up' is desired. Also provides good water resistance on leathers." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Finishing%20Oils%20&%20Waxes/Atlas%20leather%20oil-90-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Finishing Oils & Waxes</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLAS� LEATHER OIL-FM</td>
      <td width="42" align="center" valign="top"><a href="#" title="A finishing oil specially designed as a hand feel modifier. Creates a very light pull up without highlighting  the defects, and features a draggy feel." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Finishing%20Oils%20&%20Waxes/Atlas%20leather%20oil-FM-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Finishing%20Oils%20&%20Waxes/Atlas%20leather%20oil-FM-c.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Finishing Oils & Waxes</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLAS� LEATHER OIL-SW</td>
      <td width="42" align="center" valign="top"><a href="#" title="An oiling-off compound for chrome and semi chrome leathers to promote oil pull up and feel while maintaining water resistance. Can be applied to full grain, nubuck and splits." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Finishing%20Oils%20&%20Waxes/Atlas%20leather%20oil-SW-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Finishing Oils & Waxes</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLASENE� 816</td>
      <td width="42" align="center" valign="top"><a href="#" title="A hard, brittle topping wax for water resistant leather." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Finishing%20Oils%20&%20Waxes/Atlasene%20816-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Finishing Oils & Waxes</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLASENE� M</td>
      <td width="42" align="center" valign="top"><a href="#" title="A combination of waxes and waterproofing materials, which will impart a high degree of two-tone pull-up to waterproof leathers." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Finishing%20Oils%20&%20Waxes/Atlasene%20M-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Finishing Oils & Waxes</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLASOL� KLW</td>
      <td width="42" align="center" valign="top"><a href="#" title="A water-emulsifiable hot stuffing compound that is an excellent replacement product for traditional hot stuffing blends." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Finishing%20Oils%20&%20Waxes/Atlasol%20KLW-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Finishing%20Oils%20&%20Waxes/Atlasol%20KLW-c.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Finishing Oils & Waxes</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLASENE� TE</td>
      <td width="42" align="center" valign="top"><a href="#" title="A combination of paraffinic waxes, and special refined petrolatums. " onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Finishing%20Oils%20&%20Waxes/Atlasene%20TE-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Finishing Oils & Waxes</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLASENE� JD</td>
      <td width="42" align="center" valign="top"><a href="#" title="A combination of paraffinic waxes, lanolin derivatives and petrolatum.   " onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Finishing%20Oils%20&%20Waxes/Atlasene%20JD-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Spanish/Finishing%20Oils%20&%20Waxes/Atlasene%20JD-s.pdf'>Spanish</a></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Specialty Products</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">EUREKA� 550</td>
      <td width="42" align="center" valign="top"><a href="#" title="An exceptional compounded fatliquor based on natural and synthetic waxes and sulfated oils." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Specialty%20Products/Eureka%20550-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Spanish/Specialty%20Products/Eureka%20550-s.pdf'>Spanish</a></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Specialty Products</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">EUREKA� 575-S</td>
      <td width="42" align="center" valign="top"><a href="#" title="" onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Specialty%20Products/Eureka%20575-S-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Spanish/Specialty%20Products/Eureka%20575-S-s.pdf'>Spanish</a></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Specialty%20Products/Eureka%20575-S-c.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Specialty Products</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">EUREKA� FN</td>
      <td width="42" align="center" valign="top"><a href="#" title="A combination fatliquor based on lecithin and synthetic components. Produces very soft leathers designed for furniture, softy napa, suede and garments." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Specialty%20Products/Eureka%20FN-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Spanish/Specialty%20Products/Eureka%20FN-s.pdf'>Spanish</a></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Specialty%20Products/Eureka%20FN-c.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Specialty Products</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">EUREKA� GFN</td>
      <td width="42" align="center" valign="top"><a href="#" title="" onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Specialty%20Products/Eureka%20GFN-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Spanish/Specialty%20Products/Eureka%20GFN-s.pdf'>Spanish</a></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Specialty%20Products/Eureka%20GFN-c.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Specialty Products</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">EUREKA� LTS</td>
      <td width="42" align="center" valign="top"><a href="#" title="A lecithin based semi-emulsion fatliquor that offers superior softening power with a supple mellow touch. Eureka LTS is designed for very soft napa, suede, garment and upholstery leathers.   " onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Specialty%20Products/Eureka%20LTS-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Spanish/Specialty%20Products/Eureka%20LTS-s.pdf'>Spanish</a></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Specialty%20Products/Eureka%20LTS-c.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Specialty Products</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLASOL� FO</td>
      <td width="42" align="center" valign="top"><a href="#" title="" onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Specialty%20Products/Atlasol%20FO-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Spanish/Specialty%20Products/Atlasol%20FO-s.pdf'>Spanish</a></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Specialty%20Products/Atlasol%20FO-c.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Specialty Products</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLASTAN� PR</td>
      <td width="42" align="center" valign="top"><a href="#" title="A synthetic retanning agent that's water soluble with a low molecular weight copolymer." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Specialty%20Products/Atlastan%20PR-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Spanish/Specialty%20Products/Atlastan%20PR-s.pdf'>Spanish</a></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Specialty Products</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLASOL� 155</td>
      <td width="42" align="center" valign="top"><a href="#" title="A concentrated scouring and degreasing compound based on anionic-nonionic emulsifiers, odorless hydrocarbon and other additives." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Specialty%20Products/Atlasol%20155-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Specialty Products</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLASTAN� AR</td>
      <td width="42" align="center" valign="top"><a href="#" title="A synthetic organic resin tanning agent that's water soluble with a low molecular weight acrylic polymer" onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Specialty%20Products/Atlastan%20AR-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Specialty%20Products/Atlastan%20AR-c.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Specialty Products</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLASTAN� POLY-174</td>
      <td width="42" align="center" valign="top"><a href="#" title="A synthetic copolymer in emulsion form that fills and tightens all substrates while promoting the even distribution of dye, re-tan and fatliquor components" onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Specialty%20Products/Atlastan%20POLY-174-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Specialty%20Products/Atlastan%20POLY-174-c.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Specialty Products</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLAS� EM-30</td>
      <td width="42" align="center" valign="top"><a href="#" title="An emulsifier and stabilizer for oils and fatliquors used in the leather and fur industries." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Specialty%20Products/Atlas%20EM-30-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Specialty%20Products/Atlas%20EM-30-c.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Specialty Products</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLAS� BEAM #1</td>
      <td width="42" align="center" valign="top"><a href="#" title="An odorless unhairing assist. This safe, easy to use white powder virtually eliminates unpleasant sodium sulfide odors typically associated with unhairing systems." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Specialty%20Products/Atlas%20Beam%201-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Specialty Products</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLAS� DEFOAMER AFC</td>
      <td width="42" align="center" valign="top"><a href="#" title="A hydrophobized silicone compound. One of the most effective and economical defoamers in the textile and leather industries" onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Specialty%20Products/Atlas%20Defoamer%20AFC-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Specialty Products</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLAS� EMJ-10</td>
      <td width="42" align="center" valign="top"><a href="#" title="An eco-friendly nonionic surfactant. It has superior wetting power and is used as a detergent, degreaser and dispersant of natural fats when used for hide soaking. " onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Specialty%20Products/Atlas%20EMJ-10-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Specialty Products</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLAS� LEATHER CLEANER</td>
      <td width="42" align="center" valign="top"><a href="#" title="An aqueous soap detergent that's especially designed for cleaning leather items." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Specialty%20Products/Atlas%20Leather%20Cleaner-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Specialty Products</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLAS� LEATHER ODOR</td>
      <td width="42" align="center" valign="top"><a href="#" title="A compounded masking agent which exhibits a traditional leather aroma." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Specialty%20Products/Atlas%20Leather%20Odor-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Specialty%20Products/Atlas%20leather%20Odor-c.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Specialty Products</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">EUREKA� 25-A</td>
      <td width="42" align="center" valign="top"><a href="#" title="A highly sulfated tallow containing 17% moisture. It is for use on vegetable tanned leather where fullness and softening is required. " onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Specialty%20Products/Eureka%2025-A-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Specialty Products</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">EUREKA� 884</td>
      <td width="42" align="center" valign="top"><a href="#" title="A specially formulated fatliquor based upon sulfated and sulphonated natural oils" onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Specialty%20Products/Eureka%20884-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Degreaser & Emulsiphier</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLASOL� 155</td>
      <td width="42" align="center" valign="top"><a href="#" title="A concentrated scouring and degreasing compound based on anionic-nonionic emulsifiers, odorless hydrocarbon and other additives." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Degreaser%20&%20Emulsiphier/Atlasol%20155-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Degreaser & Emulsiphier</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLAS� EM-30</td>
      <td width="42" align="center" valign="top"><a href="#" title="An emulsifier and stabilizer for oils and fatliquors used in the leather and fur industries." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Degreaser%20&%20Emulsiphier/Atlas%20EM-30-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Degreaser%20&%20Emulsiphier/Atlas%20EM-30-c.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Degreaser & Emulsiphier</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLAS� EMJ-10</td>
      <td width="42" align="center" valign="top"><a href="#" title="An eco-friendly nonionic surfactant. It has superior wetting power and is used as a detergent, degreaser and dispersant of natural fats when used for hide soaking. " onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Degreaser%20&%20Emulsiphier/Atlas%20EMJ-10-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Fur & Double Face</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLASOL� 2000</td>
      <td width="42" align="center" valign="top"><a href="#" title="An electrolyte stable, light-fast synthetic fatliquor. It is especially designed for making soft, light and fluffy leathers and is suitable for all fur skins." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Fur%20&%20Double%20Face/Atlasol%202000-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Fur%20&%20Double%20Face/Atlasol%202000-c.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Fur & Double Face</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLASOL� GRL</td>
      <td width="42" align="center" valign="top"><a href="#" title="A compounded fatliquor based on bisulfited oil, fatty alcohols and synthetic lubricants. Designed for use on leathers and fur skins requiring a light and fluffy feel." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Fur%20&%20Double%20Face/Atlasol%20GRL-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Spanish/Fur%20&%20Double%20Face/Atlasol%20GRL-s.pdf'>Spanish</a></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Fur%20&%20Double%20Face/Atlasol%20GRL-c.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Sulphated Natural & Synthetic Fatliquors</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">EUREKA� FDT</td>
      <td width="42" align="center" valign="top"><a href="#" title="A skillfull blend of lanolin and high grade natural and synthetic oils. It is designed as an oiling off and kicking grease for fur dressing.
" onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Fur%20&%20Double%20Face/Eureka%20FDT-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Fur%20&%20Double%20Face/Eureka%20FDT-c.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Cationic Fatliquors</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLASOL� 20-A</td>
      <td width="42" align="center" valign="top"><a href="#" title="A concentrated cationic fatliquor based on natural materials designed to produce full-grain &quot;Pull-Up&quot; leathers and &quot;Oily&quot; nubuck leathers.
" onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Cationic%20Fatliquors/Atlasol%2020-A-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Cationic%20Fatliquors/Atlasol%2020-A-c.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Finishing Oils & Waxes</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLASOL� 20-A</td>
      <td width="42" align="center" valign="top"><a href="#" title="A concentrated cationic fatliquor based on natural materials designed to produce full-grain &quot;Pull-Up&quot; leathers and &quot;Oily&quot; nubuck leathers.
" onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Finishing%20Oils%20&%20Waxes/Atlasol%2020-A-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Finishing%20Oils%20&%20Waxes/Atlasol%2020-A-c.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Specialty Products</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">EUREKA� FDT</td>
      <td width="42" align="center" valign="top"><a href="#" title="A skillfull blend of lanolin and high grade natural and synthetic oils. It is designed as an oiling off and kicking grease for fur dressing.
" onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Specialty%20Products/Eureka%20FDT-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Specialty%20Products/Eureka%20FDT-c.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Fur & Double Face</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">EUREKA� FDT</td>
      <td width="42" align="center" valign="top"><a href="#" title="A skillfull blend of lanolin and high grade natural and synthetic oils. It is designed as an oiling off and kicking grease for fur dressing." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Fur & Double Face/Eureka FDT-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Fur & Double Face/Eureka FDT-c.pdf'>Chinese</a></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Syntans & Resins</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">Atlastan DS</td>
      <td width="42" align="center" valign="top"><a href="#" title="A naphthalene sulphonic acid sodium salt syntan
with high dispersing capacity towards vegetable extracts,
capable of improving and increasing their penetration and
fixation during the drum fast tanning operation." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Syntans & Resins/AtlastanDS.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Syntans & Resins</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">Atlastan HO</td>
      <td width="42" align="center" valign="top"><a href="#" title="A melamine-phenol based resin that imparts
fullness, mellow handle and tight grain; and it can be applied in
any stage of the retannage due to its versatility" onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Syntans & Resins/AtlastanHO.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Syntans & Resins</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">Atlastan MS</td>
      <td width="42" align="center" valign="top"><a href="#" title="A melamine based resin that imparts fullness,
rubbery handle and firm grain. " onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Syntans & Resins/AtlastanMS.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Syntans & Resins</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">Atlastan NBS</td>
      <td width="42" align="center" valign="top"><a href="#" title="A phenol-sulphonic syntan with a good tanning
power and with medium astringency which gives fullness,
roundness and tight grain." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Syntans & Resins/AtlastanNBS.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Syntans & Resins</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">Atlastan NS</td>
      <td width="42" align="center" valign="top"><a href="#" title="A naphthalene-based neutralizing syntan developed for the empty and loose areas of leathers because it de-acidifies evenly helping to keep very compact fibers." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Syntans & Resins/AtlastanNS.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Syntans & Resins</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">Atlastan RS</td>
      <td width="42" align="center" valign="top"><a href="#" title="A dicyandiamide-based resin with a specific filling action for the empty and sponge area of the leathers improving the fibers density." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Sulphated Natural & Synthetic Fatliquors/AtlastanRS.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Specialty Products</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">Eureka GFN-M</td>
      <td width="42" align="center" valign="top"><a href="#" title="A mixture of sulphated neatsfoot oil, bisulfited fish oil and lanolin, developed primarily to provide water resistance to leather." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Specialty Products/Eureka GFN-M.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Specialty Products</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">Eureka LT-M</td>
      <td width="42" align="center" valign="top"><a href="#" title="A lecithin based semi-emulsion fatliquor that offers superior softening power with a supple mellow touch." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Specialty Products/Eureka LT-M.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Bisulfited Fatliquors</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">Eureka 400-RX</td>
      <td width="42" align="center" valign="top"><a href="#" title="An excellent, economical and versatile leather fatliquor based on bisulfited high quality marine oil." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Bisulfited Fatliquors/Eureka 400-RX.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Beamhouse Products</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLASBEAM DG</td>
      <td width="42" align="center" valign="top"><a href="#" title="An excellent degreasing agent for all types of hides, including Cattle, Sheep and Pig." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Degreaser & Emulsiphier/AtlasbeamDG.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Sulphated Natural & Synthetic Fatliquors</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">Atlasol SL-2</td>
      <td width="42" align="center" valign="top"><a href="#" title="A compounded fatliquor of high quality sulphated neatsfoot and synthetic sperm oil." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Sulphated Natural & Synthetic Fatliquors/Atlasol SL-2.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Specialty Products</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">EUREKA OD</td>
      <td width="42" align="center" valign="top"><a href="#" title="A fatliquor based on high degree lecithin and special surfactants" onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Specialty Products/Eureka OD.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Beamhouse Products</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLASBEAM ENZ-S</td>
      <td width="42" align="center" valign="top"><a href="#" title="A proteolitic enzyme for soaking all hide types." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Beamhouse Products/Atlasbeam ENZ-S.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Beamhouse Products</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLASBEAM SFA-C</td>
      <td width="42" align="center" valign="top"><a href="#" title="A sulfide free liming auxiliary for reducing draw." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Beamhouse Products/Atlasbeam SFA-C.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Beamhouse Products</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLASBEAM SFA</td>
      <td width="42" align="center" valign="top"><a href="#" title="A sulfide free auxiliary for unhairing,
scud removal and reducing draw." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Beamhouse Products/Atlasbeam SFA.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Beamhouse Products</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLASBEAM PAN-6000</td>
      <td width="42" align="center" valign="top"><a href="#" title="A pancreatic bate enzyme" onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Beamhouse Products/Atlasbeam PAN-6000.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Beamhouse Products</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLASBEAM PAN-11000</td>
      <td width="42" align="center" valign="top"><a href="#" title="A pancreatic bate enzyme" onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Beamhouse Products/Atlasbeam PAN-11000.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Bisulfited Fatliquors</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLAS� 600-R</td>
      <td width="42" align="center" valign="top"><a href="#" title="Eureka 600-R is a bisulfited fatliquor based on sustainable,
high-quality vegetable oil that is oil oxidized at a relatively high
temperature." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Bisulfited Fatliquors/Atlas 600-R.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Specialty Products</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLASTAN PL</td>
      <td width="42" align="center" valign="top"><a href="#" title="ATLASTAN PL is an electrolyte stable, light-fast polymer/synthetic compound
fatliquor. It is especially designed for making soft, light and fluffy leathers." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Specialty Products/ATLASTAN PL ENGLISH.doc.pdf.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Specialty Products</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLASTAN CQ</td>
      <td width="42" align="center" valign="top"><a href="#" title="Atlastan CQ is a compound specially designed to save formulation times and production
costs. It is based on special synthetic oils combined with synthetic retanning agents to achieve soft and spongy leathers such as furniture, bag- and glove-leathers." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Specialty Products/ Atlastan CQ English.doc.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Bisulfited Fatliquors</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">EUREKA 400-RZ</td>
      <td width="42" align="center" valign="top"><a href="#" title="Eureka 400-RZ is an excellent leather lubricant based on Sulphonated Fish Oil" onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Bisulfited Fatliquors/Eureka 400-RZ.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Bisulfited Fatliquors</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">Eureka ATF</td>
      <td width="42" align="center" valign="top"><a href="#" title="Eureka ATF is a fatliquor developed as a low fogging oil.
It is based on oxidized bisulfited natural oil." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Bisulfited Fatliquors/EUREKA ATF.PDF'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
        <tr class="tablecopy">
      <td width="76" align="left" valign="top" class="tablebodycopy">Beamhouse Products</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLASBEAM #1</td>
      <td width="42" align="center" valign="top"><a href="#" title="Atlasbeam #1 is an odorless unhairing assist. This safe, easy to
use white powder virtually eliminates unpleasant sodium sulfide
odors typically associated with unhairing systems. More
importantly, Atlasbeam #1 reduces sulphide exhaust beamhouse
liquor levels by up to 70%. Atlasbeam #1 promotes a higher
grain and a more natural look in full grain leathers. In addition,
Atlasbeam #1 produces cleaner blue stock, more uniform color
crust leathers and approximately 4% more yield than other
unhairing systems. Atlasbeam #1 also exhibits a remarkable
capability to strip dyestuffs from previously dyed leather.
Please contact Atlas technicians for this procedure as each
situation must be evaluated and a specific procedure
recommended." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Beamhouse Products/Atlas Beam #1 TDS.PDF'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"></td>
    </tr>
      </table>
</div>
	<div id="index-01"> <a href="index.php"><img src="images/index_01.jpg" alt="" width="1000" height="232" border="0" /></a>
</div>
	<div id="index-02"><a href="index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image15','','images/index-over_02.jpg',1)"><img src="images/index_02.jpg" alt="About Us" name="Image15" width="190" height="36" border="0"></a></div>
	<div id="index-03">
		<img src="images/products_03.jpg" width="626" height="51" alt="" />
	</div>
	<div id="index-05"><a href="products.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image16','','images/index_05.jpg',1)"><img src="images/index-over_05.jpg" alt="Products" name="Image16" width="190" height="40" border="0"></a></div>
	<div id="index-06">
		<img src="images/products_06.jpg" width="40" height="343" alt="" />
	</div>
	<div id="index-07">
	<table width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td width="76" rowspan="2" align="left" valign="top" bgcolor="#333333" class="tabletop-white"><b>Type</b></td>
			<td width="102" rowspan="2" align="left" valign="top" bgcolor="#333333" class="tabletop-white"><b>Product</b></td>
			<td width="53" rowspan="2" align="left" valign="top" bgcolor="#333333" class="tabletop-white"><b>Definition</b></td>
	    <td colspan="5" align="center" valign="top" bgcolor="#333333"><b class="tabletop-white">Application</b></td>
	    <td width="144" rowspan="2" align="center" valign="top" bgcolor="#333333"><b class="tabletop-white">Download PDF</b></td>
			<td width="12" rowspan="2" align="center" valign="top" bgcolor="#333333">&nbsp;</td>
		</tr>
		<tr class="darkgrey">
			<td width="65" align="center" valign="top" bgcolor="#000000" class="tabletop-white"><b class="tablebodycopy">Upper Shoe<br>
Leather</b></td>
			<td width="60" align="center" valign="top" bgcolor="#000000" class="tabletop-white"><b class="tablebodycopy">Furniture<br>
Upholstery</b></td>
			<td width="64" align="center" valign="top" bgcolor="#000000" class="tabletop-white"><b class="tablebodycopy">Automotive<br>
Upholstery</b></td>
			<td width="55" align="center" valign="top" bgcolor="#000000" class="tabletop-white"><b class="tablebodycopy">Garments</b></td>
			<td width="41" align="center" valign="top" bgcolor="#000000" class="tabletop-white"><b class="tablebodycopy">Gloving</b></td>
		</tr>
	</table>
</div>
	<div id="index-08"><a href="agents.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image17','','images/index-over_08.jpg',1)"><img src="images/index_08.jpg" alt="Agents" name="Image17" width="190" height="38" border="0"></a></div>
	<div id="index-09"><a href="press.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image18','','images/index-over_09.jpg',1)"><img src="images/index_09.jpg" alt="Press" name="Image18" width="190" height="44" border="0"></a></div>
	<div id="index-10"><a href="labreports.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image19','','images/index-over_10.jpg',1)"><img src="images/index_10.jpg" alt="Lab Reports" name="Image19" width="190" height="38" border="0"></a></div>
	<div id="index-11"><a href="certification.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image20','','images/index-over_11.jpg',1)"><img src="images/index_11.jpg" alt="Certification" name="Image20" width="190" height="42" border="0"></a></div>
	<div id="index-12"><a href="contactus.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image21','','images/index-over_12.jpg',1)"><img src="images/index_12.jpg" alt="Contact Us" name="Image21" width="190" height="41" border="0"></a></div>
	<div id="index-13">
		<img src="images/index_13.jpg" width="190" height="183" alt="" />
	</div>
	<div id="index-17">
	  <p><br>
	    <a href="pp.php" style="color:#adacac; font-size:12px">Privacy Policy</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="terms.php" style="color:#adacac; font-size:12px">Terms</a>&nbsp; &nbsp;&nbsp; &nbsp;<img src="img/US_flag.png" width="58" height="30"></p>
	  <p>&nbsp;</p>
	</div></div></center>
<!-- End Save for Web Slices -->
</body>
</html>
