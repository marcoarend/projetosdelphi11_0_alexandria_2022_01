object FmReceiLstSelFamil: TFmReceiLstSelFamil
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = 'FmReceiLstSelFamil'
  ClientHeight = 256
  ClientWidth = 402
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 222
    Width = 402
    Height = 34
    Align = alBottom
    TabOrder = 0
    object Button1: TButton
      Left = 8
      Top = 4
      Width = 75
      Height = 25
      Caption = '&OK'
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 320
      Top = 4
      Width = 75
      Height = 25
      Caption = '&Cancela'
      TabOrder = 1
      OnClick = Button2Click
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 200
    Height = 222
    Align = alLeft
    TabOrder = 1
    object Label1: TLabel
      Left = 1
      Top = 1
      Width = 198
      Height = 13
      Align = alTop
      Alignment = taCenter
      Caption = 'Fam'#237'lia de produtos'
      ExplicitWidth = 93
    end
    object DBGrid1: TDBGrid
      Left = 1
      Top = 35
      Width = 198
      Height = 186
      Align = alClient
      DataSource = DsProdutosFamil
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'ID'
          Width = 40
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Title.Caption = 'Descri'#231#227'o'
          Width = 120
          Visible = True
        end>
    end
    object EdProdutosFamil: TdmkEdit
      Left = 1
      Top = 14
      Width = 198
      Height = 21
      Align = alTop
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
      OnChange = EdProdutosFamilChange
    end
  end
  object Panel3: TPanel
    Left = 200
    Top = 0
    Width = 202
    Height = 222
    Align = alClient
    TabOrder = 2
    object Label2: TLabel
      Left = 1
      Top = 1
      Width = 200
      Height = 13
      Align = alTop
      Alignment = taCenter
      Caption = 'Fornecedor'
      ExplicitWidth = 55
    end
    object EdEntidadesCptt: TdmkEdit
      Left = 1
      Top = 14
      Width = 200
      Height = 21
      Align = alTop
      TabOrder = 0
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
      OnChange = EdEntidadesCpttChange
    end
    object DBGrid2: TDBGrid
      Left = 1
      Top = 35
      Width = 200
      Height = 186
      Align = alClient
      DataSource = DsEntidadesCptt
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'ID'
          Width = 40
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Title.Caption = 'Descri'#231#227'o'
          Width = 120
          Visible = True
        end>
    end
  end
  object QrProdutosFamil: TZQuery
    Connection = DMod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM produtosfamil'
      'ORDER BY Nome')
    Params = <>
    Left = 68
    Top = 76
    object QrProdutosFamilCodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrProdutosFamilNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
  end
  object DsProdutosFamil: TDataSource
    DataSet = QrProdutosFamil
    Left = 68
    Top = 120
  end
  object QrEntidadesCptt: TZQuery
    Connection = DMod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM entidadescptt'
      'ORDER BY Nome')
    Params = <>
    Left = 288
    Top = 76
    object QrEntidadesCpttCodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrEntidadesCpttNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 255
    end
  end
  object DsEntidadesCptt: TDataSource
    DataSet = QrEntidadesCptt
    Left = 288
    Top = 120
  end
end
