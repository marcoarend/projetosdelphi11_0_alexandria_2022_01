object FmLoadInsum: TFmLoadInsum
  Left = 0
  Top = 0
  Caption = 'LOD-INSUM-001 :: Importa'#231#227'o de Produtos da Concorr'#234'ncia'
  ClientHeight = 457
  ClientWidth = 878
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 0
    Top = 323
    Width = 878
    Height = 5
    Cursor = crVSplit
    Align = alTop
    ExplicitTop = 327
  end
  object Splitter2: TSplitter
    Left = 531
    Top = 328
    Width = 5
    Height = 39
    Align = alRight
    ExplicitLeft = 533
  end
  object SGSales: TStringGrid
    Left = 0
    Top = 328
    Width = 531
    Height = 39
    Align = alClient
    ColCount = 16
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
    TabOrder = 0
    ColWidths = (
      64
      64
      64
      64
      64
      64
      64
      64
      64
      64
      64
      64
      64
      64
      64
      64)
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 367
    Width = 878
    Height = 71
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 874
      Height = 54
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 37
        Width = 874
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object Memo1: TMemo
    Left = 0
    Top = 285
    Width = 878
    Height = 38
    Align = alTop
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 206
    Font.Height = -13
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    Lines.Strings = (
      'Memo1')
    ParentFont = False
    TabOrder = 2
    WordWrap = False
    OnEnter = Memo1Enter
    OnExit = Memo1Exit
    OnGesture = Memo1Gesture
    OnKeyUp = Memo1KeyUp
    OnMouseUp = Memo1MouseUp
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 438
    Width = 878
    Height = 19
    Panels = <
      item
        Width = 120
      end
      item
        Alignment = taCenter
        Width = 60
      end
      item
        Width = 50
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 878
    Height = 285
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 4
    object Panel5: TPanel
      Left = 0
      Top = 0
      Width = 509
      Height = 285
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      object Label27: TLabel
        Left = 8
        Top = 4
        Width = 120
        Height = 13
        Caption = 'Arquivo a ser carregado:'
      end
      object SbSelArq: TSpeedButton
        Left = 420
        Top = 19
        Width = 27
        Height = 23
        Caption = '1'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Wingdings'
        Font.Style = []
        ParentFont = False
        OnClick = SbSelArqClick
      end
      object SbAbre: TSpeedButton
        Left = 448
        Top = 19
        Width = 27
        Height = 23
        Caption = #241
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Wingdings'
        Font.Style = []
        ParentFont = False
        OnClick = SbAbreClick
      end
      object SpeedButton1: TSpeedButton
        Left = 476
        Top = 19
        Width = 27
        Height = 23
        Caption = #254
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Wingdings'
        Font.Style = []
        ParentFont = False
        OnClick = SpeedButton1Click
      end
      object Label2: TLabel
        Left = 68
        Top = 44
        Width = 120
        Height = 13
        Caption = 'Linha m'#225'xima dos t'#237'tulos:'
      end
      object Label1: TLabel
        Left = 12
        Top = 44
        Width = 40
        Height = 13
        Caption = 'Planilha:'
      end
      object Label3: TLabel
        Left = 196
        Top = 44
        Width = 29
        Height = 13
        Caption = 'Linha:'
      end
      object Label4: TLabel
        Left = 22
        Top = 194
        Width = 85
        Height = 13
        Caption = 'Ind'#250'stria qu'#237'mica:'
      end
      object EdPath: TdmkEdit
        Left = 8
        Top = 20
        Width = 409
        Height = 21
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'C:\Arcano\TFL\'
        QryCampo = 'DirNFeGer'
        UpdCampo = 'DirNFeGer'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'C:\Arcano\TFL\'
        ValWarn = False
      end
      object EdMaxlinTit: TdmkEdit
        Left = 68
        Top = 60
        Width = 121
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfLongint
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '100'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 100
        ValWarn = False
      end
      object EdPlanilha: TdmkEdit
        Left = 12
        Top = 60
        Width = 53
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfLongint
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '1'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 1
        ValWarn = False
      end
      object Button4: TButton
        Left = 448
        Top = 44
        Width = 55
        Height = 25
        Caption = 'Parar'
        TabOrder = 3
        OnClick = Button4Click
      end
      object RGIQ: TRadioGroup
        Left = 24
        Top = 88
        Width = 481
        Height = 105
        Caption = 'Formato: '
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          'Indefinido'
          'CSV'
          'XLS'
          'Atlas'
          'Corichem'
          'Corium (pdf)'
          'Dermacolor'
          'Getti'
          'MK'
          'LF')
        TabOrder = 4
        OnClick = RGIQClick
      end
      object dmkEdit1: TdmkEdit
        Left = 196
        Top = 60
        Width = 53
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfLongint
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '1'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 1
        ValWarn = False
        OnRedefinido = dmkEdit1Redefinido
      end
      object EdMemiLinTxt: TdmkEdit
        Left = 256
        Top = 60
        Width = 161
        Height = 21
        TabOrder = 6
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'EdMemiLinTxt'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'EdMemiLinTxt'
        ValWarn = False
      end
      object EdEntidadesCptt: TdmkEditCB
        Left = 24
        Top = 208
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfLongint
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEntidadesCptt
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEntidadesCptt: TdmkDBLookupComboBox
        Left = 76
        Top = 208
        Width = 425
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'Sigla'
        ListSource = DsEntidadesCptt
        TabOrder = 8
        dmkEditCB = EdEntidadesCptt
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object BitBtn1: TBitBtn
        Left = 428
        Top = 228
        Width = 75
        Height = 25
        Caption = 'BitBtn1'
        TabOrder = 9
        OnClick = BitBtn1Click
      end
      object Button1: TButton
        Left = 428
        Top = 256
        Width = 75
        Height = 25
        Caption = '????'
        TabOrder = 10
        OnClick = Button1Click
      end
      object RGPDF: TRadioGroup
        Left = 24
        Top = 232
        Width = 397
        Height = 49
        Caption = 'Substituir PDF se houver novo e velho: '
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          '???'
          'Sim'
          'N'#227'o')
        TabOrder = 11
      end
    end
    object Panel2: TPanel
      Left = 509
      Top = 0
      Width = 369
      Height = 285
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object DBGCadastros: TDBGrid
        Left = 0
        Top = 45
        Width = 369
        Height = 201
        Align = alClient
        DataSource = DsCadastros0
        PopupMenu = PMCadastros
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Nome'
            Title.Caption = 'Empresa'
            Width = 248
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ITENS'
            Title.Caption = 'Qtd Produtos cadastrados'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Familias'
            Visible = True
          end>
      end
      object RGIQ_PQ: TRadioGroup
        Left = 0
        Top = 0
        Width = 369
        Height = 45
        Align = alTop
        Caption = ' Pesquisa de empresas x produtos'
        Columns = 4
        ItemIndex = 0
        Items.Strings = (
          'TODOS'
          'COM produtos'
          'SEM produtos '
          'N'#227'o pesquisados')
        TabOrder = 1
        OnClick = RGIQ_PQClick
      end
      object SGXLS: TStringGrid
        Left = 0
        Top = 246
        Width = 369
        Height = 39
        Align = alBottom
        ColCount = 16
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
        TabOrder = 2
        ColWidths = (
          64
          64
          64
          64
          64
          64
          106
          64
          64
          64
          64
          64
          64
          64
          64
          64)
      end
    end
  end
  object ListBox04: TListBox
    Left = 536
    Top = 328
    Width = 342
    Height = 39
    Align = alRight
    ItemHeight = 13
    TabOrder = 5
  end
  object Query1: TZQuery
    Params = <>
    Left = 216
    Top = 164
  end
  object Query2: TZQuery
    Params = <>
    Left = 284
    Top = 164
  end
  object QrEntidadesCptt: TZQuery
    Connection = DMod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Sigla'
      'FROM entidadescptt'
      'ORDER BY Nome')
    Params = <>
    Left = 400
    Top = 328
    object QrEntidadesCpttCodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrEntidadesCpttNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 255
    end
    object QrEntidadesCpttSigla: TWideStringField
      FieldName = 'Sigla'
      Required = True
      Size = 60
    end
  end
  object DsEntidadesCptt: TDataSource
    DataSet = QrEntidadesCptt
    Left = 400
    Top = 376
  end
  object QrCadastros0: TZQuery
    Connection = DMod.MyDB
    SQL.Strings = (
      'SELECT cad.entidadescptt, cpt.Nome,  '
      'COUNT(cad.Codigo) ITENS  '
      'FROM produtoscadas cad '
      'LEFT JOIN entidadescptt cpt ON cpt.Codigo= cad.entidadescptt '
      'GROUP BY cad.entidadescptt '
      'ORDER BY cpt.Nome ')
    Params = <>
    Left = 744
    Top = 48
    object QrCadastros0EntidadesCptt: TLargeintField
      FieldName = 'EntidadesCptt'
      Required = True
    end
    object QrCadastros0Nome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 255
    end
    object QrCadastros0ITENS: TLargeintField
      FieldName = 'ITENS'
      ReadOnly = True
    end
    object QrCadastros0Familias: TLargeintField
      FieldName = 'Familias'
    end
  end
  object DsCadastros0: TDataSource
    DataSet = QrCadastros0
    Left = 744
    Top = 96
  end
  object QrCadastros2: TZQuery
    Connection = DMod.MyDB
    SQL.Strings = (
      'SELECT cad.entidadescptt, cpt.Nome,  '
      'COUNT(cad.Codigo) ITENS  '
      'FROM produtoscadas cad '
      'LEFT JOIN entidadescptt cpt ON cpt.Codigo= cad.entidadescptt '
      'GROUP BY cad.entidadescptt '
      'ORDER BY cpt.Nome ')
    Params = <>
    Left = 744
    Top = 148
    object QrCadastros2EntidadesCptt: TLargeintField
      FieldName = 'EntidadesCptt'
      Required = True
    end
    object QrCadastros2Nome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 255
    end
    object QrCadastros2ITENS: TWideMemoField
      FieldName = 'ITENS'
      BlobType = ftWideMemo
    end
    object QrCadastros2Familias: TWideMemoField
      FieldName = 'Familias'
      BlobType = ftWideMemo
    end
  end
  object DsCadastros2: TDataSource
    DataSet = QrCadastros2
    Left = 744
    Top = 196
  end
  object PMCadastros: TPopupMenu
    OnPopup = PMCadastrosPopup
    Left = 404
    Top = 148
    object Setarcomopesquisado1: TMenuItem
      Caption = 'Setar como pesquisado'
      OnClick = Setarcomopesquisado1Click
    end
  end
end
