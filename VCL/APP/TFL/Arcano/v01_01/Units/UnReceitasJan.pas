unit UnReceitasJan;

interface

uses
  Windows, SysUtils, Classes, Menus, UnDmkEnums, mySQLDbTables, Forms,
  UnInternalConsts, UnProjGroup_Vars, dmkGeral;

type
  TUnReceitasJan = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    procedure MostraFormReceiGruNivs(Nivel5, Nivel4, Nivel3, Nivel2, Nivel1,
              Receita: Integer);
    procedure MostraFormReceitasCab(Codigo, Controle: Integer);
  end;

var
  ReceitasJan: TUnReceitasJan;


implementation

uses UnGrl_DmkDB, ReceitasCab, ReceiGruNivs;

{ TUnReceitasJan }

procedure TUnReceitasJan.MostraFormReceiGruNivs(Nivel5, Nivel4, Nivel3, Nivel2,
  Nivel1, Receita: Integer);
begin
  if Grl_DmkDB.CriaFm(TFmReceiGruNivs, FmReceiGruNivs, afmoSoMaster) then
  begin
    //if Nivel5 <> 0 then
    //begin
      if FmReceiGruNivs.TbReceiGruNiv5.Locate('Codigo', Nivel5, []) then
      ;
    //end;
    FmReceiGruNivs.ShowModal;
    FmReceiGruNivs.Destroy;
  end;
end;

procedure TUnReceitasJan.MostraFormReceitasCab(Codigo, Controle: Integer);
begin
  if Grl_DmkDB.CriaFm(TFmReceitasCab, FmReceitasCab, afmoSoMaster) then
  begin
    if Codigo <> 0 then
    begin
      //FmReceitasCab.LocCod(Codigo, Codigo);
      if FmReceitasCab.TbReceitasCab.Locate('Codigo', Codigo, []) then
        if Controle <> 0 then
          FmReceitasCab.TbReceitasIts.Locate('Controle', Controle, []);
    end;
    FmReceitasCab.ShowModal;
    FmReceitasCab.Destroy;
  end;
end;

end.
