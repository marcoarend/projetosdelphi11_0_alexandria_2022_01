unit ReceitasCab;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, StdCtrls, DBCtrls, ExtCtrls, Mask, Buttons, Db,
  ComCtrls, Menus, dmkLabel, dmkGeral, UnDmkProcFunc, UnDmkEnums, Variants,
  dmkImage, (***AppListas,***) dmkDBEdit, dmkEdit, UnInternalConsts,
  ZAbstractRODataset, ZAbstractDataset, ZAbstractTable, ZDataset, AdvToolBar,
  dmkDBGridZTO, vcl.Themes;

type
  THackDBGrid = class(TDBGrid);
  TFmReceitasCab = class(TForm)
    DsReceitasCab: TDataSource;
    DsReceitasIts: TDataSource;
    PainelDados: TPanel;
    PainelDadosReceita: TPanel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label30: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label26: TLabel;
    Label25: TLabel;
    Label24: TLabel;
    Label5: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TdmkEdit;
    DBEdit26: TDBEdit;
    DBEdit22: TDBEdit;
    DBEdit21: TDBEdit;
    DBEdit20: TDBEdit;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit5: TDBEdit;
    DBRGTipificacao: TDBRadioGroup;
    DBEdit10: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    PainelGerencia: TPanel;
    PainelControle: TPanel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtSaida: TBitBtn;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    BtDuplica: TBitBtn;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    DBLookupComboBox1: TDBLookupComboBox;
    CBSetor: TDBLookupComboBox;
    BtImportar: TBitBtn;
    PMImporta: TPopupMenu;
    Receitadeanlogos1: TMenuItem;
    GradeIts: TdmkDBGridZTO;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    SpeedButton6: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Label7: TLabel;
    Label9: TLabel;
    TbReceitasIts: TZTable;
    SpeedButton8: TSpeedButton;
    TbReceitasCab: TZTable;
    TbReceitasCabCodigo: TLargeintField;
    TbReceitasCabCodUsu: TLargeintField;
    TbReceitasCabNome: TWideStringField;
    TbReceitasCabSigla: TWideStringField;
    TbReceitasCabVersao: TLargeintField;
    TbReceitasCabVerSub: TLargeintField;
    TbReceitasCabCliente: TLargeintField;
    TbReceitasCabArtigo: TLargeintField;
    TbReceitasCabSubstrato: TLargeintField;
    TbReceitasCabTipificacao: TLargeintField;
    TbReceitasCabDataI: TDateField;
    TbReceitasCabDataA: TDateField;
    TbReceitasCabAccount: TLargeintField;
    TbReceitasCabDigitador: TLargeintField;
    TbReceitasCabTempoR: TLargeintField;
    TbReceitasCabTempoP: TLargeintField;
    TbReceitasCabTempoT: TLargeintField;
    TbReceitasCabHorasR: TLargeintField;
    TbReceitasCabHorasP: TLargeintField;
    TbReceitasCabHorasT: TLargeintField;
    TbReceitasCabSetor: TLargeintField;
    TbReceitasCabAreaM2Rebx: TFloatField;
    TbReceitasCabAreaP2Rebx: TFloatField;
    TbReceitasCabAreaM2Semi: TFloatField;
    TbReceitasCabAreaP2Semi: TFloatField;
    TbReceitasCabAreaM2Fini: TFloatField;
    TbReceitasCabAreaP2Fini: TFloatField;
    TbReceitasCabPeso: TFloatField;
    TbReceitasCabQtde: TFloatField;
    TbReceitasCabkg_m2: TFloatField;
    TbReceitasCabBRL_USD: TFloatField;
    TbReceitasCabGrupNiv1: TLargeintField;
    TbReceitasCabGrupNiv2: TLargeintField;
    TbReceitasCabGrupNiv3: TLargeintField;
    TbReceitasCabGrupNiv4: TLargeintField;
    TbReceitasCabGrupNiv5: TLargeintField;
    TbReceitasCabHvSrDB: TSmallintField;
    TbReceitasCabObserv: TWideMemoField;
    TbReceitasCabLk: TLargeintField;
    TbReceitasCabDataCad: TDateField;
    TbReceitasCabDataAlt: TDateField;
    TbReceitasCabUserCad: TLargeintField;
    TbReceitasCabUserAlt: TLargeintField;
    TbReceitasCabAlterWeb: TSmallintField;
    TbReceitasCabAtivo: TSmallintField;
    TbReceitasCabHHMM_P: TWideStringField;
    TbReceitasCabHHMM_R: TWideStringField;
    TbReceitasCabHHMM_T: TWideStringField;
    TbReceitasCabMEDIAPESO: TFloatField;
    TbReceitasItsCodigo: TLargeintField;
    TbReceitasItsOrdem: TLargeintField;
    TbReceitasItsControle: TLargeintField;
    TbReceitasItsReordem: TLargeintField;
    TbReceitasItsCodProcesso: TLargeintField;
    TbReceitasItsNomProcesso: TWideStringField;
    TbReceitasItsPorcent: TFloatField;
    TbReceitasItsCodProduto: TLargeintField;
    TbReceitasItsNomProduto: TWideStringField;
    TbReceitasItsTempoR: TLargeintField;
    TbReceitasItsTempoP: TLargeintField;
    TbReceitasItspH: TFloatField;
    TbReceitasItspH_Min: TFloatField;
    TbReceitasItspH_Max: TFloatField;
    TbReceitasItsBe: TFloatField;
    TbReceitasItsBe_Min: TFloatField;
    TbReceitasItsBe_Max: TFloatField;
    TbReceitasItsGraus: TFloatField;
    TbReceitasItsGC_Min: TFloatField;
    TbReceitasItsGC_Max: TFloatField;
    TbReceitasItsBoca: TSmallintField;
    TbReceitasItsObs: TWideStringField;
    TbReceitasItsObsProcesIni: TWideStringField;
    TbReceitasItsObsProcesFim: TWideStringField;
    TbReceitasItsDiluicao: TFloatField;
    TbReceitasItsCusto: TFloatField;
    TbReceitasItsMinimo: TFloatField;
    TbReceitasItsMaximo: TFloatField;
    TbReceitasItsOrigemPQ: TLargeintField;
    TbReceitasItsMoedaPQ: TWideStringField;
    TbReceitasItsPrecoPQ: TFloatField;
    TbReceitasItsPerICMS: TFloatField;
    TbReceitasItsPerPIS: TFloatField;
    TbReceitasItsPerCOFINS: TFloatField;
    TbReceitasItsPerIPI: TFloatField;
    TbReceitasItsMoedaFrete: TWideStringField;
    TbReceitasItsPrecoFrete: TFloatField;
    TbReceitasItsCusUnit: TFloatField;
    TbReceitasItsCusItem: TFloatField;
    TbReceitasItsLk: TLargeintField;
    TbReceitasItsDataCad: TDateField;
    TbReceitasItsDataAlt: TDateField;
    TbReceitasItsUserCad: TLargeintField;
    TbReceitasItsUserAlt: TLargeintField;
    TbReceitasItsAlterWeb: TSmallintField;
    TbReceitasItsAtivo: TSmallintField;
    Label6: TLabel;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    Label8: TLabel;
    DBEdit9: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdSigla: TDBEdit;
    Label10: TLabel;
    Label11: TLabel;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    QrSetoresCad: TZQuery;
    DsSetoresCad: TDataSource;
    QrSetoresCadCodigo: TLargeintField;
    QrSetoresCadNome: TWideStringField;
    QrEntidadesEmpr: TZQuery;
    DsEntidadesEmpr: TDataSource;
    QrEntidadesEmprCodigo: TLargeintField;
    QrEntidadesEmprNome: TWideStringField;
    DBLookupComboBox2: TDBLookupComboBox;
    DBLookupComboBox3: TDBLookupComboBox;
    Label12: TLabel;
    DBLookupComboBox4: TDBLookupComboBox;
    QrSubstratos: TZQuery;
    DsSubstratos: TDataSource;
    QrSubstratosCodigo: TLargeintField;
    QrSubstratosNome: TWideStringField;
    QrArtigos: TZQuery;
    DsArtigos: TDataSource;
    QrArtigosCodigo: TLargeintField;
    QrArtigosNome: TWideStringField;
    DBEdit4: TDBEdit;
    DBEdit6: TDBEdit;
    TbReceitasCabEspesDiviCul: TSmallintField;
    TbReceitasCabEspesDiviCab: TSmallintField;
    TbReceitasCabEspesDiviEMCM: TFloatField;
    TbReceitasCabEspesRebxCul: TSmallintField;
    TbReceitasCabEspesRebxCab: TSmallintField;
    TbReceitasCabEspesRebxEMCM: TFloatField;
    TbReceitasCabEspesSemiCul: TSmallintField;
    TbReceitasCabEspesSemiCab: TSmallintField;
    TbReceitasCabEspesSemiEMCM: TFloatField;
    TbReceitasCabEspesFiniCul: TSmallintField;
    TbReceitasCabEspesFiniCab: TSmallintField;
    TbReceitasCabEspesFiniEMCM: TFloatField;
    QrEntidadesAccn: TZQuery;
    DsEntidadesAccn: TDataSource;
    QrEntidadesAccnCodigo: TLargeintField;
    QrEntidadesAccnNome: TWideStringField;
    QrRecProcCad: TZQuery;
    DsRecProcCad: TDataSource;
    QrRecProcCadCodigo: TLargeintField;
    QrRecProcCadNome: TWideStringField;
    TbReceitasItsNO_RecProcCad: TWideStringField;
    TbReceitasCabIDServr: TLargeintField;
    TbReceitasItsCtrlUsu: TLargeintField;
    TbReceitasItsIDServr: TLargeintField;
    Label13: TLabel;
    CBReceiGruNiv5: TDBLookupComboBox;
    Label14: TLabel;
    CBReceiGruNiv4: TDBLookupComboBox;
    Label15: TLabel;
    CBReceiGruNiv3: TDBLookupComboBox;
    Label19: TLabel;
    CBReceiGruNiv2: TDBLookupComboBox;
    CBReceiGruNiv1: TDBLookupComboBox;
    Label20: TLabel;
    QrReceiGruNiv5: TZQuery;
    DsReceiGruNiv5: TDataSource;
    QrReceiGruNiv5Codigo: TLargeintField;
    QrReceiGruNiv5CodUsu: TLargeintField;
    QrReceiGruNiv5Nome: TWideStringField;
    QrReceiGruNiv4: TZQuery;
    QrReceiGruNiv4Codigo: TLargeintField;
    QrReceiGruNiv4CodUsu: TLargeintField;
    QrReceiGruNiv4Nome: TWideStringField;
    DsReceiGruNiv4: TDataSource;
    QrReceiGruNiv3: TZQuery;
    QrReceiGruNiv3Codigo: TLargeintField;
    QrReceiGruNiv3CodUsu: TLargeintField;
    QrReceiGruNiv3Nome: TWideStringField;
    DsReceiGruNiv3: TDataSource;
    QrReceiGruNiv2: TZQuery;
    QrReceiGruNiv2Codigo: TLargeintField;
    QrReceiGruNiv2CodUsu: TLargeintField;
    QrReceiGruNiv2Nome: TWideStringField;
    DsReceiGruNiv2: TDataSource;
    QrReceiGruNiv1: TZQuery;
    QrReceiGruNiv1Codigo: TLargeintField;
    QrReceiGruNiv1CodUsu: TLargeintField;
    QrReceiGruNiv1Nome: TWideStringField;
    DsReceiGruNiv1: TDataSource;
    Label21: TLabel;
    DBEdit15: TDBEdit;
    Label27: TLabel;
    DBEdit18: TDBEdit;
    Label28: TLabel;
    DBEdit19: TDBEdit;
    Label29: TLabel;
    DBMemo1: TDBMemo;
    QrProdutosTabls: TZQuery;
    DsProdutosTabls: TDataSource;
    QrProdutosTablsCodigo: TLargeintField;
    QrProdutosTablsNome: TWideStringField;
    TbReceitasItsNO_ProdutosTabls: TWideStringField;
    TbReceitasItsCodUsu: TLargeintField;
    TbReceitasItsProdutosTabls: TLargeintField;
    TbReceitasItsNO_CodProduto: TWideStringField;
    QrProdutosCadas: TZQuery;
    DsProdutosCadas: TDataSource;
    QrSoma: TZQuery;
    QrSomaTempoR: TFloatField;
    QrSomaTempoP: TFloatField;
    QrReordem: TZQuery;
    Edit1: TdmkEdit;
    BtMove: TBitBtn;
    QrProdutosCadasLista: TLargeintField;
    QrProdutosCadasCodigo: TLargeintField;
    QrProdutosCadasNome: TWideStringField;
    QrProdutosCadasSegmento: TLargeintField;
    TbReceitasItsCU_CodProduto: TIntegerField;
    QrProdutosCadasCodUsu: TLargeintField;
    TbReceitasItsTXT_SHOW_PROD: TWideStringField;
    QrEntidadesCptt: TZQuery;
    QrEntidadesCpttCodigo: TLargeintField;
    QrEntidadesCpttNome: TWideStringField;
    QrProdutosFamil: TZQuery;
    QrProdutosFamilCodigo: TLargeintField;
    QrProdutosFamilNome: TWideStringField;
    TbReceitasItsEntidadesCptt: TLargeintField;
    TbReceitasItsProdutosFamil: TLargeintField;
    TbReceitasItsNO_EntidadesCptt: TWideStringField;
    TbReceitasItsNO_ProdutosFamil: TWideStringField;
    QrEntidadesCpttSigla: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure GradeItsKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtImportarClick(Sender: TObject);
    procedure Caleiro1Click(Sender: TObject);
    procedure Receitadeanlogos1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure BtDuplicaClick(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure ImgTipoChange(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure CBSetorClick(Sender: TObject);
    procedure GradeItsDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure SpeedButton8Click(Sender: TObject);
    procedure TbReceitasCabCalcFields(DataSet: TDataSet);
    procedure TbReceitasCabAfterScroll(DataSet: TDataSet);
    procedure TbReceitasCabBeforeClose(DataSet: TDataSet);
    procedure TbReceitasCabBeforePost(DataSet: TDataSet);
    procedure TbReceitasCabAfterInsert(DataSet: TDataSet);
    procedure CBReceiGruNiv5Click(Sender: TObject);
    procedure CBReceiGruNiv4Click(Sender: TObject);
    procedure CBReceiGruNiv3Click(Sender: TObject);
    procedure CBReceiGruNiv2Click(Sender: TObject);
    procedure TbReceitasCabGrupNiv5Change(Sender: TField);
    procedure TbReceitasCabGrupNiv4Change(Sender: TField);
    procedure TbReceitasCabGrupNiv3Change(Sender: TField);
    procedure TbReceitasCabGrupNiv2Change(Sender: TField);
    procedure TbReceitasCabAfterEdit(DataSet: TDataSet);
    procedure TbReceitasItsAfterPost(DataSet: TDataSet);
    procedure TbReceitasItsAfterDelete(DataSet: TDataSet);
    procedure TbReceitasItsBeforeInsert(DataSet: TDataSet);
    procedure TbReceitasItsNewRecord(DataSet: TDataSet);
    procedure GradeItsMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure GradeItsEndDrag(Sender, Target: TObject; X, Y: Integer);
    procedure GradeItsDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure GradeItsDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure GradeItsCellClick(Column: TColumn);
    procedure BtMoveClick(Sender: TObject);
    procedure TbReceitasItsCodProdutoChange(Sender: TField);
    procedure TbReceitasItsProdutosTablsChange(Sender: TField);
    procedure GradeItsColEnter(Sender: TObject);
    procedure GradeItsColExit(Sender: TObject);
    procedure TbReceitasItsCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    FReordenendo: Boolean;
    FOrdem: Integer;
    FProdChangingLista: Boolean;
    procedure ReordenaLinhas();
    procedure AtualizaTbFormulas();
    //procedure DuplicaRegistro;
    procedure DuplicaRegistro2;
    function  EditandoGradeIts(NomeColuna: String): Boolean;
    procedure MostraFormulasimpBH();
    procedure MostraFormulasimpWE();
    procedure ReopenArtigosESubstratos();
    procedure ReopenPQs(SQLType: TSQLType);
    procedure ReopenReceiGruNiv(Nivel: Integer);
    procedure MostraReceiLstSelProd(LstProdQuim: TLstProdQuim; Lf, Tp: Integer);
    procedure MostraReceiLstSelFamil(LstProdQuim: TLstProdQuim; Lf, Tp: Integer);
  public
    { Public declarations }
    FRecPesq: Integer;
    //
    procedure LimpaCamposProduto();
  end;

var
  FmReceitasCab: TFmReceitasCab;

implementation

{$R *.DFM}

uses UnMyObjects, Module, MyVCLSkin, Principal, DmkDAC_PF, UnGrl_DmkDB,
  UnSalNewJan, MeuDBUsesZ, ReceiLstSelProd, ReceiLstSelFamil, MyGlyfs;
(***UMySQLModule, MeuDBUses, FormulasImpBH, FormulasImpWE2, FormulasPesq,
MyDBCheck, ModuleGeral, BlueDermConsts, UnPQ_PF, ***)



procedure TFmReceitasCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;
procedure TFmReceitasCab.SpeedButton1Click(Sender: TObject);
begin
  TbReceitasCab.First;
end;

procedure TFmReceitasCab.SpeedButton2Click(Sender: TObject);
begin
  TbReceitasCab.Prior;
end;

procedure TFmReceitasCab.SpeedButton3Click(Sender: TObject);
begin
  TbReceitasCab.Next;
end;

procedure TFmReceitasCab.SpeedButton4Click(Sender: TObject);
begin
  TbReceitasCab.Last;
end;

procedure TFmReceitasCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FProdChangingLista := False;
  PainelGerencia.Height := 48;
  Grl_DmkDB.AbreQuery(QrEntidadesEmpr, Dmod.MyDB);
  Grl_DmkDB.AbreQuery(QrEntidadesAccn, Dmod.MyDB);
  Grl_DmkDB.AbreQuery(QrSetoresCad, Dmod.MyDB);
  Grl_DmkDB.AbreQuery(QrEntidadesCptt, Dmod.MyDB);
  ReopenReceiGruNiv(5);
  Grl_DmkDB.AbreTableZ(TbReceitasCab, Dmod.MyDB);

  //
  Grl_DmkDB.AbreQuery(QrRecProcCad, Dmod.MyDB);
  ReopenPQs(ImgTipo.SQLType);
  TbReceitasCab.Last;
  Grl_DmkDB.AbreTableZ(TbReceitasIts, Dmod.MyDB);
end;

procedure TFmReceitasCab.ImgTipoChange(Sender: TObject);
var
  Controle: Integer;
begin
  GradeIts.Visible  := ImgTipo.SQLType <> stIns;
  GradeIts.ReadOnly := ImgTipo.SQLType = stLok;
  Controle         := TbReceitasItsControle.Value;
  //
  ReopenPQs(ImgTipo.SQLType);
  //
  TbReceitasIts.Close;
  TbReceitasIts.ReadOnly := ImgTipo.SQLType <> stUpd;
  Grl_DmkDB.AbreTableZ(TbReceitasIts, Dmod.MyDB);
  TbReceitasIts.Locate('Controle', Controle, []);
  //
  //LaAviso.Visible := ImgTipo.SQLType = stUpd;
end;

procedure TFmReceitasCab.LimpaCamposProduto();
begin
  FmReceitasCab.TbReceitasItsNomProduto.Value    := '';
  FmReceitasCab.TbReceitasItsCodProduto.Value    :=  0;
  FmReceitasCab.TbReceitasItsEntidadesCptt.Value := 0;
  FmReceitasCab.TbReceitasItsProdutosFamil.Value := 0;
end;

procedure TFmReceitasCab.MostraFormulasimpBH();
begin
(***
  Application.CreateForm(TFmFormulasImpBH, FmFormulasImpBH);
  VAR_LABEL1 := FmFormulasImpBH.LaAviso1;
  VAR_LABEL2 := FmFormulasImpBH.LaAviso2;
  MyObjects.Informa2VAR_LABELs('Setando janela "FormulasImpBH"');
  FmFormulasImpBH.PainelEscolhe.Visible := True;
  FmFormulasImpBH.FEmit                 := 0;
  FmFormulasImpBH.FNomeForm             := 'Impress�o de Receita sem Baixa';
  FmFormulasImpBH.EdReceita.Text        := DBEdit12.Text;
  FmFormulasImpBH.CBReceita.KeyValue    := TbReceitasCabNumero.Value;
  FmFormulasImpBH.EdCliInt.ValueVariant := TbFormulasClienteI.Value;
  FmFormulasImpBH.CBCliInt.KeyValue     := TbFormulasClienteI.Value;
  //
  if VAR_SETOR = CO_VAZIO then
  begin
    FmFormulasImpBH.EdPeso.ValueVariant := TbFormulasPeso.Value;
    FmFormulasImpBH.EdQtde.ValueVariant := TbFormulasQtde.Value;
  end;
  FmFormulasImpBH.ShowModal;
  FmFormulasImpBH.Destroy;
  VAR_LABEL1 := nil;
  VAR_LABEL2 := nil;
***)
end;

procedure TFmReceitasCab.MostraFormulasimpWE();
begin
(***
  if DBCheck.CriaFm(TFmFormulasImpWE2, FmFormulasImpWE2, afmoNegarComAviso) then
  begin
    FmFormulasImpWE2.FEmit := 0;
    FmFormulasImpWE2.FNomeForm              := 'Impress�o de Receita sem Baixa';
    FmFormulasImpWE2.EdReceita.ValueVariant := DBEdit12.Text;
    FmFormulasImpWE2.CBReceita.KeyValue     := TbReceitasCabNumero.Value;
    FmFormulasImpWE2.EdCliInt.ValueVariant  := TbFormulasClienteI.Value;
    FmFormulasImpWE2.CBCliInt.KeyValue      := TbFormulasClienteI.Value;
    FmFormulasImpWE2.LaData.Visible         := True;
    FmFormulasImpWE2.TPDataP.Visible        := True;
    if VAR_SETOR = CO_VAZIO then
    begin
      FmFormulasImpWE2.EdPeso.ValueVariant   := TbFormulasPeso.Value;
      FmFormulasImpWE2.EdPecas.ValueVariant  := TbFormulasQtde.Value;
      FmFormulasImpWE2.EdAreaM2.ValueVariant := 0;
    end;
    FmFormulasImpWE2.ShowModal;
    FmFormulasImpWE2.Destroy;
  end;
***)
end;

procedure TFmReceitasCab.MostraReceiLstSelFamil(LstProdQuim: TLstProdQuim; Lf,
  Tp: Integer);
begin
  if Grl_DmkDB.CriaFm(TFmReceiLstSelFamil, FmReceiLstSelFamil, afmoLiberado) then
  begin
    try
      FmReceiLstSelFamil.FLstProdQuim := LstProdQuim;
      FmReceiLstSelFamil.Position := poDefaultPosOnly;
      FmReceiLstSelFamil.Left     := Lf;
      FmReceiLstSelFamil.Top      := Tp;
      //
      FmReceiLstSelFamil.ShowModal;
    finally
      FmReceiLstSelFamil.Release;
    end;
    FmReceiLstSelFamil.Destroy;
  end;
end;

procedure TFmReceitasCab.MostraReceiLstSelProd(LstProdQuim: TLstProdQuim;
  Lf, Tp: Integer);
{
var
  NewForm: TForm2;
begin
  NewForm:=TForm2.Create(nil);
  try
    NewForm.Left:=ActiveControl.Width+
                    ActiveControl.Left+
                    ClientOrigin.X;
    NewForm.Top:=ActiveControl.Top+ClientOrigin.Y;
    NewForm.ShowModal;
  finally
    NewForm.Release;
  end;
end;
}
begin
  if Grl_DmkDB.CriaFm(TFmReceiLstSelProd, FmReceiLstSelProd, afmoLiberado) then
  begin
    try
      FmReceiLstSelProd.FLstProdQuim := LstProdQuim;
      FmReceiLstSelProd.Position := poDefaultPosOnly;
      FmReceiLstSelProd.Left     := Lf;
      FmReceiLstSelProd.Top      := Tp;
      //
      if LstProdQuim = TLstProdQuim.lpqTextos then
      begin
        FmReceiLstSelProd.DBGrid1.Visible := False;
        FmReceiLstSelProd.Height := FmReceiLstSelProd.Height -
          FmReceiLstSelProd.DBGrid1.Height;
      end;
      //
      FmReceiLstSelProd.ShowModal;
    finally
      FmReceiLstSelProd.Release;
    end;
    FmReceiLstSelProd.Destroy;
  end;
end;

procedure TFmReceitasCab.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  TbReceitasCab.Close;
end;

procedure TFmReceitasCab.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmReceitasCab.TbReceitasCabAfterEdit(DataSet: TDataSet);
begin
  TbReceitasCabDataA.Value := Date();
  TbReceitasCabVerSub.Value := TbReceitasCabVerSub.Value + 1;
end;

procedure TFmReceitasCab.TbReceitasCabAfterInsert(DataSet: TDataSet);
begin
  //Grl_DmkDB.BELY_Tb(TbReceitasCab, belyNumero);
  TbReceitasCabAtivo.Value := 1;
  TbReceitasCabDataI.Value := Date();
  TbReceitasCabDigitador.Value := VAR_USUARIO;
  TbReceitasCabGrupNiv5.Value := 0;
  //TbReceitasCabTipificacao.Value := 0;
  TbReceitasCabVersao.Value := 0;
  TbReceitasCabVerSub.Value := 1;
end;

procedure TFmReceitasCab.TbReceitasCabAfterScroll(DataSet: TDataSet);
begin
  BtAltera.Enabled := TbReceitasCabCodigo.Value > 0;
  //
  DBEdit14.ValueVariant := Geral.FDT(TbReceitasCabDataAlt.Value, 3, True);
  //
  ReopenArtigosESubstratos();
end;

procedure TFmReceitasCab.TbReceitasCabBeforeClose(DataSet: TDataSet);
begin
  DBEdit14.ValueVariant := '';
end;

procedure TFmReceitasCab.TbReceitasCabBeforePost(DataSet: TDataSet);
begin
  //if TbFormulas.State = dsInsert then TbFormulasAtivo.Value := 1;
end;

procedure TFmReceitasCab.TbReceitasCabCalcFields(DataSet: TDataSet);
begin
  if TbReceitasCabQtde.Value > 0 then
    TbReceitasCabMEDIAPESO.Value :=
    TbReceitasCabPeso.Value /
    TbReceitasCabQtde.Value
  else
    TbReceitasCabMEDIAPESO.Value := 0;
  TbReceitasCabHHMM_P.Value := dmkPF.HorasMH(TbReceitasCabTempoP.Value, True);
  TbReceitasCabHHMM_R.Value := dmkPF.HorasMH(TbReceitasCabTempoR.Value, True);
  TbReceitasCabHHMM_T.Value := dmkPF.HorasMH(TbReceitasCabTempoT.Value, True);
end;

procedure TFmReceitasCab.TbReceitasCabGrupNiv2Change(Sender: TField);
begin
  TbReceitasCabGrupNiv1.Value := 0;
end;

procedure TFmReceitasCab.TbReceitasCabGrupNiv3Change(Sender: TField);
begin
  TbReceitasCabGrupNiv2.Value := 0;
end;

procedure TFmReceitasCab.TbReceitasCabGrupNiv4Change(Sender: TField);
begin
  TbReceitasCabGrupNiv3.Value := 0;
end;

procedure TFmReceitasCab.TbReceitasCabGrupNiv5Change(Sender: TField);
begin
  TbReceitasCabGrupNiv4.Value := 0;
end;

procedure TFmReceitasCab.TbReceitasItsAfterDelete(DataSet: TDataSet);
begin
  if not FReordenendo then
    ReordenaLinhas();
  //
  AtualizaTbFormulas();
end;

procedure TFmReceitasCab.TbReceitasItsAfterPost(DataSet: TDataSet);
begin
  if not FReordenendo then
    ReordenaLinhas();
  //
  AtualizaTbFormulas();
end;

procedure TFmReceitasCab.TbReceitasItsBeforeInsert(DataSet: TDataSet);
begin
  FOrdem := TbReceitasItsOrdem.Value;
end;

procedure TFmReceitasCab.TbReceitasItsCalcFields(DataSet: TDataSet);
begin
  case TLstProdQuim(TbReceitasItsProdutosTabls.Value) of
    lpqIndef:   TbReceitasItsTXT_SHOW_PROD.Value := '? ? ? ? ? ? ? ?';
    lpqRefere:  TbReceitasItsTXT_SHOW_PROD.Value := TbReceitasItsNO_CodProduto.Value;
    lpqSimilar: TbReceitasItsTXT_SHOW_PROD.Value := TbReceitasItsNO_CodProduto.Value;
    lpqComodit: TbReceitasItsTXT_SHOW_PROD.Value := TbReceitasItsNO_CodProduto.Value;
    lpqBanhos:  TbReceitasItsTXT_SHOW_PROD.Value := TbReceitasItsNO_CodProduto.Value;
    lpqTextos:  TbReceitasItsTXT_SHOW_PROD.Value := TbReceitasItsNomProduto.Value;
    lpqBranco:  TbReceitasItsTXT_SHOW_PROD.Value := '';
    lpqFamilia: TbReceitasItsTXT_SHOW_PROD.Value :=
      TbReceitasItsNO_ProdutosFamil.Value + ' (' +
      TbReceitasItsNO_EntidadesCptt.Value + ')';
    else
  end;
end;

procedure TFmReceitasCab.TbReceitasItsCodProdutoChange(Sender: TField);
begin
  if TbReceitasItsCodProduto.Value <> 0 then
  begin
    if TbReceitasItsProdutosTabls.Value <> QrProdutosCadasLista.Value then
    begin
      try
        FProdChangingLista := True;
        TbReceitasItsProdutosTabls.Value := QrProdutosCadasLista.Value;
      finally
        FProdChangingLista := False;
      end;
    end;
  end;
end;

procedure TFmReceitasCab.TbReceitasItsNewRecord(DataSet: TDataSet);
var
  SQLType: TSQLType;
begin
  TbReceitasItsCodigo.Value := TbReceitasCabCodigo.Value;
  TbReceitasItsCodUsu.Value := TbReceitasCabCodUsu.Value;
  if FOrdem > 0 then
    TbReceitasItsOrdem.Value := FOrdem
  else
    TbReceitasItsOrdem.Value := 1;
  //Grl_DmkDB.BELY_Tb(TbReceitasIts, belyControle);
  //if TbReceitasItsControle.Value = 0 then
    TbReceitasItsControle.Value :=
      Grl_DmkDB.GetNxtCodigoInt('receitasits', 'Controle', SQLType, 0);
  //if TbReceitasItsCtrlUsu.Value = 0 then
    TbReceitasItsCtrlUsu.Value :=
      Grl_DmkDB.GetNxtCodigoInt('receitasits', 'CtrlUsu', SQLType, 0);
end;

procedure TFmReceitasCab.TbReceitasItsProdutosTablsChange(Sender: TField);
var
  Tabela: Integer;
  LstProdQuim: TLstProdQuim;
  //
  MoreWid, MoreTop, Lf, Tp: Integer;
begin
  Tabela := TbReceitasItsProdutosTabls.Value;
  if Tabela <> QrProdutosCadasLista.Value then
    TbReceitasItsCodProduto.Value := 0;
  LstProdQuim := TLstProdQuim(Tabela);
  if not FProdChangingLista then
  begin
    MoreWid := 0;
    MoreTop := 0;
    MyObjects.CelulaDBGrid_PosDownRight(TDBGrid(GradeIts), MoreWid, MoreTop,
    True, Lf, Tp);
    case TLstProdQuim(Tabela) of
      lpqIndef:   LimpaCamposProduto();
      lpqRefere:  MostraReceiLstSelProd(LstProdQuim, Lf, Tp);
      lpqSimilar: MostraReceiLstSelProd(LstProdQuim, Lf, Tp);
      lpqComodit: MostraReceiLstSelProd(LstProdQuim, Lf, Tp);
      lpqBanhos:  MostraReceiLstSelProd(LstProdQuim, Lf, Tp);
      lpqTextos:  MostraReceiLstSelProd(LstProdQuim, Lf, Tp);
      lpqBranco:  LimpaCamposProduto();
      lpqFamilia: MostraReceiLstSelFamil(LstProdQuim, Lf, Tp);
      else        LimpaCamposProduto();
    end;
  end;
end;

procedure TFmReceitasCab.BtIncluiClick(Sender: TObject);
begin
  Grl_DmkDB.IncluiRegistroTbZ(FmReceitasCab, TbReceitasCab, GradeIts, DBEdSigla, ImgTipo);
end;

procedure TFmReceitasCab.BtMoveClick(Sender: TObject);
begin
  if GradeIts.SelectedRows.Count > 0 then
  begin
    GradeIts.BeginDrag(True);
  end else
    Geral.MB_Aviso('Nenhuma linha foi selecionada!');
end;

procedure TFmReceitasCab.BtAlteraClick(Sender: TObject);
begin
  Grl_DmkDB.AlteraRegistroTb(FmReceitasCab, TbReceitasCab, DBEdSigla, ImgTipo);
  ReopenArtigosESubstratos();
end;

procedure TFmReceitasCab.ReopenArtigosESubstratos();
var
  GraGruY: Integer;
begin
  QrArtigos.Close;
  QrSubstratos.Close;
  if CBSetor.KeyValue <> Null then
  begin
    Grl_DmkDB.AbreSQLQuery0(QrArtigos, Dmod.MyDB, [
    'SELECT * ',
    'FROM artigoscad ',
    'WHERE Setor=' + Geral.FF0(CBSetor.KeyValue),
    'ORDER BY Nome ',
    '']);
    //
    Grl_DmkDB.AbreSQLQuery0(QrSubstratos, Dmod.MyDB, [
    'SELECT * ',
    'FROM artigoscad ',
    'WHERE Setor=' + Geral.FF0(CBSetor.KeyValue - 1),
    'ORDER BY Nome ',
    '']);
    //
  end;
//  ReopenReceiGruNiv(5);
(*
  ReopenReceiGruNiv(4);
  ReopenReceiGruNiv(3);
  ReopenReceiGruNiv(2);
  ReopenReceiGruNiv(1);
*)
end;

procedure TFmReceitasCab.ReopenPQs(SQLType: TSQLType);
var
  SQLCompl: String;
begin
  GradeIts.Columns[0].Visible := SQLType <> stUpd;
  //
  if SQLType = stUpd then
    //SQLCompl := 'WHERE Ativo = 1 AND (GGXNiv2>0 OR Codigo<0) '
    SQLCompl := 'AND Ativo = 1 '
  else
    SQLCompl := '';
  //
  Grl_DmkDB.AbreSQLQuery0(QrProdutosCadas, Dmod.MyDB, [
  'SELECT Lista, Codigo, CodUsu, Nome, Segmento',
  'FROM produtoscadas',
  'WHERE Segmento IN (',
  '  SELECT Codigo FROM segmentoscads',
  '  WHERE NOME IN ("BH", "WE", "DY")',
  ')',
   SQLCompl,
  'ORDER BY Nome ',
  '']);
end;

procedure TFmReceitasCab.ReopenReceiGruNiv(Nivel: Integer);
var
  Qry: TZQuery;
  Filtro: String;
  Codigo, I: Integer;
begin
  Filtro := ' WHERE PaiCod=0';
  if Nivel < 5 then
  begin
    Qry := TZQuery(FindComponent('QrReceiGruNiv' + Geral.FF0(Nivel + 1)));
    if (Qry.State <> dsInactive) and (Qry.State <> dsInactive) then
    begin
      Filtro :=
        ' WHERE PaiCod IN (0, ' +
        Geral.FF0(Qry.FieldByName('Codigo').AsInteger) + ')';
    end;
  end;
  Qry :=  TZQuery(FindComponent('QrReceiGruNiv' + Geral.FF0(Nivel)));
  Qry.Close;
  Grl_DmkDB.AbreSQLQuery0(Qry, Dmod.MyDB, [
  'SELECT Codigo, CodUsu, Nome ',
  'FROM receigruniv' + Geral.FF0(Nivel),
  Filtro,
  ' ORDER BY Nome ',
  '']);
  for I := Nivel - 1 downto 1 do
    ReopenReceiGruNiv(I);
end;

procedure TFmReceitasCab.ReordenaLinhas();
var
  I, Item: Integer;
  MyCursor: TCursor;
begin
  //Exit;
  if FReordenendo then
    Exit;
  FReordenendo := True;
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  TbReceitasIts.DisableControls;
  Item := TbReceitasItsControle.Value;
  I := 0;
  TbReceitasIts.First;
  while not TbReceitasIts.Eof do
  begin
    I := I + 1;
    TbReceitasIts.Edit;
    TbReceitasItsReordem.Value := I;
    TbReceitasIts.Post;
    TbReceitasIts.Next;
  end;
  QrReordem.Params[0].AsInteger := TbReceitasCabCodigo.Value;
  QrReordem.ExecSQL;
  TbReceitasIts.Refresh;
  TbReceitasIts.Locate('Controle', Item, []);
  TbReceitasIts.EnableControls;
  FReordenendo := False;
  Screen.Cursor := MyCursor;
end;

procedure TFmReceitasCab.BtConfirmaClick(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  ReordenaLinhas();
  Grl_DmkDB.ConfirmaRegistroTbZ_Numero(FmReceitasCab, TbReceitasCab, ImgTipo,
  PainelDados, PainelConfirma, PainelControle, TDBGrid(GradeIts), 'Codigo', 'CodUsu');
  Screen.Cursor := crDefault;
end;

procedure TFmReceitasCab.BtDesisteClick(Sender: TObject);
begin
  Grl_DmkDB.DesisteRegistroTbZ(FmReceitasCab, TbReceitasCab, 'Receitas', 'Codigo',
   ImgTipo, TbReceitasCabCodigo.Value);
  GradeIts.Visible := True;
end;

procedure TFmReceitasCab.GradeItsCellClick(Column: TColumn);
var
  Boca: Integer;
  TbStatus: TDataSetState;
begin
  //GradeIts.BeginDrag(True);
  if GradeIts.ReadOnly = False then
  begin
    if Column.FieldName = 'Boca' then
    begin
      TbStatus := TbReceitasIts.State;
      Boca := TbReceitasItsBoca.Value;
      if Boca = 0 then
        Boca := 1
      else
        Boca := 0;
      //
      if TbStatus = dsBrowse then
        TbReceitasIts.Edit;
      TbReceitasItsBoca.Value := Boca;
      if TbStatus = dsInactive then
        TbReceitasIts.Post;
    end;
  end;
end;

procedure TFmReceitasCab.GradeItsColEnter(Sender: TObject);
begin
(*
  if GradeIts.ReadOnly = False then
  begin
    if GradeIts.Columns[THackDBGrid(GradeIts).Col-1].FieldName = 'Boca' then
      GradeIts.Options := GradeIts.Options - [dgAlwaysShowEditor]
    else
      GradeIts.Options := GradeIts.Options + [dgAlwaysShowEditor];
  end else
    GradeIts.Options := GradeIts.Options - [dgAlwaysShowEditor];
*)
end;

procedure TFmReceitasCab.GradeItsColExit(Sender: TObject);
begin
(*
  if EditandoGradeIts('Boca') then
    GradeIts.Options := GradeIts.Options + [dgAlwaysShowEditor];
*)
end;

procedure TFmReceitasCab.GradeItsDragDrop(Sender, Source: TObject; X,
  Y: Integer);
begin
 if Source = GradeIts then
     GradeIts.EndDrag(True); // comanda o final da operacao
end;

procedure TFmReceitasCab.GradeItsDragOver(Sender, Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
begin
if Source = GradeIts then
     Accept := True; // aceita se vier da lista
end;

procedure TFmReceitasCab.GradeItsDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
const
  Azul = $00AC392D; //$0097572B; //$00BB741E;
  Laranja = $0001CBFE; //$000080FF;
var
  CorPen, Cor: TColor;
  Neg: Integer;
  Bold: Boolean;
begin
  if (Column.FieldName = 'Porcent')
  or (Column.FieldName = 'NO_ProdutosTabls')
  or (Column.FieldName = 'CU_CodProduto')
  or (Column.FieldName = 'TXT_SHOW_PROD')
  //or (Column.FieldName = 'CodProduto')
  or (Column.FieldName = 'NO_CodProduto') then
  begin
    case TLstProdQuim(TbReceitasItsProdutosTabls.Value) of
      lpqIndef:   begin Cor := clGray;        Neg := 1; end;
      lpqRefere:  begin Cor := Azul;          Neg := 1; end;
      lpqSimilar: begin Cor := clGray;        Neg := 0; end; //  Laranja;       Neg := 1; end;
      lpqComodit: begin Cor := clBlack;       Neg := 0; end;
      lpqBanhos:  begin Cor := clGreen;       Neg := 0; end;
      lpqTextos:  begin Cor := clRed;         Neg := 0; end;
      lpqBranco:  begin Cor := clWindow;      Neg := 0; end;
      lpqFamilia: begin Cor := clGray;        Neg := 0; end;
      else        begin Cor := clFuchsia;     Neg := 0; end;
    end;
    CorPen := $00E5E5E5;//GradeIts.Canvas.Pen.Color;
    //CorPen := ThemeServices.GetSystemColor(clBtnShadow;
    //CorPen := FmMyGlyfs.AdvOfficePagerOfficeStyler.TabAppearance.ShadowColor;
    MyObjects.DesenhaTextoEmDBGrid(TDBGrid(GradeIts), Rect, Cor, clWhite,
      Column.Alignment, Column.Field.DisplayText, Neg, CorPen);
  end else
  if (Column.FieldName = 'Boca') then
    MeuVCLSkin.DrawGrid(TDBGrid(GradeIts), Rect, 1, TbReceitasItsBoca.Value);
(***
  if GradeIts.Columns[0].Visible = True then
  begin
    if (Column.FieldName = 'STATUSPQ')then
    begin
      if TbReceitasItsSTATUSPQ.Value = CO_Forml_Status_Prod_Irregular then
      begin
        Cor  := clRed;
        Bold := True;
      end else
      begin
        Cor  := clGreen;
        Bold := False;
      end;
      with GradeIts.Canvas do
      begin
        if Bold then
          Font.Style := [fsBold]
        else
          Font.Style := [];
        Font.Color := Cor;
        FillRect(Rect);
        TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
      end;
    end;
  end;
***)
end;

procedure TFmReceitasCab.GradeItsEndDrag(Sender, Target: TObject; X, Y: Integer);
  procedure StringGrid1MouseUp(X, Y: Integer);
  var
    Column, Row: Longint;
  begin
    TStringGrid(GradeIts).MouseToCell(X, Y, Column, Row);
    Edit1.ValueVariant := Row;
  end;
var
  //MouseScrPt: TPoint;
  ACol, ARow, I, N, J, Controle: Integer;
  ArrIts: array of Integer;
  Movendo: Boolean;
begin
  if Target <> nil then
  begin
    FReordenendo := True;
    try
      //StringGrid1MouseUp(X, Y);
      TStringGrid(GradeIts).MouseToCell(X, Y, ACol, ARow);
      Edit1.ValueVariant := ARow;
      //
      if ARow > 0 then
      begin
        SetLength(ArrIts, TbReceitasIts.RecordCount);
        for I := Low(ArrIts) to High(ArrIts) do
          ArrIts[I] := -2;
        //
        N := ARow - 1;
        for I := 0 to GradeIts.SelectedRows.Count - 1 do
        begin
          if I = 0 then
            Controle := TbReceitasItsControle.Value;
          //
          TbReceitasIts.GotoBookmark(pointer(GradeIts.SelectedRows.Items[I]));
          //
          ArrIts[N] := TbReceitasItsControle.Value;
          N := N + 1;
        end;
        TbReceitasIts.First;
        I := 0;
        while not TbReceitasIts.Eof do
        begin
          //if ArrIts[N] = -2 then
          //begin
            Movendo := False;
            for N := Low(ArrIts) to High(ArrIts) do
            begin
              if ArrIts[N] = TbReceitasItsControle.Value then
              begin
                Movendo := True;
                Break;
              end;
            end;
            if not Movendo then
            begin
              while ArrIts[I] > -2 do
                I := I + 1;
              ArrIts[I] := TbReceitasItsControle.Value;
              I := I + 1;
            end;
            //
          //end;
          TbReceitasIts.Next;
        end;
        for I := Low(ArrIts) to High(ArrIts) do
        begin
          if TbReceitasIts.Locate('Controle', ArrIts[I], []) then
          begin
            TbReceitasIts.Edit;
            TbReceitasItsReordem.Value := I;
            TbReceitasIts.Post;
          end else
          begin
            Geral.MB_Erro('N�o foi poss�vel localizar o item ID "Controle" ' +
            Geral.FF0(TbReceitasItsControle.Value) + ' I: ' + Geral.FF0(I));
            //
            Exit;
          end;
        end;
        //
        QrReordem.Params[0].AsInteger := TbReceitasCabCodigo.Value;
        QrReordem.ExecSQL;
        TbReceitasIts.Refresh;
        TbReceitasIts.Locate('Controle', Controle, []);
        TbReceitasIts.EnableControls;
        FReordenendo := False;
        //Screen.Cursor := MyCursor;
      end;
    finally
      FReordenendo := False;
    end;
(*
    Exit;
    //
    MouseScrPt := GradeIts.ClientToScreen(Point(X, Y));
    ACol := GradeIts.MouseCoord(MouseScrPt.X, MouseScrPt.Y).X;   // similarly for ARow
    //Edit1.ValueVariant := Edit1.ValueVariant + 1;
    Edit1.ValueVariant := ACol;
*)
  end;
end;

procedure TFmReceitasCab.GradeItsKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Ordem: Integer;
begin
  if key = VK_INSERT then
  begin
    if GradeIts.ReadOnly = False then
    begin
      Ordem := TbReceitasItsOrdem.Value-1;
      TbReceitasIts.Insert;
      TbReceitasItsOrdem.Value := Ordem;
      //TbReceitasIts.Post;
      key := 0;
    end;
  end else
  if key = VK_F4 then
  begin
    if ImgTipo.SQLType in ([stIns, stUpd]) then
    begin
      VAR_CADASTRO := 0;
      {
      FmPrincipal.CadastroPQ(TbReceitasItsProduto.Value);
      if VAR_CADASTRO <> 0 then
      begin
        Screen.Cursor := crHourGlass;
        try
          ReopenPQs(ImgTipo.SQLType);
          //
          TbReceitasIts.Edit;
          TbReceitasItsProduto.Value := VAR_CADASTRO;
          TbReceitasIts.Post;
        finally
          Screen.Cursor := crDefault;
        end;
      end;
      }
      SalNewJan.MostraFormProdutosCadas(TbReceitasItsCodProduto.Value);
    end;
  end else
  if key = VK_F7 then
  begin
    if ImgTipo.SQLType in ([stIns, stUpd]) then
    begin
      FmMeuDBUsesZ.PesquisaNome('produtoscadas', 'Nome', 'Codigo', '');
      //
      if VAR_CADASTRO <> 0 then
      begin
        Screen.Cursor := crHourGlass;
        try
          TbReceitasIts.Edit;
          TbReceitasItsCodProduto.Value := VAR_CADASTRO;
          TbReceitasIts.Post;
        finally
          Screen.Cursor := crDefault;
        end;
      end;
    end;
  end;
end;

procedure TFmReceitasCab.GradeItsMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
 if (Sender = GradeIts) and (Button = mbLeft) then
     GradeIts.BeginDrag(False); // inicia a operacao<
end;

procedure TFmReceitasCab.AtualizaTbFormulas();
begin
(***
  QrSoma.Close;
  QrSoma.Params[0].AsInteger := TbReceitasCabCodigo.Value;
  Grl_DmkDB.AbreQuery(QrSoma, Dmod.MyDB);
  //
  TbReceitasCab.Edit;
  TbReceitasCabTempoP.Value := Trunc(QrSomaTempoP.Value);
  TbReceitasCabTempoR.Value := Trunc(QrSomaTempoR.Value);
  TbReceitasCabTempoT.Value := TbReceitasCabTempoR.Value + TbReceitasCabTempoP.Value;
  TbReceitasCab.Post;
  //
***)
end;

procedure TFmReceitasCab.BtImportarClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImporta, BtImportar);
end;

procedure TFmReceitasCab.Caleiro1Click(Sender: TObject);
begin
  //;
end;

procedure TFmReceitasCab.CBReceiGruNiv2Click(Sender: TObject);
begin
  ReopenReceiGruNiv(1);
end;

procedure TFmReceitasCab.CBReceiGruNiv3Click(Sender: TObject);
begin
  ReopenReceiGruNiv(2);
end;

procedure TFmReceitasCab.CBReceiGruNiv4Click(Sender: TObject);
begin
  ReopenReceiGruNiv(3);
end;

procedure TFmReceitasCab.CBReceiGruNiv5Click(Sender: TObject);
begin
  ReopenReceiGruNiv(4);
end;

procedure TFmReceitasCab.CBSetorClick(Sender: TObject);
begin
  ReopenArtigosESubstratos();
end;

procedure TFmReceitasCab.Receitadeanlogos1Click(Sender: TObject);
begin
  //FmPrincipal.ImportaReceita;
end;

procedure TFmReceitasCab.SbImprimeClick(Sender: TObject);
begin
(***
  VAR_CADASTRO := 0;
  //
  Grl_DmkDB.AbreMySQLQuery0(QrSetor, Dmod.MyDB, [
    'SELECT TpReceita ',
    'FROM listasetores ',
    'WHERE Codigo=' + Geral.FF0(TbFormulasSetor.Value),
    '']);
  //
  case TReceitaTipoSetor(QrSetorTpReceita.Value) of
    //recsetrNenhum:
    rectipsetrRibeira:      MostraFormulasimpBH();
    rectipsetrRecurtimento: MostraFormulasimpWE();
    //recsetrAcabamento:
    else
    begin
      Geral.MB_Aviso(
        'Nenhum tipo de receita foi definido no cadastro do setor!' + sLineBreak +
        'Ser� utilizado o de ribeira!');
      MostraFormulasimpBH();
    end;
  end;
  if VAR_CADASTRO <> 0 then
    TbFormulas.Locate('Numero', VAR_CADASTRO, []);
***)
end;

procedure TFmReceitasCab.BtDuplicaClick(Sender: TObject);
begin
  DuplicaRegistro2;
end;

procedure TFmReceitasCab.BtExcluiClick(Sender: TObject);
const
  Motivo = 0;
begin
(***
  PQ_PF.TransfereReceitaDeArquivo(TOrientTabArqBD.stabdAcessivelToMorto,
    TbReceitasCabCodigo.Value, Motivo);
  TbFormulas.Refresh;
  TbFormulas.Last;
***)
end;

procedure TFmReceitasCab.DuplicaRegistro2;
var
  Nome: String;
  Formula, Atual, Controle: Integer;
begin
(***
  if DBCheck.AcessoNegadoAoForm_4(afmoNegarComAviso, 'QUI-RECEI-DUP') then Exit;
  //
  Atual := TbReceitasCabCodigo.Value;
  //
  if(TbFormulas.State <> dsInactive) and (TbFormulas.RecordCount > 0) and (Atual <> 0) then
  begin
    if Geral.MB_Pergunta('Confirma a duplica��o da receita atual?') = ID_YES then
    begin
      Screen.Cursor := crHourGlass;
      try
        TbFormulas.DisableControls;
        TbReceitasIts.DisableControls;
        //
        Formula := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', 'Formulas', 'Formulas', 'Numero');
        Nome    := Copy(TbFormulasNome.Value, 1, 23) + CO_COPIA;
        //
        if Grl_DmkDB.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'formulas', TMeuDB,
          ['Numero'], [Atual],
          ['Numero', 'Nome', 'DataI', 'DataCad'],
          [Formula, Nome, Date, Date],
          '', True, nil, nil) then
        begin
          if (TbReceitasIts.State <> dsInactive) and (TbReceitasIts.RecordCount > 0) then
          begin
            TbReceitasIts.First;
            while not TbReceitasIts.Eof do
            begin
              Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', 'FormulasIts','FormulasIts', 'Controle');
              //
              if Grl_DmkDB.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'formulasits', TMeuDB,
                ['Controle'], [TbReceitasItsControle.Value],
                ['Numero', 'Controle'],
                [Formula, Controle],
                '', False, nil, nil) then
              begin
                //
              end;
              TbReceitasIts.Next;
            end;
          end;
        end;
      finally
        TbFormulas.EnableControls;
        TbReceitasIts.EnableControls;
        //
        Screen.Cursor := crDefault;
      end;
      TbFormulas.Refresh;
      TbFormulas.Locate('Numero', Formula, []);
      AtualizaTbFormulas;
      TbFormulas.Refresh;
      //
      Geral.MB_Aviso('A receita n� ' + Geral.FF0(Atual) +
        ' foi duplicada com sucesso!'+sLineBreak+'O n�mero da nova f�rmula � ' +
        Geral.FF0(Formula) + '!');
    end;
  end;
***)
end;

function TFmReceitasCab.EditandoGradeIts(NomeColuna: String): Boolean;
begin
  Result := (
  (Lowercase(GradeIts.Columns[THackDBGrid(GradeIts).Col].FieldName) =
    Lowercase(NomeColuna))
  and
    (GradeIts.ReadOnly = False)
  );
end;

procedure TFmReceitasCab.SbNumeroClick(Sender: TObject);
var
  Numero: String;
begin
  Numero := IntToStr(TbReceitasCabCodigo.Value);
  if InputQuery('Localiza F�rmula', 'Informe o n�mero da f�rmula desejada:',
  Numero) then TbReceitasCab.Locate('Numero', Numero, []);
end;

procedure TFmReceitasCab.SbQueryClick(Sender: TObject);
var
  NumeroAtu: Integer;
begin
(***
  NumeroAtu := TbReceitasCabCodigo.Value;
  FRecPesq  := 0;
  //
  Application.CreateForm(TFmFormulasPesq, FmFormulasPesq);
  FmFormulasPesq.ShowModal;
  FmFormulasPesq.Destroy;
  //
  Grl_DmkDB.AbreTable(TbFormulas, Dmod.MyDB);
  //
  if FRecPesq <> 0 then
    TbFormulas.Locate('Numero', FRecPesq, [])
  else if NumeroAtu <> 0 then
    TbFormulas.Locate('Numero', NumeroAtu, [])
  else
    TbFormulas.Last;
*)
end;

procedure TFmReceitasCab.SpeedButton5Click(Sender: TObject);
begin
(***
  FmPrincipal.CadastroDeEspessuras;
  CBEspessura.KeyValue := QrEspessurasCodigo.Value;
***)
end;

procedure TFmReceitasCab.SpeedButton6Click(Sender: TObject);
var
  Setor: Integer;
begin
(***
  VAR_CADASTRO := 0;
  Setor        := CBSetor.KeyValue;
  //
  FmPrincipal.CadastroDeSetores(Setor);
  //
  if VAR_CADASTRO <> 0 then
  begin
    QrListaSetores.Close;
    Grl_DmkDB.AbreQuery(QrListaSetores, Dmod.MyDB);
    //
    QrListaSetores.Locate('Codigo', VAR_CADASTRO, []);
    //
    CBSetor.KeyValue := QrListaSetoresCodigo.Value;
    CBSetor.SetFocus;
  end;
***)
end;

procedure TFmReceitasCab.SpeedButton8Click(Sender: TObject);
begin
  PainelDados.Visible := not PainelDados.Visible;
end;

{
SELECT Lista, Codigo, Nome, Segmento
FROM produtoscadas
WHERE Segmento IN (
SELECT Codigo FROM segmentoscads
WHERE NOME IN ("DY"))
ORDER BY Nome
}


end.
