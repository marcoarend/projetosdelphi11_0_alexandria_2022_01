unit UnAppJan;

interface

uses
  Windows, SysUtils, Classes, Menus, UnDmkEnums, mySQLDbTables, Forms,
  UnInternalConsts, UnProjGroup_Vars, dmkGeral;

type
  TUnAppJan = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    procedure MostraFormOpcoesApp();
    procedure MostraFormArcano_dmk(var Login, Senha: String);
  end;

var
  AppJan: TUnAppJan;


implementation

uses UnGrl_DmkDB, Arcano_dmk, OpcoesApp;

{ TUnAppJan }

procedure TUnAppJan.MostraFormArcano_dmk(var Login, Senha: String);
begin
  Login := '';
  Senha := '';
  //if Grl_DmkDB.CriaFm(TFmArcano_dmk, FmArcano_dmk, afmoSoMaster) then
  begin
    FmArcano_dmk.EdLogin.Text := '';
    FmArcano_dmk.EdSenha.Text := '';
    FmArcano_dmk.EdEmpresa.Text := '';
    FmArcano_dmk.F_OK := ID_NO;
    FmArcano_dmk.Show;
    while FmArcano_dmk.F_OK = ID_NO do
    begin
      Application.ProcessMessages;
      Sleep(10);
    end;
    if FmArcano_dmk.F_OK = ID_YES then
    begin
      Login := FmArcano_dmk.EdLogin.Text;
      Senha := FmArcano_dmk.EdSenha.Text;
    end;
    //FmArcano_dmk.Destroy;
  end;
end;

procedure TUnAppJan.MostraFormOpcoesApp();
begin
  if Grl_DmkDB.CriaFm(TFmOpcoesApp, FmOpcoesApp, afmoSoMaster) then
  begin
    FmOpcoesApp.ShowModal;
    FmOpcoesApp.Destroy;
  end;
end;

end.
