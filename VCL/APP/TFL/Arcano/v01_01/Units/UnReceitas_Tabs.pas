unit UnReceitas_Tabs;

interface

uses System.Classes, System.SysUtils, Generics.Collections, UnDmkEnums,
  UnMyLinguas, MyListas;
type
  TUnReceitas_Tabs = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  CarregaListaTabelas(DatabaseName: String; Lista: TList<TTabelas>):
              Boolean;
    function  CarregaListaSQL(Tabela: String; FListaSQL: TStringList):
              Boolean;
    function  CompletaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
    function  CarregaListaFRIndices(TabelaBase: String; FRIndices:
              TIndices; FLIndices: TList<TIndices>): Boolean;
    function  ComplementaListaSQL(Tabela: String; FListaSQL: TStringList):
              Boolean;
    function  CarregaListaFRCampos(Tabela: String; FRCampos:
              TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
    function  CompletaListaFRCampos(Tabela: String; FRCampos: TCampos;
              FLCampos: TList<TCampos>): Boolean;
    function  CompletaListaFRJanelas(FRJanelas: TJanelas; FLJanelas:
              TList<TJanelas>): Boolean;
    function  CarregaListaFRQeiLnk(TabelaCria: String; FRQeiLnk: TQeiLnk;
              FLQeiLnk: TList<TQeiLnk>): Boolean;
  end;

var
  Receitas_Tabs   : TUnReceitas_Tabs;

implementation

{ TUnReceitas_Tabs }

function TUnReceitas_Tabs.CarregaListaTabelas(DatabaseName: String;
  Lista: TList<TTabelas>): Boolean;
begin
  Result := True;
  try
    if LowerCase(DatabaseName) = LowerCase(CO_DBNome) then
    begin
      MyLinguas.AdTbLst(Lista, False, LowerCase('ArtigosCad'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('ReceitasCab'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('ReceitasIts'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('ReceiGruNiv1'), 'ReceiGruNiv');
      MyLinguas.AdTbLst(Lista, False, LowerCase('ReceiGruNiv2'), 'ReceiGruNiv');
      MyLinguas.AdTbLst(Lista, False, LowerCase('ReceiGruNiv3'), 'ReceiGruNiv');
      MyLinguas.AdTbLst(Lista, False, LowerCase('ReceiGruNiv4'), 'ReceiGruNiv');
      MyLinguas.AdTbLst(Lista, False, LowerCase('ReceiGruNiv5'), 'ReceiGruNiv');
      MyLinguas.AdTbLst(Lista, False, LowerCase('RecProcCad'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('SetoresCad'), '');
    end else
    begin
      ;
    end;
  except
    raise;
    Result := False;
  end;
end;

function TUnReceitas_Tabs.CarregaListaFRIndices(TabelaBase: String;
  FRIndices: TIndices; FLIndices: TList<TIndices>): Boolean;
begin
  Result := True;
  try
    if Uppercase(TabelaBase) = Uppercase('ArtigosCad') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'CodUsu';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 2;
      FRIndices.Column_name  := 'IDServr';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('ReceiGruNiv') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'CodUsu';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 2;
      FRIndices.Column_name  := 'IDServr';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('ReceitasCab') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'CodUsu';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 2;
      FRIndices.Column_name  := 'IDServr';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('ReceitasIts') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 2;
      FRIndices.Column_name   := 'Ordem';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 3;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'CtrlUsu';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 2;
      FRIndices.Column_name  := 'IDServr';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('RecProcCad') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'CodUsu';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 2;
      FRIndices.Column_name  := 'IDServr';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('SetoresCad') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'CodUsu';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 2;
      FRIndices.Column_name  := 'IDServr';
      FLIndices.Add(FRIndices);
      //
    end;
  except
    raise;
    Result := False;
  end;
end;

function TUnReceitas_Tabs.CarregaListaFRCampos(Tabela: String; FRCampos: TCampos;
  FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
begin
  Result := True;
  TemControle := TemControle + cTemControleNao;
  try
    //end else
    if Uppercase(Tabela) = Uppercase('ArtigosCad') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDServr';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(120)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Setor';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('ReceiGruNiv') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDServr';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PaiCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PaiUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(120)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('ReceitasCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDServr';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(120)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sigla';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Versao';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VerSub';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cliente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Artigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Substrato';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tipificacao';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataI';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataA';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Account';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Digitador';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TempoR';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TempoP';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TempoT';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HorasR';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HorasP';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HorasT';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Setor';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EspesDiviCul';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EspesDiviCab';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EspesDiviEMCM';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EspesRebxCul';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EspesRebxCab';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EspesRebxEMCM';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EspesSemiCul';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EspesSemiCab';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EspesSemiEMCM';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EspesFiniCul';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EspesFiniCab';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EspesFiniEMCM';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaM2Rebx';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaP2Rebx';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaM2Semi';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaP2Semi';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaM2Fini';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaP2Fini';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Peso';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1000.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Qtde';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '32.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'kg_m2';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BRL_USD';
      FRCampos.Tipo       := 'double(15,10)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GrupNiv1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GrupNiv2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GrupNiv3';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GrupNiv4';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GrupNiv5';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HvSrDB';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Observ';
      FRCampos.Tipo       := 'Text';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('ReceitasIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CtrlUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDServr';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Reordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodProcesso';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NomProcesso';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Porcent';
      FRCampos.Tipo       := 'double(13,4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ProdutosTabls';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodProduto';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NomProduto';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TempoR';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TempoP';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'pH';
      FRCampos.Tipo       := 'double(4,2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'pH_Min';
      FRCampos.Tipo       := 'double(4,2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'pH_Max';
      FRCampos.Tipo       := 'double(4,2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Be';
      FRCampos.Tipo       := 'double(5,2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Be_Min';
      FRCampos.Tipo       := 'double(5,2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Be_Max';
      FRCampos.Tipo       := 'double(5,2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Graus';
      FRCampos.Tipo       := 'double';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GC_Min';
      FRCampos.Tipo       := 'double';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GC_Max';
      FRCampos.Tipo       := 'double';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Boca';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Obs';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ObsProcesIni';
      FRCampos.Tipo       := 'varchar(120)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ObsProcesFim';
      FRCampos.Tipo       := 'varchar(120)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Diluicao';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Custo';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Minimo';
      FRCampos.Tipo       := 'double(5,2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Maximo';
      FRCampos.Tipo       := 'double(5,2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OrigemPQ';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MoedaPQ';
      FRCampos.Tipo       := 'char(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PrecoPQ';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PerICMS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PerPIS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PerCOFINS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PerIPI';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MoedaFrete';
      FRCampos.Tipo       := 'char(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PrecoFrete';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CusUnit';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CusItem';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EntidadesCptt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ProdutosFamil';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('RecProcCad') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDServr';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SetoresCad') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDServr';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
    end;
    ;
    //
    Result := True;
  except
    raise;
    Result := False;
  end;
end;

function TUnReceitas_Tabs.CarregaListaFRQeiLnk(TabelaCria: String;
  FRQeiLnk: TQeiLnk; FLQeiLnk: TList<TQeiLnk>): Boolean;
begin
// n�o implementado!
  Result := False;
end;

function TUnReceitas_Tabs.CarregaListaSQL(Tabela: String;
  FListaSQL: TStringList): Boolean;
begin
{
separador = "|"
}
  Result := True;
  if Uppercase(Tabela) = Uppercase('?') then
  begin
    FListaSQL.Add('Codigo');
    FListaSQL.Add('1');
  end else
  if Uppercase(Tabela) = Uppercase('ArtigosCad') then
  begin
    FListaSQL.Add('Codigo|CodUsu|Setor|Nome');
    FListaSQL.Add('0|0|0|""');
    FListaSQL.Add('1|1|0|"Couro Verde"');
    FListaSQL.Add('2|2|0|"Couro Salgado"');
    FListaSQL.Add('3|3|0|"Couro Salmourado"');
    FListaSQL.Add('4|4|1|"Couro Tripa Dividida"');
    FListaSQL.Add('5|5|1|"Couro Tripa Laminada"');
    FListaSQL.Add('6|6|1|"Couro Tripa Integral"');
    FListaSQL.Add('7|7|2|"Piquelado"');
    FListaSQL.Add('8|8|2|"Wet Blue"');
    FListaSQL.Add('9|9|2|"Wet White"');
    FListaSQL.Add('10|10|2|"Wet Brown"');
    FListaSQL.Add('11|11|3|"Semi acabado"');
  end else
  if (Uppercase(Tabela) = Uppercase('ReceiGruNiv5'))
  or (Uppercase(Tabela) = Uppercase('ReceiGruNiv4'))
  or (Uppercase(Tabela) = Uppercase('ReceiGruNiv3'))
  or (Uppercase(Tabela) = Uppercase('ReceiGruNiv2'))
  or (Uppercase(Tabela) = Uppercase('ReceiGruNiv1')) then
  begin
    FListaSQL.Add('Codigo|CodUsu|Nome');
    FListaSQL.Add('0|0|"  Indefinido!"');

  end else
  if Uppercase(Tabela) = Uppercase('RecProcCad') then
  begin
    FListaSQL.Add('Codigo|CodUsu|Nome');
    FListaSQL.Add('0|0|""');
    FListaSQL.Add('1|1|"Pr�-descarne"');
    FListaSQL.Add('2|2|"Pr�-Remolho"');
    FListaSQL.Add('3|3|"Lavar"');
    FListaSQL.Add('4|4|"Lavar bem"');
    FListaSQL.Add('5|5|"Escorrer"');
    FListaSQL.Add('6|6|"Remolho"');
    FListaSQL.Add('7|7|"Depila��o"');
    FListaSQL.Add('8|8|"Caleiro"');
    FListaSQL.Add('9|9|"Recaleiro"');
    FListaSQL.Add('10|10|"Pr�-Desencalagem"');
    FListaSQL.Add('11|11|"Desencalagem intermedi�ria"');
    FListaSQL.Add('12|12|"Desencalagem"');
    FListaSQL.Add('13|13|"Desencalagem 1"');
    FListaSQL.Add('14|14|"Desencalagem 2"');
    FListaSQL.Add('15|15|"Desencalagem 3"');
    FListaSQL.Add('16|16|"Purga"');
    FListaSQL.Add('17|17|"Condicionamento"');
    FListaSQL.Add('18|18|"P�quel"');
    FListaSQL.Add('19|19|"Curtimento"');
    FListaSQL.Add('20|20|"Basifica��o"');
    FListaSQL.Add('21|21|"Aquecimento"');
    FListaSQL.Add('22|22|"Descarga"');
    FListaSQL.Add('23|23|"Descurtimento"');
    FListaSQL.Add('24|24|"Neutraliza��o"');
    FListaSQL.Add('25|25|"Neutraliza��o 1"');
    FListaSQL.Add('26|26|"Neutraliza��o 2"');
    FListaSQL.Add('27|27|"Recromagem"');
    FListaSQL.Add('28|28|"Dormir no banho"');
    FListaSQL.Add('29|29|"Autom�tico"');
    FListaSQL.Add('30|30|"Autom�tico 5/25"');
    FListaSQL.Add('31|31|"Autom�tico 10/50"');
    FListaSQL.Add('32|32|"Recurtimento"');
    FListaSQL.Add('33|33|"Tingimento"');
    FListaSQL.Add('34|34|"Pr�-engraxe"');
    FListaSQL.Add('35|35|"Engraxe"');
    FListaSQL.Add('36|36|"Fixa��o"');
    FListaSQL.Add('37|37|"Acerto de cor"');
    FListaSQL.Add('38|38|"Hidrofuga��o"');
    FListaSQL.Add('39|39|"Anti-chama"');
    FListaSQL.Add('40|40|"Desodoriza��o"');
    FListaSQL.Add('41|41|"Pesar"');
  end else
  if Uppercase(Tabela) = Uppercase('SetoresCad') then
  begin
    FListaSQL.Add('Codigo|CodUsu|Nome');
    FListaSQL.Add('0|0|""');
    FListaSQL.Add('1|1|"Caleiro"');
    FListaSQL.Add('2|2|"Curtimento"');
    FListaSQL.Add('3|3|"Recurtimento"');
    FListaSQL.Add('4|4|"Acabamento"');
  end;
end;

function TUnReceitas_Tabs.ComplementaListaSQL(Tabela: String;
  FListaSQL: TStringList): Boolean;
begin
  Result := True;
  if Uppercase(Tabela) = Uppercase('') then
  begin
  end else if Uppercase(Tabela) = Uppercase('PerfisIts') then
  begin
    //FListaSQL.Add('"Entidades"|"Cadastro de pessoas f�sicas e jur�dicas (clientes| fornecedores| etc.)"');
    //FListaSQL.Add('""|""');
  end;
end;

function TUnReceitas_Tabs.CompletaListaFRCampos(Tabela: String; FRCampos: TCampos;
  FLCampos: TList<TCampos>): Boolean;
begin
  Result := True;
  try
{
    if Uppercase(Tabela) = Uppercase('Controle') then
    begin
      New(FRCampos);
      FRCampos.Field      := 'CMPPVai';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
    end;
}
  except
    raise;
    Result := False;
  end;
end;

function TUnReceitas_Tabs.CompletaListaFRJanelas(FRJanelas: TJanelas;
  FLJanelas: TList<TJanelas>): Boolean;
begin
  //
  // ENT-CADAS-001 :: Grupos de Empresas
  New(FRJanelas);
  FRJanelas.ID        := 'REC-CADAS-001';
  FRJanelas.Nome      := 'FmReceitasCab';
  FRJanelas.Descricao := 'Receitas de Ribeira';
  FLJanelas.Add(FRJanelas);
  //
  // ENT-CADAS-002 :: Cadastro de Clientes
  New(FRJanelas);
  FRJanelas.ID        := 'REC-CADAS-002';
  FRJanelas.Nome      := 'FmReceitasIts';
  FRJanelas.Descricao := 'Item de Receita de Ribeira';
  FLJanelas.Add(FRJanelas);
  //
  // ENT-CADAS-003 :: N�veis de Grupos de Receitas
  New(FRJanelas);
  FRJanelas.ID        := 'REC-CADAS-003';
  FRJanelas.Nome      := 'FmReceiGruNivs';
  FRJanelas.Descricao := 'N�veis de Grupos de Receitas';
  FLJanelas.Add(FRJanelas);
  //
  //
  Result := True;
end;

function TUnReceitas_Tabs.CompletaListaSQL(Tabela: String;
  FListaSQL: TStringList): Boolean;
begin
  Result := True;
  try
    ;
  except
    raise;
    Result := False;
  end;
end;

end.
