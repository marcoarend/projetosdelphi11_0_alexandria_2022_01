unit ReceiLstSelProd;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, dmkEdit, Vcl.ExtCtrls,
  Vcl.Grids, Vcl.DBGrids, Data.DB, ZAbstractRODataset, ZAbstractDataset,
  ZDataset, UnDmkEnums, DmkGeral, UnGrl_DmkDB;

type
  TFmReceiLstSelProd = class(TForm)
    EdTexto: TdmkEdit;
    Panel1: TPanel;
    Button1: TButton;
    Button2: TButton;
    DBGrid1: TDBGrid;
    QrProdutosCadas: TZQuery;
    QrProdutosCadasLista: TLargeintField;
    QrProdutosCadasCodigo: TLargeintField;
    QrProdutosCadasNome: TWideStringField;
    QrProdutosCadasSegmento: TLargeintField;
    DsProdutosCadas: TDataSource;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdTextoChange(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
    procedure ReopenPesquisa();
    procedure DefineProduto();
  public
    { Public declarations }
    FLstProdQuim: TLstProdQuim;
  end;

var
  FmReceiLstSelProd: TFmReceiLstSelProd;

implementation

{$R *.dfm}

uses Module, ReceitasCab;

procedure TFmReceiLstSelProd.Button1Click(Sender: TObject);
begin
  DefineProduto();
end;

procedure TFmReceiLstSelProd.Button2Click(Sender: TObject);
begin
  Close;
end;

procedure TFmReceiLstSelProd.DefineProduto();
  procedure SetaValorSoCodProduto();
  begin
    FmReceitasCab.TbReceitasItsCodProduto.Value :=
      QrProdutosCadasCodigo.Value;
  end;
  procedure SetaValorSoTexto();
  begin
    FmReceitasCab.TbReceitasItsNomProduto.Value := EdTexto.ValueVariant;
  end;
begin
  FmReceitasCab.LimpaCamposProduto();
  //
  case TLstProdQuim(FLstProdQuim) of
    lpqIndef:   ;
    lpqRefere:  SetaValorSoCodProduto();
    lpqSimilar: SetaValorSoCodProduto();
    lpqComodit: SetaValorSoCodProduto();
    lpqBanhos:  SetaValorSoCodProduto();
    lpqTextos:  SetaValorSoTexto();
    lpqBranco:  ;
    lpqFamilia: ;
    else
  end;
  Close;
end;

procedure TFmReceiLstSelProd.EdTextoChange(Sender: TObject);
begin
  ReopenPesquisa();
end;

procedure TFmReceiLstSelProd.FormCreate(Sender: TObject);
begin
 //Self.Left := 600;
end;

procedure TFmReceiLstSelProd.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_ESCAPE then
    Close;
end;

procedure TFmReceiLstSelProd.ReopenPesquisa();
var
  SQLWhere, SQLCompl, Texto: String;
begin
  //SQLWhere := '';
  SQLWhere := 'WHERE Lista=' + Geral.FF0(Integer(FLstProdQuim));
  SQLCompl := '';
  Texto := EdTexto.Text;
  if Trim(Texto) <> '' then
    SQLCompl := 'AND Nome LIKE "%' + Texto + '%"';
  //
(*
  case FLstProdQuim of
    TLstProdQuim.lpqRefere: SQLCompl := Geral.ATS([
      'AND Segmento IN (',
      '  SELECT Codigo FROM segmentoscads',
      '  WHERE NOME IN ("BH", "WE", "DY")',
      ')']);
    else SQLCompl := '';
  end;
  //
*)
  Grl_DmkDB.AbreSQLQuery0(QrProdutosCadas, Dmod.MyDB, [
  'SELECT Lista, Codigo, Nome, Segmento',
  'FROM produtoscadas',
  SQLWhere,
  SQLCompl,
  'ORDER BY Nome ',
  '']);
end;

end.
