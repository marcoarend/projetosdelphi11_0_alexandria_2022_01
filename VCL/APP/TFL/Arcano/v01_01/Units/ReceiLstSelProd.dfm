object FmReceiLstSelProd: TFmReceiLstSelProd
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = 'FmReceiLstSelProd'
  ClientHeight = 338
  ClientWidth = 651
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object EdTexto: TdmkEdit
    Left = 0
    Top = 0
    Width = 651
    Height = 21
    Align = alTop
    TabOrder = 0
    FormatType = dmktfString
    MskType = fmtNone
    DecimalSize = 0
    LeftZeros = 0
    NoEnterToTab = False
    NoForceUppercase = False
    ForceNextYear = False
    DataFormat = dmkdfShort
    HoraFormat = dmkhfShort
    UpdType = utYes
    Obrigatorio = False
    PermiteNulo = False
    ValueVariant = ''
    ValWarn = False
    OnChange = EdTextoChange
  end
  object Panel1: TPanel
    Left = 0
    Top = 297
    Width = 651
    Height = 41
    Align = alBottom
    TabOrder = 1
    ExplicitTop = 301
    object Button1: TButton
      Left = 8
      Top = 8
      Width = 75
      Height = 25
      Caption = '&OK'
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 568
      Top = 8
      Width = 75
      Height = 25
      Caption = '&Cancela'
      TabOrder = 1
      OnClick = Button2Click
    end
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 21
    Width = 651
    Height = 276
    Align = alClient
    DataSource = DsProdutosCadas
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object QrProdutosCadas: TZQuery
    Connection = DMod.MyDB
    SQL.Strings = (
      'SELECT Lista, Codigo, Nome, Segmento'
      'FROM produtoscadas'
      'WHERE Segmento IN ('
      'SELECT Codigo FROM segmentoscads'
      'WHERE NOME IN ("BH", "WE", "DY"))'
      'ORDER BY Nome')
    Params = <>
    Left = 320
    Top = 84
    object QrProdutosCadasLista: TLargeintField
      FieldName = 'Lista'
    end
    object QrProdutosCadasCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrProdutosCadasNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrProdutosCadasSegmento: TLargeintField
      FieldName = 'Segmento'
      Required = True
    end
  end
  object DsProdutosCadas: TDataSource
    DataSet = QrProdutosCadas
    Left = 320
    Top = 128
  end
end
