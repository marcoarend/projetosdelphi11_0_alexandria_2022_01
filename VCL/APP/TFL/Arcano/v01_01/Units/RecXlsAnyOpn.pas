unit RecXlsAnyOpn;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.ComCtrls, Vcl.StdCtrls, dmkEdit, Vcl.Buttons, Vcl.ExtCtrls, Vcl.Grids,
  dmkGeral, UnDmkEnums, UnAppPF;

type
  TFmRecXlsAnyOpn = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel2: TPanel;
    TSReceita: TTabSheet;
    Panel3: TPanel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    LaNome: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    EdCliNome: TdmkEdit;
    EdTecnico: TdmkEdit;
    EdAccount: TdmkEdit;
    EdArtigo: TdmkEdit;
    EdEspRebCul: TdmkEdit;
    EdRendiment: TdmkEdit;
    EdEspRebCab: TdmkEdit;
    EdEspFimCul: TdmkEdit;
    EdEspFimCab: TdmkEdit;
    EdRelatA: TdmkEdit;
    EdPesoCouro: TdmkEdit;
    EdCambio: TdmkEdit;
    EdData: TdmkEdit;
    EdQuantia: TdmkEdit;
    Button1: TButton;
    EdObserv: TdmkEdit;
    RGStatus: TRadioGroup;
    EdCor: TdmkEdit;
    SGReceita: TStringGrid;
    Panel5: TPanel;
    Label27: TLabel;
    SbSelArq: TSpeedButton;
    LaAviso: TLabel;
    SbAbre: TSpeedButton;
    EdArq: TdmkEdit;
    PB1: TProgressBar;
    Grade1: TStringGrid;
    Label1: TLabel;
    EdUnidCouro: TdmkEdit;
    Button2: TButton;
    procedure SbSelArqClick(Sender: TObject);
    procedure SbAbreClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SGReceitaDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
    FFeitos: array of Boolean;
    FAtuCol, FAtuLin: Integer;
    //
    procedure DefineReceitaAImportar();
    procedure CarregaReceitaRibeira();
    function  Igual(A, B: String): Boolean;
    function  PosZero(A, B: String): Boolean;
    function  DefValRefRib(const Item: TValCelRecXLS; var Texto: String;
              const Locate: Boolean = False): Boolean;
    procedure SeparaEspessura(const Texto: String; var Cul, Cab: String);
    function  Percentual(Texto: String): String;
    function  Separa_Temperatura(Texto: String): String;
    procedure SeparaQuantiaEUnidCouro(const Texto: String; var Quantia: Double;
              var UnidCouro: String);
    function  Tempo(Texto: String): String;
    function  Separa_pH(Texto: String): String;
    function  ConverteParaRecipePositionType(Produto: String): String;
    function  SetRunningTime(Tempo: String): TTime;
    function  ProdutoAuditado(TxtProd, TxtEmb: String; var ProdCod: Integer;
              var ProdNom, Embalagem, Diluicao, Temperatura: String): Boolean;
  public
    { Public declarations }
  end;

var
  FmRecXlsAnyOpn: TFmRecXlsAnyOpn;

implementation

uses UnMyObjects, Principal, {***RecRibCab, UnCreate_TFC,***} Module;

{$R *.dfm}

procedure TFmRecXlsAnyOpn.Button1Click(Sender: TObject);
{***
var
  Form: TForm;
  FmRRC: TFmRecRibCab;
  N, I: Integer;
  Quantia: Double;
  UnidCouro: String;
  NomeArq, Ext: String;
***}
begin
{***
  Form := MyObjects.FormTDICria(TFmRecRibCab, FmPrincipal.PageControl1,
    FmPrincipal.AdvToolBarPager1, True);
  FmRRC := TFmRecRibCab(Form);
  SeparaQuantiaEUnidCouro(EdQuantia.Text, Quantia, UnidCouro);
  NomeArq := ExtractFileName(LaNome.Caption);
  Ext := ExtractFileExt(LaNome.Caption);
  N := Length(NomeArq) - Length(Ext);
  NomeArq := Copy(NomeArq, 1, N);
  //
  FmRRC.FNomeSetor                 := TSReceita.Caption;
  FmRRC.EdNomeArq.Text             := NomeArq;
  FmRRC.EdCliNome.ValueVariant     := EdCliNome.ValueVariant;
  FmRRC.EdTecnico.ValueVariant     := EdTecnico.ValueVariant;
  FmRRC.EdAccount.ValueVariant     := EdAccount.ValueVariant;
  FmRRC.EdRendiment.ValueVariant   := EdRendiment.ValueVariant;
  FmRRC.EdArtigo.ValueVariant      := EdArtigo.ValueVariant;
  FmRRC.EdCor.ValueVariant         := EdCor.ValueVariant;
  FmRRC.EdEspRebCul.ValueVariant   := EdEspRebCul.ValueVariant;
  FmRRC.EdEspRebCab.ValueVariant   := EdEspRebCab.ValueVariant;
  FmRRC.EdEspFimCul.ValueVariant   := EdEspFimCul.ValueVariant;
  FmRRC.EdEspFimCab.ValueVariant   := EdEspFimCab.ValueVariant;
  FmRRC.EdRelatA.ValueVariant      := EdRelatA.ValueVariant;
  FmRRC.EdPesoCouro.ValueVariant   := EdPesoCouro.ValueVariant;
  FmRRC.EdCambio.ValueVariant      := EdCambio.ValueVariant;
  FmRRC.EdQuantia.ValueVariant     := Quantia;
  FmRRC.EdUnidCouro.ValueVariant   := UnidCouro;
  FmRRC.EdData.ValueVariant        := EdData.ValueVariant;
  FmRRC.EdObserv.ValueVariant      := EdObserv.ValueVariant;
  //
  FmRRC.EdSetor.ValueVariant       := FmRRC.FNomeSetor;
  //
  FmRRC.TbRecRibIts.Close;
  FmRRC.F_RecRibits := Create_TFC.RecriaMemTable(ntcRecRibIts, 'recribits',
    Dmod.QrUpd, True, False);
  FmRRC.TbRecRibIts.Open;
  for I := 1 to SGReceita.RowCount - 1 do
  begin
    FmRRC.TbRecRibIts.Insert;
    //
(*
  SGReceita.Cells[00, 00] :=  'Lin';
  SGReceita.Cells[01, 00] :=  'Processo';
  SGReceita.Cells[02, 00] :=  'Nome item';
  SGReceita.Cells[03, 00] :=  '%';
  SGReceita.Cells[04, 00] :=  'L / Kg';
  SGReceita.Cells[05, 00] :=  'PRODUTO';
  SGReceita.Cells[06, 00] :=  '1:X';
  SGReceita.Cells[07, 00] :=  '�C';
  SGReceita.Cells[08, 00] :=  'Rodar';
  SGReceita.Cells[09, 00] :=  'Parar';
  SGReceita.Cells[10, 00] :=  'pH';
  SGReceita.Cells[11, 00] :=  'Observa��es';
  SGReceita.Cells[12, 00] :=  'RPT';
  SGReceita.Cells[13, 00] :=  'Instru��es';
  SGReceita.Cells[14, 00] :=  'Pre�o';
*)
    FmRRC.TbRecRibItsSeq.Value := I;
    FmRRC.TbRecRibItsProcesso.Value := SGReceita.Cells[01, I];
    FmRRC.TbRecRibItsNomeItem.Value := SGReceita.Cells[02, I];
    FmRRC.TbRecRibItsPercentual.Value := Geral.DMV(SGReceita.Cells[03, I]);
    FmRRC.TbRecRibItsQuantidade.Value := Geral.DMV(SGReceita.Cells[04, I]);
    FmRRC.TbRecRibItsNomProduto.Value := SGReceita.Cells[05, I];
    FmRRC.TbRecRibItsDiluiProdu.Value := Geral.IMV(SGReceita.Cells[06, I]);
    FmRRC.TbRecRibItsTemperatur.Value := Geral.DMV(SGReceita.Cells[07, I]);
    FmRRC.TbRecRibItsRodarTempo.Value := SetRunningTime(SGReceita.Cells[08, I]);
    FmRRC.TbRecRibItsPararTempo.Value := SetRunningTime('0:00');
    FmRRC.TbRecRibItspHDesejado.Value := Geral.DMV(SGReceita.Cells[10, I]);
    FmRRC.TbRecRibItsObservacao.Value := SGReceita.Cells[11, I];
    FmRRC.TbRecRibItsRecipPosTp.Value := Geral.IMV('0' + SGReceita.Cells[12, I]);
    FmRRC.TbRecRibItsInstrucoes.Value := SGReceita.Cells[13, I];
    FmRRC.TbRecRibItsPrecoProdu.Value := Geral.DMV(SGReceita.Cells[14, I]);
    //
    FmRRC.TbRecRibIts.Post;
  end;
  Close;
***}
end;

procedure TFmRecXlsAnyOpn.CarregaReceitaRibeira();
var
  Lins, Cols, I, J, Ini, Fim, K, ProdCod: Integer;
  Texto, TxtCel, Cul, Cab, TxtProd, TxtEmb, ProdNom, Diluicao, Temperatura,
  Embalagem: String;
  //Continua: Boolean;
begin
  Lins := Grade1.RowCount;
  Cols := Grade1.ColCount;
  //
  for I := 1 to Lins - 1 do
  begin
    for J := 1 to Cols - 1 do
    begin
      FAtuCol := J;
      FAtuLin := I;
      if DefValRefRib(vcrxCliente, TxtCel) then
        EdCliNome.Text := TxtCel else
      if DefValRefRib(vcrxTecnico, TxtCel) then
        EdTecnico.Text := TxtCel else
      if DefValRefRib(vcrxAccount, TxtCel) then
        EdAccount.Text := TxtCel else
      if DefValRefRib(vcrxArtigo, TxtCel) then
        EdArtigo.Text := TxtCel else
      if DefValRefRib(vcrxEspessura, TxtCel) then
      begin
        SeparaEspessura(TxtCel, Cul, Cab);
        EdEspFimCul.Text := Cul;
        EdEspFimCab.Text := Cab;
      end else
      if DefValRefRib(vcrxRebaixe, TxtCel, True) then
      begin
        SeparaEspessura(TxtCel, Cul, Cab);
        EdEspRebCul.Text := Cul;
        EdEspRebCab.Text := Cab;
      end else
      if DefValRefRib(vcrxRendiment, TxtCel) then
        EdRendiment.Text := TxtCel else
      if DefValRefRib(vcrxRelatA, TxtCel) then
        EdRelatA.Text := TxtCel else
      if DefValRefRib(vcrxPesoCouro, TxtCel) then
        EdPesoCouro.Text := TxtCel else
      if DefValRefRib(vcrxCambio, TxtCel) then
        EdCambio.Text := TxtCel else
      if DefValRefRib(vcrxQuantia, TxtCel) then
        EdQuantia.Text := TxtCel else
      if DefValRefRib(vcrxData, TxtCel) then
        EdData.Text := TxtCel else
(*
      if DefValRefRib(vcrx??, TxtCel) then
        Ed??.Text := TxtCel else
*)
    end;

  end;
  EdObserv.Text := Grade1.Cells[1, 7];
  Ini := 0;
  for I := 1 to Lins - 1 do
  begin
    Texto := Grade1.Cells[1, I];
    if (Igual(Texto, 'PROCESSO'))
    or (PosZero(Texto, 'PROCES')) then
    begin
      Ini := I;
      Break;
    end;
  end;
  Fim := 0;
  for I := Ini to Lins - 1 do
  begin
    Texto := Trim(Grade1.Cells[1, I]);
    if pos('CUSTOS :', Texto) > 0 then
    begin
      Fim := I -1;
      Break;
    end;
  end;
  if Fim = 0 then
    Fim := Grade1.RowCount - 1;
  if Ini > 0 then
  begin
    K := 0;
    for I := Ini + 1 to Fim do
    begin
      K := K + 1;
      //
      SGReceita.RowCount := K + 1;
      //for J := 1 to Cols - 1 do
      begin
{       Parei aqui! Ver embalagem!}
        TxtProd := Trim(Grade1.Cells[05, I] + Grade1.Cells[06, I]);
        while pos('  ', TxtProd) > 0 do
          TxtProd := Geral.Substitui(TxtProd, '  ', ' ');
      // Parei aqui! 2014-05-31
        TxtEmb := '';
        Embalagem := '';
        //
//        ProdutoAuditado(TxtProd, TxtEmb, ProdCod, ProdNom, Diluicao, Temperatura);
        ProdutoAuditado(TxtProd, TxtEmb, ProdCod, ProdNom, Embalagem, Diluicao, Temperatura);
        if Temperatura = '' then
          Temperatura := Separa_Temperatura(Trim(Grade1.Cells[08, I]));
        //SGReceita.Cells[J, K] := Grade1.Cells[J, I];
        SGReceita.Cells[00, K] := Geral.FFN(K, 3);
        SGReceita.Cells[01, K] := Trim(Grade1.Cells[01, I] + Grade1.Cells[02, I]);
        SGReceita.Cells[02, K] := '';
        SGReceita.Cells[03, K] := Percentual(Grade1.Cells[03, I]);
        SGReceita.Cells[04, K] := Trim(Grade1.Cells[04, I]);
        SGReceita.Cells[05, K] := ProdNom;
        SGReceita.Cells[06, K] := Diluicao;
        SGReceita.Cells[07, K] := Temperatura;
        SGReceita.Cells[08, K] := Tempo(Trim(Grade1.Cells[07, I]));
        SGReceita.Cells[09, K] := '';
        SGReceita.Cells[10, K] := Separa_pH(Grade1.Cells[08, I]);
        SGReceita.Cells[11, K] := Trim(Grade1.Cells[09, I]);
        SGReceita.Cells[12, K] := ConverteParaRecipePositionType(Trim(SGReceita.Cells[04, K]));
        SGReceita.Cells[13, K] :=  '';
        SGReceita.Cells[14, K] := Trim(Grade1.Cells[10, I]);
{}
      end;
    end;
  end;
end;


function TFmRecXlsAnyOpn.ConverteParaRecipePositionType(Produto: String): String;
var
  Txt: String;
begin
  Txt := AnsiUppercase(Produto);
  if pos('SELLATAN', Txt) > 0 then Result := '1' else
  //...
  Result := IntToStr(Random(3));
end;

procedure TFmRecXlsAnyOpn.DefineReceitaAImportar();
var
  I, J, Lins: Integer;
  Texto: String;
begin
  Screen.Cursor := crHourGlass;
  try
    MyObjects.Informa(LaAviso, True, 'Lendo receita');
    J := Integer(High(TValCelRecXLS));
    SetLength(FFeitos, J);
    for I := 0 to J - 1 do
      FFeitos[I] := False;
    Lins := Grade1.RowCount;
    //Cols := Grade1.ColCount;
    if Lins > 1 then
    begin
      Texto := Trim(Grade1.Cells[04, 01]);
      if Igual(Texto, 'Recurtimento')
      or Igual(Texto, 'CALEIRO    ou    CURTIMENTO') then
      begin
        //PageControl1.Pages[1].Caption := Texto;
        TSReceita.Caption := Texto;
        CarregaReceitaRibeira();
      //...
      end else
      begin
        Geral.MB_Aviso('Tipo de Receita desconhecida: "' + Texto + '"');
      end;
      //
      LaNome.Caption := EdArq.Text;
      //
      MyObjects.Informa(LaAviso, False, '...');
      PageControl1.ActivePageIndex := 1;
    end else
    begin
      MyObjects.Informa(LaAviso, True, 'Arquivo inv�lido!');
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TFmRecXlsAnyOpn.DefValRefRib(const Item: TValCelRecXLS; var Texto: String;
  const Locate: Boolean): Boolean;
var
  ValCel: String;
  procedure SetaValor(Titulo: array of String; Cols, Lins: array of Integer);
  var
    I, J: Integer;
    Achou: Boolean;
    Txt: String;
  begin
    Result := False;
    Achou := False;
    for I := Low(Titulo) to High(Titulo) do
    begin
      if Locate then
        Achou := pos(Titulo[I], ValCel) = 1
      else
        Achou := Igual(Titulo[I], ValCel);
      if Achou then
        Break;
    end;
    if Achou then
    begin
      Txt := '';
      if (High(Cols) = 0) and (High(Lins) = 0) and
      (Cols[0] = 0) and (Lins[0] = 0) then
        Txt := Grade1.Cells[FAtuCol, FAtuLin]
      else
      begin
        for I := Low(Cols) to High(Cols) do
        begin
          for J := Low(Lins) to High(Lins) do
          begin
            if (Cols[I] <> 0 ) or (Lins[J] <> 0) then
              Txt := Txt + Grade1.Cells[FAtuCol + Cols[I], FAtuLin + Lins[J]];
          end;
        end;
      end;
      if Txt <> '' then
      begin
        Texto := Txt;
        Result := True;
        FFeitos[Integer(Item)] := True;
      end;
    end;
  end;
begin
  Result := False;
  if FFeitos[Integer(Item)] then
    Exit;
  ValCel := Grade1.Cells[FAtuCol, FAtuLin];
  if Trim(ValCel) = '' then
    Exit;
  case Item of
    vcrxCliente:   SetaValor(['CLIENTE :', 'CLIENTE:', 'CLIENTE'], [1], [0]);
    vcrxTecnico:   SetaValor(['T�CNICO :'], [1,2], [0]);
    vcrxAccount:   SetaValor(['ACCOUNT :'], [1], [0]);
    vcrxArtigo:    SetaValor(['ARTIGO :'], [1], [0]);
    vcrxEspessura: SetaValor(['Espessura :'], [1,2], [0]);
    vcrxRendiment: SetaValor(['Rendimento :'], [1], [0]);
    vcrxRelatA:    SetaValor(['% Relat. a:'], [0,1], [0,1]);
    vcrxPesoCouro: SetaValor(['Peso :'], [1,2], [0]);
    vcrxCambio:    SetaValor(['C�mbio :'], [1], [0]);
    vcrxQuantia:   SetaValor(['Quantia :'], [1,2], [0]);
    vcrxData:      SetaValor(['Data :'], [1], [0]);
    vcrxRebaixe:   SetaValor(['Reb:', 'Rebaixe:', 'Reb.', 'Rebaixe'], [0], [0]);
  end;
end;

procedure TFmRecXlsAnyOpn.FormCreate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  SGReceita.ColCount :=  15;
  SGReceita.ColWidths[00] :=  40;
  SGReceita.ColWidths[01] := 220;
  SGReceita.ColWidths[02] :=  72;
  SGReceita.ColWidths[03] :=  72;
  SGReceita.ColWidths[04] := 100;
  SGReceita.ColWidths[05] := 180;
  SGReceita.ColWidths[06] :=  44;
  SGReceita.ColWidths[07] :=  44;
  SGReceita.ColWidths[08] :=  44;
  SGReceita.ColWidths[09] :=  44;
  SGReceita.ColWidths[10] :=  40;
  SGReceita.ColWidths[11] := 150;
  SGReceita.ColWidths[12] :=  40;
  SGReceita.ColWidths[13] := 150;
  SGReceita.ColWidths[14] :=  72;
  //SGReceita.ColWidths[1] := ;
  SGReceita.Cells[00, 00] := 'Lin';
  SGReceita.Cells[01, 00] := 'Processo';
  SGReceita.Cells[02, 00] := 'Nome item';
  SGReceita.Cells[03, 00] := '%';
  SGReceita.Cells[04, 00] := 'L / Kg';
  SGReceita.Cells[05, 00] := 'PRODUTO';
  SGReceita.Cells[06, 00] := '1:X';
  SGReceita.Cells[07, 00] := '�C';
  SGReceita.Cells[08, 00] := 'Rodar';
  SGReceita.Cells[09, 00] := 'Parar';
  SGReceita.Cells[10, 00] := 'pH';
  SGReceita.Cells[11, 00] := 'Observa��es';
  SGReceita.Cells[12, 00] := 'RPT';
  SGReceita.Cells[13, 00] := 'Instru��es';
  SGReceita.Cells[14, 00] := 'Pre�o';
  //SGReceita.Cells[0, 00] :=  '';
end;

function TFmRecXlsAnyOpn.Igual(A, B: String): Boolean;
begin
  Result := AnsiUppercase(A) = AnsiUppercase(B);
end;

function TFmRecXlsAnyOpn.Percentual(Texto: String): String;
var
  Porcento: Double;
begin
  Porcento := Geral.DMV(Texto);
  Porcento := Porcento * 100;
  if Porcento >= 0.000001 then
    Result := Geral.FFT(Porcento, 4, siPositivo)
  else
    Result := '';
end;

function TFmRecXlsAnyOpn.PosZero(A, B: String): Boolean;
begin
  Result := Pos(AnsiUppercase(B), AnsiUppercase(A)) > 0;
end;

function TFmRecXlsAnyOpn.ProdutoAuditado(TxtProd, TxtEmb: String; var ProdCod: Integer;
var ProdNom, Embalagem, Diluicao, Temperatura: String): Boolean;
begin
  Result := False;
  if Trim(TxtProd) = '' then
  begin
    Result      := True;
    ProdCod     := 0;
    ProdNom     := '';
    Embalagem   := TxtEmb;
    Diluicao    := '';
    Temperatura := '';
  end else
  if AppPF.LocalizaProdutoSohNome(TxtProd) then
  begin
    ProdCod     := 0;
    ProdNom     := TxtProd;
    Embalagem   := TxtEmb;
    Diluicao    := '';
    Temperatura := '';
    Result := True;
  end else
  begin
    // Parei aqui - Ver o que fazer
    Result := AppPF.GerenciaProduto(TxtProd, TxtEmb, ProdCod, ProdNom, Embalagem,
      Diluicao, Temperatura);
  end;
end;

procedure TFmRecXlsAnyOpn.SbAbreClick(Sender: TObject);
const
  ColIni = 1;
  RowIni = 1;
  ExcluiLinhas = True;
begin
  //MyObjects.LimpaGrade(SGReceita, ColIni, RowIni, ExcluiLinhas);
  MyObjects.Xls_To_StringGrid(Grade1, EdArq.Text, PB1, LaAviso, nil, 1);
  DefineReceitaAImportar();
  //CarregaReceitaRibeira;
end;

procedure TFmRecXlsAnyOpn.SbSelArqClick(Sender: TObject);
var
  Arquivo: String;
begin
  if MyObjects.FileOpenDialog(Self, '', '', 'Sele��o de arquivo', '', [], Arquivo) then
    EdArq.ValueVariant := Arquivo
  else
    EdArq.ValueVariant := '';
end;

procedure TFmRecXlsAnyOpn.SeparaEspessura(const Texto: String; var Cul,
  Cab: String);
var
  I, Ini, Mid: Integer;
  Txt: String;
begin
  Cul := '';
  Cab := '';
  Ini := -1;
  if Length(Texto) > 1 then
  begin
    for I := 1 to Length(Texto) -1 do
    begin
      if CharInSet(Texto[I], ['0'..'9']) then
      begin
        Ini := I;
        Break;
      end;
    end;
    Mid := 0;
    if Ini > -1 then
    begin
      Txt := Copy(Texto, Ini);
      Mid := pos('-', Txt);
      if Mid < 1 then
        Mid := pos('/', Txt);
      if Mid < 1 then
        Mid := pos('\', Txt);
      if Mid < 1 then
        Mid := pos(':', Txt);
      if Mid < 1 then
        Mid := pos('_', Txt);
      if Mid < 1 then
        Mid := pos(' ', Txt);
    end;
    if Mid > 1 then
    begin
      Cul := Geral.SoNumeroESinalEVirgula_TT(Copy(Txt, 1, Mid - 1));
      Cab := Geral.SoNumeroESinalEVirgula_TT(Copy(Txt, Mid + 1));
    end else
    begin
      Cul := Txt;
      Cab := '';
    end;
  end;
end;

procedure TFmRecXlsAnyOpn.SeparaQuantiaEUnidCouro(const Texto: String;
  var Quantia: Double; var UnidCouro: String);
var
  QtdTxt: String;
  I: Integer;
  InNum: Boolean;
begin
  QtdTxt := '';
  UnidCouro := '';
  InNum := True;
  //
  for I := 1 to Length(Texto) do
  begin
    if InNum then
    begin
      if CharInSet(Texto[I], ['0'..'9', '.', ',']) then
      begin
        QtdTxt := QtdTxt + Texto[I];
      end else
      begin
        InNum := False;
        UnidCouro := UnidCouro + Texto[I];
      end;
    end else
    begin
      UnidCouro := UnidCouro + Texto[I];
    end;
  end;
  UnidCouro := Trim(UnidCouro);
  Quantia := Geral.DMV(QtdTxt);
end;

function TFmRecXlsAnyOpn.Separa_pH(Texto: String): String;
var
  I, K, pI, pF: Integer;
  Txt: String;
  Ini, Fim: Boolean;
begin
  Result := '';
  pI := -1;
  pF := -1;
  if Length(Texto) > 1 then
  begin
    K := pos(AnsiUppercase('pH'), AnsiUppercase(Texto));
    if K > 0 then
    begin
      Txt := Copy(Texto, K);
      for I := 1 to Length(Txt) -1 do
      begin
        if CharInSet(Txt[I], ['0'..'9']) then
        begin
          if pI = -1 then
            pI := I;
        end else
        begin
          if pI > -1 then
            pF := I;
        end;
        if (pI > -1) and (pF > -1) then
          Break;
      end;
      if pI > -1 then
      begin
        if pF > -1 then
          Result := Copy(Txt, pI, pF -1)
        else
          Result := Copy(Txt, pI)
      end;
    end;
  end;
  Result := Trim(Result);
end;

function TFmRecXlsAnyOpn.Separa_Temperatura(Texto: String): String;
var
  I, K, pI, pF: Integer;
  Txt: String;
  Ini, Fim: Boolean;
begin
  Result := '';
  pI := -1;
  pF := -1;
  if Length(Texto) > 1 then
  begin
    K := pos(AnsiUppercase('�C'), AnsiUppercase(Texto));
    if K = 0 then
      K := pos(AnsiUppercase('� C'), AnsiUppercase(Texto));
    if K = 0 then
      K := pos(AnsiUppercase('�C'), AnsiUppercase(Texto));
    if K = 0 then
      K := pos(AnsiUppercase('� C'), AnsiUppercase(Texto));
    if K > 0 then
    begin
      Txt := Copy(Texto, 1, K);
      for I := Length(Txt) -1 to 1 do
      begin
        if CharInSet(Txt[I], ['0'..'9']) then
        begin
          if pF = -1 then
            pF := I;
        end else
        begin
          if pF > -1 then
            pI := I;
        end;
        if (pI > -1) and (pF > -1) then
          Break;
      end;
      if pI > -1 then
      begin
        if pF > -1 then
          Result := Copy(Txt, pI, pF -1)
        else
          Result := Copy(Txt, pI)
      end;
    end;
  end;
  Result := Geral.SoNumeroEVirgula_TT(Result);
end;

function TFmRecXlsAnyOpn.SetRunningTime(Tempo: String): TTime;
begin
  Result := 0;
  if Trim(Tempo) <> '' then
    Result := StrToTime(Tempo);
end;

procedure TFmRecXlsAnyOpn.SGReceitaDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  Cor, c, r: Integer;
  Texto: String;
begin
{
  if ARow = 0 then
    Exit;
  c := 1;//SGReceita.FixedCols;
  r := 1;//SGReceita.FixedRows;
  //
  Texto := SGReceita.Cells[ACol, ARow];
  //
  if (ACol = 0) or (ARow = 0) then
    Cor := Panel3.Color
  else //if SGReceita.Cells[Col, Row] <> '' then
    Cor := clWindow;
  if (ACol in ([0,5])) then
    MyObjects.DesenhaTextoEmStringGrid(SGReceita, Rect, clBlack,
      Cor(*FmPrincipal.sd1.Colors[csButtonFace]*), taCenter,
      Texto, c, r, False)
  else
  if (ACol in ([1,4,7])) or (ARow = 0) then
    MyObjects.DesenhaTextoEmStringGrid(SGReceita, Rect, clBlack,
      Cor(*FmPrincipal.sd1.Colors[csButtonFace]*), taLeftJustify,
      Texto, c, r, False)
  else
    MyObjects.DesenhaTextoEmStringGrid(SGReceita, Rect, clBlack,
      Cor(*FmPrincipal.sd1.Colors[csButtonFace]*), taRightJustify,
      Texto, c, r, False);
}
end;

function TFmRecXlsAnyOpn.Tempo(Texto: String): String;
var
  hhnn: Double;
begin
  hhnn := Geral.DMV(Texto);
  if hhnn >= 1/60/24 then
    Result := Geral.FDT(hhnn, 102)
  else
    Result := '';
end;

end.
