object FmReceitasCab: TFmReceitasCab
  Left = 371
  Top = 169
  Caption = 'QUI-RECEI-001 :: Receitas de Ribeira'
  ClientHeight = 548
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 52
    Width = 1008
    Height = 197
    Align = alTop
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 0
    object PainelDadosReceita: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 197
      Align = alClient
      Alignment = taLeftJustify
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 0
      object Label16: TLabel
        Left = 6
        Top = 4
        Width = 40
        Height = 13
        Caption = 'N'#250'mero:'
        FocusControl = DBEdit12
      end
      object Label17: TLabel
        Left = 113
        Top = 4
        Width = 39
        Height = 13
        Caption = 'Cria'#231#227'o:'
        FocusControl = DBEdit13
      end
      object Label18: TLabel
        Left = 177
        Top = 4
        Width = 48
        Height = 13
        Caption = 'Altera'#231#227'o:'
        FocusControl = DBEdit14
      end
      object Label30: TLabel
        Left = 241
        Top = 4
        Width = 81
        Height = 13
        Caption = 'Nome da receita:'
        FocusControl = DBEdit26
      end
      object Label22: TLabel
        Left = 667
        Top = 3
        Width = 28
        Height = 13
        Caption = 'Setor:'
      end
      object Label23: TLabel
        Left = 448
        Top = 42
        Width = 102
        Height = 13
        Caption = 'T'#233'cnico respons'#225'vel:'
      end
      object Label26: TLabel
        Left = 810
        Top = 42
        Width = 59
        Height = 13
        Caption = 'Tempo total:'
        FocusControl = DBEdit22
      end
      object Label25: TLabel
        Left = 702
        Top = 42
        Width = 101
        Height = 13
        Caption = 'Tempo em opera'#231#227'o:'
        FocusControl = DBEdit21
      end
      object Label24: TLabel
        Left = 594
        Top = 42
        Width = 72
        Height = 13
        Caption = 'Tempo parado:'
        FocusControl = DBEdit20
      end
      object Label5: TLabel
        Left = 922
        Top = 42
        Width = 19
        Height = 13
        Caption = 'mm:'
      end
      object Label1: TLabel
        Left = 71
        Top = 42
        Width = 27
        Height = 13
        Caption = 'Peso:'
      end
      object Label2: TLabel
        Left = 6
        Top = 42
        Width = 58
        Height = 13
        Caption = 'Quantidade:'
      end
      object Label3: TLabel
        Left = 962
        Top = 42
        Width = 29
        Height = 13
        Caption = 'm. kg:'
      end
      object Label4: TLabel
        Left = 690
        Top = 82
        Width = 35
        Height = 13
        Caption = 'Cliente:'
      end
      object SpeedButton6: TSpeedButton
        Left = 772
        Top = 18
        Width = 23
        Height = 21
        Caption = '...'
        OnClick = SpeedButton6Click
      end
      object Label7: TLabel
        Left = 260
        Top = 82
        Width = 48
        Height = 13
        Caption = 'Substrato:'
      end
      object Label9: TLabel
        Left = 802
        Top = 3
        Width = 42
        Height = 13
        Caption = 'Rebaixe:'
      end
      object Label6: TLabel
        Left = 850
        Top = 3
        Width = 26
        Height = 13
        Caption = 'Semi:'
      end
      object Label8: TLabel
        Left = 898
        Top = 3
        Width = 31
        Height = 13
        Caption = 'Acab.:'
      end
      object Label10: TLabel
        Left = 58
        Top = 4
        Width = 33
        Height = 13
        Caption = 'Marca:'
        FocusControl = DBEdSigla
      end
      object Label11: TLabel
        Left = 946
        Top = 3
        Width = 36
        Height = 13
        Caption = 'Vers'#227'o:'
      end
      object Label12: TLabel
        Left = 476
        Top = 82
        Width = 30
        Height = 13
        Caption = 'Artigo:'
      end
      object Label13: TLabel
        Left = 260
        Top = 122
        Width = 106
        Height = 13
        Caption = 'Arquivamento N'#237'vel 5:'
      end
      object Label14: TLabel
        Left = 408
        Top = 122
        Width = 106
        Height = 13
        Caption = 'Arquivamento N'#237'vel 4:'
      end
      object Label15: TLabel
        Left = 556
        Top = 122
        Width = 106
        Height = 13
        Caption = 'Arquivamento N'#237'vel 3:'
      end
      object Label19: TLabel
        Left = 708
        Top = 122
        Width = 106
        Height = 13
        Caption = 'Arquivamento N'#237'vel 2:'
      end
      object Label20: TLabel
        Left = 856
        Top = 122
        Width = 106
        Height = 13
        Caption = 'Arquivamento N'#237'vel 1:'
      end
      object Label21: TLabel
        Left = 160
        Top = 40
        Width = 62
        Height = 13
        Caption = 'AreaM2Rebx'
        FocusControl = DBEdit15
      end
      object Label27: TLabel
        Left = 236
        Top = 40
        Width = 60
        Height = 13
        Caption = 'AreaM2Semi'
        FocusControl = DBEdit18
      end
      object Label28: TLabel
        Left = 312
        Top = 40
        Width = 50
        Height = 13
        Caption = 'BRL_USD'
        FocusControl = DBEdit19
      end
      object Label29: TLabel
        Left = 8
        Top = 164
        Width = 61
        Height = 13
        Caption = 'Observa'#231#227'o:'
        FocusControl = DBMemo1
      end
      object DBEdit12: TDBEdit
        Left = 6
        Top = 18
        Width = 48
        Height = 21
        TabStop = False
        Color = clBtnFace
        DataField = 'Codigo'
        DataSource = DsReceitasCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
      end
      object DBEdit13: TDBEdit
        Left = 113
        Top = 18
        Width = 60
        Height = 21
        TabStop = False
        Color = clBtnFace
        DataField = 'DataCad'
        DataSource = DsReceitasCab
        ReadOnly = True
        TabOrder = 2
      end
      object DBEdit14: TdmkEdit
        Left = 177
        Top = 18
        Width = 60
        Height = 21
        TabStop = False
        Color = clBtnFace
        ReadOnly = True
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object DBEdit26: TDBEdit
        Left = 240
        Top = 18
        Width = 421
        Height = 21
        DataField = 'Nome'
        DataSource = DsReceitasCab
        TabOrder = 4
      end
      object DBEdit22: TDBEdit
        Left = 810
        Top = 56
        Width = 108
        Height = 21
        TabStop = False
        Color = clBtnFace
        DataField = 'HHMM_T'
        DataSource = DsReceitasCab
        ReadOnly = True
        TabOrder = 22
      end
      object DBEdit21: TDBEdit
        Left = 702
        Top = 56
        Width = 108
        Height = 21
        TabStop = False
        Color = clBtnFace
        DataField = 'HHMM_R'
        DataSource = DsReceitasCab
        ReadOnly = True
        TabOrder = 21
      end
      object DBEdit20: TDBEdit
        Left = 594
        Top = 56
        Width = 107
        Height = 21
        TabStop = False
        Color = clBtnFace
        DataField = 'HHMM_P'
        DataSource = DsReceitasCab
        ReadOnly = True
        TabOrder = 20
      end
      object DBEdit1: TDBEdit
        Left = 71
        Top = 56
        Width = 86
        Height = 21
        DataField = 'Peso'
        DataSource = DsReceitasCab
        TabOrder = 15
      end
      object DBEdit2: TDBEdit
        Left = 6
        Top = 56
        Width = 61
        Height = 21
        DataField = 'Qtde'
        DataSource = DsReceitasCab
        TabOrder = 14
      end
      object DBEdit3: TDBEdit
        Left = 921
        Top = 56
        Width = 36
        Height = 21
        TabStop = False
        Color = clBtnFace
        DataField = 'EMCM'
        ReadOnly = True
        TabOrder = 23
      end
      object DBEdit5: TDBEdit
        Left = 962
        Top = 56
        Width = 36
        Height = 21
        TabStop = False
        Color = clBtnFace
        DataField = 'MEDIAPESO'
        DataSource = DsReceitasCab
        ReadOnly = True
        TabOrder = 24
      end
      object DBRGTipificacao: TDBRadioGroup
        Left = 6
        Top = 80
        Width = 247
        Height = 81
        Caption = ' Tipifica'#231#227'o: '
        Columns = 3
        DataField = 'Tipificacao'
        DataSource = DsReceitasCab
        Items.Strings = (
          'N/D'
          'Integral'
          'Laminado'
          'Repartido'
          'Dividido'
          'Rebaixado'
          'Raspa'
          'Apara'
          'Tapete')
        ParentBackground = True
        TabOrder = 25
        Values.Strings = (
          '0'
          '1'
          '2'
          '3'
          '4'
          '5'
          '6'
          '7'
          '8')
      end
      object DBEdit10: TDBEdit
        Left = 690
        Top = 96
        Width = 56
        Height = 21
        DataField = 'Cliente'
        DataSource = DsReceitasCab
        TabOrder = 28
      end
      object DBCheckBox1: TDBCheckBox
        Left = 8
        Top = 180
        Width = 49
        Height = 17
        Caption = 'Ativa.'
        DataField = 'Ativo'
        DataSource = DsReceitasCab
        TabOrder = 35
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object DBLookupComboBox1: TDBLookupComboBox
        Left = 748
        Top = 96
        Width = 249
        Height = 21
        DataField = 'Cliente'
        DataSource = DsReceitasCab
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsEntidadesEmpr
        TabOrder = 29
      end
      object CBSetor: TDBLookupComboBox
        Left = 666
        Top = 18
        Width = 107
        Height = 21
        DataField = 'Setor'
        DataSource = DsReceitasCab
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsSetoresCad
        TabOrder = 5
        OnClick = CBSetorClick
      end
      object DBEdit7: TDBEdit
        Left = 850
        Top = 18
        Width = 20
        Height = 21
        DataField = 'EspesSemiCab'
        DataSource = DsReceitasCab
        TabOrder = 8
      end
      object DBEdit8: TDBEdit
        Left = 870
        Top = 18
        Width = 20
        Height = 21
        DataField = 'EspesSemiCul'
        DataSource = DsReceitasCab
        TabOrder = 9
      end
      object DBEdit9: TDBEdit
        Left = 898
        Top = 18
        Width = 20
        Height = 21
        DataField = 'EspesFiniCab'
        DataSource = DsReceitasCab
        TabOrder = 10
      end
      object DBEdit11: TDBEdit
        Left = 918
        Top = 18
        Width = 20
        Height = 21
        DataField = 'EspesFiniCul'
        DataSource = DsReceitasCab
        TabOrder = 11
      end
      object DBEdSigla: TDBEdit
        Left = 58
        Top = 18
        Width = 52
        Height = 21
        TabStop = False
        CharCase = ecUpperCase
        DataField = 'Sigla'
        DataSource = DsReceitasCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
      end
      object DBEdit16: TDBEdit
        Left = 946
        Top = 18
        Width = 26
        Height = 21
        DataField = 'Versao'
        DataSource = DsReceitasCab
        TabOrder = 12
      end
      object DBEdit17: TDBEdit
        Left = 972
        Top = 18
        Width = 26
        Height = 21
        DataField = 'VerSub'
        DataSource = DsReceitasCab
        TabOrder = 13
      end
      object DBLookupComboBox2: TDBLookupComboBox
        Left = 448
        Top = 56
        Width = 140
        Height = 21
        DataField = 'Account'
        DataSource = DsReceitasCab
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsEntidadesAccn
        TabOrder = 19
      end
      object DBLookupComboBox3: TDBLookupComboBox
        Left = 260
        Top = 96
        Width = 209
        Height = 21
        DataField = 'Substrato'
        DataSource = DsReceitasCab
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsSubstratos
        TabOrder = 26
      end
      object DBLookupComboBox4: TDBLookupComboBox
        Left = 476
        Top = 96
        Width = 209
        Height = 21
        DataField = 'Artigo'
        DataSource = DsReceitasCab
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsArtigos
        TabOrder = 27
      end
      object DBEdit4: TDBEdit
        Left = 802
        Top = 18
        Width = 20
        Height = 21
        DataField = 'EspesRebxCab'
        DataSource = DsReceitasCab
        TabOrder = 6
      end
      object DBEdit6: TDBEdit
        Left = 822
        Top = 18
        Width = 20
        Height = 21
        DataField = 'EspesRebxCul'
        DataSource = DsReceitasCab
        TabOrder = 7
      end
      object CBReceiGruNiv5: TDBLookupComboBox
        Left = 260
        Top = 136
        Width = 144
        Height = 21
        DataField = 'GrupNiv5'
        DataSource = DsReceitasCab
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsReceiGruNiv5
        TabOrder = 30
        OnClick = CBReceiGruNiv5Click
      end
      object CBReceiGruNiv4: TDBLookupComboBox
        Left = 408
        Top = 136
        Width = 144
        Height = 21
        DataField = 'GrupNiv4'
        DataSource = DsReceitasCab
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsReceiGruNiv4
        TabOrder = 31
        OnClick = CBReceiGruNiv4Click
      end
      object CBReceiGruNiv3: TDBLookupComboBox
        Left = 556
        Top = 136
        Width = 144
        Height = 21
        DataField = 'GrupNiv3'
        DataSource = DsReceitasCab
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsReceiGruNiv3
        TabOrder = 32
        OnClick = CBReceiGruNiv3Click
      end
      object CBReceiGruNiv2: TDBLookupComboBox
        Left = 708
        Top = 136
        Width = 144
        Height = 21
        DataField = 'GrupNiv2'
        DataSource = DsReceitasCab
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsReceiGruNiv2
        TabOrder = 33
        OnClick = CBReceiGruNiv2Click
      end
      object CBReceiGruNiv1: TDBLookupComboBox
        Left = 856
        Top = 136
        Width = 144
        Height = 21
        DataField = 'GrupNiv1'
        DataSource = DsReceitasCab
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsReceiGruNiv1
        TabOrder = 34
      end
      object DBEdit15: TDBEdit
        Left = 160
        Top = 56
        Width = 72
        Height = 21
        DataField = 'AreaM2Rebx'
        DataSource = DsReceitasCab
        TabOrder = 16
      end
      object DBEdit18: TDBEdit
        Left = 236
        Top = 56
        Width = 72
        Height = 21
        DataField = 'AreaM2Semi'
        DataSource = DsReceitasCab
        TabOrder = 17
      end
      object DBEdit19: TDBEdit
        Left = 312
        Top = 56
        Width = 86
        Height = 21
        DataField = 'BRL_USD'
        DataSource = DsReceitasCab
        TabOrder = 18
      end
      object DBMemo1: TDBMemo
        Left = 76
        Top = 160
        Width = 925
        Height = 35
        DataField = 'Observ'
        DataSource = DsReceitasCab
        TabOrder = 36
      end
      object Edit1: TdmkEdit
        Left = 404
        Top = 56
        Width = 37
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 117758
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 37
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
    end
  end
  object PainelGerencia: TPanel
    Left = 0
    Top = 452
    Width = 1008
    Height = 96
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object PainelControle: TPanel
      Left = 0
      Top = 48
      Width = 1008
      Height = 48
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 172
        Height = 46
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Hint = 'ltimo registro'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Hint = 'Pr'#243'ximo registro'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Hint = 'Registro anterior'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Hint = 'Primeiro registro'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object Panel3: TPanel
        Left = 173
        Top = 1
        Width = 559
        Height = 46
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 13
          Left = 464
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtSaidaClick
        end
        object BtExclui: TBitBtn
          Tag = 12
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Exclui banco atual'
          Caption = '&Exclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtExcluiClick
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Altera banco atual'
          Caption = '&Altera'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Inclui novo banco'
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object BtDuplica: TBitBtn
          Tag = 177
          Left = 372
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Duplica'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtDuplicaClick
        end
        object BtImportar: TBitBtn
          Tag = 19
          Left = 280
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = 'I&mporta'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
          OnClick = BtImportarClick
        end
      end
    end
    object PainelConfirma: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 48
      Align = alBottom
      TabOrder = 1
      Visible = False
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 8
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confirma inclus'#245'es / altera'#231#245'es'
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 900
        Top = 1
        Width = 107
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 13
          Left = 7
          Top = 3
          Width = 90
          Height = 40
          Hint = 'Desiste de inclus'#245'es / altera'#231#245'es'
          Caption = '&Desiste'
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
      object BtMove: TBitBtn
        Tag = 14
        Left = 100
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confirma inclus'#245'es / altera'#231#245'es'
        Caption = '&Move'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtMoveClick
      end
    end
  end
  object GradeIts: TdmkDBGridZTO
    Left = 0
    Top = 249
    Width = 1008
    Height = 203
    Align = alClient
    Color = clWhite
    DataSource = DsReceitasIts
    Options = [dgEditing, dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
    OptionsEx = [dgAllowTitleClick, dgAllowAppendAfterEof, dgAutomaticColumSizes]
    ReadOnly = True
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    RowColors = <>
    OnCellClick = GradeItsCellClick
    OnColEnter = GradeItsColEnter
    OnColExit = GradeItsColExit
    OnDrawColumnCell = GradeItsDrawColumnCell
    OnDragDrop = GradeItsDragDrop
    OnDragOver = GradeItsDragOver
    OnEndDrag = GradeItsEndDrag
    OnKeyDown = GradeItsKeyDown
    OnMouseDown = GradeItsMouseDown
    Columns = <
      item
        Expanded = False
        FieldName = 'Ordem'
        Title.Caption = 'Linha'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NO_RecProcCad'
        Title.Caption = 'Processo'
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NO_ProdutosTabls'
        Title.Caption = 'Tabela'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Porcent'
        Title.Alignment = taRightJustify
        Title.Caption = ' % de uso'
        Width = 54
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TXT_SHOW_PROD'
        Title.Caption = 'Produto ou fam'#237'lia ou texto ou ...'
        Width = 300
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Boca'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -21
        Font.Name = 'Wingdings 2'
        Font.Style = []
        Title.Caption = 'B?'
        Width = 18
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Diluicao'
        Title.Alignment = taRightJustify
        Title.Caption = 'Dil.1:x'
        Width = 38
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'Graus'
        Title.Alignment = taRightJustify
        Title.Caption = #186' C'
        Width = 20
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'TempoR'
        Title.Alignment = taRightJustify
        Title.Caption = 'Roda'
        Width = 34
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'TempoP'
        Title.Alignment = taRightJustify
        Title.Caption = 'P'#225'ra'
        Width = 35
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'pH_Min'
        Title.Alignment = taRightJustify
        Title.Caption = 'pH min'
        Width = 40
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'pH_Max'
        Title.Caption = 'pH m'#225'x'
        Width = 40
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Be_Min'
        Title.Alignment = taRightJustify
        Title.Caption = #186' B'#233' min'
        Width = 44
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Be_Max'
        Title.Caption = #186' B'#233' m'#225'x'
        Width = 44
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'GC_Min'
        Title.Caption = #186'C Min'
        Width = 36
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'GC_Max'
        Title.Caption = #186'C Max'
        Width = 36
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Obs'
        Title.Caption = 'Observa'#231#245'es'
        Width = 114
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ObsProces'
        Title.Caption = 'Observa'#231#227'o processso'
        Width = 214
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Minimo'
        Title.Caption = 'M'#237'n.'
        Width = 30
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Maximo'
        Title.Caption = 'M'#225'x.'
        Width = 30
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CodProduto'
        Title.Alignment = taRightJustify
        Title.Caption = 'PQ [F4]'
        Width = 50
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CU_CodProduto'
        Title.Caption = 'C'#243'digo'
        Width = 56
        Visible = True
      end>
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
        OnChange = ImgTipoChange
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 3
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 31
        Top = 9
        Width = 239
        Height = 32
        Caption = 'Receitas de Ribeira'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 33
        Top = 11
        Width = 239
        Height = 32
        Caption = 'Receitas de Ribeira'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 32
        Top = 10
        Width = 239
        Height = 32
        Caption = 'Receitas de Ribeira'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object SpeedButton8: TSpeedButton
        Left = 4
        Top = 16
        Width = 23
        Height = 22
        Caption = 'V'
        OnClick = SpeedButton8Click
      end
    end
  end
  object DsReceitasCab: TDataSource
    DataSet = TbReceitasCab
    Left = 48
    Top = 272
  end
  object DsReceitasIts: TDataSource
    DataSet = TbReceitasIts
    Left = 44
    Top = 368
  end
  object PMImporta: TPopupMenu
    Left = 512
    Top = 432
    object Receitadeanlogos1: TMenuItem
      Caption = '&Receita de an'#225'logos'
      OnClick = Receitadeanlogos1Click
    end
  end
  object TbReceitasIts: TZTable
    Connection = DMod.MyDB
    SortedFields = 'Ordem'
    OnCalcFields = TbReceitasItsCalcFields
    BeforeInsert = TbReceitasItsBeforeInsert
    AfterPost = TbReceitasItsAfterPost
    AfterDelete = TbReceitasItsAfterDelete
    OnNewRecord = TbReceitasItsNewRecord
    TableName = 'receitasits'
    IndexFieldNames = 'Ordem Asc'
    WhereMode = wmWhereAll
    Options = [doCalcDefaults, doDontSortOnPost]
    Left = 44
    Top = 320
    object TbReceitasItsCodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object TbReceitasItsOrdem: TLargeintField
      FieldName = 'Ordem'
      Required = True
    end
    object TbReceitasItsControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object TbReceitasItsCtrlUsu: TLargeintField
      FieldName = 'CtrlUsu'
      Required = True
    end
    object TbReceitasItsIDServr: TLargeintField
      FieldName = 'IDServr'
      Required = True
    end
    object TbReceitasItsReordem: TLargeintField
      FieldName = 'Reordem'
      Required = True
    end
    object TbReceitasItsCodProcesso: TLargeintField
      FieldName = 'CodProcesso'
      DisplayFormat = '0;-0; '
    end
    object TbReceitasItsNomProcesso: TWideStringField
      FieldName = 'NomProcesso'
      Size = 30
    end
    object TbReceitasItsPorcent: TFloatField
      FieldName = 'Porcent'
      DisplayFormat = '0.000;-0.000; '
    end
    object TbReceitasItsCodProduto: TLargeintField
      FieldName = 'CodProduto'
      OnChange = TbReceitasItsCodProdutoChange
      DisplayFormat = '0;-0; '
    end
    object TbReceitasItsNomProduto: TWideStringField
      FieldName = 'NomProduto'
      Size = 60
    end
    object TbReceitasItsTempoR: TLargeintField
      FieldName = 'TempoR'
      DisplayFormat = '0.0;-0.0; '
    end
    object TbReceitasItsTempoP: TLargeintField
      FieldName = 'TempoP'
      DisplayFormat = '0.0;-0.0; '
    end
    object TbReceitasItspH: TFloatField
      FieldName = 'pH'
      DisplayFormat = '0.00;-0.00; '
    end
    object TbReceitasItspH_Min: TFloatField
      FieldName = 'pH_Min'
      DisplayFormat = '0.00;-0.00; '
    end
    object TbReceitasItspH_Max: TFloatField
      FieldName = 'pH_Max'
      DisplayFormat = '0.00;-0.00; '
    end
    object TbReceitasItsBe: TFloatField
      FieldName = 'Be'
      DisplayFormat = '0.00;-0.00; '
    end
    object TbReceitasItsBe_Min: TFloatField
      FieldName = 'Be_Min'
      DisplayFormat = '0.00;-0.00; '
    end
    object TbReceitasItsBe_Max: TFloatField
      FieldName = 'Be_Max'
      DisplayFormat = '0.00;-0.00; '
    end
    object TbReceitasItsGraus: TFloatField
      FieldName = 'Graus'
      DisplayFormat = '0;-0; '
    end
    object TbReceitasItsGC_Min: TFloatField
      FieldName = 'GC_Min'
      DisplayFormat = '0;-0; '
    end
    object TbReceitasItsGC_Max: TFloatField
      FieldName = 'GC_Max'
      DisplayFormat = '0;-0; '
    end
    object TbReceitasItsBoca: TSmallintField
      FieldName = 'Boca'
      DisplayFormat = '0;-0; '
      MaxValue = 1
    end
    object TbReceitasItsObs: TWideStringField
      FieldName = 'Obs'
    end
    object TbReceitasItsObsProcesIni: TWideStringField
      FieldName = 'ObsProcesIni'
      Size = 120
    end
    object TbReceitasItsObsProcesFim: TWideStringField
      FieldName = 'ObsProcesFim'
      Size = 120
    end
    object TbReceitasItsDiluicao: TFloatField
      FieldName = 'Diluicao'
      DisplayFormat = '0;-0; '
    end
    object TbReceitasItsCusto: TFloatField
      FieldName = 'Custo'
      DisplayFormat = '0.0000;-0.0000; '
    end
    object TbReceitasItsMinimo: TFloatField
      FieldName = 'Minimo'
      DisplayFormat = '0.000;-0.000; '
    end
    object TbReceitasItsMaximo: TFloatField
      FieldName = 'Maximo'
      DisplayFormat = '0.000;-0.000; '
    end
    object TbReceitasItsOrigemPQ: TLargeintField
      FieldName = 'OrigemPQ'
      Required = True
    end
    object TbReceitasItsMoedaPQ: TWideStringField
      FieldName = 'MoedaPQ'
      Size = 3
    end
    object TbReceitasItsPrecoPQ: TFloatField
      FieldName = 'PrecoPQ'
      DisplayFormat = '0.0000;-0.0000; '
    end
    object TbReceitasItsPerICMS: TFloatField
      FieldName = 'PerICMS'
      DisplayFormat = '0.00;-0.00; '
    end
    object TbReceitasItsPerPIS: TFloatField
      FieldName = 'PerPIS'
      DisplayFormat = '0.00;-0.00; '
    end
    object TbReceitasItsPerCOFINS: TFloatField
      FieldName = 'PerCOFINS'
      DisplayFormat = '0.00;-0.00; '
    end
    object TbReceitasItsPerIPI: TFloatField
      FieldName = 'PerIPI'
      DisplayFormat = '0.00;-0.00; '
    end
    object TbReceitasItsMoedaFrete: TWideStringField
      FieldName = 'MoedaFrete'
      Size = 3
    end
    object TbReceitasItsPrecoFrete: TFloatField
      FieldName = 'PrecoFrete'
      DisplayFormat = '0.0000;-0.0000; '
    end
    object TbReceitasItsCusUnit: TFloatField
      FieldName = 'CusUnit'
      DisplayFormat = '0.0000;-0.0000; '
    end
    object TbReceitasItsCusItem: TFloatField
      FieldName = 'CusItem'
      DisplayFormat = '0.0000;-0.0000; '
    end
    object TbReceitasItsLk: TLargeintField
      FieldName = 'Lk'
    end
    object TbReceitasItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object TbReceitasItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object TbReceitasItsUserCad: TLargeintField
      FieldName = 'UserCad'
    end
    object TbReceitasItsUserAlt: TLargeintField
      FieldName = 'UserAlt'
    end
    object TbReceitasItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object TbReceitasItsAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object TbReceitasItsNO_RecProcCad: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NO_RecProcCad'
      LookupDataSet = QrRecProcCad
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'CodProcesso'
      Size = 60
      Lookup = True
    end
    object TbReceitasItsNO_ProdutosTabls: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NO_ProdutosTabls'
      LookupDataSet = QrProdutosTabls
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'ProdutosTabls'
      Lookup = True
    end
    object TbReceitasItsCodUsu: TLargeintField
      FieldName = 'CodUsu'
      Required = True
    end
    object TbReceitasItsProdutosTabls: TLargeintField
      FieldName = 'ProdutosTabls'
      OnChange = TbReceitasItsProdutosTablsChange
    end
    object TbReceitasItsNO_CodProduto: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NO_CodProduto'
      LookupDataSet = QrProdutosCadas
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'CodProduto'
      Size = 120
      Lookup = True
    end
    object TbReceitasItsCU_CodProduto: TIntegerField
      FieldKind = fkLookup
      FieldName = 'CU_CodProduto'
      LookupDataSet = QrProdutosCadas
      LookupKeyFields = 'Codigo'
      LookupResultField = 'CodUsu'
      KeyFields = 'CodProduto'
      Lookup = True
    end
    object TbReceitasItsTXT_SHOW_PROD: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_SHOW_PROD'
      Size = 255
      Calculated = True
    end
    object TbReceitasItsEntidadesCptt: TLargeintField
      FieldName = 'EntidadesCptt'
    end
    object TbReceitasItsProdutosFamil: TLargeintField
      FieldName = 'ProdutosFamil'
    end
    object TbReceitasItsNO_EntidadesCptt: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NO_EntidadesCptt'
      LookupDataSet = QrEntidadesCptt
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Sigla'
      KeyFields = 'EntidadesCptt'
      Size = 255
      Lookup = True
    end
    object TbReceitasItsNO_ProdutosFamil: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NO_ProdutosFamil'
      LookupDataSet = QrProdutosFamil
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'ProdutosFamil'
      Size = 120
      Lookup = True
    end
  end
  object TbReceitasCab: TZTable
    Connection = DMod.MyDB
    BeforeClose = TbReceitasCabBeforeClose
    AfterScroll = TbReceitasCabAfterScroll
    OnCalcFields = TbReceitasCabCalcFields
    AfterInsert = TbReceitasCabAfterInsert
    AfterEdit = TbReceitasCabAfterEdit
    BeforePost = TbReceitasCabBeforePost
    TableName = 'receitascab'
    Left = 48
    Top = 224
    object TbReceitasCabCodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object TbReceitasCabCodUsu: TLargeintField
      FieldName = 'CodUsu'
      Required = True
    end
    object TbReceitasCabIDServr: TLargeintField
      FieldName = 'IDServr'
      Required = True
    end
    object TbReceitasCabNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 120
    end
    object TbReceitasCabSigla: TWideStringField
      FieldName = 'Sigla'
      Required = True
      Size = 30
    end
    object TbReceitasCabVersao: TLargeintField
      FieldName = 'Versao'
      Required = True
      DisplayFormat = '0'
    end
    object TbReceitasCabVerSub: TLargeintField
      FieldName = 'VerSub'
      Required = True
      DisplayFormat = '000'
    end
    object TbReceitasCabCliente: TLargeintField
      FieldName = 'Cliente'
      Required = True
    end
    object TbReceitasCabArtigo: TLargeintField
      FieldName = 'Artigo'
      Required = True
    end
    object TbReceitasCabSubstrato: TLargeintField
      FieldName = 'Substrato'
      Required = True
    end
    object TbReceitasCabTipificacao: TLargeintField
      FieldName = 'Tipificacao'
      Required = True
    end
    object TbReceitasCabDataI: TDateField
      FieldName = 'DataI'
    end
    object TbReceitasCabDataA: TDateField
      FieldName = 'DataA'
    end
    object TbReceitasCabAccount: TLargeintField
      FieldName = 'Account'
      Required = True
    end
    object TbReceitasCabDigitador: TLargeintField
      FieldName = 'Digitador'
      Required = True
    end
    object TbReceitasCabTempoR: TLargeintField
      FieldName = 'TempoR'
    end
    object TbReceitasCabTempoP: TLargeintField
      FieldName = 'TempoP'
    end
    object TbReceitasCabTempoT: TLargeintField
      FieldName = 'TempoT'
    end
    object TbReceitasCabHorasR: TLargeintField
      FieldName = 'HorasR'
    end
    object TbReceitasCabHorasP: TLargeintField
      FieldName = 'HorasP'
    end
    object TbReceitasCabHorasT: TLargeintField
      FieldName = 'HorasT'
    end
    object TbReceitasCabSetor: TLargeintField
      FieldName = 'Setor'
    end
    object TbReceitasCabAreaM2Rebx: TFloatField
      FieldName = 'AreaM2Rebx'
      ReadOnly = True
      DisplayFormat = '#,###,##0.00'
    end
    object TbReceitasCabAreaP2Rebx: TFloatField
      FieldName = 'AreaP2Rebx'
      ReadOnly = True
      DisplayFormat = '#,###,##0.00'
    end
    object TbReceitasCabAreaM2Semi: TFloatField
      FieldName = 'AreaM2Semi'
      ReadOnly = True
      DisplayFormat = '#,###,##0.00'
    end
    object TbReceitasCabAreaP2Semi: TFloatField
      FieldName = 'AreaP2Semi'
      ReadOnly = True
      DisplayFormat = '#,###,##0.00'
    end
    object TbReceitasCabAreaM2Fini: TFloatField
      FieldName = 'AreaM2Fini'
      ReadOnly = True
      DisplayFormat = '#,###,##0.00'
    end
    object TbReceitasCabAreaP2Fini: TFloatField
      FieldName = 'AreaP2Fini'
      ReadOnly = True
      DisplayFormat = '#,###,##0.00'
    end
    object TbReceitasCabPeso: TFloatField
      FieldName = 'Peso'
      DisplayFormat = '#,###,###,###'
    end
    object TbReceitasCabQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###.###'
    end
    object TbReceitasCabkg_m2: TFloatField
      FieldName = 'kg_m2'
    end
    object TbReceitasCabBRL_USD: TFloatField
      FieldName = 'BRL_USD'
      DisplayFormat = '#,###,##0.000000'
    end
    object TbReceitasCabGrupNiv1: TLargeintField
      FieldName = 'GrupNiv1'
      Required = True
    end
    object TbReceitasCabGrupNiv2: TLargeintField
      FieldName = 'GrupNiv2'
      Required = True
      OnChange = TbReceitasCabGrupNiv2Change
    end
    object TbReceitasCabGrupNiv3: TLargeintField
      FieldName = 'GrupNiv3'
      Required = True
      OnChange = TbReceitasCabGrupNiv3Change
    end
    object TbReceitasCabGrupNiv4: TLargeintField
      FieldName = 'GrupNiv4'
      Required = True
      OnChange = TbReceitasCabGrupNiv4Change
    end
    object TbReceitasCabGrupNiv5: TLargeintField
      FieldName = 'GrupNiv5'
      Required = True
      OnChange = TbReceitasCabGrupNiv5Change
    end
    object TbReceitasCabHvSrDB: TSmallintField
      FieldName = 'HvSrDB'
      Required = True
    end
    object TbReceitasCabObserv: TWideMemoField
      FieldName = 'Observ'
      Required = True
      BlobType = ftWideMemo
    end
    object TbReceitasCabLk: TLargeintField
      FieldName = 'Lk'
    end
    object TbReceitasCabDataCad: TDateField
      FieldName = 'DataCad'
      DisplayFormat = 'dd/mm/yy'
    end
    object TbReceitasCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object TbReceitasCabUserCad: TLargeintField
      FieldName = 'UserCad'
    end
    object TbReceitasCabUserAlt: TLargeintField
      FieldName = 'UserAlt'
    end
    object TbReceitasCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object TbReceitasCabHHMM_P: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'HHMM_P'
      Size = 10
      Calculated = True
    end
    object TbReceitasCabHHMM_R: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'HHMM_R'
      Size = 10
      Calculated = True
    end
    object TbReceitasCabHHMM_T: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'HHMM_T'
      Size = 10
      Calculated = True
    end
    object TbReceitasCabMEDIAPESO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'MEDIAPESO'
      Calculated = True
    end
    object TbReceitasCabAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object TbReceitasCabEspesDiviCul: TSmallintField
      FieldName = 'EspesDiviCul'
    end
    object TbReceitasCabEspesDiviCab: TSmallintField
      FieldName = 'EspesDiviCab'
    end
    object TbReceitasCabEspesDiviEMCM: TFloatField
      FieldName = 'EspesDiviEMCM'
    end
    object TbReceitasCabEspesRebxCul: TSmallintField
      FieldName = 'EspesRebxCul'
    end
    object TbReceitasCabEspesRebxCab: TSmallintField
      FieldName = 'EspesRebxCab'
    end
    object TbReceitasCabEspesRebxEMCM: TFloatField
      FieldName = 'EspesRebxEMCM'
    end
    object TbReceitasCabEspesSemiCul: TSmallintField
      FieldName = 'EspesSemiCul'
    end
    object TbReceitasCabEspesSemiCab: TSmallintField
      FieldName = 'EspesSemiCab'
    end
    object TbReceitasCabEspesSemiEMCM: TFloatField
      FieldName = 'EspesSemiEMCM'
    end
    object TbReceitasCabEspesFiniCul: TSmallintField
      FieldName = 'EspesFiniCul'
    end
    object TbReceitasCabEspesFiniCab: TSmallintField
      FieldName = 'EspesFiniCab'
    end
    object TbReceitasCabEspesFiniEMCM: TFloatField
      FieldName = 'EspesFiniEMCM'
    end
  end
  object QrSetoresCad: TZQuery
    Connection = DMod.MyDB
    SQL.Strings = (
      'SELECT * FROM setorescad'
      'ORDER BY Nome'
      '')
    Params = <>
    Left = 140
    Top = 224
    object QrSetoresCadCodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrSetoresCadNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
  end
  object DsSetoresCad: TDataSource
    DataSet = QrSetoresCad
    Left = 140
    Top = 272
  end
  object QrEntidadesEmpr: TZQuery
    Connection = DMod.MyDB
    SQL.Strings = (
      'SELECT * FROM entidadesempr'
      'ORDER BY Nome'
      '')
    Params = <>
    Left = 140
    Top = 320
    object QrEntidadesEmprCodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrEntidadesEmprNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 255
    end
  end
  object DsEntidadesEmpr: TDataSource
    DataSet = QrEntidadesEmpr
    Left = 140
    Top = 368
  end
  object QrSubstratos: TZQuery
    Connection = DMod.MyDB
    SQL.Strings = (
      'SELECT * FROM artigoscad'
      'ORDER BY Nome'
      '')
    Params = <>
    Left = 220
    Top = 224
    object QrSubstratosCodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrSubstratosNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 120
    end
  end
  object DsSubstratos: TDataSource
    DataSet = QrSubstratos
    Left = 220
    Top = 272
  end
  object QrArtigos: TZQuery
    Connection = DMod.MyDB
    SQL.Strings = (
      'SELECT * FROM artigoscad'
      'ORDER BY Nome'
      '')
    Params = <>
    Left = 220
    Top = 320
    object QrArtigosCodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrArtigosNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 120
    end
  end
  object DsArtigos: TDataSource
    DataSet = QrArtigos
    Left = 220
    Top = 368
  end
  object QrEntidadesAccn: TZQuery
    Connection = DMod.MyDB
    SQL.Strings = (
      'SELECT * FROM entidadesaccn'
      'ORDER BY Nome'
      '')
    Params = <>
    Left = 304
    Top = 224
    object QrEntidadesAccnCodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrEntidadesAccnNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 255
    end
  end
  object DsEntidadesAccn: TDataSource
    DataSet = QrEntidadesAccn
    Left = 304
    Top = 272
  end
  object QrRecProcCad: TZQuery
    Connection = DMod.MyDB
    SQL.Strings = (
      'SELECT * FROM recproccad'
      'ORDER BY Nome'
      '')
    Params = <>
    Left = 304
    Top = 320
    object QrRecProcCadCodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrRecProcCadNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
  end
  object DsRecProcCad: TDataSource
    DataSet = QrRecProcCad
    Left = 304
    Top = 368
  end
  object QrReceiGruNiv5: TZQuery
    Connection = DMod.MyDB
    SQL.Strings = (
      'SELECT * FROM setorescad'
      'ORDER BY Nome'
      '')
    Params = <>
    Left = 592
    Top = 252
    object QrReceiGruNiv5Codigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrReceiGruNiv5CodUsu: TLargeintField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrReceiGruNiv5Nome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 120
    end
  end
  object DsReceiGruNiv5: TDataSource
    DataSet = QrReceiGruNiv5
    Left = 592
    Top = 300
  end
  object QrReceiGruNiv4: TZQuery
    Connection = DMod.MyDB
    SQL.Strings = (
      'SELECT * FROM setorescad'
      'ORDER BY Nome'
      '')
    Params = <>
    Left = 672
    Top = 252
    object QrReceiGruNiv4Codigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrReceiGruNiv4CodUsu: TLargeintField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrReceiGruNiv4Nome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 120
    end
  end
  object DsReceiGruNiv4: TDataSource
    DataSet = QrReceiGruNiv4
    Left = 672
    Top = 300
  end
  object QrReceiGruNiv3: TZQuery
    Connection = DMod.MyDB
    SQL.Strings = (
      'SELECT * FROM setorescad'
      'ORDER BY Nome'
      '')
    Params = <>
    Left = 756
    Top = 252
    object QrReceiGruNiv3Codigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrReceiGruNiv3CodUsu: TLargeintField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrReceiGruNiv3Nome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 120
    end
  end
  object DsReceiGruNiv3: TDataSource
    DataSet = QrReceiGruNiv3
    Left = 756
    Top = 300
  end
  object QrReceiGruNiv2: TZQuery
    Connection = DMod.MyDB
    SQL.Strings = (
      'SELECT * FROM setorescad'
      'ORDER BY Nome'
      '')
    Params = <>
    Left = 836
    Top = 252
    object QrReceiGruNiv2Codigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrReceiGruNiv2CodUsu: TLargeintField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrReceiGruNiv2Nome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 120
    end
  end
  object DsReceiGruNiv2: TDataSource
    DataSet = QrReceiGruNiv2
    Left = 836
    Top = 300
  end
  object QrReceiGruNiv1: TZQuery
    Connection = DMod.MyDB
    SQL.Strings = (
      'SELECT * FROM setorescad'
      'ORDER BY Nome'
      '')
    Params = <>
    Left = 920
    Top = 252
    object QrReceiGruNiv1Codigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrReceiGruNiv1CodUsu: TLargeintField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrReceiGruNiv1Nome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 120
    end
  end
  object DsReceiGruNiv1: TDataSource
    DataSet = QrReceiGruNiv1
    Left = 920
    Top = 300
  end
  object QrProdutosTabls: TZQuery
    Connection = DMod.MyDB
    SQL.Strings = (
      'SELECT * FROM produtostabls'
      'ORDER BY Nome'
      '')
    Params = <>
    Left = 588
    Top = 348
    object QrProdutosTablsCodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrProdutosTablsNome: TWideStringField
      FieldName = 'Nome'
      Required = True
    end
  end
  object DsProdutosTabls: TDataSource
    DataSet = QrProdutosTabls
    Left = 588
    Top = 396
  end
  object QrProdutosCadas: TZQuery
    Connection = DMod.MyDB
    SQL.Strings = (
      'SELECT Lista, Codigo, Nome, Segmento'
      'FROM produtoscadas'
      'WHERE Segmento IN ('
      'SELECT Codigo FROM segmentoscads'
      'WHERE NOME IN ("BH", "WE", "DY"))'
      'ORDER BY Nome')
    Params = <>
    Left = 400
    Top = 228
    object QrProdutosCadasLista: TLargeintField
      FieldName = 'Lista'
    end
    object QrProdutosCadasCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrProdutosCadasNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrProdutosCadasSegmento: TLargeintField
      FieldName = 'Segmento'
      Required = True
    end
    object QrProdutosCadasCodUsu: TLargeintField
      FieldName = 'CodUsu'
    end
  end
  object DsProdutosCadas: TDataSource
    DataSet = QrProdutosCadas
    Left = 400
    Top = 272
  end
  object QrSoma: TZQuery
    SQL.Strings = (
      'SELECT SUM(TempoR) TempoR, SUM(TempoP) TempoP'
      'FROM receitasits'
      'WHERE Codigo=:P0')
    Params = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    Left = 472
    Top = 284
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSomaTempoR: TFloatField
      FieldName = 'TempoR'
    end
    object QrSomaTempoP: TFloatField
      FieldName = 'TempoP'
    end
  end
  object QrReordem: TZQuery
    Connection = DMod.MyDB
    SQL.Strings = (
      'UPDATE receitasits SET'
      'Ordem=Reordem'
      'WHERE Codigo=:P0'
      '')
    Params = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    Left = 520
    Top = 224
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrEntidadesCptt: TZQuery
    Connection = DMod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Sigla'
      'FROM entidadescptt'
      'ORDER BY Nome')
    Params = <>
    Left = 400
    Top = 328
    object QrEntidadesCpttCodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrEntidadesCpttNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 255
    end
    object QrEntidadesCpttSigla: TWideStringField
      FieldName = 'Sigla'
      Required = True
      Size = 60
    end
  end
  object QrProdutosFamil: TZQuery
    Connection = DMod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM produtosfamil'
      'ORDER BY Nome')
    Params = <>
    Left = 400
    Top = 376
    object QrProdutosFamilCodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrProdutosFamilNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
  end
end
