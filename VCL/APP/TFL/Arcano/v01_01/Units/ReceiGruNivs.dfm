object FmReceiGruNivs: TFmReceiGruNivs
  Left = 234
  Top = 170
  Caption = 'ENT-CADAS-003 :: N'#237'veis de Grupos de Receitas'
  ClientHeight = 566
  ClientWidth = 1016
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel3: TPanel
    Left = 0
    Top = 449
    Width = 1016
    Height = 47
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object PnSaiDesis: TPanel
      Left = 872
      Top = 0
      Width = 144
      Height = 47
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object GBAvisos1: TGroupBox
      Left = 257
      Top = 0
      Width = 615
      Height = 47
      Align = alClient
      Caption = ' Avisos: '
      TabOrder = 1
      ExplicitLeft = 248
      ExplicitWidth = 624
      object Panel4: TPanel
        Left = 2
        Top = 15
        Width = 611
        Height = 30
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitWidth = 446
        object LaAviso1: TLabel
          Left = 13
          Top = 2
          Width = 120
          Height = 17
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object LaAviso2: TLabel
          Left = 12
          Top = 1
          Width = 120
          Height = 17
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
      end
    end
    object Panel11: TPanel
      Left = 0
      Top = 0
      Width = 257
      Height = 47
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 2
      object CkOcultaCadSistema: TCheckBox
        Left = 8
        Top = 4
        Width = 161
        Height = 17
        Caption = 'Ocultar cadastros do sistema.'
        Checked = True
        State = cbChecked
        TabOrder = 0
        OnClick = CkOcultaCadSistemaClick
      end
      object CkDragDrop: TCheckBox
        Left = 8
        Top = 24
        Width = 233
        Height = 17
        Caption = 'Ativar arraste entre n'#237'veis (mudar n'#237'vel pai).'
        TabOrder = 1
        OnClick = CkDragDropClick
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 52
    Width = 1016
    Height = 397
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 0
      Top = 156
      Width = 1016
      Height = 10
      Cursor = crVSplit
      Align = alTop
      Beveled = True
    end
    object Panel2: TPanel
      Left = 0
      Top = 0
      Width = 1016
      Height = 156
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Splitter2: TSplitter
        Left = 240
        Top = 0
        Width = 10
        Height = 156
        Beveled = True
      end
      object Splitter3: TSplitter
        Left = 500
        Top = 0
        Width = 10
        Height = 156
        Beveled = True
      end
      object Splitter4: TSplitter
        Left = 760
        Top = 0
        Width = 10
        Height = 156
        Beveled = True
      end
      object Panel6: TPanel
        Left = 0
        Top = 0
        Width = 240
        Height = 156
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object DBGNiv5: TDBGrid
          Left = 0
          Top = 17
          Width = 240
          Height = 139
          Align = alClient
          DataSource = DsReceiGruNiv5
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnColEnter = DBGNiv1ColEnter
          OnColExit = DBGNiv1ColExit
          OnDblClick = DBGNiv5DblClick
          OnDragDrop = DBGNiv5DragDrop
          OnDragOver = DBGNiv5DragOver
          OnEnter = DBGNiv5Enter
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Cod.'
              Width = 24
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Title.Caption = 'Descri'#231#227'o do PLANO'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ordem'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              Width = 56
              Visible = True
            end>
        end
        object StaticText2: TStaticText
          Left = 0
          Top = 0
          Width = 240
          Height = 17
          Align = alTop
          Alignment = taCenter
          BorderStyle = sbsSunken
          Caption = 'N'#237'vel 5'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
        end
      end
      object Panel7: TPanel
        Left = 250
        Top = 0
        Width = 250
        Height = 156
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 1
        object DBGNiv4: TDBGrid
          Left = 0
          Top = 17
          Width = 250
          Height = 139
          Align = alClient
          DataSource = DsReceiGruNiv4
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnCellClick = DBGNiv4CellClick
          OnColEnter = DBGNiv1ColEnter
          OnColExit = DBGNiv1ColExit
          OnDblClick = DBGNiv4DblClick
          OnDragDrop = DBGNiv4DragDrop
          OnDragOver = DBGNiv4DragOver
          OnEnter = DBGNiv4Enter
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Cod.'
              Width = 24
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Title.Caption = 'Descri'#231#227'o do CONJUNTO'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ordem'
              Width = 56
              Visible = True
            end>
        end
        object StaticText3: TStaticText
          Left = 0
          Top = 0
          Width = 250
          Height = 17
          Align = alTop
          Alignment = taCenter
          BorderStyle = sbsSunken
          Caption = 'N'#237'vel 4'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
        end
      end
      object Panel8: TPanel
        Left = 510
        Top = 0
        Width = 250
        Height = 156
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 2
        object DBGNiv3: TDBGrid
          Left = 0
          Top = 17
          Width = 250
          Height = 139
          Align = alClient
          DataSource = DsReceiGruNiv3
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnCellClick = DBGNiv3CellClick
          OnColEnter = DBGNiv1ColEnter
          OnColExit = DBGNiv1ColExit
          OnDblClick = DBGNiv3DblClick
          OnDragDrop = DBGNiv3DragDrop
          OnDragOver = DBGNiv3DragOver
          OnEnter = DBGNiv3Enter
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Cod.'
              Width = 24
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Title.Caption = 'Descri'#231#227'o do GRUPO'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ordem'
              Width = 56
              Visible = True
            end>
        end
        object StaticText4: TStaticText
          Left = 0
          Top = 0
          Width = 250
          Height = 17
          Align = alTop
          Alignment = taCenter
          BorderStyle = sbsSunken
          Caption = 'N'#237'vel 3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
        end
      end
      object Panel9: TPanel
        Left = 770
        Top = 0
        Width = 246
        Height = 156
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 3
        object DBGNiv2: TDBGrid
          Left = 0
          Top = 17
          Width = 246
          Height = 139
          Align = alClient
          DataSource = DsReceiGruNiv2
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnCellClick = DBGNiv2CellClick
          OnColEnter = DBGNiv1ColEnter
          OnColExit = DBGNiv1ColExit
          OnDblClick = DBGNiv2DblClick
          OnDragDrop = DBGNiv2DragDrop
          OnDragOver = DBGNiv2DragOver
          OnEnter = DBGNiv2Enter
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Cod.'
              Width = 24
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Title.Caption = 'Descri'#231#227'o do SUBGRUPO'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ordem'
              Width = 56
              Visible = True
            end>
        end
        object StaticText5: TStaticText
          Left = 0
          Top = 0
          Width = 246
          Height = 17
          Align = alTop
          Alignment = taCenter
          BorderStyle = sbsSunken
          Caption = 'N'#237'vel2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
        end
      end
    end
    object PnExtra: TPanel
      Left = 748
      Top = 166
      Width = 268
      Height = 231
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object DBGExtra: TDBGrid
        Left = 0
        Top = 17
        Width = 268
        Height = 214
        Align = alClient
        DataSource = DsExtra
        Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnCellClick = DBGExtraCellClick
        OnColEnter = DBGNiv1ColEnter
        OnColExit = DBGNiv1ColExit
        Columns = <
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'Cod.'
            Width = 24
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Title.Caption = 'Descri'#231#227'o CONJUNTO'
            Width = 209
            Visible = True
          end>
      end
      object StaticText1: TStaticText
        Left = 0
        Top = 0
        Width = 268
        Height = 17
        Align = alTop
        Alignment = taCenter
        Caption = 'Itens n'#227'o pertencentes '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
      end
    end
    object Panel10: TPanel
      Left = 0
      Top = 166
      Width = 748
      Height = 231
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 2
      object DBGNiv1: TDBGrid
        Left = 0
        Top = 17
        Width = 748
        Height = 214
        Align = alClient
        DataSource = DsReceiGruNiv1
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnCellClick = DBGNiv1CellClick
        OnColEnter = DBGNiv1ColEnter
        OnColExit = DBGNiv1ColExit
        OnDrawColumnCell = DBGNiv1DrawColumnCell
        OnDblClick = DBGNiv1DblClick
        OnEnter = DBGNiv1Enter
        Columns = <
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'Cod.'
            Width = 24
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Title.Caption = 'Descri'#231#227'o da CONTA'
            Width = 280
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Ordem'
            Width = 56
            Visible = True
          end>
      end
      object StaticText6: TStaticText
        Left = 0
        Top = 0
        Width = 748
        Height = 17
        Align = alTop
        Alignment = taCenter
        BorderStyle = sbsSunken
        Caption = 'N'#237'vel1'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1016
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 968
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 290
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbPlano: TBitBtn
        Tag = 350
        Left = 44
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbPlanoClick
      end
      object SbConjuntos: TBitBtn
        Tag = 351
        Left = 84
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbConjuntosClick
      end
      object SbGrupos: TBitBtn
        Tag = 352
        Left = 124
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbGruposClick
      end
      object SbSubgrupos: TBitBtn
        Tag = 353
        Left = 165
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbSubgruposClick
      end
      object SbContas: TBitBtn
        Tag = 354
        Left = 205
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 5
        OnClick = SbContasClick
      end
      object BtImpExp: TBitBtn
        Tag = 270
        Left = 246
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 6
        OnClick = BtImpExpClick
      end
    end
    object GB_M: TGroupBox
      Left = 290
      Top = 0
      Width = 678
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 361
        Height = 32
        Caption = 'N'#237'veis de Grupos de Receitas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 361
        Height = 32
        Caption = 'N'#237'veis de Grupos de Receitas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 361
        Height = 32
        Caption = 'N'#237'veis de Grupos de Receitas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 496
    Width = 1016
    Height = 70
    Align = alBottom
    TabOrder = 3
    Visible = False
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 1012
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
      end
      object BtCancela: TBitBtn
        Tag = 15
        Left = 198
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Cancela'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
      end
    end
  end
  object DsReceiGruNiv5: TDataSource
    DataSet = TbReceiGruNiv5
    Left = 33
    Top = 133
  end
  object DsReceiGruNiv4: TDataSource
    DataSet = TbReceiGruNiv4
    Left = 285
    Top = 141
  end
  object DsReceiGruNiv3: TDataSource
    DataSet = TbReceiGruNiv3
    Left = 513
    Top = 145
  end
  object DsReceiGruNiv2: TDataSource
    DataSet = TbReceiGruNiv2
    Left = 789
    Top = 149
  end
  object DsReceiGruNiv1: TDataSource
    DataSet = TbReceiGruNiv1
    Left = 60
    Top = 304
  end
  object TbReceiGruNiv5: TZTable
    Connection = DMod.MyDB
    AfterOpen = TbReceiGruNiv5AfterOpen
    AfterRefresh = TbReceiGruNiv5AfterRefresh
    AfterScroll = TbReceiGruNiv5AfterScroll
    Filter = 'Codigo>0'
    Filtered = True
    BeforeInsert = TbReceiGruNiv5BeforeInsert
    BeforePost = TbReceiGruNiv5BeforePost
    AfterPost = TbReceiGruNiv5AfterPost
    AfterCancel = TbReceiGruNiv5AfterCancel
    BeforeDelete = TbReceiGruNiv5BeforeDelete
    TableName = 'receigruniv5'
    Left = 33
    Top = 89
    object TbReceiGruNiv5Codigo: TLargeintField
      FieldName = 'Codigo'
    end
    object TbReceiGruNiv5CodUsu: TLargeintField
      FieldName = 'CodUsu'
      Required = True
    end
    object TbReceiGruNiv5IDServr: TLargeintField
      FieldName = 'IDServr'
      Required = True
    end
    object TbReceiGruNiv5PaiCod: TLargeintField
      FieldName = 'PaiCod'
      Required = True
    end
    object TbReceiGruNiv5PaiUsu: TLargeintField
      FieldName = 'PaiUsu'
      Required = True
    end
    object TbReceiGruNiv5Nome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 120
    end
    object TbReceiGruNiv5Lk: TLargeintField
      FieldName = 'Lk'
    end
    object TbReceiGruNiv5DataCad: TDateField
      FieldName = 'DataCad'
    end
    object TbReceiGruNiv5DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object TbReceiGruNiv5UserCad: TLargeintField
      FieldName = 'UserCad'
    end
    object TbReceiGruNiv5UserAlt: TLargeintField
      FieldName = 'UserAlt'
    end
    object TbReceiGruNiv5AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object TbReceiGruNiv5Ordem: TLargeintField
      FieldName = 'Ordem'
    end
    object TbReceiGruNiv5Ativo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object TbReceiGruNiv4: TZTable
    Connection = DMod.MyDB
    AfterOpen = TbReceiGruNiv4AfterOpen
    AfterRefresh = TbReceiGruNiv4AfterRefresh
    AfterScroll = TbReceiGruNiv4AfterScroll
    Filter = 'Plano=-1000'
    Filtered = True
    BeforeInsert = TbReceiGruNiv4BeforeInsert
    AfterInsert = TbReceiGruNiv4AfterInsert
    AfterEdit = TbReceiGruNiv4AfterEdit
    BeforePost = TbReceiGruNiv4BeforePost
    AfterPost = TbReceiGruNiv4AfterPost
    AfterCancel = TbReceiGruNiv4AfterCancel
    BeforeDelete = TbReceiGruNiv4BeforeDelete
    TableName = 'receigruniv4'
    Left = 285
    Top = 93
    object TbReceiGruNiv4Codigo: TLargeintField
      FieldName = 'Codigo'
    end
    object TbReceiGruNiv4CodUsu: TLargeintField
      FieldName = 'CodUsu'
      Required = True
    end
    object TbReceiGruNiv4IDServr: TLargeintField
      FieldName = 'IDServr'
      Required = True
    end
    object TbReceiGruNiv4PaiCod: TLargeintField
      FieldName = 'PaiCod'
      Required = True
    end
    object TbReceiGruNiv4PaiUsu: TLargeintField
      FieldName = 'PaiUsu'
      Required = True
    end
    object TbReceiGruNiv4Nome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 120
    end
    object TbReceiGruNiv4Lk: TLargeintField
      FieldName = 'Lk'
    end
    object TbReceiGruNiv4DataCad: TDateField
      FieldName = 'DataCad'
    end
    object TbReceiGruNiv4DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object TbReceiGruNiv4UserCad: TLargeintField
      FieldName = 'UserCad'
    end
    object TbReceiGruNiv4UserAlt: TLargeintField
      FieldName = 'UserAlt'
    end
    object TbReceiGruNiv4AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object TbReceiGruNiv4Ordem: TLargeintField
      FieldName = 'Ordem'
    end
    object TbReceiGruNiv4Ativo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object TbReceiGruNiv3: TZTable
    Connection = DMod.MyDB
    AfterOpen = TbReceiGruNiv3AfterOpen
    AfterRefresh = TbReceiGruNiv3AfterRefresh
    AfterScroll = TbReceiGruNiv3AfterScroll
    Filter = 'Conjunto=-1000'
    Filtered = True
    BeforeInsert = TbReceiGruNiv3BeforeInsert
    BeforePost = TbReceiGruNiv3BeforePost
    AfterPost = TbReceiGruNiv3AfterPost
    AfterCancel = TbReceiGruNiv3AfterCancel
    BeforeDelete = TbReceiGruNiv3BeforeDelete
    TableName = 'receigruniv3'
    Left = 513
    Top = 89
    object TbReceiGruNiv3Codigo: TLargeintField
      FieldName = 'Codigo'
    end
    object TbReceiGruNiv3CodUsu: TLargeintField
      FieldName = 'CodUsu'
      Required = True
    end
    object TbReceiGruNiv3IDServr: TLargeintField
      FieldName = 'IDServr'
      Required = True
    end
    object TbReceiGruNiv3PaiCod: TLargeintField
      FieldName = 'PaiCod'
      Required = True
    end
    object TbReceiGruNiv3PaiUsu: TLargeintField
      FieldName = 'PaiUsu'
      Required = True
    end
    object TbReceiGruNiv3Nome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 120
    end
    object TbReceiGruNiv3Lk: TLargeintField
      FieldName = 'Lk'
    end
    object TbReceiGruNiv3DataCad: TDateField
      FieldName = 'DataCad'
    end
    object TbReceiGruNiv3DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object TbReceiGruNiv3UserCad: TLargeintField
      FieldName = 'UserCad'
    end
    object TbReceiGruNiv3UserAlt: TLargeintField
      FieldName = 'UserAlt'
    end
    object TbReceiGruNiv3AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object TbReceiGruNiv3Ordem: TLargeintField
      FieldName = 'Ordem'
    end
    object TbReceiGruNiv3Ativo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object TbReceiGruNiv2: TZTable
    Connection = DMod.MyDB
    AfterOpen = TbReceiGruNiv2AfterOpen
    AfterRefresh = TbReceiGruNiv2AfterRefresh
    AfterScroll = TbReceiGruNiv2AfterScroll
    Filter = 'Grupo=-1000'
    Filtered = True
    BeforeInsert = TbReceiGruNiv2BeforeInsert
    BeforePost = TbReceiGruNiv2BeforePost
    AfterPost = TbReceiGruNiv2AfterPost
    AfterCancel = TbReceiGruNiv2AfterCancel
    BeforeDelete = TbReceiGruNiv2BeforeDelete
    TableName = 'receigruniv2'
    Left = 793
    Top = 93
    object TbReceiGruNiv2Codigo: TLargeintField
      FieldName = 'Codigo'
    end
    object TbReceiGruNiv2CodUsu: TLargeintField
      FieldName = 'CodUsu'
      Required = True
    end
    object TbReceiGruNiv2IDServr: TLargeintField
      FieldName = 'IDServr'
      Required = True
    end
    object TbReceiGruNiv2PaiCod: TLargeintField
      FieldName = 'PaiCod'
      Required = True
    end
    object TbReceiGruNiv2PaiUsu: TLargeintField
      FieldName = 'PaiUsu'
      Required = True
    end
    object TbReceiGruNiv2Nome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 120
    end
    object TbReceiGruNiv2Lk: TLargeintField
      FieldName = 'Lk'
    end
    object TbReceiGruNiv2DataCad: TDateField
      FieldName = 'DataCad'
    end
    object TbReceiGruNiv2DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object TbReceiGruNiv2UserCad: TLargeintField
      FieldName = 'UserCad'
    end
    object TbReceiGruNiv2UserAlt: TLargeintField
      FieldName = 'UserAlt'
    end
    object TbReceiGruNiv2AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object TbReceiGruNiv2Ordem: TLargeintField
      FieldName = 'Ordem'
    end
    object TbReceiGruNiv2Ativo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object TbReceiGruNiv1: TZTable
    Connection = DMod.MyDB
    OnCalcFields = TbReceiGruNiv1CalcFields
    Filter = 'SubGrupo=-1000'
    Filtered = True
    BeforeInsert = TbReceiGruNiv1BeforeInsert
    BeforePost = TbReceiGruNiv1BeforePost
    BeforeDelete = TbReceiGruNiv1BeforeDelete
    TableName = 'receigruniv1'
    Left = 60
    Top = 256
    object TbReceiGruNiv1Codigo: TLargeintField
      FieldName = 'Codigo'
    end
    object TbReceiGruNiv1CodUsu: TLargeintField
      FieldName = 'CodUsu'
      Required = True
    end
    object TbReceiGruNiv1IDServr: TLargeintField
      FieldName = 'IDServr'
      Required = True
    end
    object TbReceiGruNiv1PaiCod: TLargeintField
      FieldName = 'PaiCod'
      Required = True
    end
    object TbReceiGruNiv1PaiUsu: TLargeintField
      FieldName = 'PaiUsu'
      Required = True
    end
    object TbReceiGruNiv1Nome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 120
    end
    object TbReceiGruNiv1Lk: TLargeintField
      FieldName = 'Lk'
    end
    object TbReceiGruNiv1DataCad: TDateField
      FieldName = 'DataCad'
    end
    object TbReceiGruNiv1DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object TbReceiGruNiv1UserCad: TLargeintField
      FieldName = 'UserCad'
    end
    object TbReceiGruNiv1UserAlt: TLargeintField
      FieldName = 'UserAlt'
    end
    object TbReceiGruNiv1AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object TbReceiGruNiv1Ordem: TLargeintField
      FieldName = 'Ordem'
    end
    object TbReceiGruNiv1Ativo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object QrLoc: TZQuery
    Params = <>
    Left = 204
    Top = 252
  end
  object QrNNiv4: TZQuery
    SQL.Strings = (
      'SELECT tb1.Codigo, tb1.Nome, tb2.Nome NOME_PAI'
      'FROM receigruniv4 tb1'
      'LEFT JOIN receigruniv5 tb2 ON tb2.Codigo=tb1.PaiCod'
      'WHERE tb1.Codigo>0'
      'AND tb1.PaiCod NOT IN (0, :P0)'
      'ORDER BY tb2.Nome')
    Params = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    Left = 808
    Top = 272
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrNNiv4Codigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrNNiv4Nome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrNNiv4NOME_PAI: TWideStringField
      FieldName = 'NOME_PAI'
      Size = 50
    end
  end
  object DsExtra: TDataSource
    Left = 856
    Top = 272
  end
  object QrNNiv3: TZQuery
    SQL.Strings = (
      'SELECT tb1.Codigo, tb1.Nome, tb2.Nome NOME_PAI'
      'FROM receigruniv3 tb1'
      'LEFT JOIN receigruniv4 tb2 ON tb2.Codigo=tb1.PaiCod'
      'WHERE tb1.Codigo>0'
      'AND tb1.PaiCod NOT IN (0, :P0)'
      'ORDER BY tb2.Nome')
    Params = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    Left = 808
    Top = 316
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrNNiv3Codigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrNNiv3Nome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrNNiv3NOME_PAI: TWideStringField
      FieldName = 'NOME_PAI'
      Size = 50
    end
  end
  object QrNNiv2: TZQuery
    SQL.Strings = (
      'SELECT tb1.Codigo, tb1.Nome, tb2.Nome NOME_PAI'
      'FROM receigruniv2 tb1'
      'LEFT JOIN receigruniv3 tb2 ON tb2.Codigo=tb1.PaiCod'
      'WHERE tb1.Codigo>0'
      'AND tb1.PaiCod NOT IN (0, :P0)'
      'ORDER BY tb2.Nome')
    Params = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    Left = 808
    Top = 364
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrNNiv2Codigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrNNiv2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrNNiv2NOME_PAI: TWideStringField
      FieldName = 'NOME_PAI'
      Size = 50
    end
  end
  object QrNNiv1: TZQuery
    SQL.Strings = (
      'SELECT tb1.Codigo, tb1.Nome, tb2.Nome NOME_PAI'
      'FROM receigruniv1 tb1'
      'LEFT JOIN receigruniv2 tb2 ON tb2.Codigo=tb1.PaiCod'
      'WHERE tb1.Codigo>0'
      'AND tb1.PaiCod NOT IN (0, :P0)'
      'ORDER BY tb2.Nome')
    Params = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    Left = 808
    Top = 408
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrNNiv1Codigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrNNiv1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrNNiv1NOME_PAI: TWideStringField
      FieldName = 'NOME_PAI'
      Size = 50
    end
  end
  object mySQLUpdateSQL1: TmySQLUpdateSQL
    Left = 412
    Top = 260
  end
  object QrTNiv5: TZQuery
    Connection = DMod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM receigruniv5'
      'ORDER BY Nome'
      '')
    Params = <>
    Left = 112
    Top = 104
    object QrTNiv5Codigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrTNiv5Nome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 120
    end
  end
  object QrTNiv4: TZQuery
    Connection = DMod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM receigruniv4'
      'ORDER BY Nome'
      '')
    Params = <>
    Left = 108
    Top = 152
    object QrTNiv4Codigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrTNiv4Nome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 120
    end
  end
  object QrTNiv3: TZQuery
    Connection = DMod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM receigruniv3'
      'ORDER BY Nome'
      '')
    Params = <>
    Left = 164
    Top = 104
    object QrTNiv3Codigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrTNiv3Nome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 120
    end
  end
  object QrTNiv2: TZQuery
    Connection = DMod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM receigruniv2'
      'ORDER BY Nome'
      '')
    Params = <>
    Left = 164
    Top = 152
    object QrTNiv2Codigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrTNiv2Nome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 120
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 412
    Top = 92
  end
end
