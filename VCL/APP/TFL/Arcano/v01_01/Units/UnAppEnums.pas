unit UnAppEnums;

interface

uses  System.SysUtils, System.Types;

//const
  //CO_??? = 0;

type
  TDevicesAssembler = (assembUnknown=0, assembFronius=1);
  TKindLeituraKWH = (klkwhUnknown=0, klkwhEECCMinimo=1, klkwhEECCDifer=2,
                     klkwhCXCContrato=3, klkwhCXCDifer=4, klkwhIluminPublic=5,
                     klkwhCGeraCredit=6);
type
  TUnAppEnums = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  NomeDevicesAssembler(DeviceAssembler: TDevicesAssembler): String;
    function  NomeKindLeituraKWH(Kind: TKindLeituraKWH): String;

  end;

var
  AppEnums: TUnAppEnums;


implementation

{ TUnAppEnums }

function TUnAppEnums.NomeDevicesAssembler(DeviceAssembler: TDevicesAssembler): String;
begin
  case DeviceAssembler of
    (*0*)assembUnknown: Result := 'Indefinido';
    (*1*)assembFronius: Result := 'Fronius';
    else Result := '"TDeviceAssembler n�o implementado"';
  end;
end;

function TUnAppEnums.NomeKindLeituraKWH(Kind: TKindLeituraKWH): String;
begin
  case Kind of
    klkwhUnknown:       Result := 'Indefinido';
    klkwhEECCMinimo:    Result := 'Energia el�trica consumo (contrato m�nimo)';
    klkwhEECCDifer:     Result := 'Energia el�trico consumo (diferenciado)';
    klkwhCXCContrato:   Result := 'Cobran�a extra consumo (contrato m�nimo)';
    klkwhCXCDifer:      Result := 'Cobran�a extra consumo (diferenciado)';
    klkwhIluminPublic:  Result := 'Ilumina��o p�blica';
    klkwhCGeraCredit:   Result := 'Consumo gera��o (cr�dito)';
    else Result := 'UnAppEnums.NomeKindLeituraKWH.N�oImplementado';
  end;
end;

end.
