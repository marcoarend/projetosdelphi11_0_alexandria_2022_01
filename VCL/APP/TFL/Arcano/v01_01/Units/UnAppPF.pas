unit UnAppPF;

interface

uses
  Windows, SysUtils, Classes, Data.DB,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Menus,
  Vcl.Grids, Vcl.DBGrids, Vcl.ComCtrls, Vcl.StdCtrls, Vcl.Buttons, Vcl.Mask,
  Vcl.DBCtrls, Variants,
  ZAbstractConnection, ZConnection, ZCompatibility, ZAbstractRODataset,
  ZAbstractDataset, ZDataset,
  UnInternalConsts, dmkGeral, UnProjGroup_Vars, UnAppEnums, UnGrl_Vars,
  UnDmkEnums, UnGrl_DmkDB, UnVCL_ZDB, UnMyObjects, UnitMD5;

type
  TUnAppPF = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  GerenciaProduto(const TxtPrd, TxtEmb: String; var ProdCod:
              Integer; var ProdNom, Embalagem, Diluicao, Temperatura: String):
              Boolean;
    procedure ImportaDadosSalNew(REInfo: TRichEdit);
    function  MD5UserID(UserSigla: String; UserID: Integer): String;
    function  LimpaNomeProduto(Produto: String): String;
    function  LocalizaProdutoSohNome(Produto: String): Boolean;
    function  TraduzProduto(const Texto: String; var Produto, Diluicao,
              Temperatura: String): Boolean;
  end;

var
  AppPF: TUnAppPF;


implementation

uses Module, ProdutosCadas;


{ TUnAppPF }

function TUnAppPF.GerenciaProduto(const TxtPrd, TxtEmb: String;
  var ProdCod: Integer; var ProdNom, Embalagem, Diluicao,
  Temperatura: String): Boolean;
begin
  TraduzProduto(TxtPrd, ProdNom, Diluicao, Temperatura);
  if not LocalizaProdutoSohNome(ProdNom) then
  begin
    Application.CreateForm(TFmProdutosCadas, FmProdutosCadas);
    FmProdutosCadas.FSeq                   := 1;
    FmProdutosCadas.CadastraProduto();
    //
    FmProdutosCadas.EdCodigo.ValueVariant        := 0;
    FmProdutosCadas.EdCodUsu.ValueVariant        := 0;
    FmProdutosCadas.EdNome.ValueVariant          := ProdNom;
    FmProdutosCadas.EdProdCabec.ValueVariant     := 0;
    FmProdutosCadas.CBProdCabec.KeyValue         := Null;
    FmProdutosCadas.EdHvSrDB.ValueVariant        := Integer(THaveSrcDB.hsdbCadastr);
    FmProdutosCadas.EdGrupoSegmento.ValueVariant := 0;
    FmProdutosCadas.CBGrupoSegmento.KeyValue     := Null;
    FmProdutosCadas.EdSegmento.ValueVariant      := 0;
    FmProdutosCadas.CBSegmento.KeyValue          := Null;
    FmProdutosCadas.EdFamilia.ValueVariant       := 0;
    FmProdutosCadas.CBFamilia.KeyValue           := Null;
    FmProdutosCadas.EdDivisao.ValueVariant       := 0;
    FmProdutosCadas.CBDivisao.KeyValue           := Null;
    FmProdutosCadas.EdSubDivisao.ValueVariant    := 0;
    FmProdutosCadas.CBSubDivisao.KeyValue        := Null;
    //
    FmProdutosCadas.ShowModal;
    //
    Result    := FmProdutosCadas.FConfirmado;
    ProdNom   := FmProdutosCadas.FProdNom;
    ProdCod   := FmProdutosCadas.FProdCod;
    Embalagem := FmProdutosCadas.FEmbalagem;
    Diluicao  := IntToStr(FmProdutosCadas.FDiluicao);
    //
    FmProdutosCadas.Destroy;
  end;
end;

procedure TUnAppPF.ImportaDadosSalNew(REInfo: TRichEdit);
const
  NaoCria = False;
var
  SalNewDB: TZConnection;
  Usuario, Senha, CondicaoSQL, DBNome: String;
  BaseOrig, BaseDest: TZConnection;
  //
  function ImportaTabela(TabOrig, TabDest: String): Boolean;
  begin
    Grl_DmkDB.ExportaRegistrosEntreDBs_NovoZ(LowerCase(TabOrig),
      LowerCase(TabDest), CondicaoSQL, BaseOrig, BaseDest, REInfo);
  end;
begin
  SalNewDB := TZConnection.Create(Dmod);
  Screen.Cursor := crHourGlass;
  try
    if VCL_ZDB.ConfiguraDB_SQLite(SalNewDB, Usuario, Senha, 'SalNew', NaoCria,
    DBNome) then
    begin
      Usuario     := '';
      Senha       := '';
      CondicaoSQL := '';
      BaseOrig    := SalNewDB;
      BaseDest    := Dmod.MyDB;
      //
      ImportaTabela('EntidadesEmpr', 'EntidadesEmpr');
      ImportaTabela('EntidadesAccn', 'EntidadesAccn');
      ImportaTabela('ProdDivsaoCab', 'ProdDivsaoCab');
      ImportaTabela('ProdDivSubCab', 'ProdDivSubCab');
      ImportaTabela('ProdLinhasCab', 'ProdLinhasCab');
      ImportaTabela('ProdutosFamil', 'ProdutosFamil');
      ImportaTabela('ProdutosCabec', 'ProdutosCabec');
      ImportaTabela('ProdutosCadas', 'ProdutosCadas');
      ImportaTabela('segmentoscads', 'segmentoscads');
      //
    end else
      MyObjects.AdicionaTextoCorRichEdit(REInfo, clRed, [fsBold],
        'N�o foi poss�vel abrir a base de dados: ' + DBNome);
  finally
    SalNewDB.Free;
    Screen.Cursor := crDefault;
  end;
end;

function TUnAppPF.LimpaNomeProduto(Produto: String): String;
var
  x: String;
begin
  x := Trim(Produto);
  x := Geral.Substitui(x, '  ', ' ');
  //
  Result := x;
end;

function TUnAppPF.LocalizaProdutoSohNome(Produto: String): Boolean;
var
  Nome: String;
begin
  Nome := LimpaNomeProduto(Produto);
  Grl_DmkDB.AbreSQLQuery0(Dmod.QrPsq2, Dmod.MyDB, [
  'SELECT * FROM produtoscadas ',
  'WHERE UPPER(Nome)=UPPER("' + Nome + '")',
  '']);
  Result := Dmod.QrPsq2.RecordCount > 0;
end;

function TUnAppPF.MD5UserID(UserSigla: String; UserID: Integer): String;
begin
  Result := UnMD5.StrMD5(UserSigla + '#$%' + Geral.FF0(UserID));
end;

function TUnAppPF.TraduzProduto(const Texto: String; var Produto, Diluicao,
  Temperatura: String): Boolean;
var
  I, J, K, Dil: Integer;
  Txt, Tmp, GC: String;
  Achou: Boolean;
  Sai: Boolean;
  //
  function Tem(Palavra: String): Boolean;
  begin
    Result := pos(Palavra, Tmp) > 0;
  end;

begin
  Txt := Texto;
  Txt := Geral.Substitui(Txt, '�', ' �');
  Txt := Geral.Substitui(Txt, '�', ' �');
  while pos('  ', Txt) > 0 do
    Txt := Geral.Substitui(Txt, '  ', ' ');
{
  while pos('oC', Txt) > 0 do
    Txt := Geral.Substitui(Txt, 'oC', '�C');
}
  //

  //
  Dil := 0;
  GC := '';
  Tmp := Geral.Maiusculas(Geral.SemAcento(Txt), False);
  //
  if Tem('ACIDO') then
  begin
    if Tem('FORM') then
    begin
      Tmp := '�cido F�rmico';
      Dil := 5;
    end else
    if Tem('ACET') then
    begin
      Tmp := '�cido Ac�tico';
      Dil := 5;
    end else
    if Tem('OXAL') then
    begin
      Tmp := '�cido Ox�lico';
      Dil := 5;
    end else
    if Tem('SULF') then
    begin
      Tmp := '�cido Sulf�rico';
      Dil := 10;
    end else
    if Tem('MURI') then
    begin
      Tmp := '�cido Clor�drico';
      Dil := 10;
    end else
    if Tem('CLOR') then
    begin
      Tmp := '�cido Clor�drico';
      Dil := 10;
    end else
      Tmp := '';
  end else
  if Tem('SODIO') then
  begin
    if Tem('FORM') then
      Tmp := 'Formiato de S�dio'
    else
    if Tem('ACET') then
      Tmp := 'Acetato de S�dio'
    else
    if Tem('OXAL') then
      Tmp := 'Oxalato de S�dio'
    else
    if Tem('SULF') then
      Tmp := 'Sulfato de S�dio'
    else
    if Tem('MURI') then
      Tmp := 'Cloreto de S�dio'
    else
    if Tem('CLOR') then
      Tmp := 'Cloreto de S�dio'
    else
    if Tem('BICARB') then
      Tmp := 'Bicarbonato de S�dio'
    else
    if Tem('CARBON') then
      Tmp := 'Carbonato de S�dio'
    else
      Tmp := '';
  end else
  if Tem('AMONI') then
  begin
    if Tem('SULF') then
      Tmp := 'Sulfato de Am�nio'
    else
    if Tem('CLOR') then
      Tmp := 'Cloreto de Am�nio'
    else
    if Tem('BICARB') then
      Tmp := 'Bicarbonato de Am�nio'
    else
    if Tem('ACO') then
      Tmp := 'Amon�aco'
    else
      Tmp := '';
  end else
  if Tem('AGUA') then
  begin
    Tmp := '�gua';
  end else
  if Tem('ACACIA') then
  begin
    Tmp := 'Tanino de Ac�cia';
    Dil := 10;
  end else
    Tmp := '';
  //

(*
A SECO
Ac�cia
Acetato de s�dio
Acid Black 210
�cido Ac�tico (1:5)
�cido ac�tico ( 1:5 )
�cido Ac�tico (1 : 5)
�CIDO FORMICO
�cido F�rmico
�cido F�rmico (1 : 5 / 28 oC)
�cido F�rmico (1 : 5)
�cido F�rmico (1:10)
Acido Ox�lico
�cido Ox�lico
�cido Sulf�rico (1 : 10) EM
�cido Sulf�rico (1:10)
ACTICE WB 420
ACTICIDE WB 420
ACTIVE WB 420
Agua
�gua
�gua 25 �C
�gua 28 �C
�gua 30 �C
�gua 32 �C
�gua 35 �C
�gua 35 �C
�gua 40 �C
�gua 43 �C
�gua 45 �C
�gua 48 �C
�gua 50 �C
�gua 55 �C
�gua 60 �C
�gua 65 �C
�gua 65 �C
�gua 70 �C
�gua 27 �C
�gua 27 �C (cobrir couros)
�gua 28 �C
�gua 28 �C (cobrir couros)
�gua Ambiente
�gua Ambiente/Reciclo de cromo
�gua 25 �C
�gua 25 �C
�gua 27 �C
�gua 35 �C
�gua 35 �C
�gua 40 �C
�gua 42 �C
�gua 43 �C
�gua 50 �C
�gua 55 �C
�gua 60 �C
Agua a 30 �C
�gua a 40 �C
�gua Amb. Cobrindo os couros
�gua ambiente
�gua Fria
ALSOFT MLB
Alvox MK
AMARELO S.SO. 4GL
Amoniaco
amon�aco
AQUECER
AQUECER A 45 oC EM
ARACIT DA
ARACIT RM
Autom�tico
Autom�tico - Rodar 5' / Parar 25' - Por
AUTOM�TICO 5/55 - 8 HORAS
AUTOM�TICO A NOITE 8H/10H
automatico anoite
AUTOM�TICO DURANTE
AZUL S.SO. A
Bactericida
Basificante
BEGE S.SO.E
Bemanol CR (Sthal)
Bicarbonato de Am�nia
Bicarbonato de Am�nio
Bicarbonato de S�dio
Biocid A-8
Bissulfito de s�dio
BORRON ANV
BORRON LFG
BORRON TE
BORRON 6113
BORRON ANV
BORRON DL-LA
BORRON DNC
BORRON LFG
BORRON SAF
BORRON ST
BORRON WP
Busan 144
Busan 1444
BUSAN 1451
Cal
Cal Hidratada
Carbonato de s�dio
Castanheira
Castanheiro
CASTANHO A3R
CASTANHO ADF
CASTANHO RA
CASTANHO S.SO. CG
CASTANHO S.SO. DS
CASTANHO S.SO. GLN
CASTANHO S.SO. H
CASTANHO S.SO. RE
CASTANHO S.SO. RLN
CASTANHO SELLASET M
CHROMOSAL B
Clarotan
CLORITO DE S�DIO (1 : 10)
Colocar 10% de reciclo se necess�rio
cor:
Corante
CORIPOL A
CORIPOL BR-SU
CORIPOL EFA
CORIPOL GA
CORIPOL OPS
CORIPOL A
CORIPOL A TESTE
CORIPOL ALF
CORIPOL BLV
CORIPOL BR CT
CORIPOL BR SL
CORIPOL BR SU
CORIPOL BR-CT
CORIPOL BR-OP
CORIPOL BR-SU
CORIPOL CLD
CORIPOL HDS
CORIPOL IRH
CORIPOL MK
CORIPOL MLA
CORIPOL OPS
CORIPOL RB
CORIPOL RB (sozinho)
CORIPOL SG
CORIPOL SG BR
CORIPOL SG LA
CORIPOL SG-BR
CORIPOL SG-LA
CORIPOL SLG
couros no ful�o
CROMENO FB
CROMENO 6213 New
CROMENO XP
Cromo 26/33
Cromo 33/26
Cromosal B
Delitan K1
DERMASCAL 6140 (1:5)
DERMASCAL 6140 (1:5)
DERMASCAL DU LA
DERMASCAL 6140 (1:5)
DERMASCAL F LIQ.(1 : 5)
DERMASCAL HLA
DERMASCAL U LIQ.(1 : 5)
Descal SD
Descarregar
Deslizante
Deslizante p�
Di�xido de tit�nio
DRY WALK WAS 1
Dybest
ERHA GM 3034
ERHAVIT MC
ERHAVIT SRC
ERHAVIT 6211
ERHAVIT MC
ERHAVIT MC-F
ERHAVIT SR
ERHAVIT SRC
ESCORRER
EUPILON WAS 1
Feliderm MGO
Feliderm SAN
Feliderm SAN (deslizante)
FERVURA 3 MIN. 100%
FILLER WE
Formiato de S�dio
Formiato de S�dio
Fungicida
Fungicida (1 : 20)
Fungicida a base de OIT
GIRANDO
Glicerina
HAVANA E
HAVANA SELLA STAR
INVADERM LU
INVADERM LU
INVADERM SN
LARANJA S.SO 2 GC
LARANJA S.SO. A
LF Alvejante HO
LF Desencalante P�
LF Moc Cr�
LF Remolhante SE
LS Tan BA (RODACRYL I 113)
Mabratan TA
MAGNOPAL HBR
MAGNOPAL TGR
MAGNOPAL BC3
MAGNOPAL PGN
MAGNOPAL RHN
MAGNOPAL SFT
MAGNOPAL D LA
MAGNOPAL DLA
MAGNOPAL D-LA
MAGNOPAL FTP
MAGNOPAL HBR
MAGNOPAL RHN
MAGNOPAL SFT
MARINHO S.SO. A
Metabissulfito de S�dio
Mimosa
Mimosa (Clarotan)
Mirecide - 50 / TS
MK ECT 005
MK ER 130
MK KROMIUM PP
NO FINAL
Nokotan 1016
NOKOTAN AP 810
Nutratan CR
OLIVA S.SO. 2G
OLIVA SELLA STAR
OLIVA SELLASTAR
PELLVIT B
PELLVIT IS-B
PELLVIT KAB P
PELVIT B-LA
PR�-DESCARNE
PRETO SSO IG CONC.
PRETO HS
PRETO IG conc (1:10)
PRETO S.SO BG
PRETO S.SO. BA
PRETO S.SO. BG
PRETO S.SO. IG
PRETO S.SO. WB
PRETO SELLA S�LIDO HS
PRETO SSO FT
PRODUTO L 6211
PRODUTO VL 6213
Quebracho
Reciclo
RECICLO DE CALEIRO
Remonte
RODA FEEL CLD
RODACOR AS BRANCO AZL
RODANDO
Rodar
Rodar total de 2 horas de neutraliza��o
RODOU MAIS
ROHAPON NPB
ROHAPON NPB
Royaltan
Sal
Sal de Cromo - 33% BAS/26% CONC.
Sal Fino
Sal limpo
SELLA SOLIDO PRETO BA
SELLAFIX FRD
SELLAFIX FRD
SELLASOL NG Liq.
SELLASOL BR SF
SELLASOL BR-SF
SELLASOL FT new
SELLASOL HF N
SELLASOL NG
SELLASOL NG GRAN
SELLASOL NG gran.
SELLASOL NG Liq
SELLASOL NG LIQ.
SELLASOL NG p�
SELLASOL TN GRAN
SELLASOL TN Gran.
SELLASOL TN PO
SELLASOL TN p�
SELLASOL TN-GRAN
SELLASOL TN-P�
SELLASOLIDO PRETO BA
SELLATAM FB LIQ.
SELLATAN CF NEW Liq.
SELLATAN RL LIQ.
SELLATAN AR
SELLATAN CF NEW
SELLATAN CF-NEW
SELLATAN D LA
SELLATAN D-LA
SELLATAN FB
SELLATAN FB liq.
SELLATAN FL
SELLATAN GS-B
SELLATAN KL
SELLATAN RBL
SELLATAN RL
SELLATAN RL Liq.
SELLATAN TA-LA
SEM BANHO
SERVOIL CB
SOBRA DE BANHO
SOBRA DE �GUA LIMPA
Sobra de lavagem conforme escala
Soda Barrilha
Soda C�ustca Liq.
Soda L�quida
Sorbitol
Sulfato b�sico de cromo
Sulfato de Am�nea
Sulfato de Am�nio
Sulfeto de S�dio
Sulfeto de S�dio 50%
Sulfeto de S�dio 41%
Sulfeto de S�dio 60%
TABACO SELLA STAR
Tan PVU
TANIGAN PR
Tanigan PC
Tanigan PR
TANNESCO HN Liq.
TANNESCO HN
TANNESCO HN liq.
TANNESCO HN L�Q.
Tara
Tensoativo
TURQUESA S.SO. 2BL
Ultrader DGW 710
Ultrader DWG 710
Ultraprime WAX
V�LVULAS ABERTAS
VELOCIDADE 02
VELOCIDADE 04
VER COR BANHO E CHEIRO
VERMELHO S.SO. E
Weibull Extra Light
*)

  Txt := Geral.Maiusculas(Geral.SemAcento(Txt), False);
  K := pos('�C', Txt);
  if K > 0 then
  begin
    Sai := False;
    for I := K - 1 downto 1 do
    begin
      if not Sai then
      begin
        if (CharInSet(Txt[I], (['0'..'9', ' ']))) then
          GC := Txt[I] + GC
        else
          Sai := True;
      end;
    end;
    GC := Trim(GC);
  end;

  //

  if Tmp <> '' then
  begin
    Produto := Tmp;
    Diluicao := IntToStr(Dil);
    Temperatura := GC;
  end else begin
    Produto := Texto;
    Diluicao := '';
    Temperatura := '';
  end;
end;

end.
