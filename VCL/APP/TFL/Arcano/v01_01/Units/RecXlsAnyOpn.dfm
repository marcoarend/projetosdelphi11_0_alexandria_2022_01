object FmRecXlsAnyOpn: TFmRecXlsAnyOpn
  Left = 0
  Top = 0
  Caption = 'REC-ANYOP-001'
  ClientHeight = 491
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 784
    Height = 491
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Abrir arquivo excel'
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 776
        Height = 463
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Panel5: TPanel
          Left = 0
          Top = 0
          Width = 776
          Height = 96
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Label27: TLabel
            Left = 8
            Top = 4
            Width = 120
            Height = 13
            Caption = 'Arquivo a ser carregado:'
          end
          object SbSelArq: TSpeedButton
            Left = 730
            Top = 20
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SbSelArqClick
          end
          object LaAviso: TLabel
            Left = 12
            Top = 44
            Width = 15
            Height = 22
            Caption = '...'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -19
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
          end
          object SbAbre: TSpeedButton
            Left = 752
            Top = 20
            Width = 21
            Height = 21
            Caption = '>'
            OnClick = SbAbreClick
          end
          object EdArq: TdmkEdit
            Left = 8
            Top = 20
            Width = 721
            Height = 21
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = 
              '\\NOVASERV\Sincro\_Marco\_TFL\Receitas\Apucacouros\2011 11 11 su' +
              'gest'#227'o\FLOATHER SONHO CORIPOL HDS .xls'
            QryCampo = 'DirNFeGer'
            UpdCampo = 'DirNFeGer'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 
              '\\NOVASERV\Sincro\_Marco\_TFL\Receitas\Apucacouros\2011 11 11 su' +
              'gest'#227'o\FLOATHER SONHO CORIPOL HDS .xls'
            ValWarn = False
          end
          object PB1: TProgressBar
            Left = 12
            Top = 68
            Width = 761
            Height = 17
            TabOrder = 1
          end
        end
        object Grade1: TStringGrid
          Left = 0
          Top = 96
          Width = 776
          Height = 367
          Align = alClient
          ColCount = 2
          DefaultColWidth = 44
          DefaultRowHeight = 18
          RowCount = 2
          TabOrder = 1
          ExplicitTop = 88
        end
      end
    end
    object TSReceita: TTabSheet
      Caption = 'Receita'
      ImageIndex = 1
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 776
        Height = 161
        Align = alTop
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Label3: TLabel
          Left = 4
          Top = 24
          Width = 37
          Height = 13
          Caption = 'Cliente:'
        end
        object Label4: TLabel
          Left = 312
          Top = 24
          Width = 40
          Height = 13
          Caption = 'T'#233'cnico:'
        end
        object Label5: TLabel
          Left = 488
          Top = 24
          Width = 43
          Height = 13
          Caption = 'Account:'
        end
        object Label6: TLabel
          Left = 4
          Top = 48
          Width = 33
          Height = 13
          Caption = 'Artigo:'
        end
        object Label7: TLabel
          Left = 484
          Top = 48
          Width = 43
          Height = 13
          Caption = 'Rebaixe:'
        end
        object Label8: TLabel
          Left = 636
          Top = 24
          Width = 64
          Height = 13
          Caption = 'Rend. kg/m'#178':'
        end
        object Label9: TLabel
          Left = 612
          Top = 48
          Width = 76
          Height = 13
          Caption = 'Espessura final:'
        end
        object Label10: TLabel
          Left = 4
          Top = 72
          Width = 56
          Height = 13
          Caption = '% Relat. a:'
        end
        object Label11: TLabel
          Left = 232
          Top = 72
          Width = 41
          Height = 13
          Caption = 'Peso kg:'
        end
        object Label12: TLabel
          Left = 380
          Top = 72
          Width = 84
          Height = 13
          Caption = 'C'#226'mbio BRL/USD:'
        end
        object Label13: TLabel
          Left = 664
          Top = 96
          Width = 27
          Height = 13
          Caption = 'Data:'
        end
        object Label14: TLabel
          Left = 552
          Top = 72
          Width = 42
          Height = 13
          Caption = 'Quantia:'
        end
        object LaNome: TLabel
          Left = 0
          Top = 0
          Width = 776
          Height = 13
          Align = alTop
          Caption = 'nova_receita.xls'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ExplicitWidth = 94
        end
        object Label15: TLabel
          Left = 4
          Top = 96
          Width = 67
          Height = 13
          Caption = 'Observa'#231#245'es:'
        end
        object Label16: TLabel
          Left = 348
          Top = 48
          Width = 21
          Height = 13
          Caption = 'Cor:'
        end
        object Label1: TLabel
          Left = 668
          Top = 72
          Width = 43
          Height = 13
          Caption = 'Unidade:'
        end
        object EdCliNome: TdmkEdit
          Left = 48
          Top = 20
          Width = 261
          Height = 21
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdTecnico: TdmkEdit
          Left = 356
          Top = 20
          Width = 125
          Height = 21
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdAccount: TdmkEdit
          Left = 536
          Top = 20
          Width = 93
          Height = 21
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdArtigo: TdmkEdit
          Left = 48
          Top = 44
          Width = 297
          Height = 21
          TabOrder = 3
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdEspRebCul: TdmkEdit
          Left = 528
          Top = 44
          Width = 40
          Height = 21
          Alignment = taCenter
          TabOrder = 4
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdRendiment: TdmkEdit
          Left = 704
          Top = 20
          Width = 68
          Height = 21
          Alignment = taRightJustify
          TabOrder = 8
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdEspRebCab: TdmkEdit
          Left = 568
          Top = 44
          Width = 40
          Height = 21
          Alignment = taCenter
          TabOrder = 5
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdEspFimCul: TdmkEdit
          Left = 692
          Top = 44
          Width = 40
          Height = 21
          Alignment = taCenter
          TabOrder = 6
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdEspFimCab: TdmkEdit
          Left = 732
          Top = 44
          Width = 40
          Height = 21
          Alignment = taCenter
          TabOrder = 7
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdRelatA: TdmkEdit
          Left = 64
          Top = 68
          Width = 165
          Height = 21
          TabOrder = 9
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdPesoCouro: TdmkEdit
          Left = 276
          Top = 68
          Width = 100
          Height = 21
          Alignment = taRightJustify
          TabOrder = 10
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdCambio: TdmkEdit
          Left = 468
          Top = 68
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 11
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdData: TdmkEdit
          Left = 692
          Top = 92
          Width = 80
          Height = 21
          TabOrder = 13
          FormatType = dmktfDate
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0d
          ValWarn = False
        end
        object EdQuantia: TdmkEdit
          Left = 600
          Top = 68
          Width = 65
          Height = 21
          Alignment = taRightJustify
          TabOrder = 12
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 1
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object Button1: TButton
          Left = 376
          Top = 128
          Width = 75
          Height = 25
          Caption = 'Editar'
          TabOrder = 15
          OnClick = Button1Click
        end
        object EdObserv: TdmkEdit
          Left = 80
          Top = 92
          Width = 577
          Height = 21
          TabOrder = 14
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object RGStatus: TRadioGroup
          Left = 4
          Top = 116
          Width = 365
          Height = 41
          Caption = ' Status: '
          Columns = 3
          ItemIndex = 0
          Items.Strings = (
            'Amostra'
            'Em Produ'#231#227'o'
            'Teste produ'#231#227'o')
          TabOrder = 16
        end
        object EdCor: TdmkEdit
          Left = 376
          Top = 44
          Width = 104
          Height = 21
          TabOrder = 17
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdUnidCouro: TdmkEdit
          Left = 712
          Top = 68
          Width = 60
          Height = 21
          TabOrder = 18
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 1
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object Button2: TButton
          Left = 692
          Top = 128
          Width = 75
          Height = 25
          Caption = 'Button1'
          TabOrder = 19
          OnClick = Button1Click
        end
      end
      object SGReceita: TStringGrid
        Left = 0
        Top = 161
        Width = 776
        Height = 302
        Align = alClient
        ColCount = 9
        DefaultColWidth = 44
        DefaultRowHeight = 18
        RowCount = 2
        TabOrder = 1
        OnDrawCell = SGReceitaDrawCell
      end
    end
  end
end
