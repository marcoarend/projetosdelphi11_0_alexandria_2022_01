unit Module;

// C:\Executaveis\Auxiliares\sqlite3.dll
// C:\Dermatek\SQLite\Quota.s3db

interface

uses
  System.SysUtils, System.Classes, Vcl.Forms, Data.DB, ZAbstractConnection,
  ZConnection, ZCompatibility, dmkGeral, ZAbstractRODataset, ZAbstractDataset,
  ZDataset, UnProjGroup_Vars, UnAppEnums, UnGrl_Vars, ZConnectionGroup;

type
  TDMod = class(TDataModule)
    MyDB: TZConnection;
    QrLoc: TZQuery;
    QrControle: TZQuery;
    QrAux: TZQuery;
    QrUpd: TZQuery;
    QrNxt: TZQuery;
    QrPsq1: TZQuery;
    QrNTV: TZQuery;
    QrPsq2: TZQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure RefreshUserSetInfo();
  end;

var
  DMod: TDMod;

implementation

{%CLASSGROUP 'System.Classes.TPersistent'}

uses ModuleGeralZ, UnVCL_ZDB, UnGrl_DmkDB, UnInternalConsts, MyListas,
  UnAppJan, Principal, UnMyObjects, MyGlyfs;

{$R *.dfm}

procedure TDMod.DataModuleCreate(Sender: TObject);
begin
  MyDB.Connected := False;
  VAR_TMeuDB_ZDir := GetEnvironmentVariable('USERPROFILE') +
    '\TFL\Apps' + '\SQLite\';
  //MyDB.Database := VAR_TMeuDB_ZDir  + TMeuDB + '.s3db';
  MyObjects.DefineTextoEWidth(FStatusBar_DBPath, VAR_TMeuDB_ZDir,
    FmPrincipal.AdvStatusBar.Font.Name, FmPrincipal.AdvStatusBar.Font.Size);
  //
  VAR_BDSENHA := 'wkljweryhvbirt';
  VAR_LOCAL_DB_COMPO_DATASET := QrNxt;
  VAR_LOCAL_DB_COMPO_DATABASE := MyDB;
  //
  VAR_GOTOzSQLDBNAME := MyDB;
  //
  VAR_SQLx := TStringList.Create;
  VAR_SQL1 := TStringList.Create;
  VAR_SQL2 := TStringList.Create;
  VAR_SQLa := TStringList.Create;
  //
  MAR_SQLx := TStringList.Create;
  MAR_SQL1 := TStringList.Create;
  MAR_SQLa := TStringList.Create;
  //
  if not VCL_ZDB.ConfiguraBD(MyDB, QrControle, QrLoc) then
    Application.Terminate;
  MyObjects.DefineTextoEWidth(FStatusBar_DBPath, VAR_TMeuDB_ZDir,
    FmPrincipal.AdvStatusBar.Font.Name, FmPrincipal.AdvStatusBar.Font.Size);
  //
  RefreshUserSetInfo();
  //
  // no final
  try
    Application.CreateForm(TDmodGZ, DmodGZ);
  except
    Geral.MB_Aviso('Imposs�vel criar Modulo de dados Geral "Z"');
    Application.Terminate;
    Exit;
  end;
  if VAR_USUARIO = 0 then
    AppJan.MostraFormOpcoesApp();
  if VAR_USUARIO = 0 then
  begin
    Geral.MB_Aviso('Usu�rio n�o definido!');
    Application.Terminate;
    Exit;
  end;
end;

procedure TDMod.RefreshUserSetInfo();
var
  Defindo: Boolean;
begin
  VAR_USUARIO := 0;
  VAR_LOGIN   := '';
  Grl_DmkDB.AbreSQLQuery0(QrPsq1, MyDB, [
  'SELECT UserNome, UserSigla, UserMD5, UserID ',
  'FROM controle ',
  '']);
  //
  VAR_USUARIO := QrPsq1.FieldByName('UserID').AsInteger;
  VAR_IDServr := VAR_USUARIO;
  VAR_LOGIN   := QrPsq1.FieldByName('UserNome').AsString;
  MyObjects.DefineTextoEWidth(FStatusBar_UserNome, Geral.FF0(VAR_USUARIO) +
  '-' + VAR_LOGIN, FmPrincipal.AdvStatusBar.Font.Name,
  FmPrincipal.AdvStatusBar.Font.Size);
end;

end.
