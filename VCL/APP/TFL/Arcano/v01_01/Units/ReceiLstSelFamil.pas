unit ReceiLstSelFamil;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, dmkEdit, Vcl.ExtCtrls,
  Vcl.Grids, Vcl.DBGrids, Data.DB, ZAbstractRODataset, ZAbstractDataset,
  ZDataset, UnDmkEnums, DmkGeral, UnGrl_DmkDB;

type
  TFmReceiLstSelFamil = class(TForm)
    Panel1: TPanel;
    Button1: TButton;
    Button2: TButton;
    QrProdutosFamil: TZQuery;
    DsProdutosFamil: TDataSource;
    Panel2: TPanel;
    DBGrid1: TDBGrid;
    EdProdutosFamil: TdmkEdit;
    Panel3: TPanel;
    EdEntidadesCptt: TdmkEdit;
    Label1: TLabel;
    Label2: TLabel;
    QrEntidadesCptt: TZQuery;
    DsEntidadesCptt: TDataSource;
    QrProdutosFamilCodigo: TLargeintField;
    QrProdutosFamilNome: TWideStringField;
    QrEntidadesCpttCodigo: TLargeintField;
    QrEntidadesCpttNome: TWideStringField;
    DBGrid2: TDBGrid;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure EdProdutosFamilChange(Sender: TObject);
    procedure EdEntidadesCpttChange(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenPesquisaEntidadesCptt();
    procedure ReopenPesquisaProdutosFamil();
    procedure DefineProduto();
  public
    { Public declarations }
    FLstProdQuim: TLstProdQuim;
  end;

var
  FmReceiLstSelFamil: TFmReceiLstSelFamil;

implementation

{$R *.dfm}

uses Module, ReceitasCab;

procedure TFmReceiLstSelFamil.Button1Click(Sender: TObject);
begin
  DefineProduto();
end;

procedure TFmReceiLstSelFamil.Button2Click(Sender: TObject);
begin
  Close;
end;

procedure TFmReceiLstSelFamil.DefineProduto();
begin
  FmReceitasCab.LimpaCamposProduto();
  //
  case TLstProdQuim(FLstProdQuim) of
    lpqIndef:   ;
    lpqRefere:  ;
    lpqSimilar: ;
    lpqComodit: ;
    lpqBanhos:  ;
    lpqTextos:  ;
    lpqBranco:  ;
    lpqFamilia:
    begin
      FmReceitasCab.TbReceitasItsEntidadesCptt.Value := QrEntidadesCpttCodigo.Value;
      FmReceitasCab.TbReceitasItsProdutosFamil.Value := QrProdutosFamilCodigo.Value;
    end;
    else ;
  end;
  Close;
end;

procedure TFmReceiLstSelFamil.EdEntidadesCpttChange(Sender: TObject);
begin
  ReopenPesquisaEntidadesCptt();
end;

procedure TFmReceiLstSelFamil.EdProdutosFamilChange(Sender: TObject);
begin
  ReopenPesquisaProdutosFamil();
end;

procedure TFmReceiLstSelFamil.FormCreate(Sender: TObject);
begin
 //Self.Left := 600;
end;

procedure TFmReceiLstSelFamil.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_ESCAPE then
    Close;
end;

procedure TFmReceiLstSelFamil.ReopenPesquisaEntidadesCptt();
var
  SQLWhere, Texto: String;
begin
  Texto := EdEntidadesCptt.Text;
  if Trim(Texto) <> '' then
    SQLWhere := 'WHERE Nome LIKE "%' + Texto + '%"';
  //
  Grl_DmkDB.AbreSQLQuery0(QrEntidadesCptt, Dmod.MyDB, [
  'SELECT Codigo, Nome',
  'FROM entidadescptt',
  SQLWhere,
  'ORDER BY Nome ',
  '']);
end;

procedure TFmReceiLstSelFamil.ReopenPesquisaProdutosFamil();
var
  SQLWhere, Texto: String;
begin
  Texto := EdProdutosFamil.Text;
  if Trim(Texto) <> '' then
    SQLWhere := 'WHERE Nome LIKE "%' + Texto + '%"';
  //
  Grl_DmkDB.AbreSQLQuery0(QrProdutosFamil, Dmod.MyDB, [
  'SELECT Codigo, Nome',
  'FROM produtosfamil',
  SQLWhere,
  'ORDER BY Nome ',
  '']);
end;

end.
