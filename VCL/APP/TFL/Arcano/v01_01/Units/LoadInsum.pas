unit LoadInsum;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.ComCtrls, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Grids,
  dmkGeral, dmkEdit, UnDmkEnums, Data.DB, ZAbstractRODataset, ZDataset,
  ZAbstractDataset, Vcl.Buttons, Vcl.Menus, AdvGridWorkbook, tmsAdvGridExcel,
  AdvObj, BaseGrid, AdvGrid, UnAppEnums, RichEdit, UnInternalConsts,
  (*dmkPopOutFntCBox,*)
  ClipBrd, ToolWin, ActnList, UnDmkProcFunc,
  ImgList, DBCtrls, ShellAPI, dmkDBLookupComboBox, dmkEditCB, Vcl.DBGrids,
  ActiveX,
  //Bytescout_PDFExtractor_TLB in 'c:\program files\borland\bds\4.0\Imports\Bytescout_PDFExtractor_TLB.pas';
  Bytescout_PDFExtractor_TLB; // in 'C:\Users\Admin\Documents\RAD Studio\9.0\Imports\Bytescout_PDFExtractor_TLB.pas';

type
  //
  TFmLoadInsum = class(TForm)
    SGSales: TStringGrid;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PB1: TProgressBar;
    Query1: TZQuery;
    Query2: TZQuery;
    Memo1: TMemo;
    Splitter1: TSplitter;
    StatusBar: TStatusBar;
    QrEntidadesCptt: TZQuery;
    QrEntidadesCpttCodigo: TLargeintField;
    QrEntidadesCpttNome: TWideStringField;
    QrEntidadesCpttSigla: TWideStringField;
    DsEntidadesCptt: TDataSource;
    Panel1: TPanel;
    Panel5: TPanel;
    Label27: TLabel;
    SbSelArq: TSpeedButton;
    SbAbre: TSpeedButton;
    SpeedButton1: TSpeedButton;
    Label2: TLabel;
    Label1: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    EdPath: TdmkEdit;
    EdMaxlinTit: TdmkEdit;
    EdPlanilha: TdmkEdit;
    Button4: TButton;
    RGIQ: TRadioGroup;
    dmkEdit1: TdmkEdit;
    EdMemiLinTxt: TdmkEdit;
    EdEntidadesCptt: TdmkEditCB;
    CBEntidadesCptt: TdmkDBLookupComboBox;
    Panel2: TPanel;
    DBGCadastros: TDBGrid;
    QrCadastros0: TZQuery;
    DsCadastros0: TDataSource;
    QrCadastros0EntidadesCptt: TLargeintField;
    QrCadastros0Nome: TWideStringField;
    QrCadastros0ITENS: TLargeintField;
    RGIQ_PQ: TRadioGroup;
    BitBtn1: TBitBtn;
    ListBox04: TListBox;
    QrCadastros0Familias: TLargeintField;
    QrCadastros2: TZQuery;
    QrCadastros2EntidadesCptt: TLargeintField;
    QrCadastros2Nome: TWideStringField;
    DsCadastros2: TDataSource;
    QrCadastros2ITENS: TWideMemoField;
    QrCadastros2Familias: TWideMemoField;
    PMCadastros: TPopupMenu;
    Setarcomopesquisado1: TMenuItem;
    Button1: TButton;
    Splitter2: TSplitter;
    SGXLS: TStringGrid;
    RGPDF: TRadioGroup;
    procedure SbSelArqClick(Sender: TObject);
    procedure SbAbreClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure dmkEdit1Redefinido(Sender: TObject);
    procedure Memo1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Memo1KeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Memo1Enter(Sender: TObject);
    procedure Memo1Exit(Sender: TObject);
    procedure Memo1Gesture(Sender: TObject; const EventInfo: TGestureEventInfo;
      var Handled: Boolean);
    procedure RGIQClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure RGIQ_PQClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure PMCadastrosPopup(Sender: TObject);
    procedure Setarcomopesquisado1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
    FParar: Boolean;
    FSLCampos: TStringList;
    FilPer: Double;
    FilTam, FilPos, Cento: Largeint;
    F: TextFile;
    InitDir: String;
    //
    ColPDF: Integer;
    //
    procedure AdicionaItemSG(Item: Integer; Segmento, Divisao,  SubDivisao,
              Nome, Composicao, Propriedades, pH, CargaIonica, Aspecto,
              Utilizacao, Perc_Uso, ConcouAtiv, SolidezLuz, LocalPDF: String);
    function  ArrumaTextoHTML(Texto: String): String;
    procedure CarregaProdutosMK();
    procedure CarregaProdutosCsvGetti();
    procedure CarregaProdutosCsvQualquer();
    procedure CarregaProdutosPdfCorium();
    procedure CarregaProdutosTxtLF();
    procedure CarregaProdutosURLAtlas();
    procedure CarregaProdutosURLGetti();
    procedure CarregaProdutosURLCorichem();
    procedure CarregaProdutosURLDermacolor();
    procedure CarregaProdutosURLLanxess();
    procedure CarregaProdutosURLLeatherQuimica();
    procedure CarregaProdutosXlsQualquer();
    function  ConcatCampos(Ini: Integer; Fim: Integer = 0): String;
    function  SentencaCampos(Ini: Integer; Fim: Integer = 0): String;
    function  ExtractText(const aText, OpenTag, CloseTag: String; var PosicaoFinal:
              Integer; var TxtExtraido, TxtRestante: String): Boolean;
    //
    function  LeLinhaELimpa(): String;
    procedure LimpaIQ();
    procedure UpdateCursorPos(Control: TCustomEdit); //TWinControl);
    procedure ObtemNome();
    procedure ObtemIQFamily();
    procedure ObtemNatureza();
    procedure ObtemPropriedades();
    procedure ReopenCadastros();
    procedure UpdateInfo(nI: Integer);
    procedure CadastraEntidadesCpttPsq(Codigo, CodUsu, Pesquisado:
              Integer);
  public
    { Public declarations }
  end;

var
  FmLoadInsum: TFmLoadInsum;


implementation

uses UnMyObjects, Module, UnGrl_DmkDB, UnDmkWeb;

{$R *.dfm}

{ TForm1 }

const
  CO_IDServr = 1;
  _Colunas = 14;

procedure TFmLoadInsum.AdicionaItemSG(Item: Integer; Segmento, Divisao,
  SubDivisao, Nome, Composicao, Propriedades, pH, CargaIonica, Aspecto,
  Utilizacao, Perc_Uso, ConcouAtiv, SolidezLuz, LocalPDF: String);
begin
  if  Item > 0 then
  begin
    SGSales.RowCount := Item + 1;
    SGSales.Cells[0, Item] := Geral.FF0(Item);
    SGSales.Cells[1, Item] := Segmento;
    SGSales.Cells[2, Item] := Divisao;
    SGSales.Cells[3, Item] := SubDivisao;
    SGSales.Cells[4, Item] := SubDivisao;
    SGSales.Cells[5, Item] := Nome;
    SGSales.Cells[6, Item] := Composicao;
    SGSales.Cells[7, Item] := Propriedades;
    SGSales.Cells[8, Item] := pH;
    SGSales.Cells[9, Item] := CargaIonica;
    SGSales.Cells[10, Item] := Aspecto;
    SGSales.Cells[11, Item] := Utilizacao;
    SGSales.Cells[12, Item] := Perc_Uso;
    SGSales.Cells[13, Item] := ConcOuAtiv;
    SGSales.Cells[14, Item] := SolidezLuz;
    SGSales.Cells[ColPDF, Item] := LocalPDF;
  end;
end;

function TFmLoadInsum.ArrumaTextoHTML(Texto: String): String;
begin
  Result := Trim(Texto);
  Result := Geral.Substitui(Result, '<strong>', '');
  Result := Geral.Substitui(Result, '</strong>', '');
  Result := Geral.Substitui(Result, '<br>', '');
  Result := Geral.Substitui(Result, '</br>', '');
  if Pos('</', Result) > 0 then
  begin
    Geral.MB_Aviso('Texto ainda tem "</"' + sLineBreak + sLineBreak +
    Result);
  end
end;

procedure TFmLoadInsum.BitBtn1Click(Sender: TObject);
var
 extractor: _CSVExtractor;
 //extractor: _TextExtractor;
 //Texto: String;
 I: Integer;
begin
 CoInitialize(nil);

// Create and initialize Bytescout.PDFExtractor.CSVExtractor object using CoCSVExtractor class
 extractor := CoCSVExtractor.Create();
  extractor.RegistrationKey := 'E712-9595-A530-8F21-DBC1-AB4A-AC3';
  extractor.RegistrationName := 'NOT-FOR-RESALE-SINGLE-ENDUSER-ONLY-NO-PRIVATE-SUPPORT-marco@dermatek.com.br';
 //extractor.RegistrationName := 'demo';
 //extractor.RegistrationKey := 'demo';

 // Load sample PDF document
 //extractor.LoadDocumentFromFile ('../../sample3.pdf');
 extractor.LoadDocumentFromFile ('C:\Users\Admin\Documents\ByteScout Samples\PDF Extractor SDK\sample3.pdf');

// extractor.CSVSeparatorSymbol = ','; // you can change CSV separator symbol (if needed) from "," symbol to another if needed for non-US locales
 try
   //extractor.SaveCSVToFile ('output.csv');
    I := extractor.GetPageCount;



    extractor.SaveCSVToFile ('C:\Dermatek\output.csv');
    //Texto := extractor.Get_ToString();
    //Texto := extractor.GetCSVFromPage(0);
    //Texto := extractor.GetText;
    Geral.MB_Info(Geral.FF0(I));
 except
   Exit;
 end;
 extractor := nil;

{
 CoInitialize(nil);

// Create and initialize Bytescout.PDFExtractor.CSVExtractor object using CoCSVExtractor class
 extractor := CoCSVExtractor.Create();
 extractor.RegistrationName := 'demo';
 extractor.RegistrationKey := 'demo';

 // Load sample PDF document
 //extractor.LoadDocumentFromFile ('../../sample3.pdf');
 extractor.LoadDocumentFromFile ('C:\Users\Admin\Documents\ByteScout Samples\PDF Extractor SDK\sample3.pdf');

// extractor.CSVSeparatorSymbol = ','; // you can change CSV separator symbol (if needed) from "," symbol to another if needed for non-US locales
 try
   //extractor.SaveCSVToFile ('output.csv');
   //extractor.SaveCSVToFile ('C:\Dermatek\TFL\output.csv');
    //Texto := extractor.Get_ToString();
    I := extractor.GetPageCount;
    //Texto := extractor.GetCSVFromPage(0);
    Texto := extractor.GetCSV;
    Geral.MB_Info(Geral.FF0(I));
 except
   Exit;
 end;
 // reset the extractor so could load another file
 extractor.Reset();

 // now load another file
 // Load sample PDF document
 extractor.LoadDocumentFromFile ('../../sample2.pdf');

 extractor.SaveCSVToFile ('output2.csv');


 // destroy the extractor object
 extractor := nil;
}
end;

procedure TFmLoadInsum.Button1Click(Sender: TObject);
var
  ArqOri: String;
begin
  //CarregaProdutosURLLeatherQuimica();
  ArqOri := ExtractShortPathName('C:\Arcano\xpdf-tools-win-4.00\bin64\Teste\CORIPOL GU .pdf');
  Geral.MB_Info(ArqOri);
  //pdftotext.exe C:\Arcano\xpdf-tools-win-4.00\bin64\Teste\CORIPOL GU .pdf C:\Arcano\xpdf-tools-win-4.00\bin64\Teste\x.txt
  //C:\Arcano\xpdf-tools-win-4.00\bin64>pdftotext.exe "C:\Arcano\xpdf-tools-win-4.00\bin64\Teste\CORIPOL GU .pdf" "C:\Arcano\xpdf-tools-win-4.00\bin64\Teste\x2.txt"

end;

procedure TFmLoadInsum.Button4Click(Sender: TObject);
begin
  FParar := True;
end;

procedure TFmLoadInsum.CadastraEntidadesCpttPsq(Codigo, CodUsu, Pesquisado:
  Integer);
var
  SQLType: TSQLType;
begin
  Grl_DmkDB.AbreSQLQuery0(Query1, Dmod.MyDB, [
  'SELECT Codigo ',
  'FROM entidadescpttpsq ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  '']);
  if Query1.RecordCount = 0 then
    SQLType := stIns
  else
    SQLType := stUpd;
  Query1.Close;
  //
  if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, 'entidadescpttpsq', False, [
  'CodUsu', 'Pesquisado'], [
  'Codigo'], [
  CodUsu, Pesquisado], [
  Codigo], True, dmksqlinsInsOnly, '', TDeviceType.stDesktop, False) then
    ReopenCadastros();
end;

procedure TFmLoadInsum.CarregaProdutosCsvGetti();
const
  SubDir4  = True;
  ArqNome4 = '*';
  ArqExte4 = 'csv';
var
  Cam: String;
  //
  function GetFiles(Subdiretorios: Boolean; Dir, Arq: string;
    ListBox: TCustomListBox; ClearListBox: Boolean): Integer;
  var
    search: TSearchRec;
    directory, mask: string;
  begin

    Result := 0;
    mask := Dir + Arq;
    // N�o no loop!!!!
    if ClearListBox and (ListBox <> nil) then ListBox.Items.Clear;
    //
    directory := ExtractFilePath(mask);

    // find all files
    if FindFirst(mask, $23, search) = 0 then
    begin
      repeat
        // add the files to the listbox
        if ListBox <> nil then
        begin
          if dmkPF.ComparaTextosComMascara(Arq, search.Name, False) then
          //ListBox.Items.Add(directory + search.Name);
            ListBox.Items.Add(directory + search.Name);
        end;
        Result := Result + 1;
        //Inc(Count);
      until FindNext(search) <> 0;
    end;

    if Subdiretorios then
    begin
      if FindFirst(directory + '*.*', faDirectory, search) = 0 then
      begin
        repeat
          if ((search.Attr and faDirectory) = faDirectory) and (search.Name[1] <> '.') then
            Result := Result + GetFiles(Subdiretorios, Dir + Search.Name + '\', Arq, ListBox, False);
        until FindNext(search) <> 0;
        FindClose(search);
      end;
    end;
  end;
  procedure CarregaPQdeArqCSV(Arq: String; It: Integer);
  var
    Linha, Texto, Campo, Valor: String;
    I, P, Step: Integer;
    Segmento, Divisao, SubDivisao, Nome, Natureza, Propriedades, pH,
    CargaIonica, Aspecto, Utilizacao, Perc_Uso, ConcouAtiv, SolidezLuz,
    LocalPDF: String;
  begin
    //Arq := Trim(EdPath.Text);
    if FileExists(Arq) then
    begin
      Texto      := Copy(Arq, Length(Cam) + 1);
      P          := Pos('\', Texto);
      Segmento   := Copy(Texto, 1, P - 1);
      Texto      := Copy(Texto, P + 1);
      P          := Pos('\', Texto);
      Divisao    := Copy(Texto, 1, P - 1);
      SubDivisao := Divisao;
      //
      AssignFile(F, Arq);
      try
        Reset(F);
        FilTam := FileSize(F);
        FSLCampos := TStringList.Create;
        try
          PB1.Position := 0;
          PB1.Max := 100;
          //
          (*
          Readln(F, Linha);
          while pos(';', Linha) > 0 do
            Linha := Geral.Substitui(Linha, ';', '');
          *)
          Linha := LeLinhaELimpa();
          //Geral.MB_Info(Linha);
          if Uppercase(Linha) = Uppercase('Ficha t�cnica') then
          begin
            Nome := LeLinhaELimpa();
            Texto := '';
            //
            //Segmento     := '';
            //Divisao      := '';
            //SubDivisao   := '';
            //Nome         := '';
            Natureza     := '';
            Propriedades := '';
            pH           := '';
            CargaIonica  := '';
            Aspecto      := '';
            Utilizacao   := '';
            Perc_Uso     := '';
            ConcOuAtiv   := '';
            SolidezLuz   := '';
            LocalPDF     := ExtractFileDir(Arq);
            //
            Step := 0;
            while not Eof(F) do
            begin
              Application.ProcessMessages;
              if FParar then
                Exit;
              Readln(F, Linha);
              Linha := ';' + Linha + ';';
              FSLCampos := Geral.Explode3(Linha, ';', _Colunas);
              //
              (*
              Segmento     := FSLCampos[0];
              Divisao      := FSLCampos[1];
              SubDivisao   := FSLCampos[2];
              Nome         := FSLCampos[3];
              *)
              Campo := Uppercase(Trim(FSLCampos[0]));
              Valor := SentencaCampos(1);
              if Campo = '' then
              begin
                case Step of
                  1: Natureza := Trim(Natureza + ' ' + Valor);
                  2: ConcOuAtiv := Trim(ConcOuAtiv + ' ' + Valor);
                  3: pH := Trim(pH + ' ' + Valor);
                  4: CargaIonica := Trim(CargaIonica + ' ' + Valor);
                  5: Aspecto := Trim(Aspecto + ' ' + Valor);
                  6: SolidezLuz := Trim(SolidezLuz + ' ' + Valor);
                  7: Utilizacao := Trim(Utilizacao + ' ' + Valor);
                  8: Propriedades := Trim(Propriedades + ' ' + SentencaCampos(0));
                end;
              end
              else
              if (Campo = Uppercase('Natureza Qu�mica:'))
              or (Campo = Uppercase('Natureza Qu�mica')) then
              begin
                Step := 1;
                Natureza := Trim(Natureza + ' ' + Valor);
              end else
              if (
                (Campo = Uppercase('Subst�ncia Ativa:')) or
                (Campo = Uppercase('Teor de S�lidos:'))
              ) then
              begin
                Step := 2;
                ConcOuAtiv := Trim(ConcOuAtiv + ' ' + Valor);
              end
(*
              else
              if (
                (Campo = Uppercase('pH:')) or
                (Campo = Uppercase('pH 10%:'))
              ) then
*)
              else
              if (Copy(Campo, 1, 2) = Uppercase('pH')) then
              begin
                Step := 3;
                pH := Trim(pH + ' ' + Valor);
              end else
              if Campo = Uppercase('Carga I�nica:') then
              begin
                Step := 4;
                CargaIonica := Trim(CargaIonica + ' ' + Valor);
              end else
              if Campo = Uppercase('Aspecto:') then
              begin
                Step := 5;
                Aspecto := Trim(Aspecto + ' ' + Valor);
              end else
              if Campo = Uppercase('Solidez � Luz:') then
              begin
                Step := 6;
                SolidezLuz := Trim(SolidezLuz + ' ' + Valor);
              end else
              if Campo = Uppercase('Utiliza��o:') then
              begin
                Step := 7;
                Utilizacao := Trim(Utilizacao + ' ' + Valor);
              end else
              begin
                Step := 8;
                Propriedades := Trim(Propriedades + ' ' + SentencaCampos(0));
              end;
            end;
            //
            if Natureza <> '' then
              Natureza := Uppercase(Natureza[1]) + Copy(Natureza, 2);
            //
            AdicionaItemSG(It, Segmento, Divisao, SubDivisao, Nome, Natureza,
              Propriedades, pH, CargaIonica, Aspecto, Utilizacao, Perc_Uso,
              ConcouAtiv, SolidezLuz, LocalPDF);
            UpdateInfo(It);
          end else
            Geral.MB_Aviso('"Ficha t�cnica" n�o encontrado no arquivo: ' + Arq);

        finally
          FSLCampos.Free;
        end;
      finally
        CloseFile(f);
      end;
    end else
      Geral.MB_Erro('Arquivo n�o existe: ' + Arq);
    UpdateInfo(It);
  end;
var
  I: Integer;
begin
  Cam := InitDir + 'Getti\';
  Screen.Cursor := crHourGlass;
  try
    ListBox04.Items.Clear;
    //cam := EdArqDir4.Text;
    GetFiles(SubDir4, Cam, ArqNome4 + '.' + ArqExte4, ListBox04, False);
    {
    Cam := Application.Title + 'Pesquisa\';
    Geral.WriteAppKey('Texto', Cam, EdTexto.Text, ktString, HKEY_LOCAL_MACHINE);
    Geral.WriteAppKey('Diretorio', Cam, EdDiretorio.Text, ktString, HKEY_LOCAL_MACHINE);
    ListBox1.Clear;
    MyObjects.LimpaGrade(GradeA, 1, 1, True);
    for I := 0 to MeExtensoes.Lines.Count -1 do
    begin
      MLAGeral.GetAllFiles(CkSub1.Checked, EdDiretorio.Text+'\'+
      MeExtensoes.Lines[I], ListBox1, False);
    end;
    BtPesquisa.Enabled := True;
    }
    for I := 0 to ListBox04.Items.Count -1 do
    begin
      //MLAGeral.GetAllFiles(CkSub1.Checked, EdDiretorio.Text+'\'+
      //MeExtensoes.Lines[I], ListBox1, False);
      CarregaPQdeArqCSV(ListBox04.Items[I], I + 1);
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, False,
    'Carregamento finalizada! ' +
    Geral.FF0(SGSales.RowCount - 1) + ' registros carregados. ');
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmLoadInsum.CarregaProdutosCsvQualquer();
var
  Arq, Linha, Aspecto, Perc_Uso, ConcouAtiv, LocalPDF: String;
  Item, MulLinIdx: Integer;
  RegOK: Boolean;
  //
  SQLType: TSQLType;
var
  Segmento, Divisao, SubDivisao, Nome, Natureza, Propriedades, pH, CargaIonica,
  Utilizacao, SolidezLuz: String;
  //

  procedure CarregaCSV_Padrao();
  begin
    Readln(F, Linha);
    Linha := ';' + Linha + ';';
    FSLCampos := Geral.Explode3(Linha, ';', _Colunas);

    if (Uppercase(FSLCampos[0]) <> Uppercase('Segmento'     ))
    or (Uppercase(FSLCampos[1]) <> Uppercase('Divisao'      ))
    or (Uppercase(FSLCampos[2]) <> Uppercase('SubDivisao'   ))
    or (Uppercase(FSLCampos[3]) <> Uppercase('Nome'         ))
    or (Uppercase(FSLCampos[4]) <> Uppercase('Natureza'     ))
    or (Uppercase(FSLCampos[5]) <> Uppercase('Propriedades' ))
    or (Uppercase(FSLCampos[6]) <> Uppercase('pH'           ))
    or (Uppercase(FSLCampos[7]) <> Uppercase('CargaIonica'  ))
    or (Uppercase(FSLCampos[8]) <> Uppercase('Aspecto'      ))
    or (Uppercase(FSLCampos[9]) <> Uppercase('Utilizacao'   ))
    or (Uppercase(FSLCampos[10]) <> Uppercase('Perc_Uso'    ))
    or (Uppercase(FSLCampos[11]) <> Uppercase('ConcOuAtiv'  ))
    or (Uppercase(FSLCampos[12]) <> Uppercase('SolidezLuz'  ))
    or (Uppercase(FSLCampos[13]) <> Uppercase('LocalPDF'     )) then
    begin
      Geral.MB_Aviso('T�tulos de colunas n�o conferem com:' + sLineBreak +
      'Segmento'     + sLineBreak +
      'Divisao'      + sLineBreak +
      'SubDivisao'   + sLineBreak +
      'Nome'         + sLineBreak +
      'Natureza'     + sLineBreak +
      'Propriedades' + sLineBreak +
      'pH'           + sLineBreak +
      'CargaIonica'  + sLineBreak +
      'Aspecto'      + sLineBreak +
      'Utilizacao'   + sLineBreak +
      'Perc_Uso'     + sLineBreak +
      'ConcOuAtiv'   + sLineBreak +
      'SolidezLuz'   + sLineBreak +
      'LocalPDF'     + sLineBreak +
      '');
      //
      Exit;
    end;
    Item := 0;
    while not Eof(F) do
    begin
      Application.ProcessMessages;
      if FParar then
        Exit;
      Readln(F, Linha);
      Linha := Geral.Substitui(Linha, #9, ' ');
      Linha := Geral.Substitui(Linha, #10, ' ');
      Linha := Geral.Substitui(Linha, #13, ' ');
      Linha := Geral.Substitui(Linha, '  ', ' ');
      Linha := ';' + Linha + ';';
      FSLCampos := Geral.Explode3(Linha, ';', _Colunas);
      //
      Segmento     := FSLCampos[0];
      Divisao      := FSLCampos[1];
      SubDivisao   := FSLCampos[2];
      Nome         := FSLCampos[3];
      Natureza     := FSLCampos[4];
      Propriedades := FSLCampos[5];
      pH           := FSLCampos[6];
      CargaIonica  := FSLCampos[7];
      Aspecto      := FSLCampos[8];
      Utilizacao   := FSLCampos[9];
      Perc_Uso     := FSLCampos[10];
      ConcOuAtiv   := FSLCampos[11];
      SolidezLuz   := FSLCampos[12];
      LocalPDF     := FSLCampos[13];
      //
      //
      if (Nome <> '') or (SubDivisao <> '') then
      begin
        Item := Item + 1;
        AdicionaItemSG(Item, Segmento, Divisao, SubDivisao, Nome, Natureza,
          Propriedades, pH, CargaIonica, Aspecto, Utilizacao, Perc_Uso,
          ConcouAtiv, SolidezLuz, LocalPDF);
        UpdateInfo(Item);
      end;
    end;
  end;
  //
  procedure CarregaCSV4LinhasProdutoNatureza();
  var
    Linha_1, Linha_2, Linha_3, Linha_4: String;
  begin
    Readln(F, Linha);
    Segmento     := 'DV';
    Divisao      := 'Auxiliares';
    SubDivisao   := 'Auxiliares';
    Propriedades := '';
    pH           := '';
    CargaIonica  := '';
    Aspecto      := '';
    Utilizacao   := '';
    Perc_Uso     := '';
    ConcouAtiv   := '';
    SolidezLuz   := '';
    LocalPDF     := '';
    //
    Item := 0;
    while not Eof(F) do
    begin
      Application.ProcessMessages;
      if FParar then
        Exit;
      Readln(F, Linha_1);
      Readln(F, Linha_2);
      Readln(F, Linha_3);
      Readln(F, Linha_4);
      //
      Nome         := Linha_1;
      Natureza     := Linha_3;
      //
      Application.ProcessMessages;
      Item := Item + 1;
      AdicionaItemSG(Item, Segmento, Divisao, SubDivisao, Nome, Natureza,
        Propriedades, pH, CargaIonica, Aspecto, Utilizacao, Perc_Uso,
        ConcouAtiv, SolidezLuz, LocalPDF);
    end;
  end;

  procedure CarregaCSV_NomeNatureza();
  begin
    Readln(F, Linha);
    Linha := ';' + Linha + ';';
    FSLCampos := Geral.Explode3(Linha, ';', _Colunas);

    if (Uppercase(FSLCampos[0]) <> Uppercase('Segmento'     ))
    or (Uppercase(FSLCampos[1]) <> Uppercase('Divisao'      ))
    or (Uppercase(FSLCampos[2]) <> Uppercase('SubDivisao'   ))
    or (Uppercase(FSLCampos[3]) <> Uppercase('Nome|Natureza')) then
    begin
      Geral.MB_Aviso('T�tulos de colunas n�o conferem com:' + sLineBreak +
      'Segmento'     + sLineBreak +
      'Divisao'      + sLineBreak +
      'SubDivisao'   + sLineBreak +
      'Nome|Natureza'+ sLineBreak +
      '');
      //
      Exit;
    end;
    Item := 0;
    MulLinIdx := 0;
    while not Eof(F) do
    begin
      Application.ProcessMessages;
      if FParar then
        Exit;
      Readln(F, Linha);
      Linha := ';' + Linha + ';';
      FSLCampos := Geral.Explode3(Linha, ';', _Colunas);
      //
      MulLinIdx    := MulLinIdx + 1;
      if MulLinIdx > 2 then
        MulLinIdx  := 1;
      if MulLinIdx = 1 then
      begin
        Segmento     := FSLCampos[0];
        Divisao      := FSLCampos[1];
        SubDivisao   := FSLCampos[2];
        Nome         := FSLCampos[3];
        Natureza     := ''; //FSLCampos[4];
        Propriedades := ''; //FSLCampos[5];
        pH           := ''; //FSLCampos[6];
        CargaIonica  := '';
        Aspecto      := ''; //FSLCampos[7];
        Utilizacao   := '';
        Perc_Uso     := ''; //FSLCampos[8];
        ConcOuAtiv   := ''; //FSLCampos[9];
        SolidezLuz   := '';
        LocalPDF     := ''; //FSLCampos[10];
      end else
      begin
        Natureza     := FSLCampos[3];
        Item := Item + 1;
        if (Nome <> '') or (SubDivisao <> '') then
        begin
          AdicionaItemSG(Item, Segmento, Divisao, SubDivisao, Nome, Natureza,
            Propriedades, pH, CargaIonica, Aspecto, Utilizacao, Perc_Uso,
            ConcouAtiv, SolidezLuz, LocalPDF);
          //
          UpdateInfo(Item);
        end;
      end;
      //
    end;
  end;
  procedure CarregaCSV_MultiLinhas();
  begin
    Readln(F, Linha);
    Linha := ';' + Linha + ';';
    FSLCampos := Geral.Explode3(Linha, ';', _Colunas);

    if (Uppercase(FSLCampos[0]) <> Uppercase('Segmento'     ))
    or (Uppercase(FSLCampos[1]) <> Uppercase('Divisao'      ))
    or (Uppercase(FSLCampos[2]) <> Uppercase('SubDivisao'   ))
    or (Uppercase(FSLCampos[3]) <> Uppercase('Nome'         ))
    or (Uppercase(FSLCampos[4]) <> Uppercase('Natureza'     ))
    or (Uppercase(FSLCampos[5]) <> Uppercase('Propriedades' ))
    or (Uppercase(FSLCampos[6]) <> Uppercase('pH'           ))
    or (Uppercase(FSLCampos[7]) <> Uppercase('CargaIonica'  ))
    or (Uppercase(FSLCampos[8]) <> Uppercase('Aspecto'      ))
    or (Uppercase(FSLCampos[9]) <> Uppercase('Utilizacao'   ))
    or (Uppercase(FSLCampos[10]) <> Uppercase('Perc_Uso'    ))
    or (Uppercase(FSLCampos[11]) <> Uppercase('ConcOuAtiv'  ))
    or (Uppercase(FSLCampos[12]) <> Uppercase('SolidezLuz'  ))
    or (Uppercase(FSLCampos[13]) <> Uppercase('LocalPDF'    )) then
    begin
      Geral.MB_Aviso('T�tulos de colunas n�o conferem com:' + sLineBreak +
      'Segmento'     + sLineBreak +
      'Divisao'      + sLineBreak +
      'SubDivisao'   + sLineBreak +
      'Nome'         + sLineBreak +
      'Natureza'     + sLineBreak +
      'Propriedades' + sLineBreak +
      'pH'           + sLineBreak +
      'CargaIonica'  + sLineBreak +
      'Aspecto'      + sLineBreak +
      'Utilizacao'   + sLineBreak +
      'Perc_Uso'     + sLineBreak +
      'ConcOuAtiv'   + sLineBreak +
      'SolidezLuz'   + sLineBreak +
      'LocalPDF'     + sLineBreak +
      '');
      //
      Exit;
    end;
    Item := 0;
    //MulLinIdx := 0;
    RegOK := False;
    Segmento     := '';
    Divisao      := '';
    SubDivisao   := '';
    Nome         := '';
    Natureza     := '';
    Propriedades := '';
    pH           := '';
    CargaIonica  := '';
    Aspecto      := '';
    Utilizacao   := '';
    Perc_Uso     := '';
    ConcOuAtiv   := '';
    SolidezLuz   := '';
    LocalPDF     := '';
    while not Eof(F) do
    begin
      Application.ProcessMessages;
      if FParar then
        Exit;
      Readln(F, Linha);
      Linha := ';' + Linha + ';';
      FSLCampos := Geral.Explode3(Linha, ';', _Colunas);
      //
      MulLinIdx    := MulLinIdx + 1;
      if MulLinIdx > 2 then
        MulLinIdx  := 1;
      if MulLinIdx = 1 then
      begin
        //if Trim(FSLCampos[0]) <> '' then
          Segmento     := Segmento + Trim(FSLCampos[0]);
        //if Trim(FSLCampos[1]) <> '' then
          Divisao      := Divisao + Trim(FSLCampos[1]);
        //if Trim(FSLCampos[2]) <> '' then
          SubDivisao   := SubDivisao + Trim(FSLCampos[2]);
        if Trim(FSLCampos[3]) <> '' then
        begin
          Nome         := FSLCampos[3];
          RegOK        := True;
        end;
        if Trim(FSLCampos[4]) <> '' then
          Natureza     := Natureza + ' ' + Trim(FSLCampos[4]);
        if Trim(FSLCampos[5]) <> '' then
          Propriedades     := Propriedades + ' ' + Trim(FSLCampos[5]);
        if Trim(FSLCampos[6]) <> '' then
          pH     := pH + ' ' + Trim(FSLCampos[6]);
        if Trim(FSLCampos[7]) <> '' then
          CargaIonica := CargaIonica + ' ' + Trim(FSLCampos[7]);
        if Trim(FSLCampos[8]) <> '' then
          Aspecto     := Aspecto + ' ' + Trim(FSLCampos[8]);
        if Trim(FSLCampos[9]) <> '' then
          Utilizacao     := Utilizacao+ ' ' + Trim(FSLCampos[9]);
        if Trim(FSLCampos[10]) <> '' then
          Perc_Uso     := Perc_Uso + ' ' + Trim(FSLCampos[10]);
        if Trim(FSLCampos[11]) <> '' then
          ConcouAtiv     := ConcOuAtiv + ' ' + Trim(FSLCampos[11]);
        if Trim(FSLCampos[12]) <> '' then
          SolidezLuz     := SolidezLuz + ' ' + Trim(FSLCampos[12]);
        if Trim(FSLCampos[13]) <> '' then
          LocalPDF     := LocalPDF + ' ' + Trim(FSLCampos[13]);
        if (RegOK = True) and (
          (Nome <> '') or
          (SubDivisao <> '')
        ) then
        begin
          Item := Item + 1;
          //
          AdicionaItemSG(Item, Segmento, Divisao, SubDivisao, Nome, Natureza,
            Propriedades, pH, CargaIonica, Aspecto, Utilizacao, Perc_Uso,
            ConcouAtiv, SolidezLuz, LocalPDF);
          //
          UpdateInfo(Item);
          //
          RegOK        := False;
          (*
          Segmento     := '';
          Divisao      := '';
          SubDivisao   := '';
          *)
          Nome         := '';
          Natureza     := '';
          Propriedades := '';
          pH           := '';
          cargaIonica  := '';
          Aspecto      := '';
          Utilizacao   := '';
          Perc_Uso     := '';
          ConcOuAtiv   := '';
          SolidezLuz   := '';
          LocalPDF     := '';
        end;
      end;
      //
    end;
  end;
  //
begin
  Cento := 100;
  //
  SQLType := stIns;
  FParar := False;
  //I := 0;
  Arq := Trim(EdPath.Text);
  if FileExists(Arq) then
  begin
    AssignFile(F, Arq);
    try
      Reset(F);
      FilTam := FileSize(F);
      FSLCampos := TStringList.Create;
      try
        PB1.Position := 0;
        PB1.Max := 100;
        //
        Readln(F, Linha);
        while pos(';', Linha) > 0 do
          Linha := Geral.Substitui(Linha, ';', '');
        //
        //if Uppercase(Linha) = Uppercase('Unipelli') then
        //if Uppercase(Linha) = Uppercase('Tanquimica') then
        //if Uppercase(Linha) = Uppercase('Prime') then
        if Uppercase(Linha) = Uppercase('Nome|Natureza') then
          CarregaCSV_NomeNatureza()
        else
        if Uppercase(Linha) = Uppercase('CSV MultiLinhas') then
          CarregaCSV_MultiLinhas()
        else
        if Uppercase(Linha) = Uppercase('CSV Padrao') then
          CarregaCSV_Padrao()
        else
        if Uppercase(Linha) = Uppercase('CSV 2 linhas Produto Natureza') then
          CarregaCSV4LinhasProdutoNatureza()
        else
          Geral.MB_Aviso('Arquivo n�o implementado! Cabe�alho: ' + Linha);
        //
        FilPos := FilePos(F);
        FilPer := (FilPos / FilTam) * 100;
        PB1.Position := Trunc(FilPer);
        MyObjects.Informa2(LaAviso1, LaAviso2, False,
        'Carregamento finalizada! ' +
        Geral.FF0(Item) + ' registros carregados. ' +
        Copy(FloatToStr(FilPer), 1, 5) + '%.');
        //
      finally
        FSLCampos.Free;
      end;
    finally
      CloseFile(f);
    end;
  end;
end;

procedure TFmLoadInsum.CarregaProdutosMK();
const
  pH = '';
  CargaIonica = '';
  Aspecto = '';
  Utilizacao = '';
  Perc_Uso = '';
  ConcouAtiv = '';
  SolidezLuz = '';
  LocalPDF = '';
var
  Arquivo, Linha, LMin1, LMin2: String;
  I, J, Num: Integer;
  //
  Arq, sDataI, sDataF: String;
  GruLoad, Codigo: Integer;
  SQLType: TSQLType;
  DataI, DataF: TDateTime;
  Linhas: array of String;
  //
  Item, ColAtivo: Integer;
  Segmento, Divisao, SubDivisao, Nome, Composicao, Propriedades: String;
  //
begin
  ColAtivo := 0;
  //
  DataI := Date();
  DataF := 0;
  Cento := 100;
  //
  SQLType := stIns;
  FParar := False;
  I := 0;
  Arq := Trim(EdPath.Text);
  if FileExists(Arq) then
  begin
    Item := 0;
    Segmento     := '';
    Divisao      := '';
    SubDivisao   := '';
    Nome         := '';
    Composicao   := '';
    Propriedades := '';
    //
    AssignFile(F, Arq);
    try
      Reset(F);
      FilTam := FileSize(F);
      FSLCampos := TStringList.Create;
      try
        PB1.Position := 0;
        PB1.Max := 100;

        while not Eof(F) do
        begin
          Application.ProcessMessages;
          if FParar then
            Exit;
          //I := I + 1;
          Readln(F, Linha);
          Memo1.Lines.Add(Linha);
          I := Memo1.Lines.Count;
          //
          if Uppercase(Linha) = Uppercase('Selecione') then
          begin
            AdicionaItemSG(Item, Segmento, Divisao, SubDivisao, Nome, Composicao,
              Propriedades, pH, CargaIonica, Aspecto, Utilizacao, Perc_Uso,
              ConcouAtiv, SolidezLuz, LocalPDF);
            //
            Item := Item + 1;
            SubDivisao   := Memo1.Lines[I-3] + Memo1.Lines[I-2];
            Divisao      := SubDivisao;
            Nome         := '';
            Composicao   := '';
            Propriedades := '';
            //
            ColAtivo     := 2;
          end else
          if (ColAtivo = 2) and (Nome = '') then
          begin
            Nome := Trim(Linha);
            //ColAtivo := 3;
          end else
          if (ColAtivo = 2) and (Uppercase(Linha) = Uppercase('Composi��o')) then
          begin
            ColAtivo := 3;
          end else
          if (ColAtivo = 3) and (Composicao = '') then
          begin
            Composicao := Trim(Linha);
            //ColAtivo := 4;
          end else
          if (ColAtivo = 3) and (Uppercase(Linha) = Uppercase('Propriedades')) then
          begin
            ColAtivo := 4;
          end else
          if (ColAtivo = 4) and (Propriedades = '') then
          begin
            Propriedades := Trim(Linha);
            //ColAtivo := 4;
          end;
          UpdateInfo(I);
        end;
        //
        AdicionaItemSG(Item, Segmento, Divisao, SubDivisao, Nome, Composicao,
          Propriedades, pH, CargaIonica, Aspecto, Utilizacao, Perc_Uso,
          ConcouAtiv, SolidezLuz, LocalPDF);
        //
        FilPos := FilePos(F);
        FilPer := (FilPos / FilTam) * 100;
        PB1.Position := Trunc(FilPer);
        MyObjects.Informa2(LaAviso1, LaAviso2, False,
        'Importa��o finalizada! ' +
        Geral.FF0(I) + ' registros importados. ' +
        Copy(FloatToStr(FilPer), 1, 5) + '%.');
        //
      finally
        FSLCampos.Free;
      end;
    finally
      CloseFile(f);
    end;
  end;
end;

procedure TFmLoadInsum.CarregaProdutosPdfCorium();
  var
    I: Integer;
  //
  procedure PesquisaDoDiretrioSelecionado(Cam: String);
  const
    SubDirs = True;
  var
    II: Integer;
  begin
    II := Length(Cam);
    if pos('.', Cam) = 0 then
    begin
      if Cam[II] <>  '\' then
        Cam := Cam + '\';
    end else
    begin
      Cam := ExtractFileDir(Cam);
    end;
    if not DirectoryExists(Cam) then
    begin
      Geral.MB_Aviso('Diret�rio n�o existe: ' + Cam);
      Exit;
    end;
    Cam := Cam + '*.pdf';
    ListBox04.Clear;
    MyObjects.LimpaGrade(SGSales, 1, 1, True);
    dmkPF.GetAllFiles(SubDirs, Cam, ListBox04, False);
    if ListBox04.Items.Count = 0 then
    begin
      Geral.MB_Aviso('Diret�rio vazio: ' + Cam);
      Exit;
    end;
  end;
  //
  function GeraArquivoTxt(ArquivoPDF: String): String;
  var
    ArqOri, ArqDst, StrExec: String;

  begin
    ArqOri := ExtractShortPathName(ArquivoPDF);
    ArqDst := Geral.Substitui(ArqOri, '.pdf', '.txt');
    Result := ArqDst;
    //
    if FileExists(ArqDst) then
      Exit
    else
    begin
      StrExec := 'C:\Arcano\xpdf-tools-win-4.00\bin64\pdftotext.exe "' + ArqOri +
      '" "' + ArqDst + '"';
      MyObjects.ExecutaCmd(StrExec, Memo1);
    end;
    //
    //pdftotext.exe C:\Arcano\xpdf-tools-win-4.00\bin64\Teste\CORIPOL GU .pdf C:\Arcano\xpdf-tools-win-4.00\bin64\Teste\x.txt
    //C:\Arcano\xpdf-tools-win-4.00\bin64>pdftotext.exe "C:\Arcano\xpdf-tools-win-4.00\bin64\Teste\CORIPOL GU .pdf" "C:\Arcano\xpdf-tools-win-4.00\bin64\Teste\x2.txt"
  end;
var
  J: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    PesquisaDoDiretrioSelecionado(EdPath.Text);
    for J := 0 to ListBox04.Items.Count -1 do
    begin
      GeraArquivoTxt(ListBox04.Items[J]);
      //
      // Parei aqui!
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmLoadInsum.CarregaProdutosTxtLF();
var
  Arq, Linha, S, Texto: String;
  P, I, Col: Integer;
begin
  I := 0;
  Cento := 100;
  //
  //SQLType := stIns;
  FParar := False;
  //
  Arq := Trim(EdPath.Text);
  if FileExists(Arq) then
  begin
    AssignFile(F, Arq);
    try
      Reset(F);
      FilTam := FileSize(F);
      FSLCampos := TStringList.Create;
      try
        PB1.Position := 0;
        PB1.Max := 100;
        //
        while not Eof(F) do
        begin
          Readln(F, Linha);
          P := pos(']', Linha);
          S := Copy(Linha, 2, P-2);
          if S = '0' then
          begin
            I := I + 1;
            SGSales.RowCount := I + 1;
          end;
          Texto := Copy(Linha, P + 1);
          Col := Geral.IMV(S);
          case Col of
            0:
            begin
              SGSales.Cells[0, I] := Geral.FF0(I);
              if S = '0' then
                SGSales.Cells[1, I] := Texto;
            end;
            1:
            begin
              SGSales.Cells[2, I] := Texto;
              SGSales.Cells[3, I] := Texto;
              SGSales.Cells[4, I] := Texto;
            end;
            2:
            begin
              SGSales.Cells[5, I] := Texto;
            end;
            3:
            begin
              SGSales.Cells[6, I] := Texto;
            end;
            4:
            begin
              SGSales.Cells[7, I] := Texto;
            end;
            5:
            begin
              SGSales.Cells[8, I] := Texto;
            end;
            6:
            begin
              SGSales.Cells[10, I] := Texto;
            end;
          end;
        end;

{
        while pos(';', Linha) > 0 do
          Linha := Geral.Substitui(Linha, ';', '');
        //
        if Uppercase(Linha) = Uppercase('Nome|Natureza') then
          CarregaCSV_NomeNatureza()
        else
        if Uppercase(Linha) = Uppercase('CSV MultiLinhas') then
          CarregaCSV_MultiLinhas()
        else
        if Uppercase(Linha) = Uppercase('CSV Padrao') then
          CarregaCSV_Padrao()
        else
        if Uppercase(Linha) = Uppercase('CSV 2 linhas Produto Natureza') then
          CarregaCSV4LinhasProdutoNatureza()
        else
          Geral.MB_Aviso('Arquivo n�o implementado! Cabe�alho: ' + Linha);
        //
        FilPos := FilePos(F);
        FilPer := (FilPos / FilTam) * 100;
        PB1.Position := Trunc(FilPer);
        MyObjects.Informa2(LaAviso1, LaAviso2, False,
        'Carregamento finalizada! ' +
        Geral.FF0(Item) + ' registros carregados. ' +
        Copy(FloatToStr(FilPer), 1, 5) + '%.');
        //
}
      finally
        FSLCampos.Free;
      end;
    finally
      CloseFile(f);
    end;
  end;
  //
end;

procedure TFmLoadInsum.CarregaProdutosURLAtlas();
  //
  //
(*

      <td width="76" align="left" valign="top" class="tablebodycopy">Sulphated Natural & Synthetic Fatliquors</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">EUREKA� 678</td>
      <td width="42" align="center" valign="top"><a href="#" title="" onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Sulphated%20Natural%20&%20Synthetic%20Fatliquors/Eureka%20678-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Spanish/Sulphated%20Natural%20&%20Synthetic%20Fatliquors/Eureka%20678-s.pdf'>Spanish</a></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Sulphated%20Natural%20&%20Synthetic%20Fatliquors/Eureka%20678-c.pdf'>Chinese</a></td>




      <td width="76" align="left" valign="top" class="tablebodycopy">Sulphated Natural & Synthetic Fatliquors</td>
      <td width="119" align="left" valign="top" class="tablebodycopy">ATLASOL� 178</td>
      <td width="42" align="center" valign="top"><a href="#" title="A compounded fatliquor of high quality sulphated neatsfoot and synthetic sperm oil. Designed for fatliquoring upper leathers where a greater resistance to discoloration by light is required." onClick="return false;" class="tablebodycopy">Details</a></td>
      <td width="83" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="67" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="59" align="center" valign="top" class="tablebodycopy">No</td>
      <td width="63" align="center" valign="top" class="tablebodycopy">Yes</td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/English/Sulphated%20Natural%20&%20Synthetic%20Fatliquors/Atlasol%20178-e.pdf'>English</a></td>
      <td width="17" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Spanish/Sulphated%20Natural%20&%20Synthetic%20Fatliquors/Atlasol%20178-s.pdf'>Spanish</a></td>
      <td width="52" align="center" valign="top" class="tablebodycopy"><a target='_blank' href='database/Chinese/Sulphated%20Natural%20&%20Synthetic%20Fatliquors/Atlasol%20178-c.pdf'>Chinese</a></td>
*)
  procedure CarregaGrade(const Linhas: String; var NLin: Integer);
  var
    Extraido, Restante, Texto, Lixo, aTemp, Artigos, aLink, Fonte, DestFile, Dir: String;
    OK: Boolean;
    P, I: Integer;
  begin
    Texto := Linhas;
    Restante := '';//Texto;
    OK := True;
    I := 0;
    Artigos := '';
    while OK do
    begin
      I := I + 1;
      //if  <a href="#" title="" onClick="
      OK := ExtractText(Texto, 'class="tablebodycopy">', '</td>', P,
      Extraido, Restante);
      if  Uppercase(Extraido) = Uppercase('Details</a>') then
      begin
        OK := ExtractText(Texto, '<a href="#" title="', '" onClick="', P,
        Extraido, Lixo);
      end;
      Texto := Restante;
      Extraido := Trim(Extraido);
      if Extraido <> '' then
      begin
    //Geral.MB_Info(Extraido);
        if I = 1 then
        begin
          NLin := NLin + 1;
          SGSales.RowCount := NLin + 1;
          SGSales.Cells[0, NLin-1] := Geral.FF0(NLin);
        end;
        case I of
          1:
          begin
            SGSales.Cells[2, NLin-1] := Extraido;
            SGSales.Cells[3, NLin-1] := Extraido;
            SGSales.Cells[4, NLin-1] := Extraido;
          end;
          2: SGSales.Cells[5, NLin-1] := Extraido;
          3: SGSales.Cells[6, NLin-1] := Extraido;
          4..8:
          begin
            if Uppercase(Extraido) = Uppercase('YES') then
            case I of
              4: Artigos := Artigos + ' Upper Shoe Leather ';
              5: Artigos := Artigos + ' Furniture Upholstery ';
              6: Artigos := Artigos + ' Automotive Upholstery ';
              7: Artigos := Artigos + ' Garments ';
              8: Artigos := Artigos + ' Gloving ';
            end;
          end;
          9:
          begin
            Artigos := Trim(Artigos);
            Artigos := Geral.Substitui(Artigos, '  ', ' / ');
            if Artigos <> '' then
              SGSales.Cells[7, NLin-1] := Artigos;
            if RGPDF.ItemIndex = 1 then
            begin
              begin
                OK := ExtractText(Extraido, 'database/English/', '''>English</a>', P, aLink, Lixo);
                if OK then
                begin
                  //SGSales.Cells[?, NLin-1] := aLink; // '<a target=''_blank'' href=''database/English/Sulphated%20Natural%20&%20Synthetic%20Fatliquors/Eureka%20678-e.pdf''>English</a>'
                  try
                    Fonte    := 'https://www.atlasrefinery.com/database/English/' + aLink;
                    OK := True;
                    aTemp := aLink;
                    DestFile := '';
                    while OK do
                    begin
                      if Pos('/', aTemp) > 0 then
                      begin
                        OK := ExtractText(aTemp, '/', '.pdf', P, DestFile, Lixo);
                        if DestFile <> '' then
                          DestFile := DestFile + '.pdf';
                        aTemp := DestFile;
                      end else
                      begin
                        OK := False;
                        aTemp := '';
                      end;
                    end;
                    DestFile := Trim(Geral.Substitui(DestFile, '%20', ' '));
                    if Trim(Extraido) <> '' then
                    begin
                      ForceDirectories(InitDir + 'Atlas');
                      DestFile := InitDir + 'Atlas\' + DestFile;
                      OK := False;
                      if not FileExists(DestFile) then
                        OK := DmkWeb.DownloadFile(Fonte, DestFile)
                      else
                        Ok := True;
                    end else
                      OK := True;
                    if OK then
                      SGSales.Cells[ColPDF, NLin-1] := DestFile;
                  except
                    ;//
                  end;
                end;
              end;
            end;
          end;
        end;
      end;
    end; // �
  end;

  //


const
  Fonte = 'https://www.atlasrefinery.com/products-results.php#';
  //DestFile = 'C:\Dermatek\Teste.txt';
  DestFile = 'C:\Arcano\Teste.txt';
var
  Extraido, Restante, Texto: String;
  OK: Boolean;
  P, N: Integer;
begin
  //Fonte := Edit1.Text;
  if FileExists(DestFile) then
    DeleteFile(DestFile);
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Baixando arquivo!');
  Application.ProcessMessages;
  if DmkWeb.DownloadFile(Fonte, DestFile) then
  begin
    Memo1.Lines.LoadFromFile(destFile);
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Arquivo baixado!');
  end
  else
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Erro durante o download de "' + Fonte + '"');
    Exit;
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Carregando dados ');
  //
  Texto := Memo1.Text;
  Restante := '';//Texto;
  OK := True;
  N := 1;
  while OK do
  begin
    OK := ExtractText(Texto, '<tr class="tablecopy">', '</tr>', P,
    Extraido, Restante);
    Texto := Restante;
    //Geral.MB_Info(Extraido);
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Carregando dados ' + Geral.FF0(N));
    CarregaGrade(Extraido, N);
  end; // �
  MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Dados carregados na grade');
end;

procedure TFmLoadInsum.CarregaProdutosURLCorichem();
const
  DestFile = 'C:\Arcano\Teste.txt';
  pH          = '';
  CargaIonica = '';
  Aspecto     = '';
  Utilizacao  = '';
  Perc_Uso    = '';
  ConcouAtiv  = '';
  SolidezLuz  = '';
  LocalPDF    = '';
var
  Nome, Natureza, Propriedades, Segmento, Divisao, SubDivisao: String;
  Extraido, Restante, Texto, Fonte: String;
  OK: Boolean;
  P, N, It: Integer;

  //

  procedure PesquisaProduto(URL, NomePQ: String);
  var
    ArrTexto: array of String;
    ArrI, ArrJ, TryN: Integer;
  begin
  {
  href="http://www.corichem.it/wet-products/
  href="http://www.corichem.it/tanning
  }
    //Fonte := 'http://www.corichem.it/wet-products/';
    Fonte := URL;
    //
    if FileExists(DestFile) then
      DeleteFile(DestFile);
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Baixando arquivo!');
    Application.ProcessMessages;
    // Tentativa 1
    if DmkWeb.DownloadFile(Fonte, DestFile) then
    begin
      Memo1.Lines.LoadFromFile(destFile);
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Arquivo baixado!');
    end
    else
    begin
      // Tentativa 2
      if DmkWeb.DownloadFile(Fonte, DestFile) then
      begin
        Memo1.Lines.LoadFromFile(destFile);
        MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Arquivo baixado!');
      end
      else
      begin
        // Tentativa 3
        if DmkWeb.DownloadFile(Fonte, DestFile) then
        begin
          Memo1.Lines.LoadFromFile(destFile);
          MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Arquivo baixado!');
        end
        else
        begin
          MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Erro durante o download de "' + Fonte + '"');
          ListBox04.Items.Add('Erro no download de "' + Fonte + '"');
          Exit;
        end;
      end;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Carregando dados ');
    //
    Texto := Memo1.Text;
    Restante := '';//Texto;
    OK := True;
    N := 1;
    ArrI := 0;
    while OK do
    begin
      OK := ExtractText(Texto, '<p style="text-align: justify;">', '</p>', P,
      Extraido, Restante);
      Texto := Restante;
      //Geral.MB_Info(URL + sLineBreak + NomePQ + sLineBreak + Extraido);
      if Trim(Extraido) <> '' then
      begin
        ArrI := ArrI+ 1;
        SetLength(ArrTexto, ArrI);
        ArrTexto[ArrI-1] := ArrumaTextoHTML(Extraido);
      end;
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Carregando dados ' + Geral.FF0(N));
      //CarregaGrade(Extraido, N);
    end; // �
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Dados carregados na grade');
    //
    Nome := NomePQ;
    Natureza := '';
    Propriedades := '';
    case ArrI of
      0:
      begin
        Natureza := '';
        Propriedades := '';
      end;
      1:
      begin
        Natureza := '';
        Propriedades := ArrTexto[0];
      end;
      2:
      begin
        Natureza := ArrTexto[0];
        Propriedades := ArrTexto[1];
      end;
      3:
      begin
        Natureza := ArrTexto[0];
        Propriedades := ArrTexto[1] + sLineBreak + ArrTexto[2];
      end;
      else
      begin
        Texto := '';
        for ArrJ := 0 to Length(ArrTexto) - 1 do
          Texto := Texto + 'Texto ' + Geral.FF0(ArrJ) + ': ' + ArrTexto[ArrJ] + sLineBreak;
        //
        Geral.MB_Aviso('"ArrTexto['+ Geral.FF0(ArrI) + '] n�o implementado!' +
        sLineBreak + Texto);
      end;
    end;
    It := It + 1;
    AdicionaItemSG(It, Segmento, Divisao, SubDivisao, Nome, Natureza,
      Propriedades, pH, CargaIonica, Aspecto, Utilizacao, Perc_Uso,
      ConcouAtiv, SolidezLuz, LocalPDF);
    //UpdateInfo(It);
  end;
begin
  ListBox04.Items.Clear;
  It := 0;
  //
  Segmento := 'BH';
  Divisao  := 'Chemicals for beamhouse';
  SubDivisao := Divisao;
  //
  PesquisaProduto('http://www.corichem.it/wet-products/chemicals-for-beamhouse/basificante-tb', 'Basificante TB');
	PesquisaProduto('http://www.corichem.it/wet-products/chemicals-for-beamhouse/corilime-h/', 'Corilime H');
	PesquisaProduto('http://www.corichem.it/wet-products/chemicals-for-beamhouse/corilime-snt/', 'Corilime SNT');
	PesquisaProduto('http://www.corichem.it/wet-products/chemicals-for-beamhouse/corilime-vtn/', 'Corilime VTN');
	PesquisaProduto('http://www.corichem.it/wet-products/chemicals-for-beamhouse/coripickel-ecop1/', 'Coripickel ECO/P1');
	PesquisaProduto('http://www.corichem.it/wet-products/chemicals-for-beamhouse/coripickel-ecop2/', 'Coripickel ECO/P2');
	PesquisaProduto('http://www.corichem.it/wet-products/chemicals-for-beamhouse/coripickel-wla/', 'Coripickel WLA');
	PesquisaProduto('http://www.corichem.it/wet-products/chemicals-for-beamhouse/corizim-p-1000/', 'Corizim P 1000');
	PesquisaProduto('http://www.corichem.it/wet-products/chemicals-for-beamhouse/corizim-re-07/', 'Corizim RE 07');
	PesquisaProduto('http://www.corichem.it/wet-products/chemicals-for-beamhouse/dipcor-erg/', 'Dipcor ERG');
	PesquisaProduto('http://www.corichem.it/wet-products/chemicals-for-beamhouse/dipcor-hrp1/', 'Dipcor HR/P1');
	PesquisaProduto('http://www.corichem.it/wet-products/chemicals-for-beamhouse/dipcor-lm/', 'Dipcor LM');
	PesquisaProduto('http://www.corichem.it/wet-products/chemicals-for-beamhouse/prodotto-el/', 'Prodotto EL');
	PesquisaProduto('http://www.corichem.it/wet-products/chemicals-for-beamhouse/solecal-tzb/', 'Solecal TZ/B');
  //
  Segmento := 'BH';
  Divisao  := 'Penetrators, degreasind, emulsifyng';
  SubDivisao := Divisao;
  //
	PesquisaProduto('http://www.corichem.it/wet-products/penetrators-degreasind-emulsifyng/coranil-g/', 'Coranil G');
	PesquisaProduto('http://www.corichem.it/wet-products/penetrators-degreasind-emulsifyng/coranil-h-70/', 'Coranil H 70');
	PesquisaProduto('http://www.corichem.it/wet-products/penetrators-degreasind-emulsifyng/sapol-ba/', 'Sapol BA');
	PesquisaProduto('http://www.corichem.it/wet-products/penetrators-degreasind-emulsifyng/sapol-doc/', 'Sapol DOC');
	PesquisaProduto('http://www.corichem.it/wet-products/penetrators-degreasind-emulsifyng/sapol-lr/', 'Sapol LR');
	PesquisaProduto('http://www.corichem.it/wet-products/penetrators-degreasind-emulsifyng/sapol-rv/', 'Sapol RV');
	PesquisaProduto('http://www.corichem.it/wet-products/penetrators-degreasind-emulsifyng/sapol-scs/', 'Sapol SCS');
	PesquisaProduto('http://www.corichem.it/wet-products/penetrators-degreasind-emulsifyng/sapol-sgr/', 'Sapol SGR');
  //
  Segmento := 'WE';
  Divisao  := 'Taning and retaning';
  SubDivisao := Divisao;
  //
  PesquisaProduto('http://www.corichem.it/tanning-and-retanning/coretan-e/', 'Coretan E');
  PesquisaProduto('http://www.corichem.it/tanning-and-retanning/coretan-eks/', 'Coretan EK/S');
  PesquisaProduto('http://www.corichem.it/tanning-and-retanning/coretan-ma2/', 'Coretan MA2');
  PesquisaProduto('http://www.corichem.it/tanning-and-retanning/coretan-mmc1/', 'Coretan MMC-1');
  PesquisaProduto('http://www.corichem.it/tanning-and-retanning/coretan-mtsn/', 'Coretan MTSN');
  PesquisaProduto('http://www.corichem.it/tanning-and-retanning/coretan-ns/', 'Coretan NS');
  PesquisaProduto('http://www.corichem.it/tanning-and-retanning/coretan-pwn/', 'Coretan PW/N');
  PesquisaProduto('http://www.corichem.it/tanning-and-retanning/coretan-r3/', 'Coretan R3');
  PesquisaProduto('http://www.corichem.it/tanning-and-retanning/coretan-rcs/', 'Coretan RCS');
  PesquisaProduto('http://www.corichem.it/tanning-and-retanning/coretan-st/', 'Coretan ST');
  PesquisaProduto('http://www.corichem.it/tanning-and-retanning/coretan-tal/', 'Coretan TAL');
  PesquisaProduto('http://www.corichem.it/tanning-and-retanning/coretan-v3/', 'Coretan V3');
  PesquisaProduto('http://www.corichem.it/tanning-and-retanning/sintocrom-brw/', 'Sintocrom BRW');
  PesquisaProduto('http://www.corichem.it/tanning-and-retanning/sintocrom-cr10t/', 'Sintocrom CR10/T');
  PesquisaProduto('http://www.corichem.it/tanning-and-retanning/sintocrom-gd/', 'Sintocrom GD');
  PesquisaProduto('http://www.corichem.it/tanning-and-retanning/sintocrom-sc/', 'Sintocrom SC');
  //
  Segmento := 'WE';
  Divisao  := 'Synthetic tanninins';
  SubDivisao := Divisao;
  //
  PesquisaProduto('http://www.corichem.it/wet-products/synthetic-tanninins/soletan-7s/', 'Soletan 7S');
  PesquisaProduto('http://www.corichem.it/wet-products/synthetic-tanninins/soletan-amt/', 'Soletan AMT');
  PesquisaProduto('http://www.corichem.it/wet-products/synthetic-tanninins/soletan-bbc/', 'Soletan BBC');
  PesquisaProduto('http://www.corichem.it/wet-products/synthetic-tanninins/soletan-bbn/', 'Soletan BBN');
  PesquisaProduto('http://www.corichem.it/wet-products/synthetic-tanninins/soletan-bs6-liq/', 'Soletan BS6 LIQ.');
  PesquisaProduto('http://www.corichem.it/wet-products/synthetic-tanninins/soletan-cr/', 'Soletan CR');
  PesquisaProduto('http://www.corichem.it/wet-products/synthetic-tanninins/soletan-egb/', 'Soletan EGB');
  PesquisaProduto('http://www.corichem.it/wet-products/synthetic-tanninins/soletan-fmc/', 'Soletan FMC');
  PesquisaProduto('http://www.corichem.it/wet-products/synthetic-tanninins/soletan-fs/', 'Soletan FS');
  PesquisaProduto('http://www.corichem.it/wet-products/synthetic-tanninins/soletan-hbls/', 'Soletan HBLS');
  PesquisaProduto('http://www.corichem.it/wet-products/synthetic-tanninins/soletan-mr/', 'Soletan MR');
  PesquisaProduto('http://www.corichem.it/wet-products/synthetic-tanninins/soletan-tx-200/', 'Soletan TX 200');
  PesquisaProduto('http://www.corichem.it/wet-products/synthetic-tanninins/soletan-xgl/', 'Soletan XGL');
  PesquisaProduto('http://www.corichem.it/wet-products/synthetic-tanninins/soletan-xpb/', 'Soletan XP/B');
  //
  Segmento := 'WE';
  Divisao  := 'Fatliquors';
  SubDivisao := Divisao;
  //
  PesquisaProduto('http://www.corichem.it/wet-products/fatliquors/coretan-p47/', 'Coretan P47');
  PesquisaProduto('http://www.corichem.it/wet-products/fatliquors/corfat-cod/', 'Corfat COD');
  PesquisaProduto('http://www.corichem.it/wet-products/fatliquors/corfat-cwf/', 'Corfat CWF');
  PesquisaProduto('http://www.corichem.it/wet-products/fatliquors/corfat-fi/', 'Corfat FI');
  PesquisaProduto('http://www.corichem.it/wet-products/fatliquors/corfat-ftv/', 'Corfat FTV');
  PesquisaProduto('http://www.corichem.it/wet-products/fatliquors/corfat-kf90/', 'Corfat KF90');
  PesquisaProduto('http://www.corichem.it/wet-products/fatliquors/corfat-nl/', 'Corfat NL');
  PesquisaProduto('http://www.corichem.it/wet-products/fatliquors/corfat-omv/', 'Corfat OMV');
  PesquisaProduto('http://www.corichem.it/wet-products/fatliquors/corfat-pc/', 'Corfat PC');
  PesquisaProduto('http://www.corichem.it/wet-products/fatliquors/corfat-pc4k/', 'Corfat PC4/K');
  PesquisaProduto('http://www.corichem.it/wet-products/fatliquors/corfat-pnh/', 'Corfat PN/H');
  PesquisaProduto('http://www.corichem.it/wet-products/fatliquors/corfat-sal/', 'Corfat SAL');
  PesquisaProduto('http://www.corichem.it/wet-products/fatliquors/corfat-thi/', 'Corfat THI');
  PesquisaProduto('http://www.corichem.it/wet-products/fatliquors/corfat-tl1/', 'Corfat TL1');
  PesquisaProduto('http://www.corichem.it/wet-products/fatliquors/corfat-tlq/', 'Corfat TLQ');
  PesquisaProduto('http://www.corichem.it/wet-products/fatliquors/corfat-tmn/', 'Corfat TM/N');
  PesquisaProduto('http://www.corichem.it/wet-products/fatliquors/corfat-xd/', 'Corfat XD');
  PesquisaProduto('http://www.corichem.it/wet-products/fatliquors/corifob-flex/', 'Corifob FLEX');
  PesquisaProduto('http://www.corichem.it/wet-products/fatliquors/corifob-mt/', 'Corifob MT');
  PesquisaProduto('http://www.corichem.it/wet-products/fatliquors/corifob-pmx/', 'Corifob PMX');
  PesquisaProduto('http://www.corichem.it/wet-products/fatliquors/coriliker-blwt/', 'Coriliker BLW/T');
  PesquisaProduto('http://www.corichem.it/wet-products/fatliquors/coriliker-pld/', 'Coriliker PLD');
  PesquisaProduto('http://www.corichem.it/wet-products/fatliquors/coroil-bx2/', 'Coroil BX2');
  PesquisaProduto('http://www.corichem.it/wet-products/fatliquors/coroil-gia/', 'Coroil GI/A');
  PesquisaProduto('http://www.corichem.it/wet-products/fatliquors/coroil-t8/', 'Coroil T8');
  PesquisaProduto('http://www.corichem.it/wet-products/fatliquors/coroil-we/', 'Coroil WE');
  PesquisaProduto('http://www.corichem.it/wet-products/fatliquors/olio-opb', 'Olio OPB');
  //
  Segmento := 'WE';
  Divisao  := 'Auxiliary agents';
  SubDivisao := Divisao;
  //
  PesquisaProduto('http://www.corichem.it/wet-products/ausiliary-agents/antiox-w/', 'Antiox W');
  PesquisaProduto('http://www.corichem.it/wet-products/ausiliary-agents/antischiuma-b16/', 'Antischiuma B/16');
  PesquisaProduto('http://www.corichem.it/wet-products/ausiliary-agents/antistatico-k5/', 'Antistatico K5');
  PesquisaProduto('http://www.corichem.it/wet-products/ausiliary-agents/bianco-per-botte-jp/', 'Bianco per botte JP');
  PesquisaProduto('http://www.corichem.it/wet-products/ausiliary-agents/bianco-per-botte-uc/', 'Bianco per botte UC');
  PesquisaProduto('http://www.corichem.it/tanning-and-retanning/coretan-tt/', 'Coretan TT');
  PesquisaProduto('http://www.corichem.it/wet-products/ausiliary-agents/corifil-sv/', 'Corifil SV');
  PesquisaProduto('http://www.corichem.it/wet-products/ausiliary-agents/corifil-tf/', 'Corifil TF');
  PesquisaProduto('http://www.corichem.it/wet-products/ausiliary-agents/filler-om/', 'Filler OM');
  PesquisaProduto('http://www.corichem.it/wet-products/ausiliary-agents/filler-zr/', 'Filler ZR');
  PesquisaProduto('http://www.corichem.it/wet-products/ausiliary-agents/neutralizzante-ar/', 'Neutralizzante AR');
  PesquisaProduto('http://www.corichem.it/wet-products/ausiliary-agents/neutralizzante-ls1/', 'Neutralizzante LS/1');
  PesquisaProduto('http://www.corichem.it/wet-products/ausiliary-agents/neutralizzante-w6/', 'Neutralizzante W6');
  PesquisaProduto('http://www.corichem.it/wet-products/ausiliary-agents/solacril-26/', 'Solacril 26');
  PesquisaProduto('http://www.corichem.it/wet-products/ausiliary-agents/solfix-fd/', 'Solfix FD');
  //
  Segmento := 'BH';
  Divisao  := 'Biocide agents';
  SubDivisao := Divisao;
  //
  PesquisaProduto('http://www.corichem.it/wet-products/biocide-agents/benisol-b-110/', 'Benisol B 110');
  PesquisaProduto('http://www.corichem.it/wet-products/biocide-agents/benisol-b-300/', 'Benisol B 300');
  PesquisaProduto('http://www.corichem.it/wet-products/biocide-agents/benisol-b-500/', 'Benisol B 500');
  //
  Segmento := 'FI';
  Divisao  := 'Acrylic Binders';
  SubDivisao := Divisao;
  //
  PesquisaProduto('http://www.corichem.it/finishing-products/acrylic-binders/coricryl-31025/', 'Coricryl 310/25');
  PesquisaProduto('http://www.corichem.it/finishing-products/acrylic-binders/coricryl-371/', 'Coricryl 371');
  PesquisaProduto('http://www.corichem.it/finishing-products/acrylic-binders/coricryl-400/', 'Coricryl 400');
  PesquisaProduto('http://www.corichem.it/finishing-products/acrylic-binders/coricryl-530b/', 'Coricryl 530/B');
  PesquisaProduto('http://www.corichem.it/finishing-products/acrylic-binders/coricryl-700/', 'Coricryl 700');
  PesquisaProduto('http://www.corichem.it/finishing-products/acrylic-binders/coricryl-91225/', 'Coricryl 912/25');
  //
  Segmento := 'FI';
  Divisao  := 'Compounds';
  SubDivisao := Divisao;
  //
  PesquisaProduto('http://www.corichem.it/finishing-products/compounds/coribase-150/', 'Coribase 150');
  PesquisaProduto('http://www.corichem.it/finishing-products/compounds/coribase-186/', 'Coribase 186');
  PesquisaProduto('http://www.corichem.it/finishing-products/compounds/coribase-206/', 'Coribase 206');
  PesquisaProduto('http://www.corichem.it/finishing-products/compounds/coribase-204/', 'Coribase 204');
  PesquisaProduto('http://www.corichem.it/finishing-products/compounds/coribase-207/', 'Coribase 207');
  PesquisaProduto('http://www.corichem.it/finishing-products/compounds/coribase-300/', 'Coribase 300');
  PesquisaProduto('http://www.corichem.it/finishing-products/compounds/coribase-301/', 'Coribase 301');
  PesquisaProduto('http://www.corichem.it/finishing-products/compounds/coriground-cm-71/', 'Coriground CM 71');
  PesquisaProduto('http://www.corichem.it/finishing-products/compounds/coriground-cs58g/', 'Coriground CS58/G');
  PesquisaProduto('http://www.corichem.it/finishing-products/compounds/coriground-cxb/', 'Coriground CXB');
  PesquisaProduto('http://www.corichem.it/finishing-products/compounds/coriground-gt-203/', 'Coriground GT 203');
  PesquisaProduto('http://www.corichem.it/finishing-products/compounds/coriground-xr18/', 'Coriground XR/18');
  PesquisaProduto('http://www.corichem.it/finishing-products/compounds/coriground-xr23/', 'Coriground XR/23');
  PesquisaProduto('http://www.corichem.it/finishing-products/compounds/coriground-xr185/', 'Coriground XR/185');
  //
  Segmento := 'FI';
  Divisao  := 'Polyurethanes';
  SubDivisao := Divisao;
  //
  PesquisaProduto('http://www.corichem.it/finishing-products/polyurethanes/coripur-119/', 'Coripur 119');
  PesquisaProduto('http://www.corichem.it/finishing-products/polyurethanes/coripur-148m/', 'Coripur 148/M');
  PesquisaProduto('http://www.corichem.it/finishing-products/polyurethanes/coripur-165/', 'Coripur 165');
  PesquisaProduto('http://www.corichem.it/finishing-products/polyurethanes/coripur-ad12/', 'Coripur AD12');
  PesquisaProduto('http://www.corichem.it/finishing-products/polyurethanes/coripur-ag20/', 'Coripur AG20');
  PesquisaProduto('http://www.corichem.it/finishing-products/polyurethanes/coripur-ag20-plus/', 'Coripur AG20 PLUS');
  PesquisaProduto('http://www.corichem.it/finishing-products/polyurethanes/coripur-k-180/', 'Coripur K 180');
  PesquisaProduto('http://www.corichem.it/finishing-products/polyurethanes/coripur-n220/', 'Coripur N220');
  PesquisaProduto('http://www.corichem.it/finishing-products/polyurethanes/coripur-prc/', 'Coripur PRC');
  PesquisaProduto('http://www.corichem.it/finishing-products/polyurethanes/coripur-tr650/', 'Coripur TR650');
  //
  Segmento := 'FI';
  Divisao  := 'Nitroemulsions';
  SubDivisao := Divisao;
  //
  PesquisaProduto('http://www.corichem.it/finishing-products/nitroemulsions/corigloss-sw-31/', 'Corigloss SW 31');
  PesquisaProduto('http://www.corichem.it/finishing-products/nitroemulsions/corigloss-w-12/', 'Corigloss W 12');
  PesquisaProduto('http://www.corichem.it/finishing-products/nitroemulsions/corigloss-w-14/', 'Corigloss W 14');
  PesquisaProduto('http://www.corichem.it/finishing-products/nitroemulsions/corigloss-w-K/', 'Corigloss W K');
  PesquisaProduto('http://www.corichem.it/finishing-products/nitroemulsions/corimatt-w-18/', 'Corimatt W 18');
  PesquisaProduto('http://www.corichem.it/finishing-products/nitroemulsions/corimatt-w-312/', 'Corimatt W 312');
  PesquisaProduto('http://www.corichem.it/finishing-products/nitroemulsions/corimatt-w-380/', 'Corimatt W 380');
  //
  Segmento := 'FI';
  Divisao  := 'Protein Binders';
  SubDivisao := Divisao;
  //
  PesquisaProduto('http://www.corichem.it/finishing-products/protein-binders/corilex-cma/', 'Corilex CM/A');
  PesquisaProduto('http://www.corichem.it/finishing-products/protein-binders/corilex-co-12/', 'Corilex CO 12');
  PesquisaProduto('http://www.corichem.it/finishing-products/protein-binders/corilex-co-16/', 'Corilex CO 16');
  PesquisaProduto('http://www.corichem.it/finishing-products/protein-binders/corilex-co-32/', 'Corilex CO 32');
  PesquisaProduto('http://www.corichem.it/finishing-products/protein-binders/corilex-co-80/', 'Corilex CO 80');
  PesquisaProduto('http://www.corichem.it/finishing-products/protein-binders/corilex-co-k-20/', 'Corilex CO K 20');
  PesquisaProduto('http://www.corichem.it/finishing-products/protein-binders/corilex-pa-12/', 'Corilex PA 12');
  PesquisaProduto('http://www.corichem.it/finishing-products/protein-binders/corilex-qf/', 'Corilex QF');
  PesquisaProduto('http://www.corichem.it/finishing-products/protein-binders/corilex-tl/', 'Corilex TL');
  //
  Segmento := 'FI';
  Divisao  := 'Waterbased Topcoats';
  SubDivisao := Divisao;
  //
  PesquisaProduto('http://www.corichem.it/finishing-products/waterbased-topcoats/corifix-w-11l/', 'Corifix W 11/L');
  PesquisaProduto('http://www.corichem.it/finishing-products/waterbased-topcoats/corifix-w-12l/', 'Corifix W 12/L');
  PesquisaProduto('http://www.corichem.it/finishing-products/waterbased-topcoats/corifix-w-66op/', 'Corifix W 66/OP');
  PesquisaProduto('http://www.corichem.it/finishing-products/waterbased-topcoats/corifix-w-82op/', 'Corifix W 82/OP');
  PesquisaProduto('http://www.corichem.it/finishing-products/waterbased-topcoats/corifix-w-103op/', 'Corifix W 103/OP');
  PesquisaProduto('http://www.corichem.it/finishing-products/waterbased-topcoats/corifix-w-188op/', 'Corifix W 188/OP');
  PesquisaProduto('http://www.corichem.it/finishing-products/waterbased-topcoats/corifix-w-190l/', 'Corifix W 190/L');
  PesquisaProduto('http://www.corichem.it/finishing-products/waterbased-topcoats/corifix-w-1010l/', 'Corifix W 1010/L');
  PesquisaProduto('http://www.corichem.it/finishing-products/waterbased-topcoats/corifix-w-3000l/', 'Corifix W 3000/L');
  PesquisaProduto('http://www.corichem.it/finishing-products/waterbased-topcoats/corifix-w-3200op/', 'Corifix W 3200/OP');
  PesquisaProduto('http://www.corichem.it/finishing-products/waterbased-topcoats/corifix-w-5400op/', 'Corifix W 5400/OP');
  //
  Segmento := 'FI';
  Divisao  := 'Feel Modifiers';
  SubDivisao := Divisao;
  //
  PesquisaProduto('http://www.corichem.it/finishing-products/feel-modifiers/corifeel-18/', 'Corifeel 18');
  PesquisaProduto('http://www.corichem.it/finishing-products/feel-modifiers/corifeel-19a/', 'Corifeel 19A');
  PesquisaProduto('http://www.corichem.it/finishing-products/feel-modifiers/corifeel-29/', 'Corifeel 29');
  PesquisaProduto('http://www.corichem.it/finishing-products/feel-modifiers/corifeel-815/', 'Corifeel 815');
  PesquisaProduto('http://www.corichem.it/finishing-products/feel-modifiers/corifeel-820/', 'Corifeel 820');
  PesquisaProduto('http://www.corichem.it/finishing-products/feel-modifiers/corifeel-bc/', 'Corifeel BC');
  PesquisaProduto('http://www.corichem.it/finishing-products/feel-modifiers/corifeel-be/', 'Corifeel BE');
  PesquisaProduto('http://www.corichem.it/finishing-products/feel-modifiers/corifeel-bf/', 'Corifeel BF');
  PesquisaProduto('http://www.corichem.it/finishing-products/feel-modifiers/corifeel-bh2/', 'Corifeel BH2');
  PesquisaProduto('http://www.corichem.it/finishing-products/feel-modifiers/corifeel-cae/', 'Corifeel CA/E');
  //
  Segmento := 'FI';
  Divisao  := 'Waxes & Oils';
  SubDivisao := Divisao;
  //
  PesquisaProduto('http://www.corichem.it/finishing-products/waxes-and-oils/corimul-ic/', 'Corimul IC');
  PesquisaProduto('http://www.corichem.it/finishing-products/waxes-and-oils/corimul-q8/', 'Corimul Q8');
  PesquisaProduto('http://www.corichem.it/finishing-products/waxes-and-oils/corimul-r1/', 'Corimul R1');
  PesquisaProduto('http://www.corichem.it/finishing-products/waxes-and-oils/coripolish-707/', 'Coripolish 707');
  PesquisaProduto('http://www.corichem.it/finishing-products/waxes-and-oils/coripolish-719/', 'Coripolish 719');
  PesquisaProduto('http://www.corichem.it/finishing-products/waxes-and-oils/coripolish-720/', 'Coripolish 720');
  PesquisaProduto('http://www.corichem.it/finishing-products/waxes-and-oils/coripolish-780/', 'Coripolish 780');
  PesquisaProduto('http://www.corichem.it/finishing-products/waxes-and-oils/coripolish-800/', 'Coripolish 800');
  PesquisaProduto('http://www.corichem.it/finishing-products/waxes-and-oils/coriwax-27/', 'Coriwax 27');
  PesquisaProduto('http://www.corichem.it/finishing-products/waxes-and-oils/coriwax-28/', 'Coriwax 28');
  PesquisaProduto('http://www.corichem.it/finishing-products/waxes-and-oils/coriwax-230/', 'Coriwax 230');
  PesquisaProduto('http://www.corichem.it/finishing-products/waxes-and-oils/coriwax-242/', 'Coriwax 242');
  PesquisaProduto('http://www.corichem.it/finishing-products/waxes-and-oils/coriwax-256/', 'Coriwax 256');
  PesquisaProduto('http://www.corichem.it/finishing-products/waxes-and-oils/coriwax-a/', 'Coriwax A');
  PesquisaProduto('http://www.corichem.it/finishing-products/waxes-and-oils/coriwax-au/', 'Coriwax AU');
  PesquisaProduto('http://www.corichem.it/finishing-products/waxes-and-oils/coriwax-d/', 'Coriwax D');
  PesquisaProduto('http://www.corichem.it/finishing-products/waxes-and-oils/coriwax-e/', 'Coriwax E');
  PesquisaProduto('http://www.corichem.it/finishing-products/waxes-and-oils/coriwax-f53/', 'Coriwax F53');
  PesquisaProduto('http://www.corichem.it/finishing-products/waxes-and-oils/coriwax-k-26/', 'Coriwax K 26');
  PesquisaProduto('http://www.corichem.it/finishing-products/waxes-and-oils/coriwax-lt/', 'Coriwax LT');
  PesquisaProduto('http://www.corichem.it/finishing-products/waxes-and-oils/coriwax-m/', 'Coriwax M');
  PesquisaProduto('http://www.corichem.it/finishing-products/waxes-and-oils/coriwax-n24/', 'Coriwax N24');
  PesquisaProduto('http://www.corichem.it/finishing-products/waxes-and-oils/coriwax-p9/', 'Coriwax P9');
  PesquisaProduto('http://www.corichem.it/finishing-products/waxes-and-oils/coriwax-prf/', 'Coriwax PRF');
  PesquisaProduto('http://www.corichem.it/finishing-products/waxes-and-oils/coriwax-rfd/', 'Coriwax RFD');
  PesquisaProduto('http://www.corichem.it/finishing-products/waxes-and-oils/coriwax-t32/', 'Coriwax T32');
  PesquisaProduto('http://www.corichem.it/finishing-products/waxes-and-oils/coriwax-up/', 'Coriwax UP');
  //
  Segmento := 'FI';
  Divisao  := 'Auxiliary Agents';
  SubDivisao := Divisao;
  //
  PesquisaProduto('http://www.corichem.it/finishing-products/auxiliary-agents/coridrive-m1/', 'Coridrive M1');
  PesquisaProduto('http://www.corichem.it/finishing-products/auxiliary-agents/coridrive-pnt500n/', 'Coridrive PNT500/N');
  PesquisaProduto('http://www.corichem.it/finishing-products/auxiliary-agents/corifob-w-27/', 'Corifob W 27');
  PesquisaProduto('http://www.corichem.it/finishing-products/auxiliary-agents/corifob-w-40/', 'Corifob W 40');
  PesquisaProduto('http://www.corichem.it/finishing-products/auxiliary-agents/corilink-08/', 'Corilink 08');
  PesquisaProduto('http://www.corichem.it/finishing-products/auxiliary-agents/corilink-71100/', 'Corilink 71/100');
  PesquisaProduto('http://www.corichem.it/finishing-products/auxiliary-agents/corilink-72/', 'Corilink 72');
  PesquisaProduto('http://www.corichem.it/finishing-products/auxiliary-agents/corilink-101/', 'Corilink 101');
  PesquisaProduto('http://www.corichem.it/finishing-products/auxiliary-agents/coriset-b20/', 'Coriset B/20');
  PesquisaProduto('http://www.corichem.it/finishing-products/auxiliary-agents/coriset-ef70/', 'Coriset EF70');
  PesquisaProduto('http://www.corichem.it/finishing-products/auxiliary-agents/coriset-w-2030op/', 'Coriset W 2030/OP');
  //
  Segmento := 'FI';
  Divisao  := 'Stuccoes';
  SubDivisao := Divisao;
  //
  PesquisaProduto('http://www.corichem.it/finishing-products/stuccoes/coristucco-car-1/', 'Coristucco CAR 1');
  PesquisaProduto('http://www.corichem.it/finishing-products/stuccoes/Coristucco-fx84/', 'Coristucco FX/84');
  PesquisaProduto('http://www.corichem.it/finishing-products/stuccoes/Coristucco-fx90/', 'Coristucco FX/90');
  PesquisaProduto('http://www.corichem.it/finishing-products/stuccoes/Coristucco-ks53f/', 'Coristucco KS53/F');
  PesquisaProduto('http://www.corichem.it/finishing-products/stuccoes/Coristucco-ks54f/', 'Coristucco KS54/F');
  PesquisaProduto('http://www.corichem.it/finishing-products/stuccoes/Coristucco-ks180/', 'Coristucco KS/180');
  //
  Segmento := 'WE';
  Divisao  := 'Evocor';
  SubDivisao := Divisao;
  //
  PesquisaProduto('http://www.corichem.it/evocor-products/evocor-as-1/', 'EVOCOR AS 1');
  PesquisaProduto('http://www.corichem.it/evocor-products/evocor-av-2/', 'EVOCOR AV 2');
  PesquisaProduto('http://www.corichem.it/evocor-products/evocor-cz-1/', 'EVOCOR CZ 1');
  PesquisaProduto('http://www.corichem.it/evocor-products/evocor-cz-3/', 'EVOCOR CZ 3');
  PesquisaProduto('http://www.corichem.it/evocor-products/evocor-pl-3/', 'EVOCOR PL 3');
  PesquisaProduto('http://www.corichem.it/evocor-products/evocor-pl-4/', 'EVOCOR PL 4');
{
  PesquisaProduto('', '');
}
end;

procedure TFmLoadInsum.CarregaProdutosURLDermacolor();
const
  pH          = '';
  CargaIonica = '';
  Aspecto     = '';
  Utilizacao  = '';
  Perc_Uso    = '';
  ConcouAtiv  = '';
  SolidezLuz  = '';
var
  Nome, Natureza, Propriedades, Segmento, Divisao, SubDivisao, LocalPDF: String;
  DestFile, Extraido, Restante, Texto, Fonte: String;
  OK: Boolean;
  P, N, It: Integer;

  //

  procedure PP(URL, NomePQ: String);
  var
    ArrTexto: array of String;
    ArrI, ArrJ, TryN: Integer;
  begin
  {
  href="http://www.corichem.it/wet-products/
  href="http://www.corichem.it/tanning
  }
    //Fonte := 'http://www.corichem.it/wet-products/';
    Fonte := URL;
    //
    //if FileExists(DestFile) then
      //DeleteFile(DestFile);
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Baixando arquivo!');
    Application.ProcessMessages;
    DestFile := InitDir + '\Dermacolor\' + NomePQ + '.pdf';
    LocalPDF := '';
    // Tentativa 1
    if DmkWeb.DownloadFile(Fonte, DestFile) then
    begin
      //Memo1.Lines.LoadFromFile(destFile);
      //MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Arquivo baixado!');
      LocalPDF := DestFile;
    end
    else
    begin
      // Tentativa 2
      if DmkWeb.DownloadFile(Fonte, DestFile) then
      begin
        //Memo1.Lines.LoadFromFile(destFile);
        //MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Arquivo baixado!');
        LocalPDF := DestFile;
      end
      else
      begin
        // Tentativa 3
        if DmkWeb.DownloadFile(Fonte, DestFile) then
        begin
          //Memo1.Lines.LoadFromFile(destFile);
          //MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Arquivo baixado!');
          LocalPDF := DestFile;
        end
        else
        begin
          MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Erro durante o download de "' + Fonte + '"');
          ListBox04.Items.Add('Erro no download de "' + Fonte + '"');
          //Exit;
        end;
      end;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Carregando dados ');
    //
{
    Texto := Memo1.Text;
    Restante := '';//Texto;
    OK := True;
    N := 1;
    ArrI := 0;
    while OK do
    begin
      OK := ExtractText(Texto, '<p style="text-align: justify;">', '</p>', P,
      Extraido, Restante);
      Texto := Restante;
      //Geral.MB_Info(URL + sLineBreak + NomePQ + sLineBreak + Extraido);
      if Trim(Extraido) <> '' then
      begin
        ArrI := ArrI+ 1;
        SetLength(ArrTexto, ArrI);
        ArrTexto[ArrI-1] := ArrumaTextoHTML(Extraido);
      end;
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Carregando dados ' + Geral.FF0(N));
      //CarregaGrade(Extraido, N);
    end; // �
}
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Dados carregados na grade');
    //
    Nome := NomePQ;
    Natureza := SubDivisao;
    Propriedades := '';
{
    case ArrI of
      0:
      begin
        Natureza := '';
        Propriedades := '';
      end;
      1:
      begin
        Natureza := '';
        Propriedades := ArrTexto[0];
      end;
      2:
      begin
        Natureza := ArrTexto[0];
        Propriedades := ArrTexto[1];
      end;
      3:
      begin
        Natureza := ArrTexto[0];
        Propriedades := ArrTexto[1] + sLineBreak + ArrTexto[2];
      end;
      else
      begin
        Texto := '';
        for ArrJ := 0 to Length(ArrTexto) - 1 do
          Texto := Texto + 'Texto ' + Geral.FF0(ArrJ) + ': ' + ArrTexto[ArrJ] + sLineBreak;
        //
        Geral.MB_Aviso('"ArrTexto['+ Geral.FF0(ArrI) + '] n�o implementado!' +
        sLineBreak + Texto);
      end;
    end;
}
    It := It + 1;
    AdicionaItemSG(It, Segmento, Divisao, SubDivisao, Nome, Natureza,
      Propriedades, pH, CargaIonica, Aspecto, Utilizacao, Perc_Uso,
      ConcouAtiv, SolidezLuz, LocalPDF);
    //UpdateInfo(It);
  end;
begin
  ListBox04.Items.Clear;
  It := 0;
  //
  //Fonte := 'http://www.dermacolor.it/prodotti-fase-umida/';



//BIOTASE
//Bating Agents
  Segmento := 'BH';
  Divisao  := 'BIOTASE';
  SubDivisao := 'Bating agents - Soaking enzymatic products';
  //
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/BIOTASE-B-123-ed-21.pdf', 'BIOTASE B-123'); // ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/BIOTASE-B-4000-LIQ..pdf', 'BIOTASE B-4000 LIQ.'); //    http://www.dermacolor.it/wp-content/uploads/2015/10/BIOTASE-B-4000-LIQ..pdf
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/BIOTASE-CONC-ed-21.pdf', 'BIOTASE CONC'); // ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/BIOTASE-JN-ed-21.pdf', 'BIOTASE JN'); // ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/BIOTASE-LVP-ed-21.pdf', 'BIOTASE LVP'); // ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/BIOTASE-MBT.pdf', 'BIOTASE MBT'); //
  //
//DECAL
//Deliming Agents
  Segmento := 'BH';
  Divisao  := 'DECAL';
  SubDivisao := 'Deliming Agents';
  //
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DECAL-EK-P.pdf', 'DECAL EK-P'); //
  //PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DECAL-STH06.pdf', 'DECAL STH06'); //
  PP('http://www.dermacolor.it/wp-content/uploads/2013/10/DECAL-STH06_1_SDS.pdf', 'DECAL STH06'); //
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DECAL-ZC-ed-21.pdf', 'DECAL ZC'); //  ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DECAL-ZCR-ed-21.pdf', 'DECAL ZCR'); //  ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DECAL-ZS-ed-21.pdf', 'DECAL ZS'); //  ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DECAL-ZSG-ed-21.pdf', 'DECAL ZSG'); //  ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DECAL-ZSWF1.pdf', 'DECAL ZSWF'); //
//DERMACLEAN
//Degreasing Agents
  Segmento := 'BH';
  Divisao  := 'DERMACLEAN';
  SubDivisao := 'Degreasing Agents';
  //
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMACLEAN-C-PASTA-ed-22.pdf', 'DERMACLEAN C-PASTA'); // DERMACLEAN C-PASTA ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMACLEAN-FN-ed-21.pdf', 'DERMACLEAN FN'); // DERMACLEAN FN ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMACLEAN-FN-N-ed-2.pdf', 'DERMACLEAN FN-N'); // DERMACLEAN FN-N ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMACLEAN-GA-ed-21.pdf', 'DERMACLEAN GA'); // DERMACLEAN GA ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMACLEAN-K-200-ed-21.pdf', 'DERMACLEAN K-200'); // DERMACLEAN K-200 ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMACLEAN-K-280-ed-11.pdf', 'DERMACLEAN K-280'); // DERMACLEAN K-280 ed 1
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMACLEAN-KS-ed-21.pdf', 'DERMACLEAN KS'); // DERMACLEAN KS ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMACLEAN-OCT-ed-21.pdf', 'DERMACLEAN OCT'); // DERMACLEAN OCT ed 2
//DERMADOL
//Fatliquors
  Segmento := 'WE';
  Divisao  := 'DERMADOL';
  SubDivisao := 'Fatliquors';
  //
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMADOL-10-ed-21.pdf', 'DERMADOL 10'); // DERMADOL 10 ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMADOL-10-A-ed-21.pdf', 'DERMADOL 10-A'); // DERMADOL 10-A ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMADOL-12-ed-21.pdf', 'DERMADOL 12'); // DERMADOL 12 ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMADOL-32-ed-21.pdf', 'DERMADOL 32'); // DERMADOL 32 ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMADOL-40L-ed-2.pdf', 'DERMADOL 40L'); // DERMADOL 40L ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMADOL-42-ed-21.pdf', 'DERMADOL 42'); // DERMADOL 42 ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMADOL-98-ed-21.pdf', 'DERMADOL 98'); // DERMADOL 98 ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMADOL-123-ed-21.pdf', 'DERMADOL 123'); // DERMADOL 123 ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMADOL-123-SM-ed-21.pdf', 'DERMADOL 123-SM'); // DERMADOL 123-SM ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMADOL-AK38.pdf', 'DERMADOL AK38'); // DERMADOL AK38
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMADOL-ALM-ed-21.pdf', 'DERMADOL ALM'); // DERMADOL ALM ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMADOL-B30-ed-21.pdf', 'DERMADOL B30'); // DERMADOL B30 ed 2
  //PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMADOL-BK-ed-3.pdf', 'DERMADOL BK'); // DERMADOL BK ed 3
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMADOL-BK-ed-2.pdf', 'DERMADOL BK'); // DERMADOL BK ed 3
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMADOL-BZN-ed-21.pdf', 'DERMADOL BZN'); // DERMADOL BZN ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMADOL-C-ed-22.pdf', 'DERMADOL C'); // DERMADOL C ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMADOL-C-ed-22.pdf', 'DERMADOL CR'); // DERMADOL CR ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMADOL-CV-ed-21.pdf', 'DERMADOL CV'); // DERMADOL CV ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMADOL-EF-PR-ed-21.pdf', 'DERMADOL EF-PR'); // DERMADOL EF-PR ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMADOL-EP-ed-21.pdf', 'DERMADOL EP'); // DERMADOL EP ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMADOL-ESL-ed-21.pdf', 'DERMADOL ESL'); // DERMADOL ESL ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMADOL-GLO-ed-21.pdf', 'DERMADOL GLO'); // DERMADOL GLO ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMADOL-HISN-ed-21.pdf', 'DERMADOL HISN'); // DERMADOL HISN ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMADOL-ID-ed-2.pdf', 'DERMADOL ID'); // DERMADOL ID ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMADOL-ID-50-ed-21.pdf', 'DERMADOL ID-50'); // DERMADOL ID-50 ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMADOL-IFN-ed-2.pdf', 'DERMADOL IFN'); // DERMADOL IFN ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMADOL-IGX-ed-21.pdf', 'DERMADOL IGX'); // DERMADOL IGX ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMADOL-K70-ed-21.pdf', 'DERMADOL K70'); // DERMADOL K70 ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMADOL-LE-85-ed-21.pdf', 'DERMADOL LE-85'); // DERMADOL LE-85 ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMADOL-LTK-N-ed-21.pdf', 'DERMADOL LTK-N'); // DERMADOL LTK-N ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMADOL-MAF-ed-2.pdf', 'DERMADOL MAF'); // DERMADOL MAF ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMADOL-OCS-ed-21.pdf', 'DERMADOL OCS'); // DERMADOL OCS ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMADOL-OLIO-344-ed-2.pdf', 'DERMADOL OLIO 344'); // DERMADOL OLIO 344 ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMADOL-P-ed-2.pdf', 'DERMADOL P'); // DERMADOL P ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMADOL-P-C-ed-21.pdf', 'DERMADOL P-C'); // DERMADOL P-C ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMADOL-Q-LIQ.-ed-21.pdf', 'DERMADOL Q LIQ.'); // DERMADOL Q LIQ. ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMADOL-RA-ed-21.pdf', 'DERMADOL RA'); // DERMADOL RA ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMADOL-RCA-ed-21.pdf', 'DERMADOL RCA'); // DERMADOL RCA ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMADOL-S-70-ed-21.pdf', 'DERMADOL S 70'); // DERMADOL S 70 ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMADOL-S50-ed-21.pdf', 'DERMADOL S50'); // DERMADOL S50 ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMADOL-S90-ed-2.pdf', 'DERMADOL S90'); // DERMADOL S90 ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMADOL-SW-ed-21.pdf', 'DERMADOL SW'); // DERMADOL SW ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMADOL-TN-ed-2.pdf', 'DERMADOL TN'); // DERMADOL TN ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMADOL-W-100-ed-2.pdf', 'DERMADOL W 100'); // DERMADOL W 100 ed 2
//DERMAFLEX
//Resins
  Segmento := 'WE';
  Divisao  := 'DERMAFLEX';
  SubDivisao := 'Resins';
  //
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMAFLEX-AP-ed-21.pdf', 'DERMAFLEX AP'); // DERMAFLEX AP ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMAFLEX-AR30-LIQ-ed-2.pdf', 'DERMAFLEX AR30 LIQ'); // DERMAFLEX AR30 LIQ ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMAFLEX-CF-ed-2.pdf', 'DERMAFLEX CF'); // DERMAFLEX CF ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMAFLEX-CM-ed-21.pdf', 'DERMAFLEX CM'); // DERMAFLEX CM ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMAFLEX-DFS2-ed-21.pdf', 'DERMAFLEX DFS2'); // DERMAFLEX DFS2 ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMAFLEX-HQ-ed-2.pdf', 'DERMAFLEX HQ'); // DERMAFLEX HQ ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMAFLEX-L140-LIQ-ed-21.pdf', 'DERMAFLEX L140 LIQ'); // DERMAFLEX L140 LIQ ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMAFLEX-R-11-ed-2.pdf', 'DERMAFLEX R 11'); // DERMAFLEX R 11 ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMAFLEX-SE30-Ed-2.pdf', 'DERMAFLEX SE30'); // DERMAFLEX SE30 Ed 2
  //
//DERMALENE
//Syntyans
  Segmento := 'WE';
  Divisao  := 'DERMALENE';
  SubDivisao := 'Syntyans';
  //
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMALENE-AL-SB-Ed-2.pdf', 'DERMALENE AL-SB'); // DERMALENE AL-SB Ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMALENE-BSF-ed-2.pdf', 'DERMALENE BSF'); // DERMALENE BSF ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMALENE-BSO-ed-2.pdf', 'DERMALENE BSO'); // DERMALENE BSO ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMALENE-CAS-ed-2.pdf', 'DERMALENE CAS'); // DERMALENE CAS ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMALENE-CB-ed-21.pdf', 'DERMALENE CB'); // DERMALENE CB ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMALENE-CH-ed-2.pdf', 'DERMALENE CH'); // DERMALENE CH ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMALENE-FAB-ed-2.pdf', 'DERMALENE FAB'); // DERMALENE FAB ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMALENE-L2T-ed-21.pdf', 'DERMALENE L2T'); // DERMALENE L2T ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMALENE-LXR-liq-ed-2.pdf', 'DERMALENE LXR liq'); // DERMALENE LXR liq ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMALENE-M-ed-21.pdf', 'DERMALENE M'); // DERMALENE M ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMALENE-NP-ed-21.pdf', 'DERMALENE NP'); // DERMALENE NP ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMALENE-PR-ed-21.pdf', 'DERMALENE PR'); // DERMALENE PR ed 2
  //PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMALENE-PR-CONC.pdf', 'DERMALENE PR CONC'); // DERMALENE PR CONC
  PP('http://www.dermacolor.it/wp-content/uploads/2014/07/DERMALENE-PR-CONC-Rev-2-070915-SI.pdf', 'DERMALENE PR CONC'); // DERMALENE PR CONC
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMALENE-PRD-ed-21.pdf', 'DERMALENE PRD'); // DERMALENE PRD ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMALENE-PRD-LIQ-ed-21.pdf', 'DERMALENE PRD LIQ'); // DERMALENE PRD LIQ ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMALENE-PW-ed-21.pdf', 'DERMALENE PW'); // DERMALENE PW ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMALENE-RH-N-ed-21.pdf', 'DERMALENE RH-N'); // DERMALENE RH-N ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMALENE-RRP.pdf', 'DERMALENE RRP'); // DERMALENE RRP
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMALENE-RX-liq-ed-21.pdf', 'DERMALENE RX liq'); // DERMALENE RX liq ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMALENE-SF-liq-ed-2.pdf', 'DERMALENE SF liq'); // DERMALENE SF liq ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMALENE-SG-ed-21.pdf', 'DERMALENE SG'); // DERMALENE SG ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMALENE-SRG-liq-ed-2.pdf', 'DERMALENE SRG liq'); // DERMALENE SRG liq ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMALENE-WG-ed-21.pdf', 'DERMALENE WG'); // DERMALENE WG ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMALENE-WX-ed-21.pdf', 'DERMALENE WX'); // DERMALENE WX ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMALENE-XL-liq-ed-21.pdf', 'DERMALENE XL liq'); // DERMALENE XL liq ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMALENE-XX-ed-21.pdf', 'DERMALENE XX'); // DERMALENE XX ed 2
//DERMAMINA
//Cationic Fatliquors
  Segmento := 'WE';
  Divisao  := 'DERMAMINA';
  SubDivisao := 'Cationic Fatliquors';
  //
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMAMINA-CL-ed-2.pdf', 'DERMAMINA CL'); // DERMAMINA CL ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMAMINA-GS-ed-2.pdf', 'DERMAMINA GS'); // DERMAMINA GS ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMAMINA-LW-ed-2.pdf', 'DERMAMINA LW'); // DERMAMINA LW ed 2
  //
//DERMASOFT
//Dispersing Agents
  Segmento := 'WE';
  Divisao  := 'DERMASOFT';
  SubDivisao := 'Dispersing Agents';
  //
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMASOFT-AR-ed-21.pdf', 'DERMASOFT AR'); //  DERMASOFT AR ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMASOFT-BW-ed-2.pdf', 'DERMASOFT BW'); //  DERMASOFT BW ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMASOFT-CHE-LIQ.pdf', 'DERMASOFT CHE LIQ D'); //  DERMASOFT CHE LIQ D
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMASOFT-EC-K-ed-2.pdf', 'DERMASOFT EC-K'); //  DERMASOFT EC-K ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMASOFT-FD-LIQ-ed-21.pdf', 'DERMASOFT FD LIQ'); //  DERMASOFT FD LIQ ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMASOFT-FF-LIQ-ed-2.pdf', 'DERMASOFT FF LIQ'); //  DERMASOFT FF LIQ ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMASOFT-GA-ed-21.pdf', 'DERMASOFT GA'); //  DERMASOFT GA ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMASOFT-GRF-liq-ed-2.pdf', 'DERMASOFT GRF liq'); //  DERMASOFT GRF liq ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMASOFT-KLG-liq-ed-2.pdf', 'DERMASOFT KLG liq'); //  DERMASOFT KLG liq ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMASOFT-MG-ed-21.pdf', 'DERMASOFT MG'); //  DERMASOFT MG ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMASOFT-PP-ed-21.pdf', 'DERMASOFT PP'); //  DERMASOFT PP ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMASOFT-RNA-ed-21.pdf', 'DERMASOFT RNA'); //  DERMASOFT RNA ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMASOFT-RNX-ed-21.pdf', 'DERMASOFT RNX'); //  DERMASOFT RNX ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMASOFT-SI85.pdf', 'DERMASOFT SI85'); //  DERMASOFT SI85
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMASOFT-ST-ed-21.pdf', 'DERMASOFT ST'); //  DERMASOFT ST ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMASOFT-W-ed-21.pdf', 'DERMASOFT W'); //  DERMASOFT W ed 2
  //PP('http://www.dermacolor.it/wp-content/uploads/2015/10/DERMASOFT-W-liq-ed-2.pdf', 'DERMASOFT W liq'); //  DERMASOFT W liq ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2016/09/DERMASOFT-W-LIQ_1_SDS.pdf', 'DERMASOFT W liq'); //  DERMASOFT W liq ed 2
//K-LIME
//Liming Auxiliares
  Segmento := 'BH';
  Divisao  := 'K-LIME';
  SubDivisao := 'Liming Auxiliares';
  //
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/K-LIME-52-ed-22.pdf', 'K LIME 52'); // K LIME 52 ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/K-LIME-384-ed-21.pdf', 'K LIME 384'); // K LIME 384 ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/K-LIME-CONC-ed-2-1.pdf', 'K-LIME CONC'); // K-LIME CONC ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/K-LIME-OM-ed-2.pdf', 'K LIME OM'); // K LIME OM ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/K-LIME-GLT-ed-2.pdf', 'K-LIME GLT'); // K-LIME GLT ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/K-LIME-KCK-N-ed-21.pdf', 'K-LIME KCK-N'); // K-LIME KCK-N ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/K-LIME-RP-70-ed-21.pdf', 'K-LIME RP 70'); // K-LIME RP 70 ed 2
//K-THIO
//Biocides
  Segmento := 'BH';
  Divisao  := 'K-THIO';
  SubDivisao := 'Biocides';
  //
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/K-THIO-HTR-ed-31.pdf', 'K-THIO HTR'); // K-THIO HTR ed 3
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/K-THIO-KW-ed-2.pdf', 'K-THIO KW'); // K-THIO KW ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/K-THIO-NE-2-ed-21.pdf', 'K-THIO NE-2'); // K-THIO NE-2 ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/K-THIO-S30-ed-3.pdf', 'K-THIO S30'); // K-THIO S30 ed 3
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/K-THIO-SN-ed-2.pdf', 'K-THIO SN'); // K-THIO SN ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/K-THIO-STL-ed-2.pdf', 'K-THIO STL'); // K-THIO STL ed 2
//PATO
//Special Products
  Segmento := 'WE';
  Divisao  := 'PATO';
  SubDivisao := 'Special Products';
  //
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/PATO-26-N-ed-2-1.pdf', 'PATO 26-N'); // PATO 26-N ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/PATO-60F.pdf', 'PATO 60F'); // PATO 60F
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/PATO-131-ed-2.pdf', 'PATO 131'); // PATO 131 ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/PATO-279-ed-21.pdf', 'PATO 279'); // PATO 279 ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/PATO-A-ed-2.pdf', 'PATO A'); // PATO A ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/PATO-C30-ed-2.pdf', 'PATO C30'); // PATO C30 ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/PATO-G285.pdf', 'PATO G285'); // PATO G285
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/PATO-GS606.pdf', 'PATO GS606'); // PATO GS606
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/PATO-IMP1.pdf', 'PATO IMP'); // PATO IMP
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/PATO-POL-95-ed-21.pdf', 'PATO POL-95'); // PATO POL-95 ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/PATO-TW-ed-2.pdf', 'PATO TW'); // PATO TW ed 2
//VIDERMA DYES
//Leather Dyes
  Segmento := 'FI';
  Divisao  := 'VIDERMA DYES';
  SubDivisao := 'Leather Dyes';
  //
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/AIR-BROWN-2RB-ed-2.pdf', 'AIR BROWN 2RB'); // AIR BROWN 2RB ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/AIR-FAWN-G-ed-2.pdf', 'AIR FAWN G'); // AIR FAWN G ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/VID-BLACK-TNT-130-ed-21.pdf', 'VID BLACK TNT 130'); // VID BLACK TNT 130 ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/VID-BROWN-R-ed-21.pdf', 'VID BROWN R'); // VID BROWN R ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/VID-ORANGE-P2-ed-2.pdf', 'VID ORANGE P2'); // VID ORANGE P2 ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/VID.-VIOLET-AG.pdf', 'VID. VIOLET AG'); // VID. VIOLET AG
//VIDERMA FAST DYES
//High-performance leather dyes
  Segmento := 'FI';
  Divisao  := 'VIDERMA FAST DYES';
  SubDivisao := 'High-performance leather dyes';
  //
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/AIR-BROWN-2RB-ed-2.pdf', 'AIR BROWN 2RB'); // AIR BROWN 2RB ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/AIR-FAWN-G-ed-2.pdf', 'AIR FAWN G'); // AIR FAWN G ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/VID-BLACK-TNT-130-ed-21.pdf', 'VID BLACK TNT 130'); // VID BLACK TNT 130 ed
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/VID-BROWN-R-ed-21.pdf', 'VID BROWN R'); // VID BROWN R ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/VID-ORANGE-P2-ed-2.pdf', 'VID ORANGE P2'); // VID ORANGE P2 ed 2
  PP('http://www.dermacolor.it/wp-content/uploads/2015/10/VID.-VIOLET-AG.pdf', 'VID VIOLET AG'); // VID. VIOLET AG

{
Index of /wp-content/uploads/2015/10
[ICO]	Name	Last modified	Size	Description
[PARENTDIR]	Parent Directory	 	-
[   ]	ACIDO-ACRILICO-Rev-2-071015-CLP.pdf	2017-06-29 10:57	177K
[   ]	ACIDO-ACRILICO_3_SDS.pdf	2017-06-29 10:57	555K
[   ]	AIR-BROWN-2RB-ed-2.pdf	2018-05-13 07:14	203K
[   ]	AIR-FAWN-G-ed-2.pdf	2018-05-13 07:14	203K
[   ]	BIANCO-PER-BOTTE-B-ed-2.pdf	2018-05-13 07:14	203K
[   ]	BIOTASE-AM-ed-2.pdf	2018-05-13 07:14	196K
[   ]	BIOTASE-B-80-ed-2.pdf	2018-05-13 07:14	196K
[   ]	BIOTASE-B-123-ed-2.pdf	2018-05-13 07:14	202K
[   ]	BIOTASE-B-123-ed-21.pdf	2018-05-13 07:14	202K
[   ]	BIOTASE-CONC-ed-2.pdf	2018-05-13 07:14	203K
[   ]	BIOTASE-CONC-ed-21.pdf	2018-05-13 07:14	203K
[   ]	BIOTASE-JN-ed-2.pdf	2018-05-13 07:14	203K
[   ]	BIOTASE-JN-ed-21.pdf	2018-05-13 07:14	203K
[   ]	BIOTASE-LVP-ed-2.pdf	2018-05-13 07:14	196K
[   ]	BIOTASE-LVP-ed-21.pdf	2018-05-13 07:14	203K
[   ]	BIOTASE-MBT.pdf	2018-05-13 07:14	203K
[   ]	CHEWINGUM-Rev-2-191015-CLP.pdf	2017-06-29 10:57	171K
[   ]	COMPOUND-DLS-LIQ-ed-2.pdf	2018-05-13 07:14	204K
[   ]	DECAL-BB-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DECAL-EC-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DECAL-EK-P.pdf	2018-05-13 07:14	196K
[   ]	DECAL-ZC-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DECAL-ZC-ed-21.pdf	2018-05-13 07:14	196K
[   ]	DECAL-ZCR-ed-2.pdf	2018-05-13 07:14	203K
[   ]	DECAL-ZCR-ed-21.pdf	2018-05-13 07:14	203K
[   ]	DECAL-ZE-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DECAL-ZS-ed-2.pdf	2018-05-13 07:14	203K
[   ]	DECAL-ZS-ed-21.pdf	2018-05-13 07:14	203K
[   ]	DECAL-ZSG-ed-2.pdf	2018-05-13 07:14	203K
[   ]	DECAL-ZSG-ed-21.pdf	2018-05-13 07:14	203K
[   ]	DECAL-ZSWF.pdf	2018-05-13 07:14	203K
[   ]	DECAL-ZSWF1.pdf	2018-05-13 07:14	203K
[   ]	DERMA-K-NERO-Rev-7-180314-CLP.pdf	2017-06-29 10:57	453K
[   ]	DERMACLEAN-C-PASTA-ed-2.pdf	2018-05-13 07:14	205K
[   ]	DERMACLEAN-C-PASTA-ed-21.pdf	2017-06-29 10:57	64K
[   ]	DERMACLEAN-C-PASTA-ed-22.pdf	2018-05-13 07:14	205K
[   ]	DERMACLEAN-FN-N-ed-2.pdf	2018-05-13 07:14	203K
[   ]	DERMACLEAN-FN-ed-2.pdf	2018-05-13 07:14	203K
[   ]	DERMACLEAN-FN-ed-21.pdf	2018-05-13 07:14	203K
[   ]	DERMACLEAN-FNN-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMACLEAN-GA-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMACLEAN-GA-ed-21.pdf	2018-05-13 07:14	203K
[   ]	DERMACLEAN-K-200-ed-2.pdf	2018-05-13 07:14	202K
[   ]	DERMACLEAN-K-200-ed-21.pdf	2018-05-13 07:14	202K
[   ]	DERMACLEAN-K-230-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMACLEAN-K-240-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMACLEAN-K-260-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMACLEAN-K-270-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMACLEAN-K-280-ed-1.pdf	2018-05-13 07:14	203K
[   ]	DERMACLEAN-K-280-ed-11.pdf	2018-05-13 07:14	203K
[   ]	DERMACLEAN-KS-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMACLEAN-KS-ed-21.pdf	2018-05-13 07:14	203K
[   ]	DERMACLEAN-OCT-ed-2.pdf	2018-05-13 07:14	203K
[   ]	DERMACLEAN-OCT-ed-21.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-10-A-ed-2.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-10-A-ed-21.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-10-ed-2.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-10-ed-21.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-12-ed-2.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-12-ed-21.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-32-60-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMADOL-32-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMADOL-32-ed-21.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-40L-ed-2.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-42-ed-2.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-42-ed-21.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-98-ed-2.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-98-ed-21.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-123-SM-ed-2.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-123-SM-ed-21.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-123-ed-2.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-123-ed-21.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-703-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMADOL-832-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMADOL-2132-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMADOL-AK38.pdf	2018-05-13 07:14	202K
[   ]	DERMADOL-ALM-ed-2.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-ALM-ed-21.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-B30-ed-2.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-B30-ed-21.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-BK-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMADOL-BK-ed-21.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-BZN-ed-2.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-BZN-ed-21.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-C-Rev-5-091015-CLP.pdf	2017-06-29 10:57	178K
[   ]	DERMADOL-C-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMADOL-C-ed-21.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-C-ed-22.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-CR-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMADOL-CR-ed-21.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-CV-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMADOL-CV-ed-21.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-C_7_SDS.pdf	2017-06-29 10:57	353K
[   ]	DERMADOL-EC-10-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMADOL-EF-PR-ed-2.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-EF-PR-ed-21.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-EF-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMADOL-EP-ed-2.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-EP-ed-21.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-ES-50-ed-1.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-ESL-ed-2.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-ESL-ed-21.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-GLO-ed-2.pdf	2018-05-13 07:14	202K
[   ]	DERMADOL-GLO-ed-21.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-HISN-ed-2.pdf	2018-05-13 07:14	204K
[   ]	DERMADOL-HISN-ed-21.pdf	2018-05-13 07:14	204K
[   ]	DERMADOL-ID-50-ed-2.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-ID-50-ed-21.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-ID-Rev-9-020414-CLP.pdf	2017-06-29 10:57	268K
[   ]	DERMADOL-ID-Rev-10-141016-CLP.pdf	2017-06-29 10:57	466K
[   ]	DERMADOL-ID-ed-2.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-ID-ed-4.pdf	2018-05-13 07:14	196K
[   ]	DERMADOL-IFN-ed-2.pdf	2018-05-13 07:14	204K
[   ]	DERMADOL-IGX-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMADOL-IGX-ed-21.pdf	2018-05-13 07:14	204K
[   ]	DERMADOL-K70-ed-2.pdf	2017-06-29 10:57	40K
[   ]	DERMADOL-K70-ed-21.pdf	2017-06-29 10:57	40K
[   ]	DERMADOL-LE-01-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMADOL-LE-85-ed-2.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-LE-85-ed-21.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-LR606-ed-2.pdf	2017-06-29 10:57	54K
[   ]	DERMADOL-LTK-N-ed-2.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-LTK-N-ed-21.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-MAF-ed-2.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-MAL-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMADOL-NFI-Ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMADOL-NS-EC2-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMADOL-NSI-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMADOL-NSV-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMADOL-OCS-ed-2.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-OCS-ed-21.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-OLIO-344-Rev-5-290615-CLP.pdf	2017-06-29 10:57	100K
[   ]	DERMADOL-OLIO-344-Rev-6-071116-SI.pdf	2017-06-29 10:57	286K
[   ]	DERMADOL-OLIO-344-ed-2.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-OVE-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMADOL-P-C-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMADOL-P-C-ed-21.pdf	2018-05-13 07:14	202K
[   ]	DERMADOL-P-Rev-4-110216-CLP.pdf	2017-06-29 10:57	103K
[   ]	DERMADOL-P-ed-2.pdf	2018-05-13 07:14	202K
[   ]	DERMADOL-Q-LIQ-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMADOL-Q-LIQ.-ed-2.pdf	2017-06-29 10:57	124K
[   ]	DERMADOL-Q-LIQ.-ed-21.pdf	2017-06-29 10:57	124K
[   ]	DERMADOL-RA-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMADOL-RA-ed-21.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-RCA-ed-2.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-RCA-ed-21.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-S-70-ed-2.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-S-70-ed-21.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-S-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMADOL-S50-ed-2.pdf	2017-06-29 10:57	102K
[   ]	DERMADOL-S50-ed-21.pdf	2017-06-29 10:57	102K
[   ]	DERMADOL-S50.pdf	2018-05-13 07:14	197K
[   ]	DERMADOL-S90-ed-2.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-SW-ed-2.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-SW-ed-21.pdf	2018-05-13 07:14	203K
[   ]	DERMADOL-TN-ed-2.pdf	2018-05-13 07:14	202K
[   ]	DERMADOL-TR-AS-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMADOL-W-100-ed-2.pdf	2018-05-13 07:14	204K
[   ]	DERMADOL-W100-Rev-12-180416-CLP.pdf	2017-06-29 10:57	193K
[   ]	DERMADOL-W200-Rev-7-170615-CLP.pdf	2017-06-29 10:57	187K
[   ]	DERMAFLEX-906-LIQ-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMAFLEX-AP-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMAFLEX-AP-ed-21.pdf	2018-05-13 07:14	204K
[   ]	DERMAFLEX-AR30-LIQ-ed-2.pdf	2018-05-13 07:14	204K
[   ]	DERMAFLEX-CF-ed-2.pdf	2018-05-13 07:14	203K
[   ]	DERMAFLEX-CM-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMAFLEX-CM-ed-21.pdf	2018-05-13 07:14	203K
[   ]	DERMAFLEX-DFS2-ed-2.pdf	2018-05-13 07:14	197K
[   ]	DERMAFLEX-DFS2-ed-21.pdf	2018-05-13 07:14	203K
[   ]	DERMAFLEX-HQ-ed-2.pdf	2018-05-13 07:14	204K
[   ]	DERMAFLEX-HQ-ed-3.pdf	2018-05-13 07:14	197K
[   ]	DERMAFLEX-L140-LIQ-ed-2.pdf	2018-05-13 07:14	203K
[   ]	DERMAFLEX-L140-LIQ-ed-21.pdf	2018-05-13 07:14	203K
[   ]	DERMAFLEX-R-11-ed-2.pdf	2018-05-13 07:14	203K
[   ]	DERMAFLEX-SE30-Ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMAFLEX-SE30-ed-2.pdf	2017-06-29 10:57	15K
[   ]	DERMAFLEX-SP-Ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMALENE-AL-SB-Ed-2.pdf	2017-06-29 10:57	14K
[   ]	DERMALENE-AL-SB-ed-2.pdf	2018-05-13 07:14	202K
[   ]	DERMALENE-BSF-ed-2.pdf	2018-05-13 07:14	204K
[   ]	DERMALENE-BSO-ed-2.pdf	2018-05-13 07:14	203K
[   ]	DERMALENE-CAS-ed-2.pdf	2018-05-13 07:14	203K
[   ]	DERMALENE-CB-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMALENE-CB-ed-21.pdf	2018-05-13 07:14	202K
[   ]	DERMALENE-CH-ed-2.pdf	2018-05-13 07:14	203K
[   ]	DERMALENE-FAB-ed-2.pdf	2018-05-13 07:14	210K
[   ]	DERMALENE-FT-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMALENE-L2T-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMALENE-L2T-ed-21.pdf	2018-05-13 07:14	204K
[   ]	DERMALENE-LXR-liq-ed-2.pdf	2018-05-13 07:14	203K
[   ]	DERMALENE-M-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMALENE-M-ed-21.pdf	2018-05-13 07:14	204K
[   ]	DERMALENE-NP-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMALENE-NP-ed-21.pdf	2018-05-13 07:14	203K
[   ]	DERMALENE-O-LIQ-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMALENE-PR-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMALENE-PR-ed-21.pdf	2018-05-13 07:14	203K
[   ]	DERMALENE-PRD-LIQ-ed-2.pdf	2018-05-13 07:14	202K
[   ]	DERMALENE-PRD-LIQ-ed-21.pdf	2018-05-13 07:14	202K
[   ]	DERMALENE-PRD-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMALENE-PRD-ed-21.pdf	2018-05-13 07:14	203K
[   ]	DERMALENE-PW-ed-2.pdf	2017-06-29 10:57	112K
[   ]	DERMALENE-PW-ed-21.pdf	2017-06-29 10:57	112K
[   ]	DERMALENE-RH-N-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMALENE-RH-N-ed-21.pdf	2018-05-13 07:14	203K
[   ]	DERMALENE-RRP.pdf	2018-05-13 07:14	203K
[   ]	DERMALENE-RX-liq-ed-2.pdf	2017-06-29 10:57	41K
[   ]	DERMALENE-RX-liq-ed-21.pdf	2017-06-29 10:57	41K
[   ]	DERMALENE-SCA-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMALENE-SF-liq-ed-2.pdf	2018-05-13 07:14	203K
[   ]	DERMALENE-SG-LIQ-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMALENE-SG-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMALENE-SG-ed-21.pdf	2018-05-13 07:14	203K
[   ]	DERMALENE-SG87-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMALENE-SRG-liq-ed-2.pdf	2018-05-13 07:14	203K
[   ]	DERMALENE-WG-LIQ-ed-2.pdf	2018-05-13 07:14	197K
[   ]	DERMALENE-WG-ed-2.pdf	2018-05-13 07:14	197K
[   ]	DERMALENE-WG-ed-21.pdf	2018-05-13 07:14	204K
[   ]	DERMALENE-WX-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMALENE-WX-ed-21.pdf	2018-05-13 07:14	203K
[   ]	DERMALENE-XL-liq-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMALENE-XL-liq-ed-21.pdf	2018-05-13 07:14	203K
[   ]	DERMALENE-XX-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMALENE-XX-ed-21.pdf	2018-05-13 07:14	203K
[   ]	DERMAMINA-CL-ed-2.pdf	2018-05-13 07:14	202K
[   ]	DERMAMINA-GS-ed-2.pdf	2018-05-13 07:14	204K
[   ]	DERMAMINA-LW-ed-2.pdf	2018-05-13 07:14	213K
[   ]	DERMANIL-H-ARANCIO-Rev-4-040816-CLP.pdf	2017-06-29 10:57	1.0M
[   ]	DERMANIL-H-BEIGE-Rev-3-250716-CLP.pdf	2017-06-29 10:57	213K
[   ]	DERMANIL-H-BRUNO-ROSSO-Rev-2-250716-CLP.pdf	2017-06-29 10:57	224K
[   ]	DERMASOFT-AR-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMASOFT-AR-ed-21.pdf	2018-05-13 07:14	203K
[   ]	DERMASOFT-BW-ed-2.pdf	2018-05-13 07:14	202K
[   ]	DERMASOFT-CHE-LIQ.pdf	2018-05-13 07:14	203K
[   ]	DERMASOFT-EC-K-ed-2.pdf	2018-05-13 07:14	202K
[   ]	DERMASOFT-FD-LIQ-ed-2.pdf	2018-05-13 07:14	197K
[   ]	DERMASOFT-FD-LIQ-ed-21.pdf	2018-05-13 07:14	204K
[   ]	DERMASOFT-FF-LIQ-ed-2.pdf	2018-05-13 07:14	203K
[   ]	DERMASOFT-G-ed-1.pdf	2018-05-13 07:14	203K
[   ]	DERMASOFT-GA-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMASOFT-GA-ed-21.pdf	2018-05-13 07:14	203K
[   ]	DERMASOFT-GRF-liq-ed-2.pdf	2018-05-13 07:14	203K
[   ]	DERMASOFT-KLG-liq-ed-2.pdf	2018-05-13 07:14	203K
[   ]	DERMASOFT-MA-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMASOFT-MBA-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMASOFT-MG-ed-2.pdf	2018-05-13 07:14	197K
[   ]	DERMASOFT-MG-ed-21.pdf	2018-05-13 07:14	204K
[   ]	DERMASOFT-PP-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMASOFT-PP-ed-21.pdf	2018-05-13 07:14	204K
[   ]	DERMASOFT-RNA-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMASOFT-RNA-ed-21.pdf	2018-05-13 07:14	203K
[   ]	DERMASOFT-RNX-EG-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMASOFT-RNX-I-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMASOFT-RNX-ed-2.pdf	2018-05-13 07:14	196K
[   ]	DERMASOFT-RNX-ed-21.pdf	2018-05-13 07:14	204K
[   ]	DERMASOFT-SI85.pdf	2018-05-13 07:14	215K
[   ]	DERMASOFT-ST-ed-2.pdf	2018-05-13 07:14	203K
[   ]	DERMASOFT-ST-ed-21.pdf	2018-05-13 07:14	203K
[   ]	DERMASOFT-W-ed-2.pdf	2018-05-13 07:14	210K
[   ]	DERMASOFT-W-ed-21.pdf	2018-05-13 07:14	202K
[   ]	FITAPOR-GT-ed-2.pdf	2018-05-13 07:14	204K
[   ]	FITAPOR-MM-ed-2.pdf	2018-05-13 07:14	203K
[   ]	GRASSO-BOVINO-ed-2.pdf	2018-05-13 07:14	213K
[   ]	K-LIME-52-ed-2.pdf	2018-05-13 07:14	196K
[   ]	K-LIME-52-ed-21.pdf	2018-05-13 07:14	209K
[   ]	K-LIME-52-ed-22.pdf	2018-05-13 07:14	202K
[   ]	K-LIME-384-ed-2.pdf	2018-05-13 07:14	203K
[   ]	K-LIME-384-ed-21.pdf	2018-05-13 07:14	203K
[   ]	K-LIME-CONC-ed-2.pdf	2018-05-13 07:14	196K
[   ]	K-LIME-GLT-ed-2.pdf	2018-05-13 07:14	203K
[   ]	K-LIME-KCK-N-ed-2.pdf	2018-05-13 07:14	203K
[   ]	K-LIME-KCK-N-ed-21.pdf	2018-05-13 07:14	203K
[   ]	K-LIME-OM-ed-2.pdf	2018-05-13 07:14	203K
[   ]	K-LIME-RP-70-ed-2.pdf	2018-05-13 07:14	209K
[   ]	K-LIME-RP-70-ed-21.pdf	2018-05-13 07:14	202K
[   ]	K-THIO-HTR-ed-3.pdf	2018-05-13 07:14	197K
[   ]	K-THIO-HTR-ed-31.pdf	2018-05-13 07:14	205K
[   ]	K-THIO-KW-ed-2.pdf	2018-05-13 07:14	204K
[   ]	K-THIO-NE-2-ed-2.pdf	2018-05-13 07:14	196K
[   ]	K-THIO-NE-2-ed-21.pdf	2018-05-13 07:14	196K
[   ]	K-THIO-S30-ed-2.pdf	2018-05-13 07:14	197K
[   ]	K-THIO-S30-ed-3.pdf	2018-05-13 07:14	219K
[   ]	K-THIO-S30-ed-21.pdf	2018-05-13 07:14	204K
[   ]	K-THIO-SN-ed-2.pdf	2018-05-13 07:14	204K
[   ]	K-THIO-STL-Rev-4-220415-CLP.pdf	2017-06-29 10:57	327K
[   ]	K-THIO-STL-ed-2.pdf	2018-05-13 07:14	205K
[   ]	LUTENSOL-XP-80-200115.pdf	2017-06-29 10:57	199K
[   ]	MICROEMUL-FINISH-L347-Rev-5-050815-CLP.pdf	2017-06-29 10:57	303K
[   ]	MICROFILLER-190-Rev-4-230915-CLP.pdf	2017-06-29 10:57	139K
[   ]	MICROFIX-NUBUK-Rev-7-131015-CLP.pdf	2017-06-29 10:57	173K
[   ]	MICROLAC-C-D-Rev-1-210515-CLP.pdf	2017-06-29 10:57	194K
[   ]	MICROLAC-C-D-Rev-2-270716-CLP-1.pdf	2018-05-31 15:46	1.3M
[   ]	MICROLAC-C-D-Rev-2-270716-CLP.pdf	2017-06-29 10:57	1.3M
[   ]	MICROLAC-C-Rev-9-100515-CLP.pdf	2017-06-29 10:57	229K
[   ]	MICROPUR-1035-Rev-2-141015-CLP.pdf	2017-06-29 10:57	100K
[   ]	MICROPUR-1668_1_SI.pdf	2018-05-31 15:46	304K
[   ]	MICROTEX-ACRILATO-217-Rev-3-261015-CLP.pdf	2017-06-29 10:57	373K
[   ]	MICROTEX-ACRILATO-300-Rev-3-230915-CLP.pdf	2017-06-29 10:57	162K
[   ]	MICROTOP-100-Rev-9-040314-CLP.pdf	2017-06-29 10:57	385K
[   ]	MICROWAX-63-Rev-3-131015-CLP.pdf	2017-06-29 10:57	101K
[   ]	MICROWAX-830-Rev-2-151015-CLP.pdf	2017-06-29 10:57	138K
[   ]	MICROWAX-K-10-Rev-4-230915-CLP.pdf	2017-06-29 10:57	188K
[   ]	PATO-26-N-ed-2.pdf	2018-05-13 07:14	203K
[   ]	PATO-60F.pdf	2018-05-13 07:14	213K
[   ]	PATO-131-ed-2.pdf	2018-05-13 07:14	203K
[   ]	PATO-131-ed-3.pdf	2018-05-13 07:14	196K
[   ]	PATO-279-ed-2.pdf	2018-05-13 07:14	196K
[   ]	PATO-279-ed-21.pdf	2018-05-13 07:14	203K
[   ]	PATO-340-ed-3.pdf	2018-05-13 07:14	196K
[   ]	PATO-A.pdf	2018-05-13 07:14	197K
[   ]	PATO-ARV-ed-2.pdf	2018-05-13 07:14	196K
[   ]	PATO-C30-ed-2.pdf	2018-05-13 07:14	203K
[   ]	PATO-DF-LIQ-ed-2.pdf	2018-05-13 07:14	197K
[   ]	PATO-DV-LIQ-ed-2.pdf	2018-05-13 07:14	196K
[   ]	PATO-G285.pdf	2018-05-13 07:14	392K
[   ]	PATO-GS606.pdf	2018-05-13 07:14	203K
[   ]	PATO-IMP.pdf	2018-05-13 07:14	202K
[   ]	PATO-IMP1.pdf	2018-05-13 07:14	202K
[   ]	PATO-OX-ed-2.pdf	2018-05-13 07:14	197K
[   ]	PATO-POL-95-ed-2.pdf	2018-05-13 07:14	203K
[   ]	PATO-POL-95-ed-21.pdf	2018-05-13 07:14	203K
[   ]	PATO-TW-ed-2.pdf	2018-05-13 07:14	203K
[   ]	PATO-VFL-ed-2.pdf	2018-05-13 07:14	196K
[   ]	RAR-ed-2.pdf	2018-05-13 07:14	203K
[   ]	RISCHIARANTE-1234-ed-2.pdf	2018-05-13 07:14	202K
[   ]	SEQUESTRATORE-K-LIQ.pdf	2018-05-13 07:14	203K
[   ]	SINALBIL-ed-2.pdf	2018-05-13 07:14	207K
[   ]	UNICLAR-ed-2.pdf	2018-05-13 07:14	202K
[   ]	VID-BLACK-TNT-130-ed-2.pdf	2018-05-13 07:14	203K
[   ]	VID-BLACK-TNT-130-ed-21.pdf	2018-05-13 07:14	203K
[   ]	VID-BROWN-R-ed-2.pdf	2018-05-13 07:14	203K
[   ]	VID-BROWN-R-ed-21.pdf	2018-05-13 07:14	203K
[   ]	VID-ORANGE-P2-ed-2.pdf	2018-05-13 07:14	203K
[   ]	VID.-FAST-BROWN-GL.pdf	2018-05-13 07:14	203K
[   ]	VID.-FAST-BROWN-GL1.pdf	2018-05-13 07:14	203K
[   ]	VID.-FAST-BROWN.pdf	2018-05-13 07:14	203K
[   ]	VID.-FAST-BROWN1.pdf	2018-05-13 07:14	203K
[   ]	VID.-FAST-GREY-ed-2.pdf	2018-05-13 07:14	203K
[   ]	VID.-FAST-GREY-ed-21.pdf	2018-05-13 07:14	203K
[   ]	VID.-VIOLET-AG.pdf	2018-05-13 07:14	203K
[   ]	VIDERMA-ARANCIO-SR-Rev-2-030216-SI.pdf	2017-06-29 10:57	134K
[   ]	VIDERMA-BROWN-HNK-Rev-6-181215-CLP.pdf	2017-06-29 10:57	172K
[   ]	acido-acrilico_1_SE.pdf	2017-06-29 10:57	222K
[   ]	reach-2015.pdf	2017-06-29 10:57	154K
[   ]	unpac.pdf	2017-06-29 10:57	93K
Apache/2.4.25 (Debian) Server at www.dermacolor.it Port 80
}
end;

procedure TFmLoadInsum.CarregaProdutosURLGetti();
const
  //Fonte = 'https://www.getti.solutions/acabamento';
  //Fonte = 'https://www.getti.solutions//resinas';
  Fonte = 'https://docs.wixstatic.com/ugd/b0c34a_26112df0798a49358ba6e2efd1911215.pdf';
  //DestFile = 'C:\Dermatek\Teste.txt';
  DestFile = 'C:\Arcano\Teste.txt';
var
  Extraido, Restante, Texto: String;
  OK: Boolean;
  P, N: Integer;
  //
begin
  //Fonte := Edit1.Text;
  if FileExists(DestFile) then
    DeleteFile(DestFile);
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Baixando arquivo!');
  Application.ProcessMessages;
  if DmkWeb.DownloadFile(Fonte, DestFile) then
  begin
    Memo1.Lines.LoadFromFile(destFile);
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Arquivo baixado!');
  end
  else
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Erro durante o download de "' + Fonte + '"');
    Exit;
  end;
  //
(*
consulta.SQL.Text := 'SELECT * FROM dados ORDER BY nome';
consulta.Open;
with JPFpdf1 do begin
AddPage;
SetTextColor(cNavy);
SetFillColor(cYellow);
SetFont(ffHelvetica,fsBold,18);
Cell(0, 10, 'Lista de Contatos','LTBR',0,'C',1);
Ln(16);
SetFont(ffHelvetica,fsBold,12);
SetTextColor(cWhite);
SetFillColor(cBlue);
Cell(20,6,Utf8ToAnsi('C�DIGO'),'1',0,'C',1);
Cell(135,6,'NOME','1',0,'C',1);
Cell(35,6,'FONE','1',0,'C',1);
Ln(0);
SetFont(ffHelvetica,fsNormal,12);
SetTextColor(cBlack);
while not (consulta.EOF) do
begin

Cell(20,6,FormatFloat('0000',consulta.FieldByName('id').AsInteger),'1',0,'C',0);

Cell(135,6,Utf8ToAnsi(consulta.FieldByName('nome').AsString),'1',0,'L',0);
Cell(35,6,consulta.FieldByName('fone').AsString,'1',0,'C',0);
Ln(0);
consulta.Next;
end;
SetDisplayMode(dmZoom,100);
end;
// View in Browser
AResponse.ContentType := 'application/pdf';
AResponse.ContentStream := JPFpdf1.OutToBrowserView;
*)


{
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Carregando dados ');
  //
  Texto := Memo1.Text;
  Restante := '';//Texto;
  OK := True;
  N := 1;
  while OK do
  begin
    OK := ExtractText(Texto, '<tr class="tablecopy">', '</tr>', P,
    Extraido, Restante);
    Texto := Restante;
    //Geral.MB_Info(Extraido);
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Carregando dados ' + Geral.FF0(N));
    CarregaGrade(Extraido, N);
  end; // �
  MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Dados carregados na grade');
}
end;

procedure TFmLoadInsum.CarregaProdutosURLLanxess();
const
  Fonte = 'https://pt.scribd.com/document/350217221/leather-specialties-pdf';
  //DestFile = 'C:\Dermatek\Teste.txt';
  DestFile = 'C:\Arcano\Teste.txt';
var
  Extraido, Restante, Texto: String;
  OK: Boolean;
  P, N: Integer;
  //
begin
  //Fonte := Edit1.Text;
  if FileExists(DestFile) then
    DeleteFile(DestFile);
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Baixando arquivo!');
  Application.ProcessMessages;
  if DmkWeb.DownloadFile(Fonte, DestFile) then
  begin
    Memo1.Lines.LoadFromFile(destFile);
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Arquivo baixado!');
  end
  else
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Erro durante o download de "' + Fonte + '"');
    Exit;
  end;
end;

procedure TFmLoadInsum.CarregaProdutosURLLeatherQuimica;
const
  Fonte = 'http://www.leatherquimica.com/productos.php';
  //DestFile = 'C:\Dermatek\Teste.txt';
  DestFile = 'C:\Arcano\Teste.txt';
var
  Extraido, Restante, Texto: String;
  OK: Boolean;
  P, N: Integer;
  //
begin
  //Fonte := Edit1.Text;
  if FileExists(DestFile) then
    DeleteFile(DestFile);
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Baixando arquivo!');
  Application.ProcessMessages;
  if DmkWeb.DownloadFile(Fonte, DestFile) then
  begin
    Memo1.Lines.LoadFromFile(destFile);
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Arquivo baixado!');
  end
  else
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Erro durante o download de "' + Fonte + '"');
    Exit;
  end;
end;

procedure TFmLoadInsum.CarregaProdutosXlsQualquer();
  //
  procedure BaixaPDF(URL, Pasta: String; NLin: Integer);
  var
    P: Integer;
    Lixo, aTemp, DestFile: String;
    OK: Boolean;
  begin
    if Trim(URL) <> '' then
    begin
      //OK := ExtractText(Extraido, 'database/English/', '''>English</a>', P, aLink, Lixo);
      //if OK then
      begin
        //S := URL;
        try
          //Fonte    := 'https://www.atlasrefinery.com/database/English/' + aLink;
          OK := True;
          aTemp := URL;
          DestFile := '';
          while OK do
          begin
            if Pos('/', aTemp) > 0 then
            begin
              OK := ExtractText(aTemp, '/', '.pdf', P, DestFile, Lixo);
              if DestFile <> '' then
                DestFile := DestFile + '.pdf';
              aTemp := DestFile;
            end else
            begin
              OK := False;
              aTemp := '';
            end;
          end;
          DestFile := Trim(Geral.Substitui(DestFile, '%20', ' '));
          if RGPDF.ItemIndex = 1 then
          begin
            ForceDirectories(InitDir + Pasta);
            DestFile := InitDir + Pasta + '\' + DestFile;
            OK := False;
            if not FileExists(DestFile) then
              OK := DmkWeb.DownloadFile(URL, DestFile)
            else
              Ok := True;
          end
          else
            Ok := True;
          //
          if OK then
            SGSales.Cells[ColPDF, NLin] := DestFile;
        except
          ;//
        end;
      end;
    end;
  end;
  //
var
  AXLSFile, URL, Fldr: String;
  I, J: Integer;
begin

  Fldr := Trim(CBEntidadesCptt.Text);
  if MyObjects.FIC(EdEntidadesCptt.ValueVariant = 0, EdEntidadesCptt,
    'Informe a ind�stria qu�mica!') then Exit;
  if MyObjects.FIC(Fldr = '', EdEntidadesCptt,
    'Informe a ind�stria qu�mica!') then Exit;
  //
  AXLSFile := EdPath.Text;
  //
  MyObjects.Xls_To_StringGrid(SGXLS, AXLSFile, PB1, LaAviso1, LaAviso2, 1);
  SGSales.RowCount := SGXLS.RowCount -1;
  for I := 2 to SGXLS.RowCount -1 do
  begin
    SGSales.Cells[0, I-1] := Geral.FF0(I-1);
    for J := 1 to SGXLS.ColCount -1 do
      SGSales.Cells[J, I-1] := SGXLS.Cells[J, I];
  end;
  Application.ProcessMessages();
  //ver URL download
  PB1.Position := 0;
  PB1.Max := SGSales.RowCount -1;
  for I := 1 to SGSales.RowCount -1 do
  begin
    URL := Trim(SGSales.Cells[ColPDF, I]);
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
      'Baixando ' + URL);
    if URL <> '' then
      BaixaPDF(URL, Fldr, I);
    //
  end;
end;

function TFmLoadInsum.ConcatCampos(Ini, Fim: Integer): String;
var
  I, F: Integer;
begin
  Result := '';
  if Fim = 0 then
    F := FSLCampos.Count - 1
  else
    F := Fim;
  for I := Ini to F do
  begin
    Result := Result + FSLCampos[I];
  end;
end;

procedure TFmLoadInsum.dmkEdit1Redefinido(Sender: TObject);
begin
  EdMemiLinTxt.Text := Memo1.Lines[dmkEdit1.ValueVariant];
end;

function TFmLoadInsum.ExtractText(const aText, OpenTag, CloseTag: String;
  var PosicaoFinal: Integer; var TxtExtraido, TxtRestante: String): Boolean;
var
  iAux, kAux : Integer;
begin
  Result       := False;
  PosicaoFinal := 0;
  TxtExtraido  := '';
  TxtRestante  := '';
  //
  iAux := Pos(OpenTag, aText);
  kAux := Pos(CloseTag, aText);
  if (kAux <> 0) and (iAux <> 0) then
  begin
    PosicaoFinal := Pos(CloseTag, aText) + Length(CloseTag);
    //
    iAux := Pos(OpenTag, aText) + Length(OpenTag);
    kAux := Pos(CloseTag, aText);
    //
    TxtExtraido  := Copy(aText, iAux, kAux - iAux);
    TxtRestante  := Copy(aText, PosicaoFinal);

    Result := True;
  end;
end;

procedure TFmLoadInsum.FormCreate(Sender: TObject);
begin
  ColPDF := 15;
  //
  InitDir := 'C:\Arcano\TFL\';
  Memo1.Lines.Clear;
  Grl_DmkDB.AbreQuery(QrEntidadesCptt, Dmod.MyDB);
  SGSales.Cells[0, 0] := 'Seq';
  SGSales.Cells[1, 0] := 'Segmento';
  SGSales.Cells[2, 0] := 'Divisao';
  SGSales.Cells[3, 0] := 'SubDivisao';
  SGSales.Cells[4, 0] := 'Fam�lia';
  SGSales.Cells[5, 0] := 'Nome';
  SGSales.Cells[6, 0] := 'Natureza';
  SGSales.Cells[7, 0] := 'Propriedades';
  SGSales.Cells[8, 0] := 'pH';
  SGSales.Cells[9, 0] := 'CargaIonica';
  SGSales.Cells[10, 0] := 'Aspecto';
  SGSales.Cells[11, 0] := 'Utilizacao';
  SGSales.Cells[12, 0] := 'Perc_Uso';
  SGSales.Cells[13, 0] := 'ConcOuAtiv';
  SGSales.Cells[14, 0] := 'SolidezLuz';
  SGSales.Cells[ColPDF, 0] := 'LocalPDF';

  //
  SGSales.ColWidths[1] :=  32;
  SGSales.ColWidths[2] := 200;
  SGSales.ColWidths[3] := 200;
  SGSales.ColWidths[4] := 200;
  SGSales.ColWidths[5] := 300;
  SGSales.ColWidths[6] := 400;
  SGSales.ColWidths[7] := 200;
  SGSales.ColWidths[8] := 100;
  SGSales.ColWidths[9] := 100;
  SGSales.ColWidths[10] := 200;
  SGSales.ColWidths[11] := 100;
  SGSales.ColWidths[12] := 100;
  SGSales.ColWidths[13] := 100;
  SGSales.ColWidths[14] := 100;
  SGSales.ColWidths[ColPDF] := 200;
  //
  ReopenCadastros();
end;

function TFmLoadInsum.LeLinhaELimpa: String;
begin
  Readln(F, Result);
  while pos(';', Result) > 0 do
    Result := Geral.Substitui(Result, ';', '');
end;

procedure TFmLoadInsum.LimpaIQ();
begin
  EdEntidadesCptt.ValueVariant := 0;
  CBEntidadesCptt.KeyValue     := 0;
  RGPDF.ItemIndex              := 0;
end;

procedure TFmLoadInsum.Memo1Enter(Sender: TObject);
begin
  UpdateCursorPos(Memo1);
end;

procedure TFmLoadInsum.Memo1Exit(Sender: TObject);
begin
  UpdateCursorPos(Memo1);
end;

procedure TFmLoadInsum.Memo1Gesture(Sender: TObject;
  const EventInfo: TGestureEventInfo; var Handled: Boolean);
begin
  UpdateCursorPos(Memo1);
end;

procedure TFmLoadInsum.Memo1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  UpdateCursorPos(Memo1);
end;

procedure TFmLoadInsum.Memo1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  UpdateCursorPos(Memo1);
end;

procedure TFmLoadInsum.ObtemIQFamily();
begin
//
end;

procedure TFmLoadInsum.ObtemNatureza();
begin
//
end;

procedure TFmLoadInsum.ObtemNome();
begin
 //
end;

procedure TFmLoadInsum.ObtemPropriedades();
begin
  //
end;

procedure TFmLoadInsum.PMCadastrosPopup(Sender: TObject);
begin
  Setarcomopesquisado1.Enabled := RGIQ_PQ.ItemIndex = 3;
end;

procedure TFmLoadInsum.ReopenCadastros();
begin
  QrCadastros0.Close;
  QrCadastros2.Close;
  //
  case RGIQ_PQ.ItemIndex of
    0: // TODOS
      Grl_DmkDB.AbreSQLQuery0(QrCadastros0, Dmod.MyDB, [
      'SELECT cpt.Codigo entidadescptt, cpt.Nome,  ',
      'SUM(',
      'CASE WHEN Trim(cad.Nome) <> ""',
      '       THEN 1',
      '       ELSE 0',
      '       END) ITENS,',
      'SUM(',
      'CASE WHEN Trim(cad.Nome) = ""',
      '       THEN 1',
      '       ELSE 0',
      '       END) Familias',
      'FROM entidadescptt cpt ',
      'LEFT JOIN produtoscadas cad ON cpt.Codigo=cad.entidadescptt',
      'GROUP BY cpt.Codigo ',
      'ORDER BY cpt.Nome ',
      '']);
    1: // COM produtos
      Grl_DmkDB.AbreSQLQuery0(QrCadastros0, Dmod.MyDB, [
      'SELECT cad.entidadescptt, cpt.Nome,  ',
      'SUM(',
      'CASE WHEN Trim(cad.Nome) <> ""',
      '       THEN 1',
      '       ELSE 0',
      '       END) ITENS,',
      'SUM(',
      'CASE WHEN Trim(cad.Nome) = ""',
      '       THEN 1',
      '       ELSE 0',
      '       END) Familias',
      'FROM produtoscadas cad ',
      'LEFT JOIN entidadescptt cpt ON cpt.Codigo= cad.entidadescptt ',
      'WHERE TRIM(cad.Nome) <> "" ',
      'GROUP BY cad.entidadescptt ',
      'ORDER BY cpt.Nome ',
      '']);
    2: // SEM produtos
      Grl_DmkDB.AbreSQLQuery0(QrCadastros0, Dmod.MyDB, [
      'SELECT cpt.Codigo entidadescptt, cpt.Nome, ',
      'SUM(',
      'CASE WHEN Trim(cad.Nome) <> ""',
      '       THEN 1',
      '       ELSE 0',
      '       END) ITENS,',
      'SUM(',
      'CASE WHEN Trim(cad.Nome) = ""',
      '       THEN 1',
      '       ELSE 0',
      '       END) Familias',
      'FROM entidadescptt cpt ',
      'LEFT JOIN produtoscadas cad ON cad.entidadescptt=cpt.Codigo ',
      'LEFT JOIN entidadescpttpsq psq ON psq.Codigo=cpt.Codigo ',
      'WHERE cpt.Codigo > 0',
      'AND psq.Pesquisado = 0',
      'GROUP BY cpt.Codigo',
      'ORDER BY cpt.Nome ',
      '']);
    3: // N�o pesquisados
      Grl_DmkDB.AbreSQLQuery0(QrCadastros0, Dmod.MyDB, [
      'SELECT cpt.Codigo entidadescptt, cpt.Nome,  ',
      '0 ITENS, 0 Familias',
      'FROM entidadescptt cpt ',
      'LEFT JOIN entidadescpttpsq psq ON psq.Codigo=cpt.Codigo ',
      'WHERE psq.Codigo IS null',
      'AND cpt.Codigo > 0',
      'GROUP BY cpt.Codigo',
      'ORDER BY cpt.Nome ',
      '']);
  end;
  case RGIQ_PQ.ItemIndex of
    0:  DBGCadastros.DataSource := DsCadastros0;
    1:  DBGCadastros.DataSource := DsCadastros0;
    2:  DBGCadastros.DataSource := DsCadastros0;
    3:  DBGCadastros.DataSource := DsCadastros0;
    else  DBGCadastros.DataSource := DsCadastros0;
  end;
end;

procedure TFmLoadInsum.RGIQClick(Sender: TObject);
begin
  LimpaIQ();
end;

procedure TFmLoadInsum.RGIQ_PQClick(Sender: TObject);
begin
  ReopenCadastros();
end;

procedure TFmLoadInsum.SbAbreClick(Sender: TObject);
var
  EntidadesCptt: Integer;
  Indice: String;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, '');
  if RGIQ.ItemIndex = 0 then
  begin
    Geral.MB_Info('Selecione o Formato!');
    Exit;
  end;
  //
  EntidadesCptt := EdEntidadesCptt.ValueVariant;
  //
  if MyObjects.FIC(EntidadesCptt = 0, EdEntidadesCptt,
    'Informe a ind�stria qu�mica!') then Exit;
  //CpttCod := EdEntidadesCptt.ValueVariant;
  //CpttNom := CBEntidadesCptt.KeyValue;
  //
  if RGPDF.ItemIndex = 0 then
  begin
    Geral.MB_Info('Selecione "Substituir PDF se houver novo e velho:"!');
    Exit;
  end;

  Memo1.Lines.Clear;
  //LimpaIQ();
  Indice := Uppercase(Trim(RGIQ.Items[RGIQ.ItemIndex]));
  if Indice = Uppercase('MK') then
    CarregaProdutosMK()
  else
  if Indice = Uppercase('CSV') then
    CarregaProdutosCsvQualquer()
  else
  if Indice = Uppercase('XLS') then
    CarregaProdutosXlsQualquer()
  else
  if Indice = Uppercase('Atlas') then
    CarregaProdutosURLAtlas()
  else
  if Indice = Uppercase('Getti') then
    CarregaProdutosCsvGetti()
  else
  if Indice = Uppercase('Corichem') then
    CarregaProdutosURLCorichem()
  else
  if Indice = Uppercase('Corium (pdf)') then
    CarregaProdutosPdfCorium()
  else
  if Indice = Uppercase('Dermacolor') then
    CarregaProdutosURLDermacolor()
  else
  if Indice = Uppercase('LF') then
    CarregaProdutosTxtLF()
  //
  // ...
  //
  else Geral.MB_Info('"RGIQ.ItemIndex n�o implementado: ' + Indice + '!');
end;

procedure TFmLoadInsum.SbSelArqClick(Sender: TObject);
var
  Arquivo: String;
begin
  //Arquivos OFC|*.ofc;Arquivos OFX|*.ofx
  if MyObjects.FileOpenDialog(Self, InitDir, '', 'Sele��o de arquivo',
  'Arquivos CSV|*.csv;Arquivos TXT|*.txt;Arquivos XLS|*.xls', [], Arquivo) then
    EdPath.ValueVariant := Arquivo
  else
    EdPath.ValueVariant := '';
end;

function TFmLoadInsum.SentencaCampos(Ini, Fim: Integer): String;
var
  I, F: Integer;
begin
  Result := '';
  if Fim = 0 then
    F := FSLCampos.Count - 1
  else
    F := Fim;
  for I := Ini to F do
  begin
    Result := Trim(Result + ' ' + FSLCampos[I]);
  end;
end;

procedure TFmLoadInsum.Setarcomopesquisado1Click(Sender: TObject);
var
  Query: TZQuery;
  EntidadesCptt, Pesquisado: Integer;
begin
  Query := TZQuery(TDataSource(DBGCadastros.DataSource).DataSet);
  EntidadesCptt := Query.FieldByName('EntidadesCptt').AsInteger;
  Pesquisado := 0;
  //
  CadastraEntidadesCpttPsq(EntidadesCptt, EntidadesCptt, Pesquisado);
end;

procedure TFmLoadInsum.SpeedButton1Click(Sender: TObject);
const
  EdCodigo = nil;
  EdCodUsu = nil;
var
  Linha: Integer;
var
  Nome, IQFamily, Natureza, Propriedades, pH, CargaIonica, Aspecto, Perc_Uso,
  ConcOuAtiv, LocalPDF, IQSegment, IQDivSao, IQDivSub, Utilizacao,
  SolidezLuz: String;
  Lista, Codigo, CodUsu, IDServr, EntidadesCptt, ProdCabec, GrupoSegmento,
  Segmento, Divisao, SubDivisao, Familia, HvSrDB, Diluicao, Boca, Pesquisado:
  Integer;
  //
  SQLType: TSQLType;
begin
  SQLType             := stIns; //ImgTipo.SQLType;
  PB1.Position := 0;
  PB1.Max := SGSales.RowCount - 1;
  //
  EntidadesCptt       := EdEntidadesCptt.ValueVariant;
  Lista               := Integer(TLstProdQuim.lpqSimilar); // 2
  IDServr             := VAR_IDServr;
  HvSrDB              := Integer(THaveSrcDB.hsdbImported);
  ProdCabec           := 0;
  GrupoSegmento       := 0;
  Segmento            := 0;
  Divisao             := 0;
  SubDivisao          := 0;
  Familia             := 0;
  Diluicao            := 0;
  Boca                := 0;
  //
  if MyObjects.FIC(EntidadesCptt = 0, EdEntidadesCptt,
    'Informe a ind�stria qu�mica!') then Exit;
  //
  for Linha := 1 to SGSales.RowCount -1 do
  begin
    MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
    //
    Codigo              := 0;
    CodUsu              := 0;
    IQSegment           := Trim(SGSales.Cells[1, Linha]);
    IQDivSao            := Trim(SGSales.Cells[2, Linha]);
    IQDivSub            := Trim(SGSales.Cells[3, Linha]);
    IQFamily            := Trim(SGSales.Cells[4, Linha]); //ObtemIQFamily();
    Nome                := Trim(SGSales.Cells[5, Linha]); //ObtemNome();
    Natureza            := Trim(SGSales.Cells[6, Linha]); //ObtemNatureza();
    Propriedades        := Trim(SGSales.Cells[7, Linha]); //ObtemPropriedades();
    pH                  := Trim(SGSales.Cells[8, Linha]);
    CargaIonica         := Trim(SGSales.Cells[9, Linha]);
    Aspecto             := Trim(SGSales.Cells[10, Linha]);
    Utilizacao          := Trim(SGSales.Cells[11, Linha]);
    Perc_Uso            := Trim(SGSales.Cells[12, Linha]);
    ConcOuAtiv          := Trim(SGSales.Cells[13, Linha]);
    SolidezLuz          := Trim(SGSales.Cells[14, Linha]);
    LocalPDF            := Trim(SGSales.Cells[ColPDF, Linha]);
    //
    Nome                := Geral.Substitui(Nome, '�', '');
    Nome                := Geral.Substitui(Nome, '�', '');
    if (Nome <> '') or (IQFamily <> '') then
    begin
      Grl_DmkDB.AbreSQLQuery0(Query1, Dmod.MyDB, [
      'SELECT Codigo ',
      'FROM produtoscadas ',
      'WHERE EntidadesCptt=' + Geral.FF0(EntidadesCptt),
      'AND LOWER(Nome)=LOWER("' + Nome + '")',
      'AND LOWER(IQDivSub)=LOWER("' + IQFamily + '")',
      '']);
      if Query1.RecordCount = 0 then
      begin
        Codigo := Grl_DmkDB.GetNxtCodigoInt_MulFld('produtoscadas', 'Codigo',
          SQLType, 0, ['Lista'], [Lista]);
        CodUsu := Grl_DmkDB.GetNxtCodigoInt_MulFld('produtoscadas', 'CodUsu',
          SQLType, 0, ['Lista'], [Lista]);
        //Grl_DmkDB.ObtemCodigoECodUsu(EdCodigo, EdCodUsu, SQLType, Codigo, CodUsu,
          //'produtoscadas', 'Codigo', 'CodUsu');
        //ou > ? := UMyMod.BPGS1I32('produtoscadas', 'Lista', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
        //if
        Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, 'produtoscadas', False, [
(*
        'CodUsu', 'IDServr',
        'EntidadesCptt', 'ProdCabec', 'GrupoSegmento',
        'Segmento', 'Divisao', 'SubDivisao',
        'Familia', 'Nome', 'HvSrDB',
        'Diluicao', 'Boca',
        'IQFamily', 'Natureza', 'Propriedades'], [
        'Codigo', 'Lista'], [
        CodUsu, IDServr,
        EntidadesCptt, ProdCabec, GrupoSegmento,
        Segmento, Divisao, SubDivisao,
        Familia, Nome, HvSrDB,
        Diluicao, Boca,
        IQFamily, Natureza, Propriedades], [
*)
        'IQSegment', 'IQDivSao', 'IQDivSub',
        'LocalPDF', 'CodUsu', 'IDServr',
        'EntidadesCptt', 'ProdCabec', 'GrupoSegmento',
        'Segmento', 'Divisao', 'SubDivisao',
        'Familia', 'Nome', 'HvSrDB',
        'Diluicao', 'Boca', 'IQFamily',
        'Natureza', 'Propriedades', 'pH',
        'Aspecto', 'Perc_Uso', 'ConcOuAtiv',
        'CargaIonica', 'Utilizacao', 'SolidezLuz'], [
        'Codigo', 'Lista'], [
        IQSegment, IQDivSao, IQDivSub,
        LocalPDF, CodUsu, IDServr,
        EntidadesCptt, ProdCabec, GrupoSegmento,
        Segmento, Divisao, SubDivisao,
        Familia, Nome, HvSrDB,
        Diluicao, Boca, IQFamily,
        Natureza, Propriedades, pH,
        Aspecto, Perc_Uso, ConcOuAtiv,
        CargaIonica, Utilizacao, SolidezLuz], [
        Codigo, Lista], True, dmksqlinsInsOnly, '', TDeviceType.stDesktop, False);
      end;
    end;
  end;
  Grl_DmkDB.AbreSQLQuery0(Query1, Dmod.MyDB, [
  'SELECT COUNT(cad.Codigo) ITENS ',
  'FROM produtoscadas cad ',
  'WHERE cad.entidadescptt=' + Geral.FF0(EntidadesCptt),
  'AND TRIM(Nome) <> ""',
  '']);
  Pesquisado := Query1.FieldByName('ITENS').AsInteger;
  //
  CadastraEntidadesCpttPsq(EntidadesCptt, EntidadesCptt, Pesquisado);
  //
  ReopenCadastros();
  MyObjects.LimpaGrade(SGSales, 1, 1, True);
  //
  LimpaIQ();
end;

procedure TFmLoadInsum.UpdateCursorPos(Control: TCustomEdit); //TWinControl);
function RichRow(m: TCustomMemo): Longint;
begin
  Result := m.PerForm(EM_LINEFROMCHAR,m.SelStart,0);
end;

function RichCol(m: TCustomMemo; Row: Integer): Longint;
begin
  Result := m.SelStart-m.Perform(EM_LINEINDEX,Row,0);
end;
const
  sColRowInfo = 'Linha: %3d   Coluna: %3d';
var
  CharPos: TPoint;
var
  Memo: TMemo;
  Row: Integer;
  Col: Integer;
begin
  Memo := TMemo(Control);
  Row := RichRow(Memo);
  Col := RichCol(Memo, Row);
  Caption := Format('%d : %d', [Row, Col]);
  StatusBar.Panels[0].Text := Format('%d : %d', [Row, Col]);
           {
  CharPos.Y := SendMessage(Control.Handle, EM_EXLINEFROMCHAR, 0,
    Control.SelStart);
  CharPos.X := (Control.SelStart -
    SendMessage(Control.Handle, EM_LINEINDEX, CharPos.Y, 0));
  Inc(CharPos.Y);
  Inc(CharPos.X);
  StatusBar.Panels[0].Text := Format(sColRowInfo, [CharPos.Y, CharPos.X])};
end;

procedure TFmLoadInsum.UpdateInfo(nI: Integer);
begin
  if nI >= Cento then
  begin
    FilPos := FilePos(F);
    FilPer := (FilPos / FilTam) * 100;
    MyObjects.Informa2(LaAviso1, LaAviso2, False,
    Geral.FF0(Cento) + ' registros carregados. ' +
    Copy(FloatToStr(FilPer), 1, 5) + '%.');
    //
    Cento := Cento + 100;
  end;
end;

{
http://www.italiantannerysuppliers.it/listing-banner/?filter=all&search_text=&type=leather-chemicals-wet-phase&location=&geo=on&geo_location_lat=&geo_location_long=&directory_categories=pigments&pagination=10&search_page=10&cs_loc_max_input=184&cs_loc_incr_step=2218&goe_location_enable=0&submit=

https://consumer.dow.com/en-us/industry/ind-leather-textiles.html
https://www.dow.com/en-us/microbial/markets/industrial-preservation/leather
https://consumer.dow.com/en-us/search.html?_=1538357757793&i=1&page=2&q=*&q11=products&q4=en_US&q45=yes%7Ctrue&q50=DE%7Cpublic&q51=public&q52=public&q53=public&q54=Leather+and+Textiles&q55=Leather+and+Textiles~2F~2FLeather&q56=Leather+and+Textiles~2F~2FLeather~2F~2FFinishing&tab=products&x11=category&x4=locale&x45=searchAllowed&x50=sellableCountry&x51=customerType&x52=baseRole&x53=hierarchy&x54=industry&x55=market&x56=application&tab=products

}





end.



