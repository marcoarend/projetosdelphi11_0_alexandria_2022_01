unit ReceiGruNivs;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, Db, mySQLDbTables,
  dmkPermissoes, dmkGeral, dmkImage, UnDMkEnums, UnDmkProcFunc,
  ZAbstractRODataset, ZAbstractDataset, ZAbstractTable, ZDataset;

type
  THackDBGrid = class(TDBGrid);
  TFmReceiGruNivs = class(TForm)
    DsReceiGruNiv5: TDataSource;
    Panel1: TPanel;
    Panel2: TPanel;
    Splitter1: TSplitter;
    DsReceiGruNiv4: TDataSource;
    DsReceiGruNiv3: TDataSource;
    DsReceiGruNiv2: TDataSource;
    DsReceiGruNiv1: TDataSource;
    Panel3: TPanel;
    TbReceiGruNiv5: TZTable;
    TbReceiGruNiv4: TZTable;
    TbReceiGruNiv3: TZTable;
    TbReceiGruNiv2: TZTable;
    TbReceiGruNiv1: TZTable;
    QrLoc: TZQuery;
    QrNNiv4: TZQuery;
    DsExtra: TDataSource;
    QrNNiv4Nome: TWideStringField;
    QrNNiv3: TZQuery;
    QrNNiv2: TZQuery;
    QrNNiv1: TZQuery;
    QrNNiv4NOME_PAI: TWideStringField;
    PnExtra: TPanel;
    DBGExtra: TDBGrid;
    StaticText1: TStaticText;
    mySQLUpdateSQL1: TmySQLUpdateSQL;
    QrTNiv5: TZQuery;
    QrTNiv4: TZQuery;
    QrTNiv3: TZQuery;
    QrTNiv2: TZQuery;
    dmkPermissoes1: TdmkPermissoes;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    BtOK: TBitBtn;
    BtCancela: TBitBtn;
    Splitter2: TSplitter;
    Splitter3: TSplitter;
    Splitter4: TSplitter;
    SbPlano: TBitBtn;
    SbConjuntos: TBitBtn;
    SbGrupos: TBitBtn;
    SbSubgrupos: TBitBtn;
    SbContas: TBitBtn;
    Panel6: TPanel;
    DBGNiv5: TDBGrid;
    StaticText2: TStaticText;
    Panel7: TPanel;
    DBGNiv4: TDBGrid;
    StaticText3: TStaticText;
    Panel8: TPanel;
    DBGNiv3: TDBGrid;
    StaticText4: TStaticText;
    Panel9: TPanel;
    DBGNiv2: TDBGrid;
    StaticText5: TStaticText;
    Panel10: TPanel;
    DBGNiv1: TDBGrid;
    StaticText6: TStaticText;
    BtImpExp: TBitBtn;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrTNiv5Codigo: TLargeintField;
    QrTNiv5Nome: TWideStringField;
    QrTNiv4Codigo: TLargeintField;
    QrTNiv4Nome: TWideStringField;
    QrTNiv3Codigo: TLargeintField;
    QrTNiv3Nome: TWideStringField;
    QrTNiv2Codigo: TLargeintField;
    QrTNiv2Nome: TWideStringField;
    TbReceiGruNiv5Codigo: TLargeintField;
    TbReceiGruNiv5CodUsu: TLargeintField;
    TbReceiGruNiv5IDServr: TLargeintField;
    TbReceiGruNiv5PaiCod: TLargeintField;
    TbReceiGruNiv5PaiUsu: TLargeintField;
    TbReceiGruNiv5Nome: TWideStringField;
    TbReceiGruNiv5Lk: TLargeintField;
    TbReceiGruNiv5DataCad: TDateField;
    TbReceiGruNiv5DataAlt: TDateField;
    TbReceiGruNiv5UserCad: TLargeintField;
    TbReceiGruNiv5UserAlt: TLargeintField;
    TbReceiGruNiv5AlterWeb: TSmallintField;
    TbReceiGruNiv5Ativo: TSmallintField;
    TbReceiGruNiv4Codigo: TLargeintField;
    TbReceiGruNiv4CodUsu: TLargeintField;
    TbReceiGruNiv4IDServr: TLargeintField;
    TbReceiGruNiv4PaiCod: TLargeintField;
    TbReceiGruNiv4PaiUsu: TLargeintField;
    TbReceiGruNiv4Nome: TWideStringField;
    TbReceiGruNiv4Lk: TLargeintField;
    TbReceiGruNiv4DataCad: TDateField;
    TbReceiGruNiv4DataAlt: TDateField;
    TbReceiGruNiv4UserCad: TLargeintField;
    TbReceiGruNiv4UserAlt: TLargeintField;
    TbReceiGruNiv4AlterWeb: TSmallintField;
    TbReceiGruNiv4Ativo: TSmallintField;
    TbReceiGruNiv3Codigo: TLargeintField;
    TbReceiGruNiv3CodUsu: TLargeintField;
    TbReceiGruNiv3IDServr: TLargeintField;
    TbReceiGruNiv3PaiCod: TLargeintField;
    TbReceiGruNiv3PaiUsu: TLargeintField;
    TbReceiGruNiv3Nome: TWideStringField;
    TbReceiGruNiv3Lk: TLargeintField;
    TbReceiGruNiv3DataCad: TDateField;
    TbReceiGruNiv3DataAlt: TDateField;
    TbReceiGruNiv3UserCad: TLargeintField;
    TbReceiGruNiv3UserAlt: TLargeintField;
    TbReceiGruNiv3AlterWeb: TSmallintField;
    TbReceiGruNiv3Ativo: TSmallintField;
    TbReceiGruNiv2Codigo: TLargeintField;
    TbReceiGruNiv2CodUsu: TLargeintField;
    TbReceiGruNiv2IDServr: TLargeintField;
    TbReceiGruNiv2PaiCod: TLargeintField;
    TbReceiGruNiv2PaiUsu: TLargeintField;
    TbReceiGruNiv2Nome: TWideStringField;
    TbReceiGruNiv2Lk: TLargeintField;
    TbReceiGruNiv2DataCad: TDateField;
    TbReceiGruNiv2DataAlt: TDateField;
    TbReceiGruNiv2UserCad: TLargeintField;
    TbReceiGruNiv2UserAlt: TLargeintField;
    TbReceiGruNiv2AlterWeb: TSmallintField;
    TbReceiGruNiv2Ativo: TSmallintField;
    TbReceiGruNiv1Codigo: TLargeintField;
    TbReceiGruNiv1CodUsu: TLargeintField;
    TbReceiGruNiv1IDServr: TLargeintField;
    TbReceiGruNiv1PaiCod: TLargeintField;
    TbReceiGruNiv1PaiUsu: TLargeintField;
    TbReceiGruNiv1Nome: TWideStringField;
    TbReceiGruNiv1Lk: TLargeintField;
    TbReceiGruNiv1DataCad: TDateField;
    TbReceiGruNiv1DataAlt: TDateField;
    TbReceiGruNiv1UserCad: TLargeintField;
    TbReceiGruNiv1UserAlt: TLargeintField;
    TbReceiGruNiv1AlterWeb: TSmallintField;
    TbReceiGruNiv1Ativo: TSmallintField;
    QrNNiv4Codigo: TLargeintField;
    QrNNiv3Codigo: TLargeintField;
    QrNNiv3Nome: TWideStringField;
    QrNNiv3NOME_PAI: TWideStringField;
    QrNNiv2Codigo: TLargeintField;
    QrNNiv2Nome: TWideStringField;
    QrNNiv2NOME_PAI: TWideStringField;
    QrNNiv1Codigo: TLargeintField;
    QrNNiv1Nome: TWideStringField;
    QrNNiv1NOME_PAI: TWideStringField;
    Panel11: TPanel;
    CkOcultaCadSistema: TCheckBox;
    CkDragDrop: TCheckBox;
    TbReceiGruNiv5Ordem: TLargeintField;
    TbReceiGruNiv4Ordem: TLargeintField;
    TbReceiGruNiv3Ordem: TLargeintField;
    TbReceiGruNiv2Ordem: TLargeintField;
    TbReceiGruNiv1Ordem: TLargeintField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrPlanoAfterScroll(DataSet: TDataSet);
    procedure QrConjuAfterScroll(DataSet: TDataSet);
    procedure QrGrupoAfterScroll(DataSet: TDataSet);
    procedure QrSubGrAfterScroll(DataSet: TDataSet);
    procedure TbReceiGruNiv5AfterScroll(DataSet: TDataSet);
    procedure TbReceiGruNiv5BeforePost(DataSet: TDataSet);
    procedure TbReceiGruNiv4BeforePost(DataSet: TDataSet);
    procedure TbReceiGruNiv3BeforePost(DataSet: TDataSet);
    procedure TbReceiGruNiv2BeforePost(DataSet: TDataSet);
    procedure TbReceiGruNiv1BeforePost(DataSet: TDataSet);
    procedure TbReceiGruNiv4AfterScroll(DataSet: TDataSet);
    procedure TbReceiGruNiv3AfterScroll(DataSet: TDataSet);
    procedure TbReceiGruNiv2AfterScroll(DataSet: TDataSet);
    procedure CkOcultaCadSistemaClick(Sender: TObject);
    procedure TbReceiGruNiv4AfterEdit(DataSet: TDataSet);
    procedure TbReceiGruNiv4AfterInsert(DataSet: TDataSet);
    procedure DBGExtraCellClick(Column: TColumn);
    procedure DBGNiv5DragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure DBGNiv5DragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure DBGNiv4CellClick(Column: TColumn);
    procedure DBGNiv3CellClick(Column: TColumn);
    procedure DBGNiv4DragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure DBGNiv4DragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure DBGNiv2CellClick(Column: TColumn);
    procedure DBGNiv3DragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure DBGNiv3DragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure DBGNiv1CellClick(Column: TColumn);
    procedure DBGNiv2DragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure DBGNiv2DragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure TbReceiGruNiv5BeforeInsert(DataSet: TDataSet);
    procedure TbReceiGruNiv5AfterPost(DataSet: TDataSet);
    procedure TbReceiGruNiv5AfterCancel(DataSet: TDataSet);
    procedure TbReceiGruNiv4BeforeInsert(DataSet: TDataSet);
    procedure TbReceiGruNiv4AfterPost(DataSet: TDataSet);
    procedure TbReceiGruNiv4AfterCancel(DataSet: TDataSet);
    procedure TbReceiGruNiv3BeforeInsert(DataSet: TDataSet);
    procedure TbReceiGruNiv3AfterPost(DataSet: TDataSet);
    procedure TbReceiGruNiv3AfterCancel(DataSet: TDataSet);
    procedure TbReceiGruNiv2BeforeInsert(DataSet: TDataSet);
    procedure TbReceiGruNiv2AfterPost(DataSet: TDataSet);
    procedure TbReceiGruNiv2AfterCancel(DataSet: TDataSet);
    procedure DBGNiv4Enter(Sender: TObject);
    procedure DBGNiv5Enter(Sender: TObject);
    procedure DBGNiv3Enter(Sender: TObject);
    procedure DBGNiv2Enter(Sender: TObject);
    procedure DBGNiv1Enter(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure CkDragDropClick(Sender: TObject);
    procedure TbReceiGruNiv5BeforeDelete(DataSet: TDataSet);
    procedure TbReceiGruNiv4BeforeDelete(DataSet: TDataSet);
    procedure TbReceiGruNiv3BeforeDelete(DataSet: TDataSet);
    procedure TbReceiGruNiv2BeforeDelete(DataSet: TDataSet);
    procedure TbReceiGruNiv1BeforeDelete(DataSet: TDataSet);
    procedure TbReceiGruNiv1CalcFields(DataSet: TDataSet);
    procedure DBGNiv1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGNiv1ColEnter(Sender: TObject);
    procedure DBGNiv1ColExit(Sender: TObject);
    procedure SbPlanoClick(Sender: TObject);
    procedure SbConjuntosClick(Sender: TObject);
    procedure SbGruposClick(Sender: TObject);
    procedure SbSubgruposClick(Sender: TObject);
    procedure SbContasClick(Sender: TObject);
    procedure BtImpExpClick(Sender: TObject);
    procedure DBGNiv5DblClick(Sender: TObject);
    procedure DBGNiv4DblClick(Sender: TObject);
    procedure DBGNiv3DblClick(Sender: TObject);
    procedure DBGNiv2DblClick(Sender: TObject);
    procedure DBGNiv1DblClick(Sender: TObject);
    procedure TbReceiGruNiv3AfterRefresh(DataSet: TDataSet);
    procedure TbReceiGruNiv4AfterRefresh(DataSet: TDataSet);
    procedure TbReceiGruNiv4AfterOpen(DataSet: TDataSet);
    procedure TbReceiGruNiv3AfterOpen(DataSet: TDataSet);
    procedure TbReceiGruNiv2AfterOpen(DataSet: TDataSet);
    procedure TbReceiGruNiv2AfterRefresh(DataSet: TDataSet);
    procedure TbReceiGruNiv5AfterOpen(DataSet: TDataSet);
    procedure TbReceiGruNiv5AfterRefresh(DataSet: TDataSet);
    procedure TbReceiGruNiv1BeforeInsert(DataSet: TDataSet);
    //
  private
    { Private declarations }
    FInGrade: Boolean;
    FMySrc: TDBGrid;
    function  AceitaDragOver(Source: TObject; Grade: String;
              Tabela: TZQuery): Boolean;
    procedure CancelaSemPai(DataSet: TDataSet);
    procedure MostraPlanos();
    procedure MostraConjuntos();
    procedure MostraGrupos();
    procedure MostraSubgrupos();
    procedure MostraContas();
    procedure RefreshNiv1();
    procedure RefreshNiv2();
    procedure RefreshNiv3();
    procedure RefreshNiv4();
    procedure RenomeiaTituloExtra(TitSon, TitPai: String);
    procedure VeSeAceitaDragDrop(Sender, Source: TObject; X, Y: Integer;
              TitPai, TitSon, Tabela, Campo: String);
  public
    { Public declarations }
  end;

  var
  FmReceiGruNivs: TFmReceiGruNivs;

implementation

{$R *.DFM}

uses UnMyObjects, Module, UnInternalConsts, Principal, UnGrl_DmkDB, MyVCLSkin;

procedure TFmReceiGruNivs.BtImpExpClick(Sender: TObject);
begin
(***
  FinanceiroJan.MostraPlanoImpExp;
  //
  TbPlano.Close;
  Grl_DmkDB.AbreTable(TbPlano, Dmod.MyDB);
  //
  TbConju.Close;
  Grl_DmkDB.AbreTable(TbConju, Dmod.MyDB);
  //
  TbGrupo.Close;
  Grl_DmkDB.AbreTable(TbGrupo, Dmod.MyDB);
  //
  TbSubgr.Close;
  Grl_DmkDB.AbreTable(TbSubgr, Dmod.MyDB);
  //
  TbConta.Close;
  Grl_DmkDB.AbreTable(TbConta, Dmod.MyDB);
***)
end;

procedure TFmReceiGruNivs.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmReceiGruNivs.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmReceiGruNivs.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmReceiGruNivs.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FMySrc := nil;
  PnExtra.Visible := False;
  //ReopenPlano(0);
  Grl_DmkDB.AbreQuery(QrTNiv5, Dmod.MyDB);
  Grl_DmkDB.AbreQuery(QrTNiv4, Dmod.MyDB);
  Grl_DmkDB.AbreQuery(QrTNiv3, Dmod.MyDB);
  Grl_DmkDB.AbreQuery(QrTNiv2, Dmod.MyDB);
  //
  Grl_DmkDB.AbreTableZ(TbReceiGruNiv5, Dmod.MyDB);
  Grl_DmkDB.AbreTableZ(TbReceiGruNiv4, Dmod.MyDB);
  Grl_DmkDB.AbreTableZ(TbReceiGruNiv3, Dmod.MyDB);
  Grl_DmkDB.AbreTableZ(TbReceiGruNiv2, Dmod.MyDB);
  Grl_DmkDB.AbreTableZ(TbReceiGruNiv1, Dmod.MyDB);
end;

procedure TFmReceiGruNivs.QrPlanoAfterScroll(DataSet: TDataSet);
begin
  //ReopenConju(0);
end;

procedure TFmReceiGruNivs.QrConjuAfterScroll(DataSet: TDataSet);
begin
  //ReopenGrupo(0);
end;

procedure TFmReceiGruNivs.QrGrupoAfterScroll(DataSet: TDataSet);
begin
  //ReopenSubGr(0);
end;

procedure TFmReceiGruNivs.QrSubGrAfterScroll(DataSet: TDataSet);
begin
  //ReopenConta(0);
end;

procedure TFmReceiGruNivs.TbReceiGruNiv5BeforePost(DataSet: TDataSet);
var
  SQLType: TSQLType;
  Codigo: Integer;
begin
  if (TbReceiGruNiv5.State = dsInsert) and (TbReceiGruNiv5Codigo.Value = 0) then
  begin
    TbReceiGruNiv5Codigo.ReadOnly := False;
    Codigo :=  Grl_DmkDB.GetNxtCodigoInt('receigruniv5', 'Codigo', SQLType, 0);
    TbReceiGruNiv5Codigo.Value := Codigo;
    TbReceiGruNiv5CodUsu.Value := Codigo;
    TbReceiGruNiv5IDServr.Value := VAR_IDServr;
    TbReceiGruNiv5Codigo.ReadOnly := True;
  end;
end;

procedure TFmReceiGruNivs.TbReceiGruNiv4BeforePost(DataSet: TDataSet);
var
  SQLType: TSQLType;
  Codigo: Integer;
begin
  if (TbReceiGruNiv4.State = dsInsert) then
  begin
    if (TbReceiGruNiv4Codigo.Value = 0) then
    begin
      TbReceiGruNiv4Codigo.ReadOnly := False;
      Codigo :=  Grl_DmkDB.GetNxtCodigoInt('receigruniv4', 'Codigo', SQLType, 0);
      TbReceiGruNiv4Codigo.Value := Codigo;
      TbReceiGruNiv4CodUsu.Value := Codigo;
      TbReceiGruNiv4PaiCod.Value := TbReceiGruNiv5Codigo.Value;
      TbReceiGruNiv4PaiUsu.Value := TbReceiGruNiv5CodUsu.Value;
      TbReceiGruNiv4IDServr.Value := VAR_IDServr;
      TbReceiGruNiv4Codigo.ReadOnly := True;
    end;
  end else
  begin
    if TbReceiGruNiv4Codigo.Value = 0 then TbReceiGruNiv4PaiCod.Value := 0;
    if Grl_DmkDB.SQLLoc1(Dmod.QrUpd, 'receigruniv5', 'Codigo', TbReceiGruNiv4PaiCod.Value, '',
    'O Nivel 5 n� ' + IntToStr(TbReceiGruNiv4PaiCod.Value) + ' n�o existe!') = 0 then
      TbReceiGruNiv4.Cancel;
  end;
end;

procedure TFmReceiGruNivs.TbReceiGruNiv3BeforePost(DataSet: TDataSet);
var
  SQLType: TSQLType;
  Codigo: Integer;
begin
  if (TbReceiGruNiv3.State = dsInsert) then
  begin
    if (TbReceiGruNiv3Codigo.Value = 0) then
    begin
      TbReceiGruNiv3Codigo.ReadOnly := False;
      Codigo :=  Grl_DmkDB.GetNxtCodigoInt('receigruniv3', 'Codigo', SQLType, 0);
      TbReceiGruNiv3Codigo.Value := Codigo;
      TbReceiGruNiv3CodUsu.Value := Codigo;
      TbReceiGruNiv3PaiCod.Value := TbReceiGruNiv4Codigo.Value;
      TbReceiGruNiv3PaiUsu.Value := TbReceiGruNiv4CodUsu.Value;
      TbReceiGruNiv3IDServr.Value := VAR_IDServr;
      TbReceiGruNiv3Codigo.ReadOnly := True;
    end;
  end else
  begin
    if TbReceiGruNiv3Codigo.Value = 0 then TbReceiGruNiv3PaiCod.Value := 0;
    if Grl_DmkDB.SQLLoc1(Dmod.QrUpd, 'receigruniv4', 'Codigo', TbReceiGruNiv3PaiCod.Value, '',
    'O Nivel 4 n� ' + IntToStr(TbReceiGruNiv3PaiCod.Value) + ' n�o existe!') = 0 then
      TbReceiGruNiv3.Cancel;
  end;
end;

procedure TFmReceiGruNivs.TbReceiGruNiv2BeforePost(DataSet: TDataSet);
var
  SQLType: TSQLType;
  Codigo: Integer;
begin
  if (TbReceiGruNiv2.State = dsInsert) then
  begin
    if (TbReceiGruNiv2Codigo.Value = 0) then
    begin
      TbReceiGruNiv2Codigo.ReadOnly := False;
      Codigo :=  Grl_DmkDB.GetNxtCodigoInt('receigruniv2', 'Codigo', SQLType, 0);
      TbReceiGruNiv2Codigo.Value := Codigo;
      TbReceiGruNiv2CodUsu.Value := Codigo;
      TbReceiGruNiv2PaiCod.Value := TbReceiGruNiv3Codigo.Value;
      TbReceiGruNiv2PaiUsu.Value := TbReceiGruNiv3CodUsu.Value;
      TbReceiGruNiv2IDServr.Value := VAR_IDServr;
      TbReceiGruNiv2Codigo.ReadOnly := True;
    end;
  end else
  begin
    if TbReceiGruNiv2Codigo.Value = 0 then TbReceiGruNiv2PaiCod.Value := 0;
    if Grl_DmkDB.SQLLoc1(Dmod.QrUpd, 'receigruniv3', 'Codigo', TbReceiGruNiv2PaiCod.Value, '',
    'O Nivel 3 n� ' + IntToStr(TbReceiGruNiv2PaiCod.Value) + ' n�o existe!') = 0 then
      TbReceiGruNiv2.Cancel;
  end;
end;

procedure TFmReceiGruNivs.TbReceiGruNiv1BeforePost(DataSet: TDataSet);
var
  SQLType: TSQLType;
  Codigo: Integer;
begin
  if (TbReceiGruNiv1.State = dsInsert) then
  begin
    if (TbReceiGruNiv1Codigo.Value = 0) then
    begin
      TbReceiGruNiv1Codigo.ReadOnly := False;
      Codigo :=  Grl_DmkDB.GetNxtCodigoInt('receigruniv1', 'Codigo', SQLType, 0);
      TbReceiGruNiv1Codigo.Value := Codigo;
      TbReceiGruNiv1CodUsu.Value := Codigo;
      TbReceiGruNiv1PaiCod.Value := TbReceiGruNiv2Codigo.Value;
      TbReceiGruNiv1PaiUsu.Value := TbReceiGruNiv2CodUsu.Value;
      TbReceiGruNiv1IDServr.Value := VAR_IDServr;
      TbReceiGruNiv1Codigo.ReadOnly := True;
    end;
  end else
  begin
    if TbReceiGruNiv1Codigo.Value = 0 then TbReceiGruNiv1PaiCod.Value := 0;
    if Grl_DmkDB.SQLLoc1(Dmod.QrUpd, 'receigruniv2', 'Codigo', TbReceiGruNiv1PaiCod.Value, '',
    'O Nivel 2 n� ' + IntToStr(TbReceiGruNiv1PaiCod.Value) + ' n�o existe!') = 0 then
      TbReceiGruNiv1.Cancel;
  end;
end;

procedure TFmReceiGruNivs.TbReceiGruNiv5AfterScroll(DataSet: TDataSet);
begin
  RefreshNiv4();
end;

procedure TFmReceiGruNivs.TbReceiGruNiv4AfterScroll(DataSet: TDataSet);
begin
  RefreshNiv3();
end;

procedure TFmReceiGruNivs.TbReceiGruNiv3AfterScroll(DataSet: TDataSet);
begin
  RefreshNiv2();
end;

procedure TFmReceiGruNivs.TbReceiGruNiv2AfterScroll(DataSet: TDataSet);
begin
  RefreshNiv1();
end;

procedure TFmReceiGruNivs.CkOcultaCadSistemaClick(Sender: TObject);
begin
  if CkOcultaCadSistema.Checked then
    TbReceiGruNiv5.Filter := 'Codigo>0'
  else
    TbReceiGruNiv5.Filter := '';
end;

procedure TFmReceiGruNivs.TbReceiGruNiv4AfterEdit(DataSet: TDataSet);
begin
  //if TbReceiGruNiv4Codigo.Value = 0 then
    //TbReceiGruNiv4.Cancel;
end;

procedure TFmReceiGruNivs.TbReceiGruNiv4AfterInsert(DataSet: TDataSet);
begin
  //if TbReceiGruNivCodigo.Value <> 0 then
  //Abort;
end;

procedure TFmReceiGruNivs.TbReceiGruNiv4AfterOpen(DataSet: TDataSet);
begin
  RefreshNiv3();
end;

////////////////////////////////////////////////////////////////////////////////
//  Mandar do conjunto para o plano:
////////////////////////////////////////////////////////////////////////////////

procedure TFmReceiGruNivs.DBGNiv4CellClick(Column: TColumn);
begin
(***
  if Column.FieldName = '_S' then
  begin
    Screen.Cursor := crHourGlass;
    TbReceiGruNiv4.Edit;
    if TbReceiGruNiv4_S.Value = 1 then
      TbReceiGruNiv4CtrlaSdo.Value := 0
    else
      TbReceiGruNiv4CtrlaSdo.Value := 1;
    TbReceiGruNiv4.Post;
    Screen.Cursor := crDefault;
  end else
***)
  if FInGrade then
    FInGrade := False
  else
    if CkDragDrop.Checked then DBGNiv4.BeginDrag(True);
end;

procedure TFmReceiGruNivs.DBGNiv5DragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept := AceitaDragOver(Source, 'DBGConju', QrNNiv4);
end;

procedure TFmReceiGruNivs.DBGNiv5DblClick(Sender: TObject);
begin
  MostraPlanos();
end;

procedure TFmReceiGruNivs.DBGNiv5DragDrop(Sender, Source: TObject; X,
  Y: Integer);
begin
  VeSeAceitaDragDrop(Sender, Source, X, Y, 'Nivel 5', 'Nivel 4', 'ReceiGruNiv4', 'PaiCod');
end;

////////////////////////////////////////////////////////////////////////////////
//  Mandar do Grupo para o Conjunto:
////////////////////////////////////////////////////////////////////////////////

procedure TFmReceiGruNivs.DBGNiv3CellClick(Column: TColumn);
begin
(***
  if Column.FieldName = '_S' then
  begin
    Screen.Cursor := crHourGlass;
    TbReceiGruNiv3.Edit;
    if TbReceiGruNiv3_S.Value = 1 then
      TbReceiGruNiv3CtrlaSdo.Value := 0
    else
      TbReceiGruNiv3CtrlaSdo.Value := 1;
    TbReceiGruNiv3.Post;
    Screen.Cursor := crDefault;
  end else
***)
  if FInGrade then
    FInGrade := False
  else
    if CkDragDrop.Checked then DBGNiv3.BeginDrag(True);
end;

procedure TFmReceiGruNivs.DBGNiv4DragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept := AceitaDragOver(Source, 'DBGGrupo', QrNNiv3);
  //Accept := (Source is TDBGrid) and (TDBGrid(Source).Name = 'DBGGrupo');
end;

procedure TFmReceiGruNivs.DBGNiv4DblClick(Sender: TObject);
begin
  MostraConjuntos();
end;

procedure TFmReceiGruNivs.DBGNiv4DragDrop(Sender, Source: TObject; X,
  Y: Integer);
begin
  VeSeAceitaDragDrop(Sender, Source, X, Y, 'Nivel 4', 'Nivel 3', 'ReceiGruNiv3', 'PaiCod');
end;

////////////////////////////////////////////////////////////////////////////////
//  Mandar do Sub-grupo para o Grupo:
////////////////////////////////////////////////////////////////////////////////

procedure TFmReceiGruNivs.DBGNiv2CellClick(Column: TColumn);
begin
(***
  if Column.FieldName = '_S' then
  begin
    Screen.Cursor := crHourGlass;
    TbReceiGruNiv2.Edit;
    if TbReceiGruNiv2_S.Value = 1 then
      TbReceiGruNiv2CtrlaSdo.Value := 0
    else
      TbReceiGruNiv2CtrlaSdo.Value := 1;
    TbReceiGruNiv2.Post;
    Screen.Cursor := crDefault;
  end else
***)
  if FInGrade then
    FInGrade := False
  else
    if CkDragDrop.Checked then DBGNiv2.BeginDrag(True);
end;

procedure TFmReceiGruNivs.DBGNiv3DragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept := AceitaDragOver(Source, 'DBGSubgr', QrNNiv2);
  //Accept := (Source is TDBGrid) and (TDBGrid(Source).Name = 'DBGSubgr');
end;

procedure TFmReceiGruNivs.DBGNiv3DblClick(Sender: TObject);
begin
  MostraGrupos();
end;

procedure TFmReceiGruNivs.DBGNiv3DragDrop(Sender, Source: TObject; X,
  Y: Integer);
begin
  VeSeAceitaDragDrop(Sender, Source, X, Y, 'Nivel 3', 'Nivel 2', 'ReceiGruNiv2', 'PaiCod');
end;

////////////////////////////////////////////////////////////////////////////////
//  Mandar da Conta para o Sub-grupo:
////////////////////////////////////////////////////////////////////////////////

procedure TFmReceiGruNivs.DBGNiv1CellClick(Column: TColumn);
begin
(***
  if Column.FieldName = '_S' then
  begin
    Screen.Cursor := crHourGlass;
    TbReceiGruNiv1.Edit;
    if TbReceiGruNiv1_S.Value = 1 then
      TbReceiGruNiv1CtrlaSdo.Value := 0
    else
      TbReceiGruNiv1CtrlaSdo.Value := 1;
    TbReceiGruNiv1.Post;
    Screen.Cursor := crDefault;
  end else
  if Column.FieldName = '_M' then
  begin
    Screen.Cursor := crHourGlass;
    TbReceiGruNiv1.Edit;
    if TbReceiGruNiv1_M.Value = 1 then
      TbReceiGruNiv1Mensal.Value := 'F'
    else
      TbReceiGruNiv1Mensal.Value := 'V';
    TbReceiGruNiv1.Post;
    Screen.Cursor := crDefault;
  end else
  if Column.FieldName = '_C' then
  begin
    Screen.Cursor := crHourGlass;
    TbReceiGruNiv1.Edit;
    if TbReceiGruNiv1_C.Value = 1 then
      TbReceiGruNiv1Credito.Value := 'F'
    else
      TbReceiGruNiv1Credito.Value := 'V';
    TbReceiGruNiv1.Post;
    Screen.Cursor := crDefault;
  end else
  if Column.FieldName = '_D' then
  begin
    Screen.Cursor := crHourGlass;
    TbReceiGruNiv1.Edit;
    if TbReceiGruNiv1_D.Value = 1 then
      TbReceiGruNiv1Debito.Value := 'F'
    else
      TbReceiGruNiv1Debito.Value := 'V';
    TbReceiGruNiv1.Post;
    Screen.Cursor := crDefault;
  end else
***)
  if FInGrade then
    FInGrade := False
  else
    if CkDragDrop.Checked then DBGNiv1.BeginDrag(True);
end;

procedure TFmReceiGruNivs.DBGNiv2DragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept := AceitaDragOver(Source, 'DBGConta', QrNNiv1);
  //Accept := (Source is TDBGrid) and (TDBGrid(Source).Name = 'DBGConta');
end;

procedure TFmReceiGruNivs.DBGNiv2DblClick(Sender: TObject);
begin
  MostraSubgrupos();
end;

procedure TFmReceiGruNivs.DBGNiv2DragDrop(Sender, Source: TObject; X,
  Y: Integer);
begin
  VeSeAceitaDragDrop(Sender, Source, X, Y, 'Nivel 2', 'Nivel 1', 'ReceiGruNiv1', 'PaiCod');
end;

procedure TFmReceiGruNivs.TbReceiGruNiv5BeforeInsert(DataSet: TDataSet);
begin
  // Evitar exception
  //TbReceiGruNiv4.Close;
end;

procedure TFmReceiGruNivs.TbReceiGruNiv5AfterPost(DataSet: TDataSet);
begin
  QrTNiv5.Close;
  Grl_DmkDB.AbreQuery(QrTNiv5, Dmod.MyDB);
  TbReceiGruNiv4.Refresh;
  // Evitar exception
  //Grl_DmkDB.AbreQuery(TbReceiGruNiv4, Dmod.MyDB);
end;

procedure TFmReceiGruNivs.TbReceiGruNiv5AfterRefresh(DataSet: TDataSet);
begin
  RefreshNiv4();
end;

procedure TFmReceiGruNivs.TbReceiGruNiv5AfterCancel(DataSet: TDataSet);
begin
  // Evitar exception
  //TGrl_DmkDB.AbreQuery(bConju, Dmod.MyDB);
end;

procedure TFmReceiGruNivs.TbReceiGruNiv5AfterOpen(DataSet: TDataSet);
begin
  RefreshNiv4();
end;

procedure TFmReceiGruNivs.TbReceiGruNiv4BeforeInsert(DataSet: TDataSet);
begin
  CancelaSemPai(DataSet);
  // Evitar exception
  //Grl_DmkDB.AbreQuery(TbReceiGruNiv3.Close;
end;

procedure TFmReceiGruNivs.TbReceiGruNiv4AfterPost(DataSet: TDataSet);
begin
  QrTNiv4.Close;
  Grl_DmkDB.AbreQuery(QrTNiv4, Dmod.MyDB);
  TbReceiGruNiv3.Refresh;
  // Evitar exception
  //TGrl_DmkDB.AbreQuery(bGrupo, Dmod.MyDB);
end;

procedure TFmReceiGruNivs.TbReceiGruNiv4AfterRefresh(DataSet: TDataSet);
begin
  RefreshNiv3();
end;

procedure TFmReceiGruNivs.TbReceiGruNiv4AfterCancel(DataSet: TDataSet);
begin
  // Evitar exception
  //TGrl_DmkDB.AbreQuery(bGrupo, Dmod.MyDB);
end;

procedure TFmReceiGruNivs.TbReceiGruNiv3BeforeInsert(DataSet: TDataSet);
begin
  CancelaSemPai(DataSet);
  // Evitar exception
  //TbReceiGruNiv2.Close;
end;

procedure TFmReceiGruNivs.TbReceiGruNiv3AfterPost(DataSet: TDataSet);
begin
  QrTNiv3.Close;
  Grl_DmkDB.AbreQuery(QrTNiv3, Dmod.MyDB);
  TbReceiGruNiv2.Refresh;
  // Evitar exception
  //Grl_DmkDB.AbreQuery(TbReceiGruNiv2, Dmod.MyDB);
end;

procedure TFmReceiGruNivs.TbReceiGruNiv3AfterRefresh(DataSet: TDataSet);
begin
  RefreshNiv2();
end;

procedure TFmReceiGruNivs.TbReceiGruNiv3AfterCancel(DataSet: TDataSet);
begin
  // Evitar exception
  //Grl_DmkDB.AbreQuery(TbReceiGruNiv2, Dmod.MyDB);
end;

procedure TFmReceiGruNivs.TbReceiGruNiv3AfterOpen(DataSet: TDataSet);
begin
  RefreshNiv2();
end;

procedure TFmReceiGruNivs.TbReceiGruNiv2BeforeInsert(DataSet: TDataSet);
begin
  CancelaSemPai(DataSet);
  // Evitar exception
  //TbReceiGruNiv1.Close;
end;

procedure TFmReceiGruNivs.TbReceiGruNiv2AfterPost(DataSet: TDataSet);
begin
  QrTNiv2.Close;
  Grl_DmkDB.AbreQuery(QrTNiv2, Dmod.MyDB);
  TbReceiGruNiv1.Refresh;
  // Evitar exception
  //Grl_DmkDB.AbreQuery(TbReceiGruNiv1, Dmod.MyDB);
end;

procedure TFmReceiGruNivs.TbReceiGruNiv2AfterRefresh(DataSet: TDataSet);
begin
  RefreshNiv1();
end;

procedure TFmReceiGruNivs.TbReceiGruNiv2AfterCancel(DataSet: TDataSet);
begin
  // Evitar exception
  //Grl_DmkDB.AbreQuery(TbReceiGruNiv1, Dmod.MyDB);
end;

procedure TFmReceiGruNivs.TbReceiGruNiv2AfterOpen(DataSet: TDataSet);
begin
  RefreshNiv1();
end;

procedure TFmReceiGruNivs.DBGNiv5Enter(Sender: TObject);
begin
  DsExtra.DataSet := QrNNiv4;
  RenomeiaTituloExtra('do CONJUNTO', 'do PLANO');
  FInGrade := True;
end;

procedure TFmReceiGruNivs.DBGNiv4Enter(Sender: TObject);
begin
  DsExtra.DataSet := QrNNiv3;
  RenomeiaTituloExtra('do GRUPO', 'do CONJUNTO');
  FInGrade := True;
end;

procedure TFmReceiGruNivs.DBGNiv3Enter(Sender: TObject);
begin
  DsExtra.DataSet := QrNNiv2;
  RenomeiaTituloExtra('do SUB-GRUPO', 'do GRUPO');
  FInGrade := True;
end;

procedure TFmReceiGruNivs.DBGNiv2Enter(Sender: TObject);
begin
  DsExtra.DataSet := QrNNiv1;
  RenomeiaTituloExtra('da CONTA', 'do SUB-GRUPO');
  FInGrade := True;
end;

procedure TFmReceiGruNivs.DBGNiv1Enter(Sender: TObject);
begin
  DsExtra.DataSet := nil;
  RenomeiaTituloExtra('Descri��o', '');
  FInGrade := True;
end;

procedure TFmReceiGruNivs.RefreshNiv1;
begin
  TbReceiGruNiv1.Filter := 'PaiUsu= ' + Geral.FF0(TbReceiGruNiv2CodUsu.Value);
  QrNNiv1.Close;
  if (TbReceiGruNiv2.State <> dsInactive) and (TbReceiGruNiv2.RecordCount > 0) then
  begin
    QrNNiv1.Params[0].AsInteger := TbReceiGruNiv2Codigo.Value;
    Grl_DmkDB.AbreQuery(QrNNiv1, Dmod.MyDB);
  end else
    TbReceiGruNiv1.Filter := 'PaiUsu=-1000';
end;

procedure TFmReceiGruNivs.RefreshNiv2();
begin
  QrNNiv2.Close;
  if (TbReceiGruNiv3.State <> dsInactive) and (TbReceiGruNiv3.RecordCount > 0) then
  begin
    TbReceiGruNiv2.Filter := 'PaiUsu= ' + Geral.FF0(TbReceiGruNiv3CodUsu.Value);
    QrNNiv2.Params[0].AsInteger := TbReceiGruNiv3Codigo.Value;
    Grl_DmkDB.AbreQuery(QrNNiv2, Dmod.MyDB);
  end else
    TbReceiGruNiv2.Filter := 'PaiUsu=-1000';
  RefreshNiv1();
end;

procedure TFmReceiGruNivs.RefreshNiv3();
begin
  QrNNiv3.Close;
  if (TbReceiGruNiv4.State <> dsInactive) and (TbReceiGruNiv4.RecordCount > 0) then
  begin
    TbReceiGruNiv3.Filter := 'PaiUsu= ' + Geral.FF0(TbReceiGruNiv4CodUsu.Value);
    QrNNiv3.Params[0].AsInteger := TbReceiGruNiv4Codigo.Value;
    Grl_DmkDB.AbreQuery(QrNNiv3, Dmod.MyDB);
  end else
    TbReceiGruNiv3.Filter := 'PaiUsu=-1000';
  RefreshNiv2();
end;

procedure TFmReceiGruNivs.RefreshNiv4();
begin
  QrNNiv4.Close;
  if (TbReceiGruNiv5.State <> dsInactive) and (TbReceiGruNiv5.RecordCount > 0) then
  begin
    TbReceiGruNiv4.Filter := 'PaiUsu= ' + Geral.FF0(TbReceiGruNiv5CodUsu.Value);
    QrNNiv4.Params[0].AsInteger := TbReceiGruNiv5Codigo.Value;
    Grl_DmkDB.AbreQuery(QrNNiv4, Dmod.MyDB);
  end else
    TbReceiGruNiv4.Filter := 'PaiUsu=-1000';
  RefreshNiv3();
end;

procedure TFmReceiGruNivs.RenomeiaTituloExtra(TitSon, TitPai: String);
var
  i: Integer;
begin
  for i := 0 to DBGExtra.Columns.Count -1 do
  begin
    if DBGExtra.Columns[i].FieldName = 'Nome' then
      DBGExtra.Columns[i].Title.Caption := 'Descri��o ' + TitSon;
  end;
  StaticText1.Caption := 'Itens n�o pertencentes ' + TitPai + ' atual';
end;

procedure TFmReceiGruNivs.DBGExtraCellClick(Column: TColumn);
begin
  if FInGrade then
    FInGrade := False
  else
    if CkDragDrop.Checked then DBGExtra.BeginDrag(True);
end;

function TFmReceiGruNivs.AceitaDragOver(Source: TObject; Grade: String;
  Tabela: TZQuery): Boolean;
begin
  Result := (Source is TDBGrid)  and
  (
    (TDBGrid(Source).Name = Grade) or
    (
      (TDBGrid(Source).Name = 'DBGExtra')
      and
      (DsExtra.DataSet = Tabela)
     )
   );
end;

procedure TFmReceiGruNivs.VeSeAceitaDragDrop(Sender, Source: TObject; X, Y: Integer;
TitPai, TitSon, Tabela, Campo: String);
var
  gc : TGridCoord;
  PaiNome: string;
  SonCodi, PaiCodi: Integer;
  SQLType: TSQLType;
var
  //Nome: String;
  Codigo, CodUsu, (*IDServr,*) PaiCod, PaiUsu: Integer;
  //SQLType: TSQLType;
begin
  gc := THackDBGrid(Sender).MouseCoord(X,Y);
  if (gc.X > 0) and (gc.Y > 0) then
  begin
    if THackDBGrid(Sender).DataSource.DataSet.RecordCount = 0 then Exit;
    if THackDBGrid(Sender).DataSource.DataSet.FieldByName('Codigo').AsInteger = 0 then Exit;
    PaiNome := THackDBGrid(Source).DataSource.DataSet.FieldByName('NOME_PAI').AsString;
    SonCodi := THackDBGrid(Source).DataSource.DataSet.FieldByName('Codigo').AsInteger;
    with THackDBGrid(Sender) do
    begin
      DataSource.DataSet.MoveBy(gc.Y - Row);
      PaiCodi := THackDBGrid(Sender).DataSource.DataSet.FieldByName('Codigo').AsInteger;
      //
      if Geral.MB_Pergunta('Foi solicitada a altera��o conforme abaixo: '
      + sLineBreak + sLineBreak + TitSon + ': "' +
      THackDBGrid(Source).DataSource.DataSet.FieldByName('Nome').AsString + '" ' +
      sLineBreak + sLineBreak +
      'Do ' + TitPai + ': "' + PaiNome + '" ' + sLineBreak +
      'Para o ' + TitPai + ': "' +
      THackDBGrid(Sender).DataSource.DataSet.FieldByName('Nome').AsString + '"' +
      sLineBreak + sLineBreak +
      'Confirma a altera��o?') = ID_YES then
      begin
(*
        if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, Tabela, False,
        [Campo], ['Codigo'], [PaiCodi], [SonCodi], True,
        TdmkSQLInsert.dmksqlinsInsOnly, '', TDeviceType.stDesktop, False) then
*)

  Codigo              := SonCodi;
  CodUsu              := SonCodi;
  //IDServr             := VAR_IDServr;
  PaiCod              := PaiCodi;
  PaiUsu              := PaiCodi;
  //Nome                := ;

SQLType := stUpd;

        if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, Tabela, False, [
        'CodUsu', (*'IDServr',*) 'PaiCod',
        'PaiUsu'(*, 'Nome'*)], [
        'Codigo'], [
        CodUsu, (*IDServr,*) PaiCod,
        PaiUsu(*, Nome*)], [
        Codigo], True, TdmkSQLInsert.dmksqlinsInsOnly, '', TDeviceType.stDesktop, False) then
        begin
          THackDBGrid(Sender).DataSource.DataSet.Close;
          THackDBGrid(Sender).DataSource.DataSet. Open;
          THackDBGrid(Sender).DataSource.DataSet.Locate('Codigo', PaiCodi, []);
          //
          THackDBGrid(Source).DataSource.DataSet.Close;
          THackDBGrid(Source).DataSource.DataSet. Open;
          THackDBGrid(Source).DataSource.DataSet.Locate('Codigo', SonCodi, []);
          //
          TbReceiGruNiv1.Refresh;
          TbReceiGruNiv2.Refresh;
          TbReceiGruNiv3.Refresh;
          TbReceiGruNiv4.Refresh;
          TbReceiGruNiv5.Refresh;
        end;
      end;
    end;
  end;
end;

procedure TFmReceiGruNivs.SbConjuntosClick(Sender: TObject);
begin
  MostraConjuntos();
end;

procedure TFmReceiGruNivs.SbContasClick(Sender: TObject);
begin
  MostraContas();
end;

procedure TFmReceiGruNivs.SbGruposClick(Sender: TObject);
begin
  MostraGrupos();
end;

procedure TFmReceiGruNivs.SbImprimeClick(Sender: TObject);
begin
(***
  FinanceiroJan.ImpressaoDoPlanoDeContas();
***)
end;

procedure TFmReceiGruNivs.MostraPlanos();
var
  Codigo: Integer;
begin
(***
  if (TbReceiGruNiv5.State <> dsInactive) and (TbReceiGruNiv5.RecordCount > 0) then
    Codigo := TbReceiGruNivCodigo.Value
  else
    Codigo := 0;
  //
  FinanceiroJan.CadastroDePlano(Codigo);
  //
  TbReceiGruNiv5.Close;
  Grl_DmkDB.AbreTable(TbReceiGruNiv, Dmod.MyDB);
  //
  if Codigo <> 0 then
    TbReceiGruNiv5.Locate('Codigo', Codigo, []);
***)
end;

procedure TFmReceiGruNivs.MostraConjuntos();
var
  Codigo: Integer;
begin
(***
  if (TbReceiGruNiv4.State <> dsInactive) and (TbReceiGruNiv4.RecordCount > 0) then
    Codigo := TbReceiGruNiv4Codigo.Value
  else
    Codigo := 0;
  //
  FinanceiroJan.CadastroDeConjutos(Codigo);
  //
  TbReceiGruNiv4.Close;
  Grl_DmkDB.AbreTable(TbReceiGruNiv4, Dmod.MyDB);
  //
  if Codigo <> 0 then
    TbReceiGruNiv4.Locate('Codigo', Codigo, []);
***)
end;

procedure TFmReceiGruNivs.MostraGrupos();
var
  Codigo: Integer;
begin
(***
  if (TbReceiGruNiv3.State <> dsInactive) and (TbReceiGruNiv3.RecordCount > 0) then
    Codigo := TbReceiGruNiv3Codigo.Value
  else
    Codigo := 0;
  //
  FinanceiroJan.CadastroDeGrupos(Codigo);
  //
  TbReceiGruNiv3.Close;
  Grl_DmkDB.AbreTable(TbReceiGruNiv3, Dmod.MyDB);
  //
  if Codigo <> 0 then
    TbReceiGruNiv3.Locate('Codigo', Codigo, []);
***)
end;

procedure TFmReceiGruNivs.MostraSubgrupos();
var
  Codigo: Integer;
begin
(***
  if (TbReceiGruNiv2.State <> dsInactive) and (TbReceiGruNiv2.RecordCount > 0) then
    Codigo := TbReceiGruNiv2Codigo.Value
  else
    Codigo := 0;
  //
  FinanceiroJan.CadastroDeSubGrupos(Codigo);
  //
  TbReceiGruNiv2.Close;
  Grl_DmkDB.AbreTable(TbReceiGruNiv2, Dmod.MyDB);
  //
  if Codigo <> 0 then
    TbReceiGruNiv2.Locate('Codigo', Codigo, []);
***)
end;

procedure TFmReceiGruNivs.MostraContas();
var
  Codigo: Integer;
begin
(***
  if (TbReceiGruNiv1.State <> dsInactive) and (TbReceiGruNiv1.RecordCount > 0) then
    Codigo := TbReceiGruNiv1Codigo.Value
  else
    Codigo := 0;
  //
  FinanceiroJan.CadastroDeContas(Codigo);
  //
  TbReceiGruNiv1.Close;
  Grl_DmkDB.AbreTable(TbReceiGruNiv1, Dmod.MyDB);
  //
  if Codigo <> 0 then
    TbReceiGruNiv1.Locate('Codigo', Codigo, []);
***)
end;

procedure TFmReceiGruNivs.SbPlanoClick(Sender: TObject);
begin
  MostraPlanos();
end;

procedure TFmReceiGruNivs.SbSubgruposClick(Sender: TObject);
begin
  MostraSubgrupos();
end;

procedure TFmReceiGruNivs.CancelaSemPai(DataSet: TDataSet);
var
  TbPai: TZTable;
begin
  if TZTable(Dataset) = TbReceiGruNiv1 then TbPai := TbReceiGruNiv2 else
  if TZTable(Dataset) = TbReceiGruNiv2 then TbPai := TbReceiGruNiv3 else
  if TZTable(Dataset) = TbReceiGruNiv3 then TbPai := TbReceiGruNiv4 else
  if TZTable(Dataset) = TbReceiGruNiv4 then TbPai := TbReceiGruNiv5 else
    Geral.MB_Aviso('TABLE Indefinido!');
  if (TbPai.State = dsInactive) or (TbPai.RecordCount = 0) then
  begin
    Geral.MB_Aviso('N�o h� item pai cadastrado!');
    TZTable(DataSet).Cancel;
    Abort;
  end;
end;

procedure TFmReceiGruNivs.CkDragDropClick(Sender: TObject);
begin
  PnExtra.Visible := CkDragDrop.Checked;
end;

procedure TFmReceiGruNivs.TbReceiGruNiv5BeforeDelete(DataSet: TDataSet);
begin
  Abort;
end;

procedure TFmReceiGruNivs.TbReceiGruNiv4BeforeDelete(DataSet: TDataSet);
begin
  Abort;
end;

procedure TFmReceiGruNivs.TbReceiGruNiv3BeforeDelete(DataSet: TDataSet);
begin
  Abort;
end;

procedure TFmReceiGruNivs.TbReceiGruNiv2BeforeDelete(DataSet: TDataSet);
begin
  Abort;
end;

procedure TFmReceiGruNivs.TbReceiGruNiv1BeforeDelete(DataSet: TDataSet);
begin
  Abort;
end;

procedure TFmReceiGruNivs.TbReceiGruNiv1BeforeInsert(DataSet: TDataSet);
begin
  CancelaSemPai(DataSet);
end;

procedure TFmReceiGruNivs.TbReceiGruNiv1CalcFields(DataSet: TDataSet);
begin
(***
  if TbReceiGruNiv1Mensal.Value = '' then TbReceiGruNiv1_M.Value := 0
  else TbReceiGruNiv1_M.Value := dmkPF.V_FToInt(TbReceiGruNiv1Mensal.Value[1]);

  TbReceiGruNiv1_S.Value := TbReceiGruNiv1CtrlaSdo.Value;

  if TbReceiGruNiv1Credito.Value = '' then TbReceiGruNiv1_C.Value := 0
  else TbReceiGruNiv1_C.Value := dmkPF.V_FToInt(TbReceiGruNiv1Credito.Value[1]);

  if TbReceiGruNiv1Debito.Value = '' then TbReceiGruNiv1_D.Value := 0
  else TbReceiGruNiv1_D.Value := dmkPF.V_FToInt(TbReceiGruNiv1Debito.Value[1]);
***)
end;

procedure TFmReceiGruNivs.DBGNiv1DblClick(Sender: TObject);
begin
  MostraContas();
end;

procedure TFmReceiGruNivs.DBGNiv1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
(***
  if Column.FieldName = '_M' then
    MeuVCLSkin.DrawGrid(DBGConta, Rect, 1, TbReceiGruNiv1_M.Value);
  if Column.FieldName = '_S' then
    MeuVCLSkin.DrawGrid(DBGConta, Rect, 1, TbReceiGruNiv1_S.Value);
  if Column.FieldName = '_C' then
    MeuVCLSkin.DrawGrid(DBGConta, Rect, 1, TbReceiGruNiv1_C.Value);
  if Column.FieldName = '_D' then
    MeuVCLSkin.DrawGrid(DBGConta, Rect, 1, TbReceiGruNiv1_D.Value);
***)
end;

procedure TFmReceiGruNivs.DBGNiv1ColEnter(Sender: TObject);
begin
  if (TDBGrid(Sender).Columns[THackDBGrid(Sender).Col -1].FieldName = '_S')
  or (TDBGrid(Sender).Columns[THackDBGrid(Sender).Col -1].FieldName = '_M')
  or (TDBGrid(Sender).Columns[THackDBGrid(Sender).Col -1].FieldName = '_C')
  or (TDBGrid(Sender).Columns[THackDBGrid(Sender).Col -1].FieldName = '_D') then
    TDBGrid(Sender).Options := TDBGrid(Sender).Options - [dgEditing] else
    TDBGrid(Sender).Options := TDBGrid(Sender).Options + [dgEditing];
end;

procedure TFmReceiGruNivs.DBGNiv1ColExit(Sender: TObject);
begin
  THackDBGrid(Sender).Options := THackDBGrid(Sender).Options - [dgEditing];
end;

end.
