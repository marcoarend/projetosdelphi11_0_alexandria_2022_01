unit OpcoesApp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  ZAbstractRODataset, ZAbstractDataset, ZDataset, uLkJSON, UnProjGroup_Vars,
  UnAppEnums, dmkDBGridZTO, Vcl.Menus;

type
  TFmOpcoesApp = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrDevices: TZQuery;
    Panel5: TPanel;
    Label32: TLabel;
    EdUserNome: TdmkEdit;
    SbDeviceHost: TSpeedButton;
    DsDevices: TDataSource;
    QrDevicesCodigo: TLargeintField;
    QrDevicesDeviceAssembler: TLargeintField;
    QrDevicesUniqueId: TWideStringField;
    QrDevicesDeviceSeq: TLargeintField;
    QrDevicesDeviceType: TWideStringField;
    QrDevicesPVPower: TLargeintField;
    QrDevicesCustomName: TWideStringField;
    QrDevicesLk: TLargeintField;
    QrDevicesDataCad: TDateField;
    QrDevicesDataAlt: TDateField;
    QrDevicesUserCad: TLargeintField;
    QrDevicesUserAlt: TLargeintField;
    QrDevicesAlterWeb: TSmallintField;
    QrDevicesAtivo: TSmallintField;
    GroupBox2: TGroupBox;
    dmkDBGridZTO1: TdmkDBGridZTO;
    QrDevicesNome_DeviceAssembler: TWideStringField;
    Panel6: TPanel;
    BtDevice: TBitBtn;
    PMDevice: TPopupMenu;
    MiNewDivice1: TMenuItem;
    MiUpdDevice1: TMenuItem;
    MiDelDevice1: TMenuItem;
    QrLoggers: TZQuery;
    DsLoggers: TDataSource;
    QrLoggersCodigo: TLargeintField;
    QrLoggersDeviceAssembler: TLargeintField;
    QrLoggersUniqueId: TWideStringField;
    QrLoggersDeviceSeq: TLargeintField;
    QrLoggersProductID: TWideStringField;
    QrLoggersPlatformID: TWideStringField;
    QrLoggersHWVersion: TWideStringField;
    QrLoggersSWVersion: TWideStringField;
    QrLoggersTimezoneLocation: TWideStringField;
    QrLoggersTimezoneName: TWideStringField;
    QrLoggersUTCOffset: TLargeintField;
    QrLoggersCashFactor: TFloatField;
    QrLoggersDeliveryFactor: TFloatField;
    QrLoggersCashCurrency: TWideStringField;
    QrLoggersCO2Factor: TFloatField;
    QrLoggersCO2Unit: TWideStringField;
    QrLoggersLk: TLargeintField;
    QrLoggersDataCad: TDateField;
    QrLoggersDataAlt: TDateField;
    QrLoggersUserCad: TLargeintField;
    QrLoggersUserAlt: TLargeintField;
    QrLoggersAlterWeb: TSmallintField;
    QrLoggersAtivo: TSmallintField;
    QrLoggersNome_DeviceAssembler: TWideStringField;
    EdUserSigla: TdmkEdit;
    Label1: TLabel;
    EdUserID: TdmkEdit;
    Label2: TLabel;
    EdUserMD5: TdmkEdit;
    Label3: TLabel;
    SpeedButton1: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrDevicesCalcFields(DataSet: TDataSet);
    procedure BtDeviceClick(Sender: TObject);
    procedure QrLoggersCalcFields(DataSet: TDataSet);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
  end;

  var
  FmOpcoesApp: TFmOpcoesApp;

implementation

uses UnMyObjects, UnAppPF, UnAppJan, Module, UnGrl_DmkDB, UnDmkWeb, UnMyJSON;

{$R *.DFM}

procedure TFmOpcoesApp.BtDeviceClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMDevice, BtDevice);
end;

procedure TFmOpcoesApp.BtOKClick(Sender: TObject);
const
  Codigo = 1;
  SQLType = TSQLType.stUpd;
var
  UserNome, UserSigla, UserMD5: String;
  UserID: Integer;
  //Versao: Double;
  //SQLType: TSQLType;
begin
  //Versao              := ;
  UserNome            := EdUserNome.ValueVariant;
  UserSigla           := EdUserSigla.ValueVariant;
  UserMD5             := EdUserMD5.ValueVariant;
  UserID              := EdUserID.ValueVariant;
  //
  if MyObjects.FIC(Trim(UserNome) = '', EdUserNome, 'Informe o nome do usuario!') then
    Exit;
  if MyObjects.FIC(UserMD5 <> AppPF.MD5UserID(UserSigla, UserID), EdUserMD5,
  'Chave de sincronismo incorreta!') then
    Exit;
  if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, 'controle', False, [
  (*'Versao',*) 'UserNome', 'UserSigla',
  'UserMD5', 'UserID'], [
  'Codigo'], [
  (*Versao,*) UserNome, UserSigla,
  UserMD5, UserID], [
  Codigo], True, dmksqlinsInsOnly, '', stDesktop, False) then
  begin
    Dmod.RefreshUserSetInfo();
    Close;
  end;
end;

procedure TFmOpcoesApp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOpcoesApp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOpcoesApp.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Dmod.RefreshUserSetInfo();
end;

procedure TFmOpcoesApp.FormCreate(Sender: TObject);
var
  Qry: TZQuery;
begin
  ImgTipo.SQLType := stLok;
{
  //
  Qry := TZQuery.Create(Dmod);
  try
    Qry.SQL.Text := 'SELECT * FROM controle WHERE Codigo=1';
    Grl_DmkDB.AbreQuery(Qry, Dmod.MyDB);
    VAR_DeviceAssembler := TDevicesAssembler(Qry.FieldByName('DeviceAssembler').AsInteger);
    RGDeviceAssembler.ItemIndex := Integer(VAR_DeviceAssembler);
    EdDeviceHost.Text := Qry.FieldByName('DeviceHost').AsString;
    EdURLBaseAPI.Text := Qry.FieldByName('URLBaseAPI').AsString;
    EdURLBaseAPI.Text := Qry.FieldByName('URLBaseAPI').AsString;
    EdDeviceMACAddress.Text := Qry.FieldByName('DeviceMACAddress').AsString;
    //
  finally
    Qry.Free;
  end;
  Grl_DmkDB.ReopenQuery(QrDevices, Dmod.MyDB);
  Grl_DmkDB.ReopenQuery(QrLoggers, Dmod.MyDB);
}
end;

procedure TFmOpcoesApp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOpcoesApp.QrDevicesCalcFields(DataSet: TDataSet);
begin
  QrDevicesNome_DeviceAssembler.Value := AppEnums.NomeDevicesAssembler(
    TDevicesAssembler(QrDevicesDeviceAssembler.Value));
end;

procedure TFmOpcoesApp.QrLoggersCalcFields(DataSet: TDataSet);
begin
  QrLoggersNome_DeviceAssembler.Value := AppEnums.NomeDevicesAssembler(
    TDevicesAssembler(QrLoggersDeviceAssembler.Value));
end;

procedure TFmOpcoesApp.SpeedButton1Click(Sender: TObject);
var
  Continua: Boolean;
  UserSigla, Login, Senha: String;
  UserID: Integer;
begin
  AppJan.MostraFormArcano_dmk(Login, Senha);
  if Senha = CO_Master then
  begin
    UserSigla := Trim(EdUserSigla.Text);
    UserID    := EdUserID.ValueVariant;
    EdUserMD5.Text := AppPF.MD5UserID(UserSigla, UserID);
  end;
end;

end.
