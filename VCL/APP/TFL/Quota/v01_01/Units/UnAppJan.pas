unit UnAppJan;

interface

uses
  Windows, SysUtils, Classes, Menus, UnDmkEnums, mySQLDbTables, Forms,
  UnInternalConsts, UnProjGroup_Vars, dmkGeral;

type
  TUnAppJan = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure MostraFormOpcoesApp();
  end;

var
  AppJan: TUnAppJan;


implementation

uses UnGrl_DmkDB, OpcoesApp;

{ TUnAppJan }

procedure TUnAppJan.MostraFormOpcoesApp();
begin
  if Grl_DmkDB.CriaFm(TFmOpcoesApp, FmOpcoesApp, afmoSoMaster) then
  begin
    FmOpcoesApp.ShowModal;
    FmOpcoesApp.Destroy;
  end;
end;

end.
