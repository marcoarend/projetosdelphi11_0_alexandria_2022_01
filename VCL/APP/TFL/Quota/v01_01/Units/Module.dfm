object DMod: TDMod
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 401
  Width = 363
  object MyDB: TZConnection
    ControlsCodePage = cGET_ACP
    Properties.Strings = (
      'controls_cp=GET_ACP')
    Connected = True
    Port = 0
    Database = 'C:\Dermatek\SQLite\Quota.s3db'
    Protocol = 'sqlite'
    LibraryLocation = 'C:\Executaveis\Auxiliares\sqlite3.dll'
    Left = 16
    Top = 8
  end
  object QrLoc: TZQuery
    Connection = MyDB
    Params = <>
    Left = 12
    Top = 196
  end
  object QrControle: TZQuery
    Connection = MyDB
    Params = <>
    Left = 12
    Top = 52
  end
  object QrAux: TZQuery
    Connection = MyDB
    Params = <>
    Left = 12
    Top = 148
  end
  object QrUpd: TZQuery
    Connection = MyDB
    Params = <>
    Left = 12
    Top = 100
  end
  object QrNxt: TZQuery
    Connection = MyDB
    Params = <>
    Left = 12
    Top = 244
  end
  object QrPsq1: TZQuery
    Connection = MyDB
    Params = <>
    Left = 12
    Top = 292
  end
end
