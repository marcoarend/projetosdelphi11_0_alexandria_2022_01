unit Principal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  WinSkinData, WinSkinStore, Vcl.Imaging.pngimage, AdvToolBar, AdvShapeButton,
  AdvGlowButton, Data.DB, ZAbstractRODataset, ZAbstractDataset, ZDataset,
  Vcl.ExtCtrls, Vcl.Menus, Registry, Vcl.Grids, Vcl.DBGrids, Vcl.ComCtrls,
  Vcl.StdCtrls, System.DateUtils, AdvOfficeStatusBar, AdvOfficeStatusBarStylers,
  AdvCardList, InspectorBar, InspLinks, AdvLookupBar, AdvDataLabel, ToolPanels,
  BaseGrid,
  uLkJSON, UnMyJSON, UnAppPF, UnAppJan, UnProjGroup_Vars,
  UnDmkEnums, TypInfo,
  UnAppEnums, AdvProgressBar, AdvReflectionLabel, AdvPanel, AdvMenus,
  Vcl.Buttons, Vcl.Mask, Vcl.DBCtrls, AdvMenuStylers, RTTI;

type
  TDtaTip = (dttpUltTry=0, dttpUltExe=1, dttpUltErr=2);
  TFmPrincipal = class(TForm)
    SkinStore1: TSkinStore;
    sd1: TSkinData;
    AdvToolBarPager1: TAdvToolBarPager;
    AdvPage2: TAdvPage;
    AdvToolBar2: TAdvToolBar;
    BtProduzido: TAdvGlowButton;
    AGBInclui: TAdvGlowButton;
    AGBExclui: TAdvGlowButton;
    AdvToolBar1: TAdvToolBar;
    AGBOcultar: TAdvGlowButton;
    AGBExecutar: TAdvGlowButton;
    AdvToolBarPager13: TAdvPage;
    AdvToolBar3: TAdvToolBar;
    AdvGlowButton6: TAdvGlowButton;
    AGMBVerifiBD: TAdvGlowButton;
    AdvPage1: TAdvPage;
    AdvToolBar4: TAdvToolBar;
    AdvGlowButton28: TAdvGlowButton;
    AdvToolBar5: TAdvToolBar;
    AdvGlowButton19: TAdvGlowButton;
    AdvShapeButton1: TAdvShapeButton;
    AdvQuickAccessToolBar1: TAdvQuickAccessToolBar;
    ATBBAtualiza: TAdvToolBarButton;
    BalloonHint1: TBalloonHint;
    TmConfiguraDB: TTimer;
    TmHide: TTimer;
    TmVersao: TTimer;
    TrayIcon1: TTrayIcon;
    PMMenu: TPopupMenu;
    Mostrar1: TMenuItem;
    Fechar1: TMenuItem;
    N1: TMenuItem;
    Inicializao1: TMenuItem;
    Executarnainicializao1: TMenuItem;
    NOexecutarnainicializao1: TMenuItem;
    AdvToolBar6: TAdvToolBar;
    AGBExtrair: TAdvGlowButton;
    AdvGlowButton43: TAdvGlowButton;
    AdvToolPanelTab1: TAdvToolPanelTab;
    AdvGridUndoRedo1: TAdvGridUndoRedo;
    AdvToolPanel1: TAdvToolPanel;
    AdvStatusBar: TAdvOfficeStatusBar;
    AdvOfficeStatusBarOfficeStyler1: TAdvOfficeStatusBarOfficeStyler;
    AdvToolPanel2: TAdvToolPanel;
    AdvToolPanel3: TAdvToolPanel;
    AdvPB1: TAdvProgressBar;
    Memo1: TMemo;
    Memo2: TMemo;
    LBAvisos: TListBox;
    AdvPanelStyler1: TAdvPanelStyler;
    AdvPanel1: TAdvPanel;
    AdvToolBar7: TAdvToolBar;
    AdvGlowButton1: TAdvGlowButton;
    Image1: TImage;
    AdvCadastros: TAdvToolBarButton;
    AdvPMCadastros: TAdvPopupMenu;
    Segmentos1: TMenuItem;
    AdvMenuOfficeStyler1: TAdvMenuOfficeStyler;
    Entidades1: TMenuItem;
    GruposdeEmpresas1: TMenuItem;
    Clientes1: TMenuItem;
    Accounts1: TMenuItem;
    Produtos1: TMenuItem;
    Famlias1: TMenuItem;
    Cadastro1: TMenuItem;
    Similares1: TMenuItem;
    Gruposdesegmentos1: TMenuItem;
    Divises1: TMenuItem;
    Subdivises1: TMenuItem;
    N3: TMenuItem;
    Linhas1: TMenuItem;
    Segmentos2: TMenuItem;
    Embalagens1: TMenuItem;
    Cadastro2: TMenuItem;
    aras1: TMenuItem;
    Envases1: TMenuItem;
    N2: TMenuItem;
    Importar1: TMenuItem;
    SQLite1: TMenuItem;
    Excel1: TMenuItem;
    REInfo: TRichEdit;
    procedure AGBOcultarClick(Sender: TObject);
    procedure BtProduzidoClick(Sender: TObject);
    procedure AdvGlowButton6Click(Sender: TObject);
    procedure AGMBVerifiBDClick(Sender: TObject);
    procedure AdvGlowButton28Click(Sender: TObject);
    procedure AdvGlowButton19Click(Sender: TObject);
    procedure TmConfiguraDBTimer(Sender: TObject);
    procedure TmHideTimer(Sender: TObject);
    procedure TmVersaoTimer(Sender: TObject);
    procedure TrayIcon1Click(Sender: TObject);
    procedure PMMenuPopup(Sender: TObject);
    procedure Mostrar1Click(Sender: TObject);
    procedure Fechar1Click(Sender: TObject);
    procedure Executarnainicializao1Click(Sender: TObject);
    procedure NOexecutarnainicializao1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure AdvGlowButton43Click(Sender: TObject);
    procedure AdvGlowButton1Click(Sender: TObject);
    procedure AGBExecutarClick(Sender: TObject);
    procedure Segmentos1Click(Sender: TObject);
    procedure ATBBAtualizaClick(Sender: TObject);
    procedure GruposdeEmpresas1Click(Sender: TObject);
    procedure Clientes1Click(Sender: TObject);
    procedure Accounts1Click(Sender: TObject);
    procedure Cadastro1Click(Sender: TObject);
    procedure Gruposdesegmentos1Click(Sender: TObject);
    procedure Famlias1Click(Sender: TObject);
    procedure Divises1Click(Sender: TObject);
    procedure Subdivises1Click(Sender: TObject);
    procedure Similares1Click(Sender: TObject);
    procedure Linhas1Click(Sender: TObject);
    procedure aras1Click(Sender: TObject);
    procedure Cadastro2Click(Sender: TObject);
    procedure Envases1Click(Sender: TObject);
    procedure SQLite1Click(Sender: TObject);
    procedure Excel1Click(Sender: TObject);
  private
    { Private declarations }
    FCriouDB, FShowed, FHideFirst, FPingou: Boolean;
    FHides: Integer;
    function  VerificaNovasVersoes(ApenasVerifica: Boolean = False): Boolean;
    procedure ExecutaNaInicializacao(Executa: Boolean; Titulo, Programa: String);
    procedure ConfiguraDB();
    // Inicio app
  public
    { Public declarations }
    procedure AcoesIniciaisDoAplicativo();
    procedure ReCaptionComponentesDeForm(Form: TForm);
  end;

var
  FmPrincipal: TFmPrincipal;

const
  CO_Titulo = 'Dermatek Backup - Arquivos';

var
  _AGORA: TDateTime;
  SecToday: array [0..86400] of Double;
  IniSecond: Integer = 0;
  EndSecond: Integer = 0;

implementation

uses MyVCLSkin, MyListas, UnDmkWeb, dmkGeral, About, UnMyObjects, Module,
  UnVCL_ZDB, UnInternalConsts, UnGrl_DmkDB, ZForge,
  UnDmkProcFunc, UMySQLDB, MyGlyfs, UnSalNewJan;

{$R *.dfm}

{ TFmPrincipal }

procedure TFmPrincipal.Accounts1Click(Sender: TObject);
begin
  SalNewJan.MostraFormEntidadesAccn(0);
end;

procedure TFmPrincipal.AcoesIniciaisDoAplicativo();
begin
//
end;

procedure TFmPrincipal.AdvGlowButton19Click(Sender: TObject);
var
  Link: String;
begin
  Link := DmkWeb.ObtemLinkAjuda(CO_DMKID_APP, 0);
  //
  DmkWeb.MostraWebBrowser(Link);
end;

procedure TFmPrincipal.AdvGlowButton1Click(Sender: TObject);
var
  Bmp: TBitMap;
  X, Y: Integer;
  Branco, Cinza, Vermelho: Integer;
  Perc1, Perc2: Double;
begin
  BMP := TBitmap.Create();
  BMP.LoadFromFile('LeiturasDia.bmp');
  Image1.Picture.LoadFromFile('LeiturasDia.bmp');
  Branco   := 0;
  Cinza    := 0;
  Vermelho := 0;
  //
  for Y := 0 to BMP.Height - 1 do
  begin
    for X := 0 to BMP.Width - 1 do
    begin
      case BMP.Canvas.Pixels[X, Y] of
        clWhite: Branco := Branco + 1;
        clRed: Vermelho := Vermelho + 1;
        else Cinza := Cinza + 1;
      end;
    end;
  end;
  Perc1 := Vermelho / Cinza * 100;
  Perc2 := Perc1 * 23.61 / 100;
  Geral.MB_Info(
  'Branco: ' + Geral.FF0(Branco) + sLineBreak +
  'Cinza: ' + Geral.FF0(Cinza) + sLineBreak +
  'Vermelho: ' + Geral.FF0(Vermelho) + sLineBreak + sLineBreak +
  '% Branco / Cinza: ' + Geral.FFT(Perc1, 2, siNegativo) + sLineBreak +
  'Wh vermelho dia 11/08/2018: ' + Geral.FFT(Perc2, 3, siNegativo));
end;

procedure TFmPrincipal.AdvGlowButton28Click(Sender: TObject);
begin
  Application.CreateForm(TFmAbout, FmAbout);
  FmAbout.ShowModal;
  FmAbout.Destroy;
end;

procedure TFmPrincipal.AdvGlowButton43Click(Sender: TObject);
begin
  AppJan.MostraFormOpcoesApp();
end;

procedure TFmPrincipal.AdvGlowButton6Click(Sender: TObject);
begin
  VerificaNovasVersoes;
end;

procedure TFmPrincipal.BtProduzidoClick(Sender: TObject);
var
  I: Integer;
var
  DeviceType, Unidade: String;
  DeviceSeq: Integer;
  Quantia: Double;
  SQLType: TSQLType;
  //
  Texto, Str: String;
  ProdDta, Dia, Hora: TDateTime;
begin
  Memo1.Lines.LoadFromFile('Leituras_1.Txt');
  //
  SQLType             := stIns;
  //
  DeviceType          := '75';
  DeviceSeq           := 1;
  ProdDta             := 0;
  Unidade             := 'Wh';
  Quantia             := 0;
  //
  AdvPB1.Position := 0;
  AdvPB1.Max := Memo1.Lines.Count;
  AdvPB1.Visible := True;
  Dia := 0;
  Hora := 0;
  for I := 0 to Memo1.Lines.Count - 1 do
  begin
    AdvPB1.Position := AdvPB1.Position + 1;
    Application.ProcessMessages;
    //
    Texto := Memo1.Lines[I];
    if Texto[1] = '[' then
    begin
      Str := Copy(Texto, 2, 10);
      Dia := dmkPF.ValidaDataDeSQL(Str, False);
    end else
    begin
      Str := Copy(Texto, 1, 8);
      Hora := StrToTime(Str);
      ProdDta := Dia + Hora;
      //
      Str := Copy(Texto, 13);
      Quantia := StrToFloat(Str);
      //
      //if
      Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, 'leituras', False, [
      'DeviceSeq', 'ProdDta', 'Unidade',
      'Quantia'], [
      'DeviceType'], [
      DeviceSeq, ProdDta, Unidade,
      Quantia], [
      DeviceType], True, dmksqlinsInsIgnore, '', stDesktop, False);
        Memo1.Lines.LoadFromFile('Leituras.Txt');
    end;
  end;
  AdvPB1.Visible := False;
end;

procedure TFmPrincipal.AGBExecutarClick(Sender: TObject);
begin
  AppPF.LoadSales();
end;

procedure TFmPrincipal.AGBOcultarClick(Sender: TObject);
begin
  TrayIcon1.Visible             := True;
  Application.MainFormOnTaskBar := FShowed;
  //
  FmPrincipal.Hide;
  //
  Application.MainFormOnTaskBar := FShowed;
end;

procedure TFmPrincipal.AGMBVerifiBDClick(Sender: TObject);
begin
  VCL_ZDB.MostraVerifyDB(False, Dmod.MyDB);
end;

procedure TFmPrincipal.aras1Click(Sender: TObject);
begin
  SalNewJan.MostraFormEmbalagensTar(0);
end;

procedure TFmPrincipal.ATBBAtualizaClick(Sender: TObject);
begin
  VerificaNovasVersoes();
end;

procedure TFmPrincipal.Cadastro1Click(Sender: TObject);
begin
  SalNewJan.MostraFormProdutosCadas(0);
end;

procedure TFmPrincipal.Cadastro2Click(Sender: TObject);
begin
  SalNewJan.MostraFormEmbalagensCad(0);
end;

procedure TFmPrincipal.Clientes1Click(Sender: TObject);
begin
  SalNewJan.MostraFormEntidadesEmpr(0);
end;

procedure TFmPrincipal.ConfiguraDB;
begin
  FCriouDB := True;
  //
  Application.CreateForm(TDmod, Dmod);
  //
  {XYZX ReopenBackupDir();}
  //
  TrayIcon1.Visible := True;
  //
end;

procedure TFmPrincipal.Divises1Click(Sender: TObject);
begin
  SalNewJan.MostraFormProdDivsaoCab(0);
end;
procedure TFmPrincipal.Envases1Click(Sender: TObject);
begin
  SalNewJan.MostraFormProdutosEmbal(0);
end;

procedure TFmPrincipal.Excel1Click(Sender: TObject);
begin
  AppPF.LoadSales();
end;

procedure TFmPrincipal.ExecutaNaInicializacao(Executa: Boolean; Titulo,
  Programa: String);
var
  Registry: TRegistry;
begin
  Registry := TRegistry.Create;
  try
    Registry.RootKey := HKEY_CURRENT_USER;
    Registry.OpenKey('\Software\Microsoft\Windows\CurrentVersion\Run', False);
    if Executa then
      Registry.WriteString(Titulo, Programa)
    else
      Registry.WriteString(Titulo, '');
  finally
    Registry.Free;
  end;
end;

procedure TFmPrincipal.Executarnainicializao1Click(Sender: TObject);
begin
  ExecutaNaInicializacao(True, CO_Titulo, Application.ExeName);
end;

procedure TFmPrincipal.Famlias1Click(Sender: TObject);
begin
  SalNewJan.MostraFormProdutosFamil(0);
end;

procedure TFmPrincipal.Fechar1Click(Sender: TObject);
begin
  Close;
end;

procedure TFmPrincipal.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  //
  if not FCriouDB then
    TmConfiguraDB.Enabled := True;
end;

procedure TFmPrincipal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if TrayIcon1 <> nil then
    TrayIcon1.Visible := False;
end;

procedure TFmPrincipal.FormCreate(Sender: TObject);
var
  I: Integer;
begin
  FCriouDB   := False;
  FHides     := 0;
  FShowed    := True;
  FHideFirst := False;
  //
  VAR_USA_TAG_BITBTN := True;
  //
  Geral.DefineFormatacoes;
  Application.OnMessage := MyObjects.FormMsg;
  //
  AdvToolBarPager1.ActivePageIndex := 0;
  {XYZX PCBackupDir.ActivePageIndex      := 0;}
  //
  TmVersao.Enabled := True;
  //
{
  _AGORA := Now();
  TimerP_PV.Enabled := True;
  for I := 0 to 86400 do
    SecToday[I] := 0;
}
end;

procedure TFmPrincipal.FormShow(Sender: TObject);
begin
  MeuVCLSkin.VCLSkinDefineUso(sd1, skinstore1);
end;

procedure TFmPrincipal.GruposdeEmpresas1Click(Sender: TObject);
begin
  SalNewJan.MostraFormEntidadesGrup();
end;

procedure TFmPrincipal.Gruposdesegmentos1Click(Sender: TObject);
begin
  SalNewJan.MostraFormSegmenGruCads(0);
end;

procedure TFmPrincipal.Linhas1Click(Sender: TObject);
begin
  SalNewJan.MostraFormProdLinhasCab(0);
end;

{XYZX
procedure TFmPrincipal.MostraBackupDir(Codigo: Integer);
begin
  Application.CreateForm(TFmBackupDir, FmBackupDir);
  FmBackupDir.FCodigo := Codigo;
  FmBackupDir.FQuery  := QrBackupDir;
  FmBackupDir.ShowModal;
  FmBackupDir.Destroy;
  //
  ReopenBackupDir();
end;
}
procedure TFmPrincipal.Mostrar1Click(Sender: TObject);
begin
  FmPrincipal.Visible := True;
end;

{
function TFmPrincipal.MoveResulBackup(Zipar: Boolean; Caminho, NomeDir,
  Nome, Destino: String): Boolean;
var
  Continua: Boolean;
  CaminhoZip: String;
begin
  Continua := True;
  //
  //Move o arquivo resultante
  if Zipar then
  begin
    if dmkPF.DelDir(Caminho + '\' + NomeDir + Nome) then
    begin
      LBLista1.Items.Add('Aviso: Diret�rio tempor�rio removido com sucesso!');
    end else
    begin
      LBLista1.Items.Add('Erro: Falha ao remover o diret�rio tempor�rio!');
      Continua := False;
    end;
    CaminhoZip := Caminho + '\' + NomeDir + Nome + '.zip';
  end else
    CaminhoZip := Caminho + '\' + NomeDir + Nome;
  //
  if dmkPF.MoveDir(CaminhoZip, Destino + '\') = True then
  begin
    LBLista1.Items.Add('Aviso: Diret�rio movido com sucesso!');
  end else
  begin
    LBLista1.Items.Add('Erro: Falha ao mover diret�rio!');
    Continua := False;
  end;
  LBLista1.TopIndex := LBLista1.Items.Count - 1;
  //
  Result := Continua;
end;
}

procedure TFmPrincipal.NOexecutarnainicializao1Click(Sender: TObject);
begin
  ExecutaNaInicializacao(False, CO_Titulo, Application.ExeName);
end;

procedure TFmPrincipal.PMMenuPopup(Sender: TObject);
var
  Valor: String;
begin
  Valor := Geral.ReadKey(CO_Titulo, 'Software\Microsoft\Windows\CurrentVersion\Run', ktString, '', HKEY_CURRENT_USER);
  //
  if Valor <> '' then
  begin
    Executarnainicializao1.Checked   := True;
    NOexecutarnainicializao1.Checked := False;
  end else
  begin
    Executarnainicializao1.Checked   := False;
    NOexecutarnainicializao1.Checked := True;
  end;
end;

procedure TFmPrincipal.ReCaptionComponentesDeForm(Form: TForm);
begin
  //N�o Usa
end;

procedure TFmPrincipal.Segmentos1Click(Sender: TObject);
begin
  SalNewJan.MostraFormSegmentosCads(0);
end;

procedure TFmPrincipal.Similares1Click(Sender: TObject);
begin
  SalNewJan.MostraFormProdutosCabec(0);
end;

procedure TFmPrincipal.SQLite1Click(Sender: TObject);
begin
  REInfo.Lines.Clear;
  REInfo.Visible := True;
  AppPF.ImportaDadosSalNew(REInfo);
end;

procedure TFmPrincipal.Subdivises1Click(Sender: TObject);
begin
  SalNewJan.MostraFormProdDivSubCab(0);
end;

procedure TFmPrincipal.TmConfiguraDBTimer(Sender: TObject);
begin
  TmConfiguraDB.Enabled := False;
  //
  ConfiguraDB;
end;

procedure TFmPrincipal.TmHideTimer(Sender: TObject);
begin
  TmHide.Enabled := False;
  //
  if Visible then
  begin
    if not FHideFirst then
    begin
      FHideFirst := True;
      Application.MainFormOnTaskBar := FShowed;
      Hide;
      //FmPrincipal.WindowState :=  wsMinimized;
      Application.MainFormOnTaskBar := FShowed;
      TrayIcon1.Visible := True;
    end;
  end;
end;

procedure TFmPrincipal.TmVersaoTimer(Sender: TObject);
begin
  TmVersao.Enabled := False;
  //
  if DmkWeb.RemoteConnection then
  begin
    if VerificaNovasVersoes(True) then
      DmkWeb.MostraBalloonHintMenuTopo(ATBBAtualiza, BalloonHint1,
        'H� uma nova vers�o!', 'Clique aqui para atualizar.');
  end;
end;

procedure TFmPrincipal.TrayIcon1Click(Sender: TObject);
begin
  FmPrincipal.Visible := True;
end;

function TFmPrincipal.VerificaNovasVersoes(ApenasVerifica: Boolean): Boolean;
var
  Versao: Integer;
  ArqNome: String;
begin
  Result := DmkWeb.VerificaAtualizacaoVersao2(True, True, 'Quota', 'Quota',
              '', CO_VERSAO, CO_DMKID_APP, Now, nil, dtExecAux, Versao,
              ArqNome, False, ApenasVerifica, BalloonHint1);
end;

end.
