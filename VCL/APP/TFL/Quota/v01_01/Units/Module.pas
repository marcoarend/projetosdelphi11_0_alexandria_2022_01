unit Module;

// C:\Executaveis\Auxiliares\sqlite3.dll
// C:\Dermatek\SQLite\Quota.s3db

interface

uses
  System.SysUtils, System.Classes, Vcl.Forms, Data.DB, ZAbstractConnection,
  ZConnection, ZCompatibility, dmkGeral, ZAbstractRODataset, ZAbstractDataset,
  ZDataset, UnProjGroup_Vars, UnAppEnums, UnGrl_Vars;

type
  TDMod = class(TDataModule)
    MyDB: TZConnection;
    QrLoc: TZQuery;
    QrControle: TZQuery;
    QrAux: TZQuery;
    QrUpd: TZQuery;
    QrNxt: TZQuery;
    QrPsq1: TZQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure RefreshApiBaseInfo();
  end;

var
  DMod: TDMod;

implementation

{%CLASSGROUP 'System.Classes.TPersistent'}

uses ModuleGeralZ, UnVCL_ZDB, UnGrl_DmkDB, UnInternalConsts, MyListas;

{$R *.dfm}

procedure TDMod.DataModuleCreate(Sender: TObject);
begin
  VAR_BDSENHA := 'wkljweryhvbirt';
  VAR_LOCAL_DB_COMPO_DATASET := QrNxt;
  VAR_LOCAL_DB_COMPO_DATABASE := MyDB;
  //
  VAR_GOTOzSQLDBNAME := MyDB;
  //
  VAR_SQLx := TStringList.Create;
  VAR_SQL1 := TStringList.Create;
  VAR_SQL2 := TStringList.Create;
  VAR_SQLa := TStringList.Create;
  //
  MAR_SQLx := TStringList.Create;
  MAR_SQL1 := TStringList.Create;
  MAR_SQLa := TStringList.Create;
  //
  if not VCL_ZDB.ConfiguraBD(MyDB, QrControle, QrLoc) then
    Application.Terminate;
  //
  RefreshApiBaseInfo();
  //
  // no final
  try
    Application.CreateForm(TDmodGZ, DmodGZ);
  except
    Geral.MB_Aviso('Impossível criar Modulo de dados Geral "Z"');
    Application.Terminate;
    Exit;
  end;
end;

procedure TDMod.RefreshApiBaseInfo();
var
  Defindo: Boolean;
begin
{
  VAR_DeviceHost := '';
  VAR_URLBaseAPI := '';
  //VAR_DeviceAssembler := TDevicesAssembler(0);
  Grl_DmkDB.AbreSQLQuery0(QrPsq1, MyDB, [
    'SELECT DeviceAssembler, DeviceHost, URLBaseAPI, DeviceMACAddress ',
    'FROM controle ',
    '']);
  //
  VAR_DeviceAssembler := TDevicesAssembler(QrPsq1.FieldByName('DeviceAssembler').AsInteger);
  VAR_DeviceHost := QrPsq1.FieldByName('DeviceHost').AsString;
  VAR_URLBaseAPI := QrPsq1.FieldByName('URLBaseAPI').AsString;
  VAR_DeviceMACAddress := QrPsq1.FieldByName('DeviceMACAddress').AsString;
  //
  VAR_OkDEVICEeAPI := (Trim(VAR_DeviceHost) <> '') and (Trim(VAR_URLBaseAPI) <> '');
}
end;

end.
