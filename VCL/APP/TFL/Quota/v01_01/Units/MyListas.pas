unit MyListas;

interface

uses System.Classes, System.SysUtils, Generics.Collections, UnDmkEnums,
  UnMyLinguas;
type
  TMyListas = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  CompletaListaSQL(Tabela: String; FListaSQL: TStringList):
              Boolean;
    function  CriaListaCampos(Tabela: String; FLCampos: TList<TCampos>;
              var TemControle: TTemControle): Boolean;
    function  CriaListaIndices(TabelaBase, TabelaNome: String; FLIndices:
              TList<TIndices>): Boolean;
    function  CriaListaTabelas(DatabaseName: String; Lista: TList<TTabelas>):
              Boolean;
    function  CriaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
    function  CriaListaJanelas(FLJanelas: TList<TJanelas>): Boolean;
    function  CriaListaQeiLnk(TabelaCria: String; FLQeiLnk: TList<TQeiLnk>):
              Boolean;
  end;

const
  CO_VERSAO = 1808011355;
  CO_SIGLA_APP = 'QOTA';
  CO_DMKID_APP = 0;
  CO_DBNome = 'Quota';
  CO_GRADE_APP = False;
  CO_VLOCAL = True;
  CO_EXTRA_LCT_003 = False; // True somente para Credito2

var
  MyList   : TMyListas;
  FRCampos : TCampos;
  FRIndices: TIndices;
  FRJanelas: TJanelas;
  FRQeiLnk : TQeiLnk;

implementation

uses
  UnSalNew_Tabs;

{ TMyListas }

function TMyListas.CriaListaTabelas(DatabaseName: String;
  Lista: TList<TTabelas>): Boolean;
begin
  Result := True;
  try
    if LowerCase(DatabaseName) = LowerCase(CO_DBNome) then
    begin
      SalNew_Tabs.CarregaListaTabelas(DatabaseName, Lista);
      //
      MyLinguas.AdTbLst(Lista, False, LowerCase('Controle'), '');
    end else
    begin
      ;
    end;
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaIndices(TabelaBase, TabelaNome: String;
  FLIndices: TList<TIndices>): Boolean;
begin
  Result := True;
  try
    if Uppercase(TabelaBase) = Uppercase('Controle') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else
    begin
      SalNew_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
    end;
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaSQL(Tabela: String;
  FListaSQL: TStringList): Boolean;
begin
  Result := True;
  try
    SalNew_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    ;
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CompletaListaSQL(Tabela: String;
  FListaSQL: TStringList): Boolean;
begin
  Result := True;
  try
    SalNew_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    ;
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaCampos(Tabela: String; FLCampos: TList<TCampos>;
  var TemControle: TTemControle): Boolean;
begin
{
2. Storage Classes and Datatypes
Each value stored in an SQLite database (or manipulated by the database engine)
has one of the following storage classes:

  NULL.    The value is a NULL value.
  INTEGER. The value is a signed integer, stored in 1, 2, 3, 4, 6, or 8 bytes
           depending on the magnitude of the value.
  REAL.    The value is a floating point value, stored as an 8-byte IEEE floating
           point number.
  TEXT.    The value is a text string, stored using the database encoding
           (UTF-8, UTF-16BE or UTF-16LE).
  BLOB.    The value is a blob of data, stored exactly as it was input.

A storage class is more general than a datatype. The INTEGER storage class, for
example, includes 6 different integer datatypes of different lengths. This makes
a difference on disk. But as soon as INTEGER values are read off of disk and into
memory for processing, they are converted to the most general datatype (8-byte
signed integer). And so for the most part, "storage class" is indistinguishable
from "datatype" and the two terms can be used interchangeably.
Any column in an SQLite version 3 database, except an INTEGER PRIMARY KEY column,
may be used to store a value of any storage class.
All values in SQL statements, whether they are literals embedded in SQL statement
text or parameters bound to precompiled SQL statements have an implicit storage
class. Under circumstances described below, the database engine may convert
values between numeric storage classes (INTEGER and REAL) and TEXT during query execution.

2.1. Boolean Datatype
SQLite does not have a separate Boolean storage class. Instead, Boolean values
are stored as integers 0 (false) and 1 (true).

2.2. Date and Time Datatype
SQLite does not have a storage class set aside for storing dates and/or times.
Instead, the built-in Date And Time Functions of SQLite are capable of storing
dates and times as TEXT, REAL, or INTEGER values:
TEXT as ISO8601 strings ("YYYY-MM-DD HH:MM:SS.SSS").
REAL as Julian day numbers, the number of days since noon in Greenwich on
November 24, 4714 B.C. according to the proleptic Gregorian calendar.
INTEGER as Unix Time, the number of seconds since 1970-01-01 00:00:00 UTC.
Applications can chose to store dates and times in any of these formats and
freely convert between formats using the built-in date and time functions.
}
  try
    if Uppercase(Tabela) = Uppercase('Controle') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := 'Chave prim�ria U';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Versao';
      FRCampos.Tipo       := 'bigint(20)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := 'Vers�o do aplicativo';
      FLCampos.Add(FRCampos);
      //
    end else
    begin
      SalNew_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
    end;
    SalNew_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    //
    Result := True;
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaJanelas(FLJanelas: TList<TJanelas>): Boolean;
begin
  //
{
  // ENT-CADAS-001 :: Grupos de Empresas
  New(FRJanelas);
  FRJanelas.ID        := 'ENT-CADAS-001';
  FRJanelas.Nome      := 'FmEntidadesGrup';
  FRJanelas.Descricao := 'Grupos de Empresas';
  FLJanelas.Add(FRJanelas);
}
  //
  SalNew_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Result := True;
end;


function TMyListas.CriaListaQeiLnk(TabelaCria: String;
  FLQeiLnk: TList<TQeiLnk>): Boolean;
begin
  Result := True;
  try
(*
    if Uppercase(TabelaCria) = Uppercase('VSEntiMP') then
    begin
      New(FRQeiLnk);
      FRQeiLnk.AskrTab       := TabelaCria;
      FRQeiLnk.AskrCol       := 'Codigo';
      FRQeiLnk.RplyTab       := '';
      FRQeiLnk.RplyCol       := '';
      FRQeiLnk.Purpose       := TItemTuplePurpose.itpUSGSrvrInc;
      FRQeiLnk.Seq_in_pref   := 1;
      FLQeiLnk.Add(FRQeiLnk);
      //
      New(FRQeiLnk);
      FRQeiLnk.AskrTab       := TabelaCria;
      FRQeiLnk.AskrCol       := 'MPVImpOpc';
      FRQeiLnk.RplyTab       := 'MPVImpOpc';
      FRQeiLnk.RplyCol       := 'Codigo';
      FRQeiLnk.Purpose       := TItemTuplePurpose.itpRelatUsrSrv;
      FRQeiLnk.Seq_in_pref   := 1;
      FLQeiLnk.Add(FRQeiLnk);
      //
      //
    //end else if Uppercase(TabelaBase) = Uppercase('?) then
    //begin
    end else
*)
    begin
      SalNew_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
    end;
  except
    raise;
    Result := False;
  end;
end;

{
function TMyListas.CriaListaTabelas(DatabaseName: String;
  Lista: TList<TTabelas>): Boolean;
begin
  Result := True;
  try
    if LowerCase(DatabaseName) = LowerCase(CO_DBNome) then
    begin
      MyLinguas.AdTbLst(Lista, False, LowerCase('Controle'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('EmbalagensCad'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('EmbalagensTar'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('EntidadesAccn'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('EntidadesEmpr'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('EntidadesGrup'), '');
      //MyLinguas.AdTbLst(Lista, False, LowerCase('LoadSalesStru'), '');
      //MyLinguas.AdTbLst(Lista, False, LowerCase('HvSrDBCad'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('LoadSalesData'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('ProdDivsaoCab'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('ProdDivSubCab'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('ProdLinhasCab'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('ProdutosCabec'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('ProdutosCadas'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('ProdutosEmbal'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('ProdutosFamil'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('SalesOperacao'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('SalesPerfilCl'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('SegmenGruCads'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('SegmentosCads'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('SegmentosSGEA'), '');
    end else
    begin
      ;
    end;
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaIndices(TabelaBase, TabelaNome: String;
  FLIndices: TList<TIndices>): Boolean;
begin
  Result := True;
  try
    if Uppercase(TabelaBase) = Uppercase('Controle') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('EmbalagensCad') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'CodUsu';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('EmbalagensTar') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'CodUsu';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('EntidadesAccn') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'CodUsu';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('EntidadesEmpr') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'CodUsu';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('EntidadesGrup') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'CodUsu';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('LoadSalesStru') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('HvSrDBCad') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('LoadSalesData') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('ProdDivsaoCab') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'CodUsu';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('ProdDivSubCab') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'CodUsu';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('ProdLinhasCab') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'CodUsu';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('ProdutosCabec') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'CodUsu';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('ProdutosCadas') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'CodUsu';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('ProdutosEmbal') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'CodUsu';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('ProdutosFamil') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'CodUsu';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('SalesOperacao') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'CodUsu';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('SalesPerfilCl') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'CodUsu';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('SegmenGruCads') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'CodUsu';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('SegmentosCads') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'CodUsu';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE2';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Sigla';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('SegmentosSGEA') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'CodUsu';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE2';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Segmento';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE2';
      FRIndices.Seq_in_index := 2;
      FRIndices.Column_name  := 'EntidadesEmp';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE2';
      FRIndices.Seq_in_index := 3;
      FRIndices.Column_name  := 'Account';
      FLIndices.Add(FRIndices);
      //
(*
    end else
    if Uppercase(TabelaBase) = Uppercase('Devices') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'UNIQUE1';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'DeviceAssembler';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'UNIQUE1';
      FRIndices.Seq_in_index  := 2;
      FRIndices.Column_name   := 'UniqueId';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('Leituras') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'DeviceType';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 2;
      FRIndices.Column_name   := 'DeviceSeq';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 3;
      FRIndices.Column_name   := 'ProdDta';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('Loggers') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'UNIQUE1';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'DeviceAssembler';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'UNIQUE1';
      FRIndices.Seq_in_index  := 2;
      FRIndices.Column_name   := 'UniqueId';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('OprConsMes') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 2;
      FRIndices.Column_name   := 'AnoRef';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 3;
      FRIndices.Column_name   := 'MesRef';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('OprConsMesIts') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('OprConsMesFin') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('WacProdHead') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('WacProdBody') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('WacProdValues') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'DeviceType';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 2;
      FRIndices.Column_name   := 'DeviceSeq';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 3;
      FRIndices.Column_name   := 'ProdDta';
      FLIndices.Add(FRIndices);
      //
    end else
    begin
*)
      ;
    end;
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaSQL(Tabela: String;
  FListaSQL: TStringList): Boolean;
begin
  Result := True;
  try
    ;
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CompletaListaSQL(Tabela: String;
  FListaSQL: TStringList): Boolean;
begin
  Result := True;
  try
    ;
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaCampos(Tabela: String; FLCampos: TList<TCampos>;
  var TemControle: TTemControle): Boolean;
begin
(*
2. Storage Classes and Datatypes
Each value stored in an SQLite database (or manipulated by the database engine)
has one of the following storage classes:

  NULL.    The value is a NULL value.
  INTEGER. The value is a signed integer, stored in 1, 2, 3, 4, 6, or 8 bytes
           depending on the magnitude of the value.
  REAL.    The value is a floating point value, stored as an 8-byte IEEE floating
           point number.
  TEXT.    The value is a text string, stored using the database encoding
           (UTF-8, UTF-16BE or UTF-16LE).
  BLOB.    The value is a blob of data, stored exactly as it was input.

A storage class is more general than a datatype. The INTEGER storage class, for
example, includes 6 different integer datatypes of different lengths. This makes
a difference on disk. But as soon as INTEGER values are read off of disk and into
memory for processing, they are converted to the most general datatype (8-byte
signed integer). And so for the most part, "storage class" is indistinguishable
from "datatype" and the two terms can be used interchangeably.
Any column in an SQLite version 3 database, except an INTEGER PRIMARY KEY column,
may be used to store a value of any storage class.
All values in SQL statements, whether they are literals embedded in SQL statement
text or parameters bound to precompiled SQL statements have an implicit storage
class. Under circumstances described below, the database engine may convert
values between numeric storage classes (INTEGER and REAL) and TEXT during query execution.

2.1. Boolean Datatype
SQLite does not have a separate Boolean storage class. Instead, Boolean values
are stored as integers 0 (false) and 1 (true).

2.2. Date and Time Datatype
SQLite does not have a storage class set aside for storing dates and/or times.
Instead, the built-in Date And Time Functions of SQLite are capable of storing
dates and times as TEXT, REAL, or INTEGER values:
TEXT as ISO8601 strings ("YYYY-MM-DD HH:MM:SS.SSS").
REAL as Julian day numbers, the number of days since noon in Greenwich on
November 24, 4714 B.C. according to the proleptic Gregorian calendar.
INTEGER as Unix Time, the number of seconds since 1970-01-01 00:00:00 UTC.
Applications can chose to store dates and times in any of these formats and
freely convert between formats using the built-in date and time functions.
*)
  try
    if Uppercase(Tabela) = Uppercase('Controle') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := 'Chave prim�ria U';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Versao';
      FRCampos.Tipo       := 'bigint(20)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := 'Vers�o do aplicativo';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EmbalagensCad') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(120)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sigla';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HvSrDB';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EmbalagensTar') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(120)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
(*
      //
      New(FRCampos);
      FRCampos.Field      := 'Descricao';
      FRCampos.Tipo       := 'varchar(120)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
*)
      //
      New(FRCampos);
      FRCampos.Field      := 'EmbalagensCad';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sigla';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PesoLiq';
      FRCampos.Tipo       := 'real';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PesoTar';
      FRCampos.Tipo       := 'real';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PPallet';
      FRCampos.Tipo       := 'real';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ItmsInPalt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HvSrDB';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EntidadesAccn') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sigla';
      FRCampos.Tipo       := 'varchar(6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HvSrDB';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EntidadesEmpr') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GrupoEmpresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GrupEmprSN';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pais';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Regiao';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UF';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cidade';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Perfil';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LegalUnit';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HvSrDB';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EntidadesGrup') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HvSrDB';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('LoadSalesStru') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('HvSrDBCad') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('LoadSalesData') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'GruLoad';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'xSegmento';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'xGrupoSegmento';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'xAccount';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?????';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'xGrupoEmpresa';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'cCodCli';
      FRCampos.Tipo       := 'integer(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'xCliente';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'xPais';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'xRegiao';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'xUF';
      FRCampos.Tipo       := 'varchar(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'xCidade';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'xPerfil';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'cCodCliFatura';
      FRCampos.Tipo       := 'integer(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'xClienteFatura';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'cCodCliDestino';
      FRCampos.Tipo       := 'integer(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'xClienteDestino';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'xDivisao';
      FRCampos.Tipo       := 'varchar(120)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'xSubDivisao';
      FRCampos.Tipo       := 'varchar(120)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'xLinha';
      FRCampos.Tipo       := 'varchar(120)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'xFamilia';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'xEmbalagem';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'xProduto';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'cCodProd';
      FRCampos.Tipo       := 'Integer(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'dData';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'xOperacao';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'xMoeda';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'xMoedaPed';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'qQuantidade';
      FRCampos.Tipo       := 'real';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'qBrutoBRL';
      FRCampos.Tipo       := 'real';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'qLiquidoBRL';
      FRCampos.Tipo       := 'real';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'qLiquidoUSD';
      FRCampos.Tipo       := 'real';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'qLiquidoEUR';
      FRCampos.Tipo       := 'real';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'qCustoBRL';
      FRCampos.Tipo       := 'real';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'qCustoUSD';
      FRCampos.Tipo       := 'real';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'qCustoEUR';
      FRCampos.Tipo       := 'real';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'qMPBRL';
      FRCampos.Tipo       := 'real';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'qMPUSD';
      FRCampos.Tipo       := 'real';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'qDGFBRL';
      FRCampos.Tipo       := 'real';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'qDGFUSD';
      FRCampos.Tipo       := 'real';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'qDGFEUR';
      FRCampos.Tipo       := 'real';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'cLegalUnit';
      FRCampos.Tipo       := 'Integer(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
(*
      New(FRCampos);
      FRCampos.Field      := 'cPlanta';
      FRCampos.Tipo       := 'Integer(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'xTrimestre';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
*)
    end else
    if Uppercase(Tabela) = Uppercase('ProdDivsaoCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sigla';
      FRCampos.Tipo       := 'varchar(6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HvSrDB';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('ProdDivSubCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sigla';
      FRCampos.Tipo       := 'varchar(6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HvSrDB';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('ProdLinhasCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sigla';
      FRCampos.Tipo       := 'varchar(6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HvSrDB';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('ProdutosCabec') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HvSrDB';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('ProdutosCadas') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ProdCabec';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
(*
      New(FRCampos);
      FRCampos.Field      := 'CodProd';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
*)
      New(FRCampos);
      FRCampos.Field      := 'GrupoSegmento';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Segmento';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Divisao';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SubDivisao';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Familia';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HvSrDB';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('ProdutosEmbal') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodProd';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ProdutosCadas';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EmbalagensTar';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HvSrDB';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('ProdutosFamil') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sigla';
      FRCampos.Tipo       := 'varchar(6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HvSrDB';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SalesOperacao') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EhFatura';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HvSrDB';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SalesPerfilCl') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EhFatura';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HvSrDB';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SegmenGruCads') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sigla';
      FRCampos.Tipo       := 'varchar(6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HvSrDB';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SegmentosCads') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sigla';
      FRCampos.Tipo       := 'varchar(6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HvSrDB';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SegmentosSGEA') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := ' ';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Segmento';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EntidadesEmp';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Account';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AccntSN';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HvSrDB';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
    end;
    ;
    //
    Result := True;
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaJanelas(FLJanelas: TList<TJanelas>): Boolean;
begin
  //
  // ENT-CADAS-001 :: Grupos de Empresas
  New(FRJanelas);
  FRJanelas.ID        := 'ENT-CADAS-001';
  FRJanelas.Nome      := 'FmEntidadesGrup';
  FRJanelas.Descricao := 'Grupos de Empresas';
  FLJanelas.Add(FRJanelas);
  //
  // ENT-CADAS-002 :: Cadastro de Clientes
  New(FRJanelas);
  FRJanelas.ID        := 'ENT-CADAS-002';
  FRJanelas.Nome      := 'FmEntidadesEmpr';
  FRJanelas.Descricao := 'Cadastro de Clientes';
  FLJanelas.Add(FRJanelas);
  //
  // ENT-CADAS-003 :: Cadastro de Cliente x Segmento x Account
  New(FRJanelas);
  FRJanelas.ID        := 'ENT-CADAS-003';
  FRJanelas.Nome      := 'FmSegmentosSGEA';
  FRJanelas.Descricao := 'Cadastro de Cliente x Segmento x Account';
  FLJanelas.Add(FRJanelas);
  //
  // ENT-CADAS-004 :: Cadastro de Accounts
  New(FRJanelas);
  FRJanelas.ID        := 'ENT-CADAS-004';
  FRJanelas.Nome      := 'FmEntidadesAccn';
  FRJanelas.Descricao := 'Cadastro de Accounts';
  FLJanelas.Add(FRJanelas);
  //
  // LOD-SALES-001 :: Importa��o de Movimento de Produtos
  New(FRJanelas);
  FRJanelas.ID        := 'LOD-SALES-001';
  FRJanelas.Nome      := 'FmLoadSales';
  FRJanelas.Descricao := 'Importa��o de Movimento de Produtos';
  FLJanelas.Add(FRJanelas);
  //
  // LOD-SALES-002 :: Equival�ncia Intercambial de Produtos
  New(FRJanelas);
  FRJanelas.ID        := 'LOD-SALES-002';
  FRJanelas.Nome      := 'FmLoadSalesGen';
  FRJanelas.Descricao := 'Equival�ncia Intercambial de Produtos';
  FLJanelas.Add(FRJanelas);
  //
  // LOD-SALES-003 :: Atrelamento de Produtos Equivalentes
  New(FRJanelas);
  FRJanelas.ID        := 'LOD-SALES-003';
  FRJanelas.Nome      := 'FmLoadSalesGenAtrel';
  FRJanelas.Descricao := 'Atrelamento de Produtos Equivalentes';
  FLJanelas.Add(FRJanelas);
  //
  // PRD-CADAS-001 :: Cadastro de Produtos
  New(FRJanelas);
  FRJanelas.ID        := 'PRD-CADAS-001';
  FRJanelas.Nome      := 'FmProdutosCadas';
  FRJanelas.Descricao := 'Grupos de Empresas';
  FLJanelas.Add(FRJanelas);
  //
  // PRD-CADAS-002 :: Cadastro de Refer�ncia de Produtos
  New(FRJanelas);
  FRJanelas.ID        := 'PRD-CADAS-002';
  FRJanelas.Nome      := 'FmProdutosCabec';
  FRJanelas.Descricao := 'Cadastro de Refer�ncia de Produtos';
  FLJanelas.Add(FRJanelas);
  //
  // PRD-CADAS-003 :: Divis�es de Produtos
  New(FRJanelas);
  FRJanelas.ID        := 'PRD-CADAS-003';
  FRJanelas.Nome      := 'FmProdDivsaoCab';
  FRJanelas.Descricao := 'Divis�es de Produtos';
  FLJanelas.Add(FRJanelas);
  //
  // PRD-CADAS-004 :: Sub-divis�es de Produtos
  New(FRJanelas);
  FRJanelas.ID        := 'PRD-CADAS-004';
  FRJanelas.Nome      := 'FmProdDivSubCab';
  FRJanelas.Descricao := 'Sub-divis�es de Produtos';
  FLJanelas.Add(FRJanelas);
  //
  // PRD-CADAS-005 :: Linhas de Produtos
  New(FRJanelas);
  FRJanelas.ID        := 'PRD-CADAS-005';
  FRJanelas.Nome      := 'FmProdLinhasCab';
  FRJanelas.Descricao := 'Linhas de Produtos';
  FLJanelas.Add(FRJanelas);
  //
  // PRD-CADAS-006 :: Fam�lias de Produtos
  New(FRJanelas);
  FRJanelas.ID        := 'PRD-CADAS-006';
  FRJanelas.Nome      := 'FmProdutosFamil';
  FRJanelas.Descricao := 'Fam�lias de Produtos';
  FLJanelas.Add(FRJanelas);
  //
  // PRD-CADAS-007 :: Cadastro de Embalagens
  New(FRJanelas);
  FRJanelas.ID        := 'PRD-CADAS-007';
  FRJanelas.Nome      := 'FmEmbalagensCad';
  FRJanelas.Descricao := 'Cadastro de Embalagens';
  FLJanelas.Add(FRJanelas);
  //
  // PRD-CADAS-008 :: Cadastro de Taras de Embalagem
  New(FRJanelas);
  FRJanelas.ID        := 'PRD-CADAS-008';
  FRJanelas.Nome      := 'FmEmbalagensTar';
  FRJanelas.Descricao := 'Cadastro de Taras de Embalagem';
  FLJanelas.Add(FRJanelas);
  //
  // PRD-CADAS-009 :: Cadastro de Produto x Embalagem
  New(FRJanelas);
  FRJanelas.ID        := 'PRD-CADAS-009';
  FRJanelas.Nome      := 'FmProdutosEmbal';
  FRJanelas.Descricao := 'Cadastro de Produto x Embalagem';
  FLJanelas.Add(FRJanelas);
  //
  // SEG-CADAS-001 :: Cadastro de Grupos de Segmentos
  New(FRJanelas);
  FRJanelas.ID        := 'SEG-CADAS-001';
  FRJanelas.Nome      := 'FmSegemenGruCadas';
  FRJanelas.Descricao := 'Cadastro de Grupos de Segmentos';
  FLJanelas.Add(FRJanelas);
  //
  // SEG-CADAS-002 :: Cadastro de Segmentos
  New(FRJanelas);
  FRJanelas.ID        := 'SEG-CADAS-001';
  FRJanelas.Nome      := 'FmSegementosCadas';
  FRJanelas.Descricao := 'Cadastro de Segmentos';
  FLJanelas.Add(FRJanelas);
  //
  Result := True;
end;
}

end.
