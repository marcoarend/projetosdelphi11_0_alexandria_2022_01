unit EmbalagensTar;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, (***UnGOTOy,***) UnInternalConsts, UnMsgInt, (***UMySQLModule,***) Menus,
  UnInternalConsts2, mySQLDbTables, (***UnMySQLCuringa,***) dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, ZAbstractRODataset, ZDataset, ZAbstractDataset,
  //
  UnGOTOz, UnGrl_DmkDB, dmkEditF7, dmkDBLookupComboBox, dmkEditCB;

type
  TFmEmbalagensTar = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    DBEdCodigo: TdmkDBEdit;
    DBEdNome: TdmkDBEdit;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DGDados: TDBGrid;
    QrEmbalagensTar: TZQuery;
    DsEmbalagensTar: TDataSource;
    QrProdutosEmbal: TZQuery;
    DsProdutosEmbal: TDataSource;
    EdSigla: TdmkEdit;
    Label3: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    Label9: TLabel;
    Label1: TLabel;
    DBEdCodUsu: TdmkDBEdit;
    Label8: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    QrEmbalagensTarNO_HvSrDB: TWideMemoField;
    EdCodUsu: TdmkEdit;
    Label24: TLabel;
    EdItmsInPalt: TdmkEdit;
    Label11: TLabel;
    EdPesoLiq: TdmkEdit;
    Label12: TLabel;
    Label5: TLabel;
    EdOrdem: TdmkEdit;
    EdHvSrDB: TdmkEditF7;
    Label22: TLabel;
    DBEdit4: TDBEdit;
    dmkDBEdit2: TdmkDBEdit;
    EdPesoTar: TdmkEdit;
    Label15: TLabel;
    EdPPallet: TdmkEdit;
    Label16: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    Label10: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    DBEdit1: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    QrEmbalagensCad: TZQuery;
    QrEmbalagensCadCodigo: TLargeintField;
    DsEmbalagensCad: TDataSource;
    Label25: TLabel;
    EdEmbalagensCad: TdmkEditCB;
    CBEmbalagensCad: TdmkDBLookupComboBox;
    dmkDBEdit3: TdmkDBEdit;
    Label19: TLabel;
    dmkDBEdit4: TdmkDBEdit;
    QrEmbalagensTarNO_EmbalagensCad: TWideStringField;
    QrEmbalagensTarCodigo: TLargeintField;
    QrEmbalagensTarCodUsu: TLargeintField;
    QrEmbalagensTarNome: TWideStringField;
    QrEmbalagensTarEmbalagensCad: TLargeintField;
    QrEmbalagensTarSigla: TWideStringField;
    QrEmbalagensTarPesoLiq: TFloatField;
    QrEmbalagensTarPesoTar: TFloatField;
    QrEmbalagensTarPPallet: TFloatField;
    QrEmbalagensTarItmsInPalt: TLargeintField;
    QrEmbalagensTarOrdem: TLargeintField;
    QrEmbalagensTarHvSrDB: TSmallintField;
    QrEmbalagensTarLk: TLargeintField;
    QrEmbalagensTarDataCad: TDateField;
    QrEmbalagensTarDataAlt: TDateField;
    QrEmbalagensTarUserCad: TLargeintField;
    QrEmbalagensTarUserAlt: TLargeintField;
    QrEmbalagensTarAlterWeb: TSmallintField;
    QrEmbalagensTarAtivo: TSmallintField;
    QrProdutosEmbalNO_EmbalagensTar: TWideStringField;
    QrProdutosEmbalNO_ProdutosCadas: TWideStringField;
    QrProdutosEmbalCodigo: TLargeintField;
    QrProdutosEmbalCodUsu: TLargeintField;
    QrProdutosEmbalCodProd: TLargeintField;
    QrProdutosEmbalProdutosCadas: TLargeintField;
    QrProdutosEmbalEmbalagensTar: TLargeintField;
    QrProdutosEmbalNome: TWideStringField;
    QrProdutosEmbalHvSrDB: TSmallintField;
    QrProdutosEmbalLk: TLargeintField;
    QrProdutosEmbalDataCad: TDateField;
    QrProdutosEmbalDataAlt: TDateField;
    QrProdutosEmbalUserCad: TLargeintField;
    QrProdutosEmbalUserAlt: TLargeintField;
    QrProdutosEmbalAlterWeb: TSmallintField;
    QrProdutosEmbalAtivo: TSmallintField;
    QrEmbalagensCadSigla: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrGraGruYAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrGraGruYBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrGraGruYAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrGraGruYBeforeClose(DataSet: TDataSet);
    procedure QrEmbalagensTarNO_HvSrDBGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure QrEmbalagensTarBeforeClose(DataSet: TDataSet);
    procedure QrEmbalagensTarAfterScroll(DataSet: TDataSet);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraCadastro_Com_Itens_ITS(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenEmbalagensCad();
    procedure ReopenProdutosEmbal(Codigo: Integer);

  end;

var
  FmEmbalagensTar: TFmEmbalagensTar;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module;//, (***MyDBCheck, DmkDAC_PF,***) Cadastro_Com_Itens_ITS;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmEmbalagensTar.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOz.LC(Atual, Codigo);
end;

procedure TFmEmbalagensTar.MostraCadastro_Com_Itens_ITS(SQLType: TSQLType);
begin
{
  if Grl_DmkDB.CriaFm(TFmCadastro_Com_Itens_ITS, FmCadastro_Com_Itens_ITS, afmoNegarComAviso) then
  begin
    FmCadastro_Com_Itens_ITS.ImgTipo.SQLType := SQLType;
    FmCadastro_Com_Itens_ITS.FQrCab := QrEmbalagensTar;
    FmCadastro_Com_Itens_ITS.FDsCab := DsCadastro_Com_Itens_CAB;
    FmCadastro_Com_Itens_ITS.FQrProdutosEmbal := QrProdutosEmbal;
    if SQLType = stIns then
      FmCadastro_Com_Itens_ITS.EdCPF1.ReadOnly := False
    else
    begin
      FmCadastro_Com_Itens_ITS.EdControle.ValueVariant := QrProdutosEmbalControle.Value;
      //
      FmCadastro_Com_Itens_ITS.EdCPF1.Text := MLAGeral.FormataCNPJ_TFT(QrProdutosEmbalCNPJ_CPF.Value);
      FmCadastro_Com_Itens_ITS.EdNomeEmiSac.Text := QrProdutosEmbalNome.Value;
      FmCadastro_Com_Itens_ITS.EdCPF1.ReadOnly := True;
    end;
    FmCadastro_Com_Itens_ITS.ShowModal;
    FmCadastro_Com_Itens_ITS.Destroy;
  end;
}
end;

procedure TFmEmbalagensTar.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrEmbalagensTar);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrEmbalagensTar, QrProdutosEmbal);
end;

procedure TFmEmbalagensTar.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrEmbalagensTar);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrProdutosEmbal);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrProdutosEmbal);
end;

procedure TFmEmbalagensTar.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOz.Go(Para, QrEmbalagensTarCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmEmbalagensTar.DefParams;
begin
  VAR_GOTOTABELA := 'embalagenstar';
  VAR_GOTOzSQLTABLE := QrEmbalagensTar;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOzSQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOz.LimpaVAR_SQL();

  VAR_SQLx.Add('SELECT ');
  VAR_SQLx.Add(dmkPF.ArrayToTextoCASEWHEN('sca.HvSrDB', 'NO_HvSrDB',
    TPosVirgula.pvPos, sHaveSrcDB, iHaveSrcDB) +
  'emb.Nome NO_EmbalagensCad, sca.*');
  VAR_SQLx.Add('FROM embalagenstar sca');
  VAR_SQLx.Add('LEFT JOIN embalagenscad emb ON emb.Codigo=sca.EmbalagensCad');
  VAR_SQLx.Add('WHERE sca.Codigo > 0');
  //
  VAR_SQL1.Add('AND sca.Codigo=:P0');
  //
  VAR_SQL2.Add('AND sca.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND sca.Nome Like :P0');
  //
end;

procedure TFmEmbalagensTar.ItsAltera1Click(Sender: TObject);
begin
  MostraCadastro_Com_Itens_ITS(stUpd);
end;

procedure TFmEmbalagensTar.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmEmbalagensTar.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmEmbalagensTar.QueryPrincipalAfterOpen;
begin
end;

procedure TFmEmbalagensTar.ReopenEmbalagensCad();
begin
  Grl_DmkDB.AbreSQLQuery0(QrEmbalagensCad, Dmod.MyDB, [
  'SELECT Codigo, Sigla ',
  'FROM embalagenscad  ',
  'ORDER BY Nome ',
  '']);
end;

procedure TFmEmbalagensTar.ReopenProdutosEmbal(Codigo: Integer);
begin
  Grl_DmkDB.AbreSQLQuery0(QrProdutosEmbal, Dmod.MyDB, [
  'SELECT tar.Nome NO_EmbalagensTar, ',
  'pca.Nome NO_ProdutosCadas, pem.* ',
  'FROM produtosembal pem',
  'LEFT JOIN produtoscadas pca ON pca.Codigo=pem.ProdutosCadas',
  'LEFT JOIN embalagenstar tar ON tar.Codigo=pem.EmbalagensTar',
  'WHERE pem.EmbalagensTar=' + Geral.FF0(QrEmbalagensTarCodigo.Value),
  'ORDER BY NO_ProdutosCadas, CodProd',
  '']);
  //
  QrProdutosEmbal.Locate('Codigo', Codigo, []);
end;

procedure TFmEmbalagensTar.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
{
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'Cadastro_Com_Itens_ITS', 'Controle', QrProdutosEmbalControle.Value, Dmod.MyDB?) = ID_YES then
  begin
    Controle := GOTOz.LocalizaPriorNextIntQr(QrProdutosEmbal,
      QrProdutosEmbalControle, QrProdutosEmbalControle.Value);
    ReopenProdutosEmbal(Controle);
  end;
}
end;

procedure TFmEmbalagensTar.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmEmbalagensTar.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmEmbalagensTar.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmEmbalagensTar.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmEmbalagensTar.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmEmbalagensTar.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEmbalagensTar.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrEmbalagensTarCodigo.Value;
  Close;
end;

procedure TFmEmbalagensTar.ItsInclui1Click(Sender: TObject);
begin
  MostraCadastro_Com_Itens_ITS(stIns);
end;

procedure TFmEmbalagensTar.CabAltera1Click(Sender: TObject);
begin
  Grl_DmkDB.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrEmbalagensTar, [PnDados],
  [PnEdita], EdSigla, ImgTipo, 'embalagenstar');
end;

procedure TFmEmbalagensTar.BtConfirmaClick(Sender: TObject);
var
  Nome, (*Descricao,*) Sigla: String;
  Codigo, CodUsu, EmbalagensCad, ItmsInPalt, Ordem, HvSrDB: Integer;
  PesoLiq, PesoTar, PPallet: Double;
  SQLType: TSQLType;
begin
  SQLType             := ImgTipo.SQLType;
  //
  //Codigo              := EdCodigo.ValueVariant;
  //CodUsu              := EdCodUsu.ValueVariant;
  Grl_DmkDB.ObtemCodigoECodUsu(EdCodigo, EdCodUsu, SQLType, Codigo, CodUsu,
    'embalagenstar', 'Codigo', 'CodUsu');
  Nome                := EdNome.ValueVariant;
  //Descricao           := EdDescricao.ValueVariant;
  EmbalagensCad       := EdEmbalagensCad.ValueVariant;
  Sigla               := EdSigla.ValueVariant;
  PesoLiq             := EdPesoLiq.ValueVariant;
  PesoTar             := EdPesoTar.ValueVariant;
  PPallet             := EdPPallet.ValueVariant;
  ItmsInPalt          := EdItmsInPalt.ValueVariant;
  Ordem               := EdOrdem.ValueVariant;
  HvSrDB              := EdHvSrDB.ValueVariant;
  //
  //Codigo := Grl_DmkDB.GetNxtCodigoInt('embalagenstar', 'Codigo', SQLType, Codigo);
  if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, 'embalagenstar', False, [
  'CodUsu', 'Nome', //'Descricao',
  'EmbalagensCad', 'Sigla', 'PesoLiq',
  'PesoTar', 'PPallet', 'ItmsInPalt',
  'Ordem', 'HvSrDB'], [
  'Codigo'], [
  CodUsu, Nome, //Descricao,
  EmbalagensCad, Sigla, PesoLiq,
  PesoTar, PPallet, ItmsInPalt,
  Ordem, HvSrDB], [
  Codigo], True, dmksqlinsInsOnly, '', stDesktop, False) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOz.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmEmbalagensTar.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  //if ImgTipo.SQLType = stIns then
    //UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'embalagenstar', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  GOTOz.UpdUnlockZ(Codigo, Dmod.MyDB, 'embalagenstar', 'Codigo');
end;

procedure TFmEmbalagensTar.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmEmbalagensTar.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmEmbalagensTar.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
  //
  ReopenEmbalagensCad();
end;

procedure TFmEmbalagensTar.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOz.Codigo(QrEmbalagensTarCodigo.Value, LaRegistro.Caption);
end;

procedure TFmEmbalagensTar.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOz.Nome(LaRegistro.Caption);
end;

procedure TFmEmbalagensTar.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOz.CodUsu(QrEmbalagensTarCodigo.Value, LaRegistro.Caption);
end;

procedure TFmEmbalagensTar.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOz.Fechar(ImgTipo.SQLType);
end;

procedure TFmEmbalagensTar.QrGraGruYAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmEmbalagensTar.QrGraGruYAfterScroll(DataSet: TDataSet);
begin
  ReopenProdutosEmbal(0);
end;

procedure TFmEmbalagensTar.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrEmbalagensTarCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmEmbalagensTar.SbQueryClick(Sender: TObject);
begin
  LocCod(QrEmbalagensTarCodigo.Value,
  GOTOz.CriaForm(CO_CODIGO, CO_NOME, 'embalagenstar', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmEmbalagensTar.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEmbalagensTar.CabInclui1Click(Sender: TObject);
begin
  Grl_DmkDB.ConfigPanelInsUpd(stIns, Self, PnEdita, QrEmbalagensTar, [PnDados],
  [PnEdita], EdSigla, ImgTipo, 'embalagenstar');
  EdHvSrDB.ValueVariant := Integer(THaveSrcDB.hsdbCadastr);
end;

procedure TFmEmbalagensTar.QrGraGruYBeforeClose(
  DataSet: TDataSet);
begin
  QrProdutosEmbal.Close;
end;

procedure TFmEmbalagensTar.QrGraGruYBeforeOpen(DataSet: TDataSet);
begin
  QrEmbalagensTarCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmEmbalagensTar.QrEmbalagensTarAfterScroll(DataSet: TDataSet);
begin
  ReopenProdutosEmbal(0);
end;

procedure TFmEmbalagensTar.QrEmbalagensTarBeforeClose(DataSet: TDataSet);
begin
  QrProdutosEmBal.Close;
end;

procedure TFmEmbalagensTar.QrEmbalagensTarNO_HvSrDBGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  Text := TStringField(Sender).Value;
end;

end.

