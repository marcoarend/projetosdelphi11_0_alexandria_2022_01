unit EntidadesCptt;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, (***UnGOTOy,***) UnInternalConsts, UnMsgInt, (***UMySQLModule,***) Menus,
  UnInternalConsts2, mySQLDbTables, (***UnMySQLCuringa,***) dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, ZAbstractRODataset, ZDataset, ZAbstractDataset,
  //
  UnGOTOz, UnGrl_DmkDB, dmkEditF7;

type
  TFmEntidadesCptt = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DGDados: TDBGrid;
    QrEntidadesCptt: TZQuery;
    DsEntidadesCptt: TDataSource;
    QrProdutosCadas: TZQuery;
    DsProdutosCadas: TDataSource;
    EdSigla: TdmkEdit;
    Label3: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    Label4: TLabel;
    Label9: TLabel;
    Label1: TLabel;
    DBEdCodUsu: TdmkDBEdit;
    Label8: TLabel;
    DBEdit2: TDBEdit;
    Label10: TLabel;
    DBEdit3: TDBEdit;
    EdCodUsu: TdmkEdit;
    Label24: TLabel;
    EdHvSrDB: TdmkEditF7;
    Label22: TLabel;
    EdOrdem: TdmkEdit;
    Label5: TLabel;
    QrEntidadesCpttNO_HvSrDB: TWideMemoField;
    QrEntidadesCpttCodigo: TLargeintField;
    QrEntidadesCpttCodUsu: TLargeintField;
    QrEntidadesCpttIDServr: TLargeintField;
    QrEntidadesCpttNome: TWideStringField;
    QrEntidadesCpttSigla: TWideStringField;
    QrEntidadesCpttURLProds: TWideMemoField;
    QrEntidadesCpttLk: TLargeintField;
    QrEntidadesCpttDataCad: TDateField;
    QrEntidadesCpttDataAlt: TDateField;
    QrEntidadesCpttUserCad: TLargeintField;
    QrEntidadesCpttUserAlt: TLargeintField;
    QrEntidadesCpttAlterWeb: TSmallintField;
    QrEntidadesCpttAtivo: TSmallintField;
    QrProdutosCadasLista: TLargeintField;
    QrProdutosCadasCodigo: TLargeintField;
    QrProdutosCadasCodUsu: TLargeintField;
    QrProdutosCadasIDServr: TLargeintField;
    QrProdutosCadasEntidadesCptt: TLargeintField;
    QrProdutosCadasProdCabec: TLargeintField;
    QrProdutosCadasGrupoSegmento: TLargeintField;
    QrProdutosCadasSegmento: TLargeintField;
    QrProdutosCadasDivisao: TLargeintField;
    QrProdutosCadasSubDivisao: TLargeintField;
    QrProdutosCadasFamilia: TLargeintField;
    QrProdutosCadasNome: TWideStringField;
    QrProdutosCadasHvSrDB: TSmallintField;
    QrProdutosCadasDiluicao: TLargeintField;
    QrProdutosCadasBoca: TSmallintField;
    QrProdutosCadasIQSegment: TWideStringField;
    QrProdutosCadasIQDivsao: TWideStringField;
    QrProdutosCadasIQDivSub: TWideStringField;
    QrProdutosCadasIQFamily: TWideStringField;
    QrProdutosCadasNatureza: TWideStringField;
    QrProdutosCadasPropriedades: TWideMemoField;
    QrProdutosCadaspH: TWideStringField;
    QrProdutosCadasCargaIonica: TWideStringField;
    QrProdutosCadasAspecto: TWideStringField;
    QrProdutosCadasUtilizacao: TWideMemoField;
    QrProdutosCadasPerc_Uso: TWideStringField;
    QrProdutosCadasConcOuAtiv: TWideStringField;
    QrProdutosCadasSolidezLuz: TWideStringField;
    QrProdutosCadasLocalPDF: TWideStringField;
    QrProdutosCadasLk: TLargeintField;
    QrProdutosCadasDataCad: TDateField;
    QrProdutosCadasDataAlt: TDateField;
    QrProdutosCadasUserCad: TLargeintField;
    QrProdutosCadasUserAlt: TLargeintField;
    QrProdutosCadasAlterWeb: TSmallintField;
    QrProdutosCadasAtivo: TSmallintField;
    QrEntidadesCpttHvSrDB: TSmallintField;
    Panel6: TPanel;
    Panel7: TPanel;
    Label6: TLabel;
    DBMemo1: TDBMemo;
    Label11: TLabel;
    DBMemo2: TDBMemo;
    Splitter1: TSplitter;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrGraGruYAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrGraGruYBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrGraGruYAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrGraGruYBeforeClose(DataSet: TDataSet);
    procedure QrEntidadesCpttNO_HvSrDBGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure QrEntidadesCpttAfterScroll(DataSet: TDataSet);
    procedure QrEntidadesCpttBeforeClose(DataSet: TDataSet);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraCadastro_Com_Itens_ITS(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    //***procedure ReopenCadastro_Com_Itens_ITS(Controle?: Integer);
    procedure ReopenProdutosCadas(Codigo: Integer);

  end;

var
  FmEntidadesCptt: TFmEntidadesCptt;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module;//, (***MyDBCheck, DmkDAC_PF,***) Cadastro_Com_Itens_ITS;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmEntidadesCptt.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOz.LC(Atual, Codigo);
end;

procedure TFmEntidadesCptt.MostraCadastro_Com_Itens_ITS(SQLType: TSQLType);
begin
{
  if Grl_DmkDB.CriaFm(TFmCadastro_Com_Itens_ITS, FmCadastro_Com_Itens_ITS, afmoNegarComAviso) then
  begin
    FmCadastro_Com_Itens_ITS.ImgTipo.SQLType := SQLType;
    FmCadastro_Com_Itens_ITS.FQrCab := QrEntidadesCptt;
    FmCadastro_Com_Itens_ITS.FDsCab := DsCadastro_Com_Itens_CAB;
    FmCadastro_Com_Itens_ITS.FQrIts := QrProdutosCadas;
    if SQLType = stIns then
      FmCadastro_Com_Itens_ITS.EdCPF1.ReadOnly := False
    else
    begin
      FmCadastro_Com_Itens_ITS.EdControle.ValueVariant := QrProdutosCadasControle.Value;
      //
      FmCadastro_Com_Itens_ITS.EdCPF1.Text := MLAGeral.FormataCNPJ_TFT(QrProdutosCadasCNPJ_CPF.Value);
      FmCadastro_Com_Itens_ITS.EdNomeEmiSac.Text := QrProdutosCadasNome.Value;
      FmCadastro_Com_Itens_ITS.EdCPF1.ReadOnly := True;
    end;
    FmCadastro_Com_Itens_ITS.ShowModal;
    FmCadastro_Com_Itens_ITS.Destroy;
  end;
}
end;

procedure TFmEntidadesCptt.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrEntidadesCptt);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrEntidadesCptt, QrProdutosCadas);
end;

procedure TFmEntidadesCptt.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrEntidadesCptt);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrProdutosCadas);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrProdutosCadas);
end;

procedure TFmEntidadesCptt.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOz.Go(Para, QrEntidadesCpttCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmEntidadesCptt.DefParams;
begin
  VAR_GOTOTABELA := 'entidadescptt';
  VAR_GOTOzSQLTABLE := QrEntidadesCptt;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOzSQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOz.LimpaVAR_SQL();

  VAR_SQLx.Add('SELECT ');
  VAR_SQLx.Add(dmkPF.ArrayToTextoCASEWHEN('sca.HvSrDB', 'NO_HvSrDB',
    TPosVirgula.pvPos, sHaveSrcDB, iHaveSrcDB) + ' sca.*');
  VAR_SQLx.Add('FROM entidadescptt sca');
  VAR_SQLx.Add('WHERE sca.Codigo > 0');
  //
  VAR_SQL1.Add('AND sca.Codigo=:P0');
  //
  VAR_SQL2.Add('AND sca.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND sca.Nome Like :P0');
  //
end;

procedure TFmEntidadesCptt.ItsAltera1Click(Sender: TObject);
begin
  MostraCadastro_Com_Itens_ITS(stUpd);
end;

procedure TFmEntidadesCptt.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmEntidadesCptt.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmEntidadesCptt.QueryPrincipalAfterOpen;
begin
end;

procedure TFmEntidadesCptt.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
{
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'Cadastro_Com_Itens_ITS', 'Controle', QrProdutosCadasControle.Value, Dmod.MyDB?) = ID_YES then
  begin
    Controle := GOTOz.LocalizaPriorNextIntQr(QrProdutosCadas,
      QrProdutosCadasControle, QrProdutosCadasControle.Value);
    ReopenProdutosCadas(Codigo);
  end;
}
end;

procedure TFmEntidadesCptt.ReopenProdutosCadas(Codigo: Integer);
begin
  Grl_DmkDB.AbreSQLQuery0(QrProdutosCadas, Dmod.MyDB, [
  'SELECT * ',
  'FROM produtoscadas ',
  'WHERE EntidadesCptt=' + Geral.FF0(QrEntidadesCpttCodigo.Value),
  'ORDER BY Nome',
  '']);
  //
  QrProdutosCadas.Locate('Codigo', Codigo, []);
end;


procedure TFmEntidadesCptt.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmEntidadesCptt.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmEntidadesCptt.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmEntidadesCptt.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmEntidadesCptt.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmEntidadesCptt.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEntidadesCptt.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrEntidadesCpttCodigo.Value;
  Close;
end;

procedure TFmEntidadesCptt.ItsInclui1Click(Sender: TObject);
begin
  MostraCadastro_Com_Itens_ITS(stIns);
end;

procedure TFmEntidadesCptt.CabAltera1Click(Sender: TObject);
begin
  Grl_DmkDB.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrEntidadesCptt, [PnDados],
  [PnEdita], EdSigla, ImgTipo, 'entidadescptt');
end;

procedure TFmEntidadesCptt.BtConfirmaClick(Sender: TObject);
var
  Nome, Sigla: String;
  Codigo, CodUsu, Ordem, HvSrDB: Integer;
  SQLType: TSQLType;
begin
  SQLType             := ImgTipo.SQLType;
  //
  //Codigo              := EdCodigo.ValueVariant;
  //CodUsu              := EdCodUsu.ValueVariant;
  Grl_DmkDB.ObtemCodigoECodUsu(EdCodigo, EdCodUsu, SQLType, Codigo, CodUsu,
    'entidadescptt', 'Codigo', 'CodUsu');
  Nome                := EdNome.ValueVariant;
  HvSrDB              := EdHvSrDB.ValueVariant;
  Sigla               := EdSigla.ValueVariant;
  Ordem               := EdOrdem.ValueVariant;
  //
  if MyObjects.FIC(Trim(Nome) = '', EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(Trim(Sigla) = '', EdSigla, 'Defina uma sigla!') then (*Exit*);
  //
  //Codigo := Grl_DmkDB.GetNxtCodigoInt('entidadescptt', 'Codigo', SQLType, Codigo);
  if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, 'entidadescptt', False, [
  'Nome', 'Sigla', 'Ordem',
  'CodUsu', 'HvSrDB'], [
  'Codigo'], [
  Nome, Sigla, Ordem,
  CodUsu, HvSrDB], [
  Codigo], True, dmksqlinsInsOnly, '', stDesktop, False) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOz.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmEntidadesCptt.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  //if ImgTipo.SQLType = stIns then
    //UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'entidadescptt', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  GOTOz.UpdUnlockZ(Codigo, Dmod.MyDB, 'entidadescptt', 'Codigo');
end;

procedure TFmEntidadesCptt.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmEntidadesCptt.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmEntidadesCptt.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  //DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmEntidadesCptt.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOz.Codigo(QrEntidadesCpttCodigo.Value, LaRegistro.Caption);
end;

procedure TFmEntidadesCptt.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOz.Nome(LaRegistro.Caption);
end;

procedure TFmEntidadesCptt.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOz.CodUsu(QrEntidadesCpttCodigo.Value, LaRegistro.Caption);
end;

procedure TFmEntidadesCptt.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOz.Fechar(ImgTipo.SQLType);
end;

procedure TFmEntidadesCptt.QrGraGruYAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmEntidadesCptt.QrGraGruYAfterScroll(DataSet: TDataSet);
begin
  ReopenProdutosCadas(0);
end;

procedure TFmEntidadesCptt.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrEntidadesCpttCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmEntidadesCptt.SbQueryClick(Sender: TObject);
begin
  LocCod(QrEntidadesCpttCodigo.Value,
  GOTOz.CriaForm(CO_CODIGO, CO_NOME, 'entidadescptt', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmEntidadesCptt.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEntidadesCptt.CabInclui1Click(Sender: TObject);
begin
  Grl_DmkDB.ConfigPanelInsUpd(stIns, Self, PnEdita, QrEntidadesCptt, [PnDados],
  [PnEdita], EdSigla, ImgTipo, 'entidadescptt');
  EdHvSrDB.ValueVariant := Integer(THaveSrcDB.hsdbCadastr);
end;

procedure TFmEntidadesCptt.QrGraGruYBeforeClose(
  DataSet: TDataSet);
begin
  QrProdutosCadas.Close;
end;

procedure TFmEntidadesCptt.QrGraGruYBeforeOpen(DataSet: TDataSet);
begin
  QrEntidadesCpttCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmEntidadesCptt.QrEntidadesCpttAfterScroll(DataSet: TDataSet);
begin
  ReopenProdutosCadas(0);
end;

procedure TFmEntidadesCptt.QrEntidadesCpttBeforeClose(DataSet: TDataSet);
begin
  QrProdutosCadas.Close;
end;

procedure TFmEntidadesCptt.QrEntidadesCpttNO_HvSrDBGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  Text := TStringField(Sender).Value;
end;

end.

