unit UnSalNewJan;

interface

uses
  Windows, SysUtils, Classes, Menus, UnDmkEnums, mySQLDbTables, Forms,
  UnInternalConsts, UnProjGroup_Vars, dmkGeral;

type
  TUnSalNewJan = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure MostraFormEmbalagensCad(Codigo: Integer);
    procedure MostraFormEmbalagensTar(Codigo: Integer);
    procedure MostraFormEntidadesAccn(Codigo: Integer);
    procedure MostraFormEntidadesCptt(Codigo: Integer);
    procedure MostraFormEntidadesEmpr(Codigo: Integer);
    procedure MostraFormEntidadesGrup();
    procedure MostraFormProdutosCabec(Codigo: Integer);
    procedure MostraFormProdutosCadas(Codigo: Integer);
    procedure MostraFormProdDivsaoCab(Codigo: Integer);
    procedure MostraFormProdDivSubCab(Codigo: Integer);
    procedure MostraFormProdLinhasCab(Codigo: Integer);
    procedure MostraFormProdutosEmbal(Codigo: Integer);
    procedure MostraFormProdutosFamil(Codigo: Integer);
    procedure MostraFormSegmentosCads(Codigo: Integer);
    procedure MostraFormSegmenGruCads(Codigo: Integer);
  end;

var
  SalNewJan: TUnSalNewJan;


implementation

uses UnGrl_DmkDB,
  EmbalagensCad, EmbalagensTar, ProdutosEmbal,
  EntidadesAccn, EntidadesGrup, EntidadesEmpr,
  EntidadesCptt,
  ProdutosCabec, ProdutosCadas, ProdutosFamil,
  ProdDivsaoCab, ProdDivSubCab, ProdLinhasCab,
  SegmentosCads, SegmenGruCads;

{ TUnSalNewJan }

procedure TUnSalNewJan.MostraFormEmbalagensCad(Codigo: Integer);
begin
  if Grl_DmkDB.CriaFm(TFmEmbalagensCad, FmEmbalagensCad, afmoSoMaster) then
  begin
    if Codigo <> 0 then
      FmEmbalagensCad.LocCod(Codigo, Codigo);
    FmEmbalagensCad.ShowModal;
    FmEmbalagensCad.Destroy;
  end;
end;

procedure TUnSalNewJan.MostraFormEmbalagensTar(Codigo: Integer);
begin
  if Grl_DmkDB.CriaFm(TFmEmbalagensTar, FmEmbalagensTar, afmoSoMaster) then
  begin
    if Codigo <> 0 then
      FmEmbalagensTar.LocCod(Codigo, Codigo);
    FmEmbalagensTar.ShowModal;
    FmEmbalagensTar.Destroy;
  end;
end;

procedure TUnSalNewJan.MostraFormEntidadesAccn(Codigo: Integer);
begin
  if Grl_DmkDB.CriaFm(TFmEntidadesAccn, FmEntidadesAccn, afmoSoMaster) then
  begin
    if Codigo <> 0 then
      FmEntidadesAccn.LocCod(Codigo, Codigo);
    FmEntidadesAccn.ShowModal;
    FmEntidadesAccn.Destroy;
  end;
end;

procedure TUnSalNewJan.MostraFormEntidadesCptt(Codigo: Integer);
begin
  if Grl_DmkDB.CriaFm(TFmEntidadesCptt, FmEntidadesCptt, afmoSoMaster) then
  begin
    if Codigo <> 0 then
      FmEntidadesCptt.LocCod(Codigo, Codigo);
    FmEntidadesCptt.ShowModal;
    FmEntidadesCptt.Destroy;
  end;
end;

procedure TUnSalNewJan.MostraFormEntidadesEmpr(Codigo: Integer);
begin
  if Grl_DmkDB.CriaFm(TFmEntidadesEmpr, FmEntidadesEmpr, afmoSoMaster) then
  begin
    if Codigo <> 0 then
      FmEntidadesEmpr.LocCod(Codigo, Codigo);
    FmEntidadesEmpr.ShowModal;
    FmEntidadesEmpr.Destroy;
  end;
end;

procedure TUnSalNewJan.MostraFormEntidadesGrup();
begin
  if Grl_DmkDB.CriaFm(TFmEntidadesGrup, FmEntidadesGrup, afmoSoMaster) then
  begin
    FmEntidadesGrup.ShowModal;
    FmEntidadesGrup.Destroy;
  end;
end;

procedure TUnSalNewJan.MostraFormProdDivsaoCab(Codigo: Integer);
begin
  if Grl_DmkDB.CriaFm(TFmProdDivsaoCab, FmProdDivsaoCab, afmoSoMaster) then
  begin
    if Codigo <> 0 then
      FmProdDivsaoCab.LocCod(Codigo, Codigo);
    FmProdDivsaoCab.ShowModal;
    FmProdDivsaoCab.Destroy;
  end;
end;

procedure TUnSalNewJan.MostraFormProdDivSubCab(Codigo: Integer);
begin
  if Grl_DmkDB.CriaFm(TFmProdDivSubCab, FmProdDivSubCab, afmoSoMaster) then
  begin
    if Codigo <> 0 then
      FmProdDivSubCab.LocCod(Codigo, Codigo);
    FmProdDivSubCab.ShowModal;
    FmProdDivSubCab.Destroy;
  end;
end;

procedure TUnSalNewJan.MostraFormProdLinhasCab(Codigo: Integer);
begin
  if Grl_DmkDB.CriaFm(TFmProdLinhasCab, FmProdLinhasCab, afmoSoMaster) then
  begin
    if Codigo <> 0 then
      FmProdLinhasCab.LocCod(Codigo, Codigo);
    FmProdLinhasCab.ShowModal;
    FmProdLinhasCab.Destroy;
  end;
end;

procedure TUnSalNewJan.MostraFormProdutosCabec(Codigo: Integer);
begin
  if Grl_DmkDB.CriaFm(TFmProdutosCabec, FmProdutosCabec, afmoSoMaster) then
  begin
    if Codigo <> 0 then
      FmProdutosCabec.LocCod(Codigo, Codigo);
    FmProdutosCabec.ShowModal;
    FmProdutosCabec.Destroy;
  end;
end;

procedure TUnSalNewJan.MostraFormProdutosCadas(Codigo: Integer);
begin
  if Grl_DmkDB.CriaFm(TFmProdutosCadas, FmProdutosCadas, afmoSoMaster) then
  begin
    if Codigo <> 0 then
      FmProdutosCadas.LocCod(Codigo, Codigo);
    FmProdutosCadas.ShowModal;
    FmProdutosCadas.Destroy;
  end;
end;

procedure TUnSalNewJan.MostraFormProdutosEmbal(Codigo: Integer);
begin
  if Grl_DmkDB.CriaFm(TFmProdutosEmbal, FmProdutosEmbal, afmoSoMaster) then
  begin
    if Codigo <> 0 then
      FmProdutosEmbal.LocCod(Codigo, Codigo);
    FmProdutosEmbal.ShowModal;
    FmProdutosEmbal.Destroy;
  end;
end;

procedure TUnSalNewJan.MostraFormProdutosFamil(Codigo: Integer);
begin
  if Grl_DmkDB.CriaFm(TFmProdutosFamil, FmProdutosFamil, afmoSoMaster) then
  begin
    if Codigo <> 0 then
      FmProdutosFamil.LocCod(Codigo, Codigo);
    FmProdutosFamil.ShowModal;
    FmProdutosFamil.Destroy;
  end;
end;

procedure TUnSalNewJan.MostraFormSegmenGruCads(Codigo: Integer);
begin
  if Grl_DmkDB.CriaFm(TFmSegmenGruCads, FmSegmenGruCads, afmoSoMaster) then
  begin
    if Codigo <> 0 then
      FmSegmenGruCads.LocCod(Codigo, Codigo);
    FmSegmenGruCads.ShowModal;
    FmSegmenGruCads.Destroy;
  end;
end;

procedure TUnSalNewJan.MostraFormSegmentosCads(Codigo: Integer);
begin
  if Grl_DmkDB.CriaFm(TFmSegmentosCads, FmSegmentosCads, afmoSoAdmin) then
  begin
    if Codigo <> 0 then
      FmSegmentosCads.LocCod(Codigo, Codigo);
    FmSegmentosCads.ShowModal;
    FmSegmentosCads.Destroy;
  end;
end;

end.
