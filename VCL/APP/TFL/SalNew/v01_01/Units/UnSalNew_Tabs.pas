unit UnSalNew_Tabs;

interface

uses System.Classes, System.SysUtils, Generics.Collections, UnDmkEnums,
  UnMyLinguas, MyListas;
type
  TUnSalNew_Tabs = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  CarregaListaTabelas(DatabaseName: String; Lista: TList<TTabelas>):
              Boolean;
    function  CarregaListaSQL(Tabela: String; FListaSQL: TStringList):
              Boolean;
    function  CompletaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
    function  CarregaListaFRIndices(TabelaBase: String; FRIndices:
              TIndices; FLIndices: TList<TIndices>): Boolean;
    function  ComplementaListaSQL(Tabela: String; FListaSQL: TStringList):
              Boolean;
    function  CarregaListaFRCampos(Tabela: String; FRCampos:
              TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
    function  CompletaListaFRCampos(Tabela: String; FRCampos: TCampos;
              FLCampos: TList<TCampos>): Boolean;
    function  CompletaListaFRJanelas(FRJanelas: TJanelas; FLJanelas:
              TList<TJanelas>): Boolean;
    function  CarregaListaFRQeiLnk(TabelaCria: String; FRQeiLnk: TQeiLnk;
              FLQeiLnk: TList<TQeiLnk>): Boolean;
  end;

var
  SalNew_Tabs   : TUnSalNew_Tabs;

implementation

{ TUnSalNew_Tabs }

function TUnSalNew_Tabs.CarregaListaTabelas(DatabaseName: String;
  Lista: TList<TTabelas>): Boolean;
begin
  Result := True;
  try
    if LowerCase(DatabaseName) = LowerCase(CO_DBNome) then
    begin
      MyLinguas.AdTbLst(Lista, False, LowerCase('EmbalagensCad'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('EmbalagensTar'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('EntidadesAccn'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('EntidadesEmpr'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('EntidadesGrup'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('EntidadesCptt'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('EntidadesCpttPsq'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('LoadSalesData'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('ProdDivsaoCab'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('ProdDivSubCab'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('ProdLinhasCab'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('ProdutosCabec'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('ProdutosCadas'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('ProdutosEmbal'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('ProdutosFamil'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('ProdutosTabls'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('SalesOperacao'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('SalesPerfilCl'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('SegmenGruCads'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('SegmentosCads'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('SegmentosSGEA'), '');
    end else
    begin
      ;
    end;
  except
    raise;
    Result := False;
  end;
end;

function TUnSalNew_Tabs.CarregaListaFRIndices(TabelaBase: String;
  FRIndices: TIndices; FLIndices: TList<TIndices>): Boolean;
begin
  Result := True;
  try
    if Uppercase(TabelaBase) = Uppercase('EmbalagensCad') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'CodUsu';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 2;
      FRIndices.Column_name  := 'IDServr';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('EmbalagensTar') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'CodUsu';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 2;
      FRIndices.Column_name  := 'IDServr';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('EntidadesAccn') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'CodUsu';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 2;
      FRIndices.Column_name  := 'IDServr';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('EntidadesCptt') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'CodUsu';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 2;
      FRIndices.Column_name  := 'IDServr';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('EntidadesCpttPsq') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('EntidadesEmpr') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'CodUsu';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 2;
      FRIndices.Column_name  := 'IDServr';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('EntidadesGrup') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'CodUsu';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 2;
      FRIndices.Column_name  := 'IDServr';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('LoadSalesData') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('ProdDivsaoCab') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'CodUsu';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 2;
      FRIndices.Column_name  := 'IDServr';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('ProdDivSubCab') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'CodUsu';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 2;
      FRIndices.Column_name  := 'IDServr';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('ProdLinhasCab') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'CodUsu';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 2;
      FRIndices.Column_name  := 'IDServr';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('ProdutosCabec') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'CodUsu';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 2;
      FRIndices.Column_name  := 'IDServr';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('ProdutosCadas') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Lista';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 2;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Lista';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 2;
      FRIndices.Column_name  := 'CodUsu';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 3;
      FRIndices.Column_name  := 'IDServr';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('ProdutosEmbal') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'CodUsu';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 2;
      FRIndices.Column_name  := 'IDServr';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE2';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'CodProd';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE2';
      FRIndices.Seq_in_index := 2;
      FRIndices.Column_name  := 'IDServr';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('ProdutosFamil') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'CodUsu';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 2;
      FRIndices.Column_name  := 'IDServr';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('ProdutosTabls') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('SalesOperacao') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'CodUsu';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 2;
      FRIndices.Column_name  := 'IDServr';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('SalesPerfilCl') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'CodUsu';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 2;
      FRIndices.Column_name  := 'IDServr';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('SegmenGruCads') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'CodUsu';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 2;
      FRIndices.Column_name  := 'IDServr';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('SegmentosCads') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'CodUsu';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 2;
      FRIndices.Column_name  := 'IDServr';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE2';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Sigla';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('SegmentosSGEA') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'CodUsu';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE1';
      FRIndices.Seq_in_index := 2;
      FRIndices.Column_name  := 'IDServr';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE2';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Segmento';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE2';
      FRIndices.Seq_in_index := 2;
      FRIndices.Column_name  := 'EntidadesEmp';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'UNIQUE2';
      FRIndices.Seq_in_index := 3;
      FRIndices.Column_name  := 'Account';
      FLIndices.Add(FRIndices);
      //
      ;
    end;
  except
    raise;
    Result := False;
  end;
end;

function TUnSalNew_Tabs.CarregaListaFRCampos(Tabela: String; FRCampos: TCampos;
  FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
begin
  Result := True;
  TemControle := TemControle + cTemControleNao;
  try
    //end else
    if Uppercase(Tabela) = Uppercase('EmbalagensCad') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDServr';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(120)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sigla';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HvSrDB';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EmbalagensTar') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDServr';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(120)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
(*
      //
      New(FRCampos);
      FRCampos.Field      := 'Descricao';
      FRCampos.Tipo       := 'varchar(120)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
*)
      //
      New(FRCampos);
      FRCampos.Field      := 'EmbalagensCad';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sigla';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PesoLiq';
      FRCampos.Tipo       := 'real';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PesoTar';
      FRCampos.Tipo       := 'real';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PPallet';
      FRCampos.Tipo       := 'real';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ItmsInPalt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HvSrDB';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EntidadesAccn') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDServr';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sigla';
      FRCampos.Tipo       := 'varchar(6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HvSrDB';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EntidadesCptt') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDServr';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sigla';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'URLProds';
      FRCampos.Tipo       := 'TEXT';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HvSrDB';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EntidadesCpttPsq') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pesquisado';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EntidadesEmpr') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDServr';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GrupoEmpresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GrupEmprSN';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pais';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Regiao';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UF';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cidade';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Perfil';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LegalUnit';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HvSrDB';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EntidadesGrup') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDServr';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HvSrDB';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('LoadSalesData') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'GruLoad';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'xSegmento';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'xGrupoSegmento';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'xAccount';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?????';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'xGrupoEmpresa';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'cCodCli';
      FRCampos.Tipo       := 'integer(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'xCliente';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'xPais';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'xRegiao';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'xUF';
      FRCampos.Tipo       := 'varchar(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'xCidade';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'xPerfil';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'cCodCliFatura';
      FRCampos.Tipo       := 'integer(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'xClienteFatura';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'cCodCliDestino';
      FRCampos.Tipo       := 'integer(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'xClienteDestino';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'xDivisao';
      FRCampos.Tipo       := 'varchar(120)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'xSubDivisao';
      FRCampos.Tipo       := 'varchar(120)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'xLinha';
      FRCampos.Tipo       := 'varchar(120)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'xFamilia';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'xEmbalagem';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'xProduto';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'cCodProd';
      FRCampos.Tipo       := 'Integer(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'dData';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'xOperacao';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'xMoeda';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'xMoedaPed';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'qQuantidade';
      FRCampos.Tipo       := 'real';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'qBrutoBRL';
      FRCampos.Tipo       := 'real';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'qLiquidoBRL';
      FRCampos.Tipo       := 'real';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'qLiquidoUSD';
      FRCampos.Tipo       := 'real';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'qLiquidoEUR';
      FRCampos.Tipo       := 'real';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'qCustoBRL';
      FRCampos.Tipo       := 'real';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'qCustoUSD';
      FRCampos.Tipo       := 'real';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'qCustoEUR';
      FRCampos.Tipo       := 'real';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'qMPBRL';
      FRCampos.Tipo       := 'real';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'qMPUSD';
      FRCampos.Tipo       := 'real';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'qDGFBRL';
      FRCampos.Tipo       := 'real';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'qDGFUSD';
      FRCampos.Tipo       := 'real';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'qDGFEUR';
      FRCampos.Tipo       := 'real';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'cLegalUnit';
      FRCampos.Tipo       := 'Integer(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
(*
      New(FRCampos);
      FRCampos.Field      := 'cPlanta';
      FRCampos.Tipo       := 'Integer(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'xTrimestre';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
*)
    end else
    if Uppercase(Tabela) = Uppercase('ProdDivsaoCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDServr';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sigla';
      FRCampos.Tipo       := 'varchar(6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HvSrDB';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('ProdDivSubCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDServr';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sigla';
      FRCampos.Tipo       := 'varchar(6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HvSrDB';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('ProdLinhasCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDServr';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sigla';
      FRCampos.Tipo       := 'varchar(6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HvSrDB';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('ProdutosCabec') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDServr';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HvSrDB';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('ProdutosCadas') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Lista';  // TLstProdQuim
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDServr';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EntidadesCptt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ProdCabec';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
(*
      New(FRCampos);
      FRCampos.Field      := 'CodProd';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
*)
      New(FRCampos);
      FRCampos.Field      := 'GrupoSegmento';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Segmento';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Divisao';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SubDivisao';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Familia';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HvSrDB';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Diluicao';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Boca';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IQSegment';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IQDivsao';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IQDivSub';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IQFamily';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Natureza';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Propriedades';
      FRCampos.Tipo       := 'Text';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'pH';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CargaIonica';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Aspecto';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Utilizacao';
      FRCampos.Tipo       := 'Text';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Perc_Uso';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ConcOuAtiv';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SolidezLuz';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LocalPDF';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('ProdutosEmbal') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDServr';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodProd';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ProdutosCadas';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EmbalagensTar';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HvSrDB';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('ProdutosFamil') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDServr';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sigla';
      FRCampos.Tipo       := 'varchar(6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HvSrDB';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('ProdutosTabls') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SalesOperacao') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDServr';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EhFatura';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HvSrDB';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SalesPerfilCl') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDServr';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EhFatura';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HvSrDB';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SegmenGruCads') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDServr';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sigla';
      FRCampos.Tipo       := 'varchar(6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HvSrDB';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SegmentosCads') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDServr';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sigla';
      FRCampos.Tipo       := 'varchar(6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HvSrDB';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SegmentosSGEA') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := ' ';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IDServr';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Segmento';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EntidadesEmp';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Account';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AccntSN';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HvSrDB';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FLCampos.Add(FRCampos);
      //
    end;
    ;
    //
    Result := True;
  except
    raise;
    Result := False;
  end;
end;

function TUnSalNew_Tabs.CarregaListaFRQeiLnk(TabelaCria: String;
  FRQeiLnk: TQeiLnk; FLQeiLnk: TList<TQeiLnk>): Boolean;
begin
// n�o implementado!
  Result := False;
end;

function TUnSalNew_Tabs.CarregaListaSQL(Tabela: String;
  FListaSQL: TStringList): Boolean;
begin
{
separador = "|"
}
  Result := True;
  if Uppercase(Tabela) = Uppercase('?') then
  begin
    FListaSQL.Add('Codigo');
    FListaSQL.Add('1');
  end else
  if Uppercase(Tabela) = Uppercase('EntidadesCptt') then
  begin
    FListaSQL.Add('Codigo|CodUsu|IDServr|Sigla|Nome|URLProds');
    FListaSQL.Add('0|0|-1|"???"|"? ? ? ? ?"|""');
    FListaSQL.Add('1|1|-1|"TFL"|"TFL DO BRASIL"|""');
    FListaSQL.Add('2|2|-1|"Anisinos"|"ANISINOS IND�STRIA E COM�RCIO DE PRODUTOS QU�MICOS LTDA"|""');
    FListaSQL.Add('3|3|-1|"ATC"|"ATC DO BRASIL PRODUTOS PARA COURO LTDA"|"http://www.atc.fr/la-gamme-de-produits-atc-dediee-au-traitement-du-cuir-34"');
    FListaSQL.Add('4|4|-1|"Atlas"|"ATLAS REFINERY, INC."|""');
    FListaSQL.Add('5|5|-1|"BASF"|"BASF S/A"|""');
    FListaSQL.Add('6|6|-1|"Buckman"|"BUCKMAN LABORAT�RIOS LTDA"|""');
    FListaSQL.Add('7|7|-1|"CCE"|"CCE COM�RCIO DE ANILINAS LTDA."|""');
    FListaSQL.Add('8|8|-1|"Corium"|"CORIUM QU�MICA LTDA"|""');
    FListaSQL.Add('9|9|-1|"Degani"|"DEGANI - VADUZ IND�STRIA QUIMICA LTDA."|""');
    FListaSQL.Add('10|10|-1|"Dirox"|"DIROX S.A."|""');
    FListaSQL.Add('11|11|-1|"Dissoltex"|"DISSOLTEX INDUSTRIA QUIMICA LTDA."|""');
    FListaSQL.Add('12|12|-1|"Dow"|"DOW BRASIL S/A"|""');
    FListaSQL.Add('13|13|-1|"Dynatech"|"DYNATECH IND�STRIAS QU�MICAS LTDA."|""');
    FListaSQL.Add('14|14|-1|"Dystar - DR. B�hme"|"DYSTAR IND�STRIA E COM�RCIO DE PRODUTOS QU�MICOS LTDA"|""');
    FListaSQL.Add('15|15|-1|"Ecoqu�mica"|"ECOQU�MICA IND�STRIA E COM�RCIO DE PRODUTOS QU�MICOS LTDA"|""');
    FListaSQL.Add('16|16|-1|"Ecovita"|"ECOVITA IND�STRIAS QU�MICAS LTDA"|""');
    FListaSQL.Add('17|17|-1|"Getti"|"GETTI QU�MICA LTDA."|""');
    FListaSQL.Add('18|18|-1|"Incoanil"|"INCOANIL ANILINAS LTDA"|""');
    FListaSQL.Add('19|19|-1|"Innovare"|"INNOVARE QUIMICA DO BRASIL LTDA"|""');
    FListaSQL.Add('20|20|-1|"Ipel"|"IPEL ITIBANYL PRODUTOS ESPECIAIS LTDA."|""');
    FListaSQL.Add('21|21|-1|"Iqu�mia"|"IQU�MIA LTDA"|""');
    FListaSQL.Add('22|22|-1|"Isogama"|"ISOGAMA INDUSTRIA QUIMICA LTDA."|""');
    FListaSQL.Add('23|23|-1|"Italian"|"ITALIAN CHEMICAL IND�STRIA E COM�RCIO DE PRODUTOS QUIMICOS DO BRASIL"|""');
    FListaSQL.Add('24|24|-1|"Kemia Tau"|"KEMIA TAU DO BRASIL IND. QU�MICAS LTDA"|""');
    FListaSQL.Add('25|25|-1|"Killing"|"KILLING S/A TINTAS E ADESIVOS"|""');
    FListaSQL.Add('26|26|-1|"Kromatica"|"KROMATICA IND�STRIA E COM�RCIO DE PRODUTOS QU�MICOS LTDA."|""');
    FListaSQL.Add('27|27|-1|"Lanxess"|"LANXESS IND�STRIA DE PRODUTOS QU�MICOS E PL�STICOS LTDA"|""');
    FListaSQL.Add('28|28|-1|"Leather Qu�mica"|"LEATHER QU�MICA DO BRASIL LTDA"|""');
    FListaSQL.Add('29|29|-1|"LF"|"LF QU�MICA"|"http://lfquimica.com.br"');
    FListaSQL.Add('30|30|-1|"LS"|"LS LEATHER SOLUTIONS INDUSTRIA, COM�RCIO E REPRESENTA��ES LTDA"|""');
    FListaSQL.Add('31|31|-1|"LT"|"LT QU�MICA LTDA."|""');
    FListaSQL.Add('32|32|-1|"Luxcis"|"LUXCIS QU�MICA INDUSTRIAL LTDA"|""');
    FListaSQL.Add('33|33|-1|"Manuchar"|"MANUCHAR COM�RCIO EXTERIOR LTDA"|""');
    FListaSQL.Add('34|34|-1|"MK"|"MK QU�MICA DO BRASIL LTDA"|""');
    FListaSQL.Add('35|35|-1|"Mogiana"|"MOGIANA IND�STRIA PRODUTOS QU�MICOS LTDA"|""');
    FListaSQL.Add('36|36|-1|"Noko"|"NOKO QU�MICA LTDA"|""');
    FListaSQL.Add('37|37|-1|"Nokopiel"|"NOKOPIEL QU�MICA LTDA"|""');
    FListaSQL.Add('38|38|-1|"Novozimes"|"NOVOZYMES LATIN AM�RICA LTDA"|""');
    FListaSQL.Add('39|39|-1|"Organic"|"ORGANIC QU�MICA LTDA (RIMALAC)"|""');
    FListaSQL.Add('40|40|-1|"Oxiteno"|"OXITENO S/A IND�STRIA E COM�RCIO"|""');
    FListaSQL.Add('41|41|-1|"Peri"|"PERI QU�MICA DO BRASIL LTDA"|""');
    FListaSQL.Add('42|42|-1|"Polink"|"POLINK IND�STRIA QUIMICA COM�RCIO LTDA"|""');
    FListaSQL.Add('43|43|-1|"Polivinyl"|"POLIVINYL IND�STRIA DE PRODUTOS QU�MICOS LTDA"|""');
    FListaSQL.Add('44|44|-1|"Portinari"|"PORTINARI COM�RCIO IMPORTA��O E EXPORTA��O DE COUROS E AFINS LTDA"|""');
    FListaSQL.Add('45|45|-1|"Prime"|"PRIME CHEMICAL IND�STRIA E COM�RCIO DE PRODUTOS QUIMICOS LTDA"|""');
    FListaSQL.Add('46|46|-1|"Produqu�mica"|"PRODUQU�MICA IND�STRIA E COM�RCIO S.A."|""');
    FListaSQL.Add('47|47|-1|"Pulcra"|"PULCRA ESPECIALIDADES QU�MICAS LTDA"|""');
    FListaSQL.Add('48|48|-1|"Qu�mica Carioca"|"QUIMICA CARIOCA LTDA"|""');
    FListaSQL.Add('49|49|-1|"Quimifinish"|"QUIMIFINISH IND�STRIA E COM�RCIO PRODUTOS QU�MICOS LTDA"|""');
    FListaSQL.Add('50|50|-1|"RBT"|"RBT CHEMICAL SPECIALTIES COM. DE PRODUTOS QUIMICOS LTDA"|""');
    FListaSQL.Add('51|51|-1|"Rhodia"|"RHODIA POLIAMIDA E ESPECIALIDADES - SOLVAY GROUP"|""');
    FListaSQL.Add('52|52|-1|"Schill + Seilacher"|"SCHILL + SEILACHER GMBH"|""');
    FListaSQL.Add('53|53|-1|"Seta"|"SETA S.A EXTRATIVA TANINO DE AC�CIA"|""');
    FListaSQL.Add('54|54|-1|"Silvateam"|"SILVATEAM BRASIL IND�STRIA, COM�RCIO DE PRODUTOS QU�MICOS LTDA"|""');
    FListaSQL.Add('55|55|-1|"Stahl"|"STAHL BRASIL S.A."|""');
    FListaSQL.Add('56|56|-1|"Tanac"|"TANAC S.A"|""');
    FListaSQL.Add('57|57|-1|"Tancrom"|"TANCROM IND�STRIA E COM�RCIO DE PRODUTOS QU�MICOS LTDA"|""');
    FListaSQL.Add('58|58|-1|"Tanqu�mica"|"TANQU�MICA INDUSTRIA E COM�RCIO"|""');
    FListaSQL.Add('59|59|-1|"Tecnipiel"|"TECNPIEL IMPORTA��O, EXPORTA��O, IND�STRIA E COM�RCIO LTDA"|""');
    FListaSQL.Add('60|60|-1|"Thor"|"THOR BRASIL LTDA"|""');
    FListaSQL.Add('61|61|-1|"Trumpler"|"TRUMPLER BRASILEIRA LTDA"|""');
    FListaSQL.Add('62|62|-1|"Unipelli"|"UNIPELLI IND�STRIA QU�MICA LTDA"|""');
    FListaSQL.Add('63|63|-1|"Units"|"UNITS BRASIL COM. IND. REPRES. LTDA"|""');
    FListaSQL.Add('64|64|-1|"Zschimmer & Schwarz"|"ZSCHIMMER & SCHWARZ DO BRASIL LTDA"|""');
    //
    FListaSQL.Add('65|65|-1|"Qu�mica Central"|"Qu�mica Central"|""');
    FListaSQL.Add('66|66|-1|"Quimicamil"|"Quimicamil"|""');
    FListaSQL.Add('67|67|-1|"Qu�mica Master"|"Qu�mica Master"|""');
    //
    FListaSQL.Add('68|68|-1|"Biokimica"|"Biokimica"|""');
    FListaSQL.Add('69|69|-1|"Smit & Zoon"|"SMIT & ZOON"|""');
    FListaSQL.Add('70|70|-1|"Dermochimica"|"DERMOCHIMICA S.p.A."|""');
    FListaSQL.Add('71|71|-1|"Tecnochimica"|"Tecnochimica SpA"|""');
    FListaSQL.Add('72|72|-1|"Corichem"|"Corichem S.r.l."|""');
    FListaSQL.Add('73|73|-1|"Codyeco"|"Codyeco S.p.A."|""');
    FListaSQL.Add('74|74|-1|"Dermacolor"|"DERMACOLOR S.R.L."|""');
    FListaSQL.Add('75|75|-1|"Repico"|"Repico Spa"|""');
    //
    FListaSQL.Add('76|76|-1|"Anabe"|"ANABE IND�STRIA E COM�RCIO DE PROTE�NAS LTDA."|""');
    FListaSQL.Add('77|77|-1|"Lonza - ARCH Quimica"|"Lonza - ARCH Quimica Brasil Ltda"|""');
    FListaSQL.Add('78|78|-1|"Additiva"|"Additiva Produtos Qu�micos"|""');
    FListaSQL.Add('79|79|-1|"Colorantes Industriales"|"Colorantes Industriales"|""');
    FListaSQL.Add('80|80|-1|"Seici"|"Seici"|""');
(*

    FListaSQL.Add('81|81|-1|""|""|""');
    FListaSQL.Add('82|82|-1|""|""|""');
    FListaSQL.Add('83|83|-1|""|""|""');
    FListaSQL.Add('84|84|-1|""|""|""');
    FListaSQL.Add('85|85|-1|""|""|""');
    FListaSQL.Add('86|86|-1|""|""|""');
    FListaSQL.Add('87|87|-1|""|""|""');
    FListaSQL.Add('88|88|-1|""|""|""');
    FListaSQL.Add('89|89|-1|""|""|""');

    FListaSQL.Add('9|9|-1|""|""|""');
    *)
  end else
  if Uppercase(Tabela) = Uppercase('ProdutosTabls') then
  begin
    FListaSQL.Add('Codigo|Nome');
    FListaSQL.Add('0|"Indefinido"');    // lpqIndef=0
    FListaSQL.Add('1|"Referencial"');   // lpqRefere=1
    FListaSQL.Add('2|"Similar"');       // lpqSimilar=2
    FListaSQL.Add('3|"Commodities"');   // lpqComodit=3
    FListaSQL.Add('4|"Banhos"');        // lpqBanhos=4
    FListaSQL.Add('5|"Textos"');        // lpqTextos=5
    FListaSQL.Add('6|"Em Branco"');     // lpqBranco=6
    FListaSQL.Add('7|"Fam�lia PQ"');    // lpqFamilia=7;
  end;
end;

function TUnSalNew_Tabs.ComplementaListaSQL(Tabela: String;
  FListaSQL: TStringList): Boolean;
begin
  Result := True;
  if Uppercase(Tabela) = Uppercase('') then
  begin
  end else if Uppercase(Tabela) = Uppercase('PerfisIts') then
  begin
    //FListaSQL.Add('"Entidades"|"Cadastro de pessoas f�sicas e jur�dicas (clientes| fornecedores| etc.)"');
    //FListaSQL.Add('""|""');
  end;
end;

function TUnSalNew_Tabs.CompletaListaFRCampos(Tabela: String; FRCampos: TCampos;
  FLCampos: TList<TCampos>): Boolean;
begin
  Result := True;
  try
{
    if Uppercase(Tabela) = Uppercase('Controle') then
    begin
      New(FRCampos);
      FRCampos.Field      := 'CMPPVai';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
    end;
}
  except
    raise;
    Result := False;
  end;
end;

function TUnSalNew_Tabs.CompletaListaFRJanelas(FRJanelas: TJanelas;
  FLJanelas: TList<TJanelas>): Boolean;
begin
  //
  // ENT-CADAS-001 :: Grupos de Empresas
  New(FRJanelas);
  FRJanelas.ID        := 'ENT-CADAS-001';
  FRJanelas.Nome      := 'FmEntidadesGrup';
  FRJanelas.Descricao := 'Grupos de Empresas';
  FLJanelas.Add(FRJanelas);
  //
  // ENT-CADAS-002 :: Cadastro de Clientes
  New(FRJanelas);
  FRJanelas.ID        := 'ENT-CADAS-002';
  FRJanelas.Nome      := 'FmEntidadesEmpr';
  FRJanelas.Descricao := 'Cadastro de Clientes';
  FLJanelas.Add(FRJanelas);
  //
  // ENT-CADAS-003 :: Cadastro de Cliente x Segmento x Account
  New(FRJanelas);
  FRJanelas.ID        := 'ENT-CADAS-003';
  FRJanelas.Nome      := 'FmSegmentosSGEA';
  FRJanelas.Descricao := 'Cadastro de Cliente x Segmento x Account';
  FLJanelas.Add(FRJanelas);
  //
  // ENT-CADAS-004 :: Cadastro de Accounts
  New(FRJanelas);
  FRJanelas.ID        := 'ENT-CADAS-004';
  FRJanelas.Nome      := 'FmEntidadesAccn';
  FRJanelas.Descricao := 'Cadastro de Accounts';
  FLJanelas.Add(FRJanelas);
  //
  // ENT-CADAS-005 :: Cadastro de Concorrentes
  New(FRJanelas);
  FRJanelas.ID        := 'ENT-CADAS-005';
  FRJanelas.Nome      := 'FmEntidadesCptt';
  FRJanelas.Descricao := 'Cadastro de Concorrentes';
  FLJanelas.Add(FRJanelas);
  //
  // LOD-SALES-001 :: Importa��o de Movimento de Produtos
  New(FRJanelas);
  FRJanelas.ID        := 'LOD-SALES-001';
  FRJanelas.Nome      := 'FmLoadSales';
  FRJanelas.Descricao := 'Importa��o de Movimento de Produtos';
  FLJanelas.Add(FRJanelas);
  //
  // LOD-SALES-002 :: Equival�ncia Intercambial de Produtos
  New(FRJanelas);
  FRJanelas.ID        := 'LOD-SALES-002';
  FRJanelas.Nome      := 'FmLoadSalesGen';
  FRJanelas.Descricao := 'Equival�ncia Intercambial de Produtos';
  FLJanelas.Add(FRJanelas);
  //
  // LOD-SALES-003 :: Atrelamento de Produtos Equivalentes
  New(FRJanelas);
  FRJanelas.ID        := 'LOD-SALES-003';
  FRJanelas.Nome      := 'FmLoadSalesGenAtrel';
  FRJanelas.Descricao := 'Atrelamento de Produtos Equivalentes';
  FLJanelas.Add(FRJanelas);
  //
  // PRD-CADAS-001 :: Cadastro de Produtos
  New(FRJanelas);
  FRJanelas.ID        := 'PRD-CADAS-001';
  FRJanelas.Nome      := 'FmProdutosCadas';
  FRJanelas.Descricao := 'Grupos de Empresas';
  FLJanelas.Add(FRJanelas);
  //
  // PRD-CADAS-002 :: Cadastro de Refer�ncia de Produtos
  New(FRJanelas);
  FRJanelas.ID        := 'PRD-CADAS-002';
  FRJanelas.Nome      := 'FmProdutosCabec';
  FRJanelas.Descricao := 'Cadastro de Refer�ncia de Produtos';
  FLJanelas.Add(FRJanelas);
  //
  // PRD-CADAS-003 :: Divis�es de Produtos
  New(FRJanelas);
  FRJanelas.ID        := 'PRD-CADAS-003';
  FRJanelas.Nome      := 'FmProdDivsaoCab';
  FRJanelas.Descricao := 'Divis�es de Produtos';
  FLJanelas.Add(FRJanelas);
  //
  // PRD-CADAS-004 :: Sub-divis�es de Produtos
  New(FRJanelas);
  FRJanelas.ID        := 'PRD-CADAS-004';
  FRJanelas.Nome      := 'FmProdDivSubCab';
  FRJanelas.Descricao := 'Sub-divis�es de Produtos';
  FLJanelas.Add(FRJanelas);
  //
  // PRD-CADAS-005 :: Linhas de Produtos
  New(FRJanelas);
  FRJanelas.ID        := 'PRD-CADAS-005';
  FRJanelas.Nome      := 'FmProdLinhasCab';
  FRJanelas.Descricao := 'Linhas de Produtos';
  FLJanelas.Add(FRJanelas);
  //
  // PRD-CADAS-006 :: Fam�lias de Produtos
  New(FRJanelas);
  FRJanelas.ID        := 'PRD-CADAS-006';
  FRJanelas.Nome      := 'FmProdutosFamil';
  FRJanelas.Descricao := 'Fam�lias de Produtos';
  FLJanelas.Add(FRJanelas);
  //
  // PRD-CADAS-007 :: Cadastro de Embalagens
  New(FRJanelas);
  FRJanelas.ID        := 'PRD-CADAS-007';
  FRJanelas.Nome      := 'FmEmbalagensCad';
  FRJanelas.Descricao := 'Cadastro de Embalagens';
  FLJanelas.Add(FRJanelas);
  //
  // PRD-CADAS-008 :: Cadastro de Taras de Embalagem
  New(FRJanelas);
  FRJanelas.ID        := 'PRD-CADAS-008';
  FRJanelas.Nome      := 'FmEmbalagensTar';
  FRJanelas.Descricao := 'Cadastro de Taras de Embalagem';
  FLJanelas.Add(FRJanelas);
  //
  // PRD-CADAS-009 :: Cadastro de Produto x Embalagem
  New(FRJanelas);
  FRJanelas.ID        := 'PRD-CADAS-009';
  FRJanelas.Nome      := 'FmProdutosEmbal';
  FRJanelas.Descricao := 'Cadastro de Produto x Embalagem';
  FLJanelas.Add(FRJanelas);
  //
  // SEG-CADAS-001 :: Cadastro de Grupos de Segmentos
  New(FRJanelas);
  FRJanelas.ID        := 'SEG-CADAS-001';
  FRJanelas.Nome      := 'FmSegemenGruCadas';
  FRJanelas.Descricao := 'Cadastro de Grupos de Segmentos';
  FLJanelas.Add(FRJanelas);
  //
  // SEG-CADAS-002 :: Cadastro de Segmentos
  New(FRJanelas);
  FRJanelas.ID        := 'SEG-CADAS-001';
  FRJanelas.Nome      := 'FmSegementosCadas';
  FRJanelas.Descricao := 'Cadastro de Segmentos';
  FLJanelas.Add(FRJanelas);
  //
  Result := True;
end;

function TUnSalNew_Tabs.CompletaListaSQL(Tabela: String;
  FListaSQL: TStringList): Boolean;
begin
  Result := True;
  try
    ;
  except
    raise;
    Result := False;
  end;
end;

end.
