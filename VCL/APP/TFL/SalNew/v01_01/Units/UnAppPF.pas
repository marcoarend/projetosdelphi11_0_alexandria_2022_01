unit UnAppPF;

interface

uses
  Windows, SysUtils, Classes, Menus, UnDmkEnums, mySQLDbTables, Forms,
  UnInternalConsts;

type
  TUnAppPF = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure LoadSales();
  end;

var
  AppPF: TUnAppPF;


implementation

uses UnGrl_DmkDB, LoadSales;

{ TUnAppPF }

procedure TUnAppPF.LoadSales;
begin
    Application.CreateForm(TFmLoadSales, FmLoadSales);
    FmLoadSales.ShowModal;
    FmLoadSales.Destroy;
end;

end.
