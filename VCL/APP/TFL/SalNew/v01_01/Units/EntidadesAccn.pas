unit EntidadesAccn;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, (***UnGOTOy,***) UnInternalConsts, UnMsgInt, (***UMySQLModule,***) Menus,
  UnInternalConsts2, mySQLDbTables, (***UnMySQLCuringa,***) dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, ZAbstractRODataset, ZDataset, ZAbstractDataset,
  //
  UnGOTOz, UnGrl_DmkDB, dmkEditF7;

type
  TFmEntidadesAccn = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DGDados: TDBGrid;
    QrEntidadesAccn: TZQuery;
    DsEntidadesAccn: TDataSource;
    QrSegmentosAccn: TZQuery;
    DsSegmentosAccn: TDataSource;
    QrSegmentosAccnCodigo: TLargeintField;
    QrSegmentosAccnNome: TWideStringField;
    QrSegmentosAccnSigla: TWideStringField;
    QrSegmentosAccnLk: TLargeintField;
    QrSegmentosAccnDataCad: TDateField;
    QrSegmentosAccnDataAlt: TDateField;
    QrSegmentosAccnUserCad: TLargeintField;
    QrSegmentosAccnUserAlt: TLargeintField;
    QrSegmentosAccnAlterWeb: TSmallintField;
    QrSegmentosAccnAtivo: TSmallintField;
    EdSigla: TdmkEdit;
    Label3: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    Label4: TLabel;
    Label9: TLabel;
    Label6: TLabel;
    DBEdit1: TDBEdit;
    Label1: TLabel;
    DBEdCodUsu: TdmkDBEdit;
    Label8: TLabel;
    DBEdit2: TDBEdit;
    Label10: TLabel;
    DBEdit3: TDBEdit;
    EdCodUsu: TdmkEdit;
    Label24: TLabel;
    EdHvSrDB: TdmkEditF7;
    Label22: TLabel;
    EdOrdem: TdmkEdit;
    Label5: TLabel;
    QrEntidadesAccnCodigo: TLargeintField;
    QrEntidadesAccnCodUsu: TLargeintField;
    QrEntidadesAccnNome: TWideStringField;
    QrEntidadesAccnSigla: TWideStringField;
    QrEntidadesAccnOrdem: TLargeintField;
    QrEntidadesAccnHvSrDB: TSmallintField;
    QrEntidadesAccnLk: TLargeintField;
    QrEntidadesAccnDataCad: TDateField;
    QrEntidadesAccnDataAlt: TDateField;
    QrEntidadesAccnUserCad: TLargeintField;
    QrEntidadesAccnUserAlt: TLargeintField;
    QrEntidadesAccnAlterWeb: TSmallintField;
    QrEntidadesAccnAtivo: TSmallintField;
    QrEntidadesAccnNO_HvSrDB: TWideMemoField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrGraGruYAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrGraGruYBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrGraGruYAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrGraGruYBeforeClose(DataSet: TDataSet);
    procedure QrEntidadesAccnNO_HvSrDBGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraCadastro_Com_Itens_ITS(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    //***procedure ReopenCadastro_Com_Itens_ITS(Controle?: Integer);
    procedure ReopenCadastro_Com_Itens_ITS(Controle: Integer);

  end;

var
  FmEntidadesAccn: TFmEntidadesAccn;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module;//, (***MyDBCheck, DmkDAC_PF,***) Cadastro_Com_Itens_ITS;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmEntidadesAccn.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOz.LC(Atual, Codigo);
end;

procedure TFmEntidadesAccn.MostraCadastro_Com_Itens_ITS(SQLType: TSQLType);
begin
{
  if Grl_DmkDB.CriaFm(TFmCadastro_Com_Itens_ITS, FmCadastro_Com_Itens_ITS, afmoNegarComAviso) then
  begin
    FmCadastro_Com_Itens_ITS.ImgTipo.SQLType := SQLType;
    FmCadastro_Com_Itens_ITS.FQrCab := QrEntidadesAccn;
    FmCadastro_Com_Itens_ITS.FDsCab := DsCadastro_Com_Itens_CAB;
    FmCadastro_Com_Itens_ITS.FQrIts := QrSegmentosAccn;
    if SQLType = stIns then
      FmCadastro_Com_Itens_ITS.EdCPF1.ReadOnly := False
    else
    begin
      FmCadastro_Com_Itens_ITS.EdControle.ValueVariant := QrSegmentosAccnControle.Value;
      //
      FmCadastro_Com_Itens_ITS.EdCPF1.Text := MLAGeral.FormataCNPJ_TFT(QrSegmentosAccnCNPJ_CPF.Value);
      FmCadastro_Com_Itens_ITS.EdNomeEmiSac.Text := QrSegmentosAccnNome.Value;
      FmCadastro_Com_Itens_ITS.EdCPF1.ReadOnly := True;
    end;
    FmCadastro_Com_Itens_ITS.ShowModal;
    FmCadastro_Com_Itens_ITS.Destroy;
  end;
}
end;

procedure TFmEntidadesAccn.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrEntidadesAccn);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrEntidadesAccn, QrSegmentosAccn);
end;

procedure TFmEntidadesAccn.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrEntidadesAccn);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrSegmentosAccn);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrSegmentosAccn);
end;

procedure TFmEntidadesAccn.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOz.Go(Para, QrEntidadesAccnCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmEntidadesAccn.DefParams;
begin
  VAR_GOTOTABELA := 'entidadesaccn';
  VAR_GOTOzSQLTABLE := QrEntidadesAccn;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOzSQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOz.LimpaVAR_SQL();

  VAR_SQLx.Add('SELECT ');
  VAR_SQLx.Add(dmkPF.ArrayToTextoCASEWHEN('sca.HvSrDB', 'NO_HvSrDB',
    TPosVirgula.pvPos, sHaveSrcDB, iHaveSrcDB) + ' sca.*');
  VAR_SQLx.Add('FROM entidadesaccn sca');
  VAR_SQLx.Add('WHERE sca.Codigo > 0');
  //
  VAR_SQL1.Add('AND sca.Codigo=:P0');
  //
  VAR_SQL2.Add('AND sca.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND sca.Nome Like :P0');
  //
end;

procedure TFmEntidadesAccn.ItsAltera1Click(Sender: TObject);
begin
  MostraCadastro_Com_Itens_ITS(stUpd);
end;

procedure TFmEntidadesAccn.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmEntidadesAccn.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmEntidadesAccn.QueryPrincipalAfterOpen;
begin
end;

procedure TFmEntidadesAccn.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
{
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'Cadastro_Com_Itens_ITS', 'Controle', QrSegmentosAccnControle.Value, Dmod.MyDB?) = ID_YES then
  begin
    Controle := GOTOz.LocalizaPriorNextIntQr(QrSegmentosAccn,
      QrSegmentosAccnControle, QrSegmentosAccnControle.Value);
    ReopenCadastro_Com_Itens_ITS(Controle);
  end;
}
end;

procedure TFmEntidadesAccn.ReopenCadastro_Com_Itens_ITS(Controle: Integer);
begin
{
  UnDmkDAC_PF.AbreMySQLQuery0(QrSegmentosAccn, Dmod.MyDB, [
  'SELECT * ',
  'FROM cadastro_com_itens_its ',
  'WHERE Codigo=' + Geral.FF0(QrEntidadesAccnCodigo?.Value),
  '']);
  //
  QrSegmentosAccn.Locate('Controle?, Controle, []);
}
end;


procedure TFmEntidadesAccn.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmEntidadesAccn.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmEntidadesAccn.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmEntidadesAccn.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmEntidadesAccn.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmEntidadesAccn.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEntidadesAccn.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrEntidadesAccnCodigo.Value;
  Close;
end;

procedure TFmEntidadesAccn.ItsInclui1Click(Sender: TObject);
begin
  MostraCadastro_Com_Itens_ITS(stIns);
end;

procedure TFmEntidadesAccn.CabAltera1Click(Sender: TObject);
begin
  Grl_DmkDB.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrEntidadesAccn, [PnDados],
  [PnEdita], EdSigla, ImgTipo, 'entidadesaccn');
end;

procedure TFmEntidadesAccn.BtConfirmaClick(Sender: TObject);
var
  Nome, Sigla: String;
  Codigo, CodUsu, Ordem, HvSrDB: Integer;
  SQLType: TSQLType;
begin
  SQLType             := ImgTipo.SQLType;
  //
  //Codigo              := EdCodigo.ValueVariant;
  //CodUsu              := EdCodUsu.ValueVariant;
  Grl_DmkDB.ObtemCodigoECodUsu(EdCodigo, EdCodUsu, SQLType, Codigo, CodUsu,
    'entidadesaccn', 'Codigo', 'CodUsu');
  Nome                := EdNome.ValueVariant;
  HvSrDB              := EdHvSrDB.ValueVariant;
  Sigla               := EdSigla.ValueVariant;
  Ordem               := EdOrdem.ValueVariant;
  //
  if MyObjects.FIC(Trim(Nome) = '', EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(Trim(Sigla) = '', EdSigla, 'Defina uma sigla!') then (*Exit*);
  //
  //Codigo := Grl_DmkDB.GetNxtCodigoInt('entidadesaccn', 'Codigo', SQLType, Codigo);
  if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, 'entidadesaccn', False, [
  'Nome', 'Sigla', 'Ordem',
  'CodUsu', 'HvSrDB'], [
  'Codigo'], [
  Nome, Sigla, Ordem,
  CodUsu, HvSrDB], [
  Codigo], True, dmksqlinsInsOnly, '', stDesktop, False) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOz.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmEntidadesAccn.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  //if ImgTipo.SQLType = stIns then
    //UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'entidadesaccn', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  GOTOz.UpdUnlockZ(Codigo, Dmod.MyDB, 'entidadesaccn', 'Codigo');
end;

procedure TFmEntidadesAccn.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmEntidadesAccn.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmEntidadesAccn.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmEntidadesAccn.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOz.Codigo(QrEntidadesAccnCodigo.Value, LaRegistro.Caption);
end;

procedure TFmEntidadesAccn.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOz.Nome(LaRegistro.Caption);
end;

procedure TFmEntidadesAccn.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOz.CodUsu(QrEntidadesAccnCodigo.Value, LaRegistro.Caption);
end;

procedure TFmEntidadesAccn.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOz.Fechar(ImgTipo.SQLType);
end;

procedure TFmEntidadesAccn.QrGraGruYAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmEntidadesAccn.QrGraGruYAfterScroll(DataSet: TDataSet);
begin
  ReopenCadastro_Com_Itens_ITS(0);
end;

procedure TFmEntidadesAccn.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrEntidadesAccnCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmEntidadesAccn.SbQueryClick(Sender: TObject);
begin
  LocCod(QrEntidadesAccnCodigo.Value,
  GOTOz.CriaForm(CO_CODIGO, CO_NOME, 'entidadesaccn', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmEntidadesAccn.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEntidadesAccn.CabInclui1Click(Sender: TObject);
begin
  Grl_DmkDB.ConfigPanelInsUpd(stIns, Self, PnEdita, QrEntidadesAccn, [PnDados],
  [PnEdita], EdSigla, ImgTipo, 'entidadesaccn');
  EdHvSrDB.ValueVariant := Integer(THaveSrcDB.hsdbCadastr);
end;

procedure TFmEntidadesAccn.QrGraGruYBeforeClose(
  DataSet: TDataSet);
begin
  QrSegmentosAccn.Close;
end;

procedure TFmEntidadesAccn.QrGraGruYBeforeOpen(DataSet: TDataSet);
begin
  QrEntidadesAccnCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmEntidadesAccn.QrEntidadesAccnNO_HvSrDBGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  Text := TStringField(Sender).Value;
end;

end.

