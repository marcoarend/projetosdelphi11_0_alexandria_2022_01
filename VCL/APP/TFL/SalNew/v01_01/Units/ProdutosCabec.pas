unit ProdutosCabec;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, (***UnGOTOy,***) UnInternalConsts, UnMsgInt, (***UMySQLModule,***)
  Menus, UnInternalConsts2, mySQLDbTables, (***UnMySQLCuringa,***)
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  Grids, DBGrids, Variants,
  UnDmkProcFunc, UnDmkENums, ZAbstractRODataset, ZDataset, ZAbstractDataset,
  //
  UnGOTOz, UnGrl_DmkDB, dmkDBLookupComboBox, dmkEditCB, dmkEditF7, dmkDBGridZTO;

type
  TFmProdutosCabec = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    QrProdutosCabec: TZQuery;
    DsProdutosCabec: TDataSource;
    QrProdutosCadas: TZQuery;
    DsProdutosCadas: TDataSource;
    DBEdNome: TdmkDBEdit;
    Label4: TLabel;
    DBEdCodUsu: TdmkDBEdit;
    Label2: TLabel;
    QrProdutosCabecCodigo: TLargeintField;
    QrProdutosCabecCodUsu: TLargeintField;
    QrProdutosCabecNome: TWideStringField;
    QrProdutosCabecHvSrDB: TSmallintField;
    QrProdutosCabecLk: TLargeintField;
    QrProdutosCabecDataCad: TDateField;
    QrProdutosCabecDataAlt: TDateField;
    QrProdutosCabecUserCad: TLargeintField;
    QrProdutosCabecUserAlt: TLargeintField;
    QrProdutosCabecAlterWeb: TSmallintField;
    QrProdutosCabecAtivo: TSmallintField;
    Label14: TLabel;
    DBEdit10: TDBEdit;
    Label22: TLabel;
    Label24: TLabel;
    EdCodUsu: TdmkEdit;
    EdHvSrDB: TdmkEditF7;
    DBEdit11: TDBEdit;
    QrProdutosCabecNO_HvSrDB: TWideMemoField;
    DGDados: TdmkDBGridZTO;
    QrProdutosCadasNO_HvSrDB: TWideMemoField;
    QrProdutosCadasCodigo: TLargeintField;
    QrProdutosCadasCodUsu: TLargeintField;
    QrProdutosCadasProdCabec: TLargeintField;
    QrProdutosCadasGrupoSegmento: TLargeintField;
    QrProdutosCadasSegmento: TLargeintField;
    QrProdutosCadasDivisao: TLargeintField;
    QrProdutosCadasSubDivisao: TLargeintField;
    QrProdutosCadasFamilia: TLargeintField;
    QrProdutosCadasNome: TWideStringField;
    QrProdutosCadasHvSrDB: TSmallintField;
    QrProdutosCadasLk: TLargeintField;
    QrProdutosCadasDataCad: TDateField;
    QrProdutosCadasDataAlt: TDateField;
    QrProdutosCadasUserCad: TLargeintField;
    QrProdutosCadasUserAlt: TLargeintField;
    QrProdutosCadasAlterWeb: TSmallintField;
    QrProdutosCadasAtivo: TSmallintField;
    QrProdutosCadasNO_GrupoSegmento: TWideStringField;
    QrProdutosCadasNO_Segmento: TWideStringField;
    QrProdutosCadasNO_Divisao: TWideStringField;
    QrProdutosCadasNO_SubDivisao: TWideStringField;
    QrProdutosCadasNO_Familia: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrProdutosCabecBeforeClose(DataSet: TDataSet);
    procedure QrProdutosCabecAfterScroll(DataSet: TDataSet);
    procedure EdCidadeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QrProdutosCabecNO_HvSrDBGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure QrProdutosCadasNO_HvSrDBGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraEntidadesEmpSGEA(SQLType: TSQLType);
    //
    procedure OpcoesCopia(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure ReopenProdutosCadas(Codigo: Integer);
  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);

  end;

var
  FmProdutosCabec: TFmProdutosCabec;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, SegmentosSGEA;//, (***MyDBCheck, DmkDAC_PF,***) EntidadesEmpSGEA;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmProdutosCabec.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOz.LC(Atual, Codigo);
end;

procedure TFmProdutosCabec.MostraEntidadesEmpSGEA(SQLType: TSQLType);
begin
{
  if Grl_DmkDB.CriaFm(TFmSegmentosSGEA, FmSegmentosSGEA, afmoNegarComAviso) then
  begin
    FmSegmentosSGEA.ImgTipo.SQLType := SQLType;
    //FmSegmentosSGEA.FQrCab := QrProdutosCabec;
    //FmSegmentosSGEA.FDsCab := DsCadastro_Com_Itens_CAB;
    FmSegmentosSGEA.FQrIts := QrProdutosCadas;
    if SQLType = stIns then
      FmSegmentosSGEA.EdHvSrDB.ValueVariant       := Integer(THaveSrcDB.hsdbCadastr)
     // FmSegmentosSGEA.EdCPF1.ReadOnly := False
    else
    begin
      FmSegmentosSGEA.EdCodigo.ValueVariant       := QrProdutosCadasCodigo.Value;
      FmSegmentosSGEA.EdCodUsu.ValueVariant       := QrProdutosCadasCodUsu.Value;
      FmSegmentosSGEA.EdNome.ValueVariant         := QrProdutosCadasNome.Value;
      FmSegmentosSGEA.EdEntidadesEmp.ValueVariant := QrProdutosCadasEntidadesEmp.Value;
      FmSegmentosSGEA.CBEntidadesEmp.KeyValue     := QrProdutosCadasEntidadesEmp.Value;
      FmSegmentosSGEA.EdSegmento.ValueVariant     := QrProdutosCadasSegmento.Value;
      FmSegmentosSGEA.CBSegmento.KeyValue         := QrProdutosCadasSegmento.Value;
      FmSegmentosSGEA.EdAccount.ValueVariant      := QrProdutosCadasAccount.Value;
      FmSegmentosSGEA.CBAccount.KeyValue          := QrProdutosCadasAccount.Value;
      FmSegmentosSGEA.EdAccntSN.ValueVariant      := QrProdutosCadasAccntSN.Value;
      FmSegmentosSGEA.CBAccntSN.KeyValue          := QrProdutosCadasAccntSN.Value;
      FmSegmentosSGEA.EdHvSrDB.ValueVariant       := QrProdutosCadasHvSrDB.Value;
      //
      FmSegmentosSGEA.LaEntidadesEmp.Enabled := False;
      FmSegmentosSGEA.EdEntidadesEmp.Enabled := False;
      FmSegmentosSGEA.CBEntidadesEmp.Enabled := False;
    end;
    FmSegmentosSGEA.ShowModal;
    FmSegmentosSGEA.Destroy;
  end;
}
end;

procedure TFmProdutosCabec.OpcoesCopia(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

end;

procedure TFmProdutosCabec.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrProdutosCabec);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrProdutosCabec, QrProdutosCadas);
end;

procedure TFmProdutosCabec.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrProdutosCabec);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrProdutosCadas);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrProdutosCadas);
end;

procedure TFmProdutosCabec.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOz.Go(Para, QrProdutosCabecCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmProdutosCabec.DefParams;
begin
  VAR_GOTOTABELA := 'produtoscabec';
  VAR_GOTOzSQLTABLE := QrProdutosCabec;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOzSQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOz.LimpaVAR_SQL();

  VAR_SQLx.Add('SELECT ');
  VAR_SQLx.Add(dmkPF.ArrayToTextoCASEWHEN('prc.HvSrDB', 'NO_HvSrDB',
    TPosVirgula.pvPos, sHaveSrcDB, iHaveSrcDB) + ' prc.*');
  VAR_SQLx.Add('FROM produtoscabec prc');
  VAR_SQLx.Add('WHERE prc.Codigo > 0');
  //
  VAR_SQL1.Add('AND prc.Codigo=:P0');
  //
  VAR_SQL2.Add('AND prc.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND prc.Nome Like :P0');
  //
end;

procedure TFmProdutosCabec.EdCidadeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  OpcoesCopia(Sender, Key, Shift);
end;

procedure TFmProdutosCabec.ItsAltera1Click(Sender: TObject);
begin
  MostraEntidadesEmpSGEA(stUpd);
end;

procedure TFmProdutosCabec.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmProdutosCabec.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmProdutosCabec.QueryPrincipalAfterOpen;
begin
end;

procedure TFmProdutosCabec.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
{
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'EntidadesEmpSGEA', 'Controle', QrProdutosCadasControle.Value, Dmod.MyDB?) = ID_YES then
  begin
    Controle := GOTOz.LocalizaPriorNextIntQr(QrProdutosCadas,
      QrProdutosCadasControle, QrProdutosCadasControle.Value);
    ReopenSegmentosSGEA(Segmento, Account(Controle);
  end;
}
end;

procedure TFmProdutosCabec.ReopenProdutosCadas(Codigo: Integer);
begin
  Grl_DmkDB.AbreSQLQuery0(QrProdutosCadas, Dmod.MyDB, [
  'SELECT prd.*,',
  dmkPF.ArrayToTextoCASEWHEN('prd.HvSrDB', 'NO_HvSrDB',
    TPosVirgula.pvPos, sHaveSrcDB, iHaveSrcDB),
  'sgr.Nome NO_GrupoSegmento, sca.Nome NO_Segmento,',
  'pdv.Nome NO_Divisao, psd.Nome NO_SubDivisao,',
  'prf.Nome NO_Familia',
  'FROM produtoscadas prd ',
  'LEFT JOIN segmengrucads sgr ON sgr.Codigo=prd.GrupoSegmento',
  'LEFT JOIN segmentoscads sca ON sca.Codigo=prd.Segmento',
  'LEFT JOIN proddivsaocab pdv ON pdv.Codigo=prd.Divisao',
  'LEFT JOIN proddivsubcab psd ON psd.Codigo=prd.SubDivisao',
  'LEFT JOIN produtosfamil prf ON prf.Codigo=prd.Familia',
  'WHERE prd.ProdCabec=' + Geral.FF0(QrProdutosCabecCodigo.Value),
  '']);
  //
  QrProdutosCadas.Locate('Codigo', VarArrayOf([Codigo]), []);
end;

procedure TFmProdutosCabec.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmProdutosCabec.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmProdutosCabec.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmProdutosCabec.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmProdutosCabec.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmProdutosCabec.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmProdutosCabec.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrProdutosCabecCodigo.Value;
  Close;
end;

procedure TFmProdutosCabec.ItsInclui1Click(Sender: TObject);
begin
  MostraEntidadesEmpSGEA(stIns);
end;

procedure TFmProdutosCabec.CabAltera1Click(Sender: TObject);
begin
  Grl_DmkDB.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrProdutosCabec, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'produtoscabec');
end;

procedure TFmProdutosCabec.BtConfirmaClick(Sender: TObject);
var
  Nome: String;
  Codigo, CodUsu, ProdCabec, Familia, HvSrDB: Integer;
  SQLType: TSQLType;
begin
  SQLType             := ImgTipo.SQLType;
  //
  //Codigo              := EdCodigo.ValueVariant;
  //CodUsu              := EdCodUsu.ValueVariant;
  Grl_DmkDB.ObtemCodigoECodUsu(EdCodigo, EdCodUsu, SQLType, Codigo, CodUsu,
    'produtoscabec', 'Codigo', 'CodUsu');
  Nome                := EdNome.ValueVariant;
  HvSrDB              := EdHvSrDB.ValueVariant;
  //
  if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, 'produtoscabec', False, [
  'CodUsu', 'Nome', 'HvSrDB'], [
  'Codigo'], [
  CodUsu, Nome, HvSrDB], [
  Codigo], True, dmksqlinsInsOnly, '', stDesktop, False) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOz.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmProdutosCabec.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  //if ImgTipo.SQLType = stIns then
    //UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'produtoscabec', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  GOTOz.UpdUnlockZ(Codigo, Dmod.MyDB, 'produtoscabec', 'Codigo');
end;

procedure TFmProdutosCabec.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmProdutosCabec.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmProdutosCabec.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmProdutosCabec.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOz.Codigo(QrProdutosCabecCodigo.Value, LaRegistro.Caption);
end;

procedure TFmProdutosCabec.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOz.Nome(LaRegistro.Caption);
end;

procedure TFmProdutosCabec.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOz.CodUsu(QrProdutosCabecCodigo.Value, LaRegistro.Caption);
end;

procedure TFmProdutosCabec.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOz.Fechar(ImgTipo.SQLType);
end;

procedure TFmProdutosCabec.QrProdutosCabecAfterScroll(DataSet: TDataSet);
begin
  ReopenProdutosCadas(0);
end;

procedure TFmProdutosCabec.QrProdutosCabecBeforeClose(DataSet: TDataSet);
begin
  QrProdutosCadas.Close;
end;

procedure TFmProdutosCabec.QrProdutosCabecNO_HvSrDBGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  Text := TStringField(Sender).Value;
end;

procedure TFmProdutosCabec.QrProdutosCadasNO_HvSrDBGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  Text := TStringField(Sender).Value;
end;

procedure TFmProdutosCabec.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrProdutosCabecCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmProdutosCabec.SbQueryClick(Sender: TObject);
begin
  LocCod(QrProdutosCabecCodigo.Value,
  GOTOz.CriaForm(CO_CODIGO, CO_NOME, 'produtoscabec', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmProdutosCabec.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmProdutosCabec.CabInclui1Click(Sender: TObject);
begin
  Grl_DmkDB.ConfigPanelInsUpd(stIns, Self, PnEdita, QrProdutosCabec, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'produtoscabec');
  EdHvSrDB.ValueVariant := Integer(THaveSrcDB.hsdbCadastr);
end;

end.


