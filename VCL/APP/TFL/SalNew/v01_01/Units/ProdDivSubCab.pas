unit ProdDivSubCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, (***UnGOTOy,***) UnInternalConsts, UnMsgInt, (***UMySQLModule,***) Menus,
  UnInternalConsts2, mySQLDbTables, (***UnMySQLCuringa,***) dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, ZAbstractRODataset, ZDataset, ZAbstractDataset,
  //
  UnGOTOz, UnGrl_DmkDB, dmkEditF7;

type
  TFmProdDivSubCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DGDados: TDBGrid;
    QrProdDivSubCab: TZQuery;
    DsProdDivSubCab: TDataSource;
    QrIts: TZQuery;
    DsIts: TDataSource;
    QrProdDivSubCabCodigo: TLargeintField;
    QrProdDivSubCabNome: TWideStringField;
    QrProdDivSubCabSigla: TWideStringField;
    QrProdDivSubCabLk: TLargeintField;
    QrProdDivSubCabDataCad: TDateField;
    QrProdDivSubCabDataAlt: TDateField;
    QrProdDivSubCabUserCad: TLargeintField;
    QrProdDivSubCabUserAlt: TLargeintField;
    QrProdDivSubCabAlterWeb: TSmallintField;
    QrProdDivSubCabAtivo: TSmallintField;
    QrItsCodigo: TLargeintField;
    QrItsNome: TWideStringField;
    QrItsSigla: TWideStringField;
    QrItsLk: TLargeintField;
    QrItsDataCad: TDateField;
    QrItsDataAlt: TDateField;
    QrItsUserCad: TLargeintField;
    QrItsUserAlt: TLargeintField;
    QrItsAlterWeb: TSmallintField;
    QrItsAtivo: TSmallintField;
    EdSigla: TdmkEdit;
    Label3: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    Label4: TLabel;
    Label9: TLabel;
    QrProdDivSubCabOrdem: TLargeintField;
    Label6: TLabel;
    DBEdit1: TDBEdit;
    QrProdDivSubCabCodUsu: TLargeintField;
    QrProdDivSubCabHvSrDB: TSmallintField;
    Label1: TLabel;
    DBEdCodUsu: TdmkDBEdit;
    Label8: TLabel;
    DBEdit2: TDBEdit;
    Label10: TLabel;
    DBEdit3: TDBEdit;
    QrProdDivSubCabNO_HvSrDB: TWideMemoField;
    EdCodUsu: TdmkEdit;
    Label24: TLabel;
    EdHvSrDB: TdmkEditF7;
    Label22: TLabel;
    EdOrdem: TdmkEdit;
    Label5: TLabel;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrGraGruYAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrGraGruYBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrGraGruYAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrGraGruYBeforeClose(DataSet: TDataSet);
    procedure QrProdDivSubCabNO_HvSrDBGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraCadastro_Com_Itens_ITS(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    //***procedure ReopenCadastro_Com_Itens_ITS(Controle?: Integer);
    procedure ReopenCadastro_Com_Itens_ITS(Controle: Integer);

  end;

var
  FmProdDivSubCab: TFmProdDivSubCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module;//, (***MyDBCheck, DmkDAC_PF,***) Cadastro_Com_Itens_ITS;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmProdDivSubCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOz.LC(Atual, Codigo);
end;

procedure TFmProdDivSubCab.MostraCadastro_Com_Itens_ITS(SQLType: TSQLType);
begin
{
  if Grl_DmkDB.CriaFm(TFmCadastro_Com_Itens_ITS, FmCadastro_Com_Itens_ITS, afmoNegarComAviso) then
  begin
    FmCadastro_Com_Itens_ITS.ImgTipo.SQLType := SQLType;
    FmCadastro_Com_Itens_ITS.FQrCab := QrSegmentosCads;
    FmCadastro_Com_Itens_ITS.FDsCab := DsCadastro_Com_Itens_CAB;
    FmCadastro_Com_Itens_ITS.FQrIts := QrIts;
    if SQLType = stIns then
      FmCadastro_Com_Itens_ITS.EdCPF1.ReadOnly := False
    else
    begin
      FmCadastro_Com_Itens_ITS.EdControle.ValueVariant := QrItsControle.Value;
      //
      FmCadastro_Com_Itens_ITS.EdCPF1.Text := MLAGeral.FormataCNPJ_TFT(QrItsCNPJ_CPF.Value);
      FmCadastro_Com_Itens_ITS.EdNomeEmiSac.Text := QrItsNome.Value;
      FmCadastro_Com_Itens_ITS.EdCPF1.ReadOnly := True;
    end;
    FmCadastro_Com_Itens_ITS.ShowModal;
    FmCadastro_Com_Itens_ITS.Destroy;
  end;
}
end;

procedure TFmProdDivSubCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrProdDivSubCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrProdDivSubCab, QrIts);
end;

procedure TFmProdDivSubCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrProdDivSubCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrIts);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrIts);
end;

procedure TFmProdDivSubCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOz.Go(Para, QrProdDivSubCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmProdDivSubCab.DefParams;
begin
  VAR_GOTOTABELA := 'proddivsubcab';
  VAR_GOTOzSQLTABLE := QrProdDivSubCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOzSQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOz.LimpaVAR_SQL();

  VAR_SQLx.Add('SELECT ');
  VAR_SQLx.Add(dmkPF.ArrayToTextoCASEWHEN('sca.HvSrDB', 'NO_HvSrDB',
    TPosVirgula.pvPos, sHaveSrcDB, iHaveSrcDB) + ' sca.*');
  VAR_SQLx.Add('FROM proddivsubcab sca');
  VAR_SQLx.Add('WHERE sca.Codigo > 0');
  //
  VAR_SQL1.Add('AND sca.Codigo=:P0');
  //
  VAR_SQL2.Add('AND sca.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND sca.Nome Like :P0');
  //
end;

procedure TFmProdDivSubCab.ItsAltera1Click(Sender: TObject);
begin
  MostraCadastro_Com_Itens_ITS(stUpd);
end;

procedure TFmProdDivSubCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmProdDivSubCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmProdDivSubCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmProdDivSubCab.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
{
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'Cadastro_Com_Itens_ITS', 'Controle', QrItsControle.Value, Dmod.MyDB?) = ID_YES then
  begin
    Controle := GOTOz.LocalizaPriorNextIntQr(QrIts,
      QrItsControle, QrItsControle.Value);
    ReopenCadastro_Com_Itens_ITS(Controle);
  end;
}
end;

procedure TFmProdDivSubCab.ReopenCadastro_Com_Itens_ITS(Controle: Integer);
begin
{
  UnDmkDAC_PF.AbreMySQLQuery0(QrIts, Dmod.MyDB, [
  'SELECT * ',
  'FROM cadastro_com_itens_its ',
  'WHERE Codigo=' + Geral.FF0(QrProdDivSubCabCodigo?.Value),
  '']);
  //
  QrIts.Locate('Controle?, Controle, []);
}
end;


procedure TFmProdDivSubCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmProdDivSubCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmProdDivSubCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmProdDivSubCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmProdDivSubCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmProdDivSubCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmProdDivSubCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrProdDivSubCabCodigo.Value;
  Close;
end;

procedure TFmProdDivSubCab.ItsInclui1Click(Sender: TObject);
begin
  MostraCadastro_Com_Itens_ITS(stIns);
end;

procedure TFmProdDivSubCab.CabAltera1Click(Sender: TObject);
begin
  Grl_DmkDB.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrProdDivSubCab, [PnDados],
  [PnEdita], EdSigla, ImgTipo, 'proddivsubcab');
end;

procedure TFmProdDivSubCab.BtConfirmaClick(Sender: TObject);
var
  Nome, Sigla: String;
  Codigo, CodUsu, Ordem, HvSrDB: Integer;
  SQLType: TSQLType;
begin
  SQLType             := ImgTipo.SQLType;
  //
  //Codigo              := EdCodigo.ValueVariant;
  //CodUsu              := EdCodUsu.ValueVariant;
  Grl_DmkDB.ObtemCodigoECodUsu(EdCodigo, EdCodUsu, SQLType, Codigo, CodUsu,
    'proddivsubcab', 'Codigo', 'CodUsu');
  Nome                := EdNome.ValueVariant;
  HvSrDB              := EdHvSrDB.ValueVariant;
  Sigla               := EdSigla.ValueVariant;
  Ordem               := EdOrdem.ValueVariant;
  //
  if MyObjects.FIC(Trim(Nome) = '', EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(Trim(Sigla) = '', EdSigla, 'Defina uma sigla!') then Exit;
  //
  //Codigo := Grl_DmkDB.GetNxtCodigoInt('proddivsubcab', 'Codigo', SQLType, Codigo);
  if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, 'proddivsubcab', False, [
  'Nome', 'Sigla', 'Ordem',
  'CodUsu', 'HvSrDB'], [
  'Codigo'], [
  Nome, Sigla, Ordem,
  CodUsu, HvSrDB], [
  Codigo], True, dmksqlinsInsOnly, '', stDesktop, False) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOz.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmProdDivSubCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  //if ImgTipo.SQLType = stIns then
    //UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'proddivsubcab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  GOTOz.UpdUnlockZ(Codigo, Dmod.MyDB, 'proddivsubcab', 'Codigo');
end;

procedure TFmProdDivSubCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmProdDivSubCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmProdDivSubCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmProdDivSubCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOz.Codigo(QrProdDivSubCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmProdDivSubCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOz.Nome(LaRegistro.Caption);
end;

procedure TFmProdDivSubCab.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOz.CodUsu(QrProdDivSubCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmProdDivSubCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOz.Fechar(ImgTipo.SQLType);
end;

procedure TFmProdDivSubCab.QrGraGruYAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmProdDivSubCab.QrGraGruYAfterScroll(DataSet: TDataSet);
begin
  ReopenCadastro_Com_Itens_ITS(0);
end;

procedure TFmProdDivSubCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrProdDivSubCabCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmProdDivSubCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrProdDivSubCabCodigo.Value,
  GOTOz.CriaForm(CO_CODIGO, CO_NOME, 'proddivsubcab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmProdDivSubCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmProdDivSubCab.CabInclui1Click(Sender: TObject);
begin
  Grl_DmkDB.ConfigPanelInsUpd(stIns, Self, PnEdita, QrProdDivSubCab, [PnDados],
  [PnEdita], EdSigla, ImgTipo, 'proddivsubcab');
  EdHvSrDB.ValueVariant := Integer(THaveSrcDB.hsdbCadastr);
end;

procedure TFmProdDivSubCab.QrGraGruYBeforeClose(
  DataSet: TDataSet);
begin
  QrIts.Close;
end;

procedure TFmProdDivSubCab.QrGraGruYBeforeOpen(DataSet: TDataSet);
begin
  QrProdDivSubCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmProdDivSubCab.QrProdDivSubCabNO_HvSrDBGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  Text := TStringField(Sender).Value;
end;

end.

