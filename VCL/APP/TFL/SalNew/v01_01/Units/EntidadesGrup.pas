unit EntidadesGrup;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, (***UnGOTOy,***) UnInternalConsts, UnMsgInt, (***UMySQLModule,***) Menus,
  UnInternalConsts2, mySQLDbTables, (***UnMySQLCuringa,***) dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, ZAbstractRODataset, ZDataset, ZAbstractDataset,
  //
  UnGOTOz, UnGrl_DmkDB, dmkEditF7;

type
  TFmEntidadesGrup = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DGDados: TDBGrid;
    QrEntidadesGrup: TZQuery;
    DsEntidadesGrup: TDataSource;
    QrEntidadesEmpr: TZQuery;
    DsEntidadesEmpr: TDataSource;
    DBEdNome: TdmkDBEdit;
    Label4: TLabel;
    DBEdCodUsu: TdmkDBEdit;
    Label2: TLabel;
    QrEntidadesGrupCodigo: TLargeintField;
    QrEntidadesGrupCodUsu: TLargeintField;
    QrEntidadesGrupNome: TWideStringField;
    QrEntidadesGrupLk: TLargeintField;
    QrEntidadesGrupDataCad: TDateField;
    QrEntidadesGrupDataAlt: TDateField;
    QrEntidadesGrupUserCad: TLargeintField;
    QrEntidadesGrupUserAlt: TLargeintField;
    QrEntidadesGrupAlterWeb: TSmallintField;
    QrEntidadesGrupAtivo: TSmallintField;
    QrEntidadesEmprCodigo: TLargeintField;
    QrEntidadesEmprCodUsu: TLargeintField;
    QrEntidadesEmprNome: TWideStringField;
    QrEntidadesEmprGrupoEmpresa: TLargeintField;
    QrEntidadesEmprGrupEmprSN: TLargeintField;
    QrEntidadesEmprPais: TWideStringField;
    QrEntidadesEmprRegiao: TWideStringField;
    QrEntidadesEmprUF: TWideStringField;
    QrEntidadesEmprCidade: TWideStringField;
    QrEntidadesEmprLegalUnit: TLargeintField;
    QrEntidadesEmprLk: TLargeintField;
    QrEntidadesEmprDataCad: TDateField;
    QrEntidadesEmprDataAlt: TDateField;
    QrEntidadesEmprUserCad: TLargeintField;
    QrEntidadesEmprUserAlt: TLargeintField;
    QrEntidadesEmprAlterWeb: TSmallintField;
    QrEntidadesEmprAtivo: TSmallintField;
    QrEntidadesEmprPerfil: TLargeintField;
    QrEntidadesEmprHvSrDB: TSmallintField;
    QrEntidadesEmprNO_GrupoEmpresa: TWideStringField;
    QrEntidadesEmprNO_Perfil: TWideStringField;
    QrEntidadesGrupHvSrDB: TSmallintField;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    QrEntidadesGrupNO_HvSrDB: TWideMemoField;
    QrEntidadesEmprNO_HvSrDB: TWideMemoField;
    EdHvSrDB: TdmkEditF7;
    Label22: TLabel;
    EdCodUsu: TdmkEdit;
    Label24: TLabel;
    Label7: TLabel;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrEntidadesGrupBeforeClose(DataSet: TDataSet);
    procedure QrEntidadesGrupAfterScroll(DataSet: TDataSet);
    procedure QrEntidadesGrupNO_HvSrDBGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure QrEntidadesEmprNO_HvSrDBGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure MostraCadastro_Com_Itens_ITS(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure ReopenEntidadesEmpr(Codigo: Integer);

  end;

var
  FmEntidadesGrup: TFmEntidadesGrup;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module;//, (***MyDBCheck, DmkDAC_PF,***) Cadastro_Com_Itens_ITS;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmEntidadesGrup.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOz.LC(Atual, Codigo);
end;

procedure TFmEntidadesGrup.MostraCadastro_Com_Itens_ITS(SQLType: TSQLType);
begin
{
  if Grl_DmkDB.CriaFm(TFmCadastro_Com_Itens_ITS, FmCadastro_Com_Itens_ITS, afmoNegarComAviso) then
  begin
    FmCadastro_Com_Itens_ITS.ImgTipo.SQLType := SQLType;
    FmCadastro_Com_Itens_ITS.FQrCab := QrEntidadesGrup;
    FmCadastro_Com_Itens_ITS.FDsCab := DsCadastro_Com_Itens_CAB;
    FmCadastro_Com_Itens_ITS.FQrIts := QrEntidadesEmpr;
    if SQLType = stIns then
      FmCadastro_Com_Itens_ITS.EdCPF1.ReadOnly := False
    else
    begin
      FmCadastro_Com_Itens_ITS.EdControle.ValueVariant := QrEntidadesEmprControle.Value;
      //
      FmCadastro_Com_Itens_ITS.EdCPF1.Text := MLAGeral.FormataCNPJ_TFT(QrEntidadesEmprCNPJ_CPF.Value);
      FmCadastro_Com_Itens_ITS.EdNomeEmiSac.Text := QrEntidadesEmprNome.Value;
      FmCadastro_Com_Itens_ITS.EdCPF1.ReadOnly := True;
    end;
    FmCadastro_Com_Itens_ITS.ShowModal;
    FmCadastro_Com_Itens_ITS.Destroy;
  end;
}
end;

procedure TFmEntidadesGrup.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrEntidadesGrup);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrEntidadesGrup, QrEntidadesEmpr);
end;

procedure TFmEntidadesGrup.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrEntidadesGrup);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrEntidadesEmpr);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrEntidadesEmpr);
end;

procedure TFmEntidadesGrup.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOz.Go(Para, QrEntidadesGrupCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmEntidadesGrup.DefParams;
begin
  VAR_GOTOTABELA := 'entidadesgrup';
  VAR_GOTOzSQLTABLE := QrEntidadesGrup;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOzSQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOz.LimpaVAR_SQL();

  VAR_SQLx.Add('SELECT ');
  VAR_SQLx.Add(dmkPF.ArrayToTextoCASEWHEN('egr.HvSrDB', 'NO_HvSrDB',
    TPosVirgula.pvPos, sHaveSrcDB, iHaveSrcDB) + ' egr.*');
  VAR_SQLx.Add('FROM entidadesgrup egr ');
  VAR_SQLx.Add('WHERE egr.Codigo > 0');
  //
  VAR_SQL1.Add('AND egr.Codigo=:P0');
  //
  VAR_SQL2.Add('AND egr.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND egr.Nome Like :P0');
  //
end;

procedure TFmEntidadesGrup.ItsAltera1Click(Sender: TObject);
begin
  MostraCadastro_Com_Itens_ITS(stUpd);
end;

procedure TFmEntidadesGrup.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmEntidadesGrup.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmEntidadesGrup.QueryPrincipalAfterOpen;
begin
end;

procedure TFmEntidadesGrup.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
{
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'Cadastro_Com_Itens_ITS', 'Controle', QrEntidadesEmprControle.Value, Dmod.MyDB?) = ID_YES then
  begin
    Controle := GOTOz.LocalizaPriorNextIntQr(QrEntidadesEmpr,
      QrEntidadesEmprControle, QrEntidadesEmprControle.Value);
    ReopenEntidadesEmpr(Controle);
  end;
}
end;

procedure TFmEntidadesGrup.ReopenEntidadesEmpr(Codigo: Integer);
begin
  Grl_DmkDB.AbreSQLQuery0(QrEntidadesEmpr, Dmod.MyDB, [
  'SELECT egr1.Nome NO_GrupoEmpresa, sprf.Nome NO_Perfil, ',
  dmkPF.ArrayToTextoCASEWHEN('emp.HvSrDB', 'NO_HvSrDB',
    TPosVirgula.pvPos, sHaveSrcDB, iHaveSrcDB),
  ' emp.*  ',
  'FROM entidadesempr emp ',
  'LEFT JOIN entidadesgrup egr1 ON egr1.Codigo=emp.GrupoEmpresa ',
  'LEFT JOIN salesperfilcl sprf ON sprf.Codigo=emp.Perfil ',
  'WHERE emp.GrupoEmpresa=' + Geral.FF0(QrEntidadesGrupCodigo.Value),
  '']);
  //
  QrEntidadesEmpr.Locate('Codigo', Codigo, []);
end;


procedure TFmEntidadesGrup.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmEntidadesGrup.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmEntidadesGrup.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmEntidadesGrup.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmEntidadesGrup.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmEntidadesGrup.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEntidadesGrup.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrEntidadesGrupCodigo.Value;
  Close;
end;

procedure TFmEntidadesGrup.ItsInclui1Click(Sender: TObject);
begin
  MostraCadastro_Com_Itens_ITS(stIns);
end;

procedure TFmEntidadesGrup.CabAltera1Click(Sender: TObject);
begin
  Grl_DmkDB.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrEntidadesGrup, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'entidadesgrup');
end;

procedure TFmEntidadesGrup.BtConfirmaClick(Sender: TObject);
var
  Nome: String;
  Codigo, CodUsu, HvSrDB: Integer;
  SQLType: TSQLType;
begin
  SQLType             := ImgTipo.SQLType;
  //
  //Codigo              := EdCodigo.ValueVariant;
  //CodUsu              := EdCodUsu.ValueVariant;
  Grl_DmkDB.ObtemCodigoECodUsu(EdCodigo, EdCodUsu, SQLType, Codigo, CodUsu,
    'entidadesgrup', 'Codigo', 'CodUsu');
  Nome                := EdNome.ValueVariant;
  HvSrDB              := EdHvSrDB.ValueVariant;
  //
  if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, 'entidadesgrup', False, [
  'CodUsu', 'Nome', 'HvSrDB'], [
  'Codigo'], [
  CodUsu, Nome, HvSrDB], [
  Codigo], True, dmksqlinsInsOnly, '', TDeviceType.stDesktop, False) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOz.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmEntidadesGrup.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  //if ImgTipo.SQLType = stIns then
    //UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'entidadesgrup', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  GOTOz.UpdUnlockZ(Codigo, Dmod.MyDB, 'entidadesgrup', 'Codigo');
end;

procedure TFmEntidadesGrup.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmEntidadesGrup.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmEntidadesGrup.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmEntidadesGrup.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOz.Codigo(QrEntidadesGrupCodigo.Value, LaRegistro.Caption);
end;

procedure TFmEntidadesGrup.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOz.Nome(LaRegistro.Caption);
end;

procedure TFmEntidadesGrup.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOz.CodUsu(QrEntidadesGrupCodigo.Value, LaRegistro.Caption);
end;

procedure TFmEntidadesGrup.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOz.Fechar(ImgTipo.SQLType);
end;

procedure TFmEntidadesGrup.QrEntidadesEmprNO_HvSrDBGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  Text := TStringField(Sender).Value;
end;

procedure TFmEntidadesGrup.QrEntidadesGrupAfterScroll(DataSet: TDataSet);
begin
  ReopenEntidadesEmpr(0);
end;

procedure TFmEntidadesGrup.QrEntidadesGrupBeforeClose(DataSet: TDataSet);
begin
  QrEntidadesEmpr.Close;
end;

procedure TFmEntidadesGrup.QrEntidadesGrupNO_HvSrDBGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  Text := TStringField(Sender).Value;
end;

procedure TFmEntidadesGrup.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrEntidadesGrupCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmEntidadesGrup.SbQueryClick(Sender: TObject);
begin
  LocCod(QrEntidadesGrupCodigo.Value,
  GOTOz.CriaForm(CO_CODIGO, CO_NOME, 'entidadesgrup', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmEntidadesGrup.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEntidadesGrup.CabInclui1Click(Sender: TObject);
begin
  Grl_DmkDB.ConfigPanelInsUpd(stIns, Self, PnEdita, QrEntidadesGrup, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'entidadesgrup');
  EdHvSrDB.ValueVariant := Integer(THaveSrcDB.hsdbCadastr);
end;

end.

