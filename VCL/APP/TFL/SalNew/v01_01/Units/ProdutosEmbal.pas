unit ProdutosEmbal;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, (***UnGOTOy,***) UnInternalConsts, UnMsgInt, (***UMySQLModule,***)
  Menus, UnInternalConsts2, mySQLDbTables, (***UnMySQLCuringa,***)
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  Grids, DBGrids, Variants,
  UnDmkProcFunc, UnDmkENums, ZAbstractRODataset, ZDataset, ZAbstractDataset,
  //
  UnGOTOz, UnGrl_DmkDB, dmkDBLookupComboBox, dmkEditCB, dmkEditF7, dmkDBGridZTO;

type
  TFmProdutosEmbal = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DGDados: TdmkDBGridZTO;
    QrProdutosCadas: TZQuery;
    DsProdutosCadas: TDataSource;
    QrSegmentosSGEA: TZQuery;
    DsSegmentosSGEA: TDataSource;
    DBEdNome: TdmkDBEdit;
    DBEdCodUsu: TdmkDBEdit;
    Label2: TLabel;
    Label14: TLabel;
    DBEdit10: TDBEdit;
    Label22: TLabel;
    Label24: TLabel;
    EdCodUsu: TdmkEdit;
    EdHvSrDB: TdmkEditF7;
    QrEmbalagensTar: TZQuery;
    DsEmbalagensTar: TDataSource;
    DBEdit11: TDBEdit;
    QrSegmentosSGEANO_Segmento: TWideStringField;
    QrSegmentosSGEANO_Accn_QT: TWideStringField;
    QrSegmentosSGEANO_Accn_SN: TWideStringField;
    QrSegmentosSGEACodigo: TLargeintField;
    QrSegmentosSGEACodUsu: TLargeintField;
    QrSegmentosSGEASegmento: TLargeintField;
    QrSegmentosSGEAEntidadesEmp: TLargeintField;
    QrSegmentosSGEAAccount: TLargeintField;
    QrSegmentosSGEAAccntSN: TLargeintField;
    QrSegmentosSGEAHvSrDB: TSmallintField;
    QrSegmentosSGEALk: TLargeintField;
    QrSegmentosSGEADataCad: TDateField;
    QrSegmentosSGEADataAlt: TDateField;
    QrSegmentosSGEAUserCad: TLargeintField;
    QrSegmentosSGEAUserAlt: TLargeintField;
    QrSegmentosSGEAAlterWeb: TSmallintField;
    QrSegmentosSGEAAtivo: TSmallintField;
    QrSegmentosSGEANome: TWideStringField;
    QrSegmentosSGEANO_HvSrDB: TWideMemoField;
    QrEmbalagensTarCodigo: TLargeintField;
    QrEmbalagensTarNome: TWideStringField;
    Label25: TLabel;
    EdProdutosCadas: TdmkEditCB;
    CBProdutosCadas: TdmkDBLookupComboBox;
    QrProdutosCadasCodigo: TLargeintField;
    QrProdutosCadasNome: TWideStringField;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    Label11: TLabel;
    EdEmbalagensTar: TdmkEditCB;
    CBEmbalagensTar: TdmkDBLookupComboBox;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    EdCodProd: TdmkEdit;
    Label13: TLabel;
    QrProdutosEmbal: TZQuery;
    DsProdutosEmbal: TDataSource;
    QrProdutosEmbalNO_EmbalagensTar: TWideStringField;
    QrProdutosEmbalNO_ProdutosCadas: TWideStringField;
    QrProdutosEmbalCodigo: TLargeintField;
    QrProdutosEmbalCodUsu: TLargeintField;
    QrProdutosEmbalCodProd: TLargeintField;
    QrProdutosEmbalProdutosCadas: TLargeintField;
    QrProdutosEmbalEmbalagensTar: TLargeintField;
    QrProdutosEmbalNome: TWideStringField;
    QrProdutosEmbalHvSrDB: TSmallintField;
    QrProdutosEmbalLk: TLargeintField;
    QrProdutosEmbalDataCad: TDateField;
    QrProdutosEmbalDataAlt: TDateField;
    QrProdutosEmbalUserCad: TLargeintField;
    QrProdutosEmbalUserAlt: TLargeintField;
    QrProdutosEmbalAlterWeb: TSmallintField;
    QrProdutosEmbalAtivo: TSmallintField;
    QrProdutosEmbalNO_HvSrDB: TWideMemoField;
    Label3: TLabel;
    Label5: TLabel;
    Label8: TLabel;
    Label10: TLabel;
    DBEdit1: TDBEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure EdCidadeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QrProdutosEmbalNO_HvSrDBGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure QrSegmentosSGEANO_HvSrDBGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraEntidadesEmpSGEA(SQLType: TSQLType);
    //
    procedure OpcoesCopia(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure ReopenProdutosCadas();
    procedure ReopenEmbalagensTar();
    //
  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);

  end;

var
  FmProdutosEmbal: TFmProdutosEmbal;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, SegmentosSGEA;//, (***MyDBCheck, DmkDAC_PF,***) EntidadesEmpSGEA;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmProdutosEmbal.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOz.LC(Atual, Codigo);
end;

procedure TFmProdutosEmbal.MostraEntidadesEmpSGEA(SQLType: TSQLType);
begin
  if Grl_DmkDB.CriaFm(TFmSegmentosSGEA, FmSegmentosSGEA, afmoNegarComAviso) then
  begin
    FmSegmentosSGEA.ImgTipo.SQLType := SQLType;
    //FmSegmentosSGEA.FQrCab := QrProdutosEmbal;
    //FmSegmentosSGEA.FDsCab := DsCadastro_Com_Itens_CAB;
    FmSegmentosSGEA.FQrIts := QrSegmentosSGEA;
    if SQLType = stIns then
      FmSegmentosSGEA.EdHvSrDB.ValueVariant       := Integer(THaveSrcDB.hsdbCadastr)
     // FmSegmentosSGEA.EdCPF1.ReadOnly := False
    else
    begin
      FmSegmentosSGEA.EdCodigo.ValueVariant       := QrSegmentosSGEACodigo.Value;
      FmSegmentosSGEA.EdCodUsu.ValueVariant       := QrSegmentosSGEACodUsu.Value;
      FmSegmentosSGEA.EdNome.ValueVariant         := QrSegmentosSGEANome.Value;
      FmSegmentosSGEA.EdEntidadesEmp.ValueVariant := QrSegmentosSGEAEntidadesEmp.Value;
      FmSegmentosSGEA.CBEntidadesEmp.KeyValue     := QrSegmentosSGEAEntidadesEmp.Value;
      FmSegmentosSGEA.EdSegmento.ValueVariant     := QrSegmentosSGEASegmento.Value;
      FmSegmentosSGEA.CBSegmento.KeyValue         := QrSegmentosSGEASegmento.Value;
      FmSegmentosSGEA.EdAccount.ValueVariant      := QrSegmentosSGEAAccount.Value;
      FmSegmentosSGEA.CBAccount.KeyValue          := QrSegmentosSGEAAccount.Value;
      FmSegmentosSGEA.EdAccntSN.ValueVariant      := QrSegmentosSGEAAccntSN.Value;
      FmSegmentosSGEA.CBAccntSN.KeyValue          := QrSegmentosSGEAAccntSN.Value;
      FmSegmentosSGEA.EdHvSrDB.ValueVariant       := QrSegmentosSGEAHvSrDB.Value;
      //
      FmSegmentosSGEA.LaEntidadesEmp.Enabled := False;
      FmSegmentosSGEA.EdEntidadesEmp.Enabled := False;
      FmSegmentosSGEA.CBEntidadesEmp.Enabled := False;
    end;
    FmSegmentosSGEA.ShowModal;
    FmSegmentosSGEA.Destroy;
  end;
end;

procedure TFmProdutosEmbal.OpcoesCopia(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

end;

procedure TFmProdutosEmbal.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrProdutosEmbal);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrProdutosEmbal, QrSegmentosSGEA);
end;

procedure TFmProdutosEmbal.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrProdutosEmbal);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrSegmentosSGEA);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrSegmentosSGEA);
end;

procedure TFmProdutosEmbal.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOz.Go(Para, QrProdutosEmbalCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmProdutosEmbal.DefParams;
begin
  VAR_GOTOTABELA := 'produtosembal';
  VAR_GOTOzSQLTABLE := QrProdutosEmbal;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOzSQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOz.LimpaVAR_SQL();

  VAR_SQLx.Add('SELECT tar.Nome NO_EmbalagensTar, ');
  VAR_SQLx.Add(dmkPF.ArrayToTextoCASEWHEN('pem.HvSrDB', 'NO_HvSrDB',
    TPosVirgula.pvPos, sHaveSrcDB, iHaveSrcDB));
  VAR_SQLx.Add('pca.Nome NO_ProdutosCadas, pem.* ');
  VAR_SQLx.Add('FROM produtosembal pem');
  VAR_SQLx.Add('LEFT JOIN produtoscadas pca ON pca.Codigo=pem.ProdutosCadas');
  VAR_SQLx.Add('LEFT JOIN embalagenstar tar ON tar.Codigo=pem.EmbalagensTar');
  VAR_SQLx.Add('WHERE pem.Codigo > 0');
  //
  VAR_SQL1.Add('AND pem.Codigo=:P0');
  //
  VAR_SQL2.Add('AND pem.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND pem.Nome Like :P0');
  //
end;

procedure TFmProdutosEmbal.EdCidadeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  OpcoesCopia(Sender, Key, Shift);
end;

procedure TFmProdutosEmbal.ItsAltera1Click(Sender: TObject);
begin
  MostraEntidadesEmpSGEA(stUpd);
end;

procedure TFmProdutosEmbal.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmProdutosEmbal.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmProdutosEmbal.QueryPrincipalAfterOpen;
begin
end;

procedure TFmProdutosEmbal.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
{
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'EntidadesEmpSGEA', 'Controle', QrSegmentosSGEAControle.Value, Dmod.MyDB?) = ID_YES then
  begin
    Controle := GOTOz.LocalizaPriorNextIntQr(QrSegmentosSGEA,
      QrSegmentosSGEAControle, QrSegmentosSGEAControle.Value);
    ReopenSegmentosSGEA(Segmento, Account(Controle);
  end;
}
end;

procedure TFmProdutosEmbal.ReopenProdutosCadas();
begin
  Grl_DmkDB.AbreSQLQuery0(QrProdutosCadas, Dmod.MyDB, [
  'SELECT * ',
  'FROM produtosembal  ',
  'ORDER BY Nome ',
  '']);
end;

procedure TFmProdutosEmbal.ReopenEmbalagensTar();
begin
  Grl_DmkDB.AbreSQLQuery0(QrEmbalagensTar, Dmod.MyDB, [
  'SELECT * ',
  'FROM produtosfamil  ',
  'ORDER BY Nome ',
  '']);
end;

procedure TFmProdutosEmbal.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmProdutosEmbal.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmProdutosEmbal.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmProdutosEmbal.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmProdutosEmbal.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmProdutosEmbal.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmProdutosEmbal.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrProdutosEmbalCodigo.Value;
  Close;
end;

procedure TFmProdutosEmbal.ItsInclui1Click(Sender: TObject);
begin
  MostraEntidadesEmpSGEA(stIns);
end;

procedure TFmProdutosEmbal.CabAltera1Click(Sender: TObject);
begin
  Grl_DmkDB.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrProdutosEmbal, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'produtosembal');
end;

procedure TFmProdutosEmbal.BtConfirmaClick(Sender: TObject);
var
  Nome: String;
  Codigo, CodUsu, CodProd, ProdutosCadas, EmbalagensTar, HvSrDB: Integer;
  SQLType: TSQLType;
begin
  SQLType             := ImgTipo.SQLType;
  //
  //Codigo              := EdCodigo.ValueVariant;
  //CodUsu              := EdCodUsu.ValueVariant;
  Grl_DmkDB.ObtemCodigoECodUsu(EdCodigo, EdCodUsu, SQLType, Codigo, CodUsu,
    'produtosembal', 'Codigo', 'CodUsu');
  CodProd             := EdCodProd.ValueVariant;
  ProdutosCadas       := EdProdutosCadas.ValueVariant;
  EmbalagensTar       := EdEmbalagensTar.ValueVariant;;
  Nome                := EdNome.Text;
  HvSrDB              := EdHvSrDB.ValueVariant;

  //
  if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, 'produtosembal', False, [
  'CodUsu', 'CodProd', 'ProdutosCadas',
  'EmbalagensTar', 'Nome', 'HvSrDB'], [
  'Codigo'], [
  CodUsu, CodProd, ProdutosCadas,
  EmbalagensTar, Nome, HvSrDB], [
  Codigo], True, dmksqlinsInsOnly, '', stDesktop, False) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOz.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmProdutosEmbal.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  //if ImgTipo.SQLType = stIns then
    //UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'produtosembal', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  GOTOz.UpdUnlockZ(Codigo, Dmod.MyDB, 'produtosembal', 'Codigo');
end;

procedure TFmProdutosEmbal.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmProdutosEmbal.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmProdutosEmbal.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
  //
  ReopenProdutosCadas();
  ReopenEmbalagensTar();
  //
end;

procedure TFmProdutosEmbal.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOz.Codigo(QrProdutosEmbalCodigo.Value, LaRegistro.Caption);
end;

procedure TFmProdutosEmbal.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOz.Nome(LaRegistro.Caption);
end;

procedure TFmProdutosEmbal.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOz.CodUsu(QrProdutosEmbalCodigo.Value, LaRegistro.Caption);
end;

procedure TFmProdutosEmbal.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOz.Fechar(ImgTipo.SQLType);
end;

procedure TFmProdutosEmbal.QrProdutosEmbalNO_HvSrDBGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  Text := TStringField(Sender).Value;
end;

procedure TFmProdutosEmbal.QrSegmentosSGEANO_HvSrDBGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  Text := TStringField(Sender).Value;
end;

procedure TFmProdutosEmbal.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrProdutosEmbalCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmProdutosEmbal.SbQueryClick(Sender: TObject);
begin
  LocCod(QrProdutosEmbalCodigo.Value,
  GOTOz.CriaForm(CO_CODIGO, CO_NOME, 'produtosembal', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmProdutosEmbal.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmProdutosEmbal.CabInclui1Click(Sender: TObject);
begin
  Grl_DmkDB.ConfigPanelInsUpd(stIns, Self, PnEdita, QrProdutosEmbal, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'produtosembal');
  EdHvSrDB.ValueVariant := Integer(THaveSrcDB.hsdbCadastr);
end;

end.

