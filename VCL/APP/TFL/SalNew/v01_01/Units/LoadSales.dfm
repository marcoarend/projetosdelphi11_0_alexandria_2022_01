object FmLoadSales: TFmLoadSales
  Left = 0
  Top = 0
  Caption = 'LOD-SALES-001 :: Importa'#231#227'o de Movimento de Produtos'
  ClientHeight = 348
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object SGSales: TStringGrid
    Left = 0
    Top = 133
    Width = 784
    Height = 144
    Align = alBottom
    TabOrder = 0
    Visible = False
    OnDblClick = SGSalesDblClick
    ExplicitTop = 296
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 277
    Width = 784
    Height = 71
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    ExplicitTop = 430
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 54
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 37
        Width = 780
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object Panel5: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 85
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object Label27: TLabel
      Left = 8
      Top = 4
      Width = 120
      Height = 13
      Caption = 'Arquivo a ser carregado:'
    end
    object SbSelArq: TSpeedButton
      Left = 700
      Top = 19
      Width = 27
      Height = 23
      Caption = '1'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Wingdings'
      Font.Style = []
      ParentFont = False
      OnClick = SbSelArqClick
    end
    object SbAbre: TSpeedButton
      Left = 728
      Top = 19
      Width = 27
      Height = 23
      Caption = #241
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Wingdings'
      Font.Style = []
      ParentFont = False
      OnClick = SbAbreClick
    end
    object SpeedButton1: TSpeedButton
      Left = 756
      Top = 19
      Width = 27
      Height = 23
      Caption = #254
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Wingdings'
      Font.Style = []
      ParentFont = False
      OnClick = SpeedButton1Click
    end
    object Label2: TLabel
      Left = 68
      Top = 44
      Width = 120
      Height = 13
      Caption = 'Linha m'#225'xima dos t'#237'tulos:'
    end
    object Label1: TLabel
      Left = 12
      Top = 44
      Width = 40
      Height = 13
      Caption = 'Planilha:'
    end
    object EdPath: TdmkEdit
      Left = 8
      Top = 20
      Width = 689
      Height = 21
      TabOrder = 0
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = 'C:\Dermatek\_CVS_.csv'
      QryCampo = 'DirNFeGer'
      UpdCampo = 'DirNFeGer'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 'C:\Dermatek\_CVS_.csv'
      ValWarn = False
    end
    object EdMaxlinTit: TdmkEdit
      Left = 68
      Top = 60
      Width = 121
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfLongint
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '100'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 100
      ValWarn = False
    end
    object EdPlanilha: TdmkEdit
      Left = 12
      Top = 60
      Width = 53
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfLongint
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '1'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 1
      ValWarn = False
    end
    object Button4: TButton
      Left = 728
      Top = 44
      Width = 55
      Height = 25
      Caption = 'Parar'
      TabOrder = 3
      OnClick = Button4Click
    end
  end
  object Memo1: TMemo
    Left = 0
    Top = 85
    Width = 784
    Height = 48
    Align = alClient
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 206
    Font.Height = -13
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    Lines.Strings = (
      'Memo1')
    ParentFont = False
    TabOrder = 3
    ExplicitHeight = 89
  end
  object Query1: TZQuery
    Params = <>
    Left = 216
    Top = 164
  end
  object Query2: TZQuery
    Params = <>
    Left = 284
    Top = 164
  end
end
