unit LoadSalesGen;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  ZAbstractRODataset, ZAbstractDataset, ZDataset, dmkDBGridZTO, UnGOTOz,
  dmkDBLookupComboBox, dmkEditCB;

type
  TFmLoadSalesGen = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrProdutosCadas: TZQuery;
    DsProdutosCadas: TDataSource;
    DBGCadas: TdmkDBGridZTO;
    QrProdutosCadasCodigo: TLargeintField;
    QrProdutosCadasNome: TWideStringField;
    BitBtn1: TBitBtn;
    QrProdutosCadasFamilia: TLargeintField;
    QrProdutosCabec: TZQuery;
    LargeintField1: TLargeintField;
    StringField1: TWideStringField;
    LargeintField2: TLargeintField;
    DsProdutosCabec: TDataSource;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
    procedure ReopenProdutosCadas(Codigo: Integer);
  end;

  var
  FmLoadSalesGen: TFmLoadSalesGen;

implementation

uses UnMyObjects, UnGrl_DmkDB, Module, LoadSalesGenAtrel;

{$R *.DFM}

procedure TFmLoadSalesGen.BitBtn1Click(Sender: TObject);
var
  Nome: String;
  I, Codigo, CodUsu, (*Familia,*) ProdCabec, HvSrDB: Integer;
  SQLType: TSQLType;
begin
  SQLType             := stIns;
  //
  if MyObjects.FIC(DBGCadas.SelectedRows.Count = 0, DBGCadas,
    'Nenhum produto foi selecionado!') then Exit;
  for I := 0 to DBGCadas.SelectedRows.Count-1 do
  begin
    QrProdutosCadas.GotoBookmark(pointer(DBGCadas.SelectedRows.Items[I]));
    Codigo              := QrProdutosCadasCodigo.Value;
    CodUsu              := Codigo;
    HvSrDB              := Integer(THaveSrcDB.hsdbImported);
    //Familia             := QrProdutosCadasFamilia.Value;
    Nome                := QrProdutosCadasNome.Value;
    //
    if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, 'produtoscabec', False, [
    //'Familia',
    'Nome', 'CodUsu',
    'HvSrDB'], [
    'Codigo'], [
    //Familia,
    Nome, CodUsu,
    HvSrDB], [
    Codigo], True, dmksqlinsInsOnly, '', TDeviceType.stDesktop, False) then
    begin
      ProdCabec := Codigo;
      if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, (*SQLType*)stUpd, 'produtoscadas', False, [
      'ProdCabec'], [
      'Codigo'], [
      ProdCabec], [
      Codigo], True, dmksqlinsInsOnly, '', TDeviceType.stDesktop, False) then ;
    end;
  end;
  begin
    Codigo := Grl_DmkDB.LocalizaPriorNextLargeIntQr(QrProdutosCadas,
    QrProdutosCadasCodigo, QrProdutosCadasCodigo.Value);
    ReopenProdutosCadas(Codigo);
  end;
end;

procedure TFmLoadSalesGen.BtOKClick(Sender: TObject);
begin
  if Grl_DmkDB.CriaFm(TFmLoadSalesGenAtrel, FmLoadSalesGenAtrel, afmoLiberado) then
  begin
    FmLoadSalesGenAtrel.ImgTipo.SQLType := stUpd;
    FmLoadSalesGenAtrel.ReopenProdutosCabec();
    FmLoadSalesGenAtrel.EdProdutosCadasInt.ValueVariant := QrProdutosCadasCodigo.Value;
    FmLoadSalesGenAtrel.EdProdutosCadasTxt.ValueVariant := QrProdutosCadasNome.Value;
    FmLoadSalesGenAtrel.ShowModal;
    FmLoadSalesGenAtrel.Destroy;
  end;
end;

procedure TFmLoadSalesGen.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLoadSalesGen.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLoadSalesGen.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmLoadSalesGen.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLoadSalesGen.ReopenProdutosCadas(Codigo: Integer);
begin
  Grl_DmkDB.AbreSQLQuery0(QrProdutosCadas, Dmod.MyDB, [
  'SELECT * ',
  'FROM produtoscadas  ',
  'WHERE ProdCabec=0 ',
  'ORDER BY Nome ',
  '']);
  //
  if Codigo <> 0 then
    QrProdutosCadas.Locate('Codigo', Codigo, []);
end;

end.
