unit ProdutosCadas;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, (***UnGOTOy,***) UnInternalConsts, UnMsgInt, (***UMySQLModule,***)
  Menus, UnInternalConsts2, mySQLDbTables, (***UnMySQLCuringa,***)
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  Grids, DBGrids, Variants,
  UnDmkProcFunc, UnDmkENums, ZAbstractRODataset, ZDataset, ZAbstractDataset,
  //
  UnGOTOz, UnGrl_DmkDB, dmkDBLookupComboBox, dmkEditCB, dmkEditF7, dmkDBGridZTO;

type
  TFmProdutosCadas = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DGDados: TdmkDBGridZTO;
    QrProdutosCadas: TZQuery;
    DsProdutosCadas: TDataSource;
    QrSegmentosSGEA: TZQuery;
    DsSegmentosSGEA: TDataSource;
    DBEdNome: TdmkDBEdit;
    Label4: TLabel;
    DBEdCodUsu: TdmkDBEdit;
    Label2: TLabel;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label14: TLabel;
    DBEdit10: TDBEdit;
    Label15: TLabel;
    Label22: TLabel;
    Label24: TLabel;
    EdCodUsu: TdmkEdit;
    EdProdCabec: TdmkEditCB;
    CBProdCabec: TdmkDBLookupComboBox;
    QrProdutosCabec: TZQuery;
    DsProdutosCabec: TDataSource;
    EdHvSrDB: TdmkEditF7;
    QrProdutosFamil: TZQuery;
    DsProdutosFamil: TDataSource;
    DBEdit11: TDBEdit;
    QrProdutosCadasNO_HvSrDB: TWideMemoField;
    QrSegmentosSGEANO_Segmento: TWideStringField;
    QrSegmentosSGEANO_Accn_QT: TWideStringField;
    QrSegmentosSGEANO_Accn_SN: TWideStringField;
    QrSegmentosSGEACodigo: TLargeintField;
    QrSegmentosSGEACodUsu: TLargeintField;
    QrSegmentosSGEASegmento: TLargeintField;
    QrSegmentosSGEAEntidadesEmp: TLargeintField;
    QrSegmentosSGEAAccount: TLargeintField;
    QrSegmentosSGEAAccntSN: TLargeintField;
    QrSegmentosSGEAHvSrDB: TSmallintField;
    QrSegmentosSGEALk: TLargeintField;
    QrSegmentosSGEADataCad: TDateField;
    QrSegmentosSGEADataAlt: TDateField;
    QrSegmentosSGEAUserCad: TLargeintField;
    QrSegmentosSGEAUserAlt: TLargeintField;
    QrSegmentosSGEAAlterWeb: TSmallintField;
    QrSegmentosSGEAAtivo: TSmallintField;
    QrSegmentosSGEANome: TWideStringField;
    QrSegmentosSGEANO_HvSrDB: TWideMemoField;
    QrProdutosCabecCodigo: TLargeintField;
    QrProdutosCabecNome: TWideStringField;
    QrProdutosFamilCodigo: TLargeintField;
    QrProdutosFamilNome: TWideStringField;
    QrSegmenGruCads: TZQuery;
    DsSegmenGruCads: TDataSource;
    QrSegmenGruCadsCodigo: TLargeintField;
    QrSegmenGruCadsNome: TWideStringField;
    QrSegmentosCads: TZQuery;
    DsSegmentosCads: TDataSource;
    QrProdDivsaoCab: TZQuery;
    DsProdDivsaoCab: TDataSource;
    QrProdDivSubCab: TZQuery;
    DsProdDivSubCab: TDataSource;
    QrSegmentosCadsCodigo: TLargeintField;
    QrSegmentosCadsNome: TWideStringField;
    QrProdDivsaoCabCodigo: TLargeintField;
    QrProdDivsaoCabNome: TWideStringField;
    QrProdDivSubCabCodigo: TLargeintField;
    QrProdDivSubCabNome: TWideStringField;
    Label25: TLabel;
    EdGrupoSegmento: TdmkEditCB;
    CBGrupoSegmento: TdmkDBLookupComboBox;
    Label26: TLabel;
    EdSegmento: TdmkEditCB;
    CBSegmento: TdmkDBLookupComboBox;
    Label27: TLabel;
    EdDivisao: TdmkEditCB;
    CBDivisao: TdmkDBLookupComboBox;
    QrProdutosCadasCodigo: TLargeintField;
    QrProdutosCadasCodUsu: TLargeintField;
    QrProdutosCadasProdCabec: TLargeintField;
    QrProdutosCadasGrupoSegmento: TLargeintField;
    QrProdutosCadasSegmento: TLargeintField;
    QrProdutosCadasDivisao: TLargeintField;
    QrProdutosCadasSubDivisao: TLargeintField;
    QrProdutosCadasFamilia: TLargeintField;
    QrProdutosCadasNome: TWideStringField;
    QrProdutosCadasHvSrDB: TSmallintField;
    QrProdutosCadasLk: TLargeintField;
    QrProdutosCadasDataCad: TDateField;
    QrProdutosCadasDataAlt: TDateField;
    QrProdutosCadasUserCad: TLargeintField;
    QrProdutosCadasUserAlt: TLargeintField;
    QrProdutosCadasAlterWeb: TSmallintField;
    QrProdutosCadasAtivo: TSmallintField;
    QrProdutosCadasNO_GrupoSegmento: TWideStringField;
    QrProdutosCadasNO_Segmento: TWideStringField;
    QrProdutosCadasNO_Divisao: TWideStringField;
    QrProdutosCadasNO_SubDivisao: TWideStringField;
    QrProdutosCadasNO_Familia: TWideStringField;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    Label10: TLabel;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit12: TDBEdit;
    Label28: TLabel;
    EdSubDivisao: TdmkEditCB;
    CBSubDivisao: TdmkDBLookupComboBox;
    Label11: TLabel;
    EdFamilia: TdmkEditCB;
    CBFamilia: TdmkDBLookupComboBox;
    DBEdit13: TDBEdit;
    Label12: TLabel;
    DBEdit14: TDBEdit;
    QrProdutosCadasNO_ProdCabec: TWideStringField;
    Label13: TLabel;
    EdLista: TdmkEditCB;
    CBLista: TdmkDBLookupComboBox;
    QrProdutosTabls: TZQuery;
    DsProdutosTabls: TDataSource;
    QrProdutosTablsCodigo: TLargeintField;
    QrProdutosTablsNome: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrProdutosCadasBeforeClose(DataSet: TDataSet);
    procedure QrProdutosCadasAfterScroll(DataSet: TDataSet);
    procedure EdCidadeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QrProdutosCadasNO_HvSrDBGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure QrSegmentosSGEANO_HvSrDBGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraEntidadesEmpSGEA(SQLType: TSQLType);
    //
    procedure OpcoesCopia(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure ReopenProdutosCabec();
    procedure ReopenProdutosCadas();
    //
    procedure ReopenProdDivsaoCab();
    procedure ReopenProdDivSubCab();
    procedure ReopenProdutosFamil();
    procedure ReopenProdutosTabls();
    procedure ReopenSegmenGruCads();
    procedure ReopenSegmentosCads();
    procedure ReopenSegmentosSGEA(Segmento, Account: Integer);
  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    FConfirmado: Boolean;
    FProdNom, FEmbalagem: String;
    FProdCod, FDiluicao: Integer;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure CadastraProduto();

  end;

var
  FmProdutosCadas: TFmProdutosCadas;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, SegmentosSGEA;//, (***MyDBCheck, DmkDAC_PF,***) EntidadesEmpSGEA;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmProdutosCadas.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOz.LC(Atual, Codigo);
end;

procedure TFmProdutosCadas.MostraEntidadesEmpSGEA(SQLType: TSQLType);
begin
  if Grl_DmkDB.CriaFm(TFmSegmentosSGEA, FmSegmentosSGEA, afmoNegarComAviso) then
  begin
    FmSegmentosSGEA.ImgTipo.SQLType := SQLType;
    //FmSegmentosSGEA.FQrCab := QrProdutosCadas;
    //FmSegmentosSGEA.FDsCab := DsCadastro_Com_Itens_CAB;
    FmSegmentosSGEA.FQrIts := QrSegmentosSGEA;
    if SQLType = stIns then
      FmSegmentosSGEA.EdHvSrDB.ValueVariant       := Integer(THaveSrcDB.hsdbCadastr)
     // FmSegmentosSGEA.EdCPF1.ReadOnly := False
    else
    begin
      FmSegmentosSGEA.EdCodigo.ValueVariant       := QrSegmentosSGEACodigo.Value;
      FmSegmentosSGEA.EdCodUsu.ValueVariant       := QrSegmentosSGEACodUsu.Value;
      FmSegmentosSGEA.EdNome.ValueVariant         := QrSegmentosSGEANome.Value;
      FmSegmentosSGEA.EdEntidadesEmp.ValueVariant := QrSegmentosSGEAEntidadesEmp.Value;
      FmSegmentosSGEA.CBEntidadesEmp.KeyValue     := QrSegmentosSGEAEntidadesEmp.Value;
      FmSegmentosSGEA.EdSegmento.ValueVariant     := QrSegmentosSGEASegmento.Value;
      FmSegmentosSGEA.CBSegmento.KeyValue         := QrSegmentosSGEASegmento.Value;
      FmSegmentosSGEA.EdAccount.ValueVariant      := QrSegmentosSGEAAccount.Value;
      FmSegmentosSGEA.CBAccount.KeyValue          := QrSegmentosSGEAAccount.Value;
      FmSegmentosSGEA.EdAccntSN.ValueVariant      := QrSegmentosSGEAAccntSN.Value;
      FmSegmentosSGEA.CBAccntSN.KeyValue          := QrSegmentosSGEAAccntSN.Value;
      FmSegmentosSGEA.EdHvSrDB.ValueVariant       := QrSegmentosSGEAHvSrDB.Value;
      //
      FmSegmentosSGEA.LaEntidadesEmp.Enabled := False;
      FmSegmentosSGEA.EdEntidadesEmp.Enabled := False;
      FmSegmentosSGEA.CBEntidadesEmp.Enabled := False;
    end;
    FmSegmentosSGEA.ShowModal;
    FmSegmentosSGEA.Destroy;
  end;
end;

procedure TFmProdutosCadas.OpcoesCopia(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

end;

procedure TFmProdutosCadas.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrProdutosCadas);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrProdutosCadas, QrSegmentosSGEA);
end;

procedure TFmProdutosCadas.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrProdutosCadas);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrSegmentosSGEA);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrSegmentosSGEA);
end;

procedure TFmProdutosCadas.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOz.Go(Para, QrProdutosCadasCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmProdutosCadas.DefParams;
begin
  VAR_GOTOTABELA := 'produtoscadas';
  VAR_GOTOzSQLTABLE := QrProdutosCadas;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOzSQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOz.LimpaVAR_SQL();

  VAR_SQLx.Add('SELECT prc.Nome NO_ProdCabec, prd.*, ');
  VAR_SQLx.Add(dmkPF.ArrayToTextoCASEWHEN('prd.HvSrDB', 'NO_HvSrDB',
    TPosVirgula.pvPos, sHaveSrcDB, iHaveSrcDB));
  VAR_SQLx.Add('sgr.Nome NO_GrupoSegmento, sca.Nome NO_Segmento,');
  VAR_SQLx.Add('pdv.Nome NO_Divisao, psd.Nome NO_SubDivisao,');
  VAR_SQLx.Add('prf.Nome NO_Familia ');
  VAR_SQLx.Add('FROM produtoscadas prd ');
  VAR_SQLx.Add('LEFT JOIN produtoscabec prc ON prc.Codigo=prd.ProdCabec');
  VAR_SQLx.Add('LEFT JOIN segmengrucads sgr ON sgr.Codigo=prd.GrupoSegmento');
  VAR_SQLx.Add('LEFT JOIN segmentoscads sca ON sca.Codigo=prd.Segmento');
  VAR_SQLx.Add('LEFT JOIN proddivsaocab pdv ON pdv.Codigo=prd.Divisao');
  VAR_SQLx.Add('LEFT JOIN proddivsubcab psd ON psd.Codigo=prd.SubDivisao');
  VAR_SQLx.Add('LEFT JOIN produtosfamil prf ON prf.Codigo=prd.Familia');
  VAR_SQLx.Add('WHERE prd.Codigo > 0');
  //
  VAR_SQL1.Add('AND prd.Codigo=:P0');
  //
  VAR_SQL2.Add('AND prd.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND prd.Nome Like :P0');
  //
end;

procedure TFmProdutosCadas.EdCidadeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  OpcoesCopia(Sender, Key, Shift);
end;

procedure TFmProdutosCadas.ItsAltera1Click(Sender: TObject);
begin
  MostraEntidadesEmpSGEA(stUpd);
end;

procedure TFmProdutosCadas.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmProdutosCadas.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmProdutosCadas.QueryPrincipalAfterOpen;
begin
end;

procedure TFmProdutosCadas.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
{
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'EntidadesEmpSGEA', 'Controle', QrSegmentosSGEAControle.Value, Dmod.MyDB?) = ID_YES then
  begin
    Controle := GOTOz.LocalizaPriorNextIntQr(QrSegmentosSGEA,
      QrSegmentosSGEAControle, QrSegmentosSGEAControle.Value);
    ReopenSegmentosSGEA(Segmento, Account(Controle);
  end;
}
end;

procedure TFmProdutosCadas.ReopenProdDivsaoCab();
begin
  Grl_DmkDB.AbreSQLQuery0(QrProdDivsaoCab, Dmod.MyDB, [
  'SELECT * ',
  'FROM proddivsaocab  ',
  'ORDER BY Nome ',
  '']);
end;

procedure TFmProdutosCadas.ReopenProdDivSubCab();
begin
  Grl_DmkDB.AbreSQLQuery0(QrProdDivSubCab, Dmod.MyDB, [
  'SELECT * ',
  'FROM proddivsubcab  ',
  'ORDER BY Nome ',
  '']);
end;

procedure TFmProdutosCadas.ReopenProdutosCabec();
begin
  Grl_DmkDB.AbreSQLQuery0(QrProdutosCabec, Dmod.MyDB, [
  'SELECT * ',
  'FROM produtoscabec  ',
  'ORDER BY Nome ',
  '']);
end;

procedure TFmProdutosCadas.ReopenProdutosCadas();
begin
{
SELECT prc.Nome NO_ProdCabec,
 CASE prd.HvSrDB WHEN 0 THEN "Indefinido"  WHEN 3 THEN "Importado"  WHEN 6 THEN "Cadastrado"  WHEN 9 THEN "Deletar"  ELSE "?" END NO_HvSrDB,
 prd.*
FROM produtoscadas prd
LEFT JOIN produtoscabec prc  ON prc.Codigo=prd.ProdCabec
WHERE prd.Codigo > 0
}

end;

procedure TFmProdutosCadas.ReopenProdutosFamil();
begin
  Grl_DmkDB.AbreSQLQuery0(QrProdutosFamil, Dmod.MyDB, [
  'SELECT * ',
  'FROM produtosfamil  ',
  'ORDER BY Nome ',
  '']);
end;

procedure TFmProdutosCadas.ReopenProdutosTabls();
begin
  Grl_DmkDB.AbreSQLQuery0(QrProdutosTabls, Dmod.MyDB, [
  'SELECT Codigo, Nome ',
  'FROM produtostabls ',
  'WHERE Codigo IN (0,1,2,3,4,5) ',
  'ORDER BY Nome ',
  '']);
end;

procedure TFmProdutosCadas.ReopenSegmenGruCads();
begin
  Grl_DmkDB.AbreSQLQuery0(QrSegmenGruCads, Dmod.MyDB, [
  'SELECT * ',
  'FROM segmengrucads  ',
  'ORDER BY Nome ',
  '']);
end;

procedure TFmProdutosCadas.ReopenSegmentosCads();
begin
  Grl_DmkDB.AbreSQLQuery0(QrSegmentosCads, Dmod.MyDB, [
  'SELECT * ',
  'FROM segmentoscads  ',
  'ORDER BY Nome ',
  '']);
end;

procedure TFmProdutosCadas.ReopenSegmentosSGEA(Segmento, Account: Integer);
begin
  Grl_DmkDB.AbreSQLQuery0(QrSegmentosSGEA, Dmod.MyDB, [
  'SELECT segc.Nome NO_Segmento, /*empr.Nome NO_Cliente,*/  ',
  'acqt.Nome NO_Accn_QT, acsn.Nome NO_Accn_SN, ',
  dmkPF.ArrayToTextoCASEWHEN('sgea.HvSrDB', 'NO_HvSrDB',
    TPosVirgula.pvPos, sHaveSrcDB, iHaveSrcDB),
  'sgea.*  ',
  'FROM segmentossgea sgea ',
  'LEFT JOIN segmentoscads segc ON segc.Codigo=sgea.Segmento ',
  '/*LEFT JOIN produtoscadas empr ON empr.Codigo=sgea.EntidadesEmp*/ ',
  'LEFT JOIN entidadesaccn acqt ON acqt.Codigo=sgea.Account ',
  'LEFT JOIN entidadesaccn acsn ON acsn.Codigo=sgea.AccntSN ',
  'WHERE EntidadesEmp=' + Geral.FF0(QrProdutosCadasCodigo.Value),
  'ORDER BY segc.Ordem, segc.Sigla ',
  '']);
  //
  QrSegmentosSGEA.Locate('Segmento;Account', VarArrayOf([Segmento, Account]), []);
end;

procedure TFmProdutosCadas.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmProdutosCadas.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmProdutosCadas.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmProdutosCadas.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmProdutosCadas.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmProdutosCadas.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmProdutosCadas.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrProdutosCadasCodigo.Value;
  Close;
end;

procedure TFmProdutosCadas.ItsInclui1Click(Sender: TObject);
begin
  MostraEntidadesEmpSGEA(stIns);
end;

procedure TFmProdutosCadas.CabAltera1Click(Sender: TObject);
begin
  Grl_DmkDB.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrProdutosCadas, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'produtoscadas');
end;

procedure TFmProdutosCadas.BtConfirmaClick(Sender: TObject);
var
  Nome: String;
  Lista, Codigo, CodUsu, IDServr, ProdCabec, GrupoSegmento, Segmento, Divisao,
  SubDivisao, Familia, HvSrDB, Diluicao, Boca: Integer;
  SQLType: TSQLType;
begin
{
  SQLType             := ImgTipo.SQLType;
  //
  //Codigo              := EdCodigo.ValueVariant;
  //CodUsu              := EdCodUsu.ValueVariant;
  Grl_DmkDB.ObtemCodigoECodUsu(EdCodigo, EdCodUsu, SQLType, Codigo, CodUsu,
    'produtoscadas', 'Codigo', 'CodUsu');
  ProdCabec           := EdProdCabec.ValueVariant;
  GrupoSegmento       := EdGrupoSegmento.ValueVariant;
  Segmento            := EdSegmento.ValueVariant;
  Divisao             := EdDivisao.ValueVariant;
  SubDivisao          := EdSubDivisao.ValueVariant;
  Familia             := EdFamilia.ValueVariant;
  Nome                := EdNome.ValueVariant;
  HvSrDB              := EdHvSrDB.ValueVariant;
  //
  //
  Lista               := EdLista.ValueVariant;
  //Codigo              := EdCodigo.ValueVariant;
  //CodUsu              := EdCodUsu.ValueVariant;
  Grl_DmkDB.ObtemCodigoECodUsu(EdCodigo, EdCodUsu, SQLType, Codigo, CodUsu,
    'produtoscadas', 'Codigo', 'CodUsu');
  ProdCabec           := EdProdCabec.ValueVariant;
  IDServr             := VAR_IDServr;
  GrupoSegmento       := EdGrupoSegmento.ValueVariant;
  Segmento            := EdSegmento.ValueVariant;
  Divisao             := EdDivisao.ValueVariant;
  SubDivisao          := EdSubDivisao.ValueVariant;
  Familia             := EdFamilia.ValueVariant;
  Nome                := EdNome.ValueVariant;
  HvSrDB              := EdHvSrDB.ValueVariant;
  Diluicao            := EdDiluicao.ValueVariant;
  Boca                := RGBoca.ItemIndex;

  //
  if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, 'produtoscadas', False, [
  'CodUsu', 'IDServr',
  'ProdCabec', 'GrupoSegmento', 'Segmento',
  'Divisao', 'SubDivisao', 'Familia',
  'Nome', 'HvSrDB', 'Diluicao',
  'Boca'], [
  'Lista', 'Codigo'], [
  Codigo, CodUsu, IDServr,
  ProdCabec, GrupoSegmento, Segmento,
  Divisao, SubDivisao, Familia,
  Nome, HvSrDB, Diluicao,
  Boca], [
  Lista, Codigo], True, dmksqlinsInsOnly, '', stDesktop, False) then
  begin
(*
  if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, 'produtoscadas', False, [
  'CodUsu', 'ProdCabec', 'GrupoSegmento',
  'Segmento', 'Divisao', 'SubDivisao',
  'Familia', 'Nome', 'HvSrDB'], [
  'Codigo'], [
  CodUsu, ProdCabec, GrupoSegmento,
  Segmento, Divisao, SubDivisao,
  Familia, Nome, HvSrDB], [
  Codigo], True, dmksqlinsInsOnly, '', stDesktop, False) then
  begin
*)
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOz.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then
    begin
      FConfirmado := True;
      FProdNom    := Nome;
      FEmbalagem  := '';
      FProdCod    := Codigo;
      //FDiluicao   : Integer;
      Close;
    end;
  end;
}
end;

procedure TFmProdutosCadas.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  //if ImgTipo.SQLType = stIns then
    //UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'produtoscadas', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  GOTOz.UpdUnlockZ(Codigo, Dmod.MyDB, 'produtoscadas', 'Codigo');
end;

procedure TFmProdutosCadas.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmProdutosCadas.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmProdutosCadas.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
  //
  FConfirmado := False;
  FProdNom    := '';
  FEmbalagem  := '';
  FProdCod    := 0;
  //
  ReopenProdutosCabec();
  //
  ReopenProdDivsaoCab();
  ReopenProdDivSubCab();
  ReopenProdutosFamil();
  ReopenSegmenGruCads();
  ReopenProdutosTabls();
  ReopenSegmentosCads();
end;

procedure TFmProdutosCadas.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOz.Codigo(QrProdutosCadasCodigo.Value, LaRegistro.Caption);
end;

procedure TFmProdutosCadas.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOz.Nome(LaRegistro.Caption);
end;

procedure TFmProdutosCadas.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOz.CodUsu(QrProdutosCadasCodigo.Value, LaRegistro.Caption);
end;

procedure TFmProdutosCadas.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOz.Fechar(ImgTipo.SQLType);
end;

procedure TFmProdutosCadas.QrProdutosCadasAfterScroll(DataSet: TDataSet);
begin
  ReopenSegmentosSGEA(0, 0);
end;

procedure TFmProdutosCadas.QrProdutosCadasBeforeClose(DataSet: TDataSet);
begin
  QrSegmentosSGEA.Close;
end;

procedure TFmProdutosCadas.QrProdutosCadasNO_HvSrDBGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  Text := TStringField(Sender).Value;
end;

procedure TFmProdutosCadas.QrSegmentosSGEANO_HvSrDBGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  Text := TStringField(Sender).Value;
end;

procedure TFmProdutosCadas.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrProdutosCadasCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmProdutosCadas.SbQueryClick(Sender: TObject);
begin
  LocCod(QrProdutosCadasCodigo.Value,
  GOTOz.CriaForm(CO_CODIGO, CO_NOME, 'produtoscadas', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmProdutosCadas.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmProdutosCadas.CabInclui1Click(Sender: TObject);
begin
  CadastraProduto();
end;

procedure TFmProdutosCadas.CadastraProduto();
begin
  Grl_DmkDB.ConfigPanelInsUpd(stIns, Self, PnEdita, QrProdutosCadas, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'produtoscadas');
  EdHvSrDB.ValueVariant := Integer(THaveSrcDB.hsdbCadastr);
end;

{
var
  Nome: String;
  Codigo, CodUsu, CodProd, ProdutosCadas, EmbalagensCad, HvSrDB: Integer;
  SQLType: TSQLType;
begin
  SQLType             := ImgTipo.SQLType?;
  //
  Codigo              := ;
  CodUsu              := ;
  CodProd             := ;
  ProdutosCadas       := ;
  EmbalagensCad       := ;
  Nome                := ;
  HvSrDB              := ;

  //
? := Grl_DmkDB.GetNxtCodigoInt('produtosembal', 'Codigo', SQLType, CodAtual?);
ou > ? := UMyMod.BPGS1I32('produtosembal', 'Codigo', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, 'produtosembal', auto_increment?[
'CodUsu', 'CodProd', 'ProdutosCadas',
'EmbalagensCad', 'Nome', 'HvSrDB'], [
'Codigo'], [
CodUsu, CodProd, ProdutosCadas,
EmbalagensCad, Nome, HvSrDB], [
Codigo], UserDataAlterweb?, TdmkSQLInsert.dmksqlins??, ComplUpd?: String; Device: TDeviceType.st??, Sincro?False?
}

end.

