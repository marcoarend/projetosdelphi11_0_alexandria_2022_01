unit LoadSalesGenAtrel;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkDBLookupComboBox, dmkEditCB, ZAbstractRODataset, ZAbstractDataset,
  ZDataset, UnGrl_DmkDB;

type
  TFmLoadSalesGenAtrel = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrProdutosCabec: TZQuery;
    LargeintField1: TLargeintField;
    StringField1: TWideStringField;
    DsProdutosCabec: TDataSource;
    Label13: TLabel;
    EdProdutosCabec: TdmkEditCB;
    CBProdutosCabec: TdmkDBLookupComboBox;
    Label1: TLabel;
    EdProdutosCadasInt: TdmkEdit;
    EdProdutosCadasTxt: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
    procedure ReopenProdutosCabec();
  end;

  var
  FmLoadSalesGenAtrel: TFmLoadSalesGenAtrel;

implementation

uses UnMyObjects, Module, LoadSalesGen;

{$R *.DFM}

procedure TFmLoadSalesGenAtrel.BtOKClick(Sender: TObject);
var
  Codigo, ProdCabec: Integer;
  SQLType: TSQLType;
begin
  SQLType             := ImgTipo.SQLType;
  //
  ProdCabec           := EdProdutosCabec.ValueVariant;
  Codigo              := EdProdutosCadasInt.ValueVariant;
  //
  if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, 'produtoscadas', False, [
  'ProdCabec'], [
  'Codigo'], [
  ProdCabec], [
  Codigo], True, dmksqlinsInsOnly, '', TDeviceType.stDesktop, False) then
  begin
    Codigo := Grl_DmkDB.LocalizaPriorNextLargeIntQr(FmLoadSalesGen.QrProdutosCadas,
    FmLoadSalesGen.QrProdutosCadasCodigo, FmLoadSalesGen.QrProdutosCadasCodigo.Value);
    FmLoadSalesGen.ReopenProdutosCadas(Codigo);
    //
    Close;
  end;
end;

procedure TFmLoadSalesGenAtrel.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLoadSalesGenAtrel.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLoadSalesGenAtrel.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmLoadSalesGenAtrel.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLoadSalesGenAtrel.ReopenProdutosCabec();
begin
  Grl_DmkDB.AbreSQLQuery0(QrProdutosCabec, Dmod.MyDB, [
  'SELECT * ',
  'FROM produtoscabec  ',
  'ORDER BY Nome ',
  '']);
end;

end.
