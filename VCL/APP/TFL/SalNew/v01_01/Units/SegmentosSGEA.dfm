object FmSegmentosSGEA: TFmSegmentosSGEA
  Left = 339
  Top = 185
  Caption = 'ENT-CADAS-003 :: Cadastro de Cliente x Segmento x Account'
  ClientHeight = 455
  ClientWidth = 653
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 313
    Width = 653
    Height = 28
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitTop = 215
    ExplicitWidth = 617
    object CkContinuar: TCheckBox
      Left = 16
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 48
    Width = 653
    Height = 265
    Align = alTop
    Caption = ' Dados do item: '
    TabOrder = 0
    object Label6: TLabel
      Left = 12
      Top = 16
      Width = 14
      Height = 13
      Caption = 'ID:'
    end
    object Label7: TLabel
      Left = 180
      Top = 16
      Width = 61
      Height = 13
      Caption = 'Observa'#231#227'o:'
      Visible = False
    end
    object Label1: TLabel
      Left = 12
      Top = 96
      Width = 51
      Height = 13
      Caption = 'Segmento:'
    end
    object SBSegmento: TSpeedButton
      Left = 584
      Top = 112
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SBSegmentoClick
    end
    object Label4: TLabel
      Left = 12
      Top = 136
      Width = 66
      Height = 13
      Caption = 'Account aqui:'
    end
    object SBAccount: TSpeedButton
      Left = 584
      Top = 152
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SBSegmentoClick
    end
    object Label8: TLabel
      Left = 12
      Top = 176
      Width = 61
      Height = 13
      Caption = 'Account SN:'
    end
    object SBAccntSN: TSpeedButton
      Left = 584
      Top = 192
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SBSegmentoClick
    end
    object LaEntidadesEmp: TLabel
      Left = 12
      Top = 56
      Width = 35
      Height = 13
      Caption = 'Cliente:'
    end
    object SBEntidadesEmp: TSpeedButton
      Left = 584
      Top = 72
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SBSegmentoClick
    end
    object Label2: TLabel
      Left = 96
      Top = 16
      Width = 36
      Height = 13
      Caption = 'Codigo:'
    end
    object Label22: TLabel
      Left = 15
      Top = 216
      Width = 30
      Height = 13
      Caption = 'HSDB'
    end
    object EdCodigo: TdmkEdit
      Left = 12
      Top = 32
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdNome: TdmkEdit
      Left = 180
      Top = 32
      Width = 465
      Height = 21
      TabOrder = 2
      Visible = False
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Nome'
      UpdCampo = 'Nome'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object CBSegmento: TdmkDBLookupComboBox
      Left = 68
      Top = 112
      Width = 513
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsSegmentosCads
      TabOrder = 6
      dmkEditCB = EdSegmento
      QryCampo = 'Cargo'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdSegmento: TdmkEditCB
      Left = 12
      Top = 112
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Cargo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBSegmento
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object EdAccount: TdmkEditCB
      Left = 12
      Top = 152
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 7
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Cargo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBAccount
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBAccount: TdmkDBLookupComboBox
      Left = 68
      Top = 152
      Width = 513
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsEntidadesAccn
      TabOrder = 8
      dmkEditCB = EdAccount
      QryCampo = 'Cargo'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdAccntSN: TdmkEditCB
      Left = 12
      Top = 192
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 9
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Cargo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBAccntSN
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBAccntSN: TdmkDBLookupComboBox
      Left = 68
      Top = 192
      Width = 513
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsEntidadesAcSN
      TabOrder = 10
      dmkEditCB = EdAccntSN
      QryCampo = 'Cargo'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdEntidadesEmp: TdmkEditCB
      Left = 12
      Top = 72
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Cargo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBEntidadesEmp
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBEntidadesEmp: TdmkDBLookupComboBox
      Left = 68
      Top = 72
      Width = 513
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsEntidadesEmpr
      TabOrder = 4
      dmkEditCB = EdEntidadesEmp
      QryCampo = 'Cargo'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdCodUsu: TdmkEdit
      Left = 96
      Top = 32
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdHvSrDB: TdmkEditF7
      Left = 13
      Top = 232
      Width = 40
      Height = 21
      Alignment = taRightJustify
      TabOrder = 11
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'HvSrDB'
      UpdCampo = 'HvSrDB'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      LocF7TableName = 'GetHvSrDBCad'
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 653
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitWidth = 617
    object GB_R: TGroupBox
      Left = 605
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 569
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 557
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 521
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 519
        Height = 32
        Caption = 'Cadastro de Cliente x Segmento x Account'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 519
        Height = 32
        Caption = 'Cadastro de Cliente x Segmento x Account'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 519
        Height = 32
        Caption = 'Cadastro de Cliente x Segmento x Account'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 341
    Width = 653
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    ExplicitTop = 243
    ExplicitWidth = 617
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 649
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 613
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 385
    Width = 653
    Height = 70
    Align = alBottom
    TabOrder = 4
    ExplicitTop = 287
    ExplicitWidth = 617
    object PnSaiDesis: TPanel
      Left = 507
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 471
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 505
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 469
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrEntidadesEmpr: TZQuery
    Connection = DMod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM entidadesempr emp'
      'ORDER BY Nome')
    Params = <>
    Left = 176
    Top = 180
    object QrEntidadesEmprCodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrEntidadesEmprNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 255
    end
  end
  object DsEntidadesEmpr: TDataSource
    DataSet = QrEntidadesEmpr
    Left = 176
    Top = 228
  end
  object QrEntidadesAccn: TZQuery
    Connection = DMod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM entidadesaccn'
      'ORDER BY Nome')
    Params = <>
    Left = 376
    Top = 180
    object QrEntidadesAccnCodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrEntidadesAccnNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 255
    end
  end
  object DsEntidadesAccn: TDataSource
    DataSet = QrEntidadesAccn
    Left = 376
    Top = 228
  end
  object QrSegmentosCads: TZQuery
    Connection = DMod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM segmentoscads'
      'ORDER BY Nome')
    Params = <>
    Left = 276
    Top = 180
    object QrSegmentosCadsCodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrSegmentosCadsNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
  end
  object DsSegmentosCads: TDataSource
    DataSet = QrSegmentosCads
    Left = 276
    Top = 228
  end
  object QrEntidadesAcSN: TZQuery
    Connection = DMod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM entidadesaccn'
      'ORDER BY Nome')
    Params = <>
    Left = 492
    Top = 180
    object QrEntidadesAcSNCodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrEntidadesAcSNNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 255
    end
  end
  object DsEntidadesAcSN: TDataSource
    DataSet = QrEntidadesAcSN
    Left = 492
    Top = 228
  end
end
