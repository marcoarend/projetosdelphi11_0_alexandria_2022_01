unit SegmentosSGEA;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, DBCtrls, dmkLabel, Mask, dmkDBLookupComboBox,
  ZAbstractRODataset, ZDataset, ZAbstractDataset, dmkEdit, dmkEditCB, dmkDBEdit,
  dmkGeral, Variants, dmkValUsu, dmkImage, UnDmkEnums, dmkEditF7;

type
  TFmSegmentosSGEA = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox2: TGroupBox;
    EdCodigo: TdmkEdit;
    Label6: TLabel;
    EdNome: TdmkEdit;
    Label7: TLabel;
    CBSegmento: TdmkDBLookupComboBox;
    EdSegmento: TdmkEditCB;
    Label1: TLabel;
    SBSegmento: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    Label4: TLabel;
    EdAccount: TdmkEditCB;
    CBAccount: TdmkDBLookupComboBox;
    SBAccount: TSpeedButton;
    Label8: TLabel;
    EdAccntSN: TdmkEditCB;
    CBAccntSN: TdmkDBLookupComboBox;
    SBAccntSN: TSpeedButton;
    LaEntidadesEmp: TLabel;
    EdEntidadesEmp: TdmkEditCB;
    CBEntidadesEmp: TdmkDBLookupComboBox;
    SBEntidadesEmp: TSpeedButton;
    EdCodUsu: TdmkEdit;
    Label2: TLabel;
    QrEntidadesEmpr: TZQuery;
    DsEntidadesEmpr: TDataSource;
    QrEntidadesEmprCodigo: TLargeintField;
    QrEntidadesEmprNome: TWideStringField;
    QrEntidadesAccn: TZQuery;
    DsEntidadesAccn: TDataSource;
    QrSegmentosCads: TZQuery;
    DsSegmentosCads: TDataSource;
    QrSegmentosCadsCodigo: TLargeintField;
    QrSegmentosCadsNome: TWideStringField;
    QrEntidadesAccnCodigo: TLargeintField;
    QrEntidadesAccnNome: TWideStringField;
    QrEntidadesAcSN: TZQuery;
    DsEntidadesAcSN: TDataSource;
    QrEntidadesAcSNCodigo: TLargeintField;
    QrEntidadesAcSNNome: TWideStringField;
    EdHvSrDB: TdmkEditF7;
    Label22: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SBSegmentoClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenCadastro_Com_Itens_ITS(Codigo: Integer);
    procedure ReopenEntidadesEmpr();
    procedure ReopenEntidadesAccn();
    procedure ReopenEntidadesAcSN();
    procedure ReopenSegmentosCads();

  public
    { Public declarations }
    //FQrCab,
    FQrIts: TZQuery;
    //FDsCab: TDataSource;
  end;

  var
  FmSegmentosSGEA: TFmSegmentosSGEA;

implementation

uses UnMyObjects, Module, UnInternalConsts, DmkDAC_PF, UnGrl_DmkDB;

{$R *.DFM}

procedure TFmSegmentosSGEA.BtOKClick(Sender: TObject);
var
  Codigo, CodUsu, Segmento, EntidadesEmp, Account, AccntSN, HvSrDB: Integer;
  Nome: String;
  SQLType: TSQLType;
begin
  SQLType       := ImgTipo.SQLType;
  //
  Codigo        := EdCodigo.ValueVariant;
  CodUsu        := EdCodUsu.ValueVariant;
  Nome          := EdNome.ValueVariant;
  Segmento      := EdSegmento.ValueVariant;
  EntidadesEmp  := EdEntidadesEmp.ValueVariant;
  Account       := EdAccount.ValueVariant;
  AccntSN       := EdAccntSN.ValueVariant;
  HvSrDB        := EdHvSrDB.ValueVariant; //Integer(THaveSrcDB.hsdbCadastr);
  //
  Codigo := Grl_DmkDB.GetNxtCodigoInt('segmentossgea', 'Codigo', SQLType, Codigo);
  CodUsu := Grl_DmkDB.GetNxtCodigoInt('segmentossgea', 'CodUsu', SQLType, CodUsu);
  //
  if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, 'segmentossgea', False, [
  'CodUsu', 'Segmento', 'EntidadesEmp',
  'Account', 'AccntSN', 'HvSrDB',
  'Nome'], [
  'Codigo'], [
  CodUsu, Segmento, EntidadesEmp,
  Account, AccntSN, HvSrDB,
  Nome], [
  Codigo], True, dmksqlinsInsOnly, '', stDesktop, False) then
  begin
    ReopenCadastro_Com_Itens_ITS(Codigo);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType             := stIns;
      EdCodigo.ValueVariant       := 0;
      EdCodUsu.ValueVariant       := 0;
      EdEntidadesEmp.ValueVariant := 0;
      CBEntidadesEmp.KeyValue     := Null;
      EdSegmento.ValueVariant     := 0;
      CBSegmento.KeyValue         := Null;
      EdAccount.ValueVariant      := 0;
      CBAccount.KeyValue          := Null;
      EdAccntSN.ValueVariant      := 0;
      CBAccntSN.KeyValue          := Null;
      EdNome.ValueVariant         := '';
      if EdNome.Enabled then
        EdNome.SetFocus
      else
      if EdSegmento.Enabled then
        EdSegmento.SetFocus;
    end else
      Close;
  end;
end;

procedure TFmSegmentosSGEA.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSegmentosSGEA.FormActivate(Sender: TObject);
begin
{
  DBEdCodigo.DataSource := FDsCab;
  DBEdCodUso.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
}
  MyObjects.CorIniComponente();
end;

procedure TFmSegmentosSGEA.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  ReopenEntidadesEmpr();
  ReopenEntidadesAccn();
  ReopenEntidadesAcSN();
  ReopenSegmentosCads();
end;

procedure TFmSegmentosSGEA.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSegmentosSGEA.ReopenCadastro_Com_Itens_ITS(Codigo: Integer);
begin
  if FQrIts <> nil then
  begin
    Grl_DmkDB.AbreQuery(FQrIts, FQrIts.Connection);
    //
    if Codigo <> 0 then
      FQrIts.Locate('Codigo', Codigo, []);
  end;
end;

procedure TFmSegmentosSGEA.ReopenEntidadesAccn;
begin
  Grl_DmkDB.AbreSQLQuery0(QrEntidadesAccn, Dmod.MyDB, [
  'SELECT Codigo, Nome  ',
  'FROM entidadesaccn ',
  'ORDER BY Nome ',
  '']);
end;

procedure TFmSegmentosSGEA.ReopenEntidadesAcSN();
begin
  Grl_DmkDB.AbreSQLQuery0(QrEntidadesAcSN, Dmod.MyDB, [
  'SELECT Codigo, Nome  ',
  'FROM entidadesaccn ',
  'ORDER BY Nome ',
  '']);
end;

procedure TFmSegmentosSGEA.ReopenEntidadesEmpr();
begin
  Grl_DmkDB.AbreSQLQuery0(QrEntidadesEmpr, Dmod.MyDB, [
  'SELECT Codigo, Nome  ',
  'FROM entidadesempr ',
  'ORDER BY Nome ',
  '']);
end;

procedure TFmSegmentosSGEA.ReopenSegmentosCads();
begin
  Grl_DmkDB.AbreSQLQuery0(QrSegmentosCads, Dmod.MyDB, [
  'SELECT Codigo, Nome  ',
  'FROM segmentoscads ',
  'ORDER BY Nome ',
  '']);
end;

procedure TFmSegmentosSGEA.SBSegmentoClick(Sender: TObject);
begin
end;

{
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFm_CadSel_, Fm_CadSel_, afmoNegarComAviso) then
  begin
    Fm_CadSel_.ShowModal;
    Fm_CadSel_.Destroy;
  end;
  UMyMod.SetaCodigoPesquisado(EdSel, CBSel, QrSel, VAR_CADASTRO);
}

{
object GroupBox1: TGroupBox
  Left = 0
  Top = 48
  Width = 653
  Height = 64
  Align = alTop
  Caption = ' Dados do cabe'#231'alho:'
  Enabled = False
  TabOrder = 0
  ExplicitLeft = 4
  object Label5: TLabel
    Left = 12
    Top = 20
    Width = 14
    Height = 13
    Caption = 'ID:'
    FocusControl = DBEdCodigo
  end
  object Label3: TLabel
    Left = 132
    Top = 20
    Width = 51
    Height = 13
    Caption = 'Descri'#231#227'o:'
    FocusControl = DBEdNome
  end
  object Label2: TLabel
    Left = 72
    Top = 20
    Width = 36
    Height = 13
    Caption = 'C'#243'digo:'
    FocusControl = DBEdCodUso
  end
  object DBEdCodigo: TdmkDBEdit
    Left = 12
    Top = 36
    Width = 56
    Height = 21
    TabStop = False
    DataField = 'Codigo'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    ParentShowHint = False
    ReadOnly = True
    ShowHint = True
    TabOrder = 0
    UpdCampo = 'Codigo'
    UpdType = utYes
    Alignment = taRightJustify
  end
  object DBEdNome: TDBEdit
    Left = 132
    Top = 36
    Width = 473
    Height = 21
    TabStop = False
    Color = clWhite
    DataField = 'Nome'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
  end
  object DBEdCodUso: TdmkDBEdit
    Left = 72
    Top = 36
    Width = 56
    Height = 21
    TabStop = False
    DataField = 'CodUsu'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    ParentShowHint = False
    ReadOnly = True
    ShowHint = True
    TabOrder = 2
    UpdCampo = 'Codigo'
    UpdType = utYes
    Alignment = taRightJustify
  end
end
}
end.
