object FmProdutosCadas: TFmProdutosCadas
  Left = 368
  Top = 194
  Caption = 'PRD-CADAS-001 :: Cadastro de Produtos'
  ClientHeight = 546
  ClientWidth = 784
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 450
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 157
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 8
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label4: TLabel
        Left = 136
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label2: TLabel
        Left = 68
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodUsu
      end
      object Label3: TLabel
        Left = 424
        Top = 16
        Width = 90
        Height = 13
        Caption = 'Produto refer'#234'ncia:'
        FocusControl = DBEdit1
      end
      object Label14: TLabel
        Left = 676
        Top = 16
        Width = 33
        Height = 13
        Caption = 'HSDB:'
        FocusControl = DBEdit10
      end
      object Label5: TLabel
        Left = 8
        Top = 56
        Width = 101
        Height = 13
        Caption = 'Grupo de segmentos:'
        FocusControl = DBEdit1
      end
      object Label6: TLabel
        Left = 328
        Top = 56
        Width = 51
        Height = 13
        Caption = 'Segmento:'
        FocusControl = DBEdit1
      end
      object Label8: TLabel
        Left = 8
        Top = 96
        Width = 97
        Height = 13
        Caption = 'Divis'#227'o de produtos:'
        FocusControl = DBEdit1
      end
      object Label10: TLabel
        Left = 396
        Top = 96
        Width = 117
        Height = 13
        Caption = 'Sub-divis'#227'o de produtos:'
        FocusControl = DBEdit1
      end
      object Label12: TLabel
        Left = 420
        Top = 56
        Width = 99
        Height = 13
        Caption = 'Fam'#237'lia de produtos::'
        FocusControl = DBEdit1
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 8
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsProdutosCadas
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 136
        Top = 32
        Width = 285
        Height = 21
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsProdutosCadas
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdCodUsu: TdmkDBEdit
        Left = 68
        Top = 32
        Width = 64
        Height = 21
        TabStop = False
        DataField = 'CodUsu'
        DataSource = DsProdutosCadas
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 2
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit1: TDBEdit
        Left = 424
        Top = 32
        Width = 56
        Height = 21
        DataField = 'ProdCabec'
        DataSource = DsProdutosCadas
        TabOrder = 3
      end
      object DBEdit2: TDBEdit
        Left = 480
        Top = 32
        Width = 192
        Height = 21
        DataField = 'NO_ProdCabec'
        DataSource = DsProdutosCadas
        TabOrder = 4
      end
      object DBEdit10: TDBEdit
        Left = 676
        Top = 32
        Width = 21
        Height = 21
        DataField = 'HvSrDB'
        DataSource = DsProdutosCadas
        TabOrder = 5
      end
      object DBEdit11: TDBEdit
        Left = 700
        Top = 32
        Width = 77
        Height = 21
        DataField = 'NO_HvSrDB'
        DataSource = DsProdutosCadas
        TabOrder = 6
      end
      object DBEdit3: TDBEdit
        Left = 8
        Top = 72
        Width = 32
        Height = 21
        DataField = 'GrupoSegmento'
        DataSource = DsProdutosCadas
        TabOrder = 7
      end
      object DBEdit4: TDBEdit
        Left = 40
        Top = 72
        Width = 285
        Height = 21
        DataField = 'NO_GrupoSegmento'
        DataSource = DsProdutosCadas
        TabOrder = 8
      end
      object DBEdit5: TDBEdit
        Left = 360
        Top = 72
        Width = 57
        Height = 21
        DataField = 'NO_Segmento'
        DataSource = DsProdutosCadas
        TabOrder = 9
      end
      object DBEdit6: TDBEdit
        Left = 328
        Top = 72
        Width = 32
        Height = 21
        DataField = 'Segmento'
        DataSource = DsProdutosCadas
        TabOrder = 10
      end
      object DBEdit7: TDBEdit
        Left = 40
        Top = 112
        Width = 352
        Height = 21
        DataField = 'NO_Divisao'
        DataSource = DsProdutosCadas
        TabOrder = 11
      end
      object DBEdit8: TDBEdit
        Left = 8
        Top = 112
        Width = 32
        Height = 21
        DataField = 'Divisao'
        DataSource = DsProdutosCadas
        TabOrder = 12
      end
      object DBEdit9: TDBEdit
        Left = 428
        Top = 112
        Width = 348
        Height = 21
        DataField = 'NO_SubDivisao'
        DataSource = DsProdutosCadas
        TabOrder = 13
      end
      object DBEdit12: TDBEdit
        Left = 396
        Top = 112
        Width = 32
        Height = 21
        DataField = 'SubDivisao'
        DataSource = DsProdutosCadas
        TabOrder = 14
      end
      object DBEdit13: TDBEdit
        Left = 420
        Top = 72
        Width = 32
        Height = 21
        DataField = 'Familia'
        DataSource = DsProdutosCadas
        TabOrder = 15
      end
      object DBEdit14: TDBEdit
        Left = 452
        Top = 72
        Width = 325
        Height = 21
        DataField = 'NO_Familia'
        DataSource = DsProdutosCadas
        TabOrder = 16
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 386
      Width = 784
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 87
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 261
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 548
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Produto'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 311
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Segmento'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
      end
    end
    object DGDados: TdmkDBGridZTO
      Left = 0
      Top = 157
      Width = 784
      Height = 148
      Align = alTop
      DataSource = DsSegmentosSGEA
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      RowColors = <>
      Columns = <
        item
          Expanded = False
          FieldName = 'NO_Segmento'
          Title.Caption = 'Segmento'
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_Accn_QT'
          Title.Caption = 'Account aqui'
          Width = 120
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_Accn_SN'
          Title.Caption = 'Account importado'
          Width = 120
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_HvSrDB'
          Title.Caption = 'HSDB'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Title.Caption = 'Observa'#231#227'o'
          Width = 384
          Visible = True
        end>
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 450
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 265
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 8
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 136
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label15: TLabel
        Left = 308
        Top = 56
        Width = 90
        Height = 13
        Caption = 'Produto refer'#234'ncia:'
        FocusControl = DBEdit1
      end
      object Label22: TLabel
        Left = 736
        Top = 16
        Width = 30
        Height = 13
        Caption = 'HSDB'
        FocusControl = DBEdit10
      end
      object Label24: TLabel
        Left = 68
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label25: TLabel
        Left = 8
        Top = 96
        Width = 101
        Height = 13
        Caption = 'Grupo de segmentos:'
        FocusControl = DBEdit1
      end
      object Label26: TLabel
        Left = 328
        Top = 96
        Width = 51
        Height = 13
        Caption = 'Segmento:'
        FocusControl = DBEdit1
      end
      object Label27: TLabel
        Left = 8
        Top = 136
        Width = 97
        Height = 13
        Caption = 'Divis'#227'o de produtos:'
        FocusControl = DBEdit1
      end
      object Label28: TLabel
        Left = 396
        Top = 136
        Width = 117
        Height = 13
        Caption = 'Sub-divis'#227'o de produtos:'
        FocusControl = DBEdit1
      end
      object Label11: TLabel
        Left = 440
        Top = 96
        Width = 96
        Height = 13
        Caption = 'Fam'#237'lia de produtos:'
        FocusControl = DBEdit1
      end
      object Label13: TLabel
        Left = 8
        Top = 56
        Width = 123
        Height = 13
        Caption = 'Tabela de item de receita:'
        FocusControl = DBEdit1
      end
      object EdCodigo: TdmkEdit
        Left = 8
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 136
        Top = 32
        Width = 573
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCodUsu: TdmkEdit
        Left = 68
        Top = 32
        Width = 64
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CodUsu'
        UpdCampo = 'CodUsu'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdProdCabec: TdmkEditCB
        Left = 308
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfLongint
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ProdCabec'
        UpdCampo = 'ProdCabec'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBProdCabec
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBProdCabec: TdmkDBLookupComboBox
        Left = 364
        Top = 72
        Width = 413
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsProdutosCabec
        TabOrder = 4
        dmkEditCB = EdProdCabec
        QryCampo = 'ProdCabec'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdHvSrDB: TdmkEditF7
        Left = 736
        Top = 32
        Width = 40
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'HvSrDB'
        UpdCampo = 'HvSrDB'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        LocF7TableName = 'GetHvSrDBCad'
      end
      object EdGrupoSegmento: TdmkEditCB
        Left = 8
        Top = 112
        Width = 32
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfLongint
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'GrupoSegmento'
        UpdCampo = 'GrupoSegmento'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBGrupoSegmento
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBGrupoSegmento: TdmkDBLookupComboBox
        Left = 40
        Top = 112
        Width = 281
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsSegmenGruCads
        TabOrder = 7
        dmkEditCB = EdGrupoSegmento
        QryCampo = 'GrupoSegmento'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdSegmento: TdmkEditCB
        Left = 328
        Top = 112
        Width = 32
        Height = 21
        Alignment = taRightJustify
        TabOrder = 8
        FormatType = dmktfLongint
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Segmento'
        UpdCampo = 'Segmento'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBSegmento
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBSegmento: TdmkDBLookupComboBox
        Left = 360
        Top = 112
        Width = 73
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsSegmentosCads
        TabOrder = 9
        dmkEditCB = EdSegmento
        QryCampo = 'Segmento'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdDivisao: TdmkEditCB
        Left = 8
        Top = 152
        Width = 32
        Height = 21
        Alignment = taRightJustify
        TabOrder = 12
        FormatType = dmktfLongint
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Divisao'
        UpdCampo = 'Divisao'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBDivisao
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBDivisao: TdmkDBLookupComboBox
        Left = 40
        Top = 152
        Width = 348
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsProdDivsaoCab
        TabOrder = 13
        dmkEditCB = EdDivisao
        QryCampo = 'Divisao'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdSubDivisao: TdmkEditCB
        Left = 396
        Top = 152
        Width = 32
        Height = 21
        Alignment = taRightJustify
        TabOrder = 14
        FormatType = dmktfLongint
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'SubDivisao'
        UpdCampo = 'SubDivisao'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBSubDivisao
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBSubDivisao: TdmkDBLookupComboBox
        Left = 428
        Top = 152
        Width = 348
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsProdDivSubCab
        TabOrder = 15
        dmkEditCB = EdSubDivisao
        QryCampo = 'SubDivisao'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdFamilia: TdmkEditCB
        Left = 440
        Top = 112
        Width = 32
        Height = 21
        Alignment = taRightJustify
        TabOrder = 10
        FormatType = dmktfLongint
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Familia'
        UpdCampo = 'Familia'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBFamilia
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBFamilia: TdmkDBLookupComboBox
        Left = 472
        Top = 112
        Width = 304
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsProdutosFamil
        TabOrder = 11
        dmkEditCB = EdFamilia
        QryCampo = 'Familia'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdLista: TdmkEditCB
        Left = 8
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 16
        FormatType = dmktfLongint
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ProdCabec'
        UpdCampo = 'ProdCabec'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBLista
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBLista: TdmkDBLookupComboBox
        Left = 64
        Top = 72
        Width = 241
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsProdutosTabls
        TabOrder = 17
        dmkEditCB = EdLista
        QryCampo = 'ProdCabec'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 387
      Width = 784
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 644
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 520
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 265
        Height = 32
        Caption = 'Cadastro de Produtos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 265
        Height = 32
        Caption = 'Cadastro de Produtos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 265
        Height = 32
        Caption = 'Cadastro de Produtos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 784
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 424
    Top = 376
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 300
    Top = 372
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object QrProdutosCadas: TZQuery
    Connection = DMod.MyDB
    BeforeClose = QrProdutosCadasBeforeClose
    AfterScroll = QrProdutosCadasAfterScroll
    SQL.Strings = (
      'SELECT prd.*, prc.Nome NO_ProdCabec,'
      'sgr.Nome NO_GrupoSegmento, sca.Nome NO_Segmento,'
      'pdv.Nome NO_Divisao, psd.Nome NO_SubDivisao,'
      'prf.Nome NO_Familia'
      'FROM produtoscadas prd '
      'LEFT JOIN segmengrucads sgr ON sgr.Codigo=prd.GrupoSegmento'
      'LEFT JOIN segmentoscads sca ON sca.Codigo=prd.Segmento'
      'LEFT JOIN proddivsaocab pdv ON pdv.Codigo=prd.Divisao'
      'LEFT JOIN proddivsubcab psd ON psd.Codigo=prd.SubDivisao'
      'LEFT JOIN produtosfamil prf ON prf.Codigo=prd.Familia'
      'LEFT JOIN produtoscabec prc ON prc.Codigo=prd.ProdCabec'
      'WHERE prd.ProdCabec>=0')
    Params = <>
    Left = 120
    Top = 312
    object QrProdutosCadasCodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrProdutosCadasCodUsu: TLargeintField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrProdutosCadasProdCabec: TLargeintField
      FieldName = 'ProdCabec'
      Required = True
    end
    object QrProdutosCadasGrupoSegmento: TLargeintField
      FieldName = 'GrupoSegmento'
      Required = True
    end
    object QrProdutosCadasSegmento: TLargeintField
      FieldName = 'Segmento'
      Required = True
    end
    object QrProdutosCadasDivisao: TLargeintField
      FieldName = 'Divisao'
      Required = True
    end
    object QrProdutosCadasSubDivisao: TLargeintField
      FieldName = 'SubDivisao'
      Required = True
    end
    object QrProdutosCadasFamilia: TLargeintField
      FieldName = 'Familia'
      Required = True
    end
    object QrProdutosCadasNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrProdutosCadasHvSrDB: TSmallintField
      FieldName = 'HvSrDB'
      Required = True
    end
    object QrProdutosCadasLk: TLargeintField
      FieldName = 'Lk'
    end
    object QrProdutosCadasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrProdutosCadasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrProdutosCadasUserCad: TLargeintField
      FieldName = 'UserCad'
    end
    object QrProdutosCadasUserAlt: TLargeintField
      FieldName = 'UserAlt'
    end
    object QrProdutosCadasAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrProdutosCadasAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrProdutosCadasNO_GrupoSegmento: TWideStringField
      FieldName = 'NO_GrupoSegmento'
      Required = True
      Size = 60
    end
    object QrProdutosCadasNO_Segmento: TWideStringField
      FieldName = 'NO_Segmento'
      Required = True
      Size = 60
    end
    object QrProdutosCadasNO_Divisao: TWideStringField
      FieldName = 'NO_Divisao'
      Required = True
      Size = 60
    end
    object QrProdutosCadasNO_SubDivisao: TWideStringField
      FieldName = 'NO_SubDivisao'
      Required = True
      Size = 60
    end
    object QrProdutosCadasNO_Familia: TWideStringField
      FieldName = 'NO_Familia'
      Required = True
      Size = 60
    end
    object QrProdutosCadasNO_HvSrDB: TWideMemoField
      FieldName = 'NO_HvSrDB'
      OnGetText = QrProdutosCadasNO_HvSrDBGetText
      BlobType = ftWideMemo
      Size = 20
    end
    object QrProdutosCadasNO_ProdCabec: TWideStringField
      FieldName = 'NO_ProdCabec'
      Required = True
      Size = 60
    end
  end
  object DsProdutosCadas: TDataSource
    DataSet = QrProdutosCadas
    Left = 120
    Top = 356
  end
  object QrSegmentosSGEA: TZQuery
    Connection = DMod.MyDB
    SQL.Strings = (
      'SELECT segc.Nome NO_Segmento, /*empr.Nome NO_Cliente,*/  '
      'acqt.Nome NO_Accn_QT, acsn.Nome NO_Accn_SN, sgea.*  '
      'FROM segmentossgea sgea '
      'LEFT JOIN segmentoscads segc ON segc.Codigo=sgea.Segmento '
      
        '/*LEFT JOIN entidadesempr empr ON empr.Codigo=sgea.EntidadesEmp*' +
        '/ '
      'LEFT JOIN entidadesaccn acqt ON acqt.Codigo=sgea.Account '
      'LEFT JOIN entidadesaccn acsn ON acsn.Codigo=sgea.AccntSN ')
    Params = <>
    Left = 252
    Top = 312
    object QrSegmentosSGEANO_Segmento: TWideStringField
      FieldName = 'NO_Segmento'
      Required = True
      Size = 60
    end
    object QrSegmentosSGEANO_Accn_QT: TWideStringField
      FieldName = 'NO_Accn_QT'
      Required = True
      Size = 255
    end
    object QrSegmentosSGEANO_Accn_SN: TWideStringField
      FieldName = 'NO_Accn_SN'
      Required = True
      Size = 255
    end
    object QrSegmentosSGEACodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrSegmentosSGEACodUsu: TLargeintField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrSegmentosSGEASegmento: TLargeintField
      FieldName = 'Segmento'
      Required = True
    end
    object QrSegmentosSGEAEntidadesEmp: TLargeintField
      FieldName = 'EntidadesEmp'
      Required = True
    end
    object QrSegmentosSGEAAccount: TLargeintField
      FieldName = 'Account'
      Required = True
    end
    object QrSegmentosSGEAAccntSN: TLargeintField
      FieldName = 'AccntSN'
      Required = True
    end
    object QrSegmentosSGEAHvSrDB: TSmallintField
      FieldName = 'HvSrDB'
      Required = True
    end
    object QrSegmentosSGEALk: TLargeintField
      FieldName = 'Lk'
    end
    object QrSegmentosSGEADataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrSegmentosSGEADataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrSegmentosSGEAUserCad: TLargeintField
      FieldName = 'UserCad'
    end
    object QrSegmentosSGEAUserAlt: TLargeintField
      FieldName = 'UserAlt'
    end
    object QrSegmentosSGEAAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrSegmentosSGEAAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrSegmentosSGEANome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrSegmentosSGEANO_HvSrDB: TWideMemoField
      FieldName = 'NO_HvSrDB'
      OnGetText = QrSegmentosSGEANO_HvSrDBGetText
      BlobType = ftWideMemo
      Size = 120
    end
  end
  object DsSegmentosSGEA: TDataSource
    DataSet = QrSegmentosSGEA
    Left = 252
    Top = 360
  end
  object QrProdutosCabec: TZQuery
    Connection = DMod.MyDB
    SQL.Strings = (
      'SELECT * FROM produtoscabec')
    Params = <>
    Left = 184
    Top = 244
    object QrProdutosCabecCodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrProdutosCabecNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
  end
  object DsProdutosCabec: TDataSource
    DataSet = QrProdutosCabec
    Left = 184
    Top = 288
  end
  object QrProdutosFamil: TZQuery
    Connection = DMod.MyDB
    SQL.Strings = (
      'SELECT * FROM produtosfamil')
    Params = <>
    Left = 340
    Top = 200
    object QrProdutosFamilCodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrProdutosFamilNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
  end
  object DsProdutosFamil: TDataSource
    DataSet = QrProdutosFamil
    Left = 340
    Top = 244
  end
  object QrSegmenGruCads: TZQuery
    Connection = DMod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM segmengrucads'
      'ORDERVBY Nome'
      '')
    Params = <>
    Left = 432
    Top = 196
    object QrSegmenGruCadsCodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrSegmenGruCadsNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
  end
  object DsSegmenGruCads: TDataSource
    DataSet = QrSegmenGruCads
    Left = 436
    Top = 240
  end
  object QrSegmentosCads: TZQuery
    Connection = DMod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM segmentoscads'
      'ORDERVBY Nome'
      '')
    Params = <>
    Left = 536
    Top = 200
    object QrSegmentosCadsCodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrSegmentosCadsNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
  end
  object DsSegmentosCads: TDataSource
    DataSet = QrSegmentosCads
    Left = 536
    Top = 244
  end
  object QrProdDivsaoCab: TZQuery
    Connection = DMod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM proddivsaocab'
      'ORDERVBY Nome'
      '')
    Params = <>
    Left = 632
    Top = 200
    object QrProdDivsaoCabCodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrProdDivsaoCabNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
  end
  object DsProdDivsaoCab: TDataSource
    DataSet = QrProdDivsaoCab
    Left = 632
    Top = 244
  end
  object QrProdDivSubCab: TZQuery
    Connection = DMod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM proddivsubcab'
      'ORDERVBY Nome'
      '')
    Params = <>
    Left = 720
    Top = 200
    object QrProdDivSubCabCodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrProdDivSubCabNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
  end
  object DsProdDivSubCab: TDataSource
    DataSet = QrProdDivSubCab
    Left = 720
    Top = 244
  end
  object QrProdutosTabls: TZQuery
    Connection = DMod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM produtostabls'
      'WHERE Codigo IN (0,1,2,3,4,5)'
      'ORDER BY Nome')
    Params = <>
    Left = 364
    Top = 304
    object QrProdutosTablsCodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrProdutosTablsNome: TWideStringField
      FieldName = 'Nome'
      Required = True
    end
  end
  object DsProdutosTabls: TDataSource
    DataSet = QrProdutosTabls
    Left = 364
    Top = 348
  end
end
