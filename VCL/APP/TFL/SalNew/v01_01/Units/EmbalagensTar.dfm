object FmEmbalagensTar: TFmEmbalagensTar
  Left = 368
  Top = 194
  Caption = 'PRD-CADAS-008 :: Cadastro de Taras de Embalagem'
  ClientHeight = 451
  ClientWidth = 784
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 355
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 101
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 8
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label3: TLabel
        Left = 572
        Top = 16
        Width = 200
        Height = 13
        Caption = 'Sigla (Sigla embalagem + unidade + peso):'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 136
        Top = 16
        Width = 31
        Height = 13
        Caption = 'Nome:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label24: TLabel
        Left = 68
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label11: TLabel
        Left = 292
        Top = 56
        Width = 72
        Height = 13
        Caption = 'Itens por pallet:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label12: TLabel
        Left = 376
        Top = 56
        Width = 77
        Height = 13
        Caption = 'Peso l'#237'quido kg:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label5: TLabel
        Left = 688
        Top = 56
        Width = 34
        Height = 13
        Caption = 'Ordem:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label22: TLabel
        Left = 736
        Top = 56
        Width = 33
        Height = 13
        Caption = 'HSDB:'
      end
      object Label15: TLabel
        Left = 480
        Top = 56
        Width = 67
        Height = 13
        Caption = 'Peso Tara kg:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label16: TLabel
        Left = 584
        Top = 56
        Width = 85
        Height = 13
        Caption = 'Kg l'#237'q. total pallet:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label25: TLabel
        Left = 8
        Top = 56
        Width = 58
        Height = 13
        Caption = 'Embalagem:'
        FocusControl = DBEdit1
      end
      object EdCodigo: TdmkEdit
        Left = 8
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 136
        Top = 32
        Width = 433
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdSigla: TdmkEdit
        Left = 572
        Top = 32
        Width = 205
        Height = 21
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Sigla'
        UpdCampo = 'Sigla'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCodUsu: TdmkEdit
        Left = 68
        Top = 32
        Width = 64
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CodUsu'
        UpdCampo = 'CodUsu'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdItmsInPalt: TdmkEdit
        Left = 292
        Top = 72
        Width = 81
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ItmsInPalt'
        UpdCampo = 'ItmsInPalt'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdPesoLiq: TdmkEdit
        Left = 376
        Top = 72
        Width = 101
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        QryCampo = 'PesoLiq'
        UpdCampo = 'PesoLiq'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdOrdem: TdmkEdit
        Left = 688
        Top = 72
        Width = 44
        Height = 21
        Alignment = taRightJustify
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Ordem'
        UpdCampo = 'Ordem'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdHvSrDB: TdmkEditF7
        Left = 736
        Top = 72
        Width = 40
        Height = 21
        Alignment = taRightJustify
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'HvSrDB'
        UpdCampo = 'HvSrDB'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        LocF7TableName = 'GetHvSrDBCad'
      end
      object EdPesoTar: TdmkEdit
        Left = 480
        Top = 72
        Width = 101
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        QryCampo = 'PesoTar'
        UpdCampo = 'PesoTar'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdPPallet: TdmkEdit
        Left = 584
        Top = 72
        Width = 101
        Height = 21
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        QryCampo = 'PPallet'
        UpdCampo = 'PPallet'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdEmbalagensCad: TdmkEditCB
        Left = 8
        Top = 72
        Width = 48
        Height = 21
        Alignment = taRightJustify
        TabOrder = 10
        FormatType = dmktfLongint
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'EmbalagensCad'
        UpdCampo = 'Familia'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmbalagensCad
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmbalagensCad: TdmkDBLookupComboBox
        Left = 56
        Top = 72
        Width = 233
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'Sigla'
        ListSource = DsEmbalagensCad
        TabOrder = 11
        dmkEditCB = EdEmbalagensCad
        QryCampo = 'EmbalagensCad'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 292
      Width = 784
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 644
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 355
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 101
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 60
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodUsu
      end
      object Label8: TLabel
        Left = 8
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 556
        Top = 16
        Width = 200
        Height = 13
        Caption = 'Sigla (Sigla embalagem + unidade + peso):'
        Color = clBtnFace
        ParentColor = False
      end
      object Label4: TLabel
        Left = 232
        Top = 56
        Width = 72
        Height = 13
        Caption = 'Itens por pallet:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label6: TLabel
        Left = 336
        Top = 56
        Width = 77
        Height = 13
        Caption = 'Peso l'#237'quido kg:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label10: TLabel
        Left = 440
        Top = 56
        Width = 67
        Height = 13
        Caption = 'Peso Tara kg:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label13: TLabel
        Left = 544
        Top = 56
        Width = 85
        Height = 13
        Caption = 'Kg l'#237'q. total pallet:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label14: TLabel
        Left = 644
        Top = 56
        Width = 34
        Height = 13
        Caption = 'Ordem:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label17: TLabel
        Left = 684
        Top = 56
        Width = 33
        Height = 13
        Caption = 'HSDB:'
      end
      object Label18: TLabel
        Left = 124
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label19: TLabel
        Left = 8
        Top = 56
        Width = 58
        Height = 13
        Caption = 'Embalagem:'
        Color = clBtnFace
        ParentColor = False
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 8
        Top = 32
        Width = 48
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsEmbalagensTar
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 556
        Top = 32
        Width = 221
        Height = 21
        Color = clWhite
        DataField = 'Sigla'
        DataSource = DsEmbalagensTar
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit1: TdmkDBEdit
        Left = 128
        Top = 32
        Width = 425
        Height = 21
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsEmbalagensTar
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdCodUsu: TdmkDBEdit
        Left = 60
        Top = 32
        Width = 60
        Height = 21
        TabStop = False
        DataField = 'CodUsu'
        DataSource = DsEmbalagensTar
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 3
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit2: TDBEdit
        Left = 684
        Top = 72
        Width = 36
        Height = 21
        DataField = 'HvSrDB'
        DataSource = DsEmbalagensTar
        TabOrder = 4
      end
      object DBEdit3: TDBEdit
        Left = 720
        Top = 72
        Width = 56
        Height = 21
        DataField = 'NO_HvSrDB'
        DataSource = DsEmbalagensTar
        TabOrder = 5
      end
      object DBEdit4: TDBEdit
        Left = 644
        Top = 72
        Width = 37
        Height = 21
        DataField = 'Ordem'
        DataSource = DsEmbalagensTar
        TabOrder = 6
      end
      object dmkDBEdit2: TdmkDBEdit
        Left = 232
        Top = 72
        Width = 101
        Height = 21
        Color = clWhite
        DataField = 'ItmsInPalt'
        DataSource = DsEmbalagensTar
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 7
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdit1: TDBEdit
        Left = 336
        Top = 72
        Width = 100
        Height = 21
        DataField = 'PesoLiq'
        DataSource = DsEmbalagensTar
        TabOrder = 8
      end
      object DBEdit5: TDBEdit
        Left = 440
        Top = 72
        Width = 100
        Height = 21
        DataField = 'PesoTar'
        DataSource = DsEmbalagensTar
        TabOrder = 9
      end
      object DBEdit6: TDBEdit
        Left = 544
        Top = 72
        Width = 96
        Height = 21
        DataField = 'PPallet'
        DataSource = DsEmbalagensTar
        TabOrder = 10
      end
      object dmkDBEdit3: TdmkDBEdit
        Left = 8
        Top = 72
        Width = 37
        Height = 21
        Color = clWhite
        DataField = 'EmbalagensCad'
        DataSource = DsEmbalagensTar
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 11
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit4: TdmkDBEdit
        Left = 44
        Top = 72
        Width = 185
        Height = 21
        Color = clWhite
        DataField = 'NO_EmbalagensCad'
        DataSource = DsEmbalagensTar
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 12
        UpdType = utYes
        Alignment = taLeftJustify
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 291
      Width = 784
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 87
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 261
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 521
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Embalagem'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 110
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&???'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          Visible = False
          OnClick = BtItsClick
        end
      end
    end
    object DGDados: TDBGrid
      Left = 0
      Top = 101
      Width = 784
      Height = 148
      Align = alTop
      DataSource = DsProdutosEmbal
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'CodProd'
          Title.Caption = 'ID SAP'#174
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ProdutosCadas'
          Title.Caption = 'ID produto'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_ProdutosCadas'
          Title.Caption = 'Nome Produto'
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 520
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 412
        Height = 32
        Caption = 'Cadastro de Taras de Embalagem'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 412
        Height = 32
        Caption = 'Cadastro de Taras de Embalagem'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 412
        Height = 32
        Caption = 'Cadastro de Taras de Embalagem'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 784
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 424
    Top = 376
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 300
    Top = 372
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object QrEmbalagensTar: TZQuery
    Connection = DMod.MyDB
    BeforeClose = QrEmbalagensTarBeforeClose
    AfterScroll = QrEmbalagensTarAfterScroll
    SQL.Strings = (
      'SELECT '
      
        ' CASE sca.HvSrDB WHEN 0 THEN "Indefinido"  WHEN 3 THEN "Importad' +
        'o"  WHEN 6 THEN "Cadastrado"  WHEN 9 THEN "Deletar"  ELSE "?" EN' +
        'D NO_HvSrDB, emb.Nome NO_EmbalagensCad, sca.*'
      'FROM embalagenstar sca'
      'LEFT JOIN embalagenscad emb ON emb.Codigo=sca.EmbalagensCad'
      'WHERE sca.Codigo > 0'
      'AND sca.Codigo=:P0')
    Params = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    Left = 188
    Top = 176
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmbalagensTarNO_HvSrDB: TWideMemoField
      FieldName = 'NO_HvSrDB'
      OnGetText = QrEmbalagensTarNO_HvSrDBGetText
      BlobType = ftWideMemo
      Size = 20
    end
    object QrEmbalagensTarNO_EmbalagensCad: TWideStringField
      FieldName = 'NO_EmbalagensCad'
      Required = True
      Size = 120
    end
    object QrEmbalagensTarCodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrEmbalagensTarCodUsu: TLargeintField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrEmbalagensTarNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 120
    end
    object QrEmbalagensTarEmbalagensCad: TLargeintField
      FieldName = 'EmbalagensCad'
      Required = True
    end
    object QrEmbalagensTarSigla: TWideStringField
      FieldName = 'Sigla'
      Required = True
      Size = 60
    end
    object QrEmbalagensTarPesoLiq: TFloatField
      FieldName = 'PesoLiq'
      Required = True
    end
    object QrEmbalagensTarPesoTar: TFloatField
      FieldName = 'PesoTar'
      Required = True
    end
    object QrEmbalagensTarPPallet: TFloatField
      FieldName = 'PPallet'
      Required = True
    end
    object QrEmbalagensTarItmsInPalt: TLargeintField
      FieldName = 'ItmsInPalt'
      Required = True
    end
    object QrEmbalagensTarOrdem: TLargeintField
      FieldName = 'Ordem'
      Required = True
    end
    object QrEmbalagensTarHvSrDB: TSmallintField
      FieldName = 'HvSrDB'
      Required = True
    end
    object QrEmbalagensTarLk: TLargeintField
      FieldName = 'Lk'
    end
    object QrEmbalagensTarDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEmbalagensTarDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEmbalagensTarUserCad: TLargeintField
      FieldName = 'UserCad'
    end
    object QrEmbalagensTarUserAlt: TLargeintField
      FieldName = 'UserAlt'
    end
    object QrEmbalagensTarAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrEmbalagensTarAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object DsEmbalagensTar: TDataSource
    DataSet = QrEmbalagensTar
    Left = 188
    Top = 224
  end
  object QrProdutosEmbal: TZQuery
    Connection = DMod.MyDB
    SQL.Strings = (
      'SELECT tar.Nome NO_EmbalagensTar, '
      'pca.Nome NO_ProdutosCadas, pem.* '
      'FROM produtosembal pem'
      'LEFT JOIN produtoscadas pca ON pca.Codigo=pem.ProdutosCadas'
      'LEFT JOIN embalagenstar tar ON tar.Codigo=pem.EmbalagensTar'
      'WHERE pem.EmbalagensTar=2'
      'ORDER BY NO_ProdutosCadas, CodProd')
    Params = <>
    Left = 320
    Top = 180
    object QrProdutosEmbalNO_EmbalagensTar: TWideStringField
      FieldName = 'NO_EmbalagensTar'
      Required = True
      Size = 120
    end
    object QrProdutosEmbalNO_ProdutosCadas: TWideStringField
      FieldName = 'NO_ProdutosCadas'
      Required = True
      Size = 100
    end
    object QrProdutosEmbalCodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrProdutosEmbalCodUsu: TLargeintField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrProdutosEmbalCodProd: TLargeintField
      FieldName = 'CodProd'
      Required = True
    end
    object QrProdutosEmbalProdutosCadas: TLargeintField
      FieldName = 'ProdutosCadas'
      Required = True
    end
    object QrProdutosEmbalEmbalagensTar: TLargeintField
      FieldName = 'EmbalagensTar'
      Required = True
    end
    object QrProdutosEmbalNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
    object QrProdutosEmbalHvSrDB: TSmallintField
      FieldName = 'HvSrDB'
      Required = True
    end
    object QrProdutosEmbalLk: TLargeintField
      FieldName = 'Lk'
    end
    object QrProdutosEmbalDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrProdutosEmbalDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrProdutosEmbalUserCad: TLargeintField
      FieldName = 'UserCad'
    end
    object QrProdutosEmbalUserAlt: TLargeintField
      FieldName = 'UserAlt'
    end
    object QrProdutosEmbalAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrProdutosEmbalAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object DsProdutosEmbal: TDataSource
    DataSet = QrProdutosEmbal
    Left = 320
    Top = 228
  end
  object QrEmbalagensCad: TZQuery
    Connection = DMod.MyDB
    SQL.Strings = (
      'SELECT * FROM embalagenscad')
    Params = <>
    Left = 456
    Top = 244
    object QrEmbalagensCadCodigo: TLargeintField
      FieldName = 'Codigo'
      LookupDataSet = DMod.QrAux
    end
    object QrEmbalagensCadSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 60
    end
  end
  object DsEmbalagensCad: TDataSource
    DataSet = QrEmbalagensCad
    Left = 456
    Top = 292
  end
end
