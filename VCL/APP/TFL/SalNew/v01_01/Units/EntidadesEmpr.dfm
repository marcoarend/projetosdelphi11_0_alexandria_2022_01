object FmEntidadesEmpr: TFmEntidadesEmpr
  Left = 368
  Top = 194
  Caption = 'ENT-CADAS-002 :: Cadastro de Clientes'
  ClientHeight = 480
  ClientWidth = 784
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 384
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 105
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 8
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label4: TLabel
        Left = 136
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label2: TLabel
        Left = 68
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodUsu
      end
      object Label3: TLabel
        Left = 404
        Top = 16
        Width = 89
        Height = 13
        Caption = 'Empresa do grupo:'
        FocusControl = DBEdit1
      end
      object Label5: TLabel
        Left = 660
        Top = 16
        Width = 50
        Height = 13
        Caption = 'Grupo SN:'
        FocusControl = DBEdit3
      end
      object Label6: TLabel
        Left = 8
        Top = 56
        Width = 36
        Height = 13
        Caption = 'Cidade:'
        FocusControl = DBEdit4
      end
      object Label8: TLabel
        Left = 184
        Top = 56
        Width = 17
        Height = 13
        Caption = 'UF:'
        FocusControl = DBEdit5
      end
      object Label10: TLabel
        Left = 216
        Top = 56
        Width = 37
        Height = 13
        Caption = 'Regi'#227'o:'
        FocusControl = DBEdit6
      end
      object Label11: TLabel
        Left = 304
        Top = 56
        Width = 25
        Height = 13
        Caption = 'Pa'#237's:'
        FocusControl = DBEdit7
      end
      object Label12: TLabel
        Left = 448
        Top = 56
        Width = 26
        Height = 13
        Caption = 'Perfil:'
        FocusControl = DBEdit8
      end
      object Label13: TLabel
        Left = 720
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Legal Unit:'
        FocusControl = DBEdit9
      end
      object Label14: TLabel
        Left = 676
        Top = 56
        Width = 33
        Height = 13
        Caption = 'HSDB:'
        FocusControl = DBEdit10
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 8
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsEntidadesEmpr
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 136
        Top = 32
        Width = 264
        Height = 21
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsEntidadesEmpr
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdCodUsu: TdmkDBEdit
        Left = 68
        Top = 32
        Width = 64
        Height = 21
        TabStop = False
        DataField = 'CodUsu'
        DataSource = DsEntidadesEmpr
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 2
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit1: TDBEdit
        Left = 404
        Top = 32
        Width = 56
        Height = 21
        DataField = 'GrupoEmpresa'
        DataSource = DsEntidadesEmpr
        TabOrder = 3
      end
      object DBEdit2: TDBEdit
        Left = 464
        Top = 32
        Width = 192
        Height = 21
        DataField = 'NO_GrupoEmpresa'
        DataSource = DsEntidadesEmpr
        TabOrder = 4
      end
      object DBEdit3: TDBEdit
        Left = 660
        Top = 32
        Width = 56
        Height = 21
        DataField = 'GrupEmprSN'
        DataSource = DsEntidadesEmpr
        TabOrder = 5
      end
      object DBEdit4: TDBEdit
        Left = 8
        Top = 72
        Width = 172
        Height = 21
        DataField = 'Cidade'
        DataSource = DsEntidadesEmpr
        TabOrder = 6
      end
      object DBEdit5: TDBEdit
        Left = 184
        Top = 72
        Width = 28
        Height = 21
        DataField = 'UF'
        DataSource = DsEntidadesEmpr
        TabOrder = 7
      end
      object DBEdit6: TDBEdit
        Left = 216
        Top = 72
        Width = 84
        Height = 21
        DataField = 'Regiao'
        DataSource = DsEntidadesEmpr
        TabOrder = 8
      end
      object DBEdit7: TDBEdit
        Left = 304
        Top = 72
        Width = 140
        Height = 21
        DataField = 'Pais'
        DataSource = DsEntidadesEmpr
        TabOrder = 9
      end
      object DBEdit8: TDBEdit
        Left = 448
        Top = 72
        Width = 224
        Height = 21
        DataField = 'NO_Perfil'
        DataSource = DsEntidadesEmpr
        TabOrder = 10
      end
      object DBEdit9: TDBEdit
        Left = 720
        Top = 32
        Width = 56
        Height = 21
        DataField = 'LegalUnit'
        DataSource = DsEntidadesEmpr
        TabOrder = 11
      end
      object DBEdit10: TDBEdit
        Left = 676
        Top = 72
        Width = 21
        Height = 21
        DataField = 'HvSrDB'
        DataSource = DsEntidadesEmpr
        TabOrder = 12
      end
      object DBEdit11: TDBEdit
        Left = 700
        Top = 72
        Width = 77
        Height = 21
        DataField = 'NO_HvSrDB'
        DataSource = DsEntidadesEmpr
        TabOrder = 13
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 320
      Width = 784
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 87
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 261
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 393
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Cliente'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 311
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Segmento'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
      end
    end
    object DGDados: TdmkDBGridZTO
      Left = 0
      Top = 105
      Width = 784
      Height = 148
      Align = alTop
      DataSource = DsSegmentosSGEA
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      RowColors = <>
      Columns = <
        item
          Expanded = False
          FieldName = 'NO_Segmento'
          Title.Caption = 'Segmento'
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_Accn_QT'
          Title.Caption = 'Account aqui'
          Width = 120
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_Accn_SN'
          Title.Caption = 'Account importado'
          Width = 120
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_HvSrDB'
          Title.Caption = 'HSDB'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Title.Caption = 'Observa'#231#227'o'
          Width = 384
          Visible = True
        end>
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 384
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 113
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 8
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 136
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label15: TLabel
        Left = 464
        Top = 16
        Width = 89
        Height = 13
        Caption = 'Empresa do grupo:'
        FocusControl = DBEdit1
      end
      object Label16: TLabel
        Left = 720
        Top = 16
        Width = 50
        Height = 13
        Caption = 'Grupo SN:'
        FocusControl = DBEdit3
      end
      object Label17: TLabel
        Left = 8
        Top = 56
        Width = 36
        Height = 13
        Caption = 'Cidade:'
        FocusControl = DBEdit4
      end
      object Label18: TLabel
        Left = 184
        Top = 56
        Width = 17
        Height = 13
        Caption = 'UF:'
        FocusControl = DBEdit5
      end
      object Label19: TLabel
        Left = 216
        Top = 56
        Width = 37
        Height = 13
        Caption = 'Regi'#227'o:'
        FocusControl = DBEdit6
      end
      object Label20: TLabel
        Left = 304
        Top = 56
        Width = 25
        Height = 13
        Caption = 'Pa'#237's:'
        FocusControl = DBEdit7
      end
      object Label22: TLabel
        Left = 676
        Top = 56
        Width = 30
        Height = 13
        Caption = 'HSDB'
        FocusControl = DBEdit10
      end
      object Label23: TLabel
        Left = 720
        Top = 56
        Width = 51
        Height = 13
        Caption = 'Legal Unit:'
        FocusControl = DBEdit9
      end
      object Label24: TLabel
        Left = 68
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label21: TLabel
        Left = 448
        Top = 56
        Width = 84
        Height = 13
        Caption = 'Perfil da empresa:'
        FocusControl = DBEdit1
      end
      object EdCodigo: TdmkEdit
        Left = 8
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 136
        Top = 32
        Width = 324
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCodUsu: TdmkEdit
        Left = 68
        Top = 32
        Width = 64
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CodUsu'
        UpdCampo = 'CodUsu'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdGrupoEmpresa: TdmkEditCB
        Left = 464
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfLongint
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'GrupoEmpresa'
        UpdCampo = 'GrupoEmpresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBGrupoEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBGrupoEmpresa: TdmkDBLookupComboBox
        Left = 520
        Top = 32
        Width = 196
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsEntidadesGrup
        TabOrder = 4
        dmkEditCB = EdGrupoEmpresa
        QryCampo = 'GrupoEmpresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdGrupEmprSN: TdmkEdit
        Left = 720
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'GrupEmprSN'
        UpdCampo = 'GrupEmprSN'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdCidade: TdmkEditF7
        Left = 8
        Top = 72
        Width = 172
        Height = 21
        TabOrder = 6
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Cidade'
        UpdCampo = 'Cidade'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnKeyDown = EdCidadeKeyDown
        LocF7CodiFldName = 'N'#227'o'
        LocF7NameFldName = 'Cidade'
        LocF7TableName = 'entidadesempr'
      end
      object EdUF: TdmkEdit
        Left = 184
        Top = 72
        Width = 28
        Height = 21
        TabOrder = 7
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'UF'
        UpdCampo = 'UF'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdRegiao: TdmkEditF7
        Left = 216
        Top = 72
        Width = 84
        Height = 21
        TabOrder = 8
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Regiao'
        UpdCampo = 'Regiao'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        LocF7CodiFldName = 'N'#227'o'
        LocF7NameFldName = 'Regiao'
        LocF7TableName = 'entidadesempr'
      end
      object EdPais: TdmkEditF7
        Left = 304
        Top = 72
        Width = 140
        Height = 21
        TabOrder = 9
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Pais'
        UpdCampo = 'Pais'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        LocF7CodiFldName = 'N'#227'o'
        LocF7NameFldName = 'Pais'
        LocF7TableName = 'entidadesempr'
      end
      object EdHvSrDB: TdmkEditF7
        Left = 676
        Top = 72
        Width = 40
        Height = 21
        Alignment = taRightJustify
        TabOrder = 12
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'HvSrDB'
        UpdCampo = 'HvSrDB'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        LocF7TableName = 'GetHvSrDBCad'
      end
      object EdLegalUnit: TdmkEdit
        Left = 720
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 13
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'LegalUnit'
        UpdCampo = 'LegalUnit'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdPerfil: TdmkEditCB
        Left = 448
        Top = 72
        Width = 32
        Height = 21
        Alignment = taRightJustify
        TabOrder = 10
        FormatType = dmktfLongint
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Perfil'
        UpdCampo = 'Perfil'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBPerfil
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBPerfil: TdmkDBLookupComboBox
        Left = 480
        Top = 72
        Width = 192
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsSalesPerfilCl
        TabOrder = 11
        dmkEditCB = EdPerfil
        QryCampo = 'Perfil'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 321
      Width = 784
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 644
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 520
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 256
        Height = 32
        Caption = 'Cadastro de Clientes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 256
        Height = 32
        Caption = 'Cadastro de Clientes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 256
        Height = 32
        Caption = 'Cadastro de Clientes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 784
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 424
    Top = 376
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 300
    Top = 372
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object QrEntidadesEmpr: TZQuery
    Connection = DMod.MyDB
    BeforeClose = QrEntidadesEmprBeforeClose
    AfterScroll = QrEntidadesEmprAfterScroll
    SQL.Strings = (
      'SELECT egr1.Nome NO_GrupoEmpresa, sprf.Nome NO_Perfil, emp.* '
      'FROM entidadesempr emp'
      'LEFT JOIN entidadesgrup egr1 ON egr1.Codigo=emp.GrupoEmpresa'
      'LEFT JOIN salesperfilcl sprf ON sprf.Codigo=emp.Perfil')
    Params = <>
    Left = 120
    Top = 312
    object QrEntidadesEmprCodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrEntidadesEmprCodUsu: TLargeintField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrEntidadesEmprNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 255
    end
    object QrEntidadesEmprGrupoEmpresa: TLargeintField
      FieldName = 'GrupoEmpresa'
      Required = True
    end
    object QrEntidadesEmprGrupEmprSN: TLargeintField
      FieldName = 'GrupEmprSN'
      Required = True
    end
    object QrEntidadesEmprPais: TWideStringField
      FieldName = 'Pais'
      Required = True
      Size = 60
    end
    object QrEntidadesEmprRegiao: TWideStringField
      FieldName = 'Regiao'
      Required = True
      Size = 60
    end
    object QrEntidadesEmprUF: TWideStringField
      FieldName = 'UF'
      Required = True
      Size = 2
    end
    object QrEntidadesEmprCidade: TWideStringField
      FieldName = 'Cidade'
      Required = True
      Size = 60
    end
    object QrEntidadesEmprLegalUnit: TLargeintField
      FieldName = 'LegalUnit'
      Required = True
    end
    object QrEntidadesEmprHvSrDB: TSmallintField
      FieldName = 'HvSrDB'
      ReadOnly = True
    end
    object QrEntidadesEmprLk: TLargeintField
      FieldName = 'Lk'
    end
    object QrEntidadesEmprDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEntidadesEmprDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEntidadesEmprUserCad: TLargeintField
      FieldName = 'UserCad'
    end
    object QrEntidadesEmprUserAlt: TLargeintField
      FieldName = 'UserAlt'
    end
    object QrEntidadesEmprAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrEntidadesEmprAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrEntidadesEmprNO_GrupoEmpresa: TWideStringField
      FieldName = 'NO_GrupoEmpresa'
      Required = True
      Size = 255
    end
    object QrEntidadesEmprNO_Perfil: TWideStringField
      FieldName = 'NO_Perfil'
      Required = True
      Size = 100
    end
    object QrEntidadesEmprPerfil: TLargeintField
      FieldName = 'Perfil'
      Required = True
    end
    object QrEntidadesEmprNO_HvSrDB: TWideMemoField
      FieldName = 'NO_HvSrDB'
      OnGetText = QrEntidadesEmprNO_HvSrDBGetText
      BlobType = ftWideMemo
      Size = 20
    end
  end
  object DsEntidadesEmpr: TDataSource
    DataSet = QrEntidadesEmpr
    Left = 120
    Top = 356
  end
  object QrSegmentosSGEA: TZQuery
    Connection = DMod.MyDB
    SQL.Strings = (
      'SELECT segc.Nome NO_Segmento, /*empr.Nome NO_Cliente,*/  '
      'acqt.Nome NO_Accn_QT, acsn.Nome NO_Accn_SN, sgea.*  '
      'FROM segmentossgea sgea '
      'LEFT JOIN segmentoscads segc ON segc.Codigo=sgea.Segmento '
      
        '/*LEFT JOIN entidadesempr empr ON empr.Codigo=sgea.EntidadesEmp*' +
        '/ '
      'LEFT JOIN entidadesaccn acqt ON acqt.Codigo=sgea.Account '
      'LEFT JOIN entidadesaccn acsn ON acsn.Codigo=sgea.AccntSN ')
    Params = <>
    Left = 252
    Top = 312
    object QrSegmentosSGEANO_Segmento: TWideStringField
      FieldName = 'NO_Segmento'
      Required = True
      Size = 60
    end
    object QrSegmentosSGEANO_Accn_QT: TWideStringField
      FieldName = 'NO_Accn_QT'
      Required = True
      Size = 255
    end
    object QrSegmentosSGEANO_Accn_SN: TWideStringField
      FieldName = 'NO_Accn_SN'
      Required = True
      Size = 255
    end
    object QrSegmentosSGEACodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrSegmentosSGEACodUsu: TLargeintField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrSegmentosSGEASegmento: TLargeintField
      FieldName = 'Segmento'
      Required = True
    end
    object QrSegmentosSGEAEntidadesEmp: TLargeintField
      FieldName = 'EntidadesEmp'
      Required = True
    end
    object QrSegmentosSGEAAccount: TLargeintField
      FieldName = 'Account'
      Required = True
    end
    object QrSegmentosSGEAAccntSN: TLargeintField
      FieldName = 'AccntSN'
      Required = True
    end
    object QrSegmentosSGEAHvSrDB: TSmallintField
      FieldName = 'HvSrDB'
      Required = True
    end
    object QrSegmentosSGEALk: TLargeintField
      FieldName = 'Lk'
    end
    object QrSegmentosSGEADataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrSegmentosSGEADataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrSegmentosSGEAUserCad: TLargeintField
      FieldName = 'UserCad'
    end
    object QrSegmentosSGEAUserAlt: TLargeintField
      FieldName = 'UserAlt'
    end
    object QrSegmentosSGEAAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrSegmentosSGEAAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrSegmentosSGEANome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrSegmentosSGEANO_HvSrDB: TWideMemoField
      FieldName = 'NO_HvSrDB'
      OnGetText = QrSegmentosSGEANO_HvSrDBGetText
      BlobType = ftWideMemo
      Size = 120
    end
  end
  object DsSegmentosSGEA: TDataSource
    DataSet = QrSegmentosSGEA
    Left = 252
    Top = 360
  end
  object QrEntidadesGrup: TZQuery
    Connection = DMod.MyDB
    SQL.Strings = (
      'SELECT * FROM entidadesgrup')
    Params = <>
    Left = 184
    Top = 244
    object QrEntidadesGrupCodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrEntidadesGrupCodUsu: TLargeintField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrEntidadesGrupNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 255
    end
    object QrEntidadesGrupHvSrDB: TSmallintField
      FieldName = 'HvSrDB'
      Required = True
    end
    object QrEntidadesGrupLk: TLargeintField
      FieldName = 'Lk'
    end
    object QrEntidadesGrupDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEntidadesGrupDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEntidadesGrupUserCad: TLargeintField
      FieldName = 'UserCad'
    end
    object QrEntidadesGrupUserAlt: TLargeintField
      FieldName = 'UserAlt'
    end
    object QrEntidadesGrupAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrEntidadesGrupAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object DsEntidadesGrup: TDataSource
    DataSet = QrEntidadesGrup
    Left = 184
    Top = 288
  end
  object QrSalesPerfilCl: TZQuery
    Connection = DMod.MyDB
    SQL.Strings = (
      'SELECT * FROM entidadesgrup')
    Params = <>
    Left = 340
    Top = 200
    object QrSalesPerfilClCodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrSalesPerfilClCodUsu: TLargeintField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrSalesPerfilClNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 255
    end
  end
  object DsSalesPerfilCl: TDataSource
    DataSet = QrSalesPerfilCl
    Left = 340
    Top = 244
  end
end
