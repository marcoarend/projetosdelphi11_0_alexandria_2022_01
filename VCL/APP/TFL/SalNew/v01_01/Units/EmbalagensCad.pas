unit EmbalagensCad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, (***UnGOTOy,***) UnInternalConsts, UnMsgInt, (***UMySQLModule,***) Menus,
  UnInternalConsts2, mySQLDbTables, (***UnMySQLCuringa,***) dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, ZAbstractRODataset, ZDataset, ZAbstractDataset,
  //
  UnGOTOz, UnGrl_DmkDB, dmkEditF7;

type
  TFmEmbalagensCad = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    DBEdCodigo: TdmkDBEdit;
    DBEdNome: TdmkDBEdit;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DGDados: TDBGrid;
    QrEmbalagensCad: TZQuery;
    DsEmbalagensCad: TDataSource;
    QrIts: TZQuery;
    DsIts: TDataSource;
    QrEmbalagensCadCodigo: TLargeintField;
    QrEmbalagensCadNome: TWideStringField;
    QrEmbalagensCadSigla: TWideStringField;
    QrEmbalagensCadLk: TLargeintField;
    QrEmbalagensCadDataCad: TDateField;
    QrEmbalagensCadDataAlt: TDateField;
    QrEmbalagensCadUserCad: TLargeintField;
    QrEmbalagensCadUserAlt: TLargeintField;
    QrEmbalagensCadAlterWeb: TSmallintField;
    QrEmbalagensCadAtivo: TSmallintField;
    QrItsCodigo: TLargeintField;
    QrItsNome: TWideStringField;
    QrItsSigla: TWideStringField;
    QrItsLk: TLargeintField;
    QrItsDataCad: TDateField;
    QrItsDataAlt: TDateField;
    QrItsUserCad: TLargeintField;
    QrItsUserAlt: TLargeintField;
    QrItsAlterWeb: TSmallintField;
    QrItsAtivo: TSmallintField;
    EdSigla: TdmkEdit;
    Label3: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    Label9: TLabel;
    QrEmbalagensCadOrdem: TLargeintField;
    QrEmbalagensCadCodUsu: TLargeintField;
    QrEmbalagensCadHvSrDB: TSmallintField;
    Label1: TLabel;
    DBEdCodUsu: TdmkDBEdit;
    Label8: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    QrEmbalagensCadNO_HvSrDB: TWideMemoField;
    EdCodUsu: TdmkEdit;
    Label24: TLabel;
    Label5: TLabel;
    EdOrdem: TdmkEdit;
    EdHvSrDB: TdmkEditF7;
    Label22: TLabel;
    DBEdit4: TDBEdit;
    Label2: TLabel;
    Label14: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrGraGruYAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrGraGruYBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrGraGruYAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrGraGruYBeforeClose(DataSet: TDataSet);
    procedure QrEmbalagensCadNO_HvSrDBGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraCadastro_Com_Itens_ITS(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    //***procedure ReopenCadastro_Com_Itens_ITS(Controle?: Integer);
    procedure ReopenCadastro_Com_Itens_ITS(Controle: Integer);

  end;

var
  FmEmbalagensCad: TFmEmbalagensCad;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module;//, (***MyDBCheck, DmkDAC_PF,***) Cadastro_Com_Itens_ITS;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmEmbalagensCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOz.LC(Atual, Codigo);
end;

procedure TFmEmbalagensCad.MostraCadastro_Com_Itens_ITS(SQLType: TSQLType);
begin
{
  if Grl_DmkDB.CriaFm(TFmCadastro_Com_Itens_ITS, FmCadastro_Com_Itens_ITS, afmoNegarComAviso) then
  begin
    FmCadastro_Com_Itens_ITS.ImgTipo.SQLType := SQLType;
    FmCadastro_Com_Itens_ITS.FQrCab := QrEmbalagensCad;
    FmCadastro_Com_Itens_ITS.FDsCab := DsCadastro_Com_Itens_CAB;
    FmCadastro_Com_Itens_ITS.FQrIts := QrIts;
    if SQLType = stIns then
      FmCadastro_Com_Itens_ITS.EdCPF1.ReadOnly := False
    else
    begin
      FmCadastro_Com_Itens_ITS.EdControle.ValueVariant := QrItsControle.Value;
      //
      FmCadastro_Com_Itens_ITS.EdCPF1.Text := MLAGeral.FormataCNPJ_TFT(QrItsCNPJ_CPF.Value);
      FmCadastro_Com_Itens_ITS.EdNomeEmiSac.Text := QrItsNome.Value;
      FmCadastro_Com_Itens_ITS.EdCPF1.ReadOnly := True;
    end;
    FmCadastro_Com_Itens_ITS.ShowModal;
    FmCadastro_Com_Itens_ITS.Destroy;
  end;
}
end;

procedure TFmEmbalagensCad.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrEmbalagensCad);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrEmbalagensCad, QrIts);
end;

procedure TFmEmbalagensCad.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrEmbalagensCad);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrIts);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrIts);
end;

procedure TFmEmbalagensCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOz.Go(Para, QrEmbalagensCadCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmEmbalagensCad.DefParams;
begin
  VAR_GOTOTABELA := 'embalagenscad';
  VAR_GOTOzSQLTABLE := QrEmbalagensCad;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOzSQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOz.LimpaVAR_SQL();

  VAR_SQLx.Add('SELECT ');
  VAR_SQLx.Add(dmkPF.ArrayToTextoCASEWHEN('sca.HvSrDB', 'NO_HvSrDB',
    TPosVirgula.pvPos, sHaveSrcDB, iHaveSrcDB) + ' sca.*');
  VAR_SQLx.Add('FROM embalagenscad sca');
  VAR_SQLx.Add('WHERE sca.Codigo > 0');
  //
  VAR_SQL1.Add('AND sca.Codigo=:P0');
  //
  VAR_SQL2.Add('AND sca.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND sca.Nome Like :P0');
  //
end;

procedure TFmEmbalagensCad.ItsAltera1Click(Sender: TObject);
begin
  MostraCadastro_Com_Itens_ITS(stUpd);
end;

procedure TFmEmbalagensCad.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmEmbalagensCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmEmbalagensCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmEmbalagensCad.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
{
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'Cadastro_Com_Itens_ITS', 'Controle', QrItsControle.Value, Dmod.MyDB?) = ID_YES then
  begin
    Controle := GOTOz.LocalizaPriorNextIntQr(QrIts,
      QrItsControle, QrItsControle.Value);
    ReopenCadastro_Com_Itens_ITS(Controle);
  end;
}
end;

procedure TFmEmbalagensCad.ReopenCadastro_Com_Itens_ITS(Controle: Integer);
begin
{
  UnDmkDAC_PF.AbreMySQLQuery0(QrIts, Dmod.MyDB, [
  'SELECT * ',
  'FROM cadastro_com_itens_its ',
  'WHERE Codigo=' + Geral.FF0(QrEmbalagensCadCodigo?.Value),
  '']);
  //
  QrIts.Locate('Controle?, Controle, []);
}
end;


procedure TFmEmbalagensCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmEmbalagensCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmEmbalagensCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmEmbalagensCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmEmbalagensCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmEmbalagensCad.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEmbalagensCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrEmbalagensCadCodigo.Value;
  Close;
end;

procedure TFmEmbalagensCad.ItsInclui1Click(Sender: TObject);
begin
  MostraCadastro_Com_Itens_ITS(stIns);
end;

procedure TFmEmbalagensCad.CabAltera1Click(Sender: TObject);
begin
  Grl_DmkDB.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrEmbalagensCad, [PnDados],
  [PnEdita], EdSigla, ImgTipo, 'embalagenscad');
end;

procedure TFmEmbalagensCad.BtConfirmaClick(Sender: TObject);
var
  Nome, Sigla: String;
  Codigo, CodUsu, Ordem, HvSrDB: Integer;
  SQLType: TSQLType;
begin
  SQLType             := ImgTipo.SQLType;
  //
  //Codigo              := EdCodigo.ValueVariant;
  //CodUsu              := EdCodUsu.ValueVariant;
  Grl_DmkDB.ObtemCodigoECodUsu(EdCodigo, EdCodUsu, SQLType, Codigo, CodUsu,
    'embalagenscad', 'Codigo', 'CodUsu');
  Nome                := EdNome.ValueVariant;
  Sigla               := EdSigla.ValueVariant;
  Ordem               := EdOrdem.ValueVariant;
  HvSrDB              := EdHvSrDB.ValueVariant;
  //
  //Codigo := Grl_DmkDB.GetNxtCodigoInt('embalagenscad', 'Codigo', SQLType, Codigo);
  if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, 'embalagenscad', False, [
  'CodUsu', 'Nome', 'Sigla',
  'Ordem', 'HvSrDB'], [
  'Codigo'], [
  CodUsu, Nome, Sigla,
  Ordem, HvSrDB], [
  Codigo], True, dmksqlinsInsOnly, '', stDesktop, False) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOz.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmEmbalagensCad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  //if ImgTipo.SQLType = stIns then
    //UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'embalagenscad', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  GOTOz.UpdUnlockZ(Codigo, Dmod.MyDB, 'embalagenscad', 'Codigo');
end;

procedure TFmEmbalagensCad.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmEmbalagensCad.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmEmbalagensCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmEmbalagensCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOz.Codigo(QrEmbalagensCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmEmbalagensCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOz.Nome(LaRegistro.Caption);
end;

procedure TFmEmbalagensCad.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOz.CodUsu(QrEmbalagensCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmEmbalagensCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOz.Fechar(ImgTipo.SQLType);
end;

procedure TFmEmbalagensCad.QrGraGruYAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmEmbalagensCad.QrGraGruYAfterScroll(DataSet: TDataSet);
begin
  ReopenCadastro_Com_Itens_ITS(0);
end;

procedure TFmEmbalagensCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrEmbalagensCadCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmEmbalagensCad.SbQueryClick(Sender: TObject);
begin
  LocCod(QrEmbalagensCadCodigo.Value,
  GOTOz.CriaForm(CO_CODIGO, CO_NOME, 'embalagenscad', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmEmbalagensCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEmbalagensCad.CabInclui1Click(Sender: TObject);
begin
  Grl_DmkDB.ConfigPanelInsUpd(stIns, Self, PnEdita, QrEmbalagensCad, [PnDados],
  [PnEdita], EdSigla, ImgTipo, 'embalagenscad');
  EdHvSrDB.ValueVariant := Integer(THaveSrcDB.hsdbCadastr);
end;

procedure TFmEmbalagensCad.QrGraGruYBeforeClose(
  DataSet: TDataSet);
begin
  QrIts.Close;
end;

procedure TFmEmbalagensCad.QrGraGruYBeforeOpen(DataSet: TDataSet);
begin
  QrEmbalagensCadCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmEmbalagensCad.QrEmbalagensCadNO_HvSrDBGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  Text := TStringField(Sender).Value;
end;

end.

