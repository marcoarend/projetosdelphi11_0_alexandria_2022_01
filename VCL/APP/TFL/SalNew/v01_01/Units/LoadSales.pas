unit LoadSales;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.ComCtrls, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Grids,
  dmkGeral, dmkEdit, UnDmkEnums, Data.DB, ZAbstractRODataset, ZDataset,
  ZAbstractDataset, Vcl.Buttons, Vcl.Menus, AdvGridWorkbook, tmsAdvGridExcel,
  AdvObj, BaseGrid, AdvGrid, UnAppEnums;

type

{
  TColunaExcel = ^TColuna;
  TColuna = record
    SQLColunaStr: String;
    XLSColunaInt: Integer;
    XLSTituloStr: String;
  end;
}

  TTituloSales = (
    titslsVazio            = 00,
    titslsSegmento         = 01,
    titslsGrupoSegmento    = 02,
    titslsLegalUnit        = 03,
    titslsAccount          = 04,
    titslsGrupoEmpresa     = 05,
    titslsCodigoCliente    = 06,
    titslsCliente          = 07,
    titslsPais             = 08,
    titslsRegiao           = 09,
    titslsUF               = 10,
    titslsCidade           = 11,
    titslsPerfil           = 12,
    titslsClienteFatura    = 13,
    titslsClienteDestino   = 14,
    titslsDivisao          = 15,
    titslsSubDivisao       = 16,
    titslsLinha            = 17,
    titslsFamilia          = 18,
    titslsEmbalagem        = 19,
    titslsProduto          = 20,
    titslsCodigoProduto    = 21,
    titslsData             = 22,
    titslsOperacao         = 23,
    titslsMoeda            = 24,
    titslsMoedaPed         = 25,
    titslsQuantidade       = 26,
    titslsBrutoBRL         = 27,
    titslsLiquidoBRL       = 28,
    titslsLiquidoUSD       = 29,
    titslsLiquidoEUR       = 30,
    titslsCustoBRL         = 31,
    titslsCustoUSD         = 32,
    titslsCustoEUR         = 33,
    titslsMPBRL            = 34,
    titslsMPUSD            = 35,
    titslsMPEUR            = 36,
    titslsDGFBRL           = 37,
    titslsDGFUSD           = 38,
    titslsDGFEUR           = 39
  );
  //
  TFmLoadSales = class(TForm)
    SGSales: TStringGrid;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PB1: TProgressBar;
    Query1: TZQuery;
    Query2: TZQuery;
    Panel5: TPanel;
    Label27: TLabel;
    SbSelArq: TSpeedButton;
    SbAbre: TSpeedButton;
    EdPath: TdmkEdit;
    SpeedButton1: TSpeedButton;
    EdMaxlinTit: TdmkEdit;
    Label2: TLabel;
    Memo1: TMemo;
    EdPlanilha: TdmkEdit;
    Label1: TLabel;
    Button4: TButton;
    procedure SbSelArqClick(Sender: TObject);
    procedure SbAbreClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SGSalesDblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button4Click(Sender: TObject);
  private
    { Private declarations }
    FParar: Boolean;
    FSLCampos: TStringList;
    //
    function  DefineOrdemCampos(Colunas: TStringList): Boolean;
    procedure CarregaVendasCSV();
    function  IncluiRegistroDeLinhaCSV(const Linha: Integer; const GruLoad, Codigo: Integer;
              var DataI, DataF: TDateTime): Boolean;
    function  LeArquivo(ApenasTeste: Boolean): Boolean;
    procedure Obtem_SiglEmbal_e_Peso(const Sigla: String; var SiglEmbal: String;
              var Peso: Double);
    function  ObtemValCell_Str(const Coluna, Linha: Integer; var Res: String):
              Boolean;
    function  ObtemValCell_Int(const Coluna, Linha: Integer; var Res: Integer):
              Boolean;
    function  ObtemValCell_Dta(const Coluna, Linha: Integer; var Res: TDateTime):
              Boolean;
    function  ObtemValSL_Str(const Coluna: Integer; var Res: String):
              Boolean;
    function  ObtemValSL_Int(const Coluna: Integer; var Res: Integer):
              Boolean;
    function  ObtemValSL_Dta(const Coluna: Integer; var Res: TDateTime):
             Boolean;

  public
    { Public declarations }
    function LoadSalesFromExcel(ApenasTeste: Boolean): Boolean;
    function IndexTitulo(Titulo: String): Integer;
  end;

var
  FmLoadSales: TFmLoadSales;


implementation

uses UnMyObjects, Module, UnGrl_DmkDB, LoadSalesGen;

{$R *.dfm}

{ TForm1 }

const
  CO_IDServr = 1;
  //cTitulos = 28;//24;
  cMaxTitulos = Integer(High(TTituloSales));
  cMaxDifTitSame = 2;
  TitulosSales: array[0..cMaxTitulos, 0..cMaxDifTitSame-1] of String = (
  ((*Vazio*)'', ''),
  ('Segmento', ''),
  ('GrupoSegmento', ''),
  ('LegalUnit', 'Unit'),
  ('Account', ''),
  ('GrupoEmpresa', ''),
  ('CodigoCliente', 'CodCli'),
  ('Cliente', ''),
  ('Pais', ''),
  ('Regiao', ''),
  ('UF', ''),
  ('Cidade', ''),
  ('Perfil', ''),
  ('ClienteFatura', ''),
  ('ClienteDestino', ''),
  ('Divis�o', ''),
  ('SubDivis�o', ''),
  ('Linha', ''),
  ('Familia', ''),
  ('Embalagem', ''),
  ('Produto', ''),
  ('CodigoProduto', 'CodProd'),
  ('Data', ''),
  ('Operacao', ''),
  ('Moeda', ''),
  ('MoedaPed', ''),
(*
  ('Soma de Quantidade', ''),
  ('Soma de BrutoR$', ''),
  ('Soma de LiquidoR$', ''),
  ('Soma de LiquidoU$', ''),
  ('Soma de LiquidoE$', ''),
  ('Soma de CustoR$', ''),
  ('Soma de CustoU$', ''),
  ('Soma de CustoE$', ''),
  ('Soma de MPR$', ''),
  ('Soma de MPU$', ''),
  ('Soma de MPE$', ''),
  ('Soma de DGFR$', ''),
  ('Soma de DGFU$', ''),
  ('Soma de DGFE$', '')
*)
  ('Quantidade', ''),
  ('BrutoR$', ''),
  ('LiquidoR$', ''),
  ('LiquidoU$', ''),
  ('LiquidoE$', ''),
  ('CustoR$', ''),
  ('CustoU$', ''),
  ('CustoE$', ''),
  ('MPR$', ''),
  ('MPU$', ''),
  ('MPE$', ''),
  ('DGFR$', ''),
  ('DGFU$', ''),
  ('DGFE$', '')
  );

var
  ColumnSales: array[0..cMaxTitulos] of Integer;

procedure TFmLoadSales.Button4Click(Sender: TObject);
begin
  FParar := True;
end;

procedure TFmLoadSales.CarregaVendasCSV();
var
  Arquivo, Linha, Senha: String;
  F: TextFile;
  I, J, Num, Cento: Integer;
  FilTam, FilPos: Largeint;
  //
  Arq, sDataI, sDataF: String;
  GruLoad, Codigo: Integer;
  SQLType: TSQLType;
  DataI, DataF: TDateTime;
  FilPer: Double;
begin
  DataI := Date();
  DataF := 0;
  Cento := 100;
  //
  SQLType := stIns;
  FParar := False;
  I := 0;
  Arq := Trim(EdPath.Text);
  if FileExists(Arq) then
  begin
    AssignFile(F, Arq);
    try
      Reset(F);
      FilTam := FileSize(F);
      FSLCampos := TStringList.Create;
      try
        PB1.Position := 0;
        PB1.Max := 100;

        Readln(F, Linha);

        FSLCampos := Geral.Explode(Linha, ';');
        //
        if not DefineOrdemCampos(FSLCampos) then
          Exit;
        //
        GruLoad := Grl_DmkDB.GetNxtCodigoInt('LoadSalesData', 'GruLoad', SQLType, 0);
        Codigo  := Grl_DmkDB.GetNxtCodigoInt('LoadSalesData', 'Codigo', SQLType, 0);
        while not Eof(F) do
        begin
          Application.ProcessMessages;
          if FParar then
            Exit;
          I := I + 1;
          Readln(F, Linha);
          //Memo1.Lines.Add(Linha);
          //
          //if pos(';;', Linha) > 0 then
          begin
            Linha := ';' + Linha + ';';
            FSLCampos := Geral.Explode3(Linha, ';');
          //Memo1.Lines.Add(Campos[1]);
            IncluiRegistroDeLinhaCSV(I, GruLoad, Codigo, DataI, DataF);
            Codigo := Codigo + 1;
          end;
          if I >= Cento then
          begin
            FilPos := FilePos(F);
            FilPer := (FilPos / FilTam) * 100;
            PB1.Position := Trunc(FilPer);
            MyObjects.Informa2(LaAviso1, LaAviso2, False,
            Geral.FF0(Cento) + ' registros importados. ' +
            Copy(FloatToStr(FilPer), 1, 5) + '%.');
            //
            Cento := Cento + 100;
          end;
        end;
        //
        if (DataF > 2) and (DataF >= DataI) then
        begin
          sDataI := Geral.FDT(DataI, 1);
          sDataF := Geral.FDT(DataF, 1);
          Grl_DmkDB.ExecutaSQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
          'DELETE FROM ' + LowerCase('LoadSalesData'),
          ' WHERE GruLoad <> ' + Geral.FF0(GruLoad),
          ' AND dData BETWEEN "' + sDataI + '" AND "' + sDataF + ' 23:59:59"',
          '']);
        end;
        //
        FilPos := FilePos(F);
        FilPer := (FilPos / FilTam) * 100;
        PB1.Position := Trunc(FilPer);
        MyObjects.Informa2(LaAviso1, LaAviso2, False,
        'Importa��o finalizada! ' +
        Geral.FF0(I) + ' registros importados. ' +
        Copy(FloatToStr(FilPer), 1, 5) + '%.');
        //
      finally
        FSLCampos.Free;
      end;
    finally
      CloseFile(f);
    end;
  end;
end;

function TFmLoadSales.DefineOrdemCampos(Colunas: TStringList): Boolean;
var
  I, J, N, Q, MaxLineSearch: Integer;
  Titulo, NoColsTxt, NoColsTmp: String;
begin
  Result := False;
  //
  for I := 0 to cMaxTitulos do
    ColumnSales[I] := -1;
  //
  Q := 0;
  for J := 0 to Colunas.Count -1  do
  begin
    Titulo := Trim(Colunas[J]);
    N := IndexTitulo(Titulo);
    if N > -1 then
      Q := Q + 1;
  end;
  if Q > 5 then
  begin
    for J := 0 to Colunas.Count -1  do
    begin
      Titulo := Trim(Colunas[J]);
      N := IndexTitulo(Titulo);
      if N > -1 then
        ColumnSales[N] := J;
    end;
    NoColsTxt := '';
    for I := 0 to cMaxTitulos do
    begin
      if ColumnSales[I] = -1 then
      begin
        NoColsTmp := '';
        for J := 0 to cMaxDifTitSame - 1 do
        begin
          if TitulosSales[I][J] <> '' then
          begin
            if J > 0 then
              NoColsTmp := NoColsTmp + ' ou ';
            NoColsTmp := NoColsTmp + TitulosSales[I][J];
          end;
        end;
        if NoColsTmp <> '' then
          NoColsTxt := NoColsTxt + NoColsTmp + slineBreak;
      end;
    end;
    if NoColsTxt <> '' then
    begin
      Result :=  Geral.MB_Pergunta(
      'As colunas abaixo n�o foram localizadas. Deseja continuar assim mesmo?' +
      sLineBreak + NoColsTxt + sLinebreak) = ID_YES;
    end else
      Result := True;
  end;
end;

procedure TFmLoadSales.FormCreate(Sender: TObject);
begin
  Memo1.Lines.Clear;
end;

function TFmLoadSales.IncluiRegistroDeLinhaCSV(const Linha: Integer;
const GruLoad, Codigo: Integer; var DataI, DataF: TDateTime): Boolean;
var
  xSegmento, xGrupoSegmento, xAccount, xGrupoEmpresa, xCliente,
  xPais, xRegiao, xUF, xCidade, xPerfil, xClienteFatura, xClienteDestino,
  xDivisao, xSubDivisao, xLinha, xFamilia, xEmbalagem, xProduto, xOperacao,
  xMoeda, xMoedaPed: String;
  dData: TDateTime;
  cCodCli, cCodCliFatura, cCodCliDestino, cLegalUnit, cCodProd: Integer;
  qQuantidade, qBrutoBRL, qLiquidoBRL, qLiquidoUSD, qLiquidoEUR, qCustoBRL,
  qCustoUSD, qCustoEUR, qMPBRL, qMPUSD, qDGFBRL, qDGFUSD, qDGFEUR: Double;
  SQLType: TSQLType;
  TmpStr, xCod: String;
  K, P, I: Integer;
begin
  Result := False;
  //
  xSegmento         := '';
  xGrupoSegmento    := '';
  cLegalUnit        := 0;
  xAccount          := '';
  xGrupoEmpresa     := '';
  xFamilia          := '';
  cCodcli           := 0;
  xCliente          := '';
  cCodCliFatura     := 0;
  xClienteFatura    := '';
  cCodCliDestino    := 0;
  xClienteDestino   := '';
  xDivisao          := '';
  xSubDivisao       := '';
  xLinha            := '';
  xPais             := '';
  xUF               := '';
  xOperacao         := '';
  xMoeda            := '';
  xMoedaPed         := '';
  xProduto          := '';
  xCidade           := '';
  dData             := 0;
  cCodProd          := 0;
  xEmbalagem        := '';
  xPerfil           := '';
  //cPlanta           := 0;
  xRegiao           := '';
  //xTrimestre        := '';
  //cUnit             := 0;
  qQuantidade       := 0;
  qBrutoBRL         := 0;
  qLiquidoBRL       := 0;
  qLiquidoUSD       := 0;
  qLiquidoEUR       := 0;
  qCustoBRL         := 0;
  qCustoUSD         := 0;
  qCustoEUR         := 0;
  qMPBRL            := 0;
  qMPUSD            := 0;
  qDGFBRL           := 0;
  qDGFUSD           := 0;
  qDGFEUR           := 0;
  //
{
  DataI := Date();
  DataF := 0;
}
{
  for I := LinTit + 1 to SGSales.RowCount -1 do
  begin
    MyObjects.UpdPB(PB1, LaAviso1, laAviso2);
    //sData := Trim(SGSales.Cells[iData, I]);
    //if Trim(SGSales.Cells[iData, I]) <> '' then

    if Trim(SGSales.Cells[ColumnSales[Integer(titslsData)], I]) <> '' then
    begin
      //dData := Geral.ValidaDataBR(Trim(SGSales.Cells[iData, I]), False, True);
      dData := Geral.ValidaDataBR(Trim(SGSales.Cells[ColumnSales[Integer(titslsData)], I]), False, True);
      if dData > 0 then
      begin
        if dData < DataI then
          DataI := dData;
        if dData > DataF then
          DataF := dData;
      end;
    end;
  end;
  if (DataF > 2) and (DataF >= DataI) then
  begin
    sDataI := Geral.FDT(DataI, 1);
    sDataF := Geral.FDT(DataF, 1);
    Grl_DmkDB.ExecutaSQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    'DELETE FROM ' + LowerCase('LoadSalesData'),
    ' WHERE dData BETWEEN "' + sDataI + '" AND "' + sDataF + ' 23:59:59"',
    '']);
  end;
}
  //
{
  for I := LinTit + 1 to SGSales.RowCount -1 do
  begin
    MyObjects.UpdPB(PB1, LaAviso1, laAviso2);
}
  //
  try
    if not ObtemValSL_Str(ColumnSales[Integer(titslsSegmento)]        , xSegmento     ) then Exit;
    if not ObtemValSL_Str(ColumnSales[Integer(titslsGrupoSegmento)]   , xGrupoSegmento) then Exit;
    if not ObtemValSL_Int(ColumnSales[Integer(titslsLegalUnit)]       , cLegalUnit    ) then Exit;
    if not ObtemValSL_Str(ColumnSales[Integer(titslsAccount)]         , xAccount      ) then Exit;
    if not ObtemValSL_Str(ColumnSales[Integer(titslsGrupoEmpresa)]    , xGrupoEmpresa ) then Exit;
    if not ObtemValSL_Str(ColumnSales[Integer(titslsFamilia)]         , xFamilia      ) then Exit;
    if not ObtemValSL_Int(ColumnSales[Integer(titslsCodigoCliente)]   , cCodcli       ) then Exit;
    //
    if not ObtemValSL_Str(ColumnSales[Integer(titslsClienteFatura)]   , TmpStr       ) then Exit;
    TmpStr := Trim(TmpStr);
    xCod   := '';
    for K := Length(TmpStr)-1 downto 1 do
    begin
      if CharInSet(TmpStr[K], (['0'..'9'])) then
        xCod := TmpStr[K] + xCod
      else
      begin
        P := K;
        Break;
      end;
    end;
    cCodCliFatura  := Geral.IMV(xCod);
    xClienteFatura := Trim(Copy(TmpStr, 1, K - 2));
    //
    if not ObtemValSL_Str(ColumnSales[Integer(titslsClienteDestino)]   , TmpStr       ) then Exit;
    TmpStr := Trim(TmpStr);
    xCod   := '';
    for K := Length(TmpStr)-1 downto 1 do
    begin
      if CharInSet(TmpStr[K], (['0'..'9'])) then
        xCod := TmpStr[K] + xCod
      else
      begin
        P := K;
        Break;
      end;
    end;
    cCodCliDestino  := Geral.IMV(xCod);
    xClienteDestino := Trim(Copy(TmpStr, 1, K - 2));
    //
    if not ObtemValSL_Str(ColumnSales[Integer(titslsPais)]            , xPais         ) then Exit;
    if not ObtemValSL_Str(ColumnSales[Integer(titslsDivisao)]         , xDivisao      ) then Exit;
    if not ObtemValSL_Str(ColumnSales[Integer(titslsSubDivisao)]      , xSubDivisao   ) then Exit;
    if not ObtemValSL_Str(ColumnSales[Integer(titslsLinha)]           , xLinha        ) then Exit;
    if not ObtemValSL_Str(ColumnSales[Integer(titslsUF)]              , xUF           ) then Exit;
    if not ObtemValSL_Str(ColumnSales[Integer(titslsOperacao)]        , xOperacao     ) then Exit;
    if not ObtemValSL_Str(ColumnSales[Integer(titslsMoeda)]           , xMoeda        ) then Exit;
    if not ObtemValSL_Str(ColumnSales[Integer(titslsMoedaPed)]        , xMoedaPed     ) then Exit;
    if not ObtemValSL_Str(ColumnSales[Integer(titslsProduto)]         , xProduto      ) then Exit;
    if not ObtemValSL_Str(ColumnSales[Integer(titslsCliente)]         , xCliente      ) then Exit;
    if not ObtemValSL_Str(ColumnSales[Integer(titslsCidade)]          , xCidade       ) then Exit;
    if not ObtemValSL_Dta(ColumnSales[Integer(titslsData)]            , dData         ) then Exit;
    if not ObtemValSL_Int(ColumnSales[Integer(titslsCodigoProduto)]   , cCodProd      ) then Exit;
    if not ObtemValSL_Str(ColumnSales[Integer(titslsEmbalagem)]       , xEmbalagem    ) then Exit;
    if not ObtemValSL_Str(ColumnSales[Integer(titslsPerfil)]          , xPerfil       ) then Exit;
    if not ObtemValSL_Str(ColumnSales[Integer(titslsRegiao)]          , xRegiao       ) then Exit;
    //
    qQuantidade       := Geral.DMV(FSLCampos[ColumnSales[Integer(titslsQuantidade)]      ]);
    qBrutoBRL         := Geral.DMV(FSLCampos[ColumnSales[Integer(titslsBrutoBRL)]        ]);
    qLiquidoBRL       := Geral.DMV(FSLCampos[ColumnSales[Integer(titslsLiquidoBRL)]      ]);
    qLiquidoUSD       := Geral.DMV(FSLCampos[ColumnSales[Integer(titslsLiquidoUSD)]      ]);
    qLiquidoEUR       := Geral.DMV(FSLCampos[ColumnSales[Integer(titslsLiquidoEUR)]      ]);
    qCustoBRL         := Geral.DMV(FSLCampos[ColumnSales[Integer(titslsCustoBRL)]        ]);
    qCustoUSD         := Geral.DMV(FSLCampos[ColumnSales[Integer(titslsCustoUSD)]        ]);
    qCustoEUR         := Geral.DMV(FSLCampos[ColumnSales[Integer(titslsCustoEUR)]        ]);
    qMPBRL            := Geral.DMV(FSLCampos[ColumnSales[Integer(titslsMPBRL)]           ]);
    qMPUSD            := Geral.DMV(FSLCampos[ColumnSales[Integer(titslsMPUSD)]           ]);
    qDGFBRL           := Geral.DMV(FSLCampos[ColumnSales[Integer(titslsDGFBRL)]          ]);
    qDGFUSD           := Geral.DMV(FSLCampos[ColumnSales[Integer(titslsDGFUSD)]          ]);
    qDGFEUR           := Geral.DMV(FSLCampos[ColumnSales[Integer(titslsDGFEUR)]          ]);
    //
    if (qQuantidade <> 0) then
    begin
      if (pos('total', Lowercase(xSegmento)) > 0)
      or (pos('total', Lowercase(xGrupoSegmento)) > 0) then
        Memo1.Lines.Append('Linha de segmento "' + xSegmento + '" n�o carregada!')
      else
      begin
        if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, 'loadsalesdata', False, [
        'xSegmento', 'xAccount', 'xGrupoEmpresa',
        'cCodCli', 'xCliente', 'xPais',
        'xRegiao', 'xUF', 'xCidade',
        'xPerfil', 'cCodCliFatura', 'xClienteFatura',
        'cCodCliDestino', 'xClienteDestino', 'xDivisao',
        'xSubDivisao', 'xLinha', 'xFamilia',
        'xEmbalagem', 'xProduto', 'cCodProd',
        'dData', 'xOperacao', 'xMoeda',
        'xMoedaPed', 'qQuantidade', 'qBrutoBRL',
        'qLiquidoBRL', 'qLiquidoUSD', 'qLiquidoEUR',
        'qCustoBRL', 'qCustoUSD', 'qCustoEUR',
        'qMPBRL', 'qMPUSD', 'qDGFBRL',
        'qDGFUSD', 'qDGFEUR', 'xGrupoSegmento',
        'GruLoad', 'cLegalUnit'], [
        'Codigo'], [
        xSegmento, xAccount, xGrupoEmpresa,
        cCodCli, xCliente, xPais,
        xRegiao, xUF, xCidade,
        xPerfil, cCodCliFatura, xClienteFatura,
        cCodCliDestino, xClienteDestino, xDivisao,
        xSubDivisao, xLinha, xFamilia,
        xEmbalagem, xProduto, cCodProd,
        dData, xOperacao, xMoeda,
        xMoedaPed, qQuantidade, qBrutoBRL,
        qLiquidoBRL, qLiquidoUSD, qLiquidoEUR,
        qCustoBRL, qCustoUSD, qCustoEUR,
        qMPBRL, qMPUSD, qDGFBRL,
        qDGFUSD, qDGFEUR, xGrupoSegmento,
        GruLoad, cLegalUnit], [
        Codigo], True, TDmkSQLInsert.dmksqlinsInsOnly, '', TDeviceType.stDesktop, False) then
        begin
          (*Codigo := Codigo + 1*);
          if dData > 0 then
          begin
            if dData < DataI then
              DataI := dData;
            if dData > DataF then
              DataF := dData;
          end;
        end;
      end;
    end;
    Result := True;
    //MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Importa��o CSV OK');
  except
    Geral.MB_Info(Geral.FF0(Linha));
  end;
end;

function TFmLoadSales.IndexTitulo(Titulo: String): Integer;
var
  I, J: Integer;
begin
  Result := -1;
  if Trim(Titulo) = '' then
    Exit;
  for I := Low(TitulosSales) to High(TitulosSales) do
  begin
(*
    if Lowercase(TitulosSales[I]) = Lowercase(Titulo) then
    begin
      Result := I;
      Exit;
    end;
*)
    for J := Low(TitulosSales[I]) to High(TitulosSales[I]) do
    begin
      if Lowercase(TitulosSales[I][J]) = Lowercase(Titulo) then
      begin
        Result := I;
        Exit;
      end;
    end;
  end;
end;

function TFmLoadSales.LeArquivo(ApenasTeste: Boolean): Boolean;
var
  MaxLinTit: Integer;
  Dir, Arq, Ext: String;
  Continua: Boolean;
begin
  if not ApenasTeste then
    MaxLinTit := 0
  else
    MaxLinTit := EdMaxlinTit.ValueVariant;
  Ext := Lowercase(ExtractFileExt(EdPath.Text));
  if Ext = '.txt' then
    Continua := MyObjects.LoadStringGridFromFile(SGSales, EdPath.Text)
  else
  begin
    if MyObjects.Xls_To_StringGrid(SGSales, EdPath.Text, PB1,
      LaAviso1, LaAviso2, EdPlanilha.ValueVariant, MaxLinTit)
      //LaAviso1, LaAviso2, EdPlanilha.ValueVariant, 100)
    then
    begin
      Continua := True;
      if ApenasTeste = False then
      begin
        Dir := ExtractFileDir(EdPath.Text);
        Arq := ExtractFileName(EdPath.Text);
        Ext := '_' +FormatDateTime('YYMMDD_HHNNSS', Now()) + '.Txt';
        MyObjects.SalvaStringGridToFile(SGSales, Dir + '\' + Arq + Ext, True);
      end;
    //if MyObjects.Xls_To_Array(MyArrStr, EdPath.Text, PB1, LaAviso1, LaAviso2, 1) then
    //LoadSalesFromExcel();
    end;
  end;
  if Continua then
    Result := LoadSalesFromExcel(ApenasTeste);
end;

function TFmLoadSales.LoadSalesFromExcel(ApenasTeste: Boolean): Boolean;
const
  IGNORE = True;
var
  //MyArrStr: TMyArrArrStr;
  I, j, N, F, Q: Integer;
  Titulo: String;
var
  xSegmento, xGrupoSegmento, xAccount, xGrupoEmpresa, xFamilia, xPais, xUF,
  xOperacao, xProduto, xCliente, xCidade, xEmbalagem, xPerfil, xRegiao, sdataI,
  sDataF, xClienteFatura, xClienteDestino, xDivisao, xSubDivisao, xLinha,
  xMoeda, xMoedaPed: String;
  dData, DataI, DataF: TDateTime;
  Codigo, cCodcli, cCodCliFatura, cCodCliDestino, cLegalUnit, cCodProd: Integer;
  qQuantidade, qBrutoBRL, qLiquidoBRL, qLiquidoUSD, qLiquidoEUR, qCustoBRL,
  qCustoUSD, qCustoEUR, qMPBRL, qMPUSD, qDGFBRL, qDGFUSD, qDGFEUR: Double;
  SQLType: TSQLType;
var
  NoColsTxt, NoColsTmp, TmpStr, xCod: String;
  K, P, MaxLineSearch, LinTit: Integer;
begin
//begin
  Result := False;
  SQLType := stIns;
  MaxLineSearch := EdMaxLinTit.ValueVariant;
  //
  if MyObjects.FIC(MaxLineSearch < 1, EdMaxLinTit,
  'Informe a linha m�xima dos t�tulos da tabela din�mica!') then
    Exit;
  //
  for I := 0 to cMaxTitulos do
    ColumnSales[I] := -1;

  //
{
  //SGSales.Visible := False;
  if MyObjects.Xls_To_StringGrid(SGSales, EdPath.Text, PB1, LaAviso1, LaAviso2,
  //1, LinCab + 100) then
  1) then
}
  //if MyObjects.Xls_To_Array(MyArrStr, EdPath.Text, PB1, LaAviso1, LaAviso2, 1) then
  begin
    F := 0;
    for I := 0 to MaxLineSearch do
    begin
      Q := 0;
      for J := 0 to SGSales.ColCount -1  do
      begin
        Titulo := Trim(SGSales.Cells[J, I]);
        N := IndexTitulo(Titulo);
        if N > -1 then
          Q := Q + 1;
      end;
      if Q > 5 then
      begin
        LinTit := I;
        for J := 0 to SGSales.ColCount -1  do
        begin
          Titulo := Trim(SGSales.Cells[J, I]);
          N := IndexTitulo(Titulo);
          if N > -1 then
            ColumnSales[N] := J;
        end;
        Break;
      end;
    end;
{
    for I := 1 to SGSales.ColCount - 1 do
    begin
      Titulo := Trim(SGSales.Cells[I, linCab]);
      N := IndexTitulo(Titulo);
      if N > -1 then
        ColumnSales[N] := I;
    end;
}
    NoColsTxt := '';
    for I := 0 to cMaxTitulos do
    begin
      if ColumnSales[I] = -1 then
      begin
        NoColsTmp := '';
        for J := 0 to cMaxDifTitSame - 1 do
        begin
          if TitulosSales[I][J] <> '' then
          begin
            if J > 0 then
              NoColsTmp := NoColsTmp + ' ou ';
            NoColsTmp := NoColsTmp + TitulosSales[I][J];
          end;
        end;
        if NoColsTmp <> '' then
          NoColsTxt := NoColsTxt + NoColsTmp + slineBreak;
      end;
    end;
    if NoColsTxt <> '' then
    begin
      if Geral.MB_Pergunta(
      'As colunas abaixo n�o foram localizadas. Deseja continuar assim mesmo?' +
      sLineBreak + NoColsTxt + sLinebreak) <> ID_YES then Exit;
    end;
    if ApenasTeste then
    begin
      Result := True;
      Exit;
    end;
    //if F >= cTitulos then
    begin
      xSegmento         := '';
      xGrupoSegmento    := '';
      cLegalUnit        := 0;
      xAccount          := '';
      xGrupoEmpresa     := '';
      xFamilia          := '';
      cCodcli           := 0;
      xCliente          := '';
      cCodCliFatura     := 0;
      xClienteFatura    := '';
      cCodCliDestino    := 0;
      xClienteDestino   := '';
      xDivisao          := '';
      xSubDivisao       := '';
      xLinha            := '';
      xPais             := '';
      xUF               := '';
      xOperacao         := '';
      xMoeda            := '';
      xMoedaPed         := '';
      xProduto          := '';
      xCidade           := '';
      dData             := 0;
      cCodProd          := 0;
      xEmbalagem        := '';
      xPerfil           := '';
      //cPlanta           := 0;
      xRegiao           := '';
      //xTrimestre        := '';
      //cUnit             := 0;
      qQuantidade       := 0;
      qBrutoBRL         := 0;
      qLiquidoBRL       := 0;
      qLiquidoUSD       := 0;
      qLiquidoEUR       := 0;
      qCustoBRL         := 0;
      qCustoUSD         := 0;
      qCustoEUR         := 0;
      qMPBRL            := 0;
      qMPUSD            := 0;
      qDGFBRL           := 0;
      qDGFUSD           := 0;
      qDGFEUR           := 0;
      //
      //PB1.Position := 0;
      //PB1.Max := SGSales.RowCount - MaxLineSearch;
      DataI := Date();
      DataF := 0;
      for I := LinTit + 1 to SGSales.RowCount -1 do
      begin
        MyObjects.UpdPB(PB1, LaAviso1, laAviso2);
        //sData := Trim(SGSales.Cells[iData, I]);
        //if Trim(SGSales.Cells[iData, I]) <> '' then

        if Trim(SGSales.Cells[ColumnSales[Integer(titslsData)], I]) <> '' then
        begin
          //dData := Geral.ValidaDataBR(Trim(SGSales.Cells[iData, I]), False, True);
          dData := Geral.ValidaDataBR(Trim(SGSales.Cells[ColumnSales[Integer(titslsData)], I]), False, True);
          if dData > 0 then
          begin
            if dData < DataI then
              DataI := dData;
            if dData > DataF then
              DataF := dData;
          end;
        end;
      end;
      if (DataF > 2) and (DataF >= DataI) then
      begin
        sDataI := Geral.FDT(DataI, 1);
        sDataF := Geral.FDT(DataF, 1);
        Grl_DmkDB.ExecutaSQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
        'DELETE FROM ' + LowerCase('LoadSalesData'),
        ' WHERE dData BETWEEN "' + sDataI + '" AND "' + sDataF + ' 23:59:59"',
        '']);
      end;
      //
      PB1.Position := 0;
      PB1.Max := SGSales.RowCount - LinTit;
      Codigo := Grl_DmkDB.GetNxtCodigoInt('LoadSalesData', 'Codigo', SQLType, 0);
      for I := LinTit + 1 to SGSales.RowCount -1 do
      begin
        MyObjects.UpdPB(PB1, LaAviso1, laAviso2);
        //
        if not ObtemValCell_Str(ColumnSales[Integer(titslsSegmento)]        , I, xSegmento     ) then Exit;
        if not ObtemValCell_Str(ColumnSales[Integer(titslsGrupoSegmento)]   , I, xGrupoSegmento) then Exit;
        if not ObtemValCell_Int(ColumnSales[Integer(titslsLegalUnit)]       , I, cLegalUnit    ) then Exit;
        if not ObtemValCell_Str(ColumnSales[Integer(titslsAccount)]         , I, xAccount      ) then Exit;
        if not ObtemValCell_Str(ColumnSales[Integer(titslsGrupoEmpresa)]    , I, xGrupoEmpresa ) then Exit;
        if not ObtemValCell_Str(ColumnSales[Integer(titslsFamilia)]         , I, xFamilia      ) then Exit;
        if not ObtemValCell_Int(ColumnSales[Integer(titslsCodigoCliente)]   , I, cCodcli       ) then Exit;
        //
        if not ObtemValCell_Str(ColumnSales[Integer(titslsClienteFatura)]   , I, TmpStr       ) then Exit;
        TmpStr := Trim(TmpStr);
        xCod   := '';
        for K := Length(TmpStr)-1 downto 1 do
        begin
          if CharInSet(TmpStr[K], (['0'..'9'])) then
            xCod := TmpStr[K] + xCod
          else
          begin
            P := K;
            Break;
          end;
        end;
        cCodCliFatura  := Geral.IMV(xCod);
        xClienteFatura := Trim(Copy(TmpStr, 1, K - 2));
        //
        if not ObtemValCell_Str(ColumnSales[Integer(titslsClienteDestino)]   , I, TmpStr       ) then Exit;
        TmpStr := Trim(TmpStr);
        xCod   := '';
        for K := Length(TmpStr)-1 downto 1 do
        begin
          if CharInSet(TmpStr[K], (['0'..'9'])) then
            xCod := TmpStr[K] + xCod
          else
          begin
            P := K;
            Break;
          end;
        end;
        cCodCliDestino  := Geral.IMV(xCod);
        xClienteDestino := Trim(Copy(TmpStr, 1, K - 2));
        //
        if not ObtemValCell_Str(ColumnSales[Integer(titslsPais)]            , I, xPais         ) then Exit;
        if not ObtemValCell_Str(ColumnSales[Integer(titslsDivisao)]         , I, xDivisao      ) then Exit;
        if not ObtemValCell_Str(ColumnSales[Integer(titslsSubDivisao)]      , I, xSubDivisao   ) then Exit;
        if not ObtemValCell_Str(ColumnSales[Integer(titslsLinha)]           , I, xLinha        ) then Exit;
        if not ObtemValCell_Str(ColumnSales[Integer(titslsUF)]              , I, xUF           ) then Exit;
        if not ObtemValCell_Str(ColumnSales[Integer(titslsOperacao)]        , I, xOperacao     ) then Exit;
        if not ObtemValCell_Str(ColumnSales[Integer(titslsMoeda)]           , I, xMoeda        ) then Exit;
        if not ObtemValCell_Str(ColumnSales[Integer(titslsMoedaPed)]        , I, xMoedaPed     ) then Exit;
        if not ObtemValCell_Str(ColumnSales[Integer(titslsProduto)]         , I, xProduto      ) then Exit;
        if not ObtemValCell_Str(ColumnSales[Integer(titslsCliente)]         , I, xCliente      ) then Exit;
        if not ObtemValCell_Str(ColumnSales[Integer(titslsCidade)]          , I, xCidade       ) then Exit;
        if not ObtemValCell_Dta(ColumnSales[Integer(titslsData)]            , I, dData         ) then Exit;
        if not ObtemValCell_Int(ColumnSales[Integer(titslsCodigoProduto)]   , I, cCodProd      ) then Exit;
        if not ObtemValCell_Str(ColumnSales[Integer(titslsEmbalagem)]       , I, xEmbalagem    ) then Exit;
        if not ObtemValCell_Str(ColumnSales[Integer(titslsPerfil)]          , I, xPerfil       ) then Exit;
        if not ObtemValCell_Str(ColumnSales[Integer(titslsRegiao)]          , I, xRegiao       ) then Exit;
        //
        qQuantidade       := Geral.DMV(SGSales.Cells[ColumnSales[Integer(titslsQuantidade)]      , I]);
        qBrutoBRL         := Geral.DMV(SGSales.Cells[ColumnSales[Integer(titslsBrutoBRL)]        , I]);
        qLiquidoBRL       := Geral.DMV(SGSales.Cells[ColumnSales[Integer(titslsLiquidoBRL)]      , I]);
        qLiquidoUSD       := Geral.DMV(SGSales.Cells[ColumnSales[Integer(titslsLiquidoUSD)]      , I]);
        qLiquidoEUR       := Geral.DMV(SGSales.Cells[ColumnSales[Integer(titslsLiquidoEUR)]      , I]);
        qCustoBRL         := Geral.DMV(SGSales.Cells[ColumnSales[Integer(titslsCustoBRL)]        , I]);
        qCustoUSD         := Geral.DMV(SGSales.Cells[ColumnSales[Integer(titslsCustoUSD)]        , I]);
        qCustoEUR         := Geral.DMV(SGSales.Cells[ColumnSales[Integer(titslsCustoEUR)]        , I]);
        qMPBRL            := Geral.DMV(SGSales.Cells[ColumnSales[Integer(titslsMPBRL)]           , I]);
        qMPUSD            := Geral.DMV(SGSales.Cells[ColumnSales[Integer(titslsMPUSD)]           , I]);
        qDGFBRL           := Geral.DMV(SGSales.Cells[ColumnSales[Integer(titslsDGFBRL)]          , I]);
        qDGFUSD           := Geral.DMV(SGSales.Cells[ColumnSales[Integer(titslsDGFUSD)]          , I]);
        qDGFEUR           := Geral.DMV(SGSales.Cells[ColumnSales[Integer(titslsDGFEUR)]          , I]);
        //
        if (qQuantidade <> 0) then
        begin
          if pos('total', Lowercase(xSegmento)) > 0 then
            Memo1.Lines.Append('Linha de segmento "' + xSegmento + '" n�o carregada!')
          else
          begin
            if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, 'loadsalesdata', False, [
            'xSegmento', 'xAccount', 'xGrupoEmpresa',
            'cCodCli', 'xCliente', 'xPais',
            'xRegiao', 'xUF', 'xCidade',
            'xPerfil', 'cCodCliFatura', 'xClienteFatura',
            'cCodCliDestino', 'xClienteDestino', 'xDivisao',
            'xSubDivisao', 'xLinha', 'xFamilia',
            'xEmbalagem', 'xProduto', 'cCodProd',
            'dData', 'xOperacao', 'xMoeda',
            'xMoedaPed', 'qQuantidade', 'qBrutoBRL',
            'qLiquidoBRL', 'qLiquidoUSD', 'qLiquidoEUR',
            'qCustoBRL', 'qCustoUSD', 'qCustoEUR',
            'qMPBRL', 'qMPUSD', 'qDGFBRL',
            'qDGFUSD', 'qDGFEUR', 'xGrupoSegmento',
            'cLegalUnit'], [
            'Codigo'], [
            xSegmento, xAccount, xGrupoEmpresa,
            cCodCli, xCliente, xPais,
            xRegiao, xUF, xCidade,
            xPerfil, cCodCliFatura, xClienteFatura,
            cCodCliDestino, xClienteDestino, xDivisao,
            xSubDivisao, xLinha, xFamilia,
            xEmbalagem, xProduto, cCodProd,
            dData, xOperacao, xMoeda,
            xMoedaPed, qQuantidade, qBrutoBRL,
            qLiquidoBRL, qLiquidoUSD, qLiquidoEUR,
            qCustoBRL, qCustoUSD, qCustoEUR,
            qMPBRL, qMPUSD, qDGFBRL,
            qDGFUSD, qDGFEUR, xGrupoSegmento,
            cLegalUnit], [
            Codigo], True, TDmkSQLInsert.dmksqlinsInsOnly, '', TDeviceType.stDesktop, False) then
              Codigo := Codigo + 1;
          end;
        end;
      end;
      Result := True;
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Importa��o excel OK');
    end //else
      //Geral.MB_Aviso('Quantidade de t�tulos n�o confere!')
  end;
end;

function TFmLoadSales.ObtemValCell_Str(const Coluna, Linha: Integer; var
  Res: String): Boolean;
var
  Txt: String;
begin
{
  Result := False;
  Txt := Trim(SGSales.Cells[Coluna, Linha]);
  if Txt <> '' then
  begin
    Res := Txt;
    Result := True;
  end else
    Geral.MB_Info('N�o foi poss�vel obter o valor da c�lula: coluna ' +
    Geral.FF0(Coluna) + ', linha ' + Geral.FF0(Linha));
}
  Txt := Trim(SGSales.Cells[Coluna, Linha]);
  if Txt <> '' then
    Res := Txt;
  Result := True;
end;

function TFmLoadSales.ObtemValSL_Dta(const Coluna: Integer;
  var Res: TDateTime): Boolean;
var
  Txt: String;
begin
  Txt := Trim(FSLCampos[Coluna]);
  if Txt <> '' then
    Res := Geral.ValidaDataBR(Txt, False, True);
  Result := True;
end;

function TFmLoadSales.ObtemValSL_Int(const Coluna: Integer;
  var Res: Integer): Boolean;
var
  Txt: String;
begin
  Txt := Trim(FSLCampos[Coluna]);
  if Txt <> '' then
    Res := Geral.IMV(Txt);
  Result := True;
end;

function TFmLoadSales.ObtemValSL_Str(const Coluna: Integer;
  var Res: String): Boolean;
var
  Txt: String;
begin
  Txt := Trim(FSLCampos[Coluna]);
  if Txt <> '' then
    Res := Txt;
  Result := True;
end;

procedure TFmLoadSales.Obtem_SiglEmbal_e_Peso(const Sigla: String;
  var SiglEmbal: String; var Peso: Double);
var
  I, P(*, Estagio*): Integer;
  xKg, Resto: String;

begin
  SiglEmbal := '';
  Peso      := 0.000000;
  xKg       := '';
  //Estagio   := 0;
  //
  for I := 1 to Length(Sigla) do
  begin
    //if (CharInSet(Sigla[I], (['a'..'z']))) then
    //if (CharInSet(Sigla[I], (['A'..'Z']))) then
    if (CharInSet(Sigla[I], (['0'..'9']))) then
      xKg := xKg + Sigla[I]
    else
    begin
      //Estagio := 1;
      Resto := Trim(Copy(Sigla, I));
      P := Pos('KG', Uppercase(Resto));
      if P = 1 then
        Resto := Trim(Copy(Resto, 3));
      Break;
    end;
  end;
  if xKg <> '' then
    Peso := Geral.DMV(xKg);
  SiglEmbal := Resto;
end;

procedure TFmLoadSales.SbAbreClick(Sender: TObject);
(*
const
  Teste = True;
*)
begin
(*  if LeArquivo(Teste) then
    LeArquivo(not Teste);
*)
  CarregaVendasCSV();
end;

procedure TFmLoadSales.SbSelArqClick(Sender: TObject);
var
  Arquivo: String;
begin
  if MyObjects.FileOpenDialog(Self, '', '', 'Sele��o de arquivo', '', [], Arquivo) then
    EdPath.ValueVariant := Arquivo
  else
    EdPath.ValueVariant := '';
end;

procedure TFmLoadSales.SGSalesDblClick(Sender: TObject);
begin
  //EdLinCab.ValueVariant := SGSales.Row + 1;
end;

procedure TFmLoadSales.SpeedButton1Click(Sender: TObject);
const
  IGNORE = True;
  SiglasEmbal: array[0..9] of String = (
    'FBX', 'IBC', 'IBX', 'PAB', 'PBX', 'PDR', 'PDX', 'PJC', 'PLB', 'PLU');
  //
  function IniceSigla(Sigla: String): Integer;
  var
    I: Integer;
  begin
    Result := -1;
    for I := Low(SiglasEmbal) to High(SiglasEmbal) do
    begin
      if SiglasEmbal[I] = UpperCase(Sigla) then
      begin
        Result := I;
        Exit;
      end;
    end;
  end;
var
  xPais, xUF, xCidade, xPerfil, xRegiao, Nome: String;
  I, Codigo, CodUsu, AccntSN, GrupoEmpresa, GrupEmprSN, HvSrDB, ProdutosCadas: Integer;
  SQLType: TSQLType;
var
  vNome, vCodigo: Variant;
  // EntidadesAccn
  Sigla: String;
  // SegmentosSGEA
  Segmento, EntidadesEmp, Account, Perfil, Ordem: Integer;
  // ProdutosCadas
  EntidadesCptt, Familia, ProdCabec, GrupoSegmento, Divisao, SubDivisao,
  Lista: Integer;
  // ProdutosEmba
  CodProd, EmbalagensTar: Integer;
  // SalesOperacoes
  EhFatura: Integer;
  // EmbalagensCad
  (*SiglEmbal: String;
  PesoBru, PPalLiq: Double;*)
  // EmbalagensTar
  EmbalagensCad, ItmsInPalt: Integer;
  PesoLiq, PesoTar, PPallet: Double;
begin
  FParar  := False;
  SQLType := stIns;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando dados cadastros');
  PB1.Position := 0;
  PB1.Max := SGSales.RowCount;
  //////////////////////////////////////////////////////////////////////////
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando cadastros de Segmentos de produtos');
  //////////////////////////////////////////////////////////////////////////
  Grl_DmkDB.AbreSQLQuery0(Query1, Dmod.MyDB, [
  'SELECT DISTINCT cse.Codigo, lsd.xSegmento',
  'FROM loadsalesdata lsd ',
  'LEFT JOIN segmentoscads cse ON cse.Nome=lsd.xSegmento',
  'WHERE cse.Codigo IS null',
  '']);
  Query1.First;
  PB1.Position := 0;
  PB1.Max := Query1.RecordCount;
  while not Query1.Eof do
  begin
    MyObjects.UpdPBOnly(PB1);
    if FParar then
      Exit;
    vNome := Query1.FieldByName('xSegmento').Value;
    if (vNome <> Null) and (Trim(vNome) <> '') then
    begin
      Sigla  := vNome;
      Nome   := vNome;
      Codigo := Grl_DmkDB.GetNxtCodigoInt('segmentoscads', 'Codigo', SQLType, 0);
      CodUsu := Codigo;
      HvSrDB := Integer(THaveSrcDB.hsdbImported);
      //
      Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, 'segmentoscads', False, [
      'Nome', 'Sigla', 'CodUsu', 'IDServr',
      'Ordem', 'HvSrDB'], [
      'Codigo'], [
      Nome, Sigla, CodUsu, CO_IDServr,
      Ordem, HvSrDB], [
      Codigo], True, dmksqlinsInsIgnore, '',
      TDeviceType.stDesktop, False);
    end;
    Query1.Next;
  end;
  Application.ProcessMessages;
  if FParar then
    Exit;
  //////////////////////////////////////////////////////////////////////////
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando cadastros de Grupos de Segmentos de produtos');
  //////////////////////////////////////////////////////////////////////////
  Grl_DmkDB.AbreSQLQuery0(Query1, Dmod.MyDB, [
  'SELECT DISTINCT cse.Codigo, lsd.xGrupoSegmento',
  'FROM loadsalesdata lsd ',
  'LEFT JOIN segmengrucads cse ON cse.Nome=lsd.xGrupoSegmento',
  'WHERE cse.Codigo IS null',
  '']);
  Query1.First;
  PB1.Position := 0;
  PB1.Max := Query1.RecordCount;
  while not Query1.Eof do
  begin
    MyObjects.UpdPBOnly(PB1);
    if FParar then
      Exit;
    vNome := Query1.FieldByName('xGrupoSegmento').Value;
    if (vNome <> Null) and (Trim(vNome) <> '') then
    begin
      Sigla  := vNome;
      Nome   := vNome;
      Codigo := Grl_DmkDB.GetNxtCodigoInt('segmengrucads', 'Codigo', SQLType, 0);
      CodUsu := Codigo;
      HvSrDB := Integer(THaveSrcDB.hsdbImported);
      //
      Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, 'segmengrucads', False, [
      'Nome', 'Sigla', 'CodUsu', 'IDServr',
      'Ordem', 'HvSrDB'], [
      'Codigo'], [
      Nome, Sigla, CodUsu, CO_IDServr,
      Ordem, HvSrDB], [
      Codigo], True, dmksqlinsInsIgnore, '',
      TDeviceType.stDesktop, False);
    end;
    Query1.Next;
  end;
  Application.ProcessMessages;
  if FParar then
    Exit;
  //////////////////////////////////////////////////////////////////////////
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando cadastros de Divis�es de produtos');
  //////////////////////////////////////////////////////////////////////////
  Grl_DmkDB.AbreSQLQuery0(Query1, Dmod.MyDB, [
  'SELECT DISTINCT cse.Codigo, lsd.xDivisao',
  'FROM loadsalesdata lsd ',
  'LEFT JOIN proddivsaocab cse ON cse.Nome=lsd.xDivisao',
  'WHERE cse.Codigo IS null',
  '']);
  Query1.First;
  PB1.Position := 0;
  PB1.Max := Query1.RecordCount;
  while not Query1.Eof do
  begin
    MyObjects.UpdPBOnly(PB1);
    if FParar then
      Exit;
    vNome := Query1.FieldByName('xDivisao').Value;
    if (vNome <> Null) and (Trim(vNome) <> '') then
    begin
      Sigla  := vNome;
      Nome   := vNome;
      Codigo := Grl_DmkDB.GetNxtCodigoInt('proddivsaocab', 'Codigo', SQLType, 0);
      CodUsu := Codigo;
      HvSrDB := Integer(THaveSrcDB.hsdbImported);
      //
      Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, 'proddivsaocab', False, [
      'Nome', 'Sigla', 'CodUsu', 'IDServr',
      'Ordem', 'HvSrDB'], [
      'Codigo'], [
      Nome, Sigla, CodUsu, CO_IDServr,
      Ordem, HvSrDB], [
      Codigo], True, dmksqlinsInsIgnore, '',
      TDeviceType.stDesktop, False);
    end;
    Query1.Next;
  end;
  Application.ProcessMessages;
  if FParar then
    Exit;
  //////////////////////////////////////////////////////////////////////////
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando cadastros de Sub-divis�es de produtos');
  //////////////////////////////////////////////////////////////////////////
  Grl_DmkDB.AbreSQLQuery0(Query1, Dmod.MyDB, [
  'SELECT DISTINCT cse.Codigo, lsd.xSubDivisao',
  'FROM loadsalesdata lsd ',
  'LEFT JOIN proddivsubcab cse ON cse.Nome=lsd.xSubDivisao',
  'WHERE cse.Codigo IS null',
  '']);
  Query1.First;
  PB1.Position := 0;
  PB1.Max := Query1.RecordCount;
  while not Query1.Eof do
  begin
    MyObjects.UpdPBOnly(PB1);
    if FParar then
      Exit;
    vNome := Query1.FieldByName('xSubDivisao').Value;
    if (vNome <> Null) and (Trim(vNome) <> '') then
    begin
      Sigla  := vNome;
      Nome   := vNome;
      Codigo := Grl_DmkDB.GetNxtCodigoInt('proddivsubcab', 'Codigo', SQLType, 0);
      CodUsu := Codigo;
      HvSrDB := Integer(THaveSrcDB.hsdbImported);
      //
      Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, 'proddivsubcab', False, [
      'Nome', 'Sigla', 'CodUsu', 'IDServr',
      'Ordem', 'HvSrDB'], [
      'Codigo'], [
      Nome, Sigla, CodUsu, CO_IDServr,
      Ordem, HvSrDB], [
      Codigo], True, dmksqlinsInsIgnore, '',
      TDeviceType.stDesktop, False);
    end;
    Query1.Next;
  end;
  Application.ProcessMessages;
  if FParar then
    Exit;
  //////////////////////////////////////////////////////////////////////////
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando cadastros de Linhas de produtos');
  //////////////////////////////////////////////////////////////////////////
  Grl_DmkDB.AbreSQLQuery0(Query1, Dmod.MyDB, [
  'SELECT DISTINCT cse.Codigo, lsd.xLinha',
  'FROM loadsalesdata lsd ',
  'LEFT JOIN prodlinhascab cse ON cse.Nome=lsd.xLinha',
  'WHERE cse.Codigo IS null',
  '']);
  Query1.First;
  PB1.Position := 0;
  PB1.Max := Query1.RecordCount;
  while not Query1.Eof do
  begin
    MyObjects.UpdPBOnly(PB1);
    if FParar then
      Exit;
    vNome := Query1.FieldByName('xLinha').Value;
    if (vNome <> Null) and (Trim(vNome) <> '') then
    begin
      Sigla  := vNome;
      Nome   := vNome;
      Codigo := Grl_DmkDB.GetNxtCodigoInt('prodlinhascab', 'Codigo', SQLType, 0);
      CodUsu := Codigo;
      HvSrDB := Integer(THaveSrcDB.hsdbImported);
      //
      Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, 'prodlinhascab', False, [
      'Nome', 'Sigla', 'CodUsu', 'IDServr',
      'Ordem', 'HvSrDB'], [
      'Codigo'], [
      Nome, Sigla, CodUsu, CO_IDServr,
      Ordem, HvSrDB], [
      Codigo], True, dmksqlinsInsIgnore, '',
      TDeviceType.stDesktop, False);
    end;
    Query1.Next;
  end;
  Application.ProcessMessages;
  if FParar then
    Exit;
  //////////////////////////////////////////////////////////////////////////
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Perfis');
  //////////////////////////////////////////////////////////////////////////
  Grl_DmkDB.AbreSQLQuery0(Query1, Dmod.MyDB, [
  'SELECT distinct lsd.xPerfil, spe.Codigo',
  'FROM loadsalesdata lsd',
  'LEFT JOIN salesperfilcl spe ON spe.Nome=lsd.xPerfil',
  'WHERE spe.Codigo IS null',
  '']);
  Query1.First;
  PB1.Position := 0;
  PB1.Max := Query1.RecordCount;
  while not Query1.Eof do
  begin
    MyObjects.UpdPBOnly(PB1);
    if FParar then
      Exit;
    Codigo              := 0;
    Nome                := Query1.FieldByName('xPerfil').AsString;
    if (LowerCase(Nome) = 'intercompany') then
      EhFatura             := 0
    else
      EhFatura             := 1;
    //
    //PesoLiq             := ;
    //PesoBru             := ;
    //PPalLiq             := ;
    //
    Codigo := Grl_DmkDB.GetNxtCodigoInt('salesperfilcl', 'Codigo', SQLType, 0);
    CodUsu := Codigo;
    HvSrDB := Integer(THaveSrcDB.hsdbImported);
    //if
    Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, 'salesperfilcl', False, [
    'Nome', 'EhFatura', 'CodUsu', 'IDServr',
    'HvSrDB'], [
    'Codigo'], [
    Nome, EhFatura, CodUsu, CO_IDServr,
    HvSrDB], [
    Codigo], True, dmksqlinsInsIgnore, '', stDesktop, False);
      //
   Query1.Next;
  end;
  Application.ProcessMessages;
  if FParar then
    Exit;
  //////////////////////////////////////////////////////////////////////////
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando cadastros de Grupos de Empresas');
  //////////////////////////////////////////////////////////////////////////
  Grl_DmkDB.AbreSQLQuery0(Query1, Dmod.MyDB, [
  'SELECT DISTINCT egr.Codigo, lsd.xGrupoEmpresa ',
  'FROM loadsalesdata lsd ',
  'LEFT JOIN entidadesgrup egr ON egr.Nome=lsd.xGrupoEmpresa ',
  'WHERE egr.Codigo IS null ',
  '']);
  Query1.First;
  PB1.Position := 0;
  PB1.Max := Query1.RecordCount;
  while not Query1.Eof do
  begin
    MyObjects.UpdPBOnly(PB1);
    if FParar then
      Exit;
    vNome := Query1.FieldByName('xGrupoEmpresa').Value;
    if (vNome <> Null) and (Trim(vNome) <> '') then
    begin
      Nome   := vNome;
      Grl_DmkDB.AbreSQLQuery0(Query2, Dmod.MyDB, [
      'SELECT MIN(cCodCli) cCodCli_Min ',
      'FROM loadsalesdata ',
      'WHERE Lower(xGrupoEmpresa)= Lower("' + Nome + '") ',
      '']);
      vCodigo := Query2.FieldByName('cCodCli_Min').Value;
      if (vCodigo <> Null) and (vCodigo > 0) then
      begin
        Codigo := vCodigo;
        CodUsu := Codigo;
        HvSrDB := Integer(THaveSrcDB.hsdbImported);
        //
        //Codigo := Grl_DmkDB.GetNxtCodigoInt('entidadesgrup', 'Codigo', SQLType, 0);
        //
        Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, 'EntidadesGrup', False, [
        'Nome', 'CodUsu', 'IDServr', 'HvSrDB'], [
        'Codigo'], [
        Nome, CodUsu, CO_IDServr, HvSrDB], [
        Codigo], True, dmksqlinsInsIgnore, '',
        TDeviceType.stDesktop, False);
      end;
    end;
    Query1.Next;
  end;
  Application.ProcessMessages;
  if FParar then
    Exit;
  //////////////////////////////////////////////////////////////////////////
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando Cadastros de Empresas');
  //////////////////////////////////////////////////////////////////////////
  Grl_DmkDB.AbreSQLQuery0(Query1, Dmod.MyDB, [
  'SELECT DISTINCT emp.Codigo, lsd.xCliente, ',
  'lsd.xGrupoEmpresa, egr.Codigo cGrupoEmpresa,',
  'xPais, xRegiao, xUF, xCidade, xPerfil, /*cUnit,*/ cCodCli ',
  'FROM loadsalesdata lsd ',
  'LEFT JOIN entidadesempr emp ON emp.Nome=lsd.xCliente ',
  'LEFT JOIN entidadesgrup egr ON egr.Nome=lsd.xGrupoEmpresa',
  'WHERE emp.Codigo IS null ',
  '']);
  Query1.First;
  PB1.Position := 0;
  PB1.Max := Query1.RecordCount;
  while not Query1.Eof do
  begin
    MyObjects.UpdPBOnly(PB1);
    if FParar then
      Exit;
    vCodigo := Query1.FieldByName('cCodCli').Value;
    if vCodigo <> null then
    begin
      GrupEmprSN          := 0;
      Codigo              := vCodigo;
      CodUsu              := Codigo;
      HvSrDB              := Integer(THaveSrcDB.hsdbImported);
      Nome                := Query1.FieldByName('xCliente').AsString;
      GrupoEmpresa        := Query1.FieldByName('cGrupoEmpresa').AsInteger;
      xPais               := Query1.FieldByName('xPais').AsString;
      xRegiao             := Query1.FieldByName('xRegiao').AsString;
      xUF                 := Query1.FieldByName('xUF').AsString;
      xCidade             := Query1.FieldByName('xCidade').AsString;
      xPerfil             := Query1.FieldByName('xPerfil').AsString;
      GrupEmprSN          := 0;
      //
      Grl_DmkDB.AbreSQLQuery0(Query2, Dmod.MyDB, [
      'SELECT Codigo ',
      'FROM salesperfilcl ',
      'WHERE Lower(Nome) = Lower("' + LowerCase(xPerfil) + '") ',
      '']);
      Perfil := Query2.FieldByName('Codigo').Value;
      //cUnit               := Query1.FieldByName('cUnit').AsInteger;
      //if
      //
      //Codigo := Grl_DmkDB.GetNxtCodigoInt('entidadesempr', 'Codigo', SQLType, 0);
      //
      Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, 'EntidadesEmpr', False, [
      'Nome', 'GrupoEmpresa', 'Pais',
      'Regiao', 'UF', 'Cidade',
      'Perfil', 'GrupEmprSN', 'CodUsu', 'IDServr',
      'HvSrDB'], [
      'Codigo'], [
      Nome, GrupoEmpresa, xPais,
      xRegiao, xUF, xCidade,
      Perfil, GrupEmprSN, CodUsu, CO_IDServr,
      HvSrDB], [
      Codigo], True, dmksqlinsInsIgnore, '', TDeviceType.stDesktop, False);
    end;
    Query1.Next;
  end;
  Application.ProcessMessages;
  if FParar then
    Exit;
  //////////////////////////////////////////////////////////////////////////
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando Cadastros de Accounts');
  //////////////////////////////////////////////////////////////////////////
  Grl_DmkDB.AbreSQLQuery0(Query1, Dmod.MyDB, [
  'SELECT DISTINCT acn.Codigo, lsd.xAccount',
  'FROM loadsalesdata lsd ',
  'LEFT JOIN entidadesaccn acn ON acn.Nome=lsd.xAccount',
  'WHERE acn.Codigo IS null ',
  '']);
  //
  Query1.First;
  PB1.Position := 0;
  PB1.Max := Query1.RecordCount;
  while not Query1.Eof do
  begin
    MyObjects.UpdPBOnly(PB1);
    if FParar then
      Exit;
    Nome                := Query1.FieldByName('xAccount').AsString;
    Sigla               := '';
    //
    Codigo := Grl_DmkDB.GetNxtCodigoInt('entidadesaccn', 'Codigo', SQLType, 0);
    CodUsu := Codigo;
    HvSrDB := Integer(THaveSrcDB.hsdbImported);
    //
    //if
    Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, 'entidadesaccn', False, [
    'Nome', 'Sigla', 'CodUsu', 'IDServr',
    'HvSrDB'], [
    'Codigo'], [
    Nome, Sigla, CodUsu, CO_IDServr,
    HvSrDB], [
    Codigo], True,  dmksqlinsInsOnly, '', TDeviceType.stDesktop, False);
    //
    Query1.Next;
  end;
  Application.ProcessMessages;
  if FParar then
    Exit;
  //////////////////////////////////////////////////////////////////////////
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Segmento X Cliente X Account');
  //////////////////////////////////////////////////////////////////////////
  Grl_DmkDB.AbreSQLQuery0(Query1, Dmod.MyDB, [
  'SELECT DISTINCT lsd.xSegmento, lsd.xAccount, lsd.cCodCli, ',
  'sgc.Codigo cSegmento, eac.Codigo cAccount ',
  'FROM loadsalesdata lsd ',
  'LEFT JOIN segmentoscads sgc ON sgc.Sigla=lsd.xSegmento ',
  'LEFT JOIN entidadesaccn eac ON eac.Nome=lsd.xAccount ',
  '']);
  Query1.First;
  PB1.Position := 0;
  PB1.Max := Query1.RecordCount;
  while not Query1.Eof do
  begin
    MyObjects.UpdPBOnly(PB1);
    if FParar then
      Exit;
    Codigo              := 0;
    Segmento            := Query1.FieldByName('cSegmento').AsInteger;
    EntidadesEmp        := Query1.FieldByName('cCodCli').AsInteger;
    Account             := Query1.FieldByName('cAccount').AsInteger;
    AccntSN             := Account;
    //
    Grl_DmkDB.AbreSQLQuery0(Query2, Dmod.MyDB, [
    'SELECT Segmento, EntidadesEmp, Account, AccntSN, Codigo',
    'FROM segmentossgea ',
    'WHERE Segmento=' + Geral.FF0(Segmento),
    'AND EntidadesEmp=' + Geral.FF0(EntidadesEmp),
    //'AND AccntSN=' + Geral.FF0(AccntSN),
    '']);
    //Geral.MB_SQL(Self, Query2);
    if Query2.RecordCount = 0 then
    begin
      Codigo := Grl_DmkDB.GetNxtCodigoInt('segmentossgea', 'Codigo', SQLType, 0);
      CodUsu := Codigo;
      HvSrDB := Integer(THaveSrcDB.hsdbImported);
      //if
      Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, 'segmentossgea', False, [
      'Segmento', 'EntidadesEmp', 'Account',
      'AccntSN', 'CodUsu', 'IDServr', 'HvSrDB'], [
      'Codigo'], [
      Segmento, EntidadesEmp, Account,
      AccntSN, CodUsu, CO_IDServr, HvSrDB], [
      Codigo], True, dmksqlinsInsOnly, '', TDeviceType.stDesktop, False);
    end else
    begin
      if Query2.FieldByName('AccntSN').AsInteger <> Account then
      begin
        Codigo := Query2.FieldByName('Codigo').AsInteger;
        //CodUsu := ;
        HvSrDB := Integer(THaveSrcDB.hsdbImported);
        //if
        Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, (*SQLType*)stUpd, 'segmentossgea', False, [
        //'Segmento', 'EntidadesEmp', 'Account',
        'AccntSN', (*'CodUsu', 'IDServr',*) 'HvSrDB'], [
        'Codigo'], [
        //Segmento, EntidadesEmp, Account,
        AccntSN, (*CodUsu, CO_IDServr,*) HvSrDB], [
        Codigo], True, dmksqlinsInsOnly, '', stDesktop, False);
      end;
    end;
    //
    Query1.Next;
  end;
  Application.ProcessMessages;
  if FParar then
    Exit;
{
  //////////////////////////////////////////////////////////////////////////
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Embalagens');
  //////////////////////////////////////////////////////////////////////////
  Grl_DmkDB.AbreSQLQuery0(Query1, Dmod.MyDB, [
  'SELECT DISTINCT lsd.xEmbalagem, emb.Codigo ',
  'FROM loadsalesdata lsd  ',
  'LEFT JOIN embalagenscad emb ON emb.Sigla=lsd.xEmbalagem ',
  'WHERE emb.Codigo IS null ',
  '']);
  Query1.First;
  PB1.Position := 0;
  PB1.Max := Query1.RecordCount;
  while not Query1.Eof do
  begin
    MyObjects.UpdPBOnly(PB1);
    if FParar then
      Exit;
    Codigo              := 0;
    Sigla               := Query1.FieldByName('xEmbalagem').AsString;
    Nome                := Sigla;
    Obtem_SiglEmbal_e_Peso(Sigla, SiglEmbal, PesoLiq);
    PesoBru             := 0;
    PPalLiq             := 0;
    Ordem               := 0;
    HvSrDB := Integer(THaveSrcDB.hsdbImported);
    //
    Codigo := Grl_DmkDB.GetNxtCodigoInt('embalagenscad', 'Codigo', SQLType, 0);
    CodUsu := Codigo;
    //if
    Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, 'embalagenscad', False, [
    'CodUsu', 'IDServr', 'Nome', 'Sigla',
    'SiglEmbal', 'PesoLiq', 'PesoBru',
    'PPalLiq', 'Ordem', 'HvSrDB'], [
    'Codigo'], [
    CodUsu, CO_IDServr, Nome, Sigla,
    SiglEmbal, PesoLiq, PesoBru,
    PPalLiq, Ordem, HvSrDB], [
    Codigo], True, dmksqlinsInsIgnore, '', stDesktop, False);
    //
    Query1.Next;
  end;
  Application.ProcessMessages;
  if FParar then
    Exit;
}
  //////////////////////////////////////////////////////////////////////////
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Tipos e Taras de Embalagens');
  //////////////////////////////////////////////////////////////////////////
  Grl_DmkDB.AbreSQLQuery0(Query1, Dmod.MyDB, [
  'SELECT DISTINCT lsd.xEmbalagem, tar.Codigo ',
  'FROM loadsalesdata lsd  ',
  'LEFT JOIN embalagenstar tar ON tar.Nome=lsd.xEmbalagem ',
  'WHERE tar.Codigo IS null ',
  (*
  'SELECT distinct emb.Sigla, emb.SiglEmbal, emb.PesoLiq ',
  'FROM embalagenscad emb ',
  'LEFT JOIN embalagenstar tar ON tar.Sigla=emb.Sigla ',
  'WHERE tar.Codigo IS null ',
  'ORDER BY emb.Sigla, emb.SiglEmbal, emb.PesoLiq ',
  *)
  '']);
  Query1.First;
  PB1.Position := 0;
  PB1.Max := Query1.RecordCount;
  while not Query1.Eof do
  begin
    MyObjects.UpdPBOnly(PB1);
    if FParar then
      Exit;
    Codigo              := 0;
    Nome                := Query1.FieldByName('xEmbalagem').AsString;
    Obtem_SiglEmbal_e_Peso(Nome, Sigla, PesoLiq);
    //PesoLiq             := Query1.FieldByName('PesoLiq').AsFloat;
    PPallet             := 0;
    //
    //'FBX', 'IBC', 'IBX', 'PAB', 'PBX', 'PDR', 'PDX', 'PJC', 'PLB', 'PLU');
    //PPallet             := ;
    I := IniceSigla(Sigla);
    case I of
      (*FBX*) 0: PesoTar :=  2.0;
      (*IBC*) 1: PesoTar := 65.0;
      (*IBX*) 2: PesoTar := 65.0;
      (*PAB*) 3: PesoTar :=  0.2;
      (*PBX*) 4: PesoTar :=  2.0;
      (*PDR*) 5,
      (*PDX*) 6,
      (*PJC*) 7:
      begin
        case Trunc(PesoLiq) of
  //BB	    20 kg	        2 kg     + 30 kg pallet	        18
          000..020: PesoTar := 2;
  //BB	    30 kg	        2 kg     + 30 kg pallet	        18
          021..030: PesoTar := 2;
  //BB	    60 kg	        3 kg     + 30 kg pallet	         9
          031..060: PesoTar := 3;
  //BB	    80 a 120 kg	  6 kg     + 30 kg pallet	         5
          061..120: PesoTar := 6;
  //BB	    180 a 220 kg	10 kg    + 30 kg pallet	         4
          121..300: PesoTar := 10;
          else PesoTar := PesoLiq * 0.1;
        end;
      end;
      (*PLB*) 8: PesoTar := 0.2;
      (*PLU*) 9: PesoTar := PesoLiq / 25 * 0.2;
    end;
    //
    //ItmsInPalt          := ;
    case I of
      (*FBX*) 0: ItmsInPalt := 27;
      (*IBC*) 1: ItmsInPalt := 0;
      (*IBX*) 2: ItmsInPalt := 0;
      (*PAB*) 3: ItmsInPalt := Trunc(PesoLiq / 25);
      (*PBX*) 4: ItmsInPalt := 27;
      (*PDR*) 5,
      (*PDX*) 6,
      (*PJC*) 7:
      begin
        case Trunc(PesoLiq) of
  //BB	    20 kg	        2 kg     + 30 kg pallet	        18
          000..020: ItmsInPalt := 18;
  //BB	    30 kg	        2 kg     + 30 kg pallet	        18
          021..030: ItmsInPalt := 18;
  //BB	    60 kg	        3 kg     + 30 kg pallet	         9
          031..060: ItmsInPalt := 9;
  //BB	    80 a 120 kg	  6 kg     + 30 kg pallet	         5
          061..120: ItmsInPalt := 5;
  //BB	    180 a 220 kg	10 kg    + 30 kg pallet	         4
          121..300: ItmsInPalt := 4;
          else ItmsInPalt := 1000 div Trunc(PesoLiq);
        end;
      end;
      (*PLB*) 8: ItmsInPalt := 1050 div Trunc(PesoLiq);
      (*PLU*) 9: ItmsInPalt := 0;//Trunc(PesoLiq) div 25;
    end;

    Ordem               := 0;
    HvSrDB := Integer(THaveSrcDB.hsdbImported);
    //
(*
Bem	    Kg	          Peso	   Pallet               Cabem..
BB	    20 kg	        2 kg     + 30 kg pallet	        18
BB	    30 kg	        2 kg     + 30 kg pallet	        18
BB	    60 kg	        3 kg     + 30 kg pallet	         9
BB	    80 a 120 kg	  6 kg     + 30 kg pallet	         5
BB	    180 a 220 kg	10 kg    + 30 kg pallet	         4
IBC	    950 A 1200	  65 kg	                           1
Pallet	500 a 1050	  30 kg pallet
*)
    //
    Grl_DmkDB.AbreSQLQuery0(Query2, Dmod.MyDB, [
    'SELECT Codigo, Sigla ',
    'FROM embalagenscad ',
    'WHERE Lower(Sigla)= Lower("' + Sigla + '") ',
    '']);
    if Query2.RecordCount > 0 then
    begin
      EmbalagensCad := Query2.FieldByName('Codigo').AsInteger;
    end else
    begin
      EmbalagensCad := Grl_DmkDB.GetNxtCodigoInt('embalagenscad', 'Codigo', SQLType, 0);
      //
      if not
      Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, 'embalagenscad', False, [
      'CodUsu', 'IDServr', 'Nome', 'Sigla',
      'Ordem', 'HvSrDB'], [
      'Codigo'], [
      EmbalagensCad(*CodUsu*), CO_IDServr, (*Nome*)Sigla, Sigla,
      0(*Ordem*), HvSrDB], [
      (*Codigo*)EmbalagensCad], True, dmksqlinsInsOnly, '', stDesktop, False) then
        Exit;
    end;
    Codigo := Grl_DmkDB.GetNxtCodigoInt('embalagenstar', 'Codigo', SQLType, 0);
    CodUsu := Codigo;
    //if
    Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, 'embalagenstar', False, [
    'CodUsu', 'IDServr', 'Nome', //'Descricao',
    'EmbalagensCad', 'Sigla', 'PesoLiq',
    'PesoTar', 'PPallet', 'ItmsInPalt',
    'Ordem', 'HvSrDB'], [
    'Codigo'], [
    CodUsu, CO_IDServr, Nome, //Descricao,
    EmbalagensCad, Sigla, PesoLiq,
    PesoTar, PPallet, ItmsInPalt,
    Ordem, HvSrDB], [
(*
    'CodUsu', 'IDServr', 'Nome', 'Sigla',
    'PesoLiq', 'PesoTar', 'PPallet',
    'ItmsInPalt', 'Ordem', 'HvSrDB'], [
    'Codigo'], [
    CodUsu, CO_IDServr, Nome, Sigla,
    PesoLiq, PesoTar, PPallet,
    ItmsInPalt, Ordem, HvSrDB], [
*)
    Codigo], True, dmksqlinsInsOnly, '', stDesktop, False);
    //
    Query1.Next;
  end;
  Application.ProcessMessages;
  if FParar then
    Exit;
  //////////////////////////////////////////////////////////////////////////
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Fam�lias de produtos');
  //////////////////////////////////////////////////////////////////////////
  Grl_DmkDB.AbreSQLQuery0(Query1, Dmod.MyDB, [
  'SELECT distinct lsd.xFamilia, prf.Codigo',
  'FROM loadsalesdata lsd',
  'LEFT JOIN produtosfamil prf ON prf.Nome=lsd.xFamilia',
  'WHERE prf.Codigo IS null',
  '']);
  Query1.First;
  PB1.Position := 0;
  PB1.Max := Query1.RecordCount;
  while not Query1.Eof do
  begin
    MyObjects.UpdPBOnly(PB1);
    if FParar then
      Exit;
    Codigo              := 0;
    Nome                := Query1.FieldByName('xFamilia').AsString;
    Sigla               := Nome;
    //
    Codigo := Grl_DmkDB.GetNxtCodigoInt('produtosfamil', 'Codigo', SQLType, 0);
    CodUsu := Codigo;
    HvSrDB := Integer(THaveSrcDB.hsdbImported);
    //if
    Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, 'produtosfamil', False, [
    'Nome', 'Sigla',
    'CodUsu', 'IDServr', 'HvSrDB'], [
    'Codigo'], [
    Nome, Sigla,
    CodUsu, CO_IDServr, HvSrDB], [
    Codigo], True, dmksqlinsInsReplace, '', TDeviceType.stDesktop, False);
    //
   Query1.Next;
  end;
  Application.ProcessMessages;
  if FParar then
    Exit;
  //////////////////////////////////////////////////////////////////////////
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Produtos');
  //////////////////////////////////////////////////////////////////////////
  Grl_DmkDB.AbreSQLQuery0(Query1, Dmod.MyDB, [
  'SELECT DISTINCT lsd.xProduto, ',
  'sgr.Codigo GrupoSegmento, sca.Codigo Segmento,',
  'pdv.Codigo Divisao, psd.Codigo SubDivisao,',
  'prf.Codigo cFamilia,',
  'prd.Codigo',
  'FROM loadsalesdata lsd',
  'LEFT JOIN produtoscadas prd ON prd.Nome=lsd.xProduto',
  'LEFT JOIN segmengrucads sgr ON sgr.Nome=lsd.xGrupoSegmento',
  'LEFT JOIN segmentoscads sca ON sca.Nome=lsd.xSegmento',
  'LEFT JOIN proddivsaocab pdv ON pdv.Nome=lsd.xDivisao',
  'LEFT JOIN proddivsubcab psd ON psd.Nome=lsd.xSubDivisao',
  'LEFT JOIN produtosfamil prf ON prf.Nome=lsd.xFamilia',
  'WHERE prd.Codigo IS null',
  '']);
  Query1.First;
  PB1.Position := 0;
  PB1.Max := Query1.RecordCount;
  while not Query1.Eof do
  begin
    MyObjects.UpdPBOnly(PB1);
    if FParar then
      Exit;
    Codigo              := 0;
    Nome                := Query1.FieldByName('xProduto').AsString;
    EntidadesCptt       := 1; // TFL !!!
    ProdCabec           := 0;
    GrupoSegmento       := Query1.FieldByName('GrupoSegmento').AsInteger;
    Segmento            := Query1.FieldByName('Segmento').AsInteger;
    Divisao             := Query1.FieldByName('Divisao').AsInteger;
    SubDivisao          := Query1.FieldByName('SubDivisao').AsInteger;
    Familia             := Query1.FieldByName('cFamilia').AsInteger;
    //
    Grl_DmkDB.AbreSQLQuery0(Query2, Dmod.MyDB, [
    'SELECT MIN(cCodProd) cCodProd_Min ',
    'FROM loadsalesdata ',
    'WHERE Lower(xProduto)= Lower("' + Nome + '") ',
    '']);
    vCodigo := Query2.FieldByName('cCodProd_Min').Value;
    if (vCodigo <> Null) and (vCodigo > 0) then
    begin
      Codigo := Grl_DmkDB.GetNxtCodigoInt('produtoscadas', 'Codigo', SQLType, 0);
      //Codigo := vCodigo;
      CodUsu := vCodigo;
      HvSrDB := Integer(THaveSrcDB.hsdbImported);
      Lista  := Integer(TLstProdQuim.lpqRefere);
      //if
      Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, 'produtoscadas', False, [
      'Lista', 'CodUsu', 'IDServr',
      'EntidadesCptt', 'ProdCabec', 'GrupoSegmento',
      'Segmento', 'Divisao', 'SubDivisao',
      'Familia', 'Nome', 'HvSrDB'], [
      'Codigo'], [
      Lista, CodUsu, CO_IDServr,
      EntidadesCptt, ProdCabec, GrupoSegmento,
      Segmento, Divisao, SubDivisao,
      Familia, Nome, HvSrDB], [
      Codigo], True, dmksqlinsInsIgnore, '', TDeviceType.stDesktop, False);
      //
    end;
    Query1.Next;
  end;
  Application.ProcessMessages;
  if FParar then
    Exit;
  //////////////////////////////////////////////////////////////////////////
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Produtos Embalados');
  //////////////////////////////////////////////////////////////////////////
  Grl_DmkDB.AbreSQLQuery0(Query1, Dmod.MyDB, [
  'SELECT distinct lsd.xProduto, lsd.xEmbalagem,  ',
  'lsd.cCodProd, prd.Codigo ProdutosCadas, ',
  'emb.Codigo embalagenstar',
  'FROM loadsalesdata lsd ',
  'LEFT JOIN produtoscadas prd ON prd.Nome=lsd.xProduto ',
  'LEFT JOIN embalagenstar emb ON emb.Nome=lsd.xEmbalagem',
  'LEFT JOIN produtosembal prb ON prb.EmbalagensTar=emb.Codigo',
  '                           AND prb.ProdutosCadas=prd.Codigo',
  'WHERE prb.Codigo IS null ',
  'ORDER BY lsd.xProduto ',
  '']);
  Query1.First;
  PB1.Position := 0;
  PB1.Max := Query1.RecordCount;
  while not Query1.Eof do
  begin
    MyObjects.UpdPBOnly(PB1);
    if FParar then
      Exit;
    Codigo              := 0;
    CodProd             := Query1.FieldByName('cCodProd').AsInteger;
    Nome                := Query1.FieldByName('xProduto').AsString + ' ' +
                           Query1.FieldByName('xEmbalagem').AsString;
    ProdutosCadas       := Query1.FieldByName('ProdutosCadas').AsInteger;
    EmbalagensTar       := Query1.FieldByName('EmbalagensTar').AsInteger;
    //PesoLiq             := ;
    //PesoBru             := ;
    //PPalLiq             := ;
    //
    //Codigo := CodProd;//
    Codigo := Grl_DmkDB.GetNxtCodigoInt('produtosembal', 'Codigo', SQLType, 0);
    CodUsu := Codigo;
    HvSrDB := Integer(THaveSrcDB.hsdbImported);
    //if
    Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, 'produtosembal', False, [
    'CodUsu', 'IDServr', 'CodProd', 'ProdutosCadas',
    'EmbalagensTar', 'Nome', 'HvSrDB'], [
    'Codigo'], [
    CodUsu, CO_IDServr, CodProd, ProdutosCadas,
    EmbalagensTar, Nome, HvSrDB], [
    Codigo], True, dmksqlinsInsIgnore, '', stDesktop, False);
{
    Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, 'produtosembal', False, [
    'CodProd', 'Nome', 'CodUsu',  'IDServr',
    'HvSrDB'
    (*, 'PesoLiq', 'PesoBru', 'PPalLiq'*)], [
    'Codigo'], [
    CodProd, Nome, CodUsu, CO_IDServr,
    HvSrDB
    (*, PesoLiq, PesoBru, PPalLiq*)], [
    Codigo], True, dmksqlinsInsIgnore, '', stDesktop, False);
}
      //
   Query1.Next;
  end;
  Application.ProcessMessages;
  if FParar then
    Exit;
  //////////////////////////////////////////////////////////////////////////
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Opera��es');
  //////////////////////////////////////////////////////////////////////////
  Grl_DmkDB.AbreSQLQuery0(Query1, Dmod.MyDB, [
  'SELECT distinct lsd.xOperacao, sop.Codigo',
  'FROM loadsalesdata lsd',
  'LEFT JOIN salesoperacao sop ON sop.Nome=lsd.xOperacao',
  'WHERE sop.Codigo IS null',
  '']);
  Query1.First;
  PB1.Position := 0;
  PB1.Max := Query1.RecordCount;
  while not Query1.Eof do
  begin
    MyObjects.UpdPBOnly(PB1);
    if FParar then
      Exit;
    Codigo              := 0;
    Nome                := Query1.FieldByName('xOperacao').AsString;
    if (LowerCase(Nome) = 'venda')
    or (LowerCase(Nome) = 'importa��o direta') then
      EhFatura             := 1
    else
      EhFatura             := 0;
    //
    //PesoLiq             := ;
    //PesoBru             := ;
    //PPalLiq             := ;
    //
    Codigo := Grl_DmkDB.GetNxtCodigoInt('salesoperacao', 'Codigo', SQLType, 0);
    CodUsu := Codigo;
    HvSrDB := Integer(THaveSrcDB.hsdbImported);
    //if
    Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, 'salesoperacao', False, [
    'Nome', 'EhFatura', 'CodUsu', 'IDServr',
    'HvSrDB'], [
    'Codigo'], [
    Nome, EhFatura, CodUsu, CO_IDServr,
    HvSrDB], [
    Codigo], True, dmksqlinsInsIgnore, '', stDesktop, False);
      //
   Query1.Next;
  end;
  Application.ProcessMessages;
  if FParar then
    Exit;

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Equival�ncia Intercambial de Produtos');
  //////////////////////////////////////////////////////////////////////////
  Grl_DmkDB.AbreSQLQuery0(Query1, Dmod.MyDB, [
  'SELECT * ',
  'FROM produtoscadas  ',
  'WHERE ProdCabec=0 ',
  'ORDER BY Nome ',
  '']);
  if Query1.RecordCount > 0 then
  begin
    if Grl_DmkDB.CriaFm(TFmLoadSalesGen, FmLoadSalesGen, afmoLiberado) then
    begin
      FmLoadSalesGen.ReopenProdutosCadas(0);
      FmLoadSalesGen.ShowModal;
      FmLoadSalesGen.Destroy;
    end;
  end;
  //////////////////////////////////////////////////////////////////////////
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  //////////////////////////////////////////////////////////////////////////
end;

function TFmLoadSales.ObtemValCell_Dta(const Coluna, Linha: Integer;
  var Res: TDateTime): Boolean;
var
  Txt: String;
begin
{
  Result := False;
  Txt := Trim(SGSales.Cells[Coluna, Linha]);
  if Txt <> '' then
  begin
    Res := Geral.ValidaDataBR(Txt, False, True);
    Result := True;
  end else
    Geral.MB_Info('N�o foi poss�vel obter o valor da c�lula: coluna ' +
    Geral.FF0(Coluna) + ', linha ' + Geral.FF0(Linha));
}
  Txt := Trim(SGSales.Cells[Coluna, Linha]);
  if Txt <> '' then
    Res := Geral.ValidaDataBR(Txt, False, True);
  Result := True;
end;

function TFmLoadSales.ObtemValCell_Int(const Coluna, Linha: Integer;
  var Res: Integer): Boolean;
var
  Txt: String;
begin
{
  Result := False;
  Txt := Trim(SGSales.Cells[Coluna, Linha]);
  if Txt <> '' then
  begin
    Res := Geral.IMV(Txt);
    Result := True;
  end else
    Geral.MB_Info('N�o foi poss�vel obter o valor da c�lula: coluna ' +
    Geral.FF0(Coluna) + ', linha ' + Geral.FF0(Linha));
}
  Txt := Trim(SGSales.Cells[Coluna, Linha]);
  if Txt <> '' then
    Res := Geral.IMV(Txt);
  Result := True;
end;

end.


