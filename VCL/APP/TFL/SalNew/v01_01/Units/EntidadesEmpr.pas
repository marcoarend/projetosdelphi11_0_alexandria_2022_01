unit EntidadesEmpr;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, (***UnGOTOy,***) UnInternalConsts, UnMsgInt, (***UMySQLModule,***)
  Menus, UnInternalConsts2, mySQLDbTables, (***UnMySQLCuringa,***)
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  Grids, DBGrids, Variants,
  UnDmkProcFunc, UnDmkENums, ZAbstractRODataset, ZDataset, ZAbstractDataset,
  //
  UnGOTOz, UnGrl_DmkDB, dmkDBLookupComboBox, dmkEditCB, dmkEditF7, dmkDBGridZTO;

type
  TFmEntidadesEmpr = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DGDados: TdmkDBGridZTO;
    QrEntidadesEmpr: TZQuery;
    DsEntidadesEmpr: TDataSource;
    QrSegmentosSGEA: TZQuery;
    DsSegmentosSGEA: TDataSource;
    DBEdNome: TdmkDBEdit;
    Label4: TLabel;
    DBEdCodUsu: TdmkDBEdit;
    Label2: TLabel;
    QrEntidadesEmprCodigo: TLargeintField;
    QrEntidadesEmprCodUsu: TLargeintField;
    QrEntidadesEmprNome: TWideStringField;
    QrEntidadesEmprGrupoEmpresa: TLargeintField;
    QrEntidadesEmprGrupEmprSN: TLargeintField;
    QrEntidadesEmprPais: TWideStringField;
    QrEntidadesEmprRegiao: TWideStringField;
    QrEntidadesEmprUF: TWideStringField;
    QrEntidadesEmprCidade: TWideStringField;
    QrEntidadesEmprLegalUnit: TLargeintField;
    QrEntidadesEmprHvSrDB: TSmallintField;
    QrEntidadesEmprLk: TLargeintField;
    QrEntidadesEmprDataCad: TDateField;
    QrEntidadesEmprDataAlt: TDateField;
    QrEntidadesEmprUserCad: TLargeintField;
    QrEntidadesEmprUserAlt: TLargeintField;
    QrEntidadesEmprAlterWeb: TSmallintField;
    QrEntidadesEmprAtivo: TSmallintField;
    QrEntidadesEmprNO_GrupoEmpresa: TWideStringField;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label5: TLabel;
    DBEdit3: TDBEdit;
    Label6: TLabel;
    DBEdit4: TDBEdit;
    Label8: TLabel;
    DBEdit5: TDBEdit;
    Label10: TLabel;
    DBEdit6: TDBEdit;
    Label11: TLabel;
    DBEdit7: TDBEdit;
    Label12: TLabel;
    DBEdit8: TDBEdit;
    Label13: TLabel;
    DBEdit9: TDBEdit;
    Label14: TLabel;
    DBEdit10: TDBEdit;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    EdCodUsu: TdmkEdit;
    EdGrupoEmpresa: TdmkEditCB;
    CBGrupoEmpresa: TdmkDBLookupComboBox;
    QrEntidadesGrup: TZQuery;
    QrEntidadesGrupCodigo: TLargeintField;
    QrEntidadesGrupCodUsu: TLargeintField;
    QrEntidadesGrupNome: TWideStringField;
    DsEntidadesGrup: TDataSource;
    EdGrupEmprSN: TdmkEdit;
    EdCidade: TdmkEditF7;
    EdUF: TdmkEdit;
    EdRegiao: TdmkEditF7;
    EdPais: TdmkEditF7;
    EdHvSrDB: TdmkEditF7;
    EdLegalUnit: TdmkEdit;
    EdPerfil: TdmkEditCB;
    Label21: TLabel;
    CBPerfil: TdmkDBLookupComboBox;
    QrSalesPerfilCl: TZQuery;
    DsSalesPerfilCl: TDataSource;
    QrSalesPerfilClCodigo: TLargeintField;
    QrSalesPerfilClCodUsu: TLargeintField;
    QrSalesPerfilClNome: TWideStringField;
    QrEntidadesEmprNO_Perfil: TWideStringField;
    QrEntidadesEmprPerfil: TLargeintField;
    DBEdit11: TDBEdit;
    QrEntidadesEmprNO_HvSrDB: TWideMemoField;
    QrSegmentosSGEANO_Segmento: TWideStringField;
    QrSegmentosSGEANO_Accn_QT: TWideStringField;
    QrSegmentosSGEANO_Accn_SN: TWideStringField;
    QrSegmentosSGEACodigo: TLargeintField;
    QrSegmentosSGEACodUsu: TLargeintField;
    QrSegmentosSGEASegmento: TLargeintField;
    QrSegmentosSGEAEntidadesEmp: TLargeintField;
    QrSegmentosSGEAAccount: TLargeintField;
    QrSegmentosSGEAAccntSN: TLargeintField;
    QrSegmentosSGEAHvSrDB: TSmallintField;
    QrSegmentosSGEALk: TLargeintField;
    QrSegmentosSGEADataCad: TDateField;
    QrSegmentosSGEADataAlt: TDateField;
    QrSegmentosSGEAUserCad: TLargeintField;
    QrSegmentosSGEAUserAlt: TLargeintField;
    QrSegmentosSGEAAlterWeb: TSmallintField;
    QrSegmentosSGEAAtivo: TSmallintField;
    QrSegmentosSGEANome: TWideStringField;
    QrSegmentosSGEANO_HvSrDB: TWideMemoField;
    QrEntidadesGrupHvSrDB: TSmallintField;
    QrEntidadesGrupLk: TLargeintField;
    QrEntidadesGrupDataCad: TDateField;
    QrEntidadesGrupDataAlt: TDateField;
    QrEntidadesGrupUserCad: TLargeintField;
    QrEntidadesGrupUserAlt: TLargeintField;
    QrEntidadesGrupAlterWeb: TSmallintField;
    QrEntidadesGrupAtivo: TSmallintField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrEntidadesEmprBeforeClose(DataSet: TDataSet);
    procedure QrEntidadesEmprAfterScroll(DataSet: TDataSet);
    procedure EdCidadeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QrEntidadesEmprNO_HvSrDBGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure QrSegmentosSGEANO_HvSrDBGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraEntidadesEmpSGEA(SQLType: TSQLType);
    //
    procedure OpcoesCopia(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure ReopenEntidadesGrup();
    procedure ReopenSalesPerfilCl();
  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenSegmentosSGEA(Segmento, Account: Integer);

  end;

var
  FmEntidadesEmpr: TFmEntidadesEmpr;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, SegmentosSGEA;//, (***MyDBCheck, DmkDAC_PF,***) EntidadesEmpSGEA;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmEntidadesEmpr.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOz.LC(Atual, Codigo);
end;

procedure TFmEntidadesEmpr.MostraEntidadesEmpSGEA(SQLType: TSQLType);
begin
  if Grl_DmkDB.CriaFm(TFmSegmentosSGEA, FmSegmentosSGEA, afmoNegarComAviso) then
  begin
    FmSegmentosSGEA.ImgTipo.SQLType := SQLType;
    //FmSegmentosSGEA.FQrCab := QrEntidadesEmpr;
    //FmSegmentosSGEA.FDsCab := DsCadastro_Com_Itens_CAB;
    FmSegmentosSGEA.FQrIts := QrSegmentosSGEA;
    if SQLType = stIns then
      FmSegmentosSGEA.EdHvSrDB.ValueVariant       := Integer(THaveSrcDB.hsdbCadastr)
     // FmSegmentosSGEA.EdCPF1.ReadOnly := False
    else
    begin
      FmSegmentosSGEA.EdCodigo.ValueVariant       := QrSegmentosSGEACodigo.Value;
      FmSegmentosSGEA.EdCodUsu.ValueVariant       := QrSegmentosSGEACodUsu.Value;
      FmSegmentosSGEA.EdNome.ValueVariant         := QrSegmentosSGEANome.Value;
      FmSegmentosSGEA.EdEntidadesEmp.ValueVariant := QrSegmentosSGEAEntidadesEmp.Value;
      FmSegmentosSGEA.CBEntidadesEmp.KeyValue     := QrSegmentosSGEAEntidadesEmp.Value;
      FmSegmentosSGEA.EdSegmento.ValueVariant     := QrSegmentosSGEASegmento.Value;
      FmSegmentosSGEA.CBSegmento.KeyValue         := QrSegmentosSGEASegmento.Value;
      FmSegmentosSGEA.EdAccount.ValueVariant      := QrSegmentosSGEAAccount.Value;
      FmSegmentosSGEA.CBAccount.KeyValue          := QrSegmentosSGEAAccount.Value;
      FmSegmentosSGEA.EdAccntSN.ValueVariant      := QrSegmentosSGEAAccntSN.Value;
      FmSegmentosSGEA.CBAccntSN.KeyValue          := QrSegmentosSGEAAccntSN.Value;
      FmSegmentosSGEA.EdHvSrDB.ValueVariant       := QrSegmentosSGEAHvSrDB.Value;
      //
      FmSegmentosSGEA.LaEntidadesEmp.Enabled := False;
      FmSegmentosSGEA.EdEntidadesEmp.Enabled := False;
      FmSegmentosSGEA.CBEntidadesEmp.Enabled := False;
    end;
    FmSegmentosSGEA.ShowModal;
    FmSegmentosSGEA.Destroy;
  end;
end;

procedure TFmEntidadesEmpr.OpcoesCopia(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

end;

procedure TFmEntidadesEmpr.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrEntidadesEmpr);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrEntidadesEmpr, QrSegmentosSGEA);
end;

procedure TFmEntidadesEmpr.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrEntidadesEmpr);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrSegmentosSGEA);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrSegmentosSGEA);
end;

procedure TFmEntidadesEmpr.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOz.Go(Para, QrEntidadesEmprCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmEntidadesEmpr.DefParams;
begin
  VAR_GOTOTABELA := 'entidadesempr';
  VAR_GOTOzSQLTABLE := QrEntidadesEmpr;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOzSQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOz.LimpaVAR_SQL();

  VAR_SQLx.Add('SELECT egr1.Nome NO_GrupoEmpresa, sprf.Nome NO_Perfil, ');
  VAR_SQLx.Add(dmkPF.ArrayToTextoCASEWHEN('emp.HvSrDB', 'NO_HvSrDB',
    TPosVirgula.pvPos, sHaveSrcDB, iHaveSrcDB) + ' emp.*');
  VAR_SQLx.Add('FROM entidadesempr emp');
  VAR_SQLx.Add('LEFT JOIN entidadesgrup egr1 ON egr1.Codigo=emp.GrupoEmpresa');
  VAR_SQLx.Add('LEFT JOIN salesperfilcl sprf ON sprf.Codigo=emp.Perfil');
  VAR_SQLx.Add('WHERE emp.Codigo > 0');
  //
  VAR_SQL1.Add('AND emp.Codigo=:P0');
  //
  VAR_SQL2.Add('AND emp.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND emp.Nome Like :P0');
  //
end;

procedure TFmEntidadesEmpr.EdCidadeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  OpcoesCopia(Sender, Key, Shift);
end;

procedure TFmEntidadesEmpr.ItsAltera1Click(Sender: TObject);
begin
  MostraEntidadesEmpSGEA(stUpd);
end;

procedure TFmEntidadesEmpr.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmEntidadesEmpr.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmEntidadesEmpr.QueryPrincipalAfterOpen;
begin
end;

procedure TFmEntidadesEmpr.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
{
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'EntidadesEmpSGEA', 'Controle', QrSegmentosSGEAControle.Value, Dmod.MyDB?) = ID_YES then
  begin
    Controle := GOTOz.LocalizaPriorNextIntQr(QrSegmentosSGEA,
      QrSegmentosSGEAControle, QrSegmentosSGEAControle.Value);
    ReopenSegmentosSGEA(Segmento, Account(Controle);
  end;
}
end;

procedure TFmEntidadesEmpr.ReopenEntidadesGrup();
begin
  Grl_DmkDB.AbreSQLQuery0(QrEntidadesGrup, Dmod.MyDB, [
  'SELECT * ',
  'FROM entidadesgrup  ',
  'ORDER BY Nome ',
  '']);
end;

procedure TFmEntidadesEmpr.ReopenSalesPerfilCl();
begin
  Grl_DmkDB.AbreSQLQuery0(QrSalesPerfilCl, Dmod.MyDB, [
  'SELECT * ',
  'FROM salesperfilcl  ',
  'ORDER BY Nome ',
  '']);
end;

procedure TFmEntidadesEmpr.ReopenSegmentosSGEA(Segmento, Account: Integer);
begin
  Grl_DmkDB.AbreSQLQuery0(QrSegmentosSGEA, Dmod.MyDB, [
  'SELECT segc.Nome NO_Segmento, /*empr.Nome NO_Cliente,*/  ',
  'acqt.Nome NO_Accn_QT, acsn.Nome NO_Accn_SN, ',
  dmkPF.ArrayToTextoCASEWHEN('sgea.HvSrDB', 'NO_HvSrDB',
    TPosVirgula.pvPos, sHaveSrcDB, iHaveSrcDB),
  'sgea.*  ',
  'FROM segmentossgea sgea ',
  'LEFT JOIN segmentoscads segc ON segc.Codigo=sgea.Segmento ',
  '/*LEFT JOIN entidadesempr empr ON empr.Codigo=sgea.EntidadesEmp*/ ',
  'LEFT JOIN entidadesaccn acqt ON acqt.Codigo=sgea.Account ',
  'LEFT JOIN entidadesaccn acsn ON acsn.Codigo=sgea.AccntSN ',
  'WHERE EntidadesEmp=' + Geral.FF0(QrEntidadesEmprCodigo.Value),
  'ORDER BY segc.Ordem, segc.Sigla ',
  '']);
  //
  QrSegmentosSGEA.Locate('Segmento;Account', VarArrayOf([Segmento, Account]), []);
end;

procedure TFmEntidadesEmpr.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmEntidadesEmpr.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmEntidadesEmpr.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmEntidadesEmpr.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmEntidadesEmpr.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmEntidadesEmpr.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEntidadesEmpr.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrEntidadesEmprCodigo.Value;
  Close;
end;

procedure TFmEntidadesEmpr.ItsInclui1Click(Sender: TObject);
begin
  MostraEntidadesEmpSGEA(stIns);
end;

procedure TFmEntidadesEmpr.CabAltera1Click(Sender: TObject);
begin
  Grl_DmkDB.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrEntidadesEmpr, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'entidadesempr');
end;

procedure TFmEntidadesEmpr.BtConfirmaClick(Sender: TObject);
var
  Nome, Pais, Regiao, UF, Cidade, Perfil: String;
  Codigo, CodUsu, GrupoEmpresa, GrupEmprSN, LegalUnit, HvSrDB: Integer;
  SQLType: TSQLType;
begin
  SQLType             := ImgTipo.SQLType;
  //
  //Codigo              := EdCodigo.ValueVariant;
  //CodUsu              := EdCodUsu.ValueVariant;
  Grl_DmkDB.ObtemCodigoECodUsu(EdCodigo, EdCodUsu, SQLType, Codigo, CodUsu,
    'entidadesempr', 'Codigo', 'CodUsu');
  Nome                := EdNome.ValueVariant;
  GrupoEmpresa        := EdGrupoEmpresa.ValueVariant;
  GrupEmprSN          := EdGrupEmprSN.ValueVariant;
  Pais                := EdPais.ValueVariant;
  Regiao              := EdRegiao.ValueVariant;
  UF                  := EdUF.ValueVariant;
  Cidade              := EdCidade.ValueVariant;
  Perfil              := EdPerfil.ValueVariant;
  LegalUnit           := EdLegalUnit.ValueVariant;
  HvSrDB              := EdHvSrDB.ValueVariant;
  //
(*
  if SQLType = stIns then
  begin
    if (Codigo = 0) and (CodUsu <> 0) then
      Codigo := CodUsu
    else
    begin
      Codigo := Grl_DmkDB.GetNxtCodigoInt('entidadesempr', 'Codigo', SQLType, Codigo);
    end;
  end;
*)
  if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, 'entidadesempr', False, [
  'CodUsu', 'Nome', 'GrupoEmpresa',
  'GrupEmprSN', 'Pais', 'Regiao',
  'UF', 'Cidade', 'Perfil',
  'LegalUnit', 'HvSrDB'], [
  'Codigo'], [
  CodUsu, Nome, GrupoEmpresa,
  GrupEmprSN, Pais, Regiao,
  UF, Cidade, Perfil,
  LegalUnit, HvSrDB], [
  Codigo], True, dmksqlinsInsOnly, '', stDesktop, False) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOz.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmEntidadesEmpr.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  //if ImgTipo.SQLType = stIns then
    //UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'entidadesempr', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  GOTOz.UpdUnlockZ(Codigo, Dmod.MyDB, 'entidadesempr', 'Codigo');
end;

procedure TFmEntidadesEmpr.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmEntidadesEmpr.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmEntidadesEmpr.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
  //
  ReopenEntidadesGrup();
  ReopenSalesPerfilCl();
end;

procedure TFmEntidadesEmpr.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOz.Codigo(QrEntidadesEmprCodigo.Value, LaRegistro.Caption);
end;

procedure TFmEntidadesEmpr.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOz.Nome(LaRegistro.Caption);
end;

procedure TFmEntidadesEmpr.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOz.CodUsu(QrEntidadesEmprCodigo.Value, LaRegistro.Caption);
end;

procedure TFmEntidadesEmpr.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOz.Fechar(ImgTipo.SQLType);
end;

procedure TFmEntidadesEmpr.QrEntidadesEmprAfterScroll(DataSet: TDataSet);
begin
  ReopenSegmentosSGEA(0, 0);
end;

procedure TFmEntidadesEmpr.QrEntidadesEmprBeforeClose(DataSet: TDataSet);
begin
  QrSegmentosSGEA.Close;
end;

procedure TFmEntidadesEmpr.QrEntidadesEmprNO_HvSrDBGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  Text := TStringField(Sender).Value;
end;

procedure TFmEntidadesEmpr.QrSegmentosSGEANO_HvSrDBGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  Text := TStringField(Sender).Value;
end;

procedure TFmEntidadesEmpr.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrEntidadesEmprCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmEntidadesEmpr.SbQueryClick(Sender: TObject);
begin
  LocCod(QrEntidadesEmprCodigo.Value,
  GOTOz.CriaForm(CO_CODIGO, CO_NOME, 'entidadesempr', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmEntidadesEmpr.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEntidadesEmpr.CabInclui1Click(Sender: TObject);
begin
  Grl_DmkDB.ConfigPanelInsUpd(stIns, Self, PnEdita, QrEntidadesEmpr, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'entidadesempr');
  EdLegalunit.ValueVariant := 41;
  EdHvSrDB.ValueVariant := Integer(THaveSrcDB.hsdbCadastr);
end;

end.

