object FmProdutosCabec: TFmProdutosCabec
  Left = 368
  Top = 194
  Caption = 'PRD-CADAS-002 :: Cadastro de Refer'#234'ncia de Produtos'
  ClientHeight = 480
  ClientWidth = 784
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 384
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 61
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 8
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 136
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label22: TLabel
        Left = 736
        Top = 16
        Width = 30
        Height = 13
        Caption = 'HSDB'
        FocusControl = DBEdit10
      end
      object Label24: TLabel
        Left = 68
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 8
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 136
        Top = 32
        Width = 597
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCodUsu: TdmkEdit
        Left = 68
        Top = 32
        Width = 64
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CodUsu'
        UpdCampo = 'CodUsu'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdHvSrDB: TdmkEditF7
        Left = 736
        Top = 32
        Width = 40
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'HvSrDB'
        UpdCampo = 'HvSrDB'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        LocF7TableName = 'GetHvSrDBCad'
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 321
      Width = 784
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 644
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 384
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 61
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 8
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label4: TLabel
        Left = 136
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label2: TLabel
        Left = 68
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodUsu
      end
      object Label14: TLabel
        Left = 676
        Top = 16
        Width = 33
        Height = 13
        Caption = 'HSDB:'
        FocusControl = DBEdit10
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 8
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsProdutosCabec
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 136
        Top = 32
        Width = 537
        Height = 21
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsProdutosCabec
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdCodUsu: TdmkDBEdit
        Left = 68
        Top = 32
        Width = 64
        Height = 21
        TabStop = False
        DataField = 'CodUsu'
        DataSource = DsProdutosCabec
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 2
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit10: TDBEdit
        Left = 676
        Top = 32
        Width = 21
        Height = 21
        DataField = 'HvSrDB'
        DataSource = DsProdutosCabec
        TabOrder = 3
      end
      object DBEdit11: TDBEdit
        Left = 700
        Top = 32
        Width = 77
        Height = 21
        DataField = 'NO_HvSrDB'
        DataSource = DsProdutosCabec
        TabOrder = 4
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 320
      Width = 784
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 261
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 10055
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Refer'#234'ncia'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 548
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Produto'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
      end
    end
    object DGDados: TdmkDBGridZTO
      Left = 0
      Top = 61
      Width = 784
      Height = 148
      Align = alTop
      DataSource = DsProdutosCadas
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      RowColors = <>
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'ID'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CodUsu'
          Title.Caption = 'C'#243'digo'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Width = 120
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_Segmento'
          Title.Caption = 'Segmento'
          Width = 36
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_GrupoSegmento'
          Title.Caption = 'Grupo Segmento'
          Width = 88
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_Divisao'
          Title.Caption = 'Divis'#227'o'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_SubDivisao'
          Title.Caption = 'Sub-divisao'
          Width = 104
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_Familia'
          Title.Caption = 'Fam'#237'lia'
          Width = 104
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'HvSrDB'
          Title.Caption = 'HSDB'
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 520
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 442
        Height = 32
        Caption = 'Cadastro de Refer'#234'ncia de Produtos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 442
        Height = 32
        Caption = 'Cadastro de Refer'#234'ncia de Produtos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 442
        Height = 32
        Caption = 'Cadastro de Refer'#234'ncia de Produtos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 784
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 384
    Top = 308
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 384
    Top = 256
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object QrProdutosCabec: TZQuery
    Connection = DMod.MyDB
    BeforeClose = QrProdutosCabecBeforeClose
    AfterScroll = QrProdutosCabecAfterScroll
    SQL.Strings = (
      'SELECT egr1.Nome NO_GrupoEmpresa, sprf.Nome NO_Perfil, emp.* '
      'FROM entidadesempr emp'
      'LEFT JOIN entidadesgrup egr1 ON egr1.Codigo=emp.GrupoEmpresa'
      'LEFT JOIN salesperfilcl sprf ON sprf.Codigo=emp.Perfil')
    Params = <>
    Left = 120
    Top = 312
    object QrProdutosCabecCodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrProdutosCabecCodUsu: TLargeintField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrProdutosCabecNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 255
    end
    object QrProdutosCabecHvSrDB: TSmallintField
      FieldName = 'HvSrDB'
      ReadOnly = True
    end
    object QrProdutosCabecLk: TLargeintField
      FieldName = 'Lk'
    end
    object QrProdutosCabecDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrProdutosCabecDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrProdutosCabecUserCad: TLargeintField
      FieldName = 'UserCad'
    end
    object QrProdutosCabecUserAlt: TLargeintField
      FieldName = 'UserAlt'
    end
    object QrProdutosCabecAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrProdutosCabecAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrProdutosCabecNO_HvSrDB: TWideMemoField
      FieldName = 'NO_HvSrDB'
      OnGetText = QrProdutosCabecNO_HvSrDBGetText
      BlobType = ftWideMemo
      Size = 20
    end
  end
  object DsProdutosCabec: TDataSource
    DataSet = QrProdutosCabec
    Left = 120
    Top = 356
  end
  object QrProdutosCadas: TZQuery
    Connection = DMod.MyDB
    SQL.Strings = (
      'SELECT prd.*,'
      'sgr.Nome NO_GrupoSegmento, sca.Nome NO_Segmento,'
      'pdv.Nome NO_Divisao, psd.Nome NO_SubDivisao,'
      'prf.Nome NO_Familia'
      'FROM produtoscadas prd '
      'LEFT JOIN segmengrucads sgr ON sgr.Codigo=prd.GrupoSegmento'
      'LEFT JOIN segmentoscads sca ON sca.Codigo=prd.Segmento'
      'LEFT JOIN proddivsaocab pdv ON pdv.Codigo=prd.Divisao'
      'LEFT JOIN proddivsubcab psd ON psd.Codigo=prd.SubDivisao'
      'LEFT JOIN produtosfamil prf ON prf.Codigo=prd.Familia'
      'WHERE prd.ProdCabec>=0')
    Params = <>
    Left = 252
    Top = 312
    object QrProdutosCadasNO_HvSrDB: TWideMemoField
      FieldName = 'NO_HvSrDB'
      ReadOnly = True
      OnGetText = QrProdutosCadasNO_HvSrDBGetText
      BlobType = ftWideMemo
    end
    object QrProdutosCadasCodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrProdutosCadasCodUsu: TLargeintField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrProdutosCadasProdCabec: TLargeintField
      FieldName = 'ProdCabec'
      Required = True
    end
    object QrProdutosCadasGrupoSegmento: TLargeintField
      FieldName = 'GrupoSegmento'
      Required = True
    end
    object QrProdutosCadasSegmento: TLargeintField
      FieldName = 'Segmento'
      Required = True
    end
    object QrProdutosCadasDivisao: TLargeintField
      FieldName = 'Divisao'
      Required = True
    end
    object QrProdutosCadasSubDivisao: TLargeintField
      FieldName = 'SubDivisao'
      Required = True
    end
    object QrProdutosCadasFamilia: TLargeintField
      FieldName = 'Familia'
      Required = True
    end
    object QrProdutosCadasNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrProdutosCadasHvSrDB: TSmallintField
      FieldName = 'HvSrDB'
      Required = True
    end
    object QrProdutosCadasLk: TLargeintField
      FieldName = 'Lk'
    end
    object QrProdutosCadasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrProdutosCadasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrProdutosCadasUserCad: TLargeintField
      FieldName = 'UserCad'
    end
    object QrProdutosCadasUserAlt: TLargeintField
      FieldName = 'UserAlt'
    end
    object QrProdutosCadasAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrProdutosCadasAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrProdutosCadasNO_GrupoSegmento: TWideStringField
      FieldName = 'NO_GrupoSegmento'
      Required = True
      Size = 60
    end
    object QrProdutosCadasNO_Segmento: TWideStringField
      FieldName = 'NO_Segmento'
      Required = True
      Size = 60
    end
    object QrProdutosCadasNO_Divisao: TWideStringField
      FieldName = 'NO_Divisao'
      Required = True
      Size = 60
    end
    object QrProdutosCadasNO_SubDivisao: TWideStringField
      FieldName = 'NO_SubDivisao'
      Required = True
      Size = 60
    end
    object QrProdutosCadasNO_Familia: TWideStringField
      FieldName = 'NO_Familia'
      Required = True
      Size = 60
    end
  end
  object DsProdutosCadas: TDataSource
    DataSet = QrProdutosCadas
    Left = 252
    Top = 360
  end
end
