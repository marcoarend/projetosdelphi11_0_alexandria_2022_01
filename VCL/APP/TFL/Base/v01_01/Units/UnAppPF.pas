unit UnAppPF;

interface

uses
  Windows, SysUtils, Classes, Data.DB,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Menus,
  Vcl.Grids, Vcl.DBGrids, Vcl.ComCtrls, Vcl.StdCtrls, Vcl.Buttons, Vcl.Mask,
  Vcl.DBCtrls,
  ZAbstractConnection, ZConnection, ZCompatibility, ZAbstractRODataset,
  ZAbstractDataset, ZDataset,
  UnInternalConsts, dmkGeral, UnProjGroup_Vars, UnAppEnums, UnGrl_Vars,
  UnDmkEnums, UnGrl_DmkDB, UnVCL_ZDB, UnMyObjects;

type
  TUnAppPF = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ImportaDadosSalNew(REInfo: TRichEdit);
  end;

var
  AppPF: TUnAppPF;


implementation

uses Module;


{ TUnAppPF }

procedure TUnAppPF.ImportaDadosSalNew(REInfo: TRichEdit);
const
  NaoCria = False;
var
  SalNewDB: TZConnection;
  Usuario, Senha, TabOrig, TabDest, CondicaoSQL, DBNome: String;
  BaseOrig, BaseDest: TZConnection;
begin
  SalNewDB := TZConnection.Create(Dmod);
  try
    if VCL_ZDB.ConfiguraDB_SQLite(SalNewDB, Usuario, Senha, 'SalNew', NaoCria,
    DBNome) then
    begin
      Usuario     := '';
      Senha       := '';
      CondicaoSQL := '';
      BaseOrig    := SalNewDB;
      BaseDest    := Dmod.MyDB;
      //
      TabOrig     := '?';
      TabDest     := '?';
      //
      Grl_DmkDB.ExportaRegistrosEntreDBs_NovoZ(TabOrig, TabDest, CondicaoSQL,
      BaseOrig, BaseDest, REInfo);
    end else
      MyObjects.AdicionaTextoCorRichEdit(REInfo, clRed, [fsBold],
        'N�o foi poss�vel abrir a base de dados: ' + DBNome);
  finally
    SalNewDB.Free;
  end;
end;

end.
