unit UnApp;

interface

uses
  Windows, SysUtils, Classes, Menus, UnDmkEnums, mySQLDbTables, Forms,
  UnInternalConsts;

type
  TUnApp = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure MostraFormOpcoesApp();
  end;

var
  ALL_Jan: TUnALL_Jan;


implementation


  if DBCheck.CriaFm(TFmVSBxaCab, FmVSBxaCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
    begin
      FmVSBxaCab.LocCod(Codigo, Codigo);
      FmVSBxaCab.QrVSBxaIts.Locate('Controle', Controle, []);
    end;
    FmVSBxaCab.ShowModal;
    FmVSBxaCab.Destroy;
  end;

end.
