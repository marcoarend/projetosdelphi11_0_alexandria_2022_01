unit MyListas;

interface

uses System.Classes, System.SysUtils, Generics.Collections, UnDmkEnums,
  UnMyLinguas;
type
  TMyListas = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  CompletaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
    function  CriaListaCampos(Tabela: String; FLCampos: TList<TCampos>;
              var TemControle: TTemControle): Boolean;
    function  CriaListaIndices(TabelaBase, TabelaNome: String; FLIndices:
              TList<TIndices>): Boolean;
    function  CriaListaTabelas(DatabaseName: String; Lista: TList<TTabelas>): Boolean;
    function  CriaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
    function  CriaListaJanelas(FLJanelas: TList<TJanelas>): Boolean;
    function  CriaListaQeiLnk(TabelaCria: String; FLQeiLnk: TList<TQeiLnk>): Boolean;
      //
  end;

const
  CO_VERSAO = 1808011355;
  CO_SIGLA_APP = 'bTFL';
  CO_DMKID_APP = 0;
  CO_DBNome = 'PjBsTFL';
  CO_GRADE_APP = False;
  CO_VLOCAL = True;
  CO_EXTRA_LCT_003 = False; // True somente para Credito2

var
  MyList   : TMyListas;
  FRCampos : TCampos;
  FRIndices: TIndices;
  FRJanelas: TJanelas;
  FRQeiLnk : TQeiLnk;

implementation

uses
  UnSalNew_Tabs;

{ TMyListas }

function TMyListas.CriaListaTabelas(DatabaseName: String;
  Lista: TList<TTabelas>): Boolean;
begin
  Result := True;
  try
    if LowerCase(DatabaseName) = LowerCase(CO_DBNome) then
    begin
      SalNew_Tabs.CarregaListaTabelas(DatabaseName, Lista);
      //
      MyLinguas.AdTbLst(Lista, False, LowerCase('Controle'), '');
    end else
    begin
      ;
    end;
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaIndices(TabelaBase, TabelaNome: String;
  FLIndices: TList<TIndices>): Boolean;
begin
  Result := True;
  try
    if Uppercase(TabelaBase) = Uppercase('Controle') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else
    begin
      SalNew_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
    end;
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaSQL(Tabela: String;
  FListaSQL: TStringList): Boolean;
begin
  Result := True;
  try
    SalNew_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    ;
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CompletaListaSQL(Tabela: String;
  FListaSQL: TStringList): Boolean;
begin
  Result := True;
  try
    SalNew_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    ;
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaCampos(Tabela: String; FLCampos: TList<TCampos>;
  var TemControle: TTemControle): Boolean;
begin
{
2. Storage Classes and Datatypes
Each value stored in an SQLite database (or manipulated by the database engine)
has one of the following storage classes:

  NULL.    The value is a NULL value.
  INTEGER. The value is a signed integer, stored in 1, 2, 3, 4, 6, or 8 bytes
           depending on the magnitude of the value.
  REAL.    The value is a floating point value, stored as an 8-byte IEEE floating
           point number.
  TEXT.    The value is a text string, stored using the database encoding
           (UTF-8, UTF-16BE or UTF-16LE).
  BLOB.    The value is a blob of data, stored exactly as it was input.

A storage class is more general than a datatype. The INTEGER storage class, for
example, includes 6 different integer datatypes of different lengths. This makes
a difference on disk. But as soon as INTEGER values are read off of disk and into
memory for processing, they are converted to the most general datatype (8-byte
signed integer). And so for the most part, "storage class" is indistinguishable
from "datatype" and the two terms can be used interchangeably.
Any column in an SQLite version 3 database, except an INTEGER PRIMARY KEY column,
may be used to store a value of any storage class.
All values in SQL statements, whether they are literals embedded in SQL statement
text or parameters bound to precompiled SQL statements have an implicit storage
class. Under circumstances described below, the database engine may convert
values between numeric storage classes (INTEGER and REAL) and TEXT during query execution.

2.1. Boolean Datatype
SQLite does not have a separate Boolean storage class. Instead, Boolean values
are stored as integers 0 (false) and 1 (true).

2.2. Date and Time Datatype
SQLite does not have a storage class set aside for storing dates and/or times.
Instead, the built-in Date And Time Functions of SQLite are capable of storing
dates and times as TEXT, REAL, or INTEGER values:
TEXT as ISO8601 strings ("YYYY-MM-DD HH:MM:SS.SSS").
REAL as Julian day numbers, the number of days since noon in Greenwich on
November 24, 4714 B.C. according to the proleptic Gregorian calendar.
INTEGER as Unix Time, the number of seconds since 1970-01-01 00:00:00 UTC.
Applications can chose to store dates and times in any of these formats and
freely convert between formats using the built-in date and time functions.
}
  try
    if Uppercase(Tabela) = Uppercase('Controle') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := 'Chave prim�ria U';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Versao';
      FRCampos.Tipo       := 'bigint(20)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := 'Vers�o do aplicativo';
      FLCampos.Add(FRCampos);
      //
    end else
    begin
      SalNew_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
    end;
    SalNew_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    //
    Result := True;
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaJanelas(FLJanelas: TList<TJanelas>): Boolean;
begin
  //
{
  // ENT-CADAS-001 :: Grupos de Empresas
  New(FRJanelas);
  FRJanelas.ID        := 'ENT-CADAS-001';
  FRJanelas.Nome      := 'FmEntidadesGrup';
  FRJanelas.Descricao := 'Grupos de Empresas';
  FLJanelas.Add(FRJanelas);
}
  //
  SalNew_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Result := True;
end;


function TMyListas.CriaListaQeiLnk(TabelaCria: String;
  FLQeiLnk: TList<TQeiLnk>): Boolean;
begin
  Result := True;
  try
(*
    if Uppercase(TabelaCria) = Uppercase('VSEntiMP') then
    begin
      New(FRQeiLnk);
      FRQeiLnk.AskrTab       := TabelaCria;
      FRQeiLnk.AskrCol       := 'Codigo';
      FRQeiLnk.RplyTab       := '';
      FRQeiLnk.RplyCol       := '';
      FRQeiLnk.Purpose       := TItemTuplePurpose.itpUSGSrvrInc;
      FRQeiLnk.Seq_in_pref   := 1;
      FLQeiLnk.Add(FRQeiLnk);
      //
      New(FRQeiLnk);
      FRQeiLnk.AskrTab       := TabelaCria;
      FRQeiLnk.AskrCol       := 'MPVImpOpc';
      FRQeiLnk.RplyTab       := 'MPVImpOpc';
      FRQeiLnk.RplyCol       := 'Codigo';
      FRQeiLnk.Purpose       := TItemTuplePurpose.itpRelatUsrSrv;
      FRQeiLnk.Seq_in_pref   := 1;
      FLQeiLnk.Add(FRQeiLnk);
      //
      //
    //end else if Uppercase(TabelaBase) = Uppercase('?) then
    //begin
    end else
*)
    begin
      SalNew_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
    end;
  except
    raise;
    Result := False;
  end;
end;

end.
