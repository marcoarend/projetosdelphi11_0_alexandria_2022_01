unit Principal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  WinSkinData, WinSkinStore, Vcl.Imaging.pngimage, AdvToolBar, AdvShapeButton,
  AdvGlowButton, Data.DB, ZAbstractRODataset, ZAbstractDataset, ZDataset,
  Vcl.ExtCtrls, Vcl.Menus, Registry, Vcl.Grids, Vcl.DBGrids, Vcl.ComCtrls,
  Vcl.StdCtrls, System.DateUtils, AdvOfficeStatusBar, AdvOfficeStatusBarStylers,
  AdvCardList, InspectorBar, InspLinks, AdvLookupBar, AdvDataLabel, ToolPanels,
  BaseGrid,
  uLkJSON, UnMyJSON, UnAppPF, UnAppJan, UnProjGroup_Vars,
  UnDmkEnums, TypInfo,
  UnAppEnums, AdvProgressBar, AdvReflectionLabel, AdvPanel, AdvMenus,
  Vcl.Buttons, Vcl.Mask, Vcl.DBCtrls, AdvMenuStylers, RTTI,
  UnGrl_Vars, ZConnectionGroup, ZSqlUpdate;

type
  TDtaTip = (dttpUltTry=0, dttpUltExe=1, dttpUltErr=2);
  TFmPrincipal = class(TForm)
    SkinStore1: TSkinStore;
    sd1: TSkinData;
    AdvToolBarPager1: TAdvToolBarPager;
    AdvPage2: TAdvPage;
    AdvToolBar2: TAdvToolBar;
    BtProduzido: TAdvGlowButton;
    AGBInclui: TAdvGlowButton;
    AGBExclui: TAdvGlowButton;
    AdvToolBar1: TAdvToolBar;
    AGBOcultar: TAdvGlowButton;
    AGBExecutar: TAdvGlowButton;
    AdvToolBarPager13: TAdvPage;
    AdvToolBar3: TAdvToolBar;
    AdvGlowButton6: TAdvGlowButton;
    AGMBVerifiBD: TAdvGlowButton;
    AdvPage1: TAdvPage;
    AdvToolBar4: TAdvToolBar;
    AdvGlowButton28: TAdvGlowButton;
    AdvToolBar5: TAdvToolBar;
    AdvGlowButton19: TAdvGlowButton;
    AdvShapeButton1: TAdvShapeButton;
    AdvQuickAccessToolBar1: TAdvQuickAccessToolBar;
    ATBBAtualiza: TAdvToolBarButton;
    QrXYZX: TZQuery;
    QrXYZXCodigo: TLargeintField;
    QrXYZXOrigem: TWideStringField;
    QrXYZXDestino: TWideStringField;
    DsXYZX: TDataSource;
    BalloonHint1: TBalloonHint;
    TmConfiguraDB: TTimer;
    TmHide: TTimer;
    TmVersao: TTimer;
    TrayIcon1: TTrayIcon;
    PMMenu: TPopupMenu;
    Mostrar1: TMenuItem;
    Fechar1: TMenuItem;
    N1: TMenuItem;
    Inicializao1: TMenuItem;
    Executarnainicializao1: TMenuItem;
    NOexecutarnainicializao1: TMenuItem;
    QrXYZXSubDir: TSmallintField;
    QrXYZXZipar: TSmallintField;
    QrXYZXEncrypt: TSmallintField;
    QrXYZXSenhaCrypt: TWideStringField;
    QrXYZXOmitirExt: TSmallintField;
    QrXYZXExtOmit: TWideMemoField;
    QrXYZXDias: TLargeintField;
    QrXYZXRecorrencia: TWideStringField;
    QrExeBackup: TZQuery;
    AdvToolBar6: TAdvToolBar;
    AGBExtrair: TAdvGlowButton;
    QrXYZXUltExe: TDateTimeField;
    QrXYZXAviso: TWideStringField;
    AdvGlowButton43: TAdvGlowButton;
    AdvToolPanelTab1: TAdvToolPanelTab;
    AdvGridUndoRedo1: TAdvGridUndoRedo;
    AdvToolPanel1: TAdvToolPanel;
    AdvStatusBar: TAdvOfficeStatusBar;
    AdvOfficeStatusBarOfficeStyler1: TAdvOfficeStatusBarOfficeStyler;
    AdvToolPanel2: TAdvToolPanel;
    AdvToolPanel3: TAdvToolPanel;
    AdvPB1: TAdvProgressBar;
    Memo1: TMemo;
    Memo2: TMemo;
    LBAvisos: TListBox;
    AdvPanelStyler1: TAdvPanelStyler;
    AdvPanel1: TAdvPanel;
    LaPVhora1: TLabel;
    LaPVhora2: TLabel;
    LaP_PV1A: TLabel;
    LaP_PV1B: TLabel;
    LaP_PV1C: TLabel;
    AdvToolBar7: TAdvToolBar;
    AdvGlowButton1: TAdvGlowButton;
    Image1: TImage;
    Label1: TLabel;
    AdvCadastros: TAdvToolBarButton;
    AdvPMCadastros: TAdvPopupMenu;
    Segmentos1: TMenuItem;
    BitBtn1: TBitBtn;
    DBEdit1: TDBEdit;
    AdvMenuOfficeStyler1: TAdvMenuOfficeStyler;
    Entidades1: TMenuItem;
    GruposdeEmpresas1: TMenuItem;
    Clientes1: TMenuItem;
    Accounts1: TMenuItem;
    Produtos1: TMenuItem;
    Famlias1: TMenuItem;
    Cadastro1: TMenuItem;
    Similares1: TMenuItem;
    Gruposdesegmentos1: TMenuItem;
    Divises1: TMenuItem;
    Subdivises1: TMenuItem;
    N3: TMenuItem;
    Linhas1: TMenuItem;
    Segmentos2: TMenuItem;
    Embalagens1: TMenuItem;
    Cadastro2: TMenuItem;
    aras1: TMenuItem;
    Envases1: TMenuItem;
    N2: TMenuItem;
    Update1: TMenuItem;
    este1: TMenuItem;
    ZUpdateSQL1: TZUpdateSQL;
    REInfo: TRichEdit;
    procedure AGBOcultarClick(Sender: TObject);
    procedure BtProduzidoClick(Sender: TObject);
    procedure AdvGlowButton6Click(Sender: TObject);
    procedure AGMBVerifiBDClick(Sender: TObject);
    procedure AdvGlowButton28Click(Sender: TObject);
    procedure AdvGlowButton19Click(Sender: TObject);
    procedure TmConfiguraDBTimer(Sender: TObject);
    procedure TmHideTimer(Sender: TObject);
    procedure TmVersaoTimer(Sender: TObject);
    procedure TrayIcon1Click(Sender: TObject);
    procedure PMMenuPopup(Sender: TObject);
    procedure Mostrar1Click(Sender: TObject);
    procedure Fechar1Click(Sender: TObject);
    procedure Executarnainicializao1Click(Sender: TObject);
    procedure NOexecutarnainicializao1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure AdvGlowButton43Click(Sender: TObject);
    procedure AdvGlowButton1Click(Sender: TObject);
    procedure AGBExecutarClick(Sender: TObject);
    procedure Segmentos1Click(Sender: TObject);
    procedure ATBBAtualizaClick(Sender: TObject);
    procedure GruposdeEmpresas1Click(Sender: TObject);
    procedure Clientes1Click(Sender: TObject);
    procedure Accounts1Click(Sender: TObject);
    procedure Cadastro1Click(Sender: TObject);
    procedure Gruposdesegmentos1Click(Sender: TObject);
    procedure Famlias1Click(Sender: TObject);
    procedure Divises1Click(Sender: TObject);
    procedure Subdivises1Click(Sender: TObject);
    procedure Similares1Click(Sender: TObject);
    procedure Linhas1Click(Sender: TObject);
    procedure aras1Click(Sender: TObject);
    procedure Cadastro2Click(Sender: TObject);
    procedure Envases1Click(Sender: TObject);
    procedure Update1Click(Sender: TObject);
    procedure este1Click(Sender: TObject);
  private
    { Private declarations }
    FCriouDB, FShowed, FHideFirst, FPingou: Boolean;
    FHides: Integer;
    function  VerificaNovasVersoes(ApenasVerifica: Boolean = False): Boolean;
    function  AtualizaDataHoraBackup(TipData: TDtaTip; Codigo: Integer; Msg: String): Boolean;
{
    function  ExecBackup(SubDir, OmitirExt: Boolean; Caminho, NomeDir, Nome,
              ExtOmit: String): Boolean;
    function  MoveResulBackup(Zipar: Boolean; Caminho, NomeDir, Nome,
                Destino: String): Boolean;
}
    function  ZipaArquivo(var Caminho: String; const Encryptar: Boolean;
                Senha: String): Boolean;
    procedure ExecutaNaInicializacao(Executa: Boolean; Titulo, Programa: String);
    //procedure MostraBackupDir(Codigo: Integer);
    procedure ConfiguraDB();
    //procedure ReopenBackupDir();
    procedure BackupDir(Codigo: Integer);
    procedure ExtrairArquivo();
{XYZX    procedure ExecutaBackupAutomatico();}
    // Inicio app
    procedure Teste();
    procedure JsonToMemo_1(JsonStr: String);
    procedure JsonToMemo_2(JsonStr: String);
    procedure TextoP_PVVazio(Msg: String);
  public
    { Public declarations }
    procedure AcoesIniciaisDoAplicativo();
    procedure ReCaptionComponentesDeForm(Form: TForm);
  end;

var
  FmPrincipal: TFmPrincipal;

const
  CO_Titulo = 'Dermatek Backup - Arquivos';

var
  _AGORA: TDateTime;
  SecToday: array [0..86400] of Double;
  IniSecond: Integer = 0;
  EndSecond: Integer = 0;

implementation

uses MyVCLSkin, MyListas, UnDmkWeb, dmkGeral, About, UnMyObjects, Module,
  UnVCL_ZDB, UnInternalConsts, UnGrl_DmkDB, ZForge,
  UnDmkProcFunc, UMySQLDB, MyGlyfs, UnSalNewJan;

{$R *.dfm}

{ TFmPrincipal }

procedure TFmPrincipal.Accounts1Click(Sender: TObject);
begin
  SalNewJan.MostraFormEntidadesAccn(0);
end;

procedure TFmPrincipal.AcoesIniciaisDoAplicativo();
begin
//
end;

procedure TFmPrincipal.AdvGlowButton19Click(Sender: TObject);
var
  Link: String;
begin
  Link := DmkWeb.ObtemLinkAjuda(CO_DMKID_APP, 0);
  //
  DmkWeb.MostraWebBrowser(Link);
end;

procedure TFmPrincipal.AdvGlowButton1Click(Sender: TObject);
var
  Bmp: TBitMap;
  X, Y: Integer;
  Branco, Cinza, Vermelho: Integer;
  Perc1, Perc2: Double;
begin
  BMP := TBitmap.Create();
  BMP.LoadFromFile('LeiturasDia.bmp');
  Image1.Picture.LoadFromFile('LeiturasDia.bmp');
  Branco   := 0;
  Cinza    := 0;
  Vermelho := 0;
  //
  for Y := 0 to BMP.Height - 1 do
  begin
    for X := 0 to BMP.Width - 1 do
    begin
      case BMP.Canvas.Pixels[X, Y] of
        clWhite: Branco := Branco + 1;
        clRed: Vermelho := Vermelho + 1;
        else Cinza := Cinza + 1;
      end;
    end;
  end;
  Perc1 := Vermelho / Cinza * 100;
  Perc2 := Perc1 * 23.61 / 100;
  Geral.MB_Info(
  'Branco: ' + Geral.FF0(Branco) + sLineBreak +
  'Cinza: ' + Geral.FF0(Cinza) + sLineBreak +
  'Vermelho: ' + Geral.FF0(Vermelho) + sLineBreak + sLineBreak +
  '% Branco / Cinza: ' + Geral.FFT(Perc1, 2, siNegativo) + sLineBreak +
  'Wh vermelho dia 11/08/2018: ' + Geral.FFT(Perc2, 3, siNegativo));
end;

procedure TFmPrincipal.AdvGlowButton28Click(Sender: TObject);
begin
  Application.CreateForm(TFmAbout, FmAbout);
  FmAbout.ShowModal;
  FmAbout.Destroy;
end;

procedure TFmPrincipal.AdvGlowButton43Click(Sender: TObject);
begin
  AppJan.MostraFormOpcoesApp();
end;

procedure TFmPrincipal.AdvGlowButton6Click(Sender: TObject);
begin
  VerificaNovasVersoes;
end;

procedure TFmPrincipal.BtProduzidoClick(Sender: TObject);
var
  I: Integer;
var
  DeviceType, Unidade: String;
  DeviceSeq: Integer;
  Quantia: Double;
  SQLType: TSQLType;
  //
  Texto, Str: String;
  ProdDta, Dia, Hora: TDateTime;
begin
  Memo1.Lines.LoadFromFile('Leituras_1.Txt');
  //
  SQLType             := stIns;
  //
  DeviceType          := '75';
  DeviceSeq           := 1;
  ProdDta             := 0;
  Unidade             := 'Wh';
  Quantia             := 0;
  //
  AdvPB1.Position := 0;
  AdvPB1.Max := Memo1.Lines.Count;
  AdvPB1.Visible := True;
  Dia := 0;
  Hora := 0;
  for I := 0 to Memo1.Lines.Count - 1 do
  begin
    AdvPB1.Position := AdvPB1.Position + 1;
    Application.ProcessMessages;
    //
    Texto := Memo1.Lines[I];
    if Texto[1] = '[' then
    begin
      Str := Copy(Texto, 2, 10);
      Dia := dmkPF.ValidaDataDeSQL(Str, False);
    end else
    begin
      Str := Copy(Texto, 1, 8);
      Hora := StrToTime(Str);
      ProdDta := Dia + Hora;
      //
      Str := Copy(Texto, 13);
      Quantia := StrToFloat(Str);
      //
      //if
      Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, 'leituras', False, [
      'DeviceSeq', 'ProdDta', 'Unidade',
      'Quantia'], [
      'DeviceType'], [
      DeviceSeq, ProdDta, Unidade,
      Quantia], [
      DeviceType], True, dmksqlinsInsIgnore, '', stDesktop, False);
        Memo1.Lines.LoadFromFile('Leituras.Txt');
    end;
  end;
  AdvPB1.Visible := False;
end;

procedure TFmPrincipal.AGBExecutarClick(Sender: TObject);
begin
  //AppPF.LoadSales();
end;

procedure TFmPrincipal.AGBOcultarClick(Sender: TObject);
begin
  TrayIcon1.Visible             := True;
  Application.MainFormOnTaskBar := FShowed;
  //
  FmPrincipal.Hide;
  //
  Application.MainFormOnTaskBar := FShowed;
end;

procedure TFmPrincipal.AGMBVerifiBDClick(Sender: TObject);
begin
  VCL_ZDB.MostraVerifyDB(False, Dmod.MyDB);
end;

procedure TFmPrincipal.aras1Click(Sender: TObject);
begin
  SalNewJan.MostraFormEmbalagensTar(0);
end;

procedure TFmPrincipal.ATBBAtualizaClick(Sender: TObject);
begin
  VerificaNovasVersoes();
end;

function TFmPrincipal.AtualizaDataHoraBackup(TipData: TDtaTip; Codigo: Integer; Msg: String): Boolean;
var
  Campo, Agora: String;
begin
  case TipData of
    dttpUltTry:
      Campo := 'UltTry';
    dttpUltExe:
      Campo := 'UltExe';
    dttpUltErr:
      Campo := 'UltErr';
  end;
  Agora := Geral.FDT(Now, 9);
  //
  if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, stUpd, 'backupdir', False,
    [Campo, 'Aviso'], ['Codigo'], [Agora, Msg], [Codigo],
     False, dmksqlinsInsOnly, '', stDesktop) then
  begin
    Result := True;
  end else
    Result := False;
end;

procedure TFmPrincipal.BackupDir(Codigo: Integer);
var
  Continua, SubDir, OmitirExt, Zipar, Encrypt: Boolean;
  Msg, Origem, Destino, NomeDir, Nome, ExtOmit, FileName, SenhaCrypt: String;
begin
{XYZX  PCBackupDir.ActivePageIndex := 1;
  //
  Continua := True;
  Msg      := '';
  //
  Grl_DmkDB.AbreSQLQuery0(QrExeBackup, DMod.MyDB, [
  'SELECT * ',
  'FROM backupdir ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  '']);
  //
  if QrExeBackup.RecordCount > 0 then
  begin
    Origem     := dmkPF.SimplificaBarras(QrExeBackup.FieldByName('Origem').AsString);
    Destino    := dmkPF.SimplificaBarras(QrExeBackup.FieldByName('Destino').AsString);
    NomeDir    := ExtractFileName(Origem);
    Nome       := USQLDB.Backup_CriaNome('', Now);
    SubDir     := Geral.IntToBool(QrExeBackup.FieldByName('SubDir').AsInteger);
    OmitirExt  := Geral.IntToBool(QrExeBackup.FieldByName('OmitirExt').AsInteger);
    ExtOmit    := QrExeBackup.FieldByName('ExtOmit').AsString;
    Zipar      := Geral.IntToBool(QrExeBackup.FieldByName('Zipar').AsInteger);
    Encrypt    := Geral.IntToBool(QrExeBackup.FieldByName('Encrypt').AsInteger);
    SenhaCrypt := QrExeBackup.FieldByName('SenhaCrypt').AsString;
    //
    if MyObjects.FIC(not DirectoryExists(Origem), nil, 'Backup cancelado! Diret�rio origem n�o localizado!') then
      Exit;
    //
    if not DirectoryExists(Destino) then
    begin
      if not ForceDirectories(Destino) then
      begin
        Geral.MensagemBox('Backup cancelado! Diret�rio destino n�o existe e n�o pode ser criado!',
          'Aviso', MB_OK+MB_ICONWARNING);
        Exit;
      end;
    end;
    try
      AGBExecutar.Enabled := False;

      //Salva no banco de dados o in�cio do backup
      Continua := AtualizaDataHoraBackup(dttpUltTry, Codigo, 'Backup iniciado!');

      if not Continua then
        Exit;

      //Executa o backup
      Continua := ExecBackup(SubDir, OmitirExt, Origem, NomeDir, Nome, ExtOmit);

      if not Continua then
      begin
        Msg := 'Falha ao executar o backup dos arquivos!';
        Exit;
      end;

      //Zipa o backup
      if Zipar = True then
      begin
        FileName := Origem + '\' + NomeDir + Nome;
        //
        Continua := ZipaArquivo(FileName, Encrypt, SenhaCrypt);

        if not Continua then
        begin
          Msg := 'Falha ao zipar arquivo de backup!';
          Exit;
        end;
      end;

      //Move o backup para o diret�rio correspondente
      Continua := MoveResulBackup(Zipar, Origem, NomeDir, Nome, Destino);

      if not Continua then
      begin
        //Exclui se n�o conseguir mover para n�o ocupar espa�o em disco
        DeleteFile(FileName);
        //
        Msg := 'Falha mover backup para o diret�rio de destino!';
        Exit;
      end;
    finally
      if Continua then
        AtualizaDataHoraBackup(dttpUltExe, Codigo, 'Backup finalizado!')
      else
        AtualizaDataHoraBackup(dttpUltErr, Codigo, Msg);
      //
      ReopenBackupDir;
      //
      AGBExecutar.Enabled := True;
      //
      PCBackupDir.ActivePageIndex := 0;
    end;
  end;
}
end;

{XYZX
procedure TFmPrincipal.ReopenBackupDir;

  procedure ConfiguraComponentes(Ativa: Boolean);
  begin
    AGBExecutar.Enabled  := Ativa;
    AGBInclui.Enabled    := Ativa;
    AGBAltera.Enabled    := Ativa;
    AGBExclui.Enabled    := Ativa;
    DBGBackupDir.Enabled := Ativa;
    //
    if Ativa = False then
      QrBackupDir.DisableControls
    else
      QrBackupDir.EnableControls;
  end;
begin
  try
    ConfiguraComponentes(False);
    //
    Grl_DmkDB.AbreQuery(QrBackupDir, Dmod.MyDB);
    //
    if QrBackupDir.RecordCount > 0 then
    begin
      USQLDB.Backups_ExcluiAntigos(QrBackupDirUltExe.Value, QrBackupDirDias.Value,
        QrBackupDirDestino.Value, ExtractFileName(QrBackupDirOrigem.Value));
    end;
  finally
    ConfiguraComponentes(True);
  end;
end;
 }

procedure TFmPrincipal.Cadastro1Click(Sender: TObject);
begin
  SalNewJan.MostraFormProdutosCadas(0);
end;

procedure TFmPrincipal.Cadastro2Click(Sender: TObject);
begin
  SalNewJan.MostraFormEmbalagensCad(0);
end;

procedure TFmPrincipal.Clientes1Click(Sender: TObject);
begin
  SalNewJan.MostraFormEntidadesEmpr(0);
end;

procedure TFmPrincipal.ConfiguraDB;
begin
  FCriouDB := True;
  //
  Application.CreateForm(TDmod, Dmod);
  //
  {XYZX ReopenBackupDir();}
  //
  TrayIcon1.Visible := True;
  //
end;

procedure TFmPrincipal.Divises1Click(Sender: TObject);
begin
  SalNewJan.MostraFormProdDivsaoCab(0);
end;

{
function TFmPrincipal.ExecBackup(SubDir, OmitirExt: Boolean; Caminho, NomeDir,
  Nome, ExtOmit: String): Boolean;
var
  i, j, p: Integer;
  Eh: Boolean;
  Comp1, Comp2, Cam: String;
  ListaOmiExt: TStringList;
begin
  Result      := False;
  ListaOmiExt := TStringList.Create;
  try
    //Carrega
    LBLista1.Clear;
    dmkPF.GetAllFiles(SubDir, Caminho + '\*.*', lBLista1, True);
    //
    LBLista1.TopIndex := LBLista1.Items.Count - 1;
    //f
    //Executa o backup
    Screen.Cursor := crHourGlass;
    try
      PB1.Visible  := True;
      PB1.Position := 0;
      PB1.Max      := LBLista1.Items.Count;
      //
      for i := 0 to LBLista1.Items.Count -1 do
      begin
        PB1.Position := PB1.Position + 1;
        PB1.Update;
        Application.ProcessMessages;
        //
        Eh := False;
        //
        if OmitirExt then
        begin
          ListaOmiExt := Geral.Explode(ExtOmit, ';', 0);
          //
          for j := 0 to ListaOmiExt.Count - 1 do
          begin
            p := pos('*', ListaOmiExt.Strings[j]);
            //
            if p > 0 then
            begin
              Comp1 := Copy(ExtractFileExt(LBLista1.Items[i]), 1, p - 1);
              Comp2 := Copy(ListaOmiExt.Strings[j], 1, p - 1);
            end else begin
              Comp1 := ExtractFileExt(LBLista1.Items[i]);
              Comp2 := ListaOmiExt.Strings[j];
            end;
            if Uppercase(Comp1) = Uppercase(Comp2) then
            begin
              Eh := True;
              Break;
            end;
          end;
        end;

        if Eh then
          LBLista2.Items.Add(LBLista1.Items[i] + ' n�o foi copiado.')
        else begin
          Cam := LBLista1.Items[i];
          //
          Insert('\' + NomeDir + Nome, Cam, Length(Caminho) + 1);
          ForceDirectories(ExtractFilePath(Cam));
          //
          if CopyFile(PChar(LBLista1.Items[i]), PChar(Cam), False) then
            LBLista2.Items.Add(Cam + ' foi criado.')
          else
            LBLista2.Items.Add(Cam+' n�o pode ser criado.');
        end;
        LBLista2.Update;
        Application.ProcessMessages;
        //
        LBLista2.TopIndex := LBLista2.Items.Count - 1;
      end;
      Result := True;
    finally
      PB1.Position  := 0;
      Screen.Cursor := crDefault;
    end;
    ListaOmiExt.Free;
  except
    ListaOmiExt.Free;
  end;
end;
}

{XYZX
procedure TFmPrincipal.ExecutaBackupAutomatico;
var
  Dia, Mes, Ano: Word;
  Agora, Ini, Fim: TDateTime;
  Recur: TRecurrencyHandler;
  i: Integer;
begin
  Agora := Now;
  //
  DecodeDate(Agora, Ano, Mes, Dia);
  //
  Ini := EncodeDateTime(Ano, Mes, Dia, 00, 00, 00, 000);
  Fim := EncodeDateTime(Ano, Mes, Dia, 23, 59, 59, 999);
  //
  Recur            := TRecurrencyHandler.Create;
  Recur.Recurrency := QrBackupDirRecorrencia.Value;
  Recur.StartTime  := Ini;
  Recur.EndTime    := Fim;
  Recur.TimeSpan   := Fim;
  Recur.Parse;
  Recur.Generate;
  //
  for I := 0 to Recur.Dates.Count - 1 do
  begin
    Geral.MB_Aviso('Data incial: ' + Geral.FDT(Recur.Dates[I].StartDate, 0) +
      sLineBreak + 'Data fim: ' + Geral.FDT(Recur.Dates[I].EndDate, 0));
  end;
end;
}
procedure TFmPrincipal.Envases1Click(Sender: TObject);
begin
  SalNewJan.MostraFormProdutosEmbal(0);
end;

procedure TFmPrincipal.este1Click(Sender: TObject);
var
  Path, Tabela: String;
begin
  //Dmod.MyDB.Connected := False;
  Path := VAR_TMeuDB_ZDir  + 'Quota.s3db';
  Tabela := 'embalagenscad';




{
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add(
  ' commit; ' +
  //' ATTACH DATABASE "C:\\Users\\Admin\\TFL\\Apps\\SQLite\\Quota.s3db" AS db2; ' +
  ' ATTACH DATABASE "C://Users//Admin//TFL//Apps//SQLite//Quota.s3db" AS db2; ' +
  '');
  Dmod.QrUpd.ExecSQL;
}
















  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add(
  ' pragma database_list; ' +
  '');
  Dmod.QrUpd.Open;
  while not Dmod.QrUpd.Eof do
  begin
    Geral.MB_Info(
      Dmod.QrUpd.FieldByName('name').AsString + ' -- ' +
      Dmod.QrUpd.FieldByName('file').AsString);
    //
    Dmod.QrUpd.Next;
  end;
end;

procedure TFmPrincipal.ExecutaNaInicializacao(Executa: Boolean; Titulo,
  Programa: String);
var
  Registry: TRegistry;
begin
  Registry := TRegistry.Create;
  try
    Registry.RootKey := HKEY_CURRENT_USER;
    Registry.OpenKey('\Software\Microsoft\Windows\CurrentVersion\Run', False);
    if Executa then
      Registry.WriteString(Titulo, Programa)
    else
      Registry.WriteString(Titulo, '');
  finally
    Registry.Free;
  end;
end;

procedure TFmPrincipal.Executarnainicializao1Click(Sender: TObject);
begin
  ExecutaNaInicializacao(True, CO_Titulo, Application.ExeName);
end;

procedure TFmPrincipal.Famlias1Click(Sender: TObject);
begin
  SalNewJan.MostraFormProdutosFamil(0);
end;

procedure TFmPrincipal.Fechar1Click(Sender: TObject);
begin
  Close;
end;

procedure TFmPrincipal.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  //
  if not FCriouDB then
    TmConfiguraDB.Enabled := True;
  //
  LaP_PV1A.Font.Color := VAR_COR_TIT_A;
  LaP_PV1B.Font.Color := VAR_COR_TIT_B;
  LaP_PV1C.Font.Color := VAR_COR_TIT_C;
  //
  LaPVhora1.Font.Color := VAR_COR_AVISO_A;
  LaPVhora2.Font.Color := VAR_COR_AVISO_C;
  //
  TextoP_PVVazio('<?>');
end;

procedure TFmPrincipal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if TrayIcon1 <> nil then
    TrayIcon1.Visible := False;
end;

procedure TFmPrincipal.FormCreate(Sender: TObject);
var
  I: Integer;
begin
  FCriouDB   := False;
  FHides     := 0;
  FShowed    := True;
  FHideFirst := False;
  //
  VAR_USA_TAG_BITBTN := True;
  //
  Geral.DefineFormatacoes;
  Application.OnMessage := MyObjects.FormMsg;
  //
  AdvToolBarPager1.ActivePageIndex := 0;
  {XYZX PCBackupDir.ActivePageIndex      := 0;}
  //
  TmVersao.Enabled := True;
  //
{
  _AGORA := Now();
  TimerP_PV.Enabled := True;
  for I := 0 to 86400 do
    SecToday[I] := 0;
}
end;

procedure TFmPrincipal.FormShow(Sender: TObject);
begin
  MeuVCLSkin.VCLSkinDefineUso(sd1, skinstore1);
end;

procedure TFmPrincipal.GruposdeEmpresas1Click(Sender: TObject);
begin
  SalNewJan.MostraFormEntidadesGrup();
end;

procedure TFmPrincipal.Gruposdesegmentos1Click(Sender: TObject);
begin
  SalNewJan.MostraFormSegmenGruCads(0);
end;

procedure TFmPrincipal.JsonToMemo_1(JsonStr: String);
var
  js, lista, Objeto, SubObjeto:TlkJSONobject;
  ws: TlkJSONstring;
  s: String;
  i, j, k: Integer;
  //
 json,item, Subitem:TlkJSONbase;
 Nome, Valor: String;
begin
  Memo2.Lines.Clear;
{
  js := TlkJSONobject.Create;
//  js.add('namestring', TlkJSONstring.Generate('namevalue'));
  js.Add('namestring','namevalue');
// get the text of object
  s := TlkJSON.GenerateText(js);
  writeln(s);
  writeln;
  writeln('more readable variant:');
// (ver 1.03+) generate readable text
  i := 0;
  s := GenerateReadableText(js,i);
  writeln(s);

  js.Free;
// restore object (parse text)
  js := TlkJSON.ParseText(s) as TlkJSONobject;
// and get string back
// old syntax
  ws := js.Field['namestring'] as TlkJSONstring;
  s := ws.Value;
  writeln(s);
// syntax of 0.99+
  s := js.getString('namestring');
  writeln(s);

  readln;
  js.Free;
}
// restore object (parse text)
  js := TlkJSON.ParseText(JsonStr) as TlkJSONobject;
// and get string back
// old syntax
  //ws := js.Field['Head'] as TlkJSONstring;
  //s := ws.Value;
  //Memo2.Lines.Add(s);
// syntax of 0.99+
  //s := js.getString('Head');
  //Memo2.Lines.Add(s);



  json:= TlkJSON.ParseText(JsonStr);
  //for i := 0 to pred(json.Count) do
  for i := 0 to json.Count -1 do
  begin
    item := TlkJSONObject(json).child[i];
// This is the named array, name in name, the array in Objvalue
    Nome := TlkJSONobjectmethod(item).Name;
    //Memo2.Lines.Add(Nome);

    Objeto := json.Field[Nome] as TlkJSONobject;
    Memo2.Lines.Add(Nome + ' [' + Geral.FF0(Objeto.Count) + ' iten(s)]');

{   list is the array it self. This may be too much jiggling back and forth between object types, but at least it works...}
    //lista := TlkJSONobject(TlkJSONobjectmethod(item).Objvalue);
    for j := 0 to Objeto.Count - 1 do
    begin
      SubItem := TlkJSONObject(Objeto).child[j];
      if SubItem <> nil then
      begin
        Nome := TlkJSONobjectmethod(SubItem).Name;
        //Geral.MB_Info(Objeto.Field[Nome].ClassName);
        if Objeto.Field[Nome] is TlkJSONobject then
        begin
          SubObjeto := Objeto.Field[Nome] as TlkJSONobject;

          Memo2.Lines.Add('  ' + Nome + ' [' + Geral.FF0(SubObjeto.Count) + ' iten(s)]');
        end else
        begin
          Memo2.Lines.Add('  ' + Nome + ' = ' + Objeto.getString(Nome));
        end;
      end;
{
      lista:=TlkJSONobject(TlkJSONobjectmethod(item).Objvalue);
      //list is the array it self. This may be too much jiggling back and forth between object types, but at least it works...

      for k := 0 to pred(TlkJSONobject(lista).count) do
      begin
        Nome := lista.NameOf[k];
        if tlkJSONlist(lista).Child[k].Value <> null then
        begin
          Memo2.Lines.add(Nome + '->'+tlkJSONlist(lista).Child[k].Value);
        end;
      end;
}
    end;


  end;
  //readln;
  js.Free;
end;

procedure TFmPrincipal.JsonToMemo_2(JsonStr: String);
const
  Avisa = True;
var
  js, lista, Objeto, SubObjeto:TlkJSONobject;
  ws: TlkJSONstring;
  s: String;
  i, j, k: Integer;
  //
 json,item, Subitem:TlkJSONbase;
 Nome, Valor: String;
 //
 jParsed,
 jHead, jBody,
 jRequestArguments, jStatus, jTimestamp: TlkJSONbase;
 sTimeStamp: String;
 MyArr: TMyJsonBasesArr;
begin
  Memo2.Lines.Clear;
{
  js := TlkJSONobject.Create;
//  js.add('namestring', TlkJSONstring.Generate('namevalue'));
  js.Add('namestring','namevalue');
// get the text of object
  s := TlkJSON.GenerateText(js);
  writeln(s);
  writeln;
  writeln('more readable variant:');
// (ver 1.03+) generate readable text
  i := 0;
  s := GenerateReadableText(js,i);
  writeln(s);

  js.Free;
// restore object (parse text)
  js := TlkJSON.ParseText(s) as TlkJSONobject;
// and get string back
// old syntax
  ws := js.Field['namestring'] as TlkJSONstring;
  s := ws.Value;
  writeln(s);
// syntax of 0.99+
  s := js.getString('namestring');
  writeln(s);

  readln;
  js.Free;
}
// restore object (parse text)
  js := TlkJSON.ParseText(JsonStr) as TlkJSONobject;
// and get string back
// old syntax
  //ws := js.Field['Head'] as TlkJSONstring;
  //s := ws.Value;
  //Memo2.Lines.Add(s);
// syntax of 0.99+
  //s := js.getString('Head');
  //Memo2.Lines.Add(s);



  jParsed:= TlkJSON.ParseText(JsonStr);
(*
  SetLength(MyArr, 2);
*)
  if not MyJSON.SliceExpectedBaseItems(jParsed, Avisa, ['Head', 'Body'], MyArr) then
    Exit;
  jHead := MyArr[0];
  jBody := MyArr[1];
  //
  Geral.MB_Info(TlkJSON.GenerateText(jHead));
  if not MyJSON.SliceExpectedBaseItems(jHead, Avisa, ['RequestArguments', 'Status',
  'Timestamp'], MyArr) then
    Exit;
  jRequestArguments := MyArr[0];
  jStatus           := MyArr[1];
  jTimestamp        := MyArr[2];
  //
  sTimeStamp        := jTimestamp.Value;
  Geral.MB_Info(sTimeStamp);
  Geral.MB_Info('At� aqui OK!');
  //
{
  //for i := 0 to pred(json.Count) do
  for i := 0 to json.Count -1 do
  begin
    item := TlkJSONObject(json).child[i];
    // This is the named array, name in name, the array in Objvalue
    Nome := TlkJSONobjectmethod(item).Name;
    //Memo2.Lines.Add(Nome);

    Objeto := json.Field[Nome] as TlkJSONobject;
    Memo2.Lines.Add(Nome + ' [' + Geral.FF0(Objeto.Count) + ' iten(s)]');

    (* list is the array it self. This may be too much jiggling back and forth between object types, but at least it works...*)
    //lista := TlkJSONobject(TlkJSONobjectmethod(item).Objvalue);
    for j := 0 to Objeto.Count - 1 do
    begin
      SubItem := TlkJSONObject(Objeto).child[j];
      if SubItem <> nil then
      begin
        Nome := TlkJSONobjectmethod(SubItem).Name;
        //Geral.MB_Info(Objeto.Field[Nome].ClassName);
        if Objeto.Field[Nome] is TlkJSONobject then
        begin
          SubObjeto := Objeto.Field[Nome] as TlkJSONobject;

          Memo2.Lines.Add('  ' + Nome + ' [' + Geral.FF0(SubObjeto.Count) + ' iten(s)]');
        end else
        begin
          Memo2.Lines.Add('  ' + Nome + ' = ' + Objeto.getString(Nome));
        end;
      end;
(*      lista:=TlkJSONobject(TlkJSONobjectmethod(item).Objvalue);
      //list is the array it self. This may be too much jiggling back and forth between object types, but at least it works...

      for k := 0 to pred(TlkJSONobject(lista).count) do
      begin
        Nome := lista.NameOf[k];
        if tlkJSONlist(lista).Child[k].Value <> null then
        begin
          Memo2.Lines.add(Nome + '->'+tlkJSONlist(lista).Child[k].Value);
        end;
      end;
*)
    end;


  end;
  //readln;
}
  js.Free;
end;

procedure TFmPrincipal.Linhas1Click(Sender: TObject);
begin
  SalNewJan.MostraFormProdLinhasCab(0);
end;

{XYZX
procedure TFmPrincipal.MostraBackupDir(Codigo: Integer);
begin
  Application.CreateForm(TFmBackupDir, FmBackupDir);
  FmBackupDir.FCodigo := Codigo;
  FmBackupDir.FQuery  := QrBackupDir;
  FmBackupDir.ShowModal;
  FmBackupDir.Destroy;
  //
  ReopenBackupDir();
end;
}
procedure TFmPrincipal.Mostrar1Click(Sender: TObject);
begin
  FmPrincipal.Visible := True;
end;

{
function TFmPrincipal.MoveResulBackup(Zipar: Boolean; Caminho, NomeDir,
  Nome, Destino: String): Boolean;
var
  Continua: Boolean;
  CaminhoZip: String;
begin
  Continua := True;
  //
  //Move o arquivo resultante
  if Zipar then
  begin
    if dmkPF.DelDir(Caminho + '\' + NomeDir + Nome) then
    begin
      LBLista1.Items.Add('Aviso: Diret�rio tempor�rio removido com sucesso!');
    end else
    begin
      LBLista1.Items.Add('Erro: Falha ao remover o diret�rio tempor�rio!');
      Continua := False;
    end;
    CaminhoZip := Caminho + '\' + NomeDir + Nome + '.zip';
  end else
    CaminhoZip := Caminho + '\' + NomeDir + Nome;
  //
  if dmkPF.MoveDir(CaminhoZip, Destino + '\') = True then
  begin
    LBLista1.Items.Add('Aviso: Diret�rio movido com sucesso!');
  end else
  begin
    LBLista1.Items.Add('Erro: Falha ao mover diret�rio!');
    Continua := False;
  end;
  LBLista1.TopIndex := LBLista1.Items.Count - 1;
  //
  Result := Continua;
end;
}

procedure TFmPrincipal.NOexecutarnainicializao1Click(Sender: TObject);
begin
  ExecutaNaInicializacao(False, CO_Titulo, Application.ExeName);
end;

procedure TFmPrincipal.PMMenuPopup(Sender: TObject);
var
  Valor: String;
begin
  Valor := Geral.ReadKey(CO_Titulo, 'Software\Microsoft\Windows\CurrentVersion\Run', ktString, '', HKEY_CURRENT_USER);
  //
  if Valor <> '' then
  begin
    Executarnainicializao1.Checked   := True;
    NOexecutarnainicializao1.Checked := False;
  end else
  begin
    Executarnainicializao1.Checked   := False;
    NOexecutarnainicializao1.Checked := True;
  end;
end;

procedure TFmPrincipal.ReCaptionComponentesDeForm(Form: TForm);
begin
  //N�o Usa
end;

procedure TFmPrincipal.Segmentos1Click(Sender: TObject);
begin
  SalNewJan.MostraFormSegmentosCads(0);
end;

procedure TFmPrincipal.Similares1Click(Sender: TObject);
begin
  SalNewJan.MostraFormProdutosCabec(0);
end;

procedure TFmPrincipal.Subdivises1Click(Sender: TObject);
begin
  SalNewJan.MostraFormProdDivSubCab(0);
end;

procedure TFmPrincipal.Teste();
const
  DirF = 'http://www.sped.fazenda.gov.br/spedtabelas/appconsulta/obterTabelaExterna.aspx?';
  DirD = CO_DIR_RAIZ_DMK + '\SPED\DownLoadTabsEFD';
var
  I: Integer;
  Tabela, Fonte, Destino, Linha, Versao: String;
  lstArq(*, lstLin*): TStringList;

  Arquivo: TStringList;
  //Stream: TFileStream;
  //Stream: TMemoryStream;
  Stream: TStream;
  idPacote, idTabela: Integer;
  URL, tbVersao, arqVersao, noPacote, noTabela: String;
begin
  Memo1.Lines.Clear;
  Memo2.Lines.Clear;
  //
{
  PB2.Position := 0;
  //
  Screen.Cursor := crHourGlass;
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QrSPEDEFDTabT, DmodG.AllID_DB, [
      'SELECT bg.Nome noPacote, bt.Nome noTabela, bt.* ',
      'FROM spedefdtabt bt ',
      'LEFT JOIN spedefdtabg bg ON bg.Codigo = bt.Codigo ',
      '']);
    if QrSPEDEFDTabT.RecordCount > 0 then
    begin
      if not DirectoryExists(DirD) then
      begin
        if not ForceDirectories(DirD) then
        begin
          Geral.MB_Aviso('N�o foi poss�vel criar o diret�rio tempor�rio:' +
            sLineBreak + DirD + '!');
          Exit;
        end;
      end;
      //
      PB1.Position := 0;
      PB1.Max      := QrSPEDEFDTabT.RecordCount;
      //
      MyObjects.Informa(LaAviso1, False, 'Fazendo download de tabelas!');
      //
      QrSPEDEFDTabT.First;
      while not QrSPEDEFDTabT.EOF do
      begin
        idPacote := QrSPEDEFDTabT.FieldByName('Codigo').AsInteger;
        idTabela := QrSPEDEFDTabT.FieldByName('Controle').AsInteger;
        URL      := DirF + 'idPacote=' + Geral.FF0(idPacote) + '&idTabela=' + Geral.FF0(idTabela);
        //
        Destino := DirD + '\tb_' + Geral.FF0(idTabela) + '_' + Geral.FF0(idPacote) + '.txt';
        //
        if FileExists(Destino) then
        begin
          if not DeleteFile(Destino) then
          begin
            Geral.MB_Aviso('N�o foi poss�vel remover o arquivo tempor�rio:' +
              sLineBreak + Destino + '!');
            Exit;
          end;
        end;
        //
}




{
        Stream  := TStream.Create;
        try
          if not DmkWeb.URLGetMem(URL, Stream) then
          begin
            Geral.MB_Erro('Falha ao carregar tabelas!');
            Exit;
          end;
          Stream.Free;
        except
          Stream.Free;
          //
          Geral.MB_Erro('Falha ao carregar tabelas!');
          Exit;
        end;
        //
        PB1.Position := PB1.Position + 1;
        PB1.Update;
        Application.ProcessMessages;
        //
}




{
        QrSPEDEFDTabT.Next;
      end;
      PB1.Position := 0;
      PB1.Max      := QrSPEDEFDTabT.RecordCount;
      //
      MyObjects.Informa(LaAviso1, False, 'Verificando tabelas!');
      //
      QrSPEDEFDTabT.First;
      while not QrSPEDEFDTabT.EOF do
      begin
        idPacote  := QrSPEDEFDTabT.FieldByName('Codigo').AsInteger;
        idTabela  := QrSPEDEFDTabT.FieldByName('Controle').AsInteger;
        noPacote  := QrSPEDEFDTabT.FieldByName('noPacote').AsString;
        noTabela  := QrSPEDEFDTabT.FieldByName('noTabela').AsString;
        tbVersao  := QrSPEDEFDTabT.FieldByName('Versao').AsString;
        Destino   := DirD + '\tb_' + Geral.FF0(idTabela) + '_' + Geral.FF0(idPacote) + '.txt';
        Arquivo   := TStringList.Create;
        try
          Arquivo.LoadFromFile(Destino);
          //
          arqVersao := ObtemVersaoArquivo(Destino, Arquivo);
          //
          if (arqVersao <> tbVersao) or (CkForcaCad.Checked = True) then
          begin
            if AtualizaTabela(idTabela, noPacote + ' - ' + noTabela, arqVersao, Arquivo) then
            begin
              Memo2.Text := Geral.FF0(idPacote) + ' ' + noPacote + ' - ' +
                              Geral.FF0(idTabela) + ' ' + noTabela +
                              sLineBreak + Memo2.Text;
            end;
            if idTabela = CO_TB_Muni then
              AtualizaDTB_Munici()
            else if idTabela = CO_TB_Pais then
              AtualizaBacen_Pais();
          end;
        finally
          Arquivo.Free;
        end;
        //
        PB1.Position := PB1.Position + 1;
        PB1.Update;
        Application.ProcessMessages;
        //
        QrSPEDEFDTabT.Next;
      end;
    end;
  finally
    PB1.Position  := 0;
    Screen.Cursor := crDefault;
    //
    Geral.MB_Aviso('Importa��o finalizada!');
  end;
}
end;

procedure TFmPrincipal.TextoP_PVVazio(Msg: String);
var
  TxtAgora: String;
begin
  TxtAgora := Geral.FDT(Now(), 100);
  //
  LaP_PV1A.Caption := Msg;
  LaP_PV1B.Caption := Msg;
  LaP_PV1C.Caption := Msg;
  //
  LaPVhora1.Caption := TxtAgora;
  LaPVhora2.Caption := TxtAgora;
end;

procedure TFmPrincipal.TmConfiguraDBTimer(Sender: TObject);
begin
  TmConfiguraDB.Enabled := False;
  //
  ConfiguraDB;
end;

procedure TFmPrincipal.TmHideTimer(Sender: TObject);
begin
  TmHide.Enabled := False;
  //
  if Visible then
  begin
    if not FHideFirst then
    begin
      FHideFirst := True;
      Application.MainFormOnTaskBar := FShowed;
      Hide;
      //FmPrincipal.WindowState :=  wsMinimized;
      Application.MainFormOnTaskBar := FShowed;
      TrayIcon1.Visible := True;
    end;
  end;
end;

procedure TFmPrincipal.TmVersaoTimer(Sender: TObject);
begin
  TmVersao.Enabled := False;
  //
  if DmkWeb.RemoteConnection then
  begin
    if VerificaNovasVersoes(True) then
      DmkWeb.MostraBalloonHintMenuTopo(ATBBAtualiza, BalloonHint1,
        'H� uma nova vers�o!', 'Clique aqui para atualizar.');
  end;
end;

procedure TFmPrincipal.TrayIcon1Click(Sender: TObject);
begin
  FmPrincipal.Visible := True;
end;

procedure TFmPrincipal.Update1Click(Sender: TObject);
begin
  REInfo.Lines.Clear;
  REInfo.Visible := True;
  AppPF.ImportaDadosSalNew(REInfo);
{
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add(
  //' ATTACH "'+ Path + '" AS db2; ' +
  //' UPDATE main.' + Tabela + ' ' +
  //' SET data = (SELECT data FROM db2.' + Tabela + ' WHERE main.' + Tabela + '.id = db2.' + Tabela + '.id) ' +
  //' WHERE EXISTS(SELECT 1 FROM db2.' + Tabela + ' WHERE main.' + Tabela + '.id = db2.' + Tabela + '.id); ' +
  ' ATTACH DATABASE "C:\\Users\\Admin\\TFL\\Apps\\SQLite\\Quota.s3db" AS db2; ' +
  ' UPDATE main.embalagenscad ' +
  ' SET data = (SELECT data FROM db2.embalagenscad) ' +
  '');
  Dmod.QrUpd.ExecSQL;
}
end;

function TFmPrincipal.VerificaNovasVersoes(ApenasVerifica: Boolean): Boolean;
var
  Versao: Integer;
  ArqNome: String;
begin
  Result := DmkWeb.VerificaAtualizacaoVersao2(True, True, 'PjBsTFL', 'PjBsTFL',
              '', CO_VERSAO, CO_DMKID_APP, Now, nil, dtExecAux, Versao,
              ArqNome, False, ApenasVerifica, BalloonHint1);
end;

procedure TFmPrincipal.ExtrairArquivo;
var
  Arq: String;
begin
  Application.CreateForm(TFmZForge, FmZForge);
  FmZForge.Show;
  //
  Arq := FmZForge.ExtrairArquivo;
  //
  FmZForge.Destroy;
  //
  if (Arq <> '') and (FileExists(Arq)) then
  begin
    if Geral.MB_Pergunta('Arquivo extra�do com sucesso!' + sLineBreak +
      'Deseja abrir o diret�rio?') = ID_YES
    then
      Geral.AbreArquivo(Arq, True);
  end;
end;

function TFmPrincipal.ZipaArquivo(var Caminho: String; const Encryptar: Boolean;
  Senha: String): Boolean;
var
  CamZip: String;
begin
  CamZip := '';
  //
  Application.CreateForm(TFmZForge, FmZForge);
  FmZForge.Show;
  //
  CamZip := FmZForge.ZipaArquivo(zftDiretorio, ExtractFileDir(Caminho), Caminho,
              Geral.SemAcento(ExtractFileName(Caminho)), Senha, Encryptar, False);
  //
  FmZForge.Destroy;
  //
  if Length(CamZip) > 0 then
  begin
    Caminho := CamZip;
    Result  := True;
  end
  else
    Result := False;
end;

end.
