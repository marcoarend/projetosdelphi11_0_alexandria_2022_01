unit UnLock_Dmk;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, ZCF2, UnDmkProcFunc, UnDMkEnums, DmkDAC_PF, UnGrl_Vars,
  UnProjGroup_Consts;

type
  TipLibera = (Libera4, Libera5);
  TFmUnLock_Dmk = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1a: TLabel;
    LaAviso2a: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    LaAviso1b: TLabel;
    LaAviso2b: TLabel;
    PnAtiva: TPanel;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label4: TLabel;
    EdFimData: TdmkEdit;
    EdNomePC: TdmkEdit;
    CkAceito: TCheckBox;
    GroupBox3: TGroupBox;
    Label6: TLabel;
    Label2: TLabel;
    Label5: TLabel;
    Label7: TLabel;
    EdRazao: TdmkEdit;
    EdCNPJ: TdmkEdit;
    EdTelefone: TdmkEdit;
    EdEMail: TdmkEdit;
    QrTrm: TmySQLQuery;
    QrTrmConta: TAutoIncField;
    QrUpd: TmySQLQuery;
    QrAux: TmySQLQuery;
    Timer1: TTimer;
    PnAtivaManual: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel5: TPanel;
    BitBtn2: TBitBtn;
    MeLink: TMemo;
    TabSheet2: TTabSheet;
    MeAtiva: TMemo;
    Panel6: TPanel;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    BtAtiva: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure BtAtivaClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure MeLinkClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure CkAceitoClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    FSetTerminate, FEhServ: Boolean;
    procedure ConfiguraAvisoAbas(Aba: Integer);
    procedure RemoveTerminaisInativos();
    procedure AtualizaLicenca4();
    function AtualizaDadosNaDmk(Controle: Integer): Boolean;
    function DestravaAplicativo(): Boolean;
    //function AtualizaDadosTerminal(Controle: Integer; CNPJ: String): Boolean;
  public
    { Public declarations }
    FTipLibera: TipLibera;
    FContraSenha: String;
    FAtivou, FCrypt, FLicenca6: Boolean;
    FFatorCrypt, FCrypto1, FCrypto2, FLength: Integer;
    FRazaoSocial: String;
    FNome, FCNPJ, FCPF, FETE1, FPTe1, FEEMail, FPEMail: String;
    FTipo, FCodUsu: Integer;
{$IFDEF UsaWSuport}
    function  AtualizaLicenca5(RazaoSocial, CNPJ, NomePC, DescriPC: String;
                AtualizaDescriPC, AtivaManual, MostraMsg: Boolean): Integer;
{$ENDIF}
{$IFDEF UsaWSuport}
{$IFDEF UsaREST}
    function  AtualizaLicenca6(RazaoSocial, CNPJ, NomePC, DescriPC: String;
              AtualizaDescriPC, AtivaManual, MostraMsg: Boolean): Integer;
{$ENDIF}
{$ENDIF}
    procedure MostraEdicao(Manual: Boolean);
  end;

  var
  FmUnLock_Dmk: TFmUnLock_Dmk;

implementation

uses UnMyObjects, Module, ModuleGeral, UMySQLModule, MyListas, UnLic_Dmk,
{$IFDEF UsaWSuport} dmkSOAP, UnDmkWeb, {$ENDIF} MyDBCheck,
{$IFDEF UsaREST} UnGrl_DmkREST, {$ENDIF}
UnGrl_Consts;

{$R *.DFM}

procedure TFmUnLock_Dmk.BitBtn2Click(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 1;
  ConfiguraAvisoAbas(1);
end;

procedure TFmUnLock_Dmk.BitBtn3Click(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  ConfiguraAvisoAbas(0);
end;

procedure TFmUnLock_Dmk.BitBtn4Click(Sender: TObject);
var
  Licenca: Integer;
  RazaoSocial, CNPJ, DescriPC, NomePC: String;
begin
  CNPJ        := Geral.SoNumero_TT(EdCNPJ.ValueVariant);
  NomePC      := EdNomePC.ValueVariant;
  RazaoSocial := EdRazao.ValueVariant;
  DescriPC    := EdEMail.ValueVariant;
  //
{$IFDEF UsaWSuport}
{$IfDef UsaREST}
  if FLicenca6 then
    Licenca := AtualizaLicenca6(RazaoSocial, CNPJ, NomePC, DescriPC, True, True, True)
  else
{$EndIf}
    Licenca := AtualizaLicenca5(RazaoSocial, CNPJ, NomePC, DescriPC, True, True, True);
  //
  if Licenca = 1 then
  begin
    DmkWeb.ValidaModulosAplicativo(Dmod.QrUpd, DModG.QrMasterCNPJ.Value,
      CO_DMKID_APP);
    Close;
  end;
{$ELSE}
  Close;
{$ENDIF}
end;

procedure TFmUnLock_Dmk.BtAtivaClick(Sender: TObject);
var
  Servidor: Integer;
  CNPJ: String;
begin
{$IFNDEF NAO_USA_UnitMD5}
  if Geral.MB_Pergunta('Deseja utilizar a ativa��o manual?') <> ID_YES then Exit;
  //
  CNPJ := Geral.SoNumero_TT(EdCNPJ.ValueVariant);
  if DModG.EhOServidor() then
    Servidor := 1
  else
    Servidor := 0;
  //
  if Length(CNPJ) >0 then
  begin
    MostraEdicao(True);
    MeLink.Text := 'http://www.dermatek.net.br?page=licencas&serial=' +
      DmkPF.CalculaValSerialNum2('', CNPJ, CO_DMKID_APP, 0, True) +
      '&autoriz=' + DmkPF.CalculaValSerialKey2(Geral.FDT(Date, 1), True) +
      '&nomepc=' + Geral.IndyComputerName() + '&ip=' + Geral.ObtemIP(1) +
      '&servidor=' + FormatFloat('0', Servidor);
  end else
    Geral.MB_Aviso('CNPJ n�o definido!');
{$ENDIF}
end;

procedure TFmUnLock_Dmk.BtOKClick(Sender: TObject);
var
  Licenca: Integer;
  RazaoSocial, CNPJ, DescriPC, NomePC: String;
begin
  case FTipLibera of
    Libera4: AtualizaLicenca4;
    Libera5:
    begin
      CNPJ        := Geral.SoNumero_TT(EdCNPJ.ValueVariant);
      NomePC      := EdNomePC.ValueVariant;
      RazaoSocial := EdRazao.ValueVariant;
      DescriPC    := EdEMail.ValueVariant;
      //
{$IFDEF UsaWSuport}
{$IFDEF UsaREST}
      if FLicenca6 then
        Licenca := AtualizaLicenca6(RazaoSocial, CNPJ, NomePC, DescriPC, True, False, True)
      else
{$EndIf}
        Licenca := AtualizaLicenca5(RazaoSocial, CNPJ, NomePC, DescriPC, True, False, True);
      //
      if Licenca = 1 then
      begin
        DmkWeb.ValidaModulosAplicativo(Dmod.QrUpd, DModG.QrMasterCNPJ.Value,
          CO_DMKID_APP);
        RemoveTerminaisInativos();
        //
        VAR_TERMINATE := True;
        FSetTerminate := True;
        //
        Application.Terminate;
        Exit;
      end;
{$ENDIF}
    end;
  end;
end;

procedure TFmUnLock_Dmk.BtSaidaClick(Sender: TObject);
begin
  VAR_TERMINATE := True;
  Close;
end;

procedure TFmUnLock_Dmk.CkAceitoClick(Sender: TObject);
begin
  BtOK.Enabled := CkAceito.Checked;
end;

procedure TFmUnLock_Dmk.ConfiguraAvisoAbas(Aba: Integer);
begin
  if PnAtivaManual.Visible then
  begin
    case Aba of
      0:
      begin
        MyObjects.Informa2(LaAviso1a, LaAviso2a, False,
          'Copie o c�digo abaixo e cole na barra de endere�os do navegador de um computador que esteja ');
        MyObjects.Informa2(LaAviso1b, LaAviso2b, False,
          'conectado na internet e siga os procedimentos descritos no site.');
      end;
      1:
      begin
        MyObjects.Informa2(LaAviso1a, LaAviso2a, False,
          'Cole no campo abaixo o c�digo obtido no site atrav�s do link obtido na etapa anterior.');
        MyObjects.Informa2(LaAviso1b, LaAviso2b, False, '');
      end;
      else
      begin
        MyObjects.Informa2(LaAviso1a, LaAviso2a, False, '...');
        MyObjects.Informa2(LaAviso1b, LaAviso2b, False, '...');
      end;
    end;
  end else
  begin
    MyObjects.Informa2(LaAviso1a, LaAviso2a, False, '...');
    MyObjects.Informa2(LaAviso1b, LaAviso2b, False, '...');
  end;
end;

procedure TFmUnLock_Dmk.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  ImgTipo.SQLType := stPsq;
  //
  if FTipLibera = Libera4 then
  begin
    if not FAtivou then
    begin
      FAtivou := True;
      Timer1.Enabled := True;
    end;
  end;
end;

procedure TFmUnLock_Dmk.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if FSetTerminate = False then
    VAR_TERMINATE := True;
end;

procedure TFmUnLock_Dmk.FormCreate(Sender: TObject);
{
var
  Ordinal, i: Integer;
  Texto: String;
}
begin
  ImgTipo.SQLType := stIns;
  FSetTerminate   := False;
  FLicenca6       := False;
{
  FLength  := 12;//16;
  FCrypto1 := 17342;//70563;
  FCrypto2 := 75840;//87389;
  MCNova.Date := Int(Date) + 123;
  if Uppercase(Application.Title) = Uppercase('Destravador Dermatek - WEB') then
  begin
    EdRandom.ReadOnly := False;
  end else begin
    BtOk.Enabled := False;
    FCrypt := False;
    if FContraSenha = '' then
    begin
      Randomize;
      for i := 1 to FLength do
      begin
        Ordinal := 0;
        while ((Ordinal < 48) or (Ordinal > 122)) do Ordinal := Random(123);
        if (Ordinal > 047) and (Ordinal < 058) then Texto := Texto + char(Ordinal);
        if (Ordinal > 064) and (Ordinal < 091) then Texto := Texto + char(Ordinal);
        if (Ordinal > 096) and (Ordinal < 123) then Texto := Texto + char(Ordinal);
      end;
      EdRandom.Text := ParticionaTexto(Texto);
      FContraSenha  := Texto;
    end else EdRandom.Text := FContraSenha;
  end;
}
end;

procedure TFmUnLock_Dmk.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1a, LaAviso2a, LaAviso1b, LaAviso2b], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmUnLock_Dmk.FormShow(Sender: TObject);
begin
  EdNomePC.Text := Geral.IndyComputerName();
  EdFimData.ValueVariant := Date;
  FAtivou    := False;
  MostraEdicao(False);
end;

procedure TFmUnLock_Dmk.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  Application.ProcessMessages;
  Screen.Cursor := crHourGlass;
  try
    DModG.QrWAgora.Close;
    UnDmkDAC_PF.AbreQueryApenas(DModG.QrWAgora);
    //
    //ShowMessage(FormatDateTime('dd/mm/yyyy hh:nn:ss:zzz', DModG.QrWAgoraAGORA.Value));
    EdFimData.ValueVariant := Int(DModG.QrWAgoraAGORA.Value) + 31;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmUnLock_Dmk.AtualizaLicenca4();
var
  Licenca, InfoN, InfoC, FimChek, FimData, InfoT, InfoE, NomePC, CadAlt,
  strIP: String;
  Servidor, Codigo, Controle, Vezes, Conta: Integer;
  TipoSQL: TSQLType;
begin
  InfoN := EdRazao.Text;
  if MyObjects.FIC(Trim(InfoN) = '', EdRazao, 'Informe o nome da empresa!') then Exit;
  //
  InfoC := Geral.SoNumero_TT(EdCNPJ.Text);
  if MyObjects.FIC(Trim(InfoC) = '', EdCNPJ, 'Informe o CNPJ!') then Exit;
  //
  InfoT := Geral.SoNumero1a9_TT(EdTelefone.Text);
  if MyObjects.FIC(Trim(InfoT) = '', EdTelefone, 'Informe o n�mero do telefone para contato!') then Exit;
  InfoT := Geral.SoNumero_TT(EdTelefone.Text);
  //
  InfoE := EdEmail.Text;
  if MyObjects.FIC(Trim(InfoE) = '', EdEmail, 'Informe o e-mail para contato!') then Exit;
  if MyObjects.FIC(Geral.ValidaEMail(InfoE) = False, EdEmail, 'E-mail inv�lido!') then Exit;
  //
  NomePC    := EdNomePC.Text;
  //
  Screen.Cursor := crHourGlass;
  try
    if Length(InfoC) >= 14 then
    begin
      FRazaoSocial := InfoN;
      FNome        := '';
      FCNPJ        := InfoC;
      FCPF         := '';
      FTipo        := 0;
      FETe1        := InfoT;
      FPTe1        := '';
      FEEMail      := InfoE;
      FPEMail      := '';
    end else begin
      FRazaoSocial := '';
      FNome        := InfoN;
      FCNPJ        := '';
      FCPF         := InfoC;
      FTipo        := 1;
      FETe1        := '';
      FPTe1        := InfoT;
      FEEMail      := '';
      FPEMail      := InfoE;
    end;
    DModG.QrEntDmk.Close;
    //
    DModG.QrEntDmk.Params[00].AsString := InfoC;
    DModG.QrEntDmk.Params[01].AsString := InfoC;
    UnDmkDAC_PF.AbreQueryApenas(DModG.QrEntDmk);
    if FEhServ then
    begin
      Servidor := 1;
      // Cadastra o cliente no site da dermatek
      if DModG.QrEntDmk.RecordCount = 0 then
      begin
        QrAux.Close;
        QrAux.SQL.Clear;
        QrAux.SQL.Add('LOCK TABLES entidades WRITE;');
        QrAux.ExecSQL;
        QrAux.SQL.Clear;
        QrAux.SQL.Add('SELECT MIN(Codigo) Codigo');
        QrAux.SQL.Add('FROM entidades;');
        UnDmkDAC_PF.AbreQueryApenas(QrAux);
        Codigo := QrAux.FieldByName('Codigo').AsInteger;
        //
        if Codigo > -1000 then
          Codigo := -1001
        else Codigo := Codigo -1;
        //
        FCodUsu := Codigo;
        QrAux.Close;

        UMyMod.SQLInsUpd(QrUpd, stIns, 'entidades', False, [
        'Tipo', 'CodUsu', 'RazaoSocial', 'Nome', 'CNPJ', 'CPF', 'ETe1', 'PTe1',
        'EEMail', 'PEMail'], ['Codigo'
        ], [FTipo, FCodUsu, FRazaoSocial, FNome, FCNPJ, FCPF, FETE1, FPTe1,
        FEEMail, FPEMail], [Codigo
        ], True);
        //
        QrAux.Close;
        QrAux.SQL.Clear;
        QrAux.SQL.Add('UNLOCK TABLES;');
        QrAux.ExecSQL;
        //
      end else Codigo := DModG.QrEntDmkCodigo.Value;
      //
      DModG.QrCliAplic.Close;
      DModG.QrCliAplic.Params[00].AsInteger := Codigo;
      DModG.QrCliAplic.Params[01].AsInteger := CO_DMKID_APP;
      UnDmkDAC_PF.AbreQueryApenas(DModG.QrCliAplic);
      //
      //  Cadastra Aplicativo para o cliente
      if DModG.QrCliAplic.RecordCount = 0 then
      begin
        QrAux.Close;
        QrAux.SQL.Clear;
        QrAux.SQL.Add('LOCK TABLES cliaplic WRITE;');
        QrAux.ExecSQL;
        QrAux.SQL.Clear;
        QrAux.SQL.Add('SELECT MIN(Controle) Controle');
        QrAux.SQL.Add('FROM cliaplic;');
        UnDmkDAC_PF.AbreQueryApenas(QrAux);
        Controle := QrAux.FieldByName('Controle').AsInteger;
        //
        if Controle > -1000 then
          Controle := -1001
        else Controle := Controle -1;
        //
        QrAux.Close;
        UMyMod.SQLInsUpd(QrUpd, stIns, 'cliaplic', False, [
        'Codigo', 'Aplicativo', 'Ativo', 'Telefone', 'EMail', 'NomeServer'], ['Controle'
        ], [Codigo, CO_DMKID_APP, 0, InfoT, InfoE, NomePC], [Controle
        ], True);
        //
        QrAux.Close;
        QrAux.SQL.Clear;
        QrAux.SQL.Add('UNLOCK TABLES;');
        QrAux.ExecSQL;
        //
      end else Controle := DModG.QrCliAplicControle.Value;
      //
      FimData := Geral.FDT(EdFimData.ValueVariant, 1);
      FimChek := Lic_Dmk.ChecksumLicenca(
        EdFimData.ValueVariant, InfoC);
      UMyMod.SQLInsUpd(QrUpd, stUpd, 'cliaplic', False, [
      'FimData', 'FimChek'], ['Controle'
      ], [FimData, FimChek], [Controle
      ], True);
    end // Fim se for servidor
    else begin
      Servidor := 0;
      Codigo := DModG.QrEntDmkCodigo.Value;
      //
      DModG.QrCliAplic.Close;
      DModG.QrCliAplic.Params[00].AsInteger := Codigo;
      DModG.QrCliAplic.Params[01].AsInteger := CO_DMKID_APP;
      UnDmkDAC_PF.AbreQueryApenas(DModG.QrCliAplic);
      //
      Controle := DModG.QrCliAplicControle.Value;
    end;
    //
    //Cadastra a m�quina
    DModG.QrCliAplicLi.Close;
    DModG.QrCliAplicLi.Params[00].AsInteger := Controle;
    DModG.QrCliAplicLi.Params[01].AsString  := DModG.FSerialKey;
    DModG.QrCliAplicLi.Params[02].AsString  := DModG.FSerialNum;
    UnDmkDAC_PF.AbreQueryApenas(DModG.QrCliAplicLi);
    Vezes := DModG.QrCliAplicLiConsultaNV.Value + 1;
    Conta := DModG.QrCliAplicLiConta.Value;
    DModG.QrWAgora.Close;
    UnDmkDAC_PF.AbreQueryApenas(DModG.QrWAgora);
    CadAlt := Geral.FDT(DModG.QrWAgoraAgora.Value, 105);
    strIP := Geral.ObtemIP(1);
    if DModG.QrCliAplicLi.RecordCount > 0 then
    begin
      UMyMod.SQLInsUpd(QrUpd, stUpd, 'cliaplicli', False, [
      'Controle', 'NomePC', 'IP', 'SerialKey', 'SerialNum', 'Cadastro'], ['Conta'], [
      Controle, NomePC, strIP, DModG.FSerialKey, DModG.FSerialNum, CadAlt], [Conta], True);
    end else begin
      UMyMod.SQLInsUpd(QrUpd, stIns, 'cliaplicli', True, [
      'Controle', 'NomePC', 'IP', 'SerialKey', 'SerialNum', 'ConsultaDH', 'ConsultaNV'], [(*'Conta'*)], [
      Controle, NomePC, strIP, DModG.FSerialKey, DModG.FSerialNum, CadAlt, Vezes], [(*Conta*)], True);
      DModG.QrCliAplicLi.Close;
      DModG.QrCliAplicLi.Params[00].AsInteger := Controle;
      DModG.QrCliAplicLi.Params[01].AsString  := DModG.FSerialKey;
      DModG.QrCliAplicLi.Params[02].AsString  := DModG.FSerialNum;
      UnDmkDAC_PF.AbreQueryApenas(DModG.QrCliAplicLi);
      Conta := DModG.QrCliAplicLiConta.Value;
    end;
    AtualizaDadosNaDmk(Controle);
    //
    if DestravaAplicativo() then
    begin
      if Servidor = 1 then
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'terminais', False, [
        'Servidor'], [], [0], [], False);
      end;
      DModG.ReopenTerminal();
      if DModG.QrTerminal.RecordCount = 0 then
        TipoSQL := stIns
      else
        TipoSQL := stUpd;
      UMyMod.SQLInsUpd(Dmod.QrUpd, TipoSQL, 'terminais', False, [
      'Licenca', 'Servidor', 'IP', 'Terminal'], ['SerialKey', 'SerialNum'],[
      Licenca, Servidor, strIP, Conta], [DModG.FSerialKey, DModG.FSerialNum], True);
      //
      Geral.MB_Aviso(
      'O aplicativo ser� finalizado. � necess�rio reinici�-lo manualmente!');
      ZZTerminate := True;
    end else
      Geral.MB_Aviso('N�o foi poss�vel validar esta c�pia do ' +
      Application.Title + '!' + sLineBreak + 'Avise a DERMATEK!');
  finally
    Screen.Cursor := crDefault;
  end;
  Close;
end;

function TFmUnLock_Dmk.AtualizaDadosNaDmk(Controle: Integer): Boolean;
var
  EntiMinD, EntiAtzD, LctsMinD, LctsAtzD: TDateTime;
  EntiNCad, LctsNCad: Integer;
begin
  Result   := False;
  //
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT MIN(DataCad) DataCad');
  Dmod.QrAux.SQL.Add('FROM entidades');
  Dmod.QrAux.SQL.Add('WHERE DataCad > 0');
  UnDmkDAC_PF.AbreQueryApenas(Dmod.QrAux);
  EntiMinD := Dmod.QrAux.FieldByName('DataCad').AsDateTime;
  //
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT COUNT(Codigo) Itens');
  Dmod.QrAux.SQL.Add('FROM entidades');
  UnDmkDAC_PF.AbreQueryApenas(Dmod.QrAux);
  EntiNCad := Dmod.QrAux.FieldByName('Itens').AsInteger;
  //
{
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT MIN(DataCad) DataCad');
  Dmod.QrAux.SQL.Add('FROM ' + VAR LCT);
  Dmod.QrAux.SQL.Add('WHERE DataCad > 0');
  UnDmkDAC_PF.AbreQueryApenas(Dmod.QrAux);
  LctsMinD := Dmod.QrAux.FieldByName('DataCad').AsDateTime;
}
  LctsMinD := 0;
  //
{
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT COUNT(Controle) Itens');
  Dmod.QrAux.SQL.Add('FROM ' + VAR LCT);
  UnDmkDAC_PF.AbreQueryApenas(Dmod.QrAux);
  LctsNCad := Dmod.QrAux.FieldByName('Itens').AsInteger;
  //
}
  LctsNCad := 0;
  //
  EntiAtzD := DModG.QrWAgoraAGORA.Value;
  LctsAtzD := DModG.QrWAgoraAGORA.Value;
  //
  if UMyMod.SQLInsUpd(QrUpd, stUpd, 'cliaplic', False, [
  'EntiMinD', 'EntiAtzD', 'EntiNCad',
  'LctsMinD', 'LctsAtzD', 'LctsNCad'], [
  'Controle'], [
  EntiMinD, EntiAtzD, EntiNCad,
  LctsMinD, LctsAtzD, LctsNCad], [
  Controle], False) then Result := True;
end;

function TFmUnLock_Dmk.DestravaAplicativo(): Boolean;
var
  Licenca, CNPJ: String;
  Dono: Integer;
  TipoSQL: TSQLType;
begin
  Result := False;
  try
    //
    DModG.QrCliAplic.Close;
    UnDmkDAC_PF.AbreQueryApenas(DModG.QrCliAplic);
    //
    DModG.QrEntDmk.Close;
    UnDmkDAC_PF.AbreQueryApenas(DModG.QrEntDmk);
    //
    DModG.ReopenMaster('TFmUnLock_Dmk.641');
    //
    Dmod.ReopenControle();
    //
    Dono := Dmod.QrControle.FieldByName('Dono').AsInteger;
    if Dono = 0 then Dono := -11;
    CNPJ := DModG.QrEntDmkCPFJ.Value;
    //
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT Dono FROM controle');
    UnDmkDAC_PF.AbreQueryApenas(Dmod.QrAux);
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('');
    if Dmod.QrAux.RecordCount > 0 then
      Dmod.QrUpd.SQL.Add('UPDATE controle SET ContraSenha="", Dono=:P0, CNPJ=:P1')
    else
      Dmod.QrUpd.SQL.Add('INSERT INTO controle SET Dono=:P0, CNPJ=:P1');
    Dmod.QrUpd.Params[00].AsInteger := Dono;
    Dmod.QrUpd.Params[01].AsString  := CNPJ;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;

    // Pode ainda n�o haver dono!!!
    if not Lic_Dmk.CalculaValLicenca(CNPJ, Licenca) then
      Exit;
    //
    if DModG.QrMaster.RecordCount > 0 then
    begin
      Dmod.QrUpd.SQL.Add('UPDATE master SET ');
    end else
    begin
      Dmod.QrUpd.SQL.Add('INSERT INTO master SET DataI="' + Geral.FDT(DModG.QrWAgoraAGORA.Value, 1) + '", ');
    end;
    Dmod.QrUpd.SQL.Add('Monitorar=1, Distorcao=0, Limite=0, DiasExtra=0, LicInfo="", ');
    Dmod.QrUpd.SQL.Add('Hoje="' + Geral.FDT(DModG.QrWAgoraAGORA.Value, 1) + '", ');
    Dmod.QrUpd.SQL.Add('Hora="'+ Geral.FDT(DModG.QrWAgoraAGORA.Value, 102) + '", ');
    Dmod.QrUpd.SQL.Add('DataF="'+ Geral.FDT(DModG.QrCliAplicFimData.Value, 1) +'",');
    Dmod.QrUpd.SQL.Add('Em="' + DModG.QrEntDmkNO_ENT.Value + '", ');
    Dmod.QrUpd.SQL.Add('CNPJ="' + DModG.QrEntDmkCPFJ.Value + '" ,');
    Dmod.QrUpd.SQL.Add('Licenca="' + Licenca + '",');
    Dmod.QrUpd.SQL.Add('ChekF="' + DModG.QrCliAplicFimChek.Value + '",');
    Dmod.QrUpd.SQL.Add('LicTel="' + DModG.QrEntDmkLicTel.Value + '", ');
    Dmod.QrUpd.SQL.Add('LicMail="' + DModG.QrEntDmkLicMail.Value + '"');

    if (DModG.QrMasterMasSenha.Value = '') and (DModG.QrMasterMasLogin.Value = '') then
    begin
      Dmod.QrUpd.SQL.Add(', SitSenha=1, MasLogin=''geren'', MasSenha=AES_ENCRYPT(''geren'', ''' +
      CO_USERSPNOW + ''')');
    end;
    Dmod.QrUpd.ExecSQL;
    //  O terminal ser� cadastrado automaticamente
    (*
    VAR_ESTEIP := ObtemIP(1);
    Dmod.QrTerminal.Close;
    Dmod.QrTerminal.Params[0].AsString := VAR_IP_LICENCA;
    UnDmkDAC_PF.AbreQueryApenas(Dmod.QrTerminal);
    //
    if Dmod.QrTerminal.RecordCount = 0 then
    begin
      VAR_IP_LICENCA := '';
      MyObjects.FormShow(TFmTerminais, FmTerminais);
      Geral.WriteAppKeyLM2('IPClient', Application.Title, VAR_IP_LICENCA, ktString);
    end;
    if VAR_IP_LICENCA <> '' then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE terminais SET Licenca=:P0 WHERE IP=:P1 ');
      Dmod.QrUpd.Params[0].AsFloat  := Licenca;
      Dmod.QrUpd.Params[1].AsString := VAR_IP_LICENCA;
      Dmod.QrUpd.ExecSQL;
      //
    end else begin
      Geral.MB_Aviso('IP sem Terminal definido!'+
      sLineBreak+'IP: '+VAR_IP_LICENCA));
    end;
    *)
    //
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT Codigo, CodUsu');
    Dmod.QrAux.SQL.Add('FROM entidades');
    Dmod.QrAux.SQL.Add('WHERE Codigo=:P0');
    Dmod.QrAux.Params[0].AsInteger := Dono;
    UnDmkDAC_PF.AbreQueryApenas(Dmod.QrAux);
    //
    if Dmod.QrAux.RecordCount > 0 then
    begin
      TipoSQL := stUpd;
      FCodUsu := Dmod.QrAux.FieldByName('CodUsu').AsInteger;
    end else begin
      TipoSQL := stIns;
      FCodUsu := Dono;
    end;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, TipoSQL, 'entidades', False, [
    'Tipo', 'CodUsu', 'RazaoSocial', 'Nome', 'CNPJ', 'CPF', 'ETe1', 'PTe1',
    'EEMail', 'PEMail'], ['Codigo'
    ], [FTipo, FCodUsu, FRazaoSocial, FNome, FCNPJ, FCPF, FETE1, FPTe1,
    FEEMail, FPEMail], [Dono], True);
    //
    Result := True;
  except;
    raise;
  end;
end;

{$IFDEF UsaWSuport}
function TFmUnLock_Dmk.AtualizaLicenca5(RazaoSocial, CNPJ, NomePC,
  DescriPC: String; AtualizaDescriPC, AtivaManual, MostraMsg: Boolean): Integer;
var
  Valida, Tipo: Integer;
  Terminal, Servidor, CodAplic, PosMan, QtdCliInt: Integer;
  Doc, SerialKeyOld, SerialKey, SerialNum, IP, IPServ, DataVerifi, Enti,
  OSVersion, DBVersion: String;
  SQLType: TSQLType;
  Res: ArrayOfStrings;
  LinkMan: WideString;
  SerialNumObt, SerialKeyObt, DataObt, StatusObt: String;
begin
  /////////////////////////////////
  ///0 - N�o liberou
  ///1 - Liberou
  ///2 - N�o conseguiu verificar
  /////////////////////////////////
  //
  Result    := 0;
  Valida    := 0;
  IP        := Geral.ObtemIP(1);
  CodAplic  := CO_DMKID_APP;
  SerialNum := DmkPF.CalculaValSerialNum2('', CNPJ, CodAplic, 0, True);
  SerialKey := DmkPF.CalculaValSerialKey2(Geral.FDT(DmodG.ObtemAgora(), 1), True);
  //
  if DModG.EhOServidor then
  begin
    Servidor := 1;
    IPServ   := Geral.ObtemIP(1)
  end else
  begin
    Servidor := 0;
    IPServ   := VAR_IP;
  end;
  //
  if AtivaManual then
  begin
    LinkMan      := Trim(dmkPF.PWDExDecode(MeAtiva.Text, CO_RandStrWeb01));
    PosMan       := Pos('&', LinkMan);
    SerialNumObt := Copy(LinkMan, 0, PosMan - 1);
    LinkMan      := Copy(LinkMan, PosMan + 1);
    PosMan       := Pos('&', LinkMan);
    SerialKeyObt := Copy(LinkMan, 0, PosMan - 1);
    LinkMan      := Copy(LinkMan, PosMan + 1);
    PosMan       := Pos('&', LinkMan);
    DataObt      := Copy(LinkMan, 0, PosMan - 1);
    StatusObt    := Copy(LinkMan, PosMan + 1);
    //
    if (SerialNum = SerialNumObt) and (SerialKey = SerialKeyObt) then
    begin
      Valida := 1;
    end else
    begin
      if MostraMsg then
        Geral.MB_Aviso('C�digo inv�lido!');
    end;
  end else
  begin
    try
      try
        Screen.Cursor := crHourGlass;
        //
        OSVersion := dmkPF.ObtemSysInfo2('||');
        DBVersion := DModG.ObterDBVersao;
        //
        Res := GetVCLWebServicePortType2.UnLockAplic(SerialKey, SerialNum, CNPJ,
          DescriPC, NomePC, IP, Geral.FF0(CO_VERSAO), Servidor, CodAplic,
          AtualizaDescriPC, OSVersion, DBVersion, IPServ);
        //
        Screen.Cursor := crDefault;
      except
        if Length(Res) = 0 then
        begin
          SetLength(Res, 4);
          Res[0] := Geral.FF0(-1); //N�o conseguiu verificar
          Res[1] := 'N�o foi poss�vel ativar o aplicativo tente a ativa��o manual!';
          Res[2] := '';
          Res[3] := '';
          Res[4] := '0';
        end;
        //
        Screen.Cursor := crDefault;
      end;
      //Retorna:
      //0 => C�digo da mensagem
      //1 => Texto da mensagem
      //2 => Data que expira a licen�a
      //3 => Status do aplicativo
      //
      if (Length(Res) > 0) and (Geral.IMV(Res[0]) <> 100) then
      begin
        if MostraMsg then
          Geral.MB_Aviso(Res[0] + sLineBreak + Res[1] + sLineBreak + Res[2] + sLineBreak
            + Res[3]);
        if Geral.IMV(Res[0]) = -1 then //N�o conseguiu verificar por problemas na conex�o
          Result := 2;
      end else
      begin
        DataObt   := Res[2];
        StatusObt := Res[3];
        QtdCliInt := Geral.IMV(Res[4]);
        Valida    := 1;
      end;
    except
      if MostraMsg then
        Geral.MB_Aviso('N�o foi poss�vel ativar o aplicativo tente a ativa��o manual!');
      if Res[0] = Geral.FF0(-1) then
        Valida := 2
      else
        Valida := 0;
    end;
  end;
  if Valida = 1 then
  begin 
    DataVerifi   := Geral.FDT(Date, 1);
    SerialNum    := DmkPF.CalculaValSerialNum2(DataObt, CNPJ, CodAplic,
                      Geral.IMV(StatusObt), False);
    SerialKey    := DmkPF.CalculaValSerialKey2(DataVerifi, False);
    SerialKeyOld := Geral.ReadAppKeyCU(TMeuDB + '_SerialKey', Application.Title, ktString, '');
    //
    //Geral.WriteAppKeyCU('SerialNum', Application.Title, SerialNum, ktString);
    //Geral.WriteAppKeyCU('SerialKey', Application.Title, SerialKey, ktString);
    Geral.WriteAppKeyCU(TMeuDB + '_SerialNum', Application.Title, SerialNum, ktString);
    Geral.WriteAppKeyCU(TMeuDB + '_SerialKey', Application.Title, SerialKey, ktString);
    //
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT Terminal');
    Dmod.QrAux.SQL.Add('FROM terminais');
    Dmod.QrAux.SQL.Add('WHERE SerialNum=:P0');
    Dmod.QrAux.SQL.Add('AND SerialKey=:P1');
    Dmod.QrAux.Params[0].AsString := SerialNum;
    Dmod.QrAux.Params[1].AsString := SerialKeyOld;
    UnDmkDAC_PF.AbreQueryApenas(Dmod.QrAux);
    //
    if Dmod.QrAux.RecordCount > 0 then
    begin
      SQLType := stUpd;
      Terminal := Dmod.QrAux.FieldByName('Terminal').Value;
    end else
    begin
      SQLType := stIns;
      Terminal := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'terminais', 'Terminal',
        [], [], stIns, 0, siPositivo, nil);
    end;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'terminais', False,
      ['IP', 'SerialNum', 'SerialKey', 'Servidor', 'NomePC', 'DescriPC'],
      ['Terminal'],
      [IP, SerialNum, SerialKey, Servidor, NomePC, DescriPC],
      [Terminal], True) then
    begin
      Dmod.QrAux.Close;
      Dmod.QrAux.SQL.Clear;
      Dmod.QrAux.SQL.Add('SELECT Dono FROM controle');
      UnDmkDAC_PF.AbreQueryApenas(Dmod.QrAux);
      //
      Dmod.QrUpd.SQL.Clear;
      //
      if Dmod.QrAux.RecordCount > 0 then
        Dmod.QrUpd.SQL.Add('UPDATE controle SET ContraSenha="", Dono=:P0, CNPJ=:P1')
      else
        Dmod.QrUpd.SQL.Add('INSERT INTO controle SET Dono=:P0, CNPJ=:P1');
      Dmod.QrUpd.Params[0].AsInteger := -11;
      Dmod.QrUpd.Params[1].AsString  := CNPJ;
      Dmod.QrUpd.ExecSQL;
      //
      Dmod.QrAux.Close;
      Dmod.QrAux.SQL.Clear;
      Dmod.QrAux.SQL.Add('SELECT * FROM master');
      UnDmkDAC_PF.AbreQueryApenas(Dmod.QrAux);
      //
      Dmod.QrUpd.SQL.Clear;
      if Dmod.QrAux.RecordCount > 0 then
      begin
        Dmod.QrUpd.SQL.Add('UPDATE master SET ');
      end else
      begin
        Dmod.QrUpd.SQL.Add('INSERT INTO master SET ');
      end;
      Dmod.QrUpd.SQL.Add('DataF="' + DataObt + '", ');
      Dmod.QrUpd.SQL.Add('Monitorar="' + StatusObt + '", ');
      Dmod.QrUpd.SQL.Add('QtdCliInt="' + Geral.FF0(QtdCliInt) + '", ');
      Dmod.QrUpd.SQL.Add('Em="' + RazaoSocial + '", ');
      Dmod.QrUpd.SQL.Add('CNPJ="' + CNPJ + '" ');
      Dmod.QrUpd.ExecSQL;
      //
      Dmod.QrAux.Close;
      Dmod.QrAux.SQL.Clear;
      Dmod.QrAux.SQL.Add('SELECT Tipo FROM entidades WHERE Codigo in (-1,-11)');
      UnDmkDAC_PF.AbreQueryApenas(Dmod.QrAux);
      //
      Dmod.QrUpd.SQL.Clear;
      //
      if Dmod.QrAux.RecordCount > 0 then
      begin
        if Length(CNPJ) = 11 then
          Tipo := 1
        else
          Tipo := 0;
        //
        Dmod.QrUpd.SQL.Add('UPDATE entidades SET AlterWeb=1, Tipo=' + Geral.FF0(Tipo) + ', ');
      end else
      begin
        if Length(CNPJ) = 11 then
        begin
          Dmod.QrUpd.SQL.Add('INSERT INTO entidades SET Tipo=1, ');
          //
          Tipo := 1;
        end else
        begin
          Dmod.QrUpd.SQL.Add('INSERT INTO entidades SET Tipo=0, ');
          //
          Tipo := 2;
        end;
      end;
      //
      if Tipo = 1 then
      begin
        Enti := 'Nome';
        Doc  := 'CPF';
      end else
      begin
        Enti := 'RazaoSocial';
        Doc  := 'CNPJ';
      end;
      Dmod.QrUpd.SQL.Add(Enti + '=:P0, ' + Doc + '=:P1');
      if Dmod.QrAux.RecordCount > 0 then
        Dmod.QrUpd.SQL.Add('WHERE Codigo in (-1,-11)')
      else
        Dmod.QrUpd.SQL.Add(', Filial=1, CliInt=1, Codigo=-11');
      Dmod.QrUpd.Params[0].AsString := RazaoSocial;
      Dmod.QrUpd.Params[1].AsString := CNPJ;
      Dmod.QrUpd.ExecSQL;
      //
      DBCheck.VerificaEntiCliInt;
      //
      Result := 1;
    end;
  end;
end;
{$ENDIF}

{$IFDEF UsaWSuport}
{$IFDEF UsaREST}
function TFmUnLock_Dmk.AtualizaLicenca6(RazaoSocial, CNPJ, NomePC,
  DescriPC: String; AtualizaDescriPC, AtivaManual, MostraMsg: Boolean): Integer;
const
  TimeOut = 2000; //2 segundos
var
  Valida, Tipo, Resul: Integer;
  Terminal, Servidor, CodAplic, PosMan: Integer;
  Doc, SerialKeyOld, SerialKey, SerialNum, IP, IPServ, DataVerifi, Enti,
  OSVersion, DBVersion, Resultado: String;
  SQLType: TSQLType;
  Res: ArrayOfStrings;
  LinkMan: WideString;
  SerialNumObt, SerialKeyObt, Vencimento, Status, AtualizadoEm, Mensagem: String;
  CliInt: Integer;
begin
  /////////////////////////////////
  ///0 - N�o liberou
  ///1 - Liberou
  ///2 - N�o conseguiu verificar
  /////////////////////////////////
  //
  Result    := 0;
  Valida    := 0;
  IP        := Geral.ObtemIP(1);
  CodAplic  := CO_DMKID_APP;
  SerialNum := DmkPF.CalculaValSerialNum2('', CNPJ, CodAplic, 0, True);
  SerialKey := DmkPF.CalculaValSerialKey2(Geral.FDT(DmodG.ObtemAgora(), 1), True);
  //
  if DModG.EhOServidor then
  begin
    Servidor := 1;
    IPServ   := Geral.ObtemIP(1)
  end else
  begin
    Servidor := 0;
    IPServ   := VAR_IP;
  end;
  //
  if AtivaManual then
  begin
    LinkMan      := Trim(dmkPF.PWDExDecode(MeAtiva.Text, CO_RandStrWeb01));
    PosMan       := Pos('&', LinkMan);
    SerialNumObt := Copy(LinkMan, 0, PosMan - 1);
    LinkMan      := Copy(LinkMan, PosMan + 1);
    PosMan       := Pos('&', LinkMan);
    SerialKeyObt := Copy(LinkMan, 0, PosMan - 1);
    LinkMan      := Copy(LinkMan, PosMan + 1);
    PosMan       := Pos('&', LinkMan);
    Vencimento   := Copy(LinkMan, 0, PosMan - 1);
    Status       := Copy(LinkMan, PosMan + 1);
    //
    if (SerialNum = SerialNumObt) and (SerialKey = SerialKeyObt) then
    begin
      Valida := 1;
    end else
    begin
      if MostraMsg then
        Geral.MB_Aviso('C�digo inv�lido!');
    end;
  end else
  begin
    try
      try
        Screen.Cursor := crHourGlass;
        //
        OSVersion := dmkPF.ObtemSysInfo2('||');
        DBVersion := DModG.ObterDBVersao;
        //
        Resul := Grl_DmkREST.UnLockAplic(False, SerialKey, SerialNum, CNPJ,
                 DescriPC, NomePC, IP, Geral.FF0(CO_VERSAO), Servidor, CodAplic,
                 AtualizaDescriPC, OSVersion, DBVersion, IPServ, Vencimento,
                 Status, CliInt, AtualizadoEm, Mensagem);
        //
        Screen.Cursor := crDefault;
      except //N�o conseguiu verificar por problemas na conex�o
        on E: Exception do
        begin
          Resul    := 500;
          Mensagem := 'N�o foi poss�vel ativar o aplicativo tente a ativa��o manual! [1]';
          Mensagem := sLineBreak + E.Message;
          //
          Result := 2;
          //
          Screen.Cursor := crDefault;
        end;
      end;
      //Retorna:
      //0 => C�digo da mensagem
      //1 => Texto da mensagem
      //2 => Data que expira a licen�a
      //3 => Status do aplicativo
      //
      case Resul of
        200: //Sucesso
          Valida := 1;
        500: //Erro no servidor
          Valida := 2;
        else
        begin
          if MostraMsg then
            Geral.MB_Aviso(Mensagem);
        end;
      end;
    except
      on E: Exception do
      begin
        if MostraMsg then
        begin
          Mensagem := 'N�o foi poss�vel ativar o aplicativo tente a ativa��o manual! [2]';
          Mensagem := sLineBreak + Mensagem;
          //
          Geral.MB_Aviso(Mensagem);
        end;
        //
        if Result = 2 then
          Valida := 2
        else
          Valida := 0;
      end;
    end;
  end;
  if Valida = 1 then
  begin
    DataVerifi   := Geral.FDT(Date, 1);
    SerialNum    := DmkPF.CalculaValSerialNum2(Vencimento, CNPJ, CodAplic,
                      Geral.IMV(Status), False);
    SerialKey    := DmkPF.CalculaValSerialKey2(DataVerifi, False);
    SerialKeyOld := Geral.ReadAppKeyCU(TMeuDB + '_SerialKey', Application.Title, ktString, '');
    //
    Geral.WriteAppKeyCU(TMeuDB + '_SerialNum', Application.Title, SerialNum, ktString);
    Geral.WriteAppKeyCU(TMeuDB + '_SerialKey', Application.Title, SerialKey, ktString);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
      'SELECT Terminal ',
      'FROM terminais ',
      'WHERE SerialNum = "' + SerialNum + '"',
      'AND SerialKey = "' + SerialKeyOld + '"',
      '']);
    //
    if Dmod.QrAux.RecordCount > 0 then
    begin
      SQLType := stUpd;
      Terminal := Dmod.QrAux.FieldByName('Terminal').Value;
    end else
    begin
      SQLType  := stIns;
      Terminal := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'terminais',
                  'Terminal', [], [], stIns, 0, siPositivo, nil);
    end;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'terminais', False,
      ['IP', 'SerialNum', 'SerialKey', 'Servidor', 'NomePC', 'DescriPC'],
      ['Terminal'],
      [IP, SerialNum, SerialKey, Servidor, NomePC, DescriPC],
      [Terminal], True) then
    begin
      Dmod.QrAux.Close;
      Dmod.QrAux.SQL.Clear;
      Dmod.QrAux.SQL.Add('SELECT Dono FROM controle');
      UnDmkDAC_PF.AbreQueryApenas(Dmod.QrAux);
      //
      Dmod.QrUpd.SQL.Clear;
      //
      if Dmod.QrAux.RecordCount > 0 then
        Dmod.QrUpd.SQL.Add('UPDATE controle SET ContraSenha="", Dono=:P0, CNPJ=:P1')
      else
        Dmod.QrUpd.SQL.Add('INSERT INTO controle SET Dono=:P0, CNPJ=:P1');
      Dmod.QrUpd.Params[0].AsInteger := -11;
      Dmod.QrUpd.Params[1].AsString  := CNPJ;
      Dmod.QrUpd.ExecSQL;
      //
      Dmod.QrAux.Close;
      Dmod.QrAux.SQL.Clear;
      Dmod.QrAux.SQL.Add('SELECT * FROM master');
      UnDmkDAC_PF.AbreQueryApenas(Dmod.QrAux);
      //
      Dmod.QrUpd.SQL.Clear;
      if Dmod.QrAux.RecordCount > 0 then
      begin
        Dmod.QrUpd.SQL.Add('UPDATE master SET ');
      end else
      begin
        Dmod.QrUpd.SQL.Add('INSERT INTO master SET ');
      end;
      Dmod.QrUpd.SQL.Add('DataF="' + Vencimento + '", ');
      Dmod.QrUpd.SQL.Add('Monitorar="' + Status + '", ');
      Dmod.QrUpd.SQL.Add('QtdCliInt="' + Geral.FF0(CliInt) + '", ');
      Dmod.QrUpd.SQL.Add('Em="' + RazaoSocial + '", ');
      Dmod.QrUpd.SQL.Add('CNPJ="' + CNPJ + '" ');
      Dmod.QrUpd.ExecSQL;
      //
      Dmod.QrAux.Close;
      Dmod.QrAux.SQL.Clear;
      Dmod.QrAux.SQL.Add('SELECT Tipo FROM entidades WHERE Codigo in (-1,-11)');
      UnDmkDAC_PF.AbreQueryApenas(Dmod.QrAux);
      //
      Dmod.QrUpd.SQL.Clear;
      //
      if Dmod.QrAux.RecordCount > 0 then
      begin
        if Length(CNPJ) = 11 then
          Tipo := 1
        else
          Tipo := 0;
        //
        Dmod.QrUpd.SQL.Add('UPDATE entidades SET AlterWeb=1, Tipo=' + Geral.FF0(Tipo) + ', ');
      end else
      begin
        if Length(CNPJ) = 11 then
        begin
          Dmod.QrUpd.SQL.Add('INSERT INTO entidades SET Tipo=1, ');
          //
          Tipo := 1;
        end else
        begin
          Dmod.QrUpd.SQL.Add('INSERT INTO entidades SET Tipo=0, ');
          //
          Tipo := 2;
        end;
      end;
      //
      if Tipo = 1 then
      begin
        Enti := 'Nome';
        Doc  := 'CPF';
      end else
      begin
        Enti := 'RazaoSocial';
        Doc  := 'CNPJ';
      end;
      Dmod.QrUpd.SQL.Add(Enti + '=:P0, ' + Doc + '=:P1');
      if Dmod.QrAux.RecordCount > 0 then
        Dmod.QrUpd.SQL.Add('WHERE Codigo in (-1,-11)')
      else
        Dmod.QrUpd.SQL.Add(', Filial=1, CliInt=1, Codigo=-11');
      Dmod.QrUpd.Params[0].AsString := RazaoSocial;
      Dmod.QrUpd.Params[1].AsString := CNPJ;
      Dmod.QrUpd.ExecSQL;
      //
      DBCheck.VerificaEntiCliInt;
      //
      Result := 1;
    end;
  end;
end;
{$EndIf}
{$ENDIF}

procedure TFmUnLock_Dmk.MeLinkClick(Sender: TObject);
begin
  MeLink.SelectAll;
end;

procedure TFmUnLock_Dmk.MostraEdicao(Manual: Boolean);
begin
  if not Manual then
  begin
    PnAtiva.Visible       := True;
    PnAtivaManual.Visible := False;
    GBRodaPe.Visible      := True;
    //
    if FTipLibera = Libera4 then
    begin
      QrAux.Database := DModG.DBDmk;
      QrUpd.Database := DModG.DBDmk;
      QrTrm.Database := DModG.DBDmk;
      //
      Label5.Visible     := True;
      EdTelefone.Visible := True;
      Label7.Caption     := 'E-mail para contato:';
      CkAceito.Visible   := True;
      BtOK.Enabled       := False;
      BtAtiva.Visible    := False;    
      //
      FEhServ    := DModG.EhOServidor();
    end else
    begin
      Label5.Visible     := False;
      EdTelefone.Visible := False;
      Label7.Caption     := 'Descri��o para o computador (Campo n�o obrigat�rio):';
      CkAceito.Visible   := False;
      BtOK.Enabled       := True;
      BtAtiva.Visible    := True;
    end;
    PnAtiva.SetFocus;
  end else
  begin
    PnAtiva.Visible              := False;
    PnAtivaManual.Visible        := True;
    GBRodaPe.Visible             := False;
    PageControl1.ActivePageIndex := 0;
    MeLink.Text                  := '';
    MeAtiva.Text                 := '';
    //
    PnAtivaManual.SetFocus;
  end;
  ConfiguraAvisoAbas(PageControl1.ActivePageIndex);
end;

procedure TFmUnLock_Dmk.PageControl1Change(Sender: TObject);
begin
  ConfiguraAvisoAbas(PageControl1.ActivePageIndex);
end;

procedure TFmUnLock_Dmk.RemoveTerminaisInativos;
var
  Data: TDate;
  Data_TXT: String;
begin
  //Remove terminais inativos
  Data := DModG.ObtemAgora;
  Data := Data - 90;
  Data_TXT := Geral.FDT(Data, 1);
  //
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    DELETE_FROM + ' terminais ',
    'WHERE ((DataAlt < "1900-01-01" OR DataAlt IS NULL) AND (DataCad < "' + Data_TXT + '")) ',
    'OR ((DataAlt > "1900-01-01") AND (DataAlt < "' + Data_TXT + '")) ',
    '']);
end;

end.
