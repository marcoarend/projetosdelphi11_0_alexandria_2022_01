program UnLock;

uses
  Forms,
  UnLock_MLA in 'UnLock_MLA.pas' {FmUnLock_MLA},
  UnDmkEnums in '..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnDmkEnums.pas',
  UnInternalConsts in '..\..\..\..\UTL\_UNT\v01_01\UnInternalConsts.pas',
  UnMyLinguas in '..\..\..\..\..\MultiOS\AllOS\Listas\UnMyLinguas.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.Title := 'Destravador DERMATEK';
  Application.CreateForm(TFmUnLock_MLA, FmUnLock_MLA);
  Application.Run;
end.
