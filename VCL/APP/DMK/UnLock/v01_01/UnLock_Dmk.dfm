object FmUnLock_Dmk: TFmUnLock_Dmk
  Left = 339
  Top = 185
  Caption = 'XXX-XXXXX-999 :: Destravador Dermatek - WEB'
  ClientHeight = 404
  ClientWidth = 576
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 576
    Height = 47
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 529
      Top = 0
      Width = 47
      Height = 47
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 31
        Height = 31
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 47
      Height = 47
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 47
      Top = 0
      Width = 482
      Height = 47
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 343
        Height = 31
        Caption = 'Destravador Dermatek - WEB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 343
        Height = 31
        Caption = 'Destravador Dermatek - WEB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 343
        Height = 31
        Caption = 'Destravador Dermatek - WEB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 106
    Width = 576
    Height = 228
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 576
      Height = 228
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 576
        Height = 228
        Align = alClient
        TabOrder = 0
        object PnAtivaManual: TPanel
          Left = 2
          Top = 15
          Width = 572
          Height = 211
          Align = alClient
          TabOrder = 1
          ExplicitTop = 14
          ExplicitHeight = 212
          object PageControl1: TPageControl
            Left = 1
            Top = 1
            Width = 570
            Height = 209
            ActivePage = TabSheet2
            Align = alClient
            MultiLine = True
            TabOrder = 0
            OnChange = PageControl1Change
            ExplicitHeight = 206
            object TabSheet1: TTabSheet
              Caption = 'Etapa 1'
              object Panel5: TPanel
                Left = 0
                Top = 133
                Width = 562
                Height = 48
                Align = alBottom
                ParentBackground = False
                TabOrder = 0
                ExplicitTop = 138
                ExplicitWidth = 565
                object BitBtn2: TBitBtn
                  Tag = 3
                  Left = 437
                  Top = 3
                  Width = 118
                  Height = 39
                  Caption = '&Pr'#243'xima'
                  NumGlyphs = 2
                  TabOrder = 0
                  OnClick = BitBtn2Click
                end
              end
              object MeLink: TMemo
                Left = 0
                Top = 0
                Width = 562
                Height = 133
                Align = alClient
                Lines.Strings = (
                  'MeLink')
                ReadOnly = True
                TabOrder = 1
                OnClick = MeLinkClick
                ExplicitWidth = 565
                ExplicitHeight = 138
              end
            end
            object TabSheet2: TTabSheet
              Caption = 'Etapa 2'
              ImageIndex = 1
              object MeAtiva: TMemo
                Left = 0
                Top = 0
                Width = 562
                Height = 133
                Align = alClient
                Lines.Strings = (
                  'MeLink')
                TabOrder = 0
                ExplicitWidth = 565
                ExplicitHeight = 138
              end
              object Panel6: TPanel
                Left = 0
                Top = 133
                Width = 562
                Height = 48
                Align = alBottom
                ParentBackground = False
                TabOrder = 1
                ExplicitTop = 138
                ExplicitWidth = 565
                object BitBtn3: TBitBtn
                  Tag = 2
                  Left = 7
                  Top = 3
                  Width = 118
                  Height = 39
                  Caption = '&Anterior'
                  NumGlyphs = 2
                  TabOrder = 0
                  OnClick = BitBtn3Click
                end
                object BitBtn4: TBitBtn
                  Tag = 14
                  Left = 437
                  Top = 3
                  Width = 118
                  Height = 39
                  Caption = '&Confirma'
                  NumGlyphs = 2
                  TabOrder = 1
                  OnClick = BitBtn4Click
                end
              end
            end
          end
        end
        object PnAtiva: TPanel
          Left = 2
          Top = 15
          Width = 572
          Height = 211
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          ExplicitTop = 14
          ExplicitHeight = 212
          object GroupBox2: TGroupBox
            Left = 16
            Top = 8
            Width = 544
            Height = 64
            Caption = ' Libera'#231#227'o / renova'#231#227'o da licen'#231'a: '
            Enabled = False
            TabOrder = 0
            object Label1: TLabel
              Left = 12
              Top = 20
              Width = 124
              Height = 13
              Caption = 'Data que expira a licen'#231'a:'
            end
            object Label4: TLabel
              Left = 138
              Top = 20
              Width = 132
              Height = 13
              Caption = 'Nome do meu Computador: '
              FocusControl = EdNomePC
            end
            object EdFimData: TdmkEdit
              Left = 12
              Top = 35
              Width = 123
              Height = 21
              ReadOnly = True
              TabOrder = 0
              FormatType = dmktfDate
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0d
              ValWarn = False
            end
            object EdNomePC: TdmkEdit
              Left = 138
              Top = 35
              Width = 399
              Height = 21
              TabOrder = 1
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
          end
          object CkAceito: TCheckBox
            Left = 63
            Top = 185
            Width = 450
            Height = 17
            Caption = 
              'Aceito me cadastrar no site da dermatek. (Nenhuma informa'#231#227'o ser' +
              #225' divulgada a terceiros).'
            TabOrder = 2
            OnClick = CkAceitoClick
          end
          object GroupBox3: TGroupBox
            Left = 16
            Top = 75
            Width = 544
            Height = 103
            Caption = ' Dados da empresa locat'#225'ria do sistema: '
            TabOrder = 1
            object Label6: TLabel
              Left = 12
              Top = 20
              Width = 30
              Height = 13
              Caption = 'CNPJ:'
              FocusControl = EdCNPJ
            end
            object Label2: TLabel
              Left = 126
              Top = 20
              Width = 64
              Height = 13
              Caption = 'Raz'#227'o social:'
              FocusControl = EdRazao
            end
            object Label5: TLabel
              Left = 12
              Top = 59
              Width = 108
              Height = 13
              Caption = 'Telefone para contato:'
              FocusControl = EdTelefone
            end
            object Label7: TLabel
              Left = 126
              Top = 59
              Width = 94
              Height = 13
              Caption = 'E-mail para contato:'
              FocusControl = EdEMail
            end
            object EdRazao: TdmkEdit
              Left = 126
              Top = 35
              Width = 411
              Height = 21
              TabOrder = 1
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdCNPJ: TdmkEdit
              Left = 12
              Top = 35
              Width = 110
              Height = 21
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtCPFJ
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdTelefone: TdmkEdit
              Left = 12
              Top = 75
              Width = 110
              Height = 20
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtTelCurto
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdEMail: TdmkEdit
              Left = 126
              Top = 75
              Width = 411
              Height = 20
              TabOrder = 3
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 47
    Width = 576
    Height = 59
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 14
      Width = 572
      Height = 44
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1a: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2a: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso1b: TLabel
        Left = 13
        Top = 21
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2b: TLabel
        Left = 12
        Top = 20
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 334
    Width = 576
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 432
      Top = 14
      Width = 142
      Height = 54
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 118
        Height = 39
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 14
      Width = 430
      Height = 54
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 118
        Height = 39
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtAtiva: TBitBtn
        Tag = 14
        Left = 136
        Top = 3
        Width = 118
        Height = 39
        Caption = '&Ativa manual'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtAtivaClick
      end
    end
  end
  object QrTrm: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Conta'
      'FROM cliaplicli'
      'WHERE Controle=:P0'
      'AND SerialKey=:P1'
      'AND SerialNum=:P2')
    Left = 480
    Top = 152
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrTrmConta: TAutoIncField
      FieldName = 'Conta'
    end
  end
  object QrUpd: TMySQLQuery
    Database = Dmod.MyDB
    Left = 468
    Top = 72
  end
  object QrAux: TMySQLQuery
    Database = Dmod.MyDB
    Left = 440
    Top = 72
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 100
    OnTimer = Timer1Timer
    Left = 384
    Top = 72
  end
end
