object FmUnLock_MLA: TFmUnLock_MLA
  Left = 359
  Top = 169
  Caption = 'Aviso'
  ClientHeight = 571
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 523
    Width = 1008
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Hint = 'Confere senha'
      Caption = '&OK'
      Default = True
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtOKClick
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 41
    Align = alTop
    Caption = '1.0.6.20'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 9202959
    Font.Height = -23
    Font.Name = 'Times New Roman'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
  end
  object Panel2: TPanel
    Left = 0
    Top = 41
    Width = 568
    Height = 482
    Align = alLeft
    TabOrder = 3
    object LaDemo: TLabel
      Left = 8
      Top = 8
      Width = 395
      Height = 16
      Caption = 'Prazo de demostra'#231#227'o expirado, ou renova'#231#227'o expirada.'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMenuText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object Label4: TLabel
      Left = 8
      Top = 52
      Width = 238
      Height = 16
      Caption = 'Entre em contato com a Dermatek:'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMenuText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object Label5: TLabel
      Left = 8
      Top = 84
      Width = 279
      Height = 16
      Caption = 'dermatek@dermatek.com.br     0xx44 30315027'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMenuText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = False
      ParentFont = False
    end
    object Label11: TLabel
      Left = 8
      Top = 104
      Width = 66
      Height = 13
      Caption = 'Contra senha:'
    end
    object Label1: TLabel
      Left = 8
      Top = 148
      Width = 34
      Height = 13
      Caption = 'Senha:'
    end
    object EdSenha: TMemo
      Left = 8
      Top = 164
      Width = 302
      Height = 27
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Courier New'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      WordWrap = False
      OnChange = EdSenhaChange
    end
    object EdRandom: TMemo
      Left = 8
      Top = 120
      Width = 302
      Height = 27
      TabStop = False
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Courier New'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      WordWrap = False
      OnChange = EdRandomChange
    end
    object Memo1: TMemo
      Left = 8
      Top = 208
      Width = 549
      Height = 53
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      Lines.Strings = (
        'OBS: Para que senha informada libere o uso do aplicativo '#233' '
        'necess'#225'rio selecionar a data corretamente!')
      ParentFont = False
      ReadOnly = True
      TabOrder = 2
    end
    object MCNova: TMonthCalendar
      Left = 332
      Top = 30
      Width = 225
      Height = 160
      Date = 39796.000000000000000000
      TabOrder = 3
      OnClick = MCNovaClick
    end
    object GroupBox2: TGroupBox
      Left = 7
      Top = 268
      Width = 553
      Height = 65
      Caption = ' Libera'#231#227'o / renova'#231#227'o da licen'#231'a: '
      Enabled = False
      TabOrder = 4
      object Label8: TLabel
        Left = 12
        Top = 20
        Width = 124
        Height = 13
        Caption = 'Data que expira a licen'#231'a:'
      end
      object Label9: TLabel
        Left = 140
        Top = 20
        Width = 132
        Height = 13
        Caption = 'Nome do meu Computador: '
        FocusControl = EdNomePC
      end
      object EdFimData: TdmkEdit
        Left = 12
        Top = 36
        Width = 125
        Height = 21
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfDateTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '30/12/1899 00:00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdNomePC: TdmkEdit
        Left = 140
        Top = 36
        Width = 405
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GroupBox1: TGroupBox
      Left = 7
      Top = 336
      Width = 553
      Height = 105
      Caption = ' Dados da empresa locat'#225'ria do sistema: '
      TabOrder = 5
      object Label10: TLabel
        Left = 12
        Top = 20
        Width = 30
        Height = 13
        Caption = 'CNPJ:'
        FocusControl = EdCNPJ
      end
      object Label12: TLabel
        Left = 128
        Top = 20
        Width = 64
        Height = 13
        Caption = 'Raz'#227'o social:'
        FocusControl = EdRazao
      end
      object Label13: TLabel
        Left = 12
        Top = 60
        Width = 108
        Height = 13
        Caption = 'Telefone para contato:'
        FocusControl = EdTelefone
      end
      object Label14: TLabel
        Left = 128
        Top = 60
        Width = 94
        Height = 13
        Caption = 'E-mail para contato:'
        FocusControl = EdEMail
      end
      object EdRazao: TdmkEdit
        Left = 128
        Top = 36
        Width = 417
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCNPJ: TdmkEdit
        Left = 12
        Top = 36
        Width = 112
        Height = 21
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtCPFJ
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdTelefone: TdmkEdit
        Left = 12
        Top = 76
        Width = 112
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtTelCurto
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdEMail: TdmkEdit
        Left = 128
        Top = 76
        Width = 417
        Height = 21
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object CkAceito: TCheckBox
      Left = 12
      Top = 448
      Width = 557
      Height = 17
      Caption = 
        'Aceito me cadastrar no site da dermatek quando estiver conectado' +
        '. (Nenhuma informa'#231#227'o ser'#225' divulgada).'
      TabOrder = 6
    end
  end
  object Panel4: TPanel
    Left = 568
    Top = 41
    Width = 220
    Height = 482
    Align = alLeft
    Caption = 'Panel4'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object Label3: TLabel
      Left = 1
      Top = 1
      Width = 218
      Height = 15
      Align = alTop
      Alignment = taCenter
      AutoSize = False
      Caption = 'Contra Senha Fon'#233'tica'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object ListBox1: TListBox
      Left = 1
      Top = 16
      Width = 220
      Height = 465
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ItemHeight = 13
      ParentFont = False
      TabOrder = 0
      OnKeyDown = ListBox1KeyDown
    end
  end
  object Panel5: TPanel
    Left = 788
    Top = 41
    Width = 220
    Height = 482
    Align = alLeft
    Caption = 'Panel4'
    TabOrder = 4
    object Label7: TLabel
      Left = 1
      Top = 1
      Width = 218
      Height = 15
      Align = alTop
      Alignment = taCenter
      AutoSize = False
      Caption = 'Senha Fon'#233'tica'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object ListBox2: TListBox
      Left = 1
      Top = 16
      Width = 220
      Height = 465
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ItemHeight = 13
      ParentFont = False
      TabOrder = 0
    end
  end
end
