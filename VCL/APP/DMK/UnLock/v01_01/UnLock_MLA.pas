// ini Delphi 28 Alexandria
//{$I dmk.inc}
// ini Delphi 28 Alexandria
unit UnLock_MLA;

interface

uses
  Windows, Forms, ComCtrls, StdCtrls, Controls, Buttons, Classes, ExtCtrls,
  SysUtils, IniFiles, dmkEdit, Dialogs, UnInternalConsts;

type
  TFmUnLock_MLA = class(TForm)
    Panel1: TPanel;
    BtOK: TBitBtn;
    Panel3: TPanel;
    Panel2: TPanel;
    LaDemo: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label11: TLabel;
    Label1: TLabel;
    EdSenha: TMemo;
    EdRandom: TMemo;
    Memo1: TMemo;
    MCNova: TMonthCalendar;
    Panel4: TPanel;
    ListBox1: TListBox;
    Panel5: TPanel;
    ListBox2: TListBox;
    Label3: TLabel;
    Label7: TLabel;
    GroupBox2: TGroupBox;
    Label8: TLabel;
    Label9: TLabel;
    EdFimData: TdmkEdit;
    EdNomePC: TdmkEdit;
    GroupBox1: TGroupBox;
    Label10: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    EdRazao: TdmkEdit;
    EdCNPJ: TdmkEdit;
    EdTelefone: TdmkEdit;
    EdEMail: TdmkEdit;
    CkAceito: TCheckBox;
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure EdSenhaChange(Sender: TObject);
    procedure EdCNPJExit(Sender: TObject);
    procedure MCNovaClick(Sender: TObject);
    procedure EdRandomChange(Sender: TObject);
    procedure ListBox1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    FSetTerminate: Boolean;
    FDataFinal: String;
    //
    //function Descripta(ContraSenha: String): String;
    function DeCripta(S: String; ContaStr: Integer): String;
    procedure CalculaSenha;
    procedure VerificaSenha;
    procedure CalculaDataFinal;
    function ParticionaTexto(Texto: String): String;
    function SoNumero_TT(Texto : String) : String;
    function CalculaCNPJCPF(Nume: String) : String;
    function FormataCNPJ_TFT(Texto: String): String;
    function FormataCNPJ_TT(Texto: String): String;
    procedure ListaFonetico(Edit: TMemo; ListBox: TListBox);
  public
    { Public declarations }
    FContraSenha: String;
    FCrypt: Boolean;
    FFatorCrypt, FCrypto1, FCrypto2, FLength: Integer;
    FRazaoSocial, FCNPJ, FNomePC, FTelefone, FEMail: String;
    //
    function  FIC(FaltaInfoCompo: Boolean; Compo: TComponent;
              Mensagem: String; ExibeMsg: Boolean = True): Boolean;
  end;

var
  FmUnLock_MLA: TFmUnLock_MLA;

implementation

uses dmkGeral, UnMyLinguas;

{$R *.DFM}

const
  Soletrados: Array[0..36] of String = ('0 > (zero)','1 > (uno)', '2 >(dois)',
  '3 > (tr�s)','4 > (quatro)','5 > (cinco)','6 > (seis)','7 > (sete)',
  '8 > (oito)','9 (nove)','alpha','bravo','charlie','delta','echo',
  'foxtrot','golf','hotel','india','juliet','kilo','lima','mike','november',
  'oscar','papa','quebec','romeo','sierra','tango','uniform','victor','whiskey',
  'x-ray','yankee','zulu','- (sinal de menos)');

procedure TFmUnLock_MLA.BtOKClick(Sender: TObject);
var
  InfoN, InfoC, InfoT, InfoE, NomePC: String;
begin
  if FIC(CkAceito.Checked = False, CkAceito,
  'Para confirmar � necess�rio aceitar o cadastro no site de dermatek!') then Exit;
  //
  if Uppercase(Application.MainForm.Name) <> Uppercase('FmUnLock_MLA') then
  begin
    InfoN := EdRazao.Text;
    if FIC(Trim(InfoN) = '', EdRazao, 'Informe o nome da empresa!') then Exit;
    //
    InfoC := Geral.SoNumero_TT(EdCNPJ.Text);
    if FIC(Trim(InfoC) = '', EdCNPJ, 'Informe o CNPJ!') then Exit;
    //
    InfoT := Geral.SoNumero1a9_TT(EdTelefone.Text);
    if FIC(Trim(InfoT) = '', EdTelefone, 'Informe o n�mero do telefone para contato!') then Exit;
    InfoT := Geral.SoNumero_TT(EdTelefone.Text);
    //
    InfoE := EdEmail.Text;
    if FIC(Trim(InfoE) = '', EdEmail, 'Informe o e-mail para contato!') then Exit;
    if FIC(Geral.ValidaEMail(InfoE) = False, EdEmail, 'E-mail inv�lido!') then Exit;
    //
    NomePC    := EdNomePC.Text;
  end;
  //
  FCNPJ        := FormataCNPJ_TFT(EdCNPJ.Text);
  FRazaoSocial := EdRazao.Text;
  FNomePC      := EdNomePC.Text;
  FTelefone    := Geral.SoNumero_TT(EdTelefone.Text);
  FEMail       := EdEMail.Text;
  //
  VAR_TERMINATE := True;
  FSetTerminate := True;
  //
  if Uppercase(Application.MainForm.Name) = Uppercase('FmUnLock_MLA') then
    CalculaSenha else Close;
end;

procedure TFmUnLock_MLA.VerificaSenha;
var
  Senha: String;
begin
  CalculaDataFinal;
  Senha := DeCripta(EdRandom.Text+FDataFinal, Length(EdRandom.Text+FDataFinal));
  if EdSenha.Text = Senha then
  begin
    FCrypt := True;
    BtOK.Enabled := True;
  end else
    FCrypt := False;
end;

procedure TFmUnLock_MLA.CalculaSenha;
var
 Texto: String;
 i: Integer;
begin
  CalculaDataFinal;
  if Length(EdRandom.Text) > 0 then
  begin
    Texto := '';
    for i := 1 to Length(EdRandom.Text) do
    begin
      if EdRandom.Text[i] <> '-' then
      Texto := Texto + EdRandom.Text[i];
    end;
    EdSenha.Text := DeCripta(EdRandom.Text+FDataFinal, Length(EdRandom.Text+FDataFinal));
  end;
end;

procedure TFmUnLock_MLA.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if FSetTerminate = False then
    VAR_TERMINATE := True;
end;

procedure TFmUnLock_MLA.FormCreate(Sender: TObject);
var
  Ordinal, i: Integer;
  Texto: String;
begin
  FSetTerminate := False;
  //
  FLength  := 12;//16;
  FCrypto1 := 17342;//70563;
  FCrypto2 := 75840;//87389;
  MCNova.Date := Int(Date) + 123;
  EdFimData.ValueVariant := MCNova.Date;
  EdNomePC.Text := Geral.IndyComputerName();
  if Uppercase(Application.Title) = Uppercase('Destravador DERMATEK') then
  begin
    EdRandom.ReadOnly := False;
  end else begin
    BtOk.Enabled := False;
    FCrypt := False;
    if FContraSenha = '' then
    begin
      Randomize;
      for i := 1 to FLength do
      begin
        Ordinal := 0;
        while ((Ordinal < 48) or (Ordinal > 122)) do Ordinal := Random(123);
        if (Ordinal > 047) and (Ordinal < 058) then Texto := Texto + char(Ordinal);
        if (Ordinal > 064) and (Ordinal < 091) then Texto := Texto + char(Ordinal);
        if (Ordinal > 096) and (Ordinal < 123) then Texto := Texto + char(Ordinal);
      end;
      EdRandom.Text := ParticionaTexto(Texto);
      FContraSenha  := Texto;
    end else EdRandom.Text := FContraSenha;
  end;
end;

procedure TFmUnLock_MLA.MCNovaClick(Sender: TObject);
begin
  EdFimData.ValueVariant := MCNova.Date;
end;

(*function TFmTravado.Descripta(ContraSenha: String): String;
var
  Ordinal, j, i: Integer;
  C3, b: byte;
  Letra: Char;
begin
  C3 := 227;
  b := 0;
  for j := 1 to Length(ContraSenha) do
  begin
    Letra := ContraSenha[j];
    for i := 1 to Length(ContraSenha) do
    begin
      if b = 255 then
      begin
        b := 0;
        C3 := 227;
      end else b := b + 1;
      Letra := Char(Byte(ContraSenha[j]) xor (C3 shr 8));
      C3 := (b + C3) * CO_CRYPTO1 + CO_CRYPTO2;
    end;
    Ordinal := Ord(Letra);
    if (Ordinal > 047) and (Ordinal < 058) then Result := Result + Letra;
    if (Ordinal > 064) and (Ordinal < 091) then Result := Result + Letra;
    if (Ordinal > 096) and (Ordinal < 123) then Result := Result + Letra;
  end;
end;*)

{$IfDef DELPHI12_UP}
function TFmUnLock_MLA.DeCripta(S: String; ContaStr: Integer): String;
var
  C3, i: Integer;
  Key: AnsiChar;
  b: Byte;
  //
  Fator, SubRes, Resultado: String;
  //
  Res: AnsiString;
begin
  FFatorCrypt := 227;
  C3 := FFatorCrypt;
  Resultado := '';
  ///////////////////////////////////////////////////////////////////
  while Length(Resultado) < FLength do
  begin
    Res := S;
    i := 1;
    b := 0;
    while i <= (ContaStr) do
    begin
      if b = 255 then
      begin
        b := 0;
        C3 := FFatorCrypt;
      end else b := b + 1;
      Key := AnsiChar(Byte(S[i]) xor (C3 shr 8));
      Res[i] := Key;
      //
      C3 := (b + C3) * FCrypto1 + FCrypto2;
      inc(i);
    end;
    Res := Fator + Res;
    SubRes := '';
    for i := 1 to Length(Res) do
    begin
      {
      if Res[i] in ['0'..'9'] then SubRes := SubRes + Res[i];
      if Res[i] in ['A'..'Z'] then SubRes := SubRes + Res[i];
      if Res[i] in ['a'..'z'] then SubRes := SubRes + Res[i];
      }
      if CharInSet(Res[i], ['0'..'9']) then SubRes := SubRes + Res[i];
      if CharInSet(Res[i], ['A'..'Z']) then SubRes := SubRes + Res[i];
      if CharInSet(Res[i], ['a'..'z']) then SubRes := SubRes + Res[i];
    end;
    Resultado := Resultado + SubRes;
  end;
  if Length(Res) > FLength then Res := Copy(Res, 1, FLength);
  Res := ParticionaTexto(Copy(Resultado, 2, Length(Resultado)-2));
  //
  Result := Res;
end;
{$Else}
function TFmUnLock_MLA.DeCripta(S: String; ContaStr: Integer): String;
var
  C3, i: Integer;
  Key: Char;
  b: Byte;
  //
  Fator, SubRes, Resultado: String;
  //
  Res: AnsiString;
begin
  FFatorCrypt := 227;
  C3 := FFatorCrypt;
  Resultado := '';
  ///////////////////////////////////////////////////////////////////
  while Length(Resultado) < FLength do
  begin
    Result := S;
    i := 1;
    b := 0;
    while i <= (ContaStr) do
    begin
      if b = 255 then
      begin
        b := 0;
        C3 := FFatorCrypt;
      end else b := b + 1;
      Key := Char(Byte(S[i]) xor (C3 shr 8));
      Result[i] := Key;
      //
      C3 := (b + C3) * FCrypto1 + FCrypto2;
      inc(i);
    end;
    Result := Fator + Result;
    SubRes := '';
    for i := 1 to Length(Result) do
    begin
      {
      if Result[i] in ['0'..'9'] then SubRes := SubRes + Result[i];
      if Result[i] in ['A'..'Z'] then SubRes := SubRes + Result[i];
      if Result[i] in ['a'..'z'] then SubRes := SubRes + Result[i];
      }
      if Result[i] in ['0'..'9'] then SubRes := SubRes + Result[i];
      if Result[i] in ['A'..'Z'] then SubRes := SubRes + Result[i];
      if Result[i] in ['a'..'z'] then SubRes := SubRes + Result[i];
    end;
    Resultado := Resultado + SubRes;
  end;
  if Length(Result) > FLength then Result := Copy(Result, 1, FLength);
  Result := ParticionaTexto(Copy(Resultado, 2, Length(Resultado)-2));
end;
{$EndIf}

function TFmUnLock_MLA.FIC(FaltaInfoCompo: Boolean; Compo: TComponent;
  Mensagem: String; ExibeMsg: Boolean): Boolean;
var
  Comp: TWinControl;
begin
  if FaltaInfoCompo then
  begin
    Screen.Cursor := crDefault;
    Result := True;
    if (Mensagem <> '') and ExibeMsg then
      Geral.MB_Aviso(Mensagem);
    if Compo <> nil then
    begin
      Comp := TWinControl(Compo);
      //
      if (Comp.Visible) and (Comp.Enabled) and (Comp.CanFocus) then
      try
        Comp.SetFocus;
      except
        ;
      end;
    end;
  end else Result := False;
end;

procedure TFmUnLock_MLA.FormActivate(Sender: TObject);
begin
  //MyObjects.CorIniComponente();
  if Uppercase(Application.MainForm.Name) = Uppercase('FmUnLock_MLA') then
  EdRandom.SetFocus else ListBox1.SetFocus;
end;

function TFmUnLock_MLA.ParticionaTexto(Texto: String): String;
var
  i: Integer;
  s: String;
begin
  if Length(Texto) > 0 then
  begin
    for i := 1 to Length(Texto) do
    begin
      s := s + Texto[i];
      if i / 4 = int(i/4) then s := s + '-';
    end;
    Result := s;
  end else Result := Texto;
end;

procedure TFmUnLock_MLA.EdSenhaChange(Sender: TObject);
begin
  ListaFonetico(EdSenha, ListBox2);
  VerificaSenha;
end;

procedure TFmUnLock_MLA.CalculaDataFinal;
var
  i: Integer;
  s: String;
begin
  FDataFinal := FloatToStr(Int(MCNova.Date)/17);
  if Length(FDataFinal) > 15 then
  FDataFinal := Copy(FDataFinal, 1 , 15);
  for i := Length(FDataFinal) downto 1 do s := s + FDataFinal[i];
  FDataFinal := s;
  //
end;

procedure TFmUnLock_MLA.EdCNPJExit(Sender: TObject);
var
  Num : String;
  CNPJ : String;
begin
  CNPJ := SoNumero_TT(EdCNPJ.Text);
  if CNPJ <> '' then
    begin
    Num := CalculaCNPJCPF(CNPJ);
    if FormataCNPJ_TFT(CNPJ) <> Num then
    begin
      Geral.MensagemBox('N�mero de CNPJ/CPF inv�lido', 'Erro', MB_OK+MB_ICONERROR);
      EdCNPJ.SetFocus;
    end else EdCNPJ.Text := FormataCNPJ_TT(CNPJ);
  end else EdCNPJ.Text := '';
end;

procedure TFmUnLock_MLA.EdRandomChange(Sender: TObject);
begin
  ListaFonetico(EdRandom, ListBox1);
  if not EdRandom.Focused then
  begin
    ListBox1.ItemIndex := 0;
    // erro
    //ListBox1.SetFocus;
  end;
end;

procedure TFmUnLock_MLA.ListaFonetico(Edit: TMemo; ListBox: TListBox);
var
  I: Integer;
begin
  ListBox.Items.Clear;
  for I := 1 to Length(Edit.Text) do
  begin
    case Ord(Edit.Text[I]) of
      045: ListBox.Items.Add(Soletrados[36]); // - (sinal de menos)
      //
      048: ListBox.Items.Add(Soletrados[00]); // 0
      049: ListBox.Items.Add(Soletrados[01]); // 1
      050: ListBox.Items.Add(Soletrados[02]); // 2
      051: ListBox.Items.Add(Soletrados[03]); // 3
      052: ListBox.Items.Add(Soletrados[04]); // 4
      053: ListBox.Items.Add(Soletrados[05]); // 5
      054: ListBox.Items.Add(Soletrados[06]); // 6
      055: ListBox.Items.Add(Soletrados[07]); // 7
      056: ListBox.Items.Add(Soletrados[08]); // 8
      057: ListBox.Items.Add(Soletrados[09]); // 9
      //
      065: ListBox.Items.Add(Uppercase(Soletrados[10] + ' MAI�SCULO')); // A
      066: ListBox.Items.Add(Uppercase(Soletrados[11] + ' MAI�SCULO')); // B
      067: ListBox.Items.Add(Uppercase(Soletrados[12] + ' MAI�SCULO')); // C
      068: ListBox.Items.Add(Uppercase(Soletrados[13] + ' MAI�SCULO')); // D
      069: ListBox.Items.Add(Uppercase(Soletrados[14] + ' MAI�SCULO')); // E
      070: ListBox.Items.Add(Uppercase(Soletrados[15] + ' MAI�SCULO')); // F
      071: ListBox.Items.Add(Uppercase(Soletrados[16] + ' MAI�SCULO')); // G
      072: ListBox.Items.Add(Uppercase(Soletrados[17] + ' MAI�SCULO')); // H
      073: ListBox.Items.Add(Uppercase(Soletrados[18] + ' MAI�SCULO')); // I
      074: ListBox.Items.Add(Uppercase(Soletrados[19] + ' MAI�SCULO')); // J
      075: ListBox.Items.Add(Uppercase(Soletrados[20] + ' MAI�SCULO')); // K
      076: ListBox.Items.Add(Uppercase(Soletrados[21] + ' MAI�SCULO')); // L
      077: ListBox.Items.Add(Uppercase(Soletrados[22] + ' MAI�SCULO')); // M
      078: ListBox.Items.Add(Uppercase(Soletrados[23] + ' MAI�SCULO')); // N
      079: ListBox.Items.Add(Uppercase(Soletrados[24] + ' MAI�SCULO')); // O
      080: ListBox.Items.Add(Uppercase(Soletrados[25] + ' MAI�SCULO')); // P
      081: ListBox.Items.Add(Uppercase(Soletrados[26] + ' MAI�SCULO')); // Q
      082: ListBox.Items.Add(Uppercase(Soletrados[27] + ' MAI�SCULO')); // R
      083: ListBox.Items.Add(Uppercase(Soletrados[28] + ' MAI�SCULO')); // S
      084: ListBox.Items.Add(Uppercase(Soletrados[29] + ' MAI�SCULO')); // T
      085: ListBox.Items.Add(Uppercase(Soletrados[30] + ' MAI�SCULO')); // U
      086: ListBox.Items.Add(Uppercase(Soletrados[31] + ' MAI�SCULO')); // V
      087: ListBox.Items.Add(Uppercase(Soletrados[32] + ' MAI�SCULO')); // W
      088: ListBox.Items.Add(Uppercase(Soletrados[33] + ' MAI�SCULO')); // X
      089: ListBox.Items.Add(Uppercase(Soletrados[34] + ' MAI�SCULO')); // Y
      090: ListBox.Items.Add(Uppercase(Soletrados[35] + ' MAI�SCULO')); // Z
      //
      097: ListBox.Items.Add(Lowercase(Soletrados[10] + ' min�sculo')); // a
      098: ListBox.Items.Add(Lowercase(Soletrados[11] + ' min�sculo')); // b
      099: ListBox.Items.Add(Lowercase(Soletrados[12] + ' min�sculo')); // c
      100: ListBox.Items.Add(Lowercase(Soletrados[13] + ' min�sculo')); // d
      101: ListBox.Items.Add(Lowercase(Soletrados[14] + ' min�sculo')); // e
      102: ListBox.Items.Add(Lowercase(Soletrados[15] + ' min�sculo')); // f
      103: ListBox.Items.Add(Lowercase(Soletrados[16] + ' min�sculo')); // g
      104: ListBox.Items.Add(Lowercase(Soletrados[17] + ' min�sculo')); // h
      105: ListBox.Items.Add(Lowercase(Soletrados[18] + ' min�sculo')); // i
      106: ListBox.Items.Add(Lowercase(Soletrados[19] + ' min�sculo')); // j
      107: ListBox.Items.Add(Lowercase(Soletrados[20] + ' min�sculo')); // k
      108: ListBox.Items.Add(Lowercase(Soletrados[21] + ' min�sculo')); // l
      109: ListBox.Items.Add(Lowercase(Soletrados[22] + ' min�sculo')); // m
      110: ListBox.Items.Add(Lowercase(Soletrados[23] + ' min�sculo')); // n
      111: ListBox.Items.Add(Lowercase(Soletrados[24] + ' min�sculo')); // o
      112: ListBox.Items.Add(Lowercase(Soletrados[25] + ' min�sculo')); // p
      113: ListBox.Items.Add(Lowercase(Soletrados[26] + ' min�sculo')); // q
      114: ListBox.Items.Add(Lowercase(Soletrados[27] + ' min�sculo')); // r
      115: ListBox.Items.Add(Lowercase(Soletrados[28] + ' min�sculo')); // s
      116: ListBox.Items.Add(Lowercase(Soletrados[29] + ' min�sculo')); // t
      117: ListBox.Items.Add(Lowercase(Soletrados[30] + ' min�sculo')); // u
      118: ListBox.Items.Add(Lowercase(Soletrados[31] + ' min�sculo')); // v
      119: ListBox.Items.Add(Lowercase(Soletrados[32] + ' min�sculo')); // w
      120: ListBox.Items.Add(Lowercase(Soletrados[33] + ' min�sculo')); // x
      121: ListBox.Items.Add(Lowercase(Soletrados[34] + ' min�sculo')); // y
      122: ListBox.Items.Add(Lowercase(Soletrados[35] + ' min�sculo')); // z
      //
      else ShowMessage(IntToStr(Ord(Edit.Text[I])));
    end;
  end;
end;

procedure TFmUnLock_MLA.ListBox1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  EdRandom.SelStart  := ListBox1.ItemIndex;
  EdRandom.SelLength := 1;
end;

function TFmUnLock_MLA.SoNumero_TT(Texto : String) : String;
var
  i:Integer;
begin
  Result := '';
  for i := 1 to Length(Texto) do
    //if Texto[i] in (['0'..'9']) then Result := Result + Texto[i];
{$IFDEF DELPHI12_UP}
    if CharInSet(Texto[i], (['0'..'9'])) then Result := Result + Texto[i];
{$ELSE}
    if Texto[i] in (['0'..'9']) then Result := Result + Texto[i];
{$ENDIF}
end;

function TFmUnLock_MLA.CalculaCNPJCPF(Nume : String) : String;
var
  l : Integer;
  Numero : String;
  i,j,k : Integer;
  Soma : Integer;
  Digito : Integer;
  CNPJ : Boolean;
begin
  if Nume <> '' then
    begin
    Numero := '';
    for l := 1 to (Length(Nume)-2) do
      //if Nume[l] in ['0'..'9'] then
{$IFDEF DELPHI12_UP}
      if CharInSet(Nume[l], ['0'..'9']) then
{$ELSE}
      if Nume[l] in ['0'..'9'] then
{$ENDIF}
        Numero := Numero + Chr(Ord(Nume[l]));
    case length(Numero) of
      9 : CNPJ := False;
      12 : CNPJ := True;
      else Exit;
    end;
    Result := Numero;
    //Calcula 2 vezes
    for j := 1 to 2 do begin
      k := 2;
      Soma := 0;
      for i := Length(Result) downto 1 do begin
        //Converte o d�gito para num�rico, multiplica e soma
        Soma := Soma + (Ord(Result[i]) - Ord('0')) * k;
        Inc(k);
        if (k > 9) and CNPJ then
          k := 2;
      end;
      Digito := 11 - Soma mod 11;
      if Digito >= 10 then
        Digito := 0;
      //adiciona o d�gito ao resultado
        Result := Result + Chr(Digito + Ord('0'));
      end;
    end;
end;

function TFmUnLock_MLA.FormataCNPJ_TFT(Texto: String): String;
var
  i: Integer;
begin
  Result := '';
  if Texto = '' then Exit;
  try
    for i := 1 to length(Texto) do
      //if Texto[i] in ['0'..'9'] then Result := Result + Texto[i];
{$IFDEF DELPHI12_UP}
      if CharInSet(Texto[i], ['0'..'9']) then Result := Result + Texto[i];
{$ELSE}
      if Texto[i] in ['0'..'9'] then Result := Result + Texto[i];
{$ENDIF}
  except
    Geral.MensagemBox('CNPJ inv�lido.', 'Erro', MB_OK+MB_ICONERROR);
  end;
end;

function TFmUnLock_MLA.FormataCNPJ_TT(Texto: String): String;
begin
  Texto := FormataCNPJ_TFT(Texto);
  Result := Texto;
  if Texto = '' then Exit;
  try
    if Length(Texto) = 14 then
    Result := Texto[1]+Texto[2]+'.'+
              Texto[3]+Texto[4]+Texto[5]+'.'+
              Texto[6]+Texto[7]+Texto[8]+'/'+
              Texto[9]+Texto[10]+Texto[11]+Texto[12]+'-'+
              Texto[13]+Texto[14]
    else if Length(Texto) = 11 then
    Result := Texto[1]+Texto[2]+Texto[3]+'.'+
              Texto[4]+Texto[5]+Texto[6]+'.'+
              Texto[7]+Texto[8]+Texto[9]+'-'+
              Texto[10]+Texto[11]
    else begin
      Geral.MensagemBox('CNPJ/CPF inv�lido', 'Erro', MB_OK+MB_ICONERROR);
      Exit;
    end;
  except
    Geral.MensagemBox('CNPJ/CPF inv�lido.', 'Erro', MB_OK+MB_ICONERROR);
  end;
end;

end.
