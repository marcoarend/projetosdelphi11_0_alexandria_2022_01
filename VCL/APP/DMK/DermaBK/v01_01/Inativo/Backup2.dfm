object FmBackup2: TFmBackup2
  Left = 379
  Top = 170
  Caption = 'Backup 2 (Beta)'
  ClientHeight = 494
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 398
    Align = alClient
    TabOrder = 0
    object Label1: TLabel
      Left = 12
      Top = 4
      Width = 74
      Height = 13
      Caption = 'Base de dados:'
    end
    object Label2: TLabel
      Left = 12
      Top = 48
      Width = 48
      Height = 13
      Caption = 'Destino 1:'
    end
    object Label3: TLabel
      Left = 12
      Top = 92
      Width = 48
      Height = 13
      Caption = 'Destino 2:'
      Visible = False
    end
    object SpeedButton1: TSpeedButton
      Left = 436
      Top = 64
      Width = 21
      Height = 21
      OnClick = SpeedButton1Click
    end
    object SpeedButton2: TSpeedButton
      Left = 436
      Top = 108
      Width = 21
      Height = 21
      Visible = False
      OnClick = SpeedButton2Click
    end
    object Label4: TLabel
      Left = 176
      Top = 4
      Width = 135
      Height = 13
      Caption = 'Senha alternativa (opcional):'
    end
    object Label5: TLabel
      Left = 12
      Top = 136
      Width = 95
      Height = 13
      Caption = 'Diret'#243'rio do MySQL:'
    end
    object SpeedButton3: TSpeedButton
      Left = 436
      Top = 152
      Width = 21
      Height = 21
      OnClick = SpeedButton3Click
    end
    object LaAllow: TLabel
      Left = 12
      Top = 180
      Width = 112
      Height = 13
      Caption = '--max_allowed_packet='
    end
    object LaBuffer: TLabel
      Left = 140
      Top = 180
      Width = 95
      Height = 13
      Caption = '--net_buffer_length='
    end
    object Label6: TLabel
      Left = 248
      Top = 180
      Width = 84
      Height = 13
      Caption = 'Nome do arquivo:'
    end
    object CBDB: TDBLookupComboBox
      Left = 12
      Top = 20
      Width = 161
      Height = 21
      KeyField = 'Database'
      ListField = 'Database'
      ListSource = DsDatabases
      TabOrder = 0
      OnClick = CBDBClick
      OnExit = CBDBExit
    end
    object EdDestino1: TdmkEdit
      Left = 12
      Top = 64
      Width = 421
      Height = 21
      TabOrder = 2
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object EdDestino2: TdmkEdit
      Left = 12
      Top = 108
      Width = 421
      Height = 21
      TabOrder = 3
      Visible = False
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object EdSenha: TEdit
      Left = 176
      Top = 20
      Width = 257
      Height = 21
      PasswordChar = '*'
      TabOrder = 1
    end
    object EdPathMySQL: TdmkEdit
      Left = 12
      Top = 152
      Width = 421
      Height = 21
      TabOrder = 4
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = 'C:\Arquivos de programas\MySQL\MySQL Server 4.1\bin\'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 'C:\Arquivos de programas\MySQL\MySQL Server 4.1\bin\'
    end
    object EdAllow: TEdit
      Left = 12
      Top = 196
      Width = 121
      Height = 21
      TabOrder = 5
      Text = '2.097.152'
      OnExit = EdAllowExit
    end
    object EdBuffer: TEdit
      Left = 140
      Top = 196
      Width = 101
      Height = 21
      TabOrder = 6
      Text = '32.768'
      OnExit = EdBufferExit
    end
    object EdArquivo: TEdit
      Left = 248
      Top = 196
      Width = 185
      Height = 21
      TabOrder = 7
    end
    object MeResult: TMemo
      Left = 464
      Top = 1
      Width = 319
      Height = 290
      Align = alRight
      Color = clWindowFrame
      Font.Charset = ANSI_CHARSET
      Font.Color = clSilver
      Font.Height = -13
      Font.Name = 'Lucida Console'
      Font.Style = [fsBold]
      Lines.Strings = (
        'Teste 1'
        'C:\Meus Documentos')
      ParentFont = False
      TabOrder = 8
    end
    object CkCriaDB: TCheckBox
      Left = 12
      Top = 228
      Width = 97
      Height = 17
      Caption = 'Cria database.'
      TabOrder = 9
    end
    object MeBase: TMemo
      Left = 1
      Top = 352
      Width = 782
      Height = 45
      Align = alBottom
      Lines.Strings = (
        
          '//C:\ARQUIV~1\MySQL\MYSQLS~1.1\bin\mysqldump --max_allowed_packe' +
          't=2097152 '
        
          '--net_buffer_length=65536 -h localhost -u root -pwkljweryhvbirt ' +
          ' -B seven > '
        'C:\Seven0606121728.sql'
        '')
      TabOrder = 10
      Visible = False
    end
    object MeCmd: TMemo
      Left = 1
      Top = 291
      Width = 782
      Height = 61
      Align = alBottom
      TabOrder = 11
      Visible = False
    end
    object Panel1: TPanel
      Left = 280
      Top = 236
      Width = 185
      Height = 41
      Caption = 'Panel1'
      TabOrder = 12
    end
  end
  object PainelConfirma: TPanel
    Left = 0
    Top = 446
    Width = 784
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      TabOrder = 0
      OnClick = BtOKClick
      NumGlyphs = 2
    end
    object BtSaida: TBitBtn
      Tag = 13
      Left = 362
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Sai da janela atual'
      Caption = '&Sa'#237'da'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtSaidaClick
      NumGlyphs = 2
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    Caption = 'Backup 2 (Beta)'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 782
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object QrDatabases: TmySQLQuery
    Database = FmBackup3.DbAll
    SQL.Strings = (
      'SHOW DATABASES')
    Left = 160
    Top = 140
    object QrDatabasesDatabase: TWideStringField
      FieldName = 'Database'
      Required = True
      Size = 64
    end
  end
  object DsDatabases: TDataSource
    DataSet = QrDatabases
    Left = 188
    Top = 140
  end
end
