unit Module;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, mySQLDbTables, MySQLBatch, dmkGeral;

type
  TDMod = class(TDataModule)
    QrPerfis: TmySQLQuery;
    QrPerfisLibera: TSmallintField;
    QrPerfisJanela: TWideStringField;
    QrAux: TmySQLQuery;
    QrUpd: TmySQLQuery;
    MyDB: TmySQLDatabase;
    QrMaster: TmySQLQuery;
    QrMasterCNPJ_TXT: TWideStringField;
    QrMasterTE1_TXT: TWideStringField;
    QrMasterCEP_TXT: TWideStringField;
    QrMasterEm: TWideStringField;
    QrMasterTipo: TSmallintField;
    QrMasterLogo: TBlobField;
    QrMasterDono: TIntegerField;
    QrMasterVersao: TIntegerField;
    QrMasterCNPJ: TWideStringField;
    QrMasterIE: TWideStringField;
    QrMasterECidade: TWideStringField;
    QrMasterNOMEUF: TWideStringField;
    QrMasterEFax: TWideStringField;
    QrMasterERua: TWideStringField;
    QrMasterEBairro: TWideStringField;
    QrMasterECompl: TWideStringField;
    QrMasterEContato: TWideStringField;
    QrMasterECel: TWideStringField;
    QrMasterETe1: TWideStringField;
    QrMasterETe2: TWideStringField;
    QrMasterETe3: TWideStringField;
    QrMasterEPais: TWideStringField;
    QrMasterRespons1: TWideStringField;
    QrMasterRespons2: TWideStringField;
    QrMasterECEP: TIntegerField;
    QrMasterLogo2: TBlobField;
    QrMasterLimite: TSmallintField;
    QrMasterSolicitaSenha: TIntegerField;
    QrMasterENumero: TFloatField;
    QrUpdZ: TmySQLQuery;
    QlLocal: TMySQLBatchExecute;
    MyLocDatabase: TmySQLDatabase;
    QrAuxL: TmySQLQuery;
    QrUpdL: TmySQLQuery;
    QrInsL: TmySQLQuery;
    DBAgendaAtu: TmySQLDatabase;
    procedure QrMasterCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure VerificaSenha(Tipo: Integer; Login, Senha: String);
    function Privilegios(Usuario : Integer) : Boolean;

  end;

var
  DMod: TDMod;

implementation

uses UnMLAGeral;

{$R *.DFM}

procedure TDMod.VerificaSenha(Tipo: Integer; Login, Senha: String);
begin
  // Compatibilidade
end;

function TDMod.Privilegios(Usuario : Integer) : Boolean;
begin
  Result := False;
  QrPerfis.Close;
  if Usuario > -1000 then
  begin
    QrPerfis.SQL.Clear;
    QrPerfis.SQL.Add('SELECT pip.Libera, pit.Janela');
    QrPerfis.SQL.Add('FROM perfisits pit');
    QrPerfis.SQL.Add('LEFT JOIN perfisitsperf pip ON pip.Janela=pit.Janela');
    QrPerfis.SQL.Add('AND pip.Codigo='+IntToStr(Usuario)); // Condi��o do LEFT JOIN
  end;
  QrPerfis.Open;
  if QrPerfis.RecordCount > 0 then Result := True;
end;

procedure TDMod.QrMasterCalcFields(DataSet: TDataSet);
begin
  QrMasterTE1_TXT.Value := Geral.FormataTelefone_TT(QrMasterETe1.Value);
  QrMasterCEP_TXT.Value := Geral.FormataCEP_NT(QrMasterECEP.Value);
  QrMasterCNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrMasterCNPJ.Value);
end;

end.

