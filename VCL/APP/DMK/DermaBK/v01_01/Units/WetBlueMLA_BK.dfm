object FmWetBlueMLA_BK: TFmWetBlueMLA_BK
  Left = 329
  Top = 175
  BorderIcons = []
  Caption = 'Backup de Databases Dermatek [MySQL]'
  ClientHeight = 575
  ClientWidth = 993
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poDesktopCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 993
    Height = 80
    Align = alTop
    TabOrder = 0
    object LaAviso: TLabel
      Left = 1
      Top = 40
      Width = 991
      Height = 19
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      Caption = '...'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clHotLight
      Font.Height = -17
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      ExplicitTop = 60
      ExplicitWidth = 15
    end
    object BtSaida: TBitBtn
      Tag = 13
      Left = 5
      Top = 5
      Width = 123
      Height = 49
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Ocultar'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtSaidaClick
    end
    object BtBackup: TBitBtn
      Tag = 59
      Left = 394
      Top = 5
      Width = 123
      Height = 49
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Backup'
      Enabled = False
      NumGlyphs = 2
      TabOrder = 1
      OnClick = BtBackupClick
    end
    object BtEstrutura: TBitBtn
      Tag = 35
      Left = 640
      Top = 5
      Width = 123
      Height = 49
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Estr&utura'
      Enabled = False
      NumGlyphs = 2
      TabOrder = 2
      OnClick = BtEstruturaClick
    end
    object BtRestore: TBitBtn
      Tag = 73
      Left = 517
      Top = 5
      Width = 123
      Height = 49
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Restaurar'
      Enabled = False
      NumGlyphs = 2
      TabOrder = 3
      OnClick = BtRestoreClick
    end
    object BtDesiste: TBitBtn
      Tag = 15
      Left = 251
      Top = 5
      Width = 143
      Height = 49
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Desconectar'
      Enabled = False
      NumGlyphs = 2
      TabOrder = 4
      OnClick = BtDesisteClick
    end
    object BtConfirma: TBitBtn
      Tag = 14
      Left = 128
      Top = 5
      Width = 123
      Height = 49
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Conectar'
      NumGlyphs = 2
      TabOrder = 5
      OnClick = BtConfirmaClick
    end
    object BitBtn1: TBitBtn
      Left = 1143
      Top = 5
      Width = 53
      Height = 31
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'BitBtn1'
      TabOrder = 6
      Visible = False
      OnClick = BitBtn1Click
    end
    object Progress1: TProgressBar
      Left = 1
      Top = 59
      Width = 991
      Height = 20
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      Step = 1
      TabOrder = 7
    end
    object BtAtualiza: TBitBtn
      Tag = 192
      Left = 763
      Top = 5
      Width = 123
      Height = 49
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Atualiza'
      NumGlyphs = 2
      TabOrder = 8
      OnClick = BtAtualizaClick
    end
    object BtImporta: TBitBtn
      Tag = 39
      Left = 886
      Top = 5
      Width = 123
      Height = 49
      Cursor = crHandPoint
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Importar'
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 9
      OnClick = BtImportaClick
    end
  end
  object PagCon: TPageControl
    Left = 0
    Top = 80
    Width = 993
    Height = 480
    ActivePage = TSLogin
    Align = alClient
    TabHeight = 20
    TabOrder = 1
    object TSLogin: TTabSheet
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Bases de dados / tabelas'
      object Splitter2: TSplitter
        Left = 0
        Top = 0
        Width = 4
        Height = 450
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        ExplicitHeight = 553
      end
      object Panel8: TPanel
        Left = 4
        Top = 0
        Width = 981
        Height = 450
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        ParentBackground = False
        TabOrder = 0
        object Panel9: TPanel
          Left = 1
          Top = 1
          Width = 979
          Height = 340
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          TabOrder = 0
          object Panel3: TPanel
            Left = 1
            Top = 1
            Width = 213
            Height = 338
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alLeft
            TabOrder = 0
            object LBDatabase: TListBox
              Left = 1
              Top = 1
              Width = 211
              Height = 336
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clGreen
              Font.Height = -17
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ItemHeight = 20
              ParentFont = False
              TabOrder = 0
              OnClick = LBDatabaseClick
            end
          end
          object Panel2: TPanel
            Left = 214
            Top = 1
            Width = 289
            Height = 338
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alLeft
            ParentBackground = False
            TabOrder = 1
            object Panel5: TPanel
              Left = 1
              Top = 1
              Width = 287
              Height = 69
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              BevelOuter = bvLowered
              TabOrder = 0
              object BtTudo: TBitBtn
                Tag = 127
                Left = 10
                Top = 10
                Width = 123
                Height = 49
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = '&Tudo'
                NumGlyphs = 2
                TabOrder = 0
                OnClick = BtTudoClick
              end
              object BtNenhum: TBitBtn
                Tag = 128
                Left = 145
                Top = 10
                Width = 123
                Height = 49
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = '&Nenhum'
                NumGlyphs = 2
                TabOrder = 1
                OnClick = BtNenhumClick
              end
            end
            object CBTables: TCheckListBox
              Left = 1
              Top = 70
              Width = 287
              Height = 267
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -15
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 1
            end
          end
          object Panel4: TPanel
            Left = 503
            Top = 1
            Width = 475
            Height = 338
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            BevelOuter = bvLowered
            ParentBackground = False
            TabOrder = 2
            object pnMain: TPanel
              Left = 1
              Top = 1
              Width = 473
              Height = 64
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              BevelInner = bvLowered
              TabOrder = 0
              object lbHost: TLabel
                Left = 7
                Top = 10
                Width = 25
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Host:'
              end
              object lbDb: TLabel
                Left = 185
                Top = 10
                Width = 76
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Base de Dados:'
              end
              object Label5: TLabel
                Left = 424
                Top = 10
                Width = 55
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Conectado:'
              end
              object Label3: TLabel
                Left = 287
                Top = 10
                Width = 29
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Login:'
              end
              object lbPswd: TLabel
                Left = 344
                Top = 10
                Width = 34
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Senha:'
              end
              object Label1: TLabel
                Left = 133
                Top = 10
                Width = 28
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Porta:'
              end
              object Label6: TLabel
                Left = 600
                Top = 10
                Width = 159
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Caminho do Zip Command Line'#169':'
                Visible = False
              end
              object Label2: TLabel
                Left = 497
                Top = 10
                Width = 57
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Servi'#231'o BD:'
              end
              object edHost: TdmkEdit
                Left = 7
                Top = 30
                Width = 123
                Height = 25
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = 'localhost'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 'localhost'
                ValWarn = False
                OnChange = edHostChange
                OnExit = edHostExit
              end
              object edDb: TdmkEdit
                Left = 185
                Top = 30
                Width = 99
                Height = 25
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = 'mysql'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 'mysql'
                ValWarn = False
                OnChange = edDbChange
              end
              object EdSeg: TdmkEdit
                Left = 424
                Top = 30
                Width = 70
                Height = 25
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabStop = False
                Color = clInfoBk
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clNavy
                Font.Height = -15
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                ReadOnly = True
                TabOrder = 5
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '  00:00:00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = '  00:00:00'
                ValWarn = False
              end
              object EdLogin: TdmkEdit
                Left = 287
                Top = 30
                Width = 54
                Height = 25
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 3
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = 'root'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 'root'
                ValWarn = False
                OnChange = edDbChange
              end
              object edPorta: TdmkEdit
                Left = 133
                Top = 30
                Width = 49
                Height = 25
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '3306'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 3306
                ValWarn = False
                OnChange = edPortaChange
                OnExit = edPortaExit
              end
              object edPswd: TdmkEdit
                Left = 344
                Top = 30
                Width = 77
                Height = 25
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Font.Charset = SYMBOL_CHARSET
                Font.Color = clWindowText
                Font.Height = -15
                Font.Name = 'Wingdings'
                Font.Style = []
                ParentFont = False
                TabOrder = 4
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                PasswordChar = '|'
                OnChange = edPswdChange
              end
              object EdZipPath: TdmkEdit
                Left = 600
                Top = 30
                Width = 209
                Height = 25
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 7
                Visible = False
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = 'C:\Arquivos de Programas\WinZip'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 'C:\Arquivos de Programas\WinZip'
                ValWarn = False
                OnChange = edHostChange
                OnExit = edHostExit
              end
              object EdServicoBD: TdmkEdit
                Left = 497
                Top = 30
                Width = 100
                Height = 25
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 6
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnChange = edHostChange
                OnExit = EdServicoBDExit
              end
            end
            object Panel7: TPanel
              Left = 1
              Top = 65
              Width = 473
              Height = 79
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              TabOrder = 1
              object Label7: TLabel
                Left = 1
                Top = 1
                Width = 471
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alTop
                Alignment = taCenter
                Caption = 'Pr'#243'ximo backup'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clGray
                Font.Height = -15
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                ExplicitWidth = 112
              end
              object DBGrid4: TDBGrid
                Left = 1
                Top = 17
                Width = 384
                Height = 61
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alLeft
                DataSource = DsProxBk
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Codigo'
                    ReadOnly = True
                    Title.Caption = 'ID'
                    Width = 32
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Hora'
                    Width = 48
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Host'
                    Width = 71
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Db'
                    Width = 70
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'FALTA'
                    Width = 48
                    Visible = True
                  end>
              end
              object Panel11: TPanel
                Left = 385
                Top = 17
                Width = 87
                Height = 61
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                TabOrder = 1
                object Label4: TLabel
                  Left = 6
                  Top = 10
                  Width = 64
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Host agenda:'
                end
                object EdHostAgenda: TdmkEdit
                  Left = 94
                  Top = 5
                  Width = 226
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = 'localhost'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 'localhost'
                  ValWarn = False
                  OnExit = EdHostAgendaExit
                end
                object CkWinZip: TCheckBox
                  Left = 6
                  Top = 34
                  Width = 314
                  Height = 21
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Compactar o arquivo de backup.'
                  TabOrder = 1
                  Visible = False
                  OnClick = CkWinZipClick
                end
              end
            end
            object Panel10: TPanel
              Left = 1
              Top = 144
              Width = 473
              Height = 193
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              Caption = 'Panel10'
              TabOrder = 2
              object Label8: TLabel
                Left = 1
                Top = 1
                Width = 471
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alTop
                Alignment = taCenter
                Caption = 'Backups em execu'#231#227'o'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clGray
                Font.Height = -15
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                ExplicitWidth = 157
              end
              object DBGrid3: TDBGrid
                Left = 1
                Top = 17
                Width = 471
                Height = 175
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                DataSource = DsEmExec
                Enabled = False
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Codigo'
                    ReadOnly = True
                    Title.Caption = 'ID'
                    Width = 32
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Hora'
                    Width = 48
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Host'
                    Width = 99
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Db'
                    Width = 98
                    Visible = True
                  end>
              end
            end
          end
        end
        object PageControl1: TPageControl
          Left = 1
          Top = 341
          Width = 979
          Height = 108
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          ActivePage = TabSheet6
          Align = alClient
          TabOrder = 1
          object TabSheet6: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Backup / sincronismo autom'#225'tico'
            object LaAgenda: TLabel
              Left = 0
              Top = 0
              Width = 971
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              Caption = '...'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -15
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              ExplicitWidth = 13
            end
            object DBGrid2: TDBGrid
              Left = 60
              Top = 16
              Width = 911
              Height = 64
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              DataSource = DsBkAgenda
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -15
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              ParentFont = False
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              OnDrawColumnCell = DBGrid2DrawColumnCell
              OnDblClick = DBGrid2DblClick
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Ativo'
                  Width = 35
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  ReadOnly = True
                  Title.Caption = 'ID'
                  Width = 32
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Tipo_TXT'
                  Title.Caption = 'Tipo'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'UltExe'
                  ReadOnly = True
                  Title.Caption = #218'ltimo'
                  Width = 120
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Aviso'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ReadOnly = True
                  Width = 350
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Destino'
                  Width = 250
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Hora'
                  Width = 48
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Host'
                  Width = 99
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Db'
                  Title.Caption = 'Base dados'
                  Width = 98
                  Visible = True
                end>
            end
            object Panel12: TPanel
              Left = 0
              Top = 16
              Width = 60
              Height = 64
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 1
              object BtInclui: TBitBtn
                Tag = 10
                Left = 5
                Top = 5
                Width = 49
                Height = 49
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                NumGlyphs = 2
                TabOrder = 0
                OnClick = BtIncluiClick
              end
              object BtExclui: TBitBtn
                Tag = 12
                Left = 5
                Top = 105
                Width = 49
                Height = 49
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                NumGlyphs = 2
                TabOrder = 1
                OnClick = BtExcluiClick
              end
              object BtAltera: TBitBtn
                Tag = 11
                Left = 5
                Top = 55
                Width = 49
                Height = 49
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                NumGlyphs = 2
                TabOrder = 2
                OnClick = BtAlteraClick
              end
            end
          end
        end
      end
    end
    object TSLogFile: TTabSheet
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Log do backup'
      ImageIndex = 1
      object Memo1: TMemo
        Left = 0
        Top = 0
        Width = 1233
        Height = 565
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
      end
    end
    object TSStruct: TTabSheet
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Mostra estrutura da tabela'
      ImageIndex = 2
      object Splitter1: TSplitter
        Left = 194
        Top = 0
        Width = 4
        Height = 450
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        ExplicitHeight = 553
      end
      object StructMemo: TMemo
        Left = 198
        Top = 0
        Width = 787
        Height = 450
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 1035
        ExplicitHeight = 565
      end
      object LBStrTabs: TListBox
        Left = 0
        Top = 0
        Width = 194
        Height = 450
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
      end
    end
    object TabSheet1: TTabSheet
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Log da Conex'#227'o'
      ImageIndex = 3
      object ListBox1: TListBox
        Left = 0
        Top = 0
        Width = 985
        Height = 450
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        ItemHeight = 13
        TabOrder = 0
      end
    end
    object TabSheet2: TTabSheet
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Diagn'#243'sticos'
      ImageIndex = 4
      object DBGrid1: TDBGrid
        Left = 0
        Top = 59
        Width = 1233
        Height = 506
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        DataSource = DsOpt
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Table'
            Width = 140
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Op'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Msg_type'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Msg_text'
            Width = 200
            Visible = True
          end>
      end
      object Panel6: TPanel
        Left = 0
        Top = 0
        Width = 1233
        Height = 59
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        ParentBackground = False
        TabOrder = 1
        object BtExclamacao: TBitBtn
          Tag = 10001
          Left = 10
          Top = 4
          Width = 111
          Height = 49
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Checa'
          TabOrder = 0
          OnClick = BtExclamacaoClick
        end
        object BtVerifica: TBitBtn
          Tag = 22
          Left = 123
          Top = 4
          Width = 111
          Height = 49
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Analiza'
          TabOrder = 1
          OnClick = BtVerificaClick
        end
        object BtServicos: TBitBtn
          Tag = 10090
          Left = 236
          Top = 4
          Width = 111
          Height = 49
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Repara'
          TabOrder = 2
          OnClick = BtServicosClick
        end
        object BitBtn2: TBitBtn
          Tag = 10128
          Left = 354
          Top = 4
          Width = 111
          Height = 49
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Otimiza'
          TabOrder = 3
          OnClick = BitBtn2Click
        end
      end
    end
    object TabSheet3: TTabSheet
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Log da restaura'#231#227'o'
      ImageIndex = 5
      object Memo2: TMemo
        Left = 0
        Top = 0
        Width = 1233
        Height = 565
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
      end
    end
    object TabSheet4: TTabSheet
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Log de Erro'
      ImageIndex = 6
      object Memo3: TMemo
        Left = 0
        Top = 0
        Width = 1233
        Height = 565
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
      end
    end
    object TabSheet5: TTabSheet
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Comandos DOS'
      ImageIndex = 7
      object MeResult: TMemo
        Left = 0
        Top = 0
        Width = 985
        Height = 450
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        Color = clBlack
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Courier'
        Font.Style = [fsBold]
        Lines.Strings = (
          'Nenhuma mensagem recebida...')
        ParentFont = False
        ReadOnly = True
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
    object TabSheet8: TTabSheet
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Ajuda'
      ImageIndex = 8
      object BtSobre: TBitBtn
        Tag = 10136
        Left = 5
        Top = 5
        Width = 185
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sobre'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtSobreClick
      end
      object BtContrato: TBitBtn
        Tag = 183
        Left = 4
        Top = 123
        Width = 184
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Licen'#231'a de uso'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtContratoClick
      end
      object BtAjuda: TBitBtn
        Tag = 615
        Left = 5
        Top = 62
        Width = 185
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Ajuda'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtAjudaClick
      end
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 560
    Width = 993
    Height = 15
    Panels = <
      item
        Bevel = pbNone
        Width = 37
      end
      item
        Width = 40
      end>
  end
  object OpenDialog1: TOpenDialog
    Filter = 'SQL-Backup|*.sql'
    Left = 236
    Top = 169
  end
  object PmBackup: TPopupMenu
    Left = 264
    Top = 168
    object Selecionado1: TMenuItem
      Caption = '&Selecionado'
    end
    object BlueDermGeral1: TMenuItem
      Caption = '&Geral MySQL'
    end
  end
  object PopupMenu1: TPopupMenu
    OnPopup = PopupMenu1Popup
    Left = 264
    Top = 196
    object Mostrar1: TMenuItem
      Caption = '&Mostrar'
      OnClick = Mostrar1Click
    end
    object Fechar1: TMenuItem
      Caption = '&Fechar'
      OnClick = Fechar1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Inicializao1: TMenuItem
      Caption = '&Inicializa'#231#227'o'
      object Executarnainicializao1: TMenuItem
        Caption = '&Executar na inicializa'#231#227'o'
        OnClick = Executarnainicializao1Click
      end
      object NOexecutarnainicializao1: TMenuItem
        Caption = '&N'#195'O executar na inicializa'#231#227'o'
        OnClick = NOexecutarnainicializao1Click
      end
    end
  end
  object MyTools: TMySQLTools
    Database = FmBackup3.DbAll
    OnError = MyToolsError
    OnSuccess = MyToolsSuccess
    Left = 104
    Top = 132
  end
  object QrOpt: TMySQLQuery
    Database = FmBackup3.DbAll
    SQL.Strings = (
      'CHECK TABLE Controle')
    Left = 72
    Top = 216
  end
  object DsOpt: TDataSource
    DataSet = QrOpt
    Left = 100
    Top = 216
  end
  object QrTabelas: TMySQLQuery
    Database = FmBackup3.DbAll
    SQL.Strings = (
      'SHOW TABLES FROM :p0')
    Left = 72
    Top = 292
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object DsTabelas: TDataSource
    DataSet = QrTabelas
    Left = 100
    Top = 292
  end
  object DsDatabases: TDataSource
    DataSet = QrDatabases
    Left = 100
    Top = 264
  end
  object QrDatabases: TMySQLQuery
    Database = FmBackup3.DbAll
    SQL.Strings = (
      'SHOW DATABASES')
    Left = 72
    Top = 264
  end
  object Timer1: TTimer
    Enabled = False
    OnTimer = Timer1Timer
    Left = 325
    Top = 132
  end
  object PMAgenda: TPopupMenu
    Left = 639
    Top = 244
    object Incluihorrio1: TMenuItem
      Caption = '&Inclui hor'#225'rio'
    end
    object Excluihorrio1: TMenuItem
      Caption = '&Exclui hor'#225'rio'
    end
  end
  object DbDump: TMySQLDatabase
    DesignOptions = []
    ConnectOptions = []
    KeepConnection = False
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 16
    Top = 264
  end
  object QrTbDump: TMySQLQuery
    Database = DbDump
    SQL.Strings = (
      'SHOW TABLES FROM :p0')
    Left = 16
    Top = 348
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrDBDump: TMySQLQuery
    Database = DbDump
    SQL.Strings = (
      'SHOW DATABASES')
    Left = 16
    Top = 320
  end
  object PMTipoBK: TPopupMenu
    Left = 368
    Top = 20
    object Beta1: TMenuItem
      Caption = '&Novo (mais r'#225'pido)'
      OnClick = Beta1Click
    end
    object Normal1: TMenuItem
      Caption = '&Antigo'
      OnClick = Normal1Click
    end
  end
  object TrayIcon1: TTrayIcon
    BalloonHint = 'Backup de Databases Dermatek [MySQL] *'
    BalloonTitle = 'Backup de Databases Dermatek [MySQL]'
    Icon.Data = {
      000001000800808000000100200028080100860000006060000001002000A894
      0000AE080100484800000100200088540000569D010040400000010020002842
      0000DEF101003030000001002000A8250000063402002020000001002000A810
      0000AE590200181800000100200088090000566A020010100000010020006804
      0000DE7302002800000080000000000100000100200000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000005A4C3C015A4C3C015A4C3C025A4C3C025A4C3C025A4C
      3C015A4C3C010000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00005A4C3C015A4C3C035A4C3C055A4C3C085A4C3C0B5A4C3C0C5A4C3C0C5A4C
      3C095A4C3C065A4C3C045A4C3C02000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000005A4C3C025A4C
      3C045A4C3C075A4C3C0D5A4C3C125A4C3C185A4C3C1D5A4C3C1F5A4C3C1E5A4C
      3C1A5A4C3C145A4C3C0E5A4C3C085A4C3C055A4C3C0200000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000005A4C3C015A4C3C025A4C3C055A4C3C085A4C
      3C0E5A4C3C155A4C3C1E5A4C3C285A4C3C315A4C3C385A4C3C3C5A4C3C3A5A4C
      3C335A4C3C2A5A4C3C205A4C3C175A4C3C0E5A4C3C095A4C3C055A4C3C025A4C
      3C01000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000005A4C3C015A4C3C035A4C3C055A4C3C095A4C3C0F5A4C3C175A4C
      3C205A4C3C2C5A4C3C395A4C3C475A4C3C535A4C3C5D5A4C3C615A4C3C5F5A4C
      3C575A4C3C4A5A4C3C3C5A4C3C2E5A4C3C225A4C3C175A4C3C0F5A4C3C095A4C
      3C055A4C3C025A4C3C0100000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000005A4C
      3C015A4C3C035A4C3C065A4C3C0B5A4C3C115A4C3C195A4C3C235A4C3C2F5A4C
      3C3C5A4C3C4C5A4C3C5D5A4C3C6D5A4C3C7C5A4C3C865A4C3C8B5A4C3C885A4C
      3C7E5A4C3C715A4C3C605A4C3C4E5A4C3C3E5A4C3C305A4C3C235A4C3C185A4C
      3C105A4C3C0A5A4C3C055A4C3C035A4C3C010000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005A4C3C025A4C3C045A4C
      3C075A4C3C0C5A4C3C135A4C3C1B5A4C3C265A4C3C325A4C3C405A4C3C4F5A4C
      3C615A4C3C735A4C3C845A4C3C945B4D3CA5605140D1625341E6615140D65E4F
      3FBC5A4C3C975A4C3C865A4C3C745A4C3C625A4C3C505A4C3C3F5A4C3C315A4C
      3C245A4C3C195A4C3C105A4C3C0A5A4C3C055A4C3C035A4C3C01000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000005A4C3C025A4C3C055A4C3C085A4C3C0E5A4C
      3C145A4C3C1D5A4C3C285A4C3C345A4C3C445A4C3C535A4C3C645A4C3C765A4C
      3C875A4C3C985D4F3EBC625241E5655543FF655543FF655543FF655543FF6555
      43FF645442F35E4F3FC35A4C3C9A5A4C3C895A4C3C765A4C3C635A4C3C515A4C
      3C415A4C3C325A4C3C255A4C3C1A5A4C3C115A4C3C0B5A4C3C065A4C3C035A4C
      3C01000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00005A4C3C015A4C3C025A4C3C055A4C3C095A4C3C0E5A4C3C175A4C3C205A4C
      3C2A5A4C3C385A4C3C465A4C3C575A4C3C695A4C3C7A5A4C3C8C5A4C3C9C5E4F
      3FC4625341EA655543FF655543FF655543FF655543FF655543FF655543FF6555
      43FF655543FF655543FF645442F35E4F3FC45A4C3C9C5A4C3C8A5A4C3C785A4C
      3C655A4C3C535A4C3C425A4C3C335A4C3C265A4C3C1B5A4C3C125A4C3C0B5A4C
      3C065A4C3C035A4C3C0100000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000005A4C3C015A4C
      3C035A4C3C055A4C3C0A5A4C3C105A4C3C185A4C3C215A4C3C2D5A4C3C3B5A4C
      3C4A5A4C3C5B5A4C3C6C5A4C3C7E5A4C3C8F5A4C3CA05F503FCE645442F36555
      43FF655543FF655543FF655543FF655543FF4B459DFF4B4BE5FF4645D1FF6253
      52FF655543FF655543FF655543FF655543FF645442F85F503FCD5A4C3C9D5A4C
      3C8C5A4C3C795A4C3C675A4C3C555A4C3C445A4C3C345A4C3C275A4C3C1C5A4C
      3C135A4C3C0C5A4C3C065A4C3C045A4C3C010000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000005A4C3C015A4C3C035A4C3C065A4C
      3C0B5A4C3C125A4C3C1A5A4C3C245A4C3C315A4C3C3E5A4C3C4E5A4C3C5F5A4C
      3C705A4C3C825A4C3C935B4D3CA7605140D4645442F8655543FF655543FF6555
      43FF655543FF655543FF4643BAFF5656F4FF5D5DFBFF5D5DFBFF5D5DFBFF5A5A
      F8FF4544D3FF625452FF655543FF655543FF655543FF655543FF645442F85F50
      3FCE5A4C3C9E5A4C3C8D5A4C3C7B5A4C3C695A4C3C565A4C3C455A4C3C355A4C
      3C285A4C3C1D5A4C3C135A4C3C0D5A4C3C075A4C3C045A4C3C01000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000005A4C3C025A4C3C045A4C3C075A4C3C0D5A4C3C135A4C
      3C1C5A4C3C275A4C3C335A4C3C425A4C3C515A4C3C625A4C3C745A4C3C855A4C
      3C975C4E3DB3615140DB655543FF655543FF655543FF655543FF655543FF6254
      52FF4645D4FF5B5BF9FF5F5FFDFF5F5FFDFF5F5FFDFF5F5FFDFF5F5FFDFF5F5F
      FDFF5F5FFDFF5B5BF9FF4645D4FF554B76FF655543FF655543FF655543FF6555
      43FF645442F85F503FCE5A4C3CA05A4C3C8F5A4C3C7C5A4C3C6A5A4C3C575A4C
      3C465A4C3C375A4C3C295A4C3C1E5A4C3C145A4C3C0D5A4C3C075A4C3C045A4C
      3C02000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000005A4C
      3C015A4C3C025A4C3C055A4C3C085A4C3C0E5A4C3C155A4C3C1F5A4C3C295A4C
      3C365A4C3C455A4C3C555A4C3C665A4C3C785A4C3C8A5A4C3C9A5D4F3EBE6252
      41E5655543FF655543FF655543FF655543FF655543FF554B76FF4A4AE4FF5D5D
      FBFF6060FEFF6060FEFF6060FEFF6060FEFF6060FEFF6060FEFF6060FEFF6060
      FEFF6060FEFF6060FEFF6060FEFF6060FEFF4A4AE4FF554B76FF655543FF6555
      43FF655543FF655543FF655543FC615140D95C4D3DAB5A4C3C905A4C3C7D5A4C
      3C6B5A4C3C595A4C3C485A4C3C385A4C3C2A5A4C3C1F5A4C3C155A4C3C0E5A4C
      3C085A4C3C045A4C3C0200000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005A4C3C015A4C3C035A4C
      3C055A4C3C095A4C3C0F5A4C3C175A4C3C205A4C3C2C5A4C3C3A5A4C3C485A4C
      3C585A4C3C6A5A4C3C7C5A4C3C8E5A4C3C9E5E4F3FC4645442F3655543FF6555
      43FF655543FF655543FF655543FF4C469EFF4F4FEBFF6363FFFF6363FFFF6363
      FFFF6363FFFF6363FFFF6363FFFF6363FFFF6363FFFF6363FFFF6363FFFF6363
      FFFF6363FFFF6363FFFF6363FFFF6363FFFF6363FFFF6363FFFF4B4BE5FF554C
      76FF655543FF655543FF655543FF655543FF655543FF615140DA5C4D3DAC5A4C
      3C925A4C3C805A4C3C6D5A4C3C5A5A4C3C495A4C3C395A4C3C2B5A4C3C205A4C
      3C165A4C3C0E5A4C3C085A4C3C055A4C3C020000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000005A4C3C015A4C3C035A4C3C065A4C3C0B5A4C
      3C115A4C3C195A4C3C235A4C3C2F5A4C3C3C5A4C3C4C5A4C3C5D5A4C3C6E5A4C
      3C805A4C3C915A4C3CA15F503FCF645442F8655543FF655543FF655543FF6555
      43FF655543FF4745C1FF5353F1FF6666FFFF6666FFFF6666FFFF6666FFFF6666
      FFFF6666FFFF6666FFFF6666FFFF6666FFFF6666FFFF6666FFFF6666FFFF6666
      FFFF6666FFFF6666FFFF6666FFFF6666FFFF6666FFFF6666FFFF6666FFFF6666
      FFFF4D4DE8FF51498CFF655543FF655543FF655543FF655543FF655543FF6151
      40DA5C4D3DAE5A4C3C935A4C3C815A4C3C6E5A4C3C5C5A4C3C4B5A4C3C3B5A4C
      3C2C5A4C3C205A4C3C175A4C3C0E5A4C3C085A4C3C055A4C3C025A4C3C010000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000005A4C3C025A4C3C045A4C3C075A4C3C0C5A4C3C135A4C3C1B5A4C
      3C265A4C3C325A4C3C405A4C3C4F5A4C3C615A4C3C735A4C3C845A4C3C955C4D
      3DAF615140DA655543FC655543FF655543FF655543FF655543FF5D505EFF4745
      D1FF5E5EFCFF6969FFFF6969FFFF6969FFFF6969FFFF6969FFFF6969FFFF6969
      FFFF6969FFFF6969FFFF6969FFFF6969FFFF6969FFFF6969FFFF6969FFFF6969
      FFFF6969FFFF6969FFFF6969FFFF6969FFFF6969FFFF6969FFFF6969FFFF6969
      FFFF6969FFFF6969FFFF5555F3FF4945A5FF655543FF655543FF655543FF6555
      43FF655543FF625241E45D4F3EBA5A4C3C955A4C3C835A4C3C705A4C3C5E5A4C
      3C4C5A4C3C3C5A4C3C2E5A4C3C215A4C3C175A4C3C0F5A4C3C095A4C3C055A4C
      3C025A4C3C010000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000005A4C
      3C025A4C3C055A4C3C085A4C3C0E5A4C3C145A4C3C1D5A4C3C285A4C3C345A4C
      3C445A4C3C535A4C3C645A4C3C765A4C3C875A4C3C985D4F3EBC625241E56555
      43FF655543FF655543FF655543FF655543FF564C77FF4949DDFF6464FFFF6B6B
      FFFF6B6BFFFF6B6BFFFF6B6BFFFF6B6BFFFF6B6BFFFF6B6BFFFF6B6BFFFF6B6B
      FFFF6B6BFFFF6B6BFFFF6B6BFFFF6B6BFFFF6B6BFFFF6B6BFFFF6B6BFFFF6B6B
      FFFF6B6BFFFF6B6BFFFF6B6BFFFF6B6BFFFF6B6BFFFF6B6BFFFF6B6BFFFF6B6B
      FFFF6B6BFFFF6B6BFFFF6B6BFFFF6B6BFFFF5656F4FF4A46A6FF655543FF6555
      43FF655543FF655543FF655543FF625241E55D4F3EBB5A4C3C975A4C3C855A4C
      3C725A4C3C605A4C3C4E5A4C3C3D5A4C3C2F5A4C3C225A4C3C175A4C3C0F5A4C
      3C095A4C3C055A4C3C025A4C3C01000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000005A4C3C015A4C3C025A4C3C055A4C
      3C095A4C3C0E5A4C3C175A4C3C205A4C3C2A5A4C3C385A4C3C465A4C3C575A4C
      3C695A4C3C7A5A4C3C8C5A4C3C9C5E4F3FC4625341EA655543FF655543FF6555
      43FF655543FF655543FF4D4795FF4F4FECFF6E6EFFFF6E6EFFFF6E6EFFFF6E6E
      FFFF6E6EFFFF6E6EFFFF6E6EFFFF6E6EFFFF6E6EFFFF6E6EFFFF6E6EFFFF6E6E
      FFFF6E6EFFFF6E6EFFFF6E6EFFFF6E6EFFFF6E6EFFFF6E6EFFFF6E6EFFFF6E6E
      FFFF6E6EFFFF6E6EFFFF6E6EFFFF6E6EFFFF6E6EFFFF6E6EFFFF6E6EFFFF6E6E
      FFFF6E6EFFFF6E6EFFFF6E6EFFFF6E6EFFFF6E6EFFFF6E6EFFFF5858F6FF4744
      BBFF655543FF655543FF655543FF655543FF655543FF625241E55D4F3EBC5A4C
      3C985A4C3C865A4C3C735A4C3C615A4C3C4F5A4C3C3E5A4C3C305A4C3C235A4C
      3C185A4C3C105A4C3C0A5A4C3C055A4C3C035A4C3C0100000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000005A4C3C015A4C3C035A4C3C055A4C3C0A5A4C3C105A4C
      3C185A4C3C215A4C3C2D5A4C3C3B5A4C3C4A5A4C3C5B5A4C3C6C5A4C3C7E5A4C
      3C8F5A4C3CA05F503FCE645442F3655543FF655543FF655543FF655543FF6555
      43FF4944ADFF5959F7FF7171FFFF7171FFFF7171FFFF7171FFFF7171FFFF7171
      FFFF7171FFFF7171FFFF7171FFFF7171FFFF7171FFFF7171FFFF7171FFFF7171
      FFFF7171FFFF7171FFFF7171FFFF7171FFFF7171FFFF7171FFFF7171FFFF7171
      FFFF7171FFFF7171FFFF7171FFFF7171FFFF7171FFFF7171FFFF7171FFFF7171
      FFFF7171FFFF7171FFFF7171FFFF7171FFFF7171FFFF7171FFFF7171FFFF7171
      FFFF6464FFFF4645CBFF655543FF655543FF655543FF655543FF655543FF6454
      42F35E4F3FC35A4C3C995A4C3C875A4C3C755A4C3C625A4C3C505A4C3C405A4C
      3C315A4C3C245A4C3C195A4C3C115A4C3C0A5A4C3C055A4C3C035A4C3C010000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00005A4C3C015A4C3C035A4C3C065A4C3C0B5A4C3C125A4C3C1A5A4C3C245A4C
      3C315A4C3C3E5A4C3C4E5A4C3C5F5A4C3C705A4C3C825A4C3C935B4D3CA76051
      40D4645442F8655543FF655543FF655543FF655543FF655543FF4745CBFF6767
      FFFF7474FFFF7474FFFF7474FFFF7474FFFF7474FFFF7474FFFF7474FFFF7474
      FFFF7474FFFF7474FFFF7474FFFF7474FFFF7474FFFF7474FFFF7474FFFF7474
      FFFF7474FFFF7474FFFF7474FFFF7474FFFF7474FFFF7474FFFF7474FFFF7474
      FFFF7474FFFF7474FFFF7474FFFF7474FFFF7474FFFF7474FFFF7474FFFF7474
      FFFF7474FFFF7474FFFF7474FFFF7474FFFF7474FFFF7474FFFF7474FFFF7474
      FFFF7474FFFF7474FFFF6767FFFF4745CBFF655543FF655543FF655543FF6555
      43FF655543FF645442F35E4F3FC35A4C3C9B5A4C3C895A4C3C775A4C3C645A4C
      3C525A4C3C415A4C3C325A4C3C255A4C3C1A5A4C3C115A4C3C0B5A4C3C065A4C
      3C035A4C3C010000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000005A4C3C025A4C
      3C045A4C3C075A4C3C0D5A4C3C135A4C3C1C5A4C3C275A4C3C335A4C3C425A4C
      3C515A4C3C625A4C3C745A4C3C855A4C3C975C4E3DB3615140DB655543FF6555
      43FF655543FF655543FF655543FF5E515FFF4B4BE5FF7373FFFF7878FFFF7878
      FFFF7878FFFF7878FFFF7878FFFF7878FFFF7878FFFF7878FFFF7878FFFF7878
      FFFF7878FFFF7878FFFF7878FFFF7878FFFF7878FFFF7878FFFF7878FFFF7878
      FFFF7878FFFF7878FFFF7878FFFF7878FFFF7878FFFF7878FFFF7878FFFF7878
      FFFF7878FFFF7878FFFF7878FFFF7878FFFF7878FFFF7878FFFF7878FFFF7878
      FFFF7878FFFF7878FFFF7878FFFF7878FFFF7878FFFF7878FFFF7878FFFF7878
      FFFF7878FFFF7878FFFF7878FFFF7878FFFF6A6AFFFF4948DCFF5E515FFF6555
      43FF655543FF655543FF655543FF645442F35F503FC75A4C3C9C5A4C3C8B5A4C
      3C785A4C3C655A4C3C535A4C3C435A4C3C335A4C3C265A4C3C1B5A4C3C125A4C
      3C0B5A4C3C065A4C3C035A4C3C01000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000005A4C3C015A4C3C025A4C3C055A4C3C085A4C
      3C0E5A4C3C155A4C3C1F5A4C3C295A4C3C365A4C3C455A4C3C555A4C3C665A4C
      3C785A4C3C8A5A4C3C9A5D4F3EBE625241E5655543FF655543FF655543FF6555
      43FF655543FF524A81FF5555F3FF7474FFFF7B7BFFFF7B7BFFFF7B7BFFFF7B7B
      FFFF7B7BFFFF7B7BFFFF7B7BFFFF7B7BFFFF7B7BFFFF7B7BFFFF7B7BFFFF7B7B
      FFFF7B7BFFFF7B7BFFFF7B7BFFFF7B7BFFFF7B7BFFFF7B7BFFFF7B7BFFFF7B7B
      FFFF7B7BFFFF7B7BFFFF7B7BFFFF7B7BFFFF7B7BFFFF7B7BFFFF7B7BFFFF7B7B
      FFFF7B7BFFFF7B7BFFFF7B7BFFFF7B7BFFFF7B7BFFFF7B7BFFFF7B7BFFFF7B7B
      FFFF7B7BFFFF7B7BFFFF7B7BFFFF7B7BFFFF7B7BFFFF7B7BFFFF7B7BFFFF7B7B
      FFFF7B7BFFFF7B7BFFFF7B7BFFFF7B7BFFFF7B7BFFFF7B7BFFFF7474FFFF4C4C
      E6FF5E515FFF655543FF655543FF655543FF655543FF645442F85F503FCD5A4C
      3C9D5A4C3C8C5A4C3C7A5A4C3C675A4C3C555A4C3C445A4C3C345A4C3C275A4C
      3C1C5A4C3C135A4C3C0C5A4C3C065A4C3C045A4C3C0100000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000005A4C3C015A4C3C035A4C3C055A4C3C095A4C3C0F5A4C3C175A4C
      3C205A4C3C2C5A4C3C3A5A4C3C485A4C3C585A4C3C6A5A4C3C7C5A4C3C8E5A4C
      3C9E5E4F3FC4645442F3655543FF655543FF655543FF655543FF655543FF4845
      B4FF5B5BF9FF7E7EFFFF7E7EFFFF7E7EFFFF7E7EFFFF7E7EFFFF7E7EFFFF7E7E
      FFFF7E7EFFFF7E7EFFFF7E7EFFFF7E7EFFFF7E7EFFFF7E7EFFFF7E7EFFFF7E7E
      FFFF7E7EFFFF7E7EFFFF7E7EFFFF7E7EFFFF7E7EFFFF7E7EFFFF7E7EFFFF7E7E
      FFFF7E7EFFFF7E7EFFFF7E7EFFFF7E7EFFFF7E7EFFFF7E7EFFFF7E7EFFFF7E7E
      FFFF7E7EFFFF7E7EFFFF7E7EFFFF7E7EFFFF7E7EFFFF7E7EFFFF7E7EFFFF7E7E
      FFFF7E7EFFFF7E7EFFFF7E7EFFFF7E7EFFFF7E7EFFFF7E7EFFFF7E7EFFFF7E7E
      FFFF7E7EFFFF7E7EFFFF7E7EFFFF7E7EFFFF7E7EFFFF7E7EFFFF7E7EFFFF7E7E
      FFFF7777FFFF4D4DE8FF5E515FFF655543FF655543FF655543FF655543FF6454
      42F85F503FCE5A4C3C9F5A4C3C8E5A4C3C7C5A4C3C695A4C3C575A4C3C455A4C
      3C355A4C3C295A4C3C1D5A4C3C145A4C3C0D5A4C3C075A4C3C045A4C3C010000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000005A4C
      3C015A4C3C035A4C3C065A4C3C0B5A4C3C115A4C3C195A4C3C235A4C3C2F5A4C
      3C3C5A4C3C4C5A4C3C5D5A4C3C6E5A4C3C805A4C3C915A4C3CA15F503FCF6454
      42F8655543FF655543FF655543FF655543FF655543FF4847D8FF6464FFFF8282
      FFFF8282FFFF8282FFFF8282FFFF8282FFFF8282FFFF8282FFFF8282FFFF8282
      FFFF8282FFFF8282FFFF8282FFFF8282FFFF8282FFFF8282FFFF8282FFFF8282
      FFFF8282FFFF8282FFFF8282FFFF8282FFFF8282FFFF8282FFFF8282FFFF8282
      FFFF8282FFFF8282FFFF8282FFFF8282FFFF8282FFFF8282FFFF8282FFFF8282
      FFFF8282FFFF8282FFFF8282FFFF8282FFFF8282FFFF8282FFFF8282FFFF8282
      FFFF8282FFFF8282FFFF8282FFFF8282FFFF8282FFFF8282FFFF8282FFFF8282
      FFFF8282FFFF8282FFFF8282FFFF8282FFFF8282FFFF8282FFFF8282FFFF8282
      FFFF8282FFFF8282FFFF7B7BFFFF5353F1FF534B82FF655543FF655543FF6555
      43FF655543FF645442F85F503FCF5A4C3CA25A4C3C8F5A4C3C7C5A4C3C6A5A4C
      3C575A4C3C465A4C3C375A4C3C295A4C3C1E5A4C3C145A4C3C0D5A4C3C075A4C
      3C045A4C3C020000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005A4C3C025A4C3C045A4C
      3C075A4C3C0C5A4C3C135A4C3C1B5A4C3C265A4C3C325A4C3C405A4C3C4F5A4C
      3C615A4C3C735A4C3C845A4C3C955C4D3DAF615140DA655543FC655543FF6555
      43FF655543FF655543FF5E515FFF4B4BE3FF7777FFFF8787FFFF8787FFFF8787
      FFFF8787FFFF8787FFFF8787FFFF8787FFFF8787FFFF8787FFFF8787FFFF8787
      FFFF8787FFFF8787FFFF8787FFFF8787FFFF8787FFFF8787FFFF8787FFFF8787
      FFFF8787FFFF8787FFFF8787FFFF8787FFFF8787FFFF8787FFFF8787FFFF8787
      FFFF8787FFFF8787FFFF8787FFFF8787FFFF8787FFFF8787FFFF8787FFFF8787
      FFFF8787FFFF8787FFFF8787FFFF8787FFFF8787FFFF8787FFFF8787FFFF8787
      FFFF8787FFFF8787FFFF8787FFFF8787FFFF8787FFFF8787FFFF8787FFFF8787
      FFFF8787FFFF8787FFFF8787FFFF8787FFFF8787FFFF8787FFFF8787FFFF8787
      FFFF8787FFFF8787FFFF8787FFFF8787FFFF8787FFFF5A5AF8FF534B82FF6555
      43FF655543FF655543FF655543FF655543FF615140D95C4D3DAB5A4C3C905A4C
      3C7E5A4C3C6C5A4C3C595A4C3C485A4C3C385A4C3C2A5A4C3C1F5A4C3C155A4C
      3C0E5A4C3C085A4C3C045A4C3C02000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000005A4C3C025A4C3C055A4C3C085A4C3C0E5A4C
      3C145A4C3C1D5A4C3C285A4C3C345A4C3C445A4C3C535A4C3C645A4C3C765A4C
      3C875A4C3C985D4F3EBC625241E5655543FF655543FF655543FF655543FF6555
      43FF50488BFF5050EEFF8282FFFF8989FFFF8989FFFF8989FFFF8989FFFF8989
      FFFF8989FFFF8989FFFF8989FFFF8989FFFF8989FFFF8989FFFF8989FFFF8989
      FFFF8989FFFF8989FFFF8989FFFF8989FFFF8989FFFF8989FFFF8989FFFF8989
      FFFF8989FFFF8989FFFF8989FFFF8989FFFF8989FFFF8989FFFF8989FFFF8989
      FFFF8989FFFF8989FFFF8989FFFF8989FFFF8989FFFF8989FFFF8989FFFF8989
      FFFF8989FFFF8989FFFF8989FFFF8989FFFF8989FFFF8989FFFF8989FFFF8989
      FFFF8989FFFF8989FFFF8989FFFF8989FFFF8989FFFF8989FFFF8989FFFF8989
      FFFF8989FFFF8989FFFF8989FFFF8989FFFF8989FFFF8989FFFF8989FFFF8989
      FFFF8989FFFF8989FFFF8989FFFF8989FFFF8989FFFF8989FFFF8989FFFF5B5B
      F9FF50488BFF655543FF655543FF655543FF655543FF655543FF615140DA5C4D
      3DAD5A4C3C925A4C3C805A4C3C6D5A4C3C5B5A4C3C495A4C3C395A4C3C2B5A4C
      3C205A4C3C165A4C3C0E5A4C3C085A4C3C055A4C3C0200000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000005A4C3C025A4C3C055A4C3C085A4C3C0E5A4C3C165A4C3C205A4C
      3C2A5A4C3C385A4C3C465A4C3C575A4C3C695A4C3C7A5A4C3C8C5A4C3C9C5E4F
      3FC4625341EA655543FF655543FF655543FF655543FF655543FF4B46A6FF5D5D
      FBFF8E8EFFFF8E8EFFFF8E8EFFFF8E8EFFFF8E8EFFFF8E8EFFFF8E8EFFFF8E8E
      FFFF8E8EFFFF8E8EFFFF8E8EFFFF8E8EFFFF8E8EFFFF8E8EFFFF8E8EFFFF8E8E
      FFFF8E8EFFFF8E8EFFFF8E8EFFFF8E8EFFFF8E8EFFFF8E8EFFFF8E8EFFFF8E8E
      FFFF8E8EFFFF8E8EFFFF8E8EFFFF8E8EFFFF8E8EFFFF8E8EFFFF8E8EFFFF8E8E
      FFFF8E8EFFFF8E8EFFFF8E8EFFFF8E8EFFFF8E8EFFFF8E8EFFFF8E8EFFFF8E8E
      FFFF8E8EFFFF8E8EFFFF8E8EFFFF8E8EFFFF8E8EFFFF8E8EFFFF8E8EFFFF8E8E
      FFFF8E8EFFFF8E8EFFFF8E8EFFFF8E8EFFFF8E8EFFFF8E8EFFFF8E8EFFFF8E8E
      FFFF8E8EFFFF8E8EFFFF8E8EFFFF8E8EFFFF8E8EFFFF8E8EFFFF8E8EFFFF8E8E
      FFFF8E8EFFFF8E8EFFFF8E8EFFFF8E8EFFFF8E8EFFFF8E8EFFFF8E8EFFFF8E8E
      FFFF8E8EFFFF5D5DFBFF4845BCFF655543FF655543FF655543FF655543FF6555
      43FF615140DA5C4E3DB15A4C3C945A4C3C815A4C3C6F5A4C3C5C5A4C3C4B5A4C
      3C3B5A4C3C2C5A4C3C205A4C3C165A4C3C0E5A4C3C085A4C3C045A4C3C020000
      0000000000000000000000000000000000000000000000000000000000000000
      00005A4C3C025A4C3C055A4C3C0C5A4C3C145A4C3C1F5A4C3C2B5A4C3C3A5A4C
      3C4A5A4C3C5B5A4C3C6C5A4C3C7E5A4C3C8F5A4C3CA05F503FCE645442F36555
      43FF655543FF655543FF655543FF2222C0FF3B3BD9FF7070FFFF9292FFFF9292
      FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292
      FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292
      FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292
      FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292
      FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292
      FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292
      FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292
      FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292
      FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292
      FFFF9292FFFF9292FFFF9292FFFF7070FFFF3B3BD9FF655543FF655543FF6555
      43FF655543FF655543FF625241E45D4F3EBA5A4C3C955A4C3C835A4C3C705A4C
      3C5E5A4C3C4C5A4C3C3B5A4C3C2B5A4C3C1E5A4C3C135A4C3C0A5A4C3C055A4C
      3C02000000000000000000000000000000000000000000000000000000005A4C
      3C015A4C3C055A4C3C0C5A4C3C165A4C3C255A4C3C365A4C3C495A4C3C5C5A4C
      3C705A4C3C825A4C3C935B4D3CA7605140D4645442F8655543FF655543FF6555
      43FF2222C0FF2222C0FF4848E6FF8585FFFF9696FFFF9696FFFF9696FFFF9696
      FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696
      FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696
      FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696
      FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696
      FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696
      FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696
      FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696
      FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696
      FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696
      FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696FFFF7474FFFF3B3BD9FF6555
      43FF655543FF655543FF655543FF655543FF625241E55D4F3EBB5A4C3C975A4C
      3C855A4C3C725A4C3C5D5A4C3C485A4C3C345A4C3C225A4C3C145A4C3C0A5A4C
      3C045A4C3C010000000000000000000000000000000000000000000000005A4C
      3C025A4C3C085A4C3C135A4C3C235A4C3C3B5A4C3C535A4C3C6B5A4C3C825A4C
      3C965C4E3DB3615140DB655543FF655543FF655543FF655543FF2222C0FF2929
      C7FF5656F4FF9292FFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9A
      FFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9A
      FFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9A
      FFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9A
      FFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9A
      FFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9A
      FFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9A
      FFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9A
      FFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9A
      FFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9A
      FFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF7777
      FFFF4949E7FF655543FF655543FF655543FF655543FF655543FF625241E55D4F
      3EBC5A4C3C975A4C3C815A4C3C695A4C3C4F5A4C3C365A4C3C205A4C3C105A4C
      3C065A4C3C010000000000000000000000000000000000000000000000005A4C
      3C045A4C3C0D5A4C3C1C5A4C3C335A4C3C515A4C3C705A4C3C8C5E4F3EB86252
      41E5655543FF655543FF655543FF655543FF2222C0FF3030CEFF6666FFFF9595
      FFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9D
      FFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9D
      FFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9D
      FFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9D
      FFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9D
      FFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9D
      FFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9D
      FFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9D
      FFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9D
      FFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9D
      FFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9DFFFF9D9D
      FFFF9D9DFFFF8C8CFFFF4A4AE8FF655543FF655543FF655543FF655543FF6555
      43FF645442F25E503FBD5A4C3C875A4C3C6A5A4C3C4B5A4C3C2E5A4C3C185A4C
      3C0A5A4C3C020000000000000000000000000000000000000000000000005A4C
      3C055A4C3C105A4C3C245A4C3C405A4C3C645F503FAA645442EF655543FF6555
      43FF655543FF655543FF2222C0FF3D3DDBFF7373FFFFA2A2FFFFA2A2FFFFA2A2
      FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2
      FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2
      FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2
      FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2
      FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2
      FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2
      FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2
      FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2
      FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2
      FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2
      FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2FFFFA2A2
      FFFFA2A2FFFFA2A2FFFFA2A2FFFF8F8FFFFF4B4BE9FF655543FF655543FF6555
      43FF655543FF655543FF645442EF5F503FA55A4C3C5B5A4C3C395A4C3C1F5A4C
      3C0E5A4C3C030000000000000000000000000000000000000000000000005A4C
      3C055A4C3C135A4C3C2961524189645543F0655543FF655543FF655543FF6555
      43FF2222C0FF4C4CEAFF7F7FFFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6
      FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6
      FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6
      FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6
      FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6
      FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6
      FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6
      FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6
      FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6
      FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6
      FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6
      FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6
      FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6FFFFA6A6FFFF9393FFFF5353F1FF6B5C
      4BFF655543FF655543FF655543FF655543FF645442E5615240795A4C3C245A4C
      3C0F5A4C3C040000000000000000000000000000000000000000000000005A4C
      3C065A4C3C16645442BB655543FF655543FF655543FF655543FF2A2AC8FF5555
      F3FF9898FFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAA
      FFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAA
      FFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAA
      FFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAA
      FFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAA
      FFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAA
      FFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAA
      FFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAA
      FFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAA
      FFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAA
      FFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAA
      FFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAA
      FFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFA2A2
      FFFF7470C9FF6B5C4BFF655543FF655543FF655543FF655543FF6454429E5A4C
      3C115A4C3C040000000000000000000000000000000000000000000000005A4C
      3C0663534255655543FF655543FF655543FF726353FF5D5DFBFFA4A4FFFFAEAE
      FFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAE
      FFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAE
      FFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAE
      FFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAE
      FFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAE
      FFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAE
      FFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAE
      FFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAE
      FFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAE
      FFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAE
      FFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAE
      FFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAE
      FFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAE
      FFFFAEAEFFFFA4A4FFFF5D5DFBFF6B5C4BFF655543FF655543FF655543FF6252
      41335A4C3C050000000000000000000000000000000000000000000000005A4C
      3C066454428F655543FF655543FF5F5FFDFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3
      FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3
      FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3
      FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3
      FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3
      FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3
      FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3
      FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3
      FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3
      FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3
      FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3
      FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3
      FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3
      FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3FFFFB3B3
      FFFFB3B3FFFFB3B3FFFFB3B3FFFFA9A9FFFF5050EEFF655543FF655543FF6454
      426B5A4C3C050000000000000000000000000000000000000000000000005A4C
      3C06655543C6655543FF655543FF8C8CFFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7
      FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7
      FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7
      FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7
      FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7
      FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7
      FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7
      FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7
      FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7
      FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7
      FFFF9898FFFF8C8CFFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7
      FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7
      FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7
      FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7
      FFFFB7B7FFFFB7B7FFFFB7B7FFFFB7B7FFFF716EDCFF655543FF655543FF6454
      43895A4C3C050000000000000000000000000000000000000000000000005A4C
      3C06655543C6655543FF655543FF8F8FFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBB
      FFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBB
      FFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBB
      FFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBB
      FFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBB
      FFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBB
      FFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBB
      FFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBB
      FFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBB
      FFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBB
      FFFF877B6EFF655543FF736454FF7570C1FFA4A4FFFFBBBBFFFFBBBBFFFFBBBB
      FFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBB
      FFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBB
      FFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBB
      FFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFF726FDDFF655543FF655543FF6454
      43895A4C3C050000000000000000000000000000000000000000000000005A4C
      3C06655543C6655543FF655543FF9292FFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBF
      FFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBF
      FFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBF
      FFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBF
      FFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBF
      FFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBF
      FFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBF
      FFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBF
      FFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBF
      FFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFA9A9
      FFFF655543FF655543FF655543FF655543FF655543FF7A6D5EFF7C7CFAFFB4B4
      FFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBF
      FFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBF
      FFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBF
      FFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFF7270E2FF655543FF655543FF6454
      43895A4C3C050000000000000000000000000000000000000000000000005A4C
      3C06655543C6655543FF655543FF9595FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4
      FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4
      FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4
      FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4
      FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4
      FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4
      FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4
      FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4
      FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4
      FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4FFFF7470
      C9FF655543FF655543FF655543FF655543FF655543FF655543FF655543FF6555
      43FF897D70FF8A89FEFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4
      FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4
      FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4
      FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C4FFFF7270E6FF655543FF655543FF6454
      43895A4C3C050000000000000000000000000000000000000000000000005A4C
      3C06655543C6655543FF655543FF9999FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8
      FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8
      FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8
      FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8
      FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8
      FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8
      FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8
      FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8
      FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8
      FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8FFFFBBBBFFFF6C5D
      4CFF655543FF655543FF6555433B655543A4655543F3655543FF655543FF6555
      43FF655543FF655543FF6C5D4CFF7370D0FFA4A4FFFFC8C8FFFFC8C8FFFFC8C8
      FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8
      FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8
      FFFFC8C8FFFFC8C8FFFFC8C8FFFFC8C8FFFF7372EAFF655543FF655543FF6454
      43895A4C3C050000000000000000000000000000000000000000000000005A4C
      3C06655543C6655543FF655543FF9C9CFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCC
      FFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCC
      FFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCC
      FFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCC
      FFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCC
      FFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCC
      FFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCC
      FFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCC
      FFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCC
      FFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFF8282FFFF6555
      43FF655543FF655543D300000000000000000000000065554364655543B46555
      43FF655543FF655543FF655543FF655543FF655543FF7B6E5FFF7B7AF7FFB4B4
      FFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCC
      FFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCC
      FFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFF7473EAFF655543FF655543FF6454
      43895A4C3C050000000000000000000000000000000000000000000000005A4C
      3C06655543C6655543FF655543FF9D9DFFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCF
      FFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCF
      FFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCF
      FFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCF
      FFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCF
      FFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCF
      FFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCF
      FFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCF
      FFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCF
      FFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCFFFFF5F567EFF6555
      43FF655543FF6555436400000000000000000000000000000000000000006555
      431465554374655543D3655543FF655543FF655543FF655543FF655543FF6555
      43FF817887FF8686FEFFCFCFFFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCF
      FFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCF
      FFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCFFFFF6C6CFDFF655543FF655543FF6454
      43895A4C3C050000000000000000000000000000000000000000000000005A4C
      3C06655543C6655543FF655543FFA2A2FFFFD3D3FFFFD3D3FFFFD3D3FFFFD3D3
      FFFFD3D3FFFFD3D3FFFFD3D3FFFFD3D3FFFFD3D3FFFFD3D3FFFFD3D3FFFFD3D3
      FFFFD3D3FFFFD3D3FFFFD3D3FFFFD3D3FFFFD3D3FFFFD3D3FFFFD3D3FFFFD3D3
      FFFFD3D3FFFFD3D3FFFFD3D3FFFFD3D3FFFFD3D3FFFFD3D3FFFFD3D3FFFFD3D3
      FFFFD3D3FFFFD3D3FFFFD3D3FFFFD3D3FFFFD3D3FFFFD3D3FFFFD3D3FFFFD3D3
      FFFFD3D3FFFFD3D3FFFFD3D3FFFFD3D3FFFFD3D3FFFFD3D3FFFFD3D3FFFFD3D3
      FFFFD3D3FFFFD3D3FFFFD3D3FFFFD3D3FFFFD3D3FFFFD3D3FFFFD3D3FFFFD3D3
      FFFFD3D3FFFFD3D3FFFFD3D3FFFFD3D3FFFFD3D3FFFFD3D3FFFFD3D3FFFFD3D3
      FFFFD3D3FFFFD3D3FFFFD3D3FFFFD3D3FFFFD3D3FFFFD3D3FFFFD3D3FFFFD3D3
      FFFFD3D3FFFFD3D3FFFFD3D3FFFFD3D3FFFFD3D3FFFFA2A2FFFF655543FF6555
      43FF655543E30000000000000000000000000000000000000000000000000000
      000000000000000000006555433465554394655543F3655543FF655543FF6555
      43FF655543FF655543FF6D5E4DFF5E5DE9FFD3D3FFFFD3D3FFFFD3D3FFFFD3D3
      FFFFD3D3FFFFD3D3FFFFD3D3FFFFD3D3FFFFD3D3FFFFD3D3FFFFD3D3FFFFD3D3
      FFFFD3D3FFFFD3D3FFFFD3D3FFFFD3D3FFFF6F6FFDFF655543FF655543FF6454
      43895A4C3C050000000000000000000000000000000000000000000000005A4C
      3C06655543C6655543FF655543FFA3A3FFFFD6D6FFFFD6D6FFFFD6D6FFFFD6D6
      FFFFD6D6FFFFD6D6FFFFD6D6FFFFD6D6FFFFD6D6FFFFD6D6FFFFD6D6FFFFD6D6
      FFFFD6D6FFFFD6D6FFFFD6D6FFFFD6D6FFFFD6D6FFFFD6D6FFFFD6D6FFFFD6D6
      FFFFD6D6FFFFD6D6FFFFD6D6FFFFD6D6FFFFD6D6FFFFD6D6FFFFD6D6FFFFD6D6
      FFFFD6D6FFFFD6D6FFFFD6D6FFFFD6D6FFFFD6D6FFFFD6D6FFFFD6D6FFFFD6D6
      FFFFD6D6FFFFD6D6FFFFD6D6FFFFD6D6FFFFD6D6FFFFD6D6FFFFD6D6FFFFD6D6
      FFFFD6D6FFFFD6D6FFFFD6D6FFFFD6D6FFFFD6D6FFFFD6D6FFFFD6D6FFFFD6D6
      FFFFD6D6FFFFD6D6FFFFD6D6FFFFD6D6FFFFD6D6FFFFD6D6FFFFD6D6FFFFD6D6
      FFFFD6D6FFFFD6D6FFFFD6D6FFFFD6D6FFFFD6D6FFFFD6D6FFFFD6D6FFFFD6D6
      FFFFD6D6FFFFD6D6FFFFD6D6FFFFD6D6FFFFD6D6FFFF605FE9FF655543FF6555
      43FF655543840000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000065554354655543A46555
      43FF655543FF655543FF655543FF584B5CFFD6D6FFFFD6D6FFFFD6D6FFFFD6D6
      FFFFD6D6FFFFD6D6FFFFD6D6FFFFD6D6FFFFD6D6FFFFD6D6FFFFD6D6FFFFD6D6
      FFFFD6D6FFFFD6D6FFFFD6D6FFFFD6D6FFFF7171FEFF655543FF655543FF6454
      43895A4C3C050000000000000000000000000000000000000000000000005A4C
      3C06655543C6655543FF655543FFA6A6FFFFDADAFFFFDADAFFFFDADAFFFFDADA
      FFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADA
      FFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADA
      FFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADA
      FFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADA
      FFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADA
      FFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADA
      FFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADA
      FFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADA
      FFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFBFBFFFFF655543FF655543FF6555
      43FF655543240000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000006555
      431465554366655543E2655543FF584B5CFFBFBFFFFFDADAFFFFDADAFFFFDADA
      FFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADA
      FFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFF7675F4FF655543FF655543FF6454
      43895A4C3C050000000000000000000000000000000000000000000000005A4C
      3C06655543C6655543FF655543FFA9A9FFFFDDDDFFFFDDDDFFFFDDDDFFFFDDDD
      FFFFDDDDFFFFDDDDFFFFDDDDFFFFDDDDFFFFDDDDFFFFDDDDFFFFDDDDFFFFDDDD
      FFFFDDDDFFFFDDDDFFFFDDDDFFFFDDDDFFFFDDDDFFFFDDDDFFFFDDDDFFFFDDDD
      FFFFDDDDFFFFDDDDFFFFDDDDFFFFDDDDFFFFDDDDFFFFDDDDFFFFDDDDFFFFDDDD
      FFFFDDDDFFFFDDDDFFFFDDDDFFFFDDDDFFFFDDDDFFFFDDDDFFFFDDDDFFFFDDDD
      FFFFDDDDFFFFDDDDFFFFDDDDFFFFDDDDFFFFDDDDFFFFDDDDFFFFDDDDFFFFDDDD
      FFFFDDDDFFFFDDDDFFFFDDDDFFFFDDDDFFFFDDDDFFFFDDDDFFFFDDDDFFFFDDDD
      FFFFDDDDFFFFDDDDFFFFDDDDFFFFDDDDFFFFDDDDFFFFDDDDFFFFDDDDFFFFDDDD
      FFFFDDDDFFFFDDDDFFFFDDDDFFFFDDDDFFFFDDDDFFFFDDDDFFFFDDDDFFFFDDDD
      FFFFDDDDFFFFDDDDFFFFDDDDFFFFDDDDFFFF7877F4FF655543FF655543FF6555
      43A4000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00005A4C3C02655543C3655543FF655543FFA9A9FEFFDDDDFFFFDDDDFFFFDDDD
      FFFFDDDDFFFFDDDDFFFFDDDDFFFFDDDDFFFFDDDDFFFFDDDDFFFFDDDDFFFFDDDD
      FFFFDDDDFFFFDDDDFFFFDDDDFFFFDDDDFFFF7877F5FF655543FF655543FF6454
      43895A4C3C050000000000000000000000000000000000000000000000005A4C
      3C06655543C6655543FF655543FFACACFFFFE2E2FFFFE2E2FFFFE2E2FFFFE2E2
      FFFFE2E2FFFFE2E2FFFFE2E2FFFFE2E2FFFFE2E2FFFFE2E2FFFFE2E2FFFFE2E2
      FFFFE2E2FFFFE2E2FFFFE2E2FFFFE2E2FFFFE2E2FFFFE2E2FFFFE2E2FFFFE2E2
      FFFFE2E2FFFFE2E2FFFFE2E2FFFFE2E2FFFFE2E2FFFFE2E2FFFFE2E2FFFFE2E2
      FFFFE2E2FFFFE2E2FFFFE2E2FFFFE2E2FFFFE2E2FFFFE2E2FFFFE2E2FFFFE2E2
      FFFFE2E2FFFFE2E2FFFFE2E2FFFFE2E2FFFFE2E2FFFFE2E2FFFFE2E2FFFFE2E2
      FFFFE2E2FFFFE2E2FFFFE2E2FFFFE2E2FFFFE2E2FFFFE2E2FFFFE2E2FFFFE2E2
      FFFFE2E2FFFFE2E2FFFFE2E2FFFFE2E2FFFFE2E2FFFFE2E2FFFFE2E2FFFFE2E2
      FFFFE2E2FFFFE2E2FFFFE2E2FFFFE2E2FFFFE2E2FFFFE2E2FFFFE2E2FFFFE2E2
      FFFFE2E2FFFFE2E2FFFFE2E2FFFFE2E2FFFF615565FF655543FF655543FF6555
      4344000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00005A4C3C01655543B3655543FF655543FF9E9DFEFFE2E2FFFFE2E2FFFFE2E2
      FFFFE2E2FFFFE2E2FFFFE2E2FFFFE2E2FFFFE2E2FFFFE2E2FFFFE2E2FFFFE2E2
      FFFFE2E2FFFFE2E2FFFFE2E2FFFFE2E2FFFF7979F6FF655543FF655543FF6454
      43895A4C3C050000000000000000000000000000000000000000000000005A4C
      3C06655543C6655543FF655543FFAEAEFFFFE4E4FFFFE4E4FFFFE4E4FFFFE4E4
      FFFFE4E4FFFFE4E4FFFFE4E4FFFFE4E4FFFFE4E4FFFFE4E4FFFFE4E4FFFFE4E4
      FFFFE4E4FFFFE4E4FFFFE4E4FFFFE4E4FFFFE4E4FFFFE4E4FFFFE4E4FFFFE4E4
      FFFFE4E4FFFFE4E4FFFFE4E4FFFFE4E4FFFFE4E4FFFFE4E4FFFFE4E4FFFFE4E4
      FFFFE4E4FFFFE4E4FFFFE4E4FFFFE4E4FFFFE4E4FFFFE4E4FFFFE4E4FFFFE4E4
      FFFFE4E4FFFFE4E4FFFFE4E4FFFFE4E4FFFFE4E4FFFFE4E4FFFFE4E4FFFFE4E4
      FFFFE4E4FFFFE4E4FFFFE4E4FFFFE4E4FFFFE4E4FFFFE4E4FFFFE4E4FFFFE4E4
      FFFFE4E4FFFFE4E4FFFFE4E4FFFFE4E4FFFFC9C9FFFF9393FDFFBBBBFFFFE4E4
      FFFFE4E4FFFFE4E4FFFFE4E4FFFFE4E4FFFFE4E4FFFFE4E4FFFFE4E4FFFFE4E4
      FFFFE4E4FFFFE4E4FFFFE4E4FFFF9393FDFF655543FF655543FF655543D30000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000065554384655543FF655543FF7B7AF6FFE4E4FFFFE4E4FFFFE4E4
      FFFFE4E4FFFFE4E4FFFFE4E4FFFFE4E4FFFFE4E4FFFFE4E4FFFFE4E4FFFFE4E4
      FFFFE4E4FFFFE4E4FFFFE4E4FFFFE4E4FFFF7B7AF6FF655543FF655543FF6454
      43895A4C3C050000000000000000000000000000000000000000000000005A4C
      3C06655543C6655543FF655543FFB0B0FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7
      FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7
      FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7
      FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7
      FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7
      FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7
      FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7
      FFFFE7E7FFFFB0B0FFFF7170F0FF60599FFF655543FF655543FF655543FF6256
      66FF6A68E3FF9696FDFFDADAFFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7
      FFFFE7E7FFFFE7E7FFFFE7E7FFFF60599FFF655543FF655543FF655543640000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000065554383655543FF655543FF7C7BF7FFE7E7FFFFE7E7FFFFE7E7
      FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7
      FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7FFFF7C7BF7FF655543FF655543FF6454
      43895A4C3C050000000000000000000000000000000000000000000000005A4C
      3C06655543C6655543FF655543FFB3B3FFFFEAEAFFFFEAEAFFFFEAEAFFFFEAEA
      FFFFEAEAFFFFEAEAFFFFEAEAFFFFEAEAFFFFEAEAFFFFEAEAFFFFEAEAFFFFEAEA
      FFFFEAEAFFFFEAEAFFFFEAEAFFFFEAEAFFFFEAEAFFFFEAEAFFFFEAEAFFFFEAEA
      FFFFEAEAFFFFEAEAFFFFEAEAFFFFEAEAFFFFEAEAFFFFEAEAFFFFEAEAFFFFEAEA
      FFFFEAEAFFFFEAEAFFFFEAEAFFFFEAEAFFFFEAEAFFFFEAEAFFFFEAEAFFFFEAEA
      FFFFEAEAFFFFEAEAFFFFEAEAFFFFEAEAFFFFEAEAFFFFEAEAFFFFEAEAFFFFEAEA
      FFFFEAEAFFFFEAEAFFFFEAEAFFFFEAEAFFFFEAEAFFFFCECEFFFF9797FDFF6562
      D0FF625666FF655543FF655543FF655543FF655543FF655543FF655543FF6555
      43FF655543FF655543FF655543FF60599FFF7E7EF6FFBFBFFFFFEAEAFFFFEAEA
      FFFFEAEAFFFFCECEFFFF8A8AFBFF655543FF655543FF655543F3000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000064554348655543FF655543FF5D59C3FFEAEAFFFFEAEAFFFFEAEA
      FFFFEAEAFFFFEAEAFFFFEAEAFFFFEAEAFFFFEAEAFFFFEAEAFFFFEAEAFFFFEAEA
      FFFFEAEAFFFFEAEAFFFFEAEAFFFFEAEAFFFF7E7DF7FF655543FF655543FF6454
      43895A4C3C050000000000000000000000000000000000000000000000005A4C
      3C06655543C6655543FF655543FFB4B4FFFFEDEDFFFFEDEDFFFFEDEDFFFFEDED
      FFFFEDEDFFFFEDEDFFFFEDEDFFFFEDEDFFFFEDEDFFFFEDEDFFFFEDEDFFFFEDED
      FFFFEDEDFFFFEDEDFFFFEDEDFFFFEDEDFFFFEDEDFFFFEDEDFFFFEDEDFFFFEDED
      FFFFEDEDFFFFEDEDFFFFEDEDFFFFEDEDFFFFEDEDFFFFEDEDFFFFEDEDFFFFEDED
      FFFFEDEDFFFFEDEDFFFFEDEDFFFFEDEDFFFFEDEDFFFFEDEDFFFFEDEDFFFFEDED
      FFFFEDEDFFFFEDEDFFFFEDEDFFFFEDEDFFFFEDEDFFFFEDEDFFFFEDEDFFFFEDED
      FFFFEDEDFFFFEDEDFFFFB4B4FFFF7473F1FF605AA3FF655543FF655543FF6555
      43FF655543FF655543FF655543FF655543FF655543FF645442E6645442E46555
      43FF655543FF655543FF655543FF655543FF655543FF655543FF625666FF625D
      BBFF5F5688FF655543FF655543FF655543FF655543FF6555435C000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000064544344655543FF655543FF5E5AC4FFEDEDFFFFEDEDFFFFEDED
      FFFFEDEDFFFFEDEDFFFFEDEDFFFFEDEDFFFFEDEDFFFFEDEDFFFFEDEDFFFFEDED
      FFFFEDEDFFFFEDEDFFFFEDEDFFFFEDEDFFFF7E7EF8FF655543FF655543FF6454
      43895A4C3C050000000000000000000000000000000000000000000000005A4C
      3C06655543C6655543FF655543FFB7B7FFFFF0F0FFFFF0F0FFFFF0F0FFFFF0F0
      FFFFF0F0FFFFF0F0FFFFF0F0FFFFF0F0FFFFF0F0FFFFF0F0FFFFF0F0FFFFF0F0
      FFFFF0F0FFFFF0F0FFFFF0F0FFFFF0F0FFFFF0F0FFFFF0F0FFFFF0F0FFFFF0F0
      FFFFF0F0FFFFF0F0FFFFF0F0FFFFF0F0FFFFF0F0FFFFF0F0FFFFF0F0FFFFF0F0
      FFFFF0F0FFFFF0F0FFFFF0F0FFFFF0F0FFFFF0F0FFFFF0F0FFFFF0F0FFFFF0F0
      FFFFF0F0FFFFF0F0FFFFF0F0FFFFF0F0FFFFF0F0FFFFF0F0FFFFD3D3FFFF9A9A
      FDFF6562D4FF625666FF655543FF655543FF655543FF655543FF655543FF6D5F
      4FFF807364FF655543FF655543FF655543FF655543FF655543FF655543FF6555
      43FF655543F7645442CA655543F6655543FF655543FF655543FF655543FF6555
      43FF655543FF655543FF655543FF655543FF655543BC00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000005F503F09655543FF655543FF584B5CFFF0F0FFFFF0F0FFFFF0F0
      FFFFF0F0FFFFF0F0FFFFF0F0FFFFF0F0FFFFF0F0FFFFF0F0FFFFF0F0FFFFF0F0
      FFFFF0F0FFFFF0F0FFFFF0F0FFFFF0F0FFFF8080F8FF655543FF655543FF6454
      43895A4C3C050000000000000000000000000000000000000000000000005A4C
      3C06655543C6655543FF655543FFB8B8FFFFF3F3FFFFF3F3FFFFF3F3FFFFF3F3
      FFFFF3F3FFFFF3F3FFFFF3F3FFFFF3F3FFFFF3F3FFFFF3F3FFFFF3F3FFFFF3F3
      FFFFF3F3FFFFF3F3FFFFF3F3FFFFF3F3FFFFF3F3FFFFF3F3FFFFF3F3FFFFF3F3
      FFFFF3F3FFFFF3F3FFFFF3F3FFFFF3F3FFFFF3F3FFFFF3F3FFFFF3F3FFFFF3F3
      FFFFF3F3FFFFF3F3FFFFF3F3FFFFF3F3FFFFF3F3FFFFF3F3FFFFF3F3FFFFF3F3
      FFFFF3F3FFFFF3F3FFFFF3F3FFFFB8B8FFFF7675F2FF605AA3FF655543FF6555
      43FF655543FF655543FF655543FF655543FF655543FF655543FF67583CFFF1EE
      5EFFFFFEE2FFFFFD9EFFFFFC1DFFC0BE00FF6E5F4EFF655543FF655543FF6555
      43FF655543FF655543FF635342AF60514076635442AC655543DB655543FF6555
      43FF655543FF655543E4655543A5655543540000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000005A4C3C03655543FF655543FF584B5CFFF3F3FFFFF3F3FFFFF3F3
      FFFFF3F3FFFFF3F3FFFFF3F3FFFFF3F3FFFFF3F3FFFFF3F3FFFFF3F3FFFFF3F3
      FFFFF3F3FFFFF3F3FFFFF3F3FFFFF3F3FFFF8181F9FF655543FF655543FF6454
      43895A4C3C050000000000000000000000000000000000000000000000005A4C
      3C06655543C6655543FF655543FFBABAFFFFF4F4FFFFF4F4FFFFF4F4FFFFF4F4
      FFFFF4F4FFFFF4F4FFFFF4F4FFFFF4F4FFFFF4F4FFFFF4F4FFFFF4F4FFFFF4F4
      FFFFF4F4FFFFF4F4FFFFF4F4FFFFF4F4FFFFF4F4FFFFF4F4FFFFF4F4FFFFF4F4
      FFFFF4F4FFFFF4F4FFFFF4F4FFFFF4F4FFFFF4F4FFFFF4F4FFFFF4F4FFFFF4F4
      FFFFF4F4FFFFF4F4FFFFF4F4FFFFF4F4FFFFF4F4FFFFF4F4FFFFF4F4FFFFD8D8
      FFFF9E9DFEFF6663D5FF625666FF655543FF655543FF655543FF655543FF6555
      43FF655543FF655543FF645443E0645442E6655543FF655543FFB2AD27FFFFFE
      E6FFFFFEE6FFFFFEC5FFFFFEC5FFFFFEE6FFFFFEE6FFFFFC61FFDEDC00FF8073
      65FF655543FF655543FF655543FF635442BA5A4C3C455A4C3C3A5D4E3E3A6253
      4158615240335A4C3C0C5A4C3C055A4C3C020000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000005A4C3C02655543D2655543FF584B5CFFC8C8FFFFF4F4FFFFF4F4
      FFFFF4F4FFFFF4F4FFFFF4F4FFFFF4F4FFFFF4F4FFFFF4F4FFFFF4F4FFFFF4F4
      FFFFF4F4FFFFF4F4FFFFF4F4FFFFF4F4FFFF8181F9FF655543FF655543FF6454
      43895A4C3C050000000000000000000000000000000000000000000000005A4C
      3C06655543C5655543FF655543FFBBBBFFFFF7F7FFFFF7F7FFFFF7F7FFFFF7F7
      FFFFF7F7FFFFF7F7FFFFF7F7FFFFF7F7FFFFF7F7FFFFF7F7FFFFF7F7FFFFF7F7
      FFFFF7F7FFFFF7F7FFFFF7F7FFFFF7F7FFFFF7F7FFFFF7F7FFFFF7F7FFFFF7F7
      FFFFF7F7FFFFF7F7FFFFF7F7FFFFF7F7FFFFF7F7FFFFF7F7FFFFF7F7FFFFF7F7
      FFFFF7F7FFFFF7F7FFFFF7F7FFFFF7F7FFFFBBBBFFFF7776F3FF615BA4FF6555
      43FF655543FF655543FF655543FF655543FF655543FF655543FF655543F36555
      43AA6353427A5D4E3E4F6151409B655543FF655543FF756A37FFFFFDA8FFFFFE
      ECFFFFFEECFFFFFEECFFD1CF00FFA3A100FFE0DD00FFFFFD64FFFFFEECFFFFFE
      ECFFD1CF00FF655543FF655543FF655543FF645442DC5A4C3C455A4C3C3B5A4C
      3C2F5A4C3C225A4C3C175A4C3C0D5A4C3C065A4C3C035A4C3C01000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00005A4C3C015A4C3C04655543C3655543FF655543FFBBBBFFFFF7F7FFFFF7F7
      FFFFF7F7FFFFF7F7FFFFF7F7FFFFF7F7FFFFF7F7FFFFF7F7FFFFF7F7FFFFF7F7
      FFFFF7F7FFFFF7F7FFFFF7F7FFFFF7F7FFFF8281F9FF655543FF655543FF6454
      43885A4C3C040000000000000000000000000000000000000000000000005A4C
      3C05655543C5655543FF655543FFBDBDFFFFF8F8FFFFF8F8FFFFF8F8FFFFF8F8
      FFFFF8F8FFFFF8F8FFFFF8F8FFFFF8F8FFFFF8F8FFFFF8F8FFFFF8F8FFFFF8F8
      FFFFF8F8FFFFF8F8FFFFF8F8FFFFF8F8FFFFF8F8FFFFF8F8FFFFF8F8FFFFF8F8
      FFFFF8F8FFFFF8F8FFFFF8F8FFFFF8F8FFFFF8F8FFFFF8F8FFFFF8F8FFFFF8F8
      FFFFDCDCFFFFA09FFEFF6663D7FF625666FF655543FF655543FF655543FF6555
      43FF655543FF655543FF655543FF655543D365554384645443385A4C3C0D5A4C
      3C1D5A4C3C355E4F3F72655543FD655543FF665640FFF0EC23FFFFFEEFFFFFFE
      EFFFFFFEEFFFFFFEEFFFFFFEEFFFD3CE48FF655543FF655543FF858300FFFFFD
      8BFFFFFEEFFFE0DD00FF655543FF655543FF655543FF645442DC5F503F655A4C
      3C3C5A4C3C315A4C3C245A4C3C175A4C3C0E5A4C3C075A4C3C035A4C3C010000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000005A4C3C015A4C3C015A4C3C035A4C3C045A4C3C055A4C
      3C085A4C3C0B5A4C3C0E645543A8655543FF655543FFA09FFEFFF8F8FFFFF8F8
      FFFFF8F8FFFFF8F8FFFFF8F8FFFFF8F8FFFFF8F8FFFFF8F8FFFFF8F8FFFFF8F8
      FFFFF8F8FFFFF8F8FFFFF8F8FFFFF8F8FFFF8483F9FF655543FF655543FF6454
      43885A4C3C040000000000000000000000000000000000000000000000005A4C
      3C05655543C4655543FF655543FFBEBEFFFFFAFAFFFFFAFAFFFFFAFAFFFFFAFA
      FFFFFAFAFFFFFAFAFFFFFAFAFFFFFAFAFFFFFAFAFFFFFAFAFFFFFAFAFFFFFAFA
      FFFFFAFAFFFFFAFAFFFFFAFAFFFFFAFAFFFFFAFAFFFFFAFAFFFFFAFAFFFFFAFA
      FFFFFAFAFFFFFAFAFFFFFAFAFFFFFAFAFFFFFAFAFFFFBEBEFFFF7877F4FF5F59
      A8FF655543FF655543FF655543FF655543FF655543FF655543FF655543FF6555
      43F3655543A46555436465554314000000005A4C3C035A4C3C095A4C3C165A4C
      3C2B5A4C3C49635342C6655543FF655543FF938C30FFFFFEF3FFFFFEF3FFFFFE
      F3FFFFFEF3FFFFFEF3FFFFFEF3FFFFFEF3FFE8E45DFF655542FF655543FF8583
      00FFFFFC27FFFFFEF3FFFFFC27FF655543FF655543FF655543FF655543FD6051
      40725A4C3C3D5A4C3C325A4C3C255A4C3C195A4C3C0F5A4C3C075A4C3C035A4C
      3C0100000000000000000000000000000000000000005A4C3C015A4C3C025A4C
      3C035A4C3C055A4C3C065A4C3C095A4C3C0C5A4C3C0E5A4C3C115A4C3C155A4C
      3C185A4C3C1D5A4C3C2264544294655543FF655543FF8483F9FFFAFAFFFFFAFA
      FFFFFAFAFFFFFAFAFFFFFAFAFFFFFAFAFFFFFAFAFFFFFAFAFFFFFAFAFFFFFAFA
      FFFFFAFAFFFFFAFAFFFFFAFAFFFFFAFAFFFF8483F9FF655543FF655543FF6455
      43875A4C3C040000000000000000000000000000000000000000000000005A4C
      3C04655543C3655543FF655543FFBFBFFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFB
      FFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFB
      FFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFB
      FFFFFBFBFFFFDFDFFFFFA3A2FEFF6764D8FF60546AFF655543FF655543FF6555
      43FF655543FF655543FF655543FF655543FF655543D365554384655543340000
      00000000000000000000000000005A4C3C015A4C3C055A4C3C105A4C3C225A4C
      3C3D6051408F655543FF655543FF685A37FFFEFC90FFFFFEF6FFFFFEF6FFFFFE
      F6FFFFFEF6FFFFFEF6FFFFFEF6FFFFFEF6FFFFFEF6FFECE858FF655641FF6555
      43FF757300FFFFFC2AFFFFFEF6FFFFFC2AFF655543FF655543FF655543FF6555
      43FD605140725A4C3C3E5A4C3C335A4C3C275A4C3C1A5A4C3C105A4C3C095A4C
      3C055A4C3C035A4C3C035A4C3C045A4C3C055A4C3C075A4C3C0A5A4C3C0C5A4C
      3C0F5A4C3C125A4C3C175A4C3C1A5A4C3C1F5A4C3C235A4C3C285A4C3C2C5A4C
      3C325A4C3C365A4C3C3C62534195655543FF655543FF7877F5FFFBFBFFFFFBFB
      FFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFB
      FFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFF8484FAFF655543FF655543FF6555
      43865A4C3C030000000000000000000000000000000000000000000000005A4C
      3C03655543C3655543FF655543FFBFBFFFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFD
      FFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFD
      FFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFBFBFFFFF7978
      F4FF605AA9FF655543FF655543FF655543FF655543FF655543FF655543FF6555
      43FF655543F3655543A465554364655543140000000000000000000000000000
      00000000000000000000000000005A4C3C045A4C3C0C5A4C3C1A5A4C3C335B4D
      3D59645543F0655543FF655543FFD5D123FFFFFEF9FFFFFEF9FFFFFEF9FFFFFE
      F9FFFFFEF9FFFFFEF9FFFFFEF9FFFFFEF9FFFFFEF9FFFFFEF9FFEFEB50FF6656
      41FF655543FF655543FFEFEC06FFFFFEF9FFFFFD91FF6F604FFF655543FF6555
      43FF655543FF6152418A5A4C3C3F5A4C3C345A4C3C295A4C3C1E5A4C3C155A4C
      3C105A4C3C0E5A4C3C0E5A4C3C105A4C3C145A4C3C175A4C3C1C5A4C3C205A4C
      3C245A4C3C295A4C3C2E5A4C3C335A4C3C385A4C3C3C5A4C3C425A4C3C465A4C
      3C4C5A4C3C515A4C3C565F503F84655543FF655543FF5E5BCAFFFDFDFFFFFDFD
      FFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFD
      FFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFF8484FAFF655543FF655543FF6555
      43845A4C3C020000000000000000000000000000000000000000000000005A4C
      3C02655543C1655543FF655543FFC1C1FFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFE
      FFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFE
      FFFFFEFEFFFFFEFEFFFFE0E0FFFF9494FDFF6764DBFF655543FF655543FF6555
      43FF655543FF655543FF655543FF655543FF655543FF655543D76454438E6353
      42415A4C3C0C5A4C3C085A4C3C055A4C3C045A4C3C025A4C3C01000000000000
      000000000000000000005A4C3C025A4C3C085A4C3C145A4C3C295A4C3C456252
      41B1655543FF655543FF837A37FFFFFEDCFFFFFEFCFFFFFEFCFFFFFEFCFFFFFE
      FCFFFFFEFCFFFFFEFCFFFFFEFCFFFFFEFCFFFFFEFCFFFFFEFCFFFFFEFCFFF4F0
      6CFF70624AFF655543FF655543FFD4D200FFFFFEFCFFFFFD95FF6F604FFF6555
      43FF655543FF655543FF6152418B5A4C3C415A4C3C385A4C3C2F5A4C3C285A4C
      3C225A4C3C215A4C3C235A4C3C265A4C3C2A5A4C3C305A4C3C345A4C3C395A4C
      3C3E5A4C3C435A4C3C485A4C3C4D5A4C3C525A4C3C575A4C3C5C5A4C3C615A4C
      3C645A4C3C685A4C3C6B5D4E3E7F655543FF655543FF585197FFA3A3FEFFF0F0
      FFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFE
      FFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFF8686FAFF655543FF655543FF6555
      43825A4C3C010000000000000000000000000000000000000000000000005A4C
      3C01655543C1655543FF655543FFC1C1FFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFE
      FFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFB3B3
      FFFF7978F4FF605789FF655543FF655543FF655543FF655543FF655543FF6555
      43FF655543FF655543F6645442BB625341875D4F3E445A4C3C2F5A4C3C295A4C
      3C235A4C3C1E5A4C3C185A4C3C145A4C3C0F5A4C3C0C5A4C3C085A4C3C055A4C
      3C045A4C3C025A4C3C025A4C3C055A4C3C0E5A4C3C205A4C3C3A5F503F826555
      43FF655543FF66573EFFFCF859FFFFFEFCFFFFFEFCFFFFFEFCFFFFFEFCFFFFFE
      FCFFFFFEFCFFFFFEFCFFFFFEFCFFFFFEFCFFFFFEFCFFFFFEFCFFFFFEFCFFFFFE
      FCFFF9F7A3FF716348FF655543FF655543FF958A7EFFFFFEDCFFFFFEB8FF8275
      66FF655543FF655543FF655543FF635342AD5A4C3C455A4C3C405A4C3C3C5A4C
      3C3A5A4C3C3A5A4C3C3C5A4C3C405A4C3C455A4C3C495A4C3C4E5A4C3C535A4C
      3C575A4C3C5C5A4C3C615A4C3C655A4C3C685A4C3C6A5A4C3C6D5A4C3C6F5A4C
      3C6F5A4C3C725F503F94605140A5645543EF655543FF655543FF584B5CFF584B
      5CFF5B56B3FF8686FAFFD1D1FFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFE
      FFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFF8686FAFF655543FF655543FF6555
      4381000000000000000000000000000000000000000000000000000000000000
      0000655543C0655543FF655543FFC2C2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFE2E2FFFF9494FDFF6764DBFF655543FF6555
      43FF655543FF655543FF655543FF655543FF655543FF655543FF645442DA6253
      41B25F503F7D5A4C3C585A4C3C555A4C3C4F5A4C3C4A5A4C3C445A4C3C3E5A4C
      3C385A4C3C335A4C3C2D5A4C3C285A4C3C225A4C3C1D5A4C3C185A4C3C145A4C
      3C0F5A4C3C0C5A4C3C0C5A4C3C105A4C3C1C5A4C3C315A4C3C4E645442E76555
      43FF655543FFB2AD2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFBF9A2FF716447FF655543FF655543FF827566FFFFFEDFFFFFFE
      DFFF827566FF655543FF655543FF655543FF635342AF5A4C3C4E5A4C3C4F5A4C
      3C525A4C3C565A4C3C595A4C3C5C5A4C3C605A4C3C635A4C3C665A4C3C685A4C
      3C6A5A4C3C6C5A4C3C6D5A4C3C6E5B4D3D775F503F94625241B6635342C76454
      42E3655543FF655543FF655543FF655543FF655543FF655543FF655543FF6555
      43FF655543FF584B5CFF584B5CFF57509BFFA4A4FEFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8686FAFF655543FF655543FF6555
      4380000000000000000000000000000000000000000000000000000000000000
      0000655543C0655543FF655543FFC2C2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFD2D2FFFF7A79F5FF605789FF655543FF655543FF6555
      43FF655543FF655543FF655543FF655543FF655543FF655543EC635442C36152
      419B5D4F3E755A4C3C5F5A4C3C5E5A4C3C5D5A4C3C5A5A4C3C575A4C3C515A4C
      3C4D5A4C3C465A4C3C415A4C3C3C5A4C3C365A4C3C325A4C3C2C5A4C3C275A4C
      3C225A4C3C1E5A4C3C1E5A4C3C235A4C3C315A4C3C48615241A8655543FF6555
      43FF75693CFFFFFEBCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFCFAA0FF726545FF655543FF655543FF827566FFFFFE
      BCFFFFFEDFFF9F958AFF655543FF655543FF655543FF635442C95A4C3C615A4C
      3C685A4C3C6F5A4C3C735A4C3C745A4C3C735A4C3C725A4C3C6F5D4E3E7F6051
      409A625241B7645442DA645543EC655543FF655543FF655543FF655543FF6555
      43FF655543FF655543FF655543FF655543FF584B5CFF584B5CFF57509BFF6562
      E0FF8686FAFFC2C2FFFFF1F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8686FAFF655543FF655543FF6555
      4380000000000000000000000000000000000000000000000000000000000000
      000065554384655543FF655543FF605AA9FF8484FFFFC2C2FFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE2E2FFFFA4A4FEFF6F6D
      EDFF605789FF655543FF655543FF655543FF655543FF655543FF655543FF6555
      43FF655543FF655543EC635442C36152419A5D4F3E725A4C3C5C5A4C3C5B5A4C
      3C595A4C3C575A4C3C535A4C3C4F5A4C3C4A5A4C3C455A4C3C405A4C3C3B5A4C
      3C365A4C3C335A4C3C335A4C3C3B5A4C3C4B5E4F3E7D655543FF655543FF6556
      41FFF1ED2CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFCC0FF887D54FF655543FF655543FF8684
      00FFFFFD98FFFFFFFFFF9F958AFF655543FF655543FF655543FF645442E05B4D
      3D775A4C3C7C5E503F9F605140AE625341C5645442DE655543FF655543FF6555
      43FF655543FF655543FF655543FF655543FF655543FF655543FF655543FF584B
      5CFF584B5CFF5F5CCBFF6D6CF0FF9494FDFFC2C2FFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFC2C2FFFFA4A4FEFF7979F6FF57509BFF655543FF655543FF6555
      4344000000000000000000000000000000000000000000000000000000000000
      00006555431B655543F3655543FF655543FF655543FF655543FF655543FF635F
      C3FF8786F9FFB3B3FFFFF1F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFE2E2FFFFA4A4FEFF6F6DEDFF605789FF655543FF655543FF6555
      43FF655543FF655543FF655543FF655543FF655543FF655543EC635442C26151
      408E5C4E3D645A4C3C585A4C3C585A4C3C575A4C3C555A4C3C525A4C3C4E5A4C
      3C4A5A4C3C495A4C3C4C5A4C3C565A4C3C67635342D5655543FF655543FF9F98
      3CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFDE0FF897F50FF655543FF6555
      43FF6F604FFFFFFD74FFFFFFFFFFF0ED07FF655543FF655543FF655543FF6454
      43EB655543FF655543FF655543FF655543FF655543FF655543FF655543FF6555
      43FF655543FF584B5CFF584B5CFF574D7EFF5F5CCBFF8686FAFFA4A4FEFFD2D2
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFE2E2FFFFB3B3FFFF8686FAFF6562E0FF5750
      9BFF584B5CFF584B5CFF655543FF655543FF655543FF655543FF655543E30000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000006555435C655543F3655543FF655543FF655543FF655543FF6555
      43FF655543FF655543FF655543FF605AA9FF7A79F4FFB3B3FFFFE0E0FFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE0E0FFFFA3A3FEFF6F6E
      ECFF605789FF655543FF655543FF655543FF655543FF655543FF655543FF6555
      43FF655543FF645443E0635342B66151408C5C4E3D635A4C3C575A4C3C575A4C
      3C595A4C3C5D5A4C3C655A4C3C735F503FA9655543FF655543FF67593AFFFEFC
      9BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEE0FF8A804DFF6555
      43FF655543FF757300FFFFFC2DFFFFFFFFFFFFFC2DFF655543FF655543FF6555
      43FF655543FF655543FF655543FF655543FF584B5CFF584B5CFF585197FF6562
      E0FF8686FAFFC2C2FFFFE0E0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF1F1FFFFC2C2FFFF9494
      FDFF7A79F5FF5F5CCBFF584B5CFF584B5CFF584B5CFF655543FF655543FF6555
      43FF655543FF655543FF655543FF655543FF655543FF655543E36555434C0000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000006555434465554384655543C3655543FF6555
      43FF655543FF655543FF655543FF655543FF655543FF655543FF655543FF6057
      89FF6F6EECFFA3A3FEFFD1D1FFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFE
      FFFFFEFEFFFFE0E0FFFFA3A3FEFF6F6EECFF605789FF655543FF655543FF6555
      43FF655543FF655543FF655543FF655543FF655543FF645443E0635342B66051
      40905C4E3D725A4C3C755C4E3D95645543F8655543FF655543FFDED934FFFFFE
      FCFFFFFEFCFFFFFEFCFFFFFEFCFFFFFEFCFFFFFEFCFFFFFEFCFFFFFEFCFFFFFE
      FCFFFFFEFCFFFFFEFCFFFFFEFCFFFFFEFCFFFFFEFCFFFFFEFCFFFFFEFCFFFFFE
      FCFFFFFEFCFFFFFEFCFFFFFEFCFFFFFEFCFFFFFEFCFFFFFEFCFFFFFEDDFF8C82
      48FF655543FF655543FF655543FFFFFC2DFFFFFEFCFFFFFC51FF584E7CFF584B
      5CFF584B5CFF5E5BCAFF6E6CEFFF9494FDFFC1C1FFFFFEFEFFFFFEFEFFFFFEFE
      FFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFE
      FFFFD1D1FFFFB3B3FFFF8686FAFF5E5BCAFF585197FF584B5CFF584B5CFF6555
      43FF655543FF655543FF655543FF655543FF655543FF655543FF655543FF6555
      43FF655543FF655543C3655543A0655543706555433400000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000655543046555
      434465554380655543B4655543F3655543FF655543FF655543FF655543FF6555
      43FF655543FF655543FF655543FF61556AFF6764D8FF9393FDFFCFCFFFFFFDFD
      FFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFDFDFFFFF9393FDFF6764
      D8FF61556AFF655543FF655543FF655543FF655543FF655543FF655543FF6555
      43FF655543FF645442E8625241D3655543FF655543FF81773CFFFFFED9FFFFFE
      F9FFFFFEF9FFFFFEF9FFFFFEF9FFFFFEF9FFFFFEF9FFFFFEF9FFFFFEF9FFFFFE
      F9FFFFFEF9FFFFFEF9FFFFFEF9FFFFFEF9FFFFFEF9FFFFFEF9FFFFFEF9FFFFFE
      F9FFFFFEF9FFFFFEF9FFFFFEF9FFFFFEF9FFFFFEF9FFFFFEF9FFFFFEF9FFFFFE
      DAFFB1A959FF655543FF655543FF757300FFD3D000FFFFFEF9FFFFFEB5FFCFCF
      FFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFD
      FFFFFDFDFFFFFDFDFFFFEEEEFFFFBFBFFFFF8484FAFF6C6BEFFF5E5BCAFF584B
      5CFF584B5CFF655543FF655543FF655543FF655543FF655543FF655543FF6555
      43FF655543FF655543FF655543FF655543E0655543B065554380655543506555
      4320000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000006555433465554374655543B0655543E36555
      43FF655543FF655543FF655543FF655543FF655543FF655543FF655543FF6555
      43FF635FC0FF8584F9FFBFBFFFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFD
      FFFFFDFDFFFFCFCFFFFF9393FCFF6764D8FF61556AFF655543FF655543FF6555
      43FF655543FF655543FF655543FF655543FF665640FFFBF978FFFFFEF9FFFFFE
      F9FFFFFEF9FFFFFEF9FFFFFEF9FFFFFEF9FFFFFEF9FFFFFEF9FFFFFEF9FFFFFE
      F9FFFFFEF9FFFFFEF9FFFFFEF9FFFFFEF9FFFFFEF9FFFFFEF9FFFFFEF9FFFFFE
      F9FFFFFEF9FFFFFEF9FFFFFEF9FFFFFEF9FFFFFEF9FFFFFEF9FFFFFEF9FFFFFE
      F9FFFFFEF9FFB3AC52FF655543FF655543FF757300FFD3D000FFFFFEF9FFFFFE
      F9FFFFFEF9FFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFCFCFFFFFA3A2FEFF8484
      FAFF5F5CC8FF574D7EFF584B5CFF584B5CFF655543FF655543FF655543FF6555
      43FF655543FF655543FF655543FF655543FF655543FF655543F0655543C06555
      4390655543706555434065554304000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000006555
      432465554364655543A0655543D3655543FF655543FF655543FF655543FF6555
      43FF655543FF655543FF655543FF655543FF635FC0FF7877F4FFB0B0FFFFEDED
      FFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFFBFBFFFFCECEFFFF9191FCFF6764
      D8FF60546AFF655543FF655543FF655543FFAEA836FFFFFEF6FFFFFEF6FFFFFE
      F6FFFFFEF6FFFFFEF6FFFFFEF6FFFFFEF6FFFFFEF6FFFFFEF6FFFFFEF6FFFFFE
      F6FFFFFEF6FFFFFEF6FFFFFEF6FFFFFEF6FFFFFEF6FFFFFEF6FFFFFEF6FFFFFE
      F6FFFFFEF6FFFFFEF6FFFFFEF6FFFFFEF6FFFFFEF6FFFFFEF6FFFFFEF6FFFFFE
      F6FFFFFEF6FFFFFEF6FFB6AF4BFF655543FF655543FF757300FFA3A100FFFFFE
      D5FFFFFEF6FFFFFC4AFF6966D4FF5A55B2FF584B5CFF584B5CFF655543FF6555
      43FF655543FF655543FF655543FF655543FF655543FF655543FF655543FF6555
      43FF655543D0655543B065554380655543446555432000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000655543146555435465554394655543D06555
      43FF655543FF655543FF655543FF655543FF655543FF655543FF655543FF6555
      43FF615BA4FF6C6BECFFA1A0FEFFDCDCFFFFF8F8FFFFF8F8FFFFF8F8FFFFF8F8
      FFFFF8F8FFFFCCCCFFFF9090FCFF868092FFFFFECFFFFFFEEFFFFFFEEFFFFFFE
      EFFFFFFEEFFFFFFEEFFFFFFEEFFFFFFEEFFFFFFEEFFFFFFEEFFFFFFEEFFFFFFE
      EFFFFFFEEFFFFFFEEFFFFFFEEFFFFFFEEFFFFFFEEFFFFFFEEFFFFFFEEFFFFFFE
      EFFFFFFEEFFFFFFEEFFFFFFEEFFFFFFEEFFFFFFEEFFFFFFEEFFFFFFEEFFFFFFE
      EFFFFFFEEFFFFFFEEFFFFFFEEFFFB8B242FF655543FF655543FF757300FFA3A1
      00FFFFFECFFFFFFECFFFA3A100FF655543FF655543FF655543FF655543FF6555
      43FF655543FF655543FF655543F2655543C56555438965554363655543416555
      4304000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000006555
      43046555434465554384655543C3655543FF655543FF655543FF655543FF6555
      43FF655543FF655543FF655543FF655543FF605789FF6C6BECFF9090FCFFCBCB
      FFFFF7F7FFFFF7F7FFFFF7F7FFFFFEFDEFFFFFFEECFFFFFEECFFFFFEECFFFFFE
      ECFFFFFEECFFFFFEECFFFFFEECFFFFFEECFFFFFEECFFFFFEECFFFFFEECFFFFFE
      ECFFFFFEECFFFFFEECFFFFFEECFFFFFEECFFFFFEECFFFFFEECFFFFFEECFFFFFE
      ECFFFFFEECFFFFFEECFFFFFEECFFFFFEECFFFFFEECFFFFFEECFFFFFEECFFFFFE
      ECFFFFFEECFFFFFEECFFFFFEECFFFFFEECFFDDD951FF655543FF655543FF6555
      43FF858300FFFFFD88FFFFFECCFFB2AF00FF655543FF655543FF655543FF6354
      42B6615140715D4E3E435A4C3C2B5A4C3C205A4C3C155A4C3C0C5A4C3C055A4C
      3C02000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000655543046555434065554374655543B46555
      43F3655543FF655543FF655543FF655543FF655543FF655543FF655543FF6555
      43FF625666FF6663D5FFC5C2C7FFFFFEE9FFFFFEE9FFFFFEE9FFFFFEE9FFFFFE
      E9FFFFFEE9FFFFFEE9FFFFFEE9FFFFFEE9FFFFFEE9FFFFFEE9FFFFFEE9FFFFFE
      E9FFFFFEE9FFFFFEE9FFFFFEE9FFFFFEE9FFFFFEE9FFFFFEE9FFFFFEE9FFFFFE
      E9FFFFFEE9FFFFFEE9FFFFFEE9FFFFFEE9FFFFFEE9FFFFFEE9FFFFFEE9FFFFFE
      E9FFFFFEE9FFFFFEE9FFFFFEE9FFFFFEE9FFFFFEE9FFE8E456FF655542FF6555
      43FF655543FF6E5F4EFFFFFD85FFFFFEE9FFD0CD00FF655543FF655543FF6555
      43FF645443DA5A4C3C3F5A4C3C365A4C3C2C5A4C3C205A4C3C165A4C3C0D5A4C
      3C065A4C3C030000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00006555433065554365655543A6655543E5655543FF655543FF655543FF6555
      43FF655543FF67583CFFFDFB83FFFFFEE2FFFFFEE2FFFFFEE2FFFFFEE2FFFFFE
      E2FFFFFEE2FFFFFEE2FFFFFEE2FFFFFEE2FFFFFEE2FFFFFEE2FFFFFEE2FFFFFE
      E2FFFFFEE2FFFFFEE2FFFFFEE2FFFFFEE2FFFFFEE2FFFFFEE2FFFFFEE2FFFFFE
      E2FFFFFEE2FFFFFEE2FFFFFEE2FFFFFEE2FFFFFEE2FFFFFEE2FFFFFEE2FFFFFE
      E2FFFFFEE2FFFFFEE2FFFFFEE2FFFFFEE2FFFFFEE2FFFFFEE2FFEBE74DFF6556
      41FF655543FF655543FF6E5F4EFFFFFC1DFFFFFEE2FFDEDC00FF655543FF6555
      43FF655543FF645443D95F503F5E5A4C3C375A4C3C2D5A4C3C225A4C3C175A4C
      3C0E5A4C3C065A4C3C035A4C3C01000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000005A4C3C045A4C3C0C5A4C3C1A5F503F5061524193645543F06555
      43FF655543FFC8C335FFFFFEDCFFFFFEDCFFFFFEDCFFFFFEDCFFFFFEDCFFFFFE
      DCFFFFFEDCFFFFFEDCFFFFFEDCFFFFFEDCFFFFFEDCFFFFFEDCFFFFFEDCFFFFFE
      DCFFFFFEDCFFFFFEDCFFFFFEDCFFFFFEDCFFFFFEDCFFFFFEDCFFFFFEDCFFFFFE
      DCFFFFFEDCFFFFFEDCFFFFFEDCFFFFFEDCFFFFFEDCFFFFFEDCFFFFFEDCFFFFFE
      DCFFFFFEDCFFFFFEDCFFFFFEDCFFFFFEDCFFFFFEDCFFFFFEDCFFFFFEDCFFEEEA
      43FF665641FF655543FF655543FF757300FFFFFC1AFFFFFEDCFFFFFC1AFF6555
      43FF655543FF655543FF655543FD6051406E5A4C3C3D5A4C3C335A4C3C275A4C
      3C1A5A4C3C0F5A4C3C085A4C3C035A4C3C010000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00005A4C3C025A4C3C085A4C3C145A4C3C295A4C3C45625241B1655543FF6555
      43FF7F753EFFFFFEBCFFFFFED9FFFFFED9FFFFFED9FFFFFED9FFFFFED9FFFFFE
      D9FFFFFED9FFFFFED9FFFFFED9FFFFFED9FFFFFED9FFFFFED9FFFFFED9FFFFFE
      D9FFFFFED9FFFFFED9FFFFFED9FFFFFED9FFFFFED9FFFFFED9FFFFFED9FFFFFE
      D9FFFFFED9FFFFFED9FFFFFED9FFFFFED9FFFFFED9FFFFFED9FFFFFED9FFFFFE
      D9FFFFFED9FFFFFED9FFFFFED9FFFFFED9FFFFFED9FFFFFED9FFFFFED9FFFFFE
      D9FFF4F170FF6F6149FF655543FF655543FF655543FFDBD900FFFFFED9FFFFFC
      16FF757300FF655543FF655543FF655543FD605140765A4C3C445A4C3C375A4C
      3C285A4C3C185A4C3C0E5A4C3C055A4C3C020000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000005A4C
      3C015A4C3C055A4C3C0E5A4C3C205A4C3C3A5F503F82655543FF655543FF6556
      41FFF7F443FFFFFED2FFFFFED2FFFFFED2FFFFFED2FFFFFED2FFFFFED2FFFFFE
      D2FFFFFED2FFFFFED2FFFFFED2FFFFFED2FFFFFED2FFFFFED2FFFFFED2FFFFFE
      D2FFFFFED2FFFFFED2FFFFFED2FFFFFED2FFFFFED2FFFFFED2FFFFFED2FFFFFE
      D2FFFFFED2FFFFFED2FFFFFED2FFFFFED2FFFFFED2FFFFFED2FFFFFED2FFFFFE
      D2FFFFFED2FFFFFED2FFFFFED2FFFFFED2FFFFFED2FFFFFED2FFFFFED2FFFFFE
      D2FFFFFED2FFF7F583FF706248FF655543FF655543FF655543FFCDCA00FFFFFE
      D2FFFFFD71FF858300FF655543FF655543FF655543FF6152418E5A4C3C435A4C
      3C325A4C3C205A4C3C125A4C3C085A4C3C030000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000005A4C
      3C035A4C3C0A5A4C3C185A4C3C2F5A4C3C4E645442E7655543FF655543FFA7A1
      3AFFFFFECCFFFFFECCFFFFFECCFFFFFECCFFFFFECCFFFFFECCFFFFFECCFFFFFE
      CCFFFFFECCFFFFFECCFFFFFECCFFFFFECCFFFFFECCFFFFFECCFFFFFECCFFFFFE
      CCFFFFFECCFFFFFECCFFFFFECCFFFFFECCFFFFFECCFFFFFECCFFFFFECCFFFFFE
      CCFFFFFECCFFFFFECCFFFFFECCFFFFFECCFFFFFECCFFFFFECCFFFFFECCFFFFFE
      CCFFFFFECCFFFFFECCFFFFFECCFFFFFECCFFFFFECCFFFFFECCFFFFFECCFFFFFE
      CCFFFFFECCFFFFFECCFFF9F77DFF716345FF655543FF655543FF655543FF9085
      79FFFFFDAFFFFFFD6EFF6E5F4EFF655543FF655543FF655543FF6152418A5A4C
      3C385A4C3C265A4C3C175A4C3C0B5A4C3C040000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000005A4C3C025A4C
      3C065A4C3C125A4C3C265A4C3C42615241A6655543FF655543FF716443FFFEFC
      8AFFFFFEC5FFFFFEC5FFFFFEC5FFFFFEC5FFFFFEC5FFFFFEC5FFFFFEC5FFFFFE
      C5FFFFFEC5FFFFFEC5FFFFFEC5FFFFFEC5FFFFFEC5FFFFFEC5FFFFFEC5FFFFFE
      C5FFFFFEC5FFFFFEC5FFFFFEC5FFFFFEC5FFFFFEC5FFFFFEC5FFFFFEC5FFFFFE
      C5FFFFFEC5FFFFFEC5FFFFFEC5FFFFFEC5FFFFFEC5FFFFFEC5FFFFFEC5FFFFFE
      C5FFFFFEC5FFFFFEC5FFFFFEC5FFFFFEC5FFFFFEC5FFFFFEC5FFFFFEC5FFFFFE
      C5FFFFFEC5FFFFFEC5FFFFFEC5FFFAF873FF716443FF655543FF655543FF6555
      43FFA09E00FFFFFDA8FFFFFD88FFA09E00FF655543FF655543FF655543FF6353
      42995A4C3C275A4C3C175A4C3C0C5A4C3C045A4C3C0100000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005A4C3C015A4C3C055A4C
      3C0D5A4C3C1D5A4C3C365E4F3F73655543FF655543FF655543FFE0DB2CFFFFFE
      BCFFFFFEBCFFFFFEBCFFFFFEBCFFFFFEBCFFFFFEBCFFFFFEBCFFFFFEBCFFFFFE
      BCFFFFFEBCFFFFFEBCFFFFFEBCFFFFFEBCFFFFFEBCFFFFFEBCFFFFFEBCFFFFFE
      BCFFFFFEBCFFFFFEBCFFFFFEBCFFFFFEBCFFFFFEBCFFFFFEBCFFFFFEBCFFFFFE
      BCFFFFFEBCFFFFFEBCFFFFFEBCFFFFFEBCFFFFFEBCFFFFFEBCFFFFFEBCFFFFFE
      BCFFFFFEBCFFFFFEBCFFFFFEBCFFFFFEBCFFFFFEBCFFFFFEBCFFFFFEBCFFFFFE
      BCFFFFFEBCFFFFFEBCFFFFFEBCFFFFFEBCFFFCFAA4FF847951FF655543FF6555
      43FF655543FF9F9D00FFFFFC61FFFFFD9EFF9F9D00FF655543FF655543FF6555
      43FF6454429A5A4C3C125A4C3C095A4C3C030000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005A4C3C035A4C3C095A4C
      3C175A4C3C2C5A4C3C4A635342CF655543FF655543FF938B46FFFFFEB5FFFFFE
      B5FFFFFEB5FFFFFEB5FFFFFEB5FFFFFEB5FFFFFEB5FFFFFEB5FFFFFEB5FFFFFE
      B5FFFFFEB5FFFFFEB5FFFFFEB5FFFFFEB5FFFFFEB5FFFFFEB5FFFFFEB5FFFFFE
      B5FFFFFEB5FFFFFEB5FFFFFEB5FFFFFEB5FFFFFEB5FFFFFEB5FFFFFEB5FFFFFE
      B5FFFFFEB5FFFFFEB5FFFFFEB5FFFFFEB5FFFFFEB5FFFFFEB5FFFFFEB5FFFFFE
      B5FFFFFEB5FFFFFEB5FFFFFEB5FFFFFEB5FFFFFEB5FFFFFEB5FFFFFEB5FFFFFE
      B5FFFFFEB5FFFFFEB5FFFFFEB5FFFFFEB5FFFFFEB5FFFDFB9DFF857B4CFF6555
      43FF655543FF655543FF838200FFFFFC5AFFFFFD98FFB9B600FF655543FF6555
      43FF655543FF655543A15A4C3C065A4C3C020000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000005A4C3C015A4C3C055A4C3C105A4C
      3C225A4C3C3D6051408E655543FF655543FF66573EFFFBF85DFFFFFDAFFFFFFD
      AFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFD
      AFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFD
      AFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFD
      AFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFD
      AFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFD
      AFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFDAFFFFDFB94FF7C71
      40FF655543FF655543FF655543FF838200FFFFFC37FFFFFDAFFFC7C500FF6555
      43FF655543FF655543FF655543AD5A4C3C010000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000005A4C3C035A4C3C0B5A4C3C195A4C
      3C315D4E3E62655543F5655543FF655543FFC7C23DFFFFFDA5FFFFFDA5FFFFFD
      A5FFFFFDA5FFFFFDA5FFFFFDA5FFFFFDA5FFFFFDA5FFFFFDA5FFFFFDA5FFFFFD
      A5FFFFFDA5FFFFFDA5FFFFFDA5FFFFFDA5FFFFFDA5FFFFFDA5FFFFFDA5FFFFFD
      A5FFFFFDA5FFFFFDA5FFFFFDA5FFFFFDA5FFFFFDA5FFFFFDA5FFFFFDA5FFFFFD
      A5FFFFFDA5FFFFFDA5FFFFFDA5FFFFFDA5FFFFFDA5FFFFFDA5FFFFFDA5FFFFFD
      A5FFFFFDA5FFFFFDA5FFFFFDA5FFFFFDA5FFFFFDA5FFFFFDA5FFFFFDA5FFFFFD
      A5FFFFFDA5FFFFFD6BFFFFFC30FFDBD907FFC7C31BFF9C962AFF776F29FF6A5E
      2FFF655543FF655543FF655543FF655543FF655543FFF5F20CFFFFFDA5FFD1CF
      00FF655543FF655543FF655543FF655543440000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000005A4C3C015A4C3C055A4C3C0E5A4C3C215A4C
      3C3C625341B0655543FF655543FF7B7043FFFFFD82FFFFFD9EFFFFFD9EFFFFFD
      9EFFFFFD9EFFFFFD9EFFFFFD9EFFFFFD9EFFFFFD9EFFFFFD9EFFFFFD9EFFFFFD
      9EFFFFFD9EFFFFFD9EFFFFFD9EFFFFFD9EFFFFFD9EFFFFFD9EFFFFFD9EFFFFFD
      9EFFFFFD9EFFFFFD9EFFFFFD9EFFFFFD9EFFFFFD9EFFFFFD9EFFFFFD9EFFFFFD
      9EFFFFFD9EFFFFFD9EFFFFFD9EFFFFFD9EFFFFFD9EFFFFFD9EFFFFFD9EFFFFFD
      9EFFFFFD9EFFFFFD9EFFFFFD9EFFFFFC2AFFF3F00AFFD5D114FFAAA621FF8F88
      29FF6B6128FF695C33FF67583DFF655543FF655543FF655543FF655543FF7567
      57FF84786AFFD0CD00FFDDDA00FFFFFC2AFFFFFC47FFFFFD9EFFFFFD9EFFFFFD
      9EFFDDDA00FF655543FF655543FF655543800000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000005A4C3C015A4C3C065A4C3C125A4C3C266152
      4079655543FF655543FF655542FFF3F03FFFFFFD95FFFFFD95FFFFFD95FFFFFD
      95FFFFFD95FFFFFD95FFFFFD95FFFFFD95FFFFFD95FFFFFD95FFFFFD95FFFFFD
      95FFFFFD95FFFFFD95FFFFFD95FFFFFD95FFFFFD95FFFFFD95FFFFFD95FFFFFD
      95FFFFFD95FFFFFD95FFFFFD95FFFFFD95FFFFFD95FFFFFD95FFFFFD95FFFFFD
      95FFFFFD95FFFFFD95FFFFFD95FFFFFD95FFFFFD95FFFFFD78FFE6E204FFC4C0
      1BFF9A9428FF81792DFF6A5F2CFF685B36FF655543FF655543FF655543FF6555
      43FF6D5E4DFF847769FFC3C000FFDBD900FFFAF711FFFFFC23FFFFFD95FFFFFD
      95FFFFFD95FFFFFD78FFFFFC23FFF1EE08FFDBD900FF847769FF7C6F60FF6555
      43FF655543FF655543FF655543FF655543440000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000005A4C3C015A4C3C075A4C3C135A4C3C266455
      43DE655543FF655543FF9D963EFFFFFD8BFFFFFD8BFFFFFD8BFFFFFD8BFFFFFD
      8BFFFFFD8BFFFFFD8BFFFFFD8BFFFFFD8BFFFFFD8BFFFFFD8BFFFFFD8BFFFFFD
      8BFFFFFD8BFFFFFD8BFFFFFD8BFFFFFD8BFFFFFD8BFFFFFD8BFFFFFD8BFFFFFD
      8BFFFFFD8BFFFFFD8BFFFFFD8BFFFFFD8BFFFFFD8BFFFFFD8BFFFFFD8BFFFFFD
      8BFFFFFD8BFFFFFD8BFFFFFD8BFFFFFD8BFFFFFD8BFFFFFC1DFF695C33FF6555
      43FF655543FF655543FF655543FF655543FFD9D600FFF8F50FFFFFFC1DFFFFFD
      8BFFFFFD8BFFFFFD8BFFFFFD8BFFFFFC1DFFF8F50FFFD9D600FFC0BE00FFA7A5
      00FF757300FF655543FF655543FF655543FF655543FF655543FF655543FF6555
      43FF655543FF655543FF655543AC000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000005A4C3C015A4C3C065A4C3C116354427F6555
      43FF655543FF6E6144FFFDFB6CFFFFFD85FFFFFD85FFFFFD85FFFFFD85FFFFFD
      85FFFFFD85FFFFFD85FFFFFD85FFFFFD85FFFFFD85FFFFFD85FFFFFD85FFFFFD
      85FFFFFD85FFFFFD85FFFFFD85FFFFFD85FFFFFD85FFFFFD85FFFFFD85FFFFFD
      85FFFFFD85FFFFFD85FFFFFD85FFFFFD85FFFFFD85FFFFFD85FFFFFD85FFFFFD
      85FFFFFD85FFFFFD85FFFFFD85FFFFFD85FFFFFD85FFFFFC16FF695C33FF6555
      43FF655543FF655543FF655543FF655543FFFFFD85FFFFFD67FFD7D400FFCCC9
      00FFA6A400FF6C5D4CFF655543FF655543FF655543FF655543FF655543FF6555
      43FF655543FF655543FF655543FF655543FF655543FF655543FF655543FF6555
      43FF655543C36555435A00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000005A4C3C015A4C3C0563544245655543FF6555
      43FF655543FFCFCA2EFFFFFD7BFFFFFD7BFFFFFD7BFFFFFD7BFFFFFD7BFFFFFD
      7BFFFFFD7BFFFFFD7BFFFFFD7BFFFFFD7BFFFFFD7BFFFFFD7BFFFFFD7BFFFFFD
      7BFFFFFD7BFFFFFD7BFFFFFD7BFFFFFD7BFFFFFD7BFFFFFD7BFFFFFD7BFFFFFD
      7BFFFFFD7BFFFFFD7BFFFFFD7BFFFFFD7BFFFFFD7BFFFFFD7BFFFFFD7BFFFFFD
      7BFFFFFD7BFFFFFD7BFFFFFD7BFFFFFD7BFFFFFD7BFFFDFA14FF695C33FF6555
      43FF655543C8655543FF655543FF655543FFFFFD7BFFFDFA14FF655543FF6555
      43FF655543FF655543FF655543FF655543FF655543FF655543FF655543FF6555
      43FF655543FF655543F0655543C0655543A06555438065554344655543300000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000005A4C3C015A4C3C03655543AF655543FF6555
      43FF8B8249FFFFFD71FFFFFD71FFFFFD71FFFFFD71FFFFFD71FFFFFD71FFFFFD
      71FFFFFD71FFF9F77EFFF0EE94FFF5F387FFF4F171FFE4E147FFF5F254FFFFFD
      71FFFFFD71FFFFFD71FFFFFD71FFFFFD71FFFFFD71FFFFFD71FFFFFD71FFFFFD
      71FFFFFD71FFFFFD71FFFFFD71FFFFFD71FFFFFD71FFFFFD71FFFFFD71FFFFFD
      71FFFFFD71FFFFFD71FFFFFD71FFFFFD71FFFFFD71FFFBF812FF695C33FF6555
      43FF655543C8655543FF655543FF655543FFFFFD71FFFBF812FF655543FF6555
      43FF655543FF655543FF655543FF655543C3655543B065554380655543606555
      4340655543040000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000065554345655543FF655543FF6656
      40FFF8F544FFFFFD67FFFFFD67FFFFFD67FFE6E4A0FFEBE995FFECE961FFE9E6
      35FFC0BC2CFFABA447FF83785EFF746932FF6B6129FF6B6129FFC4C021FFFFFD
      67FFFFFD67FFFFFD67FFFFFD67FFFFFD67FFFFFD67FFFFFD67FFFFFD67FFFFFD
      67FFFFFD67FFFFFD67FFFFFD67FFFFFD67FFFFFD67FFFFFD67FFFFFD67FFFFFD
      67FFFFFD67FFFFFD67FFFFFD67FFFFFD67FFFFFD67FFF9F610FF695C33FF6555
      43FF655543C8655543FF655543FF655543FFFFFD67FFF9F610FF655543FF6555
      43FF655543C66253411600000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000065554380655543FF655543FFB6B0
      40FFFFFC5EFFFFFC5EFFFFFC5EFFE4E100FFA09942FF6D6421FF6C6127FF6B60
      2BFF6B602AFF878031FF9D982CFFCDCB03FFEFEC06FFFEFB15FFFFFC5EFFFFFC
      5EFFFFFC5EFFFFFC5EFFFFFC5EFFFFFC5EFFFFFC5EFFFFFC5EFFFFFC5EFFFFFC
      5EFFFFFC5EFFFFFC5EFFFFFC5EFFFFFC5EFFFFFC5EFFFFFC5EFFFFFC5EFFFFFC
      5EFFFFFC5EFFFFFC5EFFFFFC5EFFFFFC5EFFFFFC5EFFF6F30DFF695C33FF6555
      43FF655543C8655543FF655543FF655543FFFFFC5EFFF6F30DFF655543FF6555
      43FF655543C65A4C3C0600000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000065554380655543FF655543FFBDB8
      29FFE1DE00FFFBF812FFFFFC54FFFFFC54FFFFFC54FFFFFC23FFF4F10BFFFBF8
      12FFFFFC54FFFFFC54FFFFFC54FFFFFC23FFF4F10BFFD7D400FFF4F10BFFFFFC
      54FFFFFC54FFFFFC54FFFFFC54FFFFFC54FFFFFC54FFFFFC54FFFFFC54FFFFFC
      54FFFFFC54FFFFFC54FFFFFC54FFFFFC54FFFFFC54FFFFFC54FFFFFC54FFFFFC
      54FFFFFC54FFFFFC54FFFFFC54FFFFFC54FFFFFC54FFF4F10BFF695C33FF6555
      43FF655543C8655543FF655543FF655543FFFFFC54FFF4F10BFF655543FF6555
      43FF655543C65A4C3C0600000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000065554344655543FF655543FF6555
      43FF66573DFF685B36FF6B602AFF867D30FFA59F2EFFBEBA1FFFD1CD40FFB6B1
      35FF9F983DFF8C843CFF6A5F2CFF685B36FF685B36FF655543FFBEBA1FFFFFFC
      4AFFFFFC4AFFFFFC4AFFFFFC4AFFFFFC4AFFFFFC4AFFFFFC4AFFFFFC4AFFFFFC
      4AFFFFFC4AFFFFFC4AFFFFFC4AFFFFFC4AFFFFFC4AFFFFFC4AFFFFFC4AFFFFFC
      4AFFFFFC4AFFFFFC4AFFFFFC4AFFFFFC4AFFFFFC4AFFF2EF09FF695C33FF6555
      43FF655543C8655543FF655543FF655543FFFFFC4AFFF2EF09FF655543FF6555
      43FF655543C65A4C3C0600000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000655543AC655543FF6555
      43FF655543FF655543FF655543FF655543FF655543FF655543FF655543FF6555
      43FF655543FF655543FF655543FF655543FF655543FF655543FFBDB91FFFFFFC
      41FFFFFC41FFFFFC41FFFFFC41FFFFFC41FFFFFC41FFFFFC41FFFFFC41FFFFFC
      41FFFFFC41FFFFFC41FFFFFC41FFFFFC41FFFFFC41FFFFFC41FFFFFC41FFFFFC
      41FFFFFC41FFFFFC41FFFFFC41FFFFFC41FFFFFC41FFF0ED07FF695C33FF6555
      43FF655543C8655543FF655543FF655543FFFFFC41FFF0ED07FF655543FF6555
      43FF655543C65A4C3C0600000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000655543446555
      4380655543A0655543D0655543FF655543FF655543FF655543FF655543FF6555
      43FF655543FF655543FF655543FF655543E2655543FF655543FFBBB81EFFFFFC
      37FFFFFC37FFFFFC37FFFFFC37FFFFFC37FFFFFC37FFFFFC37FFFFFC37FFFFFC
      37FFFFFC37FFFFFC37FFFFFC37FFFFFC37FFFFFC37FFFFFC37FFFFFC37FFFFFC
      37FFFFFC37FFFFFC37FFFFFC37FFFFFC37FFFFFC37FFEEEB05FF695C33FF6555
      43FF655543C8655543FF655543FF655543FFFFFC37FFEEEB05FF655543FF6555
      43FF655543C65A4C3C0600000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000655543306555436065554380655543B06555
      438065554360655543405F503F0964544389655543FF655543FFB9B51EFFFFFC
      2DFFFFFC2DFFFFFC2DFFFFFC2DFFFFFC2DFFFFFC2DFFFFFC2DFFFFFC2DFFFFFC
      2DFFFFFC2DFFFFFC2DFFFFFC2DFFFFFC2DFFFFFC2DFFFFFC2DFFFFFC2DFFFFFC
      2DFFFFFC2DFFFFFC2DFFFFFC2DFFFFFC2DFFFFFC2DFFEBE802FF695C33FF6555
      43FF655543C8655543FF655543FF655543FFFFFC2DFFEBE802FF655543FF6555
      43FF655543C65A4C3C0600000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000005A4C3C0564544389655543FF655543FFB7B31EFFFFFC
      23FFFFFC23FFFFFC23FFFFFC23FFFFFC23FFFFFC23FFFFFC23FFFFFC23FFFFFC
      23FFFFFC23FFFFFC23FFFFFC23FFFFFC23FFFFFC23FFFFFC23FFFFFC23FFFFFC
      23FFFFFC23FFFFFC23FFFFFC23FFFFFC23FFFFFC23FFE9E600FF695C33FF6555
      43FF655543C8655543FF655543FF655543FFFFFC23FFE9E600FF655543FF6555
      43FF655543C65A4C3C0600000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000005A4C3C0564544389655543FF655543FFB8B416FFFFFC
      1AFFFFFC1AFFFFFC1AFFFFFC1AFFFFFC1AFFFFFC1AFFFFFC1AFFFFFC1AFFFFFC
      1AFFFFFC1AFFFFFC1AFFFFFC1AFFFFFC1AFFFFFC1AFFFFFC1AFFFFFC1AFFFFFC
      1AFFFFFC1AFFFFFC1AFFFFFC1AFFFFFC1AFFFFFC1AFFE6E300FF695C33FF6555
      43FF655543C8655543FF655543FF655543FFFFFC1AFFE6E300FF655543FF6555
      43FF655543C65A4C3C0600000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000005A4C3C0564544389655543FF655543FFB7B216FFFDFA
      14FFFDFA14FFFDFA14FFFDFA14FFFDFA14FFFDFA14FFFDFA14FFFDFA14FFFDFA
      14FFFDFA14FFFDFA14FFFDFA14FFFDFA14FFFDFA14FFFDFA14FFFDFA14FFFDFA
      14FFFDFA14FFFDFA14FFFDFA14FFFDFA14FFFDFA14FFE3E000FF695C33FF6555
      43FF655543C8655543FF655543FF655543FFFDFA14FFE3E000FF655543FF6555
      43FF655543C65A4C3C0600000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000005A4C3C0564544389655543FF655543FFB4B015FFFAF7
      11FFFAF711FFFAF711FFFAF711FFFAF711FFFAF711FFFAF711FFFAF711FFFAF7
      11FFFAF711FFFAF711FFFAF711FFFAF711FFFAF711FFFAF711FFFAF711FFFAF7
      11FFFAF711FFFAF711FFFAF711FFFAF711FFFAF711FFDEDC00FF695C33FF6555
      43FF655543C8655543FF655543FF655543FFFAF711FFDEDC00FF655543FF6555
      43FF655543C65A4C3C0600000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000005A4C3C0564544389655543FF655543FFB2AF15FFF7F4
      0EFFF7F40EFFF7F40EFFF7F40EFFF7F40EFFF7F40EFFF7F40EFFF7F40EFFF7F4
      0EFFF7F40EFFF7F40EFFF7F40EFFF7F40EFFF7F40EFFF7F40EFFF7F40EFFF7F4
      0EFFF7F40EFFF7F40EFFF7F40EFFF7F40EFFF7F40EFFDBD900FF695C33FF6555
      43FF655543C8655543FF655543FF655543FFF7F40EFFDBD900FF655543FF6555
      43FF655543C65A4C3C0600000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000005A4C3C0564544389655543FF655543FFAFAC15FFF5F2
      0CFFF5F20CFFF5F20CFFF5F20CFFF5F20CFFF5F20CFFF5F20CFFF5F20CFFF5F2
      0CFFF5F20CFFF5F20CFFF5F20CFFF5F20CFFF5F20CFFF5F20CFFF5F20CFFF5F2
      0CFFF5F20CFFF5F20CFFF5F20CFFF5F20CFFF5F20CFFD9D600FF695C33FF6555
      43FF655543C8655543FF655543FF655543FFF5F20CFFD9D600FF655543FF6555
      43FF655543C65A4C3C0600000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000005A4C3C0564544389655543FF655543FFAEAB15FFF2EF
      09FFF2EF09FFF2EF09FFF2EF09FFF2EF09FFF2EF09FFF2EF09FFF2EF09FFF2EF
      09FFF2EF09FFF2EF09FFF2EF09FFF2EF09FFF2EF09FFF2EF09FFF2EF09FFF2EF
      09FFF2EF09FFF2EF09FFF2EF09FFF2EF09FFF2EF09FFD6D300FF695C33FF6555
      43FF655543C8655543FF655543FF655543FFF2EF09FFD6D300FF655543FF6555
      43FF655543C65A4C3C0600000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000005A4C3C0564544389655543FF655543FFAEAB0FFFEFEC
      06FFEFEC06FFEFEC06FFEFEC06FFEFEC06FFEFEC06FFEFEC06FFEFEC06FFEFEC
      06FFEFEC06FFEFEC06FFEFEC06FFEFEC06FFEFEC06FFEFEC06FFEFEC06FFEFEC
      06FFEFEC06FFEFEC06FFEFEC06FFEFEC06FFEFEC06FFD3D000FF695C33FF6555
      43FF655543C8655543FF655543FF655543FFEFEC06FFD3D000FF655543FF6555
      43FF655543C65A4C3C0600000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000005A4C3C0564544389655543FF655543FFAEAB0DFFEDEA
      04FFEDEA04FFEDEA04FFEDEA04FFEDEA04FFEDEA04FFEDEA04FFEDEA04FFEDEA
      04FFEDEA04FFEDEA04FFEDEA04FFEDEA04FFEDEA04FFEDEA04FFEDEA04FFEDEA
      04FFEDEA04FFEDEA04FFEDEA04FFEDEA04FFEDEA04FFD0CD00FF695C33FF6555
      43FF655543C8655543FF655543FF655543FFEDEA04FFD0CD00FF655543FF6555
      43FF655543C65A4C3C0600000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000005A4C3C0564544389655543FF655543FFABA80DFFEAE7
      01FFEAE701FFEAE701FFEAE701FFEAE701FFEAE701FFEAE701FFEAE701FFEAE7
      01FFEAE701FFEAE701FFEAE701FFEAE701FFEAE701FFEAE701FFEAE701FFEAE7
      01FFEAE701FFEAE701FFEAE701FFEAE701FFEAE701FFCDCA00FF695C33FF6555
      43FF655543C8655543FF655543FF655543FFEAE701FFCDCA00FF655543FF6555
      43FF655543C65A4C3C0600000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000005A4C3C0564544389655543FF655543FFAAA60DFFE7E4
      00FFE7E400FFE7E400FFE7E400FFE7E400FFE7E400FFE7E400FFE7E400FFE7E4
      00FFE7E400FFE7E400FFE7E400FFE7E400FFE7E400FFE7E400FFE7E400FFE7E4
      00FFE7E400FFE7E400FFE7E400FFE7E400FFE7E400FFCAC800FF695C33FF6555
      43FF655543C8655543FF655543FF655543FFE7E400FFCAC800FF655543FF6555
      43FF655543C65A4C3C0600000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000005A4C3C0564544389655543FF655543FFA8A50DFFE3E0
      00FFE3E000FFE3E000FFE3E000FFE3E000FFE3E000FFE3E000FFE3E000FFE3E0
      00FFE3E000FFE3E000FFE3E000FFE3E000FFE3E000FFE3E000FFE3E000FFE3E0
      00FFE3E000FFE3E000FFE3E000FFE3E000FFE3E000FFC7C500FF695C33FF6555
      43FF655543C8655543FF655543FF655543FFE3E000FFC7C500FF655543FF6555
      43FF655543C65A4C3C0600000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000005A4C3C0564544389655543FF655543FFA7A40DFFE0DD
      00FFE0DD00FFE0DD00FFE0DD00FFE0DD00FFE0DD00FFE0DD00FFE0DD00FFE0DD
      00FFE0DD00FFE0DD00FFE0DD00FFE0DD00FFE0DD00FFE0DD00FFE0DD00FFE0DD
      00FFE0DD00FFE0DD00FFE0DD00FFE0DD00FFE0DD00FFC4C200FF695C33FF6555
      43FF645543CA655543FF655543FF655543FFE0DD00FFC4C200FF655543FF6555
      43FF655543C65A4C3C0600000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000005A4C3C0564544389655543FF655543FFA6A30AFFDDDA
      00FFDDDA00FFDDDA00FFDDDA00FFDDDA00FFDDDA00FFDDDA00FFDDDA00FFDDDA
      00FFDDDA00FFDDDA00FFDDDA00FFDDDA00FFDDDA00FFDDDA00FFDDDA00FFDDDA
      00FFDDDA00FFDDDA00FFDDDA00FFDDDA00FFDDDA00FFC3C000FF695C33FF6555
      43FF645443CC655543FF655543FF655543FFDDDA00FFC3C000FF655543FF6555
      43FF655543C55A4C3C0600000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000005A4C3C0464544388655543FF655543FFA7A406FFDAD7
      00FFDAD700FFDAD700FFDAD700FFDAD700FFDAD700FFDAD700FFDAD700FFDAD7
      00FFDAD700FFDAD700FFDAD700FFDAD700FFDAD700FFDAD700FFDAD700FFDAD7
      00FFDAD700FFDAD700FFDAD700FFDAD700FFDAD700FFC0BE00FF695C33FF6555
      43FF645443CE655543FF655543FF655543FFDAD700FFC0BE00FF655543FF6555
      43FF655543C45A4C3C0500000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000005A4C3C0464554387655543FF655543FFA5A206FFD7D4
      00FFD7D400FFD7D400FFD7D400FFD7D400FFD7D400FFD7D400FFD7D400FFD7D4
      00FFD7D400FFD7D400FFD7D400FFD7D400FFD7D400FFD7D400FFD7D400FFD7D4
      00FFD7D400FFD7D400FFD7D400FFD7D400FFD7D400FFBFBC00FF695C33FF6555
      43FF645443CF655543FF655543FF655543FFD7D400FFBFBC00FF655543FF6555
      43FF655543C45A4C3C0500000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000005A4C3C0365554386655543FF655543FFA4A106FFD4D2
      00FFD4D200FFD4D200FFD4D200FFD4D200FFD4D200FFD4D200FFD4D200FFD4D2
      00FFD4D200FFD4D200FFD4D200FFD4D200FFD4D200FFD4D200FFD4D200FFD4D2
      00FFD4D200FFD4D200FFD4D200FFD4D200FFD4D200FFBCB900FF695C33FF6555
      43FF645443CF655543FF655543FF655543FFD4D200FFBCB900FF655543FF6555
      43FF655543C35A4C3C0300000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000005A4C3C0265554385655543FF655543FFA29F06FFD1CF
      00FFD1CF00FFD1CF00FFD1CF00FFD1CF00FFD1CF00FFD1CF00FFD1CF00FFD1CF
      00FFD1CF00FFD1CF00FFD1CF00FFD1CF00FFD1CF00FFD1CF00FFD1CF00FFD1CF
      00FFD1CF00FFD1CF00FFD1CF00FFD1CF00FFD1CF00FFBAB800FF695C33FF6555
      43FF645543CB655543FF655543FF655543FFD1CF00FFBAB800FF655543FF6555
      43FF655543C25A4C3C0200000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000005A4C3C0165554383655543FF655543FFA19E06FFCECC
      00FFCECC00FFCECC00FFCECC00FFCECC00FFCECC00FFCECC00FFCECC00FFCECC
      00FFCECC00FFCECC00FFCECC00FFCECC00FFCECC00FFCECC00FFCECC00FFCECC
      00FFCECC00FFCECC00FFCECC00FFCECC00FFCECC00FFB7B500FF695C33FF6555
      43FF655543FF655543FF655543FF655543FFCECC00FFB7B500FF655543FF6555
      43FF655543C15A4C3C0100000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000005A4C3C0165554382655543FF655543FFA09E05FFCDCA
      00FFCDCA00FFCDCA00FFCDCA00FFCDCA00FFCDCA00FFCDCA00FFCDCA00FFCDCA
      00FFCDCA00FFCDCA00FFCDCA00FFCDCA00FFCDCA00FFCDCA00FFCDCA00FFCDCA
      00FFCDCA00FFCDCA00FFCDCA00FFCDCA00FFCDCA00FFB7B500FF695C33FF6555
      43FF655543FF655543FF655543FF655543FFCDCA00FFB7B500FF655543FF6555
      43FF655543C00000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000065554381655543FF655543FFA09E00FFCAC8
      00FFCAC800FFCAC800FFCAC800FFCAC800FFCAC800FFCAC800FFCAC800FFCAC8
      00FFCAC800FFCAC800FFCAC800FFCAC800FFC6C300FFB4B200FFB4B200FFA6A4
      00FFA09E00FFA09E00FF8A8900FF8A8900FF807F00FF757300FF73683DFF7063
      54FF706354FF7C7166FF7C7166FF81786EFFCAC800FFB4B200FF655543FF6555
      43FF655543C00000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000065554380655543FF655543FFA09E00FFC9C6
      00FFC9C600FFC9C600FFB3B100FFB3B100FFAFAC00FFA09E00FFA09E00FF8F8D
      00FF8A8900FF8A8900FF757300FF7B7900FF8A8900FF8A8900FF959300FFA09E
      00FFA09E00FFB3B100FFB3B100FFB9B600FFC9C600FFC9C600FFC9C600FFC9C6
      00FFBFBC00FFB3B100FFB3B100FFAAA800FFA09E00FF766A5CFF655543FF6555
      43FF655543840000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000065554360655543FF655543FF918E08FFB3B1
      00FFB3B100FFC7C500FFC7C500FFC7C500FFB7B500FFB3B100FFB3B100FFB3B1
      00FFB1AF04FFB7B41AFFBCB91BFFB9B721FFACA934FFA19D2FFF89832AFF8078
      36FF7A7232FF726932FF6D613AFF6B5F39FF685B3CFF655544FF655543FF6555
      43FF655543FF655543FF655543FF655543FF655543FF655543FF655543FF6555
      43FF655543240000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000065554324655543FF655543FF655542FF6A5E
      2FFF6B6128FF6B6128FF6B602AFF6A5C37FF6A5D37FF695B39FF665645FF6656
      44FF655543FF655543FF655543FF655543FF655543FF655543FF655543FF6555
      43FF655543FF655543FF655543FF655543FF655543FF655543FF655543FF6555
      43FF655543FF655543FF655543FF655543FF655543FF655543FF655543FF6555
      438C000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000006555438C655543FF655543FF6555
      43FF655543FF655543FF655543FF655543FF655543FF655543FF655543FF6555
      43FF655543FF655543FF655543FF655543FF655543FF655543FF655543FF6555
      43FF655543FF655543FF655543FF655543FF655543FF655543FF655543FF6555
      43FF655543E0655543C0655543C0655543A06555438065554360655543240000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000065554324655543846555
      43C0655543C3655543FF655543FF655543FF655543FF655543FF655543FF6555
      43FF655543FF655543FF655543FF655543FF655543F0655543C0655543846555
      435A6555434265554324655543186555430E6555430865554302000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000655543076555430865554304655543076555
      4303655543016555430100000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE03FFFFFFFFFFFFFFFFFF
      FFFFFFFFFFF800FFFFFFFFFFFFFFFFFFFFFFFFFFFFE0003FFFFFFFFFFFFFFFFF
      FFFFFFFFFF000007FFFFFFFFFFFFFFFFFFFFFFFFFC000001FFFFFFFFFFFFFFFF
      FFFFFFFFF00000007FFFFFFFFFFFFFFFFFFFFFFFC00000001FFFFFFFFFFFFFFF
      FFFFFFFF0000000007FFFFFFFFFFFFFFFFFFFFF80000000001FFFFFFFFFFFFFF
      FFFFFFE000000000007FFFFFFFFFFFFFFFFFFF8000000000001FFFFFFFFFFFFF
      FFFFFE00000000000007FFFFFFFFFFFFFFFFF000000000000001FFFFFFFFFFFF
      FFFFC0000000000000007FFFFFFFFFFFFFFF00000000000000000FFFFFFFFFFF
      FFFC000000000000000003FFFFFFFFFFFFF0000000000000000000FFFFFFFFFF
      FF800000000000000000003FFFFFFFFFFE000000000000000000000FFFFFFFFF
      F80000000000000000000003FFFFFFFFE00000000000000000000000FFFFFFFF
      0000000000000000000000003FFFFFFC0000000000000000000000000FFFFFF0
      00000000000000000000000003FFFFC000000000000000000000000000FFFF00
      000000000000000000000000003FFC00000000000000000000000000000FF800
      0000000000000000000000000007F0000000000000000000000000000003F000
      0000000000000000000000000003F0000000000000000000000000000003F000
      0000000000000000000000000003F0000000000000000000000000000003F000
      0000000000000000000000000003F0000000000000000000000000000003F000
      0000000000000000000000000003F0000000000000000000000000000003F000
      0000000000000000000000000003F0000000000000000000000000000003F000
      0000000000000000000000000003F0000000000000000000000000000003F000
      000000000000000001C000000003F000000000000000000001F000000003F000
      000000000000000003FE00000003F000000000000000000003FFC0000003F000
      000000000000000003FFF0000003F000000000000000000007FFF8000003F000
      000000000000000007FFF8000003F00000000000000000000FFFFC000003F000
      00000000000000000FFFFC000003F00000000000000000001FFFFC000003F000
      00000000000000001FFFFC000003F00000000000000000003FFFFC000003F000
      00000000000000007FFFFC000003F00000000000000000007FFFFC000003F000
      00000000000000001FFFF8000003F00000000000000000000FFE00000003F000
      000000008000000007C000000003F0000000000F00000000000000000003F000
      0000007F00000000000000000003F0000000001E00000000000000000003F000
      0000000000000000000000000007F8000000000000000000000000000007F800
      0000000000000000000000000007F8000000000000000000000000000007F800
      000000000000000000000000000FFC00000000000000000000000000000FFF00
      000000000000000000000000003FFFE000000000000000000000000007FFFFFF
      000000000000000000000000FFFFFFFFF0000000000000000000003FFFFFFFFF
      FF00000000000000000007FFFFFFFFFFFFF0000000000000000007FFFFFFFFFF
      FFFF000000000000000003FFFFFFFFFFFFFFF80000000000000000FFFFFFFFFF
      FFFFFC00000000000000007FFFFFFFFFFFFFF800000000000000007FFFFFFFFF
      FFFFF000000000000000007FFFFFFFFFFFFFF000000000000000007FFFFFFFFF
      FFFFE000000000000000003FFFFFFFFFFFFFC000000000000000007FFFFFFFFF
      FFFFC000000000000000007FFFFFFFFFFFFF8000000000000000007FFFFFFFFF
      FFFF8000000000000000007FFFFFFFFFFFFF0000000000000000007FFFFFFFFF
      FFFF0000000000000000007FFFFFFFFFFFFF000000000000000000FFFFFFFFFF
      FFFF000000000000000001FFFFFFFFFFFFFF00000000000000000FFFFFFFFFFF
      FFFF0000000000000003FFFFFFFFFFFFFFFF80000000000001FFFFFFFFFFFFFF
      FFFF80000000000001FFFFFFFFFFFFFFFFFF80000000000001FFFFFFFFFFFFFF
      FFFF80000000000001FFFFFFFFFFFFFFFFFFC0000000000001FFFFFFFFFFFFFF
      FFFFE0000000000001FFFFFFFFFFFFFFFFFFFF000000000001FFFFFFFFFFFFFF
      FFFFFFFE0000000001FFFFFFFFFFFFFFFFFFFFFE0000000001FFFFFFFFFFFFFF
      FFFFFFFE0000000001FFFFFFFFFFFFFFFFFFFFFE0000000001FFFFFFFFFFFFFF
      FFFFFFFE0000000001FFFFFFFFFFFFFFFFFFFFFE0000000001FFFFFFFFFFFFFF
      FFFFFFFE0000000001FFFFFFFFFFFFFFFFFFFFFE0000000001FFFFFFFFFFFFFF
      FFFFFFFE0000000001FFFFFFFFFFFFFFFFFFFFFE0000000001FFFFFFFFFFFFFF
      FFFFFFFE0000000001FFFFFFFFFFFFFFFFFFFFFE0000000001FFFFFFFFFFFFFF
      FFFFFFFE0000000001FFFFFFFFFFFFFFFFFFFFFE0000000001FFFFFFFFFFFFFF
      FFFFFFFE0000000001FFFFFFFFFFFFFFFFFFFFFE0000000001FFFFFFFFFFFFFF
      FFFFFFFE0000000001FFFFFFFFFFFFFFFFFFFFFE0000000001FFFFFFFFFFFFFF
      FFFFFFFE0000000001FFFFFFFFFFFFFFFFFFFFFE0000000003FFFFFFFFFFFFFF
      FFFFFFFF0000000003FFFFFFFFFFFFFFFFFFFFFF0000000003FFFFFFFFFFFFFF
      FFFFFFFF0000000003FFFFFFFFFFFFFFFFFFFFFF0000000007FFFFFFFFFFFFFF
      FFFFFFFF800000000FFFFFFFFFFFFFFFFFFFFFFFC000001FFFFFFFFFFFFFFFFF
      FFFFFFFFFF01FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFF2800000060000000C0000000010020000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000005A4C3C025A4C3C035A4C3C035A4C
      3C025A4C3C010000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000005A4C3C015A4C3C035A4C3C085A4C3C0D5A4C3C125A4C3C135A4C
      3C105A4C3C0B5A4C3C065A4C3C01000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000005A4C
      3C025A4C3C045A4C3C0A5A4C3C135A4C3C1E5A4C3C2A5A4C3C325A4C3C355A4C
      3C2F5A4C3C235A4C3C175A4C3C0D5A4C3C065A4C3C0200000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005A4C3C025A4C3C065A4C
      3C0B5A4C3C155A4C3C215A4C3C325A4C3C445A4C3C565A4C3C60594C3C635A4C
      3C5C5A4C3C4C5A4C3C395A4C3C285A4C3C185A4C3C0E5A4C3C075A4C3C020000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000005A4C3C015A4C3C035A4C3C065A4C3C0D5A4C3C175A4C
      3C255A4C3C355A4C3C495A4C3C5F5A4C3C745A4C3C8A5C4E3EAC5D4F3EB35C4D
      3D9D594C3C7D5A4C3C665A4C3C515A4C3C3B5A4C3C285A4C3C1A5A4C3C0E5A4C
      3C075A4C3C025A4C3C0100000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00005A4C3C015A4C3C035A4C3C085A4C3C0F5A4C3C195A4C3C275A4C3C385A4C
      3C4D5A4C3C605A4C3C785A4D3C965E503FBF625241E465553DF9665539FD6454
      3FF1605140D45C4D3DA35A4C3C815A4C3C655A4C3C505A4C3C3C5A4C3C285A4C
      3C195A4C3C0E5A4C3C075A4C3C02000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000005A4C3C015A4C
      3C035A4C3C085A4C3C115A4C3C1C5A4C3C2A5A4C3C3D5A4C3C515A4C3C66594C
      3C7E5B4D3D9E5E4F3FC5625342EE66563DFF675635FF60514FFF5E515FFF6454
      40FF685638FF645442F9605040D55C4D3DA75A4C3C845A4C3C695A4C3C535A4C
      3C3D5A4C3C2A5A4C3C1B5A4C3C0F5A4C3C075A4C3C0300000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000005A4C3C015A4C3C055A4C3C0A5A4C
      3C135A4C3C205A4C3C2F5A4C3C415A4C3C575A4C3C6E594C3C835C4D3DA75F50
      3FD1645441F466553FFF685634FF5E5051FF5A518DFF5451D3FF4F50F7FF5853
      B0FF5B4F6BFF665537FF67563CFF645441FD61513FDA5C4E3EAE594B3C845A4C
      3C6D5A4C3C575A4C3C405A4C3C2C5A4C3C1D5A4C3C115A4C3C085A4C3C045A4C
      3C01000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000005A4C3C025A4C3C055A4C3C0B5A4C3C155A4C3C225A4C
      3C315A4C3C465A4C3C5A5A4C3C6F5A4C3C8A5C4E3DAD615140D864543EF46655
      3AFF65543DFF61534FFF564FA2FF5957D7FF5859FFFF6060FFFF6060FFFF5C5E
      FFFF5857F0FF5752B4FF574C74FF645440FF66553AFF65553EF9605140DB5C4D
      3DAB5A4C3C885A4C3C6D5A4C3C555A4C3C415A4C3C2D5A4C3C1D5A4C3C115A4C
      3C085A4C3C035A4C3C0100000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000005A4C
      3C015A4C3C035A4C3C075A4C3C0D5A4C3C185A4C3C255A4C3C355A4C3C4B5A4C
      3C5F594B3C745B4C3C935D4E3EB7615241E1655540FB685635FF615244FF5E51
      63FF544FAFFF5A59EAFF5D5DFFFF6061FFFF6161FFFF6060FEFF6060FEFF6161
      FEFF6262FFFF5D5EFFFF5C5CF7FF5451C7FF5B5070FF615245FF675636FF6555
      3FFB615241E15D4E3EB45A4C3C8B594C3C6E5A4C3C585A4C3C425A4C3C2E5A4C
      3C1E5A4C3C125A4C3C0A5A4C3C045A4C3C010000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005A4C3C015A4C3C045A4C
      3C085A4C3C105A4C3C1A5A4C3C295A4C3C3B5A4C3C4E5A4C3C64594C3C795B4C
      3C985E4F3EC0625241E7665541FF685635FF615244FF5A4F79FF5752BAFF5A5B
      FCFF6464FFFF6464FFFF6565FFFF6464FFFF6464FFFF6464FFFF6464FFFF6464
      FFFF6464FFFF6565FFFF6565FFFF6465FFFF5C5DFFFF5854BDFF5A5079FF6051
      44FF675637FF665542FF615140E05D4E3EB75A4C3C8C5A4C3C705A4C3C5A5A4C
      3C435A4C3C315A4C3C205A4C3C135A4C3C0A5A4C3C045A4C3C01000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000005A4C3C015A4C3C045A4C3C095A4C3C115A4C
      3C1D5A4C3C2B5A4C3C3D5A4C3C515A4C3C685A4C3C7F5B4D3D9E5F503FC96353
      3FEA66553BFF65543CFF625244FF564E8AFF5C59C7FF5C5DFFFF6868FFFF6969
      FFFF6868FFFF6868FFFF6868FFFF6868FFFF6868FFFF6868FFFF6868FFFF6868
      FFFF6868FFFF6868FFFF6868FFFF6868FFFF6969FFFF6969FFFF5F5FFFFF5C59
      C8FF564E89FF625241FF66553AFF66553CFD625240E35E4F3FBB5A4C3C905A4C
      3C725A4C3C5A5A4C3C455A4C3C315A4C3C205A4C3C135A4C3C0A5A4C3C045A4C
      3C01000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000005A4C3C025A4C3C055A4C3C0C5A4C3C135A4C3C205A4C3C2F5A4C
      3C435A4C3C58594B3C6B5A4C3C875C4E3DAA605140D5645442F6675637FF6252
      3CFF5F5157FF575097FF5B59E1FF6566FFFF6A6BFFFF6D6DFFFF6B6BFFFF6B6B
      FFFF6B6BFFFF6B6BFFFF6B6BFFFF6B6BFFFF6B6BFFFF6B6BFFFF6B6BFFFF6B6B
      FFFF6B6BFFFF6B6BFFFF6B6BFFFF6B6BFFFF6B6BFFFF6B6BFFFF6D6DFFFF6A6B
      FFFF6565FFFF5A58D9FF595083FF60514EFF655436FF66563BFF625242EA5E4F
      3EBE5B4C3C94594C3B755A4C3C5E5A4C3C485A4C3C335A4C3C215A4C3C145A4C
      3C0B5A4C3C055A4C3C0200000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000005A4C
      3C025A4C3C065A4C3C0D5A4C3C165A4C3C245A4C3C335A4C3C485A4C3C5D5A4C
      3C725A4C3C8D5D4E3EB3615140DB645442FA67563AFF645337FF5B4F69FF5B55
      A2FF5C5DEFFF6D6EFFFF6E6FFFFF7070FFFF6F6FFFFF6F6FFFFF6F6FFFFF6F6F
      FFFF6F6FFFFF6F6FFFFF6F6FFFFF6F6FFFFF6F6FFFFF6F6FFFFF6F6FFFFF6F6F
      FFFF6F6FFFFF6F6FFFFF6F6FFFFF6F6FFFFF6F6FFFFF6F6FFFFF6F6FFFFF6F6F
      FFFF7070FFFF7070FFFF6869FFFF5958E0FF5D558DFF5D4F54FF675633FF6655
      40FF625241E95F503FC35B4D3D99594C3C755A4C3C605A4C3C495A4C3C345A4C
      3C235A4C3C155A4C3C0B5A4C3C055A4C3C010000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005A4C3C025A4C3C075A4C
      3C0E5A4C3C185A4C3C265A4C3C375A4C3C4A5A4C3C5F5A4C3C775A4C3C8F5D4F
      3EB9615240E265553CFD65543CFF655437FF594E6DFF605AB6FF6363F1FF6F71
      FFFF7676FFFF7373FFFF7373FFFF7373FFFF7373FFFF7373FFFF7373FFFF7373
      FFFF7373FFFF7373FFFF7373FFFF7373FFFF7373FFFF7373FFFF7373FFFF7373
      FFFF7373FFFF7373FFFF7373FFFF7373FFFF7373FFFF7373FFFF7373FFFF7373
      FFFF7373FFFF7373FFFF7474FFFF7576FFFF6B6CFFFF6360D9FF595298FF6151
      46FF655439FF66553CFF635340ED5F4F3FC45A4C3C975A4C3C775A4C3C5F5A4C
      3C495A4C3C345A4C3C235A4C3C155A4C3C0B5A4C3C055A4C3C01000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000005A4C3C015A4C3C035A4C3C085A4C3C0F5A4C3C1B5A4C
      3C295A4C3C3B5A4C3C4F5A4C3C635A4C3C7B5B4C3C9A5E4F3EC0625341E96656
      39FD645338FF60514AFF5E5272FF5F5CCEFF6F6FF2FF7475FFFF7B7BFFFF7878
      FFFF7777FFFF7878FFFF7878FFFF7878FFFF7878FFFF7878FFFF7878FFFF7878
      FFFF7878FFFF7878FFFF7878FFFF7878FFFF7878FFFF7878FFFF7878FFFF7878
      FFFF7878FFFF7878FFFF7878FFFF7878FFFF7878FFFF7878FFFF7878FFFF7878
      FFFF7878FFFF7878FFFF7878FFFF7878FFFF7979FFFF797AFFFF7072FFFF6663
      E1FF5952A1FF605255FF63523BFF675539FF635442F05F503FC55B4D3C9A5A4C
      3C7A5A4C3C605A4C3C4C5A4C3C375A4C3C255A4C3C165A4C3C0D5A4C3C055A4C
      3C01000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00005A4C3C015A4C3C045A4C3C0A5A4C3C125A4C3C1F5A4C3C2E5A4C3C405A4C
      3C565A4C3C6B594B3C815C4D3DA55F503FCC635342F166563DFF665532FF5A4D
      5DFF5F568CFF6260DCFF7474FFFF7B7CFFFF7E7EFFFF7D7DFFFF7C7CFFFF7C7C
      FFFF7C7CFFFF7C7CFFFF7C7CFFFF7C7CFFFF7C7CFFFF7C7CFFFF7C7CFFFF7C7C
      FFFF7C7CFFFF7C7CFFFF7C7CFFFF7C7CFFFF7C7CFFFF7C7CFFFF7C7CFFFF7C7C
      FFFF7C7CFFFF7C7CFFFF7C7CFFFF7C7CFFFF7C7CFFFF7C7CFFFF7C7CFFFF7C7C
      FFFF7C7CFFFF7C7CFFFF7C7CFFFF7C7CFFFF7C7CFFFF7C7CFFFF7E7EFFFF7C7D
      FFFF7A7BFFFF6767F6FF60599EFF5C4F5FFF655334FF67563CFF645442F45F50
      3FC95B4D3DA0594B3C7B5A4C3C665A4C3C4F5A4C3C395A4C3C275A4C3C185A4C
      3C0E5A4C3C075A4C3C0200000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000005A4C3C015A4C
      3C055A4C3C0B5A4C3C145A4C3C205A4C3C305A4C3C445A4C3C585A4C3C6E5A4C
      3C855B4D3DAA60513FD564543EF365553CFF655435FF5C4F5DFF5C55A9FF6867
      E4FF7778FFFF8484FFFF8181FFFF8181FFFF8080FFFF8080FFFF8080FFFF8080
      FFFF8080FFFF8080FFFF8080FFFF8080FFFF8080FFFF8080FFFF8080FFFF8080
      FFFF8080FFFF8080FFFF8080FFFF8080FFFF8080FFFF8080FFFF8080FFFF8080
      FFFF8080FFFF8080FFFF8080FFFF8080FFFF8080FFFF8080FFFF8080FFFF8080
      FFFF8080FFFF8080FFFF8080FFFF8080FFFF8080FFFF8080FFFF8080FFFF8080
      FFFF8181FFFF8383FFFF7C7EFFFF6C6AE8FF615AACFF5A4C65FF645338FF6555
      3CFF63543FF15F503FCC5B4C3C9F5A4C3C7E5A4C3C655A4C3C4E5A4C3C395A4C
      3C275A4C3C1A5A4C3C0E5A4C3C065A4C3C020000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000005A4C3C015A4C3C025A4C3C065A4C3C0D5A4C
      3C165A4C3C245A4C3C345A4C3C475A4C3C5C594C3C735A4C3C8F5C4E3DB36152
      41DE65553CF8655437FF5F4F45FF60525FFF5C58B9FF7573E9FF7D7EFFFF898A
      FFFF8888FFFF8686FFFF8686FFFF8686FFFF8686FFFF8686FFFF8686FFFF8686
      FFFF8686FFFF8686FFFF8686FFFF8686FFFF8686FFFF8686FFFF8686FFFF8686
      FFFF8686FFFF8686FFFF8686FFFF8686FFFF8686FFFF8686FFFF8686FFFF8686
      FFFF8686FFFF8686FFFF8686FFFF8686FFFF8686FFFF8686FFFF8686FFFF8686
      FFFF8686FFFF8686FFFF8686FFFF8686FFFF8686FFFF8686FFFF8686FFFF8686
      FFFF8686FFFF8686FFFF8787FFFF8A8AFFFF8082FFFF7675EDFF5D59B8FF5E51
      5FFF60513FFF665535FF64543FF4605040D45C4D3DA35A4C3C815A4C3C665A4C
      3C505A4C3C3C5A4C3C285A4C3C195A4C3C0F5A4C3C075A4C3C025A4C3C010000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000005A4C3C015A4C3C035A4C3C085A4C3C0F5A4C3C195A4C3C275A4C
      3C385A4C3C4E5A4C3C62594B3C765B4C3D965D4F3EBD625241E5695839FF6856
      33FF5C4E4AFF5D5379FF5F5DC5FF7877FCFF898AFFFF8C8CFFFF8D8DFFFF8A8A
      FFFF8A8AFFFF8A8AFFFF8A8AFFFF8A8AFFFF8A8AFFFF8A8AFFFF8A8AFFFF8A8A
      FFFF8A8AFFFF8A8AFFFF8A8AFFFF8A8AFFFF8A8AFFFF8A8AFFFF8A8AFFFF8A8A
      FFFF8A8AFFFF8A8AFFFF8A8AFFFF8A8AFFFF8A8AFFFF8A8AFFFF8A8AFFFF8A8A
      FFFF8A8AFFFF8A8AFFFF8A8AFFFF8A8AFFFF8A8AFFFF8A8AFFFF8A8AFFFF8A8A
      FFFF8A8AFFFF8A8AFFFF8A8AFFFF8A8AFFFF8A8AFFFF8A8AFFFF8A8AFFFF8A8A
      FFFF8A8AFFFF8A8AFFFF8A8AFFFF8A8AFFFF8C8CFFFF8C8DFFFF8A8AFFFF7575
      FCFF625CB0FF5B4E70FF5E4F44FF685734FF645442F9605040D35C4E3DA75A4C
      3C825A4C3C695A4C3C535A4C3C3D5A4C3C2B5A4C3C1B5A4C3C0F5A4C3C085A4C
      3C03000000000000000000000000000000000000000000000000000000000000
      00005A4C3C025A4C3C075A4C3C0F5A4C3C1A5A4C3C295A4C3C3C5A4C3C4F5A4C
      3C665A4C3C7C5B4D3C9C60503DC564543CE967563AFF64533AFF41387BFF554D
      98FF6E6ACAFF7B7CFFFF9596FFFF9292FFFF9191FFFF9090FFFF9090FFFF9090
      FFFF9090FFFF9090FFFF9090FFFF9090FFFF9090FFFF9090FFFF9090FFFF9090
      FFFF9090FFFF9090FFFF9090FFFF9090FFFF9090FFFF9090FFFF9090FFFF9090
      FFFF9090FFFF9090FFFF9090FFFF9090FFFF9090FFFF9090FFFF9090FFFF9090
      FFFF9090FFFF9090FFFF9090FFFF9090FFFF9090FFFF9090FFFF9090FFFF9090
      FFFF9090FFFF9090FFFF9090FFFF9090FFFF9090FFFF9090FFFF9090FFFF9090
      FFFF9090FFFF9090FFFF9090FFFF9090FFFF9090FFFF9090FFFF9191FFFF9393
      FFFF9092FFFF7576FFFF6964BDFF524985FF625139FF66543BFF64543DF56051
      40D75C4E3DAD5A4C3C845A4C3C6A5A4C3C535A4C3C3E5A4C3C295A4C3C1A5A4C
      3C0E5A4C3C065A4C3C0200000000000000000000000000000000000000005A4C
      3C015A4C3C075A4C3C125A4C3C255A4C3C3B5A4C3C52594C3C6A5A4C3C845B4D
      3DA260513ECD685737F262523EFF50445BFF302DA8FF4B47BAFF6D6DF8FF8587
      FFFF9899FFFF9A9AFFFF9595FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696
      FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696
      FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696
      FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696
      FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696
      FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696
      FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696FFFF9696FFFF9595
      FFFF9696FFFF9B9BFFFF9496FFFF8282FFFF6361D4FF564C7BFF615143FF6453
      36FF65553CFA605141DD5C4E3DAE5A4C3C88594B3C6C5A4C3C525A4C3C395A4C
      3C225A4C3C105A4C3C055A4C3C01000000000000000000000000000000005A4C
      3C035A4C3C0E5A4C3C225A4C3C43594C3C605A4C3C835C4E3DAE5F5040D56958
      38F863533EFF4D415DFF3A35A9FF4545DEFF7576FFFF9697FFFFA0A0FFFF9F9F
      FFFF9B9BFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9A
      FFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9A
      FFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9A
      FFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9A
      FFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9A
      FFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9A
      FFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9A
      FFFF9A9AFFFF9A9AFFFF9C9CFFFF9F9FFFFF999BFFFF8989FEFF6766DCFF6458
      76FF5C4C4BFF675532FF645442FC605140DC5D4E3EB35A4C3C84594C3C5C5A4C
      3C3E5A4C3C1F5A4C3C0B5A4C3C02000000000000000000000000000000005A4C
      3C065A4C3C135A4C3C2D5A4C3C5D5E4F3FA562523EDA65553CFA685635FF493E
      65FF423DA2FF5151EBFF7E7FFFFFA1A1FFFFA5A5FFFFA1A1FFFFA0A0FFFF9F9F
      FFFF9F9FFFFF9F9FFFFF9F9FFFFF9F9FFFFF9F9FFFFF9F9FFFFF9F9FFFFF9F9F
      FFFF9F9FFFFF9F9FFFFF9F9FFFFF9F9FFFFF9F9FFFFF9F9FFFFF9F9FFFFF9F9F
      FFFF9F9FFFFF9F9FFFFF9F9FFFFF9F9FFFFF9F9FFFFF9F9FFFFF9F9FFFFF9F9F
      FFFF9F9FFFFF9F9FFFFF9F9FFFFF9F9FFFFF9F9FFFFF9F9FFFFF9F9FFFFF9F9F
      FFFF9F9FFFFF9F9FFFFF9F9FFFFF9F9FFFFF9F9FFFFF9F9FFFFF9F9FFFFF9F9F
      FFFF9F9FFFFF9F9FFFFF9F9FFFFF9F9FFFFF9F9FFFFF9F9FFFFF9F9FFFFF9F9F
      FFFF9F9FFFFF9F9FFFFF9F9FFFFF9F9FFFFF9F9FFFFF9F9FFFFF9F9FFFFF9F9F
      FFFF9F9FFFFF9F9FFFFF9F9FFFFF9F9FFFFFA1A1FFFFA2A2FFFFA5A6FFFF8D8F
      FFFF7572CAFF5C5388FF604F3AFF64543AFF645440FF615240DC5E4F3EA05A4C
      3C555A4C3C275A4C3C125A4C3C04000000000000000000000000000000005A4C
      3C045A4C3C175E503F6B615342D365543BF961513DFF52455CFF433FAEFF6160
      EAFF8889FFFFA8A8FFFFADADFFFFA6A6FFFFA5A5FFFFA5A5FFFFA5A5FFFFA5A5
      FFFFA5A5FFFFA5A5FFFFA5A5FFFFA5A5FFFFA5A5FFFFA5A5FFFFA5A5FFFFA5A5
      FFFFA5A5FFFFA5A5FFFFA5A5FFFFA5A5FFFFA5A5FFFFA5A5FFFFA5A5FFFFA5A5
      FFFFA5A5FFFFA5A5FFFFA5A5FFFFA5A5FFFFA5A5FFFFA5A5FFFFA5A5FFFFA5A5
      FFFFA5A5FFFFA5A5FFFFA5A5FFFFA5A5FFFFA5A5FFFFA5A5FFFFA5A5FFFFA5A5
      FFFFA5A5FFFFA5A5FFFFA5A5FFFFA5A5FFFFA5A5FFFFA5A5FFFFA5A5FFFFA5A5
      FFFFA5A5FFFFA5A5FFFFA5A5FFFFA5A5FFFFA5A5FFFFA5A5FFFFA5A5FFFFA5A5
      FFFFA5A5FFFFA5A5FFFFA5A5FFFFA5A5FFFFA5A5FFFFA5A5FFFFA5A5FFFFA5A5
      FFFFA5A5FFFFA5A5FFFFA5A5FFFFA5A5FFFFA5A5FFFFA5A5FFFFA5A5FFFFA9A9
      FFFFA9AAFFFF9293FFFF7875CFFF615784FF65543DFF635238FF65543CF96252
      41CA5E4F3E5E5A4C3C125A4C3C04000000000000000000000000000000005A4C
      3C016051406E655542F6665432FF60504CFF4D48AFFF6C6EF2FF999BFFFFACAD
      FFFFB2B2FFFFACACFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAA
      FFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAA
      FFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAA
      FFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAA
      FFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAA
      FFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAA
      FFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAA
      FFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAA
      FFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAAFFFFAAAA
      FFFFABABFFFFB0B0FFFFAAACFFFFA2A3FDFF8884CCFF6D6172FF5C4D4EFF6655
      32FF655542ED6051405C00000000000000000000000000000000000000005C4E
      3D13655541C2625246FF6D6491FF8B88D5FFA2A4FFFFB7B8FFFFB4B4FFFFB2B2
      FFFFB0B0FFFFB0B0FFFFB0B0FFFFB0B0FFFFB0B0FFFFB0B0FFFFB0B0FFFFB0B0
      FFFFB0B0FFFFB0B0FFFFB0B0FFFFB0B0FFFFB0B0FFFFB0B0FFFFB0B0FFFFB0B0
      FFFFB0B0FFFFB0B0FFFFB0B0FFFFB0B0FFFFB0B0FFFFB0B0FFFFB0B0FFFFB0B0
      FFFFB0B0FFFFB0B0FFFFB0B0FFFFB0B0FFFFB0B0FFFFB0B0FFFFB0B0FFFFB0B0
      FFFFB0B0FFFFB0B0FFFFB0B0FFFFB0B0FFFFB0B0FFFFB0B0FFFFB0B0FFFFB0B0
      FFFFB0B0FFFFB0B0FFFFB0B0FFFFB0B0FFFFB0B0FFFFB0B0FFFFB0B0FFFFB0B0
      FFFFB0B0FFFFB0B0FFFFB0B0FFFFB0B0FFFFB3B3FFFFB3B3FFFFB2B2FFFFB1B1
      FFFFB0B0FFFFB0B0FFFFB0B0FFFFB0B0FFFFB0B0FFFFB0B0FFFFB0B0FFFFB0B0
      FFFFB0B0FFFFB0B0FFFFB0B0FFFFB0B0FFFFB0B0FFFFB0B0FFFFB0B0FFFFB0B0
      FFFFB0B0FFFFB0B0FFFFB1B1FFFFB2B3FFFFB6B7FFFFA2A5FFFF8683D2FF665D
      8BFF625346FF655541AF5C4E3D06000000000000000000000000000000005D4E
      3E2C65543FE862534CFF8B8CFFFFC0C1FFFFB9B9FFFFB6B6FFFFB6B6FFFFB6B6
      FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6
      FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6
      FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6
      FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6
      FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6
      FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6
      FFFFB6B6FFFFB6B6FFFFB6B6FFFFB7B7FFFFA0A0FDFFA2A1F9FFB8B8FFFFBABB
      FFFFBABBFFFFB8B8FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6
      FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6
      FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB9B9FFFFBEC0FFFF7A79
      EEFF615148FF65543FC95C4E3E1A000000000000000000000000000000005D4E
      3E3165543EF063544BFF9998F5FFBFBFFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBB
      FFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBB
      FFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBB
      FFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBB
      FFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBB
      FFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBB
      FFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBB
      FFFFBBBBFFFFBBBBFFFFBCBCFFFFBDBDFFFF80757CFF685A4CFF786F94FF9895
      D7FFB1B0F2FFBBBDFFFFC3C3FFFFBEBEFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBB
      FFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBB
      FFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFBBBBFFFFC1C1FFFF8582
      DCFF615148FF655440CD5C4E3E1C000000000000000000000000000000005D4E
      3E3065543EEE64544BFF9C9BF4FFC5C6FFFFC1C1FFFFC1C1FFFFC1C1FFFFC1C1
      FFFFC1C1FFFFC1C1FFFFC1C1FFFFC1C1FFFFC1C1FFFFC1C1FFFFC1C1FFFFC1C1
      FFFFC1C1FFFFC1C1FFFFC1C1FFFFC1C1FFFFC1C1FFFFC1C1FFFFC1C1FFFFC1C1
      FFFFC1C1FFFFC1C1FFFFC1C1FFFFC1C1FFFFC1C1FFFFC1C1FFFFC1C1FFFFC1C1
      FFFFC1C1FFFFC1C1FFFFC1C1FFFFC1C1FFFFC1C1FFFFC1C1FFFFC1C1FFFFC1C1
      FFFFC1C1FFFFC1C1FFFFC1C1FFFFC1C1FFFFC1C1FFFFC1C1FFFFC1C1FFFFC1C1
      FFFFC1C1FFFFC1C1FFFFC1C1FFFFC1C1FFFFC1C1FFFFC1C1FFFFC1C1FFFFC1C1
      FFFFC1C1FFFFC1C1FFFFC6C7FFFFA3A2F5FF64564EFF614F35FF62503BFF6353
      37FF6C6170FF867FABFFA7A4CCFFB8B8FFFFCACBFFFFC4C5FFFFC2C2FFFFC2C2
      FFFFC1C1FFFFC1C1FFFFC1C1FFFFC1C1FFFFC1C1FFFFC1C1FFFFC1C1FFFFC1C1
      FFFFC1C1FFFFC1C1FFFFC1C1FFFFC1C1FFFFC1C1FFFFC1C1FFFFC6C6FFFF8582
      E1FF605148FF655440CC5C4E3E1B000000000000000000000000000000005D4E
      3E3064543EEE64554BFFA09FF4FFCBCCFFFFC7C7FFFFC7C7FFFFC7C7FFFFC7C7
      FFFFC7C7FFFFC7C7FFFFC7C7FFFFC7C7FFFFC7C7FFFFC7C7FFFFC7C7FFFFC7C7
      FFFFC7C7FFFFC7C7FFFFC7C7FFFFC7C7FFFFC7C7FFFFC7C7FFFFC7C7FFFFC7C7
      FFFFC7C7FFFFC7C7FFFFC7C7FFFFC7C7FFFFC7C7FFFFC7C7FFFFC7C7FFFFC7C7
      FFFFC7C7FFFFC7C7FFFFC7C7FFFFC7C7FFFFC7C7FFFFC7C7FFFFC7C7FFFFC7C7
      FFFFC7C7FFFFC7C7FFFFC7C7FFFFC7C7FFFFC7C7FFFFC7C7FFFFC7C7FFFFC7C7
      FFFFC7C7FFFFC7C7FFFFC7C7FFFFC7C7FFFFC7C7FFFFC7C7FFFFC7C7FFFFC7C7
      FFFFC7C7FFFFC7C7FFFFCACCFFFF877FA4FF5E4E3DFF655541C8655543A16555
      43F763533BFF604E31FF67584CFF796D7EFF8E88BEFFAEAFFFFFC9CAFFFFCACB
      FFFFCCCDFFFFC9C9FFFFC7C7FFFFC7C7FFFFC7C7FFFFC7C7FFFFC7C7FFFFC7C7
      FFFFC7C7FFFFC7C7FFFFC7C7FFFFC7C7FFFFC7C7FFFFC7C7FFFFCCCDFFFF8785
      E5FF605049FF655440CC5C4E3E1B000000000000000000000000000000005D4E
      3E3064543EEE64554BFFA4A3F4FFD1D1FFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCC
      FFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCC
      FFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCC
      FFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCC
      FFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCC
      FFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCC
      FFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCC
      FFFFCCCCFFFFD4D4FFFFB0B1F9FF685C75FF635235FF65554371000000006555
      432065554376655543C463533EF0614F34FF5E4E44FF6B5D52FF7D7492FF9895
      DBFFBFBEF0FFCACBFFFFD7D8FFFFCECEFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCC
      FFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFD1D2FFFF8886
      E9FF605049FF655440CC5C4E3E1B000000000000000000000000000000005D4E
      3E3064543EEE64554BFFA8A7F4FFD6D6FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1
      FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1
      FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1
      FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1
      FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1
      FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1
      FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1
      FFFFD2D2FFFFD7D7FFFF9591C0FF5A4A41FF655541DE6555431C000000000000
      000000000000000000006555433F65554390655440CE63523BFF62503CFF6150
      39FF6D616AFF8982AFFFA6A4E4FFC8C8FFFFD3D3FFFFD1D1FFFFD1D1FFFFD1D1
      FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD7D7FFFF8584
      F3FF5F4F4BFF65543ECC5C4E3E1B000000000000000000000000000000005D4E
      3E3064543EEE64554BFFABAAF4FFDADAFFFFD5D5FFFFD5D5FFFFD5D5FFFFD5D5
      FFFFD5D5FFFFD5D5FFFFD5D5FFFFD5D5FFFFD5D5FFFFD5D5FFFFD5D5FFFFD5D5
      FFFFD5D5FFFFD5D5FFFFD5D5FFFFD5D5FFFFD5D5FFFFD5D5FFFFD5D5FFFFD5D5
      FFFFD5D5FFFFD5D5FFFFD5D5FFFFD5D5FFFFD5D5FFFFD5D5FFFFD5D5FFFFD5D5
      FFFFD5D5FFFFD5D5FFFFD5D5FFFFD5D5FFFFD5D5FFFFD5D5FFFFD5D5FFFFD5D5
      FFFFD5D5FFFFD5D5FFFFD5D5FFFFD5D5FFFFD5D5FFFFD5D5FFFFD5D5FFFFD5D5
      FFFFD5D5FFFFD5D5FFFFD5D5FFFFD5D5FFFFD5D5FFFFD5D5FFFFD5D5FFFFD5D5
      FFFFDBDBFFFFC8C9FFFF6A62A4FF635233FF6555439000000000000000000000
      0000000000000000000000000000000000006555431D65554358655543A26555
      43EF63533CFF62502FFF534769FFBCBCEEFFDDDDFFFFD5D5FFFFD5D5FFFFD5D5
      FFFFD5D5FFFFD5D5FFFFD5D5FFFFD5D5FFFFD5D5FFFFD5D5FFFFDBDBFFFF8887
      F3FF5F4F4BFF65543ECC5C4E3E1B000000000000000000000000000000005D4E
      3E3064543EEE64554BFFAFAEF4FFDFDFFFFFDADAFFFFDADAFFFFDADAFFFFDADA
      FFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADA
      FFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADA
      FFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADA
      FFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADA
      FFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADA
      FFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADA
      FFFFDFE0FFFFACAAE8FF5D4E49FF65553FFC6555433500000000000000000000
      0000000000000000000000000000000000000000000000000000655543036656
      442264544272655541EF5A4B43FFAFADE0FFE0E0FFFFDADAFFFFDADAFFFFDADA
      FFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFE0E0FFFF8E8C
      EDFF5F504AFF65543FCC5C4E3E1B000000000000000000000000000000005D4E
      3E3064543EEE64554BFFB3B2F4FFE4E4FFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDF
      FFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDF
      FFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDF
      FFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDF
      FFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDF
      FFFFDFDFFFFFDFDFFFFFDFDFFFFFE0E0FFFFE0E0FFFFE2E4FFFFE5E5FFFFE0E0
      FFFFE0E0FFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFFE1E1
      FFFFE2E3FFFF7972AEFF604F38FF655543AE6555430200000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00005C4E3E0B645442D461503DFFA09DDCFFE3E4FFFFE0E0FFFFDFDFFFFFDFDF
      FFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFFE5E5FFFF918F
      EDFF5F504AFF65543FCC5C4E3E1B000000000000000000000000000000005D4E
      3E3064533EEE64554BFFB6B5F4FFE8EAFFFFE3E3FFFFE3E3FFFFE3E3FFFFE3E3
      FFFFE3E3FFFFE3E3FFFFE3E3FFFFE3E3FFFFE3E3FFFFE3E3FFFFE3E3FFFFE3E3
      FFFFE3E3FFFFE3E3FFFFE3E3FFFFE3E3FFFFE3E3FFFFE3E3FFFFE3E3FFFFE3E3
      FFFFE3E3FFFFE3E3FFFFE3E3FFFFE3E3FFFFE3E3FFFFE3E3FFFFE3E3FFFFE3E3
      FFFFE3E3FFFFE3E3FFFFE3E3FFFFE3E3FFFFE3E3FFFFE3E3FFFFE3E3FFFFE4E4
      FFFFE6E6FFFFEAEAFFFFEAEAFFFFE8E9FFFFE7E8FFFFCCCCFFFFBCBCFFFFE1E1
      FFFFE8E9FFFFE9E9FFFFEAEBFFFFE8E8FFFFE4E4FFFFE3E3FFFFE3E3FFFFECEC
      FFFFC2C2FFFF66595AFF62523CFF655543580000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00005B4D3D09655542BA604F3DFF8D89DAFFE5E6FFFFE4E4FFFFE3E3FFFFE3E3
      FFFFE3E3FFFFE3E3FFFFE3E3FFFFE3E3FFFFE3E3FFFFE3E3FFFFE9EBFFFF9392
      EEFF5F504AFF65543FCC5C4E3E1B000000000000000000000000000000005D4E
      3E3064533EEE64554BFFB9B8F4FFECEDFFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7
      FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7
      FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7
      FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7
      FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7FFFFE9E9FFFFF0F0FFFFF1F2
      FFFFE6E8FFFFDBDBFFFFCFCDEDFF9795E6FF7670ACFF6E6162FF695B5CFF7569
      77FF817ED5FFB5B2E6FFD6D5F5FFDEE0FFFFEEEFFFFFF3F4FFFFF2F3FFFFEEF0
      FFFF8984C2FF5B4B44FF655540CC655543150000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000063544204655542A6604F3EFF7E7AD2FFE7E8FFFFE8E8FFFFE7E7FFFFE7E7
      FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7FFFFE7E7FFFFEDEEFFFF9594
      EFFF5F504AFF64543FCC5C4E3E1B000000000000000000000000000000005D4E
      3E3064533EEE65554BFFBCBBF4FFF0F1FFFFEBEBFFFFEBEBFFFFEBEBFFFFEBEB
      FFFFEBEBFFFFEBEBFFFFEBEBFFFFEBEBFFFFEBEBFFFFEBEBFFFFEBEBFFFFEBEB
      FFFFEBEBFFFFEBEBFFFFEBEBFFFFEBEBFFFFEBEBFFFFEBEBFFFFEBEBFFFFEBEB
      FFFFEBEBFFFFEBEBFFFFEBEBFFFFEBEBFFFFEBEBFFFFEBEBFFFFEBEBFFFFEBEB
      FFFFEBEBFFFFEBEBFFFFECECFFFFF2F2FFFFF8F8FFFFEBECFFFFC8CAFFFFB8B5
      DDFF9993BCFF6A62A4FF605253FF605040FF5D4D3FFF604F39FD625038F35E4D
      3BFC5E4E42FF63523EFF625670FF7C76BAFFB0ABC5FFC0BEE9FFBCB9DBFF9793
      BFFF675A66FF635239FF6555436C000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000645543885F4F3DFF6E69B9FFEAEBFFFFEDEDFFFFEBEBFFFFEBEB
      FFFFEBEBFFFFEBEBFFFFEBEBFFFFEBEBFFFFEBEBFFFFEBEBFFFFF1F2FFFF9796
      EFFF5F504AFF64543FCC5C4E3E1B000000000000000000000000000000005D4E
      3E3064533EEE65554BFFBFBEF4FFF5F5FFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEF
      FFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEF
      FFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEF
      FFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFF0F0FFFFF2F2FFFFF5F5
      FFFFF4F5FFFFF5F6FFFFEDEDFFFFC3C4FFFFA19DDBFF918899FF675E8DFF594B
      59FF5D4B32FF63533BFF6A5A48FF5E4D3BFF5F4F3BFF5F4E3EFC5F4E44F16353
      43FC655541F263533FEA64533CFF615035FF5C4C3BFF594D69FF5A4C56FF5F4E
      34FF65543EFF665644C46555430C000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000006253416D604E3CFF6C6389FFEFEFFFFFF1F1FFFFEFEFFFFFEFEF
      FFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFF6F6FFFF9998
      EFFF5F504AFF64543FCC5C4E3E1B000000000000000000000000000000005D4E
      3E3064533EEE65554BFFC1C0F4FFF9F9FFFFF3F3FFFFF3F3FFFFF3F3FFFFF3F3
      FFFFF3F3FFFFF3F3FFFFF3F3FFFFF3F3FFFFF3F3FFFFF3F3FFFFF3F3FFFFF3F3
      FFFFF3F3FFFFF3F3FFFFF3F3FFFFF3F3FFFFF3F3FFFFF3F3FFFFF3F3FFFFF3F3
      FFFFF3F3FFFFF2F2FFFFF3F3FFFFF9F9FFFFFEFFFFFFF9FBFFFFEBECFFFFE5E5
      FAFFBDBCEBFF8481D5FF786E80FF695C60FF594A52FF5B4A36FF635133FB6453
      3EFF615035FFC2BB65FFE8E6C3FFE5E169FFC5BF38FF7D7061FF726335FF6252
      36FF5D4C47F9645443DD6252419A625341A7645441DE65553AF065553EEA6454
      43C0645442786454421A00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000005E4F3E5360503CFF695D65FFEFEFFFFFF5F6FFFFF3F3FFFFF3F3
      FFFFF3F3FFFFF3F3FFFFF3F3FFFFF3F3FFFFF3F3FFFFF3F3FFFFFAFAFFFF9C9B
      F0FF5F4F4BFF64543ECC5C4E3E1B000000000000000000000000000000005D4E
      3E3064533EEE65564BFFC4C3F4FFFBFBFFFFF5F5FFFFF5F5FFFFF5F5FFFFF5F5
      FFFFF5F5FFFFF5F5FFFFF5F5FFFFF5F5FFFFF5F5FFFFF5F5FFFFF5F5FFFFF5F5
      FFFFF5F5FFFFF5F5FFFFF5F5FFFFF5F5FFFFF5F5FFFFF5F5FFFFF5F5FFFFF5F5
      FFFFFAFAFFFFFFFFFFFFFFFFFFFFE1E3FFFFCACAF7FFB6B1C9FF807BBEFF6459
      7CFF635344FF5D4D42FF5E4D3BFF635238F564543CD1625241BF645442E65B4A
      42FE94893CFFFBFCC3FFFFFFF8FFFFFF9FFFE7E78DFFFEFE8CFFFFFF5EFFB9B3
      7FFF776932FF5D4B44FF645542D35C4E3E635A4C3C2F5F503F485E503E2D5A4C
      3C09000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000005E4F3E4362513CFC665963FFD6D6FFFFFAFAFFFFF5F5FFFFF5F5
      FFFFF5F5FFFFF5F5FFFFF5F5FFFFF5F5FFFFF5F5FFFFF5F5FFFFFCFCFFFF9C9B
      F0FF5F4F4BFF64543ECC5C4E3E1B000000000000000000000000000000005D4E
      3E2F64533EEE65564BFFC6C5F4FFFEFEFFFFF8F8FFFFF8F8FFFFF8F8FFFFF8F8
      FFFFF8F8FFFFF8F8FFFFF8F8FFFFF8F8FFFFF8F8FFFFF8F8FFFFF8F8FFFFF8F8
      FFFFF8F8FFFFF8F8FFFFF8F8FFFFFAFAFFFFFDFDFFFFFDFEFFFFFFFFFFFFFEFF
      FFFFE3E4FFFFB4B3FBFFA29CB3FF7D7395FF5B507AFF5C4B3CFF604F33FF6352
      3BFF64543FF4655542B5615240805E4F3F575D4E3F3F605040925E4D45F87466
      28FFE6E37DFFFFFFF9FFFFFEF9FFF4F3BAFFBDB92BFF988D18FFB0AC64FFFDFE
      B7FFEAE873FF807324FF5A4945FF645443E85E4F3E6F5A4C3C2C594C3C215A4C
      3C145A4C3C095A4C3C055A4C3C01000000000000000000000000000000000000
      000000000000000000000000000000000000000000005A4C3C015A4C3C015A4C
      3C02000000005D4F3E3C63533CEE675956FFC5C5FFFFFEFFFFFFF8F8FFFFF8F8
      FFFFF8F8FFFFF8F8FFFFF8F8FFFFF8F8FFFFF8F8FFFFF8F8FFFFFFFFFFFF9E9D
      F0FF5F4F4BFF64543ECB5C4E3E1A000000000000000000000000000000005D4E
      3E2F64533EED65564BFFC7C7F4FFFFFFFFFFFAFAFFFFFAFAFFFFFAFAFFFFFAFA
      FFFFFAFAFFFFFAFAFFFFFAFAFFFFFAFAFFFFFAFAFFFFFAFAFFFFFAFAFFFFF9F9
      FFFFFCFCFFFFFFFFFFFFFFFFFFFFFBFCFFFFF3F4FFFFE2E1F4FFA4A3F0FF817B
      B0FF776A6AFF5E5165FF574741FF5F4D32FF655438F8655542E4655543A26555
      435C6454422D5C4E3E0E00000000584B3B0F5C4D3E5C635343EC5C4B38FFB2AB
      59FFFFFFE7FFFFFEF9FFFFFEF1FFFFFFFFFFFAF9DBFFACA361FF503F23FF8D87
      1AFFFEFF74FFF7F782FF8D7F33FF57463DFF645443F46051408D5A4C3C325A4C
      3C265A4C3C1B5A4C3C0D5A4C3C04000000000000000000000000000000000000
      0000000000005A4C3C025A4C3C035A4C3C055A4C3C095A4C3C0C5A4C3C0F5A4C
      3C14594B3C0F5D4F3E3F64533BDE605257FFAFAEFEFFFFFFFFFFFAFAFFFFFAFA
      FFFFFAFAFFFFFAFAFFFFFAFAFFFFFAFAFFFFFAFAFFFFFAFAFFFFFFFFFFFF9F9E
      F0FF5F4F4BFF64543ECB5C4E3E1A000000000000000000000000000000005D4E
      3E2E64533EED65564BFFC8C8F4FFFFFFFFFFFCFCFFFFFCFCFFFFFCFCFFFFFCFC
      FFFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCFFFFFBFBFFFFFDFDFFFFFFFFFFFFFFFF
      FFFFFAFCFFFFDCDDFFFFCDCBE2FFA49FCAFF6F68ACFF655859FF605149FF5B4B
      41FF604E37FF655439E4665641C36656448E665644426656440C000000000000
      000000000000594B3C015A4C3C175A4C3C3D625341C05F4D41FF83772DFFFAF9
      BEFFFFFFFFFFFFFEF6FFFFFEF7FFFFFEF6FFFFFFFFFFFFFFDBFFBAB15AFF5544
      28FF847D12FFF4F27AFFFFFF8BFF8B7D4CFF5A493FFF655543FF605140925B4C
      3D38594C3C295A4C3C1C5A4C3C0F5A4C3C075A4C3C045A4C3C045A4C3C075A4C
      3C095A4C3C0E5A4C3C115A4C3C165A4C3C1C5A4C3C225A4C3C275A4C3C2E5A4C
      3C35594B3C355C4E3E5364533BD85B4D54FFA2A1F4FFFFFFFFFFFBFCFFFFFBFB
      FFFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCFFFFFFFFFFFFA09F
      F1FF5E4F4BFF65543ECA5D4E3E18000000000000000000000000000000005D4E
      3E2C64533EED65564BFFCACAF4FFFFFFFFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFE
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFCFFFFC7C8FFFFADA9
      D0FF988F96FF665D95FF594B58FF5E4D32FF615039FF63533EFF655440D96454
      409B5F503F685F503F3300000000000000000000000000000000000000000000
      0000594B3B055A4C3C125A4C3C275F503E82635344FF645435FFD7D379FFFFFF
      FFFFFFFEFEFFFFFEFBFFFFFEFBFFFFFEFBFFFFFEFBFFFFFFFFFFFFFFDBFFC1BA
      62FF57453FFF766624FFDFDE68FFFFFEC4FF94885DFF5A493BFF655442FF6152
      419D5A4D3C3B5A4C3C2A5A4C3C235A4C3C1A5A4C3C175A4C3C185A4C3C1D5A4C
      3C235A4C3C2A5A4C3C305A4C3C375A4C3C3E5A4C3C445A4C3C4A5A4C3C505A4C
      3C565A4C3C585A4C3C5E62523DCC5B4B48FF7E7BCEFFEFF0FFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFFFFFFFFA1A0
      F1FF5E4F4BFF65543EC85D4E3E16000000000000000000000000000000005C4E
      3E2B64533EEC65564BFFCBCAF4FFFFFFFFFFFEFEFFFFFEFEFFFFFEFEFFFFFFFF
      FFFFFFFFFFFFF9FAFFFFF6F5FAFFC1C0F3FF8E8AD0FF82777CFF675B6EFF5748
      51FF5B4A2FFF635234FC65553DF7645542D66253419D5E503E665D4E3E435C4D
      3D19594B3B04594B3B07594B3B09594B3B08594B3B04594B3B03594B3B015A4C
      3C025A4C3C0C5A4C3C1E5C4D3D54645343E15B4A3CFFA89F40FFFCFCDEFFFFFE
      FFFFFFFEFCFFFFFEFCFFFFFEFCFFFFFEFCFFFFFEFCFFFFFEFCFFFFFFFFFFFFFF
      EAFFC4BF82FF5B4B3EFF695A29FFBEB7A3FFF9F9C7FF9B9076FF5B4A3CFF6252
      3FFC625241AE5B4D3D455A4C3C335A4C3C365A4C3C355A4C3C395A4C3C3F5A4C
      3C465A4C3C4C5A4C3C50594C3C54594B3C58594B3C5B5A4C3C625A4C3C6C5B4C
      3C715B4D3C7B5E4F3E90625341D365553FFF615460FF726778FF817BAEFFA7A5
      EBFFDEDDEEFFF2F2FDFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFFFFFFFFA2A1
      F1FF5E4F4BFF65543EC85E4E3E14000000000000000000000000000000005F50
      3F2C64533FEE65564CFFD4D3FAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4F4
      FFFFB8B7F6FF6F6AC7FF65575CFF645238FF5B4C43FF5B4A39FF625036FF6555
      3CFD645443E1625341A55D4E3E695A4C3C505A4C3C47594C3C3D594B3C3A594C
      3C395A4C3C345A4C3C2D5A4C3C255A4C3C1E5A4C3C185A4C3C125A4C3C0D5A4C
      3C105A4C3C1B5A4C3C35605140AD5D4C43FF766938FFEBEAABFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFF3FFC6C184FF5D4D39FF59483CFFB2AA96FFFFFFE3FFA19687FF6151
      3FFF635341FF635342B75B4D3C55594B3C4F5A4C3C5B5A4C3C5C594C3C5E594B
      3C5C594C3C5E5B4D3C6A5C4D3D795C4E3D895D4E3E94605140AD625241C86454
      41DD66553FF6655437FC635034FE5A4937FF52423AFF4B3D53FF554A72FF6E65
      8AFF837CA2FFBBBAF4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA7A7
      F7FF5E4F4CFF64543ECA64554316000000000000000000000000000000006555
      4322645440E1635447FF9A98DFFFD6D7FFFFFFFFFFFFFFFFFFFFFFFFFFFFF1F2
      FFFFB6B6F2FFA39CA7FF837A93FF5D5388FF594946FF5E4D33FF615039FF6252
      3DFC63533EE563533FC760513FAB5E503F8E5D4F3E725A4D3C57594B3C4F5A4C
      3C4E5A4C3C4B5A4C3C465A4C3C405A4C3C395A4C3C335A4C3C2B5A4C3C265A4C
      3C295A4C3C345E4F3E80625244F3625231FFC6C05CFFFFFFFBFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFF1FFCBC690FF67583BFF59483FFFB8B35FFFFFFFCAFFACA3
      A4FF604F43FF5F4F41FF625341CB5B4D3D74594B3C6D5C4E3D885D4F3E955F50
      3EA660513FB762523FC763533FDF63533DF663533DFF63513AFF605036FF5A49
      3DFF54464CFF594F86FF6B639AFF8782A6FFA6A1B4FFABA9E7FFC4C4FFFFEEEE
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE7E8FFFFCBCCFFFF817E
      D7FF5F5046FF645440BC6555430C000000000000000000000000000000006555
      430465554395645441FF5C4E5DFF6D6174FF887C76FF908CCFFFB1B1F8FFE8E7
      F7FFFFFFFFFFFFFFFFFFF8FAFFFFE2E4FFFFD6D3DAFFA8A3CBFF716BB9FF675B
      66FF645547FF5C4D48FF5B4B3CFF614E36FD645338E963533FD5615341B55F50
      3F845B4D3D635A4C3C585A4C3C50594C3C47594C3C465A4C3C445A4C3C445A4C
      3C485B4D3D63635342D95B4B3FFF928945FFFFFFD4FFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFDFFD2CFAAFF6C5E3FFF584923FFA89F5DFFFFFF
      C7FFD4D06AFF71622BFF5C4B42FF645440DE63533FD9645338EB625237F45E4D
      39FF5A493CFF5B4B45FF5E4F4EFF5D505AFF605468FF6962A7FF8883CCFFA9A5
      D1FFD1CEDBFFDADAFFFFE9EBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFC
      FFFFEFEFF9FFC7C7FAFF9E9EF3FF8783C9FF837A8FFF71677FFF665B74FF5D4F
      59FF645441FF6555437A00000000000000000000000000000000000000000000
      000065554320655543A765543ADE615035F95A4936FF5A4B48FF61534FFF6B5C
      4FFF6D669CFF918DD5FFC6C2D4FFE6E5F0FFF0F2FFFFFBFEFFFFFFFFFFFFF9F9
      F9FFC6C5F1FF8F8DDFFF81788AFF726569FF5C4F65FF584740FF5F4E32FF6554
      37FA655541EE635343CC615240A25D4F3E7F5C4D3E6B5B4D3D5C594C3C575A4C
      3C67605140B5625145FF6C5E2EFFE5E298FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD3CFAAFF6A5B43FF51422AFFA8A3
      20FFFFFF9FFFE7E277FF6D5F3DFF534240FF564540FF574A62FF62566CFF6F64
      79FF7D748CFF827ECCFFA19FF1FFCFCFF5FFF0EFF7FFFDFEFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFCFDFFFFE7E9FFFFDCDBEFFFC7C4DDFF9C99D8FF7E7AD1FF6A62
      98FF65575DFF5E5161FF5D4E51FF584947FF58473BFF5D4C38FF635235F06655
      3BD96555439B6555431900000000000000000000000000000000000000000000
      0000000000000000000065554321655543556555438565553FB263533CDA6151
      3CFF5F4F3CFF5F4E3AFF5E4D38FF5B4F69FF6F68A7FF9A92A7FFBAB5C6FFC7C7
      FFFFEDEFFFFFFFFFFFFFFFFFFFFFE2E3FFFFB4B5FCFFA6A0B1FF837B95FF5C51
      80FF594A42FF5F4E34FF635139FF64533EFF645341E662533FC560513FAF5F50
      3EA9635443F35B4B3AFFB5AD58FFFFFFEBFFFFFFFFFFFFFEFBFFFFFEFBFFFFFE
      FBFFFFFEFBFFFFFEFBFFFFFEFBFFFFFEFBFFFFFEFBFFFFFEFBFFFFFEFBFFFFFE
      FBFFFFFEFBFFFFFEFBFFFFFEFBFFFFFFFFFFFFFFFFFFD6D2A4FF7B6D40FF503D
      3BFFA39C21FFF6F686FFDFDCA9FF8B849EFF9E98B1FFAAA9F5FFC1C1FFFFE8E9
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F9FFFFD0D1FFFFBCBDFFFFB5B2
      CDFF9C96B0FF847CAAFF625A98FF554A6BFF564749FF5B4C41FF604F3AFF604F
      3CFF61503CFF63523CEB64543DC865553FA76555428C65554365655543456555
      4317000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000065554308655543276555
      434665554376655543B1655543E265553CFE635034FF5D4B31FF564748FF5E53
      77FF7B717AFF928989FF9B99E3FFCACAFDFFFFFFFFFFFFFFFFFFEDEFFFFFD7D8
      FFFFCCC9D1FF9A95C3FF6C64A7FF635657FF615243FF5D4D43FF5D4C3AFF6250
      38FA5A4939FD84773BFFF1F0BCFFFFFFFFFFFFFEFAFFFFFEF9FFFFFEF9FFFFFE
      F9FFFFFEF9FFFFFEF9FFFFFEF9FFFFFEF9FFFFFEF9FFFFFEF9FFFFFEF9FFFFFE
      F9FFFFFEF9FFFFFEF9FFFFFEF9FFFFFEF9FFFFFEFCFFFFFFFFFFE4E1B7FF7D70
      42FF544626FF969203FFF7F794FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFC
      FFFFD2D1FEFFAFAFFDFF9391D7FF8A829BFF7B728DFF655A80FF594D74FF5445
      4EFF574639FF5F4E34FF645336FF66553CFE665640E9655542BE6555439C6555
      436E6555434A655543336555431A000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000065554306655543136555433765554374655543A965553ED76453
      36EE5E4C34FF594838FF5A4C50FF665853FF736662FF7772BFFFA19EDCFFE0DE
      E3FFEBECFFFFE6E9FFFFF2F4FFFFF0EFF7FFBAB8EBFF8884CDFF796E77FF685A
      63FF635543FFCEC97EFFFFFFFCFFFFFEF7FFFFFEF5FFFFFEF5FFFFFEF5FFFFFE
      F5FFFFFEF5FFFFFEF5FFFFFEF5FFFFFEF5FFFFFEF5FFFFFEF5FFFFFEF5FFFFFE
      F5FFFFFEF5FFFFFEF5FFFFFEF5FFFFFEF5FFFFFEF5FFFFFEF9FFFFFFFFFFE3E0
      AEFF7E7142FF55472AFF878406FFDFE082FFFFFFE7FFCDCB8BFF645FA9FF685B
      74FF64575FFF615356FF594A4DFF57473DFF5B4A39FF635236F3665537E86756
      3ED8675643B46555438E6555435B65554331655543156555430A655543010000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000655543056555
      43386555436C6555429465543DBE63523CEB5F4F3CFF5F4E3CFF5F4F39FF5F4F
      41FF5D5486FF7972ACFFACA5B1FFBFBDE8FFCBCDFFFFF2F4FFFFFFFFFFFFD0D0
      FFFFC1BECEFFFFFFE2FFFFFFF2FFFFFEEDFFFFFEEEFFFFFEEEFFFFFEEEFFFFFE
      EEFFFFFEEEFFFFFEEEFFFFFEEEFFFFFEEEFFFFFEEEFFFFFEEEFFFFFEEEFFFFFE
      EEFFFFFEEEFFFFFEEEFFFFFEEEFFFFFEEEFFFFFEEEFFFFFEEDFFFFFEEFFFFFFF
      FCFFE7E5ABFF8D8045FF57492AFF797210FFD8D77AFFF5F696FF918922FF5645
      33FF5F4E3EFF63533BE963533DBC62523FA761513F88605140536051402F6051
      4006000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000006555431065554335655543596555438B655543BE6555
      41F5655439FF615034FF5B4A34FF584B5EFF645A81FF83777FFF8F89B3FFBEBC
      E1FFFFFFEDFFFFFFEFFFFFFEEAFFFFFEEAFFFFFEEAFFFFFEEAFFFFFEEAFFFFFE
      EAFFFFFEEAFFFFFEEAFFFFFEEAFFFFFEEAFFFFFEEAFFFFFEEAFFFFFEEAFFFFFE
      EAFFFFFEEAFFFFFEEAFFFFFEEAFFFFFEEAFFFFFEEAFFFFFEEAFFFFFEEAFFFFFE
      EAFFFFFFF9FFF5F4AEFF948744FF52403FFF655927FFC9C380FFFAFAA2FFA7A0
      31FF5E4D38FF605045ED635341A05A4C3C24594B3B17594B3B12594B3B08594B
      3B02000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000006655430B6656
      441E6555434C63534286645442C565543AE5635234F55B4A34FF514136FFB8B2
      6EFFFFFFDCFFFFFEE6FFFFFEE2FFFFFEE2FFFFFEE2FFFFFEE2FFFFFEE2FFFFFE
      E2FFFFFEE2FFFFFEE2FFFFFEE2FFFFFEE2FFFFFEE2FFFFFEE2FFFFFEE2FFFFFE
      E2FFFFFEE2FFFFFEE2FFFFFEE2FFFFFEE2FFFFFEE2FFFFFEE2FFFFFEE2FFFFFE
      E2FFFFFEE4FFFFFFF0FFFBF9A4FF988C3DFF513E3CFF604F46FFCCC74AFFFFFF
      81FFB5AE38FF604F3CFF5E4D49FF625341AB5D4E3D475A4C3C265A4C3C1F5A4C
      3C115A4C3C07594B3B0200000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000594B3B015A4C3C155D4F3F59635442D65B4B3FFF8C8141FFF7F6
      A7FFFFFFE5FFFFFEDBFFFFFEDBFFFFFEDBFFFFFEDBFFFFFEDBFFFFFEDBFFFFFE
      DBFFFFFEDBFFFFFEDBFFFFFEDBFFFFFEDBFFFFFEDBFFFFFEDBFFFFFEDBFFFFFE
      DBFFFFFEDBFFFFFEDBFFFFFEDBFFFFFEDBFFFFFEDBFFFFFEDBFFFFFEDBFFFFFE
      DBFFFFFEDBFFFFFEDCFFFFFFE7FFFBFA9EFF998E48FF564440FF655A1DFFC5BF
      37FFFFFF86FFC5BE3EFF625621FF604F46FF645442CB5D4E3D4D5A4C3C2D5A4C
      3C245A4C3C135A4C3C075A4C3C02000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000594C3C055A4C3C11594C3C295F503F8E614F43F8685934FFDEDA70FFFFFF
      DFFFFFFED5FFFFFED5FFFFFED5FFFFFED5FFFFFED5FFFFFED5FFFFFED5FFFFFE
      D5FFFFFED5FFFFFED5FFFFFED5FFFFFED5FFFFFED5FFFFFED5FFFFFED5FFFFFE
      D5FFFFFED5FFFFFED5FFFFFED5FFFFFED5FFFFFED5FFFFFED5FFFFFED5FFFFFE
      D5FFFFFED5FFFFFED5FFFFFED5FFFFFFE0FFFAF9AEFF9F9457FF564542FF5A49
      38FFAFA731FFFFFF82FFD1D04AFF695F21FF5E4D47FF635443CF5C4E3E5B594C
      3C2C5A4C3C1E5A4C3C0D5A4C3C04000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000005A4C
      3C035A4C3C0D5A4C3C1F5B4D3D5D655542EE5B4B40FFA79F4EFFFFFFBFFFFFFF
      D1FFFFFECCFFFFFECCFFFFFECCFFFFFECCFFFFFECCFFFFFECCFFFFFECCFFFFFE
      CCFFFFFECCFFFFFECCFFFFFECCFFFFFECCFFFFFECCFFFFFECCFFFFFECCFFFFFE
      CCFFFFFECCFFFFFECCFFFFFECCFFFFFECCFFFFFECCFFFFFECCFFFFFECCFFFFFE
      CCFFFFFECCFFFFFECCFFFFFECCFFFFFECDFFFFFFD6FFFDFDAFFFA39953FF5645
      3CFF5C4A33FF8C8168FFFDFD95FFCECA69FF685A3CFF604F40FF635344D65D4F
      3E57594C3C1C5A4C3C125A4C3C06000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000005A4C3C015A4C
      3C085A4C3C195A4D3D3D625241BF5F4E44FF7D7133FFF2EF8CFFFFFFCCFFFFFE
      C2FFFFFEC2FFFFFEC2FFFFFEC2FFFFFEC2FFFFFEC2FFFFFEC2FFFFFEC2FFFFFE
      C2FFFFFEC2FFFFFEC2FFFFFEC2FFFFFEC2FFFFFEC2FFFFFEC2FFFFFEC2FFFFFE
      C2FFFFFEC2FFFFFEC2FFFFFEC2FFFFFEC2FFFFFEC2FFFFFEC2FFFFFEC2FFFFFE
      C2FFFFFEC2FFFFFEC2FFFFFEC2FFFFFEC2FFFFFEC3FFFFFFCAFFFFFFA3FFA59C
      5FFF5D4C41FF58473DFF8F8829FFF7F779FFE0DF59FF766B25FF5D4C45FF6555
      43DB5E4F3E4F594C3C045A4C3C045A4C3C010000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000005A4C3C055A4C
      3C12594B3C255E4F3F83635243FF61523DFFC9C563FFFFFFB6FFFFFEBBFFFFFE
      B8FFFFFEB8FFFFFEB8FFFFFEB8FFFFFEB8FFFFFEB8FFFFFEB8FFFFFEB8FFFFFE
      B8FFFFFEB8FFFFFEB8FFFFFEB8FFFFFEB8FFFFFEB8FFFFFEB8FFFFFEB8FFFFFE
      B8FFFFFEB8FFFFFEB8FFFFFEB8FFFFFEB8FFFFFEB8FFFFFEB8FFFFFEB8FFFFFE
      B8FFFFFEB8FFFFFEB8FFFFFEB8FFFFFEB8FFFFFEB8FFFFFEBCFFFFFFC5FFFFFF
      B9FFB1A96DFF645543FF5C4C3EFF8B8418FFEFEF60FFE5E55AFF807526FF5C4B
      45FF655444DE5E503F4200000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005A4C3C025A4C3C0B5A4C
      3C1C5C4D3D50635342D85B4A3FFF968A40FFFCFB9AFFFFFDB9FFFFFDAEFFFFFD
      AFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFD
      AFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFD
      AFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFD
      AEFFFFFDB6FFFFFEBBFFFFFFB9FFFFFFB9FFFFFFB7FFFFFFA8FFFFFF9BFFFFFF
      A1FFF6F597FF988F4EFF5C4B3EFF524145FF766E1CFFE6E353FFEDEB53FF877C
      24FF5A4945FF655545E35E503F3B000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005A4C3C065A4C3C125A4C
      3C2E615241A8604F43FF6F613EFFE8E56EFFFFFFAAFFFFFDA3FFFFFDA3FFFFFD
      A3FFFFFDA3FFFFFDA3FFFFFDA3FFFFFDA3FFFFFDA3FFFFFDA3FFFFFDA3FFFFFD
      A3FFFFFDA3FFFFFDA3FFFFFDA3FFFFFDA3FFFFFDA3FFFFFDA3FFFFFDA3FFFFFD
      A3FFFFFDA3FFFFFDA3FFFFFDA3FFFFFDA3FFFFFEAAFFFFFFAEFFFFFFACFFFFFF
      AFFFFFFF89FFFFFF6BFFF2F172FFDEDB7AFFCDC974FFC7C249FFB3AC26FF988F
      2BFF756B33FF635636FF695B44FF877B27FF8C8033FFAAA13EFFFFFF74FFFCFD
      6AFF9D9326FF625042FF655544A5655543050000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000005A4C3C015A4C3C095A4C3C155E50
      3F65655543EF5D4D3FFFB9B248FFFFFF95FFFFFD9AFFFFFD99FFFFFD99FFFFFD
      99FFFFFD99FFFFFD99FFFFFD99FFFFFD99FFFFFD99FFFFFD99FFFFFD99FFFFFD
      99FFFFFD99FFFFFD99FFFFFD99FFFFFD99FFFFFD99FFFFFD99FFFFFD99FFFFFD
      99FFFFFD99FFFFFD99FFFFFD9AFFFFFE9CFFFDFA6AFFE6E448FFC6C25AFFB1AB
      5FFFA8A137FF9C911BFF837826FF6D612FFF60513FFF7C7037FFA1991BFFB7AE
      20FFBDB64AFFC5BD85FFD6D282FFFAFB2FFFFFFF08FFF1F02CFFB9B188FFB0A8
      78FF9B9032FF65553FFF655544AB655543060000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000005A4C3C015A4C3C065A4C3C186455
      42C35E4D43FF817641FFF7F574FFFFFF92FFFFFD8DFFFFFD8CFFFFFD8CFFFFFD
      8CFFFFFD8CFFFFFD8CFFFFFD8CFFFFFD8CFFFFFD8CFFFFFD8CFFFFFD8CFFFFFD
      8CFFFFFD8CFFFFFD8CFFFFFD8CFFFFFD8CFFFFFD8CFFFFFD8CFFFFFD8CFFFFFD
      8CFFFFFD8CFFFFFD8CFFFFFD92FFFFFF77FFC7C01FFF6C5F29FF675834FF5B4B
      34FF59483CFFB2AB26FFDBD620FFE0DB54FFDCD97CFFE2DE7CFFF3F130FFF6F2
      07FFDEDC17FFC5C32CFF9B9730FF877A50FF847736FF776933FF605046FF5A49
      3FFF5B4A45FF655544E765554349000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000005A4C3C015A4C3D016152416D6252
      44FC66563BFFD4CF59FFFFFF88FFFFFD84FFFFFD82FFFFFD83FFFFFD82FFFFFE
      80FFFFFE80FFFFFE84FFFFFF85FFFFFD83FFFFFD82FFFFFD82FFFFFD82FFFFFD
      82FFFFFD82FFFFFD82FFFFFD82FFFFFD82FFFFFD82FFFFFD82FFFFFD82FFFFFD
      82FFFFFD82FFFFFD82FFFFFD8AFFFFFF65FFB7B01BFF534243FE625146F46150
      43FF675848FFFAF770FFEFED31FFB7B119FF9A9325FF70614FFF6B5B47FF6959
      41FF65563CFF625338FF5D4D39FF5B4A42FF5C4B46F65E4D46E4635341CC6554
      42BB6555438F6555432D00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005D4F3E25655543DD5C4B
      43FF9F9647FFFEFE6AFFFFFF7BFFFFFF76FFFFFF77FFFFFF77FFFFFF7AFFFEFC
      88FFFEFC89FFFAF771FFF6F462FFFFFD73FFFFFE77FFFFFD76FFFFFD76FFFFFD
      76FFFFFD76FFFFFD76FFFFFD76FFFFFD76FFFFFD76FFFFFD76FFFFFD76FFFFFD
      76FFFFFD76FFFFFD76FFFFFD7BFFFFFF5DFFBAB21BFF5A4A41FA655544D66352
      42FF68584AFFFCF95CFFBDB425FF513F48FF5F4E43FF625141FF625243F76252
      44DB635344BD645344A46554447F655543696555434C65554332655543110000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000635442825F4F43FF796C
      41FFF1EF5CFFFFFF6EFFF8F663FFDFDC8BFFDDDA71FFD9D542FFBBB63DFFA098
      5FFF918849FF8C842FFFB3AE2CFFFBF95EFFFFFF6CFFFFFD68FFFFFD68FFFFFD
      68FFFFFD68FFFFFD68FFFFFD68FFFFFD68FFFFFD68FFFFFD68FFFFFD68FFFFFD
      68FFFFFD68FFFFFD68FFFFFD6DFFFFFF51FFB8B01BFF5A4A41FA655544D86352
      42FF685848FFFAF750FFBFB724FF594847FF645442BF6353423D6555432D6555
      431E6555430E0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000065554308645443B9605043FFBAB4
      39FFFFFF3FFFFFFF55FFF6F433FFB5B041FF978F1EFF99901EFF9F9836FFB5B0
      3CFFD8D60FFFEEEC00FFF5F327FFFFFD5CFFFFFC5DFFFFFC5CFFFFFC5CFFFFFC
      5CFFFFFC5CFFFFFC5CFFFFFC5CFFFFFC5CFFFFFC5CFFFFFC5CFFFFFC5CFFFFFC
      5CFFFFFC5CFFFFFC5CFFFFFD61FFFFFF47FFB6AF19FF5A4B41FA655544D86352
      43FF685848FFF9F646FFBDB522FF5A4947FF635342A400000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000065554304645443A7645444FF8D82
      33FFA9A118FFB4AD32FFC5BF46FFDEDC33FFEEEB1BFFE5E227FFD6D14AFFC2BC
      45FFB8B12EFFA79E20FFBDB715FFFDFA41FFFFFE52FFFFFC4FFFFFFC4FFFFFFC
      4FFFFFFC4FFFFFFC4FFFFFFC4FFFFFFC4FFFFFFC4FFFFFFC4FFFFFFC4FFFFFFC
      4FFFFFFC4FFFFFFC4FFFFFFD53FFFFFF3CFFB5AC18FF5B4B41FA655544D86352
      43FF685847FFF9F53CFFBBB41FFF5A4947FF635342AA00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000006555434C655543EC5E4D
      45FF5A4945FF594A3CFF63543AFF716439FF7E7041FF776944FF6B5D40FF5F50
      3CFF59493FFF524149FF8C8131FFF9F738FFFFFF45FFFFFC43FFFFFC43FFFFFC
      43FFFFFC43FFFFFC43FFFFFC43FFFFFC43FFFFFC43FFFFFC43FFFFFC43FFFFFC
      43FFFFFC43FFFFFC43FFFFFD46FFFFFF32FFB3AB17FF5B4B42FA655544D86352
      43FF685846FFF8F433FFBAB21EFF5A4948FF635342AA00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000006555432C6555
      436B6555448F655444BB635344D3605045E75D4C44F85E4E44F1615143E06353
      44CD655444D25D4D47FF90872EFFF9F72EFFFFFF37FFFFFC36FFFFFC36FFFFFC
      36FFFFFC36FFFFFC36FFFFFC36FFFFFC36FFFFFC36FFFFFC36FFFFFC36FFFFFC
      36FFFFFC36FFFFFC36FFFFFD38FFFFFF27FFB1AA16FF5B4B42FA655544D86352
      43FF685945FFF7F429FFB9B01DFF5A4948FF635342AA00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000006555430C6555432F6555434F65554343665643240000
      0000625241755E4D46FF8E852FFFF7F523FFFFFF29FFFFFC28FFFFFC28FFFFFC
      28FFFFFC28FFFFFC28FFFFFC28FFFFFC28FFFFFC28FFFFFC28FFFFFC28FFFFFC
      28FFFFFC28FFFFFC28FFFFFD2AFFFFFF1BFFAFA814FF5B4B42FA655544D86352
      43FF685945FFF6F31FFFB7AE1AFF5B4A48FF635342AA00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00006253427E5E4D47FF8F852CFFF8F618FFFFFF1CFFFFFC1CFFFFFC1CFFFFFC
      1CFFFFFC1CFFFFFC1CFFFFFC1CFFFFFC1CFFFFFC1CFFFFFC1CFFFFFC1CFFFFFC
      1CFFFFFC1CFFFFFC1CFFFFFD1DFFFFFF11FFADA615FF5C4C42FA655544D86352
      44FF695943FFF5F215FFB5AC1BFF5B4A48FF635342AA00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00006253427E5E4D47FF8E842AFFF6F310FFFFFE14FFFDFA14FFFDFA14FFFDFA
      14FFFDFA14FFFDFA14FFFDFA14FFFDFA14FFFDFA14FFFDFA14FFFDFA14FFFDFA
      14FFFDFA14FFFDFA14FFFEFB14FFFFFF0BFFABA315FF5C4C42FA655544D86352
      44FF695943FFF2EF0FFFB2AA1BFF5B4A48FF635342AA00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00006253427E5E4D47FF8C832AFFF2EF0DFFFDFA10FFF9F610FFF9F610FFF9F6
      10FFF9F610FFF9F610FFF9F610FFF9F610FFF9F610FFF9F610FFF9F610FFF9F6
      10FFF9F610FFF9F610FFFBF811FFFDFB08FFA7A116FF5C4C42FA655544D86353
      44FF695943FFEEEB0CFFADA71BFF5C4A49FF635342AA00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00006253427E5F4E47FF8A822AFFEEEC0BFFFAF70CFFF6F30DFFF6F30DFFF6F3
      0DFFF6F30DFFF6F30DFFF6F30DFFF6F30DFFF6F30DFFF6F30DFFF6F30DFFF6F3
      0DFFF6F30DFFF6F30DFFF8F50EFFF9F706FFA59F16FF5D4D42FA655544D86353
      44FF695942FFECE80AFFABA51BFF5C4B49FF635342AA00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00006253427E5F4E47FF89802AFFEBE907FFF6F308FFF2EF09FFF2EF09FFF2EF
      09FFF2EF09FFF2EF09FFF2EF09FFF2EF09FFF2EF09FFF2EF09FFF2EF09FFF2EF
      09FFF2EF09FFF2EF09FFF4F10AFFF5F303FFA49C16FF5D4D42FA655544D86353
      44FF695942FFE8E408FFA9A11BFF5C4B49FF635342AA00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00006253427E5F4E47FF8B8227FFE8E603FFF2EF04FFEEEB05FFEEEB05FFEEEB
      05FFEEEB05FFEEEB05FFEEEB05FFEEEB05FFEEEB05FFEEEB05FFEEEB05FFEEEB
      05FFEEEB05FFEEEB05FFF0ED05FFF1EF00FFA19A16FF5D4D42FA655544D86353
      44FF695942FFE4E005FFA79E1BFF5C4B49FF635342AA00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00006253427E5F4E47FF8A8126FFE5E301FFEFEC01FFEBE802FFEBE802FFEBE8
      02FFEBE802FFEBE802FFEBE802FFEBE802FFEBE802FFEBE802FFEBE802FFEBE8
      02FFEBE802FFEBE802FFEDEA02FFEDEB00FF9F9816FF5E4E42FA655544D86353
      44FF685943FFE1DD03FFA59C1BFF5D4C49FF635342AA00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00006253427E5F4E47FF887F26FFE1DE00FFEAE700FFE7E400FFE7E400FFE7E4
      00FFE7E400FFE7E400FFE7E400FFE7E400FFE7E400FFE7E400FFE7E400FFE7E4
      00FFE7E400FFE7E400FFE9E600FFE9E700FF9D9616FF5E4E42FA655544D86353
      44FF685943FFDDDA01FFA29A1BFF5D4C49FF635342AA00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00006253427E5F4E47FF877E26FFDCDA00FFE5E200FFE2DF00FFE2DF00FFE2DF
      00FFE2DF00FFE2DF00FFE2DF00FFE2DF00FFE2DF00FFE2DF00FFE2DF00FFE2DF
      00FFE2DF00FFE2DF00FFE4E100FFE4E200FF9B9416FF5E4E42FA655544D86353
      44FF685843FFD8D501FF9F981BFF5D4C49FF635342AA00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00006253427E604E48FF867E25FFD9D700FFE1DE00FFDEDB00FFDEDB00FFDEDB
      00FFDEDB00FFDEDB00FFDEDB00FFDEDB00FFDEDB00FFDEDB00FFDEDB00FFDEDB
      00FFDEDB00FFDEDB00FFDFDC00FFE0DE00FF999216FF5E4E42FA645444DA6353
      44FF685843FFD5D201FF9E961BFF5E4D49FF635342A900000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00006253427D5F4E48FF877E22FFD6D400FFDDDA00FFDAD700FFDAD700FFDAD7
      00FFDAD700FFDAD700FFDAD700FFDAD700FFDAD700FFDAD700FFDAD700FFDAD7
      00FFDAD700FFDAD700FFDBD800FFDCDA00FF989116FF5E4F42FB645444DC6353
      44FF685843FFD1CE01FF9C941BFF5E4D49FF635342A800000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00006253427C604F48FF867D22FFD2D000FFD9D600FFD6D300FFD6D300FFD6D3
      00FFD6D300FFD6D300FFD6D300FFD6D300FFD6D300FFD6D300FFD6D300FFD6D3
      00FFD6D300FFD6D300FFD7D400FFD8D600FF978F16FF5F4F42FB645444DD6353
      44FF685843FFCECA01FF9B921BFF5E4D49FF635342A800000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00006353427A604F48FF857C22FFCECD00FFD5D300FFD2D000FFD2D000FFD2D0
      00FFD2D000FFD2D000FFD2D000FFD2D000FFD2D000FFD2D000FFD2D000FFD2D0
      00FFD2D000FFD2D000FFD3D100FFD4D300FF958E16FF5F4F42FA645544DA6353
      44FF685843FFC9C801FF99911BFF5E4D49FF635342A600000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000063534279604F48FF847B22FFCAC900FFD1CF00FFCECC00FFCECC00FFCECC
      00FFCECC00FFCECC00FFCECC00FFCECC00FFCECC00FFCFCD00FFD0CE00FFD1CF
      00FFD1CF00FFD2D000FFD4D300FFD6D500FF958F16FF5E4E41FE645443F86150
      41FF66563FFFC5C401FF96901BFF5E4D49FF635342A500000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000063534278604F49FF847B20FFC8C700FFCFCB00FFCDCA00FFCECB00FFCECC
      00FFCFCC00FFD0CD00FFD1CE00FFD1CF00FFD3D100FFCECA00FFC8C500FFC3C0
      00FFBFBC00FFB9B700FFB5B200FFAFAE00FF837C19FF5E4E4EFF635352FF6656
      58FF6C5E57FFC9C602FF989215FF5E4D49FF645442A700000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000064544378614E48FF857C1DFFC9C800FFD0CE00FFC5C300FFBFBD00FFBBB8
      00FFB4B300FFADAB00FFA8A700FFA1A000FF9E9D00FFA3A100FF9F9F00FFA3A1
      00FFA3A100FFA5A400FFA2A000FFA5A500FFA6A208FFA6A023FFA09A27FF9F9A
      2FFFA09C2FFFB0AE0CFF7F743AFF625144FF6555439100000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000065554365625147FF7C7224FFAFAD02FFBAB800FFBFBD00FFBAB703FFABA8
      04FFA6A305FFA19E05FF9E9B0CFFA09C18FF9E9B1CFF9A9728FF8D8824FF847D
      28FF817A28FF80772BFF80752DFF837830FF837632FF83772DFF7F732DFF7B6F
      2CFF796D2CFF6E6039FF64544BFF655542FF6555433E00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00006555432A655543F7665642FF6A5C34FF6B602DFF6E622DFF6D6036FF6C5F
      38FF6A5C3DFF6B5B42FF6B5B41FF6D5C43FF6D5E43FF6B5A45FF685844FF6555
      45FF635345FF615046FF614F46FF5F4E46FF5F4D47FF5F4D47FF604F47FF6050
      47FF615047F7625146E8655442D4655543896555430300000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000065554350655444AD635345E4635345F6635245FF635345FF6353
      45FF635344FF645344FF645344FF635344FF635344FF645343FF645443D76454
      43B26554439B6555438A655543836555437E6555437A6555437A655543676555
      43576555434C6555433565554316000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000006555430765554327655543326555433F655543436555
      434365554343655543416555433F6555433F6555433F65554338655543206555
      430A000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFE0FFFFFFFFFFFFFFFFFFFFFF003FFFFFFFFFFFFFFFFFFFFC0
      00FFFFFFFFFFFFFFFFFFFF00003FFFFFFFFFFFFFFFFFF8000007FFFFFFFFFFFF
      FFFFE0000003FFFFFFFFFFFFFFFF80000000FFFFFFFFFFFFFFFE000000001FFF
      FFFFFFFFFFF80000000007FFFFFFFFFFFFC00000000001FFFFFFFFFFFF000000
      0000007FFFFFFFFFFC0000000000001FFFFFFFFFF000000000000007FFFFFFFF
      C000000000000001FFFFFFFF00000000000000007FFFFFF80000000000000000
      1FFFFFE0000000000000000007FFFF80000000000000000001FFFC0000000000
      00000000003FF0000000000000000000001FE00000000000000000000007C000
      00000000000000000003C00000000000000000000003C0000000000000000000
      0003C00000000000000000000003C00000000000000000000007C00000000000
      000000000003C00000000000000000000003C00000000000000000000003C000
      00000000000000000003C00000000000000000000003C0000000000000004000
      0003C00000000000000078000003C000000000000000FE000003C00000000000
      0000FF800003C000000000000000FFE00003C000000000000001FFE00003C000
      000000000001FFE00003C000000000000003FFF00003C000000000000003FFF0
      0003C000000000000007FFF00003C00000000000001FFFF00003C00000000000
      0003FF100003C000000004000003F0000003C00000007000000000000003C000
      0007E000000000000003C00000000000000000000003C0000000000000000000
      0003C00000000000000000000003C00000000000000000000007E00000000000
      000000000007F8000000000000000000001FFF00000000000000000003FFFFF0
      00000000000000003FFFFFFF800000000000001FFFFFFFFFF80000000000001F
      FFFFFFFFFF80000000000007FFFFFFFFFFF0000000000003FFFFFFFFFFE00000
      00000003FFFFFFFFFFC0000000000003FFFFFFFFFF80000000000001FFFFFFFF
      FF80000000000007FFFFFFFFFF00000000000003FFFFFFFFFF00000000000001
      FFFFFFFFFE00000000000001FFFFFFFFFE00000000000003FFFFFFFFFE000000
      00000007FFFFFFFFFF0000000000003FFFFFFFFFFF00000000000FFFFFFFFFFF
      FE0000000000FFFFFFFFFFFFFE0000000000FFFFFFFFFFFFFF0000000000FFFF
      FFFFFFFFFF8000000000FFFFFFFFFFFFFFF820000000FFFFFFFFFFFFFFFFE000
      0000FFFFFFFFFFFFFFFFE0000000FFFFFFFFFFFFFFFFE0000000FFFFFFFFFFFF
      FFFFE0000000FFFFFFFFFFFFFFFFE0000000FFFFFFFFFFFFFFFFE0000000FFFF
      FFFFFFFFFFFFE0000000FFFFFFFFFFFFFFFFE0000000FFFFFFFFFFFFFFFFE000
      0000FFFFFFFFFFFFFFFFE0000000FFFFFFFFFFFFFFFFE0000000FFFFFFFFFFFF
      FFFFE0000000FFFFFFFFFFFFFFFFE0000000FFFFFFFFFFFFFFFFE0000000FFFF
      FFFFFFFFFFFFE0000000FFFFFFFFFFFFFFFFE0000000FFFFFFFFFFFFFFFFE000
      0000FFFFFFFFFFFFFFFFE0000000FFFFFFFFFFFFFFFFF0000003FFFFFFFFFFFF
      FFFFF8001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF28000000480000009000
      0000010020000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000005A4C3C015A4C3C035A4C3C035A4C3C01000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00005A4C3C035A4C3C0C5A4C3C175A4C3C205A4C3C205A4C3C165A4C3C0A5A4C
      3C03000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000005A4C3C055A4C
      3C0F5A4C3C1F5A4C3C355A4C3C4B5A4C3C5A594C3C595A4C3C485A4C3C305A4C
      3C1B5A4C3C0C5A4C3C0300000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000005A4C3C015A4C3C025A4C3C085A4C3C135A4C3C245A4C
      3C3B5A4C3C555A4C3C745B4D3C995E4F3FC45F4F3FC15B4D3D96594B3C6C5A4C
      3C4F5A4C3C345A4C3C1D5A4C3C0E5A4C3C040000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000005A4C3C025A4C3C0A5A4C3C175A4C3C285A4C3C41594C3C5C5A4C
      3C7B5C4D3DA961523EDD665538FF65543DFF65543DFF665538FD60513FD35B4D
      3D9D5A4C3C705A4C3C505A4C3C365A4C3C1F5A4C3C0F5A4C3C05000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000005A4C
      3C045A4C3C0C5A4C3C1A5A4C3C2F5A4C3C475A4C3C625A4C3C845D4E3DB66253
      3CE7665538FF64543BFF5C5176FF5251CAFF5551C8FF5D516BFF665537FF6554
      39FF61523DDB5B4D3DA1594C3C735A4C3C555A4C3C395A4C3C215A4C3C105A4C
      3C06000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005A4C3C065A4C3C0F5A4C
      3C1F5A4C3C345A4C3C4E594B3C6A5A4C3C915E4F3DC163533DF1675534FF6152
      49FF5B5284FF5856DBFF5B5CFFFF5F60FFFF5F60FFFF5A5BFFFF5553CCFF5A50
      7AFF63533FFF665538FD61513DDD5C4E3CA85A4C3C7A5A4C3C575A4C3C3C5A4C
      3C245A4C3C125A4C3C075A4C3C01000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000005A4C3C025A4C3C085A4C3C135A4C3C255A4C3C3D5A4C
      3C56594B3C735B4D3D9D60513FD4655539F9655438FF5F5256FF5750A7FF5B5A
      EDFF6061FFFF6363FFFF6363FFFF6262FFFF6262FFFF6363FFFF6364FFFF5F60
      FFFF5A58DBFF5A517FFF625343FF675535FF62523FE85C4E3DAF5A4C3C7F594C
      3C5C5A4C3C405A4C3C275A4C3C155A4C3C095A4C3C0200000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00005A4C3C035A4C3C0B5A4C3C175A4C3C2A5A4C3C425A4C3C5D5A4C3C7E5C4E
      3EAC61523EDC665537FF64533BFF5C4F6BFF5C57BAFF5E5FFEFF6868FFFF6869
      FFFF6868FFFF6868FFFF6868FFFF6868FFFF6868FFFF6868FFFF6868FFFF6868
      FFFF6869FFFF6566FFFF5D5BE4FF595196FF615248FF665535FF63533CEA5D4F
      3EBB5A4C3C845A4C3C5F5A4C3C445A4C3C2A5A4C3C165A4C3C095A4C3C020000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000005A4C3C045A4C
      3C0D5A4C3C1A5A4C3C2F5A4C3C495A4C3C645A4C3C865D4F3EB963533CEB6555
      35FF615240FF5A5081FF5C5AD3FF6767FFFF6C6DFFFF6D6DFFFF6C6CFFFF6C6C
      FFFF6C6CFFFF6C6CFFFF6C6CFFFF6C6CFFFF6C6CFFFF6C6CFFFF6C6CFFFF6C6C
      FFFF6C6CFFFF6C6CFFFF6E6EFFFF6A6BFFFF605FEFFF5A5299FF5F504FFF6655
      35FF64543BF05E4F3EBD5A4C3C8A5A4C3C645A4C3C465A4C3C2C5A4C3C175A4C
      3C0A5A4C3C020000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000005A4C3C015A4C3C065A4C3C105A4C3C205A4C
      3C355A4C3C4E594C3C6B5A4D3D935E503DC563533DF1665532FF5F504CFF5C53
      90FF6260E5FF6C6EFFFF7373FFFF7272FFFF7171FFFF7171FFFF7171FFFF7171
      FFFF7171FFFF7171FFFF7171FFFF7171FFFF7171FFFF7171FFFF7171FFFF7171
      FFFF7171FFFF7171FFFF7171FFFF7171FFFF7374FFFF6E70FFFF6766F6FF5B55
      A8FF5F5052FF665532FF64543BF65E503DC65A4C3C8E5A4C3C655A4C3C495A4C
      3C2F5A4C3C1A5A4C3C0B5A4C3C03000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000005A4C3C025A4C3C095A4C3C145A4C3C255A4C3C3C5A4C3C58594B
      3C755B4D3D9F60503ED2655539F9635238FF605158FF5F59AEFF6D6CF1FF7678
      FFFF7979FFFF7878FFFF7777FFFF7777FFFF7777FFFF7777FFFF7777FFFF7777
      FFFF7777FFFF7777FFFF7777FFFF7777FFFF7777FFFF7777FFFF7777FFFF7777
      FFFF7777FFFF7777FFFF7777FFFF7777FFFF7777FFFF7878FFFF7879FFFF787A
      FFFF6C6CF7FF5E59B6FF5E5161FF645238FF65543BF75F503ECB5A4C3C95594B
      3C6C5A4C3C4E5A4C3C325A4C3C1C5A4C3C0D5A4C3C0400000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000005A4C
      3C035A4C3C0B5A4C3C185A4C3C2C5A4C3C435A4C3C5D594C3C7F5C4E3EAD6152
      3FE1655435FD62513EFF5A5078FF6460C5FF7273FFFF7D7FFFFF7F7FFFFF7E7E
      FFFF7D7DFFFF7D7DFFFF7D7DFFFF7D7DFFFF7D7DFFFF7D7DFFFF7D7DFFFF7D7D
      FFFF7D7DFFFF7D7DFFFF7D7DFFFF7D7DFFFF7D7DFFFF7D7DFFFF7D7DFFFF7D7D
      FFFF7D7DFFFF7D7DFFFF7D7DFFFF7D7DFFFF7D7DFFFF7D7DFFFF7D7DFFFF7D7D
      FFFF7F7FFFFF7E80FFFF7374FFFF6360C3FF5E5165FF62513CFF655537FB6051
      3FD35A4D3C9B594B3C705A4C3C515A4C3C365A4C3C1F5A4C3C0F5A4C3C050000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005A4C3C045A4C3C0E5A4C
      3C1B5A4C3C2F5A4C3C485A4C3C655A4C3C875D4E3DB863533BE8635336FF6150
      41FF5E5482FF6867DCFF7C7EFFFF8687FFFF8686FFFF8484FFFF8484FFFF8484
      FFFF8484FFFF8484FFFF8484FFFF8484FFFF8484FFFF8484FFFF8484FFFF8484
      FFFF8484FFFF8484FFFF8484FFFF8484FFFF8484FFFF8484FFFF8484FFFF8484
      FFFF8484FFFF8484FFFF8484FFFF8484FFFF8484FFFF8484FFFF8484FFFF8484
      FFFF8484FFFF8484FFFF8686FFFF8687FFFF7B7BFFFF6765CDFF5D5172FF6251
      39FF645437FC60513CD45C4D3CA15A4C3C735A4C3C525A4C3C375A4C3C205A4C
      3C105A4C3C050000000000000000000000000000000000000000000000000000
      000000000000000000005A4C3C015A4C3C065A4C3C105A4C3C205A4C3C365A4C
      3C50594B3C6B5B4D3B9460523AC9645439F4655432FF5B4D55FF6059A0FF7271
      ECFF8587FFFF8E8EFFFF8B8BFFFF8A8AFFFF8A8AFFFF8A8AFFFF8A8AFFFF8A8A
      FFFF8A8AFFFF8A8AFFFF8A8AFFFF8A8AFFFF8A8AFFFF8A8AFFFF8A8AFFFF8A8A
      FFFF8A8AFFFF8A8AFFFF8A8AFFFF8A8AFFFF8A8AFFFF8A8AFFFF8A8AFFFF8A8A
      FFFF8A8AFFFF8A8AFFFF8A8AFFFF8A8AFFFF8A8AFFFF8A8AFFFF8A8AFFFF8A8A
      FFFF8A8AFFFF8A8AFFFF8A8AFFFF8A8AFFFF8B8CFFFF8D8FFFFF8385FFFF6B69
      D7FF5C527EFF5E4F42FF665434FF62513CDE5C4D3DA6594C3C765A4C3C565A4C
      3C3B5A4C3C235A4C3C115A4C3C065A4C3C010000000000000000000000000000
      0000000000005A4C3C065A4C3C115A4C3C245A4C3C3D5A4C3C58594C3C775C4D
      3BA1635337D663533CFA524551FF4A4288FF5B59CCFF8182F4FF9393FFFF9696
      FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292
      FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292
      FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292
      FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292
      FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292FFFF9292FFFF9393FFFF9797
      FFFF8A8CFFFF7473E9FF5B5395FF5D4E49FF655432FF62523EE55D4E3DB15A4C
      3C7E5A4C3C5B5A4C3C3E5A4C3C235A4C3C105A4C3C0500000000000000000000
      00005A4C3C035A4C3C115A4C3C30594B3C545A4C3C7E5C4E3DAE655437E26251
      39FE4C4164FF3D3AB3FF5C5EF7FF8C8CFFFF9D9EFFFF9D9DFFFF9A9AFFFF9999
      FFFF9999FFFF9999FFFF9999FFFF9999FFFF9999FFFF9999FFFF9999FFFF9999
      FFFF9999FFFF9999FFFF9999FFFF9999FFFF9999FFFF9999FFFF9999FFFF9999
      FFFF9999FFFF9999FFFF9999FFFF9999FFFF9999FFFF9999FFFF9999FFFF9999
      FFFF9999FFFF9999FFFF9999FFFF9999FFFF9999FFFF9999FFFF9999FFFF9999
      FFFF9999FFFF9999FFFF9999FFFF9999FFFF9999FFFF9999FFFF9999FFFF9999
      FFFF9B9CFFFF9D9EFFFF9697FFFF7878EEFF61599DFF5F5047FF645234FF6253
      3EEA5D4E3DB55A4C3C7F594B3C515A4C3C2C5A4C3C0E5A4C3C02000000000000
      00005A4C3C05594C3C195A4C3C535E4F3DAE655437EA614F37FF4B406AFF4946
      BEFF6D6FF7FF999AFFFFA7A7FFFFA3A3FFFFA0A0FFFFA0A0FFFFA0A0FFFFA0A0
      FFFFA0A0FFFFA0A0FFFFA0A0FFFFA0A0FFFFA0A0FFFFA0A0FFFFA0A0FFFFA0A0
      FFFFA0A0FFFFA0A0FFFFA0A0FFFFA0A0FFFFA0A0FFFFA0A0FFFFA0A0FFFFA0A0
      FFFFA0A0FFFFA0A0FFFFA0A0FFFFA0A0FFFFA0A0FFFFA0A0FFFFA0A0FFFFA0A0
      FFFFA0A0FFFFA0A0FFFFA0A0FFFFA0A0FFFFA0A0FFFFA0A0FFFFA0A0FFFFA0A0
      FFFFA0A0FFFFA0A0FFFFA0A0FFFFA0A0FFFFA0A0FFFFA0A0FFFFA0A0FFFFA0A0
      FFFFA0A0FFFFA0A0FFFFA1A1FFFFA4A5FFFFA0A1FFFF8584F6FF67609EFF5E4F
      4AFF615036FF62533DEF5E4F3EAA5A4C3C4B594C3C155A4C3C04000000000000
      0000000000005E503F6164543CE6604F38FF4F446EFF5754C9FF8182FEFFA5A7
      FFFFAEAEFFFFA9A9FFFFA7A7FFFFA7A7FFFFA7A7FFFFA7A7FFFFA7A7FFFFA7A7
      FFFFA7A7FFFFA7A7FFFFA7A7FFFFA7A7FFFFA7A7FFFFA7A7FFFFA7A7FFFFA7A7
      FFFFA7A7FFFFA7A7FFFFA7A7FFFFA7A7FFFFA7A7FFFFA7A7FFFFA7A7FFFFA7A7
      FFFFA7A7FFFFA7A7FFFFA7A7FFFFA7A7FFFFA7A7FFFFA7A7FFFFA7A7FFFFA7A7
      FFFFA7A7FFFFA7A7FFFFA7A7FFFFA7A7FFFFA7A7FFFFA7A7FFFFA7A7FFFFA7A7
      FFFFA7A7FFFFA7A7FFFFA7A7FFFFA7A7FFFFA7A7FFFFA7A7FFFFA7A7FFFFA7A7
      FFFFA7A7FFFFA7A7FFFFA7A7FFFFA7A7FFFFA7A7FFFFACADFFFFA7A8FFFF8D8D
      F9FF746CA1FF645549FF615034FF63543CE05E503F5500000000000000000000
      00005D4E3E1D65543FEF665758FF7670B2FF9092FFFFB3B3FFFFB4B4FFFFB0B0
      FFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAE
      FFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAE
      FFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAE
      FFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAE
      FFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFB0B0FFFFB2B2
      FFFFB0B1FFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAE
      FFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAEAEFFFFAFAFFFFFB3B3
      FFFFB2B3FFFF9E9DF4FF746DADFF635558FF65543FE35D4E3E10000000000000
      00005F503F4E625034FF7F7BCCFFBFC2FFFFBBBBFFFFB7B7FFFFB6B6FFFFB6B6
      FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6
      FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6
      FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6
      FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6
      FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB9B9FFFFAAABF9FF9D9B
      EBFFB4B5FFFFBCBEFFFFBBBBFFFFB7B8FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6
      FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6FFFFB6B6
      FFFFB7B7FFFFBABAFFFFBDC0FFFF6F6AC1FF635136FA5E4F3F34000000000000
      00005F503F54614F34FF8B87CBFFC1C3FFFFBDBDFFFFBDBDFFFFBDBDFFFFBDBD
      FFFFBDBDFFFFBDBDFFFFBDBDFFFFBDBDFFFFBDBDFFFFBDBDFFFFBDBDFFFFBDBD
      FFFFBDBDFFFFBDBDFFFFBDBDFFFFBDBDFFFFBDBDFFFFBDBDFFFFBDBDFFFFBDBD
      FFFFBDBDFFFFBDBDFFFFBDBDFFFFBDBDFFFFBDBDFFFFBDBDFFFFBDBDFFFFBDBD
      FFFFBDBDFFFFBDBDFFFFBDBDFFFFBDBDFFFFBDBDFFFFBDBDFFFFBDBDFFFFBDBD
      FFFFBDBDFFFFBDBDFFFFBDBDFFFFBDBDFFFFBDBDFFFFC1C4FFFF8F88A3FF5F4D
      2DFF6D6171FF8E88ABFFA8A8FCFFC1C1FFFFC3C5FFFFC2C2FFFFBDBDFFFFBDBD
      FFFFBDBDFFFFBDBDFFFFBDBDFFFFBDBDFFFFBDBDFFFFBDBDFFFFBDBDFFFFBDBD
      FFFFBDBDFFFFBDBDFFFFC2C3FFFF7972B8FF625135FC5E4F3F37000000000000
      00005F503E52604F34FF8E8ACBFFCACBFFFFC5C5FFFFC5C5FFFFC5C5FFFFC5C5
      FFFFC5C5FFFFC5C5FFFFC5C5FFFFC5C5FFFFC5C5FFFFC5C5FFFFC5C5FFFFC5C5
      FFFFC5C5FFFFC5C5FFFFC5C5FFFFC5C5FFFFC5C5FFFFC5C5FFFFC5C5FFFFC5C5
      FFFFC5C5FFFFC5C5FFFFC5C5FFFFC5C5FFFFC5C5FFFFC5C5FFFFC5C5FFFFC5C5
      FFFFC5C5FFFFC5C5FFFFC5C5FFFFC5C5FFFFC5C5FFFFC5C5FFFFC5C5FFFFC5C5
      FFFFC5C5FFFFC5C5FFFFC5C5FFFFC5C5FFFFC8C8FFFFBEBEFCFF685C69FF6252
      3BE1625139DB604F37FF655446FF7A6E7BFF9995CAFFB6B8FFFFCDCDFFFFCBCB
      FFFFC9C9FFFFC5C5FFFFC5C5FFFFC5C5FFFFC5C5FFFFC5C5FFFFC5C5FFFFC5C5
      FFFFC5C5FFFFC5C5FFFFCACBFFFF7973BCFF615035FB5E4F3E35000000000000
      00005F503E52604F34FF938ECBFFD1D3FFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCC
      FFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCC
      FFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCC
      FFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCC
      FFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCC
      FFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFD5D6FFFF9692D7FF5F4E39FF6555
      427F000000006555434D645542AC615139F05E4D39FF695A55FF7F779BFFA6A3
      DAFFC2C4FEFFD6D7FFFFCDCDFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCCFFFFCCCC
      FFFFCCCCFFFFCCCCFFFFD1D2FFFF7974C1FF615035FB5E4F3E35000000000000
      00005F503E525F4D34FF9692CBFFD7D9FFFFD2D2FFFFD2D2FFFFD2D2FFFFD2D2
      FFFFD2D2FFFFD2D2FFFFD2D2FFFFD2D2FFFFD2D2FFFFD2D2FFFFD2D2FFFFD2D2
      FFFFD2D2FFFFD2D2FFFFD2D2FFFFD2D2FFFFD2D2FFFFD2D2FFFFD2D2FFFFD2D2
      FFFFD2D2FFFFD2D2FFFFD2D2FFFFD2D2FFFFD2D2FFFFD2D2FFFFD2D2FFFFD2D2
      FFFFD2D2FFFFD2D2FFFFD2D2FFFFD2D2FFFFD2D2FFFFD2D2FFFFD2D2FFFFD2D2
      FFFFD2D2FFFFD2D2FFFFD2D2FFFFD5D5FFFFCFD1FFFF71667FFF604F37F36555
      4316000000000000000000000000655543146555415E63523EC5604F36FA5F4E
      36FF6C5F62FF8680BBFFD3D3FFFFD2D2FFFFD2D2FFFFD2D2FFFFD2D2FFFFD2D2
      FFFFD2D2FFFFD2D2FFFFD7D8FFFF7873CBFF615034FB5E4F3E35000000000000
      00005F503E525F4D34FF9A95CBFFE0E0FFFFD9D9FFFFD9D9FFFFD9D9FFFFD9D9
      FFFFD9D9FFFFD9D9FFFFD9D9FFFFD9D9FFFFD9D9FFFFD9D9FFFFD9D9FFFFD9D9
      FFFFD9D9FFFFD9D9FFFFD9D9FFFFD9D9FFFFD9D9FFFFD9D9FFFFD9D9FFFFD9D9
      FFFFD9D9FFFFD9D9FFFFD9D9FFFFD9D9FFFFD9D9FFFFD9D9FFFFD9D9FFFFD9D9
      FFFFD9D9FFFFD9D9FFFFD9D9FFFFD9D9FFFFD9D9FFFFD9D9FFFFD9D9FFFFD9D9
      FFFFD9D9FFFFD9D9FFFFD9D9FFFFDFE1FFFFB1B0EAFF5A4B4AFF65553E9D0000
      0000000000000000000000000000000000000000000000000000665643276555
      4175615034E55D4F54FFD0D0FFFFDCDCFFFFD9D9FFFFD9D9FFFFD9D9FFFFD9D9
      FFFFD9D9FFFFD9D9FFFFDEDFFFFF7C78C7FF615034FB5E4F3E35000000000000
      00005F503E525F4D34FF9D99CBFFE6E7FFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDF
      FFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDF
      FFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDF
      FFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDF
      FFFFDFDFFFFFE3E3FFFFE7E9FFFFE6E7FFFFE7E8FFFFE7E7FFFFE3E3FFFFDFDF
      FFFFDFDFFFFFDFDFFFFFE0E0FFFFEAEAFFFF7F78A3FF604F33FC655544370000
      0000000000000000000000000000000000000000000000000000000000000000
      000060513E9F655650FFC1C0FFFFE4E5FFFFDFDFFFFFDFDFFFFFDFDFFFFFDFDF
      FFFFDFDFFFFFDFDFFFFFE5E5FFFF807BC4FF614F34FB5E4F3E35000000000000
      00005F503E525D4C34FFA09CCBFFECEDFFFFE5E5FFFFE5E5FFFFE5E5FFFFE5E5
      FFFFE5E5FFFFE5E5FFFFE5E5FFFFE5E5FFFFE5E5FFFFE5E5FFFFE5E5FFFFE5E5
      FFFFE5E5FFFFE5E5FFFFE5E5FFFFE5E5FFFFE5E5FFFFE5E5FFFFE5E5FFFFE5E5
      FFFFE5E5FFFFE5E5FFFFE5E5FFFFE5E5FFFFE5E5FFFFE8E8FFFFEBEBFFFFEDED
      FFFFF2F2FFFFDDDEFFFFBDBCF2FF9C98C2FFA49FC4FFC0BFF5FFDFE1FFFFF1F2
      FFFFEDEDFFFFECECFFFFF3F4FFFFCBCBFFFF625358FF63523DBB000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000063533E825F504EFFB0B0FEFFEEEEFFFFE5E5FFFFE5E5FFFFE5E5FFFFE5E5
      FFFFE5E5FFFFE5E5FFFFEBECFFFF827DC5FF604F34FB5E4F3E35000000000000
      00005F503E525D4C34FFA49FCBFFF1F2FFFFEAEAFFFFEAEAFFFFEAEAFFFFEAEA
      FFFFEAEAFFFFEAEAFFFFEAEAFFFFEAEAFFFFEAEAFFFFEAEAFFFFEAEAFFFFEAEA
      FFFFEAEAFFFFEAEAFFFFEAEAFFFFEAEAFFFFEAEAFFFFEAEAFFFFEAEAFFFFEAEA
      FFFFEAEAFFFFECECFFFFEDEDFFFFF1F1FFFFF7F9FFFFE8E9FFFFD0D1F9FFB1AE
      DEFF7D77AEFF6B5E66FF574854FF5C4A30FC5B4931F95B4C57FF716568FF827C
      B2FFB8B5E0FFD4D3F6FFC9C8EFFF827AA3FF5F4E38FF65554246000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000655540635A4B49FFA4A3EBFFF5F6FFFFEAEAFFFFEAEAFFFFEAEAFFFFEAEA
      FFFFEAEAFFFFEAEAFFFFF0F1FFFF847FC7FF604F34FB5E4F3E35000000000000
      00005F503E525D4C34FFA6A2CBFFF7F7FFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEF
      FFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEF
      FFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFF0F0FFFFF1F1FFFFF4F5
      FFFFFCFCFFFFF3F5FFFFE5E7FFFFC5C3F4FF9691CAFF7D7384FF5B4E66FF5745
      32FF706042FF746546FF6B5A26FF58473AFA5B4B44F764533BF8605039E65F4D
      36F65D4C38FF5A4D5EFF5B4C43FF615038FF665642A500000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000061523F3D58483CFFA29DBCFFFCFDFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEF
      FFFFEFEFFFFFEFEFFFFFF5F6FFFF8581C7FF604F34FB5E4F3E35000000000000
      00005F503E525D4B34FFA9A4CBFFFCFDFFFFF4F4FFFFF4F4FFFFF4F4FFFFF4F4
      FFFFF4F4FFFFF4F4FFFFF4F4FFFFF4F4FFFFF4F4FFFFF4F4FFFFF4F4FFFFF4F4
      FFFFF4F4FFFFF4F4FFFFF5F5FFFFF8F8FFFFFFFFFFFFFBFBFFFFF6F7FFFFD8D8
      FFFFADAADCFF8C849FFF60557AFF604F3DFF5C4B3BFF5F4D35ED5F4E3CF96F60
      30FFF4F3AEFFFFFFBFFFEDEB6CFFC4BD7CFFA1973FFF615143FF604E43E86051
      3F865E4F3E7C63533BA461513D845F503F400000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00005C4E3C285A4937FF9790A7FFFDFFFFFFF4F4FFFFF4F4FFFFF4F4FFFFF4F4
      FFFFF4F4FFFFF5F5FFFFFAFCFFFF8783C8FF604D34FB5E4F3E35000000000000
      00005F503E525C4B34FFABA6CBFFFFFFFFFFF7F7FFFFF7F7FFFFF7F7FFFFF7F7
      FFFFF7F7FFFFF7F7FFFFF7F7FFFFF7F7FFFFF7F7FFFFF7F7FFFFF7F8FFFFFBFB
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFE8E8FFFFC4C3ECFF9E99BBFF6C6390FF6555
      4CFF5A4A43FF5E4C34FA645339C361523F8A5F4F3F57615140B15C4C32FFC9C3
      64FFFFFFFCFFFFFFE4FFCBC842FFB9B232FFE9E998FFEAE782FF6F612AFF5B4B
      44F85D4E3E6E5A4C3C1C594B3C0D00000000594B3C0100000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00005C4E3D1E5F4E34F2877E9EFFF6F7FFFFF9FAFFFFF7F7FFFFF7F7FFFFF7F7
      FFFFF7F7FFFFF7F7FFFFFDFFFFFF8984C8FF5F4D34FB5E4F3E34000000000000
      00005F503E515C4B34FFACA8CBFFFFFFFFFFFAFAFFFFFAFAFFFFFAFAFFFFFAFA
      FFFFFAFAFFFFFAFAFFFFFAFAFFFFFCFCFFFFFEFEFFFFFFFFFFFFFFFFFFFFF5F7
      FFFFDCDCF8FFB2AFD8FF7B75A9FF6E6161FF584950FF5B4A32FF635237DF6453
      40A96656415B6253411D5B4D3D01594B3B025C4D3E615C4A40FC8C8042FFFFFF
      E0FFFFFFF9FFFFFFFAFFFFFFECFFA79E61FF5A4D17FFD7D74BFFFCFA82FF796A
      35FF5B4A3FFF6152409A5A4C3C2B5A4C3C1E5A4C3C0E5A4C3C02000000000000
      000000000000000000005A4C3C015A4C3C045A4C3C085A4C3C0D5A4C3C135A4C
      3C155C4D3D2E625134E372699CFFF3F4FFFFFDFDFFFFFAFAFFFFFAFAFFFFFAFA
      FFFFFAFAFFFFFBFBFFFFFFFFFFFF8A85C8FF5F4D34FB5E503E34000000000000
      00005F503E4F5C4B34FFADA9CBFFFFFFFFFFFDFDFFFFFDFDFFFFFDFDFFFFFDFE
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFCFFFFEEEEF9FFC4C2EAFF8E89BDFF796E
      77FF584C5EFF5B4A33FF604F36F160503BC364533E7664544235645442040000
      000000000000000000005A4C3C0C5A4C3C3B5F5041D96A5A31FFEAE7A2FFFFFF
      FFFFFFFEFAFFFFFEFAFFFFFFFFFFFFFFF3FFB9B065FF54451FFFC4BE4DFFFFFF
      9BFF827455FF5A493BFF625341AC5A4C3D345A4C3C215A4C3C145A4C3C0B5A4C
      3C0B5A4C3C105A4C3C175A4C3C1D5A4C3C255A4C3C2F5A4C3C385A4C3C415A4C
      3C465B4C3C4E635236D7605789FFEAECFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFD
      FFFFFDFDFFFFFEFEFFFFFFFFFFFF8A86C8FF604D34FA5F503E31000000000000
      00005E4F3E4D5C4B34FFAFAACBFFFFFFFFFFFFFFFFFFFEFEFFFFFFFFFFFFFFFF
      FFFFFDFEFEFFD1D0FDFFA3A0C5FF837991FF5D5068FF5D4B34FF5C4B39FE5F4E
      37E662513AAD5F503E705E4F3E2D5A4C3C030000000000000000000000000000
      0000000000005A4C3C085A4C3C225F5041A45A4937FFB9B261FFFFFFFFFFFFFF
      FEFFFFFEFCFFFFFEFCFFFFFEFCFFFFFFFFFFFFFFF7FFC8C08AFF53422AFFA299
      62FFF8F8C6FF8C8168FF5A4939FF625241BD5A4C3D40594C3C2F5A4C3C2F5A4C
      3C325A4C3C3B5A4C3C445A4C3C4B594C3C4F594B3C545A4C3C5D5A4C3C685A4C
      3C6E5C4E3C7E63533DD75F504EFF7A718EFF9F99BFFFC9C8F1FFF2F2FAFFFEFE
      FFFFFEFEFFFFFFFFFFFFFFFFFFFF8B87C8FF5F4D34FA5F503E2F000000000000
      0000625241505C4A34FFB2AECCFFFFFFFFFFFFFFFFFFFFFFFFFFEEEEFFFF918F
      E3FF605473FF584639FF524033FF5C4929FF645437FF635342D15E4F3E7F594B
      3B515A4C3C40594B3B35594B3B385A4C3C375A4C3C2D5A4C3C235A4C3C1A5A4C
      3C125A4C3C105A4C3C1B5D4E3D615C4B3FFB7F733AFFFDFDDCFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCAC48EFF513F
      2EFF877B6BFFFBFBD1FF998E7EFF5A493DFF605142C95B4D3C56594C3C535A4C
      3C5E5A4C3C625A4C3C645B4D3E735E4F3D8D5F503DA461513ABE605037D75D4C
      37F05D4B39FF5C4B3BFF574740FF50435BFF5F568EFF8C85A5FFBAB8E1FFFEFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFF8D89CCFF5F4D34FB65554332000000000000
      00006656432863523AF76C6287FFA29ECFFFD4D1DDFFE2E4FFFFFAFBFFFFF5F4
      F6FFD6D4E9FF9C99D7FF897E80FF665A73FF564746FE5C4B32F65F4E37ED6050
      3BD561503BA95D4F3D845C4D3D635A4B3C4A594B3C455A4C3C425A4C3C3C5A4C
      3C355A4C3C345A4C3C4A615143D7615232FFE0DD8CFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD5D0
      AAFF5E4D37FF827933FFFBFBC4FFB1A883FF5F4E2FFF605041DE5D4E3C996051
      3BB85F5039CD5F4E3AEA5F4E36F55C4A37FE55443EFF56495EFF695E79FF7E73
      80FF8F8BBFFFAAA8EBFFDEDDF3FFF6F7FFFFFEFFFFFFFFFFFFFFFFFFFFFFE0E1
      FFFFD2D1EDFFBBB7D2FF928DCFFF645A7FFF64533BEB6555430E000000000000
      0000000000006555447662523AEC5C4B30FF564642FF62556CFF7F7472FF918D
      C8FFC7C6E7FFF1F1F8FFFAFCFFFFF1F3FFFFE4E3EAFFA9A6D8FF7E769DFF7163
      60FF594B56FF5A4935FF625134EF60513DD461513EA95D4F3C7C5B4D3D625A4C
      3C53594B3C585E4F3FAB5C4939FFADA459FFFFFFFBFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFDAD7B8FF5E4D3BFF736825FFFCFB7EFFD6D06CFF5A4A3CFF534339FF5848
      55FF625565FF726674FF7B74ABFF9D99D8FFCCCAE1FFE6E6FBFFF8FBFFFFFFFF
      FFFFFFFFFFFFF3F5FFFFE7E6F4FFBFBDE7FF9794DCFF827A95FF71667AFF5E52
      6FFF53454FFF564539FF5F4D33FB65533BE66555446A00000000000000000000
      000000000000000000006555430A655543396555406E635239AB5F4E39E55C4C
      38FF5C4B34FF594B5AFF726889FF9A9195FFAFADEAFFE5E6FFFFFFFFFFFFEDF0
      FFFFD6D6F9FFB9B4C1FF7770A5FF695B58FF5F4F4CFF59483BFF604F33F06352
      39D25F503FBD5D4D41F1786C3BFFFEFDC9FFFFFFFFFFFFFEFAFFFFFEFAFFFFFE
      FAFFFFFEFAFFFFFEFAFFFFFEFAFFFFFEFAFFFFFEFAFFFFFEFAFFFFFEFAFFFFFF
      FCFFFFFFFFFFDEDBB5FF6D5E41FF6D5F20FFEFEF62FFD8D5BFFFAEA8C8FFC8C7
      FEFFE6E9FFFFFFFFFFFFFFFFFFFFFFFFFFFFF3F4FFFFC1C0FAFFA3A1E2FF948C
      9EFF7B728DFF5D5279FF55464FFF59473DFF5C4C37FF5D4C39F5604F39D56352
      39A466553E756655414D6555432C655543060000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000006555
      432B6555426264543D99615036D25B4A36FC5B4A40FF61513DFF635872FF8881
      A4FFBDB7BEFFC9C9FFFFF4F6FFFFF8F9FFFFC7C8FFFFB2AEC2FF88809CFF6156
      70FF5D4C3EFF59492EFFD4CF89FFFFFFFFFFFFFEF9FFFFFEF8FFFFFEF8FFFFFE
      F8FFFFFEF8FFFFFEF8FFFFFEF8FFFFFEF8FFFFFEF8FFFFFEF8FFFFFEF8FFFFFE
      F8FFFFFFF9FFFFFFFFFFEDEACAFF73653FFF655C04FFD8D868FFFFFFFFFFE8E9
      CCFFB0ACDBFF9E97AEFF7C74A6FF60567EFF5C4D50FF5C4C46FF5B4A3FFF5A49
      38FE5F4E36DE645338B166563F896656425F655542396555430A000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000065554301655543156555424E6454439064533BC1604F
      32EE584839FF5C4D50FF6D5D54FF736BA1FFA59FC1FFD1D0E7FFD2D6FFFFF0F1
      FFFFD0D0FDFFB7B3BEFFFFFFE6FFFFFFF3FFFFFEEFFFFFFEEFFFFFFEEFFFFFFE
      EFFFFFFEEFFFFFFEEFFFFFFEEFFFFFFEEFFFFFFEEFFFFFFEEFFFFFFEEFFFFFFE
      EFFFFFFEEFFFFFFFEFFFFFFFFEFFF1EFC2FF817443FF5B500AFFBFBC5BFFEFF0
      90FF6E642EFF4E3C36FF604F33F1635238C461513EAB61523F7461523F436152
      4115655443050000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000006555
      43156555413C65533F7A62523FBE625137E45E4C31FC584848FF635667FF776B
      73FF9D98B9FFFFFFE9FFFFFFEDFFFFFEE8FFFFFEE8FFFFFEE8FFFFFEE8FFFFFE
      E8FFFFFEE8FFFFFEE8FFFFFEE8FFFFFEE8FFFFFEE8FFFFFEE8FFFFFEE8FFFFFE
      E8FFFFFEE8FFFFFEE8FFFFFEE8FFFFFFF7FFFEFDC2FF8C7E46FF4B3A28FFABA1
      69FFFFFF87FF8C8130FF584640FD6050419E594B3C1A594B3B0E594B3C050000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000615240095D4F3D395F503C7E62523BE35444
      31FFBAB35AFFFFFFE0FFFFFEE0FFFFFEDEFFFFFEDEFFFFFEDEFFFFFEDEFFFFFE
      DEFFFFFEDEFFFFFEDEFFFFFEDEFFFFFEDEFFFFFEDEFFFFFEDEFFFFFEDEFFFFFE
      DEFFFFFEDEFFFFFEDEFFFFFEDEFFFFFEDFFFFFFFECFFFFFFB9FF978B43FF4B38
      3AFFA8A229FFFFFF74FFA59A3DFF594838FF625244C95B4D3D3E5A4C3C235A4C
      3C155A4C3C055A4C3C0100000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000594B3B0B5C4D3D635B4B41FA8E83
      3BFFFFFFC2FFFFFFDBFFFFFED5FFFFFED5FFFFFED5FFFFFED5FFFFFED5FFFFFE
      D5FFFFFED5FFFFFED5FFFFFED5FFFFFED5FFFFFED5FFFFFED5FFFFFED5FFFFFE
      D5FFFFFED5FFFFFED5FFFFFED5FFFFFED5FFFFFED6FFFFFFE0FFFFFFC2FFA399
      5BFF4B3939FF8F8432FFFFFF75FFB6B23DFF5B4B36FF615045D65C4E3D50594C
      3C215A4C3C105A4C3C0300000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000005A4C3C015A4C3C0F5B4C3C38605041DD655639FFE2E0
      8BFFFFFFD4FFFFFECBFFFFFECAFFFFFECAFFFFFECAFFFFFECAFFFFFECAFFFFFE
      CAFFFFFECAFFFFFECAFFFFFECAFFFFFECAFFFFFECAFFFFFECAFFFFFECAFFFFFE
      CAFFFFFECAFFFFFECAFFFFFECAFFFFFECAFFFFFECAFFFFFECBFFFFFFD3FFFFFF
      BFFFA69C57FF4F3D35FF716252FFF2F17EFFBAB360FF605132FF5F4F46E05D4F
      3E4A594C3C0A5A4C3C0500000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000005A4C3C0A594C3C1F604F40A15B493FFFB3AB52FFFFFF
      BDFFFFFFBEFFFFFEBCFFFFFEBCFFFFFEBCFFFFFEBCFFFFFEBCFFFFFEBCFFFFFE
      BCFFFFFEBCFFFFFEBCFFFFFEBCFFFFFEBCFFFFFEBCFFFFFEBCFFFFFEBCFFFFFE
      BCFFFFFEBCFFFFFEBCFFFFFEBCFFFFFEBCFFFFFEBCFFFFFEBCFFFFFFBDFFFFFF
      C4FFFFFFBBFFB1A871FF58473DFF73672AFFEFEF55FFCECB4CFF635433FF604F
      46E85E4F3E370000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000005A4C3C045A4C3C165D4E3E625E4C42F97F723CFFFEFD9CFFFFFF
      B6FFFFFDAFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFD
      AFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFDAFFFFFFD
      AFFFFFFDAFFFFFFEB1FFFFFFB1FFFFFFB4FFFFFFBBFFFFFFBAFFFFFFBAFFFFFF
      A8FFFFFF9AFFFFFF9DFF9E9655FF524041FF5D502DFFD6D34AFFDCD844FF6D5E
      32FF605046EE5E4F3E2F00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00005A4C3C015A4C3C0B5A4C3C2F605042CF5F4F3CFFDAD66AFFFFFFA9FFFFFD
      A0FFFFFDA0FFFFFDA0FFFFFDA0FFFFFDA0FFFFFDA0FFFFFDA0FFFFFDA0FFFFFD
      A0FFFFFDA0FFFFFDA0FFFFFDA0FFFFFDA0FFFFFDA0FFFFFDA0FFFFFDA0FFFFFD
      A2FFFFFFA0FFFFFF96FFFDFB9EFFFAF882FFE9E54AFFC2BD56FFA29B58FF9E95
      3AFF988E1CFF827731FF766951FF948A50FFBCB51BFFC8C134FFFFFF8CFFDEDB
      50FF6F5F34FF6352449B00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00005A4C3C01594B3C0360513F7F5A4942FF9E9446FFFFFF8CFFFFFE94FFFFFD
      91FFFFFD91FFFFFD91FFFFFD91FFFFFD91FFFFFD91FFFFFD91FFFFFD91FFFFFD
      91FFFFFD91FFFFFD91FFFFFD91FFFFFD91FFFFFD91FFFFFD91FFFFFD91FFFFFF
      97FFEEEB4BFF999023FF7B6F38FF665836FF93881BFFABA229FFA69D61FFBFB7
      64FFECE813FFF1ED17FFDEDC53FFC5C159FFC4BD31FFB2AB23FF77685EFF6E5F
      49FF665740FF6554436800000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000005E4F3E25605044E971633BFFF3F171FFFFFF87FFFFFF82FFFFFF
      85FFFFFF83FFFFFF83FFFFFF87FFFFFE85FFFFFD83FFFFFD83FFFFFD83FFFFFD
      83FFFFFD83FFFFFD83FFFFFD83FFFFFD83FFFFFD83FFFFFD83FFFFFD83FFFFFF
      87FFD8D329FF514040FC604F45F75A4944FFECEA61FFDFDA21FFAFA927FF8275
      53FF786A44FF726238FF67592FFF5B4C35FF584646FC5B4A47E5625241CB6353
      43B1655543500000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000006352429E5E4E42FFCAC653FFFFFF75FFFFFF83FFFFFF79FFFEFD
      65FFE9E679FFD7D27DFFCCC753FFF2F05DFFFFFF73FFFFFD71FFFFFD71FFFFFD
      71FFFFFD71FFFFFD71FFFFFD71FFFFFD71FFFFFD71FFFFFD71FFFFFD71FFFFFF
      74FFD8D326FF5A4A3EF3645444E25C4B45FFF2F050FF908430FF524049FB6151
      42D8615144B26252448B63534465655444396555432465554310000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00006353411D5F4E44EF958A3EFFFFFF56FFFFFF4FFFCBC755FFA39C3CFF9E97
      26FF9B943EFFB0AB1AFFC8C30BFFF4F14CFFFFFF64FFFFFC60FFFFFC60FFFFFC
      60FFFFFC60FFFFFC60FFFFFC60FFFFFC60FFFFFC60FFFFFC60FFFFFC60FFFFFF
      62FFD6D021FF5A4A3EF4645444E45C4B45FFF0ED44FF968A2EFF5B4A46E05D4E
      3F05000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000066564318605044EC867A38FFAEA51FFFB9B23CFFD0CB34FFDDD919FFD1CD
      36FFC1BC49FFB8B02BFFAEA418FFEBE831FFFFFF54FFFFFC4FFFFFFC4FFFFFFC
      4FFFFFFC4FFFFFFC4FFFFFFC4FFFFFFC4FFFFFFC4FFFFFFC4FFFFFFC4FFFFFFF
      51FFD3CE1CFF5A4A3FF4645444E45D4B45FFEFEC39FF94892DFF5C4A47E35A4C
      3C0B000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000006556436B604F45DC594946F45B4A3FFF62533FFF685944FF6455
      43FF5D4E40FF584743FD5A4A43FFD8D42FFFFFFF40FFFFFC3EFFFFFC3EFFFFFC
      3EFFFFFC3EFFFFFC3EFFFFFC3EFFFFFC3EFFFFFC3EFFFFFC3EFFFFFC3EFFFFFF
      3EFFD2CC19FF5A4A40F4645444E45D4B47FFEEEB2DFF93882CFF5C4B47E35B4D
      3D0B000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000655543076555431A6555433464534366635243906454
      4377635342406151428F645441FFD8D525FFFFFF2EFFFFFC2DFFFFFC2DFFFFFC
      2DFFFFFC2DFFFFFC2DFFFFFC2DFFFFFC2DFFFFFC2DFFFFFC2DFFFFFC2DFFFFFF
      2CFFCFC913FF5A4A41F4645444E45D4B47FFEDE921FF91862CFF5C4C48E35B4D
      3D0B000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000005F50427B645541FFD8D519FFFFFF1BFFFFFC1BFFFFFC1BFFFFFC
      1BFFFFFC1BFFFFFC1BFFFFFC1BFFFFFC1BFFFFFC1BFFFFFC1BFFFFFC1BFFFFFF
      1AFFCCC70FFF5A4B42F4645444E45D4D47FFEBE816FF90842CFF5D4C48E35B4D
      3D0B000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000006050427F645541FFD4D114FFFFFF12FFFBF812FFFBF812FFFBF8
      12FFFBF812FFFBF812FFFBF812FFFBF812FFFBF812FFFBF812FFFBF812FFFFFF
      10FFC8C30EFF5C4B42F4645444E45D4D47FFE7E410FF8E822CFF5D4C48E35B4D
      3D0B000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000006050427F645542FFD0CD11FFFCF90CFFF6F30DFFF6F30DFFF6F3
      0DFFF6F30DFFF6F30DFFF6F30DFFF6F30DFFF6F30DFFF6F30DFFF6F30DFFFEFC
      0BFFC3BF0DFF5C4B42F4645444E45E4D47FFE2DE0DFF8C802CFF5D4D48E35B4D
      3D0B000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000006050427F645542FFCDC90FFFF8F508FFF2EF09FFF2EF09FFF2EF
      09FFF2EF09FFF2EF09FFF2EF09FFF2EF09FFF2EF09FFF2EF09FFF2EF09FFFAF8
      07FFC0BB0CFF5C4D42F4645445E45E4D47FFDFDA0AFF8A7F2CFF5E4D48E35B4D
      3D0B000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000006050427F645541FFCAC709FFF3F003FFEDEA04FFEDEA04FFEDEA
      04FFEDEA04FFEDEA04FFEDEA04FFEDEA04FFEDEA04FFEDEA04FFEDEA04FFF5F2
      01FFBCB70BFF5D4D43F4645445E45E4E47FFDAD507FF887C2CFF5E4D48E35B4D
      3D0B000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000006050427F645542FFC6C307FFEEEB00FFE8E500FFE8E500FFE8E5
      00FFE8E500FFE8E500FFE8E500FFE8E500FFE8E500FFE8E500FFE8E500FFF0ED
      00FFB8B30BFF5D4D43F4645445E45E4E47FFD5D105FF867B2DFF5E4D48E35B4D
      3D0B000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000006050427F645542FFC2BE07FFE8E500FFE2DF00FFE2DF00FFE2DF
      00FFE2DF00FFE2DF00FFE2DF00FFE2DF00FFE2DF00FFE2DF00FFE2DF00FFE8E7
      00FFB4B00BFF5D4E43F4645445E45F4E47FFCFCC05FF857A2DFF5E4E48E35B4D
      3D0B000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000006050427F645541FFBFBC05FFE1DE00FFDCD900FFDCD900FFDCD9
      00FFDCD900FFDCD900FFDCD900FFDCD900FFDCD900FFDCD900FFDCD900FFE2E0
      00FFB1AC0BFF5D4E43F5635345E65F4E47FFCBC705FF84782DFF5F4E48E25B4D
      3D0B000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000006051427D645541FFBCB804FFDCD900FFD7D400FFD7D400FFD7D4
      00FFD7D400FFD7D400FFD7D400FFD7D400FFD7D400FFD7D400FFD7D400FFDDDA
      00FFAEA90BFF5E4E43F5635345E75F4E47FFC7C305FF83772DFF5F4E48E25B4D
      3D0B000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000006251427C655541FFB8B504FFD7D500FFD2D000FFD2D000FFD2D0
      00FFD2D000FFD2D000FFD2D000FFD2D000FFD2D000FFD2D000FFD2D000FFD8D7
      00FFABA60BFF5E4E43F6635445E85F4F47FFC2BF05FF81762DFF5F4E48E05B4D
      3D09000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000060514279665540FFB4B203FFD1D000FFCECC00FFCFCC00FFD0CD
      00FFD2CF00FFD2CF00FFD3D100FFD2CF00FFD0CD00FFCECB00FFCCC900FFD0CE
      00FFA19E0CFF594848FF604E4AFF5D4B4EFFC1BD06FF81762CFF5F4E48E25B4D
      3D08000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000006353437B665740FFB7B500FFD4D200FFC8C600FFC4C200FFBDBC
      00FFB6B500FFB1B000FFADAB00FFADAC00FFAAA800FFA9A700FFA7A500FFA4A4
      00FF9F9C07FF958D2DFF938A36FF938C3FFFB5B210FF76693AFF615045D86454
      4306000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000065554462645541FF96910FFFADA906FFB1AD08FFA6A20BFF9B96
      0DFF96910FFF96911BFF958F23FF90892DFF81792BFF7A722EFF796E2FFF7B6F
      32FF7D6F36FF7D7031FF796C30FF76692EFF6A5C3CFF635249FF655542870000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000065554316655544D5615145FF60513FFF5F5040FF615044FF614F
      47FF604F49FF604F47FF605047FF605047FF615147FB625147EF615146E76151
      46E4615145E2615145E0625145C4635145B463534495655543626555430A0000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000655543036555433C655444596554446C6555446E6555
      436D6555436B6555436A6555436B6555435C6555432E65554311000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FFFFFFFFFFFFFF000000FFFFFFFFE1FFFFFFFF000000FFFFFFFF807FFFFFFF00
      0000FFFFFFFE001FFFFFFF000000FFFFFFE00007FFFFFF554337FFFFFFC00001
      FFFFFF553ED7FFFFFF0000007FFFFF4838FFFFFFFC0000000FFFFF6662FFFFFF
      E000000003FFFFDEE3FFFFFF8000000000FFFFF4FFFFFFFE00000000003FFF84
      CDFFFFF000000000000FFF5543FFFFC0000000000003FFFEF7FFFF0000000000
      0000FFFEF5FFFC000000000000003FFEF5FFE00000000000000007FEF5FFC000
      00000000000003FEF5FF800000000000000001FEF5FF800000000000000001FE
      F9FFC000000000000000037142FF800000000000000001E082FF800000000000
      0000015FA9FF8000000000000000015356FF8000000000000000014A39FF8000
      00000000400001563ED880000000000070000155435B800000000000FE000155
      430A800000000000FF8001000000800000000001FF8001000000800000000001
      FF8001000000800000000003FF8001000000800000000007FF80010000008000
      0000000BFF8001000000800000000001E00001000000800000E0000000000100
      0000800007C00000000001000000800000000000000001000000800000000000
      000001554305C00000000000000003554294E000000000000000074F3CFFFF00
      000000000001FF4F41FFFFE000000000003FFFA5B1FFFFFF0000000000FFFFF4
      FFFFFFFFF0000000001FFFBECEFFFFFFF8000000001FFFFEEDFFFFFFE0000000
      001FFFFEEEFFFFFFE0000000003FFFFEEEFFFFFFC0000000001FFFFEEEFFFFFF
      80000000001FFFFEEEFFFFFF80000000001FFFFEEEFFFFFFC0000000003FFFFE
      EDFFFFFFC000000001FFFFE5ABFFFFFF800000007FFFFF7210FFFFFF80000000
      7FFFFF8922FFFFFFC00000007FFFFF533BE9FFFFE00000007FFFFF513F88FFFF
      FFC000007FFFFF514006FFFFFFC000007FFFFF000000FFFFFFC000007FFFFF00
      0000FFFFFFC000007FFFFF000000FFFFFFC000007FFFFF000000FFFFFFC00000
      7FFFFF000000FFFFFFC000007FFFFF000000FFFFFFC000007FFFFF000000FFFF
      FFC000007FFFFF000000FFFFFFC000007FFFFF000000FFFFFFC000007FFFFF00
      0000FFFFFFC000007FFFFF000000FFFFFFC00000FFFFFF000000FFFFFFC00000
      FFFFFF000000FFFFFFE001FFFFFFFF000000FFFFFFFFFFFFFFFFFF5543592800
      0000400000008000000001002000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00005A4C3C025A4C3C045A4C3C045A4C3C010000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000005A4C3C035A4C
      3C0E5A4C3C1D5A4C3C295A4C3C275A4C3C185A4C3C095A4C3C02000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000005A4C3C055A4C3C125A4C3C255A4C
      3C40594C3C5D5A4C3C775A4C3C715A4C3C535A4C3C345A4C3C1C5A4C3C0B5A4C
      3C01000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000005A4C3C025A4C3C085A4C3C175A4C3C2C5A4C3C46594B3C695B4D
      3C9960503BCE655434F2655337EC5E4F3CBD5A4C3B82594B3C575A4C3C385A4C
      3C1E5A4C3C0C5A4C3C0200000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000005A4C
      3C025A4C3C0B5A4C3C1B5A4C3C32594C3C4F594B3B735C4D3CA661513ADB6654
      33FF635346FF595087FF5C5276FF665439FF665433F45F4F3BC35A4C3B89594B
      3C5B5A4C3C3B5A4C3C215A4C3C0E5A4C3C030000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005A4C3C035A4C3C0F5A4C
      3C215A4C3C39594B3C575A4C3B7F5D4E3CB3625236E6645434FF61534FFF5B53
      97FF5857E6FF5B5CFFFF5A5BFFFF5855CBFF5B5178FF62533FFF645333F85F50
      3AC75B4D3B8E594B3C605A4C3C3E5A4C3C245A4C3C105A4C3C04000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000005A4C3C065A4C3C125A4C3C275A4C3C3F594B
      3C5F5B4C3B8B5F4F3BC2635333F1645337FF5E5263FF5A55B0FF5B5CF5FF6061
      FFFF6263FFFF6262FFFF6262FFFF6262FFFF5F5FFFFF5A58DCFF5C5281FF6252
      41FF655333FC61513AD25B4D3C95594B3C645A4C3C425A4C3C265A4C3C125A4C
      3C05000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00005A4C3C025A4C3C095A4C3C175A4C3C2C5A4C3C47594C3C6B5C4D3C996050
      39CE645333F962523EFF5C5176FF5C59C9FF6161FFFF6768FFFF6869FFFF6767
      FFFF6767FFFF6767FFFF6767FFFF6767FFFF6868FFFF6869FFFF6565FFFF5D5C
      E2FF5B5391FF605248FF645333FE615139D75C4D3C9E594C3C6A5A4C3C465A4C
      3C2A5A4C3C145A4C3C065A4C3C01000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000005A4C3C025A4C
      3C0C5A4C3C1C5A4C3C33594C3C505A4B3B755D4E3CA7615139DD645331FF6051
      48FF5C538DFF605EDDFF6868FFFF6D6FFFFF6E6EFFFF6D6DFFFF6D6DFFFF6D6D
      FFFF6D6DFFFF6D6DFFFF6D6DFFFF6D6DFFFF6D6DFFFF6D6DFFFF6D6DFFFF6E6E
      FFFF6A6BFFFF6161EDFF5C549BFF60514FFF645332FF615139DD5C4E3CA35A4C
      3B705A4C3C495A4C3C2D5A4C3C165A4C3C075A4C3C0100000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000005A4C3C045A4C3C0F5A4C3C215A4C
      3C3A594C3C585A4C3B805D4F3BB4625236E9635134FF615253FF5F58A1FF6665
      EFFF7072FFFF7476FFFF7373FFFF7373FFFF7373FFFF7373FFFF7373FFFF7373
      FFFF7373FFFF7373FFFF7373FFFF7373FFFF7373FFFF7373FFFF7373FFFF7373
      FFFF7373FFFF7475FFFF7173FFFF6868F6FF5F58A7FF5F5153FF635334FF6252
      37E65D4E3CAB5A4C3B745A4C3C4D5A4C3C305A4C3C185A4C3C08000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000005A4C3C015A4C3C065A4C3C135A4C3C275A4C3C42594B3C615B4C
      3B8E5E4F3BC3635332F261503AFF5E5268FF635EB8FF7070FAFF787BFFFF7B7D
      FFFF7A7AFFFF7979FFFF7979FFFF7979FFFF7979FFFF7979FFFF7979FFFF7979
      FFFF7979FFFF7979FFFF7979FFFF7979FFFF7979FFFF7979FFFF7979FFFF7979
      FFFF7979FFFF7979FFFF7A7AFFFF7B7CFFFF787BFFFF6E6EFAFF625CB5FF5F52
      5EFF625134FF625235E95D4E3CB25A4C3B795A4C3C515A4C3C335A4C3C1B5A4C
      3C0A5A4C3C010000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000005A4C
      3C015A4C3C085A4C3C175A4C3C2D5A4C3C49594B3B6C5B4D3B9A605139D06351
      33FB60503FFF5F557FFF6764D3FF787AFFFF8283FFFF8283FFFF8080FFFF8080
      FFFF8080FFFF8080FFFF8080FFFF8080FFFF8080FFFF8080FFFF8080FFFF8080
      FFFF8080FFFF8080FFFF8080FFFF8080FFFF8080FFFF8080FFFF8080FFFF8080
      FFFF8080FFFF8080FFFF8080FFFF8080FFFF8080FFFF8283FFFF8283FFFF7676
      FEFF6762BCFF5E536BFF625038FF625235ED5D4F3AB65A4C3B80594C3C555A4C
      3C365A4C3C1E5A4C3C0C5A4C3C02000000000000000000000000000000000000
      000000000000000000000000000000000000000000005A4C3C045A4C3C0C5A4C
      3C1D5A4C3C345A4C3C505A4C3A775D4F39AB635332DF625032FF5D4E4FFF6259
      94FF7371E3FF8485FFFF8B8CFFFF8B8BFFFF8888FFFF8888FFFF8888FFFF8888
      FFFF8888FFFF8888FFFF8888FFFF8888FFFF8888FFFF8888FFFF8888FFFF8888
      FFFF8888FFFF8888FFFF8888FFFF8888FFFF8888FFFF8888FFFF8888FFFF8888
      FFFF8888FFFF8888FFFF8888FFFF8888FFFF8888FFFF8888FFFF8888FFFF8B8B
      FFFF8A8BFFFF8082FFFF6C69CBFF5F5270FF5F4F3CFF635232F45F4F3BC05A4C
      3B86594C3C595A4C3C395A4C3C205A4C3C0D5A4C3C0300000000000000000000
      00000000000000000000000000005A4C3C045A4C3C0E5A4C3C215A4C3C3B594C
      3C595A4C3B81605035B7605037EA594A42FF4E447AFF6761AEFF7D7CF6FF8F90
      FFFF9494FFFF9192FFFF9090FFFF9090FFFF9090FFFF9090FFFF9090FFFF9090
      FFFF9090FFFF9090FFFF9090FFFF9090FFFF9090FFFF9090FFFF9090FFFF9090
      FFFF9090FFFF9090FFFF9090FFFF9090FFFF9090FFFF9090FFFF9090FFFF9090
      FFFF9090FFFF9090FFFF9090FFFF9090FFFF9090FFFF9090FFFF9090FFFF9090
      FFFF9090FFFF9393FFFF9394FFFF8989FFFF716FDCFF5E5488FF5E4D41FF6250
      32F65F4F3AC75B4D3B8D594C3C5E5A4C3C3D5A4C3C225A4C3C0D5A4C3C030000
      000000000000000000005A4C3C025A4C3C115A4C3C32594B3C5D5B4D3B8F6151
      35C5615133F34F4357FF413CA7FF5E5DE6FF8888FFFF999BFFFF9C9DFFFF999A
      FFFF9898FFFF9898FFFF9898FFFF9898FFFF9898FFFF9898FFFF9898FFFF9898
      FFFF9898FFFF9898FFFF9898FFFF9898FFFF9898FFFF9898FFFF9898FFFF9898
      FFFF9898FFFF9898FFFF9898FFFF9898FFFF9898FFFF9898FFFF9898FFFF9898
      FFFF9898FFFF9898FFFF9898FFFF9898FFFF9898FFFF9898FFFF9898FFFF9898
      FFFF9898FFFF9898FFFF9898FFFF9B9BFFFF9C9EFFFF9293FFFF7574E5FF6259
      8EFF5F4E44FF615032FA5F503ACC5B4D3B93594B3C5B5A4C3C2F5A4C3C0F5A4C
      3C0100000000000000005A4C3C03594B3C1E5C4D3C75615235CE604F33FC5144
      5BFF4E49AEFF6C6CF4FF9597FFFFA7A8FFFFA3A3FFFFA0A0FFFFA0A0FFFFA0A0
      FFFFA0A0FFFFA0A0FFFFA0A0FFFFA0A0FFFFA0A0FFFFA0A0FFFFA0A0FFFFA0A0
      FFFFA0A0FFFFA0A0FFFFA0A0FFFFA0A0FFFFA0A0FFFFA0A0FFFFA0A0FFFFA0A0
      FFFFA0A0FFFFA0A0FFFFA0A0FFFFA0A0FFFFA0A0FFFFA0A0FFFFA0A0FFFFA0A0
      FFFFA0A0FFFFA0A0FFFFA0A0FFFFA0A0FFFFA0A0FFFFA0A0FFFFA0A0FFFFA0A0
      FFFFA0A0FFFFA0A0FFFFA0A0FFFFA0A0FFFFA0A0FFFFA2A2FFFFA4A6FFFF9D9F
      FFFF8583EBFF675E91FF5E4F46FF604F34FF604F3ACE5C4D3C6E594B3C195A4C
      3C0200000000000000005A4C3C0661513E9664512FFF56485FFF5B56B9FF7F7F
      FAFFA4A5FFFFB1B1FFFFABABFFFFA8A8FFFFA8A8FFFFA8A8FFFFA8A8FFFFA8A8
      FFFFA8A8FFFFA8A8FFFFA8A8FFFFA8A8FFFFA8A8FFFFA8A8FFFFA8A8FFFFA8A8
      FFFFA8A8FFFFA8A8FFFFA8A8FFFFA8A8FFFFA8A8FFFFA8A8FFFFA8A8FFFFA8A8
      FFFFA8A8FFFFA8A8FFFFA8A8FFFFA8A8FFFFA8A8FFFFA8A8FFFFA8A8FFFFA8A8
      FFFFA8A8FFFFA8A8FFFFA8A8FFFFA9A9FFFFA8A8FFFFA8A8FFFFA8A8FFFFA8A8
      FFFFA8A8FFFFA8A8FFFFA8A8FFFFA8A8FFFFA8A8FFFFA8A8FFFFA8A8FFFFAAAA
      FFFFADAFFFFFA6A9FFFF8D8BF0FF776D8FFF615247FF615033FD61513E895A4C
      3C0200000000000000005F4F3C3E625243FF776F9FFF9797F9FFB1B3FFFFB9B9
      FFFFB3B3FFFFB1B1FFFFB1B1FFFFB1B1FFFFB1B1FFFFB1B1FFFFB1B1FFFFB1B1
      FFFFB1B1FFFFB1B1FFFFB1B1FFFFB1B1FFFFB1B1FFFFB1B1FFFFB1B1FFFFB1B1
      FFFFB1B1FFFFB1B1FFFFB1B1FFFFB1B1FFFFB1B1FFFFB1B1FFFFB1B1FFFFB1B1
      FFFFB1B1FFFFB1B1FFFFB1B1FFFFB1B1FFFFB1B1FFFFB1B1FFFFB1B1FFFFB1B1
      FFFFB1B1FFFFB1B1FFFFB2B2FFFFB2B3FFFFB7B8FFFFB5B5FFFFB3B3FFFFB1B1
      FFFFB1B1FFFFB1B1FFFFB1B1FFFFB1B1FFFFB1B1FFFFB1B1FFFFB1B1FFFFB1B1
      FFFFB1B1FFFFB3B3FFFFB6B7FFFFB4B5FFFF9A9AF5FF6F6699FF625143FE5F50
      3C2F00000000000000005F503B65635248FFA09FF4FFC5C6FFFFBBBBFFFFB9B9
      FFFFB9B9FFFFB9B9FFFFB9B9FFFFB9B9FFFFB9B9FFFFB9B9FFFFB9B9FFFFB9B9
      FFFFB9B9FFFFB9B9FFFFB9B9FFFFB9B9FFFFB9B9FFFFB9B9FFFFB9B9FFFFB9B9
      FFFFB9B9FFFFB9B9FFFFB9B9FFFFB9B9FFFFB9B9FFFFB9B9FFFFB9B9FFFFB9B9
      FFFFB9B9FFFFB9B9FFFFB9B9FFFFB9B9FFFFB9B9FFFFB9B9FFFFB9B9FFFFB9B9
      FFFFB9B9FFFFB9B9FFFFBEBEFFFF8F89B8FF8A84AFFFACABF7FFBEBEFFFFC1C1
      FFFFBDBDFFFFBBBBFFFFB9B9FFFFB9B9FFFFB9B9FFFFB9B9FFFFB9B9FFFFB9B9
      FFFFB9B9FFFFB9B9FFFFB9B9FFFFBBBAFFFFC5C7FFFF9290EBFF604F45FF5F50
      3C4A00000000000000005E4E3A65635348FFA6A5F3FFC8C9FFFFC2C2FFFFC2C2
      FFFFC2C2FFFFC2C2FFFFC2C2FFFFC2C2FFFFC2C2FFFFC2C2FFFFC2C2FFFFC2C2
      FFFFC2C2FFFFC2C2FFFFC2C2FFFFC2C2FFFFC2C2FFFFC2C2FFFFC2C2FFFFC2C2
      FFFFC2C2FFFFC2C2FFFFC2C2FFFFC2C2FFFFC2C2FFFFC2C2FFFFC2C2FFFFC2C2
      FFFFC2C2FFFFC2C2FFFFC2C2FFFFC2C2FFFFC2C2FFFFC2C2FFFFC2C2FFFFC2C2
      FFFFC2C2FFFFC6C8FFFFB6B5FBFF66564EFF5B4A2DFE675849FF7B718CFF9E99
      C5FFB9BAFFFFC8C9FFFFCACAFFFFC5C5FFFFC2C3FFFFC2C2FFFFC2C2FFFFC2C2
      FFFFC2C2FFFFC2C2FFFFC2C2FFFFC2C2FFFFCACCFFFF9594E5FF5F4F43FF5F4E
      3C4800000000000000005D4E3A63635247FFACACF3FFD1D1FFFFCACAFFFFCACA
      FFFFCACAFFFFCACAFFFFCACAFFFFCACAFFFFCACAFFFFCACAFFFFCACAFFFFCACA
      FFFFCACAFFFFCACAFFFFCACAFFFFCACAFFFFCACAFFFFCACAFFFFCACAFFFFCACA
      FFFFCACAFFFFCACAFFFFCACAFFFFCACAFFFFCACAFFFFCACAFFFFCACAFFFFCACA
      FFFFCACAFFFFCACAFFFFCACAFFFFCACAFFFFCACAFFFFCACAFFFFCACAFFFFCACA
      FFFFCBCBFFFFD3D3FFFF938EC2FF5C4B33FE6454405163533E775F4E32D9604E
      37FF685A5BFF867E9DFFA8A5E3FFC7C7FFFFD3D4FFFFCECEFFFFCAC9FFFFCACA
      FFFFCACAFFFFCACAFFFFCACAFFFFCACAFFFFD3D4FFFF9997E9FF5E4E44FF5F4E
      3B4800000000000000005D4E3A63635247FFB2B1F3FFD8D8FFFFD1D1FFFFD1D1
      FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1
      FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1
      FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1
      FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1
      FFFFD4D4FFFFCCCCFFFF6B5F71FF614F37BC0000000000000000000000006453
      413D62523A985F4E31ED5E4F3FFF706368FF8E87B3FFC2C1FEFFD5D5FFFFD1D1
      FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFDBDCFFFF9A99F2FF5D4D46FF5F4F
      3A4800000000000000005D4E3A63625347FFB7B7F3FFDFE0FFFFD8D8FFFFD8D8
      FFFFD8D8FFFFD8D8FFFFD8D8FFFFD8D8FFFFD8D8FFFFD8D8FFFFD8D8FFFFD8D8
      FFFFD8D8FFFFD8D8FFFFD8D8FFFFD8D8FFFFD8D8FFFFD8D8FFFFD8D8FFFFD8D8
      FFFFD8D8FFFFD8D8FFFFD8D8FFFFD8D8FFFFD8D8FFFFD8D8FFFFD8D8FFFFD8D8
      FFFFD8D8FFFFD9DAFFFFD9D9FFFFD8D8FFFFD8D8FFFFD8D8FFFFD8D8FFFFD8D8
      FFFFE1E2FFFFA8A5DEFF5A4A42FF65543E560000000000000000000000000000
      0000000000006555420965544053625036B4503F2DFFA5A1CEFFE1E3FFFFD8D8
      FFFFD8D8FFFFD8D8FFFFD8D8FFFFD8D8FFFFE3E3FFFF9F9EEFFF5D4D46FF5E4E
      3B4800000000000000005D4E3A63625347FFBDBDF3FFE8E8FFFFE0E0FFFFE0E0
      FFFFE0E0FFFFE0E0FFFFE0E0FFFFE0E0FFFFE0E0FFFFE0E0FFFFE0E0FFFFE0E0
      FFFFE0E0FFFFE0E0FFFFE0E0FFFFE0E0FFFFE0E0FFFFE0E0FFFFE0E0FFFFE0E0
      FFFFE0E0FFFFE0E0FFFFE0E0FFFFE0E0FFFFE0E0FFFFE1E1FFFFE2E2FFFFE6E6
      FFFFECECFFFFE5E6FFFFE8E9FFFFEBEBFFFFE5E5FFFFE2E2FFFFE1E1FFFFE2E3
      FFFFE5E6FFFF7A718EFF5E4D32D7655542050000000000000000000000000000
      00000000000000000000000000005C4E3D1A5D4B31F39993C3FFE7E8FFFFE1E1
      FFFFE0E0FFFFE0E0FFFFE0E0FFFFE0E0FFFFEBEBFFFFA6A5EEFF5C4D45FF5E4E
      3B4800000000000000005D4E3A63625347FFC3C2F3FFEEEEFFFFE6E6FFFFE6E6
      FFFFE6E6FFFFE6E6FFFFE6E6FFFFE6E6FFFFE6E6FFFFE6E6FFFFE6E6FFFFE6E6
      FFFFE6E6FFFFE6E6FFFFE6E6FFFFE6E6FFFFE6E6FFFFE6E6FFFFE6E6FFFFE6E6
      FFFFE6E6FFFFE6E7FFFFE8E8FFFFEAEAFFFFF1F1FFFFF2F2FFFFE6E8FFFFD1D1
      FEFFA7A4DBFF8B839EFF958EA9FFB2B1EDFFDBDCFFFFEAEBFFFFF3F4FFFFFAFC
      FFFFC2C1F1FF5E4E4CFF63533C76000000000000000000000000000000000000
      000000000000000000000000000060513F0E5D4D33E18580BFFFECEEFFFFE7E7
      FFFFE6E6FFFFE6E6FFFFE6E6FFFFE6E6FFFFF1F2FFFFAAA9EFFF5C4D46FF5E4E
      3B4800000000000000005D4E3A63625247FFC8C7F3FFF4F4FFFFECECFFFFECEC
      FFFFECECFFFFECECFFFFECECFFFFECECFFFFECECFFFFECECFFFFECECFFFFECEC
      FFFFECECFFFFECECFFFFECECFFFFECECFFFFECECFFFFECECFFFFEDEDFFFFEEEE
      FFFFF3F3FFFFFAFAFFFFF1F2FFFFE6E6FFFFBCBCF4FF9C96B8FF6E648CFF5546
      4BFF4D3C3DFF57452EF9584734F85B4B43FC66585DFF81799BFFA8A3C4FFA49F
      C3FF746979FF5F4F3AE76555420F000000000000000000000000000000000000
      0000000000000000000000000000000000005D4D35CC7770A4FFF2F3FFFFEEEE
      FFFFECECFFFFECECFFFFECECFFFFECECFFFFF8F8FFFFADADEFFF5C4D46FF5D4E
      3B4800000000000000005D4E3A63625247FFCCCAF3FFF9FBFFFFF1F1FFFFF1F1
      FFFFF1F1FFFFF1F1FFFFF1F1FFFFF1F1FFFFF1F1FFFFF1F1FFFFF1F1FFFFF1F1
      FFFFF1F1FFFFF1F1FFFFF2F2FFFFF4F4FFFFF6F6FFFFFEFEFFFFFDFEFFFFF2F2
      FFFFD7D7FDFFABA8D5FF8981A0FF695B6DFF5D4C45FF4F3C2EFF8A7E44FFC0B7
      7FFFA59B40FF73644DFD615035FE5B4A3FF4605039C65C4B30CB58483BFB5A48
      38F7604F36C86454424400000000000000000000000000000000000000000000
      0000000000000000000000000000000000005B4B36B5756972FFF6F8FFFFF3F3
      FFFFF1F1FFFFF1F1FFFFF1F1FFFFF1F1FFFFFDFDFFFFB1B0EFFF5C4D46FF5D4E
      3B4800000000000000005D4E3A63625247FFD0CFF3FFFFFFFFFFF6F6FFFFF6F6
      FFFFF6F6FFFFF6F6FFFFF6F6FFFFF6F6FFFFF6F6FFFFF6F6FFFFF6F7FFFFF7F7
      FFFFF8FAFFFFFDFFFFFFFFFFFFFFFAFCFFFFECEDFFFFBEBCF0FF9E98B2FF7369
      8AFF63534FFF59483EFF5C4A2FD65E4E36A65B4B3CDE6E602DFFEEEDB2FFFFFF
      ECFFE6E562FFE3E15CFFE0DC85FF877B47FF57453CEA5B4D3E725C4D3B365D4E
      3B28000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000005D4C36986D6168FFE9EBFFFFFBFB
      FFFFF6F6FFFFF6F6FFFFF6F6FFFFF6F6FFFFFFFFFFFFB4B3EFFF5C4B46FF5D4E
      3B4800000000000000005D4E3A62635247FFD3D1F3FFFFFFFFFFF9F9FFFFF9F9
      FFFFF9F9FFFFF9F9FFFFF9F9FFFFFAFAFFFFFCFCFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFF8FAFFFFD7D8FCFFADA9CFFF887F9DFF675A67FF5B4B44FF594835F1604F
      34B462523C705D4F3C2B594B3B0E5B4D3E72584734FFC2BC6EFFFFFFFDFFFFFF
      FEFFEEECAFFF8A7F37FF999245FFFFFF73FF9E9348FF554439FC5E4F3F8D584B
      3C1F5A4C3C125A4C3C0600000000000000000000000000000000000000000000
      00005A4C3C035A4C3C065A4C3C0A594B3C015E4F3687655661FFDBDBFFFFFFFF
      FFFFFAFAFFFFF9F9FFFFF9F9FFFFF9F9FFFFFFFFFFFFB6B5EFFF5C4C46FF5D4F
      3B4700000000000000005D4E3A61625247FFD4D4F3FFFFFFFFFFFCFCFFFFFCFC
      FFFFFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFEFEFFDFFBDBCECFF9D96
      ADFF716585FF61524CFF56463CFE5C4A2FD06151398C63533E496454420A0000
      000000000000000000005A4C3D3E58483CE98D823FFFFFFFE2FFFFFFFFFFFFFF
      FAFFFFFFFFFFF9F7D0FF827535FF857B27FFFFFF7FFFA79E69FF58453AFF5F4F
      3FA4594B3C2C5A4C3C185A4C3C0B5A4C3C055A4C3C075A4C3C0D5A4C3C145A4C
      3C1C5A4C3C255A4C3C2E5A4C3C37594C3C355E4F3788594B5BFFCDCEFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFCFCFFFFFDFDFFFFFFFFFFFFB8B7F1FF5C4B46FF5E4F
      3B4500000000000000005C4D3A5F635248FFD9D7F3FFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFF9F8FDFFD1D1F5FFACA7BFFF7F7699FF665856FF594A43FD5847
      32EC5D4D32BC5D4E3A825D4E3C395A4C3B0A0000000000000000000000000000
      00005A4C3C02594B3C205B4C40B366572FFFE5E3A7FFFFFFFFFFFFFFFDFFFFFE
      FCFFFFFFFDFFFFFFFFFFFDFDDCFF8B7F5DFF70632CFFE7E4B0FFB1A986FF5A48
      3BFF5F4F3DB3594B3D385A4C3C285A4C3C2B5A4C3C315A4C3C3A5A4C3C42594C
      3C48594B3C4F5A4C3B5A594C3B675A4C3B6F5F4F38A35C4C3EFF766D8AFFA8A2
      BFFFCBCBECFFF3F2FBFFFFFFFFFFFFFFFFFFFFFFFFFFBBBBF2FF5C4B47FF5E4F
      3A42000000000000000060503D61625247FFD4D4F5FFFFFFFFFFFFFFFFFFFEFE
      FFFFACABEEFF6A5F82FF504143FF4A3927FF594522FF5F4D35F1604F39A75B4D
      3B64594B3B43574A3A2E594B3B315A4C3C305A4C3C2A5A4C3C205A4C3C165A4C
      3C10594C3C165C4D3E75594733FFB2AA68FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFECFF93885FFF5A493DFFDBD8AEFFC0B9
      9DFF5D4B46FF5C4C3FC45A4C3B59584A3C565A4C3B655B4D3B6D5B4D3B7B5D4E
      39985D4E39B25E4D34CB5B4931E3554437FA584847FF5F504CFF5F5060FF6961
      99FF9D98BAFFD6D5F0FFFFFFFFFFFFFFFFFFFFFFFFFFB7B7F3FF5B4C46FF6454
      3F4300000000000000006555412C615141FC6E6487FFA099A6FFBAB8E1FFDFDF
      FCFFF0F1FFFFE5E6FFFFC4C2E8FFA69EA9FF746B8EFF66574EFD584843F95847
      31EB5C4D32CA5D4E39A75D4D3B7A5A4B3B5A5A4C3B49594B3C3D594C3C395A4C
      3C375A4C3D5558483DE581753EFFFFFFD6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7FFA2997AFF5A4A20FFD3D0
      8EFFD9D593FF6B5C32FF59483BDB5C4D36BD5D4D31DA594632F055443DFF5B4C
      4CFF625458FF6D6487FF8B83A6FFAEA8BBFFC2C2F5FFEBEBFFFFFFFFFFFFFBFE
      FFFFFFFFFFFFE3E4FFFFB8B6EEFFA9A4B9FF8C85A8FF695F82FF62523FF16555
      411700000000000000000000000065554250625036B0584730E7574744FF6253
      48FF6F6581FF9A93A5FFC0BDD6FFDCDEFFFFF5F6FFFFE8E9FFFFC7C6F4FFAAA3
      AFFF776D93FF685851FF594942FF594731EE5E4C32C85D4D3AA15C4D3B7B5A4B
      3B6A5B4C3FBD615030FFDDD998FFFFFFFFFFFFFFFFFFFFFFFDFFFFFFFDFFFFFF
      FDFFFFFFFDFFFFFFFDFFFFFFFDFFFFFFFEFFFFFFFFFFFFFFFDFFA89F7FFF5545
      1CFFD1CC56FFF2EF81FF6C5F5AFF605267FF746B9FFF9B95ADFFB8B5D0FFD2D2
      FFFFF8F9FFFFFAFDFFFFFDFFFFFFF6F8FFFFD1D1FDFFB4B2D0FF9A93ADFF7971
      9FFF675968FF5D4F51FF574847FF544437F75B4932D3625136A9655542490000
      0000000000000000000000000000000000000000000065554303655440316352
      3E67604F36A05A4930D957463EFE615248FF6B5F6BFF8C84A3FFBBB6B9FFCECF
      FFFFF2F3FFFFE4E6FFFFC2C1F0FFA9A2AEFF776E8EFF695A4FFF5A4A43FF5847
      31F253422BF8A59B5FFFFFFFF9FFFFFFFEFFFFFEFAFFFFFEF9FFFFFEF9FFFFFE
      F9FFFFFEF9FFFFFEF9FFFFFEF9FFFFFEF9FFFFFEFAFFFFFFFFFFFFFFFDFFBAB2
      8FFF56471BFFB9B62FFFFBFBD9FFF7F6FFFFF2F3FFFFF9FAFFFFDDDEFFFFBDBC
      E2FFA7A2B3FF7F78A7FF6A6082FF625351FF5C4C49FF54443CFC584733DE5F4E
      32B76252398B63533D6164543F356555410E0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000006554402063533E576250398D5D4B31C9574535F85D4E
      46FF6B5D5AFF7D759DFFB2ACB3FFC0C0FCFFE1E2FFFFDFE0FFFFBDBDF7FFA49C
      B3FF847B7BFFF1EFC7FFFFFFFFFFFFFEF4FFFFFEF3FFFFFEF3FFFFFEF3FFFFFE
      F3FFFFFEF3FFFFFEF3FFFFFEF3FFFFFEF3FFFFFEF3FFFFFEF4FFFFFFFAFFFFFF
      FFFFC1BA8CFF584B13FF9C992FFFFFFFBCFF9F9B7EFF554759FF5C4C4DFD5646
      42F3564534ED5D4C32C3614F369B62523D6E65543E4665554119000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000655541106353
      3E4761513C825F4E32B8584733EE5A4B45FF685950FF746A91FFA098ABFFB9B6
      E0FFF5F5F3FFFFFFF2FFFFFFEBFFFFFEEAFFFFFEEAFFFFFEEAFFFFFEEAFFFFFE
      EAFFFFFEEAFFFFFEEAFFFFFEEAFFFFFEEAFFFFFEEAFFFFFEEAFFFFFEEBFFFFFF
      EFFFFFFFF7FFD5D08FFF5C4B24FF80763FFFF0ED82FF9D933AFF544436FD5F4E
      409F5A4C3B275A4B3B0E00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000645443085F503D365C4D3A785E4E33C64E3C2AFF897D
      49FFFFFFD3FFFFFFE4FFFFFEE0FFFFFEE0FFFFFEE0FFFFFEE0FFFFFEE0FFFFFE
      E0FFFFFEE0FFFFFEE0FFFFFEE0FFFFFEE0FFFFFEE0FFFFFEE0FFFFFEE0FFFFFE
      E0FFFFFFE5FFFFFFEDFFDDD886FF5B4B34FF7B6F39FFFDFE5EFFB5AC49FF5847
      37FF5E4E42A3594C3C2B5A4C3C175A4C3C0A594B3C0100000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000584A3A135B4A40B66A5B33FFECEB
      9AFFFFFFE1FFFFFED6FFFFFED5FFFFFED5FFFFFED5FFFFFED5FFFFFED5FFFFFE
      D5FFFFFED5FFFFFED5FFFFFED5FFFFFED5FFFFFED5FFFFFED5FFFFFED5FFFFFE
      D5FFFFFED5FFFFFFD9FFFFFFE1FFE5E298FF64563CFF6C5C2FFFEEEC65FFC4C1
      4DFF5D4C37FF5C4C44BF5A4C3D3B594C3C155A4C3C0700000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000005A4C3C025A4C3C105D4E3F81594738FFBAB365FFFFFF
      D0FFFFFFCAFFFFFEC8FFFFFEC8FFFFFEC8FFFFFEC8FFFFFEC8FFFFFEC8FFFFFE
      C8FFFFFEC8FFFFFEC8FFFFFEC8FFFFFEC8FFFFFEC8FFFFFEC8FFFFFEC8FFFFFE
      C8FFFFFEC8FFFFFEC8FFFFFFCCFFFFFFD6FFE9E695FF6A5B3FFF5A4942FFD8D5
      70FFCDC861FF645535FF5C4B44C95B4C3D2C0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000005A4C3C0A5A4C3D4559483FEF887B44FFFFFFA7FFFFFF
      C0FFFFFEB9FFFFFEB9FFFFFEB9FFFFFEB9FFFFFEB9FFFFFEB9FFFFFEB9FFFFFE
      B9FFFFFEB9FFFFFEB9FFFFFEB9FFFFFEB9FFFFFEB9FFFFFEB9FFFFFEB9FFFFFE
      B9FFFFFFBBFFFFFFBDFFFFFFBCFFFFFFBFFFFFFFCDFFF6F4ADFF817555FF5D4D
      2DFFCDCC45FFDBD84EFF6A5C35FF5C4B44CC5B4D3D1800000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000005A4C3C055A4C3C1D5E4D40B463523BFFE2DE77FFFFFFB5FFFFFE
      ABFFFFFDAAFFFFFDAAFFFFFDAAFFFFFDAAFFFFFDAAFFFFFDAAFFFFFDAAFFFFFD
      AAFFFFFDAAFFFFFDAAFFFFFDAAFFFFFDAAFFFFFDAAFFFFFDADFFFFFFB4FFFFFF
      B5FFFFFFAEFFFFFF9FFFFEFEA3FFF6F599FFECE96FFFD8D570FF9D9557FF5746
      3CFF63562BFFCDC748FFF0EF54FF7E7137FF604F43C760514003000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000594B3C045D4E3F6657473EFFACA44EFFFFFF9CFFFFFE9BFFFFFD
      99FFFFFD99FFFFFD99FFFFFD99FFFFFD99FFFFFD99FFFFFD99FFFFFD99FFFFFD
      99FFFFFD99FFFFFD99FFFFFD99FFFFFD99FFFFFD9CFFFFFF90FFEAE65BFFC6C1
      65FFB3AC52FFACA41FFF948939FF847851FFA9A022FFBAB20FFFB2AA53FFC4BF
      67FFE7E21BFFD8D234FFC5BE83FF998E3DFF615141F066564318000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000005A4C3D115C4A41D3786B40FFF7F77AFFFFFF8DFFFFFE8AFFFFFF
      88FFFFFF87FFFFFF8CFFFFFE89FFFFFD88FFFFFD88FFFFFD88FFFFFD88FFFFFD
      88FFFFFD88FFFFFD88FFFFFD88FFFFFD88FFFFFF91FFFFFD59FF786A1FFF5543
      38FE54423CFFD0CA3FFFE3DF26FFC0BA59FFB3AA53FFB8AF1FFFA39B29FF7F75
      37FF706045FF66573DFF594741FF5D4C44E36555435E00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000605141805F4F42FFD1CB57FFFFFF81FFFFFF81FFFFFF75FFFBF8
      7BFFECE885FFE5E166FFFBF86DFFFFFE77FFFFFD76FFFFFD76FFFFFD76FFFFFD
      76FFFFFD76FFFFFD76FFFFFD76FFFFFD76FFFFFF7EFFFFFD4AFF73662DFB5A49
      45E461514AFFEFED55FF887C2FFF5A4939FF584643EE584748CC5B4B47AC5F4E
      448561514365625244426554421C655543010000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000615241105C4B43E5A39940FFFFFF5BFFFAF857FFC1BC5CFFB4AE2EFFA6A0
      41FFAEA82EFFC3BE12FFF7F452FFFFFF66FFFFFD62FFFFFD62FFFFFD62FFFFFD
      62FFFFFD62FFFFFD62FFFFFD62FFFFFD62FFFFFF68FFFEFB3EFF75672EFB5C4C
      46E2635249FFEDE93EFF817236FF594944990000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000066564310614F44E68E8234FFB4AC2AFFBEB73CFFCBC622FFC7C227FFB8B2
      46FFB1AA2BFFB2AA17FFF3F03CFFFFFF53FFFFFC4FFFFFFC4FFFFFFC4FFFFFFC
      4FFFFFFC4FFFFFFC4FFFFFFC4FFFFFFC4FFFFFFF54FFFCF931FF74662FFB5C4C
      46E4635348FFEBE733FF817436FF594A43900000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000655543535E4D44BE574742E15B4B3FFE655642FF675646FF5F4F
      41FD544245F35E4F40FFE5E231FFFFFF3EFFFFFC3CFFFFFC3CFFFFFC3CFFFFFC
      3CFFFFFC3CFFFFFC3CFFFFFC3CFFFFFC3CFFFFFF40FFFAF724FF746630FB5D4C
      47E4635347FFE9E628FF807335FF5A4A43930000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000006353431C625243496353434C6252
      42175D4D426E685A3EFFE6E323FFFFFF28FFFFFC28FFFFFC28FFFFFC28FFFFFC
      28FFFFFC28FFFFFC28FFFFFC28FFFFFC28FFFFFF2AFFF8F516FF736531FB5D4C
      47E4645446FFE8E41CFF7F7235FF5A4A44930000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00005B4C42676A5A3EFFE5E216FFFFFF16FFFEFB17FFFEFB17FFFEFB17FFFEFB
      17FFFEFB17FFFEFB17FFFEFB17FFFEFB17FFFFFF18FFF5F20BFF716432FB5D4C
      47E4645445FFE5E112FF7E7135FF5B4B44930000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00005D4D426B695A3EFFE1DD11FFFFFD0DFFF9F60FFFF9F60FFFF9F60FFFF9F6
      0FFFF9F60FFFF9F60FFFF9F60FFFF9F60FFFFEFB0FFFEEED07FF706333FB5D4D
      47E4645445FFE0DB0EFF7C6F35FF5B4B44930000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00005D4D426B695A3FFFDAD80DFFFAF708FFF3F00AFFF3F00AFFF3F00AFFF3F0
      0AFFF3F00AFFF3F00AFFF3F00AFFF3F00AFFF7F50AFFE8E504FF706334FB5E4D
      47E4645444FFDAD60CFF7B6E35FF5B4B44930000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00005D4D436B695A3DFFD7D507FFF5F203FFEEEB05FFEEEB05FFEEEB05FFEEEB
      05FFEEEB05FFEEEB05FFEEEB05FFEEEB05FFF2F005FFE4E001FF6F6234FB5F4D
      47E4645444FFD6D109FF7A6D35FF5C4B44930000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00005D4D436B685A3FFFD3D003FFEFED00FFE9E600FFE9E600FFE9E600FFE9E6
      00FFE9E600FFE9E600FFE9E600FFE9E600FFEDEB00FFDEDB00FF6E6134FB5F4D
      47E4655444FFD1CD07FF796B36FF5C4C44930000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00005D4E436B685A3FFFCDCB03FFE8E500FFE2DF00FFE2DF00FFE2DF00FFE2DF
      00FFE2DF00FFE2DF00FFE2DF00FFE2DF00FFE6E300FFD8D500FF6E6134FB5F4E
      47E4655444FFCBC707FF786B36FF5D4D44930000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00005D4E446B685A3CFFC8C501FFE1DE00FFDBD800FFDBD800FFDBD800FFDBD8
      00FFDBD800FFDBD800FFDBD800FFDBD800FFDFDC00FFD2CF00FF6D6034FB5F4E
      47E6655544FFC5C207FF776A36FF5D4D44930000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00005E4E446A6A5A3CFFC4C201FFDAD900FFD5D300FFD5D300FFD5D300FFD5D3
      00FFD5D300FFD5D300FFD5D300FFD5D300FFD8D700FFCCCB00FF6D6034FB5F4E
      47E4655544FFC1BE07FF766936FF5D4D44910000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00005E4E4468695A3CFFC0BD01FFD4D400FFD0CE00FFD0CE00FFD1CF00FFD1CF
      00FFD2D000FFD2D000FFD3D100FFD3D100FFD7D600FFCBC900FF6B5E34FD5D4B
      47F2605043FFBDB907FF776836FF5D4D448F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00005E4F4468695B3BFFC0BE00FFD2D000FFCBC900FFCBC800FFC9C600FFC7C5
      00FFC5C300FFC3C000FFBEBC00FFBBB900FFB7B500FFADAB00FF7B7132FF7467
      4AFF7B704DFFBDBA0BFF746637FF5F4F45910000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00006251465D695B3CFFAFAB01FFC4C200FFBBB900FFADAA01FFA2A002FF9D9A
      09FF9A9712FF969319FF8B8718FF8B851BFF8A831DFF8E861FFF948A26FF8F86
      28FF8C8326FF7F7532FF655547FF655543570000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00006555431F645444EC67593AFF6B5E33FF6C5F38FF6A5C3EFF695A41FF6A5B
      42FF6C5C45FF6A5947FF635347FF615047FC5F4E47F85E4D48F85D4D48F85F4D
      47E55F4F47D3604F47BE6555437F655543040000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000006555431362524659625147776252468462524682635245826352
      4480635244826453436E6454433E6554431F655543126555430B655543096555
      4304000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFFFFFFFFFFFFFFFFFFFE1FFFFFFFFFFFFFF807FFFFFFFFFF
      FFE001FFFFFFFFFFFF00007FFFFFFFFFFC00001FFFFFFFFFF0000007FFFFFFFF
      C0000001FFFFFFFE000000003FFFFFF8000000000FFFFFE00000000007FFFF00
      0000000000FFFC0000000000003FF00000000000000FC0000000000000038000
      0000000000018000000000000001800000000000000180000000000000018000
      0000000000018000000000000001800000000000000180000000001C00018000
      0000001F000180000000001FC00180000000003FC00180000000003FE0018000
      0000007FE0018000000001FFE00180000000007E000180000380000000018000
      1E000000000180000000000000018000000000000001C000000000000003F000
      00000000001FFF800000000007FFFFF8000000007FFFFFFF800000000FFFFFFF
      E00000000FFFFFFF800000001FFFFFFF800000000FFFFFFF0000000007FFFFFF
      0000000007FFFFFF000000000FFFFFFF000000001FFFFFFE0000001FFFFFFFFE
      0000001FFFFFFFFF0000001FFFFFFFFFE000001FFFFFFFFFFE00001FFFFFFFFF
      FE00001FFFFFFFFFFE00001FFFFFFFFFFE00001FFFFFFFFFFE00001FFFFFFFFF
      FE00001FFFFFFFFFFE00001FFFFFFFFFFE00001FFFFFFFFFFE00001FFFFFFFFF
      FE00001FFFFFFFFFFE00001FFFFFFFFFFE00001FFFFFFFFFFF0001FFFFFFFFFF
      FFFFFFFFFFFF2800000030000000600000000100200000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000005A4C3C035A4C3C0A5A4C3C0800000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00005A4C3C065A4C3C1A5A4C3B395A4C3A5B5A4C3A505A4C3B2A5A4C3C0E0000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000005A4C3C075A4C
      3C1D5A4C3B425A4C39795F4F32BD635238F5625135E75C4E359C5A4C3B5B594C
      3B2B5A4C3C0E0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000005A4C3C025A4C3C0F5A4C3C2A5A4C3B515B4D
      398B615135D1645232F65F546DFF5A56C4FF5C55ADFF635248FF635233E55E4F
      39AE5A4C3B625A4C3B345A4C3C155A4C3C030000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000005A4C3C0A5A4C3C23594C3B4B5B4D37835F502ECB6352
      37FF615350FF5854C0FF5C5DFFFF6061FFFF5F60FFFF5859F0FF5B5184FF6354
      3CFF61502EE35D4E35975A4C3B58594C3B285A4C3C0D00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000005A4C
      3C015A4C3C115A4C3B2D5A4C3B575D4E369A614F2FD7604F39FB5E5587FF625E
      CBFF6363FCFF6667FFFF6767FFFF6666FFFF6767FFFF6767FFFF6565FFFF6160
      E3FF6159A5FF5F504DFF62502CE35E4F35A75A4C395D594C3B2F5A4C3C105A4C
      3C01000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005A4C3C055A4C3C175A4C
      3B335A4C3B645D4D389F604F2CDA5F524FFF645982FF615DD2FF6A6CFFFF6F70
      FFFF6E6EFFFF6E6EFFFF6E6EFFFF6E6EFFFF6E6EFFFF6E6EFFFF6E6EFFFF6E70
      FFFF6D6FFFFF6362E7FF635A8FFF625357FF604F30DF5D4D36A55A4C3B615A4C
      3B2F5A4C3C145A4C3C0200000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000005A4C3C055A4C3C17594C3B3A594B396A5D4D
      34B0624F2DF35F4F43FF625A9BFF6F6FF5FF7577FFFF7779FFFF7676FFFF7676
      FFFF7676FFFF7676FFFF7676FFFF7676FFFF7676FFFF7676FFFF7676FFFF7676
      FFFF7676FFFF7778FFFF7577FFFF7171FFFF6059ABFF5F4F46FF62502EF35B4C
      35A8584B39635A4C3B325A4C3C11000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000005A4C3C065A4C3C195A4C3A39594B396C5C4D34AA5E4F31E16153
      59FF675E8DFF6E6CDCFF7D7EFFFF8080FFFF7E7EFFFF7E7EFFFF7E7EFFFF7E7E
      FFFF7E7EFFFF7E7EFFFF7E7EFFFF7E7EFFFF7E7EFFFF7E7EFFFF7E7EFFFF7E7E
      FFFF7E7EFFFF7E7EFFFF7E7EFFFF8080FFFF7E7FFFFF716FE6FF685E8AFF6152
      53FF5D4C31D95C4D359C594B395B5A4C3B2C5A4C3C0F5A4C3C01000000000000
      0000000000000000000000000000000000000000000000000000000000005A4C
      3C095A4C3B1D594B3B425D4E32795D4C2FBC5E4D35FA5E4F54FF655FB6FF8384
      FFFF888AFFFF8B8CFFFF8888FFFF8888FFFF8888FFFF8888FFFF8888FFFF8888
      FFFF8888FFFF8888FFFF8888FFFF8888FFFF8888FFFF8888FFFF8888FFFF8888
      FFFF8888FFFF8888FFFF8888FFFF8888FFFF8888FFFF8A8BFFFF898BFFFF7D7D
      FEFF655EA1FF605142FF5E4D32EC5C4D31A4594B3A605A4C3B2E5A4C3C100000
      000000000000000000000000000000000000000000005A4C3C095A4C3C275A4C
      3A505C4E378B63522CD1473C5DF850499DFF7B78CBFF8A89F9FF9495FFFF9596
      FFFF9394FFFF9393FFFF9393FFFF9393FFFF9393FFFF9393FFFF9393FFFF9393
      FFFF9393FFFF9393FFFF9393FFFF9393FFFF9393FFFF9393FFFF9393FFFF9393
      FFFF9393FFFF9393FFFF9393FFFF9393FFFF9393FFFF9393FFFF9394FFFF9698
      FFFF9395FFFF8484F2FF726CB8FF584C67FF5F4E2AE95D4E38B1594B39695A4C
      3B355A4C3C115A4C3C010000000000000000000000005A4C3B1B5A4C37735A4A
      2ECB594940FF484086FF6363F0FF9698FFFFA2A4FFFF9E9FFFFF9C9CFFFF9C9C
      FFFF9C9CFFFF9C9CFFFF9C9CFFFF9C9CFFFF9C9CFFFF9C9CFFFF9C9CFFFF9C9C
      FFFF9C9CFFFF9C9CFFFF9C9CFFFF9C9CFFFF9C9CFFFF9C9CFFFF9C9CFFFF9C9C
      FFFF9C9CFFFF9C9CFFFF9C9CFFFF9C9CFFFF9C9CFFFF9C9CFFFF9C9CFFFF9C9C
      FFFF9C9CFFFF9FA1FFFFA0A2FFFF8B8BFFFF68609BFF5E4F42FF5B4A32EB5A4A
      3594594B3B310000000000000000000000005A4B3C045F4F359B59483CF7635B
      A3FF8482DFFF989AFFFFAEAEFFFFABABFFFFA6A6FFFFA6A6FFFFA7A7FFFFA7A7
      FFFFA7A7FFFFA7A7FFFFA7A7FFFFA7A7FFFFA7A7FFFFA7A7FFFFA7A7FFFFA7A7
      FFFFA7A7FFFFA7A7FFFFA7A7FFFFA7A7FFFFA7A7FFFFA7A7FFFFA7A7FFFFA7A7
      FFFFA7A7FFFFA6A6FFFFA7A8FFFFA9AAFFFFA8A8FFFFA6A6FFFFA7A7FFFFA7A7
      FFFFA7A7FFFFA6A6FFFFA8A8FFFFACACFFFFA6A8FFFF9693E5FF7E78AAFF6152
      49FF5F4D36C95D4D3D3300000000000000005D4D39476A5C56FF8E8AD5FFAFB2
      FFFFBBBCFFFFB6B6FFFFB3B3FFFFB2B2FFFFB2B2FFFFB2B2FFFFB2B2FFFFB2B2
      FFFFB2B2FFFFB2B2FFFFB2B2FFFFB2B2FFFFB2B2FFFFB2B2FFFFB2B2FFFFB2B2
      FFFFB2B2FFFFB2B2FFFFB2B2FFFFB2B2FFFFB2B2FFFFB2B2FFFFB2B2FFFFB2B2
      FFFFB2B2FFFFB3B3FFFFB0B1FFFFACABFAFFB9BAFFFFB8B9FFFFB4B4FFFFB3B3
      FFFFB2B2FFFFB2B2FFFFB2B2FFFFB2B2FFFFB4B4FFFFB7B8FFFFB9BBFFFF9C9D
      FAFF6E6274FF5F50379B00000000000000005B4B326C74687BFFC5C6FFFFC2C2
      FFFFBEBEFFFFBDBDFFFFBDBDFFFFBDBDFFFFBDBDFFFFBDBDFFFFBDBDFFFFBDBD
      FFFFBDBDFFFFBDBDFFFFBDBDFFFFBDBDFFFFBDBDFFFFBDBDFFFFBDBDFFFFBDBD
      FFFFBDBDFFFFBDBDFFFFBDBDFFFFBDBDFFFFBDBDFFFFBDBDFFFFBDBDFFFFBDBD
      FFFFBDBDFFFFC5C5FFFFA8A5D1FF655546FF877F9EFFAEAEF8FFC2C3FFFFC6C7
      FFFFC5C5FFFFBFBFFFFFBDBDFFFFBDBDFFFFBDBDFFFFBDBDFFFFBFBFFFFFC7C8
      FFFF837AA2FF5C4A2EA500000000000000005A4B3367756975FFCACBFFFFCBCC
      FFFFC9C9FFFFC9C9FFFFC9C9FFFFC9C9FFFFC9C9FFFFC9C9FFFFC9C9FFFFC9C9
      FFFFC9C9FFFFC9C9FFFFC9C9FFFFC9C9FFFFC9C9FFFFC9C9FFFFC9C9FFFFC9C9
      FFFFC9C9FFFFC9C9FFFFC9C9FFFFC9C9FFFFC9C9FFFFC9C9FFFFC9C9FFFFC9C9
      FFFFCACAFFFFD1D3FFFF867EA9FF5B492A9B5E4C318064554BED776C72FF928B
      A6FFB4B3F9FFD1D3FFFFCECEFFFFC9C9FFFFC9C9FFFFC9C9FFFFCBCBFFFFCED0
      FFFF847B9DFF5A4A2FA200000000000000005A493267766B75FFD2D3FFFFD3D4
      FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1
      FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1
      FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1FFFFD1D1
      FFFFD8D8FFFFC3C3FFFF605357FD63523A110000000063533C125F4E35825B4A
      2FEC635450FF8A82A3FFC6C7FBFFD6D6FFFFD1D1FFFFD1D1FFFFD3D3FFFFD6D8
      FFFF847CA2FF5A492EA2000000000000000059493267796D75FFDBDCFFFFDDDF
      FFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADA
      FFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADAFFFFDADA
      FFFFDCDCFFFFDFDFFFFFE2E3FFFFE6E7FFFFE4E6FFFFE1E1FFFFDEDEFFFFDDDD
      FFFFE6E8FFFF948EB7FF5A4831C2000000000000000000000000000000006555
      420A5F4E364257452DECAFADD0FFE4E6FFFFDADAFFFFDADAFFFFDCDCFFFFE0E2
      FFFF8880A1FF5A482FA20000000000000000584932677B6F75FFE6E7FFFFE8E8
      FFFFE4E4FFFFE4E4FFFFE4E4FFFFE4E4FFFFE4E4FFFFE4E4FFFFE4E4FFFFE4E4
      FFFFE4E4FFFFE4E4FFFFE4E4FFFFE4E4FFFFE5E5FFFFE5E5FFFFE7E7FFFFEEEE
      FFFFF1F3FFFFE3E4FFFFCCCAF1FFB4AFCBFFBAB8DAFFD5D6FBFFEAEBFFFFF7F9
      FFFFEAEDFFFF776B70FF5F4D356B000000000000000000000000000000000000
      00000000000059493BC6AEA9CCFFF0F2FFFFE5E5FFFFE4E4FFFFE6E6FFFFEBED
      FFFF8C849FFF59472EA20000000000000000584832677C7075FFEDEEFFFFEFEF
      FFFFEBEBFFFFEBEBFFFFEBEBFFFFEBEBFFFFEBEBFFFFEBEBFFFFEBEBFFFFEBEB
      FFFFEBEBFFFFECECFFFFEFEFFFFFF2F3FFFFF6F7FFFFFCFDFFFFF2F4FFFFD3D2
      FEFF9B96B9FF64576AFF524138FF513F35F757463AF7625248FE827992FFADA8
      BFFF8C84A4FF605143D700000000000000000000000000000000000000000000
      000000000000574538A0A8A2C0FFFBFCFFFFECECFFFFEBEBFFFFEDEDFFFFF2F5
      FFFF8F87A1FF57472EA20000000000000000584832677E7175FFF5F5FFFFF6F6
      FFFFF2F2FFFFF2F2FFFFF2F2FFFFF2F2FFFFF2F2FFFFF3F3FFFFF4F4FFFFF6F6
      FFFFF9F9FFFFFFFFFFFFF8F9FFFFE3E3FFFFC9C6E1FFAFAABFFF7D7696FF5C4C
      50F44E3D2BFFA69B72FFADA567FF978C4BFD7D6E45FA56453BE7594730BE5544
      36C3594931B261523E3800000000000000000000000000000000000000000000
      00000000000052422E7CA59DA9FFFFFFFFFFF3F3FFFFF2F2FFFFF4F4FFFFFAFC
      FFFF9189A1FF57462EA20000000000000000584832677E7275FFFBFBFFFFFCFC
      FFFFF8F8FFFFF8F8FFFFF9F9FFFFF9FAFFFFFDFEFFFFFFFFFFFFFFFFFFFFFDFE
      FFFFF0F0FFFFBFBDE1FF80789CFF625554FF5E4E3EF8564436A956452C52513F
      31A48E8337FFFFFFF4FFF8F9AAFFABA32CFFD5D373FF988D45FF54433CB85849
      3A31000000000000000000000000000000000000000000000000000000000000
      00000000000054452B5E8D85A4FFFFFFFFFFFBFBFFFFF9F9FFFFFAFAFFFFFFFF
      FFFF938BA1FF57462EA1000000000000000057473265807375FFFFFFFFFFFFFF
      FFFFFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F9FFFFBFBEE2FF9D949EFF8479
      7AFF685958FD59483EB25A492F69615139395F503C0E0000000055473A5D6F61
      3AFBF6F4C9FFFFFFFFFFFFFFFFFFD4CFB6FF908736FFD2CC51FFA79C61FF5443
      39DF5A4B3C45584B3B0E5A4C3C055A4C3C045A4C3C0A5A4C3C125A4C3C1B5A4C
      3C26584A3B2758482C646C6397FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFF948CA1FF57462EA1000000000000000057483367837878FFFFFFFFFFFFFF
      FFFFFFFFFFFFEFEFF6FFCDCBEAFF9993AFFF695B67FF56463AF355432DD45747
      318D5748352F00000000000000000000000000000000584A3B144F3E31D6A79D
      6EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE4E0C5FF64552EFFD4CF8FFFA297
      7AFF544436E5584A3A45594B3B20594B3B29584A3B32594B3B40594B3B53594B
      37625749346C5947268F4E3F43FD827CB1FFCBCBF5FFFFFFFFFFFFFFFFFFFFFF
      FFFF9B93A5FF57462EA200000000000000005F4E385570636BFFD1D1FCFFFFFF
      FFFFFFFFFFFFC3C2E5FF746A8FFF55443BFF534128FE58462CD1574731915647
      344957493735584A3B2E584A3B27584B3C1A594B3B185443398D867A3AFFF8F6
      D8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE5E1CCFF7B6D57FFB0A8
      7BFFB1AA95FF5B493BE05746368357483385574731AC57452FC2524130D55848
      3FE7635552F7706796FFA59EA8FFB3AEC0FFD4D2EAFFF7F8FFFFFBFDFFFFD9DA
      FFFF7D7692FF5E4E34A0000000000000000066554207604F3FA75C4C46E66F65
      7FFFA199AAFFC7C4D1FFD3D2EBFFC5C4F3FFA9A5C5FFA1999DFF867B79FF695C
      55FB56483BC656462C9D594A3581584A385E5749397457462FFCD9D4ADFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFDEDACBFF7C6F
      47FFC6C06FFFB7AF63FF645554FF695C70FF877C7FFF9C949BFFB0ACBCFFB8B7
      F7FFDADBFFFFE1E1FFFFDEDEF2FFCFCDDDFFBAB5BCFF8B84A4FF64597BFF5C4C
      4BEA5F4F41C6645442430000000000000000000000000000000000000000604E
      342B5846327459493BC5625342FB6D5E55FF847DA1FFB0ACD1FFD8D9FFFFF0F1
      FFFFBEBBDDFF908AAAFF73665FFF675744FE534134F8948850FFFFFFF7FFFFFF
      FFFFFFFEFAFFFFFEF9FFFFFEF9FFFFFEF9FFFFFEFAFFFFFFFBFFFFFFFFFFF3F2
      E0FF6F612FFFB3AE3FFFFFFFE9FFF8F7FFFFECECFFFFCACAFFFFB5B1CAFF8981
      ADFF6C5F74FF675745FF604F41F959493CCE5444358F5B4930576150361A6353
      3C01000000000000000000000000000000000000000000000000000000000000
      0000000000000000000063523F1262513C365F4D305E5747398B62534CD67367
      6EFF948A8DFF9E97B1FFA5A2D8FFBCBAE5FFBCB9C6FFF7F6D4FFFFFFF8FFFFFF
      F1FFFFFEF0FFFFFEF0FFFFFEF0FFFFFEF0FFFFFEF0FFFFFEF1FFFFFFF2FFFFFF
      FEFFF3F1DDFF867C3BFF928A41FFEEED9BFF716458FF524145E54E3E318C5746
      2D675C4C334661503D2A63533F0B000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000006150
      3A305D4B347D5A4A2FC55A4940F55A4B51FFBBB8B5FFFFFFF4FFFFFFE8FFFFFE
      E5FFFFFEE5FFFFFEE5FFFFFEE5FFFFFEE5FFFFFEE5FFFFFEE5FFFFFEE5FFFFFF
      E7FFFFFFF8FFF8F7C5FF756735FF8F8355FFD2CC4CFF645436F255463B5D0000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000594B3A01544338A3877B38FFF6F4B1FFFFFFDFFFFFFED8FFFFFE
      D7FFFFFED7FFFFFED7FFFFFED7FFFFFED7FFFFFED7FFFFFED7FFFFFED7FFFFFE
      D8FFFFFFD9FFFFFFE6FFF5F4BEFF82754CFF8F8536FFD7D451FF72653DFF5444
      4198594B3C1B594B3C0200000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000058493C44594937FDDBD68FFFFFFFD7FFFFFFC7FFFFFEC6FFFFFE
      C6FFFFFEC6FFFFFEC6FFFFFEC6FFFFFEC6FFFFFEC6FFFFFEC6FFFFFEC6FFFFFE
      C6FFFFFFC7FFFFFFC8FFFFFFD5FFFFFFB9FF7E7053FF867A4DFFD1CD5DFF7A6D
      41FF55453F980000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000594B3B1253413AB7988D4BFFFFFFB4FFFFFFB6FFFFFEB2FFFFFEB2FFFFFE
      B2FFFFFEB2FFFFFEB2FFFFFEB2FFFFFEB2FFFFFEB2FFFFFDB2FFFFFFB5FFFFFF
      B9FFFFFFB9FFFFFFBEFFFFFFBDFFFFFFB8FFFFFFABFF837653FF6A5C33FFDFDD
      45FF8B8039FF5B4A419600000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000057483D696D5D3EFFF3F181FFFFFFA8FFFFFEA0FFFFFD9FFFFFFD9FFFFFFD
      9FFFFFFD9FFFFFFD9FFFFFFD9FFFFFFD9FFFFFFD9FFFFFFEA2FFFFFF99FFEEEB
      7EFFD8D482FFD4CE61FFC9C476FFD6D14EFFC9C423FF9F994DFF968B33FFCEC7
      49FFC5BE61FF62523DFF6454431B000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000051403ED4ACA35BFFFFFF8FFFFFFF8EFFFFFF8FFFFFFF8EFFFFFF8AFFFFFD
      88FFFFFD88FFFFFD88FFFFFD88FFFFFD88FFFFFD88FFFFFF92FFFBF850FF6A5C
      28FE564640FFC9C22AFFD3D123FFA49C62FFA49A27FF8A8129FF76673EFF6C5C
      45FF665547F66555438300000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000005848
      4073958948FFFFFF6AFFEEED86FFDDD959FFBFBA5CFFB9B348FFF6F465FFFFFF
      72FFFFFD6FFFFFFD6FFFFFFD6FFFFFFD6FFFFFFD6FFFFFFF79FFF9F73DFF6455
      2EF35C4A4BFBD2CC3EFF756633F753423F86584847695E4C47516150443A6252
      431F6353420A0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000005E4D
      44CFC3BD35FFFFFF40FFC7C237FFAFAA28FFBFBA2EFFD8D403FFFBF84AFFFFFE
      5CFFFFFC59FFFFFC59FFFFFC59FFFFFC59FFFFFC59FFFFFF5FFFF8F532FF6759
      32F25F4F4BFBCEC82DFF6F603CE8000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000006454
      444166573BBE6D5F3BF385793AFF877B40FF746644F96B5C3BFFEBE737FFFFFF
      40FFFFFC3FFFFFFC3FFFFFFC3FFFFFFC3FFFFFFC3FFFFFFF43FFF4F223FF6657
      34F2604F4AFCCCC626FF72623BED56483D030000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000006050440A5E4D443D5E4D433D5948433C605042FDEAE724FFFFFF
      23FFFFFD24FFFFFC24FFFFFC24FFFFFC24FFFFFD25FFFFFF27FFF2F012FF6557
      37F2615048FCC9C31DFF71613CED56483E040000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000057493F1F655442FFE9E414FFFFFF
      10FFFDFA11FFFCF911FFFCF911FFFCF911FFFDFA11FFFFFF12FFECE908FF6456
      39F2615148FCC5BF18FF70603CED57483E040000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000594A3F27645442FFE1DE0FFFFBF9
      09FFF4F10BFFF4F10BFFF4F10BFFF4F10BFFF4F10BFFFBF80AFFE4E104FF6455
      3AF2615148FCBEB816FF6E613CED57483E040000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000594A4028645543FFDCD909FFF5F2
      03FFEEEB05FFEEEB05FFEEEB05FFEEEB05FFEEEB05FFF5F203FFDEDB01FF6455
      3AF2625148FCBAB414FF6E603CED57483E040000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000594A4028655543FFD5D206FFEDEA
      00FFE6E300FFE6E300FFE6E300FFE6E300FFE6E300FFEDEA00FFD7D300FF6456
      3AF2625147FCB4AE13FF6D5E3DED57493E040000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000594A4028655543FFCECB03FFE3E2
      00FFDEDB00FFDEDB00FFDEDB00FFDEDB00FFDEDB00FFE3E000FFCECC00FF6556
      3BF3625247FDAFA913FF6C5E3DED57493E040000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000005A4B4027655642FFC7C402FFDADA
      00FFD6D300FFD6D300FFD6D300FFD6D300FFD7D500FFDCDA00FFCAC700FF6455
      3AF2625146FDABA413FF6C5E3DEC57493E030000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000594B4026665542FFC4C002FFD7D4
      00FFD4D200FFD5D300FFD6D500FFD4D300FFD1D000FFD1D000FFBBBA00FF5F51
      40FA615050FFAAA413FF6C5E3CEE58493E030000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000005F4F4327655641FFC1BE00FFCECD
      00FFBCBB00FFB1B000FFABA900FFA5A500FFA5A300FFA4A300FFA19E01FF9790
      26FF979035FF9B9526FF665743F3645443010000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000066554409655443DC796D2EFF8075
      2BFF776C2FFF736632FF726639FF6F623DFF6A5D3DFF6B5C3EFC6D5E40FA7062
      3CF56D5F38DD655443BC63534661000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000065544308614F47585F4E
      497D604F487D6050477B6150467B6250466A6252463362524511625245076252
      4504000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFE3FFFFF0000FFFFF80FFFFF0000FFFF
      E003FFFF0000FFFF00007FFF0000FFFE00003FFF4020FFF0000007FF398DFFC0
      000001FF35F8FF00000000FF5AFFFC000000001FB3FFF0000000000FFFFFC000
      00000001F7FFC000000000037BFF800000000001FFFF800000000001F3FF8000
      00000001F3FF800000000001F3FF800000004001F3FF80000000F001F3FF8000
      0000FC01FAFF80000001FC018CFF80000001FC012FFF80000007FC017EFF8000
      200000014DFD8003C000000134ED800000000001369B8000000000013E46F000
      000000070000FE00000000FF0000FFF000000FFF0000FFFC000001FF0000FFFC
      000003FF0000FFF8000001FF0000FFF8000000FF0000FFF8000001FF0000FFF0
      000003FF0000FFF00000FFFF0000FFF000007FFF0000FFFC00007FFF0000FFFF
      80007FFF4110FFFF80007FFF3C82FFFF80007FFF33EEFFFF80007FFF50FFFFFF
      80007FFFABFFFFFF80007FFFF3FFFFFF80007FFFEBFFFFFF80007FFFEAFFFFFF
      8000FFFFEAFFFFFFC007FFFFEAFF280000002000000040000000010020000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000005A4C3B025A4C3A0D5A4C3B030000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000005A4C3A055A4B35345D4C2D865E4F32BD5E4D2E8D5A4B
      35355A4C3A040000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00005A4C3A0C594B33435D4C2B9860503BE75F5484FF5B57C6FF5F568CFF5F4F
      3DE75D4C2A91594B343B5A4C3A0B000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000594B38235A4B
      2E635C4B2AAB5E4F49F4605896FF6261EAFF6365FFFF6566FFFF6365FFFF6261
      EBFF605790FF5E4F42EC5C4A2AAB5A4B3056584B381200000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000005A4B3B03594A352E5A4A2B7C5B4D34D26254
      62FF665FADFF6C6BF9FF7071FFFF7171FFFF7070FFFF7070FFFF7070FFFF7071
      FFFF6F71FFFF6B69F1FF655EADFF615256FF5C4B2CBA594A2E5C584B38160000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000594B37145949304358482A8F5D4C3AE1645A81FF726ED7FF7C7C
      FFFF7D7FFFFF7E7FFFFF7D7DFFFF7D7DFFFF7D7DFFFF7D7DFFFF7D7DFFFF7D7D
      FFFF7D7DFFFF7E7FFFFF7E7FFFFF7979FFFF6E67BDFF61545EFF5A492DBF5849
      2B68594B362A00000000000000000000000000000000000000005A4C3B01594B
      391F5A4A2E62564834BA584B52F36C6395FF807EE8FF8B8DFFFF8F91FFFF8E8E
      FFFF8C8CFFFF8C8CFFFF8C8CFFFF8C8CFFFF8C8CFFFF8C8CFFFF8C8CFFFF8C8C
      FFFF8C8CFFFF8C8CFFFF8C8CFFFF8F8FFFFF8E90FFFF898AFFFF7773C3FF6155
      72FF5A4A39DB59492D83594B392F5A4B3B050000000000000000584932465847
      28C64E4363FF6865DAFF908FFFFF9D9FFFFF9FA0FFFF9C9CFFFF9A9AFFFF9A9A
      FFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9A
      FFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9AFFFF9A9BFFFF9D9DFFFF9FA1FFFF999B
      FFFF8583EAFF65586FFF584729E357483065000000005A4B372762544FE6746E
      B1FF9092FFFFAFB0FFFFADADFFFFA8A9FFFFA8A8FFFFA8A8FFFFA8A8FFFFA8A8
      FFFFA8A8FFFFA8A8FFFFA8A8FFFFA8A8FFFFA8A8FFFFA8A8FFFFA8A8FFFFA8A8
      FFFFA8A8FFFFA9A9FFFFADAFFFFFB0B1FFFFACADFFFFA9A9FFFFA9A9FFFFAAAA
      FFFFAFB0FFFFA2A3FFFF8781BAFF695A5BFE5B4B374C5949376D9792D0FFC5C8
      FFFFC1C1FFFFBABAFFFFB9B9FFFFB9B9FFFFB9B9FFFFB9B9FFFFB9B9FFFFB9B9
      FFFFB9B9FFFFB9B9FFFFB9B9FFFFB9B9FFFFB9B9FFFFB9B9FFFFB9B9FFFFB9B9
      FFFFBABAFFFFC1C1FFFF9E9AD1FF958FBAFFB6B7FFFFC4C6FFFFC2C4FFFFBCBC
      FFFFBABAFFFFBDBDFFFFC5C7FFFFA5A4F7FF5F4F439C57473670A8A4D2FFD5D7
      FFFFCBCBFFFFCACAFFFFCACAFFFFCACAFFFFCACAFFFFCACAFFFFCACAFFFFCACA
      FFFFCACAFFFFCACAFFFFCACAFFFFCBCBFFFFCBCBFFFFCBCBFFFFCBCBFFFFCBCB
      FFFFD2D2FFFFC1C0FAFF62544DEC5B4929706A5A51CD8A8295FFAFADDFFFCECF
      FFFFCDCDFFFFCBCBFFFFD2D2FFFFB6B6FCFF5D4E429B5646356DB0ADD1FFE4E5
      FFFFDADAFFFFD9D9FFFFD9D9FFFFD9D9FFFFD9D9FFFFD9D9FFFFD9D9FFFFDADA
      FFFFDADAFFFFDADAFFFFDCDCFFFFE1E1FFFFE6E8FFFFE6E8FFFFE7E8FFFFE3E4
      FFFFE9EBFFFF9B96B5FF58462E8900000000000000005C4A2F2A54432CA09790
      B1FFE4E5FFFFDBDBFFFFE2E2FFFFC0BFFDFF5C4D43995445356DBAB5D0FFF2F4
      FFFFE7E7FFFFE6E6FFFFE6E6FFFFE6E6FFFFE7E7FFFFE7E7FFFFE9E9FFFFECED
      FFFFF3F3FFFFF7F8FFFFEDEFFFFFD1D0F8FFA19CBFFF8D859BFEA9A4C1FFDAD9
      FEFFE1E1FFFF706363E3614F37050000000000000000000000005A48291D7067
      95FFE9EAFFFFEBEBFFFFEFF0FFFFCBCBFBFF5C4C41995444356DC2BDD0FFFFFF
      FFFFF3F3FFFFF3F3FFFFF5F4FFFFF7F7FFFFFCFEFFFFFFFFFFFFFCFEFFFFEFEF
      FFFFCFCEE8FFA69FB2FF736773F57B6E55FFA69C61FF7A6B30FF5B4A36F6594A
      45C5645651BE5D4D3943000000000000000000000000000000005B4B33035F53
      5EFBEEEEFFFFF9FAFFFFFCFEFFFFD6D6FDFF5C4D41995444366DC8C3D1FFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE1E0F4FFB4B0C1FF8A818AFE675A
      55C85C4C3B874A392944513E1EB9DBD8B1FFFFFFDFFFC3BD5CFFB5AE4AFF6F61
      3AD24E3F302E00000000000000000000000000000000000000005547330D6050
      42E4E7E7FEFFFFFFFFFFFFFFFFFFDDDDFDFF5D4D43985445376FCECBD3FFFFFF
      FFFFFEFEFFFFC8C5DBFF968D93FF726666FC5A4A3DC24E3E2D6151412A120000
      00000000000049392C439B9062FFFFFFFFFFFFFFFFFFFFFFF6FFA79E64FFB0A8
      76FF6E5F50E75142373A574A3912574937235648363A554632515141296A503E
      29E4837CA2FFECEDFFFFFFFFFFFFE5E5FFFF5D4F449B5C4B3A5F9F97A4FFEBEC
      FCFFD1D0F5FF8C849DFF6F6266FF635441E35244328F4F40295053452F305546
      33155042331A5E4E31CCE7E5C8FFFFFFFFFFFFFFFFFFFFFFFFFFF1EFE9FF9B91
      76FFA9A073FF6F6142D648372E8E57473BAE655549CC74696AE9928996FFA29C
      B1FFA8A3BAFFCAC9EDFFE2E0F4FFADA9C6FF6050438E000000005B4B3B585F4E
      3F99746866D4968E99FFAEA6B7FFB3AFCEFFB1ADC8FF9D94A4FF7D717BF3685A
      4FC14F3F2EC5B0A780FFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFF5F3
      EFFFA2995FFFB1AA51FFB5AEB5FFB7B3D6FFBEBCE9FFC0BCD7FFAEA9BDFF9B94
      A6FF8A7F84EA6A5D58BD5D4D40915A493A6463523F0F00000000000000000000
      0000000000005C4A3212594833426453427A74665EBB897F8CF5A29AAEFF9D97
      C3FFAAA4B1FFFEFDE9FFFFFFFAFFFFFFF1FFFFFEF0FFFFFEF0FFFFFFF1FFFFFF
      FEFFF9F7EBFFA09956FFB7B16EFFABA476FF5E4D44DD52423A6D51422F365949
      2F0F000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000004D3C2C4C5545
      31F9DAD7A6FFFFFFEEFFFFFFDDFFFFFEDCFFFFFEDCFFFFFEDCFFFFFEDCFFFFFF
      DDFFFFFFE9FFFFFFDCFFADA36DFF9B903AFFA09743FC56463A7D000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000503F3488BAB3
      6EFFFFFFC9FFFFFFC5FFFFFEC2FFFFFEC2FFFFFEC2FFFFFEC2FFFFFFC2FFFFFF
      C6FFFFFFCBFFFFFFD6FFFFFFDCFFC3BD86FF91874BFFA49B46FF61523D980000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000004E3D383C887C4DFFFFFF
      A7FFFFFFACFFFFFFA5FFFFFDA3FFFFFDA2FFFFFDA2FFFFFDA3FFFFFFA6FFFFFD
      96FFE1DD80FFD6D06AFFCEC871FFD7D254FF9E944AFFCDC83EFF9C914DFF5C4D
      3F4F000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000655541BDE8E571FFFFFF
      87FFF5F37EFFFBF97BFFFFFE80FFFFFD7FFFFFFD7FFFFFFE7FFFFFFF88FFD6D0
      50FD5C4A33F9B1A72EFF928739FF807336EB796B38C96D5F43A66758477A6454
      420E000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000584942219B913AFFFFFF4CFFDAD6
      2FFFC7C131FFDFDB29FFFFFF5EFFFFFE5CFFFFFD5CFFFFFE5CFFFFFF62FFD3CD
      3CFB5E4C3FF1A89D3FFF5C4B3BB2000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000006353440466553E9173653BCD8478
      3AE0675841D0A49B31FFFFFF39FFFFFE3BFFFFFC3BFFFFFD3BFFFFFF3CFFD3CD
      2CFB605041F3AAA034FF64543DAF000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00004938451E9D942EFFFFFF13FFFFFE17FFFFFC17FFFFFC17FFFFFF16FFD0CA
      19FB615043F3A89E2AFF63553FB2000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00004D3D44229D9329FFFFFF04FFF7F409FFF4F109FFF5F209FFFFFE06FFC7C2
      13FB605044F3A29927FF63543FB2000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00004F404525988F25FFF5F200FFEBE700FFE9E500FFEAE600FFF4F100FFC0B9
      0FFB615044F39D9225FF625340B2000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000051414525948A24FFE7E600FFDDDC00FFDDDB00FFDEDB00FFE8E500FFB7B1
      0EFB5F4F44F3978E25FF635340B2000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000052424525948B21FFE1E100FFD7D600FFD4D300FFD5D300FFDDDE00FFB5B1
      0CFF6A5C40FE978D23FF635440B4000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00005D4C471D817629FFB2AF06FFA29C10FF9A9418FF938E1DFF89821DFF847A
      25FF7F7339FF7C7038FF64544487000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000006253454D6051457D6150477C6251487A6050496C5D4C4A485E4D
      493E605045376150461C00000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFE3FFFFFF80FFFFFE0
      03FFFF8000FFFC00003FF000000F800000018000000100000000000000000000
      000000000300000003800000078000000F80003000000000000080000000F000
      001FFF80007FFF80003FFF00001FFF00001FFE0003FFFE0003FFFFE003FFFFE0
      03FFFFE003FFFFE003FFFFE003FFFFE003FFFFF007FF28000000180000003000
      0000010020000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000005A4B34125B4B2F335A4B360700000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000594A31185A4A256B5E4F47D45E5271FF5C4E39BF5A4A2854584A33070000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000059492B2A5949
      2A825E4F4CE0605895FF6160F4FF6163FFFF605DE4FF60577FFF5B4C3DC45848
      295E5949310E0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000594B36025747294359492FB0605468FA6A65
      C7FF6E70FFFF7173FFFF7272FFFF7172FFFF7272FFFF7173FFFF6D6DF5FF675E
      A5FF5B4D47DF5746267257493016000000000000000000000000000000000000
      0000000000005949310B55452858574734BD635877FF7874D8FF8283FFFF8486
      FFFF8383FFFF8383FFFF8383FFFF8383FFFF8383FFFF8383FFFF8485FFFF8486
      FFFF7E7EFCFF6E66A6FF5B4C47E05544277B57472F1D00000000000000000000
      000051422767524447D2625CA4FF8684E8FF9799FFFF999BFFFF9798FFFF9797
      FFFF9797FFFF9797FFFF9797FFFF9797FFFF9797FFFF9797FFFF9797FFFF999B
      FFFF9A9BFFFF9A9CFFFF8F8FFCFF7770B3FF5E4F4CE853422683564732075E4E
      4374827BADFF9B99FCFFAEB0FFFFB3B4FFFFAEAEFFFFACACFFFFACACFFFFACAC
      FFFFACACFFFFACACFFFFACACFFFFACACFFFFACACFFFFADADFFFFB0B0FFFFACAC
      FFFFB7B9FFFFB6B8FFFFB3B5FFFFB2B4FFFFA6A6FFFF8C85B8FF61534D8F6A5E
      62ADCCCDFFFFCDCEFFFFC4C5FFFFC4C4FFFFC3C3FFFFC3C3FFFFC3C3FFFFC3C3
      FFFFC4C4FFFFC4C4FFFFC4C4FFFFC4C6FFFFC5C5FFFFCACBFFFFBAB8EEFF695B
      52E0888097EFB0AEE4FFC8CAFFFFC8C9FFFFCBCBFFFFCDD0FFFF756B7DB96C5E
      5EA7D9DBFFFFDBDBFFFFD6D7FFFFD6D6FFFFD6D6FFFFD7D7FFFFD7D7FFFFD9D8
      FFFFDBDCFFFFE1E2FFFFE1E3FFFFDEE0FFFFE7E9FFFFEFF2FFFFA09AB6FF5644
      2720000000005D4C3475766A74EBD6D7FFFFDEDEFFFFDCDDFFFF756C7CB56C61
      5EA5EDECFFFFEEEEFFFFE9E9FFFFEAEAFFFFECECFFFFF0F2FFFFFAFBFFFFF8FA
      FFFFEFF0FFFFCAC7ECFF9890A2FF80757DFEA49FB7FEC4C1E4FF6D5E59B40000
      000000000000000000005A49358BD5D5F9FFF5F6FFFFF0F0FFFF7A7079B56F63
      5EA6FCFCFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2F2FFFFC0BCD4FF978C
      91FA675A5DB167573EE8C8C27CFF988E33FF605029E24D3D346C574834040000
      000000000000000000005847366DDCDAE3FFFFFFFFFFFFFFFFFF7D747BB57267
      61ACFFFFFFFFFFFFFFFFDFDDECFFA59EA6FF837772E45E4F46844E3E2D370000
      000041311650C3BC98FFFFFFFFFFE0DBA5FFA29850FF6B5B47B94A3C32145141
      300250422D1F4C3D25314B392990ABA6BFFFFFFFFFFFFFFFFFFF83797FBA6757
      4D75B2AAAEFFB9B3C8FF958EA6FF827881FF706361CE5D4F3F7C4F3F2A3F4333
      1D3C8D815EE7FFFFFFFFFFFFFFFFFFFFFFFFD7D4C6FFA19765FF817546E16B5E
      68BD89808CE19D94A3FFA19BB0FF9A94B1FFB0ABC1FFB1AAB3FF6D605C8B0000
      000054422E055C4B38486B5C4B8E7B6F74C3A29CAAFFA59FB7FF8C839EFF8277
      78FFEDEAD7FFFFFFFFFFFFFFFBFFFFFFFDFFFFFFFFFFDFDCC0FFAFA95AFFD9D6
      BEFF988FA7FF746B82BF706158986958466D5948373354422E01000000000000
      000000000000000000000000000000000000000000005B4B3C315A4B44C6C7C2
      A9FFFFFFF5FFFFFFE3FFFFFEE1FFFFFFE1FFFFFFE5FFFFFFF8FFE5E3BCFF9C91
      4AFF867A2EFA4F3E2E4B00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000047382E0584774ADBFFFF
      BCFFFFFFC4FFFFFFBEFFFFFEBDFFFFFFBDFFFFFFC0FFFFFFC1FFFFFFCDFFF2F0
      AAFFA9A259FFA69D3EFF5D4F3D6B000000000000000000000000000000000000
      000000000000000000000000000000000000000000005D4E3CA0ECEA87FFFFFF
      A2FFFFFF95FFFFFE94FFFFFD94FFFFFF97FFFFFD87FFA49B56FFC3BC4BFFC0B9
      48FF989036FF9D9246FF726346B4000000000000000000000000000000000000
      00000000000000000000000000000000000055453F10ABA343FFF9F755FFD0CD
      43FFF2EF4CFFFFFF69FFFFFE65FFFFFF69FFFAF753FF5B4B37FA9C913CFF6454
      3A765E4E3F2A5B49431200000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000006B5B406D6D5E398F5848
      379BC1B92CFFFFFF2FFFFFFE2FFFFFFF30FFFAF728FF6A5C3EFB9A8F36FF5749
      3A2B000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000004736
      4317BAB220FFFFFF06FFFBF80BFFFFFE09FFF3F009FF6A5C3CFD998D2DFF5B4C
      3D32000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000004E3E
      4321B4AC19FFFAF800FFECE900FFF1EE00FFE4E000FF6A5B3CFC94882BFF5A4C
      3E33000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000004F41
      4323ADA517FFEBE900FFE0DE00FFE6E300FFD9D700FF66583DFF8D832BFF594B
      3E34000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000005546
      4522A49E14FFD4D300FFBEBD00FFBCBB00FFB5B300FF837A32FF8B812DFF5F4E
      422E000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00006D5E3B95746634BE6C5E3BB76558419B60524178695A42776656435C0000
      000000000000000000000000000000000000000000000000000000000000FFE3
      FF00FF80FF00FE003F00F0000F00C0000312800000420000007A000000BB0000
      40F50000E0FF0000E0FF008000FF000000FF800001FFFC001FFFFC000FFFFC00
      0FFFF8001FFFFC007FFFFF007FFFFF007FFFFF007FFFFF007FFFFF80FFDD2800
      0000100000002000000001002000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000594A29325A4A315859492806000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000564626455C4E
      53B65F58B5FF5F5CDDFF5D547EE6584931765646231300000000000000000000
      00000000000000000000000000005343230E55462E6A615670DA6F69C7FF7474
      FFFF7577FFFF7577FFFF7576FFFF7170ECFF665E92F958493D8D524224220000
      00000000000053452B0551423B8F665E96F88885E4FF9395FFFF9395FFFF9393
      FFFF9393FFFF9393FFFF9494FFFF979AFFFF9A9DFFFF8E8FFEFF756DA6FF5748
      3EA553432A10695C61A4A09EF7FFB4B7FFFFB5B6FFFFB2B2FFFFB2B2FFFFB2B2
      FFFFB3B3FFFFB4B4FFFFB7B8FFFFABABF2FFA8A6F1FFC0C2FFFFBFC3FFFFABAA
      FEFF6E636DB2867B85C4E2E4FFFFD6D7FFFFD4D4FFFFD7D8FFFFDBDCFFFFE1E3
      FFFFD9D9FFFFD8DAFFFFECEFFFFF8B818BD15B482B3F7E7279B0C4C2F0FFEBEE
      FFFF8E869EC88C8283C0FEFFFFFFFEFEFFFFFFFFFFFFFBFCFFFFE7E6FEFFB6B0
      C6F9AAA497FF8F8674FF978F9CE5625243360000000000000000AAA4AEFEFFFF
      FFFF97909CC5958C88C7FFFFFFFFE7E6F3FFA79F9FFA786B64924D3E31295B4B
      2F8FF5F2CFFFC4BE78FF615129B6000000000000000044341E319C949BF7FFFF
      FFFFA59DA0CC7163576EA59EA7EB897F8DFF796D71F76355529C47382C5ABDB7
      9FEAFFFFFFFFFFFFFFFFBCB593FF817754DC887E92DE8E8596FF918999FFA39C
      A5E375675F780000000000000000665542106B5D524A695D659DA1999AFFFFFF
      F7FFFFFFF4FFFFFFF9FFFFFFF6FFD9D59AFF978E61FF62534862000000000000
      000000000000000000000000000000000000000000005E503362F3F09CFFFFFF
      C4FFFFFFB7FFFFFFB8FFFFFDB0FFF1EE9FFFBBB454FF8E8339F057483A0E0000
      00000000000000000000000000000000000050413804B6AE4EFFF4F35FFFF9F6
      61FFFFFF72FFFFFF6EFFA79D3FFF867A36E96C5D3C6C6A5B424E645443050000
      000000000000000000000000000000000000000000006B5B41565949377BCCC6
      25FFFFFF24FFFFFF22FFA49A2BFF6F6238C70000000000000000000000000000
      000000000000000000000000000000000000000000000000000047384106C4BF
      10FFFFFF00FFFEFD00FFA09422FF736638CD0000000000000000000000000000
      00000000000000000000000000000000000000000000000000005041410FB9B5
      0EFFE9EA00FFE3E200FF988F22FF726538D40000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000007D6F
      30BD82782AD4756B30B370633AA8695A445C0000000000000000000000000000
      000000000000FE3F0000F80F0000C00300000000366D0000E3FF0000FFFF0018
      FFFF00307BB5000061ACC007FFFFF003FFFFE003ECFFF01FA6FFF81F72E4F81F
      4684FC1F2D37}
    PopupMenu = PopupMenu1
    OnClick = TrayIcon1Click
    Left = 44
    Top = 108
  end
  object TimerIniAgenda: TTimer
    Enabled = False
    Interval = 3000
    OnTimer = TimerIniAgendaTimer
    Left = 16
    Top = 108
  end
  object DBAgenda: TMySQLDatabase
    DesignOptions = []
    ConnectOptions = []
    KeepConnection = False
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 120
    Top = 400
  end
  object QrDmk: TMySQLQuery
    Database = DBAgenda
    Left = 200
    Top = 264
  end
  object QrEmExec: TMySQLQuery
    Database = DBAgenda
    OnCalcFields = QrEmExecCalcFields
    SQL.Strings = (
      'SELECT IF(Hora < CURTIME(), 1, 0) Idx, '
      'Hora, UltTry, UltExe, UltErr, Host, Db, '
      'Destino, Codigo, User, Pass, Port,'
      'UTC_TIMESTAMP() AGORAUTC,'
      'NOW() AGORA, Dias, Tipo'
      'FROM bkagenda'
      'WHERE Hora <= CURTIME()'
      'AND UltExe < CURDATE()'
      'AND NOT (Hora IS NULL)'
      'AND Host <> ""'
      'AND DB <> ""'
      'AND Destino <> ""'
      'ORDER BY Idx, Hora')
    Left = 16
    Top = 180
    object QrEmExecIdx: TLargeintField
      FieldName = 'Idx'
      Required = True
    end
    object QrEmExecHora: TTimeField
      FieldName = 'Hora'
    end
    object QrEmExecUltTry: TDateTimeField
      FieldName = 'UltTry'
    end
    object QrEmExecUltExe: TDateTimeField
      FieldName = 'UltExe'
    end
    object QrEmExecUltErr: TDateTimeField
      FieldName = 'UltErr'
    end
    object QrEmExecHost: TWideStringField
      FieldName = 'Host'
      Size = 255
    end
    object QrEmExecDb: TWideStringField
      FieldName = 'Db'
      Size = 255
    end
    object QrEmExecDestino: TWideStringField
      FieldName = 'Destino'
      Size = 255
    end
    object QrEmExecCodigo: TAutoIncField
      FieldName = 'Codigo'
    end
    object QrEmExecUser: TWideStringField
      FieldName = 'User'
      Size = 50
    end
    object QrEmExecPass: TWideStringField
      FieldName = 'Pass'
      Size = 50
    end
    object QrEmExecPort: TIntegerField
      FieldName = 'Port'
    end
    object QrEmExecPASS_DECR: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PASS_DECR'
      Size = 50
      Calculated = True
    end
    object QrEmExecAGORAUTC: TDateTimeField
      FieldName = 'AGORAUTC'
    end
    object QrEmExecAGORA: TDateTimeField
      FieldName = 'AGORA'
    end
    object QrEmExecDias: TIntegerField
      FieldName = 'Dias'
    end
    object QrEmExecAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrEmExecZipar: TSmallintField
      FieldName = 'Zipar'
    end
    object QrEmExecTipo: TSmallintField
      FieldName = 'Tipo'
    end
  end
  object DsBkAgenda: TDataSource
    DataSet = TbBkAgenda
    Left = 100
    Top = 180
  end
  object TbBkAgenda: TMySQLTable
    Database = DBAgenda
    AfterOpen = TbBkAgendaAfterOpen
    BeforeEdit = TbBkAgendaBeforeEdit
    BeforePost = TbBkAgendaBeforePost
    BeforeDelete = TbBkAgendaBeforeDelete
    OnCalcFields = TbBkAgendaCalcFields
    FieldDefs = <
      item
        Name = 'Codigo'
        DataType = ftAutoInc
      end
      item
        Name = 'Nome'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'Host'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'Db'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'User'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'Pass'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'Port'
        DataType = ftInteger
      end
      item
        Name = 'Hora'
        DataType = ftTime
      end
      item
        Name = 'UltTry'
        DataType = ftDateTime
      end
      item
        Name = 'UltExe'
        DataType = ftDateTime
      end
      item
        Name = 'UltErr'
        DataType = ftDateTime
      end
      item
        Name = 'Destino'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'Aviso'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'Ativo'
        DataType = ftSmallint
      end>
    IndexDefs = <
      item
        Name = 'TbBkAgendaIndex1'
      end>
    StoreDefs = True
    TableName = 'bkagenda'
    Left = 72
    Top = 180
    object TbBkAgendaCodigo: TAutoIncField
      FieldName = 'Codigo'
      Origin = 'bkagenda.Codigo'
    end
    object TbBkAgendaNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'bkagenda.Nome'
      Size = 100
    end
    object TbBkAgendaHost: TWideStringField
      FieldName = 'Host'
      Origin = 'bkagenda.Host'
      Size = 255
    end
    object TbBkAgendaDb: TWideStringField
      FieldName = 'Db'
      Origin = 'bkagenda.Db'
      Size = 255
    end
    object TbBkAgendaHora: TTimeField
      FieldName = 'Hora'
      Origin = 'bkagenda.Hora'
    end
    object TbBkAgendaDestino: TWideStringField
      FieldName = 'Destino'
      Origin = 'bkagenda.Destino'
      Size = 255
    end
    object TbBkAgendaAviso: TWideStringField
      FieldName = 'Aviso'
      Origin = 'bkagenda.Aviso'
      Size = 255
    end
    object TbBkAgendaAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'bkagenda.Ativo'
      MaxValue = 1
    end
    object TbBkAgendaFALTA: TTimeField
      FieldKind = fkCalculated
      FieldName = 'FALTA'
      Calculated = True
    end
    object TbBkAgendaUltTry: TDateTimeField
      FieldName = 'UltTry'
      Origin = 'bkagenda.UltTry'
    end
    object TbBkAgendaUltExe: TDateTimeField
      FieldName = 'UltExe'
      Origin = 'bkagenda.UltExe'
    end
    object TbBkAgendaUltErr: TDateTimeField
      FieldName = 'UltErr'
      Origin = 'bkagenda.UltErr'
    end
    object TbBkAgendaUser: TWideStringField
      FieldName = 'User'
      Size = 50
    end
    object TbBkAgendaPass: TWideStringField
      FieldName = 'Pass'
      Size = 50
    end
    object TbBkAgendaPort: TIntegerField
      FieldName = 'Port'
    end
    object TbBkAgendaPASS_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PASS_TXT'
      Size = 50
      Calculated = True
    end
    object TbBkAgendaPASS_DECR: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PASS_DECR'
      Size = 50
      Calculated = True
    end
    object TbBkAgendaDias: TIntegerField
      FieldName = 'Dias'
    end
    object TbBkAgendaZipar: TSmallintField
      FieldName = 'Zipar'
    end
    object TbBkAgendaTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object TbBkAgendaTipo_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Tipo_TXT'
      Calculated = True
    end
  end
  object DsEmExec: TDataSource
    DataSet = QrEmExec
    Left = 44
    Top = 180
  end
  object TimerBkExec: TTimer
    Enabled = False
    Interval = 60000
    OnTimer = TimerBkExecTimer
    Left = 128
    Top = 180
  end
  object QrProxBk: TMySQLQuery
    AutoRefresh = True
    Database = DBAgenda
    OnCalcFields = QrProxBkCalcFields
    SQL.Strings = (
      'SELECT IF(Hora < CURTIME(), 1, 0) Idx, '
      'Hora, UltTry, UltExe, UltErr, Host, Db, '
      'Destino, Codigo  '
      'FROM bkagenda'
      'WHERE NOT (Hora IS NULL)'
      'AND Host <> ""'
      'AND DB <> ""'
      'AND Destino <> ""'
      'ORDER BY Idx, Hora'
      'LIMIT 1')
    Left = 16
    Top = 152
    object QrProxBkIdx: TLargeintField
      FieldName = 'Idx'
      Required = True
    end
    object QrProxBkHora: TTimeField
      FieldName = 'Hora'
    end
    object QrProxBkUltTry: TDateTimeField
      FieldName = 'UltTry'
    end
    object QrProxBkUltExe: TDateTimeField
      FieldName = 'UltExe'
    end
    object QrProxBkUltErr: TDateTimeField
      FieldName = 'UltErr'
    end
    object QrProxBkHost: TWideStringField
      FieldName = 'Host'
      Size = 255
    end
    object QrProxBkDb: TWideStringField
      FieldName = 'Db'
      Size = 255
    end
    object QrProxBkDestino: TWideStringField
      FieldName = 'Destino'
      Size = 255
    end
    object QrProxBkCodigo: TAutoIncField
      FieldName = 'Codigo'
    end
    object QrProxBkFALTA: TTimeField
      FieldKind = fkCalculated
      FieldName = 'FALTA'
      Calculated = True
    end
  end
  object DsProxBk: TDataSource
    DataSet = QrProxBk
    Left = 44
    Top = 152
  end
  object BalloonHint1: TBalloonHint
    Left = 623
    Top = 313
  end
  object TmVersao: TTimer
    Enabled = False
    Interval = 500
    OnTimer = TmVersaoTimer
    Left = 496
    Top = 304
  end
end
