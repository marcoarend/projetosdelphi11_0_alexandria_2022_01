unit Horarios;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, dmkGeral, dmkEdit;

type
  TFmHorarios = class(TForm)
    PainelDados: TPanel;
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    EdHorario: TdmkEdit;
    lbDb: TLabel;
    EdIP: TdmkEdit;
    Label1: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdHorarioExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmHorarios: TFmHorarios;

implementation

uses UnMyObjects;

{$R *.DFM}

procedure TFmHorarios.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmHorarios.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmHorarios.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmHorarios.EdHorarioExit(Sender: TObject);
begin
  EdHorario.Text := MLAGeral.HTH(EdHorario.Text, 'hh:nn');
end;

end.
