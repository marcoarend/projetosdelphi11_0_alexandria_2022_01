unit MyListas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs, Forms,
  ExtCtrls, ComCtrls, StdCtrls, DB, (*DBTables,*) mysqlDBTables, UnMyLinguas,
  UnInternalConsts, UnMLAGeral, dmkGeral, UnDmkEnums;

type
  TMyListas = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    //function  CriaListaImpDOS(FImpDOS: TStringList): Boolean;
    function  CriaListaUserSets(FUserSets: TStringList): Boolean;
    function  CriaListaJanelas(FLJanelas: TList): Boolean;
    //
    function  CriaListaTabelas(Database: TmySQLDatabase; Lista:
              TList): Boolean;
    //function  CriaListaTabelas(Database: TmySQLDatabase; FTabelas: TStringList): Boolean;
    function  CriaListaTabelasLocais(Lista: TList): Boolean;
    function  CriaListaIndices(Tabela: String; FLIndices: TList): Boolean;
    function  CriaListaCampos(Tabela: String; FLCampos: TList; var TemControle: TTemControle): Boolean;
    function  CriaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
    function  CompletaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
    function  ExcluiTab: Boolean;
    function  ExcluiReg: Boolean;
    function  ExcluiIdx: Boolean;
    procedure VerificaOutrosAntes(DataBase: TmySQLDatabase; Memo: TMemo);
    procedure VerificaOutrosDepois(DataBase: TmySQLDatabase; Memo: TMemo);
    procedure ModificaDadosDeIndicesAlterados(Indice, Tabela: String;
              DataBase: TmySQLDatabase; Memo: TMemo);
  end;

const
  //CO_VERSAO = 1808041130;
  CO_VERMCW = 1808041130;
  CO_VERMLA = 1909131942;
  CO_DMKID_APP = 13;
  CO_SIGLA_APP = 'DEBK';
  CO_GRADE_APP = False;
  CO_VLOCAL = False;
  //
  CO_CONTRATO_ID = 18;
  CO_CONTRATO_HASH_SH1 = '4A9B5D151F7082104D8262897D0F21AB0EF6058A';
var
  MyList: TMyListas;
  FRCampos  : TCampos;
  FRIndices : TIndices;


implementation

//uses CashTabs;

function TMyListas.CriaListaTabelas(Database: TmySQLDatabase; Lista:
 TList): Boolean;
begin
  Result := True;
  try
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaTabelasLocais(Lista: TList): Boolean;
begin
  Result := True;
  try
    //CashTb.CarregaListaTabelasLocaisCashier(FTabelasLocais);
    //FtabelasLocais.Add('');
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
begin
  Result := True;
  try
    if Uppercase(Tabela) = Uppercase('XXXXXXX') then
    begin
      FListaSQL.Add('Codigo|Nome');
      FListaSQL.Add('0|""');
    end else (*CashTb.CarregaListaSQLCashier(Tabela, FListaSQL)*);
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CompletaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
begin
  Result := True;
  try
    if Uppercase(Tabela) = Uppercase('XXXXXXXX') then
    begin
    end;
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaIndices(Tabela: String; FLIndices: TList): Boolean;
begin
  Result := True;
  try
    if Uppercase(Tabela) = Uppercase('XXXXXX') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else (*CashTb.CarregaListaFRIndicesCashier(Tabela, FRIndices, FLindices)*);
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaCampos(Tabela: String; FLCampos: TList;
  var TemControle: TTemControle): Boolean;
begin
  try
    if Uppercase(Tabela) = Uppercase('XXXXXX') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
    end else (*CashTb.CarregaListaFRCamposCashier(Tabela, FRCampos, FLCampos)*);
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
    //CashTb.CompletaListaFRCamposCashier(Tabela, FRCampos, FLCampos);
    Result := True;
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.ExcluiTab: Boolean;
begin
  Result := True;
end;

function TMyListas.ExcluiReg: Boolean;
begin
  Result := True;
end;

function TMyListas.ExcluiIdx: Boolean;
begin
  Result := True;
end;

function TMyListas.CriaListaUserSets(FUserSets: TStringList): Boolean;
begin
  Result := True;
  try
    //
  except
    raise;
    Result := False;
  end;
end;

{
function TMyListas.CriaListaImpDOS(FImpDOS: TStringList): Boolean;
begin
  Result := True;
  try
  except
    raise;
    Result := False;
  end;
end;
}

procedure TMyListas.VerificaOutrosAntes(DataBase: TmySQLDatabase; Memo: TMemo);
begin
  //
end;

procedure TMyListas.VerificaOutrosDepois(DataBase: TmySQLDatabase; Memo: TMemo);
begin
  // Nada
end;

procedure TMyListas.ModificaDadosDeIndicesAlterados(Indice, Tabela: String;
  DataBase: TmySQLDatabase; Memo: TMemo);
begin
  // Nada
end;

// XXX-XXXXX-000
function TMyListas.CriaListaJanelas(FLJanelas: TList): Boolean;
begin
{
  IBGE_DTB_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  NFe_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  CMPT_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Grade_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
}
  //
  //
  Result := True;
end;

end.
