unit Restaura;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  UnMLAGeral, ExtCtrls, StdCtrls, Buttons, CheckLst, UnInternalConsts, ComCtrls,
  Db, mySQLDbTables, MySQLDump, MySQLBatch, mySQLDirectQuery, dmkGeral, dmkEdit,
  UnMyObjects, dmkImage, UnDmkEnums;
  
type
  TFmRestaura = class(TForm)
    Panel2: TPanel;
    Panel6: TPanel;
    Panel7: TPanel;
    LaStatus: TStaticText;
    Panel8: TPanel;
    EdBackFile: TdmkEdit;
    BtAbrir: TBitBtn;
    Label1: TLabel;
    OpenDialog1: TOpenDialog;
    Memo1: TMemo;
    Memo2: TMemo;
    Splitter1: TSplitter;
    BeRestore: TMySQLBatchExecute;
    QrDelDB: TmySQLQuery;
    DBMySQL: TmySQLDatabase;
    MeBase: TMemo;
    Memo3: TMemo;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel1: TPanel;
    PnSaiDesis: TPanel;
    BtDireto: TBitBtn;
    CkMostra: TCheckBox;
    BtParar: TBitBtn;
    BtNormal: TBitBtn;
    Button1: TButton;
    BtSaida: TBitBtn;
    Progress1: TProgressBar;
    procedure FormActivate(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtNormalClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtAbrirClick(Sender: TObject);
    procedure BtPararClick(Sender: TObject);
    procedure MySQLDumpFinish(Sender: TObject);
    procedure MySQLDumpDataProcess(Sender: TObject; Percent: Integer);
    procedure BeRestoreAfterExecute(Sender: TObject);
    procedure BeRestoreAfterStatement(Sender: TObject; SQLText: String;
      const StatementNo, RowsAffected: Integer; const Success: Boolean);
    procedure BeRestoreProcess(Sender: TObject; SQLText: String;
      const StatementNo: Integer);
    procedure Button1Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure BtDiretoClick(Sender: TObject);
    procedure EdBackFileChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FParar: Boolean;
    FExecute: String;
    FContaD, FContaE, FContaS, FContaP, FContaL: Integer;
    FInicio, FCorrido, FFaltando: TDateTime;
    FNumDecl: Integer;
    FAchouDB: Boolean;
    FStrUsaDB: String;
    function  LeArquivo(Arquivo: String): String;
    function  LeArquivoZIP: String;
    function  NumeroDeDeclaracoes: Integer;
    function  NumeroDeLetras: Integer;
    function  ExecutaDirect(Arquivo: String): String;
    //N�o funcionou function  ExecutaDirect2(Arquivo: String): String;
    procedure ProcessoEmDOS(LinhaComando : PChar);
    procedure LeArquivoZIP2(Arquivo: String);

  public
    { Public declarations }
    FZipPath: String;
  end;

var
  FmRestaura: TFmRestaura;

implementation

uses DmkDAC_PF, ZForge(*20230320, UMySQLDB*), SelRadioGroup;

{$R *.DFM}

var
  WMLA_TbCount, WMLA_DuCount, WMLA_ZPCount: Integer;

procedure TFmRestaura.FormActivate(Sender: TObject);
begin
  MyObjects.CorInicomponente;
end;

procedure TFmRestaura.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmRestaura.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmRestaura.BtNormalClick(Sender: TObject);
var
  Extensao: string;
  Continua: Boolean;
begin
  //
  Continua := False;
  Memo1.Lines.Clear;
  Memo2.Lines.Clear;
  Memo3.Lines.Clear;
  BeRestore.SQL.Clear;
  Extensao := ExtractFileExt(OpenDialog1.FileName);
  if AnsiCompareText(Extensao, '.sql') = 0 then
  begin
    LeArquivo(OpenDialog1.FileName);
    Continua := True;
  end else
  if AnsiCompareText(Extensao, '.zip') = 0 then
  begin
    LeArquivoZIP;
    Continua := True;
  end else
    Geral.MB_Aviso('Extens�o inv�lida: "' + Extensao + '"');
  if (Continua = False) or (FNumDecl = 0) then
    Exit;
  //

  FContaL := 0;
  WMLA_ZPCount := 0;
  WMLA_TbCount := 0;
  WMLA_DuCount := 0;
  Progress1.Position := 0;
  //
  if Geral.MB_Pergunta('A restaura��o do backup "' + EdBackFile.Text +
    '" ser� executada na base de dados:' + sLineBreak + sLineBreak + TMeuDB +
    sLineBreak + sLineBreak + 'Confirma a restaura��o?') = ID_YES then
  begin
    //QrDelDB.SQL.Clear;
    //QrDelDB.SQL.Add('DROP DATABASE IF EXISTS '+TMeuDB);
    //QrDelDB.ExecSQL;
    //Memo1.Lines.Add('Dase de dados exclu�da: '+TMeuDB);
    FInicio := Now;
    Memo1.Lines.Add('Restaura��o iniciada [ '+
      FormatDateTime(VAR_FORMATTIME, FInicio-Int(FInicio))+' ]');
    LaStatus.Caption := 'Restaura��o iniciada...';
    //
  end else Exit;
  try
    Memo2.Lines.Clear;
    Memo2.Refresh;
    Update;
    Screen.Cursor:=crHourGlass;
    BeRestore.ExecSql;
    LaStatus.Caption := 'Pronto.';
    LaStatus.Refresh;
    LaStatus.Update;
    Screen.Cursor:=crDefault;
    Memo1.Lines.Add('Restaura��o terminada [ '+
      FormatDateTime(VAR_FORMATTIME, Now-Int(Now))+' ]');
    //
    Memo2.Lines.Clear;
    Memo2.Refresh;
  finally
    Screen.Cursor:=crDefault;
  end;
end;

procedure TFmRestaura.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmRestaura.BtAbrirClick(Sender: TObject);
begin
  ForceDirectories('C:\Temp');
  OpenDialog1.InitialDir := 'C:\Temp';
  if OpenDialog1.Execute then
  begin
    EdBackFile.Text := OpenDialog1.FileName;
    //
    {
    Memo1.Lines.Clear;
    Memo2.Lines.Clear;
    Memo3.Lines.Clear;
    BeRestore.SQL.Clear;
    Extensao := ExtractFileExt(OpenDialog1.FileName);
    if AnsiCompareText(Extensao, '.sql') = 0 then LeArquivo(OpenDialog1.FileName)
    else if AnsiCompareText(Extensao, '.zip') = 0 then LeArquivoZIP
    else Geral.MB_Aviso('Extens�o inv�lida: "'+Extensao+'"');
    }
  end;
end;

procedure TFmRestaura.BtPararClick(Sender: TObject);
begin
  FParar := True;
end;

procedure TFmRestaura.MySQLDumpFinish(Sender: TObject);
begin
  WMLA_DuCount := WMLA_DuCount +1;
  LaStatus.Caption := 'Terminado!';
  LaStatus.Refresh;
  LaStatus.Update;
  Progress1.Position := 0;
  //Progress2.Position := 0;
  LaStatus.Caption := 'Dump terminado!';
end;

procedure TFmRestaura.MySQLDumpDataProcess(Sender: TObject;
  Percent: Integer);
begin
  // Gera erro
  //Progress2.Position := Percent;
end;

function TFmRestaura.LeArquivo(Arquivo: String): String;
  procedure AddLinhaSQL(S: String);
  var
    X: String;
  begin
    if Length(S) > 1 then X := S[1]+S[2] else x := '';
    if x <> '--' then
    if x <> '/*' then
    //BeRestore.SQL.Add(MLAGeral.ANSItoASCII(S));
    BeRestore.SQL.Add(S);
    (*try
      OemToChar(PChar(S), Z);
      Memo3.Lines.Add(Z);
    except;
    end;*)
  end;
var
  F: TextFile;
  S: String;
  Declara: Integer;
begin
  Screen.Cursor := crHourGlass;
  FNumDecl := 0;
  BeRestore.SQL.Clear;
  if FileExists(Arquivo) then
  begin
    //Lista := TStringList.Create;
    //try
      AssignFile(F, Arquivo);
      Reset(F);
      Declara := 0;
      while not Eof(F) do
      begin
        Declara := Declara + 1;
        LaStatus.Caption := 'Carregando linha n� ' + Geral.FF0(Declara);
        LaStatus.Refresh;
        LaStatus.Update;
        Readln(F, S);
        //St := St + S;
        if Declara <> 1 then AddLinhaSQL(S);
        //Lista.Add(S);
        Application.ProcessMessages;
      end;
      CloseFile(F);
      //BeRestore.SQL.Text := Lista.Text;
    //finally
      //Lista.Free;
    //end;
  end;
  FNumDecl := NumeroDeDeclaracoes;
  if FNumDecl > 0 then
  begin
    LaStatus.Caption := 'SQL Carregado';
    Memo1.Lines.Add('Arquivo SQL com ' + Geral.FF0(FNumDecl) + ' declara��es.');
  end else begin
    LaStatus.Caption := 'N�o foi carregado nenhuma SQL!';
    {
    if (Length(S) > 10)  then
    begin
      LaStatus.Caption := LaStatus.Caption + ' Tentando abrir de outro modo!';
      Update;
      Application.ProcessMessages;
      // procura pelo Char(10)!
      if pos(Char(#$A), St) > 0 then
      begin
        //Geral.MB_Aviso(Geral.FF0(Ord(Char(#$A))));
        //Geral.MB_Aviso(Char(#$A));
        s2 := '';
        AssignFile(F1, OpenDialog1.Filename);
        Reset(F1);
        while not Eof(F1) do
        begin
          Read(F1, Ch);
          //
          if Ord(Ch) = 10 then
          begin
            AddLinhaSQL(s2);
            s2 := '';
          end else begin
            s2 := s2 + Ch;
          end;
        end;
        CloseFile(F1);
      end;
    end;}
  end;
  FContaD := NumeroDeLetras;
  FNumDecl := NumeroDeDeclaracoes;
  Progress1.Position := 0;
  Progress1.Max := FContaD;
  Memo1.Lines.Add('Arquivo SQL com ' + Geral.FF0(FNumDecl) + ' declara��es.');
  if FNumDecl > 0 then
    LaStatus.Caption := 'SQL Carregado'
  else
    LaStatus.Caption := 'N�o foi carregado nenhuma SQL!';
  Screen.Cursor := crDefault;
end;

{var

  F1, F2: TextFile;
  Ch: Char;
begin
  if OpenDialog1.Execute then begin
  end;

end;}

procedure TFmRestaura.LeArquivoZIP2(Arquivo: String);
var
  Arq: String;
begin
  Arq := '';
  //
  Application.CreateForm(TFmZForge, FmZForge);
  Arq := FmZForge.ExtrairArquivo(Arquivo, ExtractFileDir(Arquivo));
  FmZForge.Destroy;
  //
  if Arq <> '' then
  begin
    ExecutaDirect(Arq);
  end else
    Geral.MB_Aviso('Falha ao abrir arquivo ZIP!');
end;

function TFmRestaura.LeArquivoZIP: String;
var
  Name, NameSQL: String;
  Achou: Boolean;
  //
  Caminho, Script: String;
  i: Integer;
  SR : TSearchRec;
begin
  Achou := False;
  if not DirectoryExists(CO_DIR_RAIZ_DMK) then
    CreateDir(CO_DIR_RAIZ_DMK);
  if not DirectoryExists(CO_DIR_RAIZ_DMK + '\Backups') then
    CreateDir(CO_DIR_RAIZ_DMK + '\Backups');
  if not DirectoryExists(CO_DIR_RAIZ_DMK + '\Backups\SQL') then
    CreateDir(CO_DIR_RAIZ_DMK + '\Backups\SQL');
  if not DirectoryExists(CO_DIR_RAIZ_DMK + '\Backups\SQL\Temp') then
    CreateDir(CO_DIR_RAIZ_DMK + '\Backups\SQL\Temp');
  //
  Screen.Cursor := crHourGlass;
  BeRestore.SQL.Clear;
  Name := EdBackFile.Text;
  if FileExists(Name) then
  begin
    try
      Caminho := CO_DIR_RAIZ_DMK + '\Backups\SQL\Temp';
      NameSQL := Caminho + '\' +ChangeFileExt(ExtractFileName(Name), '.sql');
      i := FindFirst(Caminho+'\*.*', faAnyFile, SR);
      while i = 0 do
      begin
        if (SR.Attr and faDirectory) <> faDirectory then
          if not DeleteFile(Caminho+'\'+SR.Name) then
            Geral.MB_Aviso('N�o foi poss�vel excluir o arquivo '+SR.Name);
        i := FindNext(SR);
      end;
      if not FileExists(FZipPath+'\wzunzip.exe') then
      begin
        Geral.MB_Erro('"WZUNZIP.EXE" n�o localizado!');
        Exit;
      end;
      Script := PChar(FZipPath+'\wzunzip '+Name+' '+Caminho);
      ProcessoEmDos(PChar(Script));
      for i := 1 to 500 do
      begin
        if FileExists(NameSQL) then Achou := True;
        Break;
      end;
      if Achou then LeArquivo(NameSQL) else
      begin
        OpenDialog1.DefaultExt := '.sql';
        OpenDialog1.FilterIndex := 2;
        //OpenDialog1.InitialDir := Caminho;
        BtAbrirClick(Self);
      end;
    except
      Geral.MB_Aviso('Ocorreu um erro na descompacta��o.');
    end;
  end;
end;

function TFmRestaura.NumeroDeDeclaracoes: Integer;
var
  i, Conta: Integer;
begin
  Conta := 0;
  for i := 0 to BeRestore.SQL.Count - 1 do
  begin
    if Pos (';', BeRestore.SQL[i]) > 1 then Conta := Conta +1;
  end;
  Result := Conta;
end;

function TFmRestaura.NumeroDeLetras: Integer;
var
  i, Conta: Integer;
begin
  Conta := 0;
  for i := 0 to BeRestore.SQL.Count - 1 do
  begin
    Conta := Conta + Length(BeRestore.SQL[i]);
  end;
  Result := Conta;
end;

procedure TFmRestaura.BeRestoreAfterExecute(Sender: TObject);
begin
  FContaE := FContaE + 1;
  Update;
  //
  Memo1.Lines.Add('Declara��es executadas = ' + Geral.FF0(FContaP));
end;

procedure TFmRestaura.BeRestoreAfterStatement(Sender: TObject;
  SQLText: String; const StatementNo, RowsAffected: Integer;
  const Success: Boolean);
begin
  FContaL := FContaL + Length(SQLText);
  FContaS := FContaS + 1;
  if CkMostra.Checked then Memo2.Text := SQLText;
  Application.ProcessMessages;
  FCorrido  := Now() - FInicio;
  FFaltando := FCorrido * FContaD / FContaL;
  Progress1.Position := FContaL;
  LaStatus.Caption := 'Restaurado '+
    FormatFloat('0.00', Progress1.Position/Progress1.Max*100)+' % ('+
  Geral.FF0(StatementNo) + ' registros) Tempo corrido: '+
  FormatDateTime(VAR_FORMATTIME, FCorrido)+'  Tempo restante: '+
  FormatDateTime(VAR_FORMATTIME, FFaltando-FCorrido)+' (aproximado)  Tempo total: '+
  FormatDateTime(VAR_FORMATTIME, FFaltando)+' (aproximado)';
  LaStatus.Update;
end;

procedure TFmRestaura.BeRestoreProcess(Sender: TObject; SQLText: String;
  const StatementNo: Integer);
begin
  FContaP := FContaP + 1;
end;

procedure TFmRestaura.ProcessoEmDOS(LinhaComando : PChar);
var
  Startupinfo : TStartupinfo;
  ProcessInformation : TProcessInformation;
  rc : Boolean;
begin
  Fillchar(Startupinfo, SizeOf(Startupinfo), #0);
  with Startupinfo do
    cb := SizeOf(Startupinfo);
  rc := CreateProcess(NIL, LinhaComando, NIL, NIL, FALSE, NORMAL_PRIORITY_CLASS,
                      NIL, NIL, Startupinfo, ProcessInformation);
  if rc then
    WaitForSingleObject(ProcessInformation.hProcess, INFINITE);
end;

procedure TFmRestaura.Button1Click(Sender: TObject);
var
  Texto: String;
begin
//Voc� pode ler de volta no MySQL com:
//mysql banco_dados < arquivo-backup.sql
//dump --max_allowed_packet=2097152
  MeBase.Lines.Clear;
  Text := 'C:\ARQUIV~1\MySQL\MYSQLS~1.1\bin\mysql '+EdBackFile.Text;
  MeBase.Lines.Add(Texto);
  
end;

procedure TFmRestaura.SpeedButton1Click(Sender: TObject);
begin
end;

(*procedure TFmRestaura.BeRestoreBatchErrorEx(Sender: TObject;
  E: EmySQLDatabaseError; SQLText: String; StatementNo: Integer;
  var aAction: TmySQLBatchAction);
begin
  Memo1.Lines.Add('----- IN�CIO DO RELAT�RIO -----');
  Memo1.Lines.Add('Erro ao restaurar backup:');
  Memo1.Lines.Add(E.Message);
  Memo1.Lines.Add('SQL que gerou o erro:');
  Memo1.Lines.Add(SQLText);
  Memo1.Lines.Add('Declara��o n�mero ' + Geral.FF0(StatementNo));
  Memo1.Lines.Add('----- FIM DO RELAT�RIO -----');
end;*)

procedure TFmRestaura.EdBackFileChange(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := FileExists(EdBackFile.Text);
  BtNormal.Enabled := Habilita;
  BtDireto.Enabled := Habilita;
end;

procedure TFmRestaura.BtDiretoClick(Sender: TObject);
var
  Extensao: String;
begin
  FInicio := Now();
  //
  Memo1.Lines.Clear;
  Memo2.Lines.Clear;
  Memo3.Lines.Clear;
  BeRestore.SQL.Clear;
  //
  Extensao := ExtractFileExt(OpenDialog1.FileName);
  //
  if AnsiCompareText(Extensao, '.sql') = 0 then
    ExecutaDirect(OpenDialog1.FileName)
  else if AnsiCompareText(Extensao, '.zip') = 0 then
    LeArquivoZIP2(OpenDialog1.FileName)
  else
    Geral.MB_Aviso('Extens�o inv�lida: "'+Extensao+'"');
end;

(* N�o funcionou
function TFmRestaura.ExecutaDirect2(Arquivo: String): String;
var
  Msg: String;
begin
  Screen.Cursor := crHourGlass;
  try
    USQLDB.Backup_Restaura(DBMySQL, Arquivo, True, Msg, LaStatus, Progress1);
    //
    if Msg <> '' then
      Geral.MB_Aviso(Msg);
  finally
    Screen.Cursor := crDefault;
  end;
end;
*)

function TFmRestaura.ExecutaDirect(Arquivo: String): String;
  procedure ExecutaSQL(S: String);
  var
    X: String;
  begin
    if Length(S) > 1 then X := S[1]+S[2] else x := '';
    if x <> '--' then
    if x <> '/*' then
    begin
      if s = FStrUsaDB then
        //Geral.MB_Info(s)
      else
      begin
        if S <> '' then
        begin
          FExecute := FExecute + sLineBreak + S;
          if FExecute[Length(FExecute)] = ';' then
          begin
            if FExecute <> '' then
            begin
              DBMySQL.Execute(FExecute);
              FExecute := '';
            end;
          end;
        end;
      end;
    end else begin
      if FExecute <> '' then
      begin
        DBMySQL.Execute(FExecute);
        FExecute := '';
      end;
    end;
  end;
const
  TxtAchouDB = 'USE `';
var
  F: TextFile;
  S, DBName: String;
  Next, Step, I, TotDecl, Declara, Tam, Resul: Integer;
  DBs: array[0..1] of String;
begin
  Screen.Cursor := crHourGlass;
  try
    FExecute := '';
    FNumDecl := 0;
    FAchouDB := False;
    Step := 1000;
    Next := Step;
    FStrUsaDB := '';
    if FileExists(Arquivo) then
    begin
      AssignFile(F, Arquivo);
      Reset(F);
      TotDecl := 0;
      LaStatus.Caption := 'Calculando quantidade de TotDecl��es...';
      LaStatus.Refresh;
      LaStatus.Update;
      while not Eof(F) do
      begin
        TotDecl := TotDecl + 1;
        //
        if TotDecl = Next then
        begin
          LaStatus.Caption := 'Calculando quantidade de TotDecl��es...' + Geral.FF0(TotDecl);
          Next := Next + Step;
        end;
        //
        Readln(F, S);
        //Geral.MB_Aviso(S);
        //
        if pos (TxtAchouDB, s) > 0 then
        begin
           FStrUsaDB := s;
           DBName    := Copy(S, Length(TxtAchouDB));
           DBName    := Geral.Substitui(DBName, Char(96), '');
           DBName    := Geral.Substitui(DBName, ';', '');
           (*
           for I := 1 to Length(DBName) do
           begin
             Geral.MB_Info(Geral.FF0(Ord(DBName[I])));
           end;
           *)
           //Geral.MB_Aviso(DBName);
           FAchouDB := True;
           if DBMySQL.DatabaseName <> DBName then
           begin
             DBs[0] := DBMySQL.DatabaseName;
             DBs[1] := DBName;
             //
             Resul := MyObjects.SelRadioGroup('Sele��o de Base de dados',
                        'Selecione a Base de Dados', DBs, 0, 0);
             //
             if Resul >= 0 then
             begin
               UnDmkDAC_PF.ConectaMyDB_DAC(DBMySQL, DBs[Resul], DBMySQL.Host,
                 DBMySQL.Port,  VAR_SQLUSER, DBMySQL.UserPassword,
                 (*Desconecta*)True, (*Configura*)True, (*Conecta*)True);
             end else
               Exit;
           end;
        end;
      end;
      //
      Reset(F);
      Declara   := 0;
      FFaltando := 0;
      Progress1.Position := 0;
      Progress1.Max := TotDecl;
      while not Eof(F) do
      begin
        Declara := Declara + 1;
        if (Declara / 100) = (Declara div 100) then
        begin
          FCorrido  := Now() - FInicio;
          if TotDecl > 0 then
            FFaltando := FCorrido * TotDecl / Declara;
          //Progress1.Position := Progress1.Position + 1;
          Progress1.Position := Declara;
          Progress1.Refresh;
          LaStatus.Caption := 'Executando declara��o n� ' + Geral.FF0(Declara) +
            ' de ' + Geral.FF0(TotDecl) + '  -  ' +
            FormatFloat('0.00', Declara / TotDecl * 100) + '%  -  Tempo corrido: '+
            FormatDateTime(VAR_FORMATTIME, FCorrido)+'  Tempo restante: '+
            FormatDateTime(VAR_FORMATTIME, FFaltando-FCorrido)+' (aprox.)  Tempo total: '+
            FormatDateTime(VAR_FORMATTIME, FFaltando)+' (aprox.)';
          LaStatus.Refresh;
          LaStatus.Update;
        end;
        Readln(F, S);
        if Declara <> 1 then ExecutaSQL(S);
        Application.ProcessMessages;
      end;
      LaStatus.Caption := 'Executadas ' + Geral.FF0(Declara) + ' declara��es de ' +
        Geral.FF0(TotDecl) + '  =  ' + FormatFloat('0.0000', Declara / TotDecl * 100) +
        '%  -  Tempo Total: ' + FormatDateTime(VAR_FORMATTIME, FCorrido);
      CloseFile(F);
    end;
    Geral.MB_Aviso('Backup restaurado!');
  finally
    Progress1.Position := 0;
    Screen.Cursor      := crDefault;
  end;
end;

end.
