unit WetBlueMLA_BK;

interface


uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, CheckLst, ComCtrls, ExtCtrls, Spin, Menus, ShellApi,
  Registry, UnInternalConsts, MySQLTools, Db, mySQLDbTables, Grids, DBGrids,
  MySQLDump, MySQLBatch, dmkGeral, UnDmkSystem, SHFolder, dmkEdit, UnDmkProcFunc,
  UnDmkEnums, UnGrl_Vars;

type
  THackDBGrid = class(TDBGrid);
  TTipoDiagnostico = (mytdCheck, mytdAnalize, mytdRepair, mytdOptimize);
  TFmWetBlueMLA_BK = class(TForm)
    Panel1: TPanel;
    BtSaida: TBitBtn;
    PagCon: TPageControl;
    TSLogin: TTabSheet;
    BtBackup: TBitBtn;
    BtEstrutura: TBitBtn;
    TSLogFile: TTabSheet;
    Memo1: TMemo;
    TSStruct: TTabSheet;
    Splitter1: TSplitter;
    StructMemo: TMemo;
    LBStrTabs: TListBox;
    Splitter2: TSplitter;
    OpenDialog1: TOpenDialog;
    BtRestore: TBitBtn;
    BtDesiste: TBitBtn;
    BtConfirma: TBitBtn;
    PmBackup: TPopupMenu;
    Selecionado1: TMenuItem;
    BlueDermGeral1: TMenuItem;
    PopupMenu1: TPopupMenu;
    Mostrar1: TMenuItem;
    Fechar1: TMenuItem;
    TabSheet1: TTabSheet;
    ListBox1: TListBox;
    MyTools: TMySQLTools;
    QrOpt: TmySQLQuery;
    DsOpt: TDataSource;
    TabSheet2: TTabSheet;
    DBGrid1: TDBGrid;
    Panel6: TPanel;
    BtExclamacao: TBitBtn;
    BtVerifica: TBitBtn;
    BtServicos: TBitBtn;
    QrTabelas: TmySQLQuery;
    DsTabelas: TDataSource;
    DsDatabases: TDataSource;
    QrDatabases: TmySQLQuery;
    TabSheet3: TTabSheet;
    Memo2: TMemo;
    TabSheet4: TTabSheet;
    Memo3: TMemo;
    Timer1: TTimer;
    PMAgenda: TPopupMenu;
    Incluihorrio1: TMenuItem;
    Excluihorrio1: TMenuItem;
    DbDump: TmySQLDatabase;
    QrTbDump: TmySQLQuery;
    QrDBDump: TmySQLQuery;
    StatusBar1: TStatusBar;
    BitBtn1: TBitBtn;
    PMTipoBK: TPopupMenu;
    Normal1: TMenuItem;
    Beta1: TMenuItem;
    BitBtn2: TBitBtn;
    TrayIcon1: TTrayIcon;
    N1: TMenuItem;
    Inicializao1: TMenuItem;
    Executarnainicializao1: TMenuItem;
    NOexecutarnainicializao1: TMenuItem;
    TimerIniAgenda: TTimer;
    DBAgenda: TmySQLDatabase;
    QrDmk: TmySQLQuery;
    QrEmExec: TmySQLQuery;
    DsBkAgenda: TDataSource;
    Panel8: TPanel;
    Panel9: TPanel;
    Panel3: TPanel;
    LBDatabase: TListBox;
    Panel2: TPanel;
    Panel5: TPanel;
    BtTudo: TBitBtn;
    BtNenhum: TBitBtn;
    CBTables: TCheckListBox;
    Panel4: TPanel;
    pnMain: TPanel;
    lbHost: TLabel;
    lbDb: TLabel;
    Label5: TLabel;
    Label3: TLabel;
    edHost: TdmkEdit;
    edDb: TdmkEdit;
    EdSeg: TdmkEdit;
    EdLogin: TdmkEdit;
    TbBkAgenda: TmySQLTable;
    TbBkAgendaCodigo: TAutoIncField;
    TbBkAgendaNome: TWideStringField;
    TbBkAgendaHost: TWideStringField;
    TbBkAgendaDb: TWideStringField;
    TbBkAgendaHora: TTimeField;
    TbBkAgendaDestino: TWideStringField;
    TbBkAgendaAviso: TWideStringField;
    TbBkAgendaAtivo: TSmallintField;
    TbBkAgendaFALTA: TTimeField;
    DsEmExec: TDataSource;
    TimerBkExec: TTimer;
    QrEmExecIdx: TLargeintField;
    QrEmExecHora: TTimeField;
    QrEmExecUltTry: TDateTimeField;
    QrEmExecUltExe: TDateTimeField;
    QrEmExecUltErr: TDateTimeField;
    QrEmExecHost: TWideStringField;
    QrEmExecDb: TWideStringField;
    QrEmExecDestino: TWideStringField;
    QrEmExecCodigo: TAutoIncField;
    TbBkAgendaUltTry: TDateTimeField;
    TbBkAgendaUltExe: TDateTimeField;
    TbBkAgendaUltErr: TDateTimeField;
    TabSheet5: TTabSheet;
    MeResult: TMemo;
    lbPswd: TLabel;
    Panel7: TPanel;
    DBGrid4: TDBGrid;
    Label7: TLabel;
    Panel10: TPanel;
    DBGrid3: TDBGrid;
    Label8: TLabel;
    QrProxBk: TmySQLQuery;
    DsProxBk: TDataSource;
    QrProxBkIdx: TLargeintField;
    QrProxBkHora: TTimeField;
    QrProxBkUltTry: TDateTimeField;
    QrProxBkUltExe: TDateTimeField;
    QrProxBkUltErr: TDateTimeField;
    QrProxBkHost: TWideStringField;
    QrProxBkDb: TWideStringField;
    QrProxBkDestino: TWideStringField;
    QrProxBkCodigo: TAutoIncField;
    QrProxBkFALTA: TTimeField;
    Panel11: TPanel;
    Label4: TLabel;
    EdHostAgenda: TdmkEdit;
    CkWinZip: TCheckBox;
    Progress1: TProgressBar;
    LaAviso: TLabel;
    BtAtualiza: TBitBtn;
    Label1: TLabel;
    edPorta: TdmkEdit;
    edPswd: TdmkEdit;
    TbBkAgendaUser: TWideStringField;
    TbBkAgendaPass: TWideStringField;
    TbBkAgendaPort: TIntegerField;
    TbBkAgendaPASS_TXT: TWideStringField;
    TbBkAgendaPASS_DECR: TWideStringField;
    QrEmExecUser: TWideStringField;
    QrEmExecPass: TWideStringField;
    QrEmExecPort: TIntegerField;
    QrEmExecPASS_DECR: TWideStringField;
    Label6: TLabel;
    EdZipPath: TdmkEdit;
    BtImporta: TBitBtn;
    PageControl1: TPageControl;
    TabSheet6: TTabSheet;
    LaAgenda: TLabel;
    DBGrid2: TDBGrid;
    TabSheet8: TTabSheet;
    BtSobre: TBitBtn;
    BtContrato: TBitBtn;
    BtAjuda: TBitBtn;
    BalloonHint1: TBalloonHint;
    TmVersao: TTimer;
    QrEmExecAGORAUTC: TDateTimeField;
    QrEmExecAGORA: TDateTimeField;
    TbBkAgendaDias: TIntegerField;
    QrEmExecDias: TIntegerField;
    Label2: TLabel;
    EdServicoBD: TdmkEdit;
    Panel12: TPanel;
    BtInclui: TBitBtn;
    BtExclui: TBitBtn;
    QrEmExecAtivo: TSmallintField;
    QrEmExecZipar: TSmallintField;
    TbBkAgendaZipar: TSmallintField;
    BtAltera: TBitBtn;
    TbBkAgendaTipo: TSmallintField;
    TbBkAgendaTipo_TXT: TWideStringField;
    QrEmExecTipo: TSmallintField;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtTudoClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure BtEstruturaClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure BtRestoreClick(Sender: TObject);
    procedure MySQLBackUp1ActionError(Sender: TObject; PrgError: String);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure Fechar1Click(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure Mostrar1Click(Sender: TObject);
    procedure CkAoDesligarClick(Sender: TObject);
    procedure CkWinZipClick(Sender: TObject);
    procedure CkShutdownClick(Sender: TObject);
    procedure MyToolsSuccess(TableName, Status: String);
    procedure MyToolsError(TableName, ErrorMessage: String);
    procedure BtExclamacaoClick(Sender: TObject);
    procedure BtVerificaClick(Sender: TObject);
    procedure BtServicosClick(Sender: TObject);
    procedure BtBackupClick(Sender: TObject);
    procedure LBDatabaseClick(Sender: TObject);
    procedure edHostChange(Sender: TObject);
    procedure edHostExit(Sender: TObject);
    procedure edDbChange(Sender: TObject);
    procedure edPswdChange(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure Normal1Click(Sender: TObject);
    procedure Beta1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure TrayIcon1Click(Sender: TObject);
    procedure Executarnainicializao1Click(Sender: TObject);
    procedure NOexecutarnainicializao1Click(Sender: TObject);
    procedure TimerIniAgendaTimer(Sender: TObject);
    procedure TimerBkExecTimer(Sender: TObject);
    procedure QrProxBkCalcFields(DataSet: TDataSet);
    procedure BtAtualizaClick(Sender: TObject);
    procedure edPortaExit(Sender: TObject);
    procedure edPortaChange(Sender: TObject);
    procedure TbBkAgendaCalcFields(DataSet: TDataSet);
    procedure QrEmExecCalcFields(DataSet: TDataSet);
    procedure DBGrid2DblClick(Sender: TObject);
    procedure BtImportaClick(Sender: TObject);
    procedure BtSobreClick(Sender: TObject);
    procedure BtContratoClick(Sender: TObject);
    procedure BtAjudaClick(Sender: TObject);
    procedure TmVersaoTimer(Sender: TObject);
    procedure EdServicoBDExit(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure TbBkAgendaAfterOpen(DataSet: TDataSet);
    procedure TbBkAgendaBeforePost(DataSet: TDataSet);
    procedure TbBkAgendaBeforeEdit(DataSet: TDataSet);
    procedure BtAlteraClick(Sender: TObject);
    procedure DBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure TbBkAgendaBeforeDelete(DataSet: TDataSet);
    procedure EdHostAgendaExit(Sender: TObject);
  private
    { Private-Deklarationen }
    FBackupEmExec, FValidaContrato: Boolean;
    FHides, FTentativa, FVarredura: Integer;
    FPathMySQL: String;
    //
    FShowed: Boolean;
    //procedure SetOptions;
    //procedure BackupDeTabela(DataBase: String);
    //procedure WMTrayIcon(var Msg: TMessage); message WM_TRYICON;
    procedure WMQueryEndSession(var Msg: TWMQueryEndSession); message WM_QUERYENDSESSION;
    //procedure WMEndSession(var Msg: TWMEndSession); message WM_ENDSESSION;
    //procedure CompactaArquivo(Arquivo, Origem, Destino: String; Exclui: Integer);
    procedure ExecutaNaInicializacao(Executa: Boolean; Titulo, Programa: String);
    procedure ReopenProxBk();
    procedure FazerSincronismoAutomatico(Agora, AgoraUTC: TDateTime; QryEmExec: TmySQLQuery);
    procedure FazerBackupAutomatico2(Agora, AgoraUTC: TDateTime);
    //Usar FazerBackupAutomatico2 => procedure FazerBackupAutomatico(Agora, AgoraUTC: TDateTime);
    procedure AtualizaTabela();
    function  ValidaAceite(): Boolean;
    //procedure MostraBackup3();
    function  DecryptSenha(Codigo: Integer): AnsiString;
    function  VerificaNovasVersoes(ApenasVerifica: Boolean = False): Boolean;
    // N O T I F I C A � � E S
    procedure ConfiguraNotificacao(DataBase: TmySQLDatabase; Tipo: Integer;
              AgoraUTC: TDateTime; Texto: String);
    procedure AtualizaMsg(DBs: array of TmySQLDatabase; Tipo: Integer; Agora,
              AgoraUTC: TDateTime; Msg: String; SemErros: Boolean);
    // F O R M S
    procedure MostraFormCfgBackup(SQLTipo: TSQLType);
    procedure MostraFormAbout();
  public
    { Public-Deklarationen }
    FStartTime: TTime;
    FHideFirst: Boolean;
    FFazendoBackup: Boolean;
    FProximoBackup: TDateTime;
    FLinhaProximo: Integer;
    FSenha: String;
    procedure ExecutaDiagnostico(Diagnostico: TTipoDiagnostico);
    function  ObtemDataStr(Date: TDateTime): String;
  end;

var
  FmWetBlueMLA_BK: TFmWetBlueMLA_BK;
  SMyProfil,SMyHost,SMyDB,SUser,SPwd:string;
  WMLA_TbCount, WMLA_DuCount, WMLA_ZPCount: Integer;
  WMLA_Visible: Boolean;
  WMLA_T1: TTime;
  WMLA_Desliga, WMLA_Visualiza: Boolean;
const
  CO_TimerBkExec_Interval = 10000;
  CO_TimerInicio_Interval = 60000;
  CO_Titulo = 'Dermatek Backup';

implementation

{$R *.DFM}

uses (*20230320 Estrutura, NovaVersao, UnWAceites, UnDmkWeb, *) MyGlyfs, UnMLAGeral, Principal, MyVCLSkin, MeusDBs, Restaura,
  Horarios, Backup3, Module, UnMyObjects, MyListas,
  //LinkRankSkin,
  ZForge, DmkDAC_PF, About, UnGrl_Consts, UnitNotificacoesEdit,
  UMySQLDB, CfgBackup;

function GetRandomString : string;
var
  bCase : boolean;
  sTemp : string;
  iLen, i : integer;
begin
  sTemp:='';
  iLen:=Random(254)+1;
  for i:=1 to iLen do begin
    bCase:=(Random(2)=1);
    if bCase then
      sTemp:=sTemp+Chr(Random(25)+Ord('A'))
    else
      sTemp:=sTemp+Chr(Random(25)+Ord('a'));
  end;
  Result:=sTemp;
end;

procedure TFmWetBlueMLA_BK.BtDesisteClick(Sender: TObject);
begin
  QrTabelas.Close;
  QrDatabases.Close;
  BtConfirma.Enabled  := True;
  BtDesiste.Enabled   := False;
  BtBackup.Enabled    := False;
  BtRestore.Enabled   := False;
  BtEstrutura.Enabled := False;
  Dmod.MyDB.Connected := False;
  LBDatabase.Items.Clear;
  CBTables.Items.Clear;
end;

procedure TFmWetBlueMLA_BK.BtTudoClick(Sender: TObject);
var
  i: Integer;
begin
  for i:=0 to CBTables.Items.Count -1 do
      CBTables.Checked[i]:=true;
end;

procedure TFmWetBlueMLA_BK.BtNenhumClick(Sender: TObject);
var i:integer;
begin
  for i:=0 to CBTables.Items.Count -1 do
      CBTables.Checked[i]:=false;
end;

procedure TFmWetBlueMLA_BK.BtEstruturaClick(Sender: TObject);
begin
(* 20230320   Application.CreateForm(TFmEstrutura, FmEstrutura);
  FmEstrutura.ShowModal;
  FmEstrutura.Destroy;  *)
end;

procedure TFmWetBlueMLA_BK.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if TrayIcon1 <> nil then
    TrayIcon1.Visible := False;
{
var
  NotifyIconData : TNotifyIconData;
begin
  Screen.Cursor:=crDefault;
  with NotifyIconData do begin
    cbSize := sizeof(TNotifyIconData);
    Wnd := Self.Handle;
    uId := 0;
    uFlags := 0;
  end;
  Shell_NotifyIcon(NIM_DELETE,@NotifyIconData);
}
end;

{
procedure TFmWetBlueMLA_BK.WMTrayIcon(var Msg: TMessage);
var
  Pt : TPoint;
begin
  if Msg.lParam = WM_RBUTTONDOWN then begin
    GetCursorPos(Pt);
    PopupMenu1.Popup(Pt.X,Pt.Y);
  end
  else if Msg.lParam = WM_LBUTTONDBLCLK then FmWetBlueMLA_BK.Show;
end;
}

procedure TFmWetBlueMLA_BK.FormShow(Sender: TObject);
begin
  FValidaContrato := ValidaAceite;
  //
{
  if FValidaContrato then
    MeuVCLSkin.VCLSkinDefineUso(FmPrincipal.sd1, FmPrincipal.skinstore1);
}
end;

procedure TFmWetBlueMLA_BK.BtRestoreClick(Sender: TObject);
var
  Database: String;
  IP: String;
begin
  VAR_IPBACKUP := edHost.Text;
  FFazendoBackup := True;
  if LBDatabase.ItemIndex > -1 then
    DataBase := LBDatabase.Items[LBDatabase.ItemIndex];
  if Database = '' then //ShowMessage('Database n�o defenido');
  begin
    Geral.MB_Erro('Database n�o definido ou inexistente! O aplicativo ser� encerrado');
    Application.Terminate;
    Exit;
  end;
  TMeuDB      := DataBase;
  VAR_SQLUSER := Edlogin.Text;
  Application.CreateForm(TFmRestaura, FmRestaura);
  //
  if VAR_IPBACKUP <> ''  then
    IP := VAR_IPBACKUP
  else
    IP := 'localhost';
  if VAR_BACKUPDIR  = '' then
    VAR_BACKUPDIR  := CO_DIR_RAIZ_DMK + '\BackUps';
  if VAR_RESTOREDIR = '' then
    VAR_RESTOREDIR := CO_DIR_RAIZ_DMK + '\Backups\Restore';
  try
    (*
    with FmRestaura.DBMySQL do
    begin
      Connected    := False;
      Host         := IP;
      DatabaseName := TMeuDB;
      UserName     := VAR_SQLUSER;
      UserPassword := FSenha;
      Connected    := True;
    end;
    *)
    UnDmkDAC_PF.ConectaMyDB_DAC(FmRestaura.DBMySQL, TMeuDB, IP,
      edPorta.ValueVariant,  VAR_SQLUSER, FSenha, (*Desconecta*)True,
      (*Configura*)True, (*Conecta*)True);
    //
    FmRestaura.FZipPath := EdZipPath.Text;
    FmRestaura.ShowModal;
    FmRestaura.Destroy;
  except
    FmRestaura.Destroy;
  end;
  FFazendoBackup := False;
end;

procedure TFmWetBlueMLA_BK.MySQLBackUp1ActionError(Sender: TObject;
  PrgError: String);
begin
 MessageDlg(PrgError, mtError	, [mbOk], 0);
end;

procedure TFmWetBlueMLA_BK.BtConfirmaClick(Sender: TObject);
var
  Nome: String;
begin
  Screen.Cursor := crHourGlass;
  //
  FSenha := edPswd.Text;
  if FSenha = 'abo049352' then FSenha := VAR_BDSENHA;
  LBDatabase.Items.Clear;
  QrTabelas.Close;
  QrDatabases.Close;
  //
  (*
  Dmod.MyDB.Connected    := False;
  Dmod.MyDB.Host         := edHost.Text;
  Dmod.MyDB.DatabaseName := edDB.Text;
  Dmod.MyDB.UserName     := edLogin.Text;
  Dmod.MyDB.UserPassword := FSenha;
  DMod.MyDB.Port         := Geral.IMV(edPorta.Text);
  Dmod.MyDB.Connected    := True;
  *)
  UnDmkDAC_PF.ConectaMyDB_DAC(Dmod.MyDB, edDB.Text, edHost.Text,
    Geral.IMV(edPorta.Text), edLogin.Text, FSenha, (*Desconecta*)True,
    (*Configura*)True, (*Conecta*)True);
  //
  QrDatabases.Open;
  while not QrDatabases.Eof do
  begin
    Nome := QrDatabases.Fields[0].AsString;
    if Uppercase(Nome) <> 'MYSQL' then LBDatabase.Items.Add(Nome);
    QrDatabases.Next;
  end;
  LBDatabase.ItemIndex:=0;
  LBDatabaseClick(Self);
  if QrDatabases.RecordCount > 0 then
  begin
    BtConfirma.Enabled  := False;
    BtDesiste.Enabled   := True;
    BtBackup.Enabled    := True;
    BtRestore.Enabled   := True;
    BtEstrutura.Enabled := True;
  end;
  Screen.Cursor := crDefault;
end;

procedure TFmWetBlueMLA_BK.BtContratoClick(Sender: TObject);
var
  Link: String;
begin
(* 20230320   Link := DmkWeb.ObtemLinkTermo(CO_CONTRATO_ID);
  //
  DmkWeb.MostraWebBrowser(Link);*)
end;

procedure TFmWetBlueMLA_BK.FormCreate(Sender: TObject);
  {
  N�o Usa Mais
  function VerificaZipPath(Folder: String): Integer;
  begin
    Result := 0;
    if not FileExists(Folder + '\WZZIP.EXE')    then Result := Result + 2;
    if not FileExists(Folder + '\WZUNZIP.EXE')  then Result := Result + 4;
  end;
  }
var
  //NotifyIconData : TNotifyIconData;
  //Usuario: Integer;
  Compacta: Boolean;
  //AoDesligar, Desligar: Boolean;
  ErrLoc1, ErrLoc2: Integer;
  Caminho: String;
  //ZipPath: String;
begin
  VAR_USA_TAG_BITBTN := True;
  VAR_CAMINHOSKINPADRAO := CO_DIR_RAIZ_DMK + '\Skins\VCLSkin\Office 2007.skn';
  //
  FHides          := 0;
  FValidaContrato := False;
  FTentativa      := 0;
  FVarredura      := 0;
  FShowed         := True;
  //
  Application.CreateForm(TDmod, Dmod);
  QrOpt.Database  := Dmod.MyDB;
  QrDatabases.Database  := Dmod.MyDB;
  QrTabelas.Database  := Dmod.MyDB;
  //
  QrDBDump.Database  := DBDump;
  QrTbDump.Database  := DBDump;
  //
  QrDmk.Database := DBAgenda;
  //
  Geral.DefineFormatacoes;
  PagCon.ActivePageIndex := 0;
  FStartTime := Time;
  VAR_BDSENHA := 'wkljweryhvbirt';
  FSenha := VAR_BDSENHA;
  Dmod.MyDB.UserPassword := FSenha;
  //MLAGeral.VCLSkinDefineCores(QrAparencias);
  //Usarminhascores1.Checked := MLAGeral.VCLSkinVerificaUsoDeAparencias(QrAparencias);
  ErrLoc1 := 0;
  ErrLoc2 := 0;
  //
  Caminho := Geral.ReadAppKeyCU('Dermatek_Path', 'Dermatek', ktString, '');
  if Caminho = '' then
  begin
    Caminho := CO_DIR_RAIZ_DMK;
    Geral.WriteAppKeyCU('Dermatek_Path', 'Dermatek', Caminho, ktString);
  end;
  VAR_BACKUPDIR  := Caminho + '\Backups\';
  VAR_RESTOREDIR := Caminho + '\Backups\Restore\';
  //
  {
  N�o Usa Mais
  ZipPath := Geral.ReadAppKeyCU('Zip_Path', 'Dermatek', ktString, '');
  if VerificaZipPath(ZipPath) > 0 then
  begin
    ZipPath := DmkSystem.DiretoriodoSystema(CSIDL_PROGRAM_FILES, 'WinZip', CO_DIR_RAIZ_DMK);
    VerificaZipPath(ZipPath);
  end;
  if ZipPath = '' then ZipPath := EdZipPath.Text;
  if ZipPath = '' then
  begin
    ZipPath := CO_DIR_RAIZ_DMK;
    Geral.WriteAppKeyCU('Dermatek_Path', 'Dermatek', ZipPath, ktString);
  end;
  //ZipPath := MLAGeral.PastaEspecial($0002);//CSIDL_PROGRAMS);
  EdZipPath.Text := ZipPath;
  Geral.WriteAppKeyCU('ZipPath', 'Dermatek', ZipPath , ktString);
  //////////////////////////////////////////////////////////////////////////////
  case ErrLoc1 of
    2: Application.MessageBox(PChar('O execut�vel "'+ZipPath+
      '\WZZIP.EXE" n�o foi encontrado. N�o ser� poss�vel efetuar backups!'), '', MB_OK+MB_ICONERROR);
    4: Application.MessageBox(PChar('O execut�vel "'+ZipPath+
      '\WZUNZIP.EXE" n�o foi encontrado. N�o ser� poss�vel efetuar restaura��es de backups!'), '', MB_OK+MB_ICONERROR);
    6: Application.MessageBox(PChar('Os execut�veis "'+ZipPath+'\WZZIP.EXE" e "'+
      ZipPath+'\WZUNZIP.EXE" n�o foram encontradoe. '+
      'N�o ser� poss�vel efetuar backups e restaura��es!'), '', MB_OK+MB_ICONERROR);
  end;
  }
  if not DirectoryExists(Caminho)  then
    if not CreateDir(Caminho) then
      raise Exception.Create('Imposs�vel criar diret�rio "'+Caminho+'"');
  if not DirectoryExists(VAR_BACKUPDIR)  then
    if not CreateDir(VAR_BACKUPDIR) then
      raise Exception.Create('Imposs�vel criar diret�rio "'+VAR_BACKUPDIR+'"');
  if not DirectoryExists(VAR_RESTOREDIR) then
    if not CreateDir(VAR_RESTOREDIR) then
      raise Exception.Create('Imposs�vel criar diret�rio "'+VAR_RESTOREDIR+'"');
  GetWindowsDirectory(VAR_WINPATH, 144);
  (*if not FileExists(VAR_WINPATH+'\System\MyBackup.dll') then ErrLoc2 := 2;
  if not FileExists(VAR_WINPATH+'\System\LibMySQL.dll') then  ErrLoc2 := ErrLoc2 + 4;
  case ErrLoc2 of
    2: ShowMessage('A DLL '+VAR_WINPATH+'\System\MyBackup.dll n�o foi encontrada. O backup dos sistemas Dermatek n�o funcionar�.');
    4: ShowMessage('A DLL '+VAR_WINPATH+'\System\LibMySQL.dll n�o foi encontrada. O backup dos sistemas Dermatek n�o funcionar�.');
    6: ShowMessage('As DLLs '+VAR_WINPATH+'\System\MyBackup.dll e LibMySQL.dll n�o foram encontradas. O backup dos sistemas Dermatek n�o funcionar�.');
  end;*)
  if (ErrLoc2 + ErrLoc1) > 0 then
  begin
    VAR_FAZERBACKUP := False;
    // N�o encerrar o programa porque o aplicativo tem outras fun��es alem de
    // BackUp
    //Application.Terminate;
  end else VAR_FAZERBACKUP := True;
  VAR_LISTBOX1 := ListBox1;
  WMLA_Desliga := False;
  WMLA_Visualiza := False;
  WMLA_Visible := True;
  //Usuario := Geral.ReadAppKeyCU('Intervalo', Application.Title, ktInteger, 60);
  //if Usuario < 1 then Usuario := 1;
  //Timer1.Interval := (60000 * Usuario);
  //EdIntervalo.Text := Geral.FF0(Usuario);
  Compacta := Geral.ReadAppKeyCU('Compacta', Application.Title, ktBoolean, False);
  //AoDesligar := Geral.ReadAppKeyCU('AoDesligar', Application.Title, ktBoolean, False);
  //Desligar := Geral.ReadAppKeyCU('Desligar', Application.Title, ktBoolean, False);
  CkWinZip.Checked := Compacta;
  //CkAoDesligar.Checked := AoDesligar;
  //CkShutDown.Checked := Desligar;
  edPswd.Text          := VAR_BDSENHA;
  edHost.Text          := Geral.ReadAppKeyCU('IP_Backup', Application.Title, ktString, '127.0.0.1');
  edPorta.Text         := Geral.ReadAppKeyCU('Porta_Backup', Application.Title, ktInteger, '');
  EdServicoBD.Text     := Geral.ReadAppKeyCU('Servico_BD', Application.Title, ktString, '');
  EdHostAgenda.Text    := Geral.ReadAppKeyCU('Host_Agenda', Application.Title, ktString, 'localhost');
  EdHostAgenda.Enabled := True;
  //
  if Geral.IMV(edPorta.Text) = 0 then
    edPorta.Text := Geral.ReadAppKeyCU('Porta', 'Dermatek', ktInteger, '3306');
  //
  {
  with NotifyIconData do begin
    cbSize := sizeof(TNotifyIconData);
    wnd := self.Handle;
    uId := 0;
    uCallbackMessage := WM_TRYICON;
    uFlags := NIF_ICON or NIF_TIP or NIF_MESSAGE;
    hIcon := Application.Icon.Handle;
    szTip := 'Backup BDs Dermatek';
  end;
  Shell_NotifyIcon(NIM_ADD, @NotifyIconData);
  }
  TimerIniAgenda.Interval := 500;//CO_TimerInicio_Interval;
  TmVersao.Interval       := CO_TimerInicio_Interval;
  TimerIniAgenda.Enabled  := True;
  TmVersao.Enabled        := True;
  //
  FBackupEmExec := False;
end;

procedure TFmWetBlueMLA_BK.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  //
  if FValidaContrato then
  begin
    TrayIcon1.Visible := True;
    //
    if FHides = 0 then
    begin
     FHides         := FHides + 1;
     Timer1.Enabled := True;
    end;
  end;
end;

procedure TFmWetBlueMLA_BK.Fechar1Click(Sender: TObject);
begin
  Close;
end;

procedure TFmWetBlueMLA_BK.PopupMenu1Popup(Sender: TObject);
var
  Valor: String;
begin
  Valor := Geral.ReadAppKeyCU(CO_Titulo, 'Microsoft\Windows\CurrentVersion\Run', ktString, '');
  //
  if Valor <> '' then
  begin
    Executarnainicializao1.Checked   := True;
    NOexecutarnainicializao1.Checked := False;
  end else
  begin
    Executarnainicializao1.Checked   := False;
    NOexecutarnainicializao1.Checked := True;
  end;
  Mostrar1.Enabled := not FmWetBlueMLA_BK.Visible;
end;

procedure TFmWetBlueMLA_BK.QrEmExecCalcFields(DataSet: TDataSet);
begin
  QrEmExecPASS_DECR.Value := DecryptSenha(QrEmExecCodigo.Value);
end;

procedure TFmWetBlueMLA_BK.QrProxBkCalcFields(DataSet: TDataSet);
begin
  if QrProxBkHora.Value > Time() then
    QrProxBkFALTA.Value := QrProxBkHora.Value - Time()
  else
    QrProxBkFALTA.Value := (1 - Time()) + QrProxBkHora.Value;
end;

procedure TFmWetBlueMLA_BK.ReopenProxBk();
begin
  UnDmkDAC_PF.AbreQuery(QrProxBk, DBAgenda);
end;

procedure TFmWetBlueMLA_BK.MostraFormCfgBackup(SQLTipo: TSQLType);
var
  BD: String;
begin
  if DBAgenda.Connected then
  begin
    if FBackupEmExec = False then
    begin
      if MyObjects.CriaForm_AcessoTotal(TFmCfgBackup, FmCfgBackup) then
      begin
        FmCfgBackup.ImgTipo.SQLType := SQLTipo;
        FmCfgBackup.FDB             := DBAgenda;
        //
        if SQLTipo = stIns then
        begin
          if LBDatabase.ItemIndex >= 0 then
            BD := LBDatabase.Items[LBDatabase.ItemIndex]
          else
            BD := edDb.ValueVariant;
          //
          FmCfgBackup.FCodigo              := 0;
          FmCfgBackup.CBTipo.ItemIndex     := -1;
          FmCfgBackup.CBTipo.Enabled       := True;
          FmCfgBackup.EdHost.ValueVariant  := edHost.ValueVariant;
          FmCfgBackup.EdPorta.ValueVariant := edPorta.ValueVariant;
          FmCfgBackup.EdDb.ValueVariant    := BD;
          FmCfgBackup.EdUser.ValueVariant  := EdLogin.ValueVariant;
          FmCfgBackup.EdPass.ValueVariant  := edPswd.ValueVariant;
          FmCfgBackup.CkZipar.Checked      := True;
          FmCfgBackup.CkAtivo.Checked      := True;
        end else
        begin
          FmCfgBackup.FCodigo                := TbBkAgendaCodigo.Value;
          FmCfgBackup.CBTipo.ItemIndex       := TbBkAgendaTipo.Value;
          FmCfgBackup.CBTipo.Enabled         := False;
          FmCfgBackup.EdDestino.ValueVariant := TbBkAgendaDestino.Value;
          FmCfgBackup.EdHora.ValueVariant    := TbBkAgendaHora.Value;
          FmCfgBackup.EdDias.ValueVariant    := TbBkAgendaDias.Value;
          FmCfgBackup.EdHost.ValueVariant    := TbBkAgendaHost.Value;
          FmCfgBackup.EdPorta.ValueVariant   := TbBkAgendaPort.Value;
          FmCfgBackup.EdDb.ValueVariant      := TbBkAgendaDb.Value;
          FmCfgBackup.EdUser.ValueVariant    := TbBkAgendaUser.Value;
          FmCfgBackup.EdPass.ValueVariant    := TbBkAgendaPASS_DECR.Value;
          FmCfgBackup.CkZipar.Checked        := Geral.IntToBool(TbBkAgendaZipar.Value);
          FmCfgBackup.CkAtivo.Checked        := Geral.IntToBool(TbBkAgendaAtivo.Value);
        end;
        FmCfgBackup.ShowModal;
        FmCfgBackup.Destroy;
        //
        UnDmkDAC_PF.AbreTable(TbBkAgenda, DBAgenda);
        ReopenProxBk();
      end;
    end else
    begin
      Geral.MB_Aviso('O backup autom�tico est� em execu��o!' + sLineBreak +
        'Aguarda seu t�rmino e tente novamente!');
    end;
  end else
    Geral.MB_Aviso('A agenda ainda n�o foi carregada!' + sLineBreak +
      'Aguarde o carregamento e tente novamente!');
end;

procedure TFmWetBlueMLA_BK.Mostrar1Click(Sender: TObject);
begin
  FmWetBlueMLA_BK.Visible := True;
end;

procedure TFmWetBlueMLA_BK.WMQueryEndSession(var Msg: TWMQueryEndSession);
var
  DataHasChanged: Boolean;
begin
 // Let the inherited message handler respond first
 inherited;
 DataHasChanged := False;
 if DataHasChanged then begin
    MessageBeep(MB_ICONQUESTION);
    case MessageDlg('The current Windows session is ending.  Save league changes?',
           mtConfirmation, [mbYes,mbNo,mbCancel],0) of
    mrYes    : begin
                //Your data-saving code or method
                //call goes here
                Msg.Result := 1;
               end;
    mrNo     : Msg.Result := 1;
    mrCancel : Msg.Result := 0;
      end; {case} end
   else
      Msg.Result := 1;
end;

procedure TFmWetBlueMLA_BK.CkAoDesligarClick(Sender: TObject);
begin
  //Geral.WriteAppKeyCU('AoDesligar', Application.Title, CkAoDesligar.Checked, ktBoolean);
end;

procedure TFmWetBlueMLA_BK.CkWinZipClick(Sender: TObject);
begin
  Geral.WriteAppKeyCU('Compacta', Application.Title, CkWinZip.Checked, ktBoolean);
end;

procedure TFmWetBlueMLA_BK.CkShutdownClick(Sender: TObject);
begin
  //Geral.WriteAppKeyCU('Desligar', Application.Title, CkShutDown.Checked, ktBoolean);
end;

procedure TFmWetBlueMLA_BK.MyToolsSuccess(TableName, Status: String);
begin
  ShowMessage('Sucesso!');
end;

procedure TFmWetBlueMLA_BK.MyToolsError(TableName, ErrorMessage: String);
begin
  ShowMessage('Erro!');
end;

procedure TFmWetBlueMLA_BK.BtExclamacaoClick(Sender: TObject);
begin
  //mytdCheck, mytdAnalize, mytdRepair, mytdOptimize
  ExecutaDiagnostico(mytdCheck);
end;

procedure TFmWetBlueMLA_BK.BtExcluiClick(Sender: TObject);
begin
  if (TbBkAgenda.State <> dsInactive) and (TbBkAgenda.RecordCount > 0) then
  begin
    if FBackupEmExec = False then
    begin
      if USQLDB.ExcluiRegistroInt1('Confirma a exclus�o do registro selecionado?',
        'bkagenda', 'Codigo', TbBkAgendaCodigo.Value, DBAgenda) = ID_YES then
      begin
        UnDmkDAC_PF.AbreTable(TbBkAgenda, DBAgenda);
        ReopenProxBk();
      end;
    end else
      Geral.MB_Aviso('O backup autom�tico est� em execu��o!' + sLineBreak +
        'Aguarda seu t�rmino e tente novamente!');
  end;
end;

procedure TFmWetBlueMLA_BK.BtImportaClick(Sender: TObject);
const
  Descricao = nil;
begin
  if BtConfirma.Enabled = False then
  begin
    Geral.MB_Aviso('Banco de dados conectado!' + sLineBreak +
      'Desconecte e tente novamente.');
    Exit;
  end;
  MyObjects.ImportaConfigWeb(Self, Descricao, EdHost, edDb, EdPorta, EdLogin,
    edPswd);
end;

procedure TFmWetBlueMLA_BK.BtIncluiClick(Sender: TObject);
begin
  MostraFormCfgBackup(stIns);
end;

procedure TFmWetBlueMLA_BK.BtAjudaClick(Sender: TObject);
var
  Link: String;
begin
(* 20230320
    Link := DmkWeb.ObtemLinkAjuda(CO_DMKID_APP, 0);
  //
  DmkWeb.MostraWebBrowser(Link)
  *);
end;

procedure TFmWetBlueMLA_BK.BtAlteraClick(Sender: TObject);
begin
  if (TbBkAgenda.State <> dsInactive) and (TbBkAgenda.RecordCount > 0) then
    MostraFormCfgBackup(stUpd);
end;

procedure TFmWetBlueMLA_BK.BtAtualizaClick(Sender: TObject);
begin
  VerificaNovasVersoes;
end;

procedure TFmWetBlueMLA_BK.BtVerificaClick(Sender: TObject);
begin
  //mytdCheck, mytdAnalize, mytdRepair, mytdOptimize
  ExecutaDiagnostico(mytdAnalize);
end;

procedure TFmWetBlueMLA_BK.BtServicosClick(Sender: TObject);
begin
  //mytdCheck, mytdAnalize, mytdRepair, mytdOptimize
  ExecutaDiagnostico(mytdRepair);
end;

procedure TFmWetBlueMLA_BK.MostraFormAbout();
begin
  if MyObjects.CriaForm_AcessoTotal(TFmAbout, FmAbout) then
  begin
    FmAbout.ShowModal;
    FmAbout.Destroy;
  end;
end;

procedure TFmWetBlueMLA_BK.BtSobreClick(Sender: TObject);
begin
  MostraFormAbout();
end;

procedure TFmWetBlueMLA_BK.BitBtn2Click(Sender: TObject);
begin
  //mytdCheck, mytdAnalize, mytdRepair, mytdOptimize
  ExecutaDiagnostico(mytdOptimize);
end;

procedure TFmWetBlueMLA_BK.EdServicoBDExit(Sender: TObject);
begin
  Geral.WriteAppKeyCU('Servico_BD', Application.Title, EdServicoBD.Text, ktString);
end;

procedure TFmWetBlueMLA_BK.ExecutaDiagnostico(Diagnostico: TTipoDiagnostico);
var
  Tabelas, I: Integer;
  Txt: String;
begin
  Screen.Cursor := crHourGlass;
  try
    if LBDatabase.ItemIndex < 0 then
    begin
      Application.MessageBox('Base de dados n�o definida', 'Erro', MB_OK+MB_ICONERROR);
      Screen.Cursor := crDefault;
      Exit;
    end;
    QrOpt.Close;
    (*
    Dmod.MyDB.Connected := False;
    Dmod.MyDB.DatabaseName := LBDatabase.Items[LBDatabase.ItemIndex];
    Dmod.MyDB.Connected := True;
    *)
    UnDmkDAC_PF.ConectaMyDB_DAC(Dmod.MyDB, LBDatabase.Items[LBDatabase.ItemIndex],
      edHost.ValueVariant, edPorta.ValueVariant, EdLogin.ValueVariant,
      FSenha, (*Desconecta*)True, (*Configura*)True, (*Conecta*)True);
    //
    Tabelas := 0;
    case Diagnostico of
      mytdCheck    : Txt := 'CHECK ';
      mytdAnalize  : Txt := 'ANALYZE ';
      mytdRepair   : Txt := 'REPAIR ';
      mytdOptimize : Txt := 'OPTIMIZE ';
    end;
    Txt := Txt + 'TABLE ';
    for i:=0 to CBTables.Items.Count -1 do
    begin
      if CBTables.Checked[i]= True then
      begin
        if Tabelas > 0 then Txt := Txt+', ';
        Txt := Txt + CBTables.Items[i];
        Tabelas := Tabelas + 1;
      end;
    end;
    if Tabelas > 0 then
    begin
      QrOpt.Close;
      QrOpt.SQL.Clear;
      QrOpt.SQL.Add(Txt);
      QrOpt.Open;
    end else Application.MessageBox('Nenhuma tabela foi selecionada',
      'Erro', MB_OK+MB_ICONERROR);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmWetBlueMLA_BK.BtBackupClick(Sender: TObject);
var
  i: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    MyObjects.Informa(LaAviso, True, 'Configurando janela de backup');
    //
    Progress1.Position := 0;
    Application.CreateForm(TFmBackup3, FmBackup3);
    Progress1.Max := FmBackup3.ComponentCount;
    (*
    FmBackup3.DBAll.Disconnect;
    FmBackup3.DBAll.Host         := edHost.Text;
    FmBackup3.DBAll.UserName     := EdLogin.Text;
    FmBackup3.DBAll.UserPassword := EdPswd.Text;
    FmBackup3.DBAll.DataBaseName := EdDB.Text;
    FmBackup3.DbAll.Port := Geral.IMV(edPorta.Text);
    FmBackup3.DBAll.Connect;
    *)
    UnDmkDAC_PF.ConectaMyDB_DAC(FmBackup3.DBAll, EdDB.Text, edHost.Text,
      edPorta.ValueVariant,  EdLogin.Text, EdPswd.Text, (*Desconecta*)True,
      (*Configura*)True, (*Conecta*)True);
    //
    FmBackup3.QrDatabases1.Close;
    FmBackup3.QrDatabases1.Open;

    FmBackup3.EdSenha.Text := FSenha;
    FmBackup3.EdZipPath.Text := EdZipPath.Text;
    for i := 0 to FmBackup3.ComponentCount - 1 do
    begin
      Progress1.Position := Progress1.Position + 1;
      if FmBackup3.Components[i] is TmySQLQuery then
        with TButton(Components[i]) do
          case ModalResult of
            mrYes: Caption := 'Cont�nua';
            mrOK: Caption := 'Program.';
            mrNo: Caption := 'N�o cobra';
            mrCancel: Caption := 'Cancela';
          end;
      if LBDatabase.ItemIndex > -1 then
      begin
        if FmBackup3.QrDatabases1.Locate('Database', LBDatabase.Items[LBDatabase.ItemIndex], []) then
        begin
          FmBackup3.CBDB1.KeyValue := LBDatabase.Items[LBDatabase.ItemIndex];
          FmBackup3.CBDB1Click(Self);
        end;
      end;
    end;
  finally
    Screen.Cursor := crDefault;
    Progress1.Position := 0;
    MyObjects.Informa(LaAviso, False, '...');
  end;
  FmBackup3.ShowModal;
  FmBackup3.Destroy;
  //MyObjects.MostraPopUpDeBotao(PMTipoBK, BtBackup);
end;

{
procedure TFmWetBlueMLA_BK.MostraBackup3();
var
  DBName: String;
begin
  Application.CreateForm(TFmBackUp3, FmBackUp3);
  //if DBCheck.CriaFm(TFmBackUp3, FmBackUp3, afmoNegarComAviso) then
  with FmBackUp3 do
  begin
    with FmBackUp3.DbAll do
    begin
      Connected    := False;
      Host         := edHost.Text;
      DataBaseName := EdDB.Text;
      UserName     := EdLogin.Text;
      UserPassword := EdPswd.Text;
      Port := Geral.IMV(edPorta.Text);
      Connected    := True;
    end;
    //
    FmBackup3.QrDatabases1.Close;
    FmBackup3.QrDatabases1.Open;
    if TMeuDB <> '' then
    begin
      FmBackup3.QrDatabases1.First;
      while not FmBackup3.QrDatabases1.Eof do
      begin
        DBName := Lowercase(TMeuDB);
        if Lowercase(FmBackup3.QrDatabases1.Fields[0].AsString) = DBName then
        begin
          FmBackup3.CBDB1.KeyValue := FmBackup3.QrDatabases1.Fields[0].AsString;
          FmBackup3.CBDB1Click(Self);
          Break;
        end;
        //
        FmBackup3.QrDatabases1.Next;
      end;
    end;
    FmBackUp3.ShowModal;
    FmBackUp3.Destroy;
  end;
end;
}
procedure TFmWetBlueMLA_BK.LBDatabaseClick(Sender: TObject);
var
  Nome: String;
begin
  //MySQLBackUp1.WriteLogHeading:=true;
  if LBDatabase.Items.Count < 1 then
  begin
    Application.MessageBox('Lista de Bases de Dados vazia.', 'Erro',
      MB_OK+MB_ICONERROR);
    Exit;
  end;
  CBTables.Clear;
  QrTabelas.Close;
  QrTabelas.SQL.Clear;
  QrTabelas.SQL.Add('SHOW TABLES FROM '+lbdatabase.items[lbdatabase.itemindex]);
  QrTabelas.Open;
  while not QrTabelas.Eof do
  begin
    Nome := QrTabelas.Fields[0].AsString;
    if Uppercase(Nome) <> 'MASTER' then CBTables.Items.Add(Nome);
    QrTabelas.Next;
  end;
  (*with MySQLBackUp1 do
  begin
    MySQLBackUp1.Host := edPswd.Text;
    if GetTables(LBDatabase.Items[LBDatabase.ItemIndex]) > 0
       then CBTables.Items.Assign(Tables);
  end;*)
  StatusBar1.Panels[1].Text :='Base da dados ativa: -> '+LBDatabase.Items[LBDatabase.ItemIndex];
end;

procedure TFmWetBlueMLA_BK.edHostChange(Sender: TObject);
begin
  BtDesisteClick(Self);
end;

procedure TFmWetBlueMLA_BK.edHostExit(Sender: TObject);
begin
  Geral.WriteAppKeyCU('IP_Backup', Application.Title, edHost.Text, ktString);
end;

procedure TFmWetBlueMLA_BK.edDbChange(Sender: TObject);
begin
  BtDesisteClick(Self);
end;

procedure TFmWetBlueMLA_BK.EdHostAgendaExit(Sender: TObject);
begin
  Geral.WriteAppKeyCU('Host_Agenda', Application.Title, EdHostAgenda.Text, ktString);
  //
  Geral.MB_Aviso('Para que as altera��es tenham efeito o aplicativo dever� ser reiniciado!');
end;

procedure TFmWetBlueMLA_BK.edPswdChange(Sender: TObject);
begin
  BtDesisteClick(Self);
end;

procedure TFmWetBlueMLA_BK.TbBkAgendaAfterOpen(DataSet: TDataSet);
begin
  ReopenProxBk();
end;

procedure TFmWetBlueMLA_BK.TbBkAgendaBeforeDelete(DataSet: TDataSet);
begin
  Abort;
end;

procedure TFmWetBlueMLA_BK.TbBkAgendaBeforeEdit(DataSet: TDataSet);
begin
  Abort;
end;

procedure TFmWetBlueMLA_BK.TbBkAgendaBeforePost(DataSet: TDataSet);
begin
  Abort;
end;

procedure TFmWetBlueMLA_BK.TbBkAgendaCalcFields(DataSet: TDataSet);
var
  Txt: String;
  i, Tam: Integer;
begin
  TbBkAgendaPASS_DECR.Value := DecryptSenha(TbBkAgendaCodigo.Value);
  //
  Txt := '';
  Tam := Length(TbBkAgendaPASS_DECR.Value);
  //
  for i := 0 to Tam - 1 do
  begin
    Txt := Txt + 'l';
  end;
  TbBkAgendaPASS_TXT.Value := AnsiString(Txt);
  TbBkAgendaTipo_TXT.Value := USQLDB.Backup_ObtemTipoTxt(TbBkAgendaTipo.Value);
end;

procedure TFmWetBlueMLA_BK.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  if Visible then
  begin
    EdSeg.Text := FormatDateTime('  hh:nn:ss',Now - FStartTime);
    if QrProxBk.State <> dsInactive then
      QrProxBk.Refresh;
    if not FHideFirst then
    begin
      FHideFirst := True;
      Application.MainFormOnTaskBar := FShowed;
      Hide;
      //FmWetBlueMLA_BK.WindowState :=  wsMinimized;
      Application.MainFormOnTaskBar := FShowed;
      TrayIcon1.Visible := True;
    end;
  end;
end;

procedure TFmWetBlueMLA_BK.BtSaidaClick(Sender: TObject);
begin
  TrayIcon1.Visible := True;
  Application.MainFormOnTaskBar := FShowed;
  FmWetBlueMLA_BK.Hide;
  //FmWetBlueMLA_BK.WindowState :=  wsMinimized;
  Application.MainFormOnTaskBar := FShowed;
end;

procedure TFmWetBlueMLA_BK.TimerBkExecTimer(Sender: TObject);
var
  Agora, AgoraUTC: TDateTime;
begin
  // Erro grave de mysqldump?
  Timer1.Enabled := False;
  //
  EdHostAgenda.Enabled := False;
  FBackupEmExec        := True;
  TimerBkExec.Enabled  := False;
  Screen.Cursor        := crHourGlass;
  try
    FVarredura := FVarredura + 1;
    //
    if not dmkPF.ExecutavelEstaRodando('mysqldump.exe') then //O MySQLDump n�o pode estar rodando
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrEmExec, DBAgenda, [
        'SELECT IF(Hora < CURTIME(), 1, 0) Idx, ',
        'Hora, UltTry, UltExe, UltErr, Host, Db, ',
        'Destino, Codigo, User, Pass, Port, Ativo, Zipar, ',
        'UTC_TIMESTAMP() AGORAUTC, NOW() AGORA, Dias, Tipo ',
        'FROM bkagenda ',
        'WHERE Hora <= CURTIME() ',
        'AND (UltExe < CURDATE() OR UltExe IS NULL) ',
        'AND NOT (Hora IS NULL) ',
        'AND Host <> "" ',
        'AND DB <> "" ',
        'AND (',
        'Destino <> "" AND Tipo = 0 OR Tipo = 1 ',
        ') ',
        'AND Ativo = 1 ',
        'ORDER BY Idx, Hora ',
        '']);
      while not QrEmExec.Eof do
      begin
        Agora    := QrEmExec.FieldByName('AGORA').AsDateTime;
        AgoraUTC := QrEmExec.FieldByName('AGORAUTC').AsDateTime;
        //
        if QrEmExec.FieldByName('Tipo').AsInteger = 0 then
        begin
          //Backup
          FazerBackupAutomatico2(Agora, AgoraUTC);
        end else
        begin
          //Sincronismo
(* 20230320
          if dmkWeb.RemoteConnection() then
            FazerSincronismoAutomatico(Agora, AgoraUTC, QrEmExec);
*)
        end;
        //
        QrEmExec.Next;
      end;
      TbBkAgenda.Close;
      TbBkAgenda.Open;
    end;
    EdHostAgenda.Enabled := True;
    FBackupEmExec        := False;
    TimerBkExec.Enabled  := True;
    //
    Screen.Cursor := crDefault;
  except
    EdHostAgenda.Enabled := True;
    FBackupEmExec        := False;
    Screen.Cursor        := crDefault;
  end;
end;

procedure TFmWetBlueMLA_BK.TimerIniAgendaTimer(Sender: TObject);
var
  Continua: Boolean;
  DatabaseName, Host, ServicoBD: String;
  Port: Integer;
  UserName, UserPassword: String;
begin
  FTentativa := FTentativa + 1;
  TimerIniAgenda.Enabled := False;
  if LaAgenda.Caption = '...' then
    LaAgenda.Caption := 'Tentando obter agenda...';
  if DbAgenda.Connected then
  begin
    if FTentativa = 1 then
      LaAgenda.Caption := 'Erro. DB da agenda conectado antes de iniciar!';
    Memo3.Text := Geral.NowTxt() +
    'Conex�o ao BD para ver agenda, cancelada. Motivo: DB j� conectado.';
    Exit;
  end;
{
  DBAgenda.Connected    := False;
  DBAgenda.Host         := EdHostAgenda.Text;
  DBAgenda.DatabaseName := 'mysql';
  DBAgenda.UserName     := 'root';
  DBAgenda.UserPassword := FSenha;
  DBAgenda.Port := Geral.IMV(edPorta.Text);
  try
    DBAgenda.Connected  := True;
}
  Host         := EdHostAgenda.Text;
  DatabaseName := 'mysql';
  UserName     := 'root';
  UserPassword := FSenha;
  Port         := Geral.IMV(edPorta.Text);
  ServicoBD    := Geral.ReadAppKeyCU('Servico_BD', Application.Title, ktString, '');
  try
    Continua := dmkPF.ExecutavelEstaRodando('mysqld.exe');
    //
    if Continua = False then
    begin
      Continua := dmkPF.ExecutavelEstaRodando('mysqld-nt.exe');
      //
      if Continua = True then
      begin
        if ServicoBD <> '' then
          Continua := dmkPF.ServicoWindowsEstaRodando(nil, PChar(ServicoBD));
      end;
    end else
    begin
      if ServicoBD <> '' then
        Continua := dmkPF.ServicoWindowsEstaRodando(nil, PChar(ServicoBD));
    end;
    //
    if Continua = False then //O MySQLd deve estar rodando
      raise exception.Create('Erro! O MySQL n�o est� sendo executado!');
    //
    UnDmkDAC_PF.ConectaMyDB_DAC(DBAgenda, DatabaseName, Host, Port, UserName,
      UserPassword, (*Desconecta*)True, (*Configura*)True, (*Conecta*)True);
    //
    if DBAgenda.Connected then
    begin
      QrDmk.Close;
      QrDmk.SQL.Clear;
      QrDmk.SQL.Add('SHOW DATABASES LIKE "dermatek"');
      QrDmk.Open;
      if QrDmk.RecordCount = 0 then
      begin
        LaAgenda.Caption := 'Tentando criar DB "DERMATEK"...';
        //
        QrDmk.Close;
        QrDmk.SQL.Clear;
        QrDmk.SQL.Add('CREATE DATABASE dermatek');
        QrDmk.ExecSQL;
        //
        LaAgenda.Caption := 'Tentando verificar de existe o DB "DERMATEK"...';
        QrDmk.Close;
        QrDmk.SQL.Clear;
        QrDmk.SQL.Add('SHOW DATABASES LIKE "dermatek"');
        QrDmk.Open;
      end;
      //
      if QrDmk.RecordCount > 0 then
      begin
        QrDmk.Close;
        //
        (*
        DBAgenda.Connected    := False;
        DBAgenda.Host         := EdHostAgenda.Text;
        DBAgenda.DatabaseName := 'dermatek';
        DBAgenda.UserName     := 'root';
        DBAgenda.UserPassword := FSenha;
        DBAgenda.Port := Geral.IMV(edPorta.Text);
        *)
        UnDmkDAC_PF.ConectaMyDB_DAC(DBAgenda, 'dermatek', EdHostAgenda.Text,
          Geral.IMV(edPorta.Text), 'root', FSenha, (*Desconecta*)True,
          (*Configura*)True, (*Conecta*)False);
        //
        if not USQLDB.TabelaExiste('bkagenda', QrDmk.DataBase) then
        begin
          LaAgenda.Caption := 'Tentando criar a tabela "BKAGENDA"...';
          QrDmk.Close;
          QrDmk.SQL.Clear;
          QrDmk.SQL.Add('CREATE TABLE bkagenda (');
          QrDmk.SQL.Add('  Codigo       int(11) AUTO_INCREMENT                 , ');
          QrDmk.SQL.Add('  Nome         varchar(100)                           , ');
          QrDmk.SQL.Add('  Host         varchar(255)                           , ');
          QrDmk.SQL.Add('  Db           varchar(255)                           , ');
          QrDmk.SQL.Add('  User         varchar(50)                            , ');
          QrDmk.SQL.Add('  Pass         varchar(50)                            , ');
          QrDmk.SQL.Add('  Port         int(11) NOT NULL DEFAULT 3306          , ');
          QrDmk.SQL.Add('  Dias         int(11) NOT NULL DEFAULT 10            , ');
          QrDmk.SQL.Add('  Hora         time                                   , ');
          QrDmk.SQL.Add('  UltTry       datetime  DEFAULT "0000-00-00 00:00:00", ');
          QrDmk.SQL.Add('  UltExe       datetime  DEFAULT "0000-00-00 00:00:00", ');
          QrDmk.SQL.Add('  UltErr       datetime  DEFAULT "0000-00-00 00:00:00", ');
          QrDmk.SQL.Add('  Destino      varchar(255)                           , ');
          QrDmk.SQL.Add('  Aviso        varchar(255)                           , ');
          QrDmk.SQL.Add('  Tipo         tinyint(1) NOT NULL DEFAULT 0          , '); //0 => Backup / 1 => Sincronismo
          QrDmk.SQL.Add('  Zipar        tinyint(1) NOT NULL DEFAULT 1          , ');
          QrDmk.SQL.Add('  Ativo        tinyint(1) NOT NULL DEFAULT 1          , ');
          QrDmk.SQL.Add('  CurrentUser  varchar(100)                           , '); //Usu�rio corrente logado no Windows
          QrDmk.SQL.Add('  PRIMARY KEY (Codigo)');
          QrDmk.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          QrDmk.ExecSQL;
          //
          //SHOW FIELDS FROM agenda LIKE 'Codigo'
        end else
        begin
          AtualizaTabela();
        end;
        LaAgenda.Caption := 'Tentando abrir a tabela "BKAGENDA"...';
        TbBkAgenda.Close;
        TbBkAgenda.Open;
        //
        LaAgenda.Caption := 'Agenda habilitada!';
        //
        TimerBkExec.Interval := CO_TimerBkExec_Interval;
        TimerBkExec.Enabled  := True;
      end;
    end
  except
    LaAgenda.Caption := 'Erro ao obter agenda...';
    TimerIniAgenda.Enabled := True;
  end;
end;

procedure TFmWetBlueMLA_BK.TmVersaoTimer(Sender: TObject);
begin
  TmVersao.Enabled := False;
  //
(* 20230320
  if DmkWeb.RemoteConnection then
  begin
    if VerificaNovasVersoes(True) then
      DmkWeb.MostraBalloonHintMenuTopo(BtAtualiza, BalloonHint1,
        'H� uma nova vers�o!', 'Clique aqui para atualizar.');
  end;
*)
end;

(* Usar FazerBackupAutomatico2
procedure TFmWetBlueMLA_BK.FazerBackupAutomatico(Agora, AgoraUTC: TDateTime);
const
  max_allowed_packet = '2097152';
  net_buffer_length = '32768';
  default_character_set = 'latin1';
  //User = 'root';
  //Senha = 'wkljweryhvbirt';
var
  Port, Zipar: Integer;
  User, Senha,
  Cmd, Host, Db, Pasta, Arq, ArqZip: String;
begin
  LaAgenda.Caption := 'Varedura ' + Geral.FF0(FVarredura) +
    '. Fazendo backup de ' + String(QrEmExecHost.Value) + '.' + String(QrEmExecDb.Value);
  //
  if (Trim(FPathMySQL) = '') then
  begin
    FPathMySQL := USQLDB.ObtemPathMySQLBin(TbBkAgenda.Database);
    FPathMySQL := dmkPF.ValidaDiretorio(FPathMySQL, True);
  end;
  Host  := String(QrEmExecHost.Value);
  Port  := QrEmExecPort.Value;
  User  := String(QrEmExecUser.Value);
  Senha := String(QrEmExecPASS_DECR.Value);
  Db    := String(QrEmExecDb.Value);
  Pasta := String(QrEmExecDestino.Value);
  Zipar := QrEmExecZipar.Value;

  if not FileExists(dmkPF.NomeLongoParaCurto(AnsiString(FPathMySQL)) + 'mysqldump.exe') then
  begin
    AtualizaMsg(Agora, AgoraUTC, 'O MySQLDump n�o foi localizado!', False);
    Exit;
  end;

  if not DirectoryExists(Pasta) then
  begin
    AtualizaMsg(Agora, AgoraUTC, 'O diret�rio destino n�o existe ou est� inacess�vel!', False);
    Exit;
  end;

  if (Host <> '') and (Port > 0) and (User <> '') and (Senha <> '') and
    (Db <> '') and (Pasta <> '') then
  begin
    try
      UnDmkDAC_PF.ConectaMyDB_DAC(Dmod.DBAgendaAtu, Db, Host, Port, User,
        Senha, /*Desconecta*/True, /*Configura*/True, /*Conecta*/True);
    except
      AtualizaMsg(Agora, AgoraUTC, 'ERRO: Falha ao conectar no banco de dados!', False);
      Exit;
    end;
    //
    Db  := '--databases ' + Db;
    Arq := USQLDB.Backup_CriaNomeArquivo(String(QrEmExecDb.Value), Agora);
    Arq := USQLDB.Backup_ConfiguraDestinoArquivo(Pasta, Arq, True);
    Cmd := String(dmkPF.NomeLongoParaCurto(AnsiString(FPathMySQL))) + 'mysqldump ' +
    '--max_allowed_packet=' + max_allowed_packet + ' ' +
    '--net_buffer_length=' + net_buffer_length + ' ' +
    '--default-character-set=' + default_character_set + ' ' +
    //LaCharSet.Caption + EdCharSet.Text +
    //' --debug ' + > n�o funciona
    //CriaDB + XML +
    //' -h localhost -u root -pwkljweryhvbirt  -B '+CBDB.Text + ' > '+
    //' -h ' + Host + ' -u ' + User + ' -p' + Senha + ' '+ Db + ' > '+ Arq;
    ' -P' + Geral.FF0(Port)
    + ' -h ' + Host + ' -u ' + User + ' -p' + Senha + ' '+ Db + ' > '+ Arq;
    //
    //
    //Geral.MB_Aviso(Cmd);
    {}
    MyObjects.ExecutaCmd(Cmd, MeResult);
    //
    LaAgenda.Caption := 'Atualizando dados de backups...';
    //
    TbBkAgenda.Close;
    TbBkAgenda.Open;
    if FileExists(Arq) then
    begin
      //Zipa o Arquivo
      {
      N�o Usa Mais
      if MyObjects.CompactaArquivo(ExtractFileName(Arq), Pasta, Pasta, MeResult,
      True, EdZipPath.Text) then
        LaAgenda.Caption := Geral.NowTxt() + 'Arquivo zipado: ' + Arq;
      }
      if Zipar = 1 then
      begin
        LaAgenda.Caption := 'Compactando arquivo de backup...';
        //
        Application.CreateForm(TFmZForge, FmZForge);
        ArqZip := FmZForge.ZipaArquivo(zftArquivo, ExtractFileDir(Arq), Arq,
          StringReplace(ExtractFileName(Arq), ExtractFileExt(Arq),
          '', [rfReplaceAll, rfIgnoreCase]), '', False, False);
        FmZForge.Destroy;
        if Length(ArqZip) > 0 then
        begin
          LaAgenda.Caption := Geral.NowTxt() + 'Arquivo zipado: ' + Arq;
          //
          if not DeleteFile(Arq) then
            LaAgenda.Caption := 'Falha ao remover aquivo SQL!'
          else
            LaAgenda.Caption := 'Arquivo SQL removido com sucesso!';
          //
          AtualizaMsg(Agora, AgoraUTC, 'Backup do banco de dados ' + String(QrEmExecDb.Value) + ' finalizado!', True);
        end else
        begin
          LaAgenda.Caption := Geral.NowTxt() + 'Falha ao zipar arquivo: ' + Arq;
          AtualizaMsg(Agora, AgoraUTC, 'Backup realizado mas houve falha ao zipar arquivo!', True);
        end;
      end else
        AtualizaMsg(Agora, AgoraUTC, 'Backup do banco de dados ' + String(QrEmExecDb.Value) + ' finalizado!', True);
      //
      LaAgenda.Caption := 'Backup do banco de dados ' + String(QrEmExecDb.Value) + ' finalizado!';
    end else
      AtualizaMsg(Agora, AgoraUTC, 'Falha ao realizar o backup!', False);
  end else
    AtualizaMsg(Agora, AgoraUTC, 'Dados insuficientes para realizar o backup!', False);
end;
*)

procedure TFmWetBlueMLA_BK.FazerBackupAutomatico2(Agora, AgoraUTC: TDateTime);
const
  CO_Tipo = 0; //Backup
var
  Port, Zipar: Integer;
  Host, Db, User, Senha, Pasta, Msg, ArqNome, ArqBkp, ArqZip: String;
begin
  LaAgenda.Caption := 'Varedura ' + Geral.FF0(FVarredura) + '. Fazendo backup de ' +
                        String(QrEmExecHost.Value) + '.' + String(QrEmExecDb.Value);
  //
  FPathMySQL := USQLDB.ObtemPathMySQLBin(TbBkAgenda.Database);
  FPathMySQL := dmkPF.ValidaDiretorio(FPathMySQL, True);
  //
  Host    := String(QrEmExecHost.Value);
  Port    := QrEmExecPort.Value;
  User    := String(QrEmExecUser.Value);
  Senha   := String(QrEmExecPASS_DECR.Value);
  Db      := String(QrEmExecDb.Value);
  Pasta   := String(QrEmExecDestino.Value);
  Zipar   := QrEmExecZipar.Value;
  ArqNome := USQLDB.Backup_CriaNomeArquivo(Db, Agora);
  Msg     := '';
  //
  ArqBkp := USQLDB.Backup_Executa(Dmod.DBAgendaAtu, FPathMySQL, Pasta, ArqNome,
              Host, Db, User, Senha, Port, MeResult, False, False,
              ndctDropAndCreate, Msg, LaAgenda);
  //
  if ArqBkp <> '' then
  begin
    //Zipa o Arquivo
    if Zipar = 1 then
    begin
      LaAgenda.Caption := 'Compactando arquivo de backup...';
      //
      Application.CreateForm(TFmZForge, FmZForge);
      ArqZip := FmZForge.ZipaArquivo(zftArquivo, ExtractFileDir(ArqBkp), ArqBkp,
        StringReplace(ExtractFileName(ArqBkp), ExtractFileExt(ArqBkp),
        '', [rfReplaceAll, rfIgnoreCase]), '', False, False);
      FmZForge.Destroy;
      //
      if Length(ArqZip) > 0 then
      begin
        LaAgenda.Caption := Geral.NowTxt() + 'Arquivo zipado: ' + ArqBkp;
        //
        if not DeleteFile(ArqBkp) then
          LaAgenda.Caption := 'Falha ao remover aquivo SQL!'
        else
          LaAgenda.Caption := 'Arquivo SQL removido com sucesso!';
        //
        AtualizaMsg([Dmod.DBAgendaAtu], CO_Tipo, Agora, AgoraUTC, 'Backup do banco de dados ' + String(QrEmExecDb.Value) + ' finalizado!', True);
        //
        LaAgenda.Caption := 'Backup do banco de dados ' + String(QrEmExecDb.Value) + ' finalizado!';
      end else
      begin
        LaAgenda.Caption := Geral.NowTxt() + 'Falha ao zipar arquivo: ' + ArqBkp;
        AtualizaMsg([Dmod.DBAgendaAtu], CO_Tipo, Agora, AgoraUTC, 'Backup realizado mas houve falha ao zipar arquivo!', True);
      end;
    end else
      AtualizaMsg([Dmod.DBAgendaAtu], CO_Tipo, Agora, AgoraUTC, Msg, True);
  end else
  begin
    if Msg <> '' then
      AtualizaMsg([Dmod.DBAgendaAtu], CO_Tipo, Agora, AgoraUTC, Msg, False);
  end;
end;

procedure TFmWetBlueMLA_BK.FazerSincronismoAutomatico(Agora, AgoraUTC: TDateTime;
  QryEmExec: TmySQLQuery);
const
  CO_Tipo = 1; //Sincronismo
  CO_NomeOri = 'local_web.sql';
  CO_NomeDes = 'web_local.sql';
var
  DBSql, DBOri, DBDes: TmySQLDataBase;
  QryAux, QryAux2, QryDB: TmySQLQuery;
  Ori_DBNome, Ori_DB, Ori_Host, Ori_User, Ori_Pass, Des_DB, Des_Host, Des_User,
  Des_Pass, Tabelas, Tabs, Destino, Msg: String;
  Ori_Porta, Des_Porta, Empresa: Integer;
  LocTabsSincro, Erro: Boolean;
begin
  Erro  := False;
  DBSql := TmySQLDataBase.Create(Self);
  DBOri := TmySQLDataBase.Create(Self);
  DBDes := TmySQLDataBase.Create(Self);
  try
    Ori_Host   := QryEmExec.FieldByName('Host').AsString;
    Ori_DBNome := QryEmExec.FieldByName('Db').AsString;
    Ori_User   := QryEmExec.FieldByName('User').AsString;
    Ori_Pass   := QryEmExec.FieldByName('PASS_DECR').AsString;
    Ori_Porta  := QryEmExec.FieldByName('Port').AsInteger;
    //
    if (Trim(FPathMySQL) = '') then
    begin
      FPathMySQL := USQLDB.ObtemPathMySQLBin(TbBkAgenda.Database);
      FPathMySQL := dmkPF.ValidaDiretorio(FPathMySQL, True);
    end;
    //
    Destino := CO_DIR_RAIZ_DMK + '\Sincro';
    //
    if (Ori_Host <> '') and (Ori_DBNome <> '') and (Ori_User <> '') and
      (Ori_Pass <> '') and (Ori_Porta <> 0) then
    begin
      QryDB   := TmySQLQuery.Create(DBSql.Owner);
      QryAux  := TmySQLQuery.Create(DBSql.Owner);
      QryAux2 := TmySQLQuery.Create(DBSql.Owner);
      try
        try
          UnDmkDAC_PF.ConectaMyDB_DAC(DBSql, Ori_DBNome, Ori_Host,
            Ori_Porta,  Ori_User, Ori_Pass, True, True, True);
        except
          LaAgenda.Caption := 'ERRO: Falha ao conectar no banco de dados!';
          AtualizaMsg([DBOri, DBDes], CO_Tipo, Agora, AgoraUTC, 'ERRO: Falha ao conectar no banco de dados!', False);
          Exit;
        end;
        //
        UnDmkDAC_PF.AbreMySQLQuery0(QryDB, DBSql, [
          'SELECT Empresa, Web_Host, Web_Porta, Web_User, ',
          'Web_Pwd, Web_DB, AES_DECRYPT(Web_Pwd, "'+ CO_RandStrWeb01 +'") PASS_DECR ',
          'FROM web_params ',
          'WHERE Web_MySQL <> 0 ',
          '']);
        if QryDB.RecordCount > 0 then
        begin
          while not QryDB.EOF do
          begin
            Des_Host  := QryDB.FieldByName('Web_Host').AsString;
            Des_DB    := QryDB.FieldByName('Web_DB').AsString;
            Des_User  := QryDB.FieldByName('Web_User').AsString;
            Des_Pass  := QryDB.FieldByName('PASS_DECR').AsString;
            Des_Porta := QryDB.FieldByName('Web_Porta').AsInteger;
            Empresa   := QryDB.FieldByName('Empresa').AsInteger;
            //
            LocTabsSincro := False;
            //
            //Executa sincro local p/ web
            UnDmkDAC_PF.AbreMySQLQuery0(QryAux, DBSql, [
              'SELECT DISTINCT(BancoDados) BancoDados ',
              'FROM ' + CO_SINCRO_TAB,
              'WHERE Origem = 0 ',
              '']);
            //
            if QryAux.RecordCount > 0 then
            begin
              while not QryAux.EOF do
              begin
                Ori_DB := QryAux.FieldByName('BancoDados').AsString;
                //
                UnDmkDAC_PF.AbreMySQLQuery0(QryAux2, DBSql, [
                  'SELECT * ',
                  'FROM ' + CO_SINCRO_TAB,
                  'WHERE Origem = 0 ',
                  'AND BancoDados = "' + Ori_DB + '" ',
                  'ORDER BY Tabela ASC ',
                  '']);
                if QryAux2.RecordCount > 0 then
                begin
                  Tabs    := '';
                  Tabelas := '';
                  //
                  while not QryAux2.EOF do
                  begin
                    Tabelas := Tabelas + QryAux2.FieldByName('Tabela').AsString;
                    //
                    if QryAux2.RecNo <> QryAux2.RecordCount then
                      Tabelas := Tabelas + ' ';
                    //
                    QryAux2.Next;
                  end;
                  if Ori_DB = '' then
                    Ori_DB := Ori_DBNome;
                  //
                  Tabs := ' --tables ' + Tabelas;
                  //
                end;
                //
                if not dmkPF.ExecutavelEstaRodando('mysqldump.exe') then //O MySQLDump n�o pode estar rodando
                begin
                  if not USQLDB.Sincro_ExecutaUnidirecional(DBOri, DBDes,
                    Empresa, FPathMySQL, Destino, CO_NomeOri, Ori_Host, Ori_DB,
                    Ori_User, Ori_Pass, Ori_Porta, Des_Host, Des_DB, Des_User,
                    Des_Pass, Des_Porta, Tabs, True, AgoraUTC, MeResult, Msg, LaAgenda) then
                  begin
                    Erro             := True;
                    LaAgenda.Caption := Msg;
                    AtualizaMsg([DBOri, DBDes], CO_Tipo, Agora, AgoraUTC, Msg, False);
                    Exit;
                  end else
                  begin
                    LaAgenda.Caption := Msg;
                    AtualizaMsg([DBOri, DBDes], CO_Tipo, Agora, AgoraUTC, Msg, True);
                  end;
                end;
                //
                QryAux.Next;
              end;
              LocTabsSincro := True;
            end;
            //Executa sincro web p/ local
            UnDmkDAC_PF.AbreMySQLQuery0(QryAux, DBSql, [
              'SELECT DISTINCT(BancoDados) BancoDados ',
              'FROM ' + CO_SINCRO_TAB,
              'WHERE Origem = 1 ',
              '']);
            if QryAux.RecordCount > 0 then
            begin
              while not QryAux.EOF do
              begin
                Ori_DB := QryAux.FieldByName('BancoDados').AsString;
                //
                UnDmkDAC_PF.AbreMySQLQuery0(QryAux2, DBSql, [
                  'SELECT * ',
                  'FROM ' + CO_SINCRO_TAB,
                  'WHERE Origem = 1 ',
                  'AND BancoDados = "' + Ori_DB + '" ',
                  'ORDER BY Tabela ASC ',
                  '']);
                if QryAux2.RecordCount > 0 then
                begin
                  Tabs    := '';
                  Tabelas := '';
                  //
                  while not QryAux2.EOF do
                  begin
                    Tabelas := Tabelas + QryAux2.FieldByName('Tabela').AsString;
                    //
                    if QryAux2.RecNo <> QryAux2.RecordCount then
                      Tabelas := Tabelas + ' ';
                    //
                    QryAux2.Next;
                  end;
                  if Ori_DB = '' then
                    Ori_DB := Ori_DBNome;
                  //
                  Tabs := ' --tables ' + Tabelas;
                  //
                end;
                //
                if not USQLDB.Sincro_ExecutaUnidirecional(DBDes, DBOri, Empresa,
                  FPathMySQL, Destino, CO_NomeDes, Des_Host, Des_DB, Des_User,
                  Des_Pass, Des_Porta, Ori_Host, Ori_DB, Ori_User, Ori_Pass,
                  Ori_Porta, Tabs, True, AgoraUTC, MeResult, Msg, LaAgenda) then
                begin
                  LaAgenda.Caption := Msg;
                  AtualizaMsg([DBOri, DBDes], CO_Tipo, Agora, AgoraUTC, Msg, False);
                  Exit;
                end else
                begin
                  LaAgenda.Caption := Msg;
                  AtualizaMsg([DBOri, DBDes], CO_Tipo, Agora, AgoraUTC, Msg, True);
                end;
                //
                QryAux.Next;
              end;
              LocTabsSincro := True;
            end;
            //
            if not LocTabsSincro then
            begin
              LaAgenda.Caption := 'N�o foi localizada nenhuma tabela para realizar o sincronismo!';
              AtualizaMsg([DBOri, DBDes], CO_Tipo, Agora, AgoraUTC, 'N�o foi localizada nenhuma tabela para realizar o sincronismo!', False);
            end else
            begin
              USQLDB.Sincro_AtualizaDados(DBOri, AgoraUTC, Empresa, True, True);
              USQLDB.Sincro_AtualizaDados(DBDes, AgoraUTC, Empresa, True, True);
            end;
            //
            QryDB.Next;
          end;
        end else
        begin
          LaAgenda.Caption := 'Dados insuficientes para realizar o sincronismo!';
          AtualizaMsg([DBOri, DBDes], CO_Tipo, Agora, AgoraUTC, 'Dados insuficientes para realizar o sincronismo!', False);
          Exit;
        end;
      finally
        QryDB.Free;
        QryAux.Free;
        QryAux2.Free;
      end;
    end;
  finally
    DBSql.Free;
    DBOri.Free;
    DBDes.Free;
  end;
end;

function TFmWetBlueMLA_BK.ObtemDataStr(Date: TDateTime): String;
var
  AA, MM, DD, HH, NN, SS: String;
  Ano, Mes, Dia, Hora, Min, Seg, MSeg : Word;
begin
  DecodeDate(Date, Ano, Mes, Dia);
  AA := Geral.FF0(Ano);
  if Length(AA) > 2 then AA := AA[Length(AA)-1]+AA[Length(AA)];
  if Mes < 10 then MM := '0'+ Geral.FF0(Mes) else MM := Geral.FF0(Mes);
  // Estraga ordem alfabetica
  //MM := Copy(MLAGeral.VerificaMes(Mes, True), 1, 3);
  if Dia < 10 then DD := '0'+ Geral.FF0(Dia) else DD := Geral.FF0(Dia);
  DecodeTime(Now(), Hora, Min, Seg, MSeg);
  if Hora < 10 then HH := '0'+ Geral.FF0(Hora) else HH := Geral.FF0(Hora);
  if Min < 10 then NN := '0'+ Geral.FF0(Min) else NN := Geral.FF0(Min);
  if Seg < 10 then SS := '0'+ Geral.FF0(Seg) else SS := Geral.FF0(Seg);
  Result := AA+MM+DD+'_'+HH+NN+SS;
end;

{
Substitu�da pelo ZForge para n�o precisar instalar o WinZip
procedure TFmWetBlueMLA_BK.CompactaArquivo(Arquivo, Origem, Destino: String;
 Exclui: Integer);
var
  ExecDOS : PChar;
  i, Verificacoes: Integer;
  ZipPath, Options: String;
begin
  if CkWinZip.Checked then
  begin
    try
      // Zipa arquivos criados no dir A: e C:
      if Destino[1] = 'A' then Options := Options + '&';
      //if CkExclui.Checked then Options := Options + 'w';
      if Options <> '' then Options := ' -' + Options;
      Verificacoes := 500;
      /////////////////////////////////////////////////////////////////////////////
      // -yp deixa DOS Aberto
      // -& (Multiplos discos)
      /////////////////////////////////////////////////////////////////////////////
      ZipPath := Geral.ReadAppKeyCU('ZipPath', 'Dermatek', ktString, 'C:/Dermatek');
      if not FileExists(ZipPath+'\wzzip.exe') then
      begin
        Memo1.Lines.Add('O arquivo "'+ZipPath+'\wzzip.exe" n�o foi encontrado!');
        raise EAbort.Create('');
      end else begin
        ZipPath := dmkPF.NomeLongoparaCurto(ZipPath);
        ExecDOS := PChar(PChar(ZipPath+'\wzzip'+Options+' '+
          ExtractShortPathName(Destino)+'\'+Arquivo+' '+Origem));
        Memo2.Lines.Add('*** Linha de comando: ***');
        Memo2.Lines.Add(ExecDOS);
        Memo2.Lines.Add('*** Fim linha de comando ***');
        Memo2.Lines.Add('');
        try
          //WinExec(ExecDOS, SW_HIDE);
    //function WinExecAndWait32(LinhaDeComando: string; Visibility: Integer;
    //         Memo: TMemo): Longword;
          dmkPF.WinExecAndWaitOrNot(ExecDOS, SW_HIDE, nil, True);
          //MLAGeral.WinExecAndWait32(ExecDOS, SW_HIDE, nil);
          // Erro!!!!!!!!!!!
          //MLAGeral.WinExecAndWait32(ExecDOS, SW_HIDE, Memo2);
          //MLAGeral.LeSaidaDOS(ExecDOS, Memo2);
        except
          Memo2.Lines.Add('*** Compacta��o falhou! ***');
        end;
      end;
      for i := 1 to Verificacoes do
      begin
        StatusBar1.Panels[1].Text := StatusBar1.Panels[1].Text + '.';
        Sleep(1000);
        if FileExists(Destino+'\'+Arquivo) then
        begin
          WMLA_ZPCount := WMLA_ZPCount + 1;
          Break;
        end;
      end;
  (*    ExecDOS := PChar(CO_DIR_RAIZ_DMK + '\wzzip ' + CO_DIR_RAIZ_DMK + '\Backups\'+ArqNome+
                       ' ' + CO_DIR_RAIZ_DMK + '\' + Arquivo);
      WinExec(ExecDOS, SW_HIDE);*)
      /////////////////////////////////////////////////////////////////////////////
    except
      Memo1.Lines.Add('Erro na compacta��o com Win Zip!');
    end;
  end else begin
    CopyFile(PChar(Origem), PChar(Destino+Arquivo+'.sql'), True);
  end;
end;
}

procedure TFmWetBlueMLA_BK.DBGrid2DblClick(Sender: TObject);
var
  Col: Integer;
  Campo: String;
begin
  Col   := THackDBGrid(Sender).Col -1;
  Campo := UpperCase(TDBGrid(Sender).Columns[Col].FieldName);
  //
  if (TbBkAgenda.State <> dsInactive) and (TbBkAgenda.RecordCount > 0) then
  begin
    if Campo = 'DESTINO' then
    begin
      if Geral.MB_Pergunta('Deseja abrir o diret�rio destino?') = ID_YES  then
        Geral.AbreArquivo(TbBkAgendaDestino.Value);
    end else
    begin
      edHost.ValueVariant  := TbBkAgendaHost.Value;
      edPorta.ValueVariant := TbBkAgendaPort.Value;
      edDb.ValueVariant    := TbBkAgendaDb.Value;
      EdLogin.ValueVariant := TbBkAgendaUser.Value;
      edPswd.Text          := String(TbBkAgendaPASS_DECR.Value);
    end;
  end;
end;

procedure TFmWetBlueMLA_BK.DBGrid2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if Column.FieldName = 'Ativo' then
    MeuVCLSkin.DrawGrid(DBGrid2, Rect, 1, TbBkAgendaAtivo.Value);
end;

function TFmWetBlueMLA_BK.DecryptSenha(Codigo: Integer): AnsiString;
begin
  Result := '';
  QrDmk.Close;
  QrDmk.SQL.Clear;
  QrDmk.SQL.Add('SELECT AES_DECRYPT(Pass, "'+ CO_RandStrWeb01 +'") SENHA');
  QrDmk.SQL.Add('FROM bkagenda');
  QrDmk.SQL.Add('WHERE Codigo=:P0');
  QrDmk.Params[0].AsInteger := Codigo;
  QrDmk.Open;
  //
  Result := QrDmk.FieldByName('SENHA').AsAnsiString;
end;

procedure TFmWetBlueMLA_BK.edPortaChange(Sender: TObject);
begin
  BtDesisteClick(Self);
end;

procedure TFmWetBlueMLA_BK.edPortaExit(Sender: TObject);
begin
  Geral.WriteAppKeyCU('Porta_Backup', Application.Title, edPorta.Text, ktInteger);
end;

procedure TFmWetBlueMLA_BK.TrayIcon1Click(Sender: TObject);
begin
  FmWetBlueMLA_BK.Visible := True;
end;

procedure TFmWetBlueMLA_BK.BitBtn1Click(Sender: TObject);
var
  i: Integer;
begin
  Randomize;
  Screen.Cursor := crHourGlass;
  try
    QrOpt.Close;
    QrOpt.SQL.Clear;
    QrOpt.SQL.Add('DROP DATABASE IF EXISTS MeuTeste');
    QrOpt.ExecSQL;
    //
    QrOpt.Close;
    QrOpt.SQL.Clear;
    QrOpt.SQL.Add('CREATE DATABASE MeuTeste');
    QrOpt.ExecSQL;
    //
    (*QrDatabases.Close;
    QrDatabases.Open;
    while not QrDatabases.Eof do
    begin
      if Uppercase(QrDatabases.Fields[0].AsString) = 'MEUTESTE' then Break;
      QrDatabases.Next;
    end;*)
    (*
    Dmod.MyDB.Connected := False;
    Dmod.MyDB.DatabaseName := 'MeuTeste';
    Dmod.MyDB.Connected := True;
    *)
    UnDmkDAC_PF.ConectaMyDB_DAC(Dmod.MyDB, 'MeuTeste', edHost.ValueVariant,
      edPorta.ValueVariant,  EdLogin.ValueVariant, FSenha, (*Desconecta*)True,
      (*Configura*)True, (*Conecta*)True);
    //
    QrOpt.Close;
    QrOpt.SQL.Clear;
    QrOpt.SQL.Add('CREATE TABLE Teste (');
    QrOpt.SQL.Add('  ID    int(11),   ');
    QrOpt.SQL.Add('  Txt   varchar(255)');
    QrOpt.SQL.Add('  ) TYPE=MyISAM');
    QrOpt.ExecSQL;
    //
    QrOpt.Close;
    QrOpt.SQL.Clear;
    QrOpt.SQL.Add('INSERT INTO teste SET ID=:P0, Txt=:P1');
    for i := 1 to 10000 do
    begin
      QrOpt.Params[0].AsInteger := i;
      QrOpt.Params[1].AsString  := GetRandomString;
      QrOpt.ExecSQL;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmWetBlueMLA_BK.NOexecutarnainicializao1Click(Sender: TObject);
begin
  ExecutaNaInicializacao(False, CO_Titulo, Application.ExeName);
end;

procedure TFmWetBlueMLA_BK.Normal1Click(Sender: TObject);
{
var
  Database: String;
}
begin
{
  FFazendoBackup := True;
  if LBDatabase.ItemIndex > -1 then
    DataBase := LBDatabase.Items[LBDatabase.ItemIndex];
  if Database = '' then //ShowMessage('Database n�o defenido');
  begin
    Application.MessageBox('Database n�o definido ou inexistente! O aplicativo ser� encerrado',
    'Erro', MB_OK+MB_ICONERROR);
    Application.Terminate;
    Exit;
  end;
  TMeuDB := DataBase;
  TMeuDB := DataBase;
  VAR_MYSQLUSER := 'root';
  //
  VAR_CURSOR := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  VAR_IPBACKUP := edHost.Text;
  try
    Application.CreateForm(TFmDumptab, FmDumptab);
  finally
    Screen.Cursor := VAR_CURSOR;
  end;
  FmDumptab.ShowModal;
  FmDumptab.Destroy;
  FFazendoBackup := False;
}
end;

procedure TFmWetBlueMLA_BK.AtualizaTabela;

  procedure AtualizaTabela(Campo, Coluna: String);
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrDmk, DBAgenda, [
      'SHOW FIELDS FROM bkagenda LIKE "' + Campo + '"',
      '']);
    if QrDmk.RecordCount = 0 then
    begin
      UnDmkDAC_PF.ExecutaMySQLQuery0(QrDmk, DBAgenda, [
        'ALTER TABLE bkagenda',
        'ADD COLUMN ' + Coluna,
        '']);
    end;
  end;

begin
  //Usu�rio
  AtualizaTabela('User', 'User varchar(50)');
  //Senha
  AtualizaTabela('Pass', 'Pass varchar(50)');
  //Porta
  AtualizaTabela('Port', 'Port int(11) NOT NULL DEFAULT 3306');
  //Dias
  AtualizaTabela('Dias', 'Dias int(11) NOT NULL DEFAULT 10');
  //Zipar
  AtualizaTabela('Zipar', 'Zipar tinyint(1) NOT NULL DEFAULT 1');
  //Tipo
  AtualizaTabela('Tipo', 'Tipo tinyint(1) NOT NULL DEFAULT 0');
  //Ativo
  AtualizaTabela('Ativo', 'Ativo tinyint(1) NOT NULL DEFAULT 1');
  //
  QrDmk.Close;
end;

procedure TFmWetBlueMLA_BK.Beta1Click(Sender: TObject);
begin
  {
  Application.CreateForm(TFmBackup2, FmBackup2);
  FmBackup2.EdSenha.Text := FSenha;
  FmBackup2.ShowModal;
  FmBackup2.Destroy;
  }
end;

procedure TFmWetBlueMLA_BK.ExecutaNaInicializacao(Executa: Boolean; Titulo, Programa: String);
var
  Registry: TRegistry;
begin
  Registry := TRegistry.Create;
  try
    Registry.RootKey := HKEY_CURRENT_USER;
    Registry.OpenKey('\Software\Microsoft\Windows\CurrentVersion\Run', False);
    if Executa then
      Registry.WriteString(Titulo, Programa)
    else
      Registry.WriteString(Titulo, '');
  finally
    Registry.Free;
  end;
end;

procedure TFmWetBlueMLA_BK.Executarnainicializao1Click(Sender: TObject);
begin
  ExecutaNaInicializacao(True, CO_Titulo, Application.ExeName);
end;

function TFmWetBlueMLA_BK.ValidaAceite(): Boolean;
var
  HashArq: String;
  Termo, Usuario: Integer;
  DtaHora: TDateTime;
begin
(* 20230320
  Result  := False;
  HashArq := Geral.ReadAppKeyLM('Contrato_Hash', Application.Title, ktString, '');
  Termo   := Geral.ReadAppKeyLM('Contrato_Termo', Application.Title, ktInteger, 0);
  //
  if (Termo <> CO_CONTRATO_ID) or (HashArq <> CO_CONTRATO_HASH_SH1) then
  begin
    Geral.WriteAppKeyCU('Contrato_Hash', Application.Title, '', ktString);
    Geral.WriteAppKeyCU('Contrato_Termo', Application.Title, 0, ktInteger);
    //
    if UWAceites.AceitaTermos2(CO_CONTRATO_ID, Termo, Usuario, HashArq, DtaHora) then
    begin
      Geral.WriteAppKeyLM('Contrato_Hash', Application.Title, HashArq, ktString);
      Geral.WriteAppKeyLM('Contrato_Termo', Application.Title, Termo, ktInteger);
      Geral.WriteAppKeyLM('Contrato_Aceite', Application.Title, Geral.FDT(DtaHora, 11), ktString);
      //
      Close;
    end else
      Close;
  end else
*)
    Result := True;
end;

function TFmWetBlueMLA_BK.VerificaNovasVersoes(
  ApenasVerifica: Boolean): Boolean;
var
  Versao: Integer;
  ArqNome: String;
begin
(* 20230320
  Result := DmkWeb.VerificaAtualizacaoVersao2(True, True, 'DermaBK', 'DermaBK',
              '', CO_VERSAO, CO_DMKID_APP, Now, Memo1, dtExecAux, Versao,
              ArqNome, False, ApenasVerifica, BalloonHint1);
*)
end;

procedure TFmWetBlueMLA_BK.ConfiguraNotificacao(DataBase: TmySQLDatabase;
  Tipo: Integer; AgoraUTC: TDateTime; Texto: String);
var
  TipoNotifi: TNotifi;
begin
  if DataBase.Connected = True then
  begin
    if USQLDB.TabelaExiste('notifi', DataBase) then
    begin
      if Tipo = 0 then
        TipoNotifi := nftBackupAuto //Backup
      else
        TipoNotifi := nftSincronismoAuto; //Sincronismo
      //
      UnNotificacoesEdit.CriaNotificacaoPublica(DataBase, TipoNotifi, AgoraUTC, Texto);
    end;
  end;
end;

procedure TFmWetBlueMLA_BK.AtualizaMsg(DBs: array of TmySQLDatabase;
  Tipo: Integer; Agora, AgoraUTC: TDateTime; Msg: String; SemErros: Boolean);
var
  Codigo: Integer;
  Qry: TmySQLQuery;
  AgoraTxt, Campo: String;
  I: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    Codigo   := QrEmExecCodigo.Value;
    AgoraTxt := FormatDateTime('yyyy-mm-dd hh:nn:ss', Agora);
    //
    if SemErros = True then
    begin
      Campo := 'UltExe';
      //
      if Tipo = 0 then //Backup
      begin
        USQLDB.Backups_ExcluiAntigos(QrEmExecUltExe.Value, QrEmExecDias.Value,
          QrEmExecDestino.Value, QrEmExecDb.Value);
      end;
      //
      if TimerBkExec.Enabled = False then
        TimerBkExec.Interval := CO_TimerBkExec_Interval;
    end else
    begin
      Campo := 'Ulterr';
      //
      if TimerBkExec.Enabled = False then
        TimerBkExec.Interval := TimerBkExec.Interval + TimerBkExec.Interval;
    end;
    for I := Low(DBs) to High(DBs) do
      ConfiguraNotificacao(DBs[i], Tipo, AgoraUTC, Msg);
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(Qry, DBAgenda, [
      'UPDATE bkagenda SET Aviso="' + Msg + '", ',
      'UltTry="' + AgoraTxt + '", ',
      Campo + '="' + AgoraTxt + '" ',
      'WHERE Codigo=' + Geral.FF0(Codigo),
      '']);
  finally
    TbBkAgenda.Close;
    TbBkAgenda.Open;
    //
    Qry.Free;
  end;
end;

end.

