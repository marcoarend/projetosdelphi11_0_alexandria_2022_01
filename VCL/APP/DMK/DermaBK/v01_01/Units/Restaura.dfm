object FmRestaura: TFmRestaura
  Left = 342
  Top = 183
  Caption = 'FER-BACKP-101 :: Restaura Banco de Dados (1)'
  ClientHeight = 678
  ClientWidth = 1003
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 47
    Width = 1003
    Height = 450
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 780
    ExplicitHeight = 434
    object Panel6: TPanel
      Left = 0
      Top = 0
      Width = 1003
      Height = 450
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 780
      ExplicitHeight = 434
      object Splitter1: TSplitter
        Left = 0
        Top = 354
        Width = 1003
        Height = 4
        Cursor = crVSplit
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        ExplicitWidth = 975
      end
      object Panel8: TPanel
        Left = 0
        Top = 0
        Width = 1003
        Height = 172
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitWidth = 780
        object Label1: TLabel
          Left = 118
          Top = 5
          Width = 118
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Arquivo a ser restaurado:'
        end
        object EdBackFile: TdmkEdit
          Left = 117
          Top = 25
          Width = 839
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnChange = EdBackFileChange
        end
        object BtAbrir: TBitBtn
          Tag = 25
          Left = 4
          Top = 5
          Width = 110
          Height = 49
          Hint = 'Confirma a senha digitada'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Arquivo'
          NumGlyphs = 2
          TabOrder = 1
          OnClick = BtAbrirClick
        end
        object MeBase: TMemo
          Left = 0
          Top = 70
          Width = 1003
          Height = 102
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alBottom
          Lines.Strings = (
            
              '//C:\ARQUIV~1\MySQL\MYSQLS~1.1\bin\mysqldump --max_allowed_packe' +
              't=2097152 '
            
              '--net_buffer_length=65536 -h localhost -u root -pwkljweryhvbirt ' +
              ' -B seven > '
            'C:\Seven0606121728.sql'
            '')
          TabOrder = 2
          Visible = False
          ExplicitWidth = 780
        end
      end
      object Memo1: TMemo
        Left = 0
        Top = 172
        Width = 1003
        Height = 182
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        TabOrder = 1
        ExplicitWidth = 780
      end
      object Memo2: TMemo
        Left = 0
        Top = 358
        Width = 1003
        Height = 0
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 2
        ExplicitWidth = 780
      end
      object Memo3: TMemo
        Left = 0
        Top = 212
        Width = 1003
        Height = 238
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alBottom
        OEMConvert = True
        TabOrder = 3
        ExplicitTop = 196
        ExplicitWidth = 780
      end
    end
  end
  object Panel7: TPanel
    Left = 0
    Top = 497
    Width = 1003
    Height = 47
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitTop = 481
    ExplicitWidth = 780
    object LaStatus: TStaticText
      Left = 0
      Top = 0
      Width = 1003
      Height = 47
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Caption = 'Pronto.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clActiveCaption
      Font.Height = -10
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      ExplicitWidth = 780
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1003
    Height = 47
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitWidth = 780
    object GB_R: TGroupBox
      Left = 944
      Top = 0
      Width = 59
      Height = 47
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 721
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 47
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 885
      Height = 47
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 662
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 425
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Restaura Banco de Dados (1)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 425
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Restaura Banco de Dados (1)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 425
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Restaura Banco de Dados (1)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 544
    Width = 1003
    Height = 56
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    ExplicitTop = 528
    ExplicitWidth = 780
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 999
      Height = 39
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 776
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Progress1: TProgressBar
        Left = 0
        Top = 18
        Width = 999
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alBottom
        TabOrder = 0
        ExplicitWidth = 776
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 600
    Width = 1003
    Height = 78
    Align = alBottom
    TabOrder = 4
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 999
      Height = 61
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 776
      ExplicitHeight = 46
      object PnSaiDesis: TPanel
        Left = 822
        Top = 0
        Width = 177
        Height = 61
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitLeft = 599
        ExplicitHeight = 46
        object BtSaida: TBitBtn
          Tag = 13
          Left = 18
          Top = 5
          Width = 148
          Height = 49
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtDireto: TBitBtn
        Tag = 10037
        Left = 8
        Top = 5
        Width = 148
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Restaura'
        Enabled = False
        TabOrder = 1
        OnClick = BtDiretoClick
      end
      object CkMostra: TCheckBox
        Left = 167
        Top = 20
        Width = 164
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Mostra declara'#231#245'es.'
        Checked = True
        Enabled = False
        State = cbChecked
        TabOrder = 2
      end
      object BtParar: TBitBtn
        Tag = 126
        Left = 320
        Top = 5
        Width = 148
        Height = 49
        Hint = 'Confirma a senha digitada'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Parar'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BtPararClick
      end
      object BtNormal: TBitBtn
        Tag = 10037
        Left = 478
        Top = 5
        Width = 147
        Height = 49
        Hint = 'Confirma a senha digitada'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Normal'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 4
        Visible = False
        OnClick = BtNormalClick
      end
      object Button1: TButton
        Left = 635
        Top = 20
        Width = 92
        Height = 30
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'OK novo'
        TabOrder = 5
        Visible = False
        OnClick = Button1Click
      end
    end
  end
  object OpenDialog1: TOpenDialog
    Filter = 'Arquivos SQL|*.sql|Arquivos ZIP|*.zip'
    InitialDir = 'C:\Dermatek\Backups\Restore'
    Left = 101
    Top = 292
  end
  object BeRestore: TMySQLBatchExecute
    Action = baIgnore
    Database = DBMySQL
    Delimiter = ';'
    OnAfterExecute = BeRestoreAfterExecute
    OnAfterStatement = BeRestoreAfterStatement
    OnProcess = BeRestoreProcess
    Left = 73
    Top = 292
  end
  object QrDelDB: TMySQLQuery
    Database = DBMySQL
    SQL.Strings = (
      'DROP DATABASE IF EXISTS :P0'
      '')
    Left = 133
    Top = 321
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object DBMySQL: TMySQLDatabase
    ConnectOptions = []
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 133
    Top = 293
  end
end
