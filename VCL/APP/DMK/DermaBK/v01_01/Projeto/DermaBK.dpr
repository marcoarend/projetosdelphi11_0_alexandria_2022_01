program DermaBK;

uses
  Principal in '..\Units\Principal.pas' {FmPrincipal},
  Termino in '..\Units\Termino.pas' {Form1},
  Module in '..\Units\Module.pas' {DMod: TDataModule},
  MeusDBs in '..\Units\MeusDBs.pas' {FmMeusDBs},
  Restaura in '..\Units\Restaura.pas' {FmRestaura},
  MyListas in '..\Units\MyListas.pas',
  Horarios in '..\Units\Horarios.pas' {FmHorarios},
  Forms,
  UnInternalConsts in '..\..\..\..\..\UTL\_UNT\v01_01\UnInternalConsts.pas',
  UnInternalConsts2 in '..\..\..\..\..\UTL\_UNT\v01_01\UnInternalConsts2.pas',
  UnInternalConsts3 in '..\..\..\..\..\UTL\_UNT\v01_01\UnInternalConsts3.pas',
  MyGlyfs in '..\..\..\..\..\UTL\_FRM\v01_01\MyGlyfs.pas' {FmMyGlyfs},
  ResIntStrings in '..\..\..\..\..\UTL\_UNT\v01_01\ResIntStrings.pas',
  UnMLAGeral in '..\..\..\..\..\UTL\_UNT\v01_01\UnMLAGeral.pas',
  MyVCLSkin in '..\..\..\..\..\UTL\_UNT\v01_01\MyVCLSkin.pas',
  UnDmkSystem in '..\..\..\..\..\UTL\_UNT\v01_01\UnDmkSystem.pas',
  UnMsgInt in '..\..\..\..\..\UTL\_UNT\v01_01\UnMsgInt.pas',
  UnMyObjects in '..\..\..\..\..\UTL\_UNT\v01_01\UnMyObjects.pas',
  ZCF2 in '..\..\..\..\..\UTL\_UNT\v01_01\ZCF2.pas',
  PesqNome in '..\..\..\..\..\UTL\_FRM\v01_01\PesqNome.pas' {FmPesqNome},
  MeuFrx in '..\..\..\..\..\UTL\Print\v01_01\MeuFrx.pas' {FmMeuFrx},
  reinit in '..\..\..\..\..\UTL\Textos\v01_01\reinit.pas',
  Backup3 in '..\..\..\..\..\UTL\MySQL\v01_01\Backup3.pas' {FmBackup3},
  GModule in '..\..\..\..\..\UTL\MySQL\v01_01\GModule.pas' {GMod: TDataModule},
  UCreate in '..\..\..\..\..\UTL\MySQL\v01_01\UCreate.pas',
  SelRadioGroup in '..\..\..\..\..\UTL\_FRM\v01_01\SelRadioGroup.pas' {FmSelRadioGroup},
  dmkSOAP in '..\..\..\..\..\MDL\WEB\v01_01\WebService\dmkSOAP.pas',
  UnDmkProcFunc in '..\..\..\..\..\UTL\_UNT\v01_01\UnDmkProcFunc.pas',
  UCreateEDrop in '..\..\..\..\..\UTL\_UNT\v01_01\UCreateEDrop.pas',
  dmkDAC_PF in '..\..\..\..\..\UTL\MySQL\v01_01\dmkDAC_PF.pas',
  ZForge in '..\..\..\..\..\UTL\_FRM\v01_01\ZForge.pas' {FmZForge},
  UnDmkEnums in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnDmkEnums.pas',
  UnMyLinguas in '..\..\..\..\..\..\MultiOS\AllOS\Listas\UnMyLinguas.pas',
  dmkGeral in '..\..\..\..\..\..\dmkComp\dmkGeral.pas',
  UnitMD5 in '..\..\..\..\..\UTL\Encrypt\v01_01\UnitMD5.pas',
  ABOUT in '..\..\..\..\..\UTL\_FRM\v01_01\ABOUT.pas' {FmAbout},
  UnGrl_Consts in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Consts.pas',
  UnGrl_Vars in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Vars.pas',
  UnitNotificacoesEdit in '..\..\..\..\..\MDL\NOTI\v01_01\UnitNotificacoesEdit.pas',
  UnDmkListas in '..\..\..\..\..\..\MultiOS\AllOS\Listas\UnDmkListas.pas',
  CfgBackup in '..\Units\CfgBackup.pas' {FmCfgBackup},
  UnGrl_DmkDB in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_DmkDB.pas',
  UnGrl_Geral in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Geral.pas',
  UnDmkHTML2 in '..\..\..\..\..\UTL\WEB\HTML\v01_01\UnDmkHTML2.pas',
  WBFuncs in '..\..\..\..\..\UTL\WEB\HTML\v01_01\WBFuncs.pas',
  OAuth in '..\..\..\..\..\UTL\WEB\HTML\v01_01\OAuth.pas' {FmOAuth},
  UnGrlEnt in '..\..\..\..\..\..\MultiOS\MDL\ENT\v02_01\UnGrlEnt.pas',
  GetData in '..\..\..\..\..\UTL\_FRM\v01_01\GetData.pas' {FmGetData},
  Vcl.Themes,
  Vcl.Styles,
  UnTemp in '..\..\..\..\..\..\FMX\APP\BASE\v03_01\Projeto\UnTemp.pas',
  UnProjGroup_Consts in '..\Units\UnProjGroup_Consts.pas',
  GetDatasPeriodo in '..\..\..\..\..\UTL\_FRM\v01_01\GetDatasPeriodo.pas' {FmGetDatasPeriodo},
  Senha in '..\..\..\..\..\UTL\_FRM\v01_01\Senha.pas' {FmSenha},
  Sel2RadioGroups in '..\..\..\..\..\UTL\_FRM\v01_01\Sel2RadioGroups.pas' {FmSel2RadioGroups},
  Sel3RadioGroups in '..\..\..\..\..\UTL\_FRM\v01_01\Sel3RadioGroups.pas' {FmSel3RadioGroups},
  SelCheckGroup in '..\..\..\..\..\UTL\_FRM\v01_01\SelCheckGroup.pas' {FmSelCheckGroup},
  UMySQLDB in '..\..\..\..\..\UTL\MySQL\v01_01\UMySQLDB.pas',
  UnGrlTemp in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrlTemp.pas',
  WetBlueMLA_BK in '..\Units\WetBlueMLA_BK.pas' {FmWetBlueMLA_BK},
  SelListArr in '..\..\..\..\..\UTL\_FRM\v01_01\SelListArr.pas' {FmSelListArr};

{$R *.RES}

(*
  LinkRankSkin in '..\..\..\..\Outros\Geral\Forms\LinkRankSkin.pas' {FmLinkRankSkin},
*)
begin
  Application.Initialize;
  TStyleManager.TrySetStyle('Cyan Night');
  Application.Title := 'Backup Dermatek';
    if CO_VERMCW > CO_VERMLA then
      CO_VERSAO := CO_VERMCW
    else
      CO_VERSAO := CO_VERMLA;
  Application.CreateForm(TFmWetBlueMLA_BK, FmWetBlueMLA_BK);
  Application.CreateForm(TFmPrincipal, FmPrincipal);
  Application.CreateForm(TFmMyGlyfs, FmMyGlyfs);
  Application.CreateForm(TGMod, GMod);
  Application.CreateForm(TFmGetDatasPeriodo, FmGetDatasPeriodo);
  Application.CreateForm(TFmSenha, FmSenha);
  Application.CreateForm(TFmSel2RadioGroups, FmSel2RadioGroups);
  Application.CreateForm(TFmSel3RadioGroups, FmSel3RadioGroups);
  Application.CreateForm(TFmSelCheckGroup, FmSelCheckGroup);
  //Application.CreateForm(TFmLinkRankSkin, FmLinkRankSkin);
  //Application.CreateForm(TFmCoresVCLSkin, FmCoresVCLSkin);
  //Application.CreateForm(TFmUnLock_MLA, FmUnLock_MLA);
  Application.Run;
end.

