unit Pesquisa2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, dmkEdit, Vcl.Menus, UnDmkEnums;

type
  TFmPesquisa2 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    Panel6: TPanel;
    ListBox1: TListBox;
    Panel7: TPanel;
    GradeA: TStringGrid;
    Panel20: TPanel;
    Label1: TLabel;
    Label3: TLabel;
    SpeedButton1: TSpeedButton;
    Label17: TLabel;
    SpeedButton9: TSpeedButton;
    EdTexto: TdmkEdit;
    CkSub1: TCheckBox;
    CkCaseSensitive: TCheckBox;
    EdDiretorio: TdmkEdit;
    Panel21: TPanel;
    StaticText3: TStaticText;
    MeExtensoes: TMemo;
    EdArqArqs: TdmkEdit;
    Panel19: TPanel;
    StaticText2: TStaticText;
    MeExcl: TMemo;
    ListBox2: TListBox;
    Panel8: TPanel;
    Label4: TLabel;
    GradeB: TStringGrid;
    RE1: TRichEdit;
    ListBox3: TListBox;
    GroupBox2: TGroupBox;
    Panel9: TPanel;
    BtArquivos: TBitBtn;
    BtCancela: TBitBtn;
    BtPesquisa: TBitBtn;
    Panel10: TPanel;
    BitBtn1: TBitBtn;
    BtSubstitui: TBitBtn;
    PMArquivos: TPopupMenu;
    Dodiretrioselecionado1: TMenuItem;
    Listadearquivos1: TMenuItem;
    PB1: TProgressBar;
    Panel1: TPanel;
    Label2: TLabel;
    EdPAS_Guia: TdmkEdit;
    Label5: TLabel;
    MePAS_TxtIns: TMemo;
    Label6: TLabel;
    EdPAS_NumLin: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Dodiretrioselecionado1Click(Sender: TObject);
    procedure Listadearquivos1Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton9Click(Sender: TObject);
    procedure BtArquivosClick(Sender: TObject);
    procedure BtCancelaClick(Sender: TObject);
    procedure BtSubstituiClick(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
  private
    { Private declarations }
    FMemoFile: String;
    FScanAborted: Boolean;
  public
    { Public declarations }
  end;

  var
  FmPesquisa2: TFmPesquisa2;

implementation

uses UnMyObjects, Module, UnMLAGeral, UnDmkProcFunc, Principal;

{$R *.DFM}

procedure TFmPesquisa2.BtArquivosClick(Sender: TObject);
var
  Cam: String;
begin
  Cam := Application.Title + 'Pesquisa2\';
  Geral.WriteAppKey('Texto', Cam, EdTexto.Text, ktString, HKEY_LOCAL_MACHINE);
  Geral.WriteAppKey('Diretorio', Cam, EdDiretorio.Text, ktString, HKEY_LOCAL_MACHINE);
  Geral.WriteAppKey('ListaArquivos', Cam, EdArqArqs.Text, ktString, HKEY_LOCAL_MACHINE);
  //
  MyObjects.MostraPopUpDeBotao(PMArquivos, BtArquivos);
end;

procedure TFmPesquisa2.BtCancelaClick(Sender: TObject);
begin
  FScanAborted := True;
end;

procedure TFmPesquisa2.BtPesquisaClick(Sender: TObject);
var
  i, n, k, m, ic, kc: integer;
  Lista: TStringList;
  NaoQuer: Boolean;
  //
  TxtA, TxtB: String;
begin
  Screen.Cursor := crHourGlass;
  try
    FScanAborted := False;
    Lista := TStringList.Create;
    MyObjects.LimpaGrade(GradeA, 1, 1, True);
    MyObjects.LimpaGrade(GradeB, 1, 1, True);
    ic := 0;
    kc := 0;
    PB1.Position := 0;
    PB1.Max := ListBox1.Items.Count;
    //
    for i := 0 to ListBox1.Items.Count -1 do
    begin
      try
        MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, ListBox1.Items[i]);
        //
        if FScanAborted then
        begin
          Screen.Cursor := crDefault;
          Exit;
        end;
        n := FmPrincipal.ScanFile(ListBox1.Items[i], EdTexto.Text, CkCaseSensitive.Checked);
        if n > 0 then
        begin
          ic := ic + 1;
          GradeA.RowCount := ic + 1;
          GradeA.Cells[0,ic] := IntToStr(ic);
          GradeA.Cells[1,ic] := IntToStr(n);
          GradeA.Cells[2,ic] := ListBox1.Items[i];
          //ListBox2.Items.Add(ListBox1.Items[i]+' Caracter ->'+IntToStr(n));
          Lista.Clear;
          Lista.LoadFromFile(ListBox1.Items[i]);
          //
          for k := 0 to Lista.Count -1 do
          begin
            TxtA := LowerCase(EdTexto.Text);
            TxtB := LowerCase(Lista.Strings[k]);
            if Pos(TxtA, TxtB) > 0 then
            begin
              {
              Geral.MB_Aviso('Texto localizado: "' + TxtA + '"' + #13#10 +
              'Texto da linha: ' + TxtB);
              }
              NaoQuer := False;
              for m := 0 to MeExcl.Lines.Count - 1 do
              begin
                if Pos(LowerCase(MeExcl.Lines[m]), LowerCase(Lista.Strings[k])) > 0 then
                begin
                  NaoQuer := True;
                  Break;
                end;
              end;
              if not NaoQuer then
              begin
                kc := kc + 1;
                GradeB.RowCount := kc+1;
                GradeB.Cells[0,kc] := IntToStr(kc);
                GradeB.Cells[1,kc] := ListBox1.Items[i];
                GradeB.Cells[2,kc] := IntToStr(k);
                GradeB.Cells[3,kc] := Lista.Strings[k];
                GradeB.Cells[4,kc] := '';//Lista.Strings[k];
                //ListBox3.Items.Add(ListBox1.Items[i]+ '  ['+IntToStr(k)+']  '+
                  //Lista.Strings[k]);
              end;
            end;
            Application.ProcessMessages;
          end;
        end;
      finally
        // nada
      end;
    end;
    PB1.Position := 0;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    Screen.Cursor := crDefault;
  except
    raise;
  end;
end;

procedure TFmPesquisa2.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPesquisa2.BtSubstituiClick(Sender: TObject);
  function MudaExtensao(Arq, Ext: String): String;
  var
    P: Integer;
    X: String;
  begin
    P := 0;
    while Pos('.', Arq) > 0 do
    begin
      P := Pos('.', Arq);
      Delete(Arq, P, 1);
    end;
    if P > 0 then
      X := Copy(Arq, 1, P - 1)
    else
      X := Arq;
    //
    Arq := X + '.' + Ext;
    //
    Result := Arq;
  end;
const
  PAS = 'pas';
var
  I, N, J, K: Integer;
  Arq, LocTexto, NewTexto: String;
  Lista: TStringList;
  ListaLinhas: array of Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    LocTexto := EdPAS_Guia.Text;
    NewTexto := MePAS_TxtIns.Text;
    Lista := TStringList.Create;
    try
      PB1.Position := 0;
      PB1.Max := GradeB.RowCount -1;
      for I := 1 to GradeB.RowCount -1 do
      begin
        Arq := MudaExtensao(GradeB.Cells[1, I], PAS);
        MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
        'Add txt arq: ' + Arq);
        n := FmPrincipal.ScanFile(Arq, LocTexto, CkCaseSensitive.Checked);
        if n > 0 then
        begin
          SetLength(ListaLinhas, 0);
          Lista.Clear;
          Lista.LoadFromFile(Arq);
          for J := 0 to Lista.Count - 1 do
          begin
            if pos(LocTexto, Lista[J]) > 0 then
            begin
              K := Length(ListaLinhas);
              SetLength(ListaLinhas, K + 1);
              ListaLinhas[K] := J;
            end;
          end;
          for J := Length(ListaLinhas) - 1 downto 0 do
          begin
            K := ListaLinhas[J];
            if EdPAS_NumLin.ValueVariant = 0 then
              Lista[K] := Lista[K] + NewTexto
            else begin
              K := K + EdPAS_NumLin.ValueVariant - 1;
              if Lista.Count <= K then
              begin
                while Lista.Count <= K do
                  Lista.Add('');
              end;
              Lista[K] := Lista[K] + #13#10 + NewTexto;
            end;
          end;
          MLAGeral.SaveTextToFile(Arq, Lista.Text);
          //
        end;
      end;
      PB1.Position := 0;
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Substitui��o finalizada!');
    finally
      Lista.Free;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPesquisa2.Dodiretrioselecionado1Click(Sender: TObject);
var
  I: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    ListBox1.Clear;
    MyObjects.LimpaGrade(GradeA, 1, 1, True);
    for I := 0 to MeExtensoes.Lines.Count -1 do
    begin
      MyObjects.GetAllFiles(CkSub1.Checked, EdDiretorio.Text+'\'+
      MeExtensoes.Lines[I], ListBox1, False, LaAviso1, LaAviso2);
    end;
    BtPesquisa.Enabled := True;
    BtSubstitui.Enabled := True;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPesquisa2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPesquisa2.FormCreate(Sender: TObject);// Adicionado por .DFM > .PAS
var
  cam: String;
begin
  Geral.MB_Info(
  'Esta janela tem por objetivo adicionar texto em arquivos .pas atrav�s de ' + #13#10 +
  'um texto (propriedade) que ser� pesquisado em arquivos .dfm!' + #13#10 +
  '' + #13#10 +
  'CUIDADO!!!' + #13#10 +
  '' + #13#10);
  //
  ImgTipo.SQLType := stPsq;
  FMemoFile := ExtractFilePath(Application.ExeName) + 'Exclusoes.txt';
  Cam := Application.Title + 'Pesquisa2\';
  EdTexto.Text := Geral.ReadAppKey('Texto', Cam, ktString, '', HKEY_LOCAL_MACHINE);
  EdDiretorio.Text := Geral.ReadAppKey('Diretorio', Cam, ktString, '', HKEY_LOCAL_MACHINE);
  EdArqArqs.Text := Geral.ReadAppKey('ListaArquivos', Cam, ktString, '', HKEY_LOCAL_MACHINE);
  //
  //
  GradeA.Cells[0,0] := 'Seq��ncia';
  GradeA.Cells[1,0] := '1� linha';
  GradeA.Cells[2,0] := 'Nome do arquivo';
  //
  GradeB.Cells[0,0] := 'Seq��ncia';
  GradeB.Cells[1,0] := 'Nome do arquivo';
  GradeB.Cells[2,0] := 'N� linha';
  GradeB.Cells[3,0] := 'Linha inteira (Texto)';
  //
  if FileExists(FMemoFile) then Geral.LeArquivoToMemo(FMemoFile, MeExcl);
end;

procedure TFmPesquisa2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPesquisa2.Listadearquivos1Click(Sender: TObject);
  procedure LeArquivo(Arquivo: String);
  const
    Aviso = 'Arquivos localizados: ';
  var
    Arq: TextFile;
    Nome, S, X, Y: String;
    posI, posF, I, N, J: Integer;
    posE: Boolean;
  begin
    PosE := False;
    ListBox3.Items.Clear;
    posI := 1;
    //posF := 0;
    S := EdArqArqs.Text;
    for I := 1 to Length(S) do
    begin
      if S[I] = '\' then
      begin
        Nome := Copy(S, posI, I-posI);
        ListBox3.Items.Append(Nome);
        posI := I + 1;
      end;
    end;
    //
    AssignFile(Arq, Arquivo);
    if FileExists(Arquivo) then
    begin
      Reset(Arq);
      N := 0;
      while not Eof(Arq) do
      begin
        Readln(Arq, S);
        posI := 0;
        posF := 0;
        for I := 1 to Length(S) do
        begin
          if S[I] = Char(39) then
          begin
            if posI = 0 then
              posI := I
            else
            if posF = 0 then
              posF := I;
          end;
          if (posI > 0) and (posF >0) then
            N := N + 1;
          if (S[I] = ';') and (N > 0) then
          begin
            posE := True;
          end;
        end;
        if (posI > 0) and (posF >0) then
        begin
          Nome := Copy(S, PosI + 1, PosF-PosI-1);
          ListBox2.Items.Add(Nome);
          //
          J := 0;
          N := pos('..\', Nome);
          while N = 1 do
          begin
            Nome := Copy(Nome, 4);
            N := pos('..\', Nome);
            J := J + 1;
          end;
          X := '';
          for I := 0 to ListBox3.Items.Count - J -1 do
          begin
            X := X + ListBox3.Items[I] + '\';
          end;
          for I := 0 to MeExtensoes.Lines.Count - 1 do
          begin
            Nome := dmkPF.MudaExtensaoDeArquivo(Nome, Copy(MeExtensoes.Lines[I], 3));
            Y := X + Nome;
            if FileExists(Y) then
            begin
              ListBox1.Items.Add(Y);
              MyObjects.Informa2(LaAviso1, LaAviso2, True, Aviso + Geral.FF0(ListBox1.Items.Count));
            end;
          end;
        end;
        if posE then
          Break;
      end;
    end;
    CloseFile(Arq);
    MyObjects.Informa2(LaAviso1, LaAviso2, False, Aviso + Geral.FF0(ListBox1.Items.Count) + ' !!!');
  end;

begin
  Screen.Cursor := crHourGlass;
  try
    ListBox1.Clear;
    MyObjects.LimpaGrade(GradeA, 1, 1, True);
    //
    LeArquivo(EdArqArqs.Text);
    //
    BtPesquisa.Enabled := True;
    BtSubstitui.Enabled := True;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPesquisa2.SpeedButton1Click(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDiretorio);
end;

procedure TFmPesquisa2.SpeedButton9Click(Sender: TObject);
var
  IniDir, Arquivo: String;
begin
  IniDir := ExtractFileDir(Application.ExeName);
  Arquivo := '';
  if MyObjects.FileOpenDialog(Self, IniDir, Arquivo,
  'Selecione o arquivo', '', [], Arquivo) then
    EdArqArqs.Text := Arquivo;
end;

end.
