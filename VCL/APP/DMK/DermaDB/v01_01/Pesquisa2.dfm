object FmPesquisa2: TFmPesquisa2
  Left = 339
  Top = 185
  Caption = 'XXX-XXXXX-999 :: ????????????? ??????????? ????????????'
  ClientHeight = 629
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 430
        Height = 32
        Caption = 'Localizar no .DFM e inserir no .PAS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 430
        Height = 32
        Caption = 'Localizar no .DFM e inserir no .PAS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 430
        Height = 32
        Caption = 'Localizar no .DFM e inserir no .PAS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 524
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 524
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 524
        Align = alClient
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 507
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Panel6: TPanel
            Left = 0
            Top = 0
            Width = 1004
            Height = 220
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object ListBox1: TListBox
              Left = 769
              Top = 0
              Width = 114
              Height = 220
              Align = alClient
              ItemHeight = 13
              TabOrder = 0
            end
            object Panel7: TPanel
              Left = 0
              Top = 0
              Width = 584
              Height = 220
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 1
              object GradeA: TStringGrid
                Left = 0
                Top = 132
                Width = 584
                Height = 88
                Align = alClient
                ColCount = 3
                DefaultColWidth = 40
                DefaultRowHeight = 17
                RowCount = 2
                Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
                TabOrder = 0
                ColWidths = (
                  40
                  61
                  453)
              end
              object Panel20: TPanel
                Left = 0
                Top = 0
                Width = 584
                Height = 132
                Align = alTop
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 1
                object Label1: TLabel
                  Left = 8
                  Top = 4
                  Width = 30
                  Height = 13
                  Caption = 'Texto:'
                end
                object Label3: TLabel
                  Left = 8
                  Top = 44
                  Width = 42
                  Height = 13
                  Caption = 'Diret'#243'rio:'
                end
                object SpeedButton1: TSpeedButton
                  Left = 440
                  Top = 60
                  Width = 23
                  Height = 22
                  Caption = '...'
                  OnClick = SpeedButton1Click
                end
                object Label17: TLabel
                  Left = 8
                  Top = 84
                  Width = 166
                  Height = 13
                  Caption = 'Arquivo contendo lista de arquivos:'
                end
                object SpeedButton9: TSpeedButton
                  Left = 440
                  Top = 100
                  Width = 23
                  Height = 22
                  Caption = '...'
                  OnClick = SpeedButton9Click
                end
                object EdTexto: TdmkEdit
                  Left = 8
                  Top = 20
                  Width = 333
                  Height = 21
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object CkSub1: TCheckBox
                  Left = 344
                  Top = 38
                  Width = 117
                  Height = 17
                  Caption = 'Incluir subdiret'#243'rios.'
                  Checked = True
                  State = cbChecked
                  TabOrder = 1
                end
                object CkCaseSensitive: TCheckBox
                  Left = 344
                  Top = 22
                  Width = 97
                  Height = 17
                  Caption = 'Case sensitive.'
                  TabOrder = 2
                end
                object EdDiretorio: TdmkEdit
                  Left = 8
                  Top = 60
                  Width = 429
                  Height = 21
                  TabOrder = 3
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object Panel21: TPanel
                  Left = 466
                  Top = 0
                  Width = 118
                  Height = 132
                  Align = alRight
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 4
                  object StaticText3: TStaticText
                    Left = 0
                    Top = 0
                    Width = 118
                    Height = 17
                    Align = alTop
                    Alignment = taCenter
                    Caption = 'Extens'#245'es de arquivos:'
                    TabOrder = 0
                  end
                  object MeExtensoes: TMemo
                    Left = 0
                    Top = 17
                    Width = 118
                    Height = 115
                    Align = alClient
                    Enabled = False
                    Lines.Strings = (
                      '*.dfm')
                    TabOrder = 1
                    WantReturns = False
                  end
                end
                object EdArqArqs: TdmkEdit
                  Left = 8
                  Top = 100
                  Width = 429
                  Height = 21
                  TabOrder = 5
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
              end
            end
            object Panel19: TPanel
              Left = 584
              Top = 0
              Width = 185
              Height = 220
              Align = alLeft
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 2
              object StaticText2: TStaticText
                Left = 0
                Top = 0
                Width = 185
                Height = 17
                Align = alTop
                Alignment = taCenter
                Caption = 'Excess'#245'es:'
                TabOrder = 0
              end
              object MeExcl: TMemo
                Left = 0
                Top = 17
                Width = 185
                Height = 203
                Align = alLeft
                TabOrder = 1
                WantReturns = False
              end
            end
            object ListBox2: TListBox
              Left = 883
              Top = 0
              Width = 121
              Height = 220
              Align = alRight
              ItemHeight = 13
              TabOrder = 3
            end
          end
          object Panel8: TPanel
            Left = 0
            Top = 220
            Width = 1004
            Height = 20
            Align = alTop
            TabOrder = 1
            object Label4: TLabel
              Left = 4
              Top = 4
              Width = 9
              Height = 13
              Caption = '...'
            end
          end
          object GradeB: TStringGrid
            Left = 0
            Top = 240
            Width = 883
            Height = 105
            Align = alClient
            ColCount = 4
            DefaultColWidth = 40
            DefaultRowHeight = 17
            RowCount = 2
            Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goEditing]
            TabOrder = 2
            ColWidths = (
              40
              329
              69
              526)
          end
          object RE1: TRichEdit
            Left = 608
            Top = 256
            Width = 185
            Height = 89
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
            Visible = False
          end
          object ListBox3: TListBox
            Left = 883
            Top = 240
            Width = 121
            Height = 105
            Align = alRight
            ItemHeight = 13
            TabOrder = 4
          end
          object GroupBox2: TGroupBox
            Left = 0
            Top = 437
            Width = 1004
            Height = 70
            Align = alBottom
            TabOrder = 5
            object Panel9: TPanel
              Left = 2
              Top = 15
              Width = 1000
              Height = 53
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object BtArquivos: TBitBtn
                Tag = 28
                Left = 8
                Top = 4
                Width = 90
                Height = 40
                Caption = '&Arquivos'
                NumGlyphs = 2
                TabOrder = 0
                OnClick = BtArquivosClick
              end
              object BtCancela: TBitBtn
                Tag = 15
                Left = 102
                Top = 4
                Width = 90
                Height = 40
                Caption = '&Cancela'
                NumGlyphs = 2
                TabOrder = 1
                OnClick = BtCancelaClick
              end
              object BtPesquisa: TBitBtn
                Tag = 15
                Left = 195
                Top = 4
                Width = 90
                Height = 40
                Caption = '&Pesquisa'
                Enabled = False
                NumGlyphs = 2
                TabOrder = 2
                OnClick = BtPesquisaClick
              end
              object Panel10: TPanel
                Left = 889
                Top = 0
                Width = 111
                Height = 53
                Align = alRight
                BevelOuter = bvNone
                TabOrder = 3
                object BitBtn1: TBitBtn
                  Tag = 13
                  Left = 2
                  Top = 3
                  Width = 90
                  Height = 40
                  Cursor = crHandPoint
                  Hint = 'Sai da janela atual'
                  Caption = '&Sa'#237'da'
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                  OnClick = BtSaidaClick
                end
              end
              object BtSubstitui: TBitBtn
                Left = 288
                Top = 4
                Width = 90
                Height = 40
                Caption = 'Substitui'
                Enabled = False
                NumGlyphs = 2
                TabOrder = 4
                OnClick = BtSubstituiClick
              end
            end
          end
          object Panel1: TPanel
            Left = 0
            Top = 345
            Width = 1004
            Height = 92
            Align = alBottom
            BevelOuter = bvNone
            Caption = 'Panel1'
            TabOrder = 6
            object Label2: TLabel
              Left = 4
              Top = 4
              Width = 171
              Height = 13
              Caption = 'Texto guia a ser localizado no .PAS:'
            end
            object Label5: TLabel
              Left = 192
              Top = 4
              Width = 137
              Height = 13
              Caption = 'Texto a ser inserido no .PAS:'
            end
            object Label6: TLabel
              Left = 4
              Top = 44
              Width = 161
              Height = 13
              Caption = 'Linhas abaixo: ("zero" na mesma!)'
            end
            object EdPAS_Guia: TdmkEdit
              Left = 4
              Top = 20
              Width = 173
              Height = 21
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '.FormCreate('
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = '.FormCreate('
              ValWarn = False
            end
            object MePAS_TxtIns: TMemo
              Left = 192
              Top = 20
              Width = 805
              Height = 69
              Lines.Strings = (
                '// Adicionado por .DFM > .PAS'
                'CB?.ListSource = DModG.DsEmpresas;')
              TabOrder = 1
              WordWrap = False
            end
            object EdPAS_NumLin: TdmkEdit
              Left = 4
              Top = 60
              Width = 173
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 572
    Width = 1008
    Height = 57
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 40
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 23
        Width = 1004
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object PMArquivos: TPopupMenu
    Left = 32
    Top = 484
    object Dodiretrioselecionado1: TMenuItem
      Caption = '&Diret'#243'rio selecionado'
      OnClick = Dodiretrioselecionado1Click
    end
    object Listadearquivos1: TMenuItem
      Caption = '&Lista de arquivos do arquivo selecionado'
      OnClick = Listadearquivos1Click
    end
  end
end
