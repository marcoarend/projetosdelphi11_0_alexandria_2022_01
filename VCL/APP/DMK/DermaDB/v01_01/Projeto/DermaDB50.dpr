program DermaDB50;

{$DEFINE VER320}
{$UNDEF VER350}

uses
  Forms,
  AuxCtrls in '..\Units\AuxCtrls.pas' {FmAuxCtrls},
  AuxTxts in '..\Units\AuxTxts.pas' {FmAuxTxts},
  ChangeCods in '..\Units\ChangeCods.pas' {FmChangeCods},
  Conexao in '..\Units\Conexao.pas' {FmConexao},
  MyListas in '..\Units\MyListas.pas',
  PesqComp in '..\Units\PesqComp.pas' {FmPesqComp},
  Pesquisa in '..\Units\Pesquisa.pas' {FmPesquisa},
  Principal in '..\Units\Principal.pas' {FmPrincipal},
  ProcessList in '..\Units\ProcessList.pas' {FmProcessList},
  UnApp_Consts in '..\Units\UnApp_Consts.pas',
  UnApp_Vars in '..\Units\UnApp_Vars.pas',
  ResIntStrings in '..\..\..\..\..\UTL\_UNT\v01_01\ResIntStrings.pas',
  UnInternalConsts in '..\..\..\..\..\UTL\_UNT\v01_01\UnInternalConsts.pas',
  UnInternalConsts2 in '..\..\..\..\..\UTL\_UNT\v01_01\UnInternalConsts2.pas',
  UnInternalConsts3 in '..\..\..\..\..\UTL\_UNT\v01_01\UnInternalConsts3.pas',
  UnMLAGeral in '..\..\..\..\..\UTL\_UNT\v01_01\UnMLAGeral.pas',
  UnMsgInt in '..\..\..\..\..\UTL\_UNT\v01_01\UnMsgInt.pas',
  UnMyObjects in '..\..\..\..\..\UTL\_UNT\v01_01\UnMyObjects.pas',
  UnMyVCLref in '..\..\..\..\..\UTL\_UNT\v01_01\UnMyVCLref.pas',
  ZCF2 in '..\..\..\..\..\UTL\_UNT\v01_01\ZCF2.pas',
  reinit in '..\..\..\..\..\UTL\Textos\v01_01\reinit.pas',
  Megasena in '..\Units\Megasena.pas' {FmMegasena},
  PesqNome in '..\..\..\..\..\UTL\_FRM\v01_01\PesqNome.pas' {FmPesqNome},
  QuaisItens in '..\..\..\..\..\UTL\_FRM\v01_01\QuaisItens.pas' {FmQuaisItens},
  GetData in '..\..\..\..\..\UTL\_FRM\v01_01\GetData.pas' {FmGetData},
  SenhaBoss in '..\..\..\..\..\UTL\_FRM\v01_01\SenhaBoss.pas' {FmSenhaBoss},
  SenhaEspecial in '..\..\..\..\..\UTL\_FRM\v01_01\SenhaEspecial.pas' {FmSenhaEspecial},
  InstallMySQL41 in '..\..\..\..\..\UTL\MySQL\v01_01\InstallMySQL41.pas' {FmInstallMySQL41},
  MyGlyfs in '..\..\..\..\..\UTL\_FRM\v01_01\MyGlyfs.pas' {FmMyGlyfs},
  NomeX in '..\..\..\..\..\UTL\_FRM\v01_01\NomeX.pas' {FmNomeX},
  MyInis in '..\..\..\..\..\UTL\MySQL\v01_01\MyInis.pas' {FmMyInis},
  ServiceManager in '..\..\..\..\..\UTL\MySQL\v01_01\ServiceManager.pas',
  ServicoManager in '..\..\..\..\..\UTL\MySQL\v01_01\ServicoManager.pas' {FmServicoManager},
  Servidor in '..\..\..\..\..\UTL\MySQL\v01_01\Servidor.pas' {FmServidor},
  UCreate in '..\..\..\..\..\UTL\MySQL\v01_01\UCreate.pas',
  SelRadioGroup in '..\..\..\..\..\UTL\_FRM\v01_01\SelRadioGroup.pas' {FmSelRadioGroup},
  UnDmkProcFunc in '..\..\..\..\..\UTL\_UNT\v01_01\UnDmkProcFunc.pas',
  UCreateEDrop in '..\..\..\..\..\UTL\_UNT\v01_01\UCreateEDrop.pas',
  Textos in '..\..\..\..\..\UTL\Textos\v01_01\Textos.pas' {FmTextos},
  Textos2 in '..\..\..\..\..\UTL\Textos\v01_01\Textos2.pas' {FmTextos2},
  Pesquisa2 in '..\Pesquisa2.pas' {FmPesquisa2},
  UnMyLinguas in '..\..\..\..\..\..\MultiOS\AllOS\Listas\UnMyLinguas.pas',
  MemDBCheck in '..\..\..\..\..\UTL\MemoryTable\v01_01\MemDBCheck.pas',
  Module in '..\Units\Module.pas' {Dmod: TDataModule},
  DOSComando in '..\..\..\..\..\UTL\DOS\DOSComando.pas' {FmDOSComando},
  DosCommand in '..\..\..\..\..\UTL\DOS\DosCommand.pas',
  UnLock_MLA in '..\..\..\UnLock\v01_01\UnLock_MLA.pas' {FmUnLock_MLA},
  GetDataEUser in '..\..\..\..\..\UTL\_FRM\v01_01\GetDataEUser.pas' {FmGetDataEUser},
  UnDmkEnums in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnDmkEnums.pas',
  UnDmkListas in '..\..\..\..\..\..\MultiOS\AllOS\Listas\UnDmkListas.pas',
  UnMySQLCuringa in '..\..\..\..\..\UTL\MySQL\v01_01\UnMySQLCuringa.pas',
  UnALL_Jan in '..\..\..\..\..\UTL\_UNT\v01_01\UnALL_Jan.pas',
  UnGrl_Consts in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Consts.pas',
  UnGrl_Geral in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Geral.pas',
  UnGrl_Vars in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Vars.pas',
  ModuleGeral in '..\Units\ModuleGeral.pas' {DModG: TDataModule},
  Vcl.Themes,
  Vcl.Styles,
  UnProjGroup_Consts in '..\Units\UnProjGroup_Consts.pas',
  CAPICOM_TLB in '..\..\..\..\..\UTL\CAPICOM\v01_01\TLB\CAPICOM_TLB.pas',
  Sel2RadioGroups in '..\..\..\..\..\UTL\_FRM\v01_01\Sel2RadioGroups.pas' {FmSel2RadioGroups},
  Sel3RadioGroups in '..\..\..\..\..\UTL\_FRM\v01_01\Sel3RadioGroups.pas' {FmSel3RadioGroups},
  SelCheckGroup in '..\..\..\..\..\UTL\_FRM\v01_01\SelCheckGroup.pas' {FmSelCheckGroup},
  dmkDAC_PF in '..\..\..\..\..\UTL\MySQL\v01_01\dmkDAC_PF.pas',
  ABOUT in '..\..\..\..\..\UTL\_FRM\v01_01\ABOUT.pas' {FmAbout},
  SelListArr in '..\..\..\..\..\UTL\_FRM\v01_01\SelListArr.pas' {FmSelListArr};

{$R *.RES}

begin
(*
  Calculadora in '..\..\..\..\Outros\Geral\Forms\Calculadora.pas' {FmCalculadora},
  DOSComando in '..\..\..\..\Outros\DOS\DOSComando.pas' {FmDOSComando},
  DosCommand in '..\..\..\..\Outros\DOS\DosCommand.pas',
  CashTabs in '..\..\..\..\Financas\Units\CashTabs.pas',
*)
  Application.Initialize;
  TStyleManager.TrySetStyle('Auric');
  Application.Title := 'DermaDB';
  Application.CreateForm(TFmPrincipal, FmPrincipal);
  Application.CreateForm(TFmMyGlyfs, FmMyGlyfs);
  Application.CreateForm(TDmod, Dmod);
  Application.CreateForm(TDModG, DModG);
  Application.Run;
end.
