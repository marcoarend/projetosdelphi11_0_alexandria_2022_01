unit AuxCtrls;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Menus, UnDmkProcFunc, UnDmkEnums;

type
  TFmAuxCtrls = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtArquivos: TBitBtn;
    LaTitulo1C: TLabel;
    GroupBox1: TGroupBox;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox2: TGroupBox;
    Panel5: TPanel;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    PMArquivos: TPopupMenu;
    Dodiretrioselecionado1: TMenuItem;
    Listadearquivos1: TMenuItem;
    Panel6: TPanel;
    ListBox1: TListBox;
    Panel7: TPanel;
    Panel8: TPanel;
    Label3: TLabel;
    EdDiretorio: TdmkEdit;
    Label17: TLabel;
    EdArqArqs: TdmkEdit;
    SpeedButton1: TSpeedButton;
    SpeedButton9: TSpeedButton;
    GradeA: TStringGrid;
    Panel21: TPanel;
    StaticText3: TStaticText;
    MeExtensoes: TMemo;
    CkSub1: TCheckBox;
    CkCaseSensitive: TCheckBox;
    BtPesquisa1: TBitBtn;
    BtSubstitui: TBitBtn;
    ListBox3: TListBox;
    ListBox2: TListBox;
    RE1: TRichEdit;
    GradeB: TStringGrid;
    Button1: TButton;
    BtCancela: TBitBtn;
    EdClasse: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    EdPropriedade: TEdit;
    Panel19: TPanel;
    StaticText2: TStaticText;
    MeExcl: TMemo;
    GroupBox5: TGroupBox;
    GradeC: TStringGrid;
    PB1: TProgressBar;
    RGValores: TRadioGroup;
    BtPesquisa2: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtArquivosClick(Sender: TObject);
    procedure Dodiretrioselecionado1Click(Sender: TObject);
    procedure Listadearquivos1Click(Sender: TObject);
    procedure GradeADblClick(Sender: TObject);
    procedure GradeBDblClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton9Click(Sender: TObject);
    procedure BtCancelaClick(Sender: TObject);
    procedure BtPesquisa1Click(Sender: TObject);
    procedure BtPesquisa2Click(Sender: TObject);
  private
    { Private declarations }
    FScanAborted: Boolean;
  public
    { Public declarations }
  end;

  var
  FmAuxCtrls: TFmAuxCtrls;

implementation

uses UnMyObjects, Module, Principal, Textos;

{$R *.DFM}

procedure TFmAuxCtrls.BtArquivosClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMArquivos, BtArquivos);
end;

procedure TFmAuxCtrls.BtCancelaClick(Sender: TObject);
begin
  FScanAborted := True;
end;

procedure TFmAuxCtrls.BtPesquisa1Click(Sender: TObject);
const
  EndTxt = 'end';
  ObjTxt = 'object ';
var
  nxt,i, n, k, m, ic, kb, kc, (*Tam1,*) Tam2, Loc1, Trsh1, PosObj: integer;
  Lista: TStringList;
  NaoQuer, Mostra: Boolean;
  PropName, NomeCompo, ValrCompo, Lin1, LinX: String;
begin
  Screen.Cursor := crHourGlass;
  Mostra := False;
  //Tam2 := 0;
  FScanAborted := False;
  Lista := TStringList.Create;
  MyObjects.LimpaGrade(GradeA, 1, 1, True);
  MyObjects.LimpaGrade(GradeB, 1, 1, True);
  MyObjects.LimpaGrade(GradeC, 1, 1, True);
  ic := 0;
  kb := 0;
  kc := 0;
  Trsh1 := Length(ObjTxt);
  //Tam1 := Length(EdClasse.Text);
  PropName := LowerCase(EdPropriedade.Text);
  PB1.Max := ListBox1.Items.Count;
  PB1.Position := 0;
  for i := 0 to ListBox1.Items.Count -1 do
  begin
    PB1.Position := PB1.Position + 1;
    PB1.Update;
    Application.ProcessMessages;
    //
    if FScanAborted then
    begin
      Screen.Cursor := crDefault;
      Exit;
    end;
    n := FmPrincipal.ScanFile(ListBox1.Items[i], EdClasse.Text, CkCaseSensitive.Checked);
    if n > 0 then
    begin
      ic := ic + 1;
      GradeA.RowCount := ic + 1;
      GradeA.Cells[0,ic] := IntToStr(ic);
      GradeA.Cells[1,ic] := IntToStr(n);
      GradeA.Cells[2,ic] := ListBox1.Items[i];
      Lista.Clear;
      Lista.LoadFromFile(ListBox1.Items[i]);
      //
      for k := 0 to Lista.Count -1 do
      begin
        Loc1 := Pos(LowerCase(EdClasse.Text), LowerCase(Lista.Strings[k]));
        if Loc1 > 0 then
        begin
          NaoQuer := False;
          for m := 0 to MeExcl.Lines.Count - 1 do
          begin
            if Pos(LowerCase(MeExcl.Lines[m]), LowerCase(Lista.Strings[k])) > 0 then
            begin
              NaoQuer := True;
              Break;
            end;
          end;
          if not NaoQuer then
          begin
            kb := kb + 1;
            GradeB.RowCount := kb+1;
            GradeB.Cells[0,k] := IntToStr(k);
            GradeB.Cells[1,kb] := ListBox1.Items[i];
            GradeB.Cells[2,kb] := IntToStr(k);
            GradeB.Cells[3,kb] := Lista.Strings[k];
            GradeB.Cells[4,kb] := '';//Lista.Strings[k];
          end;
          // In�cio procura propriedade
          Lin1 := Lowercase(Trim(LowerCase(Lista.Strings[k])));
          PosObj := pos('object', Lin1);
          if PosObj = 1 then
          begin
            {
            if Copy(LowerCase(Lista.Strings[k]), Loc1 + Tam1) = '' then
            begin

            end;
            }
            nxt := k;
            LinX := '';
            while (nxt < Lista.Count-1) and
            (
              (LinX <> EndTxt)
              and
              (pos(PropName, LowerCase(LinX)) = 0)
            ) do
            begin
              nxt := nxt + 1;
              try
                LinX := Trim(Lista.Strings[nxt]);
              except
                LinX := '';
              end;
            end;
            Lin1 := Trim(Lista.Strings[k]);
            Tam2 := pos(':', Lin1);
            NomeCompo := Trim(Copy(Lin1, Trsh1, Tam2 - Trsh1));
            if pos(PropName, LowerCase(LinX)) > 0 then
            begin
              Tam2 := pos('=', LinX);
              ValrCompo := Trim(Copy(LinX, Tam2 + 2));
              // automatizar aqui"
              case RGValores.ItemIndex of
                0: Mostra := (ValrCompo = '0') and (Trim(ValrCompo) = '');
                1: Mostra := (ValrCompo <> '0') and (Trim(ValrCompo) <> '');
                2: Mostra := True;
              end;
              if Mostra then
              begin
                kc := kc + 1;
                GradeC.RowCount := kc+1;
                GradeC.Cells[0,kc] := IntToStr(kc);
                GradeC.Cells[1,kc] := ListBox1.Items[i];
                GradeC.Cells[2,kc] := NomeCompo;
                GradeC.Cells[3,kc] := ValrCompo;
                GradeC.Cells[4,kc] := '';
              end;
            end else
            begin
              if RGValores.ItemIndex in ([0,2]) then
              begin
                kc := kc + 1;
                GradeC.RowCount := kc+1;
                GradeC.Cells[0,kc] := IntToStr(kc);
                GradeC.Cells[1,kc] := ListBox1.Items[i];
                GradeC.Cells[2,kc] := NomeCompo;
                GradeC.Cells[3,kc] := ''; // N�o tem!
                GradeC.Cells[4,kc] := '';
              end;
            end;
          end;
          //
        end;
        Application.ProcessMessages;
      end;
    end;
  end;
  Screen.Cursor := crDefault;
end;

procedure TFmAuxCtrls.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmAuxCtrls.Dodiretrioselecionado1Click(Sender: TObject);
var
  cam: String;
  I: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    Cam := Application.Title + 'Pesquisa\';
    //FmPrincipal.WriteAppKey('Texto', Cam, EdTexto.Text, ktString, HKEY_LOCAL_MACHINE);
    Geral.WriteAppKey('Diretorio', Cam, EdDiretorio.Text, ktString, HKEY_LOCAL_MACHINE);
    ListBox1.Clear;
    MyObjects.LimpaGrade(GradeA, 1, 1, True);
    for I := 0 to MeExtensoes.Lines.Count -1 do
    begin
      dmkPF.GetAllFiles(CkSub1.Checked, EdDiretorio.Text+'\'+
      MeExtensoes.Lines[I], ListBox1, False);
    end;
    BtPesquisa1.Enabled := True;
    BtPesquisa2.Enabled := True;
    BtSubstitui.Enabled := True;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmAuxCtrls.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  ImgTipo.SQLType := stPsq;
end;

procedure TFmAuxCtrls.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmAuxCtrls.GradeADblClick(Sender: TObject);
var
  MemoryStream: TMemoryStream;
begin
  Screen.Cursor := crHourGlass;
  try
    RE1.Lines.LoadFromFile(GradeA.Cells[2, GradeB.Row]);
    Application.CreateForm(TFmTextos, FmTextos);
    FmTextos.FAtributesSize := 10;
    FmTextos.FAtributesName := 'Courier New';
    FmTextos.FSaveSit    := 0;
    FmTextos.FRichEdit   := RE1;
    MemoryStream := TMemoryStream.Create;
    RE1.Lines.SaveToStream(MemoryStream);
    MemoryStream.Position := 0;
    FmTextos.Editor.Lines.LoadFromStream(MemoryStream);
    MemoryStream.Free;
    //
    FmTextos.Splitter1.Visible := False;
    FmTextos.LbItensMD.Visible := False;
    {
    FmTextos.FTipoLoc := vpLoc;
    FmTextos.FvpCol   := 0;
    FmTextos.FvpRow   := StrToInt(GradeB.Cells[2, GradeB.Row]);
    }
    //
  finally
    Screen.Cursor := crDefault;
  end;
  FmTextos.ShowModal;
  FmTextos.Destroy;
end;

procedure TFmAuxCtrls.GradeBDblClick(Sender: TObject);
var
  MemoryStream: TMemoryStream;
begin
  Screen.Cursor := crHourGlass;
  try
    RE1.Lines.LoadFromFile(GradeB.Cells[1, GradeB.Row]);
    Application.CreateForm(TFmTextos, FmTextos);
    FmTextos.FAtributesSize := 10;
    FmTextos.FAtributesName := 'Courier New';
    FmTextos.FSaveSit    := 0;
    FmTextos.FRichEdit   := RE1;
    MemoryStream := TMemoryStream.Create;
    RE1.Lines.SaveToStream(MemoryStream);
    MemoryStream.Position := 0;
    FmTextos.Editor.Lines.LoadFromStream(MemoryStream);
    MemoryStream.Free;
    //
    FmTextos.Splitter1.Visible := False;
    FmTextos.LbItensMD.Visible := False;
    FmTextos.FTipoLoc := vpLoc;
    FmTextos.FvpCol   := 0;
    FmTextos.FvpRow   := StrToInt(GradeB.Cells[2, GradeB.Row]);
    //
  finally
    Screen.Cursor := crDefault;
  end;
  FmTextos.ShowModal;
  FmTextos.Destroy;
end;

procedure TFmAuxCtrls.Listadearquivos1Click(Sender: TObject);
  procedure LeArquivo(Arquivo: String);
  var
    Arq: TextFile;
    Nome, S, X, Y: String;
    posI, posF, I, N, J: Integer;
    posE: Boolean;
  begin
    PosE := False;
    ListBox3.Items.Clear;
    posI := 1;
    //posF := 0;
    S := EdArqArqs.Text;
    for I := 1 to Length(S) do
    begin
      if S[I] = '\' then
      begin
        Nome := Copy(S, posI, I-posI);
        ListBox3.Items.Append(Nome);
        posI := I + 1;
      end;
    end;
    //
    AssignFile(Arq, Arquivo);
    if FileExists(Arquivo) then
    begin
      Reset(Arq);
      N := 0;
      while not Eof(Arq) do
      begin
        Readln(Arq, S);
        posI := 0;
        posF := 0;
        for I := 1 to Length(S) do
        begin
          if S[I] = Char(39) then
          begin
            if posI = 0 then
              posI := I
            else
            if posF = 0 then
              posF := I;
          end;
          if (posI > 0) and (posF >0) then
            N := N + 1;
          if (S[I] = ';') and (N > 0) then
          begin
            posE := True;
          end;
        end;
        if (posI > 0) and (posF >0) then
        begin
          Nome := Copy(S, PosI + 1, PosF-PosI-1);
          ListBox2.Items.Add(Nome);
          //
          J := 0;
          N := pos('..\', Nome);
          while N = 1 do
          begin
            Nome := Copy(Nome, 4);
            N := pos('..\', Nome);
            J := J + 1;
          end;
          X := '';
          for I := 0 to ListBox3.Items.Count - J -1 do
          begin
            X := X + ListBox3.Items[I] + '\';
          end;
          for I := 0 to MeExtensoes.Lines.Count - 1 do
          begin
            Nome := dmkPF.MudaExtensaoDeArquivo(Nome, Copy(MeExtensoes.Lines[I], 3));
            Y := X + Nome;
            if FileExists(Y) then
              ListBox1.Items.Add(Y);
          end;
        end;
        if posE then
          Break;
      end;
    end;
    CloseFile(Arq);
  end;

var
  cam: String;
  //I: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    Cam := Application.Title + 'Pesquisa\';
    Geral.WriteAppKey('ListaArquivos', Cam, EdArqArqs.Text, ktString, HKEY_LOCAL_MACHINE);
    ListBox1.Clear;
    MyObjects.LimpaGrade(GradeA, 1, 1, True);
    //
    LeArquivo(EdArqArqs.Text);
    //
    BtPesquisa1.Enabled := True;
    BtPesquisa2.Enabled := True;
    BtSubstitui.Enabled := True;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmAuxCtrls.SpeedButton1Click(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDiretorio);
end;

procedure TFmAuxCtrls.SpeedButton9Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMArquivos, BtArquivos);
end;

procedure TFmAuxCtrls.BtPesquisa2Click(Sender: TObject);
const
  EndTxt = 'end';
  ObjTxt = 'object ';
var
  K, (*L,*) A, B, C,
  //D, nxt, m, kb, Tam1, Loc1, PosObj,
  i, n, ic, kc, Tam2, Trsh1: integer;
  Lista: TStringList;
  //NaoQuer,
  Mostra: Boolean;
  PropName, NomeCompo, ValrCompo, Lin1, LinX, Classe: String;
  QtdArqs: Integer;
  AlComps, MyCompsI, MyCompsF: array of Integer;
begin
  Screen.Cursor := crHourGlass;
  Mostra := False;
  Tam2 := 0;
  Classe := Lowercase(EdClasse.Text);
  FScanAborted := False;
  Lista := TStringList.Create;
  MyObjects.LimpaGrade(GradeA, 1, 1, True);
  MyObjects.LimpaGrade(GradeB, 1, 1, True);
  MyObjects.LimpaGrade(GradeC, 1, 1, True);
  ic := 0;
  //kb := 0;
  kc := 0;
  Trsh1 := Length(ObjTxt);
  //Tam1 := Length(EdClasse.Text);
  PropName := LowerCase(EdPropriedade.Text);
  QtdArqs := ListBox1.Items.Count;
  PB1.Max := QtdArqs;
  PB1.Position := 0;
  for i := 0 to QtdArqs -1 do
  begin
    PB1.Position := PB1.Position + 1;
    PB1.Update;
    Application.ProcessMessages;
    //
    if FScanAborted then
    begin
      Screen.Cursor := crDefault;
      Exit;
    end;
    n := FmPrincipal.ScanFile(ListBox1.Items[i], EdClasse.Text, CkCaseSensitive.Checked);
    if n > 0 then
    begin
      ic := ic + 1;
      GradeA.RowCount := ic + 1;
      GradeA.Cells[0,ic] := IntToStr(ic);
      GradeA.Cells[1,ic] := IntToStr(n);
      GradeA.Cells[2,ic] := ListBox1.Items[i];
      Lista.Clear;
      Lista.LoadFromFile(ListBox1.Items[i]);
      //
      //K := 0;
      A := 0;
      SetLength(AlComps, 0);
      for K := 0 to Lista.Count -1 do
      begin
        if pos('object', Lowercase(Trim(Lista[K]))) = 1 then
        begin
          A := A + 1;
          SetLength(AlComps, A);
          AlComps[A-1] := K;
        end;
      end;
      A := A + 1;
      SetLength(AlComps, A);
      AlComps[A-1] := Lista.Count -1;
      //
      SetLength(MyCompsI, 0);
      SetLength(MyCompsF, 0);
      B := 0;
      for K := 0 to A-2 do
      begin
        if pos(Classe, Lowercase(Trim(Lista[AlComps[K]]))) > 0 then
        begin
          B := B + 1;
          SetLength(MyCompsI, B);
          SetLength(MyCompsF, B);
          //
          MyCompsI[B-1] := AlComps[K];
          MyCompsF[B-1] := AlComps[K+1];
        end;
      end;
      NomeCompo := Trim(Copy(Lin1, Trsh1, Tam2 - Trsh1));
      for K := 0 to B-1 do
      begin
        for C := MyCompsI[K] to MyCompsF[K] do
        begin
          LinX := Trim(Lista.Strings[C]);
          //
          if pos(PropName, LowerCase(LinX)) > 0 then
          begin
            Tam2 := pos('=', LinX);
            ValrCompo := Trim(Copy(LinX, Tam2 + 2));
            // automatizar aqui"
            case RGValores.ItemIndex of
              0: Mostra := (ValrCompo = '0') and (Trim(ValrCompo) = '');
              1: Mostra := (ValrCompo <> '0') and (Trim(ValrCompo) <> '');
              2: Mostra := True;
            end;
            if Mostra then
            begin
              kc := kc + 1;
              GradeC.RowCount := kc+1;
              GradeC.Cells[0,kc] := IntToStr(kc);
              GradeC.Cells[1,kc] := ListBox1.Items[i];
              GradeC.Cells[2,kc] := NomeCompo;
              GradeC.Cells[3,kc] := ValrCompo;
              GradeC.Cells[4,kc] := '';
            end;
          end;
        end;
      end;
      {
      while K < Lista.Count -1 do
      begin
        Loc1 := Pos(LowerCase(EdClasse.Text), LowerCase(Lista.Strings[k]));
        if Loc1 > 0 then
        begin
          NaoQuer := False;
          for m := 0 to MeExcl.Lines.Count - 1 do
          begin
            if Pos(LowerCase(MeExcl.Lines[m]), LowerCase(Lista.Strings[k])) > 0 then
            begin
              NaoQuer := True;
              Break;
            end;
          end;
          if not NaoQuer then
          begin
            kb := kb + 1;
            GradeB.RowCount := kb+1;
            GradeB.Cells[0,k] := IntToStr(k);
            GradeB.Cells[1,kb] := ListBox1.Items[i];
            GradeB.Cells[2,kb] := IntToStr(k);
            GradeB.Cells[3,kb] := Lista.Strings[k];
            GradeB.Cells[4,kb] := '';//Lista.Strings[k];
          end;
          // In�cio procura propriedade
          Lin1 := Lowercase(Trim(LowerCase(Lista.Strings[k])));
          PosObj := pos('object', Lin1);
          if PosObj = 1 then
          begin
            (*
            if Copy(LowerCase(Lista.Strings[k]), Loc1 + Tam1) = '' then
            begin

            end;
            *)
            nxt := k;
            LinX := '';
            while (nxt < Lista.Count-1) and
            (
              (LinX <> EndTxt)
              and
              (pos(PropName, LowerCase(LinX)) = 0)
            ) do
            begin
              nxt := nxt + 1;
              try
                LinX := Trim(Lista.Strings[nxt]);
              except
                LinX := '';
              end;
            end;
            Lin1 := Trim(Lista.Strings[k]);
            Tam2 := pos(':', Lin1);
            NomeCompo := Trim(Copy(Lin1, Trsh1, Tam2 - Trsh1));
            if pos(PropName, LowerCase(LinX)) > 0 then
            begin
              Tam2 := pos('=', LinX);
              ValrCompo := Trim(Copy(LinX, Tam2 + 2));
              // automatizar aqui"
              case RGValores.ItemIndex of
                0: Mostra := (ValrCompo = '0') and (Trim(ValrCompo) = '');
                1: Mostra := (ValrCompo <> '0') and (Trim(ValrCompo) <> '');
                2: Mostra := True;
              end;
              if Mostra then
              begin
                kc := kc + 1;
                GradeC.RowCount := kc+1;
                GradeC.Cells[0,kc] := IntToStr(kc);
                GradeC.Cells[1,kc] := ListBox1.Items[i];
                GradeC.Cells[2,kc] := NomeCompo;
                GradeC.Cells[3,kc] := ValrCompo;
                GradeC.Cells[4,kc] := '';
              end;
            end else
            begin
              if RGValores.ItemIndex in ([0,2]) then
              begin
                kc := kc + 1;
                GradeC.RowCount := kc+1;
                GradeC.Cells[0,kc] := IntToStr(kc);
                GradeC.Cells[1,kc] := ListBox1.Items[i];
                GradeC.Cells[2,kc] := NomeCompo;
                GradeC.Cells[3,kc] := ''; // N�o tem!
                GradeC.Cells[4,kc] := '';
              end;
            end;
          end;
          //
        end;
        Application.ProcessMessages;
      end;
      }
    end;
  end;
  Screen.Cursor := crDefault;
end;

end.
