object FmChangeCods: TFmChangeCods
  Left = 339
  Top = 185
  Caption = 'XXX-XXXXX-999 :: Mudan'#231'a de C'#243'digos em Tabelas'
  ClientHeight = 678
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 630
    Width = 1008
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      Enabled = False
      NumGlyphs = 2
      TabOrder = 0
    end
    object Panel2: TPanel
      Left = 896
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    Caption = 'Mudan'#231'a de C'#243'digos em Tabelas'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 1006
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 582
    Align = alClient
    TabOrder = 0
    object Panel31: TPanel
      Left = 1
      Top = 1
      Width = 1006
      Height = 44
      Align = alTop
      TabOrder = 0
      object PainelDB: TPanel
        Left = 1
        Top = 1
        Width = 1004
        Height = 42
        Align = alClient
        Caption = 'PainelDB'
        Color = clGray
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 0
      end
    end
    object PageControl1: TPageControl
      Left = 151
      Top = 45
      Width = 856
      Height = 536
      ActivePage = TabSheet14
      Align = alClient
      TabOrder = 1
      OnChange = PageControl1Change
      object TabSheet14: TTabSheet
        Caption = ' Pesquisa '
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object dmkDBGridDAC1: TdmkDBGridDAC
          Left = 0
          Top = 51
          Width = 169
          Height = 457
          SQLFieldsToChange.Strings = (
            'Ativo')
          SQLIndexesOnUpdate.Strings = (
            'Codigo')
          Align = alLeft
          Columns = <
            item
              Expanded = False
              FieldName = 'Ativo'
              Title.Caption = 'ok'
              Width = 17
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Width = 115
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Visible = True
            end>
          Color = clWindow
          DataSource = DsTabs
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          SQLTable = '_tabelas_'
          EditForceNextYear = False
          Columns = <
            item
              Expanded = False
              FieldName = 'Ativo'
              Title.Caption = 'ok'
              Width = 17
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Width = 115
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Visible = True
            end>
        end
        object DBGrid2: TDBGrid
          Left = 169
          Top = 51
          Width = 679
          Height = 457
          Align = alClient
          DataSource = DsFlds
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Field'
              Title.Caption = 'Campo'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Type'
              Title.Caption = 'Tipo'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Null'
              Title.Caption = 'Nulo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Key'
              Title.Caption = 'Chave'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Default'
              Title.Caption = 'Padr'#227'o'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Extra'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Comment'
              Title.Caption = 'Coment'#225'rios'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Collation'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Privileges'
              Visible = True
            end>
        end
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 848
          Height = 51
          Align = alTop
          TabOrder = 2
          object Label13: TLabel
            Left = 4
            Top = 4
            Width = 68
            Height = 13
            Caption = 'Integer antigo:'
          end
          object Label14: TLabel
            Left = 76
            Top = 4
            Width = 63
            Height = 13
            Caption = 'Integer novo:'
          end
          object Label15: TLabel
            Left = 384
            Top = 32
            Width = 186
            Height = 13
            Caption = 'Nome te tabela ou campo obrigat'#243'rio ->'
          end
          object EdIntAnt: TdmkEdit
            Left = 4
            Top = 20
            Width = 69
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdIntAntChange
          end
          object EdIntNew: TdmkEdit
            Left = 76
            Top = 20
            Width = 69
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdIntNewChange
          end
          object CkCampos: TCheckBox
            Left = 152
            Top = 30
            Width = 181
            Height = 17
            Caption = 'Somente campos com registros.'
            Checked = True
            State = cbChecked
            TabOrder = 2
          end
          object MeNomes: TMemo
            Left = 576
            Top = 0
            Width = 185
            Height = 47
            Lines.Strings = (
              'cond'
              'ent'
              'cnd')
            TabOrder = 3
          end
          object PB1: TProgressBar
            Left = 780
            Top = 4
            Width = 200
            Height = 17
            TabOrder = 4
          end
          object PB2: TProgressBar
            Left = 780
            Top = 24
            Width = 200
            Height = 17
            TabOrder = 5
          end
          object BtTabelas: TBitBtn
            Left = 152
            Top = 4
            Width = 75
            Height = 25
            Caption = 'Tabelas'
            TabOrder = 6
            OnClick = BtTabelasClick
          end
          object BitBtn16: TBitBtn
            Left = 236
            Top = 4
            Width = 75
            Height = 25
            Caption = 'Pesquisa'
            TabOrder = 7
            OnClick = BitBtn16Click
          end
        end
      end
      object TabSheet15: TTabSheet
        Caption = ' Resultados '
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGResult: TdmkDBGridDAC
          Left = 0
          Top = 29
          Width = 453
          Height = 479
          SQLFieldsToChange.Strings = (
            'Ativo')
          SQLIndexesOnUpdate.Strings = (
            'Controle')
          Align = alLeft
          Columns = <
            item
              Expanded = False
              FieldName = 'Ativo'
              Title.Caption = 'ok'
              Width = 17
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Width = 47
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Tabela'
              Width = 157
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Campo'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Itens'
              Width = 50
              Visible = True
            end>
          Color = clWindow
          DataSource = DsTeste
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDrawColumnCell = DBGResultDrawColumnCell
          SQLTable = '_psqchge_'
          EditForceNextYear = False
          Columns = <
            item
              Expanded = False
              FieldName = 'Ativo'
              Title.Caption = 'ok'
              Width = 17
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Width = 47
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Tabela'
              Width = 157
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Campo'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Itens'
              Width = 50
              Visible = True
            end>
        end
        object DBGrid1: TDBGrid
          Left = 453
          Top = 29
          Width = 395
          Height = 479
          Align = alClient
          DataSource = DsRegistros
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
        end
        object Panel5: TPanel
          Left = 0
          Top = 0
          Width = 848
          Height = 29
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 2
          object BtExclTest: TBitBtn
            Left = 168
            Top = 0
            Width = 49
            Height = 25
            Caption = 'Exclui'
            TabOrder = 0
            OnClick = BtExclTestClick
          end
          object BtItensAMudar: TBitBtn
            Left = 224
            Top = 0
            Width = 113
            Height = 25
            Caption = 'Itens a mudar'
            TabOrder = 1
            OnClick = BtItensAMudarClick
          end
        end
      end
      object TabSheet1: TTabSheet
        Caption = ' Executar mudan'#231'a '
        ImageIndex = 2
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGMudar: TdmkDBGridDAC
          Left = 0
          Top = 49
          Width = 848
          Height = 459
          SQLFieldsToChange.Strings = (
            'Ativo')
          SQLIndexesOnUpdate.Strings = (
            'Controle')
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Ativo'
              Title.Caption = 'ok'
              Width = 17
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Width = 46
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DB'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Tabela'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Campo'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Exclui'
              Width = 35
              Visible = True
            end>
          Color = clWindow
          DataSource = DsItsChange
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          SQLTable = '_itschge_'
          EditForceNextYear = False
          Columns = <
            item
              Expanded = False
              FieldName = 'Ativo'
              Title.Caption = 'ok'
              Width = 17
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Width = 46
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DB'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Tabela'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Campo'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Exclui'
              Width = 35
              Visible = True
            end>
        end
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 848
          Height = 49
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object Label1: TLabel
            Left = 4
            Top = 4
            Width = 138
            Height = 13
            Caption = 'Nome do grupo de mudan'#231'a:'
          end
          object Label2: TLabel
            Left = 324
            Top = 4
            Width = 68
            Height = 13
            Caption = 'Integer antigo:'
          end
          object Label3: TLabel
            Left = 396
            Top = 4
            Width = 63
            Height = 13
            Caption = 'Integer novo:'
          end
          object BtExeAlterCod: TBitBtn
            Left = 520
            Top = 16
            Width = 185
            Height = 25
            Caption = 'Executa altera'#231#227'o dos c'#243'digos'
            TabOrder = 0
            OnClick = BtExeAlterCodClick
          end
          object BitBtn1: TBitBtn
            Left = 708
            Top = 16
            Width = 137
            Height = 25
            Caption = 'Exclui item selecionado'
            TabOrder = 1
            OnClick = BitBtn1Click
          end
          object CBNome: TdmkDBLookupComboBox
            Left = 4
            Top = 20
            Width = 317
            Height = 21
            KeyField = 'Nome'
            ListField = 'Nome'
            ListSource = DsNomes
            TabOrder = 2
            OnClick = CBNomeClick
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdMudarOld: TdmkEdit
            Left = 324
            Top = 20
            Width = 69
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdIntAntChange
          end
          object EdMudarNew: TdmkEdit
            Left = 396
            Top = 20
            Width = 69
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdIntNewChange
          end
        end
      end
    end
    object GridTabelas: TDBGrid
      Left = 1
      Top = 45
      Width = 150
      Height = 536
      Align = alLeft
      DataSource = DsTabelas
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
    end
  end
  object DBTest: TMySQLDatabase
    Connected = True
    DatabaseName = 'test'
    UserName = 'root'
    UserPassword = 'wkljweryhvbirt'
    Host = '127.0.0.1'
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'DatabaseName=test'
      'Host=127.0.0.1'
      'PWD=wkljweryhvbirt'
      'UID=root')
    DatasetOptions = []
    Left = 4
    Top = 8
  end
  object QrUpdTest: TMySQLQuery
    Database = DBTest
    SQL.Strings = (
      'SELECT * FROM _psqchge_')
    Left = 32
    Top = 8
  end
  object QrTabs: TMySQLQuery
    Database = DBTest
    AfterScroll = QrTabsAfterScroll
    SQL.Strings = (
      'SELECT * FROM _tabelas_'
      'ORDER BY Nome')
    Left = 60
    Top = 8
    object QrTabsNome: TWideStringField
      FieldName = 'Nome'
      Origin = '_tabelas_.Nome'
      Size = 100
    end
    object QrTabsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = '_tabelas_.Codigo'
    end
    object QrTabsAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = '_tabelas_.Ativo'
      MaxValue = 1
    end
  end
  object DsTabs: TDataSource
    DataSet = QrTabs
    Left = 88
    Top = 8
  end
  object QrFlds: TMySQLQuery
    Database = DBChg
    SQL.Strings = (
      'DESCRIBE _tabelas_')
    Left = 756
    Top = 12
    object QrFldsField: TWideStringField
      FieldName = 'Field'
      Required = True
      Size = 64
    end
    object QrFldsType: TWideStringField
      FieldName = 'Type'
      Required = True
      Size = 40
    end
    object QrFldsNull: TWideStringField
      FieldName = 'Null'
      Required = True
      Size = 3
    end
    object QrFldsKey: TWideStringField
      FieldName = 'Key'
      Required = True
      Size = 3
    end
    object QrFldsDefault: TWideStringField
      FieldName = 'Default'
      Size = 64
    end
    object QrFldsExtra: TWideStringField
      FieldName = 'Extra'
      Required = True
    end
  end
  object DsFlds: TDataSource
    DataSet = QrFlds
    Left = 784
    Top = 12
  end
  object QrTeste: TMySQLQuery
    Database = DBTest
    BeforeClose = QrTesteBeforeClose
    AfterScroll = QrTesteAfterScroll
    SQL.Strings = (
      'SELECT * FROM _psqchge_'
      'ORDER BY Tabela, Campo')
    Left = 116
    Top = 8
    object QrTesteTabela: TWideStringField
      FieldName = 'Tabela'
      Size = 100
    end
    object QrTesteCampo: TWideStringField
      FieldName = 'Campo'
      Size = 100
    end
    object QrTesteItens: TIntegerField
      FieldName = 'Itens'
    end
    object QrTesteControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrTesteAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
  end
  object DsTeste: TDataSource
    DataSet = QrTeste
    Left = 144
    Top = 8
  end
  object PMExclTest: TPopupMenu
    Left = 340
    Top = 228
    object Excluiitensmarcados1: TMenuItem
      Caption = 'Exclui itens marcados'
      OnClick = Excluiitensmarcados1Click
    end
    object Excluilinhasselecionadas1: TMenuItem
      Caption = 'Exclui linhas selecionadas'
      OnClick = Excluilinhasselecionadas1Click
    end
  end
  object PMItensAMudar: TPopupMenu
    Left = 440
    Top = 224
    object Incluiritensmarcados1: TMenuItem
      Caption = 'Incluir itens marcados'
      OnClick = Incluiritensmarcados1Click
    end
    object Incluirresultados1: TMenuItem
      Caption = '&Incluir todos itens'
      OnClick = Incluirresultados1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Recriartabela1: TMenuItem
      Caption = '&Recriar tabela'
      OnClick = Recriartabela1Click
    end
  end
  object QrRegistros: TMySQLQuery
    Database = DBChg
    Left = 812
    Top = 12
  end
  object DsRegistros: TDataSource
    DataSet = QrRegistros
    Left = 840
    Top = 12
  end
  object QrItsChange: TMySQLQuery
    Database = DBTest
    SQL.Strings = (
      'SELECT * FROM _itschge_'
      'WHERE Nome=:P0'
      'ORDER BY DB, Tabela, Campo')
    Left = 172
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrItsChangeTabela: TWideStringField
      FieldName = 'Tabela'
      Size = 100
    end
    object QrItsChangeCampo: TWideStringField
      FieldName = 'Campo'
      Size = 100
    end
    object QrItsChangeControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrItsChangeAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
    object QrItsChangeDB: TWideStringField
      FieldName = 'DB'
      Size = 100
    end
    object QrItsChangeNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrItsChangeExclui: TSmallintField
      FieldName = 'Exclui'
      MaxValue = 1
    end
  end
  object DsItsChange: TDataSource
    DataSet = QrItsChange
    Left = 200
    Top = 8
  end
  object DBChg: TMySQLDatabase
    DatabaseName = 'syndic'
    UserName = 'root'
    UserPassword = 'wkljweryhvbirt'
    Host = '127.0.0.1'
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'DatabaseName=syndic'
      'Host=127.0.0.1'
      'PWD=wkljweryhvbirt'
      'UID=root')
    DatasetOptions = []
    Left = 728
    Top = 12
  end
  object QrExecuta: TMySQLQuery
    Database = DBChg
    SQL.Strings = (
      'SHOW TABLES FROM :p0')
    Left = 868
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrTabelas: TMySQLQuery
    Database = DBChg
    SQL.Strings = (
      'SHOW TABLES FROM :p0')
    Left = 896
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object DsTabelas: TDataSource
    DataSet = QrTabelas
    Left = 924
    Top = 12
  end
  object DBAlter: TMySQLDatabase
    UserName = 'root'
    UserPassword = 'wkljweryhvbirt'
    Host = '127.0.0.1'
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'Host=127.0.0.1'
      'PWD=wkljweryhvbirt'
      'UID=root')
    DatasetOptions = []
    Left = 816
    Top = 60
  end
  object QrAlter: TMySQLQuery
    Database = DBAlter
    Left = 844
    Top = 60
  end
  object QrNomes: TMySQLQuery
    Database = DBTest
    SQL.Strings = (
      'SELECT DISTINCT Nome'
      'FROM _itschge_'
      'ORDER BY Nome')
    Left = 32
    Top = 36
    object QrNomesNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsNomes: TDataSource
    DataSet = QrNomes
    Left = 60
    Top = 36
  end
end
