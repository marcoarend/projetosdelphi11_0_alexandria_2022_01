unit UnApp_Consts;

interface

uses UnDmkEnums;

const
  CO_DB_APP_MAIN_DB = 'MEMORY';
  CO_DB_APP_PATH = 'C:\_MLArend\Dermatek\DermaDB\';

  //ALTER DATABASE some_db CHARACTER SET utf8 COLLATE utf8_general_ci
  CO_MySQL_CharacterSet = 'utf8'; //latin1';  //'';  //'latin1'
  CO_MySQL_ConnectionCollation = 'utf8_general_ci';  //'';  // 'latin1_swedish_ci'

{
  CO_Configs_Arquivo = 'Configs';
  CO_Configs_NSubDir = '';
  CO_Configs_NomeExt = 'stngs';

  CO_EntCliCad_Arquivo = 'EntCliCad';
  CO_EntCliCad_NSubDir = '';
  CO_EntCliCad_NomeExt = 'entcli';

  CO_PrdPrc_NSubDir = 'Precos';
  CO_PrdPrc_NomeExt = 'lstprc';

  CO_PrdLst_NSubDir = 'Produtos';
  CO_PrdLst_NomeExt = 'prdlst';
}

implementation

end.
