unit Principal_;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Menus, Db, mySQLDbTables, StdCtrls, ExtCtrls, ComCtrls, Grids, DBGrids,
  Buttons, MySQLBatch, DBCtrls, Registry, mySQLDirectQuery, ABSMain, dmkDBGrid,
  dmkGeral, dmkEdit, WinSkinData, WinSkinStore, dmkDBGridDAC(*, VKDBFDataSet*),
  UnDmkProcFunc;

type
  THackDBGrid = class(TDBGrid);
  TFmPrincipal = class(TForm)
    MainMenu1: TMainMenu;
    Arquivo1: TMenuItem;
    Novaconexao1: TMenuItem;
    Sair1: TMenuItem;
    MyDB: TmySQLDatabase;
    QrTabelas: TmySQLQuery;
    QrDatabases: TmySQLQuery;
    DsDatabases: TDataSource;
    DsTabelas: TDataSource;
    QrCampos: TmySQLQuery;
    DsCampos: TDataSource;
    DsExecuta: TDataSource;
    QrExecuta: TmySQLQuery;
    MyEx: TmySQLDatabase;
    Timer1: TTimer;
    OpenDialog1: TOpenDialog;
    Panel1: TPanel;
    Splitter2: TSplitter;
    PageControl2: TPageControl;
    TabSheet2: TTabSheet;
    Splitter3: TSplitter;
    Panel4: TPanel;
    PageControl3: TPageControl;
    TabSheet5: TTabSheet;
    Splitter4: TSplitter;
    GridCampos: TDBGrid;
    TabSheet6: TTabSheet;
    Splitter7: TSplitter;
    Panel2: TPanel;
    Panel3: TPanel;
    BtConfirma1: TBitBtn;
    BtAbrir: TBitBtn;
    Panel6: TPanel;
    Panel8: TPanel;
    GridRegistros: TDBGrid;
    QrRegistros: TmySQLQuery;
    DsRegistros: TDataSource;
    TbRegistros: TmySQLTable;
    MySQLBExec: TMySQLBatchExecute;
    Panel9: TPanel;
    Panel10: TPanel;
    GridDatabases: TDBGrid;
    BtRefresh: TBitBtn;
    DOS1: TMenuItem;
    Panel11: TPanel;
    GridResult: TDBGrid;
    STOpen: TStaticText;
    Pesquisa1: TMenuItem;
    Panel12: TPanel;
    Panel13: TPanel;
    EdLocTable: TEdit;
    Label1: TLabel;
    TabSheet1: TTabSheet;
    Memo2SQL: TMemo;
    Panel14: TPanel;
    BtConfirma2: TBitBtn;
    BitBtn6: TBitBtn;
    ListBox2: TListBox;
    StBatch: TStaticText;
    Textosemarquivos1: TMenuItem;
    MegaSena1: TMenuItem;
    BitBtn7: TBitBtn;
    TabSheet7: TTabSheet;
    Memo3: TMemo;
    Panel16: TPanel;
    Button1: TButton;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    SaveDialog1: TSaveDialog;
    PainelTitulo: TPanel;
    Image1: TImage;
    Button2: TButton;
    TabSheet4: TTabSheet;
    Panel5: TPanel;
    QrMinhas: TmySQLQuery;
    DsMinhas: TDataSource;
    MySQL: TmySQLDatabase;
    BitBtn8: TBitBtn;
    QrMy: TmySQLQuery;
    dmkDBGrid1: TdmkDBGrid;
    QrMinhasTabela: TWideStringField;
    QrMinhasRegistros: TIntegerField;
    QrMinhasSelecio: TSmallintField;
    dmkDBGrid2: TdmkDBGrid;
    TbReg2: TmySQLTable;
    DsReg2: TDataSource;
    BitBtn9: TBitBtn;
    mySQLDirectQuery1: TmySQLDirectQuery;
    CEP1: TMenuItem;
    IBGE1: TMenuItem;
    CoresVCLSkin1: TMenuItem;
    RichText: TTabSheet;
    Query: TABSQuery;
    QrCopy: TmySQLQuery;
    QrCopyInteiro: TIntegerField;
    QrCopyString: TWideStringField;
    QrCopySmallint: TSmallintField;
    QrCopyFloat: TFloatField;
    QrCopyData: TDateField;
    QrCopyHora: TTimeField;
    QrCopyDataHora: TDateTimeField;
    QrCopyMemo: TWideMemoField;
    QrCopyLargeInt: TLargeintField;
    Panel17: TPanel;
    DBMemo1: TDBMemo;
    Panel18: TPanel;
    Label8: TLabel;
    EdPrefixo: TEdit;
    EdSufixo: TEdit;
    Label9: TLabel;
    BtTira: TBitBtn;
    BitBtn11: TBitBtn;
    TabSheet9: TTabSheet;
    Panel19: TPanel;
    BitBtn10: TBitBtn;
    MeEstrutMySQL: TMemo;
    Panel20: TPanel;
    BtRegistros: TBitBtn;
    CkLimitar: TCheckBox;
    Ed1: TdmkEdit;
    Ed2: TdmkEdit;
    BtConfReg: TBitBtn;
    Panel21: TPanel;
    MeFiltro: TMemo;
    GroupBox1: TGroupBox;
    TabSheet10: TTabSheet;
    Memo2: TMemo;
    Panel22: TPanel;
    Memo4: TMemo;
    Memo5: TMemo;
    GroupBox2: TGroupBox;
    Label11: TLabel;
    EdCampoQr: TEdit;
    BitBtn12: TBitBtn;
    BitBtn13: TBitBtn;
    Memo6: TMemo;
    Memo7: TMemo;
    Panel23: TPanel;
    Label10: TLabel;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    BtConsertarTxt: TBitBtn;
    ListBox3: TListBox;
    Panel24: TPanel;
    TabSheet11: TTabSheet;
    Panel25: TPanel;
    BitBtn14: TBitBtn;
    MeSQLUCreate: TMemo;
    BitBtn15: TBitBtn;
    TabSheet12: TTabSheet;
    Panel26: TPanel;
    GradeHTML: TStringGrid;
    Panel27: TPanel;
    EdLinhas: TdmkEdit;
    Label7: TLabel;
    MeHTML: TMemo;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    BtCarrega: TButton;
    PMCarrega: TPopupMenu;
    ltimosalvo1: TMenuItem;
    Arquivodecomponentes1: TMenuItem;
    SkinStore1: TSkinStore;
    sd1: TSkinData;
    Panel28: TPanel;
    ListBox1: TListBox;
    MemoSQL: TRichEdit;
    Splitter1: TSplitter;
    Panel29: TPanel;
    Panel30: TPanel;
    Label12: TLabel;
    EdCampo: TEdit;
    GridCampos2: TDBGrid;
    QrFldExec: TmySQLQuery;
    QrFldExecField: TWideStringField;
    QrFldExecType: TWideStringField;
    QrFldExecNull: TWideStringField;
    QrFldExecKey: TWideStringField;
    QrFldExecDefault: TWideStringField;
    QrFldExecExtra: TWideStringField;
    QrExecut2: TmySQLQuery;
    GridTabelas: TDBGrid;
    Ferramentas1: TMenuItem;
    Alterarcdigosemtabelas1: TMenuItem;
    PageControl1: TPageControl;
    TabSheet8: TTabSheet;
    TabSheet13: TTabSheet;
    TabSheet14: TTabSheet;
    Panel15: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    BitBtn5: TBitBtn;
    EdTPre1: TEdit;
    EdCPre1: TEdit;
    EdTPos1: TEdit;
    EdCPos1: TEdit;
    EdTPre2: TEdit;
    EdCPre2: TEdit;
    EdCPos2: TEdit;
    EdTPos2: TEdit;
    CkComponentes: TCheckBox;
    CkVariaveis: TCheckBox;
    EdTPre3: TEdit;
    EdCPre3: TEdit;
    EdCPos3: TEdit;
    EdTPos3: TEdit;
    CkCriaCampos: TCheckBox;
    EdTPre4: TEdit;
    EdCPre4: TEdit;
    EdCPos4: TEdit;
    EdTPos4: TEdit;
    CkListaDBF: TCheckBox;
    CkValores: TCheckBox;
    EdTPre5: TEdit;
    EdCPre5: TEdit;
    EdCPos5: TEdit;
    EdTPos5: TEdit;
    EdTabela: TEdit;
    Memo1: TMemo;
    Panel7: TPanel;
    BitBtn3: TBitBtn;
    MeABS: TMemo;
    PageControl4: TPageControl;
    TabSheet3: TTabSheet;
    Panel31: TPanel;
    Label64: TLabel;
    EdArqDBF: TdmkEdit;
    SbDir: TSpeedButton;
    PageControl5: TPageControl;
    TabSheet15: TTabSheet;
    TabSheet16: TTabSheet;
    //DsVKDBFNTX: TDataSource;
    DBGrid1: TDBGrid;
    MeDBF: TMemo;
    BtEstruturaDBF: TButton;
    Memo8: TMemo;
    Button6: TButton;
    EdMenosQ: TdmkEdit;
    Label13: TLabel;
    Label14: TLabel;
    QrFlds: TmySQLQuery;
    QrFldsField: TWideStringField;
    QrFldsType: TWideStringField;
    QrFldsNull: TWideStringField;
    QrFldsKey: TWideStringField;
    QrFldsDefault: TWideStringField;
    QrFldsExtra: TWideStringField;
    QrIts: TmySQLQuery;
    ComponentesDelphi1: TMenuItem;
    PesqFlds: TTabSheet;
    Panel32: TPanel;
    Panel34: TPanel;
    StaticText1: TStaticText;
    MePsqFldTabs: TMemo;
    Panel35: TPanel;
    Label15: TLabel;
    EdPsqFldFlds: TEdit;
    BitBtn4: TBitBtn;
    DBTest: TmySQLDatabase;
    QrTestUpd: TmySQLQuery;
    QrTestAux: TmySQLQuery;
    Panel33: TPanel;
    Label16: TLabel;
    EdPsqFldItns: TdmkEdit;
    Panel36: TPanel;
    Label17: TLabel;
    EdPsqFldDvrs: TdmkEdit;
    DBGrid2: TDBGrid;
    DsTestAux: TDataSource;
    BitBtn16: TBitBtn;
    Separacamposdelistas1: TMenuItem;
    estes1: TMenuItem;
    Delphi1: TMenuItem;
    Componentes1: TMenuItem;
    Button7: TButton;
    Button8: TButton;
    BtExporta: TButton;
    Memo9: TMemo;
    CkTabZerada: TCheckBox;
    QrMinhasObservaco: TWideStringField;
    QrEntiCliInt: TmySQLQuery;
    GBAvisos1: TGroupBox;
    Panel37: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    ProgressBar1: TProgressBar;
    ProgressBar2: TProgressBar;
    QrTab: TmySQLQuery;
    Panel38: TPanel;
    CkUNION: TCheckBox;
    Label18: TLabel;
    EdTab: TEdit;
    QrEntiCliIntCodCliInt: TIntegerField;
    MeSQLPre: TMemo;
    MeSQLPos: TMemo;
    Splitter5: TSplitter;
    Splitter6: TSplitter;
    EdDBFiltro: TEdit;
    ListadeProcessos1: TMenuItem;
    Miscelnea1: TMenuItem;
    LocnoDFMesubstnoPAS1: TMenuItem;
    RGVersaoMySQL: TRadioGroup;
    QrCamposNull: TWideStringField;
    QrCamposKey: TWideStringField;
    QrCamposExtra: TWideStringField;
    QrCamposField_And_Type: TWideStringField;
    QrCamposPADRAO: TWideStringField;
    QrCamposTIPO: TWideStringField;
    QrCamposCAMPO: TWideStringField;
    QrCamposField: TWideStringField;
    QrCamposType: TWideMemoField;
    QrCamposDefault: TWideMemoField;
    procedure Sair1Click(Sender: TObject);
    procedure Novaconexao1Click(Sender: TObject);
    procedure QrDatabasesBeforeClose(DataSet: TDataSet);
    procedure QrDatabasesAfterScroll(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure QrTabelasBeforeClose(DataSet: TDataSet);
    procedure QrTabelasAfterScroll(DataSet: TDataSet);
    procedure MemoSQLKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtAbrirClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure QrCamposCalcFields(DataSet: TDataSet);
    procedure BtRegistrosClick(Sender: TObject);
    procedure GridCamposKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure GridTabelasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure GridDatabasesKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ListBox1DblClick(Sender: TObject);
    procedure BtRefreshClick(Sender: TObject);
    procedure QrDatabasesAfterOpen(DataSet: TDataSet);
    procedure GridResultColEnter(Sender: TObject);
    procedure EdLocTableChange(Sender: TObject);
    procedure BtConfRegClick(Sender: TObject);
    procedure TbRegistrosAfterEdit(DataSet: TDataSet);
    procedure TbRegistrosAfterInsert(DataSet: TDataSet);
    procedure TbRegistrosAfterPost(DataSet: TDataSet);
    procedure TbRegistrosAfterCancel(DataSet: TDataSet);
    procedure BitBtn6Click(Sender: TObject);
    procedure BtConfirma2Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure BitBtn7Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure BitBtn8Click(Sender: TObject);
    procedure dmkDBGrid1CellClick(Column: TColumn);
    procedure TbReg2AfterCancel(DataSet: TDataSet);
    procedure TbReg2AfterEdit(DataSet: TDataSet);
    procedure TbReg2AfterInsert(DataSet: TDataSet);
    procedure TbReg2AfterPost(DataSet: TDataSet);
    procedure BitBtn9Click(Sender: TObject);
    procedure CEP1Click(Sender: TObject);
    procedure IBGE1Click(Sender: TObject);
    procedure CoresVCLSkin1Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BtTiraClick(Sender: TObject);
    procedure BitBtn11Click(Sender: TObject);
    procedure BitBtn10Click(Sender: TObject);
    procedure BtConsertarTxtClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure BitBtn12Click(Sender: TObject);
    procedure BitBtn13Click(Sender: TObject);
    procedure MegaSena1Click(Sender: TObject);
    procedure BitBtn14Click(Sender: TObject);
    procedure BitBtn15Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure EdLinhasExit(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure BtCarregaClick(Sender: TObject);
    procedure ltimosalvo1Click(Sender: TObject);
    procedure Arquivodecomponentes1Click(Sender: TObject);
    procedure BtItensAMudarClick(Sender: TObject);
    procedure EdIntNewChange(Sender: TObject);
    procedure Alterarcdigosemtabelas1Click(Sender: TObject);
    procedure SbDirClick(Sender: TObject);
    //procedure TbVKDBFNTXBeforeClose(DataSet: TDataSet);
    //procedure TbVKDBFNTXAfterOpen(DataSet: TDataSet);
    procedure BtEstruturaDBFClick(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure ComponentesDelphi1Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn16Click(Sender: TObject);
    procedure Separacamposdelistas1Click(Sender: TObject);
    procedure estes1Click(Sender: TObject);
    procedure Componentes1Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure BtExportaClick(Sender: TObject);
    procedure EdDBFiltroChange(Sender: TObject);
    procedure EdCampoChange(Sender: TObject);
    procedure ListadeProcessos1Click(Sender: TObject);
    procedure Miscelnea1Click(Sender: TObject);
    procedure LocnoDFMesubstnoPAS1Click(Sender: TObject);
    procedure MyExBeforeConnect(Sender: TObject);
  private
    { Private declarations }
    // Exporta CodeBase
    FPararPesqFlds: Boolean;
    procedure ExportaEstrututaCriacaoTabela;
    procedure ExportaComponentesCampos;
    procedure ExportaVariaveisCampos;
    procedure ExportaCriacaoCompenentes;
    procedure ExportaConferenciaDeValores;
    procedure ReopenTabelas();
    function  Posicao(Campo: String; NaoCopiar: array of String): Integer;
    procedure ExecutaQuery_A(Tipo: Integer);
    //function ConsertarFrase(const Texto: String; var N: Integer): String;
  public
    { Public declarations }
    FConexao, FContaD: Integer;
    FLista: TStringList;
    FCaminho, FTabela, FIndice: String;
    PID_DB_TEST: TmySQLDatabase;
    function RecriaTabelaLocal(Tabela: String; Acao: Integer): Boolean;
    procedure SalvaArquivo(EdNomCli, EdCPFCli: TEdit; Grade:
              TStringGrid; Data: TDateTime; FileName: String; ChangeData: Boolean);
    function AbreArquivoINI(Grade: TStringGrid; EdNomCli, EdCPFCli: TEdit;
             Data: TDateTime; Arquivo: String): Boolean;
    procedure ReopenDatabases(DBSel, DBLoc: String);
    procedure ReopenCampos();
    procedure ListaTabsComReg();
    function  LiberaPelaSenhaAdmin(): Boolean;
    procedure ReCaptionComponentesDeForm(Form: TForm);
    function  ScanFile(const FileName: string; const forString: string;
              caseSensitive: Boolean): Longint;
    function  ConectaDBTest(): Boolean;
  end;

var
  FmPrincipal: TFmPrincipal;
  VAR_BDSENHA: String;

const
  FNaoCopiar: array[0..6] of String =
    ('LK', 'DATACAD', 'DATAALT', 'USERCAD', 'USERALT', 'ALTERWEB', 'ATIVO');
  FGradeHTML = 'C:\GradeHTML.txt';

implementation

uses Conexao, Megasena, DOSComando, Pesquisa, UnMLAGeral, UnMyObjects, Module,
UnInternalConsts, ChangeCods, PesqComp, AuxTxts,
// PesqCEP, CoresVCLSkin, Senha,
AuxCtrls, MyListas, ProcessList, Pesquisa2;

{$R *.DFM}

procedure TFmPrincipal.SalvaArquivo(EdNomCli, EdCPFCli: TEdit; Grade:
  TStringGrid; Data: TDateTime; FileName: String; ChangeData: Boolean);
begin
  //MyCBase.SalvaArquivo(EdNomCli, EdCPFCli, Grade, Data, VAR_DBPATH+FileName, ChangeData);
end;

procedure TFmPrincipal.SbDirClick(Sender: TObject);
{
var
  Arquivo, Titulo, IniDir: String;
}
begin
{  Arquivo     := EdArqDBF.Text;
  Titulo  := 'Abrir Arquivo .DBF';
  IniDir  := '';
  if MyObjects.FileOpenDialog(Self, IniDir, Arquivo, Titulo,
  'Arquivos DBF|*.DBF;Arquivos dbf|*.dbf', [], Arquivo) then
  begin
    EdArqDBF.Text := Arquivo;
    if FileExists(Arquivo) then
    begin
      TbVKDBFNTX.Close;
      TbVKDBFNTX.DBFFileName := Arquivo;
      TbVKDBFNTX.Open;
      //
      PageControl5.ActivePageIndex := 0;
    end else Geral.MensagemBox('Arquivo n�o localizado:' + #13#10 + Arquivo,
    'Aviso', MB_OK+MB_ICONWARNING);
  end;}
end;

function TFmPrincipal.ScanFile(const FileName, forString: string;
  caseSensitive: Boolean): Longint;
  { returns position of string in file or -1, if not found }
const
  BufferSize = $8001;  { 32K+1 bytes }
var
  pBuf, pEnd, pScan, pPos: PAnsiChar;
  filesize: LongInt;
  bytesRemaining: LongInt;
  bytesToRead: Integer;//Word;
  F: file;
  SearchFor: PAnsiChar;
  oldMode: Word;
begin
  Result := -1;  { assume failure }
  if (Length(forString) = 0) or (Length(FileName) = 0) then Exit;
  SearchFor := nil;
  pBuf      := nil;

  { open file as binary, 1 byte recordsize }
  AssignFile(F, FileName);
  oldMode  := FileMode;
  FileMode := 0;    { read-only access }
  Reset(F, 1);
  FileMode := oldMode;
  try { allocate memory for buffer and pchar? search string }
    SearchFor := AnsiStrAlloc(Length(forString) + 1);
    StrPCopy(SearchFor, forString);
    if not caseSensitive then  { convert to upper case }
      UpperCase(SearchFor);
    GetMem(pBuf, BufferSize);
    filesize       := System.Filesize(F);
    bytesRemaining := filesize;
    pPos           := nil;
    while bytesRemaining > 0 do
    begin
      { calc how many bytes to read this round }
      if bytesRemaining >= BufferSize then
        bytesToRead := Pred(BufferSize)
      else
        bytesToRead := bytesRemaining;

      { read a buffer full and zero-terminate the buffer }
      //BlockRead(F, pBuf^, bytesToRead);
      BlockRead(F, pBuf^, bytesToRead, bytesToRead);
      pEnd  := @pBuf[bytesToRead];
      pEnd^ := #0;
       { scan the buffer. Problem: buffer may contain #0 chars! So we
         treat it as a concatenation of zero-terminated strings. }
      pScan := pBuf;
      while pScan < pEnd do
      begin
        if not caseSensitive then { convert to upper case }
          UpperCase(pScan);
        pPos := StrPos(pScan, SearchFor);  { search for substring }
        if pPos <> nil then
        begin { Found it! }
          Result := FileSize - bytesRemaining +
            Longint(pPos) - Longint(pBuf);
          Break;
        end;
        pScan := StrEnd(pScan);
        Inc(pScan);
      end;
      if pPos <> nil then Break;
      bytesRemaining := bytesRemaining - bytesToRead;
      if bytesRemaining > 0 then
      begin
       { no luck in this buffers load. We need to handle the case of
         the search string spanning two chunks of file now. We simply
         go back a bit in the file and read from there, thus inspecting
         some characters twice
       }
        Seek(F, FilePos(F) - Length(forString));
        bytesRemaining := bytesRemaining + Length(forString);
      end;
    end; { While }
  finally
    CloseFile(F);
    if SearchFor <> nil then StrDispose(SearchFor);
    if pBuf <> nil then FreeMem(pBuf, BufferSize);
  end;
end; procedure TFmPrincipal.Separacamposdelistas1Click(Sender: TObject);
begin
  Application.CreateForm(TFmAuxTxts, FmAuxTxts);
  FmAuxTxts.ShowModal;
  FmAuxTxts.Destroy;
end;

{ ScanFile }

procedure TFmPrincipal.SpeedButton1Click(Sender: TObject);
var
  Texto: String;
  J: Integer;
begin
  Texto := MeFiltro.Text;
  Memo2.Lines.Clear;
  Memo2.Lines.Add(Texto);
  //Memo2.Lines.Add('Letra 1 = ' + FormatFloat('000', Letra1) +
    //' e Letra 2 = ' + FormatFloat('000', Letra2));
  for J := 1 to Length(Texto) do
  begin
    Memo2.Lines.Add('   ' + Texto[J] + ' = ' + FormatFloat('000', Ord(Texto[J])));
  end;
end;

procedure TFmPrincipal.SpeedButton2Click(Sender: TObject);
var
  Texto, Novo: String;
  I, N: Integer;
begin
  N := 0;
  Memo5.Lines.Clear;
  for I := 0 to Memo4.Lines.Count - 1 do
  begin
    Application.ProcessMessages;
    Texto := Memo4.Lines[I];
    Novo := Geral.ConsertaTexto_Novo(Texto, N);
      Memo5.Lines.Add(Novo);
  end;
  ShowMessage(IntToStr(N) + ' modifica��es foram executadas!');
end;

function TFmPrincipal.AbreArquivoINI(Grade: TStringGrid; EdNomCli, EdCPFCli: TEdit;
  Data: TDateTime; Arquivo: String): Boolean;
begin
  //MyCBase.AbreArquivoINI(Grade, EdNomCli, EdCPFCli, Data, Arquivo);
  Result := False;
end;

procedure TFmPrincipal.Sair1Click(Sender: TObject);
begin
  Close;
end;

procedure TFmPrincipal.Novaconexao1Click(Sender: TObject);
begin
  Application.CreateForm(TFmConexao, FmConexao);
  FmConexao.ShowModal;
  FmConexao.Destroy;
end;

procedure TFmPrincipal.QrDatabasesBeforeClose(DataSet: TDataSet);
begin
  QrTabelas.Close;
end;

procedure TFmPrincipal.QrDatabasesAfterScroll(DataSet: TDataSet);
begin
  MyEx.Connected      := False;
//http://www.microolap.com/products/connectivity/mysqldac/help/FAQ/q_07.htm
  MyEx.ConnectionCharacterSet      := 'latin1';
  MyEx.ConnectionCollation         := 'latin1_swedish_ci';
  MyEx.Host           := MyDB.Host;
  MyEx.DatabaseName   := QrDatabases.Fields[0].AsString;
  MyEx.Port           := MyDB.Port;
  MyEx.UserName       := MyDB.UserName;
  MyEx.UserPassword   := MyDB.UserPassword;
  MyEx.Connected      := True;
  //
  ReopenTabelas();
end;

procedure TFmPrincipal.ExecutaQuery_A(Tipo: Integer);
var
  ini: TDateTime;
  Tempo: TDateTime;
  Agora, SQL, Texto, Num: String;
begin
  Screen.Cursor := crHourGlass;
  try
    Agora := FormatDateTime('yymmdd"_"hhnnss"_"zzz', Now());
    Application.ProcessMessages;
    STOpen.Caption := 'Aguarde. Processando...';
    try
      Update;
      Application.ProcessMessages;
      DBMemo1.DataField := '';
      QrExecuta.Close;
      QrExecuta.SQL.Clear;
      //
      SQL := MemoSQL.Lines.Text;
      if CkUNION.Checked then
      begin
        Texto := '';
        if (pos('####a', SQL) > 0)
        or (pos('####b', SQL) > 0)
        or (pos('####d', SQL) > 0) then
        begin
          MyObjects.Informa2(LaAviso1, LaAviso2, True,
          'Verificando clientes internos para uso do ####');
          QrTab.Close;
          QrTab.Database := QrExecuta.Database;
          //
          QrEntiCliInt.Close;
          QrEntiCliInt.Database := QrExecuta.Database;
          QrEntiCliInt.Open;
          ProgressBar1.Position := 0;
          ProgressBar1.Max := QrEntiCliInt.RecordCount;
          if QrEntiCliInt.RecordCount > 0 then
          begin
            QrEntiCliInt.First;
            while not QrEntiCliInt.Eof do
            begin
              Num := Geral.FFN(QrEntiCliIntCodCliInt.Value, 4);
              QrTab.Close;
              QrTab.SQL.Text := 'SHOW TABLES LIKE "' + EdTab.Text + Num + 'a"';
              QrTab.Open;
              //
              if QrTab.RecordCount > 0 then
              begin
                Texto := Texto + Geral.Substitui(SQL, '####', Num);
                if QrEntiCliInt.RecNo < QrEntiCliInt.RecordCount then
                begin
                  Texto := Texto + #13#10+#13#10 + 'UNION' + #13#10 + #13#10;
                end;
              end;
              QrEntiCliInt.Next;
            end;
            //
            SQL := Texto;
          end else
            Geral.MensagemBox(
            'N�o h� cadastros na tabela enticliint para uso do ####',
            'Mensagem', MB_OK+MB_ICONWARNING);
        end;
      end;
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Executando SQL');
      QrExecuta.SQL.Text := MeSQLPre.Text + ' ' + SQL + ' ' + MeSQLPos.Text;
      case Tipo of
        VK_F5:
        begin
          Ini := Now;
          try
            QrExecuta.Open;
            Tempo := Now - Ini;
            STOpen.Caption := IntToStr(QrExecuta.RecordCount)+' registros em '+
              FormatDateTime('s","zzzz" segundos"', Tempo);
          except
            dmkPF.LeMeuTexto(QrExecuta.SQL.Text);
            raise;
          end;
        end;
        VK_F6:
        begin
          STOpen.Caption := '';
          try
            QrExecuta.ExecSQL;
          except
            dmkPF.LeMeuTexto(QrExecuta.SQL.Text);
          end;
        end;
      end;
      Geral.SalvaTextoEmArquivo(FCaminho+Agora+'.SQL', QrExecuta.SQL.Text, True);
      ListBox1.Items.Add(FCaminho+Agora+'.SQL');
      ListBox1.ItemIndex := ListBox1.Items.Count-1;
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    except
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Erro na execu��o!');
      STOpen.Caption := 'Erro na execu��o!';
      raise;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPrincipal.FormActivate(Sender: TObject);
begin
  Timer1.Enabled := True;
end;

procedure TFmPrincipal.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  if FConexao = 0 then
  begin
    FConexao := FConexao + 1;
    Novaconexao1Click(Self);
  end;
end;

procedure TFmPrincipal.QrTabelasBeforeClose(DataSet: TDataSet);
begin
  QrCampos.Close;
end;

procedure TFmPrincipal.QrTabelasAfterScroll(DataSet: TDataSet);
begin
  ReopenCampos();
{
  if (PageControl3.ActivePageIndex = 12) and (QrTeste.State <> dsInactive) then
  begin
    BtRegistrosClick(Self);
  end;
}
end;

procedure TFmPrincipal.MegaSena1Click(Sender: TObject);
begin
  Application.CreateForm(TFmMegaSena, FMMegaSena);
  FMMegaSena.ShowModal;
  FMMegaSena.Destroy;
end;

procedure TFmPrincipal.MemoSQLKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key in ([VK_F5,VK_F6]) then
  begin
    case PageControl3.ActivePageIndex of
      1: ExecutaQuery_A(Key);
      2: BtConfirma2Click(Self);
      else Application.MessageBox('Defina a orelha de SQL!', 'Erro',
        MB_OK+MB_ICONERROR);
    end;
  end;
end;

procedure TFmPrincipal.Miscelnea1Click(Sender: TObject);
begin
  Application.CreateForm(TFmPesquisa, FmPesquisa);
  FmPesquisa.ShowModal;
  FmPesquisa.Destroy;
end;

procedure TFmPrincipal.MyExBeforeConnect(Sender: TObject);
begin
      MyEx.ConnectionCharacterSet      := 'latin1';
      MyEx.ConnectionCollation         := 'latin1_swedish_ci';
{
  case RGVersaoMySQL.ItemIndex of
    0: Geral.MB_Aviso('Informe a vers�o MySQL!');
    1:
    begin
      MyEx.ConnectionCharacterSet      := '';
      MyEx.ConnectionCollation         := '';
    end;
    2:
    begin
      //IFDEF DELPHI16_UP
      MyEx.ConnectionCharacterSet      := 'latin1';
      MyEx.ConnectionCollation         := 'latin1_swedish_ci';
    end;
    else Geral.MB_Aviso('Vers�o MySQL n�o implemetada!');
  end;
}
end;

procedure TFmPrincipal.BtAbrirClick(Sender: TObject);
begin
  if OpenDialog1.Execute then
  begin
     if FileExists(OpenDialog1.FileName) then
     begin
       Screen.Cursor := crHourGlass;
       MemoSQL.Lines.LoadFromFile(OpenDialog1.FileName);
       Screen.Cursor := crDefault;
     end;
  end;
end;

procedure TFmPrincipal.FormCreate(Sender: TObject);
begin
{$IFNDEF NAO_USA_DB}
  Application.OnMessage := MyObjects.FormMsg;
{$ENDIF}  
  FCaminho := ExtractFilePath(Application.ExeName) + '\SQLs\Executadas\';
  ForceDirectories(FCaminho);
  STOpen.Caption := '';
  STBatch.Caption := '';
  FLista := TStringList.Create;
  PageControl1.ActivePageIndex := 0;
  PageControl2.ActivePageIndex := 0;
  PageControl3.ActivePageIndex := 0;
  PageControl4.ActivePageIndex := 0;
  PageControl5.ActivePageIndex := 0;
  //
  Memo4.Text :=   'CAO = '#195#135#065#079' COE = '#195#135#079#069'CA = '#195#135#195#131'CO =  '#195#135#079;
  //
  GradeHTML.Cells[01,00] := 'CAMPO';
  GradeHTML.Cells[02,00] := 'DESCRI��O';
  GradeHTML.Cells[03,00] := 'TIPO';
  GradeHTML.Cells[04,00] := 'RELEV�NCIA';
  MyList.ConfiguracoesIniciais(1, Application.Title);
end;

procedure TFmPrincipal.FormDestroy(Sender: TObject);
begin
  FLista.Free;
end;

procedure TFmPrincipal.QrCamposCalcFields(DataSet: TDataSet);
begin
  QrCamposPADRAO.Value := QrCampos.FieldByName('Default').AsString;
  QrCamposTIPO.Value := QrCampos.FieldByName('Type').AsString;
  QrCamposCAMPO.Value := QrCampos.FieldByName('Field').AsString;
  QrCamposField_And_Type.Value := QrCamposCampo.Value+' '+QrCamposTIPO.Value;
end;

procedure TFmPrincipal.BtRegistrosClick(Sender: TObject);
begin
  Memo8.Visible := False;
  GridRegistros.Visible := True;
  TbRegistros.Close;
  TbRegistros.TableName := QrTabelas.Fields[0].AsString;
  if CkLimitar.Checked then
  begin
    TbRegistros.Limit := StrToInt(Ed2.Text);
    TbRegistros.Offset := StrToInt(Ed1.Text);
  end else begin
    TbRegistros.Limit := -1;
    TbRegistros.Offset := 0;
  end;
  if MeFiltro.Text <> '' then
  begin
    TbRegistros.Filter := MeFiltro.Text;
    TbRegistros.Filtered := True;
  end else TbRegistros.Filtered := False;
  TbRegistros.Open;
end;

procedure TFmPrincipal.GridCamposKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (key=VK_DELETE) and (Shift=[ssCtrl]) then
  begin
    if Geral.MB_Pergunta('Confirma a exclus�o do campo "'+
    String(QrCamposCAMPO.Value) + '" da tabela "'+
    QrTabelas.Fields[0].AsString + '" do banco de dados "'+
    QrDatabases.Fields[0].AsString+'"?') = ID_YES then
    begin
      QrExecuta.SQL.Clear;
      QrExecuta.SQL.Add('ALTER TABLE '+QrTabelas.Fields[0].AsString);
      QrExecuta.SQL.Add('DROP ' + String(QrCamposCAMPO.Value));
      QrExecuta.ExecSQL;
      //
      QrCampos.Close;
      QrCampos.Open;
    end;
  end;
  if (key=VK_F5) then
  begin
    QrCampos.Close;
    QrCampos.Open;
  end;
end;

procedure TFmPrincipal.GridTabelasKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (key=VK_DELETE) and (Shift=[ssCtrl]) then
  begin
    if Geral.MB_Pergunta('Confirma a exclus�o da tabela "'+
    QrTabelas.Fields[0].AsString+'" do banco de dados "'+
    QrDatabases.Fields[0].AsString+'"?') = ID_YES then
    begin
      QrExecuta.SQL.Clear;
      QrExecuta.SQL.Add('DROP TABLE '+QrTabelas.Fields[0].AsString);
      QrExecuta.ExecSQL;
      //
      QrTabelas.Close;
      QrTabelas.Open;
    end;
  end;
  if (key=VK_F5) then
  begin
    QrTabelas.Close;
    QrTabelas.Open;
  end;
end;

procedure TFmPrincipal.IBGE1Click(Sender: TObject);
begin
  {
  Application.CreateForm(TFmIBGE_DTB, FMIBGE_DTB);
  FMIBGE_DTB.ShowModal;
  FMIBGE_DTB.Destroy;
  }
end;

procedure TFmPrincipal.GridDatabasesKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (key=VK_F5) then
    ReopenDatabases(EdDBFiltro.Text, '');
end;

procedure TFmPrincipal.ListBox1DblClick(Sender: TObject);
begin
  //MemoSQL.Text := ListBox1.Items[ListBox1.ItemIndex];
  Geral.LeArquivoToMemo(ListBox1.Items[ListBox1.ItemIndex], TMemo(MemoSQL));
{
function TUnGeral.LeArquivoToMemo(Arquivo: String; Memo: TMemo): Boolean;
var
  F: TextFile;
  S: String;
begin
  Screen.Cursor := crHourGlass;
  Result := False;
  Memo.Lines.Clear;
  //Texto := '';
  try
    if FileExists(Arquivo) then
    begin
      AssignFile(F, Arquivo);
      Reset(F);
      while not Eof(F) do
      begin
        Readln(F, S);
        Memo.Lines.Add(S);
      end;
      //Memo.Text := Texto;
      CloseFile(F);
      Result := True;
     Screen.Cursor := crDefault;
    end;
  except
    Screen.Cursor := crDefault;
    raise;
  end;
end;
}
end;

procedure TFmPrincipal.LocnoDFMesubstnoPAS1Click(Sender: TObject);
begin
  Application.CreateForm(TFmPesquisa2, FmPesquisa2);
  FmPesquisa2.ShowModal;
  FmPesquisa2.Destroy;
end;

procedure TFmPrincipal.ltimosalvo1Click(Sender: TObject);
begin
  MLAGeral.LeArquivoToStringGrid(GradeHTML, FGradeHTML);
  EdLinhas.ValueVariant := GradeHTML.RowCount -1;
end;

procedure TFmPrincipal.BtRefreshClick(Sender: TObject);
begin
  ReopenDatabases(EdDbFiltro.Text, '');
end;

procedure TFmPrincipal.QrDatabasesAfterOpen(DataSet: TDataSet);
begin
  BtRefresh.Enabled := True;
end;

procedure TFmPrincipal.GridResultColEnter(Sender: TObject);
begin
  DBMemo1.DataField := GridResult.Columns[1].Title.Caption;
end;

procedure TFmPrincipal.ReCaptionComponentesDeForm(Form: TForm);
begin
  // N�o usa ainda!
end;

{
procedure TFmPrincipal.WriteAppKey(const Key, App: String; const Value: Variant; KeyType:
 TKeyType; xHKEY: HKEY);
var
  r : TRegistry;
begin
  r := TRegistry.Create;
  r.RootKey := xHKEY; //HKEY_LOCAL_MACHINE;
  try
    r.OpenKey('Software\'+App(*Application.Title*), True);
    case KeyType of
      ktString: r.WriteString(Key, Value);
      ktBoolean: r.WriteBool(Key, Value);
      ktInteger: r.WriteInteger(Key, Value);
      ktCurrency: r.WriteCurrency(Key, Value);
      ktDate: r.WriteDate(Key, Value);
      ktTime: r.WriteTime(Key, Value);
    end;
  finally
    r.Free;
  end;
end;
}

function TFmPrincipal.RecriaTabelaLocal(Tabela: String; Acao: Integer): Boolean;
begin
  // Compatibilidade
  //Result := Ucriar.RecriaTabelaLocal(Tabela, Acao);
  Result := False;
end;

procedure TFmPrincipal.EdLinhasExit(Sender: TObject);
var
  Linhas: Integer;
begin
  Linhas := EdLinhas.ValueVariant;
  if Linhas = 0 then Linhas := 1;
  GradeHTML.RowCount := Linhas + 1;
end;

procedure TFmPrincipal.EdLocTableChange(Sender: TObject);
begin
  ReopenTabelas();
end;

procedure TFmPrincipal.estes1Click(Sender: TObject);
begin
  Application.CreateForm(TFmDOSComando, FmDosComando);
  FmDOSComando.ShowModal;
  FmDOSComando.Destroy;
end;

procedure TFmPrincipal.BtConfRegClick(Sender: TObject);
begin
  if TbRegistros.State in ([dsInsert, dsEdit]) then TbRegistros.Post;
end;

procedure TFmPrincipal.BtConsertarTxtClick(Sender: TObject);
var
  Campo, Texto, Novo: String;
  //I, O, , T, P, N, E
  N, K: Integer;
begin
  N := 0;
  Memo2.Lines.Clear;
  for K := 0 to ListBox3.Items.Count -1 do
  begin
    Campo :=  ListBox3.Items[K];
    TbRegistros.First;
    while not TbRegistros.Eof do
    begin
      Application.ProcessMessages;
      Texto := TbRegistros.FieldByName(Campo).AsString;
      Novo := Geral.ConsertaTexto_Novo(Texto, N);
      if Novo <> Texto then
      begin
        TbRegistros.Edit;
        TbRegistros.FieldByName(Campo).Value := Novo;
        TbRegistros.Post;
      end;
      TbRegistros.Next;
    end;
  end;
  ShowMessage(IntToStr(N) + ' modifica��es foram executadas!');
end;

procedure TFmPrincipal.BtItensAMudarClick(Sender: TObject);
begin
end;

{
function TFmPrincipal.ConsertarFrase(const Texto: String; var N: Integer): String;
var
  Campo, Novo, Atual: String;
  //I, O, , T, P, N, E
  J: Integer;
  Letra1, Letra2: Byte;
begin
  Atual := Texto;
  Novo := Geral.ConsertaTexto_Novo(Atual, Letra1, Letra2);
  while Novo <> Atual do
  begin
    Atual := Novo;
    Novo := Geral.ConsertaTexto_Novo(Atual, Letra1, Letra2);
    N := N + 1;
    if Novo <> Atual then
      ShowMessage('Atual: ' + Atual + #13#10 + 'Novo: ' + Novo);
    (*
    Memo2.Lines.Add(Atual);
    for J := 1 to T do
    begin
      Memo2.Lines.Add('   ' + Atual[J] + ' = ' + FormatFloat('000', Ord(Atual[j])));
    end;
    *)
  end;
  if ((Letra1 = 195) and (Letra2 in ([1..9,12,13..31,122..255]))) or
    ((Letra1 = 199) and (Letra2 in ([1..9,12,13..31,122..255]))) then
  begin
    Memo2.Lines.Add(Atual);
    Memo2.Lines.Add('Letra 1 = ' + FormatFloat('000', Letra1) +
      ' e Letra 2 = ' + FormatFloat('000', Letra2));
    for J := 1 to Length(Atual) do
    begin
      Memo2.Lines.Add('   ' + Atual[J] + ' = ' + FormatFloat('000', Ord(Atual[J])));
    end;
  end;
  Result := Novo;
end;
}

procedure TFmPrincipal.TbRegistrosAfterEdit(DataSet: TDataSet);
begin
  BtConfReg.Enabled := True;
end;

procedure TFmPrincipal.TbRegistrosAfterInsert(DataSet: TDataSet);
begin
  BtConfReg.Enabled := True;
end;

procedure TFmPrincipal.TbRegistrosAfterPost(DataSet: TDataSet);
begin
  BtConfReg.Enabled := False;
end;

{procedure TFmPrincipal.TbVKDBFNTXAfterOpen(DataSet: TDataSet);
begin
  BtEstruturaDBF.Enabled := True;
end;

procedure TFmPrincipal.TbVKDBFNTXBeforeClose(DataSet: TDataSet);
begin
  BtEstruturaDBF.Enabled := False;
end;}

procedure TFmPrincipal.TbReg2AfterCancel(DataSet: TDataSet);
begin
    BtConfReg.Enabled := False;
end;

procedure TFmPrincipal.TbReg2AfterEdit(DataSet: TDataSet);
begin
  BtConfReg.Enabled := True;
end;

procedure TFmPrincipal.TbReg2AfterInsert(DataSet: TDataSet);
begin
  BtConfReg.Enabled := True;
end;

procedure TFmPrincipal.TbReg2AfterPost(DataSet: TDataSet);
begin
  BtConfReg.Enabled := False;
end;

procedure TFmPrincipal.TbRegistrosAfterCancel(DataSet: TDataSet);
begin
    BtConfReg.Enabled := False;
end;

procedure TFmPrincipal.BitBtn6Click(Sender: TObject);
var
  F: TextFile;
  S: String;
  L: Integer;
begin
  if OpenDialog1.Execute then
  begin
    AssignFile(F, OpenDialog1.FileName);
    Reset(F);
    L := 0;
    while not Eof(F) do
    begin
      Readln(F, S);
      Memo2SQL.Lines.Add(S);
      Inc(L,1);
      STBatch.Caption := 'Carregando linha '+IntToStr(L);
      Update;
      Application.ProcessMessages;
    end;
    CloseFile(F);
  end;
end;

procedure TFmPrincipal.BtConfirma2Click(Sender: TObject);
var
  i: integer;
  str: String;
begin
  Screen.Cursor := crHourGlass;
  try
    Application.ProcessMessages;
    STBatch.Caption := 'Aguarde. Processando...';
    try
      Update;
      Application.ProcessMessages;
      MySQLBExec.SQL.Clear;
      for i := 0 to Memo2SQL.Lines.Count - 1 do
      begin
        if Length(Memo2SQL.Lines[i]) > 2 then
        begin
          Str := Copy(Memo2SQL.Lines[i], 1, 2);
          MySQLBExec.SQL.Add(Memo2SQL.Lines[i]);
        end;
      end;
      MySQLBExec.ExecSQL;
    except
      STBatch.Caption := 'Erro na execu��o!';
      raise;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPrincipal.BitBtn5Click(Sender: TObject);
begin
  Memo1.Lines.Clear;
  if EdTabela.Text <> '' then FTabela := EdTabela.Text else
    FTabela := QrTabelas.Fields[0].AsString;
  if InputQuery('Nome da Tabela no Delphi', 'Informe o nome da tabela no delphi:',
    FTabela) then
  begin
    if CkListaDBF.Checked    then ExportaEstrututaCriacaoTabela;
    if CkComponentes.Checked then ExportaComponentesCampos;
    if CkVariaveis.Checked   then ExportaVariaveisCampos;
    if CkCriaCampos.Checked  then ExportaCriacaoCompenentes;
    if CkValores.Checked     then ExportaConferenciaDeValores;
  end;
end;

procedure TFmPrincipal.ExportaEstrututaCriacaoTabela;
const
  C2 = ', ''''';
  C3 = ',';
var
  V0, V1, V2, Nome, Size, Deci: String;
  p1, p2, p3: Integer;
begin
  Memo1.Lines.Add(EdTPre1.Text+EdTPos1.Text);
  Memo1.Lines.Add(EdTPre1.Text+'* Defini��o da tabela master "'+
    QrTabelas.Fields[0].AsString+'"'+EdTPos1.Text);
  Memo1.Lines.Add(EdTPre1.Text+'DefineFile('''''+QrTabelas.Fields[0].AsString+
    ''''')'+EdTPos1.Text);
  Memo1.Lines.Add(EdTPre1.Text+'  Description(''''Tabela master '+
    QrTabelas.Fields[0].AsString+''''')'+EdTPos1.Text);

  QrCampos.First;
  while not QrCampos.Eof do
  begin
    p1 := pos('(', String(QrCamposTIPO.Value));
    if p1 > 0 then
    begin
      Nome := Trim(Copy(String(QrCamposTIPO.Value), 1, p1-1));
      p2 := pos(',', String(QrCamposTIPO.Value));
      if p2 > 0 then
      begin
        p3 := pos(')', String(QrCamposTIPO.Value));
        Size := Trim(Copy(String(QrCamposTIPO.Value), p1+1, p2-p1-1));
        Deci := Trim(Copy(String(QrCamposTIPO.Value), p2+1, p3-p2-1));
      end else begin
        p2 := pos(')', String(QrCamposTIPO.Value));
        Size := Trim(Copy(String(QrCamposTIPO.Value), p1+1, p2-p1-1));
        Deci := '0';
      end;
    end else begin
      Nome := String(QrCamposTIPO.Value);
      Size := '0';
      Deci := '0';
    end;
    V1 := Copy(String(QrCamposCAMPO.Value), 1, 10);
    V1 := Geral.CompletaString(V1+''''+'''', ' ', 12, taLeftJustify, True);
    if (Uppercase(Nome) = Uppercase('int'      ))
    or (Uppercase(Nome) = Uppercase('tinyint'  ))
    or (Uppercase(Nome) = Uppercase('mediumint'))
    or (Uppercase(Nome) = Uppercase('bigint'   ))
    or (Uppercase(Nome) = Uppercase('integer'  )) then V2 := 'N' else
    if  Uppercase(Nome) = Uppercase('date'     )  then V2 := 'D' else
    if  Uppercase(Nome) = Uppercase('datetime' )  then V2 := 'T' else
    if (Uppercase(Nome) = Uppercase('float'    ))
    or (Uppercase(Nome) = Uppercase('double'   )) then V2 := 'F' else
    if (Uppercase(Nome) = Uppercase('varchar'  ))
    or (Uppercase(Nome) = Uppercase('char'     )) then V2 := 'C' else
    if  Uppercase(Nome) = Uppercase('memo'     )  then V2 := 'M' else
    if (Uppercase(Nome) = Uppercase('blob'     ))
    or (Uppercase(Nome) = Uppercase('text'     ))
    or (Uppercase(Nome) = Uppercase('longtext' ))
    or (Uppercase(Nome) = Uppercase('longblob' )) then
    begin
      if  (Uppercase(Geral.SoNumeroELetra_TT(V1)) <> 'LOGO' )
      and (Uppercase(Geral.SoNumeroELetra_TT(V1)) <> 'LOGO2') then
        V2 := 'M' else V2 := '?';
    end else V2 := '?';
    if V2 = '?' then
      Geral.MB_Aviso('Tipo n�o implementado: ' + Nome + ' ( Campo: '+V1+' )');
    if V2 = '?' then V0 := '//' else V0 := '';
    //
    if (V2 = 'N') and (Size = '11') then
      Size := ' 8';
    V2 := V2 + ''''+ '''';
    while Length(Size) < 5 do Size := ' ' + Size;
    while Length(Deci) < 3 do Deci := ' ' + Deci;
    Memo1.Lines.Add(V0 + EdTPre1.Text + EdCPre1.Text + V1 + C2 + V2 + C3 + Size +
      C3 + Deci + EdCPos1.Text + EdTPos1.Text);
    QrCampos.Next;
  end;
  Memo1.Lines.Add(EdTPre1.Text+EdTPos1.Text);
end;

procedure TFmPrincipal.ExportaComponentesCampos;
const
  C2 = ', ''''';
  C3 = ',';
var
  v1: String;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('##########################################################');
  Memo1.Lines.Add('############                       #######################');
  Memo1.Lines.Add('############ COMPONENTES  - CAMPOS #######################');
  Memo1.Lines.Add('############                       #######################');
  Memo1.Lines.Add('##########################################################');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('    // Componentes (Campos) da tabela "'+
    QrTabelas.Fields[0].AsString+'"');
  QrCampos.First;
  while not QrCampos.Eof do
  begin
    V1 := Copy(String(QrCamposCAMPO.Value), 1, 10);
    V1 := Geral.CompletaString(V1, ' ', 10, taLeftJustify, True);
    V1 := FTabela + V1;
    Memo1.Lines.Add(EdTPre2.Text + EdCPre2.Text + V1 + EdCPos2.Text + EdTPos2.Text);
    QrCampos.Next;
  end;
  Memo1.Lines.Add('');
end;

procedure TFmPrincipal.ExportaVariaveisCampos;
const
  C2 = ', ''''';
  C3 = ',';
var
  V0, V1, V2, Nome, Size, Deci: String;
  p1, p2, p3: Integer;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('##########################################################');
  Memo1.Lines.Add('############                          ####################');
  Memo1.Lines.Add('############ COMPONENTES  - VARI�VEIS ####################');
  Memo1.Lines.Add('############                          ####################');
  Memo1.Lines.Add('##########################################################');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('    // Vari�veis (Campos) da tabela "'+
    QrTabelas.Fields[0].AsString+'"');
  QrCampos.First;
  while not QrCampos.Eof do
  begin
    p1 := pos('(', String(QrCamposTIPO.Value));
    if p1 > 0 then
    begin
      Nome := Trim(Copy(String(QrCamposTIPO.Value), 1, p1-1));
      p2 := pos(',', String(QrCamposTIPO.Value));
      if p2 > 0 then
      begin
        p3 := pos(')', String(QrCamposTIPO.Value));
        Size := Trim(Copy(String(QrCamposTIPO.Value), p1+1, p2-p1-1));
        Deci := Trim(Copy(String(QrCamposTIPO.Value), p2+1, p3-p2-1));
      end else begin
        p2 := pos(')', String(QrCamposTIPO.Value));
        Size := Trim(Copy(String(QrCamposTIPO.Value), p1+1, p2-p1-1));
        Deci := '0';
      end;
    end else begin
      Nome := String(QrCamposTIPO.Value);
      Size := '0';
      Deci := '0';
    end;
    V1 := Copy(String(QrCamposCAMPO.Value), 1, 10);
    V1 := EdCPre3.Text + V1;
    V1 := Geral.CompletaString(V1, ' ', 10+Length(EdCPre3.Text), taLeftJustify, True);
    V1 := FTabela + V1;
    if  (Uppercase(Nome) = Uppercase('int'      ))
    or  (Uppercase(Nome) = Uppercase('tinyint'  ))
    or  (Uppercase(Nome) = Uppercase('mediumint'))
    or  (Uppercase(Nome) = Uppercase('bigint'   ))
    or  (Uppercase(Nome) = Uppercase('integer'  )) then V2 := 'Integer' else
    if  Uppercase(Nome) = Uppercase('date'     ) then V2 := 'Double' else
    if  Uppercase(Nome) = Uppercase('datetime' ) then V2 := 'array[0..8] of char' else
    if  Uppercase(Nome) = Uppercase('double'   ) then V2 := 'Double' else
    if  (Uppercase(Nome) = Uppercase('varchar'  ))
    or  (Uppercase(Nome) = Uppercase('char'     )) then
      V2 := 'array[0..'+IntToStr(StrToInt(Size)+0)+'] of char' else
    if  Uppercase(Nome) = Uppercase('memo'     ) then V2 := 'M' else
    if (Uppercase(Nome) = Uppercase('blob'     ))
    or (Uppercase(Nome) = Uppercase('text'     ))
    or (Uppercase(Nome) = Uppercase('longtext' ))
    or (Uppercase(Nome) = Uppercase('longblob' )) then
    begin
      if  (Uppercase(Geral.SoNumeroELetra_TT(V1)) <> 'LOGO' )
      and (Uppercase(Geral.SoNumeroELetra_TT(V1)) <> 'LOGO2') then
        V2 := 'M' else V2 := '?';
    end else V2 := '?';
    if V2 = '?' then
      Geral.MB_Aviso('Tipo n�o implementado: '+
      Nome+' ( Campo: '+V1+' )');
    if V2 = '?' then V0 := '//' else V0 := '';
    while Length(Size) < 5 do Size := ' ' + Size;
    while Length(Deci) < 3 do Deci := ' ' + Deci;
    Memo1.Lines.Add(EdTPre3.Text + V1  + EdCPos3.Text + V2 + EdTPos3.Text);
    QrCampos.Next;
  end;
  Memo1.Lines.Add('');
end;

(*procedure TFmPrincipal.ExportaVariaveisCampos;
const
  C2 = ', ''''';
  C3 = ',';
var
  V0, V1, V2, V3, Nome, Size, Deci: String;
  p1, p2, p3: Integer;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('##########################################################');
  Memo1.Lines.Add('############                          ####################');
  Memo1.Lines.Add('############ COMPONENTES  - VARI�VEIS ####################');
  Memo1.Lines.Add('############                          ####################');
  Memo1.Lines.Add('##########################################################');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('    // Vari�veis (Campos) da tabela "'+
    QrTabelas.Fields[0].AsString+'"');
  QrCampos.First;
  while not QrCampos.Eof do
  begin
    p1 := pos('(', QrCamposTIPO.Value);
    if p1 > 0 then
    begin
      Nome := Trim(Copy(QrCamposTIPO.Value, 1, p1-1));
      p2 := pos(',', QrCamposTIPO.Value);
      if p2 > 0 then
      begin
        p3 := pos(')', QrCamposTIPO.Value);
        Size := Trim(Copy(QrCamposTIPO.Value, p1+1, p2-p1-1));
        Deci := Trim(Copy(QrCamposTIPO.Value, p2+1, p3-p2-1));
      end else begin
        p2 := pos(')', QrCamposTIPO.Value);
        Size := Trim(Copy(QrCamposTIPO.Value, p1+1, p2-p1-1));
        Deci := '0';
      end;
    end else begin
      Nome := QrCamposTIPO.Value;
      Size := '0';
      Deci := '0';
    end;
    V1 := Copy(QrCamposCAMPO.Value, 1, 10);
    V1 := V1 + EdCPre3.Text;
    V1 := Geral.CompletaString(V1, ' ', 10+Length(EdCPre3.Text), taLeftJustify, True);
    V1 := FTabela + V1;
    if  (Uppercase(Nome) = Uppercase('int'      ))
    or  (Uppercase(Nome) = Uppercase('tinyint'  ))
    or  (Uppercase(Nome) = Uppercase('mediumint'))
    or  (Uppercase(Nome) = Uppercase('bigint'   ))
    or  (Uppercase(Nome) = Uppercase('integer'  )) then V2 := 'Integer' else
    if  Uppercase(Nome) = Uppercase('date'     ) then V2 := 'Double' else
    if  Uppercase(Nome) = Uppercase('datetime' ) then V2 := 'array[0..8] of char' else
    if  Uppercase(Nome) = Uppercase('double'   ) then V2 := 'Double' else
    if  (Uppercase(Nome) = Uppercase('varchar'  ))
    or  (Uppercase(Nome) = Uppercase('char'     )) then
      V2 := 'array[0..'+IntToStr(StrToInt(Size)+0)+'] of char' else
    if  Uppercase(Nome) = Uppercase('memo'     ) then V2 := 'M' else
    if (Uppercase(Nome) = Uppercase('blob'     ))
    or (Uppercase(Nome) = Uppercase('text'     ))
    or (Uppercase(Nome) = Uppercase('longtext' ))
    or (Uppercase(Nome) = Uppercase('longblob' )) then
    begin
      if  (Uppercase(Geral.SoNumeroELetra_TT(V1)) <> 'LOGO' )
      and (Uppercase(Geral.SoNumeroELetra_TT(V1)) <> 'LOGO2') then
        V2 := 'M' else V2 := '?';
    end else V2 := '?';
    if V2 = '?' then
      Geral.MB_Avisa('Tipo n�o implementado: '+
      Nome+' ( Campo: '+V1+' )');
    if V2 = '?' then V0 := '//' else V0 := '';
    while Length(Size) < 5 do Size := ' ' + Size;
    while Length(Deci) < 3 do Deci := ' ' + Deci;
    Memo1.Lines.Add(EdTPre3.Text + V1  + EdCPos3.Text + V2 + EdTPos3.Text);
    QrCampos.Next;
  end;
  Memo1.Lines.Add('');
end;*)

procedure TFmPrincipal.ExportaCriacaoCompenentes;
const
  C2 = ', ''''';
  C3 = ',';
var
  V1, Nome, Size, Deci: String;
  p1, p2, p3: Integer;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('##########################################################');
  Memo1.Lines.Add('############                          ####################');
  Memo1.Lines.Add('############ COMPONENTES  - CRIA��O   ####################');
  Memo1.Lines.Add('############                          ####################');
  Memo1.Lines.Add('##########################################################');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('    // Cria��o dos campos da tabela "'+
    QrTabelas.Fields[0].AsString+'"');
  Memo1.Lines.Add(EdTPre4.Text +QrTabelas.Fields[0].AsString+
    ' := d4open(FcbCode_base, PChar(VAR_DBPATH + '''+
    QrTabelas.Fields[0].AsString+'.DBF''));');
  Memo1.Lines.Add('    Tag_'+ FTabela+ '_Codi := d4tag(FcbDf'+
    FTabela+', ''TAG_CODIGO'');');
  QrCampos.First;
  while not QrCampos.Eof do
  begin
    p1 := pos('(', String(QrCamposTIPO.Value));
    if p1 > 0 then
    begin
      Nome := Trim(Copy(String(QrCamposTIPO.Value), 1, p1-1));
      p2 := pos(',', String(QrCamposTIPO.Value));
      if p2 > 0 then
      begin
        p3 := pos(')', String(QrCamposTIPO.Value));
        Size := Trim(Copy(String(QrCamposTIPO.Value), p1+1, p2-p1-1));
        Deci := Trim(Copy(String(QrCamposTIPO.Value), p2+1, p3-p2-1));
      end else begin
        p2 := pos(')', String(QrCamposTIPO.Value));
        Size := Trim(Copy(String(QrCamposTIPO.Value), p1+1, p2-p1-1));
        Deci := '0';
      end;
    end else begin
      Nome := String(QrCamposTIPO.Value);
      Size := '0';
      Deci := '0';
    end;
    V1 := Copy(String(QrCamposCAMPO.Value), 1, 10);
    V1 := Geral.CompletaString(V1, ' ', 10, taLeftJustify, True);
    V1 := FTabela + V1;
    Memo1.Lines.Add(EdTPre4.Text + V1 + EdCPre4.Text + FTabela  + EdCPos4.Text +
      Geral.CompletaString(String(QrCamposCAMPO.Value), ' ', 10, taLeftJustify, True) +
      EdTPos4.Text);
    QrCampos.Next;
  end;
  Memo1.Lines.Add('');
end;

procedure TFmPrincipal.ExportaConferenciaDeValores;
const
  C2 = ', ''''';
  C3 = ',';
var
  V0, V1, V2, V3, V4, V5, Nome, Size, Deci: String;
  p1, p2, p3: Integer;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('##########################################################');
  Memo1.Lines.Add('############                          ####################');
  Memo1.Lines.Add('############ CONFER�NCIA DE VALORES   ####################');
  Memo1.Lines.Add('############                          ####################');
  Memo1.Lines.Add('##########################################################');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('    // Conferencia de valores �s vari�veis "'+
    QrTabelas.Fields[0].AsString+'"');

  QrCampos.First;
  while not QrCampos.Eof do
  begin
    p1 := pos('(', String(QrCamposTIPO.Value));
    if p1 > 0 then
    begin
      Nome := Trim(Copy(String(QrCamposTIPO.Value), 1, p1-1));
      p2 := pos(',', String(QrCamposTIPO.Value));
      if p2 > 0 then
      begin
        p3 := pos(')', String(QrCamposTIPO.Value));
        Size := Trim(Copy(String(QrCamposTIPO.Value), p1+1, p2-p1-1));
        Deci := Trim(Copy(String(QrCamposTIPO.Value), p2+1, p3-p2-1));
      end else begin
        p2 := pos(')', String(QrCamposTIPO.Value));
        Size := Trim(Copy(String(QrCamposTIPO.Value), p1+1, p2-p1-1));
        Deci := '0';
      end;
    end else begin
      Nome := String(QrCamposTIPO.Value);
      Size := '0';
      Deci := '0';
    end;
    V1 := Copy(String(QrCamposCAMPO.Value), 1, 10);
    V1 := Geral.CompletaString(V1(*+''''+''''*), ' ', 10, taLeftJustify, True);
    if  (Uppercase(Nome) = Uppercase('int'      ))
    or  (Uppercase(Nome) = Uppercase('tinyint'  ))
    or  (Uppercase(Nome) = Uppercase('mediumint'))
    or  (Uppercase(Nome) = Uppercase('bigint'   ))
    or  (Uppercase(Nome) = Uppercase('integer'  )) then V2 := 'f4int(' else
    if  Uppercase(Nome) = Uppercase('date'     ) then V2 := 'f4str(' else
    if  Uppercase(Nome) = Uppercase('datetime' ) then V2 := 'f4str(' else
    if  Uppercase(Nome) = Uppercase('double'   ) then V2 := 'f4double(' else
    if  (Uppercase(Nome) = Uppercase('varchar'  ))
    or  (Uppercase(Nome) = Uppercase('char'     )) then
      V2 := 'f4str(' else
    if  Uppercase(Nome) = Uppercase('memo'     ) then V2 := 'f4memostr(' else
    if (Uppercase(Nome) = Uppercase('blob'     ))
    or (Uppercase(Nome) = Uppercase('text'     ))
    or (Uppercase(Nome) = Uppercase('longtext' ))
    or (Uppercase(Nome) = Uppercase('longblob' )) then
    begin
      if  (Uppercase(Geral.SoNumeroELetra_TT(V1)) <> 'LOGO' )
      and (Uppercase(Geral.SoNumeroELetra_TT(V1)) <> 'LOGO2') then
        V2 := 'M' else V2 := '?';
    end else V2 := '?';
    if V2 = '?' then
      Geral.MB_Aviso('Tipo n�o implementado: ' + Nome+' ( Campo: '+V1+' )');
    if V2 = '?' then V0 := '//' else V0 := '';
    if V2 <> 'f4str(' then
    begin
      V3 := '';
      V4 := '';
      V5 := EdTPre5.Text + FTabela + EdCPre5.Text + V1  + EdCPos5.Text;
    end else begin
      V3 := 'StrLCopy('+Trim(EdTPre5.Text)+FTabela+EdCPre5.Text+Trim(V1)+', ';
      V4 := '), SizeOf('+Trim(EdTPre5.Text)+FTabela+EdCPre5.Text+Trim(V1)+')';
      V5 := '  ';
    end;
    while Length(Size) < 5 do Size := ' ' + Size;
    while Length(Deci) < 3 do Deci := ' ' + Deci;
    Memo1.Lines.Add(V0 + V5 + V3 + V2 + Trim(EdTPre5.Text) + FTabela + Trim(V1) + V4 + EdTPos5.Text);
    QrCampos.Next;
  end;
  Memo1.Lines.Add('');
end;

procedure TFmPrincipal.ReopenCampos();
var
  Campos: String;
begin
{
  QrCamposTIPO.Free;
  case RGVersaoMySQL.ItemIndex of
    1:
    begin
      QrCamposTIPO := TStringField.Create(Self);
      with QrCamposTIPO do
      begin
        FieldName = 'Type'
        Required = True
        BlobType = ftMemo
        Size = 4
      end;
    end;
  end;
}
  if Trim(EdCampo.Text) <> '' then
    Campos := ' "%' + EdCampo.Text + '%"'
  else
    Campos := '';
  QrCampos.Close;
  //
  if (QrTabelas.State = dsBrowse) then
    if QrTabelas.RecordCount > 0 then
    begin
      QrCampos.SQL.Clear;
      QrCampos.SQL.Add('DESCRIBE '+QrTabelas.Fields[0].AsString + Campos);
      QrCampos.Open;
    end;
end;

procedure TFmPrincipal.ReopenDatabases(DBSel, DBLoc: String);
var
  MyDB: String;
var
  DBAtu: String;
begin
  if Trim(DBLoc) <> '' then
    DBAtu := DBLoc
  else
  begin
    if QrDatabases.State <> dsInactive then
      DBAtu := QrDatabases.Fields[0].AsString
    else
      DBAtu := '';
  end;
  //
  MyDB := Trim(DBSel);
  //
  QrDatabases.Close;
  QrDatabases.SQL.Clear;
  QrDatabases.SQL.Add('SHOW DATABASES ');

  if MyDB <> '' then
    QrDatabases.SQL.Add('LIKE "%' + MyDB + '%"');

  QrDatabases.Open;
  //
  MyDB := Trim(DBAtu);
  try
    if MyDB <> '' then
      QrDatabases.Locate('database', MyDB, [loCaseInsensitive]);
  except
    ;
  end;
end;

procedure TFmPrincipal.ReopenTabelas;
begin
  QrTabelas.Close;
  QrTabelas.SQL.Clear;
  QrTabelas.SQL.Add('SHOW TABLES FROM '+qrdatabases.fields[0].asstring +
    ' LIKE "%' + EdLocTable.Text + '%"');
  QrTabelas.Open;
  //
  //QrTabelas.Locate('Tables_in_'+QrDatabases.Fields[0].AsString, EdLocTable.Text,
  //[loCaseInsensitive, loPartialKey]);
end;

procedure TFmPrincipal.BitBtn7Click(Sender: TObject);
var
  Valor: Double;
  i: Integer;
  Campo: String;
  Achou: Boolean;
begin
  Campo := '';
  if InputQuery('Soma de valores', 'Informe o campo a somar', Campo) then
  begin
    Achou := False;
    for i := 0 to GridResult.Columns.Count -1 do
      if GridResult.Columns[i].FieldName = Campo then Achou := True;
    if Achou = False then
      Geral.MB_Aviso('O campo "' + Campo + '" n�o existe!') else
    begin
      Valor := 0;
      QrExecuta.First;
      while not QrExecuta.Eof do
      begin
        Valor := Valor + QrExecuta.FieldByName(Campo).AsFloat;
        QrExecuta.Next;
      end;
      ShowMessage(FloatToStr(Valor));
    end;
  end;
end;

procedure TFmPrincipal.BitBtn8Click(Sender: TObject);
begin
  ListaTabsComReg();
end;
procedure TFmPrincipal.BitBtn9Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Exclui TODOS registros de das tabelas ' +
  'selecionadas?') = ID_YES then
  begin
    QrExecuta.Close;
    QrMinhas.First;
    while not QrMinhas.Eof do
    begin
      if QrMinhasSelecio.Value = 1 then
      begin
        QrExecuta.SQL.Clear;
        QrExecuta.SQL.Add('DELETE FROM ' + String(QrMinhasTabela.Value));
        QrExecuta.ExecSQL;
      end;
      QrMinhas.Next;
    end;
    QrExecuta.SQL.Clear;
    ListaTabsComReg();
  end;
end;

function TFmPrincipal.LiberaPelaSenhaAdmin: Boolean;
begin
  Result := False;
{
  VAR_FSENHA := 3;
  if MyObjects.CriaForm_AcessoTotal(TFmSenha, FmSenha) then
  begin
    FmSenha.ShowModal;
    FmSenha.Destroy;
  end;
  Result := VAR_SENHARESULT = 2;
}
end;

procedure TFmPrincipal.ListadeProcessos1Click(Sender: TObject);
begin
  Application.CreateForm(TFmProcessList, FmProcessList);
  FmProcessList.ShowModal;
  FmProcessList.Destroy;
end;

procedure TFmPrincipal.ListaTabsComReg();
var
  n: Integer;
begin
  MySQL.Connected      := False;
  MySQL.Host           := MyDB.Host;
  MySQL.DatabaseName   := 'mysql';
  MySQL.Port           := MyDB.Port;
  MySQL.UserName       := MyDB.UserName;
  MySQL.UserPassword   := MyDB.UserPassword;
  MySQL.Connected      := True;
  //
  QrMy.Close;
  QrMy.SQL.Clear;
  QrMy.SQL.Add('DROP TABLE IF EXISTS Minhas');
  QrMy.ExecSQL;
  //
  QrMy.SQL.Clear;
  QrMy.SQL.Add('CREATE TABLE Minhas (');
  QrMy.SQL.Add('  Tabela     varchar(30), ');
  QrMy.SQL.Add('  Registros  int(11) NOT NULL DEFAULT 0, ');
  QrMy.SQL.Add('  Selecio    tinyint(1) NOT NULL DEFAULT 0, ');
  QrMy.SQL.Add('  Observaco  varchar(255)');
  QrMy.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  QrMy.ExecSQL;
  //
  QrMy.SQL.Clear;
  QrMy.SQL.Add('INSERT INTO minhas SET ');
  QrMy.SQL.Add(' Tabela=:P0, ');
  QrMy.SQL.Add(' Registros=:P1');
  QrTabelas.First;
  while not QrTabelas.Eof do
  begin
    QrExecuta.Close;
    QrExecuta.SQL.Clear;
    QrExecuta.SQL.Add('SELECT COUNT(*) Registros FROM ' + QrTabelas.Fields[0].AsString);
    QrExecuta.Open;
    n := QrExecuta.FieldByName('Registros').AsInteger;
    if (n > 0) or (CkTabZerada.Checked) then
    begin
      QrMy.Params[0].AsString  := QrTabelas.Fields[0].AsString;
      QrMy.Params[1].AsInteger := n;
      //Geral.LeMeuSQLyForce(QrMy, '', nil, True, True, True);
      QrMy.ExecSQL;
    //
    end;
    //
    QrTabelas.Next;
  end;
  QrMinhas.Close;
  QrMinhas.Open;
end;

function TFmPrincipal.Posicao(Campo: String; NaoCopiar: array of String): Integer;
var
  i: Integer;
begin
  Result := 0;
  for i := 0 to 6 do
    if NaoCopiar[i] = Campo then
    begin
      Result := i + 1;
      Exit;
    end;
end;

procedure TFmPrincipal.Button1Click(Sender: TObject);
var
  Txt_00, Txt_01, Txt_02, Txt_03, Txt_04, Txt_11, Txt_12: String;
  P, Conta: Integer;
  Campo, DefVarStr, DefVarInt, DefVarDbl: String;

begin
  DefVarStr := '';
  DefVarInt := '';
  DefVarDbl := '';
  Txt_00 := '';
  //
  Memo3.Lines.Clear;
  Txt_11 := '? := UMyMod.BuscaEmLivreY_Def(''' +
  QrTabelas.Fields[0].AsString + ''', ';
  //
  Txt_12 := 'ou > ? := UMyMod.BPGS1I32(''' +
  QrTabelas.Fields[0].AsString +''', ';//
  //
  Txt_01 := 'if UMyMod.SQLInsUpd_IGNORE?(Dmod.QrUpd?, ImgTipo.SQLType?, ''' +
  QrTabelas.Fields[0].AsString + ''', auto_increment?[' +  #13#10;
  Txt_02 := '';
  Txt_03 :=  #13#10;
  Txt_04 := '';
  Conta := -1;
  QrCampos.First;
  while not QrCampos.Eof do
  begin
    if Posicao(Uppercase(String(QrCamposCAMPO.Value)), FNaoCopiar) = 0 then
    begin
      Campo := Lowercase(String(QrCamposTIPO.Value));
      P := pos('(', Campo);
      if P > 0 then
        Campo := Copy(Campo, 1, P-1);

      if (Campo = 'date') or (Campo = 'datetime') or (Campo = 'time')
      or (Campo = 'year') or (Campo = 'timestamp') or (Campo = 'char')
      or (Campo = 'varchar') or (Campo = 'text') or (Campo = 'tinytext')
      or (Campo = 'mediumtext') or (Campo = 'longblob') or (Campo = 'blob')
      or (Campo = 'tinyblob') or (Campo = 'mediumblob') or (Campo = 'longblob')
      or (Campo = 'bynary') or (Campo = 'varbinary') or (Campo = 'enum')
      or (Campo = 'set') then
      begin
        if DefVarStr <> '' then
          DefVarStr := DefVarStr + ', ';
        DefVarStr := DefVarStr + String(QrCamposCAMPO.Value);
      end else
      if (Campo = 'int') or (Campo = 'tinyint') or (Campo = 'mediumint')
      or (Campo = 'longint') or (Campo = 'integer') or (Campo = 'smallint') then
      begin
        if DefVarInt <> '' then
          DefVarInt := DefVarInt + ', ';
        DefVarInt := DefVarInt + String(QrCamposCAMPO.Value);
      end else
      if (Campo = 'bigint') or (Campo = 'double') or (Campo = 'decimal')
      or (Campo = 'numeric') or (Campo = 'float') or (Campo = 'real')
      or (Campo = 'double precision') or (Campo = 'dec') or (Campo = 'fixed') then
      begin
        if DefVarDbl <> '' then
          DefVarDbl := DefVarDbl + ', ';
        DefVarDbl := DefVarDbl + String(QrCamposCAMPO.Value);
      end;
      //
      Txt_00 := Txt_00 + '  ' + Geral.CompletaString(String(QrCamposCAMPO.Value), ' ',
        15, taLeftJustify, False) + ':= ;' + #13#10;
{
    end;
    //
    if Posicao(Uppercase(QrCamposCAMPO.Value), FNaoCopiar) = 0 then
    begin
}
      if QrCamposKey.Value = 'PRI'  then
      begin
        Txt_02 := Txt_02 + '''' + String(QrCamposCAMPO.Value) + ''', ';
        Txt_04 := Txt_04 + String(QrCamposCAMPO.Value) + ', ';
      end else begin
        Inc(Conta, 1);
        //
        if Conta = 3 then
        begin
          Txt_01  := Txt_01 + #13#10;
          Txt_03  := Txt_03 + #13#10;
          Conta := 0;
        end;
        Txt_01 := Txt_01 + '''' + String(QrCamposCAMPO.Value) + ''', ';
        Txt_03 := Txt_03 + String(QrCamposCAMPO.Value) + ', ';
      end;
    end;
    QrCampos.Next;
  end;
  Txt_01 := Copy(Txt_01, 1, Length(Txt_01) - 2) + '], [';
  if Length(Txt_02) > 1 then Txt_02 := Copy(Txt_02, 1, Length(Txt_02) - 2);
  Txt_03 := Copy(Txt_03, 1, Length(Txt_03) - 2) + '], [';
  if Length(Txt_04) > 0 then Txt_04 := Copy(Txt_04, 1, Length(Txt_04) - 2);
  //
  Memo3.Lines.Add('var');
  if DefVarStr <> '' then
    Memo3.Lines.Add('  ' + DefVarStr + ': String;');
  if DefVarInt <> '' then
    Memo3.Lines.Add('  ' + DefVarInt + ': Integer;');
  if DefVarDbl <> '' then
    Memo3.Lines.Add('  ' + DefVarDbl + ': Double;');
  //
  Memo3.Lines.Add('begin');
  Memo3.Lines.Add(Txt_00);
  Memo3.Lines.Add('  //');
  //
  Memo3.Lines.Add(Txt_11 + Txt_02 + ', ImgTipo.SQLType?, CodAtual?);');
  Memo3.Lines.Add(Txt_12 + Txt_02 + ', '''', '''', tsPosNeg?, stInsUpd?, CodAtual?);');
  Memo3.Lines.Add(Txt_01  + #13#10 + Txt_02 + '], [' + Txt_03  +
    #13#10 + Txt_04 + '], UserDataAlterweb?, IGNORE?');
end;

//Salvar e abrir SQLs para uso posterior
//Mostrar servidor, porta e outros dados da conex�o ativa

procedure TFmPrincipal.BtTiraClick(Sender: TObject);
var
  I, PIni, PFim, PStp: Integer;
  Texto, Txt: String;
  Mudou: Boolean;
begin
  PFim := 0;
  Mudou := False;
  for I := 0 to MemoSQL.Lines.Count -1 do
  begin
    Texto := MemoSQL.Lines[I];
    PIni := pos('''', Texto);
    //
    Txt := Copy(Texto, PIni + 1);
    PStp := pos('''', Txt);
    if PStp > 0 then PFim := PStp;
    while PStp > 0 do
    begin
      Txt := Copy(Txt, PStp + 1);
      PStp := pos('''', Txt);
      if PStp > 0 then PFim := PStp;
    end;
    //
    if not Mudou then
    begin
      Mudou := True;
      EdPrefixo.Text := Copy(Texto, 1, PIni);
      EdSufixo.Text  := '''' + Txt;
    end;
    //
    if PFim > 0 then
    begin
      PFim := Length(Texto) - Length(Txt) - PIni - 1;
      Texto := Copy(Texto, PIni + 1, PFim);
    end else
      Texto := Copy(Texto, PIni + 1);
    //  
    MemoSQL.Lines[I] := Texto;
  end;
end;

procedure TFmPrincipal.Alterarcdigosemtabelas1Click(Sender: TObject);
begin
  if QrDatabases.State = dsInactive then
  begin
    Geral.MensagemBox('Selecione a base de dados!', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if Geral.MensagemBox('A base de dados selecionada "' +
  QrDatabases.Fields[0].AsString + '" est� correta?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then
    Exit;
  //  
  if not LiberaPelaSenhaAdmin() then Exit;
  Application.CreateForm(TFmChangeCods, FmChangeCods);
  FmChangeCods.PainelDB.Caption := QrDatabases.Fields[0].AsString;
  FmChangeCods.DBChg.DatabaseName := QrDatabases.Fields[0].AsString;
  FmChangeCods.DBChg.Connect;
  FmChangeCods.ShowModal;
  FmChangeCods.Destroy;
end;

procedure TFmPrincipal.Arquivodecomponentes1Click(Sender: TObject); 
const
 cpt = 'Caption = ''';
var
  IniDir, Arquivo, Txt: String;
  Lista: TStringList;
  i, p, n: integer;
begin
  n := 0;
  IniDir := ExtractFileDir(Application.ExeName);
  Arquivo := '';
  if MyObjects.FileOpenDialog(FmPrincipal, IniDir, Arquivo,
  'Selecione o arquivo de Labels', '', [], Arquivo) then
  begin
    Lista := TStringList.Create;
    try
      Lista.LoadFromFile(Arquivo);
      MyObjects.LimpaGrade(GradeHTML, 1, 1, False);
      for i := 0 to Lista.Count - 1 do
      begin
        p := pos(cpt, Lista[i]);
        if p > 0 then
        begin
          p := p + Length(cpt);
          Txt := Copy(Lista[i], p);
          Txt := Copy(Txt, 1, Length(Txt)-2);
          n := n + 1;
          GradeHTML.RowCount := n + 1;
          GradeHTML.Cells[1,n] := Txt;
        end;
      end;
    finally
      Lista.Free;
    end;
  end;
end;

procedure TFmPrincipal.BitBtn10Click(Sender: TObject);
var
  Memo: TMemo;
begin
  Memo := MeEstrutMySQL;
  Memo.Lines.Clear;
  Memo.Lines.Add('      FTabelas.Add(Lowercase('''+QrTabelas.Fields[0].AsString+'''));');
  Memo.Lines.Add('');
  Memo.Lines.Add('//');
  Memo.Lines.Add('');
  Memo.Lines.Add('    end else if Uppercase(Tabela) = Uppercase('''+QrTabelas.Fields[0].AsString+''') then');
  Memo.Lines.Add('    begin');
  Memo.Lines.Add('      TemControle := ?;');
  QrCampos.First;
  while not QrCampos.Eof do
  begin
    Memo.Lines.Add('      //');
    Memo.Lines.Add('      New(FRCampos);');
    Memo.Lines.Add('      FRCampos.Field      := '''+String(QrCamposCAMPO.Value)+''';');
    Memo.Lines.Add('      FRCampos.Tipo       := '''+String(QrCamposTIPO.Value)+''';');
    Memo.Lines.Add('      FRCampos.Null       := '''+String(QrCamposNull.Value)+''';');
    Memo.Lines.Add('      FRCampos.Key        := '''+String(QrCamposKey.Value)+''';');
    Memo.Lines.Add('      FRCampos.Default    := '''+String(QrCamposPADRAO.Value)+''';');
    Memo.Lines.Add('      FRCampos.Extra      := '''+String(QrCamposExtra.Value)+''';');
    Memo.Lines.Add('      FLCampos.Add(FRCampos);');
    //
    QrCampos.Next;
  end;
  Memo.Lines.Add('    //');
end;

procedure TFmPrincipal.BitBtn11Click(Sender: TObject);
var
  I: Integer;
begin
  for I := 0 to MemoSQL.Lines.Count -1 do
    MemoSQL.Lines[I] := EdPrefixo.Text + MemoSQL.Lines[I] + EdSufixo.Text;
end;

procedure TFmPrincipal.BitBtn12Click(Sender: TObject);
var
  Campo, Texto, Novo: String;
  N: Integer;
begin
  N := 0;
  Memo2.Lines.Clear;
  Campo := EdCampoQr.Text;
  QrExecuta.First;
  while not QrExecuta.Eof do
  begin
    Application.ProcessMessages;
    Texto := QrExecuta.FieldByName(Campo).AsString;
    Novo := Geral.ConsertaTexto_Novo(Texto, N);
    if Novo <> Texto then
    begin
      // Parei aqui Fazer!!
    end;
    QrExecuta.Next;
  end;
  ShowMessage(IntToStr(N) + ' modifica��es foram executadas!');
end;

procedure TFmPrincipal.BitBtn13Click(Sender: TObject);
var
  Campo, Texto: String;
  I, O1, O2: Integer;
  //Letra1, Letra2: Byte;
begin
  //N := 0;
  Memo2.Lines.Clear;
  Memo6.Lines.Clear;
  Memo7.Lines.Clear;
  Campo := EdCampoQr.Text;
  QrExecuta.First;
  while not QrExecuta.Eof do
  begin
    Application.ProcessMessages;
    Texto := QrExecuta.FieldByName(Campo).AsString;
    //P := Length(Texto);
    Memo2.Lines.Add(Texto);
    {
    for I := 1 to P do
    begin
    }
      I := 1;
      O1 := Ord(Texto[I]);
      O2 := Ord(Texto[i+1]);
      Memo2.Lines.Add('   ' + Texto[i] + ' - ' + FormatFloat('000', O1));
    {
    end;
    }
    Memo6.Lines.Add(FormatFloat('000', O1));
    Memo7.Lines.Add(FormatFloat('000', O2));
    QrExecuta.Next;
  end;
end;

procedure TFmPrincipal.BitBtn14Click(Sender: TObject);
  function DC(Campo, Tamanho: Integer): String;
  begin
    Result := QrCampos.Fields[Campo].AsString;
    case Campo of
      3:
      begin
        if Result <> 'YES' then
          Result := 'NOT NULL'
        else
          Result := '';
      end;
      4:
      begin
        if Result = 'PRI' then
        begin
          if FIndice <> '' then
            FIndice := FIndice + ',';
          FIndice := FIndice + QrCampos.Fields[0].AsString;
        end;
        Result := '';
      end;
      5:
      begin
        if Result <> '' then
          Result := 'DEFAULT "' + Result + '"';
      end;
    end;
    //
    while length(Result) < Tamanho do
      Result := Result + ' ';
  end;
var
  Memo: TMemo;
  Linha, Virgula: String;
begin
  FIndice := '';
  Memo := MeSQLUCreate;
  Memo.Lines.Clear;

{
  if Nome = Lowercase('CNAB_Rem') then begin
    Qry.SQL.Add('CREATE TABLE CNAB_Rem (');
    Qry.SQL.Add('  Item      int(11)    AUTO_INCREMENT, ');
    Qry.SQL.Add('  Linha     int(11)    NOT NULL DEFAULT 0, ');
    Qry.SQL.Add('  Codigo    int(11)    NOT NULL DEFAULT 0, ');
    Qry.SQL.Add('  TipoDado  tinyint(3) NOT NULL DEFAULT 0, ');
    Qry.SQL.Add('  DadoI     int(11)             DEFAULT NULL, ');
    Qry.SQL.Add('  DadoF     double              DEFAULT NULL, ');
    Qry.SQL.Add('  DadoD     date                DEFAULT NULL, ');
    Qry.SQL.Add('  DadoH     time                DEFAULT NULL, ');
    Qry.SQL.Add('  DadoT     varchar(255)        DEFAULT "", ');
    Qry.SQL.Add('  PRIMARY KEY (Item)');
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
}
  Memo.Lines.Add('  if Nome = Lowercase('''+QrTabelas.Fields[0].AsString+''') then begin');
  //Memo.Lines.Add('    Qry.SQL.Add(''CREATE TABLE '''+QrTabelas.Fields[0].AsString+'(''');
  QrCampos.First;
  while not QrCampos.Eof do
  begin
    Linha := '    Qry.SQL.Add(''  '+
      DC(0,30)+' '+
      DC(1,12)+' '+
      DC(3,08)+' '+
      DC(4,00)+' '+
      DC(5,20);
    if (QrCampos.RecNo < QrCampos.RecordCount) or (FIndice <> '') then
      Virgula := ','
    else Virgula := '';
    Linha := Linha + Virgula;
    Linha := Linha + ''');';
    //
    Memo.Lines.Add(Linha);
    QrCampos.Next;
  end;
  if FIndice <> '' then
    Memo.Lines.Add('    Qry.SQL.Add(''  PRIMARY KEY ('+FIndice+')'');');
  Memo.Lines.Add('    Qry.SQL.Add('') CHARACTER SET latin1 COLLATE latin1_swedish_ci'');');
  Memo.Lines.Add('    Qry.ExecSQL;');
  Memo.Lines.Add('  end else');
end;

procedure TFmPrincipal.BitBtn15Click(Sender: TObject);
(*var
  I: Integer;
  Nome, Tipo, Tam, Deci: String;*)
begin

{
  for i := 0 to QrExecuta.FieldCount - 1 do
  begin
    //Nome := ShowMessage(TField(QrExecuta.Fields[I]).FieldName);
    Nome := QrExecuta.Fields[I].FieldName;
    case QrExecuta.Fields[I].DataType of
      ftUnknown       : Tipo := 'Unknown';
      ftString        : Tipo := 'String';
      ftSmallint      : Tipo := 'Smallint';
      ftInteger       : Tipo := 'Integer';
      ftWord          : Tipo := 'Word';
      ftBoolean       : Tipo := 'Boolean';
      ftFloat         : Tipo := 'Float';
      ftCurrency      : Tipo := 'Currency';
      ftBCD           : Tipo := 'BCD';
      ftDate          : Tipo := 'Date';
      ftTime          : Tipo := 'Time';
      ftDateTime      : Tipo := 'DateTime';
      ftBytes         : Tipo := 'Bytes';
      ftVarBytes      : Tipo := 'VarBytes';
      ftAutoInc       : Tipo := 'AutoInc';
      ftBlob          : Tipo := 'Blob';
      ftMemo          : Tipo := 'Memo';
      ftGraphic       : Tipo := 'Graphic';
      ftFmtMemo       : Tipo := 'FmtMemo';
      ftParadoxOle    : Tipo := 'ParadoxOle';
      ftDBaseOle      : Tipo := 'DBaseOle';
      ftTypedBinary   : Tipo := 'TypedBinary';
      ftCursor        : Tipo := 'Cursor';
      ftFixedChar     : Tipo := 'FixedChar';
      ftWideString    : Tipo := 'WideString';
      ftLargeint      : Tipo := 'Largeint';
      ftADT           : Tipo := 'ADT';
      ftArray         : Tipo := 'Array';
      ftReference     : Tipo := 'Reference';
      ftDataSet       : Tipo := 'DataSet';
      ftOraBlob       : Tipo := 'OraBlob';
      ftOraClob       : Tipo := 'OraClob';
      ftVariant       : Tipo := 'Variant';
      ftInterface     : Tipo := 'Interface';
      ftIDispatch     : Tipo := 'IDispatch';
      ftGuid          : Tipo := 'Guid';
      ftTimeStamp     : Tipo := 'TimeStamp';
      ftFMTBcd        : Tipo := 'FMTBcd';
      ftFixedWideChar : Tipo := 'FixedWideChar';
      ftWideMemo      : Tipo := 'WideMemo';
      ftOraTimeStamp  : Tipo := 'OraTimeStamp';
      ftOraInterval   : Tipo := 'OraInterval';
    end;
    Tam  := IntToStr(TField(QrExecuta.Fields[I]).Size);
    (*
    case QrExecuta.Fields[I].DataType of
      ftUnknown       : Tipo := 'Unknown';
      ftString        : Tipo := 'String';
      ftSmallint      : Tipo := 'Smallint';
      ftInteger       : Tipo := 'Integer';
      ftWord          : Tipo := 'Word';
      ftBoolean       : Tipo := 'Boolean';
      ftFloat         : Tam := IntToStr(TFloatField(QrExecuta.Fields[I]).Size);
      ftCurrency      : Tipo := 'Currency';
      ftBCD           : Tipo := 'BCD';
      ftDate          : Tipo := 'Date';
      ftTime          : Tipo := 'Time';
      ftDateTime      : Tipo := 'DateTime';
      ftBytes         : Tipo := 'Bytes';
      ftVarBytes      : Tipo := 'VarBytes';
      ftAutoInc       : Tipo := 'AutoInc';
      ftBlob          : Tipo := 'Blob';
      ftMemo          : Tipo := 'Memo';
      ftGraphic       : Tipo := 'Graphic';
      ftFmtMemo       : Tipo := 'FmtMemo';
      ftParadoxOle    : Tipo := 'ParadoxOle';
      ftDBaseOle      : Tipo := 'DBaseOle';
      ftTypedBinary   : Tipo := 'TypedBinary';
      ftCursor        : Tipo := 'Cursor';
      ftFixedChar     : Tipo := 'FixedChar';
      ftWideString    : Tipo := 'WideString';
      ftLargeint      : Tipo := 'Largeint';
      ftADT           : Tipo := 'ADT';
      ftArray         : Tipo := 'Array';
      ftReference     : Tipo := 'Reference';
      ftDataSet       : Tipo := 'DataSet';
      ftOraBlob       : Tipo := 'OraBlob';
      ftOraClob       : Tipo := 'OraClob';
      ftVariant       : Tipo := 'Variant';
      ftInterface     : Tipo := 'Interface';
      ftIDispatch     : Tipo := 'IDispatch';
      ftGuid          : Tipo := 'Guid';
      ftTimeStamp     : Tipo := 'TimeStamp';
      ftFMTBcd        : Tipo := 'FMTBcd';
      ftFixedWideChar : Tipo := 'FixedWideChar';
      ftWideMemo      : Tipo := 'WideMemo';
      ftOraTimeStamp  : Tipo := 'OraTimeStamp';
      ftOraInterval   : Tipo := 'OraInterval';
    end;
    *)
    ShowMessage(Nome + ' :' + Tipo + ' # ' + Tam);
  end;
}
  QrExecuta.Close;
  QrExecuta.SQL.Clear;
  QrExecuta.SQL.Add('DROP TABLE IF EXISTS _CopiaSQLExecutada_');
  QrExecuta.ExecSQL;
  //
  QrExecuta.Close;
  QrExecuta.SQL.Clear;
  QrExecuta.SQL.Text := 'CREATE TABLE _CopiaSQLExecutada_' + #13#10 + MemoSQL.Text;
  QrExecuta.ExecSQL;
  //
  ReopenDatabases(EdDBFiltro.Text, '');
  EdLocTable.Text := '_CopiaSQLExecutada_';
  //
  BitBtn14Click(Self);
  //
  QrExecuta.Close;
  QrExecuta.SQL.Clear;
  QrExecuta.SQL.Add('DROP TABLE IF EXISTS _CopiaSQLExecutada_');
  QrExecuta.ExecSQL;
  //
end;

procedure TFmPrincipal.BitBtn16Click(Sender: TObject);
begin
  FPararPesqFlds := True;
end;

procedure TFmPrincipal.BitBtn1Click(Sender: TObject);
var
  Pasta, Arq: String;
begin
  Pasta := ExtractFileDir(Application.ExeName) + '\SQLs';
  ForceDirectories(Pasta);
  SaveDialog1.InitialDir := Pasta;
  if SaveDialog1.Execute then
  begin
    Arq := SaveDialog1.FileName;
    if pos('.', Arq) = 0 then
      Arq := Arq + '.SQL';
    Geral.SalvaTextoEmArquivo(Arq, MemoSQL.Text, True);
  end;
end;

procedure TFmPrincipal.BitBtn2Click(Sender: TObject);
var
  Pasta: String;
begin
  Pasta := ExtractFileDir(Application.ExeName) + '\SQLs';
  ForceDirectories(Pasta);
  OpenDialog1.InitialDir := Pasta;
  if OpenDialog1.Execute then
    MemoSQL.Lines.LoadFromFile(OpenDialog1.FileName);
end;

procedure TFmPrincipal.BitBtn3Click(Sender: TObject);
const
  C2 = ', ''''';
  C3 = ',';
  Va = 'Query.SQL.Add(''  ';
  Vb = ''');';
var
  //V0,
  V1, V2, Nome, Size, Deci: String;
  p1, p2, p3: Integer;
  //
  //FAutoSeq: Integer;
  AllCampos: String;
begin
  MeABS.Lines.Clear;
  if EdTabela.Text <> '' then FTabela := EdTabela.Text else
    FTabela := QrTabelas.Fields[0].AsString;
  if InputQuery('Nome da Tabela ABSQuery no Delphi', 'Informe o nome da tabela ABSQuery no delphi:',
    FTabela) then
  begin
    MeABS.Lines.Add('Query.Close;                                               ');
    MeABS.Lines.Add('Query.SQL.Clear;                                           ');
    //MeABS.Lines.Add('Query.SQL.Add(''DROP TABLE ' + FTabela + ';'');            ');
    //MeABS.Lines.Add('Query.SQL.Add(''CREATE TABLE ' + FTabela + ' ('');         ');
    v1 :=  Geral.CompletaString('DROP TABLE ' + FTabela + ';' , ' ', 43, taLeftJustify, True);
    MeABS.Lines.Add(Va + V1 + Vb);
    v1 :=  Geral.CompletaString('CREATE TABLE ' + FTabela + ' (' , ' ', 43, taLeftJustify, True);
    MeABS.Lines.Add(Va + V1 + Vb);
    //
    AllCampos := '';
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if QrCampos.RecNo = 1 then
        AllCampos := String(QrCamposCAMPO.Value)
      else
        AllCampos := AllCampos + ',' + String(QrCamposCAMPO.Value);
      p1 := pos('(', String(QrCamposTIPO.Value));
      if p1 > 0 then
      begin
        Nome := Trim(Copy(String(QrCamposTIPO.Value), 1, p1-1));
        p2 := pos(',', String(QrCamposTIPO.Value));
        if p2 > 0 then
        begin
          p3 := pos(')', String(QrCamposTIPO.Value));
          Size := Trim(Copy(String(QrCamposTIPO.Value), p1+1, p2-p1-1));
          Deci := Trim(Copy(String(QrCamposTIPO.Value), p2+1, p3-p2-1));
        end else begin
          p2 := pos(')', String(QrCamposTIPO.Value));
          Size := Trim(Copy(String(QrCamposTIPO.Value), p1+1, p2-p1-1));
          Deci := '0';
        end;
      end else begin
        Nome := String(QrCamposTIPO.Value);
        Size := '0';
        Deci := '0';
      end;
      V1 := Copy(String(QrCamposCAMPO.Value), 1, 10);
      V1 := Geral.CompletaString(V1 , ' ', 13, taLeftJustify, True);
      if (Uppercase(Nome) = Uppercase('int'      )) then V2 := 'INTEGER' else
      if (Uppercase(Nome) = Uppercase('tinyint'  )) then V2 := 'SHORTINT' else
      if (Uppercase(Nome) = Uppercase('mediumint')) then V2 := 'SMALLINT,' else
      if (Uppercase(Nome) = Uppercase('bigint'   )) then V2 := 'LARGEINT,' else
      if (Uppercase(Nome) = Uppercase('integer'  )) then V2 := 'INTEGER,' else
      if  Uppercase(Nome) = Uppercase('date'     )  then V2 := 'DATE' else
      if  Uppercase(Nome) = Uppercase('time'     )  then V2 := 'TIME' else
      if  Uppercase(Nome) = Uppercase('datetime' )  then V2 := 'DATETIME' else
      if (Uppercase(Nome) = Uppercase('float'    )) then V2 := 'FLOAT' else
      if (Uppercase(Nome) = Uppercase('double'   )) then V2 := 'FLOAT' else
      if (Uppercase(Nome) = Uppercase('varchar'  )) then V2 := String(QrCamposTIPO.Value) else
      if (Uppercase(Nome) = Uppercase('char'     )) then V2 := String(QrCamposTIPO.Value) else
      if  Uppercase(Nome) = Uppercase('memo'     )  then V2 := 'MEMO' else
      if (Uppercase(Nome) = Uppercase('blob'     )) then V2 := 'MEMO' else
      if (Uppercase(Nome) = Uppercase('text'     )) then V2 := 'MEMO' else
      if (Uppercase(Nome) = Uppercase('longtext' )) then V2 := 'MEMO'
      else V2 := '?';
      if V2 = '?' then
        Geral.MB_Aviso('Tipo n�o implementado: ' + Nome + ' ( Campo: '+V1+' )');
      if QrCampos.RecNo = QrCampos.RecordCount then
        V2 := UPPERCASE(Geral.CompletaString(V2+''',''', ' ', 30, taLeftJustify, True))
      else
        V2 := UPPERCASE(Geral.CompletaString(V2+',', ' ', 30, taLeftJustify, True));
      MeABS.Lines.Add(Va + V1 + V2 + Vb);
      QrCampos.Next;
    end;
    v1 :=  Geral.CompletaString('_ATIVO_''?,?'' SHORTINT' , ' ', 43, taLeftJustify, True);
    MeABS.Lines.Add('//' + Va + V1 + Vb);
    v1 :=  Geral.CompletaString('PRIMARY KEY  PRYMARY (?),' , ' ', 43, taLeftJustify, True);
    MeABS.Lines.Add('//' + Va + V1 + Vb);
    v1 :=  Geral.CompletaString('UNIQUE INDEX UNIQUE1 (?, ?)' , ' ', 43, taLeftJustify, True);
    MeABS.Lines.Add('//' + Va + V1 + Vb);
    MeABS.Lines.Add(Va + ');' + Vb);

    MeABS.Lines.Add('//                                                           ');
    MeABS.Lines.Add('//' + 'SQL := ''INSERT INTO ' + FTabela + ' (' + AllCampos + ')'' +  ');
    MeABS.Lines.Add('//' + '       '' Values('';                                   ');
    MeABS.Lines.Add('//' + 'QrCopy.First;                                                ');
    MeABS.Lines.Add('//' + 'Vals := '''';                                                ');
    MeABS.Lines.Add('//' + 'while not QrCopy.Eof do                                      ');
    MeABS.Lines.Add('//' + 'begin                                                        ');
    MeABS.Lines.Add('//' + '  inc(FAutoSeq, 1);                                          ');
    MeABS.Lines.Add('//' + '  Vals := Vals + ''"'' + QrCopyString.Value + ''",'';        ');
    MeABS.Lines.Add('//' + '  Vals := Vals + ''"'' + QrCopyMemo.Value + ''",'';          ');
    MeABS.Lines.Add('//' + '  Vals := Vals + Geral.FFP(QrCopyInteiro.Value, 0) + '',''; ');
    MeABS.Lines.Add('//' + '  Vals := Vals + Geral.FFP(QrCopyFloat.Value, 15) + '','';  ');
    MeABS.Lines.Add('//' + '  Vals := Vals + Geral.FDT(QrResultCOData.Value, 1) + ''"');
    MeABS.Lines.Add('//' + '  Vals := Vals + ''1);'';                                    ');
    MeABS.Lines.Add('//' + '  //                                                         ');
    MeABS.Lines.Add('//' + '  Query.SQL.Add(SQL + Vals);                                 ');
    MeABS.Lines.Add('//' + '  //                                                         ');
    MeABS.Lines.Add('//' + '  QrCopy.Next;                                               ');
    MeABS.Lines.Add('//' + 'end;                                                         ');
    MeABS.Lines.Add('//                                                           ');
    MeABS.Lines.Add('Query.SQL.Add(''SELECT * FROM ' + FTabela + ';'');                 ');
    MeABS.Lines.Add('Query.Open;                                                  ');
  end;
end;

procedure TFmPrincipal.BitBtn4Click(Sender: TObject);
var
  I: Integer;
  TabAntes: String;
begin
  FPararPesqFlds := False;
  if (QrDatabases.State = dsInactive) or (QrDatabases.RecordCount = 0) then
  begin
    Geral.MensagemBox('N�o h� database para pesquisar!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  try
    TabAntes := EdLocTable.Text;
    ProgressBar1.Position := 0;
    ProgressBar1.Max := QrDatabases.RecordCount;
    ConectaDBTest();
    //
    QrTestUpd.Close;
    QrTestUpd.SQL.Clear;
    QrTestUpd.SQL.Add('DROP TABLE IF EXISTS _QtdEmFld_;');
    QrTestUpd.SQL.Add('CREATE TABLE _QtdEmFld_ (');
    QrTestUpd.SQL.Add('  DBase  varchar(30) DEFAULT "",');
    QrTestUpd.SQL.Add('  Tabel  varchar(30) DEFAULT "",');
    QrTestUpd.SQL.Add('  Itens  int(11) DEFAULT 0,');
    QrTestUpd.SQL.Add('  Valor  varchar(255) DEFAULT ""');
    QrTestUpd.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci;');
    QrTestUpd.ExecSQL;
    //
    QrDatabases.First;
    while not QrDatabases.Eof do
    begin
      ProgressBar1.Position := ProgressBar1.Position + 1;
      ProgressBar1.Update;
      Application.ProcessMessages;
      //
      for I := 0 to MePsqFldTabs.Lines.Count - 1 do
      begin
        EdLocTable.Text := MePsqFldTabs.Lines[I];
        Update;
        Application.ProcessMessages;
        //
        QrTabelas.First;
        while not QrTabelas.Eof do
        begin
          QrTestAux.Close;
          QrTestAux.SQL.Clear;
          QrTestAux.SQL.Add('SELECT ' + EdPsqFldFlds.Text + ' Valor, ');
          QrTestAux.SQL.Add('COUNT(' + EdPsqFldFlds.Text + ') Itens');
          QrTestAux.SQL.Add('FROM ' + QrTabelas.Fields[0].AsString);
          QrTestAux.SQL.Add('GROUP BY ' + EdPsqFldFlds.Text);
          QrTestAux.Open;
          ProgressBar2.Position := 0;
          if QrTestAux.RecordCount >= EdPsqFldDvrs.ValueVariant then
          begin
            ProgressBar2.Max := QrTestAux.RecordCount;
            //
            while not QrTestAux.Eof do
            begin
              if FPararPesqFlds then
                Exit;
              ProgressBar2.Position := ProgressBar2.Position + 1;
              ProgressBar1.Update;
              Application.ProcessMessages;
              //
              if QrTestAux.FieldByName('Itens').AsInteger >= EdPsqFldItns.ValueVariant then
              begin
                QrTestUpd.SQL.Clear;
                QrTestUpd.SQL.Add('INSERT INTO _QtdEmFld_ SET ');
                QrTestUpd.SQL.Add('  DBase="' + QrDatabases.Fields[0].AsString + '", ');
                QrTestUpd.SQL.Add('  Tabel="' + QrTabelas.Fields[0].AsString + '", ');
                QrTestUpd.SQL.Add('  Itens=' + FormatFloat('0', QrTestAux.FieldByName('Itens').AsInteger) + ', ');
                QrTestUpd.SQL.Add('  Valor="' + QrTestAux.FieldByName('Valor').AsString + '"');
                QrTestUpd.ExecSQL;
              end;
              //
              QrTestAux.Next;
            end;
          end;
          //
          QrTabelas.Next;
        end;
      end;
      QrDatabases.Next;
    end;
    EdLocTable.Text := TabAntes;
    //
    QrTestAux.Close;
    QrTestAux.SQL.Clear;
    QrTestAux.SQL.Add('SELECT * ');
    QrTestAux.SQL.Add('FROM _QtdEmFld_');
    QrTestAux.Open;
  finally
    Screen.Cursor := crDefault;
    ProgressBar1.Position := 0;
    ProgressBar2.Position := 0;
  end;
end;

procedure TFmPrincipal.Button2Click(Sender: TObject);
begin
  EdCampo.Text := '';
  //
  QrExecut2.Close;
  QrExecut2.SQL.Clear;
  QrExecut2.SQL.Add('DROP TABLE IF EXISTS _CopiaSQLExecutada_');
  QrExecut2.ExecSQL;
  //
  QrExecut2.Close;
  QrExecut2.SQL.Clear;
  QrExecut2.SQL.Text := 'CREATE TABLE _CopiaSQLExecutada_' + #13#10 + MemoSQL.Text;
  QrExecut2.ExecSQL;
  //
  ReopenDatabases(EdDbFiltro.Text, '');
  EdLocTable.Text := '_CopiaSQLExecutada_';
  //
{
  Memo3.Lines.Clear;
  if (QrExecuta.State = dsInactive)
  or (QrExecuta.RecordCount = 0) then
  begin
    Application.MessageBox('N�o h� dados de pesquisa (SQL) para listar!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  QrExecuta.First;
  while not QrExecuta.Eof do
  begin
    Txt1 := 'UMyMod.SQLInsUpd_IGNORE?(Dmod.QrUpd, LaTipo.Caption, ''' +
    QrExecuta.Fields[0].AsString + ''', auto_increment?[' +  #13#10;
    Txt2 := '';
    Txt3 :=  #13#10;
    Txt4 := '';
    Conta := -1;
    // Parei aqui Continuar !
    QrFldExec.Close;
    QrFldExec.Open;
    QrFldExec.First;
    while not QrFldExec.Eof do
    begin
      if posicao(Uppercase(QrFldExecField.Value), FNaoCopiar) = 0 then
      begin
        if QrFldExecKey.Value = 'PRI'  then
        begin
          Txt2 := Txt2 + '''' + QrFldExecField.Value + ''', ';
          Txt4 := Txt4 + '''' + QrExecuta.FieldByName(QrFldExecField.Value).AsString + ''', ';
        end else begin
          Inc(Conta, 1);
          //
          if Conta = 3 then
          begin
            Txt1  := Txt1 + #13#10;
            Txt3  := Txt3 + #13#10;
            Conta := 0;
          end;
          Txt1 := Txt1 + '''' + QrFldExecField.Value + ''', ';
          Txt3 := Txt3 + '''' + QrExecuta.FieldByName(QrFldExecField.Value).AsString + ''', ';
        end;
      end;
      QrFldExec.Next;
    end;
    Txt1 := Copy(Txt1, 1, Length(Txt1) - 2) + '], [';
    if Length(Txt2) > 1 then Txt2 := Copy(Txt2, 1, Length(Txt2) - 2);
    Txt3 := Copy(Txt3, 1, Length(Txt3) - 2) + '], [';
    if Length(Txt4) > 0 then Txt4 := Copy(Txt4, 1, Length(Txt4) - 2);
    Memo3.Lines.Add(Txt1  + #13#10 + Txt2 + '], [' + Txt3  +  #13#10 + Txt4 + '], UserDataAlterweb?, IGNORE?');
    //
    QrExecuta.Next;
  end;
}
  //
  Button1Click(Self);
  //
  QrExecut2.Close;
  QrExecut2.SQL.Clear;
  QrExecut2.SQL.Add('DROP TABLE IF EXISTS _CopiaSQLExecutada_');
  QrExecut2.ExecSQL;
  //
end;

procedure TFmPrincipal.Button3Click(Sender: TObject);
var
  i, n: Integer;
  BkColor, TxColor,
  Campo, Descri, Tipo, Relevancia: String;
begin
  Screen.Cursor := crHourGlass;
  MeHTML.Lines.Clear;
  MeHTML.Lines.Add('{html=');
  MeHTML.Lines.Add('<body style="COLOR: #365f91" lang="PT-BR">');
  MeHTML.Lines.Add('  <table style="FONT-SIZE: 10px; LINE-HEIGHT: 16px; BORDER-BOTTOM: #365f91 1px solid; FONT-FAMILY: Verdana, Geneva, sans-serif" cellspacing="0" cellpadding="0" border="0">');
  MeHTML.Lines.Add('	<tr style="COLOR: #365f91">');
  MeHTML.Lines.Add('	  <td width="240" style="BORDER-TOP: #365f91 1px solid; BORDER-BOTTOM: #365f91 1px solid"><b>Campo</b></td>');
  MeHTML.Lines.Add('	  <td width="auto" style="BORDER-TOP: #365f91 1px solid; BORDER-BOTTOM: #365f91 1px solid"><b>Descri��o</b></td>');
  MeHTML.Lines.Add('	  <td width="140" style="BORDER-TOP: #365f91 1px solid; BORDER-BOTTOM: #365f91 1px solid"><b>Tipo</b></td>');
  MeHTML.Lines.Add('	  <td width="140" style="BORDER-TOP: #365f91 1px solid; BORDER-BOTTOM: #365f91 1px solid"><b>Relev�ncia</b></td>');
  MeHTML.Lines.Add('	</tr>');
  //
  n := 0;
  for i := 1 to GradeHTML.RowCount - 1 do
  begin
    Campo      := Trim(GradeHTML.Cells[01,i]);
    Descri     := Trim(GradeHTML.Cells[02,i]);
    Tipo       := Trim(GradeHTML.Cells[03,i]);
    Relevancia := Trim(GradeHTML.Cells[04,i]);
    //
    if (Campo <> '') or (Descri <> '') or (Tipo <> '') or (Relevancia <> '') then
    begin
      n := n + 1;
      if (n / 2) = (n div 2) then
        BkColor := ''
      else
        BkColor := ' style="BACKGROUND-COLOR: #d3dfee"';
      //
      if Relevancia = 'Obrigat�rio' then
        TxColor := ' style="COLOR: red"'
      else
        TxColor := '';
      MeHTML.Lines.Add('	<tr' + BkColor + '>');
      MeHTML.Lines.Add('	  <td><b>' + Campo + '</b></td>');
      MeHTML.Lines.Add('	  <td>' + Descri + '</td>');
      MeHTML.Lines.Add('	  <td>' + Tipo + '</td>');
      MeHTML.Lines.Add('	  <td' + TxColor + '>' + Relevancia + '</td>');
      MeHTML.Lines.Add('	</tr>');
    end;
  end;
  //
  MeHTML.Lines.Add('  </table>');
  MeHTML.Lines.Add('</body>');
  MeHTML.Lines.Add('}');
  MeHTML.Lines.Add('');
  //
  Screen.Cursor := crDefault;
end;

procedure TFmPrincipal.Button4Click(Sender: TObject);
begin
  MyObjects.LimpaGrade(GradeHTML, 1, 1, False);
end;

procedure TFmPrincipal.Button5Click(Sender: TObject);
begin
  MLAGeral.SalvaStringGridToFile(GradeHTML, FGradeHTML, False);
end;

procedure TFmPrincipal.Button6Click(Sender: TObject);
begin
  Memo8.Visible := True;
  Memo8.Align := alClient;
  GridRegistros.Visible := False;
  Memo8.Lines.Clear;
  QrFlds.Close;
  QrFlds.SQL.Clear;
  QrFlds.SQL.Add('DESCRIBE ' + QrTabelas.Fields[0].AsString);
  QrFlds.Open;
  while not QrFlds.Eof do
  begin
    QrIts.Close;
    QrIts.SQL.Clear;
    QrIts.SQL.Add('SELECT ' + String(QrFldsField.Value));
    QrIts.SQL.Add(', COUNT(' + String(QrFldsField.Value) + ') Itens');
    QrIts.SQL.Add('FROM ' + String(QrTabelas.Fields[0].AsString));
    QrIts.SQL.Add('GROUP BY ' + String(QrFldsField.Value));
    QrIts.Open;
    QrIts.First;
    Memo8.Lines.Add(String(QrFldsField.Value) + ': ' + FormatFloat('0', QrIts.RecordCount));
    Memo8.Lines.Add('==========================================');
    if QrIts.RecordCount < 100 then
    begin
      while not QrIts.Eof do
      begin
        Memo8.Lines.Add('     ' + QrIts.Fields[0].AsString + ': ' +
        FormatFloat('0', QrIts.Fields[1].AsInteger));
        //
        QrIts.Next;
      end;
    end;
    Memo8.Lines.Add('');
    QrFlds.Next;
  end;
end;

procedure TFmPrincipal.Button7Click(Sender: TObject);
var
  I, K: Integer;
  Largura: String;
begin
  Largura := '5';
  if InputQuery('Largura das colunas', 'Informe a nova largura:', Largura) then
  begin
    K := Geral.IMV(Largura);
    if K < 1 then K := 5;
    //
    for I := 0 to GridRegistros.Columns.Count - 1 do
     GridRegistros.Columns[I].Width := K;
  end;
end;

procedure TFmPrincipal.Button8Click(Sender: TObject);
var
  I, K: Integer;
  Largura: String;
begin
  Largura := '5';
  if InputQuery('Largura das colunas', 'Informe a nova largura:', Largura) then
  begin
    K := Geral.IMV(Largura);
    if K < 1 then K := 5;
    //
    for I := 0 to GridResult.Columns.Count - 1 do
     GridResult.Columns[I].Width := K;
  end;
end;

procedure TFmPrincipal.BtEstruturaDBFClick(Sender: TObject);
{
  function DC(Texto: String; Tamanho: Integer): String;
  begin
    Result := Texto;
    while length(Result) < Tamanho do
      Result := Result + ' ';
  end;
const
  NomeTab = 'NomeTabela';
var
  Memo: TMemo;
  Linha, Virgula, V1, V2, V3, V5: String;
  //
  I, T, P: Integer;
}
begin
{  PageControl5.ActivePageIndex := 1;
  FIndice := '';
  Memo := MeDBF;
  Memo.Lines.Clear;

  Memo.Lines.Add('  if Nome = Lowercase('''+ NomeTab +''') then begin');
  for I := 0 to TbVKDBFNTX.Fields.Count - 1 do
  begin
    V1 := TbVKDBFNTX.Fields[I].FullName;
    case TbVKDBFNTX.Fields[I].DataType of
      ftUnknown:        V2 := 'Unknown';
      ftString:         V2 := 'Varchar'; // diferente
      ftSmallint:       V2 := 'Smallint';
      ftInteger:        V2 := 'Integer';
      ftWord:           V2 := 'Word';
      ftBoolean:        V2 := 'tinyint(1)'; // 'Boolean'; Diferente? n�o testado
      ftFloat:          V2 := 'Float';
      ftCurrency:       V2 := 'Currency';
      ftBCD:            V2 := 'BCD';
      ftDate:           V2 := 'Date';
      ftTime:           V2 := 'Time';
      ftDateTime:       V2 := 'DateTime';
      ftBytes:          V2 := 'Bytes';
      ftVarBytes:       V2 := 'VarBytes';
      ftAutoInc:        V2 := 'AutoInc';
      ftBlob:           V2 := 'Blob';
      ftMemo:           V2 := 'Text';  // diferente
      ftGraphic:        V2 := 'Graphic';
      ftFmtMemo:        V2 := 'FmtMemo';
      ftParadoxOle:     V2 := 'ParadoxOle';
      ftDBaseOle:       V2 := 'DBaseOle';
      ftTypedBinary:    V2 := 'TypedBinary';
      ftCursor:         V2 := 'Cursor';
      ftFixedChar:      V2 := 'FixedChar';
      ftWideString:     V2 := 'WideString';
      ftLargeint:       V2 := 'Largeint';
      ftADT:            V2 := 'ADT';
      ftArray:          V2 := 'Array';
      ftReference:      V2 := 'Reference';
      ftDataSet:        V2 := 'DataSet';
      ftOraBlob:        V2 := 'OraBlob';
      ftOraClob:        V2 := 'OraClob';
      ftVariant:        V2 := 'Variant';
      ftInterface:      V2 := 'Interface';
      ftIDispatch:      V2 := 'IDispatch';
      ftGuid:           V2 := 'Guid';
      ftTimeStamp:      V2 := 'TimeStamp';
      ftFMTBcd:         V2 := 'FMTBcd';
      ftFixedWideChar:  V2 := 'FixedWideChar';
      ftWideMemo:       V2 := 'WideMemo';
      ftOraTimeStamp:   V2 := 'OraTimeStamp';
      ftOraInterval:    V2 := 'OraInterval';
    end;
    T := TbVKDBFNTX.Fields[I].Size;
    if T > 0 then
    begin
      V2 := V2 + '(' + FormatFloat('0', T);
      case TbVKDBFNTX.Fields[I].DataType of
        ftFloat:   P := TFloatField(TbVKDBFNTX.Fields[I]).Precision;
        ftBCD:     P := TBCDField(TbVKDBFNTX.Fields[I]).Precision;
        ftFMTBcd:  P := TFMTBcdField(TbVKDBFNTX.Fields[I]).Precision;
        else P := 0;
      end;
      if P > 0 then
        V2 := V2 + ',' + FormatFloat('0', P);
      V2 := V2 + ')';
    end;
    if TbVKDBFNTX.Fields[I].Required then
      V3 := 'NOT NULL'
    else
      V3 := '';
    if TbVKDBFNTX.Fields[I].IsIndexField then
    begin
      FIndice := FIndice + V1 + ',';
      //V4 := 'PRI';
    end;
    V5 := ''; // N�o tem como saber o valor default?
    Linha := '    Qry.SQL.Add(''  '+
      DC(V1,20)+' '+
      DC(V2,12)+' '+
      DC(V3,08)+' '+
      //DC(V4,00)+' '+
      DC(V5,20);
    if (I + 1 < TbVKDBFNTX.Fields.Count) or (FIndice <> '') then
      Virgula := ','
    else Virgula := '';
    Linha := Linha + Virgula;
    Linha := Linha + ''');';
    //
    Memo.Lines.Add(Linha);
  end;
  if FIndice <> '' then
  begin
    FIndice := Copy(FIndice, 1, Length(FIndice)-2);
    Memo.Lines.Add('    Qry.SQL.Add(''  PRIMARY KEY (' + FIndice + ')'');');
  end;
  Memo.Lines.Add('    Qry.SQL.Add('') CHARACTER SET latin1 COLLATE latin1_swedish_ci'');');
  Memo.Lines.Add('    Qry.ExecSQL;');
  Memo.Lines.Add('  end else');
}end;

procedure TFmPrincipal.BtExportaClick(Sender: TObject);
var
  I: Integer;
  Linha: WideString;
  procedure AdicionaLinhaAoMemo();
  var
    Texto: String;
  begin
    Linha := Copy(Linha, 2);
    while Length(Linha) > 250 do
    begin
      Texto := '''' + Copy(Linha, 1, 250) + '''+';
      Memo9.Lines.Add(Texto);
      Linha := Copy(Linha, 251);
    end;
    Memo9.Lines.Add('''' + Linha + ''',');
  end;
begin
  Memo9.Visible := True;
  Memo9.Lines.Clear;
  //  Campos
  Linha := '';
  for I := 0 to QrExecuta.FieldCount - 1 do
  begin
    Linha := Linha + '|' + QrExecuta.Fields[I].FieldName;
  end;
  AdicionaLinhaAoMemo();
  //
  // Valores
  QrExecuta.DisableControls;
  try
    Screen.Cursor := crHourGlass;
    QrExecuta.First;
    while not QrExecuta.Eof do
    begin
      Linha := '';
      for I := 0 to QrExecuta.FieldCount - 1 do
      begin
        Linha := Linha + '|' +
          Geral.VariantToString(QrExecuta.Fields[I].Value);
      end;
      AdicionaLinhaAoMemo();
      //
      QrExecuta.Next;
    end;
  finally
    QrExecuta.EnableControls;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPrincipal.BtCarregaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCarrega, BtCarrega);
end;

procedure TFmPrincipal.CEP1Click(Sender: TObject);
begin
{
  Application.CreateForm(TFmPesqCEP, FmPesqCEP);
  FmPesqCEP.ShowModal;
  FmPesqCEP.Destroy;
}
end;

procedure TFmPrincipal.Componentes1Click(Sender: TObject);
begin
  Application.CreateForm(TFmAuxCtrls, FmAuxCtrls);
  FmAuxCtrls.ShowModal;
  FmAuxCtrls.Destroy;
end;

procedure TFmPrincipal.ComponentesDelphi1Click(Sender: TObject);
begin
  Application.CreateForm(TFmPesqComp, FmPesqComp);
  FmPesqComp.ShowModal;
  FmPesqComp.Destroy;
end;

function TFmPrincipal.ConectaDBTest: Boolean;
var
  Qry: TmySQLQuery;
  IP: String;
begin
  //Result := False;
  if not DBTest.Connected then
  begin
    if PID_DB_TEST <> nil then
    begin
      DBTest.Host         := PID_DB_TEST.Host;
      DBTest.UserName     := PID_DB_TEST.UserName;
      DBTest.UserPassword := PID_DB_TEST.UserPassword;
      DBTest.DatabaseName := PID_DB_TEST.DatabaseName;
      DBTest.Port         := PID_DB_TEST.Port;
      DBTest.Connect;
      //Result := DBTest.Connected;
    end;
    if True then

    if VAR_IP <> '' then
      IP := VAR_IP
    else
      IP := '127.0.0.1';
    Qry := TmySQLQuery.Create(Dmod);
    Qry.DataBase := Dmod.MyDB;
    Qry.SQL.Clear;
    Qry.SQL.Add('SHOW DATABASES LIKE "test"');
    Qry.Open;
    if Qry.RecordCount = 0 then
    begin
      Qry.SQL.Clear;
      Qry.SQL.Add('CREATE DATABASE test');
      Qry.ExecSQL;
    end;
    Qry.Free;
    //
    DBTest.Host := IP;
    DBTest.DatabaseName := 'test';
    if VAR_SQLUSER <> '' then
      DBTest.UserName := VAR_SQLUSER
    else
      DBTest.UserName := 'root';
    //
    if VAR_BDSENHA <> '' then
      DBTest.UserPassword := VAR_BDSENHA
    else
      DBTest.UserPassword := CO_USERSPNOW;
    //
    if VAR_PORTA <> 0 then
      DBTest.Port := VAR_PORTA;
    //
    DBTest.Connect;
    Result := DBTest.Connected;
  end else Result := True;
end;

procedure TFmPrincipal.CoresVCLSkin1Click(Sender: TObject);
begin
{
  Application.CreateForm(TFmCoresVCLSkin, FmCoresVCLSkin);
  FmCoresVCLSkin.ShowModal;
  FmCoresVCLSkin.Destroy;
}
end;

procedure TFmPrincipal.dmkDBGrid1CellClick(Column: TColumn);
var
  Status: Integer;
  Tabela: String;
begin
  if Column.FieldName = 'Selecio' then
  begin
    Status := QrMinhasSelecio.Value;
    if Status = 0 then Status := 1 else Status := 0;
    Tabela := String(QrMinhasTabela.Value);
    QrMy.SQL.Clear;
    QrMy.SQL.Add('UPDATE minhas SET Selecio=:P0');
    QrMy.SQL.Add('WHERE Tabela=:P1');
    QrMy.Params[00].AsInteger := Status;
    QrMy.Params[01].AsString := Tabela;
    QrMy.ExecSQL;
    //
    QrMinhas.Close;
    QrMinhas.Open;
    QrMinhas.Locate('Tabela', Tabela, [loCaseInsensitive])
  end else if Column.FieldName = 'Tabela' then
  begin
    TbReg2.Close;
    TbReg2.TableName := QrMinhas.Fields[0].AsString;
    (*if CkLimitar.Checked then
    begin
      TbReg2.Limit := StrToInt(Ed2.Text);
      TbReg2.Offset := StrToInt(Ed1.Text);
    end else begin
      TbReg2.Limit := -1;
      TbReg2.Offset := 0;
    end;*)
    TbReg2.Open;
  end;
end;

procedure TFmPrincipal.EdCampoChange(Sender: TObject);
begin
  ReopenCampos();
end;

procedure TFmPrincipal.EdDBFiltroChange(Sender: TObject);
begin
  QrDatabases.Close;
end;

procedure TFmPrincipal.EdIntNewChange(Sender: TObject);
begin
end;


//primeiro colocar itens na tabela de execu��o
//Euro = 1154?

end.

