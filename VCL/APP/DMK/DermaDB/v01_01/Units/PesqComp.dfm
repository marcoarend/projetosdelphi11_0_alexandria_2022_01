object FmPesqComp: TFmPesqComp
  Left = 339
  Top = 185
  Caption = '???-?????-999 :: Pesquisa em Componentes Delphi (DFM)'
  ClientHeight = 789
  ClientWidth = 1028
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1028
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 980
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 932
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 504
        Height = 32
        Caption = 'Pesquisa em Componentes Delphi (DFM)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 504
        Height = 32
        Caption = 'Pesquisa em Componentes Delphi (DFM)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 504
        Height = 32
        Caption = 'Pesquisa em Componentes Delphi (DFM)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 719
    Width = 1028
    Height = 70
    Align = alBottom
    TabOrder = 1
    object PnSaiDesis: TPanel
      Left = 882
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BitBtn1: TBitBtn
        Tag = 13
        Left = 26
        Top = 7
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 880
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtArquivos: TBitBtn
        Tag = 14
        Left = 24
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Arquivos'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtArquivosClick
      end
      object BtCancela: TBitBtn
        Tag = 15
        Left = 118
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Cancela'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtCancelaClick
      end
      object BtPesquisa: TBitBtn
        Tag = 15
        Left = 215
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Pesquisa'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtPesquisaClick
      end
      object BtSubstitui: TBitBtn
        Left = 311
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Exclui prop.'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BtSubstituiClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1028
    Height = 627
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1028
      Height = 627
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1028
        Height = 627
        Align = alClient
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 1024
          Height = 610
          Align = alClient
          TabOrder = 0
          object Panel6: TPanel
            Left = 1
            Top = 1
            Width = 1022
            Height = 360
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object ListBox1: TListBox
              Left = 769
              Top = 0
              Width = 132
              Height = 360
              Align = alClient
              ItemHeight = 13
              TabOrder = 0
              ExplicitHeight = 292
            end
            object Panel7: TPanel
              Left = 0
              Top = 0
              Width = 584
              Height = 360
              Align = alLeft
              TabOrder = 1
              ExplicitHeight = 292
              object GradeA: TStringGrid
                Left = 1
                Top = 197
                Width = 582
                Height = 162
                Align = alClient
                ColCount = 3
                DefaultColWidth = 40
                DefaultRowHeight = 17
                RowCount = 2
                Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
                TabOrder = 0
                OnDblClick = GradeADblClick
                ExplicitTop = 169
                ExplicitHeight = 122
                ColWidths = (
                  40
                  61
                  453)
              end
              object Panel20: TPanel
                Left = 1
                Top = 1
                Width = 582
                Height = 196
                Align = alTop
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 1
                object Label1: TLabel
                  Left = 8
                  Top = 4
                  Width = 108
                  Height = 13
                  Caption = 'Nome do componente:'
                end
                object Label3: TLabel
                  Left = 8
                  Top = 72
                  Width = 42
                  Height = 13
                  Caption = 'Diret'#243'rio:'
                end
                object SpeedButton1: TSpeedButton
                  Left = 440
                  Top = 88
                  Width = 23
                  Height = 22
                  Caption = '...'
                  OnClick = SpeedButton1Click
                end
                object Label17: TLabel
                  Left = 8
                  Top = 112
                  Width = 166
                  Height = 13
                  Caption = 'Arquivo contendo lista de arquivos:'
                end
                object SpeedButton9: TSpeedButton
                  Left = 440
                  Top = 128
                  Width = 23
                  Height = 22
                  Caption = '...'
                  OnClick = SpeedButton9Click
                end
                object Label18: TLabel
                  Left = 8
                  Top = 152
                  Width = 104
                  Height = 13
                  Caption = 'Substituir "Texto" por:'
                end
                object Label2: TLabel
                  Left = 172
                  Top = 4
                  Width = 60
                  Height = 13
                  Caption = 'Propriedade:'
                end
                object Label5: TLabel
                  Left = 336
                  Top = 4
                  Width = 27
                  Height = 13
                  Caption = 'Valor:'
                end
                object EdNomeCompo: TdmkEdit
                  Left = 8
                  Top = 20
                  Width = 161
                  Height = 21
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object CkSub1: TCheckBox
                  Left = 124
                  Top = 46
                  Width = 117
                  Height = 17
                  Caption = 'Incluir subdiret'#243'rios.'
                  Checked = True
                  State = cbChecked
                  TabOrder = 1
                end
                object CkCaseSensitive: TCheckBox
                  Left = 8
                  Top = 46
                  Width = 97
                  Height = 17
                  Caption = 'Case sensitive.'
                  TabOrder = 2
                end
                object EdDiretorio: TdmkEdit
                  Left = 8
                  Top = 88
                  Width = 429
                  Height = 21
                  TabOrder = 3
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object Panel21: TPanel
                  Left = 464
                  Top = 0
                  Width = 118
                  Height = 196
                  Align = alRight
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 4
                  ExplicitHeight = 168
                  object StaticText3: TStaticText
                    Left = 0
                    Top = 0
                    Width = 118
                    Height = 17
                    Align = alTop
                    Alignment = taCenter
                    Caption = 'Extens'#245'es de arquivos:'
                    TabOrder = 0
                  end
                  object MeExtensoes: TMemo
                    Left = 0
                    Top = 17
                    Width = 118
                    Height = 179
                    Align = alClient
                    Lines.Strings = (
                      '*.dfm')
                    TabOrder = 1
                    WantReturns = False
                    ExplicitHeight = 151
                  end
                end
                object EdArqArqs: TdmkEdit
                  Left = 8
                  Top = 128
                  Width = 429
                  Height = 21
                  TabOrder = 5
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdNovoTexto: TdmkEdit
                  Left = 9
                  Top = 168
                  Width = 452
                  Height = 21
                  TabOrder = 6
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdPropCompo: TdmkEdit
                  Left = 172
                  Top = 20
                  Width = 161
                  Height = 21
                  TabOrder = 7
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdValrCompo: TdmkEdit
                  Left = 336
                  Top = 20
                  Width = 121
                  Height = 21
                  TabOrder = 8
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
              end
            end
            object Panel19: TPanel
              Left = 584
              Top = 0
              Width = 185
              Height = 360
              Align = alLeft
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 2
              ExplicitHeight = 292
              object StaticText2: TStaticText
                Left = 0
                Top = 0
                Width = 185
                Height = 17
                Align = alTop
                Alignment = taCenter
                Caption = 'Excess'#245'es:'
                TabOrder = 0
              end
              object MeExcl: TMemo
                Left = 0
                Top = 17
                Width = 185
                Height = 343
                Align = alLeft
                TabOrder = 1
                WantReturns = False
                ExplicitHeight = 275
              end
            end
            object ListBox2: TListBox
              Left = 901
              Top = 0
              Width = 121
              Height = 360
              Align = alRight
              ItemHeight = 13
              TabOrder = 3
              ExplicitHeight = 292
            end
          end
          object Panel8: TPanel
            Left = 1
            Top = 361
            Width = 1022
            Height = 20
            Align = alTop
            TabOrder = 1
            ExplicitTop = 293
            object Label4: TLabel
              Left = 4
              Top = 4
              Width = 9
              Height = 13
              Caption = '...'
            end
          end
          object GradeB: TStringGrid
            Left = 1
            Top = 381
            Width = 901
            Height = 228
            Align = alClient
            ColCount = 4
            DefaultColWidth = 40
            DefaultRowHeight = 17
            RowCount = 2
            Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goEditing]
            TabOrder = 2
            OnDblClick = GradeBDblClick
            ExplicitTop = 313
            ExplicitHeight = 296
            ColWidths = (
              40
              329
              69
              526)
          end
          object RE1: TRichEdit
            Left = 608
            Top = 256
            Width = 185
            Height = 89
            TabOrder = 3
            Visible = False
            Zoom = 100
          end
          object ListBox3: TListBox
            Left = 902
            Top = 381
            Width = 121
            Height = 228
            Align = alRight
            ItemHeight = 13
            TabOrder = 4
            ExplicitTop = 313
            ExplicitHeight = 296
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 675
    Width = 1028
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1024
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object PMArquivos: TPopupMenu
    Left = 124
    Top = 588
    object Dodiretrioselecionado1: TMenuItem
      Caption = '&Diret'#243'rio selecionado'
      OnClick = Dodiretrioselecionado1Click
    end
    object Listadearquivos1: TMenuItem
      Caption = '&Lista de arquivos do arquivo selecionado'
      OnClick = Listadearquivos1Click
    end
  end
end
