object FmMegasena: TFmMegasena
  Left = 419
  Top = 217
  Caption = 'XXX-M_SENA-001 :: Mega Sena'
  ClientHeight = 448
  ClientWidth = 803
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 803
    Height = 48
    Align = alTop
    Caption = 'Mega Sena'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 0
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 801
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 683
      ExplicitHeight = 44
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 803
    Height = 400
    Align = alClient
    TabOrder = 1
    object PageControl1: TPageControl
      Left = 1
      Top = 1
      Width = 801
      Height = 398
      ActivePage = TabSheet2
      Align = alClient
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = 'Arquivo da CEF'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 669
        ExplicitHeight = 0
        object Panel1: TPanel
          Left = 0
          Top = 0
          Width = 793
          Height = 48
          Align = alTop
          TabOrder = 0
          ExplicitWidth = 669
          object Label1: TLabel
            Left = 4
            Top = 4
            Width = 74
            Height = 13
            Caption = 'Primeiro sorteio:'
          end
          object Edit1: TEdit
            Left = 4
            Top = 20
            Width = 121
            Height = 21
            TabOrder = 0
            Text = '<td>1</td>'
          end
          object BtOK: TBitBtn
            Tag = 14
            Left = 136
            Top = 4
            Width = 90
            Height = 40
            Caption = '&Abre'
            TabOrder = 1
            OnClick = BtOKClick
            NumGlyphs = 2
          end
          object BtLe: TBitBtn
            Left = 234
            Top = 4
            Width = 90
            Height = 40
            Caption = '&L'#234' arquivo'
            TabOrder = 2
            OnClick = BtLeClick
            NumGlyphs = 2
          end
        end
        object Memo: TMemo
          Left = 0
          Top = 48
          Width = 793
          Height = 322
          Align = alClient
          TabOrder = 1
          ExplicitWidth = 669
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Resultados'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 321
          Height = 370
          Align = alLeft
          BevelOuter = bvNone
          Caption = 'Panel3'
          TabOrder = 0
          object SgSorteio: TStringGrid
            Left = 0
            Top = 17
            Width = 321
            Height = 353
            Align = alClient
            ColCount = 20
            DefaultColWidth = 32
            DefaultRowHeight = 18
            FixedCols = 0
            RowCount = 2
            Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goDrawFocusSelected, goColSizing, goEditing]
            TabOrder = 0
          end
          object StaticText1: TStaticText
            Left = 0
            Top = 0
            Width = 42
            Height = 17
            Align = alTop
            Alignment = taCenter
            Caption = 'Sorteios'
            TabOrder = 1
          end
        end
        object Panel4: TPanel
          Left = 417
          Top = 0
          Width = 96
          Height = 370
          Align = alLeft
          Caption = 'Panel4'
          TabOrder = 1
          object StaticText2: TStaticText
            Left = 1
            Top = 1
            Width = 88
            Height = 17
            Align = alTop
            Caption = 'Ordem freq'#252#234'ncia'
            TabOrder = 0
          end
          object SgFreq: TStringGrid
            Left = 1
            Top = 18
            Width = 94
            Height = 351
            Align = alClient
            ColCount = 2
            DefaultColWidth = 32
            DefaultRowHeight = 18
            RowCount = 2
            TabOrder = 1
          end
        end
        object Panel5: TPanel
          Left = 321
          Top = 0
          Width = 96
          Height = 370
          Align = alLeft
          Caption = 'Panel4'
          TabOrder = 2
          object StaticText3: TStaticText
            Left = 1
            Top = 1
            Width = 81
            Height = 17
            Align = alTop
            Caption = 'Ordem num'#233'rica'
            TabOrder = 0
          end
          object SgVezes: TStringGrid
            Left = 1
            Top = 18
            Width = 94
            Height = 351
            Align = alClient
            ColCount = 2
            DefaultColWidth = 32
            DefaultRowHeight = 18
            RowCount = 2
            TabOrder = 1
          end
        end
        object Panel6: TPanel
          Left = 513
          Top = 0
          Width = 280
          Height = 370
          Align = alClient
          TabOrder = 3
          object MeJogos: TMemo
            Left = 1
            Top = 189
            Width = 278
            Height = 180
            Align = alClient
            TabOrder = 0
          end
          object LbFreq: TListBox
            Left = -27
            Top = 229
            Width = 162
            Height = 94
            ItemHeight = 13
            TabOrder = 1
            Visible = False
          end
          object Panel7: TPanel
            Left = 1
            Top = 1
            Width = 278
            Height = 188
            Align = alTop
            TabOrder = 2
            object BtJogo: TBitBtn
              Left = 6
              Top = 140
              Width = 90
              Height = 40
              Caption = 'Jogo'
              TabOrder = 0
              OnClick = BtJogoClick
              NumGlyphs = 2
            end
            object BitBtn2: TBitBtn
              Left = 6
              Top = 96
              Width = 90
              Height = 40
              Caption = '&Abre'
              TabOrder = 1
              OnClick = BitBtn2Click
              NumGlyphs = 2
            end
            object BitBtn1: TBitBtn
              Left = 6
              Top = 52
              Width = 90
              Height = 40
              Caption = '&Salva'
              TabOrder = 2
              OnClick = BitBtn1Click
              NumGlyphs = 2
            end
            object BtLista: TBitBtn
              Left = 6
              Top = 8
              Width = 90
              Height = 40
              Caption = '&Lista'
              TabOrder = 3
              OnClick = BtListaClick
              NumGlyphs = 2
            end
            object EdQtdeJogos: TdmkEdit
              Left = 100
              Top = 156
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              ValMin = '1'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '1'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 1
            end
          end
        end
      end
    end
  end
  object OpenDialog1: TOpenDialog
    Left = 193
    Top = 105
  end
end
