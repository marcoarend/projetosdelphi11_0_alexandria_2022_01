unit ChangeCods;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkLabel, Variants,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, dmkDBGridDAC, ComCtrls, Menus, Registry, dmkGeral,
  UnInternalConsts;

type
  TFmChangeCods = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    DBTest: TmySQLDatabase;
    QrUpdTest: TmySQLQuery;
    QrTabs: TmySQLQuery;
    QrTabsNome: TWideStringField;
    QrTabsCodigo: TIntegerField;
    QrTabsAtivo: TSmallintField;
    DsTabs: TDataSource;
    QrFlds: TmySQLQuery;
    QrFldsField: TWideStringField;
    QrFldsType: TWideStringField;
    QrFldsNull: TWideStringField;
    QrFldsKey: TWideStringField;
    QrFldsDefault: TWideStringField;
    QrFldsExtra: TWideStringField;
    DsFlds: TDataSource;
    QrTeste: TmySQLQuery;
    QrTesteTabela: TWideStringField;
    QrTesteCampo: TWideStringField;
    QrTesteItens: TIntegerField;
    QrTesteControle: TIntegerField;
    QrTesteAtivo: TSmallintField;
    DsTeste: TDataSource;
    Panel31: TPanel;
    PageControl1: TPageControl;
    TabSheet14: TTabSheet;
    dmkDBGridDAC1: TdmkDBGridDAC;
    DBGrid2: TDBGrid;
    TabSheet15: TTabSheet;
    DBGResult: TdmkDBGridDAC;
    DBGrid1: TDBGrid;
    PMExclTest: TPopupMenu;
    Excluiitensmarcados1: TMenuItem;
    Excluilinhasselecionadas1: TMenuItem;
    PMItensAMudar: TPopupMenu;
    Recriartabela1: TMenuItem;
    Incluirresultados1: TMenuItem;
    PainelDB: TPanel;
    QrRegistros: TmySQLQuery;
    DsRegistros: TDataSource;
    Panel5: TPanel;
    BtExclTest: TBitBtn;
    BtItensAMudar: TBitBtn;
    TabSheet1: TTabSheet;
    QrItsChange: TmySQLQuery;
    DsItsChange: TDataSource;
    DBGMudar: TdmkDBGridDAC;
    QrItsChangeTabela: TWideStringField;
    QrItsChangeCampo: TWideStringField;
    QrItsChangeControle: TIntegerField;
    QrItsChangeAtivo: TSmallintField;
    N1: TMenuItem;
    DBChg: TmySQLDatabase;
    QrExecuta: TmySQLQuery;
    QrTabelas: TmySQLQuery;
    DsTabelas: TDataSource;
    GridTabelas: TDBGrid;
    Incluiritensmarcados1: TMenuItem;
    QrItsChangeDB: TWideStringField;
    Panel6: TPanel;
    BtExeAlterCod: TBitBtn;
    DBAlter: TmySQLDatabase;
    QrAlter: TmySQLQuery;
    BitBtn1: TBitBtn;
    QrNomes: TmySQLQuery;
    DsNomes: TDataSource;
    CBNome: TdmkDBLookupComboBox;
    Label1: TLabel;
    QrItsChangeNome: TWideStringField;
    QrNomesNome: TWideStringField;
    EdMudarOld: TdmkEdit;
    Label2: TLabel;
    Label3: TLabel;
    EdMudarNew: TdmkEdit;
    Panel3: TPanel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    EdIntAnt: TdmkEdit;
    EdIntNew: TdmkEdit;
    CkCampos: TCheckBox;
    MeNomes: TMemo;
    PB1: TProgressBar;
    PB2: TProgressBar;
    BtTabelas: TBitBtn;
    BitBtn16: TBitBtn;
    QrItsChangeExclui: TSmallintField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtTabelasClick(Sender: TObject);
    procedure BitBtn16Click(Sender: TObject);
    procedure BtExclTestClick(Sender: TObject);
    procedure Excluiitensmarcados1Click(Sender: TObject);
    procedure Excluilinhasselecionadas1Click(Sender: TObject);
    procedure BtItensAMudarClick(Sender: TObject);
    procedure Recriartabela1Click(Sender: TObject);
    procedure Incluirresultados1Click(Sender: TObject);
    procedure EdIntNewChange(Sender: TObject);
    procedure EdIntAntChange(Sender: TObject);
    procedure QrTabsAfterScroll(DataSet: TDataSet);
    procedure QrTesteAfterScroll(DataSet: TDataSet);
    procedure QrTesteBeforeClose(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure DBGResultDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure Incluiritensmarcados1Click(Sender: TObject);
    procedure BtExeAlterCodClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure CBNomeClick(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
  private
    { Private declarations }
    procedure DeleteItsChangeAtual();
    procedure ReopenItsChange(Controle: Integer);
  public
    { Public declarations }
  end;

  var
  FmChangeCods: TFmChangeCods;

implementation

uses UnMyObjects, Principal;

{$R *.DFM}

procedure TFmChangeCods.BitBtn16Click(Sender: TObject);
var
  I, Cod, Controle: Integer;
  Continua, Obrigatorio: Boolean;
begin
  Screen.Cursor := crHourGlass;
  try
    Controle := 0;
    DBTest.Connect;
    QrUpdTest.Close;
    QrUpdTest.SQL.Clear;
    QrUpdTest.SQL.Add('DROP TABLE IF EXISTS _psqchge_;');
    QrUpdTest.SQL.Add('CREATE TABLE _psqchge_ (');
    QrUpdTest.SQL.Add(' DB                 varchar(100)                       ,');
    QrUpdTest.SQL.Add(' Tabela             varchar(100)                       ,');
    QrUpdTest.SQL.Add(' Campo              varchar(100)                       ,');
    QrUpdTest.SQL.Add(' Controle           int(11) NOT NULL DEFAULT 0         ,');
    QrUpdTest.SQL.Add(' Itens              int(11) NOT NULL DEFAULT 0         ,');
    QrUpdTest.SQL.Add(' Ativo              tinyint(1) NOT NULL DEFAULT 0       ');
    QrUpdTest.SQL.Add(') TYPE=MyISAM;');
    QrUpdTest.ExecSQL;
    //
{
    QrTabs.Close;
    QrTabs.SQL.Clear;
    QrTabs.SQL.Add('SELECT * FROM _tabelas_');
    QrTabs.SQL.Add('WHERE Ativo=1');
    QrTabs.SQL.Add('ORDER BY Nome');
    QrTabs.Open;
}
    PB1.Position := 0;
    PB1.Max := QrTabs.RecordCount;
    QrTabs.First;
    Cod := EdIntAnt.ValueVariant;
    while not QrTabs.Eof do
    begin
      PB1.Position := PB1.Position + 1;
      PB1.Update;
      Application.ProcessMessages;
      //
{
      Continua := False;
      if (QrTabsAtivo.Value = 1) then Continua := True;
      if Continua = False then
      begin
        for I := 0 to MeNomes.Lines.Count - 1 do
        begin
           if Pos(AnsiLowercase(MeNomes.Lines[I]), AnsiLowercase(QrTabsNome.Value)) > 0 then
           begin
             Continua := True;
             Break;
           end;
        end;
      end;
}
      PB2.Position := 0;
      PB2.Max := QrFlds.RecordCount;
      PB2.Update;
      Application.ProcessMessages;
      QrFlds.First;
      while not QrFlds.Eof do
      begin
        PB2.Position := PB2.Position + 1;
        PB2.Update;
        Application.ProcessMessages;
        //
        Continua := False;
        Obrigatorio := False;
        if pos('int', String(QrFldsType.Value)) > 0 then Continua := True;
        for I := 0 to MeNomes.Lines.Count - 1 do
        begin
           if Pos(AnsiLowercase(MeNomes.Lines[I]), Lowercase(String(QrFldsField.Value))) > 0 then
           begin
             Continua := True;
             Obrigatorio := True;
             Break;
           end;
        end;
        if Continua then
        begin
          QrExecuta.Close;
          QrExecuta.SQL.Clear;
          QrExecuta.SQL.Add('SELECT COUNT(*) Itens FROM ' + QrTabs.Fields[0].AsString);
          QrExecuta.SQL.Add('WHERE ' + String(QrFldsField.Value) + '=' + FormatFloat('0', Cod));
          QrExecuta.Open;
          //
          if (not CkCampos.Checked) or (QrExecuta.FieldByName('Itens').AsInteger > 0) or
          Obrigatorio then
          begin
            Controle := Controle + 1;
            QrUpdTest.Close;
            QrUpdTest.SQL.Clear;
            QrUpdTest.SQL.Add('INSERT INTO _psqchge_ SET ');
            QrUpdTest.SQL.Add('Tabela=:P0, Campo=:P1, Itens=:P2, Controle=:P3');
            QrUpdTest.Params[00].AsString  := QrTabs.Fields[0].AsString;
            QrUpdTest.Params[01].AsString  := String(QrFldsField.Value);
            QrUpdTest.Params[02].AsInteger := QrExecuta.FieldByName('Itens').AsInteger;
            QrUpdTest.Params[03].AsInteger := Controle;
            QrUpdTest.ExecSQL;
          end;
        end;
        QrFlds.Next;
      end;
      QrTabs.Next;
    end;
    QrTeste.Close;
    QrTeste.Open;
    PageControl1.ActivePageIndex := 1;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmChangeCods.BitBtn1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if Geral.MensagemBox('Confirma a exclus�o do registro atual?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    DeleteItsChangeAtual();
    QrItsChange.Next;
    Controle := QrItsChangeControle.Value;
    ReopenItsChange(Controle);
  end;
end;

procedure TFmChangeCods.BtExclTestClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMExclTest, BtExclTest);
end;

procedure TFmChangeCods.BtExeAlterCodClick(Sender: TObject);
begin
  if Geral.MensagemBox('Confirma a altera��o dos itens abaixo?', 'Pergunta',
  MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    try
      QrAlter.Close;
      QrItsChange.First;
      while not QrItsChange.Eof do
      begin
        if (DBAlter.DatabaseName <> String(QrItsChangeDB.Value))
        or (DBAlter.Connected = False) then
        begin
          DBAlter.Disconnect;
          DBAlter.DatabaseName := String(QrItsChangeDB.Value);
          DBAlter.Connect;
        end;
        //
        if QrItsChangeExclui.Value = 1 then
        begin
          QrAlter.SQL.Clear;
          QrAlter.SQL.Add('DELETE FROM ' + String(QrItsChangeTabela.Value) + ' WHERE');
          QrAlter.SQL.Add(String(QrItsChangeCampo.Value) + '=' + FormatFloat('0', EdMudarNew.ValueVariant));
          QrAlter.ExecSQL;
        end;
        QrAlter.SQL.Clear;
        QrAlter.SQL.Add('UPDATE ' + String(QrItsChangeTabela.Value) + ' SET ');
        QrAlter.SQL.Add(String(QrItsChangeCampo.Value) + '=' + FormatFloat('0', EdMudarNew.ValueVariant));
        QrAlter.SQL.Add('WHERE');
        QrAlter.SQL.Add(String(QrItsChangeCampo.Value) + '=' + FormatFloat('0', EdMudarOld.ValueVariant));
        QrAlter.ExecSQL;
        // N�o Fazer! Usa mais de uma vez!
        //DeleteItsChangeAtual();
        //
        DBGMudar.Update;
        Application.ProcessMessages;
        //
        QrItsChange.Next;
      end;
      Geral.MensagemBox('Altera��es realizadas com sucesso!',
      'Mensagem', MB_OK+MB_ICONINFORMATION);
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmChangeCods.BtItensAMudarClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMItensAMudar, BtItensAMudar);
end;

procedure TFmChangeCods.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmChangeCods.BtTabelasClick(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
    DBTest.Connect;
    QrUpdTest.Close;
    QrUpdTest.SQL.Clear;
    QrUpdTest.SQL.Add('DROP TABLE IF EXISTS _tabelas_;');
    QrUpdTest.SQL.Add('CREATE TABLE _tabelas_ (');
    QrUpdTest.SQL.Add(' Nome               varchar(100)                        ,');
    QrUpdTest.SQL.Add(' Codigo             int(11)                            ,');
    QrUpdTest.SQL.Add(' Ativo              tinyint(1) NOT NULL DEFAULT 0       ');
    QrUpdTest.SQL.Add(') TYPE=MyISAM;');
    QrUpdTest.ExecSQL;
    //
    QrUpdTest.SQL.Clear;
    QrUpdTest.SQL.Add('INSERT INTO _tabelas_ SET ');
    QrUpdTest.SQL.Add('Nome=:P0, Codigo=:P1');
    QrTabelas.First;
    while not QrTabelas.Eof do
    begin
      QrUpdTest.Params[0].AsString  := QrTabelas.Fields[0].AsString;
      QrUpdTest.Params[1].AsInteger := QrTabelas.RecNo;
      QrUpdTest.ExecSQL;
      //
      QrTabelas.Next;
    end;
    //
    QrTabs.Close;
    QrTabs.SQL.Clear;
    QrTabs.SQL.Add('SELECT * FROM _tabelas_');
    QrTabs.SQL.Add('ORDER BY Nome');
    QrTabs.Open;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmChangeCods.CBNomeClick(Sender: TObject);
begin
  ReopenItsChange(0);
end;

procedure TFmChangeCods.DBGResultDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Cor: TColor;
begin
  if QrTesteItens.Value = 0 then
    Cor := clRed
  else
    Cor := clBlue;
  MyObjects.DesenhaTextoEmDBGrid(TDBGrid(DBGResult), Rect,
    Cor, clWindow, Column.Alignment, Column.Field.DisplayText);
end;

procedure TFmChangeCods.DeleteItsChangeAtual();
begin
  {  Mudar pois mudei a tabela!
  QrUpdTest.Close;
  QrUpdTest.SQL.Clear;
  QrUpdTest.SQL.Add('DELETE FROM _itschge_ ');
  QrUpdTest.SQL.Add('WHERE Controle=:P0 AND ValorOld=:P1 AND ValorNew=:P2');
  QrUpdTest.Params[00].AsInteger := QrItsChangeControle.Value;
  QrUpdTest.Params[01].AsInteger := QrItsChangeValorOld.Value;
  QrUpdTest.Params[02].AsInteger := QrItsChangeValorNew.Value;
  QrUpdTest.ExecSQL;
  }
end;

procedure TFmChangeCods.EdIntAntChange(Sender: TObject);
begin
  Geral.WriteAppKey('IntAnt', Application.Title, EdIntAnt.ValueVariant, ktInteger, HKEY_LOCAL_MACHINE);
end;

procedure TFmChangeCods.EdIntNewChange(Sender: TObject);
begin
  Geral.WriteAppKey('IntNew', Application.Title, EdIntNew.ValueVariant, ktInteger, HKEY_LOCAL_MACHINE);
end;

procedure TFmChangeCods.Excluiitensmarcados1Click(Sender: TObject);
var
  Controle: Integer;
begin
  QrUpdTest.Close;
  QrUpdTest.SQL.Clear;
  QrUpdTest.SQL.Add('DELETE FROM _psqchge_ ');
  QrUpdTest.SQL.Add('WHERE Ativo=1');
  QrUpdTest.ExecSQL;
  //
  if QrTeste.State <> dsInactive then
  begin
    if QrTesteAtivo.Value = 1 then
    begin
      while not QrTeste.Eof do
      begin
        if QrTesteAtivo.Value = 0 then Break;
        QrTeste.Next;
      end;
    end;
    Controle := QrTesteControle.Value;
  end else
    Controle := 0;
  QrTeste.Close;
  QrTeste.Open;
  QrTeste.Locate('Controle', Controle, []);
end;

procedure TFmChangeCods.Excluilinhasselecionadas1Click(Sender: TObject);
var
  I: Integer;
begin
  QrUpdTest.Close;
  QrUpdTest.SQL.Clear;
  QrUpdTest.SQL.Add('DELETE FROM _psqchge_');
  QrUpdTest.SQL.Add('WHERE Controle=:P0');
  with DBGResult.DataSource.DataSet do
  for I := 0 to DBGResult.SelectedRows.Count-1 do
  begin
    //GotoBookmark(pointer(DBGResult.SelectedRows.Items[I]));
    GotoBookmark(DBGResult.SelectedRows.Items[I]);
    //
    QrUpdTest.Params[0].AsInteger := QrTesteControle.Value;
    QrUpdTest.ExecSQL;
  end;
  QrTeste.Close;
  QrTeste.Open;
end;

procedure TFmChangeCods.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  QrTabelas.Close;
  QrTabelas.SQL.Clear;
  QrTabelas.SQL.Add('SHOW TABLES FROM ' + DBChg.DatabaseName);
  QrTabelas.Open;
  try
    QrTabs.Open;
  except
    ;
  end;
  try
    QrTeste.Open;
  except
    ;
  end;
  try
    QrNomes.Open;
  except
    ;
  end;
end;

procedure TFmChangeCods.FormCreate(Sender: TObject);
begin
  EdIntAnt.ValueVariant := Geral.ReadAppKey('IntAnt', Application.Title, ktInteger, 0, HKEY_LOCAL_MACHINE);
  EdIntNew.ValueVariant := Geral.ReadAppKey('IntNew', Application.Title, ktInteger, 0, HKEY_LOCAL_MACHINE);
end;

procedure TFmChangeCods.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmChangeCods.Incluiritensmarcados1Click(Sender: TObject);
begin
{ Parei aqui! Refazer porque mudei tabela _itenschge_
  QrUpdTest.Close;
  QrUpdTest.SQL.Clear;
  QrUpdTest.SQL.Add('INSERT INTO _itschge_ ');
  QrUpdTest.SQL.Add('SELECT "' + PainelDB.Caption + '" DB, Tabela, Campo, Controle, Itens, ');
  QrUpdTest.SQL.Add(FormatFloat('0', EdIntAnt.ValueVariant) + ' ValorOld, ');
  QrUpdTest.SQL.Add(FormatFloat('0', EdIntNew.ValueVariant) + ' ValorOld, ');
  QrUpdTest.SQL.Add('1 Ativo');
  QrUpdTest.SQL.Add('FROM _psqchge_ WHERE Ativo=1;');
  QrUpdTest.SQL.Add('DELETE FROM _psqchge_ WHERE Ativo=1;');
  QrUpdTest.ExecSQL;
  //
  ReopenItsChange(0);
  //
  QrTeste.Close;
  QrTeste.Open;
}
end;

procedure TFmChangeCods.Incluirresultados1Click(Sender: TObject);
begin
{ Parei aqui! Refazer porque mudei tabela _itenschge_
  QrUpdTest.Close;
  QrUpdTest.SQL.Clear;
  QrUpdTest.SQL.Add('INSERT INTO _itschge_ ');
  QrUpdTest.SQL.Add('SELECT Tabela, Campo, Controle, Itens, ');
  QrUpdTest.SQL.Add(FormatFloat('0', EdIntAnt.ValueVariant) + ' ValorOld, ');
  QrUpdTest.SQL.Add(FormatFloat('0', EdIntNew.ValueVariant) + ' ValorOld, ');
  QrUpdTest.SQL.Add('1 Ativo');
  QrUpdTest.SQL.Add('FROM _psqchge_;');
  QrUpdTest.SQL.Add('DELETE FROM _psqchge_;');
  QrUpdTest.ExecSQL;
  //
  ReopenItsChange(0);
  PageControl1.ActivePageIndex := 2;
  //
  QrTeste.Close;
  QrTeste.Open;
}
end;

procedure TFmChangeCods.PageControl1Change(Sender: TObject);
begin
  if PageControl1.ActivePageIndex = 2 then
  begin
    QrNomes.Close;
    QrNomes.Open;
  end;
end;

procedure TFmChangeCods.QrTabsAfterScroll(DataSet: TDataSet);
begin
  QrFlds.SQL.Clear;
  QrFlds.SQL.Add('DESCRIBE '+QrTabs.Fields[0].AsString);
  QrFlds.Open;
end;

procedure TFmChangeCods.QrTesteAfterScroll(DataSet: TDataSet);
begin
  QrRegistros.Close;
  QrRegistros.SQL.Clear;
  if QrTesteItens.Value > 0 then
  begin
    QrRegistros.SQL.Add('SELECT * FROM ' + String(QrTesteTabela.Value));
    QrRegistros.SQL.Add('WHERE ' + String(QrTesteCampo.Value) + '=' +
    FormatFloat('0', EdIntAnt.ValueVariant));
  end else begin
    QrRegistros.SQL.Add('SELECT xx.' + String(QrTesteCampo.Value) + ', xx.* ');
    QrRegistros.SQL.Add('FROM ' + String(QrTesteTabela.Value) + ' xx');
    QrRegistros.SQL.Add('ORDER BY xx.' + String(QrTesteCampo.Value));
    QrRegistros.SQL.Add('LIMIT 0, 1000');
  end;
  QrRegistros.Open;
end;

procedure TFmChangeCods.QrTesteBeforeClose(DataSet: TDataSet);
begin
  QrRegistros.Close;
end;

procedure TFmChangeCods.Recriartabela1Click(Sender: TObject);
begin
  if Geral.MensagemBox('Confirma a recria��o da tabela?' + #13#10 +
  'Todos itens atuais ser�o perdidos!', 'Pergunta',
  MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    QrUpdTest.Close;
    QrUpdTest.SQL.Clear;
    QrUpdTest.SQL.Add('DROP TABLE IF EXISTS _ItsChge_;');
    QrUpdTest.SQL.Add('CREATE TABLE _ItsChge_ (');
    QrUpdTest.SQL.Add(' DB                 varchar(100)                       ,');
    QrUpdTest.SQL.Add(' Tabela             varchar(100)                       ,');
    QrUpdTest.SQL.Add(' Campo              varchar(100)                       ,');
    QrUpdTest.SQL.Add(' Controle           int(11) NOT NULL DEFAULT 0         ,');
    QrUpdTest.SQL.Add(' Itens              int(11) NOT NULL DEFAULT 0         ,');
    QrUpdTest.SQL.Add(' ValorOld           int(11) NOT NULL DEFAULT 0         ,');
    QrUpdTest.SQL.Add(' ValorNew           int(11) NOT NULL DEFAULT 0         ,');
    QrUpdTest.SQL.Add(' Ativo              tinyint(1) NOT NULL DEFAULT 0       ');
    QrUpdTest.SQL.Add(') TYPE=MyISAM;');
    QrUpdTest.ExecSQL;
  end;
end;

procedure TFmChangeCods.ReopenItsChange(Controle: Integer);
begin
  QrItsChange.Close;
  if CBNome.KeyValue <> Null then
  begin
    QrItsChange.Params[0].AsString := CBNome.Text;
    QrItsChange.Open;
    QrItsChange.Locate('Controle', Controle, []);
  end;
end;

end.
