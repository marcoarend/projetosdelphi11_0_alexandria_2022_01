unit DBsDACab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnMLAGeral, UnInternalConsts, UnMsgInt, UnInternalConsts2,
  mySQLDbTables, dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage,
  dmkRadioGroup, Menus, Grids, DBGrids, UnDmkProcFunc, UnDmkENums, UnGOTOm,
  ABSMain;

type
  TFmDBsDACab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DGDados: TDBGrid;
    QrDBsDACab: TABSQuery;
    QrDBsDACabCodigo: TIntegerField;
    QrDBsDACabNome: TWideStringField;
    QrDBsDACabNomeUTF: TWideStringField;
    DsDBsDACab: TDataSource;
    QrDBsDAIts: TABSQuery;
    DsDBsDAIts: TDataSource;
    QrDBsDAItsCodigo: TIntegerField;
    QrDBsDAItsNome: TWideStringField;
    QrDBsDAItsNomeUTF: TWideStringField;
    QrDBsDAItsControle: TIntegerField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrDBsDACabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrDBsDACabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrDBsDACabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrDBsDACabBeforeClose(DataSet: TDataSet);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure MostraDBsDAIts(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure ReopenDBsDAIts(Controle: Integer);

  end;

var
  FmDBsDACab: TFmDBsDACab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, UMemModule, Module, ModMemTab, DmkDAC_PF, DBsDAIts, WinRegDB,
MemDBCheck;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmDBsDACab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOm.LC(Atual, Codigo);
end;

procedure TFmDBsDACab.MostraDBsDAIts(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmDBsDAIts, FmDBsDAIts, afmoNegarComAviso) then
  begin
    FmDBsDAIts.ImgTipo.SQLType := SQLType;
    FmDBsDAIts.FQrCab := QrDBsDACab;
    FmDBsDAIts.FDsCab := DsDBsDACab;
    FmDBsDAIts.FQrIts := QrDBsDAIts;
    if SQLType = stIns then
      //
    else
    begin
      FmDBsDAIts.EdControle.ValueVariant := QrDBsDAItsControle.Value;
      //
      FmDBsDAIts.EdNome.Text := QrDBsDAItsNome.Value;
    end;
    FmDBsDAIts.ShowModal;
    FmDBsDAIts.Destroy;
  end;
end;

procedure TFmDBsDACab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrDBsDACab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrDBsDACab, QrDBsDAIts);
end;

procedure TFmDBsDACab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrDBsDACab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrDBsDAIts);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrDBsDAIts);
end;

procedure TFmDBsDACab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOm.Go(Para, QrDBsDACabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmDBsDACab.DefParams;
begin
  VAR_GOTOTABELA := 'dbsdacab';
  VAR_GOTOABSTABLE := QrDBsDACab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOm.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM dbsdacab');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmDBsDACab.ItsAltera1Click(Sender: TObject);
begin
  MostraDBsDAIts(stUpd);
end;

procedure TFmDBsDACab.CabExclui1Click(Sender: TObject);
var
  Codigo: Integer;
  CtrFmt, Caminho: String;
begin
  if Geral.MB_Pergunta(
  'Confirma a exclus�o do cadastro do App do Registro do Windows?') = ID_YES then
  begin
    Caminho := MyObjects.DBsDA_CaminhoTabela('DBsDACab');
    CtrFmt  := MyObjects.DBsDA_FmtRegistro(QrDBsDACabCodigo.Value);
    if Geral.DelValAppKeyCU(Caminho, CtrFmt) then
    begin
      Caminho := MyObjects.DBsDA_CaminhoRegistro(Caminho, QrDBsDACabCodigo.Value);
      if Geral.DelKeyAppKeyCU(Caminho)then
      begin
        if DmModMemTab.DBsDACab_Exclui(QrDBsDACabCodigo.Value) then
        begin
          Codigo := UMemMod.ProximoRegistro(QrDBsDACab, 'Codigo', Codigo);
          LocCod(Codigo, Codigo);
        end;
      end;
    end;
  end;
end;

procedure TFmDBsDACab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmDBsDACab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmDBsDACab.ReopenDBsDAIts(Controle: Integer);
begin
  UMemMod.AbreABSQuery1(QrDBsDAIts, [
  'SELECT * ',
  'FROM dbsdaits ',
  'WHERE Codigo=' + Geral.FF0(QrDBsDACabCodigo.Value)
  ]);
//
end;

procedure TFmDBsDACab.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
  CtrFmt, Caminho: String;
begin
  if Geral.MB_Pergunta(
  'Confirma a exclus�o do cadastro de BD do Registro do Windows?') = ID_YES then
  begin
    Caminho := MyObjects.DBsDA_CaminhoTabela('DBsDAIts');
    CtrFmt  := MyObjects.DBsDA_FmtRegistro(QrDBsDAItsControle.Value);
    if Geral.DelValAppKeyCU(Caminho, CtrFmt) then
    begin
      Caminho := MyObjects.DBsDA_CaminhoRegistro(Caminho, QrDBsDAItsControle.Value);
      if Geral.DelKeyAppKeyCU(Caminho)then
      begin
        if DmModMemTab.DBsDAIts_Exclui(QrDBsDAItsControle.Value) then
        begin
          Controle := UMemMod.ProximoRegistro(QrDBsDAIts, 'Controle', Controle);
          ReopenDBsDAIts(Controle);
        end;
      end;
    end;
  end;
end;

procedure TFmDBsDACab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmDBsDACab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmDBsDACab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmDBsDACab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmDBsDACab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmDBsDACab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmDBsDACab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrDBsDACabCodigo.Value;
  Close;
end;

procedure TFmDBsDACab.ItsInclui1Click(Sender: TObject);
begin
  MostraDBsDAIts(stIns);
end;

procedure TFmDBsDACab.CabAltera1Click(Sender: TObject);
begin
  UMemMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrDBsDACab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'dbsdacab');
end;

procedure TFmDBsDACab.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  CodFmt, Nome: String;
  //
  Caminho: String;
begin
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UnWinRegDB.BPGS1I32('dbsdacab', 'Codigo', '', '',
    tsPos, ImgTipo.SQLType, QrDBsDACabCodigo.Value);
  CodFmt := MyObjects.DBsDA_FmtRegistro(Codigo);
  if DmModMemTab.DBsDACab_CarregaRegistro(Codigo, Nome) then
  begin
    //
    Caminho := MyObjects.DBsDA_CaminhoTabela('DBsDACab');
    //Caminho :=  Application.Title + '\DBS\' + TMeuDB + '\' + 'dbsdacab\Records';
    Geral.WriteAppKeyCU(CodFmt, Caminho, Nome, ktString);
    //
    Caminho := MyObjects.DBsDA_CaminhoRegistro(Caminho, Codigo);
    Geral.WriteAppKeyCU('Codigo', Caminho, Codigo, ktInteger);
    Geral.WriteAppKeyCU('Nome', Caminho, Nome, ktString);
    //
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOm.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmDBsDACab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
{{
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'dbsdacab', Codigo);
}
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
{{
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'dbsdacab', 'Codigo');
}
end;

procedure TFmDBsDACab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmDBsDACab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmDBsDACab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmDBsDACab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOm.Codigo(QrDBsDACabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmDBsDACab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOm.Nome(LaRegistro.Caption);
end;

procedure TFmDBsDACab.SbNovoClick(Sender: TObject);
begin
{{
  LaRegistro.Caption := GOTOm.CodUsu(QrDBsDACabCodigo.Value, LaRegistro.Caption);
}
end;

procedure TFmDBsDACab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOm.Fechar(ImgTipo.SQLType);
end;

procedure TFmDBsDACab.QrDBsDACabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmDBsDACab.QrDBsDACabAfterScroll(DataSet: TDataSet);
begin
  ReopenDBsDAIts(0);
end;

procedure TFmDBsDACab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrDBsDACabCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmDBsDACab.SbQueryClick(Sender: TObject);
begin
{
  LocCod(QrDBsDACabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'dbsdacab', Dmod.MyDB, CO_VAZIO));
}
end;

procedure TFmDBsDACab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmDBsDACab.CabInclui1Click(Sender: TObject);
begin
  UMemMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrDBsDACab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'dbsdacab');
end;

procedure TFmDBsDACab.QrDBsDACabBeforeClose(
  DataSet: TDataSet);
begin
  QrDBsDAIts.Close;
end;

procedure TFmDBsDACab.QrDBsDACabBeforeOpen(DataSet: TDataSet);
begin
  QrDBsDACabCodigo.DisplayFormat := FFormatFloat;
end;

end.

