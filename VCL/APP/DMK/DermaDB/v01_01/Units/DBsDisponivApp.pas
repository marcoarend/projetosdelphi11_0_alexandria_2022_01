unit DBsDisponivApp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkDBGridZTO,
  ABSMain, dmkRadioGroup,
  // Provisorio
  System.Win.Registry;

type
  TFmDBsDisponivApp = class(TForm)
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    Panel5: TPanel;
    Label1: TLabel;
    EdNome: TdmkEdit;
    dmkDBGridZTO1: TdmkDBGridZTO;
    BtInsUpd: TBitBtn;
    Label4: TLabel;
    EdCodigo: TdmkEdit;
    Qr_: TABSQuery;
    Qr_versao: TFloatField;
    Qr_CliCodi: TWideStringField;
    Qr_CliNome: TWideStringField;
    Qr_NomeUTF: TWideStringField;
    Qr_Cidade: TWideStringField;
    Qr_UF: TWideStringField;
    Qr_Pais: TWideStringField;
    Qr_Ativo: TSmallintField;
    Ds_: TDataSource;
    QrDBsDisponivApp: TABSQuery;
    DsDBsDisponivApp: TDataSource;
    QrDBsDisponivAppCodigo: TIntegerField;
    QrDBsDisponivAppNome: TWideStringField;
    QrDBsDisponivAppNomeUTF: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtInsUpdClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure dmkDBGridZTO1DblClick(Sender: TObject);
    procedure EdNomeChange(Sender: TObject);
    procedure EdCodigoChange(Sender: TObject);
  private
    { Private declarations }
    procedure DefinedadosPorSelecao();
    procedure ReabrePesquisa();
  public
    { Public declarations }
  end;

  var
  FmDBsDisponivApp: TFmDBsDisponivApp;

implementation

uses UnMyObjects, Module, UMemModule, UnApp_CreateMT;

{$R *.DFM}

procedure TFmDBsDisponivApp.BtInsUpdClick(Sender: TObject);
const
  Ativo = '1';
var
  CliCodi, CliNome, Cidade, UF, Pais: String;
begin
  //Geral.WriteAppKeyCU('Teste3', 'DermaDB\Apps', 'Teste App', ktString);
  Geral.WriteAppKeyCU('QtdItens', 'DermaDB\Apps', 3, ktInteger);
  Geral.WriteAppKeyCU('1', 'DermaDB\Apps\Lista', 'BlueDerm', ktString);
  Geral.WriteAppKeyCU('2', 'DermaDB\Apps\Lista', 'Bugstrol', ktString);
  Geral.WriteAppKeyCU('3', 'DermaDB\Apps\Lista', 'Credito2', ktString);
  //Geral.WriteAppKeyCU('Teste4', 'DermaDB/Apps', 'Teste App', ktString);
{{
  CliCodi := EdCliCodi.Text;
  CliNome := EdCliNome.Text;
  Cidade  := EdCidade.Text;
  UF      := EdUF.Text;
  Pais    := EdPais.Text;
  //
  if MyObjects.FIC(Trim(CliCodi) = '', EdCliCodi, 'Defina o c�digo do cliente!') then
    Exit;
  if MyObjects.FIC(Trim(CliNome) = '', EdCliNome, 'Defina o nome do cliente!') then
    Exit;
  if Dmod.InserirRegistroEntCliCad(CliCodi, CliNome, Cidade, UF, Pais, Ativo) then
  begin
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, MSG_SAVE_XML);
    Dmod.SalvaXML_EntCliCod(False);
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    //
    //
    Dmod.AbreEntCliCad(Dmod.TbEntCliCad);
    UMemMod.AbreABSTable1(Dmod.TbEntCliCad);
    //
    EdCliCodi.Text := '';
    EdCliNome.Text := '';
    EdCidade.Text  := '';
    EdUF.Text      := '';
    //EdPais.Text    := '';
    //
    ReabrePesquisa();
    QrEntCliCad.Locate('CliCodi', CliCodi, []);
  end;
}
end;

procedure TFmDBsDisponivApp.BtOKClick(Sender: TObject);
begin
  DefinedadosPorSelecao();
end;

procedure TFmDBsDisponivApp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmDBsDisponivApp.DefinedadosPorSelecao();
begin
  Close;
end;

procedure TFmDBsDisponivApp.dmkDBGridZTO1DblClick(Sender: TObject);
begin
  EdCodigo.ValueVariant := QrDBsDisponivAppCodigo.Value;
  EdNome.Text           := QrDBsDisponivAppNome.Value;
end;

procedure TFmDBsDisponivApp.EdCodigoChange(Sender: TObject);
begin
{{
  MyObjects.AlteraCaptionBtInsUpd(BtInsUpd,
    Dmod.LocalizaEntCliCad(EdCliCodi.Text));
}
end;

procedure TFmDBsDisponivApp.EdNomeChange(Sender: TObject);
begin
  ReabrePesquisa();
end;

procedure TFmDBsDisponivApp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmDBsDisponivApp.FormCreate(Sender: TObject);
  function ReadAppsKeyCU(const Key: String; KeyType: TKeyType;
    DefValue: Variant): Variant;
  var
    r : TRegistry;
  begin
    r := TRegistry.Create(KEY_READ);
    //r.RootKey := HKEY_LOCAL_MACHINE;
    r.RootKey := HKEY_CURRENT_USER;
    try
      r.OpenKeyReadOnly('Software\');
      Result := DefValue;
      if r.ValueExists(Key) then
      begin
        case KeyType of
          ktString: Result := r.ReadString(Key);
          ktBoolean: Result := r.ReadBool(Key);
          ktInteger: Result := r.ReadInteger(Key);
          ktCurrency: Result := r.ReadCurrency(Key);
          ktDate: Result := r.ReadDate(Key);
          ktTime: Result := r.ReadTime(Key);
          //ktBinary: Result := r.ReadBinaryData(Key);
        end;
      end;
    finally
      r.Free;
    end;
  end;

  function CarregaDataBases(var MeuDB: String; const ExeName: String): Boolean;
  var
    U, I, Conta: Integer;
    BDs: array of String;
  begin
    Result := False;
    Conta := Geral.ReadAppKeyCU('Count', Application.Title+'\Databases', ktInteger, 0);
    if Conta > 0 then
    begin
      SetLength(BDs, Conta);
      for I := 0 to Conta -1 do
        BDs[I] := Geral.ReadAppKeyCU(FormatFloat('0', I),
        Application.Title+'\Databases', ktString, MeuDB);
      U := Geral.ReadAppKeyCU('Last', Application.Title + '\' + ExeName, ktInteger, -1);
      I := MyObjects.SelRadioGroup('Sele��o da Base de Dados',
      'Lista de databases cadastradas', BDs, 2, U);
      if I > -1 then
      begin
        MeuDB := BDs[I];
        Geral.WriteAppKeyCU('Last', Application.Title + '\' + ExeName, I, ktInteger);
      end;
        //
      Result := MeuDB <> '';
      if not Result then
        Halt(0);
    end;// else TMeuDB := MeuDB;
  end;

  function CarregaApps(): Boolean;
  const
    ExeName = 'DermaDB';
    MeuDB = 'xyz';
  var
    U, I, Conta: Integer;
    BDs: array of String;
  begin
    Result := False;
    Conta := Geral.ReadAppKeyCU('Count', Application.Title+'\Apps', ktInteger, 0);
    if Conta > 0 then
    begin
      SetLength(BDs, Conta);
      for I := 0 to Conta -1 do
        BDs[I] := Geral.ReadAppKeyCU(FormatFloat('0', I),
        Application.Title+'\Apps', ktString, MeuDB);
      U := Geral.ReadAppKeyCU('Last', Application.Title + '\' + ExeName, ktInteger, -1);
      (*
      I := MyObjects.SelRadioGroup('Sele��o da Base de Dados',
      'Lista de databases cadastradas', BDs, 2, U);
      if I > -1 then
      begin
        MeuDB := BDs[I];
        Geral.WriteAppKeyCU('Last', Application.Title + '\' + ExeName, I, ktInteger);
      end;
        //
      Result := MeuDB <> '';
      if not Result then
        Halt(0);
      *)
    end;// else TMeuDB := MeuDB;
  end;
begin
  ReabrePesquisa();

  // Provisorio



end;

procedure TFmDBsDisponivApp.ReabrePesquisa();
var
  Texto: String;
begin
  Texto := Geral.MyUTF(EdNome.Text);
  QrDBsDisponivApp.Close;
  UMemMod.AbreABSQuery1(QrDBsDisponivApp, [
  'SELECT dda.* ',
  'FROM dbsdisponivapp dda ',
  'WHERE dda.NomeUTF LIKE "%' + Texto + '%"',
  'ORDER BY dda.NomeUTF ',
  '']);
end;

end.
