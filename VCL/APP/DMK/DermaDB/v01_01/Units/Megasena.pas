unit Megasena;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, ComCtrls, dmkEdit, dmkGeral;

type
  TFmMegasena = class(TForm)
    PainelTitulo: TPanel;
    Image1: TImage;
    OpenDialog1: TOpenDialog;
    Panel2: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel1: TPanel;
    Label1: TLabel;
    Edit1: TEdit;
    BtOK: TBitBtn;
    Memo: TMemo;
    Panel3: TPanel;
    SgSorteio: TStringGrid;
    StaticText1: TStaticText;
    Panel4: TPanel;
    StaticText2: TStaticText;
    SgFreq: TStringGrid;
    Panel5: TPanel;
    StaticText3: TStaticText;
    SgVezes: TStringGrid;
    Panel6: TPanel;
    MeJogos: TMemo;
    LbFreq: TListBox;
    BtLe: TBitBtn;
    Panel7: TPanel;
    BtJogo: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn1: TBitBtn;
    BtLista: TBitBtn;
    EdQtdeJogos: TdmkEdit;
    procedure BtOKClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtLeClick(Sender: TObject);
    procedure BtListaClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BtJogoClick(Sender: TObject);
  private
    { Private declarations }
    FArquivo: String;
  public
    { Public declarations }
  end;

  var
  FmMegasena: TFmMegasena;

implementation

uses UnMyObjects;

{$R *.DFM}

procedure TFmMegasena.BtOKClick(Sender: TObject);
begin
  Memo.Lines.Clear;
  if OpenDialog1.Execute then
    Memo.Lines.LoadFromFile(OpenDialog1.FileName);
    //MLAGeral.LeArquivoToMemo(OpenDialog1.FileName, Memo);
end;

procedure TFmMegasena.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmMegasena.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmMegasena.FormCreate(Sender: TObject);
var
  i: Integer;
begin
  FArquivo := ExtractFileDir(Application.Exename)+'\MegaSena.der';
  SgSorteio.Cells[00,00] := 'N�';
  SgSorteio.Cells[01,00] := 'Data';
  SgSorteio.Cells[02,00] := 'N� 1';
  SgSorteio.Cells[03,00] := 'N� 2';
  SgSorteio.Cells[04,00] := 'N� 3';
  SgSorteio.Cells[05,00] := 'N� 4';
  SgSorteio.Cells[06,00] := 'N� 5';
  SgSorteio.Cells[07,00] := 'N� 6';
  //
  SgSorteio.ColWidths[00] := 32;
  SgSorteio.ColWidths[01] := 64;
  SgSorteio.ColWidths[02] := 32;
  SgSorteio.ColWidths[03] := 32;
  SgSorteio.ColWidths[04] := 32;
  SgSorteio.ColWidths[05] := 32;
  SgSorteio.ColWidths[06] := 32;
  SgSorteio.ColWidths[07] := 32;
  //
  SgVezes.Cells[00,00] := 'N�';
  SgVezes.Cells[00,01] := 'x';
  //
  SgFreq.Cells[00,00] := 'N�';
  SgFreq.Cells[00,01] := 'x';
  //
  SgVezes.RowCount := 61;
  SgFreq.RowCount := 61;
  for i := 1 to 60 do
  begin
    SgVezes.Cells[00, i] := IntToStr(i);
    SgFreq.Cells[00, i] := IntToStr(i);
  end;
end;

procedure TFmMegasena.BtLeClick(Sender: TObject);
var
  i, j, c, k, m: Integer;
  s: String;
begin
  j := 0;
  m := 0;
  for i := 1 to Memo.Lines.Count do
  begin
    s := Memo.Lines[i];
    if s = Edit1.Text then
    begin
      j := i;
      Break;
    end;
  end;
  if j = 0 then Exit;
  c := 0;
  while j < Memo.Lines.Count do
  begin
    k := Geral.IMV(Geral.SoNumero_TT(Memo.Lines[j]));
    if k > 0 then
    begin
      m := k;
      c := c + 1;
      SgSorteio.RowCount := c + 1;
      SgSorteio.Cells[00, c] := IntToStr(m);                            // N�mero concurso
      SgSorteio.Cells[01, c] := Copy(Memo.Lines[j+02], 5, 10);          // Data sorteio
      SgSorteio.Cells[02, c] := Geral.SoNumero_TT(Memo.Lines[j+04]); // 1� Dezena
      SgSorteio.Cells[03, c] := Geral.SoNumero_TT(Memo.Lines[j+06]); // 2� Dezena
      SgSorteio.Cells[04, c] := Geral.SoNumero_TT(Memo.Lines[j+08]); // 3� Dezena
      SgSorteio.Cells[05, c] := Geral.SoNumero_TT(Memo.Lines[j+10]); // 4� Dezena
      SgSorteio.Cells[06, c] := Geral.SoNumero_TT(Memo.Lines[j+12]); // 5� Dezena
      SgSorteio.Cells[07, c] := Geral.SoNumero_TT(Memo.Lines[j+14]); // 6� Dezena
      SgSorteio.Cells[08, c] := Geral.SoNumero_TT(Memo.Lines[j+16]); // Arrecada��o Total
      SgSorteio.Cells[09, c] := Geral.SoNumero_TT(Memo.Lines[j+18]); // Ganhadores Sena
      SgSorteio.Cells[10, c] := Geral.SoNumero_TT(Memo.Lines[j+20]); // Rateio Sena
      SgSorteio.Cells[11, c] := Geral.SoNumero_TT(Memo.Lines[j+22]); // Ganhadores Quina
      SgSorteio.Cells[12, c] := Geral.SoNumero_TT(Memo.Lines[j+24]); // Rateio Quina
      SgSorteio.Cells[13, c] := Geral.SoNumero_TT(Memo.Lines[j+26]); // Ganhadores Quadra
      SgSorteio.Cells[14, c] := Geral.SoNumero_TT(Memo.Lines[j+28]); // Rateio Quadra
      SgSorteio.Cells[15, c] := Geral.SoNumero_TT(Memo.Lines[j+30]); // Acumulado?
      SgSorteio.Cells[16, c] := Geral.SoNumero_TT(Memo.Lines[j+32]); // Valor acumulado
      SgSorteio.Cells[17, c] := Geral.SoNumero_TT(Memo.Lines[j+34]); // Estimativa pr�mio
      SgSorteio.Cells[18, c] := Geral.SoNumero_TT(Memo.Lines[j+36]); // Acumulado Natal
    end;
    j := j + 42;
  end;
  c := c + 1;
  SgSorteio.RowCount := c + 1;
  SgSorteio.Cells[00, c] := IntToStr(m + 1);
  SgSorteio.Cells[01, c] := Copy(Memo.Lines[j+02], 5, 10);
  SgSorteio.Cells[02, c] := '';
  SgSorteio.Cells[03, c] := '';
  SgSorteio.Cells[04, c] := '';
  SgSorteio.Cells[05, c] := '';
  SgSorteio.Cells[06, c] := '';
  SgSorteio.Cells[07, c] := '';
end;

procedure TFmMegasena.BtListaClick(Sender: TObject);
var
  i, j, c: Integer;
  //s, x: String;
begin
  for i := 1 to 60 do
  begin
    SgVezes.Cells[1,i] := '';
    SgFreq.Cells[1,i] := '';
  end;
  for i := 1 to SgSorteio.RowCount - 1 do
  begin
    for j := 2 to 7 do
    begin
      c := Geral.IMV(SgSorteio.Cells[j, i]);
      if c > 0 then
        SgVezes.Cells[01, c] := IntToStr(Geral.IMV(SgVezes.Cells[01, c]) + 1);
    end;
  end;
  SgFreq.RowCount := SgVezes.RowCount;
  LbFreq.Items.Clear;
  LbFreq.Sorted := True;
  for i := 1 to SgVezes.RowCount - 1 do
    LbFreq.Items.Add(FormatFloat('00000000', Geral.IMV(SgVezes.Cells[01,i]))+
    FormatFloat('00', i));
  LbFreq.Sorted := True;
  for i := 0 to LbFreq.Items.Count - 1 do
  begin
    SgFreq.Cells[00, i+1] := IntToStr(Geral.IMV(Copy(LbFreq.Items[i], 9, 2)));
    SgFreq.Cells[01, i+1] := IntToStr(Geral.IMV(Copy(LbFreq.Items[i], 1, 8)));
  end;
end;

procedure TFmMegasena.BitBtn1Click(Sender: TObject);
var
  i: Integer;
  s: String;
begin
  for i := 1 to SgSorteio.RowCount do
  begin
    s := SgSorteio.Cells[0,i];
    MeJogos.Lines.Add(s+'d='+SgSorteio.Cells[1,i]);
    MeJogos.Lines.Add(s+'1='+SgSorteio.Cells[2,i]);
    MeJogos.Lines.Add(s+'2='+SgSorteio.Cells[3,i]);
    MeJogos.Lines.Add(s+'3='+SgSorteio.Cells[4,i]);
    MeJogos.Lines.Add(s+'4='+SgSorteio.Cells[5,i]);
    MeJogos.Lines.Add(s+'5='+SgSorteio.Cells[6,i]);
    MeJogos.Lines.Add(s+'6='+SgSorteio.Cells[7,i]);
  end;
  MLAGeral.ExportaMemoToFile(MeJogos, FArquivo, True, True, False);
end;

procedure TFmMegasena.BitBtn2Click(Sender: TObject);
var
  F: TextFile;
  S, Sor, Col, Txt: String;
  //WinPath : Array[0..144] of Char;
  i, j, n, k: Integer;
begin
  //
  for i := 1 to SgSorteio.ColCount -1 do
    for j := 1 to SgSorteio.RowCount - 1 do
      SgSorteio.Cells[i, j] := '';
  SgSorteio.RowCount := 2;
 SgSorteio.Cells[0, 1] := '1';
  Screen.Cursor := crHourGlass;
  MeJogos.Lines.Clear;
  //Texto := '';
  try
    if FileExists(FArquivo) then
    begin
      AssignFile(F, FArquivo);
      Reset(F);
      while not Eof(F) do
      begin
        Readln(F, S);
        n := pos('=', S);
        if n > 0 then
        begin
          Sor := Copy(S, 1, n-2);
          Col := Copy(S, n-1, 1);
          Txt := Copy(S, n+1, Length(S));
          k := Geral.IMV(Sor);
          if k > 0 then
          begin
            if SgSorteio.RowCount < k + 1 then
            begin
              SgSorteio.RowCount := k + 1;
              SgSorteio.Cells[0, k] := IntToStr(k);
            end;
            if Col = 'd' then SgSorteio.Cells[1, k] := Txt else
            if Col = '1' then SgSorteio.Cells[2, k] := Txt else
            if Col = '2' then SgSorteio.Cells[3, k] := Txt else
            if Col = '3' then SgSorteio.Cells[4, k] := Txt else
            if Col = '4' then SgSorteio.Cells[5, k] := Txt else
            if Col = '5' then SgSorteio.Cells[6, k] := Txt else
            if Col = '6' then SgSorteio.Cells[7, k] := Txt ;//else
          end;
        end;
      end;
      CloseFile(F);
     Screen.Cursor := crDefault;
    end;
  except
    Screen.Cursor := crDefault;
    raise;
  end;
end;

procedure TFmMegasena.BtJogoClick(Sender: TObject);
var
  Nums: array[01..06] of integer;
  k, i, j, n: Integer;
  Dif: Boolean;
  Txt: String;
begin
  MeJogos.Text := '';
  for K := 1 to EdQtdeJogos.ValueVariant do
  begin
    Randomize;
    Txt := '';
    n := 0;
    for i := 1 to 6 do Nums[i] := 0;
    for i := 1 to 6 do
    begin
      Dif := False;
      while Dif = False do
      begin
        Dif := True;
        n := Random(60);
        for j := 1 to 6 do
        begin
          if (n = Nums[j]) and (Dif = True) then
            Dif := False;
        end;
      end;
      Nums[i] := n;
      Txt := Txt + FormatFloat('00', Geral.IMV(SgFreq.Cells[0, n]));
      if i < 6 then Txt := Txt + ' - ';
    end;
    MeJogos.Lines.Add('Jogo ' + FormatFloat('000', K) + ':     ' + Txt);
  end;
end;

end.
