unit UnApp_CreateMemTable;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db, DbCtrls, UnApp_Consts,
  UnInternalConsts2, ComCtrls, Registry, ABSMain,(* DbTables,*) undmkProcFunc,
  dmkGeral;

const
  TMax_SWMS = 2;

type
  TNomeTabCriaMT = ( ntcAInformar, (*ntcDatabases,*)
    ntcConfigs,
    ntcDBsDisponivApp,

    //..
    ntcFimLista);
  TLista_SWMS = set of ntcAInformar .. ntcFimLista;
  //
  TApp_CreateMT = class(TObject)
  private
    procedure Cria_ntcCliPrcCab(Qry: TABSQuery);
    procedure Cria_ntcCliPrcIts(Qry: TABSQuery);
    procedure Cria_ntcEntCliCad(Qry: TABSQuery);
    procedure Cria_ntcPrdLstCab(Qry: TABSQuery);
    procedure Cria_ntcPrdLstIts(Qry: TABSQuery);
    procedure Cria_ntcPrdPrcCab(Qry: TABSQuery);
    procedure Cria_ntcPrdPrcIts(Qry: TABSQuery);
    procedure Cria_ntcRecRibCab(Qry: TABSQuery);
    procedure Cria_ntcRecRibIts(Qry: TABSQuery);
    { Private declarations }

  public
    { Public declarations }
    procedure ConfiguracoesIniciais(UsaCoresRel: integer; AppIDtxt: String);
    function RecriaMemTable(Tabela: TNomeTabCria_TFC; UsaNomeTab: String;
             Qry: TABSQuery; Executa: Boolean; UniqueTableName: Boolean;
             Repeticoes: Integer = 1; NomeTab: String = ''): String;
    (*procedure Cria_ntcDatabases(Qry: TABSQuery);*)
    //
    procedure Cria_ntcConfigs(Qry: TABSQuery);
    procedure Cria_ntcDBsDisponivApp(Qry: TABSQuery);
    //...
    //
  end;

var
  App_CreateMT: TApp_CreateMT;

  TIndex_TFC: array[0..Integer(ntcFimLista)] of TNomeTabCria_TFC;

implementation

uses UnMyObjects(*, Module*);

procedure TApp_CreateMT.ConfiguracoesIniciais(UsaCoresRel: integer;
  AppIDtxt: String);
begin
  dmkPF.ConfigIniApp(UsaCoresRel);
{
  if Uppercase(AppIDtxt) = 'TFC' then
  begin
    VAR_CLIENTE1 := 'Cliente';
    VAR_CLIENTE2 := '';
    VAR_CLIENTE3 := '';
    VAR_CLIENTE4 := '';
    VAR_FORNECE1 := 'Fornecedor';
    VAR_FORNECE2 := 'Funcion�rio';
    VAR_FORNECE3 := '';
    VAR_FORNECE4 := '';
    VAR_FORNECE5 := '';
    VAR_FORNECE6 := '';
    VAR_FORNECE7 := '';
    VAR_FORNECE8 := '';
    VAR_QUANTI1NOME := '?????????';
    VAR_CAMPOTRANSPORTADORA := '?????????';
  end else
    Geral.MensagemBox(
    'Database para configura��es de "CheckBox" n�o definidos!', 'Aviso!',
    MB_OK+MB_ICONWARNING)
}
end;

procedure TApp_CreateMT.Cria_ntcConfigs(Qry: TABSQuery);
begin
(*
  Qry.SQL.Add('  Versao              float               ,');
  Qry.SQL.Add('  AccNome             varchar(255)        ,');
  Qry.SQL.Add('  AccID               varchar(60)         ,');
  Qry.SQL.Add('  UserID              varchar(60)         ,');
  Qry.SQL.Add('  DefLstPrc           varchar(255)        ,');
  //
  Qry.SQL.Add('  Ativo               smallint             ');
  // AccID nao pode ser null
  //Qry.SQL.Add('  PRIMARY KEY (AccID)                      ');
  Qry.SQL.Add(');                                         ');
*)
end;

procedure TApp_CreateMT.Cria_ntcDBsDisponivApp(Qry: TABSQuery);
begin
  Qry.SQL.Add('  Codigo              int(11)             ,');
  Qry.SQL.Add('  Nome                varchar(100)        ,');
  Qry.SQL.Add('  NomeUTF             varchar(100)        ,');
  //
  Qry.SQL.Add('  Ativo                smallint           ,');
  Qry.SQL.Add('  PRIMARY KEY (CliCodi)                    ');
  Qry.SQL.Add(');                                         ');
end;

function TApp_CreateMT.RecriaMemTable(Tabela: TNomeTabCria_TFC; UsaNomeTab: String;
Qry: TABSQuery; Executa: Boolean; UniqueTableName: Boolean;
Repeticoes: Integer = 1; NomeTab: String = ''): String;
  function ObtemTabela(const TabIdx: Integer; var NomeIdx: String): Boolean;
  begin
    Result := True;
    case TabIdx of
      Integer(ntcAInformar):         NomeIdx := Lowercase(UsaNomeTab);
      (*Integer(ntcDatabases):         NomeIdx := Lowercase('databases');*)
      //Integer(ntcConfigs):         NomeIdx := Lowercase(CO_Configs_Arquivo);
      Integer(ntcDBsDisponivApp):  NomeIdx := Lowercase('DBsDisponivApp');
      //
      // ...
      else
      begin
        Result := False;
        NomeIdx := '';
      end;
    end;
  end;
var
  Nome, TabNo: String;
  P, I: Integer;
  Continua: Boolean;
  TabACriar: TNomeTabCria_TFC;
begin
  TabNo := '';
  if NomeTab = '' then
  begin
    ObtemTabela(Integer(Tabela), Nome);
  end else
    Nome := Lowercase(NomeTab);
  //
  if Nome = '' then
  begin
    Geral.MensagemBox(
    'Tabela em mem�ria sem nome definido! (RecriaMemTable)',
    'Erro', MB_OK+MB_ICONERROR);
    Result := '';
    Exit;
  end;
  if UniqueTableName then
  begin
    TabNo := '_' + FormatFloat('0', VAR_USUARIO) + '_' + Nome;
    //  caso for Master ou Admin (n�meros negativos)
    P := pos('-', TabNo);
    if P > 0 then
      TabNo[P] := '_';
  end else TabNo := Nome;
  Qry.Close;
  Qry.SQL.Clear;
  Qry.SQL.Add('DROP TABLE IF EXISTS ' + TabNo);
  Qry.ExecSQL;
  //
  Qry.Close;
  Qry.SQL.Clear;
  Qry.SQL.Add('CREATE TABLE ' + TabNo +' (');
  //
  TabACriar := Tabela;
  if TabACriar = ntcAInformar then
  begin
    // I n�o pode ser 0 por que o ntcAInformar � a pr�pria tabela
    for I := 1 to Integer(ntcFimLista) do
    begin
      if ObtemTabela(I, Nome) then
      begin
        if Nome = UsaNomeTab then
        begin
          TabACriar := TNomeTabCria_TFC(I);
          Break;
        end;  
      end;
    end;
  end;
  Continua := True;
  case TabACriar of
    //
    (*ntcDatabases                 : Cria_ntcDatabases(Qry);*)
    //
    ntcConfigs                 : Cria_ntcConfigs(Qry);
    ntcDBsDisponivApp          : Cria_ntcDBsDisponivApp(Qry);
    //
    //
    else
    begin
      Continua := False;
      Geral.MensagemBox('N�o foi poss�vel criar a tabela em mem�ria "' +
        Nome + '" por falta de implementa��o!', 'Erro', MB_OK+MB_ICONERROR);
    end;
  end;
  if Continua and Executa then
    Qry.ExecSQL;
  Result := TabNo;
end;

end.
