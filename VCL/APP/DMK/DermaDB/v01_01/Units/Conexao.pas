unit Conexao;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Menus, Grids, dmkEdit, dmkGeral, Data.DB,
  mySQLDbTables, UnDmkProcFunc, UnInternalConsts;

type
  TFmConexao = class(TForm)
    Panel2: TPanel;
    PMAcao: TPopupMenu;
    Carregarconexo1: TMenuItem;
    Salvarconexo1: TMenuItem;
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    Panel5: TPanel;
    BtConfirmar: TBitBtn;
    Panel7: TPanel;
    EdSenha: TdmkEdit;
    Label1: TLabel;
    EdLogin: TdmkEdit;
    Label3: TLabel;
    EdPorta: TdmkEdit;
    Label2: TLabel;
    EdDB: TdmkEdit;
    lbDb: TLabel;
    EdHost: TdmkEdit;
    lbHost: TLabel;
    EdDescricao: TdmkEdit;
    Label5: TLabel;
    EdItem: TdmkEdit;
    Label4: TLabel;
    Panel6: TPanel;
    BitBtn3: TBitBtn;
    Panel1: TPanel;
    Panel3: TPanel;
    BtDesiste: TBitBtn;
    BtConfirma: TBitBtn;
    Panel8: TPanel;
    BitBtn2: TBitBtn;
    Grade: TStringGrid;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel4: TPanel;
    BtImporta: TBitBtn;
    RGVersaoMySQL_: TRadioGroup;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure EdSenhaExit(Sender: TObject);
    procedure EdSenhaChange(Sender: TObject);
    procedure EdSenhaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure GradeDblClick(Sender: TObject);
    procedure EdItemChange(Sender: TObject);
    procedure BtConfirmarClick(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure GradeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure GradeDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure EdSenhaDblClick(Sender: TObject);
    procedure BtImportaClick(Sender: TObject);
  private
    { Private declarations }
    //FSecureStr: String;
    FAtivou: Boolean;
    procedure EditsToGrade(Item: Integer);
    procedure GradeToEdits(Item: Integer);
    procedure ExcluiItem(Item: Integer);
    procedure SalvaStringGridToFile_PWD(Grade: TStringGrid; Arq: String);
    procedure LeArquivoToStringGrid_PWD(Grade: TStringGrid; Arq: String);
    //procedure EncriptaSQL(Texto, Chave: String);
  public
    { Public declarations }
    FArqLLM: String;
  end;

var
  FmConexao: TFmConexao;

implementation

uses UnMyObjects, Principal, UnMLAGeral;

{$R *.DFM}

procedure TFmConexao.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmConexao.BtImportaClick(Sender: TObject);
begin
  MyObjects.ImportaConfigWeb(
    Self, EdDescricao, EdHost, EdDB, EdPorta, EdLogin, EdSenha(*, nil*));
end;
{
procedure TFmConexao.EncriptaSQL(Texto, Chave: String);
var
  i, x: Integer;
begin
  for i := 1 to Length(EdSenha.Text)  do
  begin
    x := ord(EdSenha.Text[i]);
    case x of
      48..57: Memo1.lines.Add(FormatFloat('00', x-47));
      65..90: Memo1.lines.Add(FormatFloat('00', x-54));
      97..122: Memo1.lines.Add(FormatFloat('00', x-60));
    end;
  end;
end;
}

procedure TFmConexao.BtConfirmaClick(Sender: TObject);
var
  Qry: TmySQLQuery;
  Versao, UsaPwd: String;
begin
(*
  case RGVersaoMySQL.ItemIndex of
    0: Geral.MB_Aviso('Informe a vers�o MySQL!');
    1, 2: // Nada!
    else Geral.MB_Aviso('Vers�o MySQL n�o implemetada!');
  end;
  if (RGVersaoMySQL.ItemIndex < 1) or (RGVersaoMySQL.ItemIndex > 2) then
    Exit;
*)
  Screen.Cursor := crHourGlass;
  Versao := '';
  FmPrincipal.RGVersaoMySQL_.ItemIndex := RGVersaoMySQL_.ItemIndex;
  with FmPrincipal do
  begin
    //
    MyDB.Connected    := False;
    MyDB.Host         := EdHost.Text;
    MyDB.DatabaseName := EdDB.Text;
    MyDB.Port         := StrToInt(EdPorta.Text);
    MyDB.UserName     := EdLogin.Text;
    MyDB.UserPassword := EdSenha.Text;
    //
    try
      MyDB.Connected := True;
    except
      on E: Exception do
        Geral.MB_Erro('N�o foi poss�vel conectar:' + sLineBreak +
        'Host        : ' +  MyDB.Host            + sLineBreak +
        'DatabaseName: ' +  MyDB.DatabaseName    + sLineBreak +
        'Port        : ' +  Geral.FF0(MyDB.Port) + sLineBreak +
        'UserName    : ' +  MyDB.UserName        + sLineBreak +
        //'UserPassword: ' +  MyDB.UserPassword    + sLineBreak +
        '' + sLineBreak +
        E.Message);
    end;
    if MyDB.Connected then
    begin
      FmPrincipal.CriaTabelaCampos();
      FmPrincipal.ReopenDatabases(EdDb.Text, EdDb.Text);
      FmPrincipal.PainelTitulo.Font.Color := clBlue;
      Qry := TmySQLQuery.Create(Self);
      try
        Qry.Database := MyDB;
        Qry.SQL.Text := 'SELECT VERSION()';
        Qry.Open;
        Versao := 'Vers�o MySQL: ' + Qry.Fields[0].AsString;
      finally
        FmPrincipal.PainelTitulo.Caption :=
        ' Conectado: ' + EdDescricao.Text + ' [' + EdHost.Text + '] ' + Versao;
        MyObjects.Informa2(FmPrincipal.LaAviso1, FmPrincipal.LaAviso2, False,
          FmPrincipal.PainelTitulo.Caption);
        Qry.Free;
      end;
    end else begin
      FmPrincipal.PainelTitulo.Caption := 'N�o conectado';
      FmPrincipal.PainelTitulo.Font.Color := clRed;
    end;
  end;
  Screen.Cursor := crDefault;
  Close;
end;

procedure TFmConexao.FormActivate(Sender: TObject);
begin
  EdSenha.SetFocus;
  if not FAtivou then
  begin
    FAtivou := True;
    if Geral.IMV(Grade.Cells[00,01]) = 1 then
      GradeToEdits(1)
    else begin
      Grade.Cells[00,01] := '01';
      Grade.Cells[01,01] := 'Conex�o local';
      Grade.Cells[02,01] := 'localhost';
      Grade.Cells[03,01] := '';
      Grade.Cells[04,01] := '3306';
      Grade.Cells[05,01] := 'root';
      Grade.Cells[06,01] := '';
      Grade.Cells[07,01] := '';
      //
    end;
  end;
end;

procedure TFmConexao.EdSenhaExit(Sender: TObject);
begin
  //
end;

procedure TFmConexao.EdSenhaChange(Sender: TObject);
begin
  VAR_BDSENHA := EdSenha.Text;
end;

procedure TFmConexao.EdSenhaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then BtDesisteClick(Self) else
  if key=VK_RETURN then BtConfirmaClick(Self);
end;

procedure TFmConexao.FormCreate(Sender: TObject);
var
  Pasta: String;
begin
  FAtivou := False;
  Pasta := ExtractFileDir(Application.ExeName) + '\MySQL';
  ForceDirectories(Pasta);
  FArqLLM := Pasta + '\Lista.llm';
  //
  Grade.ColWidths[00] := 032; // Item
  Grade.ColWidths[01] := 200; // Descri��o
  Grade.ColWidths[02] := 200; // Servidor (IP / URL)
  Grade.ColWidths[03] := 100; // Base de dados
  Grade.ColWidths[04] := 036; // Porta (3306)
  Grade.ColWidths[05] := 100; // Login
  Grade.ColWidths[06] := 300; // Senha  (mask)
  Grade.ColWidths[07] := 000; // Senha (real)
  //
  Grade.Cells[00,00] := 'Item';
  Grade.Cells[01,00] := 'Descri��o';
  Grade.Cells[02,00] := 'Servidor (IP / URL)';
  Grade.Cells[03,00] := 'Base de dados';
  Grade.Cells[04,00] := 'Porta';
  Grade.Cells[05,00] := 'Login';
  Grade.Cells[06,00] := 'Senha';
  Grade.Cells[07,00] := '';
  //
  MyObjects.LimpaGrade(Grade, 1, 1, True);
  if FileExists(FArqLLM) then
    LeArquivoToStringGrid_PWD(Grade, FArqLLM);
end;

procedure TFmConexao.EditsToGrade(Item: Integer);
begin
  Grade.Cells[00,Item] := IntToStr(Item);
  Grade.Cells[01,Item] := EdDescricao.Text;
  Grade.Cells[02,Item] := EdHost.Text;
  Grade.Cells[03,Item] := EdDB.Text;
  Grade.Cells[04,Item] := EdPorta.Text;
  Grade.Cells[05,Item] := EdLogin.Text;
  Grade.Cells[06,Item] := EdSenha.Text;
  Grade.Cells[07,Item] := EdSenha.Text;
end;

procedure TFmConexao.GradeToEdits(Item: Integer);
begin
  EdItem.Text        := IntToStr(Item);
  EdDescricao.Text   := Grade.Cells[01,Item];
  EdHost.Text        := Grade.Cells[02,Item];
  EdDB.Text          := Grade.Cells[03,Item];
  EdPorta.Text       := Grade.Cells[04,Item];
  EdLogin.Text       := Grade.Cells[05,Item];
  EdSenha.Text       := Grade.Cells[07,Item];
  //
  EdDescricao.SetFocus;
end;

procedure TFmConexao.GradeDblClick(Sender: TObject);
begin
  if Grade.Row > 0 then GradeToEdits(Grade.Row);
end;

procedure TFmConexao.EdItemChange(Sender: TObject);
begin
  BtConfirmar.Enabled := Geral.IMV(EdItem.Text) > 0;
end;

procedure TFmConexao.BtConfirmarClick(Sender: TObject);
var
  i: Integer;
begin
  i := Geral.IMV(EdItem.Text);
  if i > 0 then
  begin
    EditsToGrade(i);
    SalvaStringGridToFile_PWD(Grade, FArqLLM);
  end;
end;

procedure TFmConexao.BitBtn2Click(Sender: TObject);
var
  i: Integer;
begin
  if Grade.Cells[00,01] = '' then
    EditsToGrade(1)
  else
  begin
    i := Grade.RowCount;
    Grade.RowCount := Grade.RowCount + 1;
    EditsToGrade(i);
    SalvaStringGridToFile_PWD(Grade, FArqLLM);
  end;
end;

procedure TFmConexao.BitBtn3Click(Sender: TObject);
begin
  ExcluiItem(Geral.IMV(EdItem.Text));
end;

procedure TFmConexao.GradeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_DELETE) and (shift = [ssCtrl]) then
    ExcluiItem(Grade.Row);
end;

procedure TFmConexao.ExcluiItem(Item: Integer);
begin
  if (Item < 1) or (Item > Grade.RowCount -1) then
  begin
    Application.MessageBox(PChar('O item ' + IntToStr(Item) + ' n�o existe!'),
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if Application.MessageBox(PChar('Confirma a exclus�o do item ' + IntToStr(
  Item) + '?'), 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    if MyObjects.ExcluiLinhaStringGrid(Grade) then
      SalvaStringGridToFile_PWD(Grade, FArqLLM);
  end;
end;

procedure TFmConexao.GradeDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
  function Mascara(Texto: String): String;
  var
    i: Integer;
  begin
    Result := '';
    for i := 1 to Length(Texto) do
    begin
      Result := Result + 'l';
    end;
  end;

begin
  if (ACol = 0) or (ARow = 0) then
  begin
  end else
    with Grade.Canvas do
    begin
    {
  Grade.Cells[00,00] := 'Item';
  Grade.Cells[01,00] := 'Descri��o';
  Grade.Cells[02,00] := 'Servidor (IP / URL)';
  Grade.Cells[03,00] := 'Base de dados';
  Grade.Cells[04,00] := 'Porta (3306)';
  Grade.Cells[05,00] := 'Login';
  Grade.Cells[06,00] := 'Senha';
    }
      if ACol in ([4]) then
        MyObjects.DesenhaTextoEmStringGrid(Grade, Rect, clBlack,
          clWhite, taRightJustify, Grade.Cells[Acol, ARow], 1, 1, False)
      else
      if ACol in ([0]) then
        MyObjects.DesenhaTextoEmStringGrid(Grade, Rect, clBlack,
          clWhite, taCenter, Grade.Cells[Acol, ARow], 1, 1, False)
      else
      if ACol in ([6]) then
        MyObjects.DesenhaTextoEmStringGridEx(Grade, Rect, clBlack,
          clWhite, taLeftJustify, Mascara(Grade.Cells[07, ARow]), 1, 1, False,
          'Wingdings', 8, [], 'l')
      else
        MyObjects.DesenhaTextoEmStringGrid(Grade, Rect, clBlack,
          clWhite, taLeftJustify, Grade.Cells[Acol, ARow], 1, 1, False);
    end;

end;

procedure TFmConexao.EdSenhaDblClick(Sender: TObject);
var
  Senha: String;
begin
  if InputQuery('Informe a senha', 'Senha:', Senha) then
  begin
    if Senha = 'wkljweryhvbirt' then
      ShowMessage(EdSenha.Text)
    else
      Application.MessageBox('Senha inv�lida!', 'Aviso', MB_OK+MB_ICONWARNING);
  end;
end;

procedure TFmConexao.SalvaStringGridToFile_PWD(Grade: TStringGrid; Arq: String);
var
  F: TextFile;
  x, y, i: Integer;
  Senha, Nums: String;
begin
  AssignFile(F, Arq);
  Rewrite(F);
  Writeln(F, Grade.ColCount);
  Writeln(F, Grade.RowCount);
  for x:=0 to Grade.ColCount-1 do
  begin
    for y:=0 to Grade.RowCount-1 do
    begin
      if x >= Grade.ColCount-2 then
      begin
        Senha := dmkPF.Criptografia(Grade.Cells[x,y], CO_LLM_SecureStr);
        Nums := '';
        for i := 1 to Length(Senha) do
          Nums := Nums + FormatFloat('000', ord(Senha[i]));
        Writeln(F, Nums);
      end else
        Writeln(F, Grade.Cells[x,y]);
    end;
  end;
  CloseFile(F);
end;

procedure TFmConexao.LeArquivoToStringGrid_PWD(Grade: TStringGrid; Arq: String);
var
  F: TextFile;
  Tmp, x, y: Integer;
  TmpStr: string;
  Senha: String;
  num, i: Integer;
begin
  AssignFile(F, Arq);
  Reset(F);
  Readln(F, Tmp);
  Grade.ColCount := Tmp;
  Readln(F, Tmp);
  Grade.RowCount:=Tmp;
  for x:=0 to Grade.ColCount-1 do
  begin
    for y:=0 to Grade.RowCount-1 do
    begin
      Readln(F, TmpStr);
      if x >= Grade.ColCount-2 then
      begin
        Senha := '';
        if Length(TmpStr) > 0then
        begin
          for i := 1 to Trunc(Length(TmpStr) / 3) do
          begin
            try
              num := Geral.IMV(Copy(TmpStr, 1 + ((i-1) * 3), 3));
              Senha := Senha + (Char(num));
            except

            end;
          end;
          TmpStr := dmkPF.Criptografia(Senha, CO_LLM_SecureStr);
          Grade.Cells[x,y] := TmpStr;
        end;
      end else
        Grade.Cells[x,y] := TmpStr;
    end;
  end;
  CloseFile(F);
end;

end.
