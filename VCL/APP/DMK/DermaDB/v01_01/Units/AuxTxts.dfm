object FmAuxTxts: TFmAuxTxts
  Left = 339
  Top = 185
  Caption = '???-?????-999 :: Manipula'#231#227'o de Textos'
  ClientHeight = 770
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 630
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 582
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 534
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 277
        Height = 32
        Caption = 'Manipula'#231#227'o de Textos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 277
        Height = 32
        Caption = 'Manipula'#231#227'o de Textos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 277
        Height = 32
        Caption = 'Manipula'#231#227'o de Textos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 656
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    ExplicitTop = 289
    ExplicitWidth = 630
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 626
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 700
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 2
    ExplicitTop = 333
    ExplicitWidth = 630
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 484
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 482
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        TabOrder = 0
        OnClick = BtOKClick
        NumGlyphs = 2
      end
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 1008
    Height = 608
    Align = alClient
    TabOrder = 3
    ExplicitTop = 4
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 591
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitHeight = 378
      object Panel3: TPanel
        Left = 716
        Top = 0
        Width = 288
        Height = 591
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitHeight = 378
        object GroupBox2: TGroupBox
          Left = 0
          Top = 0
          Width = 288
          Height = 125
          Align = alTop
          TabOrder = 0
          ExplicitWidth = 400
          object Panel5: TPanel
            Left = 2
            Top = 15
            Width = 284
            Height = 108
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            ExplicitWidth = 280
            ExplicitHeight = 134
            object Label1: TLabel
              Left = 4
              Top = 4
              Width = 112
              Height = 13
              Caption = 'Texto antes do campo: '
            end
            object Label2: TLabel
              Left = 4
              Top = 44
              Width = 117
              Height = 13
              Caption = 'Texto depois do campo: '
            end
            object EdCutPre: TEdit
              Left = 4
              Top = 20
              Width = 273
              Height = 21
              TabOrder = 0
              Text = 'object QrLotesIts'
            end
            object EdCutPos: TEdit
              Left = 3
              Top = 60
              Width = 273
              Height = 21
              TabOrder = 1
              Text = ': T'
            end
            object CkDuplic: TCheckBox
              Left = 8
              Top = 88
              Width = 217
              Height = 17
              Caption = 'Permitir duplica'#231#227'o de resultado.'
              TabOrder = 2
            end
          end
        end
        object GroupBox3: TGroupBox
          Left = 0
          Top = 125
          Width = 288
          Height = 466
          Align = alClient
          Caption = ' Resultados: '
          TabOrder = 1
          ExplicitLeft = 92
          ExplicitTop = 196
          ExplicitWidth = 185
          ExplicitHeight = 105
          object MeResultados: TMemo
            Left = 2
            Top = 15
            Width = 284
            Height = 449
            Align = alClient
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Courier New'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            ExplicitLeft = 1
            ExplicitTop = 1
            ExplicitWidth = 448
            ExplicitHeight = 376
          end
        end
      end
      object GroupBox4: TGroupBox
        Left = 0
        Top = 0
        Width = 716
        Height = 591
        Align = alClient
        Caption = ' Texto a ser pesquisado: '
        TabOrder = 1
        ExplicitLeft = 308
        ExplicitTop = 164
        ExplicitWidth = 185
        ExplicitHeight = 105
        object MeTextoBase: TMemo
          Left = 2
          Top = 15
          Width = 712
          Height = 574
          Align = alClient
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = []
          Lines.Strings = (
            'object QrLotesItsCodigo: TIntegerField'
            '  FieldName = '#39'Codigo'#39
            '  Origin = '#39'lotesits.Codigo'#39
            'end'
            'object QrLotesItsControle: TIntegerField'
            '  FieldName = '#39'Controle'#39
            '  Origin = '#39'lotesits.Controle'#39
            'end'
            'object QrLotesItsComp: TIntegerField'
            '  FieldName = '#39'Comp'#39
            '  Origin = '#39'lotesits.Comp'#39
            'end'
            'object QrLotesItsBanco: TIntegerField'
            '  FieldName = '#39'Banco'#39
            '  Origin = '#39'lotesits.Banco'#39
            '  DisplayFormat = '#39'000'#39
            'end'
            'object QrLotesItsAgencia: TIntegerField'
            '  FieldName = '#39'Agencia'#39
            '  Origin = '#39'lotesits.Agencia'#39
            '  DisplayFormat = '#39'0000'#39
            'end'
            'object QrLotesItsConta: TWideStringField'
            '  FieldName = '#39'Conta'#39
            '  Origin = '#39'lotesits.Conta'#39
            '  Size = 8'
            'end'
            'object QrLotesItsCheque: TIntegerField'
            '  FieldName = '#39'Cheque'#39
            '  Origin = '#39'lotesits.Cheque'#39
            '  DisplayFormat = '#39'000000'#39
            'end'
            'object QrLotesItsEmitente: TWideStringField'
            '  FieldName = '#39'Emitente'#39
            '  Origin = '#39'lotesits.Emitente'#39
            '  Size = 50'
            'end'
            'object QrLotesItsValor: TFloatField'
            '  FieldName = '#39'Valor'#39
            '  Origin = '#39'lotesits.Valor'#39
            '  DisplayFormat = '#39'#,###,##0.00'#39
            'end'
            'object QrLotesItsVencto: TDateField'
            '  FieldName = '#39'Vencto'#39
            '  Origin = '#39'lotesits.Vencto'#39
            '  DisplayFormat = '#39'dd/mm/yy'#39
            'end'
            'object QrLotesItsDDeposito: TDateField'
            '  FieldName = '#39'DDeposito'#39
            '  Origin = '#39'lotesits.DDeposito'#39
            '  DisplayFormat = '#39'dd/mm/yy'#39
            'end'
            'object QrLotesItsTxaCompra: TFloatField'
            '  FieldName = '#39'TxaCompra'#39
            '  Origin = '#39'lotesits.TxaCompra'#39
            '  DisplayFormat = '#39'#,###,##0.0000'#39
            'end'
            'object QrLotesItsTxaAdValorem: TFloatField'
            '  FieldName = '#39'TxaAdValorem'#39
            '  Origin = '#39'lotesits.TxaAdValorem'#39
            '  DisplayFormat = '#39'#,###,##0.0000'#39
            'end'
            'object QrLotesItsVlrCompra: TFloatField'
            '  FieldName = '#39'VlrCompra'#39
            '  Origin = '#39'lotesits.VlrCompra'#39
            '  DisplayFormat = '#39'#,###,##0.00'#39
            'end'
            'object QrLotesItsVlrAdValorem: TFloatField'
            '  FieldName = '#39'VlrAdValorem'#39
            '  Origin = '#39'lotesits.VlrAdValorem'#39
            '  DisplayFormat = '#39'#,###,##0.00'#39
            'end'
            'object QrLotesItsDMais: TIntegerField'
            '  FieldName = '#39'DMais'#39
            '  Origin = '#39'lotesits.DMais'#39
            'end'
            'object QrLotesItsDias: TIntegerField'
            '  FieldName = '#39'Dias'#39
            '  Origin = '#39'lotesits.Dias'#39
            'end'
            'object QrLotesItsLk: TIntegerField'
            '  FieldName = '#39'Lk'#39
            '  Origin = '#39'lotesits.Lk'#39
            'end'
            'object QrLotesItsDataCad: TDateField'
            '  FieldName = '#39'DataCad'#39
            '  Origin = '#39'lotesits.DataCad'#39
            'end'
            'object QrLotesItsDataAlt: TDateField'
            '  FieldName = '#39'DataAlt'#39
            '  Origin = '#39'lotesits.DataAlt'#39
            'end'
            'object QrLotesItsUserCad: TIntegerField'
            '  FieldName = '#39'UserCad'#39
            '  Origin = '#39'lotesits.UserCad'#39
            'end'
            'object QrLotesItsUserAlt: TIntegerField'
            '  FieldName = '#39'UserAlt'#39
            '  Origin = '#39'lotesits.UserAlt'#39
            'end'
            'object QrLotesItsCPF: TWideStringField'
            '  FieldName = '#39'CPF'#39
            '  Origin = '#39'lotesits.CPF'#39
            '  Size = 15'
            'end'
            'object QrLotesItsTxaJuros: TFloatField'
            '  FieldName = '#39'TxaJuros'#39
            '  Origin = '#39'lotesits.TxaJuros'#39
            'end'
            'object QrLotesItsDCompra: TDateField'
            '  FieldName = '#39'DCompra'#39
            '  Origin = '#39'lotesits.DCompra'#39
            '  DisplayFormat = '#39'dd/mm/yy'#39
            'end'
            'object QrLotesItsCPF_TXT: TWideStringField'
            '  FieldKind = fkCalculated'
            '  FieldName = '#39'CPF_TXT'#39
            '  Size = 30'
            '  Calculated = True'
            'end'
            'object QrLotesItsDuplicata: TWideStringField'
            '  FieldName = '#39'Duplicata'#39
            '  Origin = '#39'lotesits.Duplicata'#39
            '  Size = 12'
            'end'
            'object QrLotesItsEmissao: TDateField'
            '  FieldName = '#39'Emissao'#39
            '  Origin = '#39'lotesits.Emissao'#39
            '  DisplayFormat = '#39'dd/mm/yy'#39
            'end'
            'object QrLotesItsDevolucao: TIntegerField'
            '  FieldName = '#39'Devolucao'#39
            '  Origin = '#39'lotesits.Devolucao'#39
            'end'
            'object QrLotesItsDesco: TFloatField'
            '  FieldName = '#39'Desco'#39
            '  Origin = '#39'lotesits.Desco'#39
            'end'
            'object QrLotesItsQuitado: TIntegerField'
            '  FieldName = '#39'Quitado'#39
            '  Origin = '#39'lotesits.Quitado'#39
            'end'
            'object QrLotesItsBruto: TFloatField'
            '  FieldName = '#39'Bruto'#39
            '  Origin = '#39'lotesits.Bruto'#39
            'end'
            'object QrLotesItsPraca: TIntegerField'
            '  FieldName = '#39'Praca'#39
            '  Origin = '#39'lotesits.Praca'#39
            '  DisplayFormat = '#39'000'#39
            'end'
            'object QrLotesItsBcoCobra: TIntegerField'
            '  FieldName = '#39'BcoCobra'#39
            '  Origin = '#39'lotesits.BcoCobra'#39
            'end'
            'object QrLotesItsAgeCobra: TIntegerField'
            '  FieldName = '#39'AgeCobra'#39
            '  Origin = '#39'lotesits.AgeCobra'#39
            'end'
            'object QrLotesItsCartDep: TIntegerField'
            '  FieldName = '#39'CartDep'#39
            '  Origin = '#39'lotesits.CartDep'#39
            'end'
            'object QrLotesItsDescAte: TDateField'
            '  FieldName = '#39'DescAte'#39
            '  Origin = '#39'lotesits.DescAte'#39
            'end'
            'object QrLotesItsTipific: TSmallintField'
            '  FieldName = '#39'Tipific'#39
            'end'
            'object QrLotesItsStatusSPC: TSmallintField'
            '  FieldName = '#39'StatusSPC'#39
            'end'
            'object QrLotesItsStatusSPC_TXT: TWideStringField'
            '  FieldKind = fkCalculated'
            '  FieldName = '#39'StatusSPC_TXT'#39
            '  Size = 30'
            '  Calculated = True'
            'end')
          ParentFont = False
          TabOrder = 0
          ExplicitLeft = 108
          ExplicitTop = 92
          ExplicitWidth = 716
          ExplicitHeight = 378
        end
      end
    end
  end
end
