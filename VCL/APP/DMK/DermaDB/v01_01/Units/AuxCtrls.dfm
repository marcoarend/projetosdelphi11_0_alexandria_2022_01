object FmAuxCtrls: TFmAuxCtrls
  Left = 339
  Top = 185
  Caption = '???-?????-999 :: Pesquisas em componentes'
  ClientHeight = 770
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 404
        Height = 32
        Caption = 'Pesquisa de Controles em Forms'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 404
        Height = 32
        Caption = 'Pesquisa de Controles em Forms'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 404
        Height = 32
        Caption = 'Pesquisa de Controles em Forms'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 640
    Width = 1008
    Height = 60
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 43
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 26
        Width = 1004
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 700
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtArquivos: TBitBtn
        Tag = 28
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Arquivos'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtArquivosClick
      end
      object BtPesquisa1: TBitBtn
        Tag = 15
        Left = 139
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Pesquisa'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtPesquisa1Click
      end
      object BtSubstitui: TBitBtn
        Left = 391
        Top = 4
        Width = 120
        Height = 40
        Caption = 'Substitui'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
      end
      object Button1: TButton
        Left = 516
        Top = 12
        Width = 75
        Height = 25
        Caption = 'Button1'
        TabOrder = 3
      end
      object BtCancela: TBitBtn
        Tag = 15
        Left = 266
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Cancela'
        NumGlyphs = 2
        TabOrder = 4
        OnClick = BtCancelaClick
      end
      object BtPesquisa2: TBitBtn
        Tag = 15
        Left = 611
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Pesquisa'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 5
        OnClick = BtPesquisa2Click
      end
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 1008
    Height = 592
    Align = alClient
    TabOrder = 3
    object Panel2: TPanel
      Left = 2
      Top = 241
      Width = 1004
      Height = 276
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Panel3: TPanel
        Left = 716
        Top = 0
        Width = 288
        Height = 276
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object GroupBox2: TGroupBox
          Left = 0
          Top = 0
          Width = 288
          Height = 213
          Align = alTop
          Caption = ' Pesquisa de propriedade: '
          TabOrder = 0
          object Panel5: TPanel
            Left = 2
            Top = 15
            Width = 284
            Height = 196
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object Label1: TLabel
              Left = 8
              Top = 4
              Width = 34
              Height = 13
              Caption = 'Classe:'
            end
            object Label2: TLabel
              Left = 8
              Top = 44
              Width = 60
              Height = 13
              Caption = 'Propriedade:'
            end
            object EdClasse: TEdit
              Left = 8
              Top = 20
              Width = 121
              Height = 21
              TabOrder = 0
              Text = 'TBitBtn'
            end
            object EdPropriedade: TEdit
              Left = 8
              Top = 60
              Width = 121
              Height = 21
              TabOrder = 1
              Text = 'Tag'
            end
            object RGValores: TRadioGroup
              Left = 132
              Top = 0
              Width = 145
              Height = 85
              Caption = 'Valores: '
              ItemIndex = 2
              Items.Strings = (
                'Nulos e zerados'
                'Preenchidos'
                'Tudo')
              TabOrder = 2
            end
          end
        end
        object GroupBox3: TGroupBox
          Left = 0
          Top = 213
          Width = 288
          Height = 63
          Align = alClient
          Caption = ' Resultados: '
          TabOrder = 1
        end
      end
      object GroupBox4: TGroupBox
        Left = 0
        Top = 0
        Width = 716
        Height = 276
        Align = alClient
        Caption = ' Texto a ser pesquisado: '
        TabOrder = 1
        object ListBox3: TListBox
          Left = 593
          Top = 15
          Width = 121
          Height = 259
          Align = alRight
          ItemHeight = 13
          TabOrder = 0
        end
        object GradeB: TStringGrid
          Left = 2
          Top = 15
          Width = 591
          Height = 259
          Align = alClient
          ColCount = 4
          DefaultColWidth = 40
          DefaultRowHeight = 17
          RowCount = 2
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goEditing]
          TabOrder = 1
          OnDblClick = GradeBDblClick
          ColWidths = (
            40
            329
            69
            526)
        end
      end
    end
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 226
      Align = alTop
      TabOrder = 1
      object ListBox1: TListBox
        Left = 774
        Top = 1
        Width = 114
        Height = 224
        Align = alClient
        ItemHeight = 13
        TabOrder = 0
      end
      object Panel7: TPanel
        Left = 1
        Top = 1
        Width = 588
        Height = 224
        Align = alLeft
        TabOrder = 1
        object Panel8: TPanel
          Left = 1
          Top = 1
          Width = 586
          Height = 112
          Align = alTop
          TabOrder = 0
          object Label3: TLabel
            Left = 8
            Top = 8
            Width = 42
            Height = 13
            Caption = 'Diret'#243'rio:'
          end
          object Label17: TLabel
            Left = 8
            Top = 48
            Width = 166
            Height = 13
            Caption = 'Arquivo contendo lista de arquivos:'
          end
          object SpeedButton1: TSpeedButton
            Left = 440
            Top = 24
            Width = 23
            Height = 22
            Caption = '...'
            OnClick = SpeedButton1Click
          end
          object SpeedButton9: TSpeedButton
            Left = 440
            Top = 64
            Width = 23
            Height = 22
            Caption = '...'
            OnClick = SpeedButton9Click
          end
          object EdDiretorio: TdmkEdit
            Left = 8
            Top = 24
            Width = 429
            Height = 21
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = 'C:\Projetos\Delphi 2007\Aplicativos\Ativos\Credito2'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 'C:\Projetos\Delphi 2007\Aplicativos\Ativos\Credito2'
            ValWarn = False
          end
          object EdArqArqs: TdmkEdit
            Left = 8
            Top = 64
            Width = 429
            Height = 21
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = 'C:\Projetos\Delphi 2007\Aplicativos\Ativos\Credito2\Credito2.dpr'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 'C:\Projetos\Delphi 2007\Aplicativos\Ativos\Credito2\Credito2.dpr'
            ValWarn = False
          end
          object Panel21: TPanel
            Left = 467
            Top = 1
            Width = 118
            Height = 110
            Align = alRight
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 2
            object StaticText3: TStaticText
              Left = 0
              Top = 0
              Width = 118
              Height = 17
              Align = alTop
              Alignment = taCenter
              Caption = 'Extens'#245'es de arquivos:'
              TabOrder = 0
              ExplicitWidth = 114
            end
            object MeExtensoes: TMemo
              Left = 0
              Top = 17
              Width = 118
              Height = 93
              Align = alClient
              Lines.Strings = (
                '*.dfm')
              TabOrder = 1
              WantReturns = False
            end
          end
          object CkSub1: TCheckBox
            Left = 8
            Top = 90
            Width = 117
            Height = 17
            Caption = 'Incluir subdiret'#243'rios.'
            Checked = True
            State = cbChecked
            TabOrder = 3
          end
          object CkCaseSensitive: TCheckBox
            Left = 132
            Top = 90
            Width = 97
            Height = 17
            Caption = 'Case sensitive.'
            TabOrder = 4
          end
        end
        object GradeA: TStringGrid
          Left = 1
          Top = 113
          Width = 586
          Height = 110
          Align = alClient
          ColCount = 3
          DefaultColWidth = 40
          DefaultRowHeight = 17
          RowCount = 2
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
          TabOrder = 1
          OnDblClick = GradeADblClick
          ColWidths = (
            40
            61
            453)
        end
        object RE1: TRichEdit
          Left = 403
          Top = 135
          Width = 185
          Height = 89
          TabOrder = 2
          Visible = False
        end
      end
      object ListBox2: TListBox
        Left = 888
        Top = 1
        Width = 115
        Height = 224
        Align = alRight
        ItemHeight = 13
        TabOrder = 2
      end
      object Panel19: TPanel
        Left = 589
        Top = 1
        Width = 185
        Height = 224
        Align = alLeft
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 3
        object StaticText2: TStaticText
          Left = 0
          Top = 0
          Width = 58
          Height = 17
          Align = alTop
          Alignment = taCenter
          Caption = 'Excess'#245'es:'
          TabOrder = 0
        end
        object MeExcl: TMemo
          Left = 0
          Top = 17
          Width = 185
          Height = 207
          Align = alLeft
          TabOrder = 1
          WantReturns = False
        end
      end
    end
    object GroupBox5: TGroupBox
      Left = 2
      Top = 517
      Width = 1004
      Height = 73
      Align = alClient
      Caption = ' Resultados: '
      TabOrder = 2
      object GradeC: TStringGrid
        Left = 2
        Top = 15
        Width = 1000
        Height = 56
        Align = alClient
        ColCount = 4
        DefaultColWidth = 40
        DefaultRowHeight = 17
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goEditing]
        TabOrder = 0
        OnDblClick = GradeBDblClick
        ColWidths = (
          40
          329
          231
          371)
      end
    end
  end
  object PMArquivos: TPopupMenu
    Left = 48
    Top = 696
    object Dodiretrioselecionado1: TMenuItem
      Caption = '&Diret'#243'rio selecionado'
      OnClick = Dodiretrioselecionado1Click
    end
    object Listadearquivos1: TMenuItem
      Caption = '&Lista de arquivos do arquivo selecionado'
      OnClick = Listadearquivos1Click
    end
  end
end
