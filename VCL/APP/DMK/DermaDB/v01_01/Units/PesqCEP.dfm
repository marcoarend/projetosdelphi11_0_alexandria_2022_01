object FmPesqCEP: TFmPesqCEP
  Left = 339
  Top = 185
  Caption = 'Pesquisa CEP'
  ClientHeight = 494
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 446
    Width = 784
    Height = 48
    Align = alBottom
    TabOrder = 1
    object Panel2: TPanel
      Left = 672
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object PB1: TProgressBar
      Left = 28
      Top = 20
      Width = 625
      Height = 17
      TabOrder = 1
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    Caption = 'Pesquisa CEP'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 2
      Top = 2
      Width = 780
      Height = 44
      Align = alClient
      Transparent = True
      ExplicitWidth = 788
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 398
    Align = alClient
    TabOrder = 0
    object PageControl1: TPageControl
      Left = 1
      Top = 1
      Width = 782
      Height = 396
      ActivePage = TabSheet3
      Align = alClient
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = ' Pesquisa '
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 774
          Height = 368
          Align = alClient
          ParentBackground = False
          TabOrder = 0
          object Splitter1: TSplitter
            Left = 1
            Top = 137
            Width = 772
            Height = 5
            Cursor = crVSplit
            Align = alTop
            ExplicitTop = 90
          end
          object Memo1: TMemo
            Left = 1
            Top = 1
            Width = 772
            Height = 136
            Align = alTop
            TabOrder = 0
          end
          object DBGrid1: TDBGrid
            Left = 1
            Top = 159
            Width = 772
            Height = 160
            Align = alClient
            DataSource = DataSource1
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
          end
          object StaticText1: TStaticText
            Left = 1
            Top = 142
            Width = 61
            Height = 17
            Align = alTop
            Caption = '  0 registros.'
            TabOrder = 2
          end
          object Panel4: TPanel
            Left = 1
            Top = 319
            Width = 772
            Height = 48
            Align = alBottom
            TabOrder = 3
            object BitBtn1: TBitBtn
              Tag = 14
              Left = 20
              Top = 4
              Width = 90
              Height = 40
              Caption = '&OK'
              NumGlyphs = 2
              TabOrder = 0
              OnClick = BtOKClick
            end
            object Panel5: TPanel
              Left = 660
              Top = 1
              Width = 111
              Height = 46
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 1
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Compara c'#243'digo de cidades 2004 - 2008'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel6: TPanel
          Left = 0
          Top = 320
          Width = 774
          Height = 48
          Align = alBottom
          ParentBackground = False
          TabOrder = 0
          object BtAcao: TBitBtn
            Tag = 14
            Left = 20
            Top = 4
            Width = 90
            Height = 40
            Caption = '&A'#231#227'o'
            NumGlyphs = 2
            TabOrder = 0
            OnClick = BtAcaoClick
          end
          object Panel7: TPanel
            Left = 662
            Top = 1
            Width = 111
            Height = 46
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 1
          end
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'Cidades (CMB)'
        ImageIndex = 2
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Splitter2: TSplitter
          Left = 0
          Top = 49
          Width = 774
          Height = 5
          Cursor = crVSplit
          Align = alTop
        end
        object Panel8: TPanel
          Left = 0
          Top = 0
          Width = 774
          Height = 49
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Memo2: TMemo
            Left = 129
            Top = 0
            Width = 645
            Height = 49
            Align = alClient
            Lines.Strings = (
              'SELECT * FROM cmb')
            TabOrder = 0
          end
          object Panel9: TPanel
            Left = 0
            Top = 0
            Width = 129
            Height = 49
            Align = alLeft
            TabOrder = 1
            object BitBtn2: TBitBtn
              Left = 4
              Top = 4
              Width = 120
              Height = 40
              Caption = 'SQL'
              TabOrder = 0
              OnClick = BitBtn2Click
            end
          end
        end
        object DBGrid2: TDBGrid
          Left = 0
          Top = 71
          Width = 774
          Height = 297
          Align = alClient
          DataSource = DataSource2
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
        end
        object StaticText2: TStaticText
          Left = 0
          Top = 54
          Width = 774
          Height = 17
          Align = alTop
          Caption = '  0 registros.'
          TabOrder = 2
          ExplicitWidth = 61
        end
      end
    end
  end
  object DCP_twofish1: TDCP_twofish
    Id = 6
    Algorithm = 'Twofish'
    MaxKeySize = 256
    BlockSize = 128
    Left = 92
    Top = 8
  end
  object DNCEP: TDatabase
    AliasName = 'MyCEP'
    DatabaseName = 'DBCEP'
    LoginPrompt = False
    Params.Strings = (
      'User='
      'Password='#171#185#189#191#165#208#178#254#222#162#167#179#188#187)
    SessionName = 'Default'
    TransIsolation = tiDirtyRead
    Left = 8
    Top = 8
  end
  object Query1: TQuery
    DatabaseName = 'DBCEP'
    Left = 36
    Top = 8
  end
  object DataSource1: TDataSource
    DataSet = Query1
    Left = 64
    Top = 8
  end
  object PMAcao: TPopupMenu
    Left = 40
    Top = 364
    object RecriatabelaDBTest1: TMenuItem
      Caption = '&Recria tabela (DB Test)'
      OnClick = RecriatabelaDBTest1Click
    end
    object Carregadadosde20041: TMenuItem
      Caption = 'Carrega dados de 2004'
      OnClick = Carregadadosde20041Click
    end
    object Carregadadosde20081: TMenuItem
      Caption = 'Carrega dados de 2008'
      OnClick = Carregadadosde20081Click
    end
  end
  object MySQL: TmySQLDatabase
    DatabaseName = 'Test'
    UserName = 'root'
    UserPassword = 'wkljweryhvbirt'
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'DatabaseName=Test'
      'UID=root'
      'PWD=wkljweryhvbirt')
    DatasetOptions = []
    Left = 120
    Top = 8
  end
  object QrLocal: TQuery
    DatabaseName = 'DBCEP'
    SQL.Strings = (
      'SELECT * '
      'FROM log_localidade')
    Left = 148
    Top = 8
    object QrLocalLOC_NU_SEQUENCIAL: TIntegerField
      FieldName = 'LOC_NU_SEQUENCIAL'
      Origin = 'DBCEP.LOG_LOCALIDADE.LOC_NU_SEQUENCIAL'
    end
    object QrLocalLOC_NOSUB: TWideStringField
      FieldName = 'LOC_NOSUB'
      Origin = 'DBCEP.LOG_LOCALIDADE.LOC_NOSUB'
      Size = 50
    end
    object QrLocalLOC_NO: TWideStringField
      FieldName = 'LOC_NO'
      Origin = 'DBCEP.LOG_LOCALIDADE.LOC_NO'
      Size = 60
    end
    object QrLocalCEP: TWideStringField
      FieldName = 'CEP'
      Origin = 'DBCEP.LOG_LOCALIDADE.CEP'
      Size = 16
    end
    object QrLocalUFE_SG: TWideStringField
      FieldName = 'UFE_SG'
      Origin = 'DBCEP.LOG_LOCALIDADE.UFE_SG'
      Size = 2
    end
    object QrLocalLOC_IN_SITUACAO: TIntegerField
      FieldName = 'LOC_IN_SITUACAO'
      Origin = 'DBCEP.LOG_LOCALIDADE.LOC_IN_SITUACAO'
    end
    object QrLocalLOC_IN_TIPO_LOCALIDADE: TWideStringField
      FieldName = 'LOC_IN_TIPO_LOCALIDADE'
      Origin = 'DBCEP.LOG_LOCALIDADE.LOC_IN_TIPO_LOCALIDADE'
      Size = 1
    end
    object QrLocalLOC_NU_SEQUENCIAL_SUB: TIntegerField
      FieldName = 'LOC_NU_SEQUENCIAL_SUB'
      Origin = 'DBCEP.LOG_LOCALIDADE.LOC_NU_SEQUENCIAL_SUB'
    end
    object QrLocalLOC_KEY_DNE: TWideStringField
      FieldName = 'LOC_KEY_DNE'
      Origin = 'DBCEP.LOG_LOCALIDADE.LOC_KEY_DNE'
      Size = 16
    end
  end
  object Query2: TQuery
    DatabaseName = 'Teste'
    SQL.Strings = (
      'SELECT * '
      'FROM cmb')
    Left = 200
    Top = 156
    object Query1CODIGO: TFloatField
      FieldName = 'CODIGO'
      Origin = 'TESTE."CMB.DBF".CODIGO'
    end
    object Query1MUNICIPIO: TWideStringField
      FieldName = 'MUNICIPIO'
      Origin = 'TESTE."CMB.DBF".MUNICIPIO'
      Size = 50
    end
    object Query1UF: TWideStringField
      FieldName = 'UF'
      Origin = 'TESTE."CMB.DBF".UF'
      Size = 2
    end
    object Query1COD_JURISD: TFloatField
      FieldName = 'COD_JURISD'
      Origin = 'TESTE."CMB.DBF".COD_JURISD'
    end
  end
  object DataSource2: TDataSource
    DataSet = Query2
    Left = 172
    Top = 156
  end
  object Database1: TDatabase
    AliasName = 'Teste'
    DatabaseName = 'Teste'
    SessionName = 'Default'
    Left = 228
    Top = 156
  end
end
