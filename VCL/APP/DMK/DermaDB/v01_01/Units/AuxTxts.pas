unit AuxTxts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, UnDmkEnums;

type
  TFmAuxTxts = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    GroupBox1: TGroupBox;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox2: TGroupBox;
    Panel5: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    EdCutPre: TEdit;
    EdCutPos: TEdit;
    CkDuplic: TCheckBox;
    GroupBox3: TGroupBox;
    MeResultados: TMemo;
    GroupBox4: TGroupBox;
    MeTextoBase: TMemo;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmAuxTxts: TFmAuxTxts;

implementation

uses UnMyObjects, Module;

{$R *.DFM}

procedure TFmAuxTxts.BtOKClick(Sender: TObject);
var
  I, pPre, pPos: Integer;
  CutPre, CutPos, TxtFind: String;
  LocPre, LocPos: Boolean;
begin
  Screen.Cursor := crHourGlass;
  try
    MeResultados.Lines.Clear;
    //
    CutPre := EdCutPre.Text;
    CutPos := EdCutPos.Text;
    //
    LocPre := CutPre <> '';
    LocPos := CutPos <> '';
    //for I := 0 to MeTextoBase.Lines.Count - 1 do
    for I := 1 to MeTextoBase.Lines.Count do
    begin
      TxtFind := MeTextoBase.Lines[I];
      //
      if LocPre then
        pPre := pos(CutPre, TxtFind) + Length(CutPre)
      else
        pPre := 1;
      //
      if LocPos then
        pPos := pos(CutPos, TxtFind)
      else
        pPos := Length(TxtFind);
      //
      if (pPre > 0) and (pPos > 0) then
      begin
        TxtFind := Copy(TxtFind, pPre, pPos - pPre);
        //
        if CkDuplic.Checked then
          MeResultados.Lines.Add(TxtFind)
        else
        begin
          if MeResultados.Lines[1] <> TxtFind then
          if MeResultados.Lines[MeResultados.Lines.Count] <> TxtFind then
          if pos(#10 + TxtFind + #13, MeResultados.Text) = 0 then
            MeResultados.Lines.Add(TxtFind)
        end;
      end;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmAuxTxts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmAuxTxts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  ImgTipo.SQLType := stPsq;
end;

procedure TFmAuxTxts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
