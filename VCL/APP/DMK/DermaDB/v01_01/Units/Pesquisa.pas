unit Pesquisa;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, ComCtrls, Grids, dmkGeral, dmkEdit,
  ShellAPI, Menus, StrUtils, UnDmkProcFunc, dmkImage, UnDmkEnums,
  ComObj, Variants, System.IOUtils;


type
  TFmPesquisa = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel1: TPanel;
    Panel2: TPanel;
    ListBox1: TListBox;
    Panel4: TPanel;
    Panel3: TPanel;
    Label4: TLabel;
    Panel5: TPanel;
    ListBox4: TListBox;
    ListBox5: TListBox;
    Panel6: TPanel;
    Label7: TLabel;
    SpeedButton2: TSpeedButton;
    EdDiretorio2: TdmkEdit;
    Panel8: TPanel;
    BitBtn1: TBitBtn;
    BtBackup: TBitBtn;
    Label8: TLabel;
    EdDataStr: TdmkEdit;
    Progress: TProgressBar;
    GradeB: TStringGrid;
    GradeA: TStringGrid;
    Panel10: TPanel;
    BitBtn2: TBitBtn;
    CkSub2: TCheckBox;
    MeExt: TMemo;
    Label01: TLabel;
    TabSheet3: TTabSheet;
    Panel11: TPanel;
    Label5: TLabel;
    EdArq1: TdmkEdit;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    EdArq2: TdmkEdit;
    Label6: TLabel;
    OpenDialog1: TOpenDialog;
    Panel12: TPanel;
    Panel13: TPanel;
    Memo2: TMemo;
    Memo1: TMemo;
    EdLinha: TEdit;
    BitBtn3: TBitBtn;
    Label9: TLabel;
    EdLinha1: TEdit;
    Label10: TLabel;
    EdLinha2: TEdit;
    Label12: TLabel;
    BitBtn4: TBitBtn;
    RE1: TRichEdit;
    TabSheet4: TTabSheet;
    Panel14: TPanel;
    Panel15: TPanel;
    Panel16: TPanel;
    BtPesquisa2: TBitBtn;
    Panel17: TPanel;
    BitBtn8: TBitBtn;
    MeArq: TMemo;
    PnAviso: TPanel;
    MeCopy: TMemo;
    MeTxt: TMemo;
    PB2: TProgressBar;
    Panel18: TPanel;
    StaticText1: TStaticText;
    Panel19: TPanel;
    StaticText2: TStaticText;
    MeExcl: TMemo;
    Panel20: TPanel;
    Label1: TLabel;
    EdTxtPre: TdmkEdit;
    CkSub1: TCheckBox;
    CkCaseSensitive: TCheckBox;
    Label3: TLabel;
    EdDiretorio: TdmkEdit;
    SpeedButton1: TSpeedButton;
    Panel21: TPanel;
    StaticText3: TStaticText;
    MeExtensoes: TMemo;
    StaticText4: TStaticText;
    MeAnteriores: TMemo;
    MeMostrar: TMemo;
    MeShow: TMemo;
    TabSheet5: TTabSheet;
    Panel22: TPanel;
    Panel23: TPanel;
    BitBtn5: TBitBtn;
    ListBox04: TListBox;
    Ed4_Cam: TdmkEdit;
    Label14: TLabel;
    Label15: TLabel;
    Ed4_Dir: TdmkEdit;
    Label16: TLabel;
    Ed4_Arq: TdmkEdit;
    SpeedButton6: TSpeedButton;
    SpeedButton7: TSpeedButton;
    SpeedButton8: TSpeedButton;
    Label17: TLabel;
    EdArqArqs: TdmkEdit;
    SpeedButton9: TSpeedButton;
    PMArquivos: TPopupMenu;
    Dodiretrioselecionado1: TMenuItem;
    Listadearquivos1: TMenuItem;
    ListBox2: TListBox;
    ListBox3: TListBox;
    TabSheet6: TTabSheet;
    Panel24: TPanel;
    Label20: TLabel;
    SbArqScan: TSpeedButton;
    EdArqScan: TdmkEdit;
    Panel25: TPanel;
    MeCI_Txt: TMemo;
    Panel26: TPanel;
    Panel27: TPanel;
    MeCI_Nao: TMemo;
    Panel28: TPanel;
    Splitter1: TSplitter;
    Panel29: TPanel;
    Panel30: TPanel;
    MeCI_Sim: TMemo;
    Splitter2: TSplitter;
    Panel31: TPanel;
    Splitter3: TSplitter;
    Panel32: TPanel;
    MeCI_Cor: TMemo;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel33: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PB1: TProgressBar;
    GBRodaPe: TGroupBox;
    Panel7: TPanel;
    BitBtn6: TBitBtn;
    Panel34: TPanel;
    Label22: TLabel;
    EdPAIni: TdmkEdit;
    EdPAFim: TdmkEdit;
    Label23: TLabel;
    EdPACasas: TdmkEdit;
    Label24: TLabel;
    EdPATxtPos: TdmkEdit;
    Label25: TLabel;
    CkPAUsa: TCheckBox;
    GradeC: TStringGrid;
    CkPARemoveBrancos: TCheckBox;
    SGDirs: TStringGrid;
    Panel35: TPanel;
    Label2: TLabel;
    EdArqNome4: TdmkEdit;
    Label13: TLabel;
    EdArqExte4: TdmkEdit;
    EdArqDir4: TdmkEdit;
    Label11: TLabel;
    SpeedButton5: TSpeedButton;
    CkSubDir4: TCheckBox;
    Salvar: TBitBtn;
    Panel36: TPanel;
    BtArquivos: TBitBtn;
    BtCancela: TBitBtn;
    BtPesquisa: TBitBtn;
    BtSubstitui: TBitBtn;
    PCSubstituir: TPageControl;
    TabSheet7: TTabSheet;
    Panel37: TPanel;
    Label19: TLabel;
    Label18: TLabel;
    EdNewTexto: TdmkEdit;
    TabSheet8: TTabSheet;
    Panel9: TPanel;
    BtSaida: TBitBtn;
    dmkEdit4: TdmkEdit;
    Label28: TLabel;
    Label29: TLabel;
    EdOldTextoPre: TdmkEdit;
    Label30: TLabel;
    EdOldTextoPos: TdmkEdit;
    Label31: TLabel;
    EdNewTextoPre: TdmkEdit;
    Label32: TLabel;
    EdNewTextoPos: TdmkEdit;
    EdOldTexto: TdmkEdit;
    BitBtn7: TBitBtn;
    CkEspacosPre: TCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure BtArquivosClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtCancelaClick(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BtBackupClick(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure GradeBDblClick(Sender: TObject);
    procedure GradeADblClick(Sender: TObject);
    procedure BtPesquisa2Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure ListBox04DblClick(Sender: TObject);
    procedure ListBox04Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
    procedure SpeedButton8Click(Sender: TObject);
    procedure SpeedButton9Click(Sender: TObject);
    procedure Dodiretrioselecionado1Click(Sender: TObject);
    procedure Listadearquivos1Click(Sender: TObject);
    procedure BtSubstituiClick(Sender: TObject);
    procedure EdTxtPreChange(Sender: TObject);
    procedure SbArqScanClick(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure SalvarClick(Sender: TObject);
    procedure SGDirsDblClick(Sender: TObject);
    procedure BitBtn7Click(Sender: TObject);
    procedure EdOldTextoPreRedefinido(Sender: TObject);
  private
    { Private declarations }
    FScanAborted: Boolean;
    function ComparaTextosComMascara(Txt1, Txt2: String; CharCase: Boolean): Boolean;
    function Xls_To_StringGrid(AGrid: TStringGrid; AXLSFile: String; PB1:
             TProgressBar; LaAviso1, LaAviso2: TLabel; Planilha: Word): Boolean;
    procedure DefinePasta();
    procedure CarregaCaminhos();
    procedure SubstituiTextoSimples();
    procedure SubstituiTextoAvancado();
  public
    { Public declarations }
    FMemoFile, FArqCam: String;
  end;

  var
  FmPesquisa: TFmPesquisa;

implementation

uses UnMyObjects, Principal, Textos;

{$R *.DFM}

procedure TFmPesquisa.FormCreate(Sender: TObject);
var
  cam: String;
begin
  ImgTipo.SQLType := stPsq;
  PageControl1.ActivePageIndex := 0;
  PCSubstituir.ActivePageIndex := 0;
  FMemoFile := ExtractFilePath(Application.ExeName) + 'Exclusoes.txt';
  Cam := Application.Title + 'Pesquisa\';
  EdTxtPre.Text := Geral.ReadAppKey('Texto', Cam, ktString, '', HKEY_LOCAL_MACHINE);
  //EdMascara.Text := FmPrincipal.ReadAppKey('Mascara', Cam, ktString, '*.*', HKEY_LOCAL_MACHINE);
  EdDiretorio.Text := Geral.ReadAppKey('Diretorio', Cam, ktString, '', HKEY_LOCAL_MACHINE);
  EdArqArqs.Text := Geral.ReadAppKey('ListaArquivos', Cam, ktString, '', HKEY_LOCAL_MACHINE);
  //
  (*EdExt1.Text := FmPrincipal.ReadAppKey('Ext1', Cam, ktString, '', HKEY_LOCAL_MACHINE);
  EdExt2.Text := FmPrincipal.ReadAppKey('Ext2', Cam, ktString, '', HKEY_LOCAL_MACHINE);
  EdExt3.Text := FmPrincipal.ReadAppKey('Ext3', Cam, ktString, '', HKEY_LOCAL_MACHINE);
  EdExt4.Text := FmPrincipal.ReadAppKey('Ext4', Cam, ktString, '', HKEY_LOCAL_MACHINE);*)
  //
  //
  EdDataStr.Text := FormatDateTime('yymmdd_hhnnss', now);
  //
  GradeA.Cells[0,0] := 'Seq��ncia';
  GradeA.Cells[1,0] := '1� linha';
  GradeA.Cells[2,0] := 'Nome do arquivo';
  //
  GradeB.Cells[0,0] := 'Seq��ncia';
  GradeB.Cells[1,0] := 'Texto pesquisado';
  GradeB.Cells[2,0] := 'Nome do arquivo';
  GradeB.Cells[3,0] := 'N� linha';
  GradeB.Cells[4,0] := 'Linha inteira (Texto)';
  //
  //
  GradeC.Cells[0,0] := 'Seq��ncia';
  GradeC.Cells[1,0] := 'Texto pesquisado';
  GradeC.Cells[2,0] := 'Itens';
  //
  SGDirs.Cells[0,0] := 'Seq��ncia';
  SGDirs.Cells[1,0] := 'Texto pesquisado';
  //
  DefinePasta();

  if FileExists(FMemoFile) then Geral.LeArquivoToMemo(FMemoFile, MeExcl);
end;

procedure TFmPesquisa.BtSaidaClick(Sender: TObject);
begin
  MLAGeral.ExportaMemoToFile(MeExcl, FMemoFile, True, False, False);
  Close;
end;

procedure TFmPesquisa.BtSubstituiClick(Sender: TObject);
begin
  case PCSubstituir.ActivePageIndex of
    0: SubstituiTextoSimples();
    1: SubstituiTextoAvancado();
    else Geral.MB_Erro('Forma de substitui��o n�o definida!');
  end;
end;

procedure TFmPesquisa.CarregaCaminhos();
var
  F: TextFile;
  Tmp, x, y: Integer;
  TmpStr: string;
  Senha: String;
  num, i: Integer;
begin
  if not FileExists(FArqCam) then
    Exit;
  AssignFile(F, FArqCam);
  Reset(F);
  Readln(F, Tmp);
  SGDirs.ColCount := Tmp;
  Readln(F, Tmp);
  SGDirs.RowCount := Tmp + 1;
  for x := 0 to SGDirs.ColCount-1 do
  begin
    for y := 0 to SGDirs.RowCount-1 do
    begin
      Readln(F, TmpStr);
      if x = 0 then
        SGDirs.Cells[x,y] := IntToStr(y)
      else
        SGDirs.Cells[x,y] := TmpStr;
    end;
  end;
  CloseFile(F);
  // Fazer s� no final! Para n�o dar erro!
  SGDirs.RowCount := SGDirs.RowCount + 1;
end;

function TFmPesquisa.ComparaTextosComMascara(Txt1, Txt2: String; CharCase: Boolean): Boolean;
var
  t1, t2, ta, tb, s: String;
  n, k: Integer;
begin
  if not CharCase then
    Result := AnsiCompareText(Txt1, Txt2) = 0
  else
    Result := AnsiCompareStr(Txt1, Txt2) = 0;
  if not Result then
  begin
    n := pos('*', Txt1);
    if n > 0  then
    begin
      if CharCase then
      begin
        t1 := Txt1;
        t2 := Txt2;
      end else begin
        t1 := AnsiLowercase(Txt1);
        t2 := AnsiLowercase(Txt2);
      end;
      // Ver se tem m�scara antes da palavra
      if n > 1 then
      begin
        ta := Copy(t1, 1, n-1) + '*';
        tb := Copy(t2, 1, n-1) + '*';
        t2 := Copy(t2, n+1);
      end else begin
        ta := '';
        tb := '';
      end;
      t1 := Copy(t1, n+1);

      // se combinar, continua
      if ta = tb then
      begin
        n := pos('*', t1);
        while n > 0 do
        begin
          if n = 1 then
          begin
            //Result := True;
            Break;
          end else begin
            s := Copy(t1, 1, n-1);
            k := pos(s, t2);
            if k > 0 then
            begin
              t2 := Copy(t2, k + Length(s));
              t1 := Copy(t1, n+1);
            end;
            n := pos('*', t1);
          end;
        end;
        if t2 > t1 then
          s := Copy(t2, Length(t2) - Length(t1) + 1)
        else
          s := t2;
        Result := (t1 = '') or (t1 = s);
      end;
    end;
  end;
end;

procedure TFmPesquisa.DefinePasta;
var
  Pasta: String;
begin
  Pasta := ExtractFileDir(Application.ExeName) + '\Pesquisa';
  ForceDirectories(Pasta);
  FArqCam := Pasta + '\Caminhos.gtf';
  //
  CarregaCaminhos();
end;

procedure TFmPesquisa.Dodiretrioselecionado1Click(Sender: TObject);
var
  cam: String;
  I: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    Cam := Application.Title + 'Pesquisa\';
    Geral.WriteAppKey('Texto', Cam, EdTxtPre.Text, ktString, HKEY_LOCAL_MACHINE);
    //Geral.WriteAppKey('Mascara', Cam, EdMascara.Text, ktString, HKEY_LOCAL_MACHINE);
    Geral.WriteAppKey('Diretorio', Cam, EdDiretorio.Text, ktString, HKEY_LOCAL_MACHINE);
    ListBox1.Clear;
    MyObjects.LimpaGrade(GradeA, 1, 1, True);
    for I := 0 to MeExtensoes.Lines.Count -1 do
    begin
      dmkPF.GetAllFiles(CkSub1.Checked, EdDiretorio.Text+'\'+
      MeExtensoes.Lines[I], ListBox1, False);
    end;
    BtPesquisa.Enabled := True;
    BtSubstitui.Enabled := True;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPesquisa.EdOldTextoPreRedefinido(Sender: TObject);
begin
  CkEspacosPre.Enabled := EdOldTextoPre.Text = EmptyStr;
  if CkEspacosPre.Enabled = False then
    CkEspacosPre.Checked := False;
end;

procedure TFmPesquisa.EdTxtPreChange(Sender: TObject);
begin
  EdOldTexto.Text := EdTxtPre.Text;
  EdNewTexto.Text := EdTxtPre.Text;
end;

procedure TFmPesquisa.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPesquisa.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPesquisa.SalvarClick(Sender: TObject);
var
  F: TextFile;
  x, y, i, Linhas: Integer;
  Senha, Nums, Txt: String;
begin
  Linhas := 0;
  for y := 0 to SGDirs.RowCount-1 do
  begin
    if Trim(SGDirs.Cells[1,y]) <> EmptyStr then
      Linhas := Linhas;
  end;
  Linhas := Linhas + 2;
  AssignFile(F, FArqCam);
  Rewrite(F);
  Writeln(F, SGDirs.ColCount);
  Writeln(F, Linhas); // SGDirs.RowCount);
  //Linhas := 1;
  for x := 0 to SGDirs.ColCount-1 do
  begin
    for y := 0 to SGDirs.RowCount-1 do
    begin
      Txt := SGDirs.Cells[1,y];
      if Trim(Txt) <> EmptyStr then
      begin
        Txt := SGDirs.Cells[x,y];
        Writeln(F, Txt);
      end;
    end;
  end;
  CloseFile(F);
  //
  CarregaCaminhos();
end;

procedure TFmPesquisa.SbArqScanClick(Sender: TObject);
const
  C = ' $%&()*+,-./0123456789:;<=>?ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz';
var
  I, (*J,*) N, A, Lin, Col: Integer;
  Texto, Novo: String;
begin
  Screen.Cursor := crHourGlass;
  try
    MeCI_Txt.Lines.Clear;
    MeCI_Sim.Lines.Clear;
    MeCI_Nao.Lines.Clear;
    Application.ProcessMessages;
    Texto := MLAGeral.LoadFileToText(EdArqScan.Text);
    Novo := '';
    Lin := 1;
    Col := 0;
    //A := 0;
    N := 0;
    for I := 1 to Length(Texto) do
    begin
      Col := Col + 1;
      A := N;
      N := Ord(Texto[I]);
      if N = 10 then
      begin
        if (I > 1) and (A <> 10) then
        begin
          Lin := Lin + 1;
          Col := 0;
          Novo := Novo + Texto[I];
        end else
        begin
          MeCI_Sim.Lines.Add(
            'Lin: ' + Geral.FFn(Lin, 6) +
            ' Col: ' + Geral.FFN(Col, 4) + ' = ' + Geral.FFN(N, 3));
          MeCI_Sim.Update;
        end;
      end
      else
      if N = 13 then
      begin
        Col := 0;
        if (I > 1) and (A <> 13) then
        begin
          Novo := Novo + Texto[I];
        end else
        begin
          MeCI_Sim.Lines.Add(
            'Lin: ' + Geral.FFn(Lin, 6) +
            ' Col: ' + Geral.FFN(Col, 4) + ' = ' + Geral.FFN(N, 3));
          MeCI_Sim.Update;
        end;
      end
      else
      if N in ([39]) then
      begin
        Novo := Novo + Texto[I];
      end else
      begin
        if (N  < 32) then
        begin
          MeCI_Sim.Lines.Add(
            'Lin: ' + Geral.FFn(Lin, 6) +
            ' Col: ' + Geral.FFN(Col, 4) + ' = ' + Geral.FFN(N, 3));
          MeCI_Sim.Update;
        end else
        {
        if (N  = ?) then
        begin
          Novo := Novo + ?;
          MeCI_Cor.Lines.Add(
            'Lin: ' + Geral.FFn(Lin, 6) +
            ' Col: ' + Geral.FFN(Col, 4) + ' ?>?');
          MeCI_Sim.Update;
        end else
        }
        begin
          Novo := Novo + Texto[I];
          if pos(Texto[I], C) = 0 then
          begin
            MeCI_Nao.Lines.Add(
            'L: ' + Geral.FFn(Lin, 6) +
            ' C: ' + Geral.FFN(Col, 4) + ' = ' + Geral.FFN(N, 3) + '(' + Texto[I] + ')' );
            MeCI_Sim.Update;
          end;
        end;
      end;
      //
      Application.ProcessMessages;
    end;
    MeCI_Txt.Text := Novo;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPesquisa.SGDirsDblClick(Sender: TObject);
var
  Row, Col: Integer;
begin
  EdArqDir4.Text := SGDirs.Cells[1, SGDirs.Row];
end;

procedure TFmPesquisa.SpeedButton1Click(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDiretorio);
end;

procedure TFmPesquisa.BtArquivosClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMArquivos, BtArquivos);
end;

procedure TFmPesquisa.BtCancelaClick(Sender: TObject);
begin
  FScanAborted := True;
end;

procedure TFmPesquisa.BtPesquisa2Click(Sender: TObject);
var
  i, J, n, P, {ic, kc,} E, O, Z, Y: integer;
  //Lista: TStringList;
  //NaoQuer: Boolean;
  Linha, Texto, Txt1, Txt2, Txt3: String;
begin
  MeShow.Lines.Clear;
  Screen.Cursor := crHourGlass;
  PB2.Position := 0;
  PB2.Max := ListBox1.Items.Count;
  //Lista := TStringList.Create;
  MyObjects.LimpaGrade(GradeA, 1, 1, True);
  MyObjects.LimpaGrade(GradeB, 1, 1, True);
  MyObjects.LimpaGrade(GradeC, 1, 1, True);
  //ic := 0;
  //kc := 0;
  for i := 0 to ListBox1.Items.Count -1 do
  begin
    PB2.Position := PB2.Position + 1;
    Update;
    MeArq.Lines.Clear;
    PnAviso.Caption := 'Avaliando o arquivo "' + ListBox1.Items[i] + '"...';
    PnAviso.Update;
    Application.ProcessMessages;
    //
    if FScanAborted then
    begin
      Screen.Cursor := crDefault;
      Exit;
    end;
    n := 0;
    for O := 0 to MeAnteriores.Lines.Count -1 do
    begin
      if n <> 0 then Break;
      n := FmPrincipal.ScanFile(ListBox1.Items[i], MeAnteriores.Lines[O], CkCaseSensitive.Checked);
    end;
    if n > 0 then
    begin
      PnAviso.Caption := 'Carregando o arquivo "' + ListBox1.Items[i] + '"...';
      PnAviso.Update;
      Application.ProcessMessages;
      //
      MeArq.Lines.LoadFromFile(ListBox1.Items[i]);
      // Parei aqui
      MeCopy.Lines.Clear;
      MeTxt.Lines.Clear;
      for J := 0 to MeArq.Lines.Count - 1 do
      begin
        Linha := MeArq.Lines[J];
        for Z := 0 to MeAnteriores.Lines.Count - 1 do
        begin
          Texto := MeAnteriores.Lines[Z];
          P := pos(Texto, Linha);
          if P > 0 then
          begin
            p := p + Length(Texto);
            Txt1 := Copy(Linha, 1, p-1);
            Txt3 := Trim(Copy(Linha, p));
            E := pos(' ', Txt3);
            if E > 0 then
            begin
              Txt2 := Copy(Txt3, 1, E);
              Txt3 := Copy(Txt3, E + 1);
            end else begin
              Txt2 := Txt3;
              Txt3 := ''
            end;
            Txt2 := Lowercase(Txt2);
            if Txt2 <> '' then Txt2 := ' ' + Txt2;
            MeTxt.Lines.Add(Txt1 + Txt2 + Txt3);
            Linha := Txt1 + Txt2 + Txt3;
            if Txt3 <> '' then
            begin
              for Y := 0 to MeMostrar.Lines.Count - 1 do
              begin
                if pos(MeMostrar.Lines[Y], Txt3) > 0 then
                  MeShow.Lines.Add(ListBox1.Items[i] + ' : [' +
                  IntToStr(J) + '] >> ' + Linha);
              end;
            end
          end;
        end;
        MeCopy.Lines.Add(Linha);
      end;
      Geral.SalvaTextoEmArquivo(ListBox1.Items[i], MeCopy.Text, True);
    end;
  end;
  Screen.Cursor := crDefault;
end;

procedure TFmPesquisa.BtPesquisaClick(Sender: TObject);
var
  IC, KB, KC: Integer;
  Lista: TStringList;
  NaoQuer: Boolean;
  //
  TxtA, TxtB: String;
  //
  procedure PesquisaTexto(TextoPsq: String; RemoveBrancos: Boolean);
  var
    I, N, K, M, SubItens: Integer;
    Continua: Boolean;
  begin
    SubItens := 0;
    for i := 0 to ListBox1.Items.Count -1 do
    begin
      try
        MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, ListBox1.Items[i]);
        //
        if FScanAborted then
        begin
          Screen.Cursor := crDefault;
          Exit;
        end;
        Continua := False;
        if RemoveBrancos then
          Continua := True
        else
        begin
          n := FmPrincipal.ScanFile(ListBox1.Items[i], TextoPsq, CKCaseSensitive.Checked);
          Continua := n > 0;
        end;
        if Continua then
        begin
          ic := ic + 1;
          GradeA.RowCount := ic + 1;
          GradeA.Cells[0,ic] := IntToStr(ic);
          GradeA.Cells[1,ic] := IntToStr(n);
          GradeA.Cells[2,ic] := ListBox1.Items[i];
          //ListBox2.Items.Add(ListBox1.Items[i]+' Caracter ->'+IntToStr(n));
          Lista.Clear;
          Lista.LoadFromFile(ListBox1.Items[i]);
          //
          for k := 0 to Lista.Count -1 do
          begin
            TxtA := LowerCase(TextoPsq);
            TxtB := LowerCase(Lista.Strings[k]);
            if RemoveBrancos then
            begin
              while pos('  ', TxtB) > 0 do
                TxtB := StringReplace(TxtB, '  ', ' ',[rfReplaceAll]);
            end;
            if Pos(TxtA, TxtB) > 0 then
            begin
              {
              Geral.MB_Aviso('Texto localizado: "' + TxtA + '"' + #13#10 +
              'Texto da linha: ' + TxtB);
              }
              NaoQuer := False;
              for m := 0 to MeExcl.Lines.Count - 1 do
              begin
                if Pos(LowerCase(MeExcl.Lines[m]), LowerCase(Lista.Strings[k])) > 0 then
                begin
                  NaoQuer := True;
                  Break;
                end;
              end;
              if not NaoQuer then
              begin
                SubItens := SubItens + 1;
                KB := KB + 1;
                GradeB.RowCount := KB+1;
                GradeB.Cells[0,KB] := IntToStr(KB);
                GradeB.Cells[1,KB] := TextoPsq;
                GradeB.Cells[2,KB] := ListBox1.Items[i];
                GradeB.Cells[3,KB] := IntToStr(k);
                GradeB.Cells[4,KB] := Lista.Strings[k];
                GradeB.Cells[5,KB] := '';//Lista.Strings[k];
                //ListBox3.Items.Add(ListBox1.Items[i]+ '  ['+IntToStr(k)+']  '+
                  //Lista.Strings[k]);
              end;
            end;
            Application.ProcessMessages;
          end;
        end;
      finally
        // nada
      end;
    end;
    KC := KC + 1;
    GradeC.RowCount := KC+1;
    GradeC.Cells[0,KC] := IntToStr(KC);
    GradeC.Cells[1,KC] := TextoPsq;
    GradeC.Cells[2,KC] := Geral.FF0(SubItens);
  end;
  //
var
  PAIni, PAFim, PACasas, PA_I: Integer;
  PAPos, TxtPA: String;
begin
  Screen.Cursor := crHourGlass;
  try
    FScanAborted := False;
    Lista := TStringList.Create;
    MyObjects.LimpaGrade(GradeA, 1, 1, True);
    MyObjects.LimpaGrade(GradeB, 1, 1, True);
    ic := 0;
    KB := 0;
    KC := 0;
    PB1.Position := 0;
    PB1.Max := ListBox1.Items.Count;
    //
    if CkPAUsa.Checked then
    begin
      PAIni   := EdPAIni.ValueVariant;
      PAFim   := EdPAFim.ValueVariant;
      PACasas := EdPACasas.ValueVariant;
      PAPos   := EdPATxtPos.Text;
      //
      PB1.Max := (ListBox1.Items.Count) * (PAFim - PAIni + 1);
      //
      for PA_I := PAIni to PAFim do
      begin
        TxtPA := EdTxtPre.Text + Geral.FFN(PA_I, PACasas) + PAPos;
        PesquisaTexto(TxtPA, CkPARemoveBrancos.Checked);
      end;
    end else
    begin
      PB1.Max := ListBox1.Items.Count;
      PesquisaTexto(EdTxtPre.Text, False);
    end;
    //
    PB1.Position := 0;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    Screen.Cursor := crDefault;
  except
    raise;
  end;
end;

procedure TFmPesquisa.SpeedButton2Click(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDiretorio2);
end;

procedure TFmPesquisa.BitBtn1Click(Sender: TObject);
var
  cam: String;
begin
  Screen.Cursor := crHourGlass;
  try
    Cam := Application.Title + 'Pesquisa\';
    //
    (*?
    Geral.WriteAppKey('Ext1', Cam, EdExt1.Text, ktString, HKEY_LOCAL_MACHINE);
    Geral.WriteAppKey('Ext2', Cam, EdExt2.Text, ktString, HKEY_LOCAL_MACHINE);
    Geral.WriteAppKey('Ext3', Cam, EdExt3.Text, ktString, HKEY_LOCAL_MACHINE);
    Geral.WriteAppKey('Ext4', Cam, EdExt4.Text, ktString, HKEY_LOCAL_MACHINE);*)
    Geral.WriteAppKey('Diretorio2', Cam, EdDiretorio2.Text, ktString, HKEY_LOCAL_MACHINE);
    ListBox5.Clear;
    dmkPF.GetAllFiles(CkSub2.Checked, EdDiretorio2.Text+'\*.*', ListBox5, True);
    BtBackup.Enabled := True;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPesquisa.BtBackupClick(Sender: TObject);
var
  i, j, p: Integer;
  Cam, Comp1, Comp2: String;
  Eh: Boolean;
begin
  Screen.Cursor := crHourGlass;
  Progress.Position := 0;
  Progress.Max := ListBox5.Items.Count;
  Progress.Visible := True;
  for i := 0 to ListBox5.Items.Count -1 do
  begin
    Progress.Position := Progress.Position + 1;
    Progress.Update;
    Application.ProcessMessages;
    Eh := False;
    for j := 0 to MeExt.Lines.Count - 1 do
    begin
      p := pos('*', MeExt.Lines[j]);
      if p > 0 then
      begin
        Comp1 := Copy(ExtractFileExt(ListBox5.Items[i]), 1, p-1);
        Comp2 := Copy(MeExt.Lines[j], 1, p-1);
      end else begin
        Comp1 := ExtractFileExt(ListBox5.Items[i]);
        Comp2 := MeExt.Lines[j];
      end;
      if Uppercase(Comp1) = Uppercase(Comp2) then
      begin
        Eh := True;
        Break;
      end;
    end;
    (*if (Uppercase(ExtractFileExt(ListBox5.Items[i])) = Uppercase(EdExt1.Text))
    or (Uppercase(ExtractFileExt(ListBox5.Items[i])) = Uppercase(EdExt2.Text))
    or (Uppercase(ExtractFileExt(ListBox5.Items[i])) = Uppercase(EdExt3.Text))
    or (Uppercase(ExtractFileExt(ListBox5.Items[i])) = Uppercase(EdExt4.Text))*)
    if Eh then
      ListBox4.Items.Add(ListBox5.Items[i]+' n�o foi copiado.')
    else begin
      Cam := ListBox5.Items[i];
      Insert(EdDataStr.Text, Cam, Length(EdDiretorio2.Text)+1);
      ForceDirectories(ExtractFilePath(Cam));
      if CopyFile(PChar(ListBox5.Items[i]), PChar(Cam), False) then
        ListBox4.Items.Add(Cam+' foi criado.')
      else ListBox4.Items.Add(Cam+' n�o pode ser criado.');
    end;
    ListBox4.Update;
    Application.ProcessMessages;
  end;
  Progress.Visible := False;
  Screen.Cursor := crDefault;
end;

procedure TFmPesquisa.SpeedButton3Click(Sender: TObject);
begin
  if OpenDialog1.Execute then
    EdArq1.Text := OpenDialog1.FileName;
end;

procedure TFmPesquisa.SpeedButton4Click(Sender: TObject);
begin
  if OpenDialog1.Execute then
    EdArq2.Text := OpenDialog1.FileName;
end;

procedure TFmPesquisa.SpeedButton5Click(Sender: TObject);
var
  IniDir, Arquivo: String;
  //Lista: TStringList;
  //i, p,
  //n: integer;
begin
  //n := 0;
  IniDir := ExtractFileDir(Application.ExeName);
  Arquivo := '';
  if MyObjects.FileOpenDialog(Self, IniDir, Arquivo,
  'Selecione o diret�rio', '', [], Arquivo) then
  begin
    EdArqDir4.Text := ExtractFilePath(Arquivo);
  end;
end;

procedure TFmPesquisa.SpeedButton6Click(Sender: TObject);
begin
  Ed4_Cam.CopyToClipboard;
end;

procedure TFmPesquisa.SpeedButton7Click(Sender: TObject);
begin
  Ed4_Dir.CopyToClipboard;
end;

procedure TFmPesquisa.SpeedButton8Click(Sender: TObject);
begin
  Ed4_Arq.CopyToClipboard;
end;

procedure TFmPesquisa.SpeedButton9Click(Sender: TObject);
var
  IniDir, Arquivo: String;
begin
  IniDir := ExtractFileDir(Application.ExeName);
  Arquivo := '';
  if MyObjects.FileOpenDialog(Self, IniDir, Arquivo,
  'Selecione o arquivo', '', [], Arquivo) then
    EdArqArqs.Text := Arquivo;
end;

procedure TFmPesquisa.SubstituiTextoAvancado();
var
  i, n, ic, J, qIni: integer;
  Lista: TStringList;
  LocTexto, NewTextoPre, OldTextoPre, NewTextoPos, OldTextoPos, SentencaOri, EspacosIni, SentencaNew: String;
begin
  Lista := TStringList.Create;
  try
    LocTexto := EdTxtPre.Text;
    OldTextoPre := EdOldTextoPre.Text;
    NewTextoPre := EdNewTextoPre.Text;
    OldTextoPos := EdOldTextoPos.Text;
    NewTextoPos := EdNewTextoPos.Text;

    if MyObjects.FIC(LocTexto = EmptyStr, nil,
      'N�o foi definido nenhum texto a ser localizado!') then Exit;

    if MyObjects.FIC((OldTextoPre = EmptyStr) and (CkEspacosPre.Checked = False), nil,
      'N�o foi definido nenhum texto ANTES a ser substitu�do!') then Exit;

    if MyObjects.FIC(OldTextoPos = EmptyStr, nil,
      'N�o foi definido nenhum texto DEPOIS a ser substitu�do!') then Exit;

    if NewTextoPre = EmptyStr then
      if Geral.MB_Pergunta(
      'N�o foi definido nenhum texto ANTES substituto! Deseja continuar assim mesmo?')
      <> ID_YES then Exit;

    if NewTextoPos = EmptyStr then
      if Geral.MB_Pergunta(
      'N�o foi definido nenhum texto DEPOIS substituto! Deseja continuar assim mesmo?')
      <> ID_YES then Exit;
    //
    //
    if Geral.MB_Pergunta(
    'Confirma a substitui��o de "' + OldTextoPre + '%' + OldTextoPos + '" por "' +
    NewTextoPre + '%' + NewTextoPos + '" nas linhas em que for localizado o texto "' +
    LocTexto + '"? N�o ser� poss�vel reverter o arquivo!') <> ID_YES then
      Exit;
    //
    Screen.Cursor := crHourGlass;
    MyObjects.LimpaGrade(GradeA, 1, 1, True);
    MyObjects.LimpaGrade(GradeB, 1, 1, True);
    MyObjects.LimpaGrade(GradeC, 1, 1, True);
    ic := 0;
    //kc := 0;
    for i := 0 to ListBox1.Items.Count -1 do
    begin
      if FScanAborted then
      begin
         Screen.Cursor := crDefault;
         Exit;
      end;
      n := FmPrincipal.ScanFile(ListBox1.Items[i], LocTexto, CkCaseSensitive.Checked);
      if n > 0 then
      begin
        ic := ic + 1;
        GradeA.RowCount := ic + 1;
        GradeA.Cells[0,ic] := IntToStr(ic);
        GradeA.Cells[1,ic] := IntToStr(n);
        GradeA.Cells[2,ic] := ListBox1.Items[i];
        //
        Lista.Clear;
        Lista.LoadFromFile(ListBox1.Items[i]);
        for J := 0 to Lista.Count - 1 do
        begin
          if pos(LocTexto, Lista[J]) > 0 then
          begin
            //Lista[J] := ReplaceStr(Lista[J], OldTextoPre, NewTextoPre);
            //Lista[J] := ReplaceStr(Lista[J], OldTextoPos, NewTextoPos);
            //
            SentencaOri := Lista[J];
            //
            if CkEspacosPre.Checked then
            begin
              SentencaNew := TrimLeft(SentencaOri);
              qIni := Length(SentencaOri) - Length(SentencaNew);
              if qIni > 0 then
                EspacosIni  := Copy(SentencaOri, 1, qIni)
              else
                EspacosIni  := EmptyStr;
              //
              Lista[J] := EspacosIni + NewTextoPre + SentencaNew;
            end else
            begin
              Lista[J] := StringReplace(Lista[J], OldTextoPre, NewTextoPre, [(*rfReplaceAll,*) rfIgnoreCase]);
            end;
            Lista[J] := StringReplace(Lista[J], OldTextoPos, NewTextoPos, [(*rfReplaceAll,*) rfIgnoreCase]);
          end;
        end;
        Lista.SaveToFile(ListBox1.Items[i]);
        //
      end;
    end;
    Screen.Cursor := crDefault;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPesquisa.SubstituiTextoSimples();
var
  i, n, ic, J: integer;
  Lista: TStringList;
  LocTexto, NewTexto, OldTexto(*, Linha*): String;
begin
  Lista := TStringList.Create;
  try
    LocTexto := EdTxtPre.Text;
    NewTexto := EdNewTexto.Text;
    OldTexto := EdOldTexto.Text;
    if LocTexto = '' then
    begin
      Geral.MensagemBox('N�o foi definido nenhum texto a ser localizado!',
      'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
    if OldTexto = '' then
    begin
      Geral.MensagemBox('N�o foi definido nenhum texto a ser substitu�do!',
      'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
    if NewTexto = '' then
    begin
      if Geral.MensagemBox(
      'N�o foi definido nenhum texto substituto! Deseja continuar assim mesmo?',
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then
      Exit;
    end;
    //
    if Geral.MensagemBox(
    'Confirma a substitui��o de "' + OldTexto + '" por "' +
    NewTexto + '" nas linhas em que for localizado o texto "' +
    LocTexto + '"? N�o ser� poss�vel reverter o arquivo!',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then
      Exit;
    //
    Screen.Cursor := crHourGlass;
    MyObjects.LimpaGrade(GradeA, 1, 1, True);
    MyObjects.LimpaGrade(GradeB, 1, 1, True);
    MyObjects.LimpaGrade(GradeC, 1, 1, True);
    ic := 0;
    //kc := 0;
    for i := 0 to ListBox1.Items.Count -1 do
    begin
      if FScanAborted then
      begin
         Screen.Cursor := crDefault;
         Exit;
      end;
      n := FmPrincipal.ScanFile(ListBox1.Items[i], LocTexto, CkCaseSensitive.Checked);
      if n > 0 then
      begin
        ic := ic + 1;
        GradeA.RowCount := ic + 1;
        GradeA.Cells[0,ic] := IntToStr(ic);
        GradeA.Cells[1,ic] := IntToStr(n);
        GradeA.Cells[2,ic] := ListBox1.Items[i];
        {
        Lista.Clear;
        Lista.LoadFromFile(ListBox1.Items[i]);
        }
        //Lista := MLAGeral.LoadFileToText(ListBox1.Items[i]);
        Lista.Clear;
        Lista.LoadFromFile(ListBox1.Items[i]);
        for J := 0 to Lista.Count - 1 do
        begin
          if pos(LocTexto, Lista[J]) > 0 then
          begin
            Lista[J] := ReplaceStr(Lista[J], OldTexto, NewTexto);
          end;
        end;
        //MLAGeral.SaveTextToFile(ListBox1.Items[i], Lista.Text);
        Lista.SaveToFile(ListBox1.Items[i]);
        //
      end;
    end;
    Screen.Cursor := crDefault;
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TFmPesquisa.Xls_To_StringGrid(AGrid: TStringGrid; AXLSFile: String;
  PB1: TProgressBar; LaAviso1, LaAviso2: TLabel; Planilha: Word): Boolean;
const
  xlCellTypeLastCell = $0000000B;
  CharWid = 7.2;
var
  XLApp, Sheet: OLEVariant;
  RangeMatrix: Variant;
  x, y, k, r, Larg, m, n: Integer;
  ColWid: array[0..255] of Integer;
  Info: String;
  Valor: Variant;
begin
  Info := '';
  Screen.Cursor := crHourGlass;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo arquivo excel ...');
  if PB1 <> nil then
    PB1.Position := 0;
  Result := False;
  try
    XLApp := CreateOleObject('Excel.Application');
  except
    try
      XLApp := CreateOleObject('Works.Application');
    except
      //raise;
      Geral.MB_Aviso('N�o foi poss�vel iniciar uma inst�ncia do "Excel"!' +
      sLineBreak + 'Verifique se o aplicativo "Excel" est� instalado neste computador!"');
      Exit;
    end;
  end;
  try
    //MyObjects.LimpaGrade(AGrid, 1, 1, True);
    XLApp.Visible := False;
    XLApp.Workbooks.Open(AXLSFile);
    try
      Sheet := XLApp.Workbooks[ExtractFileName(AXLSFile)].WorkSheets[Planilha];
    except
      Sheet := XLApp.Workbooks[ExtractFileName(AXLSFile)].WorkSheets[1];
    end;
    Sheet.Cells.SpecialCells(xlCellTypeLastCell, EmptyParam).Activate;
    x := XLApp.ActiveCell.Row;
    y := XLApp.ActiveCell.Column;
    //AGrid.RowCount := x;
    //AGrid.ColCount := y + 1;
    if PB1 <> nil then
    begin
      PB1.Max := x * (y + 1);
      PB1.Update;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Lendo arquivo excel. Pode demorar se o arquivo conter muitos dados ...');
    RangeMatrix := XLApp.Range['A1', XLApp.Cells.Item[X, Y]].Value;   // >... pode demorar
    k := 1; // 1?
    m := Round(CharWid * 5); // 2012-02-25 de "CharWid * 3" para "CharWid * 5"
    for n := 0 to 255 do ColWid[n] := m;
    repeat
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Carregando linha ' + Geral.FF0(K));
      Application.ProcessMessages;  // lendo linha
      //AGrid.Cells[0, (k - 1)] := FormatFloat('000', (k - 1));
      for r := 1 to y do
      begin
        if PB1 <> nil then
        begin
          PB1.Position := PB1.Position + 1;
          PB1.Update;
        end;
        Application.ProcessMessages;
        //
        try
          Valor := RangeMatrix[K, R];
          if VarType(Valor) = 10 (*vt_error*) then
            Valor := '#ERROR';
        except
          on E: Exception do
          begin
             Info := 'Erro na linha ' + Geral.FF0(K) + ' coluna "' +
               dmkPF.IntToColTxt(R) + '"';
             Geral.MB_Erro('Erro de importa��o do excel: ' +
               E.Message + sLineBreak + Info);
          end;
        end;
        if VarType(Valor) = varString then
          if Valor = EdTxtPre.Text then
            Geral.MB_Info(AXLSFile);
(*
        AGrid.Cells[r, (k - 1)] := Valor;
        Larg := Round((Length(Valor) + 1) * CharWid);
        if Larg > ColWid[r] then
          ColWid[r] := Larg;
*)
      end;
      Inc(k, 1);
      //AGrid.RowCount := k + 2;
    until k > x;
    if PB1 <> nil then
    begin
      PB1.Position := PB1.Max;
      PB1.Update;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Limpando mem�ria ...');
    RangeMatrix := Unassigned; // Limpando mem�ria
    // Deixar vazio para concatena��o limpa!
    Info := '';
  finally
    if Info = '' then
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Fechando arquivo');
    if not VarIsEmpty(XLApp) then
    begin
      XLApp.ActiveWorkBook.Saved:= 1;
      XLApp.DisplayAlerts:= 0;
      XLApp.ActiveWorkBook.Close(SaveChanges:= 0);
      XLApp.Quit;
      XLAPP := Unassigned;
      Sheet := Unassigned;
      Result := True;
    end;
    Screen.Cursor := crDefault;
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Ajustando largura de colunas ...');
{
  for n := 0 to AGrid.ColCount -1 do
    AGrid.ColWidths[n] := ColWid[n];
}
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, Info);
end;

procedure TFmPesquisa.BitBtn4Click(Sender: TObject);
begin
  Memo1.Lines.LoadFromFile(EdArq1.Text);
  Memo2.Lines.LoadFromFile(EdArq2.Text);
end;

procedure TFmPesquisa.BitBtn5Click(Sender: TObject);
  function GetFiles(Subdiretorios: Boolean; Dir, Arq: string;
    ListBox: TCustomListBox; ClearListBox: Boolean): Integer;
  var
    search: TSearchRec;
    directory, mask: string;
  begin

    Result := 0;
    mask := Dir + Arq;
    // N�o no loop!!!!
    if ClearListBox and (ListBox <> nil) then ListBox.Items.Clear;
    //
    directory := ExtractFilePath(mask);

    // find all files
    if FindFirst(mask, $23, search) = 0 then
    begin
      repeat
        // add the files to the listbox
        if ListBox <> nil then
        begin
          if ComparaTextosComMascara(Arq, search.Name, False) then
          //ListBox.Items.Add(directory + search.Name);
            ListBox.Items.Add(directory + search.Name);
        end;
        Result := Result + 1;
        //Inc(Count);
      until FindNext(search) <> 0;
    end;

    if Subdiretorios then
    begin
      if FindFirst(directory + '*.*', faDirectory, search) = 0 then
      begin
        repeat
          if ((search.Attr and faDirectory) = faDirectory) and (search.Name[1] <> '.') then
            Result := Result + GetFiles(Subdiretorios, Dir + Search.Name + '\', Arq, ListBox, False);
        until FindNext(search) <> 0;
        FindClose(search);
      end;
    end;
  end;
var
  cam: String;
  //I: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    ListBox04.Items.Clear;
    cam := EdArqDir4.Text;
    if cam[Length(cam)] <> '\' then
      cam := cam + '\';
    GetFiles(CkSubDir4.Checked, cam, EdArqNome4.Text + '.' + EdArqExte4.Text, ListBox04, False);
    {
    Cam := Application.Title + 'Pesquisa\';
    Geral.WriteAppKey('Texto', Cam, EdTxtPre.Text, ktString, HKEY_LOCAL_MACHINE);
    Geral.WriteAppKey('Diretorio', Cam, EdDiretorio.Text, ktString, HKEY_LOCAL_MACHINE);
    ListBox1.Clear;
    MyObjects.LimpaGrade(GradeA, 1, 1, True);
    for I := 0 to MeExtensoes.Lines.Count -1 do
    begin
      MLAGeral.GetAllFiles(CkSub1.Checked, EdDiretorio.Text+'\'+
      MeExtensoes.Lines[I], ListBox1, False);
    end;
    BtPesquisa.Enabled := True;
    }
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPesquisa.BitBtn6Click(Sender: TObject);
var
  i, n, k, m, ic, kc: integer;
  Lista: TStringList;
  NaoQuer: Boolean;
  //
  TxtA, TxtB: String;
begin
  Screen.Cursor := crHourGlass;
  try
    FScanAborted := False;
    Lista := TStringList.Create;
    MyObjects.LimpaGrade(GradeA, 1, 1, True);
    MyObjects.LimpaGrade(GradeB, 1, 1, True);
    ic := 0;
    kc := 0;
    PB1.Position := 0;
    PB1.Max := ListBox1.Items.Count;
    //
    for i := 0 to ListBox1.Items.Count -1 do
    begin
      try
        MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, ListBox1.Items[i]);
        //
        if FScanAborted then
        begin
          Screen.Cursor := crDefault;
          Exit;
        end;
        Xls_To_StringGrid(Gradeb, ListBox1.Items[i], PB1, LaAviso1, LaAviso2, 1);
{
        n := FmPrincipal.ScanFile(ListBox1.Items[i], EdTxtPre.Text, CkCaseSensitive.Checked);
        if n > 0 then
        begin
          ic := ic + 1;
          GradeA.RowCount := ic + 1;
          GradeA.Cells[0,ic] := IntToStr(ic);
          GradeA.Cells[1,ic] := IntToStr(n);
          GradeA.Cells[2,ic] := ListBox1.Items[i];
          //ListBox2.Items.Add(ListBox1.Items[i]+' Caracter ->'+IntToStr(n));
          Lista.Clear;
          Lista.LoadFromFile(ListBox1.Items[i]);
          //
          for k := 0 to Lista.Count -1 do
          begin
            TxtA := LowerCase(EdTxtPre.Text);
            TxtB := LowerCase(Lista.Strings[k]);
            if Pos(TxtA, TxtB) > 0 then
            begin
              (*
              Geral.MB_Aviso('Texto localizado: "' + TxtA + '"' + #13#10 +
              'Texto da linha: ' + TxtB);
              *)
              NaoQuer := False;
              for m := 0 to MeExcl.Lines.Count - 1 do
              begin
                if Pos(LowerCase(MeExcl.Lines[m]), LowerCase(Lista.Strings[k])) > 0 then
                begin
                  NaoQuer := True;
                  Break;
                end;
              end;
              if not NaoQuer then
              begin
                kc := kc + 1;
                GradeB.RowCount := kc+1;
                GradeB.Cells[0,kc] := IntToStr(kc);
                GradeB.Cells[1,kc] := ListBox1.Items[i];
                GradeB.Cells[2,kc] := IntToStr(k);
                GradeB.Cells[3,kc] := Lista.Strings[k];
                GradeB.Cells[4,kc] := '';//Lista.Strings[k];
                //ListBox3.Items.Add(ListBox1.Items[i]+ '  ['+IntToStr(k)+']  '+
                  //Lista.Strings[k]);
              end;
            end;
            Application.ProcessMessages;
          end;
        end;
}
      finally
        // nada
      end;
    end;
    PB1.Position := 0;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    Screen.Cursor := crDefault;
  except
    raise;
  end;
end;

procedure TFmPesquisa.BitBtn7Click(Sender: TObject);
var
  before, after : string;

begin
  // Try to replace all occurrences of a or A to THE
  before := 'This is a way to live A big life';

  after  := StringReplace(before, ' a ', ' THE ',
                          [(*rfReplaceAll,*) rfIgnoreCase]);
  ShowMessage('Before = '+before);
  ShowMessage('After  = '+after);
end;

procedure TFmPesquisa.BitBtn3Click(Sender: TObject);
var
  i, k: Integer;
  Difere: Boolean;
begin
  Difere := False;
  EdLinha.Text := '';
  if Memo1.Lines.Count > Memo2.Lines.Count then
    k := Memo1.Lines.Count else k := Memo2.Lines.Count;
  for i := 0 to k -1 do
  begin
    if Memo1.Lines[i] <> Memo2.Lines[i] then
    begin
      Difere := True;
      EdLinha.Text := IntToStr(i);
      EdLinha1.Text := Memo1.Lines[i];
      EdLinha2.Text := Memo2.Lines[i];
      //
      Break;
    end;
  end;
  if Difere = False then
    Application.MessageBox('N�o foram identificadas diferen�as nos textos!',
    'Mensagem', MB_OK+MB_ICONINFORMATION);
end;

procedure TFmPesquisa.GradeBDblClick(Sender: TObject);
var
  MemoryStream: TMemoryStream;
begin
  Screen.Cursor := crHourGlass;
  try
    RE1.Lines.LoadFromFile(GradeB.Cells[1, GradeB.Row]);
    Application.CreateForm(TFmTextos, FmTextos);
    FmTextos.FAtributesSize := 10;
    FmTextos.FAtributesName := 'Courier New';
    FmTextos.FSaveSit    := 0;
    FmTextos.FRichEdit   := RE1;
    MemoryStream := TMemoryStream.Create;
    RE1.Lines.SaveToStream(MemoryStream);
    MemoryStream.Position := 0;
    FmTextos.Editor.Lines.LoadFromStream(MemoryStream);
    MemoryStream.Free;
    //
    FmTextos.Splitter1.Visible := False;
    FmTextos.LbItensMD.Visible := False;
    FmTextos.FTipoLoc := vpLoc;
    FmTextos.FvpCol   := 0;
    FmTextos.FvpRow   := StrToInt(GradeB.Cells[3, GradeB.Row]);
    //
  finally
    Screen.Cursor := crDefault;
  end;
  FmTextos.ShowModal;
  FmTextos.Destroy;
end;

procedure TFmPesquisa.Listadearquivos1Click(Sender: TObject);
  procedure LeArquivo(Arquivo: String);
  var
    Arq: TextFile;
    Nome, S, X, Y: String;
    posI, posF, I, N, J: Integer;
    posE: Boolean;
  begin
    PosE := False;
    ListBox3.Items.Clear;
    posI := 1;
    //posF := 0;
    S := EdArqArqs.Text;
    for I := 1 to Length(S) do
    begin
      if S[I] = '\' then
      begin
        Nome := Copy(S, posI, I-posI);
        ListBox3.Items.Append(Nome);
        posI := I + 1;
      end;
    end;
    //
    AssignFile(Arq, Arquivo);
    if FileExists(Arquivo) then
    begin
      Reset(Arq);
      N := 0;
      while not Eof(Arq) do
      begin
        Readln(Arq, S);
        posI := 0;
        posF := 0;
        for I := 1 to Length(S) do
        begin
          if S[I] = Char(39) then
          begin
            if posI = 0 then
              posI := I
            else
            if posF = 0 then
              posF := I;
          end;
          if (posI > 0) and (posF >0) then
            N := N + 1;
          if (S[I] = ';') and (N > 0) then
          begin
            posE := True;
          end;
        end;
        if (posI > 0) and (posF >0) then
        begin
          Nome := Copy(S, PosI + 1, PosF-PosI-1);
          ListBox2.Items.Add(Nome);
          //
          J := 0;
          N := pos('..\', Nome);
          while N = 1 do
          begin
            Nome := Copy(Nome, 4);
            N := pos('..\', Nome);
            J := J + 1;
          end;
          X := '';
          for I := 0 to ListBox3.Items.Count - J -1 do
          begin
            X := X + ListBox3.Items[I] + '\';
          end;
          for I := 0 to MeExtensoes.Lines.Count - 1 do
          begin
            Nome := dmkPF.MudaExtensaoDeArquivo(Nome, Copy(MeExtensoes.Lines[I], 3));
            Y := X + Nome;
            if FileExists(Y) then
              ListBox1.Items.Add(Y);
          end;
        end;
        if posE then
          Break;
      end;
    end;
    CloseFile(Arq);
  end;

var
  cam: String;
  //I: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    Cam := Application.Title + 'Pesquisa\';
    Geral.WriteAppKey('ListaArquivos', Cam, EdArqArqs.Text, ktString, HKEY_LOCAL_MACHINE);
    ListBox1.Clear;
    MyObjects.LimpaGrade(GradeA, 1, 1, True);
    //
    LeArquivo(EdArqArqs.Text);
    //
    BtPesquisa.Enabled := True;
    BtSubstitui.Enabled := True;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPesquisa.ListBox04Click(Sender: TObject);
var
  Nome: String;
begin
  if ListBox04.Items.Count > 0 then
  begin
    Nome := ListBox04.Items[ListBox04.ItemIndex];
    Ed4_Cam.Text := Nome;
    Ed4_Dir.Text := ExtractFilePath(Nome);
    Ed4_Arq.Text := ExtractFileName(Nome);
  end;
end;

procedure TFmPesquisa.ListBox04DblClick(Sender: TObject);
var
  Nome: String;
begin
  if ListBox04.Items.Count > 0 then
  begin
    Nome := ListBox04.Items[ListBox04.ItemIndex];
    ShellExecute(Application.Handle, PChar('open'), PChar(Nome), PChar(''), nil, SW_NORMAL);
  end;
end;

procedure TFmPesquisa.GradeADblClick(Sender: TObject);
var
  MemoryStream: TMemoryStream;
begin
  Screen.Cursor := crHourGlass;
  try
    RE1.Lines.LoadFromFile(GradeA.Cells[3, GradeB.Row]);
    Application.CreateForm(TFmTextos, FmTextos);
    FmTextos.FAtributesSize := 10;
    FmTextos.FAtributesName := 'Courier New';
    FmTextos.FSaveSit    := 0;
    FmTextos.FRichEdit   := RE1;
    MemoryStream := TMemoryStream.Create;
    RE1.Lines.SaveToStream(MemoryStream);
    MemoryStream.Position := 0;
    FmTextos.Editor.Lines.LoadFromStream(MemoryStream);
    MemoryStream.Free;
    //
    FmTextos.Splitter1.Visible := False;
    FmTextos.LbItensMD.Visible := False;
    {
    FmTextos.FTipoLoc := vpLoc;
    FmTextos.FvpCol   := 0;
    FmTextos.FvpRow   := StrToInt(GradeB.Cells[3, GradeB.Row]);
    }
    //
  finally
    Screen.Cursor := crDefault;
  end;
  FmTextos.ShowModal;
  FmTextos.Destroy;
end;

end.

