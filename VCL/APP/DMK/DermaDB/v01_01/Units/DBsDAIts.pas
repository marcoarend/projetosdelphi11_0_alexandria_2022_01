unit DBsDAIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, ABSMain, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmDBsDAIts = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    EdNome: TdmkEdit;
    Label7: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenDBsDAIts(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TABSQuery;
    FDsCab: TDataSource;
  end;

  var
  FmDBsDAIts: TFmDBsDAIts;

implementation

uses UnMyObjects, Module, UnInternalConsts, DmkDAC_PF, WinRegDB, ModMemTab,
  UMemModule;

{$R *.DFM}

procedure TFmDBsDAIts.BtOKClick(Sender: TObject);
var
  Codigo, Controle: Integer;
  CtrFmt, Nome: String;
  //
  Caminho: String;
begin
  Codigo   := Geral.IMV(DBEdCodigo.Text);
  Controle := EdControle.ValueVariant;
  Nome     := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Controle := UnWinRegDB.BPGS1I32('dbsdaits', 'Controle', '', '',
    tsPos, ImgTipo.SQLType, Controle);
  CtrFmt := MyObjects.DBsDA_FmtRegistro(Controle);
  //
  if DmModMemTab.DBsDAIts_CarregaRegistro(Codigo, Controle, Nome) then
  begin
    //
    Caminho := MyObjects.DBsDA_CaminhoTabela('DBsDAIts');
    Geral.WriteAppKeyCU(CtrFmt, Caminho, Nome, ktString);
    //
    Caminho := MyObjects.DBsDA_CaminhoRegistro(Caminho, Controle);
    Geral.WriteAppKeyCU('Codigo', Caminho, Codigo, ktInteger);
    Geral.WriteAppKeyCU('Controle', Caminho, Controle, ktInteger);
    Geral.WriteAppKeyCU('Nome', Caminho, Nome, ktString);
    //
    ReopenDBsDAIts(Controle);
    //if ImgTipo.SQLType = stIns then
    begin
      Dmod.CriaDatabase(Nome);
    end;
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      //
      EdControle.ValueVariant  := 0;
      EdNome.ValueVariant      := '';
      EdNome.SetFocus;
    end else Close;
  end;
end;

procedure TFmDBsDAIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmDBsDAIts.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmDBsDAIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmDBsDAIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmDBsDAIts.ReopenDBsDAIts(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UMemMod.AbreABSQuery1(FQrIts, []);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

end.
