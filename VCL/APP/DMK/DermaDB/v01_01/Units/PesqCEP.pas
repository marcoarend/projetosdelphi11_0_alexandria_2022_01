unit PesqCEP;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral,  
    
     ComCtrls, DB,
  (*DBTables,*) Grids, DBGrids, DCPcrypt2, DCPblockciphers, DCPtwofish,
  dmkRadioGroup, Menus, mySQLDbTables;

type
  TFmPesqCEP = class(TForm)
    PainelConfirma: TPanel;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel3: TPanel;
    DCP_twofish1: TDCP_twofish;
    DNCEP: TDatabase;
    Memo1: TMemo;
    Splitter1: TSplitter;
    DBGrid1: TDBGrid;
    Query1: TQuery;
    DataSource1: TDataSource;
    StaticText1: TStaticText;
    TabSheet2: TTabSheet;
    Panel4: TPanel;
    BitBtn1: TBitBtn;
    Panel5: TPanel;
    Panel6: TPanel;
    BtAcao: TBitBtn;
    Panel7: TPanel;
    PMAcao: TPopupMenu;
    RecriatabelaDBTest1: TMenuItem;
    MySQL: TmySQLDatabase;
    Carregadadosde20041: TMenuItem;
    Carregadadosde20081: TMenuItem;
    QrLocal: TQuery;
    QrLocalLOC_NU_SEQUENCIAL: TIntegerField;
    QrLocalLOC_NOSUB: TWideStringField;
    QrLocalLOC_NO: TWideStringField;
    QrLocalCEP: TWideStringField;
    QrLocalUFE_SG: TWideStringField;
    QrLocalLOC_IN_SITUACAO: TIntegerField;
    QrLocalLOC_IN_TIPO_LOCALIDADE: TWideStringField;
    QrLocalLOC_NU_SEQUENCIAL_SUB: TIntegerField;
    QrLocalLOC_KEY_DNE: TWideStringField;
    PB1: TProgressBar;
    TabSheet3: TTabSheet;
    Query2: TQuery;
    Query1CODIGO: TFloatField;
    Query1MUNICIPIO: TWideStringField;
    Query1UF: TWideStringField;
    Query1COD_JURISD: TFloatField;
    DataSource2: TDataSource;
    Database1: TDatabase;
    Panel8: TPanel;
    DBGrid2: TDBGrid;
    Memo2: TMemo;
    Panel9: TPanel;
    BitBtn2: TBitBtn;
    Splitter2: TSplitter;
    StaticText2: TStaticText;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure RecriatabelaDBTest1Click(Sender: TObject);
    procedure BtAcaoClick(Sender: TObject);
    procedure Carregadadosde20041Click(Sender: TObject);
    procedure Carregadadosde20081Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
  private
    { Private declarations }
    procedure CarregaDados(Versao: String);
  public
    { Public declarations }
  end;

  var
  FmPesqCEP: TFmPesqCEP;

implementation

uses UnMyObjects, Principal;

{$R *.DFM}

procedure TFmPesqCEP.BitBtn2Click(Sender: TObject);
begin
  Query2.Close;
  Query2.SQL.Text := Memo2.Text;
  Query2.Open;
  //
  StaticText2.Caption := 'Registros: ' + IntToStr(Query2.RecordCount);
end;

procedure TFmPesqCEP.BtAcaoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAcao, BtAcao);
end;

procedure TFmPesqCEP.BtOKClick(Sender: TObject);
begin
  Query1.Close;
  Query1.SQL.Clear;
  Query1.SQL.Text := Memo1.Text;
  Query1.Open;
  //
  StaticText1.Caption := '   ' + IntToStr(Query1.RecordCount) + ' registros.';
end;

procedure TFmPesqCEP.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPesqCEP.Carregadadosde20041Click(Sender: TObject);
begin
  CarregaDados('2004');
end;

procedure TFmPesqCEP.Carregadadosde20081Click(Sender: TObject);
begin
  CarregaDados('2008');
end;

procedure TFmPesqCEP.CarregaDados(Versao: String);
begin
  if Application.MessageBox(PChar('Confirma a importa��o dos dados de ' +
  Versao  + '? A base de dados deve estar com a vers�o de ' + Versao + '!'),
  'Pergunta', MB_YESNOCANCEL + MB_ICONQUESTION) = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    QrLocal.Close;
    QrLocal.Open;
    PB1.Position := 0;
    PB1.Max := QrLocal.RecordCount;
    if Versao = '2004' then
    begin
      while not QrLocal.Eof do
      begin
        PB1.Position := PB1.Position + 1;
        MySQL.Execute(
          'INSERT INTO Loc2004 SET '  +
          'Sequen= ' + FormatFloat('0', QrLocalLOC_NU_SEQUENCIAL.Value) + ',' +
          'SubSeq= ' + FormatFloat('0', QrLocalLOC_NU_SEQUENCIAL_SUB.Value) + ',' +
          'Distri="' + String(QrLocalLOC_NO.Value) + '", ' +
          'Munici="' + String(QrLocalLOC_NOSUB.Value) + '" ');
        //
        QrLocal.Next;
      end;
    end;
    if Versao = '2008' then
    begin
      while not QrLocal.Eof do
      begin
        PB1.Position := PB1.Position + 1;
        MySQL.Execute(
          'INSERT INTO Loc2008 SET '  +
          'Sequen= ' + FormatFloat('0', QrLocalLOC_NU_SEQUENCIAL.Value) + ',' +
          'SubSeq= ' + FormatFloat('0', QrLocalLOC_NU_SEQUENCIAL_SUB.Value) + ',' +
          'Distri="' + String(QrLocalLOC_NO.Value) + '", ' +
          'Munici="' + String(QrLocalLOC_NOSUB.Value) + '" ');
        //
        QrLocal.Next;
      end;
    end;
  end;
  Screen.Cursor := crDefault;
  PB1.Position := 0;
end;

procedure TFmPesqCEP.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPesqCEP.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmPesqCEP.RecriatabelaDBTest1Click(Sender: TObject);
begin
  MySQL.Connect;
  MySQL.Execute('DROP TABLE IF EXISTS Loc2004');
  MySQL.Execute(
    'CREATE TABLE Loc2004 ('  +
    'Sequen Integer(11), '   +
    'SubSeq Integer(11), '   +
    'Munici varchar(100),'   +
    'Distri varchar(100),'   +
    'Iguais     tinyint(1) NOT NULL DEFAULT 0' +
    ') TYPE=MyISAM');

  //

  MySQL.Execute('DROP TABLE IF EXISTS Loc2008');
  MySQL.Execute(
    'CREATE TABLE Loc2008 ('  +
    'Sequen Integer(11), '   +
    'SubSeq Integer(11), '   +
    'Munici varchar(100),'   +
    'Distri varchar(100),'   +
    'Iguais     tinyint(1) NOT NULL DEFAULT 0' +
    ') TYPE=MyISAM');

  //

  Application.MessageBox('A tabela foi riada com sucesso!',
  'Mensagem', MB_OK+MB_ICONINFORMATION);
end;

end.
