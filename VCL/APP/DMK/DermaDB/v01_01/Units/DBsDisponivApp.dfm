object FmDBsDisponivApp: TFmDBsDisponivApp
  Left = 339
  Top = 185
  Caption = 'XXX-XXXXX-999 :: ????????????? ??????????? ????????????'
  ClientHeight = 719
  ClientWidth = 997
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 997
    Height = 605
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 624
    ExplicitHeight = 347
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 997
      Height = 605
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 624
      ExplicitHeight = 347
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 997
        Height = 605
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 624
        ExplicitHeight = 347
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 993
          Height = 50
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Label1: TLabel
            Left = 92
            Top = 8
            Width = 31
            Height = 13
            Caption = 'Nome:'
          end
          object Label4: TLabel
            Left = 8
            Top = 8
            Width = 36
            Height = 13
            Caption = 'C'#243'digo:'
          end
          object EdNome: TdmkEdit
            Left = 92
            Top = 24
            Width = 313
            Height = 21
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdNomeChange
          end
          object EdCodigo: TdmkEdit
            Left = 8
            Top = 24
            Width = 80
            Height = 21
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdCodigoChange
          end
        end
        object dmkDBGridZTO1: TdmkDBGridZTO
          Left = 2
          Top = 65
          Width = 403
          Height = 538
          Align = alLeft
          DataSource = DsDBsDisponivApp
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          OnDblClick = dmkDBGridZTO1DblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'C'#243'digo'
              Width = 76
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Width = 281
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 605
    Width = 997
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    ExplicitTop = 347
    ExplicitWidth = 624
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 993
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 620
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 649
    Width = 997
    Height = 70
    Align = alBottom
    TabOrder = 2
    ExplicitTop = 391
    ExplicitWidth = 624
    object PnSaiDesis: TPanel
      Left = 851
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 478
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = 'I&gnora'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 849
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 476
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Seleciona'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtInsUpd: TBitBtn
        Tag = 10
        Left = 136
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Insere'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtInsUpdClick
      end
    end
  end
  object Qr_: TABSQuery
    CurrentVersion = '7.20 '
    DatabaseName = 'MEMORY'
    InMemory = True
    ReadOnly = False
    Left = 136
    Top = 252
    object Qr_versao: TFloatField
      FieldName = 'versao'
    end
    object Qr_CliCodi: TWideStringField
      FieldName = 'CliCodi'
      Size = 60
    end
    object Qr_CliNome: TWideStringField
      FieldName = 'CliNome'
      Size = 255
    end
    object Qr_NomeUTF: TWideStringField
      FieldName = 'NomeUTF'
      Size = 255
    end
    object Qr_Cidade: TWideStringField
      FieldName = 'Cidade'
      Size = 255
    end
    object Qr_UF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object Qr_Pais: TWideStringField
      FieldName = 'Pais'
      Size = 255
    end
    object Qr_Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object Ds_: TDataSource
    DataSet = Qr_
    Left = 136
    Top = 300
  end
  object QrDBsDisponivApp: TABSQuery
    CurrentVersion = '7.20 '
    DatabaseName = 'MEMORY'
    InMemory = True
    ReadOnly = False
    Left = 40
    Top = 108
    object QrDBsDisponivAppCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDBsDisponivAppNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrDBsDisponivAppNomeUTF: TWideStringField
      FieldName = 'NomeUTF'
      Size = 100
    end
  end
  object DsDBsDisponivApp: TDataSource
    DataSet = QrDBsDisponivApp
    Left = 40
    Top = 156
  end
end
