unit PesqComp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Menus, UnDmkProcFunc, UnDmkEnums;

type
  TAcaoVarredura = (avPesquisa, avExclui);
  TFmPesqComp = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    Panel5: TPanel;
    Panel6: TPanel;
    ListBox1: TListBox;
    Panel7: TPanel;
    GradeA: TStringGrid;
    Panel20: TPanel;
    Label1: TLabel;
    Label3: TLabel;
    SpeedButton1: TSpeedButton;
    Label17: TLabel;
    SpeedButton9: TSpeedButton;
    EdNomeCompo: TdmkEdit;
    CkSub1: TCheckBox;
    CkCaseSensitive: TCheckBox;
    EdDiretorio: TdmkEdit;
    Panel21: TPanel;
    StaticText3: TStaticText;
    MeExtensoes: TMemo;
    EdArqArqs: TdmkEdit;
    Panel19: TPanel;
    StaticText2: TStaticText;
    MeExcl: TMemo;
    ListBox2: TListBox;
    Panel8: TPanel;
    Label4: TLabel;
    GradeB: TStringGrid;
    RE1: TRichEdit;
    ListBox3: TListBox;
    PMArquivos: TPopupMenu;
    Dodiretrioselecionado1: TMenuItem;
    Listadearquivos1: TMenuItem;
    BitBtn1: TBitBtn;
    Label18: TLabel;
    EdNovoTexto: TdmkEdit;
    BtArquivos: TBitBtn;
    BtCancela: TBitBtn;
    BtPesquisa: TBitBtn;
    BtSubstitui: TBitBtn;
    Label2: TLabel;
    EdPropCompo: TdmkEdit;
    Label5: TLabel;
    EdValrCompo: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton9Click(Sender: TObject);
    procedure Dodiretrioselecionado1Click(Sender: TObject);
    procedure Listadearquivos1Click(Sender: TObject);
    procedure GradeADblClick(Sender: TObject);
    procedure GradeBDblClick(Sender: TObject);
    procedure BtArquivosClick(Sender: TObject);
    procedure BtCancelaClick(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtSubstituiClick(Sender: TObject);
  private
    { Private declarations }
    FScanAborted: Boolean;
    //
    procedure VarreArquivos(AcaoVarredura: TAcaoVarredura);
  public
    { Public declarations }
  end;

  var
  FmPesqComp: TFmPesqComp;

implementation

uses UnMyObjects, Module, Principal, Textos;

{$R *.DFM}

procedure TFmPesqComp.BtArquivosClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMArquivos, BtArquivos);
end;

procedure TFmPesqComp.BtCancelaClick(Sender: TObject);
begin
  FScanAborted := True;
end;

procedure TFmPesqComp.BtPesquisaClick(Sender: TObject);
begin
  VarreArquivos(avPesquisa);
end;

procedure TFmPesqComp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPesqComp.BtSubstituiClick(Sender: TObject);
begin
  VarreArquivos(avExclui);
end;

procedure TFmPesqComp.Dodiretrioselecionado1Click(Sender: TObject);
var
  cam: String;
  I: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    Cam := Application.Title + 'PesqComp\';
    Geral.WriteAppKey('NomeCompo', Cam, EdNomeCompo.Text, ktString, HKEY_LOCAL_MACHINE);
    Geral.WriteAppKey('PropCompo', Cam, EdPropCompo.Text, ktString, HKEY_LOCAL_MACHINE);
    Geral.WriteAppKey('Diretorio', Cam, EdDiretorio.Text, ktString, HKEY_LOCAL_MACHINE);
    Geral.WriteAppKey('ListaArquivos', Cam, EdArqArqs.Text, ktString, HKEY_LOCAL_MACHINE);
    ListBox1.Clear;
    MyObjects.LimpaGrade(GradeA, 1, 1, True);
    for I := 0 to MeExtensoes.Lines.Count -1 do
    begin
      dmkPF.GetAllFiles(CkSub1.Checked, EdDiretorio.Text+'\'+
      MeExtensoes.Lines[I], ListBox1, False);
    end;
    BtPesquisa.Enabled := True;
    BtSubstitui.Enabled := True;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPesqComp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  ImgTipo.SQLType := stPsq;
end;

procedure TFmPesqComp.FormCreate(Sender: TObject);
var
  Cam: String;
begin
  //PageControl1.ActivePageIndex := 0;
  //
  Cam := Application.Title + 'PesqComp\';
  EdNomeCompo.Text := Geral.ReadAppKey('NomeCompo', Cam, ktString, '', HKEY_LOCAL_MACHINE);
  EdPropCompo.Text := Geral.ReadAppKey('PropCompo', Cam, ktString, '', HKEY_LOCAL_MACHINE);
  EdDiretorio.Text := Geral.ReadAppKey('Diretorio', Cam, ktString, '', HKEY_LOCAL_MACHINE);
  EdArqArqs.Text := Geral.ReadAppKey('ListaArquivos', Cam, ktString, '', HKEY_LOCAL_MACHINE);
end;

procedure TFmPesqComp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPesqComp.GradeADblClick(Sender: TObject);
var
  MemoryStream: TMemoryStream;
begin
  Screen.Cursor := crHourGlass;
  try
    RE1.Lines.LoadFromFile(GradeA.Cells[2, GradeB.Row]);
    Application.CreateForm(TFmTextos, FmTextos);
    FmTextos.FAtributesSize := 10;
    FmTextos.FAtributesName := 'Courier New';
    FmTextos.FSaveSit    := 0;
    FmTextos.FRichEdit   := RE1;
    MemoryStream := TMemoryStream.Create;
    RE1.Lines.SaveToStream(MemoryStream);
    MemoryStream.Position := 0;
    FmTextos.Editor.Lines.LoadFromStream(MemoryStream);
    MemoryStream.Free;
    //
    FmTextos.Splitter1.Visible := False;
    FmTextos.LbItensMD.Visible := False;
    {
    FmTextos.FTipoLoc := vpLoc;
    FmTextos.FvpCol   := 0;
    FmTextos.FvpRow   := StrToInt(GradeB.Cells[2, GradeB.Row]);
    }
    //
  finally
    Screen.Cursor := crDefault;
  end;
  FmTextos.ShowModal;
  FmTextos.Destroy;
end;

procedure TFmPesqComp.GradeBDblClick(Sender: TObject);
var
  MemoryStream: TMemoryStream;
begin
  Screen.Cursor := crHourGlass;
  try
    RE1.Lines.LoadFromFile(GradeB.Cells[1, GradeB.Row]);
    Application.CreateForm(TFmTextos, FmTextos);
    FmTextos.FAtributesSize := 10;
    FmTextos.FAtributesName := 'Courier New';
    FmTextos.FSaveSit    := 0;
    FmTextos.FRichEdit   := RE1;
    MemoryStream := TMemoryStream.Create;
    RE1.Lines.SaveToStream(MemoryStream);
    MemoryStream.Position := 0;
    FmTextos.Editor.Lines.LoadFromStream(MemoryStream);
    MemoryStream.Free;
    //
    FmTextos.Splitter1.Visible := False;
    FmTextos.LbItensMD.Visible := False;
    FmTextos.FTipoLoc := vpLoc;
    FmTextos.FvpCol   := 0;
    FmTextos.FvpRow   := StrToInt(GradeB.Cells[2, GradeB.Row]);
    //
  finally
    Screen.Cursor := crDefault;
  end;
  FmTextos.ShowModal;
  FmTextos.Destroy;
end;

procedure TFmPesqComp.Listadearquivos1Click(Sender: TObject);
  procedure LeArquivo(Arquivo: String);
  var
    Arq: TextFile;
    Nome, S, X, Y: String;
    posI, posF, I, N, J: Integer;
    posE: Boolean;
  begin
    PosE := False;
    ListBox3.Items.Clear;
    posI := 1;
    //posF := 0;
    S := EdArqArqs.Text;
    for I := 1 to Length(S) do
    begin
      if S[I] = '\' then
      begin
        Nome := Copy(S, posI, I-posI);
        ListBox3.Items.Append(Nome);
        posI := I + 1;
      end;
    end;
    //
    AssignFile(Arq, Arquivo);
    if FileExists(Arquivo) then
    begin
      Reset(Arq);
      N := 0;
      while not Eof(Arq) do
      begin
        Readln(Arq, S);
        posI := 0;
        posF := 0;
        for I := 1 to Length(S) do
        begin
          if S[I] = Char(39) then
          begin
            if posI = 0 then
              posI := I
            else
            if posF = 0 then
              posF := I;
          end;
          if (posI > 0) and (posF >0) then
            N := N + 1;
          if (S[I] = ';') and (N > 0) then
          begin
            posE := True;
          end;
        end;
        if (posI > 0) and (posF >0) then
        begin
          Nome := Copy(S, PosI + 1, PosF-PosI-1);
          ListBox2.Items.Add(Nome);
          //
          J := 0;
          N := pos('..\', Nome);
          while N = 1 do
          begin
            Nome := Copy(Nome, 4);
            N := pos('..\', Nome);
            J := J + 1;
          end;
          X := '';
          for I := 0 to ListBox3.Items.Count - J -1 do
          begin
            X := X + ListBox3.Items[I] + '\';
          end;
          for I := 0 to MeExtensoes.Lines.Count - 1 do
          begin
            Nome := dmkPF.MudaExtensaoDeArquivo(Nome, Copy(MeExtensoes.Lines[I], 3));
            Y := X + Nome;
            if FileExists(Y) then
              ListBox1.Items.Add(Y);
          end;
        end;
        if posE then
          Break;
      end;
    end;
    CloseFile(Arq);
  end;

var
  cam: String;
  //I: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    Cam := Application.Title + 'Pesquisa\';
    Geral.WriteAppKey('ListaArquivos', Cam, EdArqArqs.Text, ktString, HKEY_LOCAL_MACHINE);
    ListBox1.Clear;
    MyObjects.LimpaGrade(GradeA, 1, 1, True);
    //
    LeArquivo(EdArqArqs.Text);
    //
    BtPesquisa.Enabled := True;
    BtSubstitui.Enabled := True;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPesqComp.SpeedButton1Click(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDiretorio);
end;

procedure TFmPesqComp.SpeedButton9Click(Sender: TObject);
var
  IniDir, Arquivo: String;
begin
  IniDir := ExtractFileDir(Application.ExeName);
  Arquivo := '';
  if MyObjects.FileOpenDialog(Self, IniDir, Arquivo,
  'Selecione o arquivo', '', [], Arquivo) then
    EdArqArqs.Text := Arquivo;
end;

procedure TFmPesqComp.VarreArquivos(AcaoVarredura: TAcaoVarredura);
var
  i, n, k, ic, kc, p, nMudou, iVC: Integer;
  Lista: TStringList;
  TxtLin, PropCompo, ValrCompo, NomeCompo, Compare, TxtValr: String;
  EmPesquisa, UsaValrCompo, OkFind: Boolean;
begin
  Screen.Cursor := crHourGlass;
  FScanAborted := False;
  EmPesquisa := False;
  PropCompo := EdPropCompo.Text;
  ValrCompo := EdValrCompo.Text;
  UsaValrCompo := Trim(ValrCompo) <> EmptyStr;
  NomeCompo := EdNomeCompo.Text;
  Lista := TStringList.Create;
  MyObjects.LimpaGrade(GradeA, 1, 1, True);
  MyObjects.LimpaGrade(GradeB, 1, 1, True);
  ic := 0;
  kc := 0;
  for i := 0 to ListBox1.Items.Count -1 do
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, ListBox1.Items[i]);
    //
    if FScanAborted then
    begin
      Screen.Cursor := crDefault;
      Exit;
    end;
    nMudou := 0;
    n := FmPrincipal.ScanFile(ListBox1.Items[i], EdNomeCompo.Text, CkCaseSensitive.Checked);
    if n > 0 then
    begin
      ic := ic + 1;
      GradeA.RowCount := ic + 1;
      GradeA.Cells[0,ic] := IntToStr(ic);
      GradeA.Cells[1,ic] := IntToStr(n);
      GradeA.Cells[2,ic] := ListBox1.Items[i];
      //ListBox2.Items.Add(ListBox1.Items[i]+' Caracter ->'+IntToStr(n));
      Lista.Clear;
      Lista.LoadFromFile(ListBox1.Items[i]);
      //
      for k := 0 to Lista.Count -1 do
      begin
        TxtLin := Trim(Lista.Strings[k]);
        if TxtLin = 'end' then
          EmPesquisa := False
        else begin
          if pos('object ', TxtLin) = 1 then
            EmPesquisa := False;
          //
          p := pos(': ' + NomeCompo, TxtLin);
          if p > 0 then
          begin
            p := pos(NomeCompo, TxtLin);
            Compare := Copy(TxtLin, p);
            EmPesquisa := NomeCompo = Compare;
          end;
        end;
        if EmPesquisa then
        begin
          p := pos(PropCompo + ' = ', TxtLin);
          if p = 1 then
          begin
            case AcaoVarredura of
              avPesquisa:
              begin
                OkFind := False;
                if UsaValrCompo then
                begin
                  iVC := Length(PropCompo + ' = ');
                  TxtValr := Trim(Copy(TxtLin, iVC));
                  OkFind := Lowercase(TxtValr) = LowerCase(ValrCompo);
                end else
                  OkFind := True;
                if OkFind then
                begin
                  kc := kc + 1;
                  GradeB.RowCount := kc+1;
                  GradeB.Cells[0,kc] := IntToStr(kc);
                  GradeB.Cells[1,kc] := ListBox1.Items[i];
                  GradeB.Cells[2,kc] := IntToStr(k);
                  GradeB.Cells[3,kc] := Lista.Strings[k];
                  GradeB.Cells[4,kc] := '';//Lista.Strings[k];
                  //ListBox3.Items.Add(ListBox1.Items[i]+ '  ['+IntToStr(k)+']  '+
                    //Lista.Strings[k]);
                end;
              end;
              avExclui:
              begin
                Lista.Strings[k] := '';
                nMudou := nMudou + 1;
              end;
            end;
          end;
        end;
        Application.ProcessMessages;
      end;
      //
      if (AcaoVarredura in ([avExclui])) and (nMudou > 0) then
      begin
        MLAGeral.SaveTextToFile(ListBox1.Items[i], Lista.Text);
      end;
    end;
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Rastreamento finalizado!');
  Screen.Cursor := crDefault;
end;

end.
