unit ModMemTab;

interface

uses
  System.SysUtils, System.Classes, Vcl.Forms, Vcl.Controls,
  System.Win.Registry, Winapi.Windows,
  ABSMain, Data.DB,
  dmkGeral, UnInternalConsts;

type
  TDmModMemTab = class(TDataModule)
    TbDBsDACab: TABSTable;
    TbDBsDACabCodigo: TIntegerField;
    DsDBsDACab: TDataSource;
    QrUpd: TABSQuery;
    TbDBsDAIts: TABSTable;
    DsDBsDAIts: TDataSource;
    TbDBsDAItsCodigo: TIntegerField;
    TbDBsDAItsControle: TIntegerField;
    TbDBsDACabNome: TStringField;
    TbDBsDACabNomeUTF: TStringField;
    TbDBsDAItsNome: TStringField;
    TbDBsDAItsNomeUTF: TStringField;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function  AbreTabelasLifeTime(): Boolean;
    //
    function  DBsDACab_Abre(TbCab: TABSTable): Boolean;
    function  DBsDACab_CarregaRegistro(Codigo: Integer;
              Nome: String): Boolean;
    function  DBsDACab_Exclui(Codigo: Integer): Boolean;
    function  DBsDACab_Localiza(Codigo: Integer): Boolean;
    //
    function  DBsDAIts_Abre(TbIts: TABSTable): Boolean;
    function  DBsDAIts_CarregaRegistro(Codigo, Controle: Integer;
              Nome: String): Boolean;
    function  DBsDAIts_Exclui(Controle: Integer): Boolean;
    function  DBsDAIts_Localiza(Controle: Integer): Boolean;
    //
  end;

var
  DmModMemTab: TDmModMemTab;

implementation

uses UnApp_CreateMT, UMemModule, UnMyObjects;
{%CLASSGROUP 'System.Classes.TPersistent'}


{$R *.dfm}

{ TDmModMemTab }

function TDmModMemTab.AbreTabelasLifeTime: Boolean;
begin
{{
  TbConfigs.Close;
  Create_TFC.RecriaMemTable(ntcConfigs, CO_Configs_Arquivo, QrUpd, True, False);
  UMemMod.AbreABSTable1(TbConfigs);
  AbreTabelaConfigs(TbConfigs, True);
  //
  //
  //
  //
  //
  //
  TbPrdPrcCab.Close;
  Create_TFC.RecriaMemTable(ntcPrdPrcCab, 'prdprccab', QrUpd, True, False);
  UMemMod.AbreABSTable1(TbPrdPrcCab);
  //
  TbPrdPrcIts.Close;
  Create_TFC.RecriaMemTable(ntcPrdPrcIts, 'prdprcits', QrUpd, True, False);
  UMemMod.AbreABSTable1(TbPrdPrcIts);
  //
  //
  //
  TbCliPrcCab.Close;
  Create_TFC.RecriaMemTable(ntcCliPrcCab, 'cliprccab', QrUpd, True, False);
  UMemMod.AbreABSTable1(TbCliPrcCab);
  //
  TbCliPrcIts.Close;
  Create_TFC.RecriaMemTable(ntcCliPrcIts, 'cliprcits', QrUpd, True, False);
  UMemMod.AbreABSTable1(TbCliPrcIts);
  //
  //
  //
}
  DBsDACab_Abre(TbDBsDACab);
  DBsDAIts_Abre(TbDBsDAIts);
{{
  AbreTabelaPrdLstCad();
  AbreListaDePrecos(sListaPadrao, TbPrdPrcCab, TbPrdPrcIts);
}
end;

procedure TDmModMemTab.DataModuleCreate(Sender: TObject);
begin
  TMeuDB := 'DermaDB';
  /////////////////////////////////////////
  //VAR_GOTOMySQLDBNAME := 'MEMORY'; //CO_DB_APP_MAIN_DB;
  //
  VAR_SQLx := TStringList.Create;
  VAR_SQL1 := TStringList.Create;
  VAR_SQL2 := TStringList.Create;
  VAR_SQLa := TStringList.Create;
  //
  MAR_SQLx := TStringList.Create;
  MAR_SQL1 := TStringList.Create;
  MAR_SQL2 := TStringList.Create;
  MAR_SQLa := TStringList.Create;
  //
  AbreTabelasLifeTime();
end;

function TDmModMemTab.DBsDACab_Abre(TbCab: TABSTable): Boolean;
(*
  function ListaRegistrosReg(Tabela: String): TStringList;
  var
    Reg: TRegistry;
    Caminho: String;
  begin
    Reg := TRegistry.Create(KEY_READ);
    Reg.RootKey := HKEY_CURRENT_USER;
    try
      Caminho := CaminhoTabela('DBsDACab');
      Reg.OpenKeyReadOnly('Software\' + Caminho);
      Result := TStringList.Create;
      Reg.GetValueNames(Result);
    finally
      Reg.Free;
    end;
  end;
*)
var
{
  Caminho: String;
var
  QtdItens,
  MostraForm: Boolean;
/////////
var
  Tabela: String;
}
  Lista: TStrings;
  I: Integer;
  Codigo: Integer;
  Nome: String;
  Reg: TRegistry;
  Caminho: String;
begin
  Result := False;
  //MostraForm := False;
  //
  if TbDBsDACab.Exists then
    TbDBsDACab.DeleteTable;
  App_CreateMT.RecriaMemTable(ntcDBsDACab, 'DBsDACab', QrUpd, True, False);
  UMemMod.AbreABSTable1(TbDBsDACab);
  //
  Reg := TRegistry.Create(KEY_READ);
  Reg.RootKey := HKEY_CURRENT_USER;
  try
    Caminho := MyObjects.DBsDA_CaminhoTabela('DBsDACab');
    Reg.OpenKeyReadOnly('Software\' + Caminho);
    Lista := TStringList.Create;
    try
      Reg.GetValueNames(Lista);
      for I := 0 to Lista.Count - 1 do
      begin
        Codigo := Geral.IMV(Lista[I]);
        Nome := Reg.ReadString(Lista[I]);
        DBsDACab_CarregaRegistro(Codigo, Nome);
      end;
    finally
      Lista.Free;
    end;
  finally
    Reg.Free;
  end;
(*
  Lista := ListaRegistrosReg('DBsDACab');
  try
    for I := 0 to Lista.Count - 1 do
    begin
      Codigo := Geral.IMV(Lista[I]);
      Nome := r.ReadString(Lista[I]);
      DBsDACab_CarregaRegistro(Codigo, Nome);
    end;
  finally
    Lista.Free;
  end;
*)

///////////////////////////// XML //////////////////////////////////////////////
{
  Caminho := GMod.NomeCompletoArquivo(CO_DBsDACab_Arquivo, CO_DBsDACab_NSubDir, CO_DBsDACab_NomeExt);
  if not FileExists(Caminho) then
  begin
    if Geral.MB_Pergunta('O arquivo de cadastros de clientes n�o foi localizado!' +
    sLineBreak + 'Deseja cri�-lo vazio?') = ID_YES then
    begin
      //XML_Vazio := '';
      DBsDACab_XML_DOC := TXMLDocument.Create(nil);
      DBsDACab_XML_DOC.Active := False;
      DBsDACab_XML_DOC.FileName := '';
      DBsDACab_XML_STR := GetDBsDACab(DBsDACab_XML_DOC);
      DBsDACab_XML_DOC.Version := '1.0';
      DBsDACab_XML_DOC.Encoding := 'UTF-8';
      //
      Gmod.SalvaXML_De_Tabela(CO_DBsDACab_Arquivo, CO_DBsDACab_NSubDir,
        CO_DBsDACab_NomeExt, DBsDACab_XML_DOC.XML.Text(*XMLVazio*));
      DBsDACab_XML_DOC := nil;
      //
      MostraForm := True;
    end;
  end;
  if not FileExists(Caminho) then
    Exit;
  //
(* Criando o Documento XML ... *)
  DBsDACab_XML_DOC := TXMLDocument.Create(nil);
  DBsDACab_XML_DOC.FileName := Caminho;
  DBsDACab_XML_DOC.Active := True;
  DBsDACab_XML_STR := GetDBsDACab(DBsDACab_XML_DOC);
  DBsDACab_XML_DOC.Active := True;
  //
  Versao    := DBsDACab_XML_STR.Versao;
  for I := 0 to DBsDACab_XML_STR.Clientes.Count - 1 do
  begin
    DBsDACab := DBsDACab_XML_STR.Clientes.DBsDACabCab[I];
    //
    CliCodi   := DBsDACab.Item.CliCodi;
    CliNome   := DBsDACab.Item.CliNome;
    Cidade    := DBsDACab.Item.Cidade;
    UF        := DBsDACab.Item.UF;
    Pais      := DBsDACab.Item.Pais;

    TbCab.Insert;
    TbCab.FieldByName('versao').AsFloat     := Geral.DMV_Dot(Versao);
    TbCab.FieldByName('CliCodi').AsString   := CliCodi;
    TbCab.FieldByName('CliNome').AsString   := CliNome;
    TbCab.FieldByName('NomeUTF').AsString   := Geral.MyUTF(CliNome);
    TbCab.FieldByName('Cidade').AsString    := Cidade;
    TbCab.FieldByName('UF').AsString        := UF;
    TbCab.FieldByName('Pais').AsString      := Pais;
    //
    TbCab.FieldByName('Ativo').AsInteger    := Ativo;
    GMod.PostTable(TbCab, CO_DBsDACab_Arquivo, 'AbreDBsDACab');
  end;
  //
  DBsDACab_XML_DOC := nil;
  //
}
end;

function TDmModMemTab.DBsDACab_CarregaRegistro(Codigo: Integer;
  Nome: String): Boolean;
var
  NomeUTF: String;
  //SQLType: TSQLType;
begin
  Screen.Cursor := crHourGlass;
  try
    if DBsDACab_Localiza(Codigo) then
      //SQLType := stUpd
      TbDBsDACab.Edit
    else
      //SQLType := stIns;
      TbDBsDACab.Insert;
    //
    try
      TbDBsDACabCodigo.Value     := Codigo;
      TbDBsDACabNome.Value       := Nome;
      //
      NomeUTF := Geral.MyUTF(Nome);
      TbDBsDACabNomeUTF.Value    := NomeUTF;
      //
      TbDBsDACab.Post;
      Result := True;
    except
      on E: Exception do
      begin
        Geral.MB_Erro('Erro ao cadastrar "DBsDACab":' + sLineBreak +
        Geral.FF0(Codigo) + ' - ' + Nome + sLineBreak + sLineBreak + E.Message);
      end;
    end;
    //
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TDmModMemTab.DBsDACab_Exclui(Codigo: Integer): Boolean;
begin
  Result := False;
  if DBsDACab_Localiza(Codigo) then
  begin
    TbDBsDACab.Delete;
    Result := not DBsDACab_Localiza(Codigo);
  end;
end;

function TDmModMemTab.DBsDACab_Localiza(Codigo: Integer): Boolean;
begin
  Result := TbDBsDACab.Locate('Codigo', Codigo, []);
end;

function TDmModMemTab.DBsDAIts_Abre(TbIts: TABSTable): Boolean;
var
  Lista: TStrings;
  I: Integer;
  Codigo, Controle: Integer;
  Nome: String;
  Reg: TRegistry;
  Caminho, CamFlds: String;
begin
  Result := False;
  //
  if TbDBsDAIts.Exists then
    TbDBsDAIts.DeleteTable;
  App_CreateMT.RecriaMemTable(ntcDBsDAIts, 'DBsDAIts', QrUpd, True, False);
  UMemMod.AbreABSTable1(TbDBsDAIts);
  //
  Reg := TRegistry.Create(KEY_READ);
  Reg.RootKey := HKEY_CURRENT_USER;
  try
    Caminho := MyObjects.DBsDA_CaminhoTabela('DBsDAIts');
    Reg.OpenKeyReadOnly('Software\' + Caminho);
    Lista := TStringList.Create;
    try
      Reg.GetValueNames(Lista);
      for I := 0 to Lista.Count - 1 do
      begin
        Controle := Geral.IMV(Lista[I]);
        Nome := Reg.ReadString(Lista[I]);
        //
        //Codigo := Geral.ReadAppKeyCU()
        CamFlds := MyObjects.DBsDA_CaminhoRegistro(Caminho, Controle);
        Codigo  := Geral.ReadAppKeyCU('Codigo', CamFlds, ktInteger, 0);
        //
        DBsDAIts_CarregaRegistro(Codigo, Controle, Nome);
      end;
    finally
      Lista.Free;
    end;
  finally
    Reg.Free;
  end;
end;

function TDmModMemTab.DBsDAIts_CarregaRegistro(Codigo, Controle: Integer;
  Nome: String): Boolean;
var
  NomeUTF: String;
  //SQLType: TSQLType;
begin
  Screen.Cursor := crHourGlass;
  try
    if DBsDAIts_Localiza(Controle) then
      //SQLType := stUpd
      TbDBsDAIts.Edit
    else
      //SQLType := stIns;
      TbDBsDAIts.Insert;
    //
    try
      TbDBsDAItsControle.Value   := Controle;
      TbDBsDAItsCodigo.Value     := Codigo;
      TbDBsDAItsNome.Value       := Nome;
      //
      NomeUTF := Geral.MyUTF(Nome);
      TbDBsDAItsNomeUTF.Value    := NomeUTF;
      //
      TbDBsDAIts.Post;
      Result := True;
    except
      on E: Exception do
      begin
        Geral.MB_Erro('Erro ao cadastrar "DBsDAIts":' + sLineBreak +
        Geral.FF0(Codigo) + ' - ' + Nome + sLineBreak + sLineBreak + E.Message);
      end;
    end;
    //
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TDmModMemTab.DBsDAIts_Exclui(Controle: Integer): Boolean;
begin
  Result := False;
  if DBsDAIts_Localiza(Controle) then
  begin
    TbDBsDAIts.Delete;
    Result := not DBsDAIts_Localiza(Controle);
  end;
end;

function TDmModMemTab.DBsDAIts_Localiza(Controle: Integer): Boolean;
begin
  Result := TbDBsDAIts.Locate('Controle', Controle, []);
end;

end.
