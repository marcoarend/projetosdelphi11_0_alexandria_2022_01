object FmPesquisa: TFmPesquisa
  Left = 242
  Top = 176
  Caption = '???-?????-999 :: Pesquisa de Texto em Arquivos'
  ClientHeight = 661
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 48
    Width = 1008
    Height = 553
    ActivePage = TabSheet5
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Pesquisa'
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 1000
        Height = 525
        Align = alClient
        TabOrder = 0
        object Panel2: TPanel
          Left = 1
          Top = 1
          Width = 998
          Height = 276
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object ListBox1: TListBox
            Left = 769
            Top = 0
            Width = 108
            Height = 276
            Align = alClient
            ItemHeight = 13
            TabOrder = 0
          end
          object Panel4: TPanel
            Left = 0
            Top = 0
            Width = 584
            Height = 276
            Align = alLeft
            TabOrder = 1
            object GradeA: TStringGrid
              Left = 1
              Top = 193
              Width = 582
              Height = 82
              Align = alClient
              ColCount = 3
              DefaultColWidth = 40
              DefaultRowHeight = 17
              RowCount = 2
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
              TabOrder = 0
              OnDblClick = GradeADblClick
              ColWidths = (
                40
                63
                453)
            end
            object Panel20: TPanel
              Left = 1
              Top = 1
              Width = 582
              Height = 132
              Align = alTop
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 1
              object Label1: TLabel
                Left = 8
                Top = 4
                Width = 30
                Height = 13
                Caption = 'Texto:'
              end
              object Label3: TLabel
                Left = 8
                Top = 44
                Width = 42
                Height = 13
                Caption = 'Diret'#243'rio:'
              end
              object SpeedButton1: TSpeedButton
                Left = 440
                Top = 60
                Width = 23
                Height = 22
                Caption = '...'
                OnClick = SpeedButton1Click
              end
              object Label17: TLabel
                Left = 8
                Top = 84
                Width = 166
                Height = 13
                Caption = 'Arquivo contendo lista de arquivos:'
              end
              object SpeedButton9: TSpeedButton
                Left = 440
                Top = 100
                Width = 23
                Height = 22
                Caption = '...'
                OnClick = SpeedButton9Click
              end
              object EdTxtPre: TdmkEdit
                Left = 8
                Top = 20
                Width = 333
                Height = 21
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnChange = EdTxtPreChange
              end
              object CkSub1: TCheckBox
                Left = 344
                Top = 38
                Width = 117
                Height = 17
                Caption = 'Incluir subdiret'#243'rios.'
                Checked = True
                State = cbChecked
                TabOrder = 1
              end
              object CkCaseSensitive: TCheckBox
                Left = 344
                Top = 22
                Width = 97
                Height = 17
                Caption = 'Case sensitive.'
                Checked = True
                State = cbChecked
                TabOrder = 2
              end
              object EdDiretorio: TdmkEdit
                Left = 8
                Top = 60
                Width = 429
                Height = 21
                TabOrder = 3
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object Panel21: TPanel
                Left = 464
                Top = 0
                Width = 118
                Height = 132
                Align = alRight
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 4
                object StaticText3: TStaticText
                  Left = 0
                  Top = 0
                  Width = 118
                  Height = 17
                  Align = alTop
                  Alignment = taCenter
                  Caption = 'Extens'#245'es de arquivos:'
                  TabOrder = 0
                end
                object MeExtensoes: TMemo
                  Left = 0
                  Top = 17
                  Width = 118
                  Height = 115
                  Align = alClient
                  Lines.Strings = (
                    '*.pas'
                    '*.dfm')
                  TabOrder = 1
                  WantReturns = False
                end
              end
              object EdArqArqs: TdmkEdit
                Left = 8
                Top = 100
                Width = 429
                Height = 21
                TabOrder = 5
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object BitBtn6: TBitBtn
                Left = 380
                Top = 0
                Width = 75
                Height = 25
                Caption = 'EXCEL'
                TabOrder = 6
                OnClick = BitBtn6Click
              end
            end
            object Panel34: TPanel
              Left = 1
              Top = 133
              Width = 582
              Height = 60
              Align = alTop
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 2
              object Label22: TLabel
                Left = 136
                Top = 0
                Width = 28
                Height = 13
                Caption = 'ID Ini:'
              end
              object Label23: TLabel
                Left = 220
                Top = 0
                Width = 33
                Height = 13
                Caption = 'ID Fim:'
              end
              object Label24: TLabel
                Left = 304
                Top = 0
                Width = 46
                Height = 13
                Caption = 'Casas ID:'
              end
              object Label25: TLabel
                Left = 360
                Top = 0
                Width = 78
                Height = 13
                Caption = 'Texto posfixado:'
              end
              object EdPAIni: TdmkEdit
                Left = 136
                Top = 16
                Width = 80
                Height = 21
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = '0'
                ValWarn = False
              end
              object EdPAFim: TdmkEdit
                Left = 220
                Top = 16
                Width = 80
                Height = 21
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = '0'
                ValWarn = False
              end
              object EdPACasas: TdmkEdit
                Left = 304
                Top = 16
                Width = 53
                Height = 21
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMax = '100'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = '0'
                ValWarn = False
              end
              object EdPATxtPos: TdmkEdit
                Left = 360
                Top = 16
                Width = 180
                Height = 21
                TabOrder = 3
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnChange = EdTxtPreChange
              end
              object CkPAUsa: TCheckBox
                Left = 8
                Top = 16
                Width = 125
                Height = 17
                Caption = 'Pesquisa avan'#231'ada:'
                TabOrder = 4
              end
              object CkPARemoveBrancos: TCheckBox
                Left = 136
                Top = 40
                Width = 201
                Height = 17
                Caption = 'Remover espac'#231'os em branco.'
                TabOrder = 5
              end
            end
          end
          object Panel19: TPanel
            Left = 584
            Top = 0
            Width = 185
            Height = 276
            Align = alLeft
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 2
            object StaticText2: TStaticText
              Left = 0
              Top = 0
              Width = 185
              Height = 17
              Align = alTop
              Alignment = taCenter
              Caption = 'Excess'#245'es:'
              TabOrder = 0
            end
            object MeExcl: TMemo
              Left = 0
              Top = 17
              Width = 185
              Height = 259
              Align = alLeft
              TabOrder = 1
              WantReturns = False
            end
          end
          object ListBox2: TListBox
            Left = 877
            Top = 0
            Width = 121
            Height = 276
            Align = alRight
            ItemHeight = 13
            TabOrder = 3
          end
        end
        object Panel3: TPanel
          Left = 1
          Top = 277
          Width = 998
          Height = 20
          Align = alTop
          TabOrder = 1
          object Label4: TLabel
            Left = 4
            Top = 4
            Width = 9
            Height = 13
            Caption = '...'
          end
        end
        object GradeB: TStringGrid
          Left = 1
          Top = 297
          Width = 607
          Height = 83
          Align = alClient
          DefaultColWidth = 40
          DefaultRowHeight = 17
          RowCount = 2
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goEditing]
          TabOrder = 2
          OnDblClick = GradeBDblClick
          ColWidths = (
            40
            140
            329
            69
            526)
        end
        object RE1: TRichEdit
          Left = 584
          Top = 200
          Width = 185
          Height = 89
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
          Visible = False
        end
        object ListBox3: TListBox
          Left = 878
          Top = 297
          Width = 121
          Height = 83
          Align = alRight
          ItemHeight = 13
          TabOrder = 4
        end
        object GBRodaPe: TGroupBox
          Left = 1
          Top = 380
          Width = 998
          Height = 144
          Align = alBottom
          TabOrder = 5
          object Panel7: TPanel
            Left = 2
            Top = 15
            Width = 994
            Height = 127
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object Panel36: TPanel
              Left = 0
              Top = 0
              Width = 197
              Height = 127
              Align = alLeft
              TabOrder = 0
              object BtArquivos: TBitBtn
                Tag = 28
                Left = 4
                Top = 4
                Width = 90
                Height = 40
                Caption = '&Arquivos'
                NumGlyphs = 2
                TabOrder = 0
                OnClick = BtArquivosClick
              end
              object BtCancela: TBitBtn
                Tag = 15
                Left = 100
                Top = 4
                Width = 90
                Height = 40
                Caption = '&Cancela'
                NumGlyphs = 2
                TabOrder = 1
                OnClick = BtCancelaClick
              end
              object BtPesquisa: TBitBtn
                Tag = 22
                Left = 4
                Top = 50
                Width = 90
                Height = 40
                Caption = '&Pesquisa'
                Enabled = False
                NumGlyphs = 2
                TabOrder = 2
                OnClick = BtPesquisaClick
              end
              object BtSubstitui: TBitBtn
                Tag = 11
                Left = 100
                Top = 50
                Width = 90
                Height = 40
                Caption = 'Substitui'
                Enabled = False
                NumGlyphs = 2
                TabOrder = 3
                OnClick = BtSubstituiClick
              end
              object BitBtn7: TBitBtn
                Left = 16
                Top = 100
                Width = 75
                Height = 25
                Caption = 'BitBtn7'
                TabOrder = 4
                OnClick = BitBtn7Click
              end
            end
            object PCSubstituir: TPageControl
              Left = 197
              Top = 0
              Width = 797
              Height = 127
              ActivePage = TabSheet8
              Align = alClient
              TabOrder = 1
              object TabSheet7: TTabSheet
                Caption = 'Substitui'#231#227'o simples'
                object Panel37: TPanel
                  Left = 0
                  Top = 0
                  Width = 789
                  Height = 99
                  Align = alClient
                  TabOrder = 0
                  object Label19: TLabel
                    Left = 2
                    Top = 0
                    Width = 240
                    Height = 13
                    Caption = 'Texto a ser substituido na linha do texto localizado:'
                  end
                  object Label18: TLabel
                    Left = 290
                    Top = 0
                    Width = 78
                    Height = 13
                    Caption = 'Texto substituto:'
                  end
                  object EdOldTexto: TdmkEdit
                    Left = 2
                    Top = 16
                    Width = 284
                    Height = 21
                    TabOrder = 0
                    FormatType = dmktfString
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '  '
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = '  '
                    ValWarn = False
                  end
                  object EdNewTexto: TdmkEdit
                    Left = 290
                    Top = 16
                    Width = 284
                    Height = 21
                    TabOrder = 1
                    FormatType = dmktfString
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '  UnDmkDAC_PF.AbreQuery('
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = '  UnDmkDAC_PF.AbreQuery('
                    ValWarn = False
                  end
                end
              end
              object TabSheet8: TTabSheet
                Caption = 'Substitui'#231#227'o avan'#231'ada'
                ImageIndex = 1
                object Label28: TLabel
                  Left = 4
                  Top = 4
                  Width = 49
                  Height = 13
                  Caption = 'Texto fixo:'
                  Enabled = False
                end
                object Label29: TLabel
                  Left = 212
                  Top = 4
                  Width = 279
                  Height = 13
                  Caption = 'Texto ANTES a ser substituido na linha do texto localizado:'
                end
                object Label30: TLabel
                  Left = 212
                  Top = 44
                  Width = 283
                  Height = 13
                  Caption = 'Texto DEPOIS a ser substituido na linha do texto localizado:'
                end
                object Label31: TLabel
                  Left = 500
                  Top = 4
                  Width = 117
                  Height = 13
                  Caption = 'Texto substituto ANTES:'
                end
                object Label32: TLabel
                  Left = 500
                  Top = 44
                  Width = 121
                  Height = 13
                  Caption = 'Texto substituto DEPOIS:'
                end
                object dmkEdit4: TdmkEdit
                  Left = 4
                  Top = 20
                  Width = 200
                  Height = 21
                  Enabled = False
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdOldTextoPre: TdmkEdit
                  Left = 212
                  Top = 20
                  Width = 284
                  Height = 21
                  TabOrder = 1
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                  OnRedefinido = EdOldTextoPreRedefinido
                end
                object EdOldTextoPos: TdmkEdit
                  Left = 212
                  Top = 60
                  Width = 284
                  Height = 21
                  TabOrder = 2
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '.Open;'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = '.Open;'
                  ValWarn = False
                end
                object EdNewTextoPre: TdmkEdit
                  Left = 500
                  Top = 20
                  Width = 284
                  Height = 21
                  TabOrder = 3
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = 'UnDmkDAC_PF.AbreQuery('
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 'UnDmkDAC_PF.AbreQuery('
                  ValWarn = False
                end
                object EdNewTextoPos: TdmkEdit
                  Left = 500
                  Top = 60
                  Width = 284
                  Height = 21
                  TabOrder = 4
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = ', Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; '
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ', Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; '
                  ValWarn = False
                end
                object CkEspacosPre: TCheckBox
                  Left = 4
                  Top = 47
                  Width = 193
                  Height = 17
                  Caption = 'Manter espa'#231'os do in'#237'cio da frase.'
                  Checked = True
                  State = cbChecked
                  TabOrder = 5
                end
              end
            end
          end
        end
        object GradeC: TStringGrid
          Left = 608
          Top = 297
          Width = 270
          Height = 83
          Align = alRight
          ColCount = 3
          DefaultColWidth = 40
          DefaultRowHeight = 17
          RowCount = 2
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goEditing]
          TabOrder = 6
          OnDblClick = GradeBDblClick
          ColWidths = (
            40
            140
            56)
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Backup'
      ImageIndex = 1
      object Panel5: TPanel
        Left = 0
        Top = 0
        Width = 1000
        Height = 477
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object ListBox4: TListBox
          Left = 0
          Top = 410
          Width = 1000
          Height = 67
          Align = alBottom
          ItemHeight = 13
          TabOrder = 0
        end
        object ListBox5: TListBox
          Left = 630
          Top = 0
          Width = 370
          Height = 410
          Align = alClient
          ItemHeight = 13
          TabOrder = 1
        end
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 445
          Height = 410
          Align = alLeft
          TabOrder = 2
          object Label7: TLabel
            Left = 4
            Top = 4
            Width = 42
            Height = 13
            Caption = 'Diret'#243'rio:'
          end
          object SpeedButton2: TSpeedButton
            Left = 416
            Top = 20
            Width = 23
            Height = 22
            Caption = '...'
            OnClick = SpeedButton2Click
          end
          object Label8: TLabel
            Left = 4
            Top = 64
            Width = 39
            Height = 13
            Caption = 'DataStr:'
          end
          object Label01: TLabel
            Left = 428
            Top = 84
            Width = 184
            Height = 16
            Caption = ' Extens'#245'es a omitir (Ex.:  .txt): '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
          end
          object EdDiretorio2: TdmkEdit
            Left = 4
            Top = 20
            Width = 409
            Height = 21
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdDataStr: TdmkEdit
            Left = 4
            Top = 80
            Width = 100
            Height = 21
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object CkSub2: TCheckBox
            Left = 4
            Top = 44
            Width = 113
            Height = 17
            Caption = 'Incluir subdiret'#243'rios.'
            TabOrder = 2
          end
        end
        object MeExt: TMemo
          Left = 445
          Top = 0
          Width = 185
          Height = 410
          Align = alLeft
          Lines.Strings = (
            '.~*'
            '.dcu'
            '.exe'
            '.map')
          TabOrder = 3
        end
      end
      object Panel8: TPanel
        Left = 0
        Top = 477
        Width = 1000
        Height = 48
        Align = alBottom
        TabOrder = 1
        object BitBtn1: TBitBtn
          Left = 24
          Top = 4
          Width = 90
          Height = 40
          Caption = '&Arquivos'
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BitBtn1Click
        end
        object BtBackup: TBitBtn
          Left = 118
          Top = 4
          Width = 90
          Height = 40
          Caption = '&Backup'
          Enabled = False
          NumGlyphs = 2
          TabOrder = 1
          OnClick = BtBackupClick
        end
        object Progress: TProgressBar
          Left = 212
          Top = 20
          Width = 565
          Height = 17
          TabOrder = 2
        end
        object Panel10: TPanel
          Left = 888
          Top = 1
          Width = 111
          Height = 46
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 3
          object BitBtn2: TBitBtn
            Tag = 13
            Left = 6
            Top = 3
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Sai da janela atual'
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Compara arquivos'
      ImageIndex = 2
      object Panel11: TPanel
        Left = 0
        Top = 0
        Width = 1000
        Height = 97
        Align = alTop
        TabOrder = 0
        object Label5: TLabel
          Left = 8
          Top = 8
          Width = 48
          Height = 13
          Caption = 'Arquivo 1:'
        end
        object SpeedButton3: TSpeedButton
          Left = 8
          Top = 24
          Width = 23
          Height = 22
          Caption = '...'
          OnClick = SpeedButton3Click
        end
        object SpeedButton4: TSpeedButton
          Left = 8
          Top = 64
          Width = 23
          Height = 22
          Caption = '...'
          OnClick = SpeedButton4Click
        end
        object Label6: TLabel
          Left = 8
          Top = 48
          Width = 48
          Height = 13
          Caption = 'Arquivo 2:'
        end
        object EdArq1: TdmkEdit
          Left = 36
          Top = 24
          Width = 860
          Height = 21
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdArq2: TdmkEdit
          Left = 36
          Top = 64
          Width = 860
          Height = 21
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object BitBtn3: TBitBtn
          Tag = 14
          Left = 907
          Top = 48
          Width = 90
          Height = 40
          Caption = '&Compara'
          NumGlyphs = 2
          TabOrder = 2
          OnClick = BitBtn3Click
        end
        object BitBtn4: TBitBtn
          Tag = 14
          Left = 907
          Top = 4
          Width = 90
          Height = 40
          Caption = 'Carre&ga'
          NumGlyphs = 2
          TabOrder = 3
          OnClick = BitBtn4Click
        end
      end
      object Panel12: TPanel
        Left = 0
        Top = 469
        Width = 1000
        Height = 56
        Align = alBottom
        TabOrder = 1
        object Label9: TLabel
          Left = 8
          Top = 8
          Width = 29
          Height = 13
          Caption = 'Linha:'
        end
        object Label10: TLabel
          Left = 92
          Top = 8
          Width = 48
          Height = 13
          Caption = 'Arquivo 1:'
        end
        object Label12: TLabel
          Left = 548
          Top = 8
          Width = 48
          Height = 13
          Caption = 'Arquivo 2:'
        end
        object EdLinha: TEdit
          Left = 8
          Top = 24
          Width = 77
          Height = 21
          TabOrder = 0
          Text = '0'
        end
        object EdLinha1: TEdit
          Left = 92
          Top = 24
          Width = 450
          Height = 21
          TabOrder = 1
        end
        object EdLinha2: TEdit
          Left = 548
          Top = 24
          Width = 450
          Height = 21
          TabOrder = 2
        end
      end
      object Panel13: TPanel
        Left = 0
        Top = 97
        Width = 1000
        Height = 372
        Align = alClient
        Caption = 'Panel13'
        TabOrder = 2
        object Memo2: TMemo
          Left = 507
          Top = 1
          Width = 506
          Height = 370
          Align = alLeft
          TabOrder = 0
          WordWrap = False
        end
        object Memo1: TMemo
          Left = 1
          Top = 1
          Width = 506
          Height = 370
          Align = alLeft
          TabOrder = 1
          WordWrap = False
        end
      end
    end
    object TabSheet4: TTabSheet
      Caption = '  Uppercase/lowercase'
      ImageIndex = 3
      object Panel14: TPanel
        Left = 0
        Top = 0
        Width = 1000
        Height = 525
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Panel15: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 21
          Align = alTop
          Caption = 'Utilize a aba "Pesquisa" para configura'#231#227'o!'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object Panel16: TPanel
          Left = 0
          Top = 477
          Width = 1000
          Height = 48
          Align = alBottom
          TabOrder = 1
          object BtPesquisa2: TBitBtn
            Tag = 15
            Left = 215
            Top = 4
            Width = 90
            Height = 40
            Caption = '&Pesquisa'
            NumGlyphs = 2
            TabOrder = 0
            OnClick = BtPesquisa2Click
          end
          object Panel17: TPanel
            Left = 888
            Top = 1
            Width = 111
            Height = 46
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 1
            object BitBtn8: TBitBtn
              Tag = 13
              Left = 6
              Top = 3
              Width = 90
              Height = 40
              Cursor = crHandPoint
              Hint = 'Sai da janela atual'
              Caption = '&Sa'#237'da'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtSaidaClick
            end
          end
        end
        object MeArq: TMemo
          Left = 109
          Top = 70
          Width = 300
          Height = 252
          Align = alLeft
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          WantReturns = False
          WordWrap = False
        end
        object PnAviso: TPanel
          Left = 0
          Top = 21
          Width = 1000
          Height = 32
          Align = alTop
          Caption = '...'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 3
        end
        object MeCopy: TMemo
          Left = 409
          Top = 70
          Width = 300
          Height = 252
          Align = alLeft
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
          TabOrder = 4
          WantReturns = False
          WordWrap = False
        end
        object MeTxt: TMemo
          Left = 709
          Top = 70
          Width = 291
          Height = 252
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
          TabOrder = 5
          WantReturns = False
          WordWrap = False
        end
        object PB2: TProgressBar
          Left = 0
          Top = 53
          Width = 1000
          Height = 17
          Align = alTop
          TabOrder = 6
        end
        object Panel18: TPanel
          Left = 0
          Top = 70
          Width = 109
          Height = 252
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 7
          object StaticText1: TStaticText
            Left = 0
            Top = 158
            Width = 109
            Height = 17
            Align = alBottom
            Alignment = taCenter
            Caption = 'Textos a mostrar'
            TabOrder = 0
            ExplicitWidth = 82
          end
          object StaticText4: TStaticText
            Left = 0
            Top = 0
            Width = 109
            Height = 17
            Align = alTop
            Alignment = taCenter
            Caption = 'Textos anteriores'
            TabOrder = 1
            ExplicitWidth = 85
          end
          object MeAnteriores: TMemo
            Left = 0
            Top = 17
            Width = 109
            Height = 141
            Align = alClient
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Courier New'
            Font.Style = []
            Lines.Strings = (
              'FROM '
              'LEFT JOIN '
              'INSERT INTO '
              'UPDATE ')
            ParentFont = False
            TabOrder = 2
            WantReturns = False
            WordWrap = False
          end
          object MeMostrar: TMemo
            Left = 0
            Top = 175
            Width = 109
            Height = 77
            Align = alBottom
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Courier New'
            Font.Style = []
            Lines.Strings = (
              ',')
            ParentFont = False
            TabOrder = 3
            WantReturns = False
            WordWrap = False
          end
        end
        object MeShow: TMemo
          Left = 0
          Top = 322
          Width = 1000
          Height = 155
          Align = alBottom
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
          TabOrder = 8
          WantReturns = False
          WordWrap = False
        end
      end
    end
    object TabSheet5: TTabSheet
      Caption = ' Localizar Arquivo '
      ImageIndex = 4
      object Panel22: TPanel
        Left = 0
        Top = 0
        Width = 1000
        Height = 205
        Align = alTop
        TabOrder = 0
        object SGDirs: TStringGrid
          Left = 1
          Top = 41
          Width = 998
          Height = 163
          Align = alClient
          ColCount = 2
          DefaultColWidth = 24
          RowCount = 10
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColMoving, goEditing]
          TabOrder = 0
          OnDblClick = SGDirsDblClick
          ColWidths = (
            24
            934)
        end
        object Panel35: TPanel
          Left = 1
          Top = 1
          Width = 998
          Height = 40
          Align = alTop
          Caption = 'Panel35'
          TabOrder = 1
          object Label2: TLabel
            Left = 0
            Top = 0
            Width = 84
            Height = 13
            Caption = 'Nome do arquivo:'
          end
          object Label13: TLabel
            Left = 144
            Top = 0
            Width = 47
            Height = 13
            Caption = 'Extens'#227'o:'
          end
          object Label11: TLabel
            Left = 200
            Top = 4
            Width = 42
            Height = 13
            Caption = 'Diret'#243'rio:'
          end
          object SpeedButton5: TSpeedButton
            Left = 628
            Top = 16
            Width = 23
            Height = 22
            Caption = '...'
            OnClick = SpeedButton5Click
          end
          object EdArqNome4: TdmkEdit
            Left = 0
            Top = 16
            Width = 141
            Height = 21
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '*'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = '*'
            ValWarn = False
          end
          object EdArqExte4: TdmkEdit
            Left = 144
            Top = 16
            Width = 49
            Height = 21
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = 'pas'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 'pas'
            ValWarn = False
          end
          object EdArqDir4: TdmkEdit
            Left = 196
            Top = 16
            Width = 429
            Height = 21
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = 'C:\_Compilers\projetosdelphi11_0_alexandria_2022_01'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 'C:\_Compilers\projetosdelphi11_0_alexandria_2022_01'
            ValWarn = False
          end
          object CkSubDir4: TCheckBox
            Left = 668
            Top = 20
            Width = 117
            Height = 17
            Caption = 'Incluir subdiret'#243'rios.'
            Checked = True
            State = cbChecked
            TabOrder = 3
          end
          object Salvar: TBitBtn
            Left = 924
            Top = 12
            Width = 75
            Height = 25
            Caption = 'Salvar'
            TabOrder = 4
            OnClick = SalvarClick
          end
        end
      end
      object Panel23: TPanel
        Left = 0
        Top = 446
        Width = 1000
        Height = 79
        Align = alBottom
        TabOrder = 1
        object Label14: TLabel
          Left = 104
          Top = 8
          Width = 90
          Height = 13
          Caption = 'Caminho completo:'
        end
        object Label15: TLabel
          Left = 104
          Top = 32
          Width = 42
          Height = 13
          Caption = 'Diret'#243'rio:'
        end
        object Label16: TLabel
          Left = 104
          Top = 56
          Width = 39
          Height = 13
          Caption = 'Arquivo:'
        end
        object SpeedButton6: TSpeedButton
          Left = 976
          Top = 4
          Width = 21
          Height = 21
          Caption = 'c'
          OnClick = SpeedButton6Click
        end
        object SpeedButton7: TSpeedButton
          Left = 976
          Top = 28
          Width = 21
          Height = 21
          Caption = 'c'
          OnClick = SpeedButton7Click
        end
        object SpeedButton8: TSpeedButton
          Left = 976
          Top = 52
          Width = 21
          Height = 21
          Caption = 'c'
          OnClick = SpeedButton8Click
        end
        object BitBtn5: TBitBtn
          Tag = 15
          Left = 7
          Top = 4
          Width = 90
          Height = 40
          Caption = '&Pesquisa'
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BitBtn5Click
        end
        object Ed4_Cam: TdmkEdit
          Left = 204
          Top = 4
          Width = 769
          Height = 21
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object Ed4_Dir: TdmkEdit
          Left = 204
          Top = 28
          Width = 769
          Height = 21
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object Ed4_Arq: TdmkEdit
          Left = 204
          Top = 52
          Width = 769
          Height = 21
          TabOrder = 3
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
      end
      object ListBox04: TListBox
        Left = 0
        Top = 205
        Width = 1000
        Height = 241
        Align = alClient
        ItemHeight = 13
        TabOrder = 2
        OnClick = ListBox04Click
        OnDblClick = ListBox04DblClick
      end
    end
    object TabSheet6: TTabSheet
      Caption = 'Caracteres inv'#225'lidos'
      ImageIndex = 5
      object Splitter1: TSplitter
        Left = 307
        Top = 29
        Width = 5
        Height = 496
        Align = alRight
        ExplicitLeft = 820
        ExplicitHeight = 510
      end
      object Panel24: TPanel
        Left = 0
        Top = 0
        Width = 1000
        Height = 29
        Align = alTop
        TabOrder = 0
        object Label20: TLabel
          Left = 8
          Top = 8
          Width = 39
          Height = 13
          Caption = 'Arquivo:'
        end
        object SbArqScan: TSpeedButton
          Left = 976
          Top = 4
          Width = 21
          Height = 21
          Caption = 'ok'
          OnClick = SbArqScanClick
        end
        object EdArqScan: TdmkEdit
          Left = 52
          Top = 4
          Width = 921
          Height = 21
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
      end
      object Panel25: TPanel
        Left = 0
        Top = 29
        Width = 307
        Height = 496
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 1
        object MeCI_Txt: TMemo
          Left = 0
          Top = 17
          Width = 307
          Height = 479
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          Lines.Strings = (
            '')
          ParentFont = False
          TabOrder = 0
          WordWrap = False
        end
        object Panel28: TPanel
          Left = 0
          Top = 0
          Width = 307
          Height = 17
          Align = alTop
          Caption = 'Texto Corrigido:'
          TabOrder = 1
        end
      end
      object Panel26: TPanel
        Left = 764
        Top = 29
        Width = 236
        Height = 496
        Align = alRight
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 2
        object Splitter2: TSplitter
          Left = 0
          Top = 17
          Width = 5
          Height = 479
          ExplicitLeft = 820
          ExplicitHeight = 493
        end
        object Panel27: TPanel
          Left = 0
          Top = 0
          Width = 236
          Height = 17
          Align = alTop
          Caption = 'Caracteres n'#227'o removidos:'
          TabOrder = 0
        end
        object MeCI_Nao: TMemo
          Left = 5
          Top = 17
          Width = 231
          Height = 479
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          Lines.Strings = (
            '')
          ParentFont = False
          TabOrder = 1
        end
      end
      object Panel29: TPanel
        Left = 312
        Top = 29
        Width = 216
        Height = 496
        Align = alRight
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 3
        object Panel30: TPanel
          Left = 0
          Top = 0
          Width = 216
          Height = 17
          Align = alTop
          Caption = 'Caracteres removidos:'
          TabOrder = 0
        end
        object MeCI_Sim: TMemo
          Left = 0
          Top = 17
          Width = 216
          Height = 479
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          Lines.Strings = (
            '')
          ParentFont = False
          TabOrder = 1
        end
      end
      object Panel31: TPanel
        Left = 528
        Top = 29
        Width = 236
        Height = 496
        Align = alRight
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 4
        object Splitter3: TSplitter
          Left = 0
          Top = 17
          Width = 5
          Height = 479
          ExplicitLeft = 820
          ExplicitHeight = 493
        end
        object Panel32: TPanel
          Left = 0
          Top = 0
          Width = 236
          Height = 17
          Align = alTop
          Caption = 'Caracteres substitu'#237'dos:'
          TabOrder = 0
        end
        object MeCI_Cor: TMemo
          Left = 5
          Top = 17
          Width = 231
          Height = 479
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          Lines.Strings = (
            '')
          ParentFont = False
          TabOrder = 1
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 592
        Height = 32
        Caption = '???-?????-999 :: Pesquisa de Texto em Arquivos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 592
        Height = 32
        Caption = '???-?????-999 :: Pesquisa de Texto em Arquivos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 592
        Height = 32
        Caption = '???-?????-999 :: Pesquisa de Texto em Arquivos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 601
    Width = 1008
    Height = 60
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel33: TPanel
      Left = 2
      Top = 15
      Width = 893
      Height = 43
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 26
        Width = 893
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
    object Panel9: TPanel
      Left = 895
      Top = 15
      Width = 111
      Height = 43
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 14
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
  end
  object OpenDialog1: TOpenDialog
    Left = 172
    Top = 388
  end
  object PMArquivos: TPopupMenu
    Left = 92
    Top = 388
    object Dodiretrioselecionado1: TMenuItem
      Caption = '&Diret'#243'rio selecionado'
      OnClick = Dodiretrioselecionado1Click
    end
    object Listadearquivos1: TMenuItem
      Caption = '&Lista de arquivos do arquivo selecionado'
      OnClick = Listadearquivos1Click
    end
  end
end
