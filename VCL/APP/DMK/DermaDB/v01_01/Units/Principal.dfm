object FmPrincipal: TFmPrincipal
  Left = 203
  Top = 173
  Caption = 'Administrador do MySQL'
  ClientHeight = 783
  ClientWidth = 1239
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1239
    Height = 660
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Splitter2: TSplitter
      Left = 137
      Top = 0
      Height = 660
      ExplicitLeft = 101
      ExplicitHeight = 469
    end
    object PageControl2: TPageControl
      Left = 140
      Top = 0
      Width = 1099
      Height = 660
      ActivePage = TabSheet2
      Align = alClient
      TabOrder = 0
      object TabSheet2: TTabSheet
        Caption = 'Database'
        object Splitter3: TSplitter
          Left = 122
          Top = 0
          Height = 632
          ExplicitLeft = 124
          ExplicitTop = 20
        end
        object Panel4: TPanel
          Left = 125
          Top = 0
          Width = 966
          Height = 632
          Align = alClient
          TabOrder = 0
          object PageControl3: TPageControl
            Left = 1
            Top = 1
            Width = 964
            Height = 630
            ActivePage = TabSheet6
            Align = alClient
            TabOrder = 0
            object TabSheet5: TTabSheet
              Caption = 'Tabelas'
              object Splitter4: TSplitter
                Left = 0
                Top = 148
                Width = 956
                Height = 3
                Cursor = crVSplit
                Align = alTop
                ExplicitWidth = 800
              end
              object GridCampos: TDBGrid
                Left = 0
                Top = 0
                Width = 956
                Height = 148
                Align = alTop
                DataSource = DsCampos
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                OnDrawColumnCell = GridCamposDrawColumnCell
                OnKeyDown = GridCamposKeyDown
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Field'
                    Title.Caption = 'Campo'
                    Width = 140
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Type'
                    Title.Caption = 'Tipo'
                    Width = 140
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Null'
                    Title.Caption = 'Nulo'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Key'
                    Title.Caption = 'Chave'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Default'
                    Title.Caption = 'Padr'#227'o'
                    Width = 140
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Extra'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Comment'
                    Title.Caption = 'Coment'#225'rios'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Collation'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Privileges'
                    Visible = True
                  end>
              end
              object Panel6: TPanel
                Left = 0
                Top = 151
                Width = 956
                Height = 451
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 1
                object Panel8: TPanel
                  Left = 0
                  Top = 0
                  Width = 956
                  Height = 101
                  Align = alTop
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 0
                  object Panel20: TPanel
                    Left = 0
                    Top = 0
                    Width = 305
                    Height = 101
                    Align = alLeft
                    TabOrder = 0
                    object Label13: TLabel
                      Left = 4
                      Top = 80
                      Width = 188
                      Height = 13
                      Caption = 'Listar resultado quando tiver menos que'
                    end
                    object Label14: TLabel
                      Left = 256
                      Top = 80
                      Width = 42
                      Height = 13
                      Caption = 'registros.'
                    end
                    object BtRegistros: TBitBtn
                      Left = 4
                      Top = 4
                      Width = 100
                      Height = 40
                      Caption = '&Registros'
                      NumGlyphs = 2
                      TabOrder = 0
                      OnClick = BtRegistrosClick
                    end
                    object CkLimitar: TCheckBox
                      Left = 108
                      Top = 4
                      Width = 85
                      Height = 17
                      Caption = 'Limitar (0, -1)'
                      Checked = True
                      State = cbChecked
                      TabOrder = 1
                    end
                    object Ed1: TdmkEdit
                      Left = 108
                      Top = 22
                      Width = 41
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 2
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                    end
                    object Ed2: TdmkEdit
                      Left = 152
                      Top = 22
                      Width = 41
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 3
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '50'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 50
                      ValWarn = False
                    end
                    object BtConfReg: TBitBtn
                      Left = 200
                      Top = 4
                      Width = 100
                      Height = 40
                      Caption = '&Confirme'
                      Enabled = False
                      NumGlyphs = 2
                      TabOrder = 4
                      OnClick = BtConfRegClick
                    end
                    object Button6: TButton
                      Left = 8
                      Top = 48
                      Width = 205
                      Height = 25
                      Caption = 'Listar quantidade de registros por campo'
                      TabOrder = 5
                      OnClick = Button6Click
                    end
                    object EdMenosQ: TdmkEdit
                      Left = 196
                      Top = 76
                      Width = 57
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 6
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '100'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 100
                      ValWarn = False
                    end
                    object Button7: TButton
                      Left = 220
                      Top = 48
                      Width = 81
                      Height = 25
                      Caption = 'Largura=?'
                      TabOrder = 7
                      OnClick = Button7Click
                    end
                  end
                  object Panel21: TPanel
                    Left = 597
                    Top = 0
                    Width = 359
                    Height = 101
                    Align = alClient
                    Caption = 'Panel21'
                    TabOrder = 1
                    object MeFiltro: TMemo
                      Left = 37
                      Top = 1
                      Width = 321
                      Height = 99
                      Align = alClient
                      TabOrder = 0
                    end
                    object Panel24: TPanel
                      Left = 1
                      Top = 1
                      Width = 36
                      Height = 99
                      Align = alLeft
                      Caption = 'Filtro:'
                      TabOrder = 1
                    end
                  end
                  object GroupBox1: TGroupBox
                    Left = 305
                    Top = 0
                    Width = 224
                    Height = 101
                    Align = alLeft
                    Caption = ' Consertar texto:'
                    TabOrder = 2
                    object Panel23: TPanel
                      Left = 2
                      Top = 15
                      Width = 115
                      Height = 84
                      Align = alLeft
                      TabOrder = 0
                      object Label10: TLabel
                        Left = 68
                        Top = 44
                        Width = 41
                        Height = 13
                        Caption = 'Campos:'
                      end
                      object SpeedButton1: TSpeedButton
                        Left = 8
                        Top = 44
                        Width = 23
                        Height = 22
                        Visible = False
                        OnClick = SpeedButton1Click
                      end
                      object SpeedButton2: TSpeedButton
                        Left = 32
                        Top = 44
                        Width = 23
                        Height = 22
                        Visible = False
                        OnClick = SpeedButton2Click
                      end
                      object BtConsertarTxt: TBitBtn
                        Left = 7
                        Top = 4
                        Width = 100
                        Height = 40
                        Caption = 'C&onsertar'
                        NumGlyphs = 2
                        TabOrder = 0
                        OnClick = BtConsertarTxtClick
                      end
                    end
                    object ListBox3: TListBox
                      Left = 117
                      Top = 15
                      Width = 105
                      Height = 84
                      Align = alClient
                      ItemHeight = 13
                      Items.Strings = (
                        'Nome'
                        'RazaoSocial'
                        'Fantasia'
                        'Apelido')
                      TabOrder = 1
                    end
                  end
                  object Panel39: TPanel
                    Left = 529
                    Top = 0
                    Width = 68
                    Height = 101
                    Align = alLeft
                    BevelOuter = bvNone
                    TabOrder = 3
                    object Button9: TButton
                      Left = 5
                      Top = 12
                      Width = 60
                      Height = 81
                      Caption = 'Txt do  Registro'
                      TabOrder = 0
                      WordWrap = True
                      OnClick = Button9Click
                    end
                  end
                end
                object GridRegistros: TDBGrid
                  Left = 0
                  Top = 101
                  Width = 956
                  Height = 206
                  Align = alClient
                  DataSource = DsRegistros
                  TabOrder = 1
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                end
                object Panel22: TPanel
                  Left = 0
                  Top = 363
                  Width = 956
                  Height = 88
                  Align = alBottom
                  Caption = 'Panel22'
                  TabOrder = 2
                  object Memo4: TMemo
                    Left = 1
                    Top = 1
                    Width = 424
                    Height = 86
                    Align = alLeft
                    Lines.Strings = (
                      'Memo4')
                    TabOrder = 0
                  end
                  object Memo5: TMemo
                    Left = 425
                    Top = 1
                    Width = 530
                    Height = 86
                    Align = alClient
                    Lines.Strings = (
                      'Memo5')
                    TabOrder = 1
                  end
                end
                object Memo8: TMemo
                  Left = 0
                  Top = 307
                  Width = 956
                  Height = 56
                  Align = alBottom
                  Lines.Strings = (
                    'Memo8')
                  TabOrder = 3
                end
              end
            end
            object TabSheet6: TTabSheet
              Caption = 'SQL'
              ImageIndex = 1
              object Splitter7: TSplitter
                Left = 121
                Top = 0
                Height = 602
                ExplicitLeft = 117
                ExplicitHeight = 345
              end
              object Panel2: TPanel
                Left = 124
                Top = 0
                Width = 832
                Height = 602
                Align = alClient
                TabOrder = 0
                object Panel3: TPanel
                  Left = 1
                  Top = 1
                  Width = 830
                  Height = 100
                  Align = alTop
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 0
                  object BtConfirma1: TBitBtn
                    Left = 4
                    Top = 4
                    Width = 100
                    Height = 40
                    Caption = 'Executa F5'#13#10'Abre F6'
                    Enabled = False
                    NumGlyphs = 2
                    TabOrder = 0
                  end
                  object BtAbrir: TBitBtn
                    Left = 596
                    Top = 4
                    Width = 100
                    Height = 40
                    Caption = '&Abrir outro'
                    NumGlyphs = 2
                    TabOrder = 1
                    OnClick = BtAbrirClick
                  end
                  object BitBtn7: TBitBtn
                    Left = 108
                    Top = 4
                    Width = 100
                    Height = 40
                    Caption = 'Soma'
                    NumGlyphs = 2
                    TabOrder = 2
                    OnClick = BitBtn7Click
                  end
                  object BitBtn1: TBitBtn
                    Left = 212
                    Top = 4
                    Width = 100
                    Height = 40
                    Caption = 'Salvar SQL'
                    NumGlyphs = 2
                    TabOrder = 3
                    OnClick = BitBtn1Click
                  end
                  object BitBtn2: TBitBtn
                    Left = 316
                    Top = 4
                    Width = 100
                    Height = 40
                    Caption = '&Abrir SQL'
                    NumGlyphs = 2
                    TabOrder = 4
                    OnClick = BitBtn2Click
                  end
                  object Panel17: TPanel
                    Left = 0
                    Top = 48
                    Width = 830
                    Height = 52
                    Align = alBottom
                    ParentBackground = False
                    TabOrder = 5
                    object DBMemo1: TDBMemo
                      Left = 729
                      Top = 1
                      Width = 100
                      Height = 50
                      Align = alClient
                      DataSource = DsExecuta
                      TabOrder = 0
                    end
                    object Panel18: TPanel
                      Left = 1
                      Top = 1
                      Width = 376
                      Height = 50
                      Align = alLeft
                      ParentBackground = False
                      TabOrder = 1
                      object Label8: TLabel
                        Left = 4
                        Top = 5
                        Width = 35
                        Height = 13
                        Caption = 'Prefixo:'
                      end
                      object Label9: TLabel
                        Left = 4
                        Top = 29
                        Width = 32
                        Height = 13
                        Caption = 'Sufixo:'
                      end
                      object EdPrefixo: TEdit
                        Left = 44
                        Top = 2
                        Width = 173
                        Height = 22
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -11
                        Font.Name = 'Courier New'
                        Font.Style = []
                        ParentFont = False
                        TabOrder = 0
                      end
                      object EdSufixo: TEdit
                        Left = 44
                        Top = 26
                        Width = 77
                        Height = 22
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -11
                        Font.Name = 'Courier New'
                        Font.Style = []
                        ParentFont = False
                        TabOrder = 1
                        Text = #39');'
                      end
                      object BtTira: TBitBtn
                        Left = 222
                        Top = 1
                        Width = 73
                        Height = 36
                        Caption = 'Tira'
                        NumGlyphs = 2
                        TabOrder = 2
                        OnClick = BtTiraClick
                      end
                      object BitBtn11: TBitBtn
                        Left = 296
                        Top = 1
                        Width = 73
                        Height = 36
                        Caption = 'P'#245'e'
                        NumGlyphs = 2
                        TabOrder = 3
                        OnClick = BitBtn11Click
                      end
                      object Button11: TButton
                        Left = 132
                        Top = 24
                        Width = 75
                        Height = 25
                        Caption = 'Fonte'
                        TabOrder = 4
                        OnClick = Button11Click
                      end
                    end
                    object GroupBox2: TGroupBox
                      Left = 377
                      Top = 1
                      Width = 352
                      Height = 50
                      Align = alLeft
                      Caption = ' Consertar texto:'
                      TabOrder = 2
                      object Label11: TLabel
                        Left = 4
                        Top = 24
                        Width = 36
                        Height = 13
                        Caption = 'Campo:'
                      end
                      object Label19: TLabel
                        Left = 244
                        Top = 8
                        Width = 86
                        Height = 13
                        Caption = 'Campo do valor >:'
                      end
                      object EdCampoQr: TEdit
                        Left = 44
                        Top = 20
                        Width = 77
                        Height = 21
                        TabOrder = 0
                      end
                      object BitBtn12: TBitBtn
                        Left = 123
                        Top = 8
                        Width = 66
                        Height = 40
                        Caption = 'C&onsertar'
                        Enabled = False
                        NumGlyphs = 2
                        TabOrder = 1
                        OnClick = BitBtn12Click
                      end
                      object BitBtn13: TBitBtn
                        Left = 188
                        Top = 8
                        Width = 53
                        Height = 40
                        Caption = '&Ler'
                        NumGlyphs = 2
                        TabOrder = 2
                        OnClick = BitBtn13Click
                      end
                      object EdFldoDoMemo1: TEdit
                        Left = 244
                        Top = 24
                        Width = 105
                        Height = 21
                        TabOrder = 3
                      end
                    end
                  end
                  object Button8: TButton
                    Left = 420
                    Top = 4
                    Width = 81
                    Height = 40
                    Caption = 'Largura=?'
                    TabOrder = 6
                    OnClick = Button8Click
                  end
                  object BtExporta: TButton
                    Left = 504
                    Top = 4
                    Width = 90
                    Height = 40
                    Caption = 'Exporta Dados'
                    TabOrder = 7
                    OnClick = BtExportaClick
                  end
                  object Button10: TButton
                    Left = 697
                    Top = 4
                    Width = 100
                    Height = 40
                    Caption = 'Txt do  Registro'
                    TabOrder = 8
                    WordWrap = True
                    OnClick = Button10Click
                  end
                end
                object Panel11: TPanel
                  Left = 1
                  Top = 101
                  Width = 830
                  Height = 500
                  Align = alClient
                  BevelOuter = bvNone
                  TabOrder = 1
                  object Splitter1: TSplitter
                    Left = 400
                    Top = 17
                    Height = 379
                    ExplicitLeft = 185
                    ExplicitHeight = 352
                  end
                  object GridResult: TDBGrid
                    Left = 403
                    Top = 17
                    Width = 427
                    Height = 379
                    Align = alClient
                    DataSource = DsExecuta
                    TabOrder = 0
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -11
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    OnColEnter = GridResultColEnter
                  end
                  object STOpen: TStaticText
                    Left = 0
                    Top = 0
                    Width = 830
                    Height = 17
                    Align = alTop
                    Caption = 'STOpen'
                    TabOrder = 1
                  end
                  object Panel28: TPanel
                    Left = 0
                    Top = 17
                    Width = 400
                    Height = 379
                    Align = alLeft
                    TabOrder = 2
                    object Splitter5: TSplitter
                      Left = 1
                      Top = 97
                      Width = 398
                      Height = 3
                      Cursor = crVSplit
                      Align = alTop
                      ExplicitTop = 101
                      ExplicitWidth = 118
                    end
                    object Splitter6: TSplitter
                      Left = 1
                      Top = 233
                      Width = 398
                      Height = 3
                      Cursor = crVSplit
                      Align = alBottom
                      ExplicitTop = 104
                      ExplicitWidth = 115
                    end
                    object Label20: TLabel
                      Left = 1
                      Top = 42
                      Width = 398
                      Height = 13
                      Align = alTop
                      Alignment = taCenter
                      Caption = '/* SQL PREFIXO*/'
                      ExplicitWidth = 91
                    end
                    object Label21: TLabel
                      Left = 1
                      Top = 220
                      Width = 398
                      Height = 13
                      Align = alBottom
                      Alignment = taCenter
                      Caption = '/* SQL SUFIXO*/'
                      ExplicitWidth = 84
                    end
                    object ListBox1: TListBox
                      Left = 1
                      Top = 278
                      Width = 398
                      Height = 100
                      Align = alBottom
                      ItemHeight = 13
                      TabOrder = 0
                      OnDblClick = ListBox1DblClick
                    end
                    object MemoSQL: TMemo
                      Left = 1
                      Top = 100
                      Width = 398
                      Height = 120
                      Align = alClient
                      Font.Charset = ANSI_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Courier New'
                      Font.Style = []
                      ParentFont = False
                      ScrollBars = ssBoth
                      TabOrder = 1
                      WordWrap = False
                      OnKeyDown = MemoSQLKeyDown
                    end
                    object Panel38: TPanel
                      Left = 1
                      Top = 1
                      Width = 398
                      Height = 41
                      Align = alTop
                      TabOrder = 2
                      object Label18: TLabel
                        Left = 180
                        Top = 12
                        Width = 81
                        Height = 13
                        Caption = 'Sufixo de verific.:'
                      end
                      object CkUNION: TCheckBox
                        Left = 8
                        Top = 12
                        Width = 153
                        Height = 17
                        Caption = 'Usar "####" para UNION.'
                        TabOrder = 0
                      end
                      object EdTab: TEdit
                        Left = 264
                        Top = 7
                        Width = 45
                        Height = 22
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -11
                        Font.Name = 'Courier New'
                        Font.Style = []
                        ParentFont = False
                        TabOrder = 1
                        Text = 'lct'
                      end
                    end
                    object MeSQLPre: TMemo
                      Left = 1
                      Top = 55
                      Width = 398
                      Height = 42
                      Align = alTop
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Courier New'
                      Font.Style = []
                      ParentFont = False
                      TabOrder = 3
                    end
                    object MeSQLPos: TMemo
                      Left = 1
                      Top = 236
                      Width = 398
                      Height = 42
                      Align = alBottom
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Courier New'
                      Font.Style = []
                      ParentFont = False
                      TabOrder = 4
                    end
                  end
                  object Memo9: TMemo
                    Left = 0
                    Top = 396
                    Width = 830
                    Height = 104
                    Align = alBottom
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -11
                    Font.Name = 'Courier New'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 3
                    Visible = False
                    WordWrap = False
                  end
                end
              end
              object Panel29: TPanel
                Left = 0
                Top = 0
                Width = 121
                Height = 602
                Align = alLeft
                Caption = 'Panel29'
                TabOrder = 1
                object Panel30: TPanel
                  Left = 1
                  Top = 1
                  Width = 119
                  Height = 41
                  Align = alTop
                  BevelOuter = bvNone
                  TabOrder = 0
                  object Label12: TLabel
                    Left = 4
                    Top = 0
                    Width = 25
                    Height = 13
                    Caption = 'Filtro:'
                  end
                  object EdCampo: TEdit
                    Left = 4
                    Top = 16
                    Width = 113
                    Height = 21
                    TabOrder = 0
                    OnChange = EdCampoChange
                  end
                end
                object GridCampos2: TDBGrid
                  Left = 1
                  Top = 42
                  Width = 119
                  Height = 559
                  Align = alClient
                  DataSource = DsCampos
                  TabOrder = 1
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  OnDrawColumnCell = GridCampos2DrawColumnCell
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'Field'
                      Title.Caption = 'Campo'
                      Visible = True
                    end>
                end
              end
            end
            object TabSheet1: TTabSheet
              Caption = 'SQL Batch'
              ImageIndex = 2
              object Memo2SQL: TMemo
                Left = 0
                Top = 65
                Width = 956
                Height = 471
                Align = alClient
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Courier New'
                Font.Style = []
                ParentFont = False
                TabOrder = 0
                WantReturns = False
                WordWrap = False
                OnKeyDown = MemoSQLKeyDown
              end
              object Panel14: TPanel
                Left = 0
                Top = 0
                Width = 956
                Height = 48
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 1
                object BtConfirma2: TBitBtn
                  Left = 4
                  Top = 4
                  Width = 100
                  Height = 40
                  Caption = '&Executa F5'
                  NumGlyphs = 2
                  TabOrder = 0
                  OnClick = BtConfirma2Click
                end
                object BitBtn6: TBitBtn
                  Left = 560
                  Top = 4
                  Width = 100
                  Height = 40
                  Caption = '&Abrir'
                  NumGlyphs = 2
                  TabOrder = 1
                  OnClick = BitBtn6Click
                end
              end
              object ListBox2: TListBox
                Left = 0
                Top = 536
                Width = 956
                Height = 66
                Align = alBottom
                ItemHeight = 13
                TabOrder = 2
                OnDblClick = ListBox1DblClick
              end
              object StBatch: TStaticText
                Left = 0
                Top = 48
                Width = 956
                Height = 17
                Align = alTop
                Caption = 'STBatch'
                TabOrder = 3
                ExplicitWidth = 46
              end
            end
            object RichText: TTabSheet
              Caption = ' Outros BDs '
              ImageIndex = 7
              object PageControl1: TPageControl
                Left = 0
                Top = 0
                Width = 956
                Height = 602
                ActivePage = TabSheet8
                Align = alClient
                TabOrder = 0
                object TabSheet8: TTabSheet
                  Caption = ' DBF '
                  object PageControl4: TPageControl
                    Left = 0
                    Top = 0
                    Width = 948
                    Height = 574
                    ActivePage = TabSheet3
                    Align = alClient
                    TabOrder = 0
                    object TabSheet3: TTabSheet
                      Caption = 'DBF para MySQL'
                      object Panel31: TPanel
                        Left = 0
                        Top = 0
                        Width = 940
                        Height = 53
                        Align = alTop
                        TabOrder = 0
                        object Label64: TLabel
                          Left = 8
                          Top = 4
                          Width = 63
                          Height = 13
                          Caption = 'Arquivo DBF:'
                        end
                        object SbDir: TSpeedButton
                          Left = 638
                          Top = 20
                          Width = 21
                          Height = 21
                          Caption = '...'
                          OnClick = SbDirClick
                        end
                        object EdArqDBF: TdmkEdit
                          Left = 8
                          Top = 20
                          Width = 629
                          Height = 21
                          MaxLength = 255
                          TabOrder = 0
                          FormatType = dmktfString
                          MskType = fmtNone
                          DecimalSize = 0
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = ''
                          ValWarn = False
                        end
                        object BtEstruturaDBF: TButton
                          Left = 664
                          Top = 16
                          Width = 75
                          Height = 29
                          Caption = 'Cria estrutura'
                          Enabled = False
                          TabOrder = 1
                          OnClick = BtEstruturaDBFClick
                        end
                      end
                      object PageControl5: TPageControl
                        Left = 0
                        Top = 53
                        Width = 940
                        Height = 493
                        ActivePage = TabSheet16
                        Align = alClient
                        TabOrder = 1
                        object TabSheet15: TTabSheet
                          Caption = 'Registros'
                          object DBGrid1: TDBGrid
                            Left = 0
                            Top = 0
                            Width = 932
                            Height = 465
                            Align = alClient
                            DataSource = DsVKDBFNTX
                            TabOrder = 0
                            TitleFont.Charset = DEFAULT_CHARSET
                            TitleFont.Color = clWindowText
                            TitleFont.Height = -11
                            TitleFont.Name = 'MS Sans Serif'
                            TitleFont.Style = []
                          end
                        end
                        object TabSheet16: TTabSheet
                          Caption = 'Estrutura'
                          ImageIndex = 1
                          object MeDBF: TMemo
                            Left = 0
                            Top = 0
                            Width = 932
                            Height = 465
                            Align = alClient
                            Font.Charset = DEFAULT_CHARSET
                            Font.Color = clWindowText
                            Font.Height = -13
                            Font.Name = 'Courier New'
                            Font.Style = []
                            ParentFont = False
                            TabOrder = 0
                          end
                        end
                      end
                    end
                  end
                end
                object TabSheet13: TTabSheet
                  Caption = 'Cria estrutura ABS Table'
                  ImageIndex = 1
                  object Panel7: TPanel
                    Left = 0
                    Top = 0
                    Width = 948
                    Height = 33
                    Align = alTop
                    BevelOuter = bvNone
                    ParentBackground = False
                    TabOrder = 0
                    object BitBtn3: TBitBtn
                      Left = 4
                      Top = 4
                      Width = 75
                      Height = 25
                      Caption = 'OK'
                      TabOrder = 0
                      OnClick = BitBtn3Click
                    end
                  end
                  object MeABS: TMemo
                    Left = 0
                    Top = 33
                    Width = 948
                    Height = 541
                    Align = alClient
                    Font.Charset = ANSI_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -11
                    Font.Name = 'Courier New'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 1
                  end
                end
                object TabSheet14: TTabSheet
                  Caption = 'Cria Estrutura CodeBase'
                  ImageIndex = 2
                  object Panel15: TPanel
                    Left = 0
                    Top = 0
                    Width = 948
                    Height = 181
                    Align = alTop
                    TabOrder = 0
                    object Label2: TLabel
                      Left = 96
                      Top = 4
                      Width = 63
                      Height = 13
                      Caption = 'Todas linhas:'
                    end
                    object Label3: TLabel
                      Left = 316
                      Top = 4
                      Width = 59
                      Height = 13
                      Caption = 'Campos pr'#233':'
                    end
                    object Label4: TLabel
                      Left = 576
                      Top = 4
                      Width = 83
                      Height = 13
                      Caption = 'Todas linhas p'#243's:'
                    end
                    object Label5: TLabel
                      Left = 460
                      Top = 4
                      Width = 61
                      Height = 13
                      Caption = 'Campos p'#243's:'
                    end
                    object Label6: TLabel
                      Left = 96
                      Top = 152
                      Width = 114
                      Height = 13
                      Caption = 'Mudar Nome da tabela: '
                    end
                    object BitBtn5: TBitBtn
                      Left = 8
                      Top = 148
                      Width = 75
                      Height = 25
                      Caption = 'OK'
                      TabOrder = 0
                      OnClick = BitBtn5Click
                    end
                    object EdTPre1: TEdit
                      Left = 96
                      Top = 20
                      Width = 217
                      Height = 21
                      TabOrder = 1
                      OnChange = EdLocTableChange
                    end
                    object EdCPre1: TEdit
                      Left = 316
                      Top = 20
                      Width = 141
                      Height = 21
                      TabOrder = 2
                      OnChange = EdLocTableChange
                    end
                    object EdTPos1: TEdit
                      Left = 576
                      Top = 20
                      Width = 89
                      Height = 21
                      TabOrder = 3
                      Text = #39');'
                      OnChange = EdLocTableChange
                    end
                    object EdCPos1: TEdit
                      Left = 460
                      Top = 20
                      Width = 113
                      Height = 21
                      TabOrder = 4
                      OnChange = EdLocTableChange
                    end
                    object EdTPre2: TEdit
                      Left = 96
                      Top = 44
                      Width = 217
                      Height = 21
                      TabOrder = 5
                      OnChange = EdLocTableChange
                    end
                    object EdCPre2: TEdit
                      Left = 316
                      Top = 44
                      Width = 141
                      Height = 21
                      TabOrder = 6
                      OnChange = EdLocTableChange
                    end
                    object EdCPos2: TEdit
                      Left = 460
                      Top = 44
                      Width = 113
                      Height = 21
                      TabOrder = 7
                      OnChange = EdLocTableChange
                    end
                    object EdTPos2: TEdit
                      Left = 576
                      Top = 44
                      Width = 89
                      Height = 21
                      TabOrder = 8
                      OnChange = EdLocTableChange
                    end
                    object CkComponentes: TCheckBox
                      Left = 8
                      Top = 48
                      Width = 85
                      Height = 17
                      Caption = 'Componentes'
                      Checked = True
                      State = cbChecked
                      TabOrder = 9
                    end
                    object CkVariaveis: TCheckBox
                      Left = 8
                      Top = 76
                      Width = 85
                      Height = 17
                      Caption = 'Vari'#225'veis'
                      Checked = True
                      State = cbChecked
                      TabOrder = 10
                    end
                    object EdTPre3: TEdit
                      Left = 96
                      Top = 72
                      Width = 217
                      Height = 21
                      TabOrder = 11
                      OnChange = EdLocTableChange
                    end
                    object EdCPre3: TEdit
                      Left = 316
                      Top = 72
                      Width = 141
                      Height = 21
                      TabOrder = 12
                      OnChange = EdLocTableChange
                    end
                    object EdCPos3: TEdit
                      Left = 460
                      Top = 72
                      Width = 113
                      Height = 21
                      TabOrder = 13
                      OnChange = EdLocTableChange
                    end
                    object EdTPos3: TEdit
                      Left = 576
                      Top = 72
                      Width = 89
                      Height = 21
                      TabOrder = 14
                      OnChange = EdLocTableChange
                    end
                    object CkCriaCampos: TCheckBox
                      Left = 8
                      Top = 100
                      Width = 85
                      Height = 17
                      Caption = 'Cria campos'
                      Checked = True
                      State = cbChecked
                      TabOrder = 15
                    end
                    object EdTPre4: TEdit
                      Left = 96
                      Top = 96
                      Width = 217
                      Height = 21
                      TabOrder = 16
                      OnChange = EdLocTableChange
                    end
                    object EdCPre4: TEdit
                      Left = 316
                      Top = 96
                      Width = 141
                      Height = 21
                      TabOrder = 17
                      OnChange = EdLocTableChange
                    end
                    object EdCPos4: TEdit
                      Left = 460
                      Top = 96
                      Width = 113
                      Height = 21
                      TabOrder = 18
                      OnChange = EdLocTableChange
                    end
                    object EdTPos4: TEdit
                      Left = 576
                      Top = 96
                      Width = 89
                      Height = 21
                      TabOrder = 19
                      Text = #39');'
                      OnChange = EdLocTableChange
                    end
                    object CkListaDBF: TCheckBox
                      Left = 8
                      Top = 24
                      Width = 85
                      Height = 17
                      Caption = 'Lista DBF'
                      Checked = True
                      State = cbChecked
                      TabOrder = 20
                    end
                    object CkValores: TCheckBox
                      Left = 8
                      Top = 124
                      Width = 85
                      Height = 17
                      Caption = 'Valores'
                      Checked = True
                      State = cbChecked
                      TabOrder = 21
                    end
                    object EdTPre5: TEdit
                      Left = 96
                      Top = 120
                      Width = 217
                      Height = 21
                      TabOrder = 22
                      OnChange = EdLocTableChange
                    end
                    object EdCPre5: TEdit
                      Left = 316
                      Top = 120
                      Width = 141
                      Height = 21
                      TabOrder = 23
                      OnChange = EdLocTableChange
                    end
                    object EdCPos5: TEdit
                      Left = 460
                      Top = 120
                      Width = 113
                      Height = 21
                      TabOrder = 24
                      OnChange = EdLocTableChange
                    end
                    object EdTPos5: TEdit
                      Left = 576
                      Top = 120
                      Width = 89
                      Height = 21
                      TabOrder = 25
                      OnChange = EdLocTableChange
                    end
                    object EdTabela: TEdit
                      Left = 212
                      Top = 148
                      Width = 121
                      Height = 21
                      TabOrder = 26
                    end
                  end
                  object Memo1: TMemo
                    Left = 0
                    Top = 181
                    Width = 948
                    Height = 393
                    Align = alClient
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -11
                    Font.Name = 'Lucida Console'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 1
                  end
                end
              end
            end
            object TabSheet7: TTabSheet
              Caption = 'Array of fields'
              ImageIndex = 4
              object Memo3: TMemo
                Left = 0
                Top = 41
                Width = 956
                Height = 561
                Align = alClient
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                ScrollBars = ssVertical
                TabOrder = 0
              end
              object Panel16: TPanel
                Left = 0
                Top = 0
                Width = 956
                Height = 41
                Align = alTop
                TabOrder = 1
                object Button1: TButton
                  Left = 24
                  Top = 12
                  Width = 129
                  Height = 25
                  Caption = 'Lista array de ins/upd'
                  TabOrder = 0
                  OnClick = Button1Click
                end
                object Button2: TButton
                  Left = 156
                  Top = 12
                  Width = 137
                  Height = 25
                  Caption = 'Dados da SQL executada'
                  TabOrder = 1
                  OnClick = Button2Click
                end
              end
            end
            object TabSheet4: TTabSheet
              Caption = 'Limpar tabelas'
              ImageIndex = 5
              object Panel5: TPanel
                Left = 0
                Top = 0
                Width = 956
                Height = 53
                Align = alTop
                TabOrder = 0
                object BitBtn8: TBitBtn
                  Left = 14
                  Top = 7
                  Width = 90
                  Height = 40
                  Caption = 'Lista'
                  TabOrder = 0
                  OnClick = BitBtn8Click
                end
                object BitBtn9: TBitBtn
                  Left = 110
                  Top = 7
                  Width = 90
                  Height = 40
                  Caption = 'Exclui'
                  TabOrder = 1
                  OnClick = BitBtn9Click
                end
                object CkTabZerada: TCheckBox
                  Left = 212
                  Top = 20
                  Width = 161
                  Height = 17
                  Caption = 'Incluir tabelas sem registros.'
                  TabOrder = 2
                end
              end
              object dmkDBGrid1: TdmkDBGrid
                Left = 0
                Top = 53
                Width = 182
                Height = 549
                Align = alLeft
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Selecio'
                    Title.Caption = '?'
                    Width = 18
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Registros'
                    Width = 48
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Tabela'
                    Width = 80
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Observaco'
                    Title.Caption = 'Observa'#231#245'es'
                    Visible = True
                  end>
                Color = clWindow
                DataSource = DsMinhas
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
                TabOrder = 1
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                OnCellClick = dmkDBGrid1CellClick
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Selecio'
                    Title.Caption = '?'
                    Width = 18
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Registros'
                    Width = 48
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Tabela'
                    Width = 80
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Observaco'
                    Title.Caption = 'Observa'#231#245'es'
                    Visible = True
                  end>
              end
              object dmkDBGrid2: TdmkDBGrid
                Left = 182
                Top = 53
                Width = 774
                Height = 549
                Align = alClient
                Color = clWindow
                DataSource = DsReg2
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
                TabOrder = 2
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
              end
            end
            object TabSheet9: TTabSheet
              Caption = 'MyLista'
              ImageIndex = 8
              object Panel19: TPanel
                Left = 0
                Top = 0
                Width = 956
                Height = 33
                Align = alTop
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object BitBtn10: TBitBtn
                  Left = 4
                  Top = 4
                  Width = 75
                  Height = 25
                  Caption = 'OK'
                  TabOrder = 0
                  OnClick = BitBtn10Click
                end
              end
              object MeEstrutMySQL: TMemo
                Left = 0
                Top = 33
                Width = 956
                Height = 569
                Align = alClient
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Courier New'
                Font.Style = []
                ParentFont = False
                ScrollBars = ssVertical
                TabOrder = 1
              end
            end
            object TabSheet11: TTabSheet
              Caption = 'UCreate'
              ImageIndex = 9
              object Panel25: TPanel
                Left = 0
                Top = 0
                Width = 956
                Height = 33
                Align = alTop
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object BitBtn14: TBitBtn
                  Left = 4
                  Top = 4
                  Width = 109
                  Height = 25
                  Caption = 'Tabela selecionada'
                  TabOrder = 0
                  OnClick = BitBtn14Click
                end
                object BitBtn15: TBitBtn
                  Left = 124
                  Top = 4
                  Width = 75
                  Height = 25
                  Caption = 'SQL escrita'
                  TabOrder = 1
                  OnClick = BitBtn15Click
                end
              end
              object MeSQLUCreate: TMemo
                Left = 0
                Top = 33
                Width = 956
                Height = 569
                Align = alClient
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Courier New'
                Font.Style = []
                ParentFont = False
                ScrollBars = ssVertical
                TabOrder = 1
              end
            end
            object TabSheet10: TTabSheet
              Caption = ' Texto UTF8?ISO88599'
              ImageIndex = 10
              object Memo2: TMemo
                Left = 0
                Top = 0
                Width = 842
                Height = 602
                Align = alClient
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Courier New'
                Font.Style = []
                ParentFont = False
                TabOrder = 0
              end
              object Memo6: TMemo
                Left = 842
                Top = 0
                Width = 57
                Height = 602
                Align = alRight
                TabOrder = 1
              end
              object Memo7: TMemo
                Left = 899
                Top = 0
                Width = 57
                Height = 602
                Align = alRight
                TabOrder = 2
              end
            end
            object TabSheet12: TTabSheet
              Caption = 'Tab HTML'
              ImageIndex = 11
              object Panel26: TPanel
                Left = 0
                Top = 0
                Width = 661
                Height = 602
                Align = alLeft
                Caption = 'Panel26'
                TabOrder = 0
                object GradeHTML: TStringGrid
                  Left = 1
                  Top = 45
                  Width = 660
                  Height = 556
                  Align = alLeft
                  DefaultColWidth = 32
                  DefaultRowHeight = 17
                  Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goEditing, goAlwaysShowEditor]
                  TabOrder = 0
                  ColWidths = (
                    32
                    194
                    270
                    64
                    72)
                  RowHeights = (
                    17
                    17
                    17
                    17
                    17)
                end
                object Panel27: TPanel
                  Left = 1
                  Top = 1
                  Width = 659
                  Height = 44
                  Align = alTop
                  TabOrder = 1
                  object Label7: TLabel
                    Left = 8
                    Top = 4
                    Width = 34
                    Height = 13
                    Caption = 'Linhas:'
                  end
                  object EdLinhas: TdmkEdit
                    Left = 8
                    Top = 20
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '4'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 4
                    ValWarn = False
                    OnExit = EdLinhasExit
                  end
                  object Button3: TButton
                    Left = 580
                    Top = 16
                    Width = 75
                    Height = 25
                    Caption = 'Gera HTML'
                    TabOrder = 1
                    OnClick = Button3Click
                  end
                  object Button4: TButton
                    Left = 92
                    Top = 16
                    Width = 75
                    Height = 25
                    Caption = 'Limpa'
                    TabOrder = 2
                    OnClick = Button4Click
                  end
                  object Button5: TButton
                    Left = 172
                    Top = 15
                    Width = 75
                    Height = 25
                    Caption = 'Salva'
                    TabOrder = 3
                    OnClick = Button5Click
                  end
                  object BtCarrega: TButton
                    Left = 252
                    Top = 15
                    Width = 75
                    Height = 25
                    Caption = 'Carrega'
                    TabOrder = 4
                    OnClick = BtCarregaClick
                  end
                end
              end
              object MeHTML: TMemo
                Left = 661
                Top = 0
                Width = 295
                Height = 602
                Align = alClient
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Courier New'
                Font.Style = []
                ParentFont = False
                TabOrder = 1
                WordWrap = False
              end
            end
            object PesqFlds: TTabSheet
              Caption = 'PesqFlds'
              ImageIndex = 10
              object Panel32: TPanel
                Left = 0
                Top = 0
                Width = 956
                Height = 233
                Align = alTop
                TabOrder = 0
                object Panel34: TPanel
                  Left = 1
                  Top = 1
                  Width = 185
                  Height = 231
                  Align = alLeft
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 0
                  object StaticText1: TStaticText
                    Left = 0
                    Top = 39
                    Width = 185
                    Height = 17
                    Align = alTop
                    Alignment = taCenter
                    BevelOuter = bvNone
                    Caption = 'Tabelas:'
                    TabOrder = 1
                    ExplicitWidth = 45
                  end
                  object MePsqFldTabs: TMemo
                    Left = 0
                    Top = 56
                    Width = 185
                    Height = 97
                    Align = alLeft
                    Lines.Strings = (
                      'lanctos'
                      'lct00%')
                    TabOrder = 2
                    WantReturns = False
                  end
                  object Panel35: TPanel
                    Left = 0
                    Top = 0
                    Width = 185
                    Height = 39
                    Align = alTop
                    BevelOuter = bvNone
                    TabOrder = 0
                    object Label15: TLabel
                      Left = 0
                      Top = 0
                      Width = 36
                      Height = 13
                      Align = alTop
                      Alignment = taCenter
                      Caption = 'Campo:'
                    end
                    object EdPsqFldFlds: TEdit
                      Left = 0
                      Top = 13
                      Width = 185
                      Height = 21
                      Align = alTop
                      TabOrder = 0
                      Text = 'Protesto'
                    end
                  end
                  object Panel33: TPanel
                    Left = 0
                    Top = 192
                    Width = 185
                    Height = 39
                    Align = alBottom
                    BevelOuter = bvNone
                    TabOrder = 4
                    object Label16: TLabel
                      Left = 0
                      Top = 0
                      Width = 122
                      Height = 13
                      Align = alTop
                      Alignment = taCenter
                      Caption = 'M'#237'nimo de itens por valor:'
                    end
                    object EdPsqFldItns: TdmkEdit
                      Left = 0
                      Top = 13
                      Width = 185
                      Height = 21
                      Align = alTop
                      Alignment = taRightJustify
                      TabOrder = 0
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '2'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 2
                      ValWarn = False
                    end
                  end
                  object Panel36: TPanel
                    Left = 0
                    Top = 153
                    Width = 185
                    Height = 39
                    Align = alBottom
                    BevelOuter = bvNone
                    TabOrder = 3
                    object Label17: TLabel
                      Left = 0
                      Top = 0
                      Width = 148
                      Height = 13
                      Align = alTop
                      Alignment = taCenter
                      Caption = 'Diversidade m'#237'nima de valores:'
                    end
                    object EdPsqFldDvrs: TdmkEdit
                      Left = 0
                      Top = 13
                      Width = 185
                      Height = 21
                      Align = alTop
                      Alignment = taRightJustify
                      TabOrder = 0
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '2'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 2
                      ValWarn = False
                    end
                  end
                end
                object BitBtn4: TBitBtn
                  Left = 192
                  Top = 20
                  Width = 75
                  Height = 25
                  Caption = 'Pesquisa'
                  TabOrder = 1
                  OnClick = BitBtn4Click
                end
                object BitBtn16: TBitBtn
                  Left = 192
                  Top = 48
                  Width = 75
                  Height = 25
                  Caption = 'Parar Pesq.'
                  TabOrder = 2
                  OnClick = BitBtn16Click
                end
              end
              object DBGrid2: TDBGrid
                Left = 0
                Top = 233
                Width = 956
                Height = 369
                Align = alClient
                DataSource = DsTestAux
                TabOrder = 1
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
              end
            end
            object TabSheet17: TTabSheet
              Caption = ' Pings no Servidor ativo'
              ImageIndex = 11
              object MemoPing: TMemo
                Left = 0
                Top = 0
                Width = 300
                Height = 602
                Align = alLeft
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Lucida Console'
                Font.Style = []
                ParentFont = False
                TabOrder = 0
                WordWrap = False
              end
            end
          end
        end
        object Panel12: TPanel
          Left = 0
          Top = 0
          Width = 122
          Height = 632
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 1
          object Panel13: TPanel
            Left = 0
            Top = 0
            Width = 122
            Height = 41
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object Label1: TLabel
              Left = 4
              Top = 0
              Width = 42
              Height = 13
              Caption = 'Localiza:'
            end
            object EdLocTable: TEdit
              Left = 4
              Top = 16
              Width = 113
              Height = 21
              TabOrder = 0
              OnChange = EdLocTableChange
            end
          end
          object GridTabelas: TDBGrid
            Left = 0
            Top = 41
            Width = 122
            Height = 591
            Align = alClient
            DataSource = DsTabelas
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnKeyDown = GridTabelasKeyDown
          end
        end
      end
    end
    object Panel9: TPanel
      Left = 0
      Top = 0
      Width = 137
      Height = 660
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 1
      object Panel10: TPanel
        Left = 0
        Top = 0
        Width = 137
        Height = 69
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object BtRefresh: TBitBtn
          Left = 1
          Top = 0
          Width = 100
          Height = 40
          Caption = '&Refresh [F5]'
          Enabled = False
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BtRefreshClick
        end
        object EdDBFiltro: TEdit
          Left = 4
          Top = 44
          Width = 90
          Height = 21
          TabOrder = 1
          OnChange = EdDBFiltroChange
        end
      end
      object GridDatabases: TDBGrid
        Left = 0
        Top = 69
        Width = 137
        Height = 519
        Align = alClient
        DataSource = DsDatabases
        PopupMenu = PMDatabases
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnKeyDown = GridDatabasesKeyDown
      end
      object RGVersaoMySQL_: TRadioGroup
        Left = 0
        Top = 588
        Width = 137
        Height = 72
        Align = alBottom
        Caption = '  Vers'#227'o MySQL'
        ItemIndex = 0
        Items.Strings = (
          '????'
          '4.0'
          '5.0')
        TabOrder = 2
        Visible = False
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1239
    Height = 48
    Align = alTop
    Alignment = taLeftJustify
    Caption = 'N'#227'o conectado'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 1237
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 17
      ExplicitTop = 25
      ExplicitWidth = 1006
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 708
    Width = 1239
    Height = 75
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel37: TPanel
      Left = 2
      Top = 15
      Width = 1235
      Height = 58
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object ProgressBar1: TProgressBar
        Left = 0
        Top = 24
        Width = 1235
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
      object ProgressBar2: TProgressBar
        Left = 0
        Top = 41
        Width = 1235
        Height = 17
        Align = alBottom
        TabOrder = 1
      end
    end
  end
  object MainMenu1: TMainMenu
    Left = 828
    Top = 4
    object Arquivo1: TMenuItem
      Caption = '&Arquivo'
      object Novaconexao1: TMenuItem
        Caption = '&Nova conex'#227'o'
        OnClick = Novaconexao1Click
      end
      object Sair1: TMenuItem
        Caption = '&Sair'
        OnClick = Sair1Click
      end
    end
    object DOS1: TMenuItem
      Caption = '&DOS'
      object estes1: TMenuItem
        Caption = '&Testes'
        OnClick = estes1Click
      end
    end
    object Pesquisa1: TMenuItem
      Caption = '&Pesquisa'
      object Textosemarquivos1: TMenuItem
        Caption = '&Textos em arquivos'
        object Miscelnea1: TMenuItem
          Caption = 'Miscel'#226'nea'
          OnClick = Miscelnea1Click
        end
        object ComponentesDelphi1: TMenuItem
          Caption = 'Componentes &Delphi'
          OnClick = ComponentesDelphi1Click
        end
        object Delphi1: TMenuItem
          Caption = 'Delphi'
          object Componentes1: TMenuItem
            Caption = 'Componentes'
            OnClick = Componentes1Click
          end
          object Separacamposdelistas1: TMenuItem
            Caption = 'Separa campos de Textos'
            OnClick = Separacamposdelistas1Click
          end
          object LocnoDFMesubstnoPAS1: TMenuItem
            Caption = 'Loc. no DFM e subst no PAS'
            OnClick = LocnoDFMesubstnoPAS1Click
          end
        end
      end
      object MegaSena1: TMenuItem
        Caption = '&Mega Sena'
        OnClick = MegaSena1Click
      end
      object CEP1: TMenuItem
        Caption = '&CEP'
        OnClick = CEP1Click
      end
      object IBGE1: TMenuItem
        Caption = '&IBGE'
        Enabled = False
        OnClick = IBGE1Click
      end
      object CoresVCLSkin1: TMenuItem
        Caption = '&Cores VCL Skin'
        OnClick = CoresVCLSkin1Click
      end
    end
    object Ferramentas1: TMenuItem
      Caption = '&Ferramentas'
      object Alterarcdigosemtabelas1: TMenuItem
        Caption = '&Alterar c'#243'digos em tabelas'
        OnClick = Alterarcdigosemtabelas1Click
      end
      object ListadeProcessos1: TMenuItem
        Caption = 'Lista de Processos'
        OnClick = ListadeProcessos1Click
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object Databaseemregistro1: TMenuItem
        Caption = 'Database em registro'
        OnClick = Databaseemregistro1Click
      end
    end
  end
  object MyDB: TMySQLDatabase
    ConnectOptions = []
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    AfterConnect = MyDBAfterConnect
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 800
    Top = 4
  end
  object QrTabelas: TMySQLQuery
    Database = MyDB
    BeforeClose = QrTabelasBeforeClose
    AfterScroll = QrTabelasAfterScroll
    SQL.Strings = (
      'SHOW TABLES FROM :p0')
    Left = 688
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrDatabases: TMySQLQuery
    Database = MyDB
    AfterOpen = QrDatabasesAfterOpen
    BeforeClose = QrDatabasesBeforeClose
    AfterScroll = QrDatabasesAfterScroll
    SQL.Strings = (
      'SHOW DATABASES')
    Left = 744
    Top = 4
  end
  object DsDatabases: TDataSource
    DataSet = QrDatabases
    Left = 772
    Top = 4
  end
  object DsTabelas: TDataSource
    DataSet = QrTabelas
    Left = 716
    Top = 4
  end
  object QrCampos: TMySQLQuery
    Database = MyEx
    BeforeOpen = QrCamposBeforeOpen
    AfterOpen = QrCamposAfterOpen
    OnCalcFields = QrCamposCalcFields
    SQL.Strings = (
      '/*SHOW FULL COLUMNS FROM entidades*/'
      'DESCRIBE user "%%"')
    OnGetSQLFldName = QrCamposGetSQLFldName
    OnCheckSQLField = QrCamposCheckSQLField
    Left = 632
    Top = 4
  end
  object DsCampos: TDataSource
    DataSet = QrCampos
    Left = 660
    Top = 4
  end
  object DsExecuta: TDataSource
    DataSet = QrExecuta
    Left = 576
    Top = 4
  end
  object QrExecuta: TMySQLQuery
    Database = MyEx
    SQL.Strings = (
      'SHOW TABLES FROM :p0')
    Left = 548
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object MyEx: TMySQLDatabase
    DatabaseName = 'mysql'
    ConnectOptions = []
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'DatabaseName=mysql')
    BeforeConnect = MyExBeforeConnect
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 604
    Top = 4
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 100
    OnTimer = Timer1Timer
    Left = 91
    Top = 253
  end
  object OpenDialog1: TOpenDialog
    Filter = 'SQL|*.SQL|TXT|*.txt'
    InitialDir = 'C:\Dermatek\Backups\Restore'
    Left = 63
    Top = 253
  end
  object QrRegistros: TMySQLQuery
    Database = MyEx
    Left = 7
    Top = 253
  end
  object DsRegistros: TDataSource
    DataSet = TbRegistros
    Left = 35
    Top = 253
  end
  object TbRegistros: TMySQLTable
    Database = MyEx
    Filtered = True
    AfterInsert = TbRegistrosAfterInsert
    AfterEdit = TbRegistrosAfterEdit
    AfterPost = TbRegistrosAfterPost
    AfterCancel = TbRegistrosAfterCancel
    Left = 7
    Top = 281
  end
  object MySQLBExec: TMySQLBatchExecute
    Database = MyEx
    Delimiter = ';'
    Left = 119
    Top = 253
  end
  object SaveDialog1: TSaveDialog
    Filter = 'SQL|*.SQL|TXT|*.txt'
    Left = 64
    Top = 280
  end
  object QrMinhas: TMySQLQuery
    Database = MySQL
    BeforeClose = QrTabelasBeforeClose
    AfterScroll = QrTabelasAfterScroll
    SQL.Strings = (
      'SELECT * FROM minhas')
    Left = 12
    Top = 8
    object QrMinhasTabela: TWideStringField
      FieldName = 'Tabela'
      Size = 255
    end
    object QrMinhasRegistros: TIntegerField
      FieldName = 'Registros'
    end
    object QrMinhasSelecio: TSmallintField
      FieldName = 'Selecio'
    end
    object QrMinhasObservaco: TWideStringField
      FieldName = 'Observaco'
      Size = 255
    end
  end
  object DsMinhas: TDataSource
    DataSet = QrMinhas
    Left = 40
    Top = 8
  end
  object MySQL: TMySQLDatabase
    DatabaseName = 'mysql'
    UserName = 'root'
    UserPassword = 'wkljweryhvbirt'
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'PWD=wkljweryhvbirt'
      'UID=root'
      'DatabaseName=mysql')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 68
    Top = 8
  end
  object QrMy: TMySQLQuery
    Database = MySQL
    Left = 96
    Top = 8
  end
  object TbReg2: TMySQLTable
    Database = MyEx
    Filtered = True
    AfterInsert = TbReg2AfterInsert
    AfterEdit = TbReg2AfterEdit
    AfterPost = TbReg2AfterPost
    AfterCancel = TbReg2AfterCancel
    Left = 124
    Top = 8
  end
  object DsReg2: TDataSource
    DataSet = TbReg2
    Left = 152
    Top = 8
  end
  object mySQLDirectQuery1: TMySQLDirectQuery
    Database = MySQL
    Left = 312
    Top = 8
  end
  object QrCopy: TMySQLQuery
    Database = MySQL
    Left = 244
    Top = 8
    object QrCopyInteiro: TIntegerField
      FieldName = 'Inteiro'
    end
    object QrCopyString: TWideStringField
      FieldName = 'String'
      Size = 100
    end
    object QrCopySmallint: TSmallintField
      FieldName = 'Smallint'
    end
    object QrCopyFloat: TFloatField
      FieldName = 'Float'
    end
    object QrCopyData: TDateField
      FieldName = 'Data'
    end
    object QrCopyHora: TTimeField
      FieldName = 'Hora'
    end
    object QrCopyDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrCopyMemo: TWideMemoField
      FieldName = 'Memo'
      BlobType = ftWideMemo
    end
    object QrCopyLargeInt: TLargeintField
      FieldName = 'LargeInt'
    end
  end
  object PMCarrega: TPopupMenu
    Left = 280
    Top = 8
    object ltimosalvo1: TMenuItem
      Caption = #218'ltimo salvo'
      OnClick = ltimosalvo1Click
    end
    object Arquivodecomponentes1: TMenuItem
      Caption = 'Arquivo de componentes Label'
      OnClick = Arquivodecomponentes1Click
    end
  end
  object QrFldExec: TMySQLQuery
    Database = MyEx
    SQL.Strings = (
      'DESCRIBE TABLE _CopiaSQLExecutada_')
    Left = 856
    Top = 4
    object QrFldExecField: TWideStringField
      FieldName = 'Field'
      Required = True
      Size = 64
    end
    object QrFldExecType: TWideStringField
      FieldName = 'Type'
      Required = True
      Size = 40
    end
    object QrFldExecNull: TWideStringField
      FieldName = 'Null'
      Required = True
      Size = 3
    end
    object QrFldExecKey: TWideStringField
      FieldName = 'Key'
      Required = True
      Size = 3
    end
    object QrFldExecDefault: TWideStringField
      FieldName = 'Default'
      Size = 64
    end
    object QrFldExecExtra: TWideStringField
      FieldName = 'Extra'
      Required = True
    end
  end
  object QrExecut2: TMySQLQuery
    Database = MyEx
    SQL.Strings = (
      'SHOW TABLES FROM :p0')
    Left = 884
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object DsVKDBFNTX: TDataSource
    Left = 372
    Top = 8
  end
  object QrFlds: TMySQLQuery
    Database = MyEx
    Left = 132
    Top = 352
    object QrFldsField: TWideStringField
      FieldName = 'Field'
      Required = True
      Size = 64
    end
    object QrFldsNull: TWideStringField
      FieldName = 'Null'
      Required = True
      Size = 3
    end
    object QrFldsKey: TWideStringField
      FieldName = 'Key'
      Required = True
      Size = 3
    end
    object QrFldsType: TWideMemoField
      FieldName = 'Type'
      BlobType = ftWideMemo
    end
    object QrFldsDefault: TWideMemoField
      FieldName = 'Default'
      BlobType = ftWideMemo
    end
    object QrFldsExtra: TWideStringField
      FieldName = 'Extra'
      Size = 255
    end
  end
  object QrIts: TMySQLQuery
    Database = MyEx
    Left = 132
    Top = 380
  end
  object DBTest: TMySQLDatabase
    DatabaseName = 'test'
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'DatabaseName=test')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 316
    Top = 212
  end
  object QrTestUpd: TMySQLQuery
    Database = DBTest
    Left = 344
    Top = 212
  end
  object QrTestAux: TMySQLQuery
    Database = MyEx
    Left = 372
    Top = 212
  end
  object DsTestAux: TDataSource
    DataSet = QrTestAux
    Left = 400
    Top = 212
  end
  object QrEntiCliInt: TMySQLQuery
    Database = DBTest
    SQL.Strings = (
      'SELECT CodCliInt'
      'FROM enticliint'
      'ORDER BY CodCliInt')
    Left = 444
    Top = 268
    object QrEntiCliIntCodCliInt: TIntegerField
      FieldName = 'CodCliInt'
    end
  end
  object QrTab: TMySQLQuery
    Database = DBTest
    SQL.Strings = (
      'SHOW TABLES LIKE :P0')
    Left = 472
    Top = 268
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object PMExporta: TPopupMenu
    Left = 940
    Top = 132
    object Separadoporpipe1: TMenuItem
      Caption = 'Separado por pipe '#39'|'#39
      OnClick = Separadoporpipe1Click
    end
    object SCV1: TMenuItem
      Caption = 'SCV '
      OnClick = SCV1Click
    end
  end
  object frxReport1: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 43716.475114236120000000
    ReportOptions.LastChange = 43716.475114236120000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 923
    Top = 474
    Datasets = <>
    Variables = <>
    Style = <>
  end
  object frxDBDataset1: TfrxDBDataset
    UserName = 'frxDBDataset1'
    CloseDataSource = False
    BCDToCurrency = False
    DataSetOptions = []
    Left = 591
    Top = 363
  end
  object TimerPingServer: TTimer
    Enabled = False
    Interval = 1800000
    OnTimer = TimerPingServerTimer
    Left = 92
    Top = 204
  end
  object PMDatabases: TPopupMenu
    Left = 52
    Top = 428
    object SQLparaexclusodoDBselecionado2: TMenuItem
      Caption = 'SQL para exclus'#227'o do DB selecionado'
      OnClick = SQLparaexclusodoDBselecionado2Click
    end
  end
end
