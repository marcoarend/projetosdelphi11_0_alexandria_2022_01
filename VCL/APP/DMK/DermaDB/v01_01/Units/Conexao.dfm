object FmConexao: TFmConexao
  Left = 248
  Top = 165
  Caption = 'Nova conex'#227'o'
  ClientHeight = 713
  ClientWidth = 1016
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 225
    Height = 665
    Align = alLeft
    TabOrder = 0
    object Panel5: TPanel
      Left = 1
      Top = 293
      Width = 223
      Height = 48
      Align = alTop
      Alignment = taLeftJustify
      TabOrder = 1
      object BtConfirmar: TBitBtn
        Left = 12
        Top = 4
        Width = 204
        Height = 40
        Caption = 'Confirmar altera'#231#227'o no item acima'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtConfirmarClick
      end
    end
    object Panel7: TPanel
      Left = 1
      Top = 1
      Width = 223
      Height = 292
      Align = alTop
      TabOrder = 0
      object Label1: TLabel
        Left = 12
        Top = 248
        Width = 34
        Height = 13
        Caption = 'Senha:'
      end
      object Label3: TLabel
        Left = 12
        Top = 208
        Width = 29
        Height = 13
        Caption = 'Login:'
      end
      object Label2: TLabel
        Left = 12
        Top = 168
        Width = 28
        Height = 13
        Caption = 'Porta:'
      end
      object lbDb: TLabel
        Left = 12
        Top = 128
        Width = 76
        Height = 13
        Caption = 'Base de Dados:'
      end
      object lbHost: TLabel
        Left = 12
        Top = 88
        Width = 25
        Height = 13
        Caption = 'Host:'
      end
      object Label5: TLabel
        Left = 12
        Top = 48
        Width = 147
        Height = 13
        Caption = 'Nome (Descri'#231#227'o da conex'#227'o):'
      end
      object Label4: TLabel
        Left = 12
        Top = 8
        Width = 119
        Height = 13
        Caption = 'Item selecionado da lista:'
      end
      object EdSenha: TdmkEdit
        Left = 14
        Top = 265
        Width = 204
        Height = 20
        Font.Charset = SYMBOL_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Wingdings'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        PasswordChar = 'l'
        OnChange = EdSenhaChange
        OnDblClick = EdSenhaDblClick
        OnExit = EdSenhaExit
        OnKeyDown = EdSenhaKeyDown
      end
      object EdLogin: TdmkEdit
        Left = 12
        Top = 224
        Width = 204
        Height = 21
        TabOrder = 4
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'root'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'root'
        ValWarn = False
      end
      object EdPorta: TdmkEdit
        Left = 12
        Top = 184
        Width = 204
        Height = 21
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '3306'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = '3306'
        ValWarn = False
      end
      object EdDB: TdmkEdit
        Left = 12
        Top = 144
        Width = 204
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdHost: TdmkEdit
        Left = 12
        Top = 104
        Width = 204
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'localhost'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'localhost'
        ValWarn = False
      end
      object EdDescricao: TdmkEdit
        Left = 12
        Top = 64
        Width = 205
        Height = 21
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'Conex'#227'o local'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'Conex'#227'o local'
        ValWarn = False
      end
      object EdItem: TdmkEdit
        Left = 12
        Top = 24
        Width = 205
        Height = 21
        TabStop = False
        Enabled = False
        TabOrder = 6
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = '000'
        ValWarn = False
        OnChange = EdItemChange
      end
    end
    object Panel6: TPanel
      Left = 1
      Top = 389
      Width = 223
      Height = 48
      Align = alTop
      TabOrder = 2
      object BitBtn3: TBitBtn
        Left = 12
        Top = 4
        Width = 204
        Height = 40
        Caption = '&Excluir item acima'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BitBtn3Click
      end
    end
    object Panel1: TPanel
      Left = 1
      Top = 525
      Width = 223
      Height = 80
      Align = alTop
      TabOrder = 3
      object Panel3: TPanel
        Left = 112
        Top = 1
        Width = 110
        Height = 78
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtDesiste: TBitBtn
          Left = 1
          Top = 31
          Width = 100
          Height = 40
          Caption = '&Desiste'
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
      object BtConfirma: TBitBtn
        Left = 4
        Top = 32
        Width = 100
        Height = 40
        Caption = '&Conectar'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtConfirmaClick
      end
    end
    object Panel8: TPanel
      Left = 1
      Top = 341
      Width = 223
      Height = 48
      Align = alTop
      TabOrder = 4
      object BitBtn2: TBitBtn
        Left = 12
        Top = 4
        Width = 204
        Height = 40
        Caption = 'Adicionar novo item acima'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BitBtn2Click
      end
    end
    object Panel4: TPanel
      Left = 1
      Top = 437
      Width = 223
      Height = 48
      Align = alTop
      TabOrder = 5
      object BtImporta: TBitBtn
        Left = 12
        Top = 4
        Width = 204
        Height = 40
        Caption = 'Importar Config de *.llm'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtImportaClick
      end
    end
    object RGVersaoMySQL_: TRadioGroup
      Left = 1
      Top = 485
      Width = 223
      Height = 40
      Align = alTop
      Caption = '  Vers'#227'o MySQL'
      Columns = 3
      ItemIndex = 0
      Items.Strings = (
        '????'
        '4.0'
        '5.0')
      TabOrder = 6
      Visible = False
    end
  end
  object Grade: TStringGrid
    Left = 225
    Top = 48
    Width = 791
    Height = 665
    Align = alClient
    ColCount = 8
    DefaultColWidth = 32
    DefaultRowHeight = 18
    TabOrder = 1
    OnDblClick = GradeDblClick
    OnDrawCell = GradeDrawCell
    OnKeyDown = GradeKeyDown
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1016
    Height = 48
    Align = alTop
    Caption = 'Nova Conex'#227'o a Servidor de MySQL'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 1014
      Height = 46
      Align = alClient
      Transparent = True
    end
  end
  object PMAcao: TPopupMenu
    Left = 144
    Top = 24
    object Carregarconexo1: TMenuItem
      Caption = '&Carregar conex'#227'o'
    end
    object Salvarconexo1: TMenuItem
      Caption = '&Salvar conex'#227'o'
    end
  end
  object OpenDialog1: TOpenDialog
    Filter = 'SQL|*.SQL|TXT|*.txt'
    InitialDir = 'C:\Dermatek\Backups\Restore'
    Left = 427
    Top = 65533
  end
  object SaveDialog1: TSaveDialog
    Filter = 'SQL|*.SQL|TXT|*.txt'
    Left = 428
    Top = 24
  end
end
