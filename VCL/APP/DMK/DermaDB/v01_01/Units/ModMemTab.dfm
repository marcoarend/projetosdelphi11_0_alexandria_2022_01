object DmModMemTab: TDmModMemTab
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 669
  Width = 985
  object TbDBsDACab: TABSTable
    CurrentVersion = '7.91 '
    DatabaseName = 'MEMORY'
    InMemory = True
    ReadOnly = False
    TableName = 'dbsdacab'
    Exclusive = False
    Left = 120
    Top = 4
    object TbDBsDACabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object TbDBsDACabNome: TStringField
      FieldName = 'Nome'
      Size = 100
    end
    object TbDBsDACabNomeUTF: TStringField
      FieldName = 'NomeUTF'
      Size = 100
    end
  end
  object DsDBsDACab: TDataSource
    DataSet = TbDBsDACab
    Left = 120
    Top = 52
  end
  object QrUpd: TABSQuery
    CurrentVersion = '7.91 '
    DatabaseName = 'MEMORY'
    InMemory = True
    ReadOnly = False
    Left = 32
    Top = 4
  end
  object TbDBsDAIts: TABSTable
    CurrentVersion = '7.91 '
    DatabaseName = 'MEMORY'
    InMemory = True
    ReadOnly = False
    TableName = 'dbsdaits'
    Exclusive = False
    Left = 120
    Top = 100
    object TbDBsDAItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object TbDBsDAItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object TbDBsDAItsNome: TStringField
      FieldName = 'Nome'
      Size = 100
    end
    object TbDBsDAItsNomeUTF: TStringField
      FieldName = 'NomeUTF'
      Size = 100
    end
  end
  object DsDBsDAIts: TDataSource
    DataSet = TbDBsDAIts
    Left = 120
    Top = 148
  end
end
