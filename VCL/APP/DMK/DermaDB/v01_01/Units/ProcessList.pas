unit ProcessList;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums;

type
  TFmProcessList = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Memo1: TMemo;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmProcessList: TFmProcessList;

implementation

uses UnMyObjects, Module, UnDmkProcFunc;

{$R *.DFM}

//mysqld.exe
procedure TFmProcessList.BtOKClick(Sender: TObject);
var
  List: TStringList;
  I: Integer;
begin
  Memo1.Lines.Clear;
  //
  List := TStringList.Create;
  try
    if dmkPF.RunningProcessesList(List, False) then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, False,
      'Quantidade de processos: ' + Geral.FF0(List.Count));
      //
      for I := 0 to List.Count - 1 do
      begin
        //I := List.IndexOf(Executavel);
        //if I > -1 then Result := True;
        Memo1.Lines.Add(List[I]);
      end;
    end;
  finally
    List.Free;
  end;
end;

procedure TFmProcessList.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmProcessList.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmProcessList.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmProcessList.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
