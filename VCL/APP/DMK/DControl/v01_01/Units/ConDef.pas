unit ConDef;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, dmkDBGrid, Menus, frxClass, frxDBSet, DmkDAC_PF, UnDmkEnums;

type
  TFmConDef = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtConditional: TBitBtn;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    DBGConDef: TdmkDBGrid;
    Splitter1: TSplitter;
    DBMemo1: TDBMemo;
    Splitter2: TSplitter;
    StaticText1: TStaticText;
    Panel6: TPanel;
    StaticText2: TStaticText;
    DBGApl: TdmkDBGrid;
    PMConditional: TPopupMenu;
    BtAplic: TBitBtn;
    DsConDef: TDataSource;
    QrConDef: TmySQLQuery;
    QrConDefCodigo: TIntegerField;
    QrConDefNome: TWideStringField;
    QrConDefObserv: TWideStringField;
    QrConDefLk: TIntegerField;
    QrConDefDataCad: TDateField;
    QrConDefDataAlt: TDateField;
    QrConDefUserCad: TIntegerField;
    QrConDefUserAlt: TIntegerField;
    QrConDefAlterWeb: TSmallintField;
    QrConDefAtivo: TSmallintField;
    Inclui1: TMenuItem;
    Altera1: TMenuItem;
    Exclui1: TMenuItem;
    PmAplic: TPopupMenu;
    Exclui2: TMenuItem;
    QrConDefApl: TmySQLQuery;
    DsConDefApl: TDataSource;
    QrConDefAplCodigo: TIntegerField;
    QrConDefAplControle: TIntegerField;
    QrConDefAplAplicativo: TIntegerField;
    QrConDefAplLk: TIntegerField;
    QrConDefAplDataCad: TDateField;
    QrConDefAplDataAlt: TDateField;
    QrConDefAplUserCad: TIntegerField;
    QrConDefAplUserAlt: TIntegerField;
    QrConDefAplAlterWeb: TSmallintField;
    QrConDefAplAtivo: TSmallintField;
    QrConDefAplNOMEAPLIC: TWideStringField;
    SbImprime: TBitBtn;
    PMImprime: TPopupMenu;
    Listaporaplicativos1: TMenuItem;
    ListaporConditionalDefines1: TMenuItem;
    frxCAD_CNDEF_001_1: TfrxReport;
    frxDsPesq: TfrxDBDataset;
    QrPesq: TmySQLQuery;
    IntegerField3: TIntegerField;
    StringField1: TWideStringField;
    QrPesqCodigo: TIntegerField;
    QrPesqNome: TWideStringField;
    QrPesqObserv: TWideStringField;
    QrPesqAtivo: TSmallintField;
    frxCAD_CNDEF_001_2: TfrxReport;
    Inclui2: TMenuItem;
    N1: TMenuItem;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Inclui1Click(Sender: TObject);
    procedure Altera1Click(Sender: TObject);
    procedure BtConditionalClick(Sender: TObject);
    procedure BtAplicClick(Sender: TObject);
    procedure QrConDefAfterScroll(DataSet: TDataSet);
    procedure QrConDefBeforeClose(DataSet: TDataSet);
    procedure PmAplicPopup(Sender: TObject);
    procedure Exclui1Click(Sender: TObject);
    procedure Exclui2Click(Sender: TObject);
    procedure PMConditionalPopup(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure frxCAD_CNDEF_001_1GetValue(const VarName: string;
      var Value: Variant);
    procedure ListaporConditionalDefines1Click(Sender: TObject);
    procedure Listaporaplicativos1Click(Sender: TObject);
    procedure frxCAD_CNDEF_001_2GetValue(const VarName: string;
      var Value: Variant);
    procedure Inclui2Click(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenPesq(Ordem: String);
  public
    { Public declarations }
    procedure ReopenConDef(Codigo: Integer);
    procedure ReopenConDefApl(Codigo, Controle: Integer);
  end;

  var
  FmConDef: TFmConDef;

implementation

uses UnMyObjects, Module, ConDefIts, UMySQLModule, MyDBCheck, UCreate,
  ModuleGeral;

{$R *.DFM}

procedure TFmConDef.Altera1Click(Sender: TObject);
begin
  UMyMod.FormInsUpd_Show(TFmConDefIts, FmConDefIts, afmoNegarComAviso,
    QrConDef, stUpd);
end;

procedure TFmConDef.BtAplicClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PmAplic, BtAplic);
end;

procedure TFmConDef.BtConditionalClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMConditional, BtConditional);
end;

procedure TFmConDef.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmConDef.Exclui1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrConDefCodigo.Value;
  //
  if Geral.MensagemBox('Confirma a exclus�o do item ID n�mero ' +
    FormatFloat('0', Codigo) + '?', 'Exclus�o de registro',
    MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM condef WHERE Codigo=:P0');
    Dmod.QrUpd.Params[0].AsInteger := Codigo;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenConDef(0);
  end;
end;

procedure TFmConDef.Exclui2Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrConDefApl, TDBGrid(DBGApl),
  'condefapl', ['Controle'], ['Controle'], istPergunta, '');
  //
  ReopenConDefApl(QrConDefCodigo.Value, 0);
end;

procedure TFmConDef.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmConDef.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  ReopenConDef(0);
end;

procedure TFmConDef.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmConDef.frxCAD_CNDEF_001_1GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_TITULOREL' then
    Value := 'Lista Conditional Defines'
  else if VarName = 'VARF_CODI_FRX' then
    Value := 'CAD-CNDEF-001-1';
end;

procedure TFmConDef.frxCAD_CNDEF_001_2GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_TITULOREL' then
    Value := 'Conditional Defines por aplicativos'
  else if VarName = 'VARF_CODI_FRX' then
    Value := 'CAD-CNDEF-001-2';
end;

procedure TFmConDef.Inclui1Click(Sender: TObject);
begin
  UMyMod.FormInsUpd_Show(TFmConDefIts, FmConDefIts, afmoNegarComAviso,
    QrConDef, stIns);
end;

procedure TFmConDef.Inclui2Click(Sender: TObject);
  procedure InsereItemAtual();
  var
    Codigo, Controle, Aplicativo: Integer;
  begin
    Aplicativo := DModG.QrSelCodsNivel1.Value;
    Codigo     := QrConDefCodigo.Value;
    Controle   := UMyMod.BuscaNovoCodigo_Int(Dmod.QrUpd, 'condefapl', 'Controle',
      [], [], stIns, 0, siPositivo, nil);
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'condefapl', False,
      ['Aplicativo', 'Codigo'], ['Controle'],
      [Aplicativo, Codigo], [Controle], True);
  end;
var
  Codigo: Integer;
begin
  try
    DBGConDef.Enabled := False;
    //
    Codigo := QrConDefCodigo.Value;
    //
    if DBCheck.EscolheCodigosMultiplos_0('...', Caption,
      'Seleciones os aplicativos desejados:', nil, 'Ativo', 'Nivel1', 'Nome', [
      'DELETE FROM _selcods_; ',
      'INSERT INTO _selcods_ ',
      'SELECT Codigo Nivel1, 0 Nivel2, ',
      '0 Nivel3, 0 Nivel4, 0 Nivel5, Nome, 0 Ativo ',
      'FROM ' + TMeuDB + '.aplicativos ',
      'WHERE Ativo = 1 ',
      'AND Categoria IN (0, 1)', //Apenas aplicativos desktop
      'AND Codigo NOT IN ',
      '( ',
      'SELECT Aplicativo ',
      'FROM ' + TMeuDB + '.condefapl ',
      'WHERE Codigo=' + Geral.FF0(Codigo),
      ') ',
      ''],[
      'SELECT * FROM _selcods_; ',
      ''], Dmod.QrUpd) then
    begin
      DModG.QrSelCods.First;
      while not DModG.QrSelCods.Eof do
      begin
        if DModG.QrSelCodsAtivo.Value = 1 then
          InsereItemAtual();
        DModG.QrSelCods.Next;
      end;
      ReopenConDefApl(Codigo, 0);
    end;
  finally
    DBGConDef.Enabled := True;
  end;
end;

procedure TFmConDef.Listaporaplicativos1Click(Sender: TObject);
begin
  ReopenPesq('NOMEAPLIC');
  //
  MyObjects.frxMostra(frxCAD_CNDEF_001_2, 'Conditional Defines por aplicativos');
end;

procedure TFmConDef.ListaporConditionalDefines1Click(Sender: TObject);
begin
  ReopenPesq('Nome');
  //
  MyObjects.frxMostra(frxCAD_CNDEF_001_1, 'Lista Conditional Defines');
end;

procedure TFmConDef.PmAplicPopup(Sender: TObject);
var
  Enab1, Enab2: Boolean;
begin
  Enab1 := (QrConDef.State <> dsInactive) and (QrConDef.RecordCount > 0);
  Enab2 := (QrConDefApl.State <> dsInactive) and (QrConDefApl.RecordCount > 0);
  //
  Inclui2.Enabled := Enab1;
  Exclui2.Enabled := Enab1 and Enab2;
end;

procedure TFmConDef.PMConditionalPopup(Sender: TObject);
var
  Enab1: Boolean;
begin
  Enab1 := (QrConDef.State <> dsInactive) and (QrConDef.RecordCount > 0);
  //
  Altera1.Enabled := Enab1;
  Exclui1.Enabled := Enab1;
end;

procedure TFmConDef.QrConDefAfterScroll(DataSet: TDataSet);
begin
  ReopenConDefApl(QrConDefCodigo.Value, 0);
end;

procedure TFmConDef.QrConDefBeforeClose(DataSet: TDataSet);
begin
  QrConDefApl.Close;
end;

procedure TFmConDef.ReopenConDef(Codigo: Integer);
begin
  QrConDef.Close;
  QrConDef.Database := Dmod.MyDB;
  QrConDef.Open;
  //
  if Codigo <> 0 then
    QrConDef.Locate('Codigo', Codigo, []);
end;

procedure TFmConDef.ReopenConDefApl(Codigo, Controle: Integer);
begin
  QrConDefApl.Close;
  QrConDefApl.Database := Dmod.MyDB;
  QrConDefApl.Params[0].AsInteger := Codigo;
  QrConDefApl.Open;
  //
  if Controle <> 0 then
    QrConDefApl.Locate('Controle', Controle, []);
end;

procedure TFmConDef.ReopenPesq(Ordem: String);
var
  Ord: String;
begin
  if Ordem <> '' then
    Ord := 'ORDER BY ' + Ordem
  else
    Ord := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq, Dmod.MyDB, [
  'SELECT cda.Aplicativo, apl.Nome NOMEAPLIC, ',
  'cdf.Codigo, cdf.Nome, cdf.Observ, cdf.Ativo ',
  'FROM condefapl cda ',
  'LEFT JOIN condef cdf ON cdf.Codigo = cda.Codigo ',
  'LEFT JOIN aplicativos apl ON apl.Codigo = cda.Aplicativo ',
  Ord,
  '']);
end;

procedure TFmConDef.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

end.
