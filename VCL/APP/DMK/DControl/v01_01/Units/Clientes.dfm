object FmClientes: TFmClientes
  Left = 368
  Top = 194
  Caption = 'CAD-CLIEN-001 :: Cadastro de Clientes'
  ClientHeight = 876
  ClientWidth = 1186
  Color = clBtnFace
  Constraints.MinHeight = 320
  Constraints.MinWidth = 788
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 120
  TextHeight = 16
  object PnEdita: TPanel
    Left = 0
    Top = 118
    Width = 1186
    Height = 758
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1186
      Height = 127
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 20
        Top = 20
        Width = 16
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'ID:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 96
        Top = 20
        Width = 44
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cliente:'
        FocusControl = DBEdNome
      end
      object SpeedButton5: TSpeedButton
        Left = 816
        Top = 39
        Width = 26
        Height = 26
        Hint = 'Inclui item de carteira'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        OnClick = SpeedButton5Click
      end
      object Label26: TLabel
        Left = 20
        Top = 69
        Width = 77
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'ID do cliente:'
        FocusControl = EdCli_Id
      end
      object EdCodigo: TdmkEdit
        Left = 20
        Top = 39
        Width = 69
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdCliente: TdmkEditCB
        Left = 96
        Top = 39
        Width = 69
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cliente'
        UpdCampo = 'Cliente'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCliente
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCliente: TdmkDBLookupComboBox
        Left = 165
        Top = 39
        Width = 649
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsEntidades
        TabOrder = 2
        dmkEditCB = EdCliente
        QryCampo = 'Cliente'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object CkAtivo: TdmkCheckBox
        Left = 238
        Top = 91
        Width = 59
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Ativo'
        TabOrder = 4
        QryCampo = 'Ativo'
        UpdCampo = 'Ativo'
        UpdType = utYes
        ValCheck = '1'
        ValUncheck = '0'
        OldValor = #0
      end
      object EdCli_Id: TdmkEdit
        Left = 20
        Top = 89
        Width = 210
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        CharCase = ecLowerCase
        MaxLength = 15
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Cli_Id'
        UpdCampo = 'Cli_Id'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnExit = EdCli_IdExit
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 681
      Width = 1186
      Height = 77
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 15
        Top = 21
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 1014
        Top = 18
        Width = 170
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 9
          Top = 2
          Width = 147
          Height = 50
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 118
    Width = 1186
    Height = 758
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1186
      Height = 135
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 20
        Top = 20
        Width = 16
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 94
        Top = 20
        Width = 131
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Raz'#227'o Social / Nome:'
        FocusControl = DBEdNome
      end
      object LaIE: TLabel
        Left = 809
        Top = 71
        Width = 54
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'I. E. / RG:'
        FocusControl = dmkDBEdit3
      end
      object LaCNPJ: TLabel
        Left = 950
        Top = 20
        Width = 74
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'CNPJ / CPF:'
        FocusControl = dmkDBEdit2
      end
      object LaFantasia: TLabel
        Left = 20
        Top = 71
        Width = 152
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Nome Fantasia / Apelido:'
        FocusControl = dmkDBEdit1
      end
      object Label4: TLabel
        Left = 950
        Top = 71
        Width = 77
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'ID do cliente:'
        FocusControl = dmkDBEdit4
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 20
        Top = 39
        Width = 69
        Height = 24
        Hint = 'N'#186' do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsClientes
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 94
        Top = 39
        Width = 851
        Height = 24
        Hint = 'Nome do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'ENTRAZ'
        DataSource = DsClientes
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit3: TdmkDBEdit
        Left = 809
        Top = 92
        Width = 136
        Height = 24
        Hint = 'Nome do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'ENTIE'
        DataSource = DsClientes
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit2: TdmkDBEdit
        Left = 950
        Top = 39
        Width = 136
        Height = 24
        Hint = 'Nome do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'ENTCNPJ_TXT'
        DataSource = DsClientes
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit1: TdmkDBEdit
        Left = 20
        Top = 92
        Width = 785
        Height = 24
        Hint = 'Nome do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'ENTFAN'
        DataSource = DsClientes
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBCheckBox1: TDBCheckBox
        Left = 1093
        Top = 42
        Width = 80
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Ativo'
        DataField = 'Ativo'
        DataSource = DsClientes
        TabOrder = 3
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object dmkDBEdit4: TdmkDBEdit
        Left = 950
        Top = 92
        Width = 136
        Height = 24
        Hint = 'Nome do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        CharCase = ecLowerCase
        Color = clWhite
        DataField = 'Cli_Id'
        DataSource = DsClientes
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 6
        UpdType = utYes
        Alignment = taLeftJustify
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 679
      Width = 1186
      Height = 79
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 18
        Width = 212
        Height = 59
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 158
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 108
          Top = 5
          Width = 50
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 59
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 10
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 214
        Top = 18
        Width = 51
        Height = 59
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 265
        Top = 18
        Width = 919
        Height = 59
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtContratos: TBitBtn
          Tag = 183
          Left = 448
          Top = 5
          Width = 148
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Contratos'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtContratosClick
        end
        object BtCliente: TBitBtn
          Tag = 10043
          Left = 5
          Top = 5
          Width = 148
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Cliente'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtClienteClick
        end
        object Panel2: TPanel
          Left = 756
          Top = 0
          Width = 163
          Height = 59
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 5
          object BtSaida: TBitBtn
            Tag = 13
            Left = 5
            Top = 5
            Width = 148
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtAplic: TBitBtn
          Tag = 10203
          Left = 153
          Top = 5
          Width = 147
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Aplicativo'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAplicClick
        end
        object BtObser: TBitBtn
          Tag = 10204
          Left = 596
          Top = 5
          Width = 147
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Observa'#231#245'es'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtObserClick
        end
        object BtModulos: TBitBtn
          Tag = 10205
          Left = 300
          Top = 5
          Width = 148
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&M'#243'dulos de aplic.'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtModulosClick
        end
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 135
      Width = 1186
      Height = 487
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      ActivePage = TabSheet1
      Align = alTop
      TabHeight = 25
      TabOrder = 2
      OnChange = PageControl1Change
      object TabSheet3: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Aplicativos'
        ImageIndex = 2
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Splitter6: TSplitter
          Left = 0
          Top = 255
          Width = 1178
          Height = 12
          Cursor = crVSplit
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alBottom
          Color = clBtnFace
          ParentColor = False
          ExplicitTop = 246
          ExplicitWidth = 1177
        end
        object Label3: TLabel
          Left = 0
          Top = 0
          Width = 782
          Height = 20
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          Alignment = taCenter
          Caption = 
            'D'#234' um duplo clique nas colunas Configura'#231#227'o FTP e Cfg do banco d' +
            'e dados para abrir o cadastro'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -17
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Panel10: TPanel
          Left = 0
          Top = 267
          Width = 1178
          Height = 185
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 0
          object DBGCliAplicMo: TDBGrid
            Left = 0
            Top = 20
            Width = 1178
            Height = 165
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            DataSource = DsCliAplicMo
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -15
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'ModuloStr'
                Title.Caption = 'M'#243'dulo'
                Width = 100
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Descri'
                Title.Caption = 'Descri'#231#227'o'
                Width = 350
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Conta'
                Title.Caption = 'ID'
                Visible = True
              end>
          end
          object StaticText4: TStaticText
            Left = 0
            Top = 0
            Width = 457
            Height = 20
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            Alignment = taCenter
            BorderStyle = sbsSunken
            Caption = 'M'#243'dulos dispon'#237'veis para o cliente para o aplicativo selecionado'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
          end
        end
        object DBGCliAplic: TdmkDBGrid
          Left = 0
          Top = 20
          Width = 1178
          Height = 235
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'AplicativoTxt'
              Title.Caption = 'Aplicativo'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TipAmb_TXT'
              Title.Caption = 'Ambiente'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Beta'
              Width = 50
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QtdLicen'
              Title.Caption = 'Qtde. Licen'#231'as'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QtdCliInt'
              Title.Caption = 'Qtde. Cli. Int.'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'STATAPLIC_TXT'
              Title.Caption = 'Status do aplicativo'
              Width = 130
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FimData_TXT'
              Title.Caption = 'Expira em'
              Width = 75
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'ID'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FTPCfg'
              Title.Caption = 'Configura'#231#227'o FTP'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataBaseCfg'
              Title.Caption = 'Cfg do banco de dados'
              Width = 150
              Visible = True
            end>
          Color = clWindow
          DataSource = DsCliAplic
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -15
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDblClick = DBGCliAplicDblClick
          FieldsCalcToOrder.Strings = (
            'TipAmb_TXT=TipAmb'
            'STATAPLIC_TXT=StatAplic')
          Columns = <
            item
              Expanded = False
              FieldName = 'AplicativoTxt'
              Title.Caption = 'Aplicativo'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TipAmb_TXT'
              Title.Caption = 'Ambiente'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Beta'
              Width = 50
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QtdLicen'
              Title.Caption = 'Qtde. Licen'#231'as'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QtdCliInt'
              Title.Caption = 'Qtde. Cli. Int.'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'STATAPLIC_TXT'
              Title.Caption = 'Status do aplicativo'
              Width = 130
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FimData_TXT'
              Title.Caption = 'Expira em'
              Width = 75
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'ID'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FTPCfg'
              Title.Caption = 'Configura'#231#227'o FTP'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataBaseCfg'
              Title.Caption = 'Cfg do banco de dados'
              Width = 150
              Visible = True
            end>
        end
      end
      object TabSheet2: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Contratos'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGCliContr: TDBGrid
          Left = 0
          Top = 0
          Width = 1178
          Height = 452
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          DataSource = DsCliContr
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -15
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'ID'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Versao'
              Title.Caption = 'Vers'#227'o'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Descri'#231#227'o do  contrato'
              Width = 230
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DContrato_TXT'
              Title.Caption = 'Data contrato'
              Width = 90
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtaAssina_TXT'
              Title.Caption = 'Data assinatura'
              Width = 90
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DInstal_TXT'
              Title.Caption = 'In'#237'cio vig'#234'ncia'
              Width = 90
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DVencimento_TXT'
              Title.Caption = '1'#186' vencimento'
              Width = 90
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtaPrxRenw_TXT'
              Title.Caption = 'Pr'#243'x. renova'#231#227'o'
              Width = 90
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtaCntrFim_TXT'
              Title.Caption = 'Rescis'#227'o'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ddMesVcto'
              Title.Caption = 'Dia vcto.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValorMes'
              Title.Caption = 'Valor'
              Width = 75
              Visible = True
            end>
        end
      end
      object TabSheet1: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Observa'#231#245'es'
        object Splitter4: TSplitter
          Left = 517
          Top = 0
          Width = 12
          Height = 452
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Color = clBtnFace
          ParentColor = False
          ExplicitHeight = 443
        end
        object Panel8: TPanel
          Left = 0
          Top = 0
          Width = 517
          Height = 452
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object EdPesquisa: TdmkEdit
            Left = 0
            Top = 0
            Width = 517
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = 'Pesquisar'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 'Pesquisar'
            ValWarn = False
            OnChange = EdPesquisaChange
            OnEnter = EdPesquisaEnter
            OnExit = EdPesquisaExit
          end
          object DBGCliObser: TdmkDBGrid
            Left = 0
            Top = 26
            Width = 517
            Height = 426
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            Columns = <
              item
                Expanded = False
                FieldName = 'Nome'
                Width = 300
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Controle'
                Visible = True
              end>
            Color = clWindow
            DataSource = DsCliObser
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -15
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Nome'
                Width = 300
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Controle'
                Visible = True
              end>
          end
        end
        object Panel7: TPanel
          Left = 529
          Top = 0
          Width = 649
          Height = 452
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object DBRichEdit2: TDBRichEdit
            Left = 0
            Top = 59
            Width = 649
            Height = 393
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            DataField = 'Texto'
            DataSource = DsCliObser
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ReadOnly = True
            ScrollBars = ssVertical
            TabOrder = 0
            Zoom = 100
          end
          object Panel6: TPanel
            Left = 0
            Top = 0
            Width = 649
            Height = 59
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            ParentBackground = False
            TabOrder = 1
            object BtLocaliza: TBitBtn
              Tag = 277
              Left = 5
              Top = 5
              Width = 49
              Height = 49
              Cursor = crHandPoint
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtLocalizaClick
            end
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1186
    Height = 64
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 1127
      Top = 0
      Width = 59
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 15
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
        OnChange = ImgTipoChange
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 320
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 5
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 57
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 108
        Top = 10
        Width = 50
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 160
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 212
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
      object BtEntidades: TBitBtn
        Tag = 132
        Left = 265
        Top = 10
        Width = 49
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
        OnClick = BtEntidadesClick
      end
    end
    object GB_M: TGroupBox
      Left = 320
      Top = 0
      Width = 807
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 299
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastro de Clientes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 299
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastro de Clientes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 299
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastro de Clientes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 64
    Width = 1186
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 1182
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 15
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 15
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrClientes: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrClientesBeforeOpen
    AfterOpen = QrClientesAfterOpen
    BeforeClose = QrClientesBeforeClose
    AfterScroll = QrClientesAfterScroll
    OnCalcFields = QrClientesCalcFields
    SQL.Strings = (
      'SELECT cli.*, ent.Tipo ENTTIPO, '
      'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) ENTRAZ,'
      'IF (ent.Tipo=0, ent.Fantasia, ent.Apelido) ENTFAN,'
      'IF (ent.Tipo=0, ent.CNPJ, ent.CPF) ENTCNPJ,'
      'IF (ent.Tipo=0, ent.IE, ent.RG) ENTIE,'
      'IF (ent.Tipo=0, ent.EUF, ent.PUF) ENTUF'
      'FROM clientes cli'
      'LEFT JOIN entidades ent ON ent.Codigo = cli.Cliente')
    Left = 64
    Top = 64
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'clientes.Codigo'
    end
    object QrClientesCliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'clientes.Cliente'
    end
    object QrClientesLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'clientes.Lk'
    end
    object QrClientesDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'clientes.DataCad'
    end
    object QrClientesDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'clientes.DataAlt'
    end
    object QrClientesUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'clientes.UserCad'
    end
    object QrClientesUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'clientes.UserAlt'
    end
    object QrClientesAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'clientes.AlterWeb'
    end
    object QrClientesAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'clientes.Ativo'
    end
    object QrClientesENTRAZ: TWideStringField
      FieldName = 'ENTRAZ'
      Size = 100
    end
    object QrClientesENTFAN: TWideStringField
      FieldName = 'ENTFAN'
      Size = 60
    end
    object QrClientesENTCNPJ: TWideStringField
      FieldName = 'ENTCNPJ'
      Size = 18
    end
    object QrClientesENTIE: TWideStringField
      FieldName = 'ENTIE'
    end
    object QrClientesENTCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ENTCNPJ_TXT'
      Calculated = True
    end
    object QrClientesENTTIPO: TSmallintField
      FieldName = 'ENTTIPO'
      Origin = 'entidades.Tipo'
    end
    object QrClientesCli_Id: TWideStringField
      FieldName = 'Cli_Id'
      Size = 15
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 92
    Top = 64
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCliente
    CanDel01 = BtContratos
    Left = 120
    Top = 64
  end
  object QrEntidades: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) Nome'
      'FROM entidades ent'
      'WHERE Cliente1 = "V"'
      'ORDER BY Nome')
    Left = 524
    Top = 12
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntidadesNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 552
    Top = 12
  end
  object PMCliente: TPopupMenu
    OnPopup = PMClientePopup
    Left = 272
    Top = 616
    object Inclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = Inclui1Click
    end
    object Altera1: TMenuItem
      Caption = '&Altera'
      OnClick = Altera1Click
    end
    object Exclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = Exclui1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Ativadesativa1: TMenuItem
      Caption = 'Ativa / desativa'
      OnClick = Ativadesativa1Click
    end
  end
  object QrCliObser: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM cliobser'
      'WHERE Codigo=:P0')
    Left = 148
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCliObserCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCliObserControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCliObserNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrCliObserLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCliObserDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCliObserDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCliObserUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCliObserUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCliObserAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCliObserAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrCliObserTexto: TWideMemoField
      FieldName = 'Texto'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object DsCliObser: TDataSource
    DataSet = QrCliObser
    Left = 176
    Top = 64
  end
  object FindDialog1: TFindDialog
    OnFind = FindDialog1Find
    Left = 456
    Top = 353
  end
  object PMObser: TPopupMenu
    OnPopup = PMObserPopup
    Left = 744
    Top = 628
    object Inclui4: TMenuItem
      Caption = '&Inclui'
      OnClick = Inclui4Click
    end
    object Altera4: TMenuItem
      Caption = '&Altera'
      OnClick = Altera4Click
    end
    object Exclui4: TMenuItem
      Caption = '&Exclui'
      OnClick = Exclui4Click
    end
  end
  object QrCliContr: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT con.Codigo, con.Versao, con.Nome, con.ddMesVcto, con.Valo' +
        'rMes, '
      
        'IF(DContrato < "1900-01-01", "", DATE_FORMAT(DContrato, "%d/%m/%' +
        'Y")) DContrato_TXT,'
      
        'IF(DtaAssina< "1900-01-01", "", DATE_FORMAT(DtaAssina, "%d/%m/%Y' +
        '")) DtaAssina_TXT, '
      
        'IF(DInstal< "1900-01-01", "", DATE_FORMAT(DInstal, "%d/%m/%Y")) ' +
        'DInstal_TXT, '
      
        'IF(DVencimento< "1900-01-01", "", DATE_FORMAT(DVencimento, "%d/%' +
        'm/%Y")) DVencimento_TXT, '
      
        'IF(DtaPrxRenw< "1900-01-01", "", DATE_FORMAT(DtaPrxRenw, "%d/%m/' +
        '%Y")) DtaPrxRenw_TXT, '
      
        'IF(DtaCntrFim< "1900-01-01", "", DATE_FORMAT(DtaCntrFim, "%d/%m/' +
        '%Y")) DtaCntrFim_TXT '
      'FROM contratos con '
      'WHERE con.Contratada=:P0'
      'AND con.Contratante=:P1')
    Left = 204
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrCliContrCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCliContrVersao: TFloatField
      FieldName = 'Versao'
    end
    object QrCliContrNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrCliContrDContrato_TXT: TWideStringField
      FieldName = 'DContrato_TXT'
      Size = 10
    end
    object QrCliContrDtaAssina_TXT: TWideStringField
      FieldName = 'DtaAssina_TXT'
      Size = 10
    end
    object QrCliContrDInstal_TXT: TWideStringField
      FieldName = 'DInstal_TXT'
      Size = 10
    end
    object QrCliContrDVencimento_TXT: TWideStringField
      FieldName = 'DVencimento_TXT'
      Size = 10
    end
    object QrCliContrDtaPrxRenw_TXT: TWideStringField
      FieldName = 'DtaPrxRenw_TXT'
      Size = 10
    end
    object QrCliContrddMesVcto: TSmallintField
      FieldName = 'ddMesVcto'
    end
    object QrCliContrValorMes: TFloatField
      FieldName = 'ValorMes'
      DisplayFormat = '#,###,###,##0.00'
    end
  end
  object DsCliContr: TDataSource
    DataSet = QrCliContr
    Left = 232
    Top = 64
  end
  object QrCliAplic: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrCliAplicBeforeClose
    AfterScroll = QrCliAplicAfterScroll
    OnCalcFields = QrCliAplicCalcFields
    SQL.Strings = (
      'SELECT apl.Nome NOMEAPL, apl.Categoria, apl.Sigla SIGLAAPL, '
      'ftp.Nome FTPCfg_TXT, msc.Nome DataBaseCfg_TXT, cla.*'
      'FROM cliaplic cla'
      'LEFT JOIN aplicativos apl ON apl.Codigo = cla.Aplicativo'
      'LEFT JOIN ftpconfig ftp ON ftp.Codigo = cla.FTPCfg'
      'LEFT JOIN mysqlconf msc ON msc.Codigo = cla.DataBaseCfg'
      'WHERE cla.Codigo=:P0'
      'ORDER BY NOMEAPL')
    Left = 260
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCliAplicCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'cliaplic.Codigo'
    end
    object QrCliAplicControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'cliaplic.Controle'
    end
    object QrCliAplicAplicativo: TIntegerField
      FieldName = 'Aplicativo'
      Origin = 'cliaplic.Aplicativo'
    end
    object QrCliAplicAplicativoTxt: TWideStringField
      FieldName = 'AplicativoTxt'
      Size = 50
    end
    object QrCliAplicLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'cliaplic.Lk'
    end
    object QrCliAplicDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'cliaplic.DataCad'
    end
    object QrCliAplicDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'cliaplic.DataAlt'
    end
    object QrCliAplicUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'cliaplic.UserCad'
    end
    object QrCliAplicUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'cliaplic.UserAlt'
    end
    object QrCliAplicAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'cliaplic.AlterWeb'
    end
    object QrCliAplicAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'cliaplic.Ativo'
    end
    object QrCliAplicFimData: TDateField
      FieldName = 'FimData'
      Origin = 'cliaplic.FimData'
    end
    object QrCliAplicStatAplic: TSmallintField
      FieldName = 'StatAplic'
    end
    object QrCliAplicQtdLicen: TIntegerField
      FieldName = 'QtdLicen'
    end
    object QrCliAplicSTATAPLIC_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'STATAPLIC_TXT'
      Size = 50
      Calculated = True
    end
    object QrCliAplicQtdCliInt: TIntegerField
      FieldName = 'QtdCliInt'
    end
    object QrCliAplicDataBaseCfg: TIntegerField
      FieldName = 'DataBaseCfg'
    end
    object QrCliAplicFTPCfg: TIntegerField
      FieldName = 'FTPCfg'
    end
    object QrCliAplicBeta: TSmallintField
      FieldName = 'Beta'
      MaxValue = 1
    end
    object QrCliAplicTipAmb_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TipAmb_TXT'
      Size = 5
      Calculated = True
    end
    object QrCliAplicTipAmb: TSmallintField
      FieldName = 'TipAmb'
    end
    object QrCliAplicFimData_TXT: TWideStringField
      FieldName = 'FimData_TXT'
      Size = 10
    end
  end
  object DsCliAplic: TDataSource
    DataSet = QrCliAplic
    Left = 288
    Top = 64
  end
  object QrCliAplicMo: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT mol.Modulo, mol.Nome, cam.*'
      'FROM cliaplicmo cam'
      'LEFT JOIN aplicmodul amu ON amu.Controle=cam.Modulo'
      'LEFT JOIN modulos mol ON mol.Modulo=amu.Modulo'
      'WHERE cam.Controle=:P0')
    Left = 316
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCliAplicMoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCliAplicMoControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCliAplicMoConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrCliAplicMoModulo: TIntegerField
      FieldName = 'Modulo'
    end
    object QrCliAplicMoModuloStr: TWideStringField
      FieldName = 'ModuloStr'
      Size = 50
    end
    object QrCliAplicMoDescri: TWideStringField
      FieldName = 'Descri'
      Size = 100
    end
  end
  object DsCliAplicMo: TDataSource
    DataSet = QrCliAplicMo
    Left = 344
    Top = 64
  end
  object PMContratos: TPopupMenu
    OnPopup = PMContratosPopup
    Left = 624
    Top = 624
    object Incluinovocontrato1: TMenuItem
      Caption = '&Inclui novo contrato'
      OnClick = Incluinovocontrato1Click
    end
    object Gerenciacontratoselecionado1: TMenuItem
      Caption = '&Gerencia contrato selecionado'
      OnClick = Gerenciacontratoselecionado1Click
    end
  end
  object PMAplic: TPopupMenu
    OnPopup = PMAplicPopup
    Left = 388
    Top = 621
    object Inclui2: TMenuItem
      Caption = '&Inclui App Local'
      OnClick = Inclui2Click
    end
    object IncluiAppWeb1: TMenuItem
      Caption = 'Inclui App &Web'
      OnClick = IncluiAppWeb1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Altera2: TMenuItem
      Caption = '&Altera'
      OnClick = Altera2Click
    end
    object Exclui2: TMenuItem
      Caption = '&Exclui'
      OnClick = Exclui2Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Listadecomputadoresparaoaplicselec1: TMenuItem
      Caption = '&Lista de computadores para o aplic. selec.'
      OnClick = Listadecomputadoresparaoaplicselec1Click
    end
    object AbrirURLdowebapp1: TMenuItem
      Caption = 'Abrir &URL do web app'
      OnClick = AbrirURLdowebapp1Click
    end
  end
  object PMImprime: TPopupMenu
    OnPopup = PMImprimePopup
    Left = 16
    Top = 32
    object Lista1: TMenuItem
      Caption = '&Lista de clientes'
      object Ativos1: TMenuItem
        Caption = '&Apenas ativos'
        OnClick = Ativos1Click
      end
      object odos1: TMenuItem
        Caption = '&Todos'
        OnClick = odos1Click
      end
    end
  end
  object frxCAD_CLIEN_001_001: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39664.693294085700000000
    ReportOptions.LastChange = 41277.717304849540000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxCAD_CLIEN_001_001GetValue
    Left = 100
    Top = 368
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsListaClientes
        DataSetName = 'frxDsListaClientes'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 56.692950000000000000
        Top = 18.897650000000000000
        Width = 699.213050000000000000
        object Shape2: TfrxShapeView
          Width = 699.213050000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo38: TfrxMemoView
          Width = 684.094930000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line9: TfrxLineView
          Top = 18.897650000000000000
          Width = 699.213050000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo39: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 593.386210000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 487.559370000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_TITULOREL]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Top = 37.795300000000000000
          Width = 699.213050000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 136.063080000000000000
        Width = 699.213050000000000000
        object Memo1: TfrxMemoView
          Width = 75.590551180000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ID Cliente')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 75.590600000000000000
          Width = 75.590551180000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ID Entidade')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 151.181200000000000000
          Width = 298.582870000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Raz'#227'o Social / Nome')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 657.638220000000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ativo')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 449.764070000000000000
          Width = 207.874150000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome Fantasia / Apelido')
          ParentFont = False
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 211.653680000000000000
        Width = 699.213050000000000000
        object Memo10: TfrxMemoView
          Left = 657.638220000000000000
          Width = 41.574781180000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[COUNT(MasterData1)]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 616.063390000000000000
          Width = 41.574781180000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'TOTAL')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 287.244280000000000000
        Width = 699.213050000000000000
        OnAfterPrint = 'PageFooter1OnAfterPrint'
        object Memo13: TfrxMemoView
          Width = 427.086890000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 427.086890000000000000
          Width = 272.126160000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 173.858380000000000000
        Width = 699.213050000000000000
        DataSet = frxDsListaClientes
        DataSetName = 'frxDsListaClientes'
        RowCount = 0
        object CheckBox4: TfrxCheckBoxView
          Left = 657.638220000000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          CheckColor = clBlack
          CheckStyle = csCheck
          DataField = 'Ativo'
          DataSet = frxDsListaClientes
          DataSetName = 'frxDsListaClientes'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo5: TfrxMemoView
          Width = 75.590551180000000000
          Height = 13.228346460000000000
          DataField = 'Codigo'
          DataSet = frxDsListaClientes
          DataSetName = 'frxDsListaClientes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsListaClientes."Codigo"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 75.590600000000000000
          Width = 75.590551180000000000
          Height = 13.228346460000000000
          DataField = 'ID'
          DataSet = frxDsListaClientes
          DataSetName = 'frxDsListaClientes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsListaClientes."ID"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 151.181200000000000000
          Width = 298.582870000000000000
          Height = 13.228346460000000000
          DataField = 'ENTRAZ'
          DataSet = frxDsListaClientes
          DataSetName = 'frxDsListaClientes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsListaClientes."ENTRAZ"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 449.764070000000000000
          Width = 207.874150000000000000
          Height = 13.228346460000000000
          DataField = 'ENTFAN'
          DataSet = frxDsListaClientes
          DataSetName = 'frxDsListaClientes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsListaClientes."ENTFAN"]')
          ParentFont = False
        end
      end
    end
  end
  object QrListaClientes: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cli.Codigo, ent.Codigo ID, cli.Ativo, '
      'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) ENTRAZ,'
      'IF (ent.Tipo=0, ent.Fantasia, ent.Apelido) ENTFAN'
      'FROM clientes cli'
      'LEFT JOIN entidades ent ON ent.Codigo = cli.Cliente'
      'WHERE cli.Ativo = 1'
      'ORDER BY ENTRAZ')
    Left = 128
    Top = 368
    object QrListaClientesENTRAZ: TWideStringField
      FieldName = 'ENTRAZ'
      Size = 100
    end
    object QrListaClientesENTFAN: TWideStringField
      FieldName = 'ENTFAN'
      Size = 60
    end
    object QrListaClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrListaClientesID: TIntegerField
      FieldName = 'ID'
    end
    object QrListaClientesAtivo: TIntegerField
      FieldName = 'Ativo'
    end
  end
  object frxDsListaClientes: TfrxDBDataset
    UserName = 'frxDsListaClientes'
    CloseDataSource = False
    FieldAliases.Strings = (
      'ENTRAZ=ENTRAZ'
      'ENTFAN=ENTFAN'
      'Codigo=Codigo'
      'ID=ID'
      'Ativo=Ativo')
    DataSet = QrListaClientes
    BCDToCurrency = False
    Left = 156
    Top = 368
  end
  object CSTabSheetChamou: TdmkCompoStore
    Left = 444
    Top = 123
  end
  object PMModulos: TPopupMenu
    OnPopup = PMModulosPopup
    Left = 496
    Top = 624
    object Incluimduloaoaplicativoselecionado1: TMenuItem
      Caption = 'Inclui m'#243'dulo ao aplicativo selecionado'
      OnClick = Incluimduloaoaplicativoselecionado1Click
    end
    object Excluimduloaoaplicativoselecionado1: TMenuItem
      Caption = 'Exclui m'#243'dulo ao aplicativo selecionado'
      OnClick = Excluimduloaoaplicativoselecionado1Click
    end
  end
end
