unit ConDefIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, dmkCheckBox, UnDmkEnums;

type
  TFmConDefIts = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    EdNome: TdmkEdit;
    Label9: TLabel;
    Label1: TLabel;
    EdObserv: TdmkEdit;
    CkContinuar: TCheckBox;
    CkAtivo: TdmkCheckBox;
    EdCodigo: TdmkEdit;
    Label3: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmConDefIts: TFmConDefIts;

implementation

uses UnMyObjects, Module, ConDef, UMySQLModule;

{$R *.DFM}

procedure TFmConDefIts.BtOKClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.ValueVariant;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('condef', 'Codigo', ImgTipo.SQLType,
    FmConDef.QrConDefCodigo.Value);
  if UMyMod.ExecSQLInsUpdFm(FmConDefIts, ImgTipo.SQLType, 'condef', Codigo,
    Dmod.QrUpd) then
  begin
    FmConDef.ReopenConDef(Codigo);
    if CkContinuar.Checked then
    begin
      Geral.MensagemBox('Item adicionado com sucesso!', 'Aviso', MB_OK+MB_ICONWARNING);
      ImgTipo.SQLType := stIns;
      //
      EdNome.ValueVariant   := '';
      EdObserv.ValueVariant := '';
      CkAtivo.Checked       := True;
      EdNome.SetFocus;
    end else
      Close;
  end;
end;

procedure TFmConDefIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmConDefIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmConDefIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmConDefIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
