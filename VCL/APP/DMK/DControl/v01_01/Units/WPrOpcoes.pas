unit WPrOpcoes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, ComCtrls, DBCtrls, Db, mySQLDbTables,
  dmkEdit, dmkDBLookupComboBox, dmkEditCB, dmkGeral, dmkRadioGroup, dmkCheckBox,
  dmkLabel;

type
  TFmWPrOpcoes = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    Panel2: TPanel;
    BtDesiste: TBitBtn;
    Panel1: TPanel;
    QrProAltPas: TmySQLQuery;
    DsProAltPas: TDataSource;
    QrProAltPasCodigo: TIntegerField;
    QrProAltPasNome: TWideStringField;
    QrProDowArq: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsProDowArq: TDataSource;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    EdProAltPas: TdmkEditCB;
    CBProAltPas: TdmkDBLookupComboBox;
    EdProDowArq: TdmkEditCB;
    CBProDowArq: TdmkDBLookupComboBox;
    EdProAtivApl: TdmkEditCB;
    CBProAtivApl: TdmkDBLookupComboBox;
    Label3: TLabel;
    QrProAtivApl: TmySQLQuery;
    IntegerField2: TIntegerField;
    StringField2: TWideStringField;
    DsProAtivApl: TDataSource;
    PainelTitulo: TPanel;
    Image1: TImage;
    QrWPrOpcoes: TmySQLQuery;
    DsWPrOpcoes: TDataSource;
    QrWPrOpcoesProAltPas: TIntegerField;
    QrWPrOpcoesProAtivApl: TIntegerField;
    QrWPrOpcoesProDowArq: TIntegerField;
    Label7: TLabel;
    EdDiasExp: TdmkEdit;
    Label16: TLabel;
    procedure BtDesisteClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmWPrOpcoes: TFmWPrOpcoes;

implementation

uses Module, UnInternalConsts, UMySQLModule, UnMyObjects;

{$R *.DFM}

procedure TFmWPrOpcoes.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWPrOpcoes.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmWPrOpcoes.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmWPrOpcoes.FormCreate(Sender: TObject);
begin
  QrWPrOpcoes.Close;
  QrWPrOpcoes.Open;
  //
  QrProAltPas.Close;
  QrProAltPas.Open;
  QrProDowArq.Close;
  QrProDowArq.Open;
  QrProAtivApl.Close;
  QrProAtivApl.Open;
  //
  EdProAltPas.ValueVariant  := QrWPrOpcoesProAltPas.Value;
  CBProAltPas.KeyValue      := QrWPrOpcoesProAltPas.Value;
  EdProDowArq.ValueVariant  := QrWPrOpcoesProDowArq.Value;
  CBProDowArq.KeyValue      := QrWPrOpcoesProDowArq.Value;
  EdProAtivApl.ValueVariant := QrWPrOpcoesProAtivApl.Value;
  CBProAtivApl.KeyValue     := QrWPrOpcoesProAtivApl.Value;
  EdDiasExp.ValueVariant    := Dmod.QrWControlDiasExp.Value;
end;

procedure TFmWPrOpcoes.BtOKClick(Sender: TObject);
begin
  Dmod.QrUpdN.SQL.Clear;
  Dmod.QrUpdN.SQL.Add('UPDATE wcontrol SET ProAltPas=:P00, ');
  Dmod.QrUpdN.SQL.Add('ProDowArq=:P01, ProAtivApl=:P02, DiasExp=:P03');
  Dmod.QrUpdN.Params[00].AsInteger := EdProAltPas.ValueVariant;
  Dmod.QrUpdN.Params[01].AsInteger := EdProDowArq.ValueVariant;
  Dmod.QrUpdN.Params[02].AsInteger := EdProAtivApl.ValueVariant;
  Dmod.QrUpdN.Params[03].AsInteger := EdDiasExp.ValueVariant;
  Dmod.QrUpdN.ExecSQL;
  //
  QrWPrOpcoes.Close;
  QrWPrOpcoes.Open;
  //
  Close;  
end;

end.
