unit BackupDirIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, dmkMemo, dmkCheckBox, Variants, UnDmkEnums;

type
  TFmBackupDirIts = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    PnExtOmit: TPanel;
    StaticText3: TStaticText;
    MeExt: TdmkMemo;
    Panel5: TPanel;
    PnDados1: TPanel;
    Label32: TLabel;
    Label9: TLabel;
    SpeedButton2: TSpeedButton;
    RGWeb: TRadioGroup;
    EdOrigem: TdmkEdit;
    EdCodigo: TdmkEdit;
    CkExtOmit: TdmkCheckBox;
    PnDirWeb: TPanel;
    LaFTPDir: TLabel;
    EdFTPWebDir: TdmkEditCB;
    CBFTPWebDir: TdmkDBLookupComboBox;
    PnDados4: TPanel;
    GbOpcoes: TGroupBox;
    LaSenha: TLabel;
    CkZipar: TdmkCheckBox;
    CkEncryptar: TdmkCheckBox;
    EdSenha: TdmkEdit;
    CkSubDir: TdmkCheckBox;
    CkContinuar: TCheckBox;
    PnDirLoc: TPanel;
    Label1: TLabel;
    SpeedButton1: TSpeedButton;
    EdDestino: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure CkEncryptarClick(Sender: TObject);
    procedure CkZiparClick(Sender: TObject);
    procedure EdSenhaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdDestinoExit(Sender: TObject);
    procedure CkExtOmitClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure MostraEdicao(SQLType: TSQLType; Codigo, WEB: Integer);
  public
    { Public declarations }
    FCodigo, FWeb, FSubDir, FZipar, FEncrypt, FFTPWebDir: Integer;
    FDirDestino, FSenhaCrypt, FCaminho: String;
  end;

  var
    FmBackupDirIts: TFmBackupDirIts;
  const
    FFormatFloat = '00000';

implementation

uses Module, UnMyObjects, BackupDir, UMySQLModule;

{$R *.DFM}

procedure TFmBackupDirIts.BtOKClick(Sender: TObject);
var
  Codigo, FTPWebDir, Web: Integer;
  Origem, Destino, Senha, Extensoes: String;
  OmitExt, SubDir, Encrypt, Zipar: Boolean;
begin
  Origem  := EdOrigem.ValueVariant;
  OmitExt := CkExtOmit.Checked;
  Web     := RGWeb.ItemIndex;
  SubDir  := CkSubDir.Checked;
  Zipar   := CkZipar.Checked;
  Encrypt := CkEncryptar.Checked;
  //
  if MyObjects.FIC(Length(Origem) = 0, EdOrigem, 'Diret�rio de origem n�o definido!') then Exit;
  if MyObjects.FIC(Web < 0, RGWeb, 'Tipo de diret�rio destino n�o definido!') then Exit;
  //
  case RGWeb.ItemIndex of
    0:
    begin
      Destino   := EdDestino.ValueVariant;
      FTPWebDir := 0;
      //
      if MyObjects.FIC(Length(Destino) = 0, EdDestino, 'Diret�rio destino n�o definido!') then Exit;
    end
    else
    begin
      //Fazer FTP Destino   := DModFTP.QrFTPDirWeb_FTPh.Value + DModFTP.QrFTPDirCaminho.Value;
      //Fazer FTP FTPWebDir := EdFTPWebDir.ValueVariant;
      //
      if MyObjects.FIC(FTPWebDir = 0, EdFTPWebDir, 'Diret�rio FTP n�o definido!') then Exit;
    end;
  end;
  if OmitExt then
  begin
    Extensoes := MeExt.Text;
    //
    if MyObjects.FIC(Length(Extensoes) = 0, MeExt, 'Extens�es n�o definidas!') then Exit;
  end else
    Extensoes := '';
  if Encrypt then
  begin
    Senha := EdSenha.ValueVariant;
    //
    if MyObjects.FIC(Length(Senha) = 0, EdSenha, 'Senha n�o definida!') then Exit;
  end else
    Senha := '';
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('backupdir', 'Codigo', ImgTipo.SQLType, FCodigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'backupdir', False,
  [
    'Caminho', 'Destino', 'FTPWebDir',
    'SubDir', 'Zipar', 'Encrypt',
    'SenhaCrypt', 'Web', 'OmitirExt',
    'ExtOmit'
  ], ['Codigo'],
  [
    Origem, Destino, FTPWebDir,
    SubDir, Zipar, Encrypt,
    Senha, Web, OmitExt,
    Extensoes
  ], [Codigo], True) then
  begin
    if CkContinuar.Checked then
    begin
      Geral.MensagemBox('Dados salvos com sucesso!', 'Aviso', MB_OK+MB_ICONWARNING);
      EdOrigem.ValueVariant := '';
      EdOrigem.SetFocus;
    end else
      Close;
  end;
end;

procedure TFmBackupDirIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmBackupDirIts.CkEncryptarClick(Sender: TObject);
begin
  LaSenha.Visible := CkEncryptar.Checked;
  EdSenha.Visible := CkEncryptar.Checked;
  //
  if not CkEncryptar.Checked then
    EdSenha.ValueVariant := '';
end;

procedure TFmBackupDirIts.CkExtOmitClick(Sender: TObject);
begin
  PnExtOmit.Visible := CkExtOmit.Checked;
end;

procedure TFmBackupDirIts.CkZiparClick(Sender: TObject);
var
  Zipar: Boolean;
begin
  Zipar               := CkZipar.Checked;
  CkEncryptar.Visible := Zipar;
  CkEncryptar.Checked := False;
  LaSenha.Visible     := Zipar;
  EdSenha.Visible     := Zipar; 
end;

procedure TFmBackupDirIts.EdDestinoExit(Sender: TObject);
var
  Dir: String;
begin
  Dir := EdOrigem.ValueVariant;
  if Length(Dir) > 0 then
  begin
    if not DirectoryExists(Dir) then
    begin
      if Geral.MensagemBox(PChar('O diret�rio selecionado n�o existe! Deseja cri�-lo?'),
        'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES
      then begin
        ForceDirectories(Dir);
        if DirectoryExists(Dir) then
          Geral.MensagemBox('Diret�rio criado com sucesso!', 'Aviso', MB_OK+MB_ICONWARNING)
        else
          Geral.MensagemBox('Falha ao criar diret�rios!', 'Aviso', MB_OK+MB_ICONWARNING);
      end;
    end;
  end;
end;

procedure TFmBackupDirIts.EdSenhaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F4 then
  begin
    EdSenha.ValueVariant := VAR_BDSENHA;
  end;
end;

procedure TFmBackupDirIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  //
  MostraEdicao(ImgTipo.SQLType, 0, FWeb);
end;

procedure TFmBackupDirIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmBackupDirIts.MostraEdicao(SQLType: TSQLType; Codigo, WEB: Integer);
begin
  if SQLType = stIns then
  begin
    EdCodigo.ValueVariant := FormatFloat(FFormatFloat, Codigo);
    EdOrigem.ValueVariant := '';
    CkSubDir.Checked      := True;
    CkZipar.Checked       := False;
    CkEncryptar.Checked   := False;
    EdSenha.ValueVariant  := '';
    LaSenha.Visible       := False;
    EdSenha.Visible       := False;
    CkExtOmit.Checked     := True;
    PnExtOmit.Visible     := True;
    RGWeb.Enabled         := False;
    CkContinuar.Checked   := False;
    CkContinuar.Visible   := True;
    //
    EdDestino.ValueVariant   := '';
    EdFTPWebDir.ValueVariant := 0;
    CBFTPWebDir.KeyValue     := Null;
    //
    if WEB = 0 then //Local
    begin
      RGWeb.ItemIndex  := 0;
      PnDirLoc.Visible := True;
      PnDirWeb.Visible := False;
    end else
    begin // WEB
      RGWeb.ItemIndex  := 1;
      PnDirLoc.Visible := False;
      PnDirWeb.Visible := True;
    end;
  end else
  begin
    EdCodigo.ValueVariant := FCodigo;
    EdOrigem.ValueVariant := FCaminho;
    CkSubDir.Checked      := MLAGeral.ITB(FSubDir);
    CkZipar.Checked       := MLAGeral.ITB(FZipar);
    CkEncryptar.Visible   := FEncrypt > 0;
    LaSenha.Visible       := FEncrypt > 0;
    EdSenha.Visible       := FEncrypt > 0;
    CkEncryptar.Checked   := MLAGeral.ITB(FEncrypt);
    EdSenha.ValueVariant  := FSenhaCrypt;
    RGWeb.Enabled         := False;
    CkContinuar.Checked   := False;
    CkContinuar.Visible   := False;
    //
    EdDestino.ValueVariant   := FDirDestino;
    EdFTPWebDir.ValueVariant := FFTPWebDir;
    CBFTPWebDir.KeyValue     := FFTPWebDir;
    //
    if WEB = 0 then //Local
    begin
      RGWeb.ItemIndex  := 0;
      PnDirLoc.Visible := True;
      PnDirWeb.Visible := False;
    end else
    begin // WEB
      RGWeb.ItemIndex  := 1;
      PnDirLoc.Visible := False;
      PnDirWeb.Visible := True;
    end;
  end;
  EdOrigem.SetFocus;
end;

procedure TFmBackupDirIts.SpeedButton1Click(Sender: TObject);
var
  Dir: string;
  Cam: integer;
begin
  MyObjects.DefineDiretorio(Self, EdDestino);
  Dir := EdDestino.ValueVariant;
  //
  Cam := Length(Dir);
  if Cam > 0 then
  begin
    if Copy(Dir, Cam, 1) = '\' then
      EdDestino.ValueVariant := Copy(Dir, 1, Cam - 1)
    else
      EdDestino.ValueVariant := Dir;
  end;
end;

procedure TFmBackupDirIts.SpeedButton2Click(Sender: TObject);
var
  Dir: string;
  Cam: integer;
begin
  MyObjects.DefineDiretorio(Self, EdOrigem);
  Dir := EdOrigem.ValueVariant;
  //
  Cam := Length(Dir);
  if Cam > 0 then
  begin
    if Copy(Dir, Cam, 1) = '\' then
      EdOrigem.ValueVariant := Copy(Dir, 1, Cam - 1)
    else
      EdOrigem.ValueVariant := Dir;
  end;
end;

end.
