unit VersaoGer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkDBLookupComboBox, dmkEdit,
  dmkEditCB, dmkDBGrid, Vcl.Menus, VCLTee.TeCanvas, VCLTee.TeePenDlg,
  VCLTee.TeEngine, VCLTee.TeeProcs, VCLTee.Chart, VCLTee.Series,
  VCLTee.TeeDraw3D, VCLTee.TeeGDIPlus, VCLTee.DBChart, mySQLDbTables,
  dmkCompoStore, dmkPermissoes, System.Variants;

type
  TFmVersaoGer = class(TForm)
    PnCabeca: TPanel;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtMenu: TBitBtn;
    LaTitulo1C: TLabel;
    PnAplicativo: TPanel;
    LaAplicativo: TLabel;
    EdAplicativo: TdmkEditCB;
    CBAplicativo: TdmkDBLookupComboBox;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabControl1: TTabControl;
    PnVersaoCad: TPanel;
    BtUpload: TBitBtn;
    EdVersao: TdmkEdit;
    Label1: TLabel;
    SBAppSel: TSpeedButton;
    EdAppSel: TdmkEdit;
    Label15: TLabel;
    PMMenu: TPopupMenu;
    Moverversoatualpara1: TMenuItem;
    BETA1: TMenuItem;
    Emproduo1: TMenuItem;
    Gerenciargrupodetestesdaversoatual1: TMenuItem;
    N1: TMenuItem;
    Histricodealteraes1: TMenuItem;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    ImgWEB: TdmkImage;
    QrAplicativos: TmySQLQuery;
    IntegerField1: TIntegerField;
    DsAplicativos: TDataSource;
    QrWVersao: TmySQLQuery;
    DsWVersao: TDataSource;
    QrWVersaoVersao: TWideStringField;
    QrWVersaoAplicativo: TIntegerField;
    QrWVersaoTipo: TIntegerField;
    QrWVersaoDataCad: TDateField;
    QrWVersaoAtivo: TSmallintField;
    QrWVersaoCodigo: TIntegerField;
    N2: TMenuItem;
    Excluirversoatual1: TMenuItem;
    QrGrafico: TmySQLQuery;
    QrGraficoVersao: TWideStringField;
    QrGraficoTotal: TFloatField;
    CSTabSheetChamou: TdmkCompoStore;
    dmkPermissoes1: TdmkPermissoes;
    dmkDBGrid1: TdmkDBGrid;
    DBChart1: TDBChart;
    Series2: TPieSeries;
    dmkDBGrid2: TdmkDBGrid;
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    QrGraficoVersaoAtu: TWideStringField;
    QrComputadores: TmySQLQuery;
    DsComputadores: TDataSource;
    QrComputadoresNomePC: TWideStringField;
    QrComputadoresVersaoAtu: TWideStringField;
    QrComputadoresCliente_TXT: TWideStringField;
    QrComputadoresDataVerifi: TDateTimeField;
    QrComputadoresPCSuporte: TSmallintField;
    QrLoc: TmySQLQuery;
    QrAplicativosNome: TWideStringField;
    QrWVersaoArqWeb: TIntegerField;
    QrWVersaoArquivo: TWideStringField;
    QrWVersaoCaminho: TWideStringField;
    QrWVersaoFTPConfig: TIntegerField;
    QrWVersaoOrigem: TIntegerField;
    QrWControl: TmySQLQuery;
    Baixarverso1: TMenuItem;
    SaveFile: TSaveDialog;
    SbReabre: TBitBtn;
    EdSubVersao: TdmkEdit;
    Label2: TLabel;
    QrWVersaoSubVersao: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Chart1ClickSeries(Sender: TCustomChart; Series: TChartSeries;
      ValueIndex: Integer; Button: TMouseButton; Shift: TShiftState; X,
      Y: Integer);
    procedure DBChart1ClickSeries(Sender: TCustomChart; Series: TChartSeries;
      ValueIndex: Integer; Button: TMouseButton; Shift: TShiftState; X,
      Y: Integer);
    procedure EdAplicativoChange(Sender: TObject);
    procedure BtMenuClick(Sender: TObject);
    procedure BtUploadClick(Sender: TObject);
    procedure SBAppSelClick(Sender: TObject);
    procedure dmkDBGrid1CellClick(Column: TColumn);
    procedure Excluirversoatual1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TabControl1Change(Sender: TObject);
    procedure Histricodealteraes1Click(Sender: TObject);
    procedure PMMenuPopup(Sender: TObject);
    procedure Emproduo1Click(Sender: TObject);
    procedure BETA1Click(Sender: TObject);
    procedure Gerenciargrupodetestesdaversoatual1Click(Sender: TObject);
    procedure Baixarverso1Click(Sender: TObject);
    procedure SbReabreClick(Sender: TObject);
  private
    { Private declarations }
    procedure MostraEdicao(Mostra: Boolean);
    procedure ReopenWVersao(Aplicativo, Tipo, Codigo: Integer);
    procedure AtualizaAtivo(Codigo, Aplicativo, Tipo, Ativo: Integer);
    procedure AtualizaTipo(Tipo: Integer);
    procedure ConfiguraGrafico();
    procedure ReopenComputadores(Aplicativo: Integer; Versao: String);
    procedure ReopenWContrrol();
    procedure MostraVersaoApp(Aplicativo: Integer);
    function  VerificaSeVersaoExiste(Aplicativo: Integer; Versao: String): Boolean;
  public
    { Public declarations }
  end;

  var
  FmVersaoGer: TFmVersaoGer;

implementation

uses UnMyObjects, Module, UnDmkWeb, UMySQLModule, DmkDAC_PF, UnMLAGeral,
  MyGlyfs, Principal, UnFTP, MyDBCheck, VersaoGerIts, MyListas, UnitVersoes_Jan;

{$R *.DFM}

procedure TFmVersaoGer.AtualizaAtivo(Codigo, Aplicativo, Tipo, Ativo: Integer);
begin
  if Ativo = 1 then
  begin
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpdN, Dmod.MyDBn, [
      'UPDATE wversao SET Ativo=0 ',
      'WHERE Aplicativo=' + Geral.FF0(Aplicativo) + ' ',
      'AND Tipo=' + Geral.FF0(Tipo) + ' ',
      '']);
  end;
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpdN, Dmod.MyDBn, [
    'UPDATE wversao SET Ativo="' + Geral.FF0(Ativo) + '"',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    '']);
end;

procedure TFmVersaoGer.BtUploadClick(Sender: TObject);
var
  Codigo, Aplicativo, Tipo, ArqWeb, AppZipar, WebDirApp, CliInt, Nivel: Integer;
  Versao, SubVersao, Arquivo, Nome, NomeArq, Caminho: String;
  Arquivos: TStringList;
begin
  try
    BtUpload.Enabled := False;
    //
    ReopenWContrrol;
    //
    AppZipar  := QrWControl.FieldByName('AppZipar').AsInteger;
    WebDirApp := QrWControl.FieldByName('WebDirApp').AsInteger;
    //
    if MyObjects.FIC(WebDirApp = 0, nil, 'Diret�rio padr�o n�o definido nas op��es WEB do aplicativo!') then Exit;
    //
    if TabControl1.TabIndex = 0 then
      Tipo := 1
    else
      Tipo := 0;
    //
    Aplicativo := EdAplicativo.ValueVariant;
    Versao     := EdVersao.ValueVariant;
    SubVersao  := EdSubVersao.ValueVariant;
    ArqWeb     := 0;
    Arquivo    := EdAppSel.ValueVariant;
    //
    if MyObjects.FIC(Aplicativo = 0, EdAplicativo, 'Defina o aplicativo!') then Exit;
    if MyObjects.FIC(Versao = '', EdVersao, 'Defina a vers�o!') then Exit;
    if MyObjects.FIC(Arquivo = '', EdAppSel, 'Defina o arquivo do execut�vel!') then Exit;
    if MyObjects.FIC(VerificaSeVersaoExiste(Aplicativo, Versao), nil, 'Esta vers�o j� foi cadastrada!') then Exit;
    //
    Nome    := ExtractFileName(Arquivo);
    Nome    := Copy(Nome, 1, length(Nome) - length(ExtractFileExt(Arquivo)));
    NomeArq := Nome + '_' + Geral.SoNumero_TT(Versao);
    //
    if Trim(LowerCase(Nome)) <> Trim(LowerCase(QrAplicativosNome.Value)) then
    begin
      if Geral.MB_Pergunta('O execut�vel selecionado tem nome diferente do aplicativo selecionado!'
        + sLineBreak + 'Deseja continuar?' + sLineBreak + sLineBreak +
        'Nome do aplicativo: ' + QrAplicativosNome.Value + sLineBreak +
        'Nome do arquivo selecionado: ' + Nome) <> ID_YES
      then
        Exit;
    end;
    if AppZipar = 1 then
    begin
      if not FmPrincipal.ZipaArquivo(Arquivo, NomeArq, False, '', False) then
      begin
        Geral.MB_Aviso('Falha ao zipar arquivo!');
        Exit;
      end;
    end;
    try
      Arquivos := TStringList.Create;
      Arquivos.Add(Arquivo);
      //
      Caminho := QrWControl.FieldByName('Caminho').AsString;
      CliInt  := QrWControl.FieldByName('CliInt').AsInteger;
      Nivel   := QrWControl.FieldByName('Nivel').AsInteger;
      ArqWeb  := UFTP.UploadFTP_DB(FmPrincipal.PageControl1,
                   FmPrincipal.AdvToolBarPager1, stIns, ExtractFileName(Arquivo),
                   NomeArq, '', '', 0, CO_DMKID_APP,
                   QrWControl.FieldByName('FTPConfig').AsInteger, 1,
                   QrWControl.FieldByName('WebDirApp').AsInteger, CliInt, Nivel,
                   dtApp, 0, 0, Arquivos, False, Dmod.QrUpdN, Dmod.QrAuxN,
                   Dmod.MyDBn, Caminho, True);
    finally
      Arquivos.Free;
    end;
    if ArqWeb <> 0 then
    begin
      //Salva no banco de dados
      Codigo := UMyMod.BuscaNovoCodigo_Int(Dmod.QrUpdN, 'wversao', 'Codigo', [], [],
                  stIns, 0, siPositivo, nil);
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpdN, stIns, 'wversao', False,
        ['Versao', 'SubVersao', 'Aplicativo', 'ArqWeb', 'Tipo', 'Ativo'], ['Codigo'],
        [Versao, SubVersao, Aplicativo, ArqWeb, Tipo, 0], [Codigo], True) then
      begin
        EdAppSel.ValueVariant    := '';
        EdVersao.ValueVariant    := '';
        EdSubVersao.ValueVariant := '';
        EdAppSel.SetFocus;
        //
        ReopenWVersao(Aplicativo, Tipo, Codigo);
      end;
    end;
  finally
    BtUpload.Enabled := True;
  end;
end;

procedure TFmVersaoGer.AtualizaTipo(Tipo: Integer);
var
  Codigo: Integer;
begin
  Codigo := QrWVersaoCodigo.Value;
  //
  if UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpdN, Dmod.MyDBn, [
    'UPDATE wversao SET Ativo=0, Tipo=' + Geral.FF0(Tipo),
    'WHERE Codigo=' + Geral.FF0(Codigo) + ' ',
    '']) then
  begin
    if Tipo = 1 then
      TabControl1.TabIndex := 0
    else
      TabControl1.TabIndex := 1;
    //
    ReopenWVersao(EdAplicativo.ValueVariant, Tipo, Codigo);
  end;
end;

procedure TFmVersaoGer.Baixarverso1Click(Sender: TObject);
var
  DirSave, Host, Usuario, Senha: String;
  Passivo: Integer;
begin
  ReopenWContrrol;
  //
  UFTP.ReopenFTPConfig(QrWControl.FieldByName('FTPConfig').AsInteger,
    CO_DMKID_APP, QrLoc, Dmod.MyDBn);
    //
  Host    := QrLoc.FieldByName('Web_FTPh').AsString;
  Usuario := QrLoc.FieldByName('Web_FTPu').AsString;
  Senha   := QrLoc.FieldByName('SENHA').AsString;
  Passivo := QrLoc.FieldByName('Passivo').AsInteger;

  DirSave := UFTP.DownloadFileIndy(FmPrincipal.PageControl1,
               FmPrincipal.AdvToolBarPager1, SaveFile, QrWVersaoCaminho.Value,
               QrWVersaoArquivo.Value, '', Host, Usuario, Senha,
               Geral.IntToBool(Passivo));
  //
  if Length(DirSave) > 0 then
  begin
    if Geral.MB_Pergunta('Deseja abrir o diret�rio?') = ID_YES then
      Geral.AbreArquivo(DirSave);
  end;
end;

procedure TFmVersaoGer.BETA1Click(Sender: TObject);
begin
  AtualizaTipo(0);
end;

procedure TFmVersaoGer.BtMenuClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMMenu, BtMenu);
end;

procedure TFmVersaoGer.BtSaidaClick(Sender: TObject);
begin
  if TFmVersaoGer(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmVersaoGer.Chart1ClickSeries(Sender: TCustomChart;
  Series: TChartSeries; ValueIndex: Integer; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  //
end;

procedure TFmVersaoGer.ConfiguraGrafico;
var
  Aplicativo, Tipo: Integer;
begin
  Aplicativo := EdAplicativo.ValueVariant;
  //
  if Aplicativo <> 0 then
  begin
    if TabControl1.TabIndex = 0 then
      Tipo := 1
    else
      Tipo := 0;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrGrafico, Dmod.MyDBn, [
      'SELECT CONCAT("Vers�o ", li.VersaoAtu) Versao, li.VersaoAtu, COUNT(li.VersaoAtu) + 0.000 Total ',
      'FROM cliaplicli li ',
      'LEFT JOIN cliaplic ap ON ap.Controle = li.Controle ',
      'LEFT JOIN clientes cl ON cl.Codigo = ap.Codigo ',
      'WHERE ap.Aplicativo=' + Geral.FF0(Aplicativo),
      'AND cl.Ativo = 1 ',
      'AND VersaoAtu IN ( ',
      'SELECT Versao ',
      'FROM wversao ',
      'WHERE Tipo=' + Geral.FF0(Tipo),
      'AND Aplicativo=' + Geral.FF0(Aplicativo),
      ') ',
      'GROUP BY li.VersaoAtu ',
      ' ',
      'UNION ',
      ' ',
      'SELECT "Outros" Versao, "" VersaoAtu, COUNT(li.VersaoAtu) + 0.000 Total ',
      'FROM cliaplicli li ',
      'LEFT JOIN cliaplic ap ON ap.Controle = li.Controle ',
      'LEFT JOIN clientes cl ON cl.Codigo = ap.Codigo ',
      'WHERE ap.Aplicativo=' + Geral.FF0(Aplicativo),
      'AND VersaoAtu NOT IN ( ',
      'SELECT Versao ',
      'FROM wversao ',
      'WHERE Tipo=' + Geral.FF0(Tipo),
      'AND Aplicativo=' + Geral.FF0(Aplicativo),
      ') ',
      'AND cl.Ativo = 1 ',
      'ORDER BY Total DESC ',
      '']);
  end;
end;

procedure TFmVersaoGer.DBChart1ClickSeries(Sender: TCustomChart;
  Series: TChartSeries; ValueIndex: Integer; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  Aplicativo: Integer;
  Versao: String;
begin
  QrGrafico.RecNo := ValueIndex + 1;
  //
  Aplicativo := EdAplicativo.ValueVariant;
  Versao     := QrGraficoVersaoAtu.Value;
  //
  ReopenComputadores(Aplicativo, Versao);
  QrWVersao.Locate('Aplicativo;Versao', VarArrayOf([Aplicativo, Versao]), []);
end;

procedure TFmVersaoGer.dmkDBGrid1CellClick(Column: TColumn);
var
  Codigo, Aplicativo, Tipo, Ativo: Integer;
begin
  if Column.FieldName = 'Ativo' then
  begin
    if (QrWVersao.State <> dsInactive) and (QrWVersao.RecordCount > 0) then
    begin
      Codigo     := QrWVersaoCodigo.Value;
      Aplicativo := QrWVersaoAplicativo.Value;
      Tipo       := QrWVersaoTipo.Value;
      //
      if QrWVersaoAtivo.Value = 0 then
        Ativo := 1
      else
        Ativo := 0;
      //
      AtualizaAtivo(Codigo, Aplicativo, Tipo, Ativo);
      ReopenWVersao(Aplicativo, Tipo, Codigo);
    end;
  end;
end;

procedure TFmVersaoGer.EdAplicativoChange(Sender: TObject);
begin
  MostraVersaoApp(EdAplicativo.ValueVariant);
end;

procedure TFmVersaoGer.Emproduo1Click(Sender: TObject);
begin
  AtualizaTipo(1);
end;

procedure TFmVersaoGer.Excluirversoatual1Click(Sender: TObject);
var
  Codigo, Aplicativo, Tipo, ArqWeb, FTPConfig: Integer;
  Caminho, Arquivo: String;
  Origem: TOrigFTP;
begin
  if Geral.MB_Pergunta('Confirma a exclus�o da vers�o atual?' + sLineBreak +
    'ATEN��O: O arquivo do execut�vel tamb�m ser� removido do servidor!') = ID_YES then
  begin
    Codigo     := QrWVersaoCodigo.Value;
    Aplicativo := QrWVersaoAplicativo.Value;
    Tipo       := QrWVersaoTipo.Value;
    ArqWeb     := QrWVersaoArqWeb.Value;
    Arquivo    := QrWVersaoArquivo.Value;
    Caminho    := QrWVersaoCaminho.Value;
    FTPConfig  := QrWVersaoFTPConfig.Value;
    Origem     := UFTP.ConverteOrigem(QrWVersaoOrigem.Value);
    //
    UFTP.ReopenFTPConfig(FTPConfig, CO_DMKID_APP, QrLoc, Dmod.MyDBn);
    //
    if UFTP.DeletaArquvivoFTP(Arquivo, Caminho, QrLoc.FieldByName('Web_FTPh').AsString,
      QrLoc.FieldByName('Web_FTPu').AsString, QrLoc.FieldByName('SENHA').AsString,
      QrLoc.FieldByName('Passivo').AsInteger, ArqWeb, Origem, Dmod.MyDBn, True) then
    begin
      if UMyMod.ExcluiRegistroInt1('', 'wversao', 'Codigo', Codigo, Dmod.MyDBn) = ID_YES then
      begin
        UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpdN, Dmod.MyDBn, [
          'DELETE FROM wversaoits ',
          'WHERE Codigo=' + Geral.FF0(Codigo),
          '']);
        //
        ReopenWVersao(Aplicativo, Tipo, Codigo);
      end;
    end else
      Geral.MB_Aviso('Falha ao excluir!');
  end;
end;

procedure TFmVersaoGer.FormActivate(Sender: TObject);
begin
  if TFmVersaoGer(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end;
  DmkWeb.CarregaImagemFormWEB(ImgWEB);
end;

procedure TFmVersaoGer.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UMyMod.AbreQuery(QrAplicativos, Dmod.MyDB);
  //
  MostraEdicao(False);
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False,
    'Clique no gr�fico para atualizar a lista dos computadores!');
end;

procedure TFmVersaoGer.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVersaoGer.FormShow(Sender: TObject);
begin
  FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
end;

procedure TFmVersaoGer.Gerenciargrupodetestesdaversoatual1Click(
  Sender: TObject);
begin
  if DBCheck.CriaFm(TFmVersaoGerIts, FmVersaoGerIts, afmoNegarComAviso) then
  begin
    FmVersaoGerIts.FCodigo     := QrWVersaoCodigo.Value;
    FmVersaoGerIts.FAplicativo := QrWVersaoAplicativo.Value;
    FmVersaoGerIts.ShowModal;
    FmVersaoGerIts.Destroy;
  end;
end;

procedure TFmVersaoGer.Histricodealteraes1Click(Sender: TObject);
begin
  UnVersoes_Jan.MostraWHisAlt(True, FmPrincipal.PageControl1,
    FmPrincipal.AdvToolBarPager1);
end;

procedure TFmVersaoGer.MostraEdicao(Mostra: Boolean);
begin
  if Mostra then
  begin
    EdAppSel.ValueVariant := False;
    EdVersao.ValueVariant := False;
    //
    dmkDBGrid1.PopupMenu := PMMenu;
    TabControl1.TabIndex := 0;
    PageControl1.Visible := True;
  end else
  begin
    PageControl1.Visible := False;
  end;
end;

procedure TFmVersaoGer.PMMenuPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrWVersao.State <> dsInactive) and (QrWVersao.RecordCount > 0);
  //
  Moverversoatualpara1.Enabled                := Enab;
  Gerenciargrupodetestesdaversoatual1.Enabled := Enab;
  Excluirversoatual1.Enabled                  := Enab;
  Baixarverso1.Enabled                        := Enab;
  //
  if Enab then
  begin
    Emproduo1.Visible                           := QrWVersaoTipo.Value = 0;
    BETA1.Visible                               := QrWVersaoTipo.Value = 1;
    Gerenciargrupodetestesdaversoatual1.Enabled := QrWVersaoTipo.Value = 0;
  end;
end;

procedure TFmVersaoGer.MostraVersaoApp(Aplicativo: Integer);
var
  Tipo: Integer;
begin
  if Aplicativo <> 0 then
  begin
    if TabControl1.TabIndex = 0 then
      Tipo := 1
    else
      Tipo := 0;
    //
    ReopenWVersao(Aplicativo, Tipo, 0);
    //
    PageControl1.Visible := True;
  end;
end;

procedure TFmVersaoGer.ReopenComputadores(Aplicativo: Integer; Versao: String);
begin
  if Versao <> '' then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrComputadores, Dmod.MyDBn, [
      'SELECT li.NomePC, li.VersaoAtu, li.PCSuporte, ',
      'IF(en.Tipo=0, en.RazaoSocial, en.Nome) Cliente_TXT, ',
      'IF(li.Cadastro > li.ConsultaDH, li.Cadastro, li.ConsultaDH) DataVerifi ',
      'FROM cliaplicli li ',
      'LEFT JOIN cliaplic ap ON ap.Controle = li.Controle ',
      'LEFT JOIN clientes cl ON cl.Codigo = ap.Codigo ',
      'LEFT JOIN entidades en ON en.Codigo = cl.Cliente ',
      'WHERE li.VersaoAtu = "' + Versao + '" ',
      'AND ap.Aplicativo = ' + Geral.FF0(Aplicativo),
      'AND cl.Ativo = 1 ',
      'ORDER BY Cliente_TXT, li.VersaoAtu, li.NomePC ',
      '']);
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrComputadores, Dmod.MyDBn, [
      'SELECT li.NomePC, li.VersaoAtu, li.PCSuporte, ',
      'IF(en.Tipo=0, en.RazaoSocial, en.Nome) Cliente_TXT, ',
      'IF(li.Cadastro > li.ConsultaDH, li.Cadastro, li.ConsultaDH) DataVerifi ',
      'FROM cliaplicli li ',
      'LEFT JOIN cliaplic ap ON ap.Controle = li.Controle ',
      'LEFT JOIN clientes cl ON cl.Codigo = ap.Codigo ',
      'LEFT JOIN entidades en ON en.Codigo = cl.Cliente ',
      'WHERE li.VersaoAtu NOT IN ',
      '( ',
      'SELECT Versao ',
      'FROM wversao ',
      'WHERE Aplicativo=' + Geral.FF0(Aplicativo),
      ') ',
      'AND ap.Aplicativo = ' + Geral.FF0(Aplicativo),
      'AND cl.Ativo = 1 ',
      'ORDER BY Cliente_TXT, li.VersaoAtu, li.NomePC ',
      '']);
  end;
end;

procedure TFmVersaoGer.ReopenWContrrol;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrWControl, Dmod.MyDBn, [
    'SELECT wc.AppZipar, wc.WebDirApp, ',
    'fd.Caminho, fd.FTPConfig, fd.CliInt, fd.Nivel ',
    'FROM wcontrol wc ',
    'LEFT JOIN ftpwebdir fd ON fd.Codigo = wc.WebDirApp ',
    '']);
end;

procedure TFmVersaoGer.ReopenWVersao(Aplicativo, Tipo, Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrWVersao, Dmod.MyDBn, [
    'SELECT wve.Codigo, wve.Versao, wve.Aplicativo, wve.SubVersao, ',
    'wve.Tipo, IF(wve.Ativo=0, "Ativado", "Desativado") Ativo_TXT, ',
    'wve.DataCad, wve.Ativo, wve.ArqWeb, arq.Arquivo, ',
    'dir.Caminho, dir.FTPConfig, arq.Origem ',
    'FROM wversao wve ',
    'LEFT JOIN ftpwebarq arq ON arq.Codigo = wve.ArqWeb ',
    'LEFT JOIN ftpwebdir dir ON dir.Codigo = arq.FTPDir ',
    'WHERE Aplicativo=' + Geral.FF0(Aplicativo),
    'AND Tipo=' + Geral.FF0(Tipo),
    'ORDER BY wve.Versao DESC ',
    '']);
  if QrWVersao.RecordCount > 0 then
  begin
    ConfiguraGrafico;
    QrComputadores.Close;
    //
    DBChart1.Visible   := True;
    dmkDBGrid2.Visible := True;
  end else
  begin
    QrGrafico.Close;
    QrComputadores.Close;
    //
    DBChart1.Visible   := False;
    dmkDBGrid2.Visible := False;
  end;
  //
  EdAppSel.ValueVariant    := '';
  EdVersao.ValueVariant    := '';
  EdSubVersao.ValueVariant := '';
  //
  if Codigo <> 0 then
    QrWVersao.Locate('Codigo', Codigo, []);
end;

procedure TFmVersaoGer.SBAppSelClick(Sender: TObject);
begin
  MyObjects.DefineArquivo1(Self, EdAppSel);
  //
  if EdAppSel.ValueVariant <> '' then
  begin
    EdVersao.ValueVariant := MLAGeral.GetVersion(EdAppSel.ValueVariant, True);
    //
    EdVersao.SetFocus;
  end;
end;

procedure TFmVersaoGer.SbReabreClick(Sender: TObject);
begin
  MostraVersaoApp(EdAplicativo.ValueVariant);
end;

procedure TFmVersaoGer.TabControl1Change(Sender: TObject);
var
  Tipo: Integer;
begin
  if TabControl1.TabIndex = 0 then
    Tipo := 1
  else
    Tipo := 0;
  //
  ReopenWVersao(EdAplicativo.ValueVariant, Tipo, 0);
end;

function TFmVersaoGer.VerificaSeVersaoExiste(Aplicativo: Integer;
  Versao: String): Boolean;
begin
  Result := False;
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDBn, [
      'SELECT Codigo ',
      'FROM wversao ',
      'WHERE Aplicativo=' + Geral.FF0(Aplicativo),
      'AND Versao="' + Versao + '"',
      '']);
    if QrLoc.RecordCount > 0 then
      Result := True;
  finally
    QrLoc.Close;
  end;
end;

end.
