object FmWHisAlt: TFmWHisAlt
  Left = 368
  Top = 194
  Caption = 'WEB-HISAL-002 :: Hist'#243'rico de Altera'#231#245'es'
  ClientHeight = 692
  ClientWidth = 1088
  Color = clBtnFace
  Constraints.MinHeight = 320
  Constraints.MinWidth = 788
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 120
  TextHeight = 16
  object PnEdita: TPanel
    Left = 0
    Top = 118
    Width = 1088
    Height = 574
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1088
      Height = 80
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 20
        Top = 20
        Width = 16
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'ID:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label8: TLabel
        Left = 96
        Top = 20
        Width = 182
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Tipo de hist'#243'rico de altera'#231#227'o:'
      end
      object SBHisAltTip: TSpeedButton
        Left = 572
        Top = 39
        Width = 26
        Height = 26
        Hint = 'Inclui item de carteira'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        OnClick = SBHisAltTipClick
      end
      object EdCodigo: TdmkEdit
        Left = 20
        Top = 39
        Width = 69
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdWHisAltTip: TdmkEditCB
        Left = 96
        Top = 39
        Width = 69
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'HisAltTip'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBWHisAltTip
        IgnoraDBLookupComboBox = False
      end
      object CBWHisAltTip: TdmkDBLookupComboBox
        Left = 166
        Top = 39
        Width = 400
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsWHisAltTip
        TabOrder = 2
        dmkEditCB = EdWHisAltTip
        QryCampo = 'HisAltTip'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object CkFinalizado: TdmkCheckBox
        Left = 604
        Top = 42
        Width = 80
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Finalizado'
        TabOrder = 3
        QryCampo = 'Finalizado'
        UpdCampo = 'Finalizado'
        UpdType = utYes
        ValCheck = '1'
        ValUncheck = '0'
        OldValor = #0
      end
      object CkContinuar: TdmkCheckBox
        Left = 698
        Top = 42
        Width = 80
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Continuar'
        TabOrder = 4
        UpdType = utYes
        ValCheck = '1'
        ValUncheck = '0'
        OldValor = #0
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 496
      Width = 1088
      Height = 78
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 2
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 15
        Top = 21
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 916
        Top = 18
        Width = 170
        Height = 58
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 9
          Top = 2
          Width = 147
          Height = 50
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object MeNome: TdmkMemo
      Left = 0
      Top = 80
      Width = 1088
      Height = 167
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      TabOrder = 1
      QryCampo = 'Nome'
      UpdCampo = 'Nome'
      UpdType = utYes
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 118
    Width = 1088
    Height = 574
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBCntrl: TGroupBox
      Left = 0
      Top = 495
      Width = 1088
      Height = 79
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 0
      object Panel3: TPanel
        Left = 913
        Top = 18
        Width = 173
        Height = 59
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 12
          Top = 5
          Width = 148
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtInclui: TBitBtn
        Tag = 10
        Left = 12
        Top = 23
        Width = 148
        Height = 50
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Inclui'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtIncluiClick
      end
      object BtAltera: TBitBtn
        Tag = 11
        Left = 165
        Top = 23
        Width = 148
        Height = 50
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Altera'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtAlteraClick
      end
      object BtExclui: TBitBtn
        Tag = 12
        Left = 318
        Top = 23
        Width = 147
        Height = 50
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Exclui'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = BtExcluiClick
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 0
      Width = 1088
      Height = 474
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      ActivePage = TabSheet1
      Align = alTop
      TabHeight = 25
      TabOrder = 1
      OnChange = PageControl1Change
      object TabSheet1: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastro'
        object Panel6: TPanel
          Left = 710
          Top = 0
          Width = 370
          Height = 439
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object DBGWHisAltIts: TDBGrid
            Left = 0
            Top = 0
            Width = 370
            Height = 204
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            DataSource = DsWHisAltIts
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -15
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'NOMEAPL'
                Title.Caption = 'Aplicativo'
                Width = 130
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VersaoApp'
                Title.Caption = 'Vers'#227'o'
                Width = 100
                Visible = True
              end>
          end
          object PnAplicMenu: TPanel
            Left = 0
            Top = 204
            Width = 370
            Height = 59
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alBottom
            TabOrder = 1
            object BtInclui2: TBitBtn
              Tag = 10
              Left = 4
              Top = 5
              Width = 49
              Height = 49
              Cursor = crHandPoint
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtInclui2Click
            end
            object BtAltera2: TBitBtn
              Tag = 11
              Left = 55
              Top = 5
              Width = 50
              Height = 49
              Cursor = crHandPoint
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 1
              OnClick = BtAltera2Click
            end
            object BtExclui2: TBitBtn
              Tag = 12
              Left = 106
              Top = 5
              Width = 49
              Height = 49
              Cursor = crHandPoint
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 2
              OnClick = BtExclui2Click
            end
          end
          object PnAplic: TPanel
            Left = 0
            Top = 263
            Width = 370
            Height = 176
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alBottom
            TabOrder = 2
            Visible = False
            object Label1: TLabel
              Left = 11
              Top = 7
              Width = 62
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Aplicativo:'
            end
            object Label11: TLabel
              Left = 11
              Top = 60
              Width = 47
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Vers'#227'o:'
            end
            object SpeedButton1: TSpeedButton
              Left = 199
              Top = 80
              Width = 26
              Height = 26
              Hint = 'Inclui item de carteira'
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '...'
              OnClick = SpeedButton1Click
            end
            object Panel9: TPanel
              Left = 1
              Top = 116
              Width = 368
              Height = 59
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alBottom
              ParentBackground = False
              TabOrder = 4
              object BtConfirma2: TBitBtn
                Tag = 14
                Left = 10
                Top = 6
                Width = 148
                Height = 49
                Cursor = crHandPoint
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = '&Confirma'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -15
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                NumGlyphs = 2
                ParentFont = False
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtConfirma2Click
              end
              object Panel10: TPanel
                Left = 199
                Top = 1
                Width = 168
                Height = 57
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alRight
                BevelOuter = bvNone
                TabOrder = 1
                object BtDesiste2: TBitBtn
                  Tag = 15
                  Left = 9
                  Top = 5
                  Width = 147
                  Height = 49
                  Cursor = crHandPoint
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = '&Desiste'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -15
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  NumGlyphs = 2
                  ParentFont = False
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                  OnClick = BtDesiste2Click
                end
              end
            end
            object EdAplic: TdmkEditCB
              Left = 11
              Top = 27
              Width = 69
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdAplicChange
              DBLookupComboBox = CBAplic
              IgnoraDBLookupComboBox = False
            end
            object CBAplic: TdmkDBLookupComboBox
              Left = 80
              Top = 27
              Width = 277
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsAplic
              TabOrder = 1
              dmkEditCB = EdAplic
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object CkContIts: TdmkCheckBox
              Left = 231
              Top = 82
              Width = 80
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Continuar'
              TabOrder = 3
              UpdType = utYes
              ValCheck = '1'
              ValUncheck = '0'
              OldValor = #0
            end
            object CBVersaoApp: TComboBox
              Left = 11
              Top = 80
              Width = 185
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              AutoDropDown = True
              TabOrder = 2
            end
          end
        end
        object Panel2: TPanel
          Left = 0
          Top = 0
          Width = 710
          Height = 439
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object Splitter4: TSplitter
            Left = 0
            Top = 317
            Width = 710
            Height = 12
            Cursor = crVSplit
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alBottom
            ExplicitTop = 309
            ExplicitWidth = 708
          end
          object Panel7: TPanel
            Left = 0
            Top = 0
            Width = 710
            Height = 169
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            ParentBackground = False
            TabOrder = 0
            object Label2: TLabel
              Left = 11
              Top = 59
              Width = 69
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Data inicial:'
            end
            object Label3: TLabel
              Left = 160
              Top = 59
              Width = 59
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Data final:'
            end
            object Label4: TLabel
              Left = 11
              Top = 7
              Width = 65
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Descri'#231#227'o:'
            end
            object TPDataI: TdmkEditDateTimePicker
              Left = 11
              Top = 80
              Width = 142
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Date = 40017.908086238430000000
              Time = 40017.908086238430000000
              TabOrder = 1
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
            end
            object TPDataF: TdmkEditDateTimePicker
              Left = 160
              Top = 80
              Width = 142
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Date = 40017.908141423620000000
              Time = 40017.908141423620000000
              TabOrder = 2
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
            end
            object BtReabre: TBitBtn
              Tag = 18
              Left = 11
              Top = 113
              Width = 148
              Height = 49
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '&Reabre'
              NumGlyphs = 2
              TabOrder = 4
              OnClick = BtReabreClick
            end
            object EdDescricao: TdmkEdit
              Left = 11
              Top = 30
              Width = 532
              Height = 25
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = 'EdDescricao'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 'EdDescricao'
              ValWarn = False
            end
            object RGFinalizado: TRadioGroup
              Left = 309
              Top = 59
              Width = 235
              Height = 47
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Finalizado'
              Columns = 3
              ItemIndex = 0
              Items.Strings = (
                'Sim'
                'N'#227'o'
                'Ambos')
              TabOrder = 3
            end
          end
          object DBGWHisAlt: TdmkDBGrid
            Left = 0
            Top = 169
            Width = 710
            Height = 148
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            Columns = <
              item
                Expanded = False
                FieldName = 'NOMETIP'
                Title.Caption = 'Tipo de altera'#231#227'o'
                Width = 221
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Finalizado'
                Title.Caption = 'Finaliz.'
                Width = 37
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DATA'
                Title.Caption = 'Data'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'ID'
                Visible = True
              end>
            Color = clWindow
            DataSource = DsWHisAlt
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
            ParentFont = False
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnCellClick = DBGWHisAltCellClick
            OnDrawColumnCell = DBGWHisAltDrawColumnCell
            Columns = <
              item
                Expanded = False
                FieldName = 'NOMETIP'
                Title.Caption = 'Tipo de altera'#231#227'o'
                Width = 221
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Finalizado'
                Title.Caption = 'Finaliz.'
                Width = 37
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DATA'
                Title.Caption = 'Data'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'ID'
                Visible = True
              end>
          end
          object DBMemo1: TDBMemo
            Left = 0
            Top = 329
            Width = 710
            Height = 110
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alBottom
            DataField = 'Nome'
            DataSource = DsWHisAlt
            ScrollBars = ssVertical
            TabOrder = 2
            OnChange = DBMemo1Change
          end
        end
      end
      object TabSheet2: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Gerencia'
        ImageIndex = 1
        object Splitter2: TSplitter
          Left = 628
          Top = 68
          Width = 12
          Height = 355
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          ExplicitHeight = 363
        end
        object LaTotal: TLabel
          Left = 0
          Top = 423
          Width = 1080
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alBottom
          ExplicitWidth = 3
        end
        object Panel5: TPanel
          Left = 0
          Top = 0
          Width = 1080
          Height = 68
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          ParentBackground = False
          TabOrder = 0
          object Label5: TLabel
            Left = 6
            Top = 11
            Width = 69
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Aplicativos:'
          end
          object Label6: TLabel
            Left = 486
            Top = 11
            Width = 47
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Vers'#227'o:'
          end
          object EdAplicativo: TdmkEditCB
            Left = 6
            Top = 33
            Width = 69
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'HisAltTip'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdAplicativoChange
            DBLookupComboBox = CBAplicativo
            IgnoraDBLookupComboBox = False
          end
          object CBAplicativo: TdmkDBLookupComboBox
            Left = 75
            Top = 33
            Width = 400
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Aplicativo'
            ListField = 'NOMEAPL'
            ListSource = DsWHistAltApl
            TabOrder = 1
            dmkEditCB = EdAplicativo
            QryCampo = 'HisAltTip'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object CBVersao: TComboBox
            Left = 486
            Top = 33
            Width = 185
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            AutoDropDown = True
            TabOrder = 2
            Text = 'CBVersao'
          end
          object BtReabre2: TBitBtn
            Tag = 18
            Left = 678
            Top = 10
            Width = 148
            Height = 49
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Reabre'
            NumGlyphs = 2
            TabOrder = 3
            OnClick = BtReabre2Click
          end
          object BtWEB: TBitBtn
            Tag = 532
            Left = 831
            Top = 10
            Width = 184
            Height = 49
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Visualizar na WEB'
            NumGlyphs = 2
            TabOrder = 4
            OnClick = BtWEBClick
          end
        end
        object dmkDBGrid2: TdmkDBGrid
          Left = 0
          Top = 68
          Width = 628
          Height = 355
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alLeft
          Columns = <
            item
              Expanded = False
              FieldName = 'NOMETIP'
              Title.Caption = 'Tipo de altera'#231#227'o'
              Width = 250
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Finalizado'
              Width = 55
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DATA'
              Title.Caption = 'Data'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'ID'
              Visible = True
            end>
          Color = clWindow
          DataSource = DsWHistAltItens
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          ParentFont = False
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnCellClick = dmkDBGrid2CellClick
          OnDrawColumnCell = dmkDBGrid2DrawColumnCell
          OnDblClick = dmkDBGrid2DblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'NOMETIP'
              Title.Caption = 'Tipo de altera'#231#227'o'
              Width = 250
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Finalizado'
              Width = 55
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DATA'
              Title.Caption = 'Data'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'ID'
              Visible = True
            end>
        end
        object DBMemo2: TDBMemo
          Left = 640
          Top = 68
          Width = 440
          Height = 355
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          DataField = 'Nome'
          DataSource = DsWHistAltItens
          ScrollBars = ssVertical
          TabOrder = 2
          OnChange = DBMemo2Change
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1088
    Height = 64
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 980
      Top = 0
      Width = 108
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 59
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
      object ImgWEB: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 110
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 6
        Top = 6
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object BtEnvia: TBitBtn
        Tag = 244
        Left = 55
        Top = 6
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtEnviaClick
      end
    end
    object GB_M: TGroupBox
      Left = 110
      Top = 0
      Width = 870
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      ExplicitLeft = 62
      ExplicitWidth = 918
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 328
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Hist'#243'rico de Altera'#231#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 328
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Hist'#243'rico de Altera'#231#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 328
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Hist'#243'rico de Altera'#231#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 64
    Width = 1088
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 1084
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 15
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 15
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrWHisAlt: TmySQLQuery
    Database = DModG.AllID_DB
    BeforeOpen = QrWHisAltBeforeOpen
    AfterOpen = QrWHisAltAfterOpen
    BeforeClose = QrWHisAltBeforeClose
    AfterScroll = QrWHisAltAfterScroll
    SQL.Strings = (
      'SELECT alt.*, tip.Nome NOMETIP, tip.Cor,'
      'if ((SELECT COUNT(its.Controle) '
      'FROM whisaltits its '
      'WHERE its.Codigo = alt.Codigo'
      ') > 0, 1, 0) ACHOU,'
      'IF(alt.DataAlt > 0, alt.DataAlt, alt.DataCad) DATA'
      'FROM whisalt alt'
      'LEFT JOIN whisalttip tip ON tip.Codigo = alt.WHisAltTip'
      'ORDER BY Codigo DESC')
    Left = 64
    Top = 64
    object QrWHisAltCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'whisalt.Codigo'
    end
    object QrWHisAltAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'whisalt.AlterWeb'
    end
    object QrWHisAltAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'whisalt.Ativo'
    end
    object QrWHisAltLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'whisalt.Lk'
    end
    object QrWHisAltDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'whisalt.DataCad'
    end
    object QrWHisAltDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'whisalt.DataAlt'
    end
    object QrWHisAltUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'whisalt.UserCad'
    end
    object QrWHisAltUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'whisalt.UserAlt'
    end
    object QrWHisAltACHOU: TLargeintField
      FieldName = 'ACHOU'
      Required = True
    end
    object QrWHisAltWHisAltTip: TIntegerField
      FieldName = 'WHisAltTip'
      Origin = 'whisalt.WHisAltTip'
    end
    object QrWHisAltNOMETIP: TWideStringField
      FieldName = 'NOMETIP'
      Origin = 'whisalttip.Nome'
      Size = 50
    end
    object QrWHisAltFinalizado: TSmallintField
      FieldName = 'Finalizado'
      MaxValue = 1
    end
    object QrWHisAltDATA: TWideStringField
      FieldName = 'DATA'
      Size = 10
    end
    object QrWHisAltNome: TWideMemoField
      FieldName = 'Nome'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrWHisAltCor: TWideStringField
      FieldName = 'Cor'
      Size = 10
    end
  end
  object DsWHisAlt: TDataSource
    DataSet = QrWHisAlt
    Left = 92
    Top = 64
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanIns02 = BtInclui2
    CanUpd01 = BtAltera
    CanUpd02 = BtAltera2
    CanDel01 = BtExclui
    CanDel02 = BtExclui2
    Left = 120
    Top = 64
  end
  object QrWHisAltIts: TmySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT apl.Nome NOMEAPL, its.*'
      'FROM whisaltits its'
      'LEFT JOIN aplicativos apl ON apl.Codigo = its.Aplicativo'
      'WHERE its.Codigo=:P0'
      'ORDER BY NOMEAPL ASC, its.VersaoApp DESC')
    Left = 148
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrWHisAltItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'whisaltits.Codigo'
    end
    object QrWHisAltItsControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'whisaltits.Controle'
    end
    object QrWHisAltItsAplicativo: TIntegerField
      FieldName = 'Aplicativo'
      Origin = 'whisaltits.Aplicativo'
    end
    object QrWHisAltItsLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'whisaltits.Lk'
    end
    object QrWHisAltItsDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'whisaltits.DataCad'
    end
    object QrWHisAltItsDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'whisaltits.DataAlt'
    end
    object QrWHisAltItsUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'whisaltits.UserCad'
    end
    object QrWHisAltItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'whisaltits.UserAlt'
    end
    object QrWHisAltItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'whisaltits.AlterWeb'
    end
    object QrWHisAltItsAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'whisaltits.Ativo'
    end
    object QrWHisAltItsNOMEAPL: TWideStringField
      FieldName = 'NOMEAPL'
      Origin = 'aplicativos.Nome'
      Size = 50
    end
    object QrWHisAltItsVersaoApp: TWideStringField
      FieldName = 'VersaoApp'
      Size = 13
    end
  end
  object DsWHisAltIts: TDataSource
    DataSet = QrWHisAltIts
    Left = 176
    Top = 64
  end
  object DsAplic: TDataSource
    DataSet = QrAplic
    Left = 736
    Top = 185
  end
  object QrWHisAltTip: TmySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT Codigo, Nome, Cor'
      'FROM whisalttip'
      'ORDER BY Nome')
    Left = 764
    Top = 185
    object QrWHisAltTipCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'whisalttip.Codigo'
    end
    object QrWHisAltTipNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'whisalttip.Nome'
      Size = 50
    end
  end
  object DsWHisAltTip: TDataSource
    DataSet = QrWHisAltTip
    Left = 792
    Top = 185
  end
  object QrAplic: TmySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT apl.Codigo, apl.Nome'
      'FROM aplicativos apl'
      'WHERE apl.Codigo NOT IN'
      '('
      'SELECT Aplicativo'
      'FROM whisaltits'
      'WHERE Codigo=:P0'
      ')'
      'AND apl.Ativo = 1'
      'ORDER BY apl.Nome')
    Left = 708
    Top = 185
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
      Origin = 'aplicativos.Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Origin = 'aplicativos.Nome'
      Size = 50
    end
  end
  object QrLoc: TmySQLQuery
    Database = DModG.AllID_DB
    Left = 620
    Top = 169
  end
  object QrWHistAltApl: TmySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT its.Aplicativo, apl.Nome NOMEAPL'
      'FROM whisaltits its'
      'LEFT JOIN aplicativos apl ON apl.Codigo = its.Aplicativo'
      'GROUP BY Aplicativo'
      'ORDER BY NOMEAPL')
    Left = 266
    Top = 330
    object QrWHistAltAplAplicativo: TIntegerField
      FieldName = 'Aplicativo'
      Origin = 'whisaltits.Aplicativo'
    end
    object QrWHistAltAplNOMEAPL: TWideStringField
      FieldName = 'NOMEAPL'
      Origin = 'aplicativos.Nome'
      Size = 50
    end
  end
  object DsWHistAltApl: TDataSource
    DataSet = QrWHistAltApl
    Left = 294
    Top = 330
  end
  object frxDsWHistAltApl: TfrxDBDataset
    UserName = 'frxDsWHistAltApl'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Aplicativo=Aplicativo'
      'NOMEAPL=NOMEAPL')
    DataSet = QrWHistAltApl
    BCDToCurrency = False
    Left = 322
    Top = 330
  end
  object QrWHistAltItens: TmySQLQuery
    Database = DModG.AllID_DB
    BeforeClose = QrWHistAltItensBeforeClose
    AfterScroll = QrWHistAltItensAfterScroll
    SQL.Strings = (
      'SELECT alt.Codigo, alt.Nome, alt.Finalizado, tip.Cor,'
      'tip.Cor, tip.Nome NOMETIP, its.VersaoApp,'
      
        'DATE_FORMAT(IF(alt.DataAlt > 0, alt.DataAlt, alt.DataCad), "%d/%' +
        'm/%Y") DATA'
      'FROM whisalt alt'
      'LEFT JOIN whisaltits its ON its.Codigo = alt.Codigo'
      'LEFT JOIN whisalttip tip ON tip.Codigo = alt.WHisAltTip'
      'WHERE its.VersaoApp=:P0'
      'AND its.Aplicativo=:P1'
      'ORDER BY alt.Nome DESC')
    Left = 350
    Top = 330
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrWHistAltItensCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'whisalt.Codigo'
    end
    object QrWHistAltItensNOMETIP: TWideStringField
      FieldName = 'NOMETIP'
      Origin = 'whisalttip.Nome'
      Size = 50
    end
    object QrWHistAltItensFinalizado: TSmallintField
      FieldName = 'Finalizado'
      MaxValue = 1
    end
    object QrWHistAltItensNome: TWideMemoField
      FieldName = 'Nome'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrWHistAltItensDATA: TWideStringField
      FieldName = 'DATA'
      Size = 10
    end
    object QrWHistAltItensVersaoApp: TWideStringField
      FieldName = 'VersaoApp'
      Size = 13
    end
    object QrWHistAltItensCor: TWideStringField
      FieldName = 'Cor'
      Size = 10
    end
  end
  object DsWHistAltItens: TDataSource
    DataSet = QrWHistAltItens
    Left = 378
    Top = 330
  end
  object frxDsWHistAltItens: TfrxDBDataset
    UserName = 'frxDsWHistAltItens'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'NOMETIP=NOMETIP'
      'Finalizado=Finalizado'
      'Nome=Nome'
      'DATA=DATA'
      'VersaoApp=VersaoApp'
      'Cor=Cor')
    DataSet = QrWHistAltItens
    BCDToCurrency = False
    Left = 406
    Top = 330
  end
  object OpenDialog1: TOpenDialog
    Options = [ofHideReadOnly, ofAllowMultiSelect, ofEnableSizing]
    Left = 332
    Top = 16
  end
  object PMImprime: TPopupMenu
    OnPopup = PMImprimePopup
    Left = 16
    Top = 8
    object Versoatual1: TMenuItem
      Caption = '&Vers'#227'o atual'
      object Somentefinalizados1: TMenuItem
        Caption = '&Somente finalizados'
        OnClick = Somentefinalizados1Click
      end
      object odos1: TMenuItem
        Caption = '&Todos'
        OnClick = odos1Click
      end
    end
  end
  object frxWEB_HISAL_002_001: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39720.627769097200000000
    ReportOptions.LastChange = 41179.720772349500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure CheckBox1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  CheckBox1.Height := Memo8.Height;                             ' +
        '                                              '
      'end;'
      ''
      'begin'
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      'end.')
    Left = 198
    Top = 354
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsWHistAltApl
        DataSetName = 'frxDsWHistAltApl'
      end
      item
        DataSet = frxDsWHistAltItens
        DataSetName = 'frxDsWHistAltItens'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ColumnHeader1: TfrxColumnHeader
        FillType = ftBrush
        Height = 30.125996460000000000
        Top = 98.267780000000000000
        Width = 680.315400000000000000
        object Memo10: TfrxMemoView
          Left = 154.960573780000000000
          Top = 16.897650000000000000
          Width = 434.645827950000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = -0.000278270000000000
          Top = 16.897650000000000000
          Width = 154.960629920000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Tipo de altera'#231#227'o')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 634.960908190000000000
          Top = 16.897650000000000000
          Width = 45.354335590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Finalizado')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 589.606680000000000000
          Top = 16.897650000000000000
          Width = 45.354335590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Width = 680.315299920000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Aplicativo: [frxDsWHistAltApl."NOMEAPL"]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 328.819110000000000000
        Width = 680.315400000000000000
        OnAfterPrint = 'PageFooter1OnAfterPrint'
        object Memo4: TfrxMemoView
          Top = 7.559059999999988000
          Width = 359.055350000000000000
          Height = 13.228346460000000000
          DataSet = DModFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Top = 7.559059999999988000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 56.692950000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape2: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo38: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line9: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo39: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'HIST'#211'RICO DE ALTERA'#199#213'ES')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Top = 37.795300000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 230.551330000000000000
        Width = 680.315400000000000000
        AllowSplit = True
        DataSet = frxDsWHistAltItens
        DataSetName = 'frxDsWHistAltItens'
        RowCount = 0
        Stretched = True
        object Memo3: TfrxMemoView
          Left = 154.960852050000000000
          Width = 434.645827950000000000
          Height = 13.228346460000000000
          StretchMode = smMaxHeight
          AllowHTMLTags = True
          DataField = 'Nome'
          DataSet = frxDsWHistAltItens
          DataSetName = 'frxDsWHistAltItens'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsWHistAltItens."Nome"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Width = 154.960629920000000000
          Height = 13.228346460000000000
          StretchMode = smMaxHeight
          DataField = 'NOMETIP'
          DataSet = frxDsWHistAltItens
          DataSetName = 'frxDsWHistAltItens'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsWHistAltItens."NOMETIP"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 589.606958270000000000
          Width = 45.354335590000000000
          Height = 13.228346460000000000
          StretchMode = smMaxHeight
          DataField = 'DATA'
          DataSet = frxDsWHistAltItens
          DataSetName = 'frxDsWHistAltItens'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsWHistAltItens."DATA"]')
          ParentFont = False
        end
        object CheckBox1: TfrxCheckBoxView
          Left = 634.961040000000000000
          Width = 45.354360000000000000
          Height = 13.228346460000000000
          OnBeforePrint = 'CheckBox1OnBeforePrint'
          CheckColor = clBlack
          CheckStyle = csCheck
          DataField = 'Finalizado'
          DataSet = frxDsWHistAltItens
          DataSetName = 'frxDsWHistAltItens'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Height = 17.007874020000000000
        Top = 188.976500000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsWHistAltItens."VersaoApp"'
        object Memo2: TfrxMemoView
          Left = 37.795300000000000000
          Width = 642.519999920000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Vers'#227'o: [frxDsWHistAltItens."VersaoApp"]')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Top = 268.346630000000000000
        Width = 680.315400000000000000
      end
    end
  end
  object frxDsWHistAltItensFinaliz: TfrxDBDataset
    UserName = 'frxDsWHistAltItensFinaliz'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'NOMETIP=NOMETIP'
      'Finalizado=Finalizado'
      'Nome=Nome'
      'DATA=DATA'
      'VersaoApp=VersaoApp'
      'Cor=Cor')
    DataSet = QrWHistAltItensFinaliz
    BCDToCurrency = False
    Left = 462
    Top = 330
  end
  object QrWHistAltItensFinaliz: TmySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT alt.Codigo, alt.Nome, alt.Finalizado, '
      'tip.Cor, tip.Nome NOMETIP, its.VersaoApp,'
      
        'DATE_FORMAT(IF(alt.DataAlt > 0, alt.DataAlt, alt.DataCad), "%d/%' +
        'm/%Y") DATA'
      'FROM whisalt alt'
      'LEFT JOIN whisaltits its ON its.Codigo = alt.Codigo'
      'LEFT JOIN whisalttip tip ON tip.Codigo = alt.WHisAltTip'
      'WHERE its.VersaoApp=:P0'
      'AND its.Aplicativo=:P1'
      'AND alt.Finalizado = 1'
      'ORDER BY alt.Nome DESC')
    Left = 434
    Top = 330
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object IntegerField3: TIntegerField
      FieldName = 'Codigo'
      Origin = 'whisalt.Codigo'
    end
    object StringField3: TWideStringField
      FieldName = 'NOMETIP'
      Origin = 'whisalttip.Nome'
      Size = 50
    end
    object SmallintField1: TSmallintField
      FieldName = 'Finalizado'
      MaxValue = 1
    end
    object MemoField1: TWideMemoField
      FieldName = 'Nome'
      BlobType = ftWideMemo
      Size = 4
    end
    object StringField4: TWideStringField
      FieldName = 'DATA'
      Size = 10
    end
    object QrWHistAltItensFinalizVersaoApp: TWideStringField
      FieldName = 'VersaoApp'
      Size = 13
    end
    object QrWHistAltItensFinalizCor: TWideStringField
      FieldName = 'Cor'
      Size = 10
    end
  end
  object frxWEB_HISAL_002_002: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39720.627769097200000000
    ReportOptions.LastChange = 41529.455545821760000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      'end.')
    Left = 170
    Top = 354
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsWHistAltApl
        DataSetName = 'frxDsWHistAltApl'
      end
      item
        DataSet = frxDsWHistAltItensFinaliz
        DataSetName = 'frxDsWHistAltItensFinaliz'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ColumnHeader1: TfrxColumnHeader
        FillType = ftBrush
        Height = 30.125996460000000000
        Top = 98.267780000000000000
        Width = 680.315400000000000000
        object Memo10: TfrxMemoView
          Left = 154.960573780000000000
          Top = 16.897650000000000000
          Width = 480.000187950000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = -0.000278270000000000
          Top = 16.897650000000000000
          Width = 154.960629920000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Tipo de altera'#231#227'o')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 634.961040000000000000
          Top = 16.897650000000000000
          Width = 45.354335590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Width = 680.315299920000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Aplicativo: [frxDsWHistAltApl."NOMEAPL"]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 328.819110000000000000
        Width = 680.315400000000000000
        OnAfterPrint = 'PageFooter1OnAfterPrint'
        object Memo4: TfrxMemoView
          Top = 7.559059999999988000
          Width = 359.055350000000000000
          Height = 13.228346460000000000
          DataSet = DModFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Top = 7.559059999999988000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 56.692950000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape2: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo38: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line9: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo39: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'HIST'#211'RICO DE ALTERA'#199#213'ES')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Top = 37.795300000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 230.551330000000000000
        Width = 680.315400000000000000
        AllowSplit = True
        DataSet = frxDsWHistAltItensFinaliz
        DataSetName = 'frxDsWHistAltItensFinaliz'
        RowCount = 0
        Stretched = True
        object Memo3: TfrxMemoView
          Left = 154.960852050000000000
          Width = 480.000187950000000000
          Height = 13.228346460000000000
          StretchMode = smMaxHeight
          AllowHTMLTags = True
          DataField = 'Nome'
          DataSet = frxDsWHistAltItensFinaliz
          DataSetName = 'frxDsWHistAltItensFinaliz'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsWHistAltItensFinaliz."Nome"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Width = 154.960629920000000000
          Height = 13.228346460000000000
          StretchMode = smMaxHeight
          DataField = 'NOMETIP'
          DataSet = frxDsWHistAltItensFinaliz
          DataSetName = 'frxDsWHistAltItensFinaliz'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsWHistAltItensFinaliz."NOMETIP"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 634.961318270000000000
          Width = 45.354335590000000000
          Height = 13.228346460000000000
          StretchMode = smMaxHeight
          DataSet = frxDsWHistAltItensFinaliz
          DataSetName = 'frxDsWHistAltItensFinaliz'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsWHistAltItensFinaliz."DATA"]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Height = 17.007874020000000000
        Top = 188.976500000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsWHistAltItensFinaliz."VersaoApp"'
        object Memo2: TfrxMemoView
          Left = 37.795300000000000000
          Width = 642.519999920000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Vers'#227'o: [frxDsWHistAltItensFinaliz."VersaoApp"]')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Top = 268.346630000000000000
        Width = 680.315400000000000000
      end
    end
  end
  object CSTabSheetChamou: TdmkCompoStore
    Left = 420
    Top = 11
  end
  object PMAplic: TPopupMenu
    OnPopup = PMAplicPopup
    Left = 568
    Top = 240
    object Incluiaplicativo1: TMenuItem
      Caption = '&Inclui aplicativo'
      OnClick = Incluiaplicativo1Click
    end
    object Incluidiversosaplicativos1: TMenuItem
      Caption = 'Inclui &diversos aplicativos'
      OnClick = Incluidiversosaplicativos1Click
    end
  end
end
