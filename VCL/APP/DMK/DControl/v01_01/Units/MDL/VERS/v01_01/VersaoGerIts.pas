unit VersaoGerIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkDBGrid, mySQLDbTables,
  Vcl.Menus, DmkDAC_PF;

type
  TFmVersaoGerIts = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    dmkDBGrid1: TdmkDBGrid;
    QrWVersaoIts: TmySQLQuery;
    DsWVersaoIts: TDataSource;
    BtMenu: TBitBtn;
    PMMenu: TPopupMenu;
    Excluirdispositivo1: TMenuItem;
    Adicionardispositivo1: TMenuItem;
    QrWVersaoItsDispositivo: TIntegerField;
    QrWVersaoItsDescriPC: TWideStringField;
    QrWVersaoItsNomePC: TWideStringField;
    QrWVersaoItsServidor: TSmallintField;
    QrWVersaoItsBeta: TSmallintField;
    QrWVersaoItsPCSuporte: TSmallintField;
    QrWVersaoItsAplicativo_TXT: TWideStringField;
    QrWVersaoItsCliente_TXT: TWideStringField;
    QrWVersaoItsCliente: TIntegerField;
    QrWVersaoItsAplicativo: TIntegerField;
    QrWVersaoItsCodigo: TFloatField;
    QrWVersaoItsControle: TFloatField;
    QrWVersaoItsBetaCliApp: TSmallintField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtMenuClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Excluirdispositivo1Click(Sender: TObject);
    procedure PMMenuPopup(Sender: TObject);
    procedure Adicionardispositivo1Click(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenWVersaoIts();
  public
    { Public declarations }
    FCodigo, FAplicativo: Integer;
  end;

  var
    FmVersaoGerIts: TFmVersaoGerIts;

implementation

uses UnMyObjects, Module, Principal, UMySQLModule, UnitVersoes_Jan;

{$R *.DFM}

procedure TFmVersaoGerIts.Adicionardispositivo1Click(Sender: TObject);
begin
  UnVersoes_Jan.MostraFormWUnLockGeren(0, FAplicativo, True, FmPrincipal.PageControl1,
    FmPrincipal.AdvToolBarPagerNovo);
  Close;
end;

procedure TFmVersaoGerIts.BtMenuClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMMenu, BtMenu);
end;

procedure TFmVersaoGerIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVersaoGerIts.Excluirdispositivo1Click(Sender: TObject);
var
  Controle: Integer;
begin
  Controle := Trunc(QrWVersaoItsControle.Value);
  //
  UMyMod.ExcluiRegistroInt1('Confirma a remo��o do dispositivo da lista atual?',
    'wversaoits', 'Controle', Controle, Dmod.MyDBn);
  ReopenWVersaoIts();
end;

procedure TFmVersaoGerIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVersaoGerIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  dmkDBGrid1.PopupMenu := PMMenu;
end;

procedure TFmVersaoGerIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVersaoGerIts.FormShow(Sender: TObject);
begin
  ReopenWVersaoIts();
end;

procedure TFmVersaoGerIts.PMMenuPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrWVersaoIts.State <> dsInactive) and (QrWVersaoIts.RecordCount > 0);
  //
  Excluirdispositivo1.Enabled := Enab and (QrWVersaoItsControle.Value > 0);
end;

procedure TFmVersaoGerIts.ReopenWVersaoIts();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrWVersaoIts, Dmod.MyDBn, [
    'SELECT its.Codigo, its.Controle, its.Dispositivo, ',
    'ali.DescriPC, ali.NomePC, ali.Servidor, ali.Beta, ',
    'ali.PCSuporte, apl.Nome Aplicativo_TXT, ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) Cliente_TXT, ',
    'cli.Cliente, cap.Aplicativo, cap.Beta BetaCliApp ',
    'FROM wversaoits its ',
    'LEFT JOIN cliaplicli ali ON ali.Conta = its.Dispositivo ',
    'LEFT JOIN cliaplic cap ON cap.Controle = ali.Controle ',
    'LEFT JOIN clientes cli ON cli.Codigo = cap.Codigo ',
    'LEFT JOIN aplicativos apl ON apl.Codigo = cap.Aplicativo ',
    'LEFT JOIN entidades ent ON ent.Codigo = cli.Cliente ',
    'WHERE its.Codigo=' + Geral.FF0(FCodigo),
    ' ',
    'UNION ',
    ' ',
    'SELECT 0 + 0.000 Codigo, 0 + 0.000 Controle, ali.Conta Dispositivo, ',
    'ali.DescriPC, ali.NomePC, ali.Servidor, ali.Beta, ',
    'ali.PCSuporte, apl.Nome Aplicativo_TXT, ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) Cliente_TXT, ',
    'cli.Cliente, cap.Aplicativo, cap.Beta BetaCliApp ',
    'FROM cliaplicli ali ',
    'LEFT JOIN cliaplic cap ON cap.Controle = ali.Controle ',
    'LEFT JOIN clientes cli ON cli.Codigo = cap.Codigo ',
    'LEFT JOIN aplicativos apl ON apl.Codigo = cap.Aplicativo ',
    'LEFT JOIN entidades ent ON ent.Codigo = cli.Cliente ',
    'WHERE (ali.Beta=1 OR cap.Beta=1) ',
    'AND cap.Aplicativo=' + Geral.FF0(FAplicativo),
    'ORDER BY Controle, Dispositivo ',
    '']);
end;

end.
