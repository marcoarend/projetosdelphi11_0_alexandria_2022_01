unit UnitVersoes;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  ComCtrls, dmkGeral, Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db,
  DbCtrls, Variants, mySQLDbTables, dmkEditCB, dmkEdit, dmkRadioGroup,
  UnDmkEnums, UnDmkProcFunc, UnDmkWeb;

type
  TUnitVersoes = class(TObject)
  private
    { Private declarations }
    // A P L I C A T I V O S
    function  App_Valida(TipAmb: Integer; DataBase: TmySQLDatabase;  Codigo,
              Controle, Aplicativo: Integer): Boolean;
  public
    { Public declarations }
    // B A N C O   D E   D A D O S
    procedure BD_ReopenClientes(Query: TMySQLQuery; DataBase: TMySQLDataBase);
    procedure BD_ReopenAplicativos(Query: TMySQLQuery; DataBase: TMySQLDataBase;
              Cliente: Integer);
    // H I S T � R I C O   D E   A L T E R A � � E S
    function  HisAlt_GeraListaHTML(Aplicativo: Integer; VersaoApp: String;
              DataBase: TmySQLDatabase; PB: TProgressBar): String;
    procedure HisAlt_ExcluiHistoricoDeVersoesAntigas(DataBase: TMySQLDataBase; Dias: Integer);
    procedure ReopenWHisAltOpc(QueryOpc: TmySQLQuery; DataBase: TmySQLDatabase);
    // A P L I C A T I V O S
    procedure App_ReopenAplicativos(TipAmb: Integer; DataBase: TmySQLDatabase;
              Query: TmySQLQuery; SQLCompl: String);
    procedure App_ConfiguraVersao(Query: TMySQLQuery; DataBase: TMySQLDataBase;
              ComboVersao: TComboBox; Aplicativo: Integer);
    function  App_InsUpdCliAplicWeb(Codigo, Controle, Aplicativo, QtdLicen,
              QtdCliInt, FTPCfg, DataBaseCfg, Ativo: Integer; FimData: TDate;
              AplicativoTxt: String; DataBase: TmySQLDatabase;
              QueryUpd: TmySQLQuery; EdAplicativo: TdmkEditCB = nil;
              EdDataBaseCfg: TdmkEditCB = nil): Integer;
    function  App_InsUpdCliAplic(Codigo, Controle, Aplicativo, StatAplic,
              QtdLicen, QtdUsuarios, QtdCliInt, Beta, Ativo: Integer;
              FimData: TDate; AplicativoTxt: String; DataBase: TmySQLDatabase;
              QueryUpd: TmySQLQuery; EdAplicativo: TdmkEditCB = nil;
              RGStatAplic: TdmkRadioGroup = nil): Integer;
    function  App_ObtemDescriTipAmb(TipAmb: Integer): String;
    function  App_ObtemDescriStatus(Status: Integer): String;
    function  App_ObtemCategoria(Categoria: Integer): String;
    function  App_ObtemListaCategorias(MostraBranco: Boolean = False): TStrings;
    function  App_ObtemLicencasTipos(): TStrings;
    function  App_ObtemListaTipAmb(MostraBranco: Boolean = False): TStrings;
    function  App_ObtemVersaoApp(Form: TForm; TipAmb, AppId: Integer;
              AppNome: String): String;
    // L I C E N � A S
    procedure Lic_RemoveComputadoresInativos(Agora: TDateTime; Query: TMySQLQuery;
              DataBase: TMySQLDataBase);
    procedure Lic_AtualizaVencimentoLicencas(DataBase: TMySQLDataBase);
  end;

var
  UnVersoes: TUnitVersoes;
const
  CO_App_TipAmb_Tot = 1;
  CO_App_TipAmb_Int: array[0..CO_App_TipAmb_Tot] of integer = (0,1);
  CO_App_TipAmb_Str: array[0..CO_App_TipAmb_Tot] of string = ('Local', 'Web');
  //
  CO_App_Categoria_Tot = 3;
  CO_App_Categoria_Int: array[0..CO_App_Categoria_Tot] of integer = (0, 1, 2, 3);
  CO_App_Categoria_Str: array[0..CO_App_Categoria_Tot] of string = (
    'Desktop',
    'Desktop - Auxiliares',
    'WEB (Deprecado)',
    'Mobile (Deprecado)');
  //
  CO_App_LicencasTip_Tot = 4;
  CO_App_LicencasTip_Int: array[0..CO_App_LicencasTip_Tot] of integer = (0, 1, 2, 3, 4);
  CO_App_LicencasTip_Str: array[0..CO_App_LicencasTip_Tot] of string = (
    'Livre (sem licenciamento)',
    'Por per�odo',
    'Por quantidade de clientes internos',
    'Por quantidade de dispositivos',
    'Por licen�as individuais');
  //
  CO_App_Status_Tot = 4;
  CO_App_Status_Int: array[0..CO_App_Status_Tot] of integer = (0, 1, 2, 3, 4);
  CO_App_Status_Str: array[0..CO_App_Status_Tot] of string = (
    'Bloqueado',
    'Monitorado',
    'Vencido',
    'Alugado',
    'Liberado');

implementation

uses DmkDAC_PF, MyDBCheck, UMySQLModule, UnMyObjects;

{ TUnitVersoes }

function TUnitVersoes.App_InsUpdCliAplic(Codigo, Controle, Aplicativo, StatAplic,
  QtdLicen, QtdUsuarios, QtdCliInt, Beta, Ativo: Integer; FimData: TDate;
  AplicativoTxt: String; DataBase: TmySQLDatabase; QueryUpd: TmySQLQuery;
  EdAplicativo: TdmkEditCB = nil; RGStatAplic: TdmkRadioGroup = nil): Integer;
var
  TipAmb: Integer;
  SQLTipo: TSQLType;
  FimDataStr: String;
begin
  Result := 0;
  TipAmb := CO_App_TipAmb_Int[0];
  //
  if MyObjects.FIC(Aplicativo = 0, EdAplicativo, 'Aplicativo n�o definido!') then Exit;
  if MyObjects.FIC(StatAplic = 0, RGStatAplic, 'Status do aplicativo n�o definido!') then Exit;
  if MyObjects.FIC(App_Valida(TipAmb, DataBase, Codigo, Controle, Aplicativo) = False,
    nil, 'Este aplicativo j� foi inserido para este cliente!') then Exit;
  //
  if Controle = 0 then
  begin
    SQLTipo  := stIns;
    Controle := UMyMod.BuscaEmLivreY_Def('cliaplic', 'Controle', stIns, 0);
  end else
  begin
    SQLTipo := stUpd;
  end;
  //
  FimDataStr := Geral.FDT(FimData, 1);
  //
  if UMyMod.SQLInsUpd(QueryUpd, SQLTipo, 'cliaplic', False, ['TipAmb',
    'Aplicativo', 'AplicativoTxt', 'StatAplic', 'QtdLicen', 'QtdUsuarios',
    'QtdCliInt', 'FimData', 'Beta', 'Ativo', 'Codigo'], ['Controle'],
    [TipAmb, Aplicativo, AplicativoTxt, StatAplic, QtdLicen, QtdUsuarios,
    QtdCliInt, FimDataStr, Beta, Ativo, Codigo], [Controle], True) then
  begin
    Result := Controle;
  end;
end;

procedure TUnitVersoes.ReopenWHisAltOpc(QueryOpc: TmySQLQuery; DataBase: TmySQLDatabase);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QueryOpc, DataBase, [
    'SELECT * ',
    'FROM whisaltopc ',
    '']);
end;

function TUnitVersoes.App_InsUpdCliAplicWeb(Codigo, Controle, Aplicativo,
  QtdLicen, QtdCliInt, FTPCfg, DataBaseCfg, Ativo: Integer; FimData: TDate;
  AplicativoTxt: String; DataBase: TmySQLDatabase; QueryUpd: TmySQLQuery;
  EdAplicativo: TdmkEditCB = nil; EdDataBaseCfg: TdmkEditCB = nil): Integer;
var
  TipAmb: Integer;
  SQLTipo: TSQLType;
  FimDataStr: String;
begin
  Result := 0;
  TipAmb := CO_App_TipAmb_Int[1];
  //
  if EdAplicativo <> nil then
    if MyObjects.FIC(Aplicativo = 0, EdAplicativo, 'Aplicativo n�o definido!') then Exit;
  if EdDataBaseCfg <> nil then
    if MyObjects.FIC(DataBaseCfg = 0, EdDataBaseCfg, 'Configura��o do banco de dados n�o definida!') then Exit;
  if MyObjects.FIC(App_Valida(TipAmb, DataBase, Codigo, Controle, Aplicativo) = False,
    nil, 'Este aplicativo j� foi inserido para este cliente!') then Exit;
  //
  if Controle = 0 then
  begin
    SQLTipo  := stIns;
    Controle := UMyMod.BuscaEmLivreY_Def('cliaplic', 'Controle', stIns, 0);
  end else
  begin
    SQLTipo := stUpd;
  end;
  //
  FimDataStr := Geral.FDT(FimData, 1);
  //
  if UMyMod.SQLInsUpd(QueryUpd, SQLTipo, 'cliaplic', False,
    ['TipAmb', 'Aplicativo', 'AplicativoTxt', 'QtdLicen',
    'QtdCliInt', 'FTPCfg', 'DataBaseCfg', 'FimData', 'Ativo', 'Codigo'],
    ['Controle'], [TipAmb, Aplicativo, AplicativoTxt, QtdLicen,
    QtdCliInt, FTPCfg, DataBaseCfg, FimDataStr, Ativo, Codigo], [Controle],
    True) then
  begin
    Result := Controle;
  end;
end;

function TUnitVersoes.App_ObtemCategoria(Categoria: Integer): String;
begin
  case Categoria of
    0:
      Result := CO_App_Categoria_Str[0];
    1:
      Result := CO_App_Categoria_Str[1];
    2:
      Result := CO_App_Categoria_Str[2];
    3:
      Result := CO_App_Categoria_Str[3];
  end;
end;

function TUnitVersoes.App_ObtemDescriStatus(Status: Integer): String;
begin
  if Status = -1 then
    Result := ''
  else
    Result := CO_App_Status_Str[Status];
end;

function TUnitVersoes.App_ObtemDescriTipAmb(TipAmb: Integer): String;
begin
  Result := CO_App_TipAmb_Str[TipAmb];
end;

function TUnitVersoes.App_ObtemListaCategorias(MostraBranco: Boolean): TStrings;
var
  I: Integer;
  Lista: TStringList;
begin
  Lista := TStringList.Create;
  //
  if MostraBranco = True then
    Lista.Add('Todos');
  //
  for I := Low(CO_App_Categoria_Str) to High(CO_App_Categoria_Str) do
    Lista.Add(CO_App_Categoria_Str[I]);
  //
  Result := Lista;
end;

function TUnitVersoes.App_ObtemLicencasTipos(): TStrings;
var
  I: Integer;
  Lista: TStringList;
begin
  Lista := TStringList.Create;
  //
  for I := Low(CO_App_LicencasTip_Str) to High(CO_App_LicencasTip_Str) do
    Lista.Add(CO_App_LicencasTip_Str[I]);
  //
  Result := Lista;
end;

function TUnitVersoes.App_ObtemListaTipAmb(MostraBranco: Boolean): TStrings;
var
  Lista: TStringList;
  I: Integer;
begin
  Lista := TStringList.Create;
  //
  if MostraBranco = True then
    Lista.Add('N�o informado');
  //
  for I := Low(CO_App_TipAmb_Str) to High(CO_App_TipAmb_Str) do
      Lista.Add(CO_App_TipAmb_Str[I]);
  //
  Result := Lista;
end;

function TUnitVersoes.App_ObtemVersaoApp(Form: TForm; TipAmb, AppId: Integer;
  AppNome: String): String;
const
  WebArqVersDir = '\application\config\unprj_consts.php';
  VarVersao = 'CO_VERSAO';

  function ObtemVersaoDeWebApp(Arq: String): String;
  var
    MeuArq : TextFile;
    Linha, Versao: String;
  begin
    Result := '';
    //
    if FileExists(Arq) then
    begin
      try
        AssignFile(MeuArq, Arq);
        Reset(MeuArq);
        //
        while not Eof(MeuArq) do
        begin
          ReadLn(MeuArq, Linha);
          //
          if Pos(VarVersao, Linha) > 0 then
          begin
            if Pos('=', Linha) > 0 then
            begin
              Versao := Copy(Linha, Pos('=', Linha) + 1);
              Versao := Geral.SoNumero_TT(Versao);
              Versao := Geral.VersaoTxt2006(Geral.IMV(Versao));
              Result := Versao;
            end;
            Exit;
          end;
        end;
      finally
        CloseFile(MeuArq);
      end;
    end;
  end;

  function ValidaNomeApp(Arq: String): Boolean;
  var
    Nome: String;
  begin
    Result := True;
    Nome   := ExtractFileName(Arq);
    Nome   := Copy(Nome, 1, length(Nome) - length(ExtractFileExt(Arq)));
    //
    if Trim(LowerCase(Nome)) <> Trim(LowerCase(AppNome)) then
    begin
      if Geral.MB_Pergunta('O aplicativo informado tem nome diferente do aplicativo selecionado!'
        + sLineBreak + 'Deseja continuar?' + sLineBreak + sLineBreak +
        'Aplicativo informado: ' + AppNome + sLineBreak +
        'Aplicativo selecionado: ' + Nome) <> ID_YES
      then
        Result := False;
    end;
  end;

var
  Edit: TdmkEdit;
  Arquivo: String;
begin
  Result := '';
  //
  if MyObjects.FIC(AppId = 0, nil, 'Informe o aplicativo!') then Exit;
  //
  if TipAmb = CO_App_TipAmb_Int[0] then //Desktop
  begin
    if MyObjects.FileOpenDialog(Form, '', '', 'Selecione o aplicativo', '', [],
      Arquivo) then
    begin
      if ValidaNomeApp(Arquivo) = False then
        Exit;
      //
      Result := dmkPF.GetVersion(Arquivo, True);
    end;
  end else
  if TipAmb = CO_App_TipAmb_Int[1] then //Web
  begin
    Edit := TdmkEdit.Create(Form);
    try
      Arquivo := MyObjects.DefineDiretorio(Form, Edit);
      //
      if Arquivo <> '' then
      begin
        Arquivo := Arquivo + WebArqVersDir;
        //
        Result := ObtemVersaoDeWebApp(Arquivo);
      end;
    finally
      Edit.Free;
    end;
  end else
    Geral.MB_Erro('Tipo de ambiente n�o implementado para a fun��o: "TUnitVersoes.App_ObtemVersaoApp"');
end;

procedure TUnitVersoes.App_ReopenAplicativos(TipAmb: Integer;
  DataBase: TmySQLDatabase; Query: TmySQLQuery; SQLCompl: String);
begin
  if TipAmb = CO_App_TipAmb_Int[0] then //Local
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Query, DataBase, [
      'SELECT Codigo, Nome ',
      'FROM aplicativos ',
      'WHERE Ativo=1 ',
      'AND Categoria IN (0, 1, 2, 3) ',
      SQLCompl,
      'ORDER BY Nome ',
      '']);
  end else
  if TipAmb = CO_App_TipAmb_Int[1] then //Web
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Query, DataBase, [
      'SELECT Codigo, Nome ',
      'FROM appwebcab ',
      'WHERE Ativo=1 ',
      SQLCompl,
      'ORDER BY Nome ',
      '']);
  end;
end;

function TUnitVersoes.App_Valida(TipAmb: Integer; DataBase: TmySQLDatabase;
  Codigo, Controle, Aplicativo: Integer): Boolean;
var
  Qry: TmySQLQuery;
begin
  Result := False;
  //
  Qry := TmySQLQuery.Create(DataBase);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DataBase, [
      'SELECT Controle ',
      'FROM cliaplic ',
      'WHERE Codigo=' + Geral.FF0(Codigo),
      'AND Aplicativo=' + Geral.FF0(Aplicativo),
      'AND TipAmb=' + Geral.FF0(CO_App_TipAmb_Int[TipAmb]),
      '']);
    if Qry.RecordCount > 0 then
    begin
      if Qry.FieldByName('Controle').AsInteger <> Controle then
        Result := False
      else
        Result := True;
    end else
      Result := True;
  finally
    Qry.Free;
  end;
end;

procedure TUnitVersoes.Lic_AtualizaVencimentoLicencas(DataBase: TMySQLDataBase);
var
  QryOpc: TmySQLQuery;
  AtualizAuto, Dias: Integer;
begin
  QryOpc := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QryOpc, DataBase, [
      'SELECT * ',
      'FROM cliaplicop ',
      '']);
    AtualizAuto := QryOpc.FieldByName('AtualizAuto').AsInteger;
    Dias        := QryOpc.FieldByName('Dias').AsInteger;
    //
    if AtualizAuto = 1 then
    begin
      //Continuar daqui ver como fazer os servi�os (Exemplo: Licenciamento de Cloud)
    end;
  finally
    QryOpc.Free;
  end;
end;

procedure TUnitVersoes.App_ConfiguraVersao(Query: TMySQLQuery;
  DataBase: TMySQLDataBase; ComboVersao: TComboBox; Aplicativo: Integer);
var
  Versao: String;
begin
  ComboVersao.Items.Clear;
  ComboVersao.Text := '';
  //
  if Aplicativo <> 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Query, DataBase, [
      'SELECT li.VersaoAtu ',
      'FROM cliaplicli li ',
      'LEFT JOIN cliaplic ca ON ca.Controle = li.Controle ',
      'WHERE li.VersaoAtu <> "" ',
      'AND ca.Aplicativo=' + Geral.FF0(Aplicativo),
      'GROUP BY li.VersaoAtu ',
      'ORDER BY li.VersaoAtu ',
      '']);
    if Query.RecordCount > 0 then
    begin
      while not Query.Eof do
      begin
        Versao := Query.FieldByName('VersaoAtu').AsString;
        //
        ComboVersao.Items.Add(Versao);
        //
        Query.Next;
      end;
    end;
  end;
  Query.Close;
end;

function TUnitVersoes.HisAlt_GeraListaHTML(Aplicativo: Integer; VersaoApp: String;
  DataBase: TmySQLDatabase; PB: TProgressBar): String;
var
  Query: TmySQLQuery;
  Lista, Nome, Tipo, Cor: String;
begin
  Result := '';
  Query  := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Query, DataBase, [
      'SELECT alt.Codigo, alt.Nome, alt.Finalizado, tip.Cor, ',
      'tip.Cor, tip.Nome NOMETIP, its.VersaoApp, ',
      'DATE_FORMAT(IF(alt.DataAlt > 0, alt.DataAlt, alt.DataCad), "%d/%m/%Y") DATA ',
      'FROM whisalt alt ',
      'LEFT JOIN whisaltits its ON its.Codigo = alt.Codigo ',
      'LEFT JOIN whisalttip tip ON tip.Codigo = alt.WHisAltTip ',
      'WHERE its.VersaoApp="' + VersaoApp + '"',
      'AND its.Aplicativo=' + Geral.FF0(Aplicativo),
      'ORDER BY NOMETIP ',
      '']);
    if (Query.State <> dsInactive) and (Query.RecordCount > 0) then
    begin
      Query.DisableControls;
      try
        PB.Position := 0;
        PB.Max      := Query.RecordCount;
        //
        Query.First;
        //
        Nome  := '';
        Tipo  := '';
        Cor   := '';
        Lista := '';
        //
        while not Query.Eof do
        begin
          Nome := StringReplace(Query.FieldByName('Nome').AsString, sLineBreak, '</br>', [rfReplaceAll, rfIgnoreCase]);
          Cor  := Query.FieldByName('Cor').AsString;
          //
          if Tipo <> Query.FieldByName('NOMETIP').AsString then
          begin
            Tipo := Query.FieldByName('NOMETIP').AsString;
            //
            if Lista <> '' then
              Lista := Lista + '</ul></li></ul>';
            //
            Lista := Lista + '<ul style="color:#' + Cor + '"><li><strong>' +
                       Tipo + '</strong><ul style="color:#' + Cor + '">';
          end;
          //
          Lista := Lista + '<li>' + Nome + '</li>';
          //
          Query.Next;
        end;
        Lista := Lista + '</ul></li></ul>';
        //
        Result := Lista;
      finally
        Query.EnableControls;
        //
       PB.Position := 0;
      end;
    end;
  finally
    Query.Free;
  end;
end;

procedure TUnitVersoes.HisAlt_ExcluiHistoricoDeVersoesAntigas(DataBase: TMySQLDataBase;
  Dias: Integer);
var
  Query: TmySQLQuery;
  DataStr, ListaCod: String;
begin
  //////////////////////////////////////////////////////////////////////////////
  //Exclui hist�rico das vers�es que n�o est�o mais dispon�veis para download //
  //para n�o sobrecarregar o banco de dados com informa��es n�o relevantes!   //
  //////////////////////////////////////////////////////////////////////////////
  Screen.Cursor := crHourGlass;
  Query         := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  try
    //Exclui vers�es antigas
    UnDmkDAC_PF.ExecutaMySQLQuery0(Query, DataBase, [
      'DELETE ',
      'FROM whisaltits ',
      'WHERE VersaoApp NOT IN ',
      '( ',
      'SELECT Versao ',
      'FROM wversao ',
      ') ',
      'AND VersaoApp <> "" ',
      '']);
    DataStr  := Geral.FDT(Date - Dias, 1);
    ListaCod := '';
    //
    //Verifica itens sem nenhuma aplicativo / vers�o
    UnDmkDAC_PF.AbreMySQLQuery0(Query, DataBase, [
      'SELECT alt.Codigo ',
      'FROM whisalt alt ',
      'LEFT JOIN whisaltits its ON its.Codigo=alt.Codigo ',
      'WHERE IF(alt.DataAlt > 0, alt.DataAlt, alt.DataCad) < "' + DataStr + '" ',
      'AND alt.Finalizado = True ',
      'GROUP BY alt.Codigo ',
      'HAVING COUNT(its.Codigo) = 0 ',
      '']);
    while not Query.Eof do
    begin
      if Query.RecNo = Query.RecordCount then
        ListaCod := ListaCod + Geral.FF0(Query.FieldByName('Codigo').AsInteger)
      else
        ListaCod := ListaCod + Geral.FF0(Query.FieldByName('Codigo').AsInteger) + ', ';
      //
      Query.Next;
    end;
    //Exclui itens sem nenhuma aplicativo / vers�o
    if ListaCod <> '' then
    begin
      UnDmkDAC_PF.ExecutaMySQLQuery0(Query, DataBase, [
        'DELETE ',
        'FROM whisalt ',
        'WHERE Codigo IN (' + ListaCod + ')',
        '']);
    end;
  finally
    Query.Free;
    //
    Screen.Cursor := crDefault;
  end;
end;

procedure TUnitVersoes.Lic_RemoveComputadoresInativos(Agora: TDateTime;
  Query: TMySQLQuery; DataBase: TMySQLDataBase);
const
  CO_DiasDescon = 35;
var
  Descon: TDateTime;
begin
  Descon := Agora - CO_DiasDescon;
  //
  UnDmkDAC_PF.ExecutaMySQLQuery0(Query, DataBase, [
    'DELETE cal ',
    'FROM cliaplicli cal ',
    'LEFT JOIN cliaplic cap ON cap.Controle = cal.Controle ',
    'LEFT JOIN aplicativos apl ON apl.Codigo = cap.Aplicativo ',
    'WHERE cal.ConsultaDH < "' + Geral.FDT(Descon, 01) + '" ',
    'AND apl.Categoria IN (0, 1) ',
    '']);
end;

procedure TUnitVersoes.BD_ReopenAplicativos(Query: TMySQLQuery;
  DataBase: TMySQLDataBase; Cliente: Integer);
begin
  if Cliente = 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Query, Database, [
      'SELECT apl.Codigo, apl.Nome ',
      'FROM aplicativos apl ',
      'WHERE apl.Ativo = 1 ',
      'AND apl.Categoria IN (0, 1) ',
      'ORDER BY apl.Nome ',
      '']);
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Query, Database, [
      'SELECT apl.Codigo, apl.Nome ',
      'FROM cliaplic cap ',
      'LEFT JOIN aplicativos apl ON apl.Codigo = cap.Aplicativo ',
      'LEFT JOIN clientes cli ON cli.Codigo = cap.Codigo ',
      'WHERE apl.Ativo = 1 ',
      'AND cli.Codigo=' + Geral.FF0(Cliente),
      'AND apl.Categoria IN (0, 1) ',
      'GROUP BY apl.Codigo ',
      'ORDER BY apl.Nome ',
      '']);
  end;
end;

procedure TUnitVersoes.BD_ReopenClientes(Query: TMySQLQuery; DataBase: TMySQLDataBase);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Database, [
    'SELECT cli.Codigo, ',
    'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) Nome ',
    'FROM clientes cli ',
    'LEFT JOIN entidades ent ON ent.Codigo = cli.Cliente ',
    'WHERE Cliente1 = "V" ',
    'ORDER BY Nome ',
    '']);
end;

end.
