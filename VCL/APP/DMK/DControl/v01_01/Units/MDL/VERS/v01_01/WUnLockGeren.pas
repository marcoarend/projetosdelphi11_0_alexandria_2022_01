unit WUnLockGeren;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, dmkPermissoes, dmkDBGrid, Menus, dmkValUsu, Variants, dmkCheckBox,
  UnDmkEnums, DmkDAC_PF, VCLTee.TeEngine, VCLTee.Series, VCLTee.TeeProcs,
  VCLTee.Chart, VCLTee.DBChart, dmkCompoStore;

type
  THackDBGrid = class(TDBGrid);
  TFmWUnLockGeren = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel5: TPanel;
    QrWUnLockGeren: TmySQLQuery;
    DsWUnLockGeren: TDataSource;
    QrWUnLockGerenConta: TAutoIncField;
    QrWUnLockGerenNomePC: TWideStringField;
    QrWUnLockGerenCadastro: TDateTimeField;
    QrWUnLockGerenConsultaDH: TDateTimeField;
    QrWUnLockGerenIP: TWideStringField;
    QrWUnLockGerenServidor: TSmallintField;
    QrWUnLockGerenPCSuporte: TSmallintField;
    QrWUnLockGerenDescriPC: TWideStringField;
    QrWUnLockGerenStatus: TSmallintField;
    QrWUnLockGerenNOMEAPLIC: TWideStringField;
    QrWUnLockGerenFimData: TDateField;
    QrWUnLockGerenNOMECLI: TWideStringField;
    QrWUnLockGerenNOMESTATUS: TWideStringField;
    Label3: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    Label8: TLabel;
    EdAplicativo: TdmkEditCB;
    CBAplicativo: TdmkDBLookupComboBox;
    BtReabre: TBitBtn;
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesNome: TWideStringField;
    DsClientes: TDataSource;
    QrAplic: TmySQLQuery;
    QrAplicCodigo: TIntegerField;
    QrAplicNome: TWideStringField;
    DsAplic: TDataSource;
    QrWUnLockGerenAtivo: TSmallintField;
    QrWUnLockGerenQtdLicen: TIntegerField;
    QrWUnLockGerenControle: TIntegerField;
    QrWUnLockGerenCliente: TIntegerField;
    QrLoc: TmySQLQuery;
    QrWUnLockGerenVersaoAtu: TWideStringField;
    ImgWEB: TdmkImage;
    QrWUnLockGerenOSVersion: TWideStringField;
    QrWUnLockGerenDBVersion: TWideStringField;
    QrWUnLockGerenBeta: TSmallintField;
    BtMenu: TBitBtn;
    PMMenu: TPopupMenu;
    Marcardispositivocomo1: TMenuItem;
    Dispositivodesuporte1: TMenuItem;
    DispositivoBETA1: TMenuItem;
    Excluirdispositivoatual1: TMenuItem;
    N2: TMenuItem;
    Vencimentodalicenadoclienteselecionado2: TMenuItem;
    RGAtivo: TRadioGroup;
    RGSuporte: TRadioGroup;
    RGBeta: TRadioGroup;
    CBVersao: TComboBox;
    Label1: TLabel;
    Label2: TLabel;
    CBVersaoDB: TComboBox;
    CBVersaoOS: TComboBox;
    Label4: TLabel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    dmkDBGrid1: TdmkDBGrid;
    QrWUnLockGerenCliAtivo: TWideStringField;
    AtivarDesativardispositivoselecionado1: TMenuItem;
    N1: TMenuItem;
    AdicionardispositivosselecionadosalistadeversoBETA1: TMenuItem;
    PB1: TProgressBar;
    CSTabSheetChamou: TdmkCompoStore;
    Panel13: TPanel;
    QrWUnLockGerenBetaCliApp: TSmallintField;
    QrWUnLockGerenIPServidor: TWideStringField;
    QrWUnLockGerenOSArquitetu: TWideStringField;
    LaTotal: TLabel;
    N3: TMenuItem;
    Estatsticasdelicenasdeaplicativos1: TMenuItem;
    BtOpcoes: TBitBtn;
    QrWUnLockGerenFimData_Txt: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure dmkDBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure EdClienteChange(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Dispositivodesuporte1Click(Sender: TObject);
    procedure DispositivoBETA1Click(Sender: TObject);
    procedure Excluirdispositivoatual1Click(Sender: TObject);
    procedure Vencimentodalicenadoclienteselecionado2Click(Sender: TObject);
    procedure BtMenuClick(Sender: TObject);
    procedure AtivarDesativardispositivoselecionado1Click(Sender: TObject);
    procedure EdAplicativoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure AdicionardispositivosselecionadosalistadeversoBETA1Click(
      Sender: TObject);
    procedure EdAplicativoChange(Sender: TObject);
    procedure dmkDBGrid1CellClick(Column: TColumn);
    procedure PMMenuPopup(Sender: TObject);
    procedure QrWUnLockGerenBeforeClose(DataSet: TDataSet);
    procedure Estatsticasdelicenasdeaplicativos1Click(Sender: TObject);
    procedure QrWUnLockGerenAfterScroll(DataSet: TDataSet);
    procedure BtOpcoesClick(Sender: TObject);
    procedure dmkDBGrid1DblClick(Sender: TObject);
  private
    { Private declarations }
    procedure ConfiguraCombo(Campo: String; Combo: TComboBox);
    procedure AtualizaCampoInt(Campo: String; Valor, Index: Integer);
  public
    { Public declarations }
    FNChange: Boolean;
    procedure MostraEdicao(Cliente, Aplicativo: Integer);
    procedure ReopenWUnLockGeren(Cliente, Aplicativo, Conta: Integer);
  end;

  var
  FmWUnLockGeren: TFmWUnLockGeren;

implementation

uses Module, UnMyObjects, Principal, MyGlyfs, MyDBCheck, GetData, UnDmkWeb,
  ModuleGeral, UMySQLModule, UnitVersoes, UnitVersoes_Jan, UnGrl_Vars;

{$R *.DFM}

procedure TFmWUnLockGeren.AdicionardispositivosselecionadosalistadeversoBETA1Click(
  Sender: TObject);

  function InsereDispositivoNaVersao(Dispositivo, Versao, Beta: Integer): Boolean;
  var
    Controle: Integer;
  begin
    Result := False;
    //
    if Beta = 0 then
    begin
      Controle := UMyMod.BuscaNovoCodigo_Int(Dmod.QrUpdN, 'wversaoits', 'Controle',
                    [], [], stIns, 0, siPositivo, nil);
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpdN, stIns, 'wversaoits', False, ['Dispositivo',
        'Codigo'], ['Controle'], [Dispositivo, Versao], [Controle], True)
      then
        Result := True;
    end else
      Result := True;
  end;
const
  Aviso  = '...';
  Titulo = 'Sele��o de vers�o';
  Prompt = 'Informe a vers�o: [F7 para pesquisar]';
  Campo  = 'Descricao';
var
  i, Aplicativo, Dispositivo, Beta: Integer;
  Versao: Variant;
begin
  Aplicativo := EdAplicativo.ValueVariant;
  //
  if MyObjects.FIC(Aplicativo = 0, EdAplicativo, 'Defina o aplicativo!') then Exit;
  //
  Versao := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
    'SELECT Codigo, Versao ' + Campo,
    'FROM wversao ',
    'WHERE Aplicativo=' + Geral.FF0(Aplicativo),
    'AND Tipo = 0 ', //BETA
    'ORDER BY ' + Campo + ' DESC ',
    ''], Dmod.MyDBn, True);
  //
  if Versao <> Null then
  begin
    try
      QrWUnLockGeren.DisableControls;
      dmkDBGrid1.Enabled := False;
      PB1.Position       := 0;
      PB1.Max            := dmkDBGrid1.SelectedRows.Count;
      PB1.Visible        := True;
      //
      if dmkDBGrid1.SelectedRows.Count > 1 then
      begin
        with dmkDBGrid1.DataSource.DataSet do
        begin
          for i:= 0 to dmkDBGrid1.SelectedRows.Count-1 do
          begin
            GotoBookmark(dmkDBGrid1.SelectedRows.Items[i]);
            PB1.Position := PB1.Position + 1;
            PB1.Update;
            Application.ProcessMessages;
            //
            Dispositivo := QrWUnLockGerenConta.Value;
            Beta        := QrWUnLockGerenBeta.Value;
            //
            if not InsereDispositivoNaVersao(Dispositivo, Versao, Beta) then
            begin
              Geral.MB_Aviso('Falha ao atualizar lista!');
              Exit;
            end;
          end;
        end;
      end else
      begin
        Dispositivo := QrWUnLockGerenConta.Value;
        Beta        := QrWUnLockGerenBeta.Value;
        //
        if not InsereDispositivoNaVersao(Dispositivo, Versao, Beta) then
        begin
          Geral.MB_Aviso('Falha ao atualizar lista!');
          Exit;
        end;
      end;
    finally
      PB1.Visible        := False;
      dmkDBGrid1.Enabled := True;
      QrWUnLockGeren.EnableControls;
    end;
  end;
end;

procedure TFmWUnLockGeren.AtivarDesativardispositivoselecionado1Click(
  Sender: TObject);
  function VerificaQtdPCUtilizados(Controle: Integer): Integer;
  begin
    Result := 0;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDBn, [
      'SELECT COUNT(Conta) Conta',
      'FROM cliaplicli',
      'WHERE Controle=' + Geral.FF0(Controle),
      'AND PCSuporte=0',
      '']);
    if QrLoc.RecordCount > 0 then
      Result := QrLoc.FieldByName('Conta').AsInteger;
  end;
var
  Conta, Controle, Ativo, TotComp, TotCompUtil, Aplicativo, Cliente: Integer;
begin
  Controle    := QrWUnLockGerenControle.Value;
  TotComp     := QrWUnLockGerenQtdLicen.Value;
  TotCompUtil := VerificaQtdPCUtilizados(Controle);
  Conta       := QrWUnLockGerenConta.Value;
  Ativo       := QrWUnLockGerenAtivo.Value;
  Cliente     := EdCliente.ValueVariant;
  Aplicativo  := EdAplicativo.ValueVariant;
  //
  if Ativo = 0 then
    Ativo := 1
  else
    Ativo := 0;
  //
  if TotComp > TotCompUtil then
  begin
    try
      Screen.Cursor := crHourGlass;
      //
      AtualizaCampoInt('Ativo', Ativo, Conta);
      ReopenWUnLockGeren(Cliente, Aplicativo, Conta);
    finally
      Screen.Cursor := crDefault;
    end;
  end else
    Geral.MB_Aviso('Este dispositivo n�o pode ser ativado!' + sLineBreak +
      'Motivo: Este cliente atingiu o limite de dispositivos liberados para ele!');
end;

procedure TFmWUnLockGeren.AtualizaCampoInt(Campo: String; Valor, Index: Integer);
begin
  UMyMod.SQLInsUpd(Dmod.QrUpdN, stUpd, 'cliaplicli', False,
    [Campo, 'DataAlt', 'UserAlt'], ['Conta'],
    [Valor, FormatDateTime(VAR_FORMATDATE, Date), VAR_WEB_USR_ID], [Index], False);
end;

procedure TFmWUnLockGeren.BtMenuClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //
  MyObjects.MostraPopUpDeBotao(PMMenu, BtMenu);
end;

procedure TFmWUnLockGeren.BtOpcoesClick(Sender: TObject);
begin
  UnVersoes_Jan.MostraFormWUnLockOpc();
end;

procedure TFmWUnLockGeren.BtReabreClick(Sender: TObject);
var
  Cliente, Aplicativo: Integer;
begin
  Cliente    := EdCliente.ValueVariant;
  Aplicativo := EdAplicativo.ValueVariant;
  //
  ReopenWUnLockGeren(Cliente, Aplicativo, 0);
end;

procedure TFmWUnLockGeren.BtSaidaClick(Sender: TObject);
begin
  if TFmWUnLockGeren(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmWUnLockGeren.ConfiguraCombo(Campo: String; Combo: TComboBox);
var
  Resul: String;
begin
  Combo.Items.Clear;
  Combo.Text := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDBn, [
    'SELECT ' + Campo,
    'FROM cliaplicli ',
    'WHERE ' + Campo + ' <> "" ',
    'GROUP BY ' + Campo,
    'ORDER BY ' + Campo,
    '']);
  if QrLoc.RecordCount > 0 then
  begin
    while not QrLoc.Eof do
    begin
      Resul := QrLoc.FieldByName(Campo).AsString;
      //
      Combo.Items.Add(Resul);
      //
      QrLoc.Next;
    end;
  end;
  QrLoc.Close;
end;

procedure TFmWUnLockGeren.DispositivoBETA1Click(Sender: TObject);
var
  i, Beta, Conta: Integer;
begin
  try
    Screen.Cursor      := crHourGlass;
    dmkDBGrid1.Enabled := False;
    QrWUnLockGeren.DisableControls;
    //
    if dmkDBGrid1.SelectedRows.Count > 1 then
    begin
      PB1.Visible  := True;
      PB1.Position := 0;
      PB1.Max      := dmkDBGrid1.SelectedRows.Count;
      //
      with dmkDBGrid1.DataSource.DataSet do
      begin
        for i:= 0 to dmkDBGrid1.SelectedRows.Count-1 do
        begin
          GotoBookmark(dmkDBGrid1.SelectedRows.Items[i]);
          PB1.Position := PB1.Position + 1;
          PB1.Update;
          Application.ProcessMessages;
          //
          Conta := QrWUnLockGerenConta.Value;
          if QrWUnLockGerenBeta.Value = 0 then
            Beta := 1
          else
            Beta := 0;
          //
          AtualizaCampoInt('Beta', Beta, Conta);
        end;
      end;
    end else
    begin
      PB1.Visible := False;
      Conta       := QrWUnLockGerenConta.Value;
      if QrWUnLockGerenBeta.Value = 0 then
        Beta := 1
      else
        Beta := 0;
      //
      AtualizaCampoInt('Beta', Beta, Conta);
    end;
    ReopenWUnLockGeren(EdCliente.ValueVariant, EdAplicativo.ValueVariant, Conta);
  finally
    QrWUnLockGeren.EnableControls;
    dmkDBGrid1.Enabled := True;
    Screen.Cursor      := crDefault;
  end;
end;

procedure TFmWUnLockGeren.Dispositivodesuporte1Click(Sender: TObject);
var
  i, PCSuporte, Conta: Integer;
begin
  try
    Screen.Cursor      := crHourGlass;
    dmkDBGrid1.Enabled := False;
    QrWUnLockGeren.DisableControls;
    //
    if dmkDBGrid1.SelectedRows.Count > 1 then
    begin
      PB1.Visible  := True;
      PB1.Position := 0;
      PB1.Max      := dmkDBGrid1.SelectedRows.Count;
      //
      with dmkDBGrid1.DataSource.DataSet do
      begin
        for i:= 0 to dmkDBGrid1.SelectedRows.Count-1 do
        begin
          GotoBookmark(dmkDBGrid1.SelectedRows.Items[i]);
          PB1.Position := PB1.Position + 1;
          PB1.Update;
          Application.ProcessMessages;
          //
          Conta := QrWUnLockGerenConta.Value;
          if QrWUnLockGerenPCSuporte.Value = 0 then
            PCSuporte := 1
          else
            PCSuporte := 0;
          AtualizaCampoInt('PCSuporte', PCSuporte, Conta);
        end;
      end;
      PB1.Visible := False;
    end else
    begin
      PB1.Visible := False;
      Conta       := QrWUnLockGerenConta.Value;
      if QrWUnLockGerenPCSuporte.Value = 0 then
        PCSuporte := 1
      else
        PCSuporte := 0;
      AtualizaCampoInt('PCSuporte', PCSuporte, Conta);
    end;
    ReopenWUnLockGeren(EdCliente.ValueVariant, EdAplicativo.ValueVariant, Conta);
  finally
    QrWUnLockGeren.EnableControls;
    dmkDBGrid1.Enabled := True;
    Screen.Cursor      := crDefault;
  end;
end;

procedure TFmWUnLockGeren.dmkDBGrid1CellClick(Column: TColumn);
var
  Conta, PCSuporte, Beta: Integer;
begin
  if (QrWUnLockGeren.State <> dsInactive) and (QrWUnLockGeren.RecordCount > 0) then
  begin
    if Column.FieldName = 'PCSuporte' then
    begin
      Conta := QrWUnLockGerenConta.Value;
      //
      if QrWUnLockGerenPCSuporte.Value = 0 then
        PCSuporte := 1
      else
        PCSuporte := 0;
      AtualizaCampoInt('PCSuporte', PCSuporte, Conta);
      ReopenWUnLockGeren(EdCliente.ValueVariant, EdAplicativo.ValueVariant, Conta);
    end;
    if Column.FieldName = 'Beta' then
    begin
      Conta := QrWUnLockGerenConta.Value;
      if QrWUnLockGerenBeta.Value = 0 then
        Beta := 1
      else
        Beta := 0;
      AtualizaCampoInt('Beta', Beta, Conta);
      ReopenWUnLockGeren(EdCliente.ValueVariant, EdAplicativo.ValueVariant, Conta);
    end;
  end;
end;

procedure TFmWUnLockGeren.dmkDBGrid1DblClick(Sender: TObject);
var
  Campo: String;
begin
  if (QrWUnLockGeren.State <> dsInactive) and (QrWUnLockGeren.RecordCount > 0) then
  begin
    Campo := UpperCase(TDBGrid(Sender).Columns[THackDBGrid(Sender).Col -1].FieldName);
    //
    if UpperCase(Campo) = UpperCase('IP') then
    begin
      Geral.MB_Aviso(QrWUnLockGerenIP.Value);
    end else
    if UpperCase(Campo) = UpperCase('IPServidor') then
    begin
      Geral.MB_Aviso(QrWUnLockGerenIPServidor.Value);
    end;
  end;
end;

procedure TFmWUnLockGeren.dmkDBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Cor: TColor;
  Bold: Boolean;
  Dias: Integer;
  DataFimTxt: String;
  DataAtu, DataFim: TDate;
begin
  if (Column.FieldName = 'FimData_Txt')then
  begin
    Dias       := Dmod.QrOpcoesDCon.FieldByName('ApliDiaAvi').AsInteger;
    DataAtu    := Date;
    DataFim    := QrWUnLockGerenFimData.Value;
    DataFimTxt := QrWUnLockGerenFimData_Txt.Value;
    //
    if (DataFim <= (Dias + DataAtu)) and (DataFimTxt <> '') then
    begin
      Cor  := clRed;
      Bold := True;
    end else
    begin
      Cor  := clBlack;
      Bold := False;
    end;
    with dmkDBGrid1.Canvas do
    begin
      if Bold then Font.Style := [fsBold] else Font.Style := [];
      Font.Color := Cor;
      FillRect(Rect);
      TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
    end;
  end;
end;

procedure TFmWUnLockGeren.EdAplicativoChange(Sender: TObject);
var
  Aplic: Integer;
begin
  if FNChange = False then
  begin
    Aplic := EdAplicativo.ValueVariant;
    //
    UnVersoes.App_ConfiguraVersao(QrLoc, Dmod.MyDBn, CBVersao, Aplic);
  end;
end;

procedure TFmWUnLockGeren.EdAplicativoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
  begin
    EdCliente.ValueVariant := 0;
    CBCliente.KeyValue     := Null;
    //
    UnVersoes.BD_ReopenAplicativos(QrAplic, Dmod.MyDB, 0);
  end;
end;

procedure TFmWUnLockGeren.EdClienteChange(Sender: TObject);
var
  Cliente: Integer;
begin
  if FNChange = False then
  begin
    Cliente := EdCliente.ValueVariant;
    //
    UnVersoes.BD_ReopenAplicativos(QrAplic, Dmod.MyDB, Cliente);
    //
    EdAplicativo.ValueVariant := 0;
    CBAplicativo.KeyValue     := Null;
  end;
end;

procedure TFmWUnLockGeren.Estatsticasdelicenasdeaplicativos1Click(
  Sender: TObject);
var
  Cliente, Aplicativo: Integer;
begin
  Cliente    := EdCliente.ValueVariant;
  Aplicativo := EdAplicativo.ValueVariant;
  //
  UnVersoes_Jan.MostraFormWUnLockStatis(Cliente, Aplicativo, True,
    FmPrincipal.PageControl1, FmPrincipal.AdvToolBarPagerNovo);
end;

procedure TFmWUnLockGeren.Excluirdispositivoatual1Click(Sender: TObject);
var
  NomePC: String;
  Conta, Cliente, Aplicativo: Integer;
begin
  if not DBCheck.LiberaPelaSenhaBoss then Exit;
  //
  Conta      := QrWUnLockGerenConta.Value;
  NomePC     := QrWUnLockGerenNomePC.Value;
  Cliente    := EdCliente.ValueVariant;
  Aplicativo := EdAplicativo.ValueVariant;
  //
  if Geral.MensagemBox('Confirma a exclus�o do dispositivo ' + NomePC + '?',
    'Exclus�o de registro', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    try
      Screen.Cursor := crHourGlass;
      //
      Dmod.QrUpdN.SQL.Clear;
      Dmod.QrUpdN.SQL.Add('DELETE FROM cliaplicli  WHERE Conta=:P0');
      Dmod.QrUpdN.Params[0].AsInteger := Conta;
      Dmod.QrUpdN.ExecSQL;
      //
      ReopenWUnLockGeren(Cliente, Aplicativo, 0);
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmWUnLockGeren.FormActivate(Sender: TObject);
begin
  DmkWeb.CarregaImagemFormWEB(ImgWEB);
  //
  if TFmWUnLockGeren(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end;
end;

procedure TFmWUnLockGeren.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  //
  UnVersoes.BD_ReopenClientes(QrClientes, Dmod.MyDB);
  UnVersoes.BD_ReopenAplicativos(QrAplic, Dmod.MyDB, 0);
  ConfiguraCombo('DBVersion', CBVersaoDB);
  ConfiguraCombo('OSVersion', CBVersaoOS);
  UnVersoes.App_ConfiguraVersao(QrLoc, Dmod.MyDBn, CBVersao, 0);
  //
  dmkDBGrid1.PopupMenu := PMMenu;
  PB1.Visible          := False;
  //
  FNChange := True;
  //
  //N�o est� terminado
  BtOpcoes.Visible := False;
end;

procedure TFmWUnLockGeren.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWUnLockGeren.FormShow(Sender: TObject);
begin
{$IfNDef cSkinRank} //Berlin
{$IfNDef cAlphaSkin} //Berlin
  FmMyGlyfs.DefineGlyfs(TForm(Sender));
{$EndIf}
{$EndIf}
{$IfDef cSkinRank} //Berlin
  if FmPrincipal.Sd1.Active then
    FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
{$EndIf}
{$IfDef cAlphaSkin} //Berlin
  if FmPrincipal.sSkinManager1.Active then
    FmMyGlyfs.DefineGlyfsTDI2(FmPrincipal.sSkinManager1, Sender);
{$EndIf}
end;

procedure TFmWUnLockGeren.MostraEdicao(Cliente, Aplicativo: Integer);
begin
  if Cliente <> 0 then
  begin
    EdCliente.ValueVariant := Cliente;
    CBCliente.KeyValue     := Cliente;
    EdCliente.SetFocus;
  end else
  begin
    EdCliente.ValueVariant := 0;
    CBCliente.KeyValue     := Null;
  end;
  if Aplicativo <> 0 then
  begin
    EdAplicativo.ValueVariant := Aplicativo;
    CBAplicativo.KeyValue     := Aplicativo;
    EdAplicativo.SetFocus;
  end else
  begin
    EdAplicativo.ValueVariant := 0;
    CBAplicativo.KeyValue     := Null;
  end;
  CBVersao.Text       := '';
  CBVersaoDB.Text     := '';
  CBVersaoOS.Text     := '';
  RGAtivo.ItemIndex   := 0;
  RGSuporte.ItemIndex := 1;
  RGBeta.ItemIndex    := 2;
end;

procedure TFmWUnLockGeren.PMMenuPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrWUnLockGeren.State <> dsInactive) and (QrWUnLockGeren.RecordCount > 0);
  //
  Marcardispositivocomo1.Enabled                              := Enab;
  AtivarDesativardispositivoselecionado1.Enabled              := Enab;
  Excluirdispositivoatual1.Enabled                            := Enab;
  Vencimentodalicenadoclienteselecionado2.Enabled             := Enab;
  AdicionardispositivosselecionadosalistadeversoBETA1.Enabled := Enab and (QrWUnLockGerenBeta.Value = 0);
  Estatsticasdelicenasdeaplicativos1.Enabled                  := Enab;
end;

procedure TFmWUnLockGeren.QrWUnLockGerenAfterScroll(DataSet: TDataSet);
begin
  LaTotal.Caption := 'Total de itens: ' + Geral.FF0(QrWUnLockGeren.RecordCount);
end;

procedure TFmWUnLockGeren.QrWUnLockGerenBeforeClose(DataSet: TDataSet);
begin
  LaTotal.Caption := '';
end;

procedure TFmWUnLockGeren.ReopenWUnLockGeren(Cliente, Aplicativo, Conta: Integer);
var
  Status: TStrings;
  Versao, VersaoDB, VersaoOS: String;
  Ativo, Suporte, Beta, TotStat, i: Integer;
begin
  Versao   := CBVersao.Text;
  VersaoDB := CBVersaoDB.Text;
  VersaoOS := CBVersaoOS.Text;
  Ativo    := RGAtivo.ItemIndex;
  Suporte  := RGSuporte.ItemIndex;
  Beta     := RGBeta.ItemIndex;
  //
  UnVersoes.Lic_RemoveComputadoresInativos(DmodG.ObtemAgora, Dmod.QrAuxN, Dmod.MyDBn);
  //
  QrWUnLockGeren.Close;
  QrWUnLockGeren.SQL.Clear;
  QrWUnLockGeren.SQL.Add('SELECT li.Conta, li.NomePC, li.Cadastro, cl.Cliente, li.Beta, ');
  QrWUnLockGeren.SQL.Add('li.IP, li.Servidor, li.PCSuporte, li.DescriPC, li.VersaoAtu, ');
  QrWUnLockGeren.SQL.Add('ap.Nome NOMEAPLIC, ca.FimData, ca.Beta BetaCliApp, li.Ativo, ');
  QrWUnLockGeren.SQL.Add('li.OSVersion, li.DBVersion, li.IPServidor, li.OSArquitetu, ');
  QrWUnLockGeren.SQL.Add('IF (en.Tipo=0, en.RazaoSocial, en.Nome) NOMECLI, ');
  QrWUnLockGeren.SQL.Add('ca.Controle, li.ConsultaDH, li.Status, ca.QtdLicen, ');
  QrWUnLockGeren.SQL.Add('IF(ca.FimData <= "1899-12-30", "", DATE_FORMAT(ca.FimData, "%d/%m/%Y")) FimData_Txt, ');
  QrWUnLockGeren.SQL.Add('IF(cl.Ativo=0, "Inativo", "Ativo") CliAtivo, CASE li.Status ');
  //
  Status  := FmPrincipal.ObtemNomeStatusAplicativo;
  TotStat := Status.Count;
  //
  for i := 0 to TotStat - 1 do
  begin
    if i = TotStat - 1 then
      QrWUnLockGeren.SQL.Add('WHEN '+ Geral.FF0(i) +' THEN "'+ Status[i] +'" END NOMESTATUS ')
    else
      QrWUnLockGeren.SQL.Add('WHEN '+ Geral.FF0(i) +' THEN "'+ Status[i] +'"');
  end;
  //
  QrWUnLockGeren.SQL.Add('FROM cliaplicli li');
  QrWUnLockGeren.SQL.Add('LEFT JOIN cliaplic ca ON ca.Controle = li.Controle');
  QrWUnLockGeren.SQL.Add('LEFT JOIN aplicativos ap ON ap.Codigo = ca.Aplicativo');
  QrWUnLockGeren.SQL.Add('LEFT JOIN clientes cl ON cl.Codigo = ca.Codigo');
  QrWUnLockGeren.SQL.Add('LEFT JOIN entidades en ON en.Codigo = cl.Cliente');
  QrWUnLockGeren.SQL.Add('WHERE li.Controle > 0');
  QrWUnLockGeren.SQL.Add('AND ap.Categoria IN (0, 1) ');
  //
  case Ativo of
      0: QrWUnLockGeren.SQL.Add('AND li.Ativo = 1');
      1: QrWUnLockGeren.SQL.Add('AND li.Ativo = 0');
    else ;
  end;
  //
  case Suporte of
      0: QrWUnLockGeren.SQL.Add('AND li.PCSuporte = 1');
      1: QrWUnLockGeren.SQL.Add('AND li.PCSuporte = 0');
    else ;
  end;
  //
  case Beta of
      0:
      begin
        QrWUnLockGeren.SQL.Add('AND (li.Beta = 1');
        QrWUnLockGeren.SQL.Add('OR ca.Beta = 1)');
      end;
      1:
      begin
        QrWUnLockGeren.SQL.Add('AND (li.Beta = 0');
        QrWUnLockGeren.SQL.Add('OR ca.Beta = 0)');
      end
    else ;
  end;
  if Versao <> '' then
    QrWUnLockGeren.SQL.Add('AND li.VersaoAtu LIKE "%' + Versao + '%"');
  if VersaoDB <> '' then
    QrWUnLockGeren.SQL.Add('AND li.DBVersion LIKE "%' + VersaoDB + '%"');
  if VersaoOS <> '' then
    QrWUnLockGeren.SQL.Add('AND li.OSVersion LIKE "%' + VersaoOS + '%"');
  if Cliente > 0 then
    QrWUnLockGeren.SQL.Add('AND cl.Codigo=' + Geral.FF0(Cliente));
  if Aplicativo > 0 then
    QrWUnLockGeren.SQL.Add('AND ap.Codigo=' + Geral.FF0(Aplicativo));
  QrWUnLockGeren.SQL.Add('ORDER BY ca.FimData, NOMECLI, NOMEAPLIC');
  QrWUnLockGeren.Open;
  //
  if Conta > 0 then
    QrWUnLockGeren.Locate('Conta', Conta, []);
end;

procedure TFmWUnLockGeren.Vencimentodalicenadoclienteselecionado2Click(
  Sender: TObject);
var
  Conta, Controle, Cliente, Aplicativo: Integer;
  Data: TDateTime;
begin
  Conta      := QrWUnLockGerenConta.Value;
  Controle   := QrWUnLockGerenControle.Value;
  Cliente    := EdCliente.ValueVariant;
  Aplicativo := EdAplicativo.ValueVariant;
  //
  if MyObjects.FIC(Cliente = 0, EdCliente, 'Cliente n�o definido!') then Exit;
  if MyObjects.FIC(Aplicativo = 0, EdAplicativo, 'Defina o aplicativo!') then Exit;
  if MyObjects.FIC(((QrWUnLockGeren.State = dsInactive) or
    (QrWUnLockGeren.RecordCount = 0)), nil, 'Dispositivo n�o definido!') then Exit;
  //
  if DBCheck.ObtemData(DmodG.ObtemAgora, Data, DModG.ObtemAgora) then
  begin
    try
      Screen.Cursor := crHourGlass;
      //
      Dmod.QrUpd.Close;
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE cliaplic SET FimData=:P0, ');
      Dmod.QrUpd.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Controle=:Pz');
      Dmod.QrUpd.Params[00].AsString  := Geral.FDT(Data, 1);
      Dmod.QrUpd.Params[01].AsString  := Geral.FDT(DModG.ObtemAgora, 1);
      Dmod.QrUpd.Params[02].AsInteger := VAR_USUARIO;
      Dmod.QrUpd.Params[03].AsInteger := Controle;
      Dmod.QrUpd.ExecSQL;
      //
      Dmod.QrUpdN.Close;
      Dmod.QrUpdN.SQL.Clear;
      Dmod.QrUpdN.SQL.Add('UPDATE cliaplic SET FimData=:P0, ');
      Dmod.QrUpdN.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Controle=:Pz');
      Dmod.QrUpdN.Params[00].AsString  := Geral.FDT(Data, 1);
      Dmod.QrUpdN.Params[01].AsString  := Geral.FDT(DModG.ObtemAgora, 1);
      Dmod.QrUpdN.Params[02].AsInteger := VAR_USUARIO;
      Dmod.QrUpdN.Params[03].AsInteger := Controle;
      Dmod.QrUpdN.ExecSQL;
      //
      ReopenWUnLockGeren(Cliente, Aplicativo, Conta);
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

end.
