unit WUnLockOpc;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, mySQLDbTables, dmkEdit;

type
  TFmWUnLockOpc = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrCliAplicOp: TmySQLQuery;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    EdDias: TdmkEdit;
    CkAtualizAuto: TCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CkAtualizAutoClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmWUnLockOpc: TFmWUnLockOpc;

implementation

uses UnMyObjects, Module, UMySQLModule;

{$R *.DFM}

procedure TFmWUnLockOpc.BtOKClick(Sender: TObject);
var
  Codigo, AtualizAuto, Dias: Integer;
begin
  Codigo      := QrCliAplicOp.FieldByName('Codigo').AsInteger;
  AtualizAuto := Geral.BoolToInt(CkAtualizAuto.Checked);
  //
  if AtualizAuto = 1 then
    Dias := EdDias.ValueVariant
  else
    Dias := 0;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'cliaplicop', False,
    ['AtualizAuto', 'Dias'], ['Codigo'],
    [AtualizAuto, Dias], [Codigo], True) then
  begin
    Close;
  end;
end;

procedure TFmWUnLockOpc.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWUnLockOpc.CkAtualizAutoClick(Sender: TObject);
begin
  EdDias.Enabled := CkAtualizAuto.Checked;
end;

procedure TFmWUnLockOpc.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmWUnLockOpc.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UMyMod.AbreQuery(QrCliAplicOp, DMod.MyDB);
  //
  CkAtualizAuto.Checked := Geral.IntToBool(QrCliAplicOp.FieldByName('AtualizAuto').AsInteger);
  EdDias.Enabled        := CkAtualizAuto.Checked;
  EdDias.ValueVariant   := QrCliAplicOp.FieldByName('Dias').AsInteger;
end;

procedure TFmWUnLockOpc.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
