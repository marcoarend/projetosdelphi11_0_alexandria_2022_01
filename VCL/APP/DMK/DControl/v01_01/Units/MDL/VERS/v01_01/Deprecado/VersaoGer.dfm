object FmVersaoGer: TFmVersaoGer
  Left = 339
  Top = 185
  Caption = 'WEB-VERSA-001 :: Gerenciamento de vers'#245'es'
  ClientHeight = 624
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 0
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 872
      Height = 48
      Align = alClient
      TabOrder = 1
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 326
        Height = 32
        Caption = 'Gerenciamento de vers'#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 326
        Height = 32
        Caption = 'Gerenciamento de vers'#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 326
        Height = 32
        Caption = 'Gerenciamento de vers'#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
    object GB_R: TGroupBox
      Left = 920
      Top = 0
      Width = 88
      Height = 48
      Align = alRight
      TabOrder = 2
      object ImgTipo: TdmkImage
        Left = 48
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
      object ImgWEB: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 462
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 462
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 462
        Align = alClient
        TabOrder = 0
        object PnAplicativo: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 55
          Align = alTop
          TabOrder = 0
          object LaAplicativo: TLabel
            Left = 10
            Top = 9
            Width = 125
            Height = 13
            Caption = 'Aplicativo: (Apenas ativos)'
          end
          object EdAplicativo: TdmkEditCB
            Left = 10
            Top = 25
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdAplicativoChange
            DBLookupComboBox = CBAplicativo
            IgnoraDBLookupComboBox = False
          end
          object CBAplicativo: TdmkDBLookupComboBox
            Left = 66
            Top = 25
            Width = 342
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsAplicativos
            TabOrder = 1
            dmkEditCB = EdAplicativo
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object SbReabre: TBitBtn
            Tag = 18
            Left = 412
            Top = 6
            Width = 40
            Height = 40
            NumGlyphs = 2
            TabOrder = 2
            OnClick = SbReabreClick
          end
        end
        object PageControl1: TPageControl
          Left = 2
          Top = 70
          Width = 1004
          Height = 390
          ActivePage = TabSheet1
          Align = alClient
          TabHeight = 25
          TabOrder = 1
          object TabSheet1: TTabSheet
            Caption = 'Desktop'
            object TabControl1: TTabControl
              Left = 0
              Top = 0
              Width = 996
              Height = 355
              Align = alClient
              TabHeight = 25
              TabOrder = 0
              Tabs.Strings = (
                'Em produ'#231#227'o'
                'BETA (Testes)')
              TabIndex = 0
              OnChange = TabControl1Change
              object Splitter1: TSplitter
                Left = 449
                Top = 91
                Width = 10
                Height = 260
                ExplicitLeft = 374
                ExplicitTop = 84
                ExplicitHeight = 274
              end
              object Splitter2: TSplitter
                Left = 909
                Top = 91
                Width = 10
                Height = 260
                ExplicitLeft = 828
                ExplicitTop = 85
                ExplicitHeight = 274
              end
              object PnVersaoCad: TPanel
                Left = 4
                Top = 31
                Width = 988
                Height = 60
                Align = alTop
                ParentBackground = False
                TabOrder = 0
                object Label1: TLabel
                  Left = 292
                  Top = 12
                  Width = 36
                  Height = 13
                  Caption = 'Vers'#227'o:'
                end
                object SBAppSel: TSpeedButton
                  Left = 265
                  Top = 30
                  Width = 21
                  Height = 21
                  Caption = '...'
                  OnClick = SBAppSelClick
                end
                object Label15: TLabel
                  Left = 12
                  Top = 12
                  Width = 90
                  Height = 13
                  Caption = 'Aplicativo desktop:'
                end
                object Label2: TLabel
                  Left = 447
                  Top = 12
                  Width = 57
                  Height = 13
                  Caption = 'Sub vers'#227'o:'
                end
                object BtUpload: TBitBtn
                  Tag = 14
                  Left = 605
                  Top = 14
                  Width = 120
                  Height = 40
                  Caption = '&OK'
                  NumGlyphs = 2
                  TabOrder = 3
                  OnClick = BtUploadClick
                end
                object EdVersao: TdmkEdit
                  Left = 292
                  Top = 30
                  Width = 150
                  Height = 21
                  TabOrder = 1
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdAppSel: TdmkEdit
                  Left = 12
                  Top = 30
                  Width = 250
                  Height = 21
                  ReadOnly = True
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdSubVersao: TdmkEdit
                  Left = 447
                  Top = 30
                  Width = 150
                  Height = 21
                  TabOrder = 2
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
              end
              object dmkDBGrid1: TdmkDBGrid
                Left = 4
                Top = 91
                Width = 445
                Height = 260
                Align = alLeft
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Versao'
                    Title.Caption = 'Vers'#227'o'
                    Width = 150
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DataCad'
                    Title.Caption = 'Cadastrada em'
                    Width = 100
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Ativo'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SubVersao'
                    Title.Caption = 'Sub vers'#227'o'
                    Width = 75
                    Visible = True
                  end>
                Color = clWindow
                DataSource = DsWVersao
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
                PopupMenu = PMMenu
                TabOrder = 1
                TitleFont.Charset = ANSI_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                OnCellClick = dmkDBGrid1CellClick
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Versao'
                    Title.Caption = 'Vers'#227'o'
                    Width = 150
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DataCad'
                    Title.Caption = 'Cadastrada em'
                    Width = 100
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Ativo'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SubVersao'
                    Title.Caption = 'Sub vers'#227'o'
                    Width = 75
                    Visible = True
                  end>
              end
              object DBChart1: TDBChart
                Left = 459
                Top = 91
                Width = 450
                Height = 260
                PrintProportional = False
                Title.Font.Height = -19
                Title.Text.Strings = (
                  'N'#186' de dispositivos por vers'#227'o')
                OnClickSeries = DBChart1ClickSeries
                Chart3DPercent = 50
                Legend.Alignment = laBottom
                View3D = False
                View3DOptions.Elevation = 315
                View3DOptions.Orthogonal = False
                View3DOptions.Perspective = 0
                View3DOptions.Rotation = 360
                Align = alLeft
                TabOrder = 2
                PrintMargins = (
                  24
                  18
                  7
                  16)
                ColorPaletteIndex = 6
                object Series2: TPieSeries
                  Marks.Arrow.Visible = True
                  Marks.Callout.Brush.Color = clBlack
                  Marks.Callout.Arrow.Visible = True
                  Marks.Visible = True
                  DataSource = QrGrafico
                  XLabelsSource = 'Versao'
                  XValues.Order = loAscending
                  YValues.Name = 'Pie'
                  YValues.Order = loNone
                  YValues.ValueSource = 'Total'
                  Frame.InnerBrush.BackColor = clRed
                  Frame.InnerBrush.Gradient.EndColor = clGray
                  Frame.InnerBrush.Gradient.MidColor = clWhite
                  Frame.InnerBrush.Gradient.StartColor = 4210752
                  Frame.InnerBrush.Gradient.Visible = True
                  Frame.MiddleBrush.BackColor = clYellow
                  Frame.MiddleBrush.Gradient.EndColor = 8553090
                  Frame.MiddleBrush.Gradient.MidColor = clWhite
                  Frame.MiddleBrush.Gradient.StartColor = clGray
                  Frame.MiddleBrush.Gradient.Visible = True
                  Frame.OuterBrush.BackColor = clGreen
                  Frame.OuterBrush.Gradient.EndColor = 4210752
                  Frame.OuterBrush.Gradient.MidColor = clWhite
                  Frame.OuterBrush.Gradient.StartColor = clSilver
                  Frame.OuterBrush.Gradient.Visible = True
                  Frame.Visible = False
                  Frame.Width = 4
                  OtherSlice.Legend.Visible = False
                end
              end
              object dmkDBGrid2: TdmkDBGrid
                Left = 919
                Top = 91
                Width = 73
                Height = 260
                Align = alClient
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Cliente_TXT'
                    Title.Caption = 'Cliente'
                    Width = 200
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NomePC'
                    Title.Caption = 'Dispositivo'
                    Width = 150
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'VersaoAtu'
                    Title.Caption = 'Vers'#227'o'
                    Width = 100
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DataVerifi'
                    Title.Caption = 'Data da '#250'ltima verifica'#231#227'o'
                    Width = 150
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PCSuporte'
                    Title.Caption = 'PC Suporte'
                    Visible = True
                  end>
                Color = clWindow
                DataSource = DsComputadores
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
                TabOrder = 3
                TitleFont.Charset = ANSI_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                OnCellClick = dmkDBGrid1CellClick
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Cliente_TXT'
                    Title.Caption = 'Cliente'
                    Width = 200
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NomePC'
                    Title.Caption = 'Dispositivo'
                    Width = 150
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'VersaoAtu'
                    Title.Caption = 'Vers'#227'o'
                    Width = 100
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DataVerifi'
                    Title.Caption = 'Data da '#250'ltima verifica'#231#227'o'
                    Width = 150
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PCSuporte'
                    Title.Caption = 'PC Suporte'
                    Visible = True
                  end>
              end
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 510
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 554
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtMenu: TBitBtn
        Tag = 237
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Menu'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtMenuClick
      end
    end
  end
  object PMMenu: TPopupMenu
    OnPopup = PMMenuPopup
    Left = 717
    Top = 298
    object Moverversoatualpara1: TMenuItem
      Caption = '&Mover vers'#227'o atual para'
      object Emproduo1: TMenuItem
        Caption = '&Em produ'#231#227'o'
        OnClick = Emproduo1Click
      end
      object BETA1: TMenuItem
        Caption = '&BETA'
        OnClick = BETA1Click
      end
    end
    object Gerenciargrupodetestesdaversoatual1: TMenuItem
      Caption = '&Gerenciar grupo de testes da vers'#227'o atual'
      OnClick = Gerenciargrupodetestesdaversoatual1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Baixarverso1: TMenuItem
      Caption = '&Baixar vers'#227'o selecionada'
      OnClick = Baixarverso1Click
    end
    object Excluirversoatual1: TMenuItem
      Caption = '&Excluir vers'#227'o atual'
      OnClick = Excluirversoatual1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Histricodealteraes1: TMenuItem
      Caption = '&Hist'#243'rico de altera'#231#245'es'
      OnClick = Histricodealteraes1Click
    end
  end
  object QrAplicativos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM aplicativos'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 548
    Top = 297
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
      Origin = 'aplicativos.Codigo'
    end
    object QrAplicativosNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsAplicativos: TDataSource
    DataSet = QrAplicativos
    Left = 576
    Top = 297
  end
  object QrWVersao: TmySQLQuery
    Database = Dmod.MyDBn
    Left = 492
    Top = 297
    object QrWVersaoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrWVersaoVersao: TWideStringField
      FieldName = 'Versao'
      Size = 13
    end
    object QrWVersaoAplicativo: TIntegerField
      FieldName = 'Aplicativo'
    end
    object QrWVersaoTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrWVersaoDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrWVersaoAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
    object QrWVersaoArqWeb: TIntegerField
      FieldName = 'ArqWeb'
    end
    object QrWVersaoArquivo: TWideStringField
      FieldName = 'Arquivo'
      Size = 50
    end
    object QrWVersaoCaminho: TWideStringField
      FieldName = 'Caminho'
      Size = 255
    end
    object QrWVersaoFTPConfig: TIntegerField
      FieldName = 'FTPConfig'
    end
    object QrWVersaoOrigem: TIntegerField
      FieldName = 'Origem'
    end
    object QrWVersaoSubVersao: TWideStringField
      FieldName = 'SubVersao'
      Size = 13
    end
  end
  object DsWVersao: TDataSource
    DataSet = QrWVersao
    Left = 520
    Top = 297
  end
  object QrGrafico: TmySQLQuery
    Database = Dmod.MyDBn
    Left = 688
    Top = 297
    object QrGraficoVersao: TWideStringField
      FieldName = 'Versao'
    end
    object QrGraficoTotal: TFloatField
      FieldName = 'Total'
    end
    object QrGraficoVersaoAtu: TWideStringField
      FieldName = 'VersaoAtu'
      Size = 13
    end
  end
  object CSTabSheetChamou: TdmkCompoStore
    Left = 420
    Top = 11
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 391
    Top = 11
  end
  object QrComputadores: TmySQLQuery
    Database = Dmod.MyDBn
    Left = 604
    Top = 297
    object QrComputadoresNomePC: TWideStringField
      FieldName = 'NomePC'
      Size = 255
    end
    object QrComputadoresVersaoAtu: TWideStringField
      FieldName = 'VersaoAtu'
      Size = 13
    end
    object QrComputadoresCliente_TXT: TWideStringField
      FieldName = 'Cliente_TXT'
      Size = 100
    end
    object QrComputadoresDataVerifi: TDateTimeField
      FieldName = 'DataVerifi'
    end
    object QrComputadoresPCSuporte: TSmallintField
      FieldName = 'PCSuporte'
      MaxValue = 1
    end
  end
  object DsComputadores: TDataSource
    DataSet = QrComputadores
    Left = 632
    Top = 297
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDBn
    Left = 660
    Top = 297
  end
  object QrWControl: TmySQLQuery
    Database = Dmod.MyDBn
    Left = 745
    Top = 297
  end
  object SaveFile: TSaveDialog
    DefaultExt = '*'
    Left = 449
    Top = 10
  end
end
