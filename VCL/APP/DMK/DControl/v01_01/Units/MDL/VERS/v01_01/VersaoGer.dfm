object FmVersaoGer: TFmVersaoGer
  Left = 339
  Top = 185
  Caption = 'WEB-VERSA-001 :: Gerenciamento de vers'#245'es'
  ClientHeight = 614
  ClientWidth = 741
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 741
    Height = 47
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 0
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 758
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 1
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 382
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Gerenciamento de vers'#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 382
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Gerenciamento de vers'#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 382
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Gerenciamento de vers'#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
    object GB_R: TGroupBox
      Left = 817
      Top = 0
      Width = 109
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 2
      object ImgTipo: TdmkImage
        Left = 59
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
      object ImgWEB: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 47
    Width = 741
    Height = 455
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 741
      Height = 455
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 926
      ExplicitHeight = 569
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 741
        Height = 455
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 926
        ExplicitHeight = 569
        object PnAplicativo: TPanel
          Left = 2
          Top = 15
          Width = 737
          Height = 68
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          TabOrder = 0
          ExplicitTop = 18
          ExplicitWidth = 921
          object LaAplicativo: TLabel
            Left = 12
            Top = 11
            Width = 125
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Aplicativo: (Apenas ativos)'
          end
          object EdAplicativo: TdmkEditCB
            Left = 12
            Top = 31
            Width = 69
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdAplicativoChange
            DBLookupComboBox = CBAplicativo
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBAplicativo: TdmkDBLookupComboBox
            Left = 81
            Top = 31
            Width = 421
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsAplicativos
            TabOrder = 1
            dmkEditCB = EdAplicativo
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object SbReabre: TBitBtn
            Tag = 18
            Left = 507
            Top = 7
            Width = 49
            Height = 50
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            NumGlyphs = 2
            TabOrder = 2
            OnClick = SbReabreClick
          end
        end
        object PageControl1: TPageControl
          Left = 2
          Top = 83
          Width = 737
          Height = 370
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          ActivePage = TabSheet1
          Align = alClient
          TabHeight = 25
          TabOrder = 1
          ExplicitTop = 86
          ExplicitHeight = 367
          object TabSheet1: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Desktop'
            object TabControl1: TTabControl
              Left = 0
              Top = 0
              Width = 729
              Height = 335
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              TabHeight = 25
              TabOrder = 0
              Tabs.Strings = (
                'Em produ'#231#227'o'
                'BETA (Testes)')
              TabIndex = 0
              OnChange = TabControl1Change
              ExplicitHeight = 332
              object PnVersaoCad: TPanel
                Left = 4
                Top = 31
                Width = 721
                Height = 74
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alTop
                ParentBackground = False
                TabOrder = 0
                ExplicitLeft = 5
                ExplicitTop = 38
                ExplicitWidth = 901
                object Label1: TLabel
                  Left = 359
                  Top = 15
                  Width = 36
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Vers'#227'o:'
                end
                object SBAppSel: TSpeedButton
                  Left = 326
                  Top = 37
                  Width = 26
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = '...'
                  OnClick = SBAppSelClick
                end
                object Label15: TLabel
                  Left = 15
                  Top = 15
                  Width = 90
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Aplicativo desktop:'
                end
                object Label2: TLabel
                  Left = 550
                  Top = 15
                  Width = 57
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Sub vers'#227'o:'
                end
                object BtUpload: TBitBtn
                  Tag = 14
                  Left = 745
                  Top = 17
                  Width = 147
                  Height = 49
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = '&OK'
                  NumGlyphs = 2
                  TabOrder = 3
                  OnClick = BtUploadClick
                end
                object EdVersao: TdmkEdit
                  Left = 359
                  Top = 37
                  Width = 185
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabOrder = 1
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdAppSel: TdmkEdit
                  Left = 15
                  Top = 37
                  Width = 307
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  ReadOnly = True
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdSubVersao: TdmkEdit
                  Left = 550
                  Top = 37
                  Width = 185
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabOrder = 2
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
              end
              object dmkDBGrid1: TdmkDBGrid
                Left = 4
                Top = 105
                Width = 721
                Height = 226
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Versao'
                    Title.Caption = 'Vers'#227'o'
                    Width = 150
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DataCad'
                    Title.Caption = 'Cadastrada em'
                    Width = 100
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Ativo'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SubVersao'
                    Title.Caption = 'Sub vers'#227'o'
                    Width = 75
                    Visible = True
                  end>
                Color = clWindow
                DataSource = DsWVersao
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
                PopupMenu = PMMenu
                TabOrder = 1
                TitleFont.Charset = ANSI_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                OnCellClick = dmkDBGrid1CellClick
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Versao'
                    Title.Caption = 'Vers'#227'o'
                    Width = 150
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DataCad'
                    Title.Caption = 'Cadastrada em'
                    Width = 100
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Ativo'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SubVersao'
                    Title.Caption = 'Sub vers'#227'o'
                    Width = 75
                    Visible = True
                  end>
              end
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 502
    Width = 741
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 921
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 546
    Width = 741
    Height = 68
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 746
      Top = 18
      Width = 177
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 18
      Width = 744
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtMenu: TBitBtn
        Tag = 237
        Left = 15
        Top = 5
        Width = 147
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Menu'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtMenuClick
      end
    end
  end
  object PMMenu: TPopupMenu
    OnPopup = PMMenuPopup
    Left = 184
    Top = 306
    object Moverversoatualpara1: TMenuItem
      Caption = '&Mover vers'#227'o atual para'
      object Emproduo1: TMenuItem
        Caption = '&Em produ'#231#227'o'
        OnClick = Emproduo1Click
      end
      object BETA1: TMenuItem
        Caption = '&BETA'
        OnClick = BETA1Click
      end
    end
    object Gerenciargrupodetestesdaversoatual1: TMenuItem
      Caption = '&Gerenciar grupo de testes da vers'#227'o atual'
      OnClick = Gerenciargrupodetestesdaversoatual1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Baixarverso1: TMenuItem
      Caption = '&Baixar vers'#227'o selecionada'
      OnClick = Baixarverso1Click
    end
    object Excluirversoatual1: TMenuItem
      Caption = '&Excluir vers'#227'o atual'
      OnClick = Excluirversoatual1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Histricodealteraes1: TMenuItem
      Caption = '&Hist'#243'rico de altera'#231#245'es'
      OnClick = Histricodealteraes1Click
    end
    object Gerenciaraplicativos1: TMenuItem
      Caption = 'Gerenciar licen'#231'as de &aplicativos'
      OnClick = Gerenciaraplicativos1Click
    end
    object Estatsticasdeaplicativos1: TMenuItem
      Caption = 'E&stat'#237'sticas de licen'#231'as de aplicativos'
      OnClick = Estatsticasdeaplicativos1Click
    end
  end
  object QrAplicativos: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM aplicativos'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 100
    Top = 305
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
      Origin = 'aplicativos.Codigo'
    end
    object QrAplicativosNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsAplicativos: TDataSource
    DataSet = QrAplicativos
    Left = 128
    Top = 305
  end
  object QrWVersao: TMySQLQuery
    Database = Dmod.MyDBn
    Left = 44
    Top = 305
    object QrWVersaoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrWVersaoVersao: TWideStringField
      FieldName = 'Versao'
      Size = 13
    end
    object QrWVersaoAplicativo: TIntegerField
      FieldName = 'Aplicativo'
    end
    object QrWVersaoTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrWVersaoDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrWVersaoAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
    object QrWVersaoArqWeb: TIntegerField
      FieldName = 'ArqWeb'
    end
    object QrWVersaoArquivo: TWideStringField
      FieldName = 'Arquivo'
      Size = 50
    end
    object QrWVersaoCaminho: TWideStringField
      FieldName = 'Caminho'
      Size = 255
    end
    object QrWVersaoFTPConfig: TIntegerField
      FieldName = 'FTPConfig'
    end
    object QrWVersaoOrigem: TIntegerField
      FieldName = 'Origem'
    end
    object QrWVersaoSubVersao: TWideStringField
      FieldName = 'SubVersao'
      Size = 13
    end
  end
  object DsWVersao: TDataSource
    DataSet = QrWVersao
    Left = 72
    Top = 305
  end
  object CSTabSheetChamou: TdmkCompoStore
    Left = 420
    Top = 11
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 391
    Top = 11
  end
  object QrLoc: TMySQLQuery
    Database = Dmod.MyDBn
    Left = 156
    Top = 305
  end
  object QrWControl: TMySQLQuery
    Database = Dmod.MyDBn
    Left = 212
    Top = 305
  end
  object SaveFile: TSaveDialog
    DefaultExt = '*'
    Left = 449
    Top = 10
  end
end
