unit WHisAltApl;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  mySQLDbTables, UnDmkEnums, DmkDAC_PF, dmkDBGridZTO, Variants, dmkRadioGroup;

type
  TFmWHisAltApl = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    ImgWEB: TdmkImage;
    Panel5: TPanel;
    Label1: TLabel;
    EdModulo: TdmkEditCB;
    CBModulo: TdmkDBLookupComboBox;
    BtReabre2: TBitBtn;
    QrModulos: TmySQLQuery;
    DsModulos: TDataSource;
    QrModulosCodigo: TIntegerField;
    QrModulosNome: TWideStringField;
    QrModulosModulo: TWideStringField;
    QrAplicativos: TmySQLQuery;
    DBGridApl: TdmkDBGridZTO;
    DsAplicativos: TDataSource;
    QrAplicativosCodigo: TIntegerField;
    QrAplicativosNome: TWideStringField;
    RGCategoria: TdmkRadioGroup;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtReabre2Click(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure SelecinarTodos(Selecionar: Boolean);
    procedure ReabreAplicativos(Modulo, Categoria: Integer);
  public
    { Public declarations }
    FCodigo: Integer;
  end;

  var
  FmWHisAltApl: TFmWHisAltApl;

implementation

uses UnMyObjects, Module, ModuleGeral, UnDmkWeb, UMySQLModule, UCreate, UnitVersoes;

{$R *.DFM}

procedure TFmWHisAltApl.BtNenhumClick(Sender: TObject);
begin
  SelecinarTodos(False);
end;

procedure TFmWHisAltApl.BtOKClick(Sender: TObject);
  procedure InsereRegistro(Aplicativo: Integer);
  var
    Controle: Integer;
  begin
    Controle := UMyMod.BuscaNovoCodigo_Int(Dmod.QrUpdN, 'whisaltits',
                  'Controle', [], [], stIns, 0, siPositivo, nil);
    //
    UMyMod.SQLInsUpd(Dmod.QrUpdN, stIns, 'whisaltits', False,
      ['Aplicativo', 'VersaoApp', 'Codigo'], ['Controle'],
      [Aplicativo, '', FCodigo], [Controle], True);
  end;
var
  I: Integer;
begin
  Screen.Cursor := crHourGlass;
  BtOK.Enabled  := False;
  try
    if DBGridApl.SelectedRows.Count > 1 then
    begin
      with DBGridApl.DataSource.DataSet do
      for I := 0 to DBGridApl.SelectedRows.Count - 1 do
      begin
        GotoBookmark(DBGridApl.SelectedRows.Items[I]);
        //
        InsereRegistro(QrAplicativosCodigo.Value);
      end;
    end else
      InsereRegistro(QrAplicativosCodigo.Value);
  finally
    Screen.Cursor := crDefault;
    BtOK.Enabled  := True;
  end;
  Close;
end;

procedure TFmWHisAltApl.BtReabre2Click(Sender: TObject);
begin
  ReabreAplicativos(EdModulo.ValueVariant, RGCategoria.ItemIndex);
end;

procedure TFmWHisAltApl.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWHisAltApl.BtTodosClick(Sender: TObject);
begin
  SelecinarTodos(True);
end;

procedure TFmWHisAltApl.FormActivate(Sender: TObject);
begin
  DmkWeb.CarregaImagemFormWEB(ImgWEB);
  //
  MyObjects.CorIniComponente();
end;

procedure TFmWHisAltApl.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UMyMod.AbreQuery(QrModulos, Dmod.MyDBn);
  //
  DBGridApl.DataSource := DsAplicativos;
  CBModulo.ListSource  := DsModulos;
  //
  EdModulo.ValueVariant := 0;
  CBModulo.KeyValue     := Null;
  RGCategoria.Items     := UnVersoes.App_ObtemListaCategorias(True);
  RGCategoria.ItemIndex := 0;
  //
  ReabreAplicativos(0, 0);
end;

procedure TFmWHisAltApl.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWHisAltApl.ReabreAplicativos(Modulo, Categoria: Integer);
var
  Codigo: Integer;
  Nome, SQLCompl: String;
begin
  Screen.Cursor := crHourGlass;
  try
    if Categoria > 0 then
      SQLCompl := 'AND apl.Categoria=' + Geral.FF0(Categoria - 1)
    else
      SQLCompl := '';
    //
    if Modulo = 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrAplicativos, Dmod.MyDB, [
        'SELECT apl.Codigo, apl.Nome ',
        'FROM aplicativos apl ',
        'WHERE apl.Ativo = 1 ',
        SQLCompl,
        'ORDER BY apl.Nome ',
        '']);
    end else
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrAplicativos, Dmod.MyDB, [
        'SELECT apl.Codigo, apl.Nome ',
        'FROM aplicmodul amo ',
        'LEFT JOIN aplicativos apl ON apl.Codigo = amo.Codigo ',
        'WHERE amo.Modulo="' + QrModulosModulo.Value + '"',
        'AND amo.Ativo = 1 ',
        'AND apl.Ativo = 1 ',
        SQLCompl,
        'ORDER BY apl.Nome ',
        '']);
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmWHisAltApl.SelecinarTodos(Selecionar: Boolean);
begin
  if (QrAplicativos.State = dsInactive) or (QrAplicativos.RecordCount = 0) then
    Exit;
  //
  try
    Screen.Cursor     := crHourGlass;
    DBGridApl.Enabled := False;
    //
    QrAplicativos.First;
    while not QrAplicativos.Eof do
    begin
      DBGridApl.SelectedRows.CurrentRowSelected := Selecionar;
      QrAplicativos.Next;
    end;
  finally
    DBGridApl.Enabled := True;
    Screen.Cursor     := crDefault;
  end;
end;

end.
