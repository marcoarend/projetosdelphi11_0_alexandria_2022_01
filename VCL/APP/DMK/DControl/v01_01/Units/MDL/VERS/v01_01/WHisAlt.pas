unit WHisAlt;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, DmkDAC_PF, UMySQLModule,
  mySQLDbTables, UnMySQLCuringa, dmkGeral, UnInternalConsts2, dmkPermissoes,
  dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, unDmkProcFunc,
  ComCtrls, dmkEditDateTimePicker, dmkCheckBox, Variants, dmkMemo, dmkDBGrid,
  dmkDBLookupComboBox, dmkEditCB, Grids, DBGrids, dmkValUsu, frxClass, frxDBSet,
  Menus, UnDmkEnums, dmkCompoStore, UnDmkImg;

type
  TFmWHisAlt = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    GBEdita: TGroupBox;
    GBCntrl: TGroupBox;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrWHisAlt: TmySQLQuery;
    DsWHisAlt: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    QrWHisAltCodigo: TIntegerField;
    QrWHisAltAlterWeb: TSmallintField;
    QrWHisAltAtivo: TSmallintField;
    QrWHisAltLk: TIntegerField;
    QrWHisAltDataCad: TDateField;
    QrWHisAltDataAlt: TDateField;
    QrWHisAltUserCad: TIntegerField;
    QrWHisAltUserAlt: TIntegerField;
    QrWHisAltWHisAltTip: TIntegerField;
    QrWHisAltNOMETIP: TWideStringField;
    QrWHisAltFinalizado: TSmallintField;
    QrWHisAltDATA: TWideStringField;
    QrWHisAltNome: TWideMemoField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel6: TPanel;
    DBGWHisAltIts: TDBGrid;
    PnAplicMenu: TPanel;
    BtInclui2: TBitBtn;
    BtAltera2: TBitBtn;
    BtExclui2: TBitBtn;
    PnAplic: TPanel;
    Label1: TLabel;
    Label11: TLabel;
    SpeedButton1: TSpeedButton;
    Panel9: TPanel;
    BtConfirma2: TBitBtn;
    Panel10: TPanel;
    BtDesiste2: TBitBtn;
    EdAplic: TdmkEditCB;
    CBAplic: TdmkDBLookupComboBox;
    CkContIts: TdmkCheckBox;
    Panel2: TPanel;
    Panel7: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    TPDataI: TdmkEditDateTimePicker;
    TPDataF: TdmkEditDateTimePicker;
    BtReabre: TBitBtn;
    EdDescricao: TdmkEdit;
    RGFinalizado: TRadioGroup;
    DBGWHisAlt: TdmkDBGrid;
    Splitter4: TSplitter;
    DBMemo1: TDBMemo;
    Panel5: TPanel;
    Label5: TLabel;
    EdAplicativo: TdmkEditCB;
    CBAplicativo: TdmkDBLookupComboBox;
    Label6: TLabel;
    dmkDBGrid2: TdmkDBGrid;
    Splitter2: TSplitter;
    DBMemo2: TDBMemo;
    Label8: TLabel;
    EdWHisAltTip: TdmkEditCB;
    CBWHisAltTip: TdmkDBLookupComboBox;
    SBHisAltTip: TSpeedButton;
    CkFinalizado: TdmkCheckBox;
    CkContinuar: TdmkCheckBox;
    MeNome: TdmkMemo;
    CBVersao: TComboBox;
    QrWHisAltIts: TmySQLQuery;
    DsWHisAltIts: TDataSource;
    QrWHisAltItsCodigo: TIntegerField;
    QrWHisAltItsControle: TIntegerField;
    QrWHisAltItsAplicativo: TIntegerField;
    QrWHisAltItsLk: TIntegerField;
    QrWHisAltItsDataCad: TDateField;
    QrWHisAltItsDataAlt: TDateField;
    QrWHisAltItsUserCad: TIntegerField;
    QrWHisAltItsUserAlt: TIntegerField;
    QrWHisAltItsAlterWeb: TSmallintField;
    QrWHisAltItsAtivo: TSmallintField;
    QrWHisAltItsNOMEAPL: TWideStringField;
    DsAplic: TDataSource;
    QrWHisAltTip: TmySQLQuery;
    QrWHisAltTipCodigo: TIntegerField;
    QrWHisAltTipNome: TWideStringField;
    DsWHisAltTip: TDataSource;
    QrWHisAltItsVersaoApp: TWideStringField;
    QrAplic: TmySQLQuery;
    QrLoc: TmySQLQuery;
    CBVersaoApp: TComboBox;
    QrWHistAltApl: TmySQLQuery;
    QrWHistAltAplAplicativo: TIntegerField;
    QrWHistAltAplNOMEAPL: TWideStringField;
    DsWHistAltApl: TDataSource;
    frxDsWHistAltApl: TfrxDBDataset;
    BtReabre2: TBitBtn;
    QrWHistAltItens: TmySQLQuery;
    QrWHistAltItensCodigo: TIntegerField;
    QrWHistAltItensNOMETIP: TWideStringField;
    QrWHistAltItensFinalizado: TSmallintField;
    QrWHistAltItensNome: TWideMemoField;
    QrWHistAltItensDATA: TWideStringField;
    DsWHistAltItens: TDataSource;
    frxDsWHistAltItens: TfrxDBDataset;
    OpenDialog1: TOpenDialog;
    PMImprime: TPopupMenu;
    Versoatual1: TMenuItem;
    Somentefinalizados1: TMenuItem;
    odos1: TMenuItem;
    frxWEB_HISAL_002_001: TfrxReport;
    QrWHistAltItensVersaoApp: TWideStringField;
    frxDsWHistAltItensFinaliz: TfrxDBDataset;
    QrWHistAltItensFinaliz: TmySQLQuery;
    IntegerField3: TIntegerField;
    StringField3: TWideStringField;
    SmallintField1: TSmallintField;
    MemoField1: TWideMemoField;
    StringField4: TWideStringField;
    QrWHistAltItensFinalizVersaoApp: TWideStringField;
    QrWHisAltCor: TWideStringField;
    QrWHistAltItensCor: TWideStringField;
    QrWHistAltItensFinalizCor: TWideStringField;
    CSTabSheetChamou: TdmkCompoStore;
    ImgWEB: TdmkImage;
    PMAplic: TPopupMenu;
    Incluiaplicativo1: TMenuItem;
    Incluidiversosaplicativos1: TMenuItem;
    BtWEB: TBitBtn;
    LaTotal: TLabel;
    BtEnvia: TBitBtn;
    QrWHistAltItensFinalizWHisAltTip: TIntegerField;
    QrWHistAltItensFinalizCorHex: TIntegerField;
    PB1: TProgressBar;
    RGTipAmb: TRadioGroup;
    QrWHisAltItsTipAmb: TSmallintField;
    RGPsqTipAmb: TRadioGroup;
    QrAplicCodigo: TIntegerField;
    QrAplicNome: TWideStringField;
    BtOpcoes: TBitBtn;
    QrWHisAltOpc: TmySQLQuery;
    QrWHisAltACHOU: TFloatField;
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrWHisAltAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrWHisAltBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
    procedure DBGWHisAltDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGWHisAltCellClick(Column: TColumn);
    procedure BtInclui2Click(Sender: TObject);
    procedure BtAltera2Click(Sender: TObject);
    procedure BtExclui2Click(Sender: TObject);
    procedure BtConfirma2Click(Sender: TObject);
    procedure BtDesiste2Click(Sender: TObject);
    procedure QrWHisAltAfterScroll(DataSet: TDataSet);
    procedure QrWHisAltBeforeClose(DataSet: TDataSet);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SBHisAltTipClick(Sender: TObject);
    procedure EdAplicChange(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure EdAplicativoChange(Sender: TObject);
    procedure BtReabre2Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure PMImprimePopup(Sender: TObject);
    procedure Somentefinalizados1Click(Sender: TObject);
    procedure odos1Click(Sender: TObject);
    procedure dmkDBGrid2CellClick(Column: TColumn);
    procedure dmkDBGrid2DblClick(Sender: TObject);
    procedure DBMemo2Change(Sender: TObject);
    procedure DBMemo1Change(Sender: TObject);
    procedure dmkDBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FormShow(Sender: TObject);
    procedure Incluiaplicativo1Click(Sender: TObject);
    procedure PMAplicPopup(Sender: TObject);
    procedure Incluidiversosaplicativos1Click(Sender: TObject);
    procedure BtWEBClick(Sender: TObject);
    procedure QrWHistAltItensAfterScroll(DataSet: TDataSet);
    procedure QrWHistAltItensBeforeClose(DataSet: TDataSet);
    procedure BtEnviaClick(Sender: TObject);
    procedure QrWHistAltItensFinalizCalcFields(DataSet: TDataSet);
    procedure RGTipAmbClick(Sender: TObject);
    procedure CBVersaoAppKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RGPsqTipAmbClick(Sender: TObject);
    procedure BtOpcoesClick(Sender: TObject);
  private
    FCodigo: Integer;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure ReopenHisAlt(Codigo: Integer);
    procedure FinalizaItem(Finalizado, Codigo: Integer);
    procedure ReopenAplic(TipAmb: Integer; Todos: Boolean);
    procedure ReopenHisAltIts(Controle, Aplicativo: Integer);
    procedure SetaVersoesAtuais(Aplicativo, TipAmb: Integer; Atual: Boolean);
    procedure ReopenWHistAltApl(TipAmb: Integer);
    procedure ReopenWHisAltItens(Codigo, Aplicativo, TipAmb: Integer; Versao: String);
    procedure ReopenWHisAltImp(Codigo: Integer; ApenasFinal: Boolean);
    procedure ImprimeWHisAlt(ApenasFinal: Boolean);
    procedure EnviaEmailHisAlt();
    function  ValidaFinaliz(Codigo, Finalizado: Integer): Boolean;
  public
    { Public declarations }
  end;

var
  FmWHisAlt: TFmWHisAlt;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, ModuleGeral, MyDBCheck, WHisAltTip, MyGlyfs,
  Principal, UnDmkWeb, WHisAltApl, UnitVersoes, UnMailEnv, UnAppPF,
  UnitVersoes_Jan;

{$R *.DFM}

procedure TFmWHisAlt.CBVersaoAppKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F5 then
    SetaVersoesAtuais(EdAplic.ValueVariant, RGTipAmb.ItemIndex, True);
end;

procedure TFmWHisAlt.CriaOForm;
begin
  DefineONomeDoForm;
end;

procedure TFmWHisAlt.QueryPrincipalAfterOpen;
begin
end;

function TFmWHisAlt.ValidaFinaliz(Codigo, Finalizado: Integer): Boolean;
begin
  if Finalizado = 1 then
  begin
    Result := False;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDBn, [
      'SELECT Controle ',
      'FROM whisaltits ',
      'WHERE Codigo=' + Geral.FF0(Codigo),
      '']);
    if QrLoc.RecordCount > 0 then
      Result := True;
    //
    QrLoc.Close;
  end else
    Result := True;
end;

procedure TFmWHisAlt.ReopenAplic(TipAmb: Integer; Todos: Boolean);
var
  Codigo: Integer;
begin
  if Todos then
  begin
    if TipAmb = CO_App_TipAmb_Int[0] then //Local
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrAplic, Dmod.MyDBn, [
        'SELECT apl.Codigo, apl.Nome ',
        'FROM aplicativos apl ',
        'WHERE apl.Ativo = 1 ',
        'ORDER BY apl.Nome ',
        '']);
    end else
    if TipAmb = CO_App_TipAmb_Int[1] then //Web
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrAplic, Dmod.MyDBn, [
        'SELECT apl.Codigo, apl.Nome ',
        'FROM appwebcab apl ',
        'WHERE apl.Ativo = 1 ',
        'ORDER BY apl.Nome ',
        '']);
    end else
      QrAplic.Close;
  end else
  begin                
    if (QrWHisAlt.State <> dsInactive) and (QrWHisAlt.RecordCount > 0) then
      Codigo := QrWHisAltCodigo.Value
    else
      Codigo := 0;
    //
    if TipAmb = CO_App_TipAmb_Int[0] then //Local
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrAplic, Dmod.MyDBn, [
        'SELECT apl.Codigo, apl.Nome ',
        'FROM aplicativos apl ',
        'WHERE apl.Codigo NOT IN ',
        '( ',
        'SELECT Aplicativo ',
        'FROM whisaltits ',
        'WHERE Codigo=' + Geral.FF0(Codigo),
        'AND TipAmb=' + Geral.FF0(CO_App_TipAmb_Int[0]),
        ') ',
        'AND apl.Ativo = 1 ',
        'ORDER BY apl.Nome ',
        '']);
    end else
    if TipAmb = CO_App_TipAmb_Int[1] then //Web
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrAplic, Dmod.MyDBn, [
        'SELECT apl.Codigo, apl.Nome ',
        'FROM appwebcab apl ',
        'WHERE apl.Codigo NOT IN ',
        '( ',
        'SELECT Aplicativo ',
        'FROM whisaltits ',
        'WHERE Codigo=' + Geral.FF0(Codigo),
        'AND TipAmb=' + Geral.FF0(CO_App_TipAmb_Int[1]),
        ') ',
        'AND apl.Ativo = 1 ',
        'ORDER BY apl.Nome ',
        '']);
    end else
      QrAplic.Close;
  end;
end;

procedure TFmWHisAlt.ReopenHisAlt(Codigo: Integer);
var
  Finalizado: Integer;
  Descri: String;
begin
  Descri     := EdDescricao.ValueVariant;
  Finalizado := RGFinalizado.ItemIndex;
  //
  QrWHisAlt.Close;
  QrWHisAlt.SQL.Clear;
  QrWHisAlt.SQL.Add('SELECT alt.*, tip.Nome NOMETIP, tip.Cor, ');
  QrWHisAlt.SQL.Add('if ((SELECT COUNT(its.Controle)');
  QrWHisAlt.SQL.Add('FROM whisaltits its');
  QrWHisAlt.SQL.Add('WHERE its.Codigo = alt.Codigo');
  QrWHisAlt.SQL.Add(') > 0, 1.0, 0.0) ACHOU,');
  QrWHisAlt.SQL.Add('DATE_FORMAT(IF(alt.DataAlt > 0, alt.DataAlt, alt.DataCad), "%d/%m/%Y") DATA');
  QrWHisAlt.SQL.Add('FROM whisalt alt');
  QrWHisAlt.SQL.Add('LEFT JOIN whisalttip tip ON tip.Codigo = alt.WHisAltTip');
  QrWHisAlt.SQL.Add('WHERE alt.Nome Like "%'+ Descri +'%"');
  QrWHisAlt.SQL.Add(dmkPF.SQL_Periodo('AND IF(alt.DataAlt > 2, alt.DataAlt, alt.DataCad) ',
                      TPDataI.Date, TPDataF.Date, True, True));
  case Finalizado of
    0: QrWHisAlt.SQL.Add('AND Finalizado=1');
    1: QrWHisAlt.SQL.Add('AND Finalizado=0');
  end;
  QrWHisAlt.SQL.Add('ORDER BY alt.Codigo DESC');
  UMyMod.AbreQuery(QrWHisAlt, DMod.MyDBn);
  //
  if Codigo > 0 then
    QrWHisAlt.Locate('Codigo', Codigo, []);
end;

procedure TFmWHisAlt.ReopenHisAltIts(Controle, Aplicativo: Integer);
begin
  QrWHisAltIts.Close;
  QrWHisAltIts.Params[0].AsInteger := QrWHisAltCodigo.Value;
  UMyMod.AbreQuery(QrWHisAltIts, DMod.MyDBn);
  //
  if Controle > 0 then
    QrWHisAltIts.Locate('Controle', Controle, [])
  else if Aplicativo <> 0 then
    QrWHisAltIts.Locate('Aplicativo', Aplicativo, [])
end;

procedure TFmWHisAlt.ReopenWHisAltItens(Codigo, Aplicativo, TipAmb: Integer; Versao: String);
var
  SQLAplic, SQLVersao, SQLTipAmb: String;
begin
  SQLAplic  := 'AND its.Aplicativo=' + Geral.FF0(Aplicativo);
  SQLVersao := 'AND its.VersaoApp="' + Versao + '"';
  //
  if TipAmb <> - 1 then
    SQLTipAmb := 'AND its.TipAmb="' + Geral.FF0(CO_App_TipAmb_Int[TipAmb]) + '"'
  else
    SQLTipAmb := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrWHistAltItens, Dmod.MyDBn, [
    'SELECT alt.Codigo, alt.Nome, alt.Finalizado, tip.Cor, ',
    'tip.Cor, tip.Nome NOMETIP, its.VersaoApp, ',
    'DATE_FORMAT(IF(alt.DataAlt > 0, alt.DataAlt, alt.DataCad), "%d/%m/%Y") DATA ',
    'FROM whisaltits its ',
    'LEFT JOIN whisalt alt ON alt.Codigo = its.Codigo ',
    'LEFT JOIN whisalttip tip ON tip.Codigo = alt.WHisAltTip ',
    'WHERE alt.Codigo <> 0 ',
    SQLAplic,
    SQLVersao,
    SQLTipAmb,
    'ORDER BY alt.Nome DESC ',
    '']);
  if Codigo > 0 then
    QrWHistAltItens.Locate('Codigo', Codigo, []);
end;

procedure TFmWHisAlt.ReopenWHisAltImp(Codigo: Integer; ApenasFinal: Boolean);
var
  SQL: String;
begin
  if ApenasFinal = True then
    SQL := 'AND alt.Finalizado=1 '
  else
    SQL := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrWHistAltItensFinaliz, Dmod.MyDBn, [
    'SELECT alt.Codigo, alt.Nome, alt.Finalizado, ',
    'tip.Cor, tip.Nome NOMETIP, its.VersaoApp, alt.WHisAltTip, ',
    'DATE_FORMAT(IF(alt.DataAlt > 0, alt.DataAlt, alt.DataCad), "%d/%m/%Y") DATA, ',
    'IF(alt.DataAlt > 0, alt.DataAlt, alt.DataCad) Dta_AlCad ',
    'FROM whisalt alt ',
    'LEFT JOIN whisaltits its ON its.Codigo = alt.Codigo ',
    'LEFT JOIN whisalttip tip ON tip.Codigo = alt.WHisAltTip ',
    'WHERE its.VersaoApp="' + CBVersao.Text + '"',
    'AND its.Aplicativo=' + Geral.FF0(EdAplicativo.ValueVariant),
    SQL,
    'ORDER BY its.VersaoApp, NOMETIP, Dta_AlCad, alt.Finalizado',
    '']);
  if Codigo > 0 then
    QrWHistAltItensFinaliz.Locate('Codigo', Codigo, []);
end;

procedure TFmWHisAlt.ReopenWHistAltApl(TipAmb: Integer);
begin
  if TipAmb > -1 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrWHistAltApl, Dmod.MyDBn, [
      'SELECT its.Aplicativo, IF(its.TipAmb = 0, apl.Nome, app.Nome) NOMEAPL ',
      'FROM whisaltits its ',
      'LEFT JOIN aplicativos apl ON apl.Codigo = its.Aplicativo ',
      'LEFT JOIN appwebcab app ON app.Codigo = its.Aplicativo ',
      'WHERE its.TipAmb=' + Geral.FF0(CO_App_TipAmb_Int[TipAmb]),
      'GROUP BY Aplicativo ',
      'ORDER BY NOMEAPL ',
      '']);
  end else
    QrWHistAltApl.Close;
end;

procedure TFmWHisAlt.RGPsqTipAmbClick(Sender: TObject);
begin
  ReopenWHistAltApl(RGPsqTipAmb.ItemIndex);
end;

procedure TFmWHisAlt.RGTipAmbClick(Sender: TObject);
var
  Todos: Boolean;
  TipAmb: Integer;
begin
  Todos  := ImgTipo.SQLType = stUpd;
  TipAmb := RGTipAmb.ItemIndex - 1;
  //
  ReopenAplic(TipAmb, Todos);
end;

procedure TFmWHisAlt.SBHisAltTipClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmWHisAltTip, FmWHisAltTip, afmoNegarComAviso) then
  begin
    FmWHisAltTip.ShowModal;
    FmWHisAltTip.Destroy;
    QrWHisAltTip.Close;
    QrWHisAltTip.Open;
    if VAR_CADASTRO <> 0 then
    begin
      EdWHisAltTip.ValueVariant := VAR_CADASTRO;
      CBWHisAltTip.KeyValue     := VAR_CADASTRO;
      EdWHisAltTip.SetFocus;
    end;
  end;
end;

procedure TFmWHisAlt.SbImprimeClick(Sender: TObject);
begin
  if PageControl1.ActivePageIndex <> 1 then
  begin
    PageControl1.ActivePageIndex := 1;
    PageControl1Change(Self);
  end;
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmWHisAlt.SetaVersoesAtuais(Aplicativo, TipAmb: Integer; Atual: Boolean);
var
  Combo: TComboBox;
begin
  if Aplicativo <> 0 then
  begin
    if Atual then
    begin
      if TipAmb = CO_App_TipAmb_Int[0] then //Local
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDBn, [
          'SELECT Versao ',
          'FROM wversao ',
          'WHERE Aplicativo=' + Geral.FF0(Aplicativo),
          'ORDER BY Versao DESC ',
          '']);
      end else
        QrLoc.Close;
      //
      Combo := CBVersaoApp; 
    end else
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDBn, [
        'SELECT VersaoApp Versao ',
        'FROM whisaltits ',
        'WHERE Aplicativo=' + Geral.FF0(Aplicativo),
        'AND TipAmb=' + Geral.FF0(CO_App_TipAmb_Int[TipAmb]),
        'AND VersaoApp <> "" ',
        'GROUP BY VersaoApp ',
        'ORDER BY VersaoApp DESC ',
        '']);
      Combo := CBVersao;
    end;
    //
    Combo.Items.Clear;
    //
    if (QrLoc.State <> dsInactive) and (QrLoc.RecordCount > 0) then
    begin
      while not QrLoc.Eof do
      begin
        Combo.Items.Add(QrLoc.FieldByName('Versao').AsString);
        //
        QrLoc.Next;
      end;
    end;
    QrLoc.Close;
  end;
end;

procedure TFmWHisAlt.Somentefinalizados1Click(Sender: TObject);
begin
  ImprimeWHisAlt(True);
end;

procedure TFmWHisAlt.SpeedButton1Click(Sender: TObject);
var
  Txt: String;
begin
  Txt := UnVersoes.App_ObtemVersaoApp(FmWHisAlt, RGTipAmb.ItemIndex - 1,
           EdAplic.ValueVariant, QrAplicNome.Value);
  //
  CBVersaoApp.Text := Txt;
  CBVersaoApp.SetFocus;
end;

procedure TFmWHisAlt.DefineONomeDoForm;
begin
end;

procedure TFmWHisAlt.dmkDBGrid2CellClick(Column: TColumn);
var
  Codigo, Finalizado: Integer;
begin
  if Column.FieldName = 'Finalizado' then
  begin
    Finalizado := QrWHistAltItensFinalizado.Value;
    Codigo     := QrWHistAltItensCodigo.Value;
    //
    FinalizaItem(Finalizado, Codigo);
    ReopenWHisAltItens(Codigo, EdAplicativo.ValueVariant, RGPsqTipAmb.ItemIndex, CBVersao.Text);
  end;
end;

procedure TFmWHisAlt.dmkDBGrid2DblClick(Sender: TObject);
var
  Aplicativo: Integer;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  //
  TPDataI.Date           := StrToDate(QrWHistAltItensDATA.Value);
  RGFinalizado.ItemIndex := 2;
  FCodigo                := QrWHistAltItensCodigo.Value;
  Aplicativo             := EdAplicativo.ValueVariant;
  //
  if TPDataF.Date < TPDataI.Date then
    TPDataF.Date := TPDataI.Date;
  //
  ReopenHisAlt(FCodigo);
  ReopenHisAltIts(0, Aplicativo);
  PageControl1.ActivePageIndex := 0;
  //
  BtInclui.Visible := True;
  BtAltera.Visible := True;
  BtExclui.Visible := True;
end;

procedure TFmWHisAlt.dmkDBGrid2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Cor: TColor;
begin
  if QrWHistAltItensCor.Value <> '' then
    Cor := DmkImg.HexToTColor(QrWHistAltItensCor.Value)
  else
    Cor := clBlack;
  //
  with dmkDBGrid2.Canvas do
  begin
    Font.Color := Cor;
    FillRect(Rect);
    TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
  end;
end;

procedure TFmWHisAlt.EdAplicativoChange(Sender: TObject);
var
  Aplic, TipAmb: Integer;
begin
  Aplic  := EdAplicativo.ValueVariant;
  TipAmb := RGPsqTipAmb.ItemIndex;
  //
  SetaVersoesAtuais(Aplic, TipAmb, False);
end;

procedure TFmWHisAlt.EdAplicChange(Sender: TObject);
var
  Aplic, TipAmb: Integer;
begin
  Aplic  := EdAplic.ValueVariant;
  TipAmb := RGTipAmb.ItemIndex - 1;
  //
  SetaVersoesAtuais(Aplic, TipAmb, True);
end;

procedure TFmWHisAlt.EnviaEmailHisAlt;
var
  Aplic, PreEmail: Integer;
  Lista, URL, Versao: String;
begin
  if (QrWHistAltItens.State <> dsInactive) and (QrWHistAltItens.RecordCount > 0) then
  begin
    UnVersoes.ReopenWHisAltOpc(QrWHisAltOpc, Dmod.MyDBn);
    //
    Aplic  := EdAplicativo.ValueVariant;
    Versao := CBVersao.Text;
    //
    if MyObjects.FIC(Aplic = 0, EdAplicativo, 'Selecione o aplicativo e tente novamente!') then
      Exit;
    if MyObjects.FIC(Versao = '', CBVersao, 'Selecione a vers�o e tente novamente!') then
      Exit;
    //
    URL      := dmkWeb.ObtemLinkHisAlt(Aplic, Versao);
    Lista    := UnVersoes.HisAlt_GeraListaHTML(EdAplicativo.ValueVariant,
                CBVersao.Text, Dmod.MyDBn, PB1);
    PreEmail := QrWHisAltOpc.FieldByName('PreMailHA').AsInteger;
    //
    if MyObjects.FIC(PreEmail = 0, nil, 'O pr� e-mail n�o foi definido nas configura��es!') then
      Exit;
    //
    if UMailEnv.Monta_e_Envia_Mail([EdAplicativo.ValueVariant, CBVersao.Text,
      PreEmail], meHisAlt, [], [QrWHistAltAplNOMEAPL.Value, Versao, Lista, URL],
      True, True)
    then
      Geral.MensagemBox('E-mail enviado com sucesso!', 'Aviso',
        MB_OK+MB_ICONWARNING);
  end;
end;

procedure TFmWHisAlt.DBGWHisAltCellClick(Column: TColumn);
var
  Codigo, Finalizado: Integer;
begin
  if Column.FieldName = 'Finalizado' then
  begin
    Finalizado := QrWHisAltFinalizado.Value;
    Codigo     := QrWHisAltCodigo.Value;
    //
    FinalizaItem(Finalizado, Codigo);
    ReopenHisAlt(Codigo);
  end;
end;

procedure TFmWHisAlt.DBGWHisAltDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Cor: TColor;
begin
  if QrWHisAltCor.Value <> '' then
    Cor := DmkImg.HexToTColor(QrWHisAltCor.Value)
  else
    Cor := clBlack;
  //
  with DBGWHisAlt.Canvas do
  begin
    Font.Color := Cor;
    FillRect(Rect);
    TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
  end;
end;

procedure TFmWHisAlt.DBMemo1Change(Sender: TObject);
begin
  case Trunc(QrWHisAltACHOU.Value) of
    0: DBMemo1.Font.Color := clRed;
    1: DBMemo1.Font.Color := clBlack;
  end;
end;

procedure TFmWHisAlt.DBMemo2Change(Sender: TObject);
begin
  case Trunc(QrWHisAltACHOU.Value) of
    0: DBMemo1.Font.Color := clRed;
    1: DBMemo1.Font.Color := clBlack;
  end;
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmWHisAlt.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWHisAlt.BtWEBClick(Sender: TObject);
var
  Aplic: Integer;
  Link, Versao: String;
begin
  Aplic  := EdAplicativo.ValueVariant;
  Versao := CBVersao.Text;
  //
  if MyObjects.FIC(Aplic = 0, EdAplicativo, 'Selecione o aplicativo e tente novamente!') then
    Exit;
  if MyObjects.FIC(Versao = '', CBVersao, 'Selecione a vers�o e tente novamente!') then
    Exit;
  //
  Link := dmkWeb.ObtemLinkHisAlt(Aplic, Versao);
  //
  DmkWeb.MostraWebBrowser(Link, True, False, 0, 0);
end;

procedure TFmWHisAlt.BtInclui2Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAplic, BtInclui2);
end;

procedure TFmWHisAlt.BtAltera2Click(Sender: TObject);
begin
  if (QrWHisAlt.State <> dsInactive) and (QrWHisAlt.RecordCount > 0) and
    (QrWHisAltIts.State <> dsInactive) and (QrWHisAltIts.RecordCount > 0) then
  begin
    if DBGWHisAlt.SelectedRows.Count > 1 then
      Geral.MB_Aviso('Voc� deve alterar um item de cada vez!' + sLineBreak +
        'Selecione apenas um item e tente novamente!')
    else
      MostraEdicao(2, stUpd, 0);
  end;
end;

procedure TFmWHisAlt.BtEnviaClick(Sender: TObject);
begin
  if PageControl1.ActivePageIndex <> 1 then
  begin
    PageControl1.ActivePageIndex := 1;
    PageControl1Change(Self);
  end;
  EnviaEmailHisAlt;
end;

procedure TFmWHisAlt.BtExclui2Click(Sender: TObject);
begin
  if (QrWHisAlt.State <> dsInactive) and (QrWHisAlt.RecordCount > 0) and
    (QrWHisAltIts.State <> dsInactive) and (QrWHisAltIts.RecordCount > 0) then
  begin
    if QrWHisAltFinalizado.Value = 0 then //Exclui apenas n�o finalizados
    begin
      (* Desativado em: 02/10/2016 => Motivo: Aparentemente n�o tem problema excluir v�rios
      if DBGWHisAlt.SelectedRows.Count > 1 then
        Geral.MB_Aviso('Voc� deve excluir um item de cada vez!' + sLineBreak +
          'Selecione apenas um item e tente novamente!')
      else
      begin
      *)
      DBCheck.QuaisItens_Exclui(Dmod.QrUpdN, QrWHisAltIts, DBGWHisAltIts,
        'whisaltits', ['Controle'], ['Controle'], istPergunta, '');
      //
      ReopenHisAlt(QrWHisAltCodigo.Value);
    end else
      Geral.MB_Aviso('Exclus�o abortada!' + sLineBreak +
        'Motivo: Este item j� foi finalizado!');
  end;
end;

procedure TFmWHisAlt.BtConfirma2Click(Sender: TObject);

  procedure InsereRegistro(Codigo, TipAmb: Integer; Versao: String);
  var
    Controle: Integer;
  begin
    if ImgTipo.SQLType = stIns then
      Controle := UMyMod.BuscaNovoCodigo_Int(Dmod.QrUpdN, 'whisaltits',
        'Controle', [], [], stIns, 0, siPositivo, nil)
    else
      Controle := QrWHisAltItsControle.Value;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpdN, ImgTipo.SQLType, 'whisaltits', False,
      ['Aplicativo', 'TipAmb', 'VersaoApp', 'Codigo'], ['Controle'],
      [EdAplic.ValueVariant, TipAmb, Versao, Codigo], [Controle], True);
  end;
var
  i, TipAmb, Aplic: Integer;
  Versao: String;
begin
  TipAmb := RGTipAmb.ItemIndex - 1;
  Aplic  := EdAplic.ValueVariant;
  Versao := CBVersaoApp.Text;
  //
  if MyObjects.FIC(TipAmb < 0, RGTipAmb, 'Tipo de ambiente n�o definido!') then Exit;
  if MyObjects.FIC(Aplic = 0, EdAplic, 'Aplicativo n�o definido!') then Exit;
  //
  if DBGWHisAlt.SelectedRows.Count > 1 then
  begin
    if ImgTipo.SQLType = stIns then
    begin
      with DBGWHisAlt.DataSource.DataSet do
      for i:= 0 to DBGWHisAlt.SelectedRows.Count - 1 do
      begin
        GotoBookmark(DBGWHisAlt.SelectedRows.Items[i]);
        //
        InsereRegistro(QrWHisAltCodigo.Value, TipAmb, Versao);
      end;
    end else
    begin
      Geral.MB_Aviso('Voc� deve alterar um item de cada vez!' + sLineBreak +
        'Selecione apenas um item e tente novamente!');
      Exit;
    end;
  end else
    InsereRegistro(QrWHisAltCodigo.Value, TipAmb, Versao);
  //
  ReopenHisAlt(QrWHisAltCodigo.Value);
  ReopenAplic(-1, False);
  //
  if CkContIts.Checked then
  begin
    Geral.MB_Aviso('Dados salvos com sucesso!');
    RGTipAmb.ItemIndex := 0;
    RGTipAmb.SetFocus;
  end else
    MostraEdicao(0, stLok, 0);
end;

procedure TFmWHisAlt.BtDesiste2Click(Sender: TObject);
begin
  MostraEdicao(0, stLok, 0);
end;

procedure TFmWHisAlt.BtReabre2Click(Sender: TObject);
begin
  ReopenWHisAltItens(0, EdAplicativo.ValueVariant, RGPsqTipAmb.ItemIndex, CBVersao.Text);
end;

procedure TFmWHisAlt.BtReabreClick(Sender: TObject);
begin
  ReopenHisAlt(0);
end;

procedure TFmWHisAlt.BtAlteraClick(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrWHisAlt.State <> dsInactive) and (QrWHisAlt.RecordCount > 0);
  //
  if Enab then
    MostraEdicao(1, stUpd, QrWHisAltCodigo.Value);
end;

procedure TFmWHisAlt.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrWHisAltCodigo.Value;
  if TFmWHisAlt(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmWHisAlt.BtConfirmaClick(Sender: TObject);
var
  Codigo, HisAltTip, Finalizado: Integer;
  Nome: String;
begin
  Nome      := MeNome.Text;
  HisAltTip := EdWHisAltTip.ValueVariant;
  //
  if MyObjects.FIC(Length(Nome) = 0, MeNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(HisAltTip = 0, EdWHisAltTip,
    'Defina o tipo de hist�rico de altera��o!') then Exit;
  //
  if ImgTipo.SQLType = stIns then
  begin
    Finalizado := 0;
    Codigo     := UMyMod.BuscaNovoCodigo_Int(Dmod.QrUpdN, 'whisalt', 'Codigo',
                    [], [], stIns, 0, siPositivo, nil);
  end else
  begin
    Finalizado := Geral.BoolToInt(CkFinalizado.Checked);
    Codigo     := QrWHisAltCodigo.Value;
    //
    if MyObjects.FIC(ValidaFinaliz(Codigo, Finalizado) = False, nil,
      'Voc� deve informar no m�nimo um aplicativo antes de finalizar!') then Exit;
  end;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpdN, ImgTipo.SQLType, 'whisalt', False,
    ['Nome', 'WHisAltTip', 'Finalizado'], ['Codigo'],
    [Nome, HisAltTip, Finalizado], [Codigo], True) then
  begin
    ReopenHisAlt(Codigo);
    if CkContinuar.Checked then
    begin
      Geral.MB_Aviso('Dados salvos com sucesso!');
      //
      MeNome.Text := '';
      //
      EdWHisAltTip.SetFocus;
    end else
      MostraEdicao(0, stLok, 0);
  end;
end;

procedure TFmWHisAlt.BtDesisteClick(Sender: TObject);
begin
  MostraEdicao(0, stLok, 0);
end;

procedure TFmWHisAlt.BtExcluiClick(Sender: TObject);
var
  Codigo: Integer;
begin
  if (QrWHisAltIts.State <> dsInactive) and (QrWHisAltIts.RecordCount > 0) then
  begin
    Geral.MB_Aviso('Exclus�o abortada!' + sLineBreak +
      'Motivo: O item selecionado possui subitens.')
  end else
  begin
    if Geral.MB_Pergunta('Deseja excluir o item selecionado?') = ID_YES then
    begin
      Codigo := QrWHisAltCodigo.Value;
      //
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpdN, Dmod.MyDBn, [
        'DELETE FROM whisalt ',
        'WHERE Codigo=' + Geral.FF0(Codigo),
        '']);
      //
      ReopenHisAlt(0);
    end;
  end;
end;

procedure TFmWHisAlt.BtIncluiClick(Sender: TObject);
begin
  MostraEdicao(1, stIns, 0);
end;

procedure TFmWHisAlt.BtOpcoesClick(Sender: TObject);
begin
  UnVersoes_Jan.MostraFormWHisAltOpc();
end;

procedure TFmWHisAlt.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FCodigo         := 0;
  //
  RGTipAmb.Items    := UnVersoes.App_ObtemListaTipAmb(True);
  RGPsqTipAmb.Items := UnVersoes.App_ObtemListaTipAmb(False);
  //
  EdAplicativo.ValueVariant := 0;
  CBAplicativo.KeyValue     := Null;
  CBVersao.Text             := '';
  //
  MostraEdicao(0, stLok, 0);
  CriaOForm;
  //
  UnVersoes.HisAlt_ExcluiHistoricoDeVersoesAntigas(Dmod.MyDBn, 30);
  //
  UMyMod.AbreQuery(QrWHisAltTip, DMod.MyDBn);
  ReopenHisAlt(0);
end;

procedure TFmWHisAlt.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmWHisAlt.QrWHisAltAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
  //
  Panel9.Visible := QrWHisAlt.RecordCount > 0;
end;

procedure TFmWHisAlt.QrWHisAltAfterScroll(DataSet: TDataSet);
begin
  ReopenHisAltIts(0, 0);
end;

procedure TFmWHisAlt.FinalizaItem(Finalizado, Codigo: Integer);
begin
  if ValidaFinaliz(Codigo, Finalizado) = True then
  begin
    if Finalizado = 0 then
      Finalizado := 1
    else
      Finalizado := 0;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpdN, stUpd, 'whisalt', False,
      ['Finalizado'], ['Codigo'],
      [Finalizado], [Codigo], True);
  end;
end;

procedure TFmWHisAlt.FormActivate(Sender: TObject);
begin
  DmkWeb.CarregaImagemFormWEB(ImgWEB);
  //
  if TFmWHisAlt(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end;
end;

procedure TFmWHisAlt.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWHisAlt.FormShow(Sender: TObject);
begin
{$IfNDef cSkinRank} //Berlin
{$IfNDef cAlphaSkin} //Berlin
  FmMyGlyfs.DefineGlyfs(TForm(Sender));
{$EndIf}
{$EndIf}
{$IfDef cSkinRank} //Berlin
  if FmPrincipal.Sd1.Active then
    FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
{$EndIf}
{$IfDef cAlphaSkin} //Berlin
  if FmPrincipal.sSkinManager1.Active then
    FmMyGlyfs.DefineGlyfsTDI2(FmPrincipal.sSkinManager1, Sender);
{$EndIf}
end;

procedure TFmWHisAlt.ImprimeWHisAlt(ApenasFinal: Boolean);
begin
  ReopenWHisAltImp(0, ApenasFinal);
  //
  MyObjects.frxDefineDataSets(frxWEB_HISAL_002_001, [
    DModG.frxDsDono,
    frxDsWHistAltApl,
    frxDsWHistAltItensFinaliz
    ]);
  MyObjects.frxMostra(frxWEB_HISAL_002_001, 'Hist�rico de altera��es');
  //Agrupar por tipo e colorir
end;

procedure TFmWHisAlt.Incluiaplicativo1Click(Sender: TObject);
begin
  MostraEdicao(2, stIns, 0);
end;

procedure TFmWHisAlt.Incluidiversosaplicativos1Click(Sender: TObject);
begin
  if DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrWebParams, 1) then
  begin
    if DBCheck.CriaFm(TFmWHisAltApl, FmWHisAltApl, afmoNegarComAviso) then
    begin
      FmWHisAltApl.FCodigo := QrWHisAltCodigo.Value;
      FmWHisAltApl.ShowModal;
      FmWHisAltApl.Destroy;
      //
      ReopenHisAlt(QrWHisAltCodigo.Value);
    end;
  end;
end;

procedure TFmWHisAlt.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
var
  Hoje: TDateTime;
begin
  Hoje := DModG.ObtemAgora;
  //
  PageControl1.Align := alClient;
  MeNome.Align       := alClient;
  //
  EdDescricao.ValueVariant := '';
  TPDataI.Date             := Geral.PrimeiroDiaDoMes(Hoje);
  TPDataF.Date             := Geral.UltimoDiaDoMes(Hoje);
  RGFinalizado.ItemIndex   := 2; //Ambos
  //
  ImgTipo.SQLType := SQLType;
  //
  case Mostra of
    0:
    begin
      GBCntrl.Visible              := True;
      PnDados.Visible              := True;
      PnEdita.Visible              := False;
      PnAplic.Visible              := False;
      PnAplicMenu.Visible          := True;
      PageControl1.ActivePageIndex := 0;
      //
      DBGWHisAlt.Enabled    := True;
      DBGWHisAltIts.Enabled := True;
      //
      BtInclui.Enabled := True;
      BtAltera.Enabled := True;
      BtExclui.Enabled := True;
      BtReabre.Enabled := True;     
    end;
    1:
    begin
      PnEdita.Visible := True;
      PnDados.Visible := False;
      GBCntrl.Visible := False;
      //
      DBGWHisAlt.Enabled    := False;
      DBGWHisAltIts.Enabled := False;
      //
      if SQLType = stIns then
      begin
        EdCodigo.ValueVariant     := FormatFloat(FFormatFloat, Codigo);
        EdWHisAltTip.ValueVariant := 0;
        CBWHisAltTip.KeyValue     := Null;
        MeNome.Text               := '';
        CkFinalizado.Enabled      := False;
        CkFinalizado.Checked      := False;
        CkContinuar.Checked       := False;
        CkContinuar.Visible       := True;
      end else begin
        EdCodigo.ValueVariant     := QrWHisAltCodigo.Value;
        EdWHisAltTip.ValueVariant := QrWHisAltWHisAltTip.Value;
        CBWHisAltTip.KeyValue     := QrWHisAltWHisAltTip.Value;
        MeNome.Text               := QrWHisAltNome.Value;
        CkFinalizado.Enabled      := True;
        CkFinalizado.Checked      := Geral.IntToBool(QrWHisAltFinalizado.Value);
        CkContinuar.Checked       := False;
        CkContinuar.Visible       := False;
        //
      end;
      EdWHisAltTip.SetFocus;
    end;
    2:
    begin
      PnAplic.Visible     := True;
      PnAplicMenu.Visible := False;
      //
      DBGWHisAlt.Enabled    := False;
      DBGWHisAltIts.Enabled := False;
      //
      if SQLType = stIns then
      begin
        ReopenAplic(-1, False);
        //
        RGTipAmb.ItemIndex   := 0;
        EdAplic.ValueVariant := 0;
        CBAplic.KeyValue     := Null;
        CBVersaoApp.Text     := '';
        CkContIts.Checked    := False;
        CkContIts.Visible    := True;
      end else begin
        ReopenAplic(QrWHisAltItsTipAmb.Value, True);
        //
        RGTipAmb.ItemIndex   := QrWHisAltItsTipAmb.Value + 1;
        EdAplic.ValueVariant := QrWHisAltItsAplicativo.Value;
        CBAplic.KeyValue     := QrWHisAltItsAplicativo.Value;
        CBVersaoApp.Text     := QrWHisAltItsVersaoApp.Value;
        CkContIts.Checked    := False;
        CkContIts.Visible    := False;
      end;
      BtInclui.Enabled := False;
      BtAltera.Enabled := False;
      BtExclui.Enabled := False;
      BtReabre.Enabled := False;
      //
      RGTipAmb.SetFocus;
    end;
    else
      Geral.MB_Aviso('A��o de Inclus�o/altera��o n�o definida!');
  end;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmWHisAlt.odos1Click(Sender: TObject);
begin
  ImprimeWHisAlt(False);
end;

procedure TFmWHisAlt.PageControl1Change(Sender: TObject);
var
  Aplic, TipAmb: Integer;
  Versao: String;
  Enab: Boolean;
begin
  Enab := PageControl1.ActivePageIndex = 0;
  //
  BtInclui.Visible := Enab;
  BtAltera.Visible := Enab;
  BtExclui.Visible := Enab;
  //
  case PageControl1.ActivePageIndex of
    0:
    begin
      try
        Screen.Cursor := crHourGlass;
        //
        MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
        //
        ReopenHisAlt(FCodigo);
        QrWHistAltApl.Close;
      finally
        Screen.Cursor := crDefault;
      end;
    end;
    1:
    begin
      try
        Screen.Cursor := crHourGlass;
        //
        MyObjects.Informa2(LaAviso1, LaAviso2, False, 'D� um duplo clique na grade para localizar o item!');
        //
        TipAmb := RGPsqTipAmb.ItemIndex;
        Aplic  := EdAplicativo.ValueVariant;
        Versao := CBVersao.Text;
        //
        ReopenWHistAltApl(TipAmb);
        //
        if (Aplic <> 0) and (TipAmb > -1) then
        begin
          EdAplicativo.ValueVariant := Aplic;
          CBAplicativo.KeyValue     := Aplic;
          //
          SetaVersoesAtuais(Aplic, TipAmb, False);
        end;
        if Versao <> '' then
          CBVersao.Text := Versao;
        //
        ReopenWHisAltItens(0, Aplic, TipAmb, Versao);
      finally
        Screen.Cursor := crDefault;
      end;
    end;
  end;
end;

procedure TFmWHisAlt.PMAplicPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrWHisAlt.State <> dsInactive) and (QrWHisAlt.RecordCount > 0);
  //
  Incluiaplicativo1.Enabled          := Enab;
  Incluidiversosaplicativos1.Enabled := Enab;
end;

procedure TFmWHisAlt.PMImprimePopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrWHistAltItens.State <> dsInactive) and (QrWHistAltItens.RecordCount > 0);
  // 
  Versoatual1.Enabled := Enab;
end;

procedure TFmWHisAlt.QrWHisAltBeforeClose(DataSet: TDataSet);
begin
  QrWHisAltIts.Close;
  QrAplic.Close;
end;

procedure TFmWHisAlt.QrWHisAltBeforeOpen(DataSet: TDataSet);
begin
  QrWHisAltCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmWHisAlt.QrWHistAltItensAfterScroll(DataSet: TDataSet);
begin
  LaTotal.Caption := 'Total de itens: ' + Geral.FF0(QrWHistAltItens.RecordCount)
end;

procedure TFmWHisAlt.QrWHistAltItensBeforeClose(DataSet: TDataSet);
begin
  LaTotal.Caption := '';
end;

procedure TFmWHisAlt.QrWHistAltItensFinalizCalcFields(DataSet: TDataSet);
begin
  QrWHistAltItensFinalizCorHex.Value := DmkImg.HexToTColor(QrWHistAltItensFinalizCor.Value);
end;

end.

