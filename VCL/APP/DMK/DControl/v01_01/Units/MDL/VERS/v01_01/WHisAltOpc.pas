unit WHisAltOpc;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, dmkEdit, UnDmkEnums, DmkDAC_PF,
  dmkDBLookupComboBox, dmkEditCB, dmkPermissoes, mySQLDbTables;

type
  TFmWHisAltOpc = class(TForm)
    PnCabeca: TPanel;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    ImgWEB: TdmkImage;
    Label4: TLabel;
    EdPreMailHA: TdmkEditCB;
    CBPreMailHA: TdmkDBLookupComboBox;
    SpeedButton1: TSpeedButton;
    QrPreEmail: TmySQLQuery;
    IntegerField1: TIntegerField;
    QrPreEmailNome: TWideStringField;
    DsPreEmail: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    QrWHisAltOpc: TmySQLQuery;
    DsWHisAltOpc: TDataSource;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
    procedure MostraEdicao;
  public
    { Public declarations }
  end;

  var
  FmWHisAltOpc: TFmWHisAltOpc;

implementation

uses UnMyObjects, Module, UMySQLModule, ModuleGeral, UnDmkWeb, MyDBCheck, PreEmail;

{$R *.DFM}

procedure TFmWHisAltOpc.BtOKClick(Sender: TObject);
var
  PreMailHA: Integer;
begin
  PreMailHA := EdPreMailHA.ValueVariant;
  //
  if UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDBn, [
    'UPDATE whisaltopc SET ',
    'PreMailHA=' + Geral.FF0(PreMailHA),
    '']) then
  begin
    Close;
  end;
end;

procedure TFmWHisAltOpc.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWHisAltOpc.FormActivate(Sender: TObject);
begin
  DmkWeb.CarregaImagemFormWEB(ImgWEB);
  MyObjects.CorIniComponente();
end;

procedure TFmWHisAltOpc.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  MostraEdicao;
{$IfNDef NO_USE_EMAILDMK}
  UMyMod.AbreQuery(QrPreEmail, DMod.MyDB);
{$EndIf}
  //
  CBPreMailHA.ListSource := DsPreEmail;
end;

procedure TFmWHisAltOpc.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWHisAltOpc.MostraEdicao;
begin
  UMyMod.AbreQuery(QrWHisAltOpc, DMod.MyDBn);
  //
  EdPreMailHA.ValueVariant := QrWHisAltOpc.FieldByName('PreMailHA').AsInteger;
  CBPreMailHA.KeyValue     := QrWHisAltOpc.FieldByName('PreMailHA').AsInteger;
end;

procedure TFmWHisAltOpc.SpeedButton1Click(Sender: TObject);
{$IfNDef NO_USE_EMAILDMK}
var
  PreEmail: Integer;
begin
  VAR_CADASTRO := 0;
  PreEmail     := EdPreMailHA.ValueVariant;
  //
  if DBCheck.CriaFm(TFmPreEmail, FmPreEmail, afmoNegarComAviso) then
  begin
    if PreEmail <> 0 then
      FmPreEmail.LocCod(PreEmail, PreEmail);
    FmPreEmail.ShowModal;
    FmPreEmail.Destroy;
    //
    if VAR_CADASTRO <> 0 then
    begin
      UMyMod.SetaCodigoPesquisado(EdPreMailHA, CBPreMailHA, QrPreEmail, VAR_CADASTRO);
      EdPreMailHA.SetFocus;
    end;
  end;
{$Else}
begin
  dmkPF.InfoSemModulo(mdlappEmail);
{$EndIf}
end;

end.
