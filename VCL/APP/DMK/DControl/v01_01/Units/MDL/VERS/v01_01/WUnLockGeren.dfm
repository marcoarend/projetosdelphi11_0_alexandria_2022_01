object FmWUnLockGeren: TFmWUnLockGeren
  Left = 339
  Top = 185
  ActiveControl = EdCliente
  Caption = 'WEB-LICAP-001 :: Gerenciamento de licen'#231'as de aplicativos'
  ClientHeight = 895
  ClientWidth = 1238
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 120
  TextHeight = 16
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1238
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 1130
      Top = 0
      Width = 108
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 59
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
      object ImgWEB: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
      object BtOpcoes: TBitBtn
        Tag = 348
        Left = 5
        Top = 5
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOpcoesClick
      end
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 1071
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 589
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Gerenciamento de licen'#231'as de aplicativos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 589
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Gerenciamento de licen'#231'as de aplicativos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 589
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Gerenciamento de licen'#231'as de aplicativos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 59
    Width = 1238
    Height = 695
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1238
      Height = 695
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1238
        Height = 695
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 18
          Width = 1234
          Height = 190
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          TabOrder = 0
          object Label3: TLabel
            Left = 15
            Top = 7
            Width = 44
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Cliente:'
          end
          object Label8: TLabel
            Left = 543
            Top = 7
            Width = 204
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Aplicativo: [F4 para mostrar todos]'
          end
          object Label1: TLabel
            Left = 965
            Top = 7
            Width = 47
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Vers'#227'o:'
          end
          object Label2: TLabel
            Left = 15
            Top = 65
            Width = 168
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Vers'#227'o do banco de dados:'
          end
          object Label4: TLabel
            Left = 207
            Top = 65
            Width = 191
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Vers'#227'o do sistema operacional:'
          end
          object EdCliente: TdmkEditCB
            Left = 15
            Top = 28
            Width = 69
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdClienteChange
            DBLookupComboBox = CBCliente
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBCliente: TdmkDBLookupComboBox
            Left = 84
            Top = 28
            Width = 449
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsClientes
            TabOrder = 1
            dmkEditCB = EdCliente
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdAplicativo: TdmkEditCB
            Left = 543
            Top = 28
            Width = 69
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdAplicativoChange
            OnKeyDown = EdAplicativoKeyDown
            DBLookupComboBox = CBAplicativo
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBAplicativo: TdmkDBLookupComboBox
            Left = 612
            Top = 28
            Width = 344
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsAplic
            TabOrder = 3
            dmkEditCB = EdAplicativo
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object BtReabre: TBitBtn
            Tag = 18
            Left = 15
            Top = 123
            Width = 147
            Height = 49
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Reabre'
            NumGlyphs = 2
            TabOrder = 10
            OnClick = BtReabreClick
          end
          object RGAtivo: TRadioGroup
            Left = 399
            Top = 65
            Width = 246
            Height = 49
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Mostrar dispositivos'
            Columns = 3
            Items.Strings = (
              'Ativos'
              'Inativos'
              'Ambos')
            TabOrder = 7
          end
          object RGSuporte: TRadioGroup
            Left = 652
            Top = 65
            Width = 246
            Height = 49
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Mostrar dispositivos de suporte'
            Columns = 3
            Items.Strings = (
              'Sim'
              'N'#227'o'
              'Ambos')
            TabOrder = 8
          end
          object RGBeta: TRadioGroup
            Left = 903
            Top = 65
            Width = 247
            Height = 49
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Mostrar dispositivos BETA'
            Columns = 3
            Items.Strings = (
              'Sim'
              'N'#227'o'
              'Ambos')
            TabOrder = 9
          end
          object CBVersao: TComboBox
            Left = 965
            Top = 28
            Width = 185
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            AutoDropDown = True
            TabOrder = 4
          end
          object CBVersaoDB: TComboBox
            Left = 15
            Top = 89
            Width = 184
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            AutoDropDown = True
            TabOrder = 5
          end
          object CBVersaoOS: TComboBox
            Left = 207
            Top = 89
            Width = 184
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            AutoDropDown = True
            TabOrder = 6
          end
          object PB1: TProgressBar
            Left = 170
            Top = 138
            Width = 980
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 11
          end
        end
        object PageControl1: TPageControl
          Left = 2
          Top = 208
          Width = 1234
          Height = 485
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          ActivePage = TabSheet1
          Align = alClient
          TabHeight = 25
          TabOrder = 1
          object TabSheet1: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Lista'
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object LaTotal: TLabel
              Left = 0
              Top = 434
              Width = 3
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alBottom
            end
            object dmkDBGrid1: TdmkDBGrid
              Left = 0
              Top = 0
              Width = 1226
              Height = 434
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Ativo'
                  Width = 40
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Conta'
                  Title.Caption = 'ID'
                  Width = 75
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMECLI'
                  Title.Caption = 'Cliente'
                  Width = 200
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CliAtivo'
                  Title.Caption = 'Cli. Ativo'
                  Width = 75
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMEAPLIC'
                  Title.Caption = 'Aplicativo'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'FimData_Txt'
                  Title.Caption = 'Data de expira'#231#227'o'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMESTATUS'
                  Title.Caption = 'Status'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DescriPC'
                  Title.Caption = 'Descri'#231#227'o do computador'
                  Width = 150
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NomePC'
                  Title.Caption = 'Nome do computador'
                  Width = 150
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Servidor'
                  Width = 75
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'PCSuporte'
                  Title.Caption = 'PC Suporte'
                  Width = 75
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Beta'
                  Title.Caption = 'PC app Beta'
                  Width = 75
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'BetaCliApp'
                  Title.Caption = 'App BETA'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ConsultaDH'
                  Title.Caption = 'Data da '#250'ltima verifica'#231#227'o'
                  Width = 140
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Cadastro'
                  Width = 140
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'VersaoAtu'
                  Title.Caption = 'Vers'#227'o'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'IP'
                  Title.Caption = 'IP (Local)'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'IPServidor'
                  Title.Caption = 'IP Servidor'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DBVersion'
                  Title.Caption = 'Vers'#227'o do banco de dados'
                  Width = 150
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'OSVersion'
                  Title.Caption = 'Vers'#227'o do sistema operacional'
                  Width = 150
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'OSArquitetu'
                  Title.Caption = 'Arquitetura'
                  Width = 100
                  Visible = True
                end>
              Color = clWindow
              DataSource = DsWUnLockGeren
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
              TabOrder = 0
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -14
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              OnCellClick = dmkDBGrid1CellClick
              OnDrawColumnCell = dmkDBGrid1DrawColumnCell
              OnDblClick = dmkDBGrid1DblClick
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Ativo'
                  Width = 40
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Conta'
                  Title.Caption = 'ID'
                  Width = 75
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMECLI'
                  Title.Caption = 'Cliente'
                  Width = 200
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CliAtivo'
                  Title.Caption = 'Cli. Ativo'
                  Width = 75
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMEAPLIC'
                  Title.Caption = 'Aplicativo'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'FimData_Txt'
                  Title.Caption = 'Data de expira'#231#227'o'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMESTATUS'
                  Title.Caption = 'Status'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DescriPC'
                  Title.Caption = 'Descri'#231#227'o do computador'
                  Width = 150
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NomePC'
                  Title.Caption = 'Nome do computador'
                  Width = 150
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Servidor'
                  Width = 75
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'PCSuporte'
                  Title.Caption = 'PC Suporte'
                  Width = 75
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Beta'
                  Title.Caption = 'PC app Beta'
                  Width = 75
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'BetaCliApp'
                  Title.Caption = 'App BETA'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ConsultaDH'
                  Title.Caption = 'Data da '#250'ltima verifica'#231#227'o'
                  Width = 140
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Cadastro'
                  Width = 140
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'VersaoAtu'
                  Title.Caption = 'Vers'#227'o'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'IP'
                  Title.Caption = 'IP (Local)'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'IPServidor'
                  Title.Caption = 'IP Servidor'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DBVersion'
                  Title.Caption = 'Vers'#227'o do banco de dados'
                  Width = 150
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'OSVersion'
                  Title.Caption = 'Vers'#227'o do sistema operacional'
                  Width = 150
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'OSArquitetu'
                  Title.Caption = 'Arquitetura'
                  Width = 100
                  Visible = True
                end>
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 754
    Width = 1238
    Height = 55
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 1234
      Height = 35
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 809
    Width = 1238
    Height = 86
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 1058
      Top = 18
      Width = 178
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 18
      Width = 1056
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtMenu: TBitBtn
        Tag = 237
        Left = 15
        Top = 5
        Width = 147
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Menu'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtMenuClick
      end
      object Panel13: TPanel
        Left = 828
        Top = 0
        Width = 228
        Height = 66
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 608
    Top = 12
  end
  object QrWUnLockGeren: TMySQLQuery
    Database = Dmod.MyDBn
    BeforeClose = QrWUnLockGerenBeforeClose
    AfterScroll = QrWUnLockGerenAfterScroll
    SQL.Strings = (
      'SELECT li.Conta, li.NomePC, li.Cadastro, li.ConsultaDH, li.Beta,'
      'li.IP, li.Servidor, li.PCSuporte, li.DescriPC, li.Status,'
      'ap.Nome NOMEAPLIC, ca.FimData, ca.Controle, '
      'IF (en.Tipo=0, en.RazaoSocial, en.Nome) NOMECLI,'
      '"Monitorado" NOMESTATUS, li.Ativo, ca.QtdLicen,'
      'cl.Cliente, li.VersaoAtu, cl.Ativo + 0.000 CliAtivo,'
      'li.OSVersion, li.DBVersion'
      'FROM cliaplicli li'
      'LEFT JOIN cliaplic ca ON ca.Controle = li.Controle'
      'LEFT JOIN aplicativos ap ON ap.Codigo = ca.Aplicativo'
      'LEFT JOIN clientes cl ON cl.Codigo = ca.Codigo'
      'LEFT JOIN entidades en ON en.Codigo = cl.Cliente'
      'WHERE cl.Ativo = 1'
      'AND li.Controle > 0'
      'ORDER BY ca.FimData, NOMECLI, NOMEAPLIC')
    Left = 414
    Top = 282
    object QrWUnLockGerenConta: TAutoIncField
      FieldName = 'Conta'
    end
    object QrWUnLockGerenNomePC: TWideStringField
      FieldName = 'NomePC'
      Size = 255
    end
    object QrWUnLockGerenCadastro: TDateTimeField
      FieldName = 'Cadastro'
    end
    object QrWUnLockGerenConsultaDH: TDateTimeField
      FieldName = 'ConsultaDH'
    end
    object QrWUnLockGerenIP: TWideStringField
      FieldName = 'IP'
      Size = 40
    end
    object QrWUnLockGerenServidor: TSmallintField
      FieldName = 'Servidor'
      MaxValue = 1
    end
    object QrWUnLockGerenPCSuporte: TSmallintField
      FieldName = 'PCSuporte'
      MaxValue = 1
    end
    object QrWUnLockGerenDescriPC: TWideStringField
      FieldName = 'DescriPC'
      Size = 50
    end
    object QrWUnLockGerenStatus: TSmallintField
      FieldName = 'Status'
    end
    object QrWUnLockGerenNOMEAPLIC: TWideStringField
      FieldName = 'NOMEAPLIC'
      Size = 50
    end
    object QrWUnLockGerenFimData: TDateField
      FieldName = 'FimData'
    end
    object QrWUnLockGerenNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrWUnLockGerenNOMESTATUS: TWideStringField
      FieldName = 'NOMESTATUS'
      Size = 100
    end
    object QrWUnLockGerenAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
    object QrWUnLockGerenQtdLicen: TIntegerField
      FieldName = 'QtdLicen'
    end
    object QrWUnLockGerenControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrWUnLockGerenCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrWUnLockGerenVersaoAtu: TWideStringField
      FieldName = 'VersaoAtu'
      Size = 13
    end
    object QrWUnLockGerenOSVersion: TWideStringField
      FieldName = 'OSVersion'
      Size = 50
    end
    object QrWUnLockGerenDBVersion: TWideStringField
      FieldName = 'DBVersion'
      Size = 50
    end
    object QrWUnLockGerenBeta: TSmallintField
      FieldName = 'Beta'
      MaxValue = 1
    end
    object QrWUnLockGerenCliAtivo: TWideStringField
      FieldName = 'CliAtivo'
      Size = 8
    end
    object QrWUnLockGerenBetaCliApp: TSmallintField
      FieldName = 'BetaCliApp'
      MaxValue = 1
    end
    object QrWUnLockGerenIPServidor: TWideStringField
      FieldName = 'IPServidor'
      Size = 40
    end
    object QrWUnLockGerenOSArquitetu: TWideStringField
      FieldName = 'OSArquitetu'
      Size = 50
    end
    object QrWUnLockGerenFimData_Txt: TWideStringField
      FieldName = 'FimData_Txt'
      Size = 10
    end
  end
  object DsWUnLockGeren: TDataSource
    DataSet = QrWUnLockGeren
    Left = 442
    Top = 282
  end
  object QrClientes: TMySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT cli.Codigo,'
      'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) Nome'
      'FROM clientes cli'
      'LEFT JOIN entidades ent ON ent.Codigo = cli.Cliente'
      'WHERE Cliente1 = "V"'
      'ORDER BY Nome')
    Left = 358
    Top = 282
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
    end
    object QrClientesNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 386
    Top = 282
  end
  object QrAplic: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM aplicativos'
      'WHERE Ativo = 1'
      'AND Codigo=:P0')
    Left = 301
    Top = 282
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAplicCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'aplicativos.Codigo'
    end
    object QrAplicNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'aplicativos.Nome'
      Size = 50
    end
  end
  object DsAplic: TDataSource
    DataSet = QrAplic
    Left = 329
    Top = 282
  end
  object QrLoc: TMySQLQuery
    Database = Dmod.MyDB
    Left = 272
    Top = 282
  end
  object PMMenu: TPopupMenu
    OnPopup = PMMenuPopup
    Left = 568
    Top = 352
    object Vencimentodalicenadoclienteselecionado2: TMenuItem
      Caption = 
        '&Atualizar vencto. da licen'#231'a do cliente atual para o aplicativo' +
        ' sel.'
      OnClick = Vencimentodalicenadoclienteselecionado2Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object AtivarDesativardispositivoselecionado1: TMenuItem
      Caption = 'Ativar / Desativar &dispositivo atual'
      OnClick = AtivarDesativardispositivoselecionado1Click
    end
    object Excluirdispositivoatual1: TMenuItem
      Caption = '&Excluir dispositivo atual'
      OnClick = Excluirdispositivoatual1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Marcardispositivocomo1: TMenuItem
      Caption = '&Marcar / desmarcar dispositivo(s) selecionado(s) como'
      object Dispositivodesuporte1: TMenuItem
        Caption = '&Suporte'
        OnClick = Dispositivodesuporte1Click
      end
      object DispositivoBETA1: TMenuItem
        Caption = '&BETA'
        OnClick = DispositivoBETA1Click
      end
    end
    object AdicionardispositivosselecionadosalistadeversoBETA1: TMenuItem
      Caption = '&Adicionar dispositivo(s) selecionado(s) a lista de vers'#227'o BETA'
      OnClick = AdicionardispositivosselecionadosalistadeversoBETA1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Estatsticasdelicenasdeaplicativos1: TMenuItem
      Caption = 'E&stat'#237'sticas de licen'#231'as de aplicativos'
      OnClick = Estatsticasdelicenasdeaplicativos1Click
    end
  end
  object CSTabSheetChamou: TdmkCompoStore
    Left = 636
    Top = 12
  end
end
