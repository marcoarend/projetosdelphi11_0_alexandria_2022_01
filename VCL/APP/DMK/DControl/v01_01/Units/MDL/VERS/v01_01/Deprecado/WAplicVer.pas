unit WAplicVer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, dmkDBGrid, dmkDBLookupComboBox, dmkEdit,
  dmkEditCB, mySQLDbTables, dmkPermissoes, UnDmkEnums;

type
  TFmWaAplicVer = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    PnDados: TPanel;
    PnAplic: TPanel;
    Label10: TLabel;
    EdAplicativo: TdmkEditCB;
    CBAplicativo: TdmkDBLookupComboBox;
    dmkDBGrid1: TdmkDBGrid;
    PnEdita: TPanel;
    Label6: TLabel;
    Label1: TLabel;
    EdAplicVer: TdmkEdit;
    EdAplicApl: TdmkEditCB;
    CBAplicApl: TdmkDBLookupComboBox;
    CkBeta: TCheckBox;
    GroupBox1: TGroupBox;
    Panel6: TPanel;
    BtDesiste: TBitBtn;
    Panel7: TPanel;
    BtConfirma: TBitBtn;
    Label3: TLabel;
    dmkEdit1: TdmkEdit;
    SBArquivo: TSpeedButton;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BitBtn1: TBitBtn;
    QrAplicativos: TmySQLQuery;
    StringField7: TWideStringField;
    QrAplicativosCodigo: TIntegerField;
    DsAplicativos: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    QrAplicApl: TmySQLQuery;
    StringField1: TWideStringField;
    IntegerField2: TIntegerField;
    DsAplicApl: TDataSource;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmWaAplicVer: TFmWaAplicVer;

implementation

uses UnMyObjects, Module;

{$R *.DFM}

procedure TFmWaAplicVer.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWaAplicVer.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmWaAplicVer.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmWaAplicVer.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
