unit UnitVersoes_Jan;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  ComCtrls, dmkGeral, Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db,
  DbCtrls, Variants, mySQLDbTables, UnDmkEnums, UnDmkProcFunc, UnDmkWeb;

type
  TUnitVersoes_Jan = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure MostraFormWVersao(AbrirEmAba: Boolean; Aplicativo: Integer;
              InOwner: TWincontrol; Pager: TWinControl);
    procedure MostraFormWUnLockGeren(Cliente, Aplicativo: Integer;
              AbrirEmAba: Boolean; InOwner: TWincontrol; Pager: TWinControl);
    procedure MostraFormWUnLockStatis(Cliente, Aplicativo: Integer;
              AbrirEmAba: Boolean; InOwner: TWincontrol; Pager: TWinControl);
    procedure MostraFormWHisAlt(AbrirEmAba: Boolean; InOwner: TWincontrol;
              Pager: TWinControl);
    procedure MostraFormWHisAltTip();
    procedure MostraFormWUnLockOpc();
    procedure MostraFormWHisAltOpc();
  end;

var
  UnVersoes_Jan: TUnitVersoes_Jan;

implementation

uses DmkDAC_PF, MyDBCheck, UMySQLModule, UnMyObjects, Module, ModuleGeral,
  VersaoGer, WUnLockGeren, WUnLockStatis, WHisAlt, WHisAltTip, WUnLockOpc,
  WHisAltOpc;

{ TUnitVersoes_Jan }

procedure TUnitVersoes_Jan.MostraFormWHisAlt(AbrirEmAba: Boolean;
  InOwner: TWincontrol; Pager: TWinControl);
begin
  if DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrWebParams, 1) then
  begin
    if AbrirEmAba then
    begin
      if FmWHisAlt = nil then
        MyObjects.FormTDICria(TFmWHisAlt, InOwner, Pager, True, True);
    end else
    begin
      if DBCheck.CriaFm(TFmWHisAlt, FmWHisAlt, afmoNegarComAviso) then
      begin
        FmWHisAlt.ShowModal;
        FmWHisAlt.Destroy;
        //
        FmWHisAlt := nil;
      end;
    end;
  end;
end;

procedure TUnitVersoes_Jan.MostraFormWHisAltOpc();
begin
  if DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrWebParams, 1) then
  begin
    if DBCheck.CriaFm(TFmWHisAltOpc, FmWHisAltOpc, afmoNegarComAviso) then
    begin
      FmWHisAltOpc.ShowModal;
      FmWHisAltOpc.Destroy;
    end;
  end;
end;

procedure TUnitVersoes_Jan.MostraFormWHisAltTip;
begin
  if DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrWebParams, 1) then
  begin
    if DBCheck.CriaFm(TFmWHisAltTip, FmWHisAltTip, afmoNegarComAviso) then
    begin
      FmWHisAltTip.ShowModal;
      FmWHisAltTip.Destroy;
    end;
  end;
end;

procedure TUnitVersoes_Jan.MostraFormWUnLockGeren(Cliente, Aplicativo: Integer;
  AbrirEmAba: Boolean; InOwner: TWincontrol; Pager: TWinControl);
var
  Form: TForm;
begin
  if (DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrWebParams, 1)) and
    (DmkWeb.ConectarUsuarioWEB(False)) then
  begin
    if AbrirEmAba then
    begin
      if FmWUnLockGeren = nil then
      begin
        Form := MyObjects.FormTDICria(TFmWUnLockGeren, InOwner, Pager, True, True);
        if Form <> nil then
        begin
          TFmWUnLockGeren(Form).MostraEdicao(Cliente, Aplicativo);
          TFmWUnLockGeren(Form).ReopenWUnLockGeren(Cliente, Aplicativo, 0);
          TFmWUnLockGeren(Form).FNChange := False;
        end;
      end;
    end else
    begin
      if DBCheck.CriaFm(TFmWUnLockGeren, FmWUnLockGeren, afmoNegarComAviso) then
      begin
        FmWUnLockGeren.MostraEdicao(Cliente, Aplicativo);
        FmWUnLockGeren.ReopenWUnLockGeren(Cliente, Aplicativo, 0);
        FmWUnLockGeren.FNChange := False;
        FmWUnLockGeren.ShowModal;
        FmWUnLockGeren.Destroy;
        //
        FmWUnLockGeren := nil;
      end;
    end;
  end;
end;

procedure TUnitVersoes_Jan.MostraFormWUnLockOpc;
begin
  if DBCheck.CriaFm(TFmWUnLockOpc, FmWUnLockOpc, afmoNegarComAviso) then
  begin
    FmWUnLockOpc.ShowModal;
    FmWUnLockOpc.Destroy;
  end;
end;

procedure TUnitVersoes_Jan.MostraFormWUnLockStatis(Cliente, Aplicativo: Integer;
  AbrirEmAba: Boolean; InOwner: TWincontrol; Pager: TWinControl);
var
  Form: TForm;
begin
  if (DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrWebParams, 1)) and
    (DmkWeb.ConectarUsuarioWEB(False)) then
  begin
    if AbrirEmAba then
    begin
      if FmWUnLockStatis = nil then
      begin
        Form := MyObjects.FormTDICria(TFmWUnLockStatis, InOwner, Pager, True, True);
        if Form <> nil then
        begin
          TFmWUnLockStatis(Form).MostraEdicao(Cliente, Aplicativo);
          TFmWUnLockStatis(Form).ReopenWUnLockStatis(Cliente, Aplicativo);
          TFmWUnLockStatis(Form).FNChange := False;
        end;
      end;
    end else
    begin
      if DBCheck.CriaFm(TFmWUnLockStatis, FmWUnLockStatis, afmoNegarComAviso) then
      begin
        FmWUnLockStatis.MostraEdicao(Cliente, Aplicativo);
        FmWUnLockStatis.ReopenWUnLockStatis(Cliente, Aplicativo);
        FmWUnLockStatis.FNChange := False;
        FmWUnLockStatis.ShowModal;
        FmWUnLockStatis.Destroy;
        //
        FmWUnLockStatis := nil;
      end;
    end;
  end;
end;

procedure TUnitVersoes_Jan.MostraFormWVersao(AbrirEmAba: Boolean;
  Aplicativo: Integer; InOwner: TWincontrol; Pager: TWinControl);
var
  Form: TForm;
begin
  if (DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrWebParams, 1)) and
    (DmkWeb.ConectarUsuarioWEB(False)) then
  begin
    if AbrirEmAba then
    begin
      if FmVersaoGer = nil then
      begin
        Form := MyObjects.FormTDICria(TFmVersaoGer, InOwner, Pager, True, True);
        if Form <> nil then
        begin
          if Aplicativo <> 0 then
          begin
            TFmVersaoGer(Form).EdAplicativo.ValueVariant := Aplicativo;
            TFmVersaoGer(Form).CBAplicativo.KeyValue     := Aplicativo;
          end;
        end;
      end;
    end else
    begin
      if DBCheck.CriaFm(TFmVersaoGer, FmVersaoGer, afmoNegarComAviso) then
      begin
        if Aplicativo <> 0 then
        begin
          FmVersaoGer.EdAplicativo.ValueVariant := Aplicativo;
          FmVersaoGer.CBAplicativo.KeyValue     := Aplicativo;
        end;
        FmVersaoGer.ShowModal;
        FmVersaoGer.Destroy;
        //
        FmVersaoGer := nil;
      end;
    end;
  end;
end;

end.
