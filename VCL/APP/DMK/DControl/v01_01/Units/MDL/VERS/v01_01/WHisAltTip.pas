unit WHisAltTip;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, dmkCheckBox, DmkDAC_PF, UnDmkEnums, UnDmkImg;

type
  TFmWHisAltTip = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrWHisAltTip: TmySQLQuery;
    QrWHisAltTipCodigo: TIntegerField;
    QrWHisAltTipNome: TWideStringField;
    DsWHisAltTip: TDataSource;
    DBCheckBox1: TDBCheckBox;
    CbAtivo: TdmkCheckBox;
    QrWHisAltTipAtivo: TSmallintField;
    QrLoc: TmySQLQuery;
    ImgWEB: TdmkImage;
    Label4: TLabel;
    EdCor: TdmkEdit;
    Label22: TLabel;
    SpeedButton5: TSpeedButton;
    ColorDialog1: TColorDialog;
    EdDBCor: TEdit;
    QrWHisAltTipCor: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrWHisAltTipAfterOpen(DataSet: TDataSet);
    procedure QrWHisAltTipBeforeOpen(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure QrWHisAltTipAfterScroll(DataSet: TDataSet);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmWHisAltTip: TFmWHisAltTip;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, UnDmkWeb;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmWHisAltTip.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmWHisAltTip.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrWHisAltTipCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmWHisAltTip.DefParams;
begin
  VAR_GOTOTABELA := 'whisalttip';
  VAR_GOTOMYSQLTABLE := QrWHisAltTip;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDBn;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM whisalttip');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmWHisAltTip.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmWHisAltTip.QueryPrincipalAfterOpen;
begin
end;

procedure TFmWHisAltTip.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmWHisAltTip.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmWHisAltTip.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmWHisAltTip.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmWHisAltTip.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmWHisAltTip.SpeedButton5Click(Sender: TObject);
begin
  if ColorDialog1.Execute then
    EdCor.Color := ColorDialog1.Color;
end;

procedure TFmWHisAltTip.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWHisAltTip.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrWHisAltTip, [PnDados],
    [PnEdita], EdNome, ImgTipo, 'whisalttip');
  //
  if QrWHisAltTipCor.Value <> '' then
    EdCor.ValueVariant := DmkImg.HexToTColor(QrWHisAltTipCor.Value)
  else
    EdCor.ValueVariant := clWindow;
end;

procedure TFmWHisAltTip.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrWHisAltTipCodigo.Value;
  Close;
end;

procedure TFmWHisAltTip.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Cor, Nome: String;
begin
  Nome := EdNome.ValueVariant;
  Cor  := DmkImg.TColorToHex(EdCor.Color);
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  if ImgTipo.SQLType = stIns then
    Codigo := UMyMod.BuscaNovoCodigo_Int(Dmod.QrUpdN, 'whisalttip', 'Codigo',
      [], [], stIns, 0, siPositivo, nil)
  else
    Codigo := QrWHisAltTipCodigo.Value;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpdN, ImgTipo.SQLType, 'whisalttip', False,
    ['Nome', 'Cor'], ['Codigo'], [Nome, Cor], [Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    //
    GOTOy.BotoesSb(ImgTipo.SQLType);
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmWHisAltTip.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo          := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDBn, 'whisalttip', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);  
end;

procedure TFmWHisAltTip.BtExcluiClick(Sender: TObject);
var
  Codigo: Integer;
begin
  if (QrWHisAltTip.State <> dsInactive) or (QrWHisAltTip.RecordCount > 0) then
  begin
    Codigo := QrWHisAltTipCodigo.Value;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDBn, [
    'SELECT * ',
    'FROM whisalt ',
    'WHERE WHisAltTip=' + Geral.FF0(Codigo),
    '']);
    if QrLoc.RecordCount > 0 then
    begin
      Geral.MB_Aviso('Este Tipo j� foi utilizado no hist�rico de atualiza��es e por isso n�o poder� ser exclu�-do!');
      Exit;
    end;
    UMyMod.ExcluiRegistroInt1('Confirma a exclus�o deste item?',
      'whisalttip', 'Codigo', Codigo, DMod.MyDBn);
    Va(vpLast);
  end;
end;

procedure TFmWHisAltTip.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrWHisAltTip, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'whisalttip');
end;

procedure TFmWHisAltTip.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
  CriaOForm;
end;

procedure TFmWHisAltTip.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrWHisAltTipCodigo.Value, LaRegistro.Caption);
end;

procedure TFmWHisAltTip.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Info('Impress�o indispon�vel para esta janela!');
end;

procedure TFmWHisAltTip.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmWHisAltTip.SbNovoClick(Sender: TObject);
begin
  Geral.MB_Info('Tipo de pesquisa indispon�vel para esta janela!');
end;

procedure TFmWHisAltTip.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmWHisAltTip.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  DmkWeb.CarregaImagemFormWEB(ImgWEB);
end;

procedure TFmWHisAltTip.SbQueryClick(Sender: TObject);
begin
  LocCod(QrWHisAltTipCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'whisalttip', Dmod.MyDBn, CO_VAZIO));
end;

procedure TFmWHisAltTip.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWHisAltTip.QrWHisAltTipAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmWHisAltTip.QrWHisAltTipAfterScroll(DataSet: TDataSet);
var
  Cor: Integer;
begin
  if QrWHisAltTipCor.Value <> '' then
    Cor := DmkImg.HexToTColor(QrWHisAltTipCor.Value)
  else
    Cor := 0;
  //
  if Cor = 0 then
  begin
    DBEdNome.Font.Color := clBlack;
    EdDBCor.Color       := clBlack;
  end else
  begin
    DBEdNome.Font.Color := Cor;
    EdDBCor.Color       := Cor;
  end;
end;

procedure TFmWHisAltTip.QrWHisAltTipBeforeOpen(DataSet: TDataSet);
begin
  QrWHisAltTipCodigo.DisplayFormat := FFormatFloat;
end;

end.

