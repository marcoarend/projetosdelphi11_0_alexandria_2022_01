object FmVersaoGerIts: TFmVersaoGerIts
  Left = 339
  Top = 185
  Caption = 'WEB-VERSA-002 :: Geren. de vers'#245'es - Dispositivos'
  ClientHeight = 571
  ClientWidth = 1241
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 120
  TextHeight = 16
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1241
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 1182
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 1123
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 463
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Geren. de vers'#245'es - Dispositivos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 463
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Geren. de vers'#245'es - Dispositivos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 463
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Geren. de vers'#245'es - Dispositivos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 59
    Width = 1241
    Height = 372
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1241
      Height = 372
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1241
        Height = 372
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
        object dmkDBGrid1: TdmkDBGrid
          Left = 2
          Top = 18
          Width = 1236
          Height = 351
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Cliente_TXT'
              Title.Caption = 'Cliente'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Aplicativo_TXT'
              Title.Caption = 'Aplicativo'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DescriPC'
              Title.Caption = 'Descri'#231#227'o do computador'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NomePC'
              Title.Caption = 'Nome do computador'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Servidor'
              Width = 75
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PCSuporte'
              Title.Caption = 'PC Suporte'
              Width = 75
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Beta'
              Title.Caption = 'PC app Beta'
              Width = 75
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'BetaCliApp'
              Title.Caption = 'App Beta'
              Visible = True
            end>
          Color = clWindow
          DataSource = DsWVersaoIts
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -14
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Cliente_TXT'
              Title.Caption = 'Cliente'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Aplicativo_TXT'
              Title.Caption = 'Aplicativo'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DescriPC'
              Title.Caption = 'Descri'#231#227'o do computador'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NomePC'
              Title.Caption = 'Nome do computador'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Servidor'
              Width = 75
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PCSuporte'
              Title.Caption = 'PC Suporte'
              Width = 75
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Beta'
              Title.Caption = 'PC app Beta'
              Width = 75
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'BetaCliApp'
              Title.Caption = 'App Beta'
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 431
    Width = 1241
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 1236
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 485
    Width = 1241
    Height = 86
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 1061
      Top = 18
      Width = 177
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 18
      Width = 1059
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtMenu: TBitBtn
        Tag = 237
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Menu'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtMenuClick
      end
    end
  end
  object QrWVersaoIts: TMySQLQuery
    Database = Dmod.MyDBn
    Left = 164
    Top = 169
    object QrWVersaoItsDispositivo: TIntegerField
      FieldName = 'Dispositivo'
    end
    object QrWVersaoItsDescriPC: TWideStringField
      FieldName = 'DescriPC'
      Size = 50
    end
    object QrWVersaoItsNomePC: TWideStringField
      FieldName = 'NomePC'
      Size = 255
    end
    object QrWVersaoItsServidor: TSmallintField
      FieldName = 'Servidor'
      MaxValue = 1
    end
    object QrWVersaoItsBeta: TSmallintField
      FieldName = 'Beta'
      MaxValue = 1
    end
    object QrWVersaoItsPCSuporte: TSmallintField
      FieldName = 'PCSuporte'
      MaxValue = 1
    end
    object QrWVersaoItsAplicativo_TXT: TWideStringField
      FieldName = 'Aplicativo_TXT'
      Size = 50
    end
    object QrWVersaoItsCliente_TXT: TWideStringField
      FieldName = 'Cliente_TXT'
      Size = 100
    end
    object QrWVersaoItsCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrWVersaoItsAplicativo: TIntegerField
      FieldName = 'Aplicativo'
    end
    object QrWVersaoItsCodigo: TFloatField
      FieldName = 'Codigo'
    end
    object QrWVersaoItsControle: TFloatField
      FieldName = 'Controle'
    end
    object QrWVersaoItsBetaCliApp: TSmallintField
      FieldName = 'BetaCliApp'
      MaxValue = 1
    end
  end
  object DsWVersaoIts: TDataSource
    DataSet = QrWVersaoIts
    Left = 192
    Top = 169
  end
  object PMMenu: TPopupMenu
    OnPopup = PMMenuPopup
    Left = 40
    Top = 232
    object Adicionardispositivo1: TMenuItem
      Caption = '&Gerenciar aplicativos'
      OnClick = Adicionardispositivo1Click
    end
    object Excluirdispositivo1: TMenuItem
      Caption = '&Excluir dispositivo'
      OnClick = Excluirdispositivo1Click
    end
  end
end
