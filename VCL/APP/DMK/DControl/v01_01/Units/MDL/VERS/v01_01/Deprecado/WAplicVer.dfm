object FmWaAplicVer: TFmWaAplicVer
  Left = 339
  Top = 185
  Caption = 'WEB-APLVE-001 :: Vers'#245'es de aplicativos'
  ClientHeight = 570
  ClientWidth = 624
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 92
    Width = 624
    Height = 248
    Align = alClient
    TabOrder = 3
    object PnAplic: TPanel
      Left = 1
      Top = 1
      Width = 622
      Height = 48
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label10: TLabel
        Left = 5
        Top = 3
        Width = 49
        Height = 13
        Caption = 'Aplicativo:'
      end
      object EdAplicativo: TdmkEditCB
        Left = 5
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBAplicativo
        IgnoraDBLookupComboBox = False
      end
      object CBAplicativo: TdmkDBLookupComboBox
        Left = 61
        Top = 20
        Width = 346
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsAplicativos
        TabOrder = 1
        dmkEditCB = EdAplicativo
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object dmkDBGrid1: TdmkDBGrid
      Left = 1
      Top = 49
      Width = 622
      Height = 198
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'ID'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Versao'
          Title.Caption = 'Vers'#227'o atual'
          Width = 200
          Visible = True
        end
        item
          Expanded = False
          Title.Caption = 'Data da '#250'ltima modifica'#231#227'o'
          Width = 150
          Visible = True
        end
        item
          Expanded = False
          Title.Caption = 'Beta'
          Visible = True
        end>
      Color = clWindow
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 1
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'ID'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Versao'
          Title.Caption = 'Vers'#227'o atual'
          Width = 200
          Visible = True
        end
        item
          Expanded = False
          Title.Caption = 'Data da '#250'ltima modifica'#231#227'o'
          Width = 150
          Visible = True
        end
        item
          Expanded = False
          Title.Caption = 'Beta'
          Visible = True
        end>
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 340
    Width = 624
    Height = 160
    Align = alBottom
    TabOrder = 4
    object Label6: TLabel
      Left = 369
      Top = 7
      Width = 36
      Height = 13
      Caption = 'Vers'#227'o:'
    end
    object Label1: TLabel
      Left = 6
      Top = 7
      Width = 49
      Height = 13
      Caption = 'Aplicativo:'
    end
    object Label3: TLabel
      Left = 6
      Top = 50
      Width = 217
      Height = 13
      Caption = 'Usu'#225'rios com permiss'#227'o para utilizar a vers'#227'o:'
    end
    object SBArquivo: TSpeedButton
      Left = 525
      Top = 66
      Width = 21
      Height = 21
      Hint = 'Inclui item de carteira'
      Caption = '...'
    end
    object EdAplicVer: TdmkEdit
      Left = 369
      Top = 24
      Width = 148
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'ECEP'
      UpdCampo = 'ECEP'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object EdAplicApl: TdmkEditCB
      Left = 6
      Top = 24
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBAplicApl
      IgnoraDBLookupComboBox = False
    end
    object CBAplicApl: TdmkDBLookupComboBox
      Left = 63
      Top = 24
      Width = 300
      Height = 21
      KeyField = 'CodUsu'
      ListField = 'Nome'
      TabOrder = 1
      dmkEditCB = EdAplicApl
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object CkBeta: TCheckBox
      Left = 525
      Top = 24
      Width = 90
      Height = 17
      Caption = 'Vers'#227'o BETA'
      TabOrder = 3
    end
    object GroupBox1: TGroupBox
      Left = 1
      Top = 89
      Width = 622
      Height = 70
      Align = alBottom
      TabOrder = 4
      object Panel6: TPanel
        Left = 476
        Top = 15
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 12
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object Panel7: TPanel
        Left = 2
        Top = 15
        Width = 474
        Height = 53
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 12
          Top = 4
          Width = 120
          Height = 40
          Caption = '&Confirma'
          NumGlyphs = 2
          TabOrder = 0
        end
      end
    end
    object dmkEdit1: TdmkEdit
      Left = 6
      Top = 66
      Width = 511
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'ECEP'
      UpdCampo = 'ECEP'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 624
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 576
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 528
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 271
        Height = 32
        Caption = 'Vers'#245'es de aplicativos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 271
        Height = 32
        Caption = 'Vers'#245'es de aplicativos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 271
        Height = 32
        Caption = 'Vers'#245'es de aplicativos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 48
    Width = 624
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 620
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 500
    Width = 624
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 478
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 476
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtInclui: TBitBtn
        Tag = 10
        Left = 9
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Inclui nova entidade'
        Caption = '&Inclui'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
      end
      object BtAltera: TBitBtn
        Tag = 11
        Left = 135
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Altera entidade atual'
        Caption = '&Altera'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object BitBtn1: TBitBtn
        Tag = 11
        Left = 260
        Top = 4
        Width = 150
        Height = 40
        Cursor = crHandPoint
        Hint = 'Altera entidade atual'
        Caption = '&Lista de usu'#225'rios'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
      end
    end
  end
  object QrAplicativos: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM aplicativos'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 365
    Top = 9
    object StringField7: TWideStringField
      FieldName = 'Nome'
      Origin = 'grasrv1.Nome'
      Size = 50
    end
    object QrAplicativosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsAplicativos: TDataSource
    DataSet = QrAplicativos
    Left = 393
    Top = 9
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtAltera
    Left = 336
    Top = 9
  end
  object QrAplicApl: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM aplicativos'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 421
    Top = 9
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Origin = 'grasrv1.Nome'
      Size = 50
    end
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsAplicApl: TDataSource
    DataSet = QrAplicApl
    Left = 449
    Top = 9
  end
end
