unit WUnLockStatis;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, VCLTee.TeEngine, Variants,
  VCLTee.Series, VCLTee.TeeProcs, VCLTee.Chart, VCLTee.DBChart, dmkCompoStore,
  dmkPermissoes, dmkDBLookupComboBox, dmkEdit, dmkEditCB, mySQLDbTables,
  VclTee.TeeGDIPlus;

type
  TFmWUnLockStatis = class(TForm)
    PnCabeca: TPanel;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    PnPesquisa: TPanel;
    CSTabSheetChamou: TdmkCompoStore;
    dmkPermissoes1: TdmkPermissoes;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    ImgWEB: TdmkImage;
    Label3: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    EdAplicativo: TdmkEditCB;
    CBAplicativo: TdmkDBLookupComboBox;
    Label8: TLabel;
    CBVersao: TComboBox;
    Label1: TLabel;
    BtReabre: TBitBtn;
    PB1: TProgressBar;
    DsClientes: TDataSource;
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesNome: TWideStringField;
    QrAplic: TmySQLQuery;
    QrAplicCodigo: TIntegerField;
    QrAplicNome: TWideStringField;
    DsAplic: TDataSource;
    RGAtivo: TRadioGroup;
    RGSuporte: TRadioGroup;
    RGBeta: TRadioGroup;
    QrLoc: TmySQLQuery;
    GridPanel2: TGridPanel;
    GridPanel4: TGridPanel;
    GraficoDispOS: TChart;
    PieSeries3: TPieSeries;
    GraficoDispArq: TChart;
    PieSeries4: TPieSeries;
    GraficoDispDB: TChart;
    PieSeries5: TPieSeries;
    GridPanel3: TGridPanel;
    GraficoDispAplic: TChart;
    Series1: TPieSeries;
    GraficoDispVersao: TChart;
    PieSeries1: TPieSeries;
    QrAplicativos: TmySQLQuery;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EdAplicativoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdAplicativoChange(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
    procedure GraficoDispAplicClickSeries(Sender: TCustomChart;
      Series: TChartSeries; ValueIndex: Integer; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    { Private declarations }
    function  ObtemParametrosSQL(Clien, Aplic: Integer): String;
    procedure ConfiguraGrafico(Query: TmySQLQuery; Grafico: TChart; Serie: TPieSeries; Texto: TStrings);
    procedure ReabreDispPorApp(Cliente, Aplicativo: Integer);
    procedure ReabreDispPorVersao(Cliente, Aplicativo: Integer; AplicativoTxt: String);
    procedure ReabreDispPorOS(Cliente, Aplicativo: Integer);
    procedure ReabreDispPorArq(Cliente, Aplicativo: Integer);
    procedure ReabreDispPorDB(Cliente, Aplicativo: Integer);
  public
    { Public declarations }
    FNChange: Boolean;
    procedure MostraEdicao(Cliente, Aplicativo: Integer);
    procedure ReopenWUnLockStatis(Cliente, Aplicativo: Integer);
  end;

  var
    FmWUnLockStatis: TFmWUnLockStatis;

implementation

uses UnMyObjects, Principal, MyGlyfs, Module, ModuleGeral, UnDmkWeb,
  UMySQLModule, UnitVersoes, DmkDAC_PF, UnDmkProcFunc;

{$R *.DFM}

procedure TFmWUnLockStatis.BtReabreClick(Sender: TObject);
var
  Cliente, Aplicativo: Integer;
begin
  Cliente    := EdCliente.ValueVariant;
  Aplicativo := EdAplicativo.ValueVariant;
  //
  ReopenWUnLockStatis(Cliente, Aplicativo);
end;

procedure TFmWUnLockStatis.ReopenWUnLockStatis(Cliente, Aplicativo: Integer);
const
  Total = 5;
begin
  Screen.Cursor := crHourGlass;
  PB1.Position  := 0;
  PB1.Max       := Total;
  try
    UnVersoes.Lic_RemoveComputadoresInativos(DmodG.ObtemAgora, Dmod.QrAuxN, Dmod.MyDBn);
    //
    PB1.Position := PB1.Position + 1;
    ReabreDispPorApp(Cliente, Aplicativo);
    //
    PB1.Position := PB1.Position + 1;
    ReabreDispPorOS(Cliente, Aplicativo);
    //
    PB1.Position := PB1.Position + 1;
    ReabreDispPorArq(Cliente, Aplicativo);
    //
    PB1.Position := PB1.Position + 1;
    ReabreDispPorDB(Cliente, Aplicativo);
    //
    PB1.Position := PB1.Position + 1;
    if Aplicativo <> 0 then
      ReabreDispPorVersao(Cliente, Aplicativo, QrAplicNome.Value)
    else
      GraficoDispVersao.Visible := False;
  finally
    PB1.Position  := 0;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmWUnLockStatis.BtSaidaClick(Sender: TObject);
begin
  if TFmWUnLockStatis(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmWUnLockStatis.ConfiguraGrafico(Query: TmySQLQuery; Grafico: TChart;
  Serie: TPieSeries; Texto: TStrings);
begin
  Grafico.Title.Text.Clear;
  Grafico.Title.Text       := Texto;
  Grafico.Title.Font.Size  := 10;
  Grafico.Title.Font.Style := [fsBold];
  //
  Serie.Clear;
  //
  if Query.RecordCount > 0 then
  begin
    Grafico.Visible := True;
    //
    while not Query.EOF do
    begin
      Serie.Add(Query.FieldByName('Total').AsInteger, Query.FieldByName('Nome').AsString);
      //
      Query.Next;
    end;
  end else
    Grafico.Visible := False;
end;

procedure TFmWUnLockStatis.EdAplicativoChange(Sender: TObject);
var
  Aplic: Integer;
begin
  if FNChange = False then
  begin
    Aplic := EdAplicativo.ValueVariant;
    //
    UnVersoes.App_ConfiguraVersao(QrLoc, Dmod.MyDBn, CBVersao, Aplic);
  end;
end;

procedure TFmWUnLockStatis.EdAplicativoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
  begin
    EdCliente.ValueVariant := 0;
    CBCliente.KeyValue     := Null;
    //
    UnVersoes.BD_ReopenAplicativos(QrAplic, Dmod.MyDB, 0);
  end;
end;

procedure TFmWUnLockStatis.EdClienteChange(Sender: TObject);
var
  Cliente: Integer;
begin
  if FNChange = False then
  begin
    Cliente := EdCliente.ValueVariant;
    //
    UnVersoes.BD_ReopenAplicativos(QrAplic, Dmod.MyDB, Cliente);
    //
    EdAplicativo.ValueVariant := 0;
    CBAplicativo.KeyValue     := Null;
  end;
end;

procedure TFmWUnLockStatis.FormActivate(Sender: TObject);
begin
  DmkWeb.CarregaImagemFormWEB(ImgWEB);
  //
  if TFmWUnLockStatis(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end;
  //
  ImgTipo.SQLType := stPsq;
end;

procedure TFmWUnLockStatis.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnVersoes.BD_ReopenClientes(QrClientes, Dmod.MyDB);
  UnVersoes.BD_ReopenAplicativos(QrAplic, Dmod.MyDB, 0);
  //
  GraficoDispOS.Visible     := False;
  GraficoDispArq.Visible    := False;
  GraficoDispDB.Visible     := False;
  GraficoDispAplic.Visible  := False;
  GraficoDispVersao.Visible := False;
  //
  FNChange := True;
end;

procedure TFmWUnLockStatis.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWUnLockStatis.FormShow(Sender: TObject);
begin
{$IfNDef cSkinRank} //Berlin
{$IfNDef cAlphaSkin} //Berlin
  FmMyGlyfs.DefineGlyfs(TForm(Sender));
{$EndIf}
{$EndIf}
{$IfDef cSkinRank} //Berlin
  if FmPrincipal.Sd1.Active then
    FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
{$EndIf}
{$IfDef cAlphaSkin} //Berlin
  if FmPrincipal.sSkinManager1.Active then
    FmMyGlyfs.DefineGlyfsTDI2(FmPrincipal.sSkinManager1, Sender);
{$EndIf}
end;

procedure TFmWUnLockStatis.GraficoDispAplicClickSeries(Sender: TCustomChart;
  Series: TChartSeries; ValueIndex: Integer; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  Aplicativo: Integer;
  AplicativoTxt: String;
begin
  QrAplicativos.RecNo := ValueIndex + 1;
  //
  Aplicativo    := QrAplicativos.FieldByName('Codigo').AsInteger;
  AplicativoTxt := QrAplicativos.FieldByName('Nome').AsString;
  //
  if Aplicativo <> 0 then
  begin
    EdAplicativo.ValueVariant := Aplicativo;
    CBAplicativo.KeyValue     := Aplicativo;
    //
    ReabreDispPorVersao(EdCliente.ValueVariant, Aplicativo, AplicativoTxt);
  end;
end;

procedure TFmWUnLockStatis.MostraEdicao(Cliente, Aplicativo: Integer);
begin
  if Cliente <> 0 then
  begin
    EdCliente.ValueVariant := Cliente;
    CBCliente.KeyValue     := Cliente;
    EdCliente.SetFocus;
  end else
  begin
    EdCliente.ValueVariant := 0;
    CBCliente.KeyValue     := Null;
  end;
  if Aplicativo <> 0 then
  begin
    EdAplicativo.ValueVariant := Aplicativo;
    CBAplicativo.KeyValue     := Aplicativo;
    EdAplicativo.SetFocus;
  end else
  begin
    EdAplicativo.ValueVariant := 0;
    CBAplicativo.KeyValue     := Null;
  end;
  CBVersao.Text       := '';
  RGAtivo.ItemIndex   := 0;
  RGSuporte.ItemIndex := 1;
  RGBeta.ItemIndex    := 2;
end;

function TFmWUnLockStatis.ObtemParametrosSQL(Clien, Aplic: Integer): String;
var
  Cliente, Aplicativo, Ativo, Suporte, Beta: Integer;
  Versao, SQLCli, SQLApl, SQLVersao, SQLAtivo, SQLSuporte, SQLBETA: String;
begin
  Versao  := CBVersao.Text;
  Ativo   := RGAtivo.ItemIndex;
  Suporte := RGSuporte.ItemIndex;
  Beta    := RGBeta.ItemIndex;
  //
  if Clien = 0 then
    Cliente := EdCliente.ValueVariant
  else
    Cliente := Clien;
  //
  if Aplic = 0 then
    Aplicativo := EdAplicativo.ValueVariant
  else
    Aplicativo := Aplic;
  //
  if Cliente <> 0 then
    SQLCli := ' AND cap.Codigo=' + Geral.FF0(Cliente)
  else
    SQLCli := ' ';
  if Aplicativo <> 0 then
    SQLApl := ' AND cap.Aplicativo=' + Geral.FF0(Aplicativo)
  else
    SQLApl := ' ';
  if Versao <> '' then
    SQLVersao := ' AND cal.VersaoAtu LIKE "%' + Versao + '%"'
  else
    SQLVersao := ' ';
  case Ativo of
      0: SQLAtivo := ' AND cal.Ativo = 1';
      1: SQLAtivo := ' AND cal.Ativo = 0';
    else SQLAtivo := ' ';
  end;
  case Suporte of
      0: SQLSuporte := ' AND cal.PCSuporte = 1';
      1: SQLSuporte := ' AND cal.PCSuporte = 0';
    else SQLSuporte := ' ';
  end;
  case Beta of
      0: SQLBETA := ' AND (cal.Beta = 1 OR cap.Beta = 1)';
      1: SQLBETA := ' AND (cal.Beta = 0 OR cap.Beta = 0)';
    else SQLBETA := ' ';
  end;
  Result := 'WHERE cal.Controle > 0 AND apl.Categoria IN (0, 1)';
  Result := Result + SQLCli + SQLApl + SQLVersao + SQLAtivo + SQLSuporte + SQLBETA;
end;

procedure TFmWUnLockStatis.ReabreDispPorApp(Cliente, Aplicativo: Integer);
var
  Txt: TStringList;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrAplicativos, Dmod.MyDBn, [
    'SELECT apl.Codigo, apl.Nome, COUNT(cap.Aplicativo) Total ',
    'FROM cliaplicli cal ',
    'LEFT JOIN cliaplic cap ON cap.Controle = cal.Controle ',
    'LEFT JOIN aplicativos apl ON apl.Codigo = cap.Aplicativo ',
    ObtemParametrosSQL(Cliente, Aplicativo),
    'GROUP BY cap.Aplicativo ',
    'ORDER BY Total DESC ',
     '']);
  Txt := TStringList.Create;
  try
    Txt.Add('Dipositivos por aplicativo');
    //
    ConfiguraGrafico(QrAplicativos, GraficoDispAplic, Series1, Txt);
  finally
    Txt.Free;
  end;
end;

procedure TFmWUnLockStatis.ReabreDispPorArq(Cliente, Aplicativo: Integer);
var
  Txt: TStringList;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDBn, [
    'SELECT IF(cal.OSArquitetu <> "", cal.OSArquitetu, "N�o informado") Nome, ',
    'COUNT(IF(cal.OSArquitetu <> "", cal.OSArquitetu, "N�o informado")) Total ',
    'FROM cliaplicli cal ',
    'LEFT JOIN cliaplic cap ON cap.Controle = cal.Controle ',
    'LEFT JOIN aplicativos apl ON apl.Codigo = cap.Aplicativo ',
    ObtemParametrosSQL(Cliente, Aplicativo),
    'GROUP BY IF(cal.OSArquitetu <> "", cal.OSArquitetu, "N�o informado") ',
    'ORDER BY Total DESC ',
     '']);
  Txt := TStringList.Create;
  try
    Txt.Add('Dispositivos por arquitetura');
    //
    ConfiguraGrafico(QrLoc, GraficoDispArq, PieSeries4, Txt);
  finally
    Txt.Free;
  end;
end;

procedure TFmWUnLockStatis.ReabreDispPorDB(Cliente, Aplicativo: Integer);
var
  Txt: TStringList;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDBn, [
    'SELECT IF(cal.DBVersion <> "", cal.DBVersion, "N�o informado") Nome, ',
    'COUNT(IF(cal.DBVersion <> "", cal.DBVersion, "N�o informado")) Total ',
    'FROM cliaplicli cal ',
    'LEFT JOIN cliaplic cap ON cap.Controle = cal.Controle ',
    'LEFT JOIN aplicativos apl ON apl.Codigo = cap.Aplicativo ',
    ObtemParametrosSQL(Cliente, Aplicativo),
    'GROUP BY IF(cal.DBVersion <> "", cal.DBVersion, "N�o informado") ',
    'ORDER BY Total DESC ',
     '']);
  Txt := TStringList.Create;
  try
    Txt.Add('Disp. por banco de dados');
    //
    ConfiguraGrafico(QrLoc, GraficoDispDB, PieSeries5, Txt);
  finally
    Txt.Free;
  end;
end;

procedure TFmWUnLockStatis.ReabreDispPorOS(Cliente, Aplicativo: Integer);
var
  Txt: TStringList;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDBn, [
    'SELECT IF(cal.OSVersion <> "", cal.OSVersion, "N�o informado") Nome, ',
    'COUNT(IF(cal.OSVersion <> "", cal.OSVersion, "N�o informado")) Total ',
    'FROM cliaplicli cal ',
    'LEFT JOIN cliaplic cap ON cap.Controle = cal.Controle ',
    'LEFT JOIN aplicativos apl ON apl.Codigo = cap.Aplicativo ',
    ObtemParametrosSQL(Cliente, Aplicativo),
    'GROUP BY IF(cal.OSVersion <> "", cal.OSVersion, "N�o informado") ',
    'ORDER BY Total DESC ',
     '']);
  Txt := TStringList.Create;
  try
    Txt.Add('Disp. por sistema operacional');
    //
    ConfiguraGrafico(QrLoc, GraficoDispOS, PieSeries3, Txt);
  finally
    Txt.Free;
  end;
end;

procedure TFmWUnLockStatis.ReabreDispPorVersao(Cliente, Aplicativo: Integer;
  AplicativoTxt: String);
var
  Txt: TStringList;
  AppTxt: String;
begin
  if MyObjects.FIC(Aplicativo = 0, nil, 'Aplicativo n�o definido!') then Exit;
  //
  AppTxt := AplicativoTxt;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDBn, [
    'SELECT cal.VersaoAtu Nome, COUNT(cal.VersaoAtu) Total ',
    'FROM cliaplicli cal ',
    'LEFT JOIN cliaplic cap ON cap.Controle = cal.Controle ',
    'LEFT JOIN aplicativos apl ON apl.Codigo = cap.Aplicativo ',
    ObtemParametrosSQL(Cliente, Aplicativo),
    'GROUP BY cal.VersaoAtu ',
    'ORDER BY Total DESC ',
     '']);
  Txt := TStringList.Create;
  try
    Txt.Add(AppTxt);
    Txt.Add('Dipositivos por vers�o');
    //
    ConfiguraGrafico(QrLoc, GraficoDispVersao, PieSeries1, Txt);
  finally
    Txt.Free;
  end;
end;

end.
