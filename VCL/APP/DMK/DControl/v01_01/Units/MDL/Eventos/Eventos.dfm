object FmEventos: TFmEventos
  Left = 0
  Top = 0
  Caption = 'EVE-CONVI-001 :: Gerenciamento de convites'
  ClientHeight = 567
  ClientWidth = 820
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  WindowState = wsMaximized
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 473
    Width = 820
    Height = 94
    Align = alBottom
    TabOrder = 0
    object PnQrCodeOpc: TPanel
      Left = 328
      Top = 1
      Width = 262
      Height = 92
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 262
        Height = 92
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        Caption = 'QrCode'
        TabOrder = 0
        object SpeedButton5: TSpeedButton
          Left = 67
          Top = 51
          Width = 27
          Height = 27
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '...'
          OnClick = SpeedButton5Click
        end
        object Label22: TLabel
          Left = 7
          Top = 29
          Width = 21
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Cor:'
        end
        object ShCor: TShape
          Left = 7
          Top = 50
          Width = 52
          Height = 27
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Brush.Color = clBlack
        end
        object CkPreview: TCheckBox
          Left = 7
          Top = 86
          Width = 241
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Gerar preview do QrCode'
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          Font.Quality = fqAntialiased
          ParentColor = False
          ParentFont = False
          TabOrder = 0
          OnClick = CkPreviewClick
        end
      end
    end
    object Pn1: TPanel
      Left = 1
      Top = 1
      Width = 327
      Height = 92
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 1
      object Label4: TLabel
        Left = 21
        Top = 81
        Width = 114
        Height = 18
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Altura (pixels):'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 21
        Top = 60
        Width = 127
        Height = 18
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Largura (pixels):'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 21
        Top = 39
        Width = 41
        Height = 18
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'DPIs:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label1: TLabel
        Left = 21
        Top = 18
        Width = 63
        Height = 18
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Arquivo:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LaArquivo: TLabel
        Left = 157
        Top = 18
        Width = 48
        Height = 18
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Arquivo'
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaDPIs: TLabel
        Left = 157
        Top = 39
        Width = 22
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'DPIs'
      end
      object LaLargura: TLabel
        Left = 157
        Top = 60
        Width = 75
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Largura (pixels)'
      end
      object LaAltura: TLabel
        Left = 157
        Top = 81
        Width = 67
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Altura (pixels)'
      end
    end
    object BtGerar: TBitBtn
      Tag = 24
      Left = 599
      Top = 12
      Width = 157
      Height = 57
      Cursor = crHandPoint
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Gerar Convites'
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = BtGerarClick
    end
    object BtImprime: TBitBtn
      Tag = 24
      Left = 766
      Top = 12
      Width = 157
      Height = 57
      Cursor = crHandPoint
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Imprimir'
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = BtImprimeClick
    end
    object PB1: TProgressBar
      Left = 599
      Top = 77
      Width = 157
      Height = 22
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabOrder = 4
    end
  end
  object ScrollBox1: TScrollBox
    Left = 0
    Top = 0
    Width = 820
    Height = 473
    Align = alClient
    TabOrder = 1
    object PnCont: TPanel
      Left = 0
      Top = 0
      Width = 995
      Height = 556
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      BevelOuter = bvNone
      TabOrder = 0
      object ImgImagem: TImage
        Left = 0
        Top = 0
        Width = 995
        Height = 556
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
      end
      object PnQrCode: TPanel
        Left = 143
        Top = 156
        Width = 141
        Height = 167
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        ParentBackground = False
        TabOrder = 0
        OnDblClick = PnQrCodeDblClick
        OnMouseDown = PnQrCodeMouseDown
        OnMouseMove = PnQrCodeMouseMove
        OnMouseUp = PnQrCodeMouseUp
      end
    end
  end
  object MainMenu1: TMainMenu
    Left = 288
    Top = 88
    object Arquivo1: TMenuItem
      Caption = '&Arquivo'
      object Abrir1: TMenuItem
        Caption = '&Abrir'
        OnClick = Abrir1Click
      end
    end
    object Inserir1: TMenuItem
      Caption = '&Inserir'
      object Painelparanumerao1: TMenuItem
        Caption = '&Painel para numera'#231#227'o'
        RadioItem = True
        OnClick = Painelparanumerao1Click
      end
    end
  end
  object OpenPictureDialog1: TOpenPictureDialog
    Filter = 'BMP (*.bmp)|*.bmp'
    Left = 481
    Top = 201
  end
  object ColorDialog1: TColorDialog
    Left = 204
    Top = 64
  end
  object QrWEventoIt: TMySQLQuery
    Database = Dmod.MyDBn
    OnCalcFields = QrWEventoItCalcFields
    Left = 424
    Top = 87
    object QrWEventoItCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrWEventoItControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrWEventoItSecurityStr: TWideStringField
      FieldName = 'SecurityStr'
      Size = 3
    end
    object QrWEventoItArquivo_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Arquivo_TXT'
      Size = 255
      Calculated = True
    end
  end
  object frxEVE_CONVI_001: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 42125.994829294000000000
    ReportOptions.LastChange = 42126.031691041660000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure MasterData1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  Picture1.Picture.LoadFromFile(<VAR_IMG>);                     ' +
        '                         '
      'end;'
      ''
      'begin'
      'end.')
    OnGetValue = frxEVE_CONVI_001GetValue
    Left = 384
    Top = 288
    Datasets = <
      item
        DataSet = frxDsWEventoIt
        DataSetName = 'frxDsWEventoIt'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 297.000000000000000000
      PaperHeight = 420.000000000000000000
      PaperSize = 8
      Columns = 2
      ColumnWidth = 148.500000000000000000
      ColumnPositions.Strings = (
        '0'
        '148,5')
      Frame.Typ = []
      MirrorMode = []
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 377.952755910000000000
        Top = 16.000000000000000000
        Width = 561.260205000000000000
        OnBeforePrint = 'MasterData1OnBeforePrint'
        Columns = 1
        DataSet = frxDsWEventoIt
        DataSetName = 'frxDsWEventoIt'
        RowCount = 0
        object Picture1: TfrxPictureView
          AllowVectorExport = True
          Left = 18.897637800000000000
          Top = 18.897637800000000000
          Width = 453.543307090000000000
          Height = 340.157480310000000000
          Frame.Typ = []
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
      end
    end
  end
  object frxDsWEventoIt: TfrxDBDataset
    UserName = 'frxDsWEventoIt'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'SecurityStr=SecurityStr'
      'Arquivo=Arquivo')
    DataSet = QrWEventoIt
    BCDToCurrency = False
    DataSetOptions = []
    Left = 453
    Top = 86
  end
end
