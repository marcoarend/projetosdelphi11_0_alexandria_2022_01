unit Eventos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.Menus, Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.ExtDlgs, frxClass, frxDBSet,
  Data.DB, mySQLDbTables, Vcl.ComCtrls, Vcl.Buttons, (*QRCode,*), frxClass;

type
  TCanSize = (csLeft, csUp, csRight, csDown);
  TCanSizes = set of TCanSize;
  TMoveControle = class(TControl);
  TFmEventos = class(TForm)
    MainMenu1: TMainMenu;
    Arquivo1: TMenuItem;
    Abrir1: TMenuItem;
    PnDados: TPanel;
    ScrollBox1: TScrollBox;
    OpenPictureDialog1: TOpenPictureDialog;
    Inserir1: TMenuItem;
    Painelparanumerao1: TMenuItem;
    PnQrCodeOpc: TPanel;
    Pn1: TPanel;
    Label4: TLabel;
    Label3: TLabel;
    Label2: TLabel;
    Label1: TLabel;
    LaArquivo: TLabel;
    LaDPIs: TLabel;
    LaLargura: TLabel;
    LaAltura: TLabel;
    GroupBox1: TGroupBox;
    CkPreview: TCheckBox;
    SpeedButton5: TSpeedButton;
    Label22: TLabel;
    ColorDialog1: TColorDialog;
    ShCor: TShape;
    QrWEventoIt: TmySQLQuery;
    BtGerar: TBitBtn;
    PnCont: TPanel;
    PnQrCode: TPanel;
    ImgImagem: TImage;
    QrWEventoItCodigo: TIntegerField;
    QrWEventoItControle: TIntegerField;
    QrWEventoItSecurityStr: TWideStringField;
    frxEVE_CONVI_001: TfrxReport;
    frxDsWEventoIt: TfrxDBDataset;
    BtImprime: TBitBtn;
    PB1: TProgressBar;
    QrWEventoItArquivo_TXT: TWideStringField;
    procedure Abrir1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Painelparanumerao1Click(Sender: TObject);
    procedure PnQrCodeMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure PnQrCodeMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure PnQrCodeMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormDestroy(Sender: TObject);
    procedure CkPreviewClick(Sender: TObject);
    procedure PnQrCodeDblClick(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure BtGerarClick(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure frxEVE_CONVI_001GetValue(const VarName: string;
      var Value: Variant);
    procedure QrWEventoItCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    FQrCode: TQRCode;
    FLaQrCode: TLabel;
    FArquivo: String;
    FZoom, FImageW, FImageH, FVarH, FVarW: Integer;
    FCanSizes: TCanSizes;
    procedure CarregaImagem();
    procedure ConfiguraQrCode(Code: String);
    procedure ReopenWEventoIt(Codigo: Integer);
    function  GetBMPFileDPI(FileName: String): LongInt;
  public
    { Public declarations }
  end;

var
  FmEventos: TFmEventos;
  mouse_x: Integer;     //Dist�ncia do Cursor do Mouse at� o in�cio do TImage em X
  mouse_y: Integer;     //Dist�ncia do Cursor do Mouse at� o in�cio do TImage em Y
  IS_DRAG: Boolean;     //Verificar se o usu�rio est� "desenhando"
  CURSOR_ATUAL: Integer; //Tipo de Cursor: crSizeNWSE, crSizeNS, etc...

implementation

{$R *.dfm}

uses Module, UnMyObjects, MyDBCheck, UnInternalConsts, dmkGeral, DmkDAC_PF,
  UMySQLModule, UnDmkEnums, UnGrl_Vars;

//Obtem a quantidade de DPIs da imagem a ser carregada
function TFmEventos.GetBMPFileDPI(FileName: String): LongInt;
var
  Stream: TFileStream;
  Data: Word;
  A: Double;
begin
  Result := 0;
  Stream := TFileStream.Create(FileName, fmOpenRead or fmShareDenyWrite);
  try
    Stream.Position := 38;
    //
    if Stream.Read(Data,2) = 2 then
    begin
      A      := Data;
      Result := Round(A / 39.370079);
    end;
  finally
    Stream.Free;
  end;
end;

procedure TFmEventos.Abrir1Click(Sender: TObject);
const
  Aviso  = '...';
  Titulo = 'Sele��o de evento';
  Prompt = 'Informe a vers�o: [F7 para pesquisar]';
  Campo  = 'Descricao';
var
  Evento: Variant;
  SQLCompl: String;
begin
  if VAR_WEB_CLIUSER = 3 then
    SQLCompl := 'WHERE wei.Respons=' + Geral.FF0(VAR_WEB_CLIUSER)
  else
    SQLCompl := '';
  //
  Evento := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
    'SELECT wev.Codigo, wev.Nome ' + Campo,
    'FROM weventos wev ',
    SQLCompl,
    'ORDER BY wev.DataHora DESC ',
    ''], Dmod.MyDBn, True);
  //
  if Evento <> Null then
  begin
     ReopenWEventoIt(Evento);
     //
     if QrWEventoIt.RecordCount > 0 then
     begin
       CarregaImagem();
     end else
     begin
       Geral.MB_Aviso('N�o foram localizados convites para o evento selecionado!');
       Close;
     end;
  end else
    Close;
end;

procedure TFmEventos.BtGerarClick(Sender: TObject);
var
  ImgPrin, Img: TBitmap;
  Controle, Codigo: Integer;
  DirTemp, CodConvite, FileStr: String;
begin
  if (QrWEventoIt.State <> dsInactive) and (QrWEventoIt.RecordCount > 0) then
  begin
    DirTemp := CO_DIR_RAIZ_DMK + '\Eventos';
    //
    if not DirectoryExists(DirTemp) then
      ForceDirectories(DirTemp);
    //
    QrWEventoIt.First;
    //
    PB1.Visible  := True;
    PB1.Max      := QrWEventoIt.RecordCount;
    PB1.Position := 0;
    //
    while not QrWEventoIt.Eof do
    begin
      if Painelparanumerao1.Checked then
      begin
        ImgPrin := TBitmap.Create;
        Img     := TBitmap.Create;
        //
        try
          Codigo     := QrWEventoIt.FieldByName('Codigo').AsInteger;
          Controle   := QrWEventoIt.FieldByName('Controle').AsInteger;
          CodConvite := QrWEventoIt.FieldByName('SecurityStr').AsString +
                        Geral.FF0(Codigo) + Geral.FF0(Controle);
          FileStr    := DirTemp + '\' + CodConvite + '.bmp';
          //
          ConfiguraQrCode(CodConvite);
          //
          ImgPrin.Height := ImgImagem.Height;
          ImgPrin.Width  := ImgImagem.Width;
          ImgPrin.LoadFromFile(FArquivo);
          //
          Img.Height := FQrCode.Height;
          Img.Width  := FQrCode.Width;
          FQrCode.paintBarcode(Img.Canvas);

          ImgPrin.Canvas.Font.Name  := FLaQrCode.Font.Name;
          ImgPrin.Canvas.Font.Color := FLaQrCode.Font.Color;
          ImgPrin.Canvas.Font.Size  := FLaQrCode.Font.Size;
          ImgPrin.Canvas.TextOut(FLaQrCode.Left, FLaQrCode.Top, FLaQrCode.Caption);

          ImgPrin.Canvas.Draw(FQrCode.Left, FQrCode.Top, Img);
          ImgPrin.SaveToFile(FileStr);
        finally
          FreeAndNil(FQrCode);
          FreeAndNil(FLaQrCode);
          Img.Free;
        end;
      end;
      PB1.Position := PB1.Position + 1;
      PB1.Update;
      Application.ProcessMessages;
      //
      QrWEventoIt.Next;
    end;
    ReopenWEventoIt(Codigo);
    PB1.Visible := False;
  end;
end;

procedure TFmEventos.BtImprimeClick(Sender: TObject);
begin
  MyObjects.frxDefineDataSets(frxEVE_CONVI_001, [
    frxDsWEventoIt
    ]);
  //
  MyObjects.frxMostra(frxEVE_CONVI_001, 'Impress�o de Convites')
end;

procedure TFmEventos.CarregaImagem;
var
  DPI: LongInt;
begin
  if OpenPictureDialog1.Execute then
  begin
    ImgImagem.Stretch      := False;
    ImgImagem.Proportional := False;
    FArquivo               := OpenPictureDialog1.FileName;
    //
    Screen.Cursor := crHourGlass;
    try
      ImgImagem.Picture.LoadFromFile(FArquivo);
      //
      FImageW := ImgImagem.Picture.Width;
      FImageH := ImgImagem.Picture.Height;
      DPI     := GetBMPFileDPI(FArquivo);
      //
      ImgImagem.Width  := FImageW;
      ImgImagem.Height := FImageH;
      PnCont.Width     := FImageW;
      PnCont.Height    := FImageH;
      //
      LaArquivo.Caption := ExtractFileName(FArquivo);
      LaLargura.Caption := FormatFloat('0', FImageW);
      LaAltura.Caption  := FormatFloat('0', FImageH);
      LaDPIs.Caption    := FormatFloat('0', DPI);
    finally
      Screen.Cursor := crDefault;
    end;
  end;
  ImgImagem.Stretch          := True;
  ImgImagem.Proportional     := True;
  Painelparanumerao1.Enabled := True;
  PnDados.Visible            := True;
end;

procedure TFmEventos.CkPreviewClick(Sender: TObject);
begin
  if CkPreview.Checked then
  begin
    ConfiguraQrCode('CSM11');
  end else
  begin
    FreeAndNil(FQrCode);
    FreeAndNil(FLaQrCode);
    //
    PnQrCode.Visible := True;
  end;
end;

procedure TFmEventos.ConfiguraQrCode(Code: String);
const
  CO_DistQrCode = 21;
  CO_Borda = 5;
  CO_Fonte_Tam = 7;
  CO_Fonte_Size = 8;
var
  Tam, Carac, LargText: Integer;
begin
  Tam := Trunc((PnQrCode.Width - (CO_Borda * 2)) / CO_DistQrCode);
  //
  FQrCode              := TQRCode.Create(Self);
  FQrCode.Parent       := PnCont;
  FQrCode.Height       := (Tam * 21) + (CO_Borda * 2);
  FQrCode.Width        := (Tam * 21) + (CO_Borda * 2);
  FQrCode.moduleWidth  := Tam;
  FQrCode.marginPixels := CO_Borda;
  FQrCode.Left         := PnQrCode.Left + Trunc((PnQrCode.Width - FQrCode.Width) / 2);
  FQrCode.code         := Code;
  FQrCode.backColor    := clWhite;
  FQrCode.barColor     := ShCor.Brush.Color;
  FQrCode.Top          := PnQrCode.Top;
  //
  Carac    := Length(Code) + 1;
  LargText := Trunc(FQrCode.Width / Carac);
  //
  FLaQrCode             := TLabel.Create(Self);
  FLaQrCode.Parent      := PnCont;
  FLaQrCode.Width       := FQrCode.Width;
  FLaQrCode.Alignment   := taCenter;
  FLaQrCode.Font.Color  := ShCor.Brush.Color;
  FLaQrCode.Font.Name   := 'Lucida Console';
  FLaQrCode.Font.Size   := Round((LargText * CO_Fonte_Tam) / CO_Fonte_Size);
  FLaQrCode.Left        := FQrCode.Left;
  FLaQrCode.Top         := FQrCode.Top + FQrCode.Height;
  FLaQrCode.Caption     := Code;
  //
  PnQrCode.Visible := False;
end;

procedure TFmEventos.FormCreate(Sender: TObject);
begin
  FZoom                      := 100;
  FImageW                    := 0;
  FImageH                    := 0;
  PnDados.Visible            := False;
  PnQrCode.Visible           := False;
  PnQrCodeOpc.Visible        := False;
  Painelparanumerao1.Enabled := False;
  PnQrCode.Height            := 128;
  PnQrCode.Width             := 108;
  FVarH                      := Round((PnQrCode.Height * 10) / 100);
  FVarW                      := Round((PnQrCode.Width * 10) / 100);
  PB1.Visible                := False;
end;

procedure TFmEventos.FormDestroy(Sender: TObject);
begin
  FQrCode.Free;
  FLaQrCode.Free;
end;

procedure TFmEventos.frxEVE_CONVI_001GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VAR_IMG' then
    Value := QrWEventoItArquivo_TXT.Value
  else
  if VarName = 'VAR_IMG_H' then
    Value := ImgImagem.Height
  else
  if VarName = 'VAR_IMG_W' then
    Value := ImgImagem.Width;
end;

procedure TFmEventos.Painelparanumerao1Click(Sender: TObject);
begin
  Painelparanumerao1.Checked := not Painelparanumerao1.Checked;
  //
  if ImgImagem.Picture <> nil then
  begin
    PnQrCode.Visible    := Painelparanumerao1.Checked;
    PnQrCodeOpc.Visible := Painelparanumerao1.Checked;
  end;
end;

procedure TFmEventos.PnQrCodeDblClick(Sender: TObject);
begin
  ShowMessage('Altura: ' + FormatFloat('0', PnQrCode.Height) + ' Largura: ' + FormatFloat('0', PnQrCode.Width));
end;

procedure TFmEventos.PnQrCodeMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  //Dist�ncia do Cursor do Mouse at� o in�cio do TImage - em X
  mouse_x := Mouse.CursorPos.X - PnQrCode.Left - FmEventos.Left;

  //Dist�ncia do Cursor do Mouse at� o in�cio do TImage - em Y
  mouse_y := Mouse.CursorPos.Y - PnQrCode.Top - FmEventos.Top;

  //O usu�rio est� desenhando
  IS_DRAG := True;
end;

procedure TFmEventos.PnQrCodeMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);

  function ValidaWidth(W: Integer): Boolean;
  begin
    Result := (W > 10) and ((ImgImagem.Width + ImgImagem.Left) >= (PnQrCode.Left + W));
  end;

  function ValidaHeight(H: Integer): Boolean;
  begin
    Result := (H > 10) and ((ImgImagem.Height + ImgImagem.Top) >= (PnQrCode.Top + H));
  end;

  function ValidaLeft(L: Integer): Boolean;
  begin
    Result := (ImgImagem.Left <= L) and (L <= (ImgImagem.Left + ImgImagem.Width - PnQrCode.Width));
  end;

  function ValidatTop(T: Integer): Boolean;
  begin
    Result := (ImgImagem.Top <= T) and (T <= (ImgImagem.Top + ImgImagem.Height - PnQrCode.Height));
  end;

const
  CO_QrCodeDist = 21;
var
  Rects: array[0..7]of TRect;
  delta, L, T, W: Integer;
  old_imagex: Integer; //vari�vel auxiliar: antiga posi��o x do TImage
  old_imagey: Integer; //vari�vel auxiliar: antiga posi��o y do TImage
  old_width: Integer;
  new_left, new_width, new_top: Integer;
  pt: TPoint;
  Acao: Integer;
  ValidaT, ValidaW, ValidaL: Boolean;
begin
  if IS_DRAG = False then
  //Se n�o estiver desenhando, o programa somente alterar� o
  //cursor do mouse dependendo da regi�o do TImage
  begin
    if Shift = [] then
    begin
      with Sender as TPanel do
      begin
        delta := 6;
        pt := Point(X,Y);

        Rects[0] := Rect(0, 0, delta, delta);

        //Definindo o cursor do mouse para cada "canto" do TImage
        if PtInRect(Rects[0], pt) then begin Cursor := crSizeNWSE; FCanSizes := [csLeft, csUp]; CURSOR_ATUAL := 0 end
        else
        begin
          Cursor := crDefault; PnQrCode.cursor := crDefault; CURSOR_ATUAL := 8;
        end;
      end;
    end;
  end else
  //Caso contr�rio (usu�rio est� desenhando) o programa ir� redimensionar ou
  //mover o TImage
  begin
    if IS_DRAG = True then
    begin
      case CURSOR_ATUAL of
        0: //Redimensionamento para cima e � esquerda
        begin
          old_imagex := PnQrCode.Left;
          old_width  := PnQrCode.Width;
          old_imagey := PnQrCode.top;
          new_left   := old_imagex;
          new_width  := old_width;
          new_top    := old_imagey;

          L := Mouse.CursorPos.X - mouse_x - FmEventos.Left;

          if L > old_imagex then
            Acao := 1 //Aumenta
          else if L < old_imagex then
            Acao := 2 //Diminui
          else
            Acao := 0; //Nada

          if Acao in [1,2] then
          begin
            if Acao = 2 then //Diminiu
              L := old_imagex - CO_QrCodeDist
            else
              L := old_imagex + CO_QrCodeDist;

            if ValidaLeft(L) then
            begin
              ValidaL  := True;
              new_left := L;
            end else
              ValidaL := False;

            if Acao = 2 then //Diminiu
              W := old_width + CO_QrCodeDist
            else
              W := old_width - CO_QrCodeDist;

            if ValidaWidth(W) then
            begin
              new_width := W;
              ValidaW   := True;
            end else
              ValidaW := False;

            //T := Mouse.CursorPos.Y - mouse_y - Form1.Top;

            if Acao = 2 then //Diminui
              T := old_imagey - CO_QrCodeDist
            else
              T := old_imagey + CO_QrCodeDist;

            if ValidatTop(T) then
            begin
              new_top := T;
              ValidaT := True;
            end else
              ValidaT := False;

            if ValidaL and ValidaW and ValidaT then
            begin
              PnQrCode.Width  := new_width;
              PnQrCode.Left   := new_left;
              PnQrCode.Top    := new_top;
              PnQrCode.Height := PnQrCode.Height + (old_imagey - new_top);
            end;
          end;
        end;
        8: //Mover TImage
        begin
          L := Mouse.CursorPos.X - mouse_x - FmEventos.Left;

          if ValidaLeft(L) then
            PnQrCode.left := L;

          T := Mouse.CursorPos.Y - mouse_y - FmEventos.Top;

          if ValidatTop(T) then
            PnQrCode.Top := T;
        end;
      end;
    end;
  end;
end;

procedure TFmEventos.PnQrCodeMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  //O usu�rio n�o est� desenhando mais...
  IS_DRAG := False;
end;

procedure TFmEventos.QrWEventoItCalcFields(DataSet: TDataSet);
var
  Dir, Nome: String;
begin
  Dir  := CO_DIR_RAIZ_DMK + '\Eventos\';
  Nome := QrWEventoIt.FieldByName('SecurityStr').AsString +
            Geral.FF0(QrWEventoItCodigo.Value) +
            Geral.FF0(QrWEventoItControle.Value) + '.bmp';
  //
  QrWEventoItArquivo_TXT.Value := Dir + Nome;
end;

procedure TFmEventos.ReopenWEventoIt(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrWEventoIt, Dmod.MyDBn, [
    'SELECT * ',
    'FROM weventosit ',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    '']);
end;

procedure TFmEventos.SpeedButton5Click(Sender: TObject);
begin
  if ColorDialog1.Execute then
  begin
    ShCor.Brush.Color := ColorDialog1.Color;
    CkPreview.Checked := False;
  end;
end;

end.
