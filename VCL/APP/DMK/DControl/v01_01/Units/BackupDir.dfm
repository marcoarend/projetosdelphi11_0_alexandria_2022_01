object FmBackupDir: TFmBackupDir
  Left = 339
  Top = 185
  Caption = 'FER-BADIR-001 :: Backup de Diret'#243'rios'
  ClientHeight = 606
  ClientWidth = 1088
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 120
  TextHeight = 16
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1088
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 1029
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 970
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 298
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Backup de Diret'#243'rios'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 298
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Backup de Diret'#243'rios'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 298
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Backup de Diret'#243'rios'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 59
    Width = 1088
    Height = 406
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1088
      Height = 406
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1088
        Height = 406
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
        object PnAuto: TPanel
          Left = 2
          Top = 18
          Width = 517
          Height = 386
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alLeft
          TabOrder = 0
          object Panel5: TPanel
            Left = 1
            Top = 130
            Width = 515
            Height = 255
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alBottom
            TabOrder = 0
            ExplicitTop = 129
            object PageControl1: TPageControl
              Left = 1
              Top = 1
              Width = 513
              Height = 253
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              ActivePage = TabSheet1
              Align = alClient
              TabOrder = 0
              ExplicitWidth = 512
              object TabSheet1: TTabSheet
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Geral'
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object Label32: TLabel
                  Left = 2
                  Top = 6
                  Width = 133
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Diret'#243'rio para backup:'
                end
                object Label2: TLabel
                  Left = 2
                  Top = 58
                  Width = 101
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Diret'#243'rio destino:'
                end
                object Label3: TLabel
                  Left = 2
                  Top = 111
                  Width = 280
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Senha: (somente quando zipado e encryptado)'
                end
                object DBEdit1: TDBEdit
                  Left = 2
                  Top = 26
                  Width = 494
                  Height = 21
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  DataField = 'Caminho'
                  DataSource = DsBackupDir
                  TabOrder = 0
                end
                object DBCheckBox1: TDBCheckBox
                  Left = 2
                  Top = 161
                  Width = 148
                  Height = 21
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Incluir subdiret'#243'rios.'
                  DataField = 'SubDir'
                  DataSource = DsBackupDir
                  TabOrder = 3
                  ValueChecked = '1'
                  ValueUnchecked = '0'
                end
                object DBEdit2: TDBEdit
                  Left = 2
                  Top = 78
                  Width = 494
                  Height = 21
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  DataField = 'Destino'
                  DataSource = DsBackupDir
                  TabOrder = 1
                end
                object DBCheckBox2: TDBCheckBox
                  Left = 151
                  Top = 161
                  Width = 148
                  Height = 21
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Zipar o arquivo.'
                  DataField = 'Zipar'
                  DataSource = DsBackupDir
                  TabOrder = 4
                  ValueChecked = '1'
                  ValueUnchecked = '0'
                end
                object DBCheckBox3: TDBCheckBox
                  Left = 2
                  Top = 187
                  Width = 148
                  Height = 21
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Encryptar'
                  DataField = 'Encrypt'
                  DataSource = DsBackupDir
                  TabOrder = 5
                  ValueChecked = '1'
                  ValueUnchecked = '0'
                end
                object DBEdit4: TDBEdit
                  Left = 2
                  Top = 130
                  Width = 494
                  Height = 21
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  DataField = 'SenhaCrypt'
                  DataSource = DsBackupDir
                  TabOrder = 2
                end
                object DBCheckBox4: TDBCheckBox
                  Left = 151
                  Top = 187
                  Width = 148
                  Height = 21
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'WEB'
                  DataField = 'Web'
                  DataSource = DsBackupDir
                  TabOrder = 6
                  ValueChecked = '1'
                  ValueUnchecked = '0'
                end
              end
              object TabSheet2: TTabSheet
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Extens'#245'es a omitir'
                ImageIndex = 1
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object DBMemo1: TDBMemo
                  Left = 0
                  Top = 0
                  Width = 502
                  Height = 218
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alClient
                  DataField = 'ExtOmit'
                  DataSource = DsBackupDir
                  TabOrder = 0
                end
              end
            end
          end
          object DBGrid1: TdmkDBGrid
            Left = 1
            Top = 1
            Width = 515
            Height = 129
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'ID'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ULTIMOINI_TXT'
                Title.Caption = #218'ltimo backup iniciado em'
                Width = 150
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ULTIMOFIM_TXT'
                Title.Caption = #218'ltimo backup finalizado em'
                Width = 150
                Visible = True
              end>
            Color = clWindow
            DataSource = DsBackupDir
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
            TabOrder = 1
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -14
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'ID'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ULTIMOINI_TXT'
                Title.Caption = #218'ltimo backup iniciado em'
                Width = 150
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ULTIMOFIM_TXT'
                Title.Caption = #218'ltimo backup finalizado em'
                Width = 150
                Visible = True
              end>
          end
        end
        object Panel7: TPanel
          Left = 519
          Top = 18
          Width = 567
          Height = 386
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          TabOrder = 1
          object Splitter1: TSplitter
            Left = 1
            Top = 208
            Width = 564
            Height = 4
            Cursor = crVSplit
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
          end
          object Panel8: TPanel
            Left = 1
            Top = 22
            Width = 564
            Height = 186
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object ListBox5: TListBox
              Left = 0
              Top = 0
              Width = 564
              Height = 186
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Style = lbOwnerDrawFixed
              Align = alClient
              ItemHeight = 13
              TabOrder = 0
            end
          end
          object Panel9: TPanel
            Left = 1
            Top = 212
            Width = 565
            Height = 62
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 1
            ExplicitWidth = 564
            ExplicitHeight = 61
            object ListBox4: TListBox
              Left = 0
              Top = 0
              Width = 564
              Height = 62
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Style = lbOwnerDrawFixed
              Align = alClient
              ItemHeight = 13
              TabOrder = 0
            end
          end
          object PBProgress: TProgressBar
            Left = 1
            Top = 1
            Width = 565
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            TabOrder = 2
            ExplicitWidth = 564
          end
          object Panel6: TPanel
            Left = 1
            Top = 274
            Width = 565
            Height = 111
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 3
            ExplicitTop = 273
            ExplicitWidth = 564
            object Memo1: TMemo
              Left = 0
              Top = 0
              Width = 564
              Height = 111
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              TabOrder = 0
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 465
    Width = 1088
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 1084
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 519
    Width = 1088
    Height = 87
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 908
      Top = 18
      Width = 178
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 5
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 18
      Width = 906
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtAgendar: TBitBtn
        Tag = 10015
        Left = 15
        Top = 5
        Width = 147
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Agendar'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtAgendarClick
      end
      object BtExecutar: TBitBtn
        Tag = 14
        Left = 166
        Top = 5
        Width = 148
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Executar'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtExecutarClick
      end
      object GroupBox2: TGroupBox
        Left = 320
        Top = 5
        Width = 302
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Op'#231#245'es'
        TabOrder = 2
        object CkDesligar: TdmkCheckBox
          Left = 10
          Top = 20
          Width = 279
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Desligar o computador ao finalizar o backup.'
          TabOrder = 0
          UpdType = utYes
          ValCheck = #0
          ValUncheck = #0
          OldValor = #0
        end
      end
    end
  end
  object PMBackIts: TPopupMenu
    OnPopup = PMBackItsPopup
    Left = 508
    Top = 10
    object Local1: TMenuItem
      Caption = '&Local'
      object Inclui1: TMenuItem
        Caption = '&Inclui'
        OnClick = Inclui1Click
      end
      object Altera1: TMenuItem
        Caption = '&Altera'
        OnClick = Altera1Click
      end
      object Exclui1: TMenuItem
        Caption = '&Exclui'
        Enabled = False
        OnClick = Exclui1Click
      end
    end
    object WEB1: TMenuItem
      Caption = '&WEB'
      object Inclui2: TMenuItem
        Caption = '&Inclui'
        OnClick = Inclui2Click
      end
      object Altera2: TMenuItem
        Caption = '&Altera'
        OnClick = Altera2Click
      end
      object Exclui2: TMenuItem
        Caption = '&Exclui'
        OnClick = Exclui2Click
      end
    end
  end
  object PMExecutar: TPopupMenu
    Left = 536
    Top = 10
    object Selecionados1: TMenuItem
      Caption = '&Selecionados'
      OnClick = Selecionados1Click
    end
    object Todos1: TMenuItem
      Caption = '&Todos'
      OnClick = Todos1Click
    end
  end
  object QrBackupDir: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrBackupDirAfterScroll
    SQL.Strings = (
      'SELECT *,'
      
        'IF(UltimoIni < 2, '#39#39', DATE_FORMAT(UltimoIni, "%d/%m/%Y %H:%i:%s"' +
        ')) ULTIMOINI_TXT,'
      
        'IF(UltimoFim < 2, '#39#39', DATE_FORMAT(UltimoFim, "%d/%m/%Y %H:%i:%s"' +
        ')) ULTIMOFIM_TXT'
      'FROM backupdir')
    Left = 563
    Top = 10
    object QrBackupDirCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrBackupDirCaminho: TWideStringField
      FieldName = 'Caminho'
      Size = 255
    end
    object QrBackupDirDestino: TWideStringField
      FieldName = 'Destino'
      Size = 255
    end
    object QrBackupDirFTPWebDir: TIntegerField
      FieldName = 'FTPWebDir'
    end
    object QrBackupDirSubDir: TSmallintField
      FieldName = 'SubDir'
    end
    object QrBackupDirZipar: TSmallintField
      FieldName = 'Zipar'
    end
    object QrBackupDirEncrypt: TSmallintField
      FieldName = 'Encrypt'
    end
    object QrBackupDirSenhaCrypt: TWideStringField
      FieldName = 'SenhaCrypt'
      Size = 50
    end
    object QrBackupDirWeb: TSmallintField
      FieldName = 'Web'
    end
    object QrBackupDirExtOmit: TWideMemoField
      FieldName = 'ExtOmit'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrBackupDirLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrBackupDirDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrBackupDirDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrBackupDirUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrBackupDirUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrBackupDirAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrBackupDirAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrBackupDirOmitirExt: TSmallintField
      FieldName = 'OmitirExt'
    end
    object QrBackupDirUltimoIni: TDateTimeField
      FieldName = 'UltimoIni'
    end
    object QrBackupDirUltimoFim: TDateTimeField
      FieldName = 'UltimoFim'
    end
    object QrBackupDirDESTINO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DESTINO_TXT'
      Size = 255
      Calculated = True
    end
    object QrBackupDirULTIMOINI_TXT: TWideStringField
      FieldName = 'ULTIMOINI_TXT'
      Size = 19
    end
    object QrBackupDirULTIMOFIM_TXT: TWideStringField
      FieldName = 'ULTIMOFIM_TXT'
      Size = 19
    end
  end
  object DsBackupDir: TDataSource
    DataSet = QrBackupDir
    Left = 592
    Top = 10
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 1
    OnTimer = Timer1Timer
    Left = 488
    Top = 168
  end
  object CSTabSheetChamou: TdmkCompoStore
    Left = 420
    Top = 11
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 448
    Top = 11
  end
end
