unit AplicModul;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, dmkDBGrid, mySQLDbTables, DmkDAC_PF,
  UnDmkEnums;

type
  TFmAplicModul = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtInclui: TBitBtn;
    LaTitulo1C: TLabel;
    Panel6: TPanel;
    StaticText4: TStaticText;
    DBGModulos: TdmkDBGrid;
    Panel5: TPanel;
    StaticText1: TStaticText;
    dmkDBGrid1: TdmkDBGrid;
    QrAplicativos: TmySQLQuery;
    DsAplicativos: TDataSource;
    QrAplicativosCodigo: TIntegerField;
    QrAplicativosNome: TWideStringField;
    QrAplicModul: TmySQLQuery;
    QrAplicModulCodigo: TIntegerField;
    QrAplicModulControle: TIntegerField;
    QrAplicModulModulo: TWideStringField;
    QrAplicModulNome: TWideStringField;
    DsAplicModul: TDataSource;
    BtExclui: TBitBtn;
    BtModulos: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrAplicativosAfterScroll(DataSet: TDataSet);
    procedure QrAplicativosBeforeClose(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtModulosClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenAplicativos(Aplicativo: Integer);
    procedure ReopenAplicModul;
  public
    { Public declarations }
    FAplicativo: Integer;
  end;

  var
  FmAplicModul: TFmAplicModul;

implementation

uses UnMyObjects, Module, ModuleGeral, MyDBCheck, Principal, UMySQLModule;

{$R *.DFM}

procedure TFmAplicModul.BtExcluiClick(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrAplicativos.State <> dsInactive) and (QrAplicativos.RecordCount > 0);
  Enab2 := (QrAplicModul.State <> dsInactive) and (QrAplicModul.RecordCount > 0);
  //
  if (not Enab) or (not Enab2) then Exit;
  //
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrAplicModul, TDBGrid(DBGModulos),
    'aplicmodul', ['Controle'], ['Controle'], istPergunta, '');
end;

procedure TFmAplicModul.BtIncluiClick(Sender: TObject);
  procedure InsereItemAtual();
  var
    Modulo: String;
    Codigo, Controle: Integer;
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT Modulo ',
    'FROM modulos ',
    'WHERE Codigo=' + Geral.FF0(DModG.QrSelCodsNivel1.Value),
    '']);
    Modulo   := Dmod.QrAux.FieldByName('Modulo').AsString;
    Codigo   := QrAplicativosCodigo.Value;
    Controle := UMyMod.BuscaNovoCodigo_Int(Dmod.QrUpd, 'aplicmodul', 'Controle',
      [], [], stIns, 0, siPositivo, nil);
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'aplicmodul', False,
      ['Codigo', 'Modulo'], ['Controle'], [Codigo, Modulo], [Controle], True);
  end;
var
  Codigo: Integer;
begin
  if (QrAplicativos.State = dsInactive) or (QrAplicativos.RecordCount = 0) then Exit;
  //
  Codigo := QrAplicativosCodigo.Value;
  //
  if DBCheck.EscolheCodigosMultiplos_0('...', Caption,
    'Seleciones os m�dulos desejados:', nil, 'Ativo', 'Nivel1', 'Nome', [
    'DELETE FROM _selcods_; ',
    'INSERT INTO _selcods_ ',
    'SELECT Codigo Nivel1, 0 Nivel2, ',
    '0 Nivel3, 0 Nivel4, 0 Nivel5, CONCAT(Modulo, " - ", Nome) Nome, 1 Ativo ',
    'FROM ' + TMeuDB + '.modulos',
    'WHERE Modulo NOT IN ( ',
    'SELECT Modulo FROM ' + TMeuDB + '.aplicmodul WHERE Codigo=' + Geral.FF0(Codigo),
    ')'],[
    'SELECT * FROM _selcods_; ',
    ''], Dmod.QrUpd) then
  begin
    DModG.QrSelCods.First;
    while not DModG.QrSelCods.Eof do
    begin
      if DModG.QrSelCodsAtivo.Value = 1 then
        InsereItemAtual();
      DModG.QrSelCods.Next;
    end;
    ReopenAplicModul;
  end;
end;

procedure TFmAplicModul.BtModulosClick(Sender: TObject);
begin
  FmPrincipal.MostraModulos;
end;

procedure TFmAplicModul.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmAplicModul.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmAplicModul.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmAplicModul.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmAplicModul.FormShow(Sender: TObject);
begin
  ReopenAplicativos(FAplicativo);
end;

procedure TFmAplicModul.QrAplicativosAfterScroll(DataSet: TDataSet);
begin
  ReopenAplicModul();
end;

procedure TFmAplicModul.QrAplicativosBeforeClose(DataSet: TDataSet);
begin
  QrAplicModul.Close;
end;

procedure TFmAplicModul.ReopenAplicativos(Aplicativo: Integer);
begin
  UMyMod.AbreQuery(QrAplicativos, Dmod.MyDB);
  //
  if Aplicativo <> 0 then
    QrAplicativos.Locate('Codigo', Aplicativo, []);
end;

procedure TFmAplicModul.ReopenAplicModul;
begin
  QrAplicModul.Close;
  QrAplicModul.Params[0].AsInteger := QrAplicativosCodigo.Value;
  UMyMod.AbreQuery(QrAplicModul, Dmod.MyDB);
end;

end.
