unit ClientesObser;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, Mask, dmkDBEdit, dmkEdit, dmkMemo,
  dmkPermissoes, UnDmkEnums;

type
  TFmClientesObser = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    Label12: TLabel;
    Label3: TLabel;
    Label2: TLabel;
    CkContinuar: TCheckBox;
    EdControle: TdmkEdit;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    MeTexto: TdmkMemo;
    dmkPermissoes1: TdmkPermissoes;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FControle: Integer;
  end;

  var
  FmClientesObser: TFmClientesObser;

implementation

uses UnMyObjects, Module, UMySQLModule;

{$R *.DFM}

procedure TFmClientesObser.BtOKClick(Sender: TObject);
var
  Codigo, Controle: Integer;
  Nome, Texto: String;
begin
  Codigo := EdCodigo.ValueVariant;
  Nome   := EdNome.ValueVariant;
  Texto  := MeTexto.Text;
  //
  if MyObjects.FIC(Codigo = 0, nil, 'C�digo n�o definido!') then Exit;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina um descri��o!') then Exit;
  if MyObjects.FIC(Length(Texto) = 0, MeTexto, 'Defina um texto!') then Exit;
  //
  Controle := UMyMod.BuscaEmLivreY_Def('cliobser', 'Controle', ImgTipo.SQLType, FControle);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'cliobser', False,
    ['Nome', 'Texto', 'Codigo'], ['Controle'],
    [Nome, Texto, Codigo], [Controle], True) then
  begin
    FControle := Controle;
    //
    if CkContinuar.Checked then
    begin
      Geral.MB_Aviso('Dados salvos com sucesso!');
      //
      ImgTipo.SQLType     := stIns;
      EdNome.ValueVariant := '';
      MeTexto.Text        := '';
      EdNome.SetFocus;
    end else
      Close;
  end;
end;

procedure TFmClientesObser.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmClientesObser.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  EdNome.SetFocus;
  //
  if ImgTipo.SQLType = stIns then
  begin
    CkContinuar.Visible := True;
    CkContinuar.Checked := False;
  end else
  begin
    CkContinuar.Visible := False;
    CkContinuar.Checked := False;
  end;
end;

procedure TFmClientesObser.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmClientesObser.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
