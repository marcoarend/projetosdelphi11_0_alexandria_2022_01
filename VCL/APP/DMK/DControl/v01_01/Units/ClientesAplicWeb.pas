unit ClientesAplicWeb;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, dmkPermissoes, mySQLDbTables,
  dmkCheckBox, dmkRadioGroup, dmkEditDateTimePicker, dmkDBLookupComboBox,
  dmkEditCB, dmkEdit, Mask, dmkDBEdit, Variants, UnDmkEnums, DmkDAC_PF;

type
  TFmClientesAplicWeb = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    DsAplicativos: TDataSource;
    QrAplicativos: TmySQLQuery;
    QrAplicativosCodigo: TIntegerField;
    QrAplicativosNome: TWideStringField;
    EdControle: TdmkEdit;
    Label3: TLabel;
    Label9: TLabel;
    EdAplicativo: TdmkEditCB;
    CBAplicativo: TdmkDBLookupComboBox;
    LaFTPCfg: TLabel;
    EdFTPCfg: TdmkEditCB;
    CBFTPCfg: TdmkDBLookupComboBox;
    SBFTPCfg: TSpeedButton;
    LaDataBaseCfg: TLabel;
    EdDataBaseCfg: TdmkEditCB;
    CBDataBaseCfg: TdmkDBLookupComboBox;
    SBDataBaseCfg: TSpeedButton;
    TPFimData: TdmkEditDateTimePicker;
    Label1: TLabel;
    CkAtivo: TdmkCheckBox;
    QrFTPConfig: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsFTPConfig: TDataSource;
    QrMySQLConf: TmySQLQuery;
    IntegerField2: TIntegerField;
    StringField2: TWideStringField;
    DsMySQLConf: TDataSource;
    QrLoc: TmySQLQuery;
    ImgWEB: TdmkImage;
    LaQtdLicen: TLabel;
    EdQtdLicen: TdmkEdit;
    LaQtdCliInt: TLabel;
    EdQtdCliInt: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SBFTPCfgClick(Sender: TObject);
    procedure SBDataBaseCfgClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    function  ValidaNomeWeb(Controle, Aplicativo: Integer; NomeWeb: String): Boolean;
    function  ValidaAplicativo(Codigo, Controle, Aplicativo: Integer): Boolean;
    procedure MostraEdicao(SQLType: TSQLType; Query: TMySQLQuery);
    procedure ReopenAplicativos();
  public
    { Public declarations }
    FSQLType: TSQLType;
    FQuery: TMySQLQuery;
    FCodigo, FControle: Integer;
  end;

  var
  FmClientesAplicWeb: TFmClientesAplicWeb;

implementation

uses UnMyObjects, Module, Principal, ModuleGeral, UMySQLModule, UnFTPJan,
  UnDmkWeb, MyListas, UnitVersoes;

{$R *.DFM}

procedure TFmClientesAplicWeb.BtOKClick(Sender: TObject);
var
  Controle, Aplicativo, QtdLicen, QtdCliInt, DataBaseCfg, FTPCfg,
  Ativo: Integer;
  DataFim: TDate;
  AplicativoTxt: String;
begin
  Aplicativo    := EdAplicativo.ValueVariant;
  AplicativoTxt := QrAplicativosNome.Value;
  FTPCfg        := EdFTPCfg.ValueVariant;
  DataBaseCfg   := EdDataBaseCfg.ValueVariant;
  QtdLicen      := EdQtdLicen.ValueVariant;
  QtdCliInt     := EdQtdCliInt.ValueVariant;
  DataFim       := TPFimData.Date;
  Ativo         := Geral.BoolToInt(CkAtivo.Checked);
  //
  Controle := UnVersoes.App_InsUpdCliAplicWeb(FCodigo, FControle, Aplicativo,
              QtdLicen, QtdCliInt, FTPCfg, DataBaseCfg, Ativo,
              DataFim, AplicativoTxt, Dmod.MyDB, Dmod.QrUpd, EdAplicativo);
  //
  if Controle <> 0 then
  begin
    FControle := Controle;
    //
    Close;
  end;
end;

procedure TFmClientesAplicWeb.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := FControle;
  Close;
end;

procedure TFmClientesAplicWeb.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  DmkWeb.CarregaImagemFormWEB(ImgWEB);
end;

procedure TFmClientesAplicWeb.FormCreate(Sender: TObject);
var
  Status: TStrings;
begin
  ImgTipo.SQLType := stLok;
  //
  CBFTPCfg.ListSource      := DsFTPConfig;
  CBDataBaseCfg.ListSource := DsMySQLConf;
end;

procedure TFmClientesAplicWeb.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmClientesAplicWeb.FormShow(Sender: TObject);
begin
  UMyMod.AbreQuery(QrFTPConfig, Dmod.MyDBn);
  UMyMod.AbreQuery(QrMySQLConf, Dmod.MyDB);
  //
  ReopenAplicativos;
  //
  MostraEdicao(FSQLType, FQuery);
end;

procedure TFmClientesAplicWeb.MostraEdicao(SQLType: TSQLType; Query: TMySQLQuery);
begin
  if SQLType = stIns then
  begin
    EdControle.ValueVariant    := 0;
    EdAplicativo.ValueVariant  := 0;
    CBAplicativo.KeyValue      := Null;
    EdFTPCfg.ValueVariant      := 0;
    CBFTPCfg.KeyValue          := Null;
    EdDataBaseCfg.ValueVariant := 0;
    CBDataBaseCfg.KeyValue     := Null;
    TPFimData.Date             := 0;
    CkAtivo.Checked            := True;
  end else
  begin
    EdControle.ValueVariant    := Query.FieldByName('Controle').AsInteger;
    EdAplicativo.ValueVariant  := Query.FieldByName('Aplicativo').AsInteger;
    CBAplicativo.KeyValue      := Query.FieldByName('Aplicativo').AsInteger;
    EdFTPCfg.ValueVariant      := Query.FieldByName('FTPCfg').AsInteger;
    CBFTPCfg.KeyValue          := Query.FieldByName('FTPCfg').AsInteger;
    EdDataBaseCfg.ValueVariant := Query.FieldByName('DataBaseCfg').AsInteger;
    CBDataBaseCfg.KeyValue     := Query.FieldByName('DataBaseCfg').AsInteger;
    TPFimData.Date             := Query.FieldByName('FimData').AsDateTime;
    CkAtivo.Checked            := Geral.IntToBool(Query.FieldByName('Ativo').AsInteger);
  end;
  ImgTipo.SQLType := SQLType;
  //
  EdAplicativo.SetFocus;
end;

procedure TFmClientesAplicWeb.ReopenAplicativos;

  function ObtemListaCliAplic(): String;
  var
    Res, AppStr: String;
    Qry: TmySQLQuery;
  begin
    Res := '';
    Qry := TmySQLQuery.Create(Dmod.MyDB);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT Aplicativo ',
        'FROM cliaplic ',
        'WHERE Codigo=' + Geral.FF0(FCodigo),
        '']);
      if Qry.RecordCount > 0 then
      begin
        while not Qry.Eof do
        begin
          AppStr := Geral.FF0(Qry.FieldByName('Aplicativo').AsInteger);
          //
          if Qry.RecNo = 0 then
            Res := AppStr
          else
            Res := Res + ' ,' + Res;
        end;
      end;
    finally
      Qry.Free;
    end;
    Result := Res;
  end;

var
  SQLComp: String;
begin
  if ImgTipo.SQLType = stIns then
  begin
    SQLComp := ObtemListaCliAplic();
    //
    if SQLComp <> '' then
      SQLComp := 'AND Codigo NOT IN (' + SQLComp + ')';
    //
    UnVersoes.App_ReopenAplicativos(CO_App_TipAmb_Int[1], Dmod.MyDBn, QrAplicativos, SQLComp);
  end else
    UnVersoes.App_ReopenAplicativos(CO_App_TipAmb_Int[1], Dmod.MyDBn, QrAplicativos, '');
end;

procedure TFmClientesAplicWeb.SBFTPCfgClick(Sender: TObject);
var
  FTPCfg: Integer;
begin
  FTPCfg := EdFTPCfg.ValueVariant;
  //
  UFTPJan.MostraFTPConfig(FTPCfg);
  //
  if VAR_CADASTRO > 0 then
    UMyMod.SetaCodigoPesquisado(EdFTPCfg, CBFTPCfg, QrFTPConfig, VAR_CADASTRO);
end;

function TFmClientesAplicWeb.ValidaAplicativo(Codigo, Controle,
  Aplicativo: Integer): Boolean;
begin
  Result := False;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
    'SELECT Controle ',
    'FROM cliaplic ',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    'AND Aplicativo=' + Geral.FF0(Aplicativo),
    '']);
  if QrLoc.RecordCount > 0 then
  begin
    if QrLoc.FieldByName('Controle').AsInteger <> Controle then
      Result := False
    else
      Result := True;
  end else
    Result := True;
end;

function TFmClientesAplicWeb.ValidaNomeWeb(Controle, Aplicativo: Integer;
  NomeWeb: String): Boolean;
begin
  Result := False;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
    'SELECT Controle ',
    'FROM cliaplic ',
    'WHERE NomeWeb = "' + NomeWeb + '" ',
    'AND Aplicativo = ' + Geral.FF0(Aplicativo),
    '']);
  //
  if QrLoc.RecordCount > 0 then
  begin
    if QrLoc.FieldByName('Controle').AsInteger <> Controle then
      Result := False
    else
      Result := True;
  end else
    Result := True;
end;

procedure TFmClientesAplicWeb.SBDataBaseCfgClick(Sender: TObject);
var
  DataBaseCfg: Integer;
begin
  DataBaseCfg := EdDataBaseCfg.ValueVariant;
  //
  FmPrincipal.MostraContasMySQL(DataBaseCfg);
  //
  if VAR_CADASTRO > 0 then
    UMyMod.SetaCodigoPesquisado(EdDataBaseCfg, CBDataBaseCfg, QrMySQLConf, VAR_CADASTRO);
end;

end.
