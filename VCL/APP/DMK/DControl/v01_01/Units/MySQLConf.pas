unit MySQLConf;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt, UnDmkProcFunc,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, Grids, DmkDAC_PF,
  UnDmkEnums, dmkCheckBox;

type
  TFmMySQLConf = class(TForm)
    PnDados: TPanel;
    DsMySQLConf: TDataSource;
    QrMySQLConf: TmySQLQuery;
    PnEdita: TPanel;
    dmkPermissoes1: TdmkPermissoes;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    Label9: TLabel;
    EdNome: TdmkEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrMySQLConfCodigo: TIntegerField;
    QrMySQLConfWeb_Host: TWideStringField;
    QrMySQLConfWeb_User: TWideStringField;
    QrMySQLConfWeb_Pwd: TWideStringField;
    QrMySQLConfWeb_DB: TWideStringField;
    QrMySQLConfLk: TIntegerField;
    QrMySQLConfDataCad: TDateField;
    QrMySQLConfDataAlt: TDateField;
    QrMySQLConfUserCad: TIntegerField;
    QrMySQLConfUserAlt: TIntegerField;
    QrMySQLConfAlterWeb: TSmallintField;
    QrMySQLConfAtivo: TSmallintField;
    QrMySQLConfNome: TWideStringField;
    Label1: TLabel;
    EdCodigo: TdmkEdit;
    Label7: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    EdWeb_Host: TdmkEdit;
    Label32: TLabel;
    Label33: TLabel;
    EdWeb_User: TdmkEdit;
    Label34: TLabel;
    EdWeb_Pwd: TdmkEdit;
    Label35: TLabel;
    EdWeb_DB: TdmkEdit;
    Label4: TLabel;
    EdWeb_Porta: TdmkEdit;
    Label5: TLabel;
    dmkDBEdit2: TdmkDBEdit;
    Label6: TLabel;
    dmkDBEdit3: TdmkDBEdit;
    Label10: TLabel;
    dmkDBEdit4: TdmkDBEdit;
    Label11: TLabel;
    dmkDBEdit5: TdmkDBEdit;
    Label12: TLabel;
    dmkDBEdit6: TdmkDBEdit;
    QrMySQLConfWeb_Porta: TIntegerField;
    BtTestar: TBitBtn;
    QrMySQLConfSENHA: TWideStringField;
    SbExporta: TBitBtn;
    CkAtivo: TdmkCheckBox;
    DBCheckBox1: TDBCheckBox;
    Grade: TStringGrid;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrMySQLConfAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrMySQLConfBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtTestarClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbExportaClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
  private
    FSecureStr: String;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    //procedure IncluiRegistro;
    //procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmMySQLConf: TFmMySQLConf;
const
  FFormatFloat = '00000';

implementation

uses Module, UnMyObjects, UnGrl_Consts, UnitVersoes;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmMySQLConf.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmMySQLConf.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrMySQLConfCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmMySQLConf.DefParams;
begin
  VAR_GOTOTABELA := 'mysqlconf';
  VAR_GOTOMYSQLTABLE := QrMySQLConf;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *, ');
  VAR_SQLx.Add('AES_DECRYPT(Web_Pwd, "'+ CO_RandStrWeb01 +'") SENHA');
  VAR_SQLx.Add('FROM mysqlconf');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmMySQLConf.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      GBCntrl.Visible := True;
      PnDados.Visible := True;
      PnEdita.Visible := False;
    end;
    1:
    begin
      PnEdita.Visible := True;
      PnDados.Visible := False;
      GBCntrl.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.ValueVariant    := FormatFloat(FFormatFloat, Codigo);
        EdNome.ValueVariant      := '';
        EdWeb_Host.ValueVariant  := '';
        EdWeb_DB.ValueVariant    := '';
        EdWeb_Porta.ValueVariant := 0;
        EdWeb_User.ValueVariant  := '';
        EdWeb_Pwd.ValueVariant   := '';
        CkAtivo.Checked          := True;
      end else
      begin
        EdCodigo.ValueVariant    := QrMySQLConfCodigo.Value;
        EdNome.ValueVariant      := QrMySQLConfNome.Value;
        EdWeb_Host.ValueVariant  := QrMySQLConfWeb_Host.Value;
        EdWeb_DB.ValueVariant    := QrMySQLConfWeb_DB.Value;
        EdWeb_Porta.ValueVariant := QrMySQLConfWeb_Porta.Value;
        EdWeb_User.ValueVariant  := QrMySQLConfWeb_User.Value;
        EdWeb_Pwd.ValueVariant   := QrMySQLConfSENHA.Value;
        CkAtivo.Checked          := Geral.IntToBool(QrMySQLConfAtivo.Value);
      end;
      EdNome.SetFocus;
    end;
    else Geral.MensagemBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmMySQLConf.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmMySQLConf.QueryPrincipalAfterOpen;
begin
end;

procedure TFmMySQLConf.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmMySQLConf.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmMySQLConf.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmMySQLConf.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmMySQLConf.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmMySQLConf.BtTestarClick(Sender: TObject);
const
  Avisa = True;
var
  Servidor, DB, User, Pass: String;
  Porta: Integer;
begin
  Servidor := EdWeb_Host.ValueVariant;
  DB       := EdWeb_DB.ValueVariant;
  Porta    := EdWeb_Porta.ValueVariant;
  User     := EdWeb_User.ValueVariant;
  Pass     := EdWeb_Pwd.ValueVariant;
  //
  if MyObjects.FIC(Length(Servidor) = 0, EdWeb_Host, 'Host n�o definido!') then Exit;
  if MyObjects.FIC(Length(User) = 0, EdWeb_User, 'Usu�rio n�o definido!') then Exit;
  if MyObjects.FIC(Length(Pass) = 0, EdWeb_Pwd, 'Senha n�o definida!') then Exit;
  //
  UnDmkDAC_PF.TestarConexaoMySQL(Self, Servidor, User, Pass, DB, Porta, Avisa);
end;

procedure TFmMySQLConf.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrMySQLConfCodigo.Value;
  Close;
end;

procedure TFmMySQLConf.BtAlteraClick(Sender: TObject);
begin
  MostraEdicao(1, stUpd, QrMySQLConfCodigo.Value);
end;

procedure TFmMySQLConf.BtConfirmaClick(Sender: TObject);
var
  Codigo, Porta, Ativo: Integer;
  Descri, Host, DB, User, Pass: String;
begin
  Descri := EdNome.ValueVariant;
  Host   := EdWeb_Host.ValueVariant;
  DB     := EdWeb_DB.ValueVariant;
  Porta  := EdWeb_Porta.ValueVariant;
  User   := EdWeb_User.ValueVariant;
  Pass   := EdWeb_Pwd.ValueVariant;
  Ativo  := Geral.BoolToInt(CkAtivo.Checked);
  //
  if MyObjects.FIC(Length(Descri) = 0, EdNome, 'Descri��o n�o definida!') then Exit;
  if MyObjects.FIC(Length(Host) = 0, EdWeb_Host, 'Host n�o definido!') then Exit;
  if MyObjects.FIC(Length(User) = 0, EdWeb_User, 'Usu�rio n�o definido!') then Exit;
  if MyObjects.FIC(Length(Pass) = 0, EdWeb_Pwd, 'Senha n�o definida!') then Exit;
  //
  Codigo := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'mysqlconf', 'Codigo', [], [],
    ImgTipo.SQLType, EdCodigo.ValueVariant, siPositivo, EdCodigo);
  //
  Dmod.QrUpd.SQL.Clear;
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpd.SQL.Add('INSERT INTO mysqlconf SET ')
  else
    Dmod.QrUpd.SQL.Add('UPDATE mysqlconf SET ');
  Dmod.QrUpd.SQL.Add('Web_Host=:P0, Web_User=:P1, Web_DB=:P2, ');
  Dmod.QrUpd.SQL.Add('Web_Pwd=AES_ENCRYPT(:P3, :P4), Web_Porta=:P5, ');
  Dmod.QrUpd.SQL.Add('Nome=:P6, Ativo=:P7, ');
  //
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpd.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else
    Dmod.QrUpd.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  //
  Dmod.QrUpd.Params[00].AsString  := Host;
  Dmod.QrUpd.Params[01].AsString  := User;
  Dmod.QrUpd.Params[02].AsString  := DB;
  Dmod.QrUpd.Params[03].AsString  := Pass;
  Dmod.QrUpd.Params[04].AsString  := CO_RandStrWeb01;
  Dmod.QrUpd.Params[05].AsInteger := Porta;
  Dmod.QrUpd.Params[06].AsString  := Descri;
  Dmod.QrUpd.Params[07].AsInteger := Ativo;
  //
  Dmod.QrUpd.Params[08].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpd.Params[09].AsInteger := VAR_USUARIO;
  Dmod.QrUpd.Params[10].AsInteger := Codigo;
  //
  Dmod.QrUpd.ExecSQL;
  //
  MostraEdicao(0, stLok, 0);
  LocCod(Codigo, Codigo);
end;

procedure TFmMySQLConf.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'mysqlconf', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'mysqlconf', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'mysqlconf', 'Codigo');
end;

procedure TFmMySQLConf.BtExcluiClick(Sender: TObject);
begin
  Geral.MB_Aviso('Exclus�o n�o implementada!');
end;

procedure TFmMySQLConf.BtIncluiClick(Sender: TObject);
begin
  MostraEdicao(1, stIns, 0);
end;

procedure TFmMySQLConf.FormCreate(Sender: TObject);
begin
  FSecureStr := 'oieih3v834'; //Deve ser igual a do DermaDB
  //
  ImgTipo.SQLType := stLok;
  GBEdita.Align   := alClient;
  GBDados.Align   := alClient;
  //
  Grade.ColCount := 7;
  Grade.RowCount := 2;
  //
  CriaOForm;
end;

procedure TFmMySQLConf.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrMySQLConfCodigo.Value, LaRegistro.Caption);
end;

procedure TFmMySQLConf.SbExportaClick(Sender: TObject);

  procedure CamposToGrade(Item: Integer);
  begin
    Grade.Cells[00,Item] := Geral.FF0(Item);
    Grade.Cells[01,Item] := QrMySQLConfNome.Value;
    Grade.Cells[02,Item] := QrMySQLConfWeb_Host.Value;
    Grade.Cells[03,Item] := QrMySQLConfWeb_DB.Value;
    Grade.Cells[04,Item] := Geral.FF0(QrMySQLConfWeb_Porta.Value);
    Grade.Cells[05,Item] := QrMySQLConfWeb_User.Value;;
    Grade.Cells[06,Item] := QrMySQLConfSENHA.Value;
  end;

  procedure SalvaStringGridToFile_PWD(Grade: TStringGrid; Arq: String);
  var
    F: TextFile;
    x, y, i: Integer;
    Valor, Senha, Nums: String;
  begin
    AssignFile(F, Arq);
    Rewrite(F);
    Writeln(F, Grade.ColCount);
    Writeln(F, Grade.RowCount);
    //
    for x:=0 to Grade.ColCount-1 do
    begin
      for y:=0 to Grade.RowCount-1 do
      begin
        if x >= Grade.ColCount-1 then
        begin
          Senha := dmkPF.Criptografia(Grade.Cells[x,y], FSecureStr);
          Nums := '';
          for i := 1 to Length(Senha) do
            Nums := Nums + FormatFloat('000', ord(Senha[i]));
          Writeln(F, Nums);
        end else
        begin
          Valor := Grade.Cells[x,y];
          //
          Writeln(F, Valor);
        end;
      end;
    end;
    CloseFile(F);
  end;

var
  Arquivo: String;
begin
  if MyObjects.FileOpenDialog(Self, '', QrMySQLConfNome.Value, 'Abrir',
    '*.llm', [], Arquivo) then
  begin
    Grade.Cells[00,00] := 'Item';
    Grade.Cells[01,00] := 'Descri��o';
    Grade.Cells[02,00] := 'Servidor (IP / URL)';
    Grade.Cells[03,00] := 'Base de dados';
    Grade.Cells[04,00] := 'Porta';
    Grade.Cells[05,00] := 'Login';
    Grade.Cells[06,00] := 'Senha';
    //
    MyObjects.LimpaGrade(Grade, 1, 1, True);
    //
    CamposToGrade(1);
    //
    SalvaStringGridToFile_PWD(Grade, Arquivo + '.llm');
    //
    Geral.MensagemBox('Arquivo salvo com sucesso!', 'Aviso',
      MB_OK+MB_ICONWARNING);
  end;
end;

procedure TFmMySQLConf.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
end;

procedure TFmMySQLConf.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmMySQLConf.SbNovoClick(Sender: TObject);
begin
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmMySQLConf.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmMySQLConf.QrMySQLConfAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmMySQLConf.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmMySQLConf.SbQueryClick(Sender: TObject);
begin
  LocCod(QrMySQLConfCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'mysqlconf', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmMySQLConf.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmMySQLConf.QrMySQLConfBeforeOpen(DataSet: TDataSet);
begin
  QrMySQLConfCodigo.DisplayFormat := FFormatFloat;
end;

end.

