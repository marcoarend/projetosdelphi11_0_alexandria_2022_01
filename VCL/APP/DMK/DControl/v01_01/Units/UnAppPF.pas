unit UnAppPF;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db, DbCtrls, mySQLDBTables,
  UnInternalConsts2, ComCtrls, dmkGeral, UnDmkEnums, SHDocVw;

type
  TUnAppPF = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    // O U T R O S
    function  AcaoEspecificaDeApp(Servico: String; CliInt, EntCliInt: Integer;
              Query: TmySQLQuery = nil; Query2: TmySQLQuery = nil): Boolean;
    procedure Html_ConfigarImagem(WebBrowser: TWebBrowser);
    procedure Html_ConfigarUrl(WebBrowser: TWebBrowser);
    procedure Html_ConfigarVideo(WebBrowser: TWebBrowser);
    // A G E N D A
    function  AGEN_CriaTarefa(OriID: Integer; OriNum: Double; Descri: String = '';
              Prioridade: Integer = -1; DataCad: TDate = 0): Integer;
    procedure AGEN_MostraFormAgenTarGer(Form: TForm; AbrirEmAba: Boolean;
              InOwner: TWincontrol; Pager: TWinControl;
              Controle: Integer);
    procedure AGEN_MostraFormOrigem(OriID: Integer; OriNum: Double);
    function  AGEN_ExcluiTarefasItem(DataBase: TComponent; Query: TDataSet;
              Controle: Integer; Msg: String; ExcluiOriID: Boolean = False): Boolean;
    procedure MostraOrigemFat(FatParcRef: Integer);
    procedure MostraOrigemFatGenerico(FatID: Integer; FatNum: Double; FatParcela,
              FatParcRef: Integer);
  end;

var
  AppPF: TUnAppPF;

implementation

uses UnMyObjects, Module, Principal, MyDBCheck, DmkDAC_PF, ModuleGeral,
  UnBloqGerl_Jan, UMySQLModule, UnitHelpJan, UnGrlAgenda, UnitAgendaJan,
  UnitWOrdSerJan, UnGrlHelp;

{ TUnAppPF }

function TUnAppPF.AGEN_CriaTarefa(OriID: Integer; OriNum: Double;
  Descri: String = ''; Prioridade: Integer = -1; DataCad: TDate = 0): Integer;
begin
  Result := UnAgendaJan.MostraFormAgenTarIts(0, 0, CO_Agend_Categorias_Cod[OriID],
              OriNum, Descri, Prioridade, DataCad);
end;

function TUnAppPF.AGEN_ExcluiTarefasItem(DataBase: TComponent; Query: TDataSet;
  Controle: Integer; Msg: String; ExcluiOriID: Boolean): Boolean;
begin
  GrlAgenda.ExcluiTarefasItem(DataBase, Query, Controle, Msg, ExcluiOriID);
end;

procedure TUnAppPF.AGEN_MostraFormOrigem(OriID: Integer; OriNum: Double);
var
  Frm: TForm;
begin
  if OriID = CO_Agend_Categorias_Cod[1] then //WOrdSer
    UnWOrdSerJan.MostraWOrdSer(True, FmPrincipal.PageControl1,
      FmPrincipal.AdvToolBarPagerNovo, Trunc(OriNum), False, False);
end;

procedure TUnAppPF.Html_ConfigarImagem(WebBrowser: TWebBrowser);
begin
  UnHelpJan.MostraFormHelpFAQImg(WebBrowser);
end;

procedure TUnAppPF.Html_ConfigarUrl(WebBrowser: TWebBrowser);
var
  Confirmou: Boolean;
begin
  UnHelpJan.MostraFormHelpFAQUrl(WebBrowser, thtAll, Confirmou);
end;

procedure TUnAppPF.Html_ConfigarVideo(WebBrowser: TWebBrowser);
begin
  UnHelpJan.MostraFormHelpFAQVideo(WebBrowser);
end;

procedure TUnAppPF.MostraOrigemFat(FatParcRef: Integer);
begin
  //Compatibilidade
end;

procedure TUnAppPF.MostraOrigemFatGenerico(FatID: Integer; FatNum: Double; FatParcela,
  FatParcRef: Integer);
begin
  // Fazer??!!!!
end;

procedure TUnAppPF.AGEN_MostraFormAgenTarGer(Form: TForm; AbrirEmAba: Boolean;
  InOwner: TWincontrol; Pager: TWinControl; Controle: Integer);
begin
  UnAgendaJan.MostraFormAgenTarGer(Form, AbrirEmAba, InOwner, Pager, Controle);
end;

function TUnAppPF.AcaoEspecificaDeApp(Servico: String; CliInt,
  EntCliInt: Integer; Query: TmySQLQuery = nil; Query2: TmySQLQuery = nil): Boolean;
begin
  if (Servico = 'ArreFut') and (Query <> nil) and (Query2 <> nil) then
  begin
    UBloqGerl_Jan.MostraFmCondGerArreFut(stIns, CliInt, EntCliInt, nil, Query, Query2);
    //
    Result := True;
  end else
  if (Servico = 'LctGer2') and (Query <> nil) and (Query2 = nil) then
  begin
    UBloqGerl_Jan.MostraFmCondGerArreFut(stIns, CliInt, EntCliInt, nil, Query, nil);
    //
    Result := True;
  end else
    Result := True;
end;

end.
