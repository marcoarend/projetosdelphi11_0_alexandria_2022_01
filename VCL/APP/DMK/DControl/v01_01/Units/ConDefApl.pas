unit ConDefApl;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, dmkDBGridDAC, UnDmkEnums;

type
  TFmConDefApl = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrAplic: TmySQLQuery;
    dmkDBGridDAC1: TdmkDBGridDAC;
    QrTmpApl: TmySQLQuery;
    QrTmpAplCodigo: TIntegerField;
    QrTmpAplAtivo: TSmallintField;
    QrTmpAplNome: TWideStringField;
    DsTmpApl: TDataSource;
    QrAplicCodigo: TIntegerField;
    QrAplicNome: TWideStringField;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure dmkDBGridDAC1AfterSQLExec(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    FTmpTable: String;
    procedure ReopenAplic(Codigo: Integer);
    procedure MontaTmpTable(Codigo: Integer);
    procedure ReopenTmpApl;
    procedure AtualizaTodos(Status: Integer);
  public
    { Public declarations }
    FCodigo: Integer;
  end;

  var
  FmConDefApl: TFmConDefApl;

implementation

uses UnMyObjects, Module, UCreate, ModuleGeral, UMySQLModule, ConDef;

{$R *.DFM}

procedure TFmConDefApl.AtualizaTodos(Status: Integer);
begin
  DmodG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('UPDATE ' + FTmpTable + ' SET Ativo=:P0');
  DmodG.QrUpdPID1.Params[00].AsInteger := Status;
  DmodG.QrUpdPID1.ExecSQL;
  //
  ReopenTmpApl;
end;

procedure TFmConDefApl.BtNenhumClick(Sender: TObject);
begin
  AtualizaTodos(0);
end;

procedure TFmConDefApl.BtOKClick(Sender: TObject);
var
  Codigo, Controle, Aplic: Integer;
begin
  try
    Screen.Cursor := crHourGlass;
    //
    Codigo   := FCodigo;
    Controle := 0;
    //
    QrTmpApl.First;
    while not QrTmpApl.Eof do
    begin
      Aplic := QrTmpAplCodigo.Value;
      //
      if QrTmpAplAtivo.Value = 1 then
      begin
        Controle := UMyMod.BuscaNovoCodigo_Int(Dmod.QrUpd, 'condefapl', 'Controle', [], [], stIns, 0, siPositivo, nil);
        //
        if UMyMod.SQLInsUpd_Old(Dmod.QrUpd, CO_INCLUSAO, 'condefapl', False,
        [
          'Aplicativo', 'Codigo'
        ], ['Controle'],
        [
          Aplic, Codigo
        ], [Controle])
        then begin
        end;
      end;
      //
      QrTmpApl.Next;
    end;
    FmConDef.ReopenConDefApl(FCodigo, Controle);
  finally
    Screen.Cursor := crDefault;
  end;
  Close;
end;

procedure TFmConDefApl.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmConDefApl.BtTodosClick(Sender: TObject);
begin
  AtualizaTodos(1);
end;

procedure TFmConDefApl.dmkDBGridDAC1AfterSQLExec(Sender: TObject);
begin
  ReopenTmpApl;
end;

procedure TFmConDefApl.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmConDefApl.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmConDefApl.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmConDefApl.FormShow(Sender: TObject);
begin
  MontaTmpTable(FCodigo);
end;

procedure TFmConDefApl.MontaTmpTable(Codigo: Integer);
begin
  try
    Screen.Cursor := crHourGlass;
    //
    FTmpTable := UCriar.RecriaTempTableNovo(ntrtt_CodNom, DmodG.QrUpdPID1, True, 1, 'MalaDirCon');
    dmkDBGridDAC1.SQLTable := FTmpTable;
    //
    ReopenAplic(Codigo);
    //
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('INSERT INTO ' + FTmpTable + ' SET ');
    DmodG.QrUpdPID1.SQL.Add('Codigo=:P0, Nome=:P1, Ativo=:P2');
    while not QrAplic.Eof do
    begin
      DmodG.QrUpdPID1.Params[00].AsInteger := QrAplicCodigo.Value;
      DmodG.QrUpdPID1.Params[01].AsString  := QrAplicNome.Value;
      DmodG.QrUpdPID1.Params[02].AsInteger := 0;
      DmodG.QrUpdPID1.ExecSQL;
      //
      QrAplic.Next;
    end;
    QrAplic.Close;
    ReopenTmpApl;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmConDefApl.ReopenAplic(Codigo: Integer);
begin
  QrAplic.Close;
  QrAplic.Database := Dmod.MyDB;
  QrAplic.Params[0].AsInteger := Codigo;
  QrAplic.Open;
end;

procedure TFmConDefApl.ReopenTmpApl;
begin
  QrTmpApl.Close;
  QrTmpApl.Database := DModG.MyPID_DB;
  QrTmpApl.SQL.Clear;
  QrTmpApl.SQL.Add('SELECT * FROM ' + FTmpTable + ' ORDER BY Nome');
  QrTmpApl.Open;
end;

end.
