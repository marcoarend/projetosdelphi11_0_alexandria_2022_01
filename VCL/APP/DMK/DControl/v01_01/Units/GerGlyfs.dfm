object FmGerGlyfs: TFmGerGlyfs
  Left = 339
  Top = 185
  Caption = 'GER-GLYFS-001 :: Gerenciamento de Glyfs'
  ClientHeight = 492
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 293
        Height = 32
        Caption = 'Gerenciamento de Glyfs'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 293
        Height = 32
        Caption = 'Gerenciamento de Glyfs'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 293
        Height = 32
        Caption = 'Gerenciamento de Glyfs'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 330
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 330
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 330
        Align = alClient
        TabOrder = 0
        object Splitter1: TSplitter
          Left = 2
          Top = 175
          Width = 1004
          Height = 10
          Cursor = crVSplit
          Align = alTop
          ExplicitTop = 129
          ExplicitWidth = 780
        end
        object PnTopo: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 160
          Align = alTop
          TabOrder = 0
          object Panel5: TPanel
            Left = 955
            Top = 39
            Width = 48
            Height = 120
            Align = alRight
            TabOrder = 0
            object BtUpdate: TBitBtn
              Tag = 18
              Left = 4
              Top = 4
              Width = 40
              Height = 40
              NumGlyphs = 2
              TabOrder = 0
              OnClick = BtUpdateClick
            end
          end
          object dmkDBGrid1: TdmkDBGrid
            Left = 1
            Top = 39
            Width = 954
            Height = 120
            Align = alClient
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'ID'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Width = 100
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Descri'
                Title.Caption = 'Descri'#231#227'o'
                Width = 350
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Descri2'
                Width = 350
                Visible = True
              end>
            Color = clWindow
            DataSource = DsGerGlyfs
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
            TabOrder = 1
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnDblClick = dmkDBGrid1DblClick
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'ID'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Width = 100
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Descri'
                Title.Caption = 'Descri'#231#227'o'
                Width = 350
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Descri2'
                Width = 350
                Visible = True
              end>
          end
          object PB1: TProgressBar
            Left = 1
            Top = 1
            Width = 1002
            Height = 17
            Align = alTop
            TabOrder = 2
            Visible = False
          end
          object EdPesquisa: TdmkEdit
            Left = 1
            Top = 18
            Width = 1002
            Height = 21
            Align = alTop
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            OnChange = EdPesquisaChange
          end
        end
        object Panel6: TPanel
          Left = 2
          Top = 185
          Width = 1004
          Height = 143
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object PageControl1: TPageControl
            Left = 0
            Top = 0
            Width = 1004
            Height = 143
            ActivePage = TabSheet1
            Align = alClient
            TabOrder = 0
            object TabSheet1: TTabSheet
              Caption = 'Arquivos'
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object Splitter2: TSplitter
                Left = 550
                Top = 0
                Width = 10
                Height = 115
                ExplicitLeft = 0
                ExplicitHeight = 161
              end
              object dmkDBGrid2: TdmkDBGrid
                Left = 0
                Top = 0
                Width = 550
                Height = 115
                Align = alLeft
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Codigo'
                    Title.Caption = 'ID'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Pasta'
                    Title.Caption = 'Nome diret'#243'rio'
                    Width = 100
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Caminho'
                    Title.Caption = 'Diret'#243'rio'
                    Width = 320
                    Visible = True
                  end>
                Color = clWindow
                DataSource = DsGerGlyfsDi
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
                TabOrder = 0
                TitleFont.Charset = ANSI_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Codigo'
                    Title.Caption = 'ID'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Pasta'
                    Title.Caption = 'Nome diret'#243'rio'
                    Width = 100
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Caminho'
                    Title.Caption = 'Diret'#243'rio'
                    Width = 320
                    Visible = True
                  end>
              end
              object Panel7: TPanel
                Left = 560
                Top = 0
                Width = 436
                Height = 115
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 1
                object ImgGerGlyfsIt: TImage
                  Left = 0
                  Top = 48
                  Width = 436
                  Height = 67
                  Align = alClient
                  ExplicitLeft = 608
                  ExplicitTop = 56
                  ExplicitWidth = 105
                  ExplicitHeight = 105
                end
                object Panel8: TPanel
                  Left = 0
                  Top = 0
                  Width = 436
                  Height = 48
                  Align = alTop
                  ParentBackground = False
                  TabOrder = 0
                  object BtPasta: TBitBtn
                    Tag = 274
                    Left = 4
                    Top = 4
                    Width = 40
                    Height = 40
                    NumGlyphs = 2
                    TabOrder = 0
                    OnClick = BtPastaClick
                  end
                  object EdArquivo: TBitBtn
                    Tag = 366
                    Left = 46
                    Top = 4
                    Width = 40
                    Height = 40
                    NumGlyphs = 2
                    TabOrder = 1
                    OnClick = EdArquivoClick
                  end
                end
              end
            end
            object TabSheet2: TTabSheet
              Caption = 'Aplicativos'
              ImageIndex = 1
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 161
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 378
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 492
        Height = 16
        Caption = 
          'O diret'#243'rio padr'#227'o de Glyfs deve ser configurado nas op'#231#245'es espe' +
          'c'#237'ficas do aplciativo'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 492
        Height = 16
        Caption = 
          'O diret'#243'rio padr'#227'o de Glyfs deve ser configurado nas op'#231#245'es espe' +
          'c'#237'ficas do aplciativo'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 422
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
    end
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    Left = 328
    Top = 284
  end
  object DsGerGlyfsDi: TDataSource
    DataSet = QrGerGlyfsIt
    Left = 600
    Top = 12
  end
  object QrGerGlyfsIt: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrGerGlyfsItBeforeClose
    AfterScroll = QrGerGlyfsItAfterScroll
    SQL.Strings = (
      'SELECT * '
      'FROM gerglyfsit')
    Left = 572
    Top = 12
    object QrGerGlyfsItCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'gerglyfsit.Codigo'
    end
    object QrGerGlyfsItNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'gerglyfsit.Nome'
      Size = 50
    end
    object QrGerGlyfsItLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'gerglyfsit.Lk'
    end
    object QrGerGlyfsItDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'gerglyfsit.DataCad'
    end
    object QrGerGlyfsItDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'gerglyfsit.DataAlt'
    end
    object QrGerGlyfsItUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'gerglyfsit.UserCad'
    end
    object QrGerGlyfsItUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'gerglyfsit.UserAlt'
    end
    object QrGerGlyfsItAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'gerglyfsit.AlterWeb'
    end
    object QrGerGlyfsItAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'gerglyfsit.Ativo'
    end
    object QrGerGlyfsItPasta: TWideStringField
      FieldName = 'Pasta'
      Origin = 'gerglyfsit.Pasta'
      Size = 50
    end
    object QrGerGlyfsItCaminho: TWideStringField
      FieldName = 'Caminho'
      Origin = 'gerglyfsit.Caminho'
      Size = 255
    end
  end
  object QrLoc2: TmySQLQuery
    Database = Dmod.MyDB
    Left = 357
    Top = 284
  end
  object QrGerGlyfs: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrGerGlyfsBeforeClose
    AfterScroll = QrGerGlyfsAfterScroll
    SQL.Strings = (
      'SELECT * '
      'FROM gerglyfs')
    Left = 516
    Top = 12
    object QrGerGlyfsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'gerglyfs.Codigo'
    end
    object QrGerGlyfsNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'gerglyfs.Nome'
      Size = 50
    end
    object QrGerGlyfsLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'gerglyfs.Lk'
    end
    object QrGerGlyfsDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'gerglyfs.DataCad'
    end
    object QrGerGlyfsDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'gerglyfs.DataAlt'
    end
    object QrGerGlyfsUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'gerglyfs.UserCad'
    end
    object QrGerGlyfsUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'gerglyfs.UserAlt'
    end
    object QrGerGlyfsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'gerglyfs.AlterWeb'
    end
    object QrGerGlyfsAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'gerglyfs.Ativo'
    end
    object QrGerGlyfsDescri: TWideStringField
      FieldName = 'Descri'
      Origin = 'gerglyfs.Descri'
      Size = 100
    end
    object QrGerGlyfsDescri2: TWideStringField
      FieldName = 'Descri2'
      Origin = 'gerglyfs.Descri2'
      Size = 100
    end
  end
  object DsGerGlyfs: TDataSource
    DataSet = QrGerGlyfs
    Left = 544
    Top = 12
  end
end
