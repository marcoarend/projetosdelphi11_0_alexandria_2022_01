unit GerGlyfs;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, DBClient, dmkDBGrid, DmkDAC_PF, UnDmkProcFunc, UnDmkEnums;

type
  THackDBGrid = class(TDBGrid);
  TFmGerGlyfs = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    PnTopo: TPanel;
    Panel5: TPanel;
    dmkDBGrid1: TdmkDBGrid;
    Splitter1: TSplitter;
    BtUpdate: TBitBtn;
    QrLoc: TmySQLQuery;
    DsGerGlyfsDi: TDataSource;
    Panel6: TPanel;
    QrGerGlyfsIt: TmySQLQuery;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Splitter2: TSplitter;
    dmkDBGrid2: TdmkDBGrid;
    Panel7: TPanel;
    ImgGerGlyfsIt: TImage;
    Panel8: TPanel;
    BtPasta: TBitBtn;
    EdArquivo: TBitBtn;
    QrLoc2: TmySQLQuery;
    PB1: TProgressBar;
    QrGerGlyfsItCodigo: TIntegerField;
    QrGerGlyfsItNome: TWideStringField;
    QrGerGlyfsItLk: TIntegerField;
    QrGerGlyfsItDataCad: TDateField;
    QrGerGlyfsItDataAlt: TDateField;
    QrGerGlyfsItUserCad: TIntegerField;
    QrGerGlyfsItUserAlt: TIntegerField;
    QrGerGlyfsItAlterWeb: TSmallintField;
    QrGerGlyfsItAtivo: TSmallintField;
    QrGerGlyfsItPasta: TWideStringField;
    QrGerGlyfsItCaminho: TWideStringField;
    EdPesquisa: TdmkEdit;
    QrGerGlyfs: TmySQLQuery;
    DsGerGlyfs: TDataSource;
    QrGerGlyfsCodigo: TIntegerField;
    QrGerGlyfsNome: TWideStringField;
    QrGerGlyfsLk: TIntegerField;
    QrGerGlyfsDataCad: TDateField;
    QrGerGlyfsDataAlt: TDateField;
    QrGerGlyfsUserCad: TIntegerField;
    QrGerGlyfsUserAlt: TIntegerField;
    QrGerGlyfsAlterWeb: TSmallintField;
    QrGerGlyfsAtivo: TSmallintField;
    QrGerGlyfsDescri: TWideStringField;
    QrGerGlyfsDescri2: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtUpdateClick(Sender: TObject);
    procedure BtPastaClick(Sender: TObject);
    procedure EdArquivoClick(Sender: TObject);
    procedure QrGerGlyfsItAfterScroll(DataSet: TDataSet);
    procedure QrGerGlyfsItBeforeClose(DataSet: TDataSet);
    procedure EdPesquisaChange(Sender: TObject);
    procedure QrGerGlyfsAfterScroll(DataSet: TDataSet);
    procedure QrGerGlyfsBeforeClose(DataSet: TDataSet);
    procedure dmkDBGrid1DblClick(Sender: TObject);
  private
    { Private declarations }
    procedure AdicionaArquivos(Diretorio: String; Sub: Boolean);
    procedure ReopenGerGlyfs(Texto: String);
    procedure ReopenGerGlyfsIt();
    function ExtractName(Filename: String): String;
  public
    { Public declarations }
  end;

  var
  FmGerGlyfs: TFmGerGlyfs;

implementation

uses UnMyObjects, Module, UMySQLModule;

{$R *.DFM}

procedure TFmGerGlyfs.BtUpdateClick(Sender: TObject);
  procedure InsereGerGlyfs();
  var
    Nome: String;
    Codigo: Integer;
  begin
    Codigo := 0;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
    'SELECT DISTINCT(Nome) Nome ',
    'FROM gerglyfsit ',
    'WHERE Nome NOT IN ',
    '( ',
    'SELECT Nome ',
    'FROM gerglyfs ',
    ') ',
    'ORDER BY Nome ',
    '']);
    if QrLoc.RecordCount > 0 then
    begin
      PB1.Position := 0;
      PB1.Max      := QrLoc.RecordCount;
      PB1.Visible  := True;
      //
      while not QrLoc.Eof do
      begin
        Codigo := UMyMod.BuscaEmLivreY_Def('gerglyfs', 'Codigo', stIns, Codigo, nil);
        Nome   := QrLoc.FieldByName('Nome').AsString;
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gerglyfs', False, [
          'Nome'
        ], ['Codigo'],
        [
          Nome
        ], [Codigo], True) then
        begin
        end;
        Application.ProcessMessages;
        PB1.Update;
        PB1.Position := PB1.Position + 1;
        QrLoc.Next;
      end;
    end;
    PB1.Visible := False;
  end;
  procedure DeletaGerGlyfsItExcluidos();
  var
    Codigo: Integer;
    Caminho: String;
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
    'SELECT  Codigo, Caminho ',
    'FROM gerglyfsit ',
    '']);
    if QrLoc.RecordCount > 0 then
    begin
      PB1.Position := 0;
      PB1.Max      := QrLoc.RecordCount;
      PB1.Visible  := True;
      //
      while not QrLoc.Eof do
      begin
        Codigo  := QrLoc.FieldByName('Codigo').AsInteger;
        Caminho := QrLoc.FieldByName('Caminho').AsString;
        //
        if not FileExists(Caminho) then
          UMyMod.ExcluiRegistroInt1('', 'gerglyfsit', 'Codigo', Codigo, Dmod.MyDB);
        //
        Application.ProcessMessages;
        PB1.Update;
        PB1.Position := PB1.Position + 1;
        //
        QrLoc.Next;
      end;
    end;
    PB1.Visible := False;
  end;
  procedure DeletaGerGlyfsExcluidos();
  begin
    QrLoc.Close;
    QrLoc.SQL.Clear;
    QrLoc.SQL.Add('DELETE FROM gerglyfs WHERE Nome NOT IN (SELECT Nome FROM gerglyfsit)');
    QrLoc.ExecSQL;
 end;
var
  Dir: String;
begin
  Dmod.ReopenControle;
  //
  Dir := Dmod.QrOpcoesDCon.FieldByName('CaminhoGlyfs').AsString;
  if (Length(Dir) > 0) and (DirectoryExists(Dir)) then
  begin
    try
      Screen.Cursor := crHourGlass;
      //
      AdicionaArquivos(Dir, True);
      InsereGerGlyfs;
      DeletaGerGlyfsItExcluidos;
      DeletaGerGlyfsExcluidos;
    finally
      QrGerGlyfs.Close;
      QrGerGlyfs.Open;
      //
      Screen.Cursor := crDefault;
      //
      Geral.MensagemBox('Itens atualizados com sucesso!', 'Aviso', MB_OK+MB_ICONWARNING);
    end;
  end;
end;

procedure TFmGerGlyfs.dmkDBGrid1DblClick(Sender: TObject);
  procedure AtualizaBD(Campo, CampoValor: String);
  var
    Codigo: Integer;
  begin
    Codigo := QrGerGlyfsCodigo.Value;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gerglyfs', False, [
      Campo
    ], ['Codigo'],
    [
      CampoValor
    ], [Codigo], True) then
    begin
      ReopenGerGlyfs(EdPesquisa.ValueVariant);
    end;
  end;
var
  Codigo: Integer;
  Campo, Descri: String;
begin
  if (QrGerGlyfs.State <> dsInactive) and (QrGerGlyfs.RecordCount > 0) then
  begin
    Campo  := UpperCase(TDBGrid(Sender).Columns[THackDBGrid(Sender).Col -1].FieldName);
    Codigo := QrGerGlyfsCodigo.Value;
    //
    if Campo = 'DESCRI' then
    begin
      Descri := QrGerGlyfsDescri.Value;
      //
      if InputQuery('Descri��o', 'Informe a descri��o:', Descri) then
        AtualizaBD('Descri', Descri);
     //                
     QrGerGlyfs.Locate('Codigo', Codigo, []);
    end
    else if Campo = 'DESCRI2' then
    begin
      Descri := QrGerGlyfsDescri2.Value;
      //
      if InputQuery('Descri��o', 'Informe a descri��o:', Descri) then
        AtualizaBD('Descri2', Descri);
     //
     QrGerGlyfs.Locate('Codigo', Codigo, []);
    end
  end;
end;

procedure TFmGerGlyfs.BtPastaClick(Sender: TObject);
var
  Dir: String;
begin
  Dir := ExtractFileDir(QrGerGlyfsItCaminho.Value);
  //
  if DirectoryExists(Dir) then
    Geral.AbreArquivo(Dir);
end;

procedure TFmGerGlyfs.EdArquivoClick(Sender: TObject);
var
  Arquivo: String;
begin
  Arquivo := QrGerGlyfsItCaminho.Value;
  //
  if FileExists(Arquivo) then
    Geral.AbreArquivo(Arquivo, True);
end;

procedure TFmGerGlyfs.EdPesquisaChange(Sender: TObject);
var
  Texto: String;
begin
  Texto := LowerCase(EdPesquisa.ValueVariant);
  //
  ReopenGerGlyfs(Texto);
end;

function TFmGerGlyfs.ExtractName(Filename: String): String;
var
  aExt : String;
  aPos : Integer;
begin
  aExt := ExtractFileExt(Filename);
  Result := ExtractFileName(Filename);
  if aExt <> '' then
  begin
    aPos := Pos(aExt,Result);
    if aPos > 0 then
    begin
      Delete(Result,aPos,Length(aExt));
    end;
  end;
end;

procedure TFmGerGlyfs.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGerGlyfs.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGerGlyfs.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PB1.Visible     := False;
  //
  ReopenGerGlyfs('');
end;

procedure TFmGerGlyfs.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGerGlyfs.QrGerGlyfsAfterScroll(DataSet: TDataSet);
begin
  ReopenGerGlyfsIt;
end;

procedure TFmGerGlyfs.QrGerGlyfsBeforeClose(DataSet: TDataSet);
begin
  QrGerGlyfsIt.Close;
end;

procedure TFmGerGlyfs.QrGerGlyfsItAfterScroll(DataSet: TDataSet);
var
  Caminho, Extensao: String;
begin
  ImgGerGlyfsIt.Visible := True;
  Caminho  := QrGerGlyfsItCaminho.Value;
  Extensao := ExtractFileExt(Caminho);
  //
  if (FileExists(Caminho)) and (UpperCase(Extensao) <> '.GIF') then
    ImgGerGlyfsIt.Picture.LoadFromFile(QrGerGlyfsItCaminho.Value)
  else
    ImgGerGlyfsIt.Visible := False;
end;

procedure TFmGerGlyfs.QrGerGlyfsItBeforeClose(DataSet: TDataSet);
begin
  ImgGerGlyfsIt.Visible := False;
end;

procedure TFmGerGlyfs.ReopenGerGlyfs(Texto: String);
var
  SQLPesq: String;
begin
  if Length(Texto) > 0 then
    SQLPesq := 'AND Nome LIKE "%'+ Texto +'%" OR Descri LIKE "%'+
                 Texto +'%" OR Descri2 LIKE "%'+ Texto +'%"'
  else
    SQLPesq := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrGerGlyfs, Dmod.MyDB, [
  'SELECT * ',
  'FROM gerglyfs ',
  'WHERE Ativo = 1',
  SQLPesq,
  '']);
end;

procedure TFmGerGlyfs.ReopenGerGlyfsIt;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGerGlyfsIt, Dmod.MyDB, [
  'SELECT it.* ',
  'FROM gerglyfsit it ',
  'WHERE it.Nome="' + QrGerGlyfsNome.Value + '"',
  '']);
end;

procedure TFmGerGlyfs.AdicionaArquivos(Diretorio: String; Sub: Boolean);
  function TemAtributo(Attr, Val: Integer): Boolean;
  begin
    Result := Attr and Val = Val;
  end;
  procedure AdicionaItem(Caminho: String);
    procedure InsereGerGlyfsIt(NomeArq, DirName, Caminho: String);
    var
      Codigo: Integer;
    begin
      Codigo := 0;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
      'SELECT  Codigo ',
      'FROM gerglyfsit ',
      'WHERE Caminho = "' + Trim(dmkPF.DuplicaBarras(Caminho)) + '"',
      '']);
      if QrLoc.RecordCount = 0 then
      begin
        Codigo := UMyMod.BuscaEmLivreY_Def('gerglyfsit', 'Codigo', stIns, Codigo, nil);
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gerglyfsit', False, [
          'Nome', 'Pasta', 'Caminho'
        ], ['Codigo'],
        [
          NomeArq, DirName, Caminho
        ], [Codigo], True) then
        begin
        end;
      end;
    end;
  var
    DirName, NomeArq: String;
  begin
    NomeArq := ExtractName(Caminho);
    DirName := ExtractFileName(ExtractFileDir(Caminho));
    //
    InsereGerGlyfsIt(NomeArq, DirName, Caminho);
  end;
var
  F: TSearchRec;
  Ret: Integer;
  TempNome: string;
begin
  Ret := FindFirst(Diretorio+'\*.*', faAnyFile, F);
  try
    while Ret = 0 do
    begin
      if TemAtributo(F.Attr, faDirectory) then
      begin
        if (F.Name <> '.') And (F.Name <> '..') then
        begin
          if Sub = True then
          begin
            TempNome := Diretorio+'\' + F.Name;
            AdicionaArquivos(TempNome, True);
          end;
        end;
      end else
      begin
        AdicionaItem(Diretorio+'\' + F.Name);
      end;
      Ret := FindNext(F);
    end;
  finally
    FindClose(F);
  end;
end;

end.
