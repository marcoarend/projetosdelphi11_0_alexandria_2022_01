unit Clientes;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt, Variants,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, dmkDBLookupComboBox, dmkEditCB, dmkCheckBox, DmkDAC_PF, Menus,
  ComCtrls, Grids, DBGrids, dmkDBGrid, frxClass, frxDBSet, UnDmkEnums,
  dmkCompoStore, ShellAPI, Datasnap.DBClient;

type
  THackDBGrid = class(TDBGrid);
  TFmClientes = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtContratos: TBitBtn;
    BtCliente: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrClientes: TmySQLQuery;
    DsClientes: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Label9: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    SpeedButton5: TSpeedButton;
    CkAtivo: TdmkCheckBox;
    QrEntidades: TmySQLQuery;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNome: TWideStringField;
    DsEntidades: TDataSource;
    dmkDBEdit3: TdmkDBEdit;
    LaIE: TLabel;
    dmkDBEdit2: TdmkDBEdit;
    LaCNPJ: TLabel;
    LaFantasia: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    QrClientesCodigo: TIntegerField;
    QrClientesCliente: TIntegerField;
    QrClientesLk: TIntegerField;
    QrClientesDataCad: TDateField;
    QrClientesDataAlt: TDateField;
    QrClientesUserCad: TIntegerField;
    QrClientesUserAlt: TIntegerField;
    QrClientesAlterWeb: TSmallintField;
    QrClientesAtivo: TSmallintField;
    QrClientesENTRAZ: TWideStringField;
    QrClientesENTFAN: TWideStringField;
    QrClientesENTCNPJ: TWideStringField;
    QrClientesENTIE: TWideStringField;
    QrClientesENTCNPJ_TXT: TWideStringField;
    QrClientesENTTIPO: TSmallintField;
    DBCheckBox1: TDBCheckBox;
    PMCliente: TPopupMenu;
    Inclui1: TMenuItem;
    Altera1: TMenuItem;
    Exclui1: TMenuItem;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel8: TPanel;
    EdPesquisa: TdmkEdit;
    QrCliObser: TmySQLQuery;
    QrCliObserCodigo: TIntegerField;
    QrCliObserControle: TIntegerField;
    QrCliObserNome: TWideStringField;
    QrCliObserLk: TIntegerField;
    QrCliObserDataCad: TDateField;
    QrCliObserDataAlt: TDateField;
    QrCliObserUserCad: TIntegerField;
    QrCliObserUserAlt: TIntegerField;
    QrCliObserAlterWeb: TSmallintField;
    QrCliObserAtivo: TSmallintField;
    QrCliObserTexto: TWideMemoField;
    DsCliObser: TDataSource;
    DBGCliObser: TdmkDBGrid;
    Splitter4: TSplitter;
    Panel7: TPanel;
    DBRichEdit2: TDBRichEdit;
    FindDialog1: TFindDialog;
    PMObser: TPopupMenu;
    Inclui4: TMenuItem;
    Altera4: TMenuItem;
    Exclui4: TMenuItem;
    DBGCliContr: TDBGrid;
    TabSheet3: TTabSheet;
    Panel10: TPanel;
    DBGCliAplicMo: TDBGrid;
    StaticText4: TStaticText;
    DBGCliAplic: TdmkDBGrid;
    Splitter6: TSplitter;
    QrCliContr: TmySQLQuery;
    DsCliContr: TDataSource;
    QrCliAplic: TmySQLQuery;
    QrCliAplicCodigo: TIntegerField;
    QrCliAplicControle: TIntegerField;
    QrCliAplicAplicativo: TIntegerField;
    QrCliAplicLk: TIntegerField;
    QrCliAplicDataCad: TDateField;
    QrCliAplicDataAlt: TDateField;
    QrCliAplicUserCad: TIntegerField;
    QrCliAplicUserAlt: TIntegerField;
    QrCliAplicAlterWeb: TSmallintField;
    QrCliAplicAtivo: TSmallintField;
    QrCliAplicFimData: TDateField;
    QrCliAplicStatAplic: TSmallintField;
    QrCliAplicQtdLicen: TIntegerField;
    QrCliAplicSTATAPLIC_TXT: TWideStringField;
    QrCliAplicQtdCliInt: TIntegerField;
    DsCliAplic: TDataSource;
    QrCliAplicMo: TmySQLQuery;
    DsCliAplicMo: TDataSource;
    PMContratos: TPopupMenu;
    BtAplic: TBitBtn;
    PMAplic: TPopupMenu;
    Inclui2: TMenuItem;
    Altera2: TMenuItem;
    Exclui2: TMenuItem;
    N1: TMenuItem;
    Listadecomputadoresparaoaplicselec1: TMenuItem;
    BtEntidades: TBitBtn;
    PMImprime: TPopupMenu;
    Lista1: TMenuItem;
    Ativos1: TMenuItem;
    odos1: TMenuItem;
    frxCAD_CLIEN_001_001: TfrxReport;
    QrListaClientes: TmySQLQuery;
    QrListaClientesENTRAZ: TWideStringField;
    QrListaClientesENTFAN: TWideStringField;
    QrListaClientesCodigo: TIntegerField;
    QrListaClientesID: TIntegerField;
    QrListaClientesAtivo: TIntegerField;
    frxDsListaClientes: TfrxDBDataset;
    QrCliAplicMoConta: TIntegerField;
    BtObser: TBitBtn;
    Label3: TLabel;
    QrCliAplicDataBaseCfg: TIntegerField;
    QrCliAplicFTPCfg: TIntegerField;
    CSTabSheetChamou: TdmkCompoStore;
    AbrirURLdowebapp1: TMenuItem;
    QrCliAplicBeta: TSmallintField;
    BtModulos: TBitBtn;
    PMModulos: TPopupMenu;
    Incluimduloaoaplicativoselecionado1: TMenuItem;
    Excluimduloaoaplicativoselecionado1: TMenuItem;
    Panel6: TPanel;
    BtLocaliza: TBitBtn;
    QrCliContrCodigo: TIntegerField;
    QrCliContrVersao: TFloatField;
    QrCliContrNome: TWideStringField;
    QrCliContrDContrato_TXT: TWideStringField;
    QrCliContrDtaAssina_TXT: TWideStringField;
    QrCliContrDInstal_TXT: TWideStringField;
    QrCliContrDVencimento_TXT: TWideStringField;
    QrCliContrDtaPrxRenw_TXT: TWideStringField;
    QrCliContrddMesVcto: TSmallintField;
    QrCliContrValorMes: TFloatField;
    Incluinovocontrato1: TMenuItem;
    Gerenciacontratoselecionado1: TMenuItem;
    Ativadesativa1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    IncluiAppWeb1: TMenuItem;
    QrCliAplicAplicativoTxt: TWideStringField;
    QrCliAplicTipAmb_TXT: TWideStringField;
    QrCliAplicTipAmb: TSmallintField;
    QrCliAplicFimData_TXT: TWideStringField;
    EdCli_Id: TdmkEdit;
    Label26: TLabel;
    QrClientesCli_Id: TWideStringField;
    dmkDBEdit4: TdmkDBEdit;
    Label4: TLabel;
    QrCliAplicMoCodigo: TIntegerField;
    QrCliAplicMoControle: TIntegerField;
    QrCliAplicMoModulo: TIntegerField;
    QrCliAplicMoModuloStr: TWideStringField;
    QrCliAplicMoDescri: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrClientesAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrClientesBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure PMClientePopup(Sender: TObject);
    procedure Altera1Click(Sender: TObject);
    procedure Inclui1Click(Sender: TObject);
    procedure Exclui1Click(Sender: TObject);
    procedure QrClientesAfterScroll(DataSet: TDataSet);
    procedure QrClientesBeforeClose(DataSet: TDataSet);
    procedure EdPesquisaChange(Sender: TObject);
    procedure EdPesquisaEnter(Sender: TObject);
    procedure EdPesquisaExit(Sender: TObject);
    procedure FindDialog1Find(Sender: TObject);
    procedure PMObserPopup(Sender: TObject);
    procedure Inclui4Click(Sender: TObject);
    procedure Altera4Click(Sender: TObject);
    procedure Exclui4Click(Sender: TObject);
    procedure QrCliAplicCalcFields(DataSet: TDataSet);
    procedure BtContratosClick(Sender: TObject);
    procedure BtObserClick(Sender: TObject);
    procedure PMContratosPopup(Sender: TObject);
    procedure PMAplicPopup(Sender: TObject);
    procedure BtAplicClick(Sender: TObject);
    procedure Inclui2Click(Sender: TObject);
    procedure Exclui2Click(Sender: TObject);
    procedure Altera2Click(Sender: TObject);
    procedure Listadecomputadoresparaoaplicselec1Click(Sender: TObject);
    procedure BtClienteClick(Sender: TObject);
    procedure BtEntidadesClick(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure ImgTipoChange(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure Ativos1Click(Sender: TObject);
    procedure odos1Click(Sender: TObject);
    procedure frxCAD_CLIEN_001_001GetValue(const VarName: string;
      var Value: Variant);
    procedure PMImprimePopup(Sender: TObject);
    procedure Incluimduloaoaplicativoselecionado1Click(Sender: TObject);
    procedure QrCliAplicAfterScroll(DataSet: TDataSet);
    procedure QrCliAplicBeforeClose(DataSet: TDataSet);
    procedure Excluimduloaoaplicativoselecionado1Click(Sender: TObject);
    procedure DBGCliAplicDblClick(Sender: TObject);
    procedure QrClientesCalcFields(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure AbrirURLdowebapp1Click(Sender: TObject);
    procedure PMModulosPopup(Sender: TObject);
    procedure BtLocalizaClick(Sender: TObject);
    procedure Incluinovocontrato1Click(Sender: TObject);
    procedure Gerenciacontratoselecionado1Click(Sender: TObject);
    procedure BtModulosClick(Sender: TObject);
    procedure Ativadesativa1Click(Sender: TObject);
    procedure IncluiAppWeb1Click(Sender: TObject);
    procedure EdCli_IdExit(Sender: TObject);
  private
    FCliAtivos: Boolean;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure ReopenCliAplicMo(Conta: Integer);
    procedure ConfiguraMensagemAbas(Aba: Integer);
    procedure ImprimeListaClientes(Ativo: Boolean);
    procedure MostraClientesAplic(SQLType: TSQLType);
    procedure MostraClientesAplicWeb(SQLType: TSQLType);
    procedure MostraClientesObser(SQLType: TSQLType);
    procedure IncluiModuloAplicativo();
    function  FormataCli_Id(Cli_Id: String): String;
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenCliObser(Controle: Integer; Nome: String);
    procedure ReopenCliContr(Contrato: Integer);
    procedure ReopenCliAplic(Controle: Integer);
  end;

var
  FmClientes: TFmClientes;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, ModuleGeral, Principal, ClientesObser, ClientesAplic,
  ClientesAplicWeb, MyDBCheck, UnFTPJan, MyListas, MyGlyfs, UnitVersoes_Jan,
  UnitVersoes, UnDmkWeb;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmClientes.Listadecomputadoresparaoaplicselec1Click(Sender: TObject);
begin
  UnVersoes_Jan.MostraFormWUnLockGeren(QrClientesCodigo.Value,
    QrCliAplicAplicativo.Value, True, FmPrincipal.PageControl1,
    FmPrincipal.AdvToolBarPagerNovo);
end;

procedure TFmClientes.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmClientes.MostraClientesAplic(SQLType: TSQLType);
var
  IncluiModulos: Boolean;
begin
  if DBCheck.CriaFm(TFmClientesAplic, FmClientesAplic, afmoNegarComAviso) then
  begin
    if SQLType = stIns then
    begin
      FmClientesAplic.FSQLType  := stIns;
      FmClientesAplic.FCodigo   := QrClientesCodigo.Value;
      FmClientesAplic.FControle := 0;
      FmClientesAplic.FQuery    := QrCliAplic;
    end else
    begin
      FmClientesAplic.FSQLType  := SQLType;
      FmClientesAplic.FCodigo   := QrCliAplicCodigo.Value;
      FmClientesAplic.FControle := QrCliAplicControle.Value;
      FmClientesAplic.FQuery    := QrCliAplic;
    end;
    //
    FmClientesAplic.ShowModal;
    //
    ReopenCliAplic(FmClientesAplic.FControle);
    IncluiModulos := FmClientesAplic.FModulo;
    //
    FmClientesAplic.Destroy;
    //
    if IncluiModulos then
      IncluiModuloAplicativo;
  end;
end;

procedure TFmClientes.MostraClientesAplicWeb(SQLType: TSQLType);
var
  IncluiModulos: Boolean;
begin
  if DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrWebParams, 1) then
  begin
    if DBCheck.CriaFm(TFmClientesAplicWeb, FmClientesAplicWeb, afmoNegarComAviso) then
    begin
      if SQLType = stIns then
      begin
        FmClientesAplicWeb.FSQLType  := stIns;
        FmClientesAplicWeb.FCodigo   := QrClientesCodigo.Value;
        FmClientesAplicWeb.FControle := 0;
        FmClientesAplicWeb.FQuery    := QrCliAplic;
      end else
      begin
        FmClientesAplicWeb.FSQLType  := SQLType;
        FmClientesAplicWeb.FCodigo   := QrCliAplicCodigo.Value;
        FmClientesAplicWeb.FControle := QrCliAplicControle.Value;
        FmClientesAplicWeb.FQuery    := QrCliAplic;
      end;
      //
      FmClientesAplicWeb.ShowModal;
      //
      ReopenCliAplic(FmClientesAplicWeb.FControle);
      //
      FmClientesAplicWeb.Destroy;
    end;
  end;
end;

procedure TFmClientes.MostraClientesObser(SQLType: TSQLType);
var
  Controle: Integer;
begin
  if DBCheck.CriaFm(TFmClientesObser, FmClientesObser, afmoNegarComAviso) then
  begin
    if SQLType = stIns then
    begin
      FmClientesObser.ImgTipo.SQLType         := stIns;
      FmClientesObser.EdCodigo.ValueVariant   := QrClientesCodigo.Value;
      FmClientesObser.EdControle.ValueVariant := 0;
      FmClientesObser.EdNome.ValueVariant     := '';
      FmClientesObser.MeTexto.Text            := '';
      FmClientesObser.CkContinuar.Checked     := True;
      FmClientesObser.CkContinuar.Visible     := True;
      //
      FmClientesObser.FControle := 0;
    end else
    begin
      FmClientesObser.ImgTipo.SQLType         := stUpd;
      FmClientesObser.EdCodigo.ValueVariant   := QrCliObserCodigo.Value;
      FmClientesObser.EdControle.ValueVariant := QrCliObserControle.Value;
      FmClientesObser.EdNome.ValueVariant     := QrCliObserNome.Value;
      FmClientesObser.MeTexto.Text            := QrCliObserTexto.Value;
      FmClientesObser.CkContinuar.Checked     := False;
      FmClientesObser.CkContinuar.Visible     := False;
      //
      FmClientesObser.FControle := QrCliObserControle.Value;
    end;
    FmClientesObser.ShowModal;
    //
    Controle := FmClientesObser.FControle;
    //
    FmClientesObser.Destroy;
  end;
  if Controle <> 0 then
    ReopenCliObser(Controle, '');
end;

procedure TFmClientes.odos1Click(Sender: TObject);
begin
  FCliAtivos := False;
  //
  ImprimeListaClientes(False);
end;

procedure TFmClientes.PageControl1Change(Sender: TObject);
var
  Aba: Integer;
begin
  Aba := PageControl1.ActivePageIndex;
  //
  ConfiguraMensagemAbas(Aba);
end;

procedure TFmClientes.PMAplicPopup(Sender: TObject);
var
  Enab1, Enab2, Enab3: Boolean;
begin
  Enab1 := (QrClientes.State <> dsInactive) and (QrClientes.RecordCount > 0);
  Enab2 := (QrCliAplic.State <> dsInactive) and (QrCliAplic.RecordCount > 0);
  Enab3 := QrCliAplicTipAmb.Value = 0; //Local
  //
  Inclui2.Enabled := Enab1;
  Altera2.Enabled := Enab1 and Enab2;
  Exclui2.Enabled := Enab1 and Enab2;
  //
  Listadecomputadoresparaoaplicselec1.Enabled := Enab1 and Enab2 and Enab3;
  //
  AbrirURLdowebapp1.Visible := False; //Deprecado
end;

procedure TFmClientes.PMClientePopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrClientes.State <> dsInactive) and (QrClientes.RecordCount > 0);
  //
  Altera1.Enabled        := Enab;
  Ativadesativa1.Enabled := Enab;
end;

procedure TFmClientes.PMContratosPopup(Sender: TObject);
var
  Enab1, Enab2: Boolean;
begin
  Enab1 := (QrClientes.State <> dsInactive) and (QrClientes.RecordCount > 0);
  Enab2 := (QrCliContr.State <> dsInactive) and (QrCliContr.RecordCount > 0);
  //
  Incluinovocontrato1.Enabled          := Enab1;
  Gerenciacontratoselecionado1.Enabled := Enab1 and Enab2;
end;

procedure TFmClientes.PMImprimePopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrClientes.State <> dsInactive) and (QrClientes.RecordCount > 0);
  //
  Lista1.Enabled := Enab;
end;

procedure TFmClientes.PMModulosPopup(Sender: TObject);
var
  Enab1, Enab2, Enab3: Boolean;
begin
  Enab1 := (QrClientes.State <> dsInactive) and (QrClientes.RecordCount > 0);
  Enab2 := (QrCliAplic.State <> dsInactive) and (QrCliAplic.RecordCount > 0);
  Enab3 := (QrCliAplicMo.State <> dsInactive) and (QrCliAplicMo.RecordCount > 0);
  //
  if QrCliAplicTipAmb.Value = 2 then //Web
  begin
    Incluimduloaoaplicativoselecionado1.Enabled := False;
    Excluimduloaoaplicativoselecionado1.Enabled := False;
  end else
  begin
    Incluimduloaoaplicativoselecionado1.Enabled := Enab1 and Enab2;
    Excluimduloaoaplicativoselecionado1.Enabled := Enab1 and Enab2 and Enab3;
  end;
end;

procedure TFmClientes.PMObserPopup(Sender: TObject);
var
  Enab1, Enab2: Boolean;
begin
  Enab1 := (QrClientes.State <> dsInactive) and (QrClientes.RecordCount > 0);
  Enab2 := (QrCliObser.State <> dsInactive) and (QrCliObser.RecordCount > 0);
  //
  Inclui4.Enabled := Enab1;
  Altera4.Enabled := Enab1 and Enab2;
  Exclui4.Enabled := Enab1 and Enab2;
end;

procedure TFmClientes.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrClientesCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmClientes.DefParams;
begin
  VAR_GOTOTABELA := 'clientes';
  VAR_GOTOMYSQLTABLE := QrClientes;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT cli.*, ent.Tipo ENTTIPO,');
  VAR_SQLx.Add('IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) ENTRAZ,');
  VAR_SQLx.Add('IF (ent.Tipo=0, ent.Fantasia, ent.Apelido) ENTFAN,');
  VAR_SQLx.Add('IF (ent.Tipo=0, ent.CNPJ, ent.CPF) ENTCNPJ,');
  VAR_SQLx.Add('IF (ent.Tipo=0, ent.IE, ent.RG) ENTIE, ');
  VAR_SQLx.Add('IF (ent.Tipo=0, ent.EUF, ent.PUF) ENTUF ');
  VAR_SQLx.Add('FROM clientes cli');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo = cli.Cliente');
  VAR_SQLx.Add('WHERE cli.Codigo > 0');
  //
  VAR_SQL1.Add('AND cli.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND cli.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND cli.Nome Like :P0');
  //
end;

procedure TFmClientes.EdCli_IdExit(Sender: TObject);
begin
  if EdCli_Id.ValueVariant <> '' then
    EdCli_Id.ValueVariant := FormataCli_Id(EdCli_Id.ValueVariant);
end;

procedure TFmClientes.EdPesquisaChange(Sender: TObject);
var
  Texto: String;
begin
  Texto := EdPesquisa.ValueVariant;
  //
  if (UpperCase(Texto) <> 'PESQUISAR') and (Length(Texto) > 0) then
    ReopenCliObser(0, Texto)
  else
    ReopenCliObser(0, '');
end;

procedure TFmClientes.EdPesquisaEnter(Sender: TObject);
begin
  if Length(EdPesquisa.ValueVariant) > 0 then
    EdPesquisa.ValueVariant := '';
end;

procedure TFmClientes.EdPesquisaExit(Sender: TObject);
begin
  if Length(EdPesquisa.ValueVariant) = 0 then
    EdPesquisa.ValueVariant := 'Pesquisar';
end;

procedure TFmClientes.Exclui1Click(Sender: TObject);
begin
  Geral.MB_Aviso('Exclus�o n�o implementada!');
end;

procedure TFmClientes.Exclui2Click(Sender: TObject);
begin
  if (QrCliAplicMo.State <> dsInactive) and (QrCliAplicMo.RecordCount > 0) then
  begin
    Geral.MB_Aviso('Exclus�o abortada!' + sLineBreak +
      'Motivo: Existem m�dulos adicionados para este aplicativo.');
    Exit;
  end;
  //
  DBCheck.ExcluiRegistro(Dmod.QrUpd, QrCliAplic, 'cliaplic',
    ['Controle'], ['Controle'], True, 'Confirma a exclus�o do registro?');
  //
  ReopenCliAplic(0);
end;

procedure TFmClientes.Exclui4Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrClientesCodigo.Value;
  //
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrCliObser, TDBGrid(DBGCliObser),
    'cliobser', ['Controle'], ['Controle'], istPergunta, '');
  LocCod(Codigo, Codigo);
end;

procedure TFmClientes.Excluimduloaoaplicativoselecionado1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrCliAplicMo, TDBGrid(DBGCliAplicMo),
    'cliaplicmo', ['Conta'], ['Conta'], istPergunta, '');
  //
  ReopenCliAplicMo(0);
end;

procedure TFmClientes.ConfiguraMensagemAbas(Aba: Integer);
  procedure MostraAviso(Texto: String);
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, False, Texto);
  end;
begin
  case Aba of
      0: MostraAviso('Ap�s incluir o(s) aplicativo(s) inclua os m�dulos para cada aplicativo!');
    1,2: MostraAviso('Em cima do texto, clique com o bot�o direito do mouse para abrir a lista de op��es!');
    else MostraAviso('...');
  end;
end;

procedure TFmClientes.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmClientes.QueryPrincipalAfterOpen;
begin
end;

procedure TFmClientes.ReopenCliAplic(Controle: Integer);
var
  Codigo: Integer;
begin
  Codigo := QrClientesCodigo.Value;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrCliAplic, Dmod.MyDB, [
    'SELECT cap.*, ',
    'IF(cap.FimData <= "1899-12-30", "", DATE_FORMAT(cap.FimData, "%d/%m/%Y")) FimData_TXT ',
    'FROM cliaplic cap',
    'WHERE cap.Codigo=' + Geral.FF0(Codigo),
    'ORDER BY cap.AplicativoTxt ',
    '']);
  //
  if Controle <> 0 then
    QrCliAplic.Locate('Controle', Controle, []);
end;

procedure TFmClientes.ReopenCliAplicMo(Conta: Integer);
var
  Controle: Integer;
begin
  Controle := QrCliAplicControle.Value;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrCliAplicMo, Dmod.MyDB, [
    'SELECT cam.*',
    'FROM cliaplicmo cam',
    'WHERE Controle=' + Geral.FF0(Controle),
    '']);
  //
  if Conta <> 0 then
    QrCliAplicMo.Locate('Conta', Conta, []);
end;

procedure TFmClientes.ReopenCliContr(Contrato: Integer);
var
  Contratante, Contratada: Integer;
begin
  Contratante := QrClientesCliente.Value;
  Contratada  := DModG.QrFiliLogCodigo.Value;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrCliContr, Dmod.MyDB, [
    'SELECT con.Codigo, con.Versao, con.Nome, con.ddMesVcto, con.ValorMes, ',
    'IF(DContrato < "1900-01-01", "", DATE_FORMAT(DContrato, "%d/%m/%Y")) DContrato_TXT, ',
    'IF(DtaAssina< "1900-01-01", "", DATE_FORMAT(DtaAssina, "%d/%m/%Y")) DtaAssina_TXT, ',
    'IF(DInstal< "1900-01-01", "", DATE_FORMAT(DInstal, "%d/%m/%Y")) DInstal_TXT, ',
    'IF(DVencimento< "1900-01-01", "", DATE_FORMAT(DVencimento, "%d/%m/%Y")) DVencimento_TXT, ',
    'IF(DtaPrxRenw< "1900-01-01", "", DATE_FORMAT(DtaPrxRenw, "%d/%m/%Y")) DtaPrxRenw_TXT, ',
    'IF(DtaCntrFim< "1900-01-01", "", DATE_FORMAT(DtaCntrFim, "%d/%m/%Y")) DtaCntrFim_TXT ',
    'FROM contratos con ',
    'WHERE con.Contratada=' + Geral.FF0(Contratada),
    'AND con.Contratante='  + Geral.FF0(Contratante),
    '']);
  //
  if Contrato <> 0 then
    QrCliContr.Locate('Codigo', Contrato, []);
end;

procedure TFmClientes.ReopenCliObser(Controle: Integer; Nome: String);
var
  Codigo: Integer;
  SQL: String;
begin
  Codigo := QrClientesCodigo.Value;
  //
  if Length(Nome) > 0 then
    SQL := 'AND Nome LIKE "%' + Nome + '%"'
  else
    SQL := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrCliObser, Dmod.MyDB, [
    'SELECT *',
    'FROM cliobser',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    SQL,
    '']);
  if Controle <> 0 then
    QrCliObser.Locate('Controle', Controle, []);
end;

procedure TFmClientes.DBGCliAplicDblClick(Sender: TObject);
var
  Enab: Boolean;
  FTPCfg, DataBaseCfg: Integer;
  Campo: String;
begin
  Enab := (QrCliAplic.State <> dsInactive) and (QrCliAplic.RecordCount > 0);
  //
  if Enab then
  begin
    Campo := UpperCase(TDBGrid(Sender).Columns[THackDBGrid(Sender).Col -1].FieldName);
    //
    if Campo = 'FTPCFG' then
    begin
      FTPCfg := QrCliAplicFTPCfg.Value;
      //
      if FTPCfg <> 0 then
        UFTPJan.MostraFTPConfig(FTPCfg);
    end
    else if Campo = 'DATABASECFG' then
    begin
      DataBaseCfg := QrCliAplicDataBaseCfg.Value;
      //
      if DataBaseCfg <> 0 then
        FmPrincipal.MostraContasMySQL(DataBaseCfg);
    end
  end;
end;

procedure TFmClientes.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmClientes.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmClientes.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmClientes.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmClientes.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmClientes.SpeedButton5Click(Sender: TObject);
begin
  DmodG.CadastroDeEntidade(EdCliente.ValueVariant, fmcadEntidade2, fmcadEntidade2);
  //
  UMyMod.AbreQuery(QrEntidades, Dmod.MyDB);
  //
  if VAR_CADASTRO > 0 then
    UMyMod.SetaCodigoPesquisado(EdCliente, CBCliente, QrEntidades, VAR_CADASTRO);
end;

procedure TFmClientes.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmClientes.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrClientesCodigo.Value;
  //
  if TFmClientes(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmClientes.Altera2Click(Sender: TObject);
begin
  if QrCliAplicTipAmb.Value = 0 then
    MostraClientesAplic(stUpd)
  else
    MostraClientesAplicWeb(stUpd);
end;

procedure TFmClientes.Altera4Click(Sender: TObject);
begin
  MostraClientesObser(stUpd);
end;

procedure TFmClientes.Ativadesativa1Click(Sender: TObject);
begin
  FmPrincipal.EntidadeAtivaDesativa(QrClientesCliente.Value);
  LocCod(QrClientesCodigo.Value, QrClientesCodigo.Value);
end;

procedure TFmClientes.Ativos1Click(Sender: TObject);
begin
  FCliAtivos := True;
  //
  ImprimeListaClientes(True);
end;

procedure TFmClientes.BtAplicClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //
  MyObjects.MostraPopUpDeBotao(PMAplic, BtAplic);
end;

procedure TFmClientes.BtClienteClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //
  MyObjects.MostraPopUpDeBotao(PMCliente, BtCliente);
end;

procedure TFmClientes.BtConfirmaClick(Sender: TObject);

  function VerificaSeClienteExiste(Codigo: Integer; Cliente, Campo: String): Integer;
  begin
    Result := 0;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(DModG.QrAux, Dmod.MyDB, [
      'SELECT Codigo',
      'FROM clientes',
      'WHERE ' + Campo + '="' + Cliente + '"',
      '']);
    if DModG.QrAux.RecordCount > 0 then
    begin
      if DModG.QrAux.FieldByName('Codigo').AsInteger <> Codigo then
        Result := DModG.QrAux.FieldByName('Codigo').AsInteger;
    end;
  end;

var
  Cliente, Cli_Id: String;
  Codigo, ClienteDup, ClienteStrDup: Integer;
begin
  Cliente := EdCliente.ValueVariant;
  Cli_Id  := FormataCli_Id(EdCli_Id.ValueVariant);
  //
  if MyObjects.FIC(Geral.IMV(Cliente) = 0, EdCliente, 'Cliente n�o definido!') then Exit;
  if MyObjects.FIC(Cli_Id = '', EdCli_Id, 'ID do cliente n�o definido!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('clientes', 'Codigo', '', '', tsPos,
    ImgTipo.SQLType, QrClientesCodigo.Value);
  //
  ClienteDup    := VerificaSeClienteExiste(Codigo, Cliente, 'Cliente');
  ClienteStrDup := VerificaSeClienteExiste(Codigo, Cli_Id, 'Cli_Id');
  //
  if MyObjects.FIC(ClienteDup <> 0, EdCliente, 'Cliente j� cadastrado no ID n�mero '
    + Geral.FF0(ClienteDup) + '!') then Exit;
  //
  if MyObjects.FIC(ClienteStrDup <> 0, EdCli_Id, 'ID do cliente j� cadastrado no ID n�mero '
    + Geral.FF0(ClienteStrDup) + '!') then Exit;
  //
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita,
    'clientes', Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmClientes.BtContratosClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 1;
  //
  MyObjects.MostraPopUpDeBotao(PMContratos, BtContratos);
end;

procedure TFmClientes.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo          := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'clientes', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType, TFmClientes(Self));
end;

procedure TFmClientes.BtEntidadesClick(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrClientes.State <> dsInactive) and (QrClientes.RecordCount > 0);
  //
  if Enab then
  begin
    DModG.CadastroDeEntidade(QrClientesCliente.Value, fmcadEntidade2,
      fmcadEntidade2, False, True, FmPrincipal.PageControl1,
      FmPrincipal.AdvToolBarPagerNovo);
  end;
end;

procedure TFmClientes.BtLocalizaClick(Sender: TObject);
begin
  FindDialog1.Execute;
end;

procedure TFmClientes.BtModulosClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //
  MyObjects.MostraPopUpDeBotao(PMModulos, BtAplic);
end;

procedure TFmClientes.BtObserClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 2;
  //
  MyObjects.MostraPopUpDeBotao(PMObser, BtObser);
end;

procedure TFmClientes.AbrirURLdowebapp1Click(Sender: TObject);
(*
var
  Link, Url, NomeWeb: String;
*)
begin
(* DEPRECADO
  Url     := QrCliAplicURLWebApp.Value;
  NomeWeb := QrCliAplicNomeWeb.Value;
  Link    := Url + '/' + NomeWeb + '.php';
  //
  ShellExecute(Application.Handle, nil, PChar(Link), nil, nil, sw_hide);
*)
end;

procedure TFmClientes.Altera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrClientes, [PnDados],
    [PnEdita], EdCliente, ImgTipo, 'clientes');
end;

procedure TFmClientes.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType    := stLok;
  GBEdita.Align      := alClient;
  PageControl1.Align := alClient;
  FCliAtivos         := False;
  CriaOForm;
  //
  UMyMod.AbreQuery(QrEntidades, Dmod.MyDB);
  //
  ConfiguraMensagemAbas(0);
end;

procedure TFmClientes.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrClientesCodigo.Value, LaRegistro.Caption);
end;

procedure TFmClientes.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmClientes.SbNomeClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := CuringaLoc.CriaForm('cli.Codigo',
    'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome)', 'clientes cli', Dmod.MyDB,
    '', False, 'LEFT JOIN entidades ent ON ent.Codigo=cli.Cliente');
  //
  LocCod(QrClientesCodigo.Value, Codigo);
end;

procedure TFmClientes.SbNovoClick(Sender: TObject);
begin
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmClientes.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmClientes.QrCliAplicAfterScroll(DataSet: TDataSet);
begin
  ReopenCliAplicMo(0);
end;

procedure TFmClientes.QrCliAplicBeforeClose(DataSet: TDataSet);
begin
  QrCliAplicMo.Close;
end;

procedure TFmClientes.QrCliAplicCalcFields(DataSet: TDataSet);
begin
  QrCliAplicSTATAPLIC_TXT.Value := UnVersoes.App_ObtemDescriStatus(QrCliAplicStatAplic.Value);
  QrCliAplicTipAmb_TXT.Value    := UnVersoes.App_ObtemDescriTipAmb(QrCliAplicTipAmb.Value);
end;

procedure TFmClientes.QrClientesAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmClientes.QrClientesAfterScroll(DataSet: TDataSet);
begin
  ReopenCliObser(0, '');
  ReopenCliContr(0);
  ReopenCliAplic(0);
end;

procedure TFmClientes.FindDialog1Find(Sender: TObject);
begin
  MyObjects.LocalizaTextoEmRich(TRichEdit(DBRichEdit2), FindDialog1);
end;

procedure TFmClientes.FormActivate(Sender: TObject);
begin
  if TFmClientes(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end;
end;

function TFmClientes.FormataCli_Id(Cli_Id: String): String;
var
  Txt: String;
begin
  Txt := Geral.SemAcento(Cli_Id);
  Txt := Geral.SoLetra_TT(Txt);
  //
  Result := Txt;
end;

procedure TFmClientes.SbQueryClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := CuringaLoc.CriaForm('cli.Codigo',
    'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome)', 'clientes cli', Dmod.MyDB,
    '', False, 'LEFT JOIN entidades ent ON ent.Codigo=cli.Cliente');
  //
  if Codigo <> 0 then
    LocCod(QrClientesCodigo.Value, Codigo);
end;

procedure TFmClientes.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmClientes.FormShow(Sender: TObject);
begin
{$IfNDef cSkinRank} //Berlin
{$IfNDef cAlphaSkin} //Berlin
  FmMyGlyfs.DefineGlyfs(TForm(Sender));
{$EndIf}
{$EndIf}
{$IfDef cSkinRank} //Berlin
  if FmPrincipal.Sd1.Active then
    FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
{$EndIf}
{$IfDef cAlphaSkin} //Berlin
  if FmPrincipal.sSkinManager1.Active then
    FmMyGlyfs.DefineGlyfsTDI2(FmPrincipal.sSkinManager1, Sender);
{$EndIf}
end;

procedure TFmClientes.frxCAD_CLIEN_001_001GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_TITULOREL' then
  begin
    if FCliAtivos then
      Value := 'LISTA DE CLIENTES ATIVOS'
    else
      Value := 'LISTA DE CLIENTES';
  end;
end;

procedure TFmClientes.Gerenciacontratoselecionado1Click(Sender: TObject);
var
  Contrato: Integer;
begin
  Contrato := QrCliContrCodigo.Value;
  //
  FmPrincipal.CadastroDeContratos(Contrato);
  //
  ReopenCliContr(Contrato);
end;

procedure TFmClientes.ImgTipoChange(Sender: TObject);
begin
  if ImgTipo.SQLType = stLok then
    ConfiguraMensagemAbas(-1)
  else
    ConfiguraMensagemAbas(PageControl1.ActivePageIndex);
end;

procedure TFmClientes.ImprimeListaClientes(Ativo: Boolean);
var
  SQL: String;
begin
  if Ativo then
    SQL := 'WHERE cli.Ativo = 1'
  else
    SQL := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrListaClientes, Dmod.MyDB, [
  'SELECT cli.Codigo, ent.Codigo ID, cli.Ativo, ',
  'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) ENTRAZ, ',
  'IF (ent.Tipo=0, ent.Fantasia, ent.Apelido) ENTFAN ',
  'FROM clientes cli ',
  'LEFT JOIN entidades ent ON ent.Codigo = cli.Cliente ',
  SQL,
  'ORDER BY ENTRAZ ',
  '']);
  //
  MyObjects.frxMostra(frxCAD_CLIEN_001_001, 'Lista de Clientes');
end;

procedure TFmClientes.Inclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrClientes, [PnDados],
    [PnEdita], EdCliente, ImgTipo, 'clientes');
end;

procedure TFmClientes.Inclui2Click(Sender: TObject);
begin
  MostraClientesAplic(stIns);
end;

procedure TFmClientes.Inclui4Click(Sender: TObject);
begin
  MostraClientesObser(stIns);
end;

procedure TFmClientes.IncluiAppWeb1Click(Sender: TObject);
begin
  MostraClientesAplicWeb(stIns);
end;

procedure TFmClientes.Incluimduloaoaplicativoselecionado1Click(Sender: TObject);
begin
  IncluiModuloAplicativo;
end;

procedure TFmClientes.IncluiModuloAplicativo;

  procedure InsereItemAtual(Aplic, TipAmb, Modulo: Integer);
  var
    Qry: TMySQLQuery;
    Codigo, Controle, Conta: Integer;
    ModuloStr, Descri: String;
  begin
    Qry := TMySQLQuery.Create(Dmod);
    try
      Codigo   := QrCliAplicCodigo.Value;
      Controle := QrCliAplicControle.Value;
      Conta    := UMyMod.BuscaNovoCodigo_Int(Dmod.QrUpd, 'cliaplicmo', 'Conta',
                    [], [], stIns, 0, siPositivo, nil);
      //
      if TipAmb = 1 then //Web
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDBn, [
          'SELECT Description, Controller, Module ',
          'FROM api_cab ',
          'WHERE Id=' + Geral.FF0(Modulo),
          '']);
        //
        ModuloStr := Qry.FieldByName('Controller').AsString;
        Descri    := Qry.FieldByName('Module').AsString + ' - ';
        Descri    := Descri + Qry.FieldByName('Description').AsString;
      end else
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
          'SELECT mol.Modulo, mol.Nome ',
          'FROM aplicmodul amo ',
          'LEFT JOIN modulos mol ON mol.Modulo = amo.Modulo ',
          'WHERE amo.Controle=' + Geral.FF0(Modulo),
          '']);
        //
        ModuloStr := Qry.FieldByName('Modulo').AsString;
        Descri    := Qry.FieldByName('Nome').AsString;
      end;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'cliaplicmo', False,
        ['Modulo', 'ModuloStr', 'Descri', 'Codigo', 'Controle'], ['Conta'],
        [Modulo, ModuloStr, Descri, Codigo, Controle], [Conta], True);
    finally
      Qry.Free;
    end;
  end;

const
  Aviso  = '...';
  Titulo = 'Sele��o de vers�o';
  Prompt = 'Informe a vers�o: [F7 para pesquisar]';
  Campo  = 'Descricao';
var
  Cliente, Aplic, TipAmb: Integer;
  Modulo: Variant;
begin
  Cliente := QrClientesCodigo.Value;
  Aplic   := QrCliAplicAplicativo.Value;
  TipAmb  := QrCliAplicTipAmb.Value;
  //
  if TipAmb = 1 then //WEB
  begin
    Modulo := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
      'SELECT cab.Id Codigo, ',
      'CONCAT(cab.Controller, " (", cab.Module, " - ", cab.Description, ")") Descricao ',
      'FROM appwebits its ',
      'LEFT JOIN api_cab cab ON cab.Id=its.Api_Cab ',
      'WHERE its.Codigo=' + Geral.FF0(Aplic),
      ''], Dmod.MyDBn, True);
    //
    if Modulo <> null then
    begin
      if MyObjects.FIC(Modulo = 0, nil, 'M�dulo n�o informado!') then Exit;
      InsereItemAtual(Aplic, TipAmb, Modulo);
      ReopenCliAplicMo(0);
    end;
  end else
  begin
    if DBCheck.EscolheCodigosMultiplos_0('...', 'CAD-CLIEN-001 :: M�dulos do aplicativo',
      'Seleciones os m�dulos desejados:', nil, 'Ativo', 'Nivel1', 'Nome', [
      'DELETE FROM _selcods_; ',
      'INSERT INTO _selcods_ ',
      'SELECT amo.Controle Nivel1, 0 Nivel2, ',
      '0 Nivel3, 0 Nivel4, 0 Nivel5, mol.Nome, 0 Ativo ',
      'FROM ' + TMeuDB + '.aplicmodul amo ',
      'LEFT JOIN ' + TMeuDB + '.modulos mol ON mol.Modulo = amo.Modulo ',
      'WHERE amo.Codigo=' + Geral.FF0(Aplic),
      'AND Controle NOT IN ',
      '( ',
      'SELECT mol.Modulo ',
      'FROM ' + TMeuDB + '.cliaplicmo mol ',
      'LEFT JOIN ' + TMeuDB + '.cliaplic cap ON cap.Controle = mol.Controle ',
      'WHERE mol.Codigo=' + Geral.FF0(Cliente),
      'AND cap.Aplicativo=' + Geral.FF0(Aplic),
      ') ',
      ''],[
      'SELECT * FROM _selcods_; ',
      ''], Dmod.QrUpd) then
    begin
      DModG.QrSelCods.First;
      while not DModG.QrSelCods.Eof do
      begin
        Modulo := DModG.QrSelCodsNivel1.Value;
        //
        if DModG.QrSelCodsAtivo.Value = 1 then
          InsereItemAtual(Aplic, TipAmb, Modulo);
        //
        DModG.QrSelCods.Next;
      end;
      ReopenCliAplicMo(0);
    end;
  end;
end;

procedure TFmClientes.Incluinovocontrato1Click(Sender: TObject);
begin
  FmPrincipal.CadastroDeContratos(0);
  //
  ReopenCliContr(0);
end;

procedure TFmClientes.QrClientesBeforeClose(DataSet: TDataSet);
begin
  QrCliObser.Close;
  QrCliContr.Close;
  QrCliAplic.Close;
end;

procedure TFmClientes.QrClientesBeforeOpen(DataSet: TDataSet);
begin
  QrClientesCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmClientes.QrClientesCalcFields(DataSet: TDataSet);
begin
  QrClientesENTCNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrClientesENTCNPJ.Value);
end;

end.

