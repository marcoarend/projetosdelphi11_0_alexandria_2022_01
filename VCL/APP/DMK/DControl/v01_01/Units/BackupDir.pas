unit BackupDir;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, UnDmkWeb,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, dmkDBGrid, Mask, dmkCheckBox, Menus, dmkCompoStore, dmkPermissoes,
  UnDmkEnums, UnDmkProcFunc;

type
  TFmBackupDir = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtAgendar: TBitBtn;
    LaTitulo1C: TLabel;
    PnAuto: TPanel;
    Panel5: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Label32: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    DBEdit2: TDBEdit;
    DBCheckBox2: TDBCheckBox;
    DBCheckBox3: TDBCheckBox;
    DBEdit4: TDBEdit;
    DBCheckBox4: TDBCheckBox;
    TabSheet2: TTabSheet;
    DBMemo1: TDBMemo;
    DBGrid1: TdmkDBGrid;
    Panel7: TPanel;
    Splitter1: TSplitter;
    Panel8: TPanel;
    ListBox5: TListBox;
    Panel9: TPanel;
    ListBox4: TListBox;
    PBProgress: TProgressBar;
    Panel6: TPanel;
    Memo1: TMemo;
    BtExecutar: TBitBtn;
    GroupBox2: TGroupBox;
    CkDesligar: TdmkCheckBox;
    PMBackIts: TPopupMenu;
    Inclui1: TMenuItem;
    Altera1: TMenuItem;
    Exclui1: TMenuItem;
    PMExecutar: TPopupMenu;
    Selecionados1: TMenuItem;
    Todos1: TMenuItem;
    QrBackupDir: TmySQLQuery;
    QrBackupDirCodigo: TIntegerField;
    QrBackupDirCaminho: TWideStringField;
    QrBackupDirDestino: TWideStringField;
    QrBackupDirFTPWebDir: TIntegerField;
    QrBackupDirSubDir: TSmallintField;
    QrBackupDirZipar: TSmallintField;
    QrBackupDirEncrypt: TSmallintField;
    QrBackupDirSenhaCrypt: TWideStringField;
    QrBackupDirWeb: TSmallintField;
    QrBackupDirExtOmit: TWideMemoField;
    QrBackupDirLk: TIntegerField;
    QrBackupDirDataCad: TDateField;
    QrBackupDirDataAlt: TDateField;
    QrBackupDirUserCad: TIntegerField;
    QrBackupDirUserAlt: TIntegerField;
    QrBackupDirAlterWeb: TSmallintField;
    QrBackupDirAtivo: TSmallintField;
    QrBackupDirOmitirExt: TSmallintField;
    QrBackupDirUltimoIni: TDateTimeField;
    QrBackupDirUltimoFim: TDateTimeField;
    QrBackupDirDESTINO_TXT: TWideStringField;
    DsBackupDir: TDataSource;
    Local1: TMenuItem;
    WEB1: TMenuItem;
    QrBackupDirULTIMOINI_TXT: TWideStringField;
    QrBackupDirULTIMOFIM_TXT: TWideStringField;
    Inclui2: TMenuItem;
    Altera2: TMenuItem;
    Exclui2: TMenuItem;
    Timer1: TTimer;
    CSTabSheetChamou: TdmkCompoStore;
    dmkPermissoes1: TdmkPermissoes;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtAgendarClick(Sender: TObject);
    procedure BtExecutarClick(Sender: TObject);
    procedure PMBackItsPopup(Sender: TObject);
    procedure QrBackupDirAfterScroll(DataSet: TDataSet);
    procedure Inclui1Click(Sender: TObject);
    procedure Altera1Click(Sender: TObject);
    procedure Selecionados1Click(Sender: TObject);
    procedure Todos1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Inclui2Click(Sender: TObject);
    procedure Altera2Click(Sender: TObject);
    procedure Exclui1Click(Sender: TObject);
    procedure Exclui2Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Timer1Timer(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FTodos, FTerminou: Boolean;
    procedure MostraBackupDirIts(SQLType: TSQLType; Web: Integer);
    procedure ExcluiBackupDir(Codigo: Integer);
    procedure ExecBackup(SubDir: Boolean; Caminho, NomeDir, Nome: String);
    procedure MoveResulBackup(Zipar, Web: Boolean; Caminho, NomeDir, Nome,
                Destino: String);
    function  ZipaArquivo(var Caminho: String; const Encryptar: Boolean;
                Senha: String): Boolean;
    function  ObtemVariaveisFTP(const Diretorio: Integer; var FTPConfig: Integer;
                var FTPCaminho: String): Boolean;
    function  UpdDadosDB(Codigo: Integer; Campo: String): Boolean;
  public
    { Public declarations }
    FCodigo: Integer;
    function ExecutaBackup(): Boolean;
    procedure ReopenBackupDir(Codigo: Integer);
  end;

  var
  FmBackupDir: TFmBackupDir;

implementation

uses Module, UnMyObjects, ZForge, BackupDirIts, MyDBCheck, UMySQLModule,
  MyGlyfs, Principal, ModuleGeral;

{$R *.DFM}

procedure TFmBackupDir.Altera1Click(Sender: TObject);
begin
  if(QrBackupDir.State <> dsInactive) and (QrBackupDir.RecordCount > 0) then
    MostraBackupDirIts(stUpd, 0);
end;

procedure TFmBackupDir.Altera2Click(Sender: TObject);
begin
  if(QrBackupDir.State <> dsInactive) and (QrBackupDir.RecordCount > 0) then
    MostraBackupDirIts(stUpd, 1);
end;

procedure TFmBackupDir.BtAgendarClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMBackIts, BtAgendar);
end;

procedure TFmBackupDir.BtExecutarClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMExecutar, BtExecutar);
end;

procedure TFmBackupDir.BtSaidaClick(Sender: TObject);
begin
  if TFmBackupDir(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmBackupDir.ExcluiBackupDir(Codigo: Integer);
begin
  if(QrBackupDir.State = dsInactive) and (QrBackupDir.RecordCount = 0) then Exit;
  //
  if Geral.MensagemBox('Confirma a exclus�o do item selecionado?',
  'Pergunta de exclus�o', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    try
      Screen.Cursor := crHourGlass;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM backupdir WHERE Codigo=:P0');
      Dmod.QrUpd.Params[0].AsInteger := Codigo;
      Dmod.QrUpd.ExecSQL;
      //
      ReopenBackupDir(0);
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmBackupDir.Exclui1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrBackupDirCodigo.Value;
  //
  ExcluiBackupDir(Codigo);
end;

procedure TFmBackupDir.Exclui2Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrBackupDirCodigo.Value;
  //
  ExcluiBackupDir(Codigo);
end;

procedure TFmBackupDir.ExecBackup(SubDir: Boolean; Caminho, NomeDir, Nome: String);
var
  i, j, p: Integer;
  Eh: Boolean;
  Comp1, Comp2, Cam: String;
begin
  //Carrega
  ListBox5.Clear;
  dmkPF.GetAllFiles(SubDir, Caminho + '\*.*', ListBox5, True);

  //Executa o backup
  Screen.Cursor     := crHourGlass;
  PBProgress.Visible  := True;
  PBProgress.Position := 0;
  PBProgress.Max      := ListBox5.Items.Count;
  PBProgress.Visible  := True;
  for i := 0 to ListBox5.Items.Count -1 do
  begin
    PBProgress.Position := PBProgress.Position + 1;
    PBProgress.Update;
    Application.ProcessMessages;
    Eh := False;
    for j := 0 to DBMemo1.Lines.Count - 1 do
    begin
      p := pos('*', DBMemo1.Lines[j]);
      if p > 0 then
      begin
        Comp1 := Copy(ExtractFileExt(ListBox5.Items[i]), 1, p-1);
        Comp2 := Copy(DBMemo1.Lines[j], 1, p-1);
      end else begin
        Comp1 := ExtractFileExt(ListBox5.Items[i]);
        Comp2 := DBMemo1.Lines[j];
      end;
      if Uppercase(Comp1) = Uppercase(Comp2) then
      begin
        Eh := True;
        Break;
      end;
    end;
    if Eh then
      ListBox4.Items.Add(ListBox5.Items[i]+' n�o foi copiado.')
    else begin
      Cam := ListBox5.Items[i];
      Insert('\' + NomeDir + Nome, Cam, Length(Caminho)+1);
      ForceDirectories(ExtractFilePath(Cam));
      if CopyFile(PChar(ListBox5.Items[i]), PChar(Cam), False) then
        ListBox4.Items.Add(Cam+' foi criado.')
      else ListBox4.Items.Add(Cam+' n�o pode ser criado.');
    end;
    ListBox4.Update;
    Application.ProcessMessages;
  end;
  PBProgress.Visible := False;
  Screen.Cursor      := crDefault;
end;

function TFmBackupDir.ZipaArquivo(var Caminho: String; const Encryptar: Boolean;
  Senha: String): Boolean;
var
  CamZip: String;
begin
  CamZip := '';
  //
  if DBCheck.CriaFm(TFmZForge, FmZForge, afmoNegarComAviso) then
  begin
    FmZForge.Show;
    CamZip := FmZForge.ZipaArquivo(zftDiretorio, ExtractFileDir(Caminho), Caminho,
      Geral.SemAcento(ExtractFileName(Caminho)), Senha, Encryptar, False);
    FmZForge.Destroy;
  end;
  if Length(CamZip) > 0 then
  begin
    Caminho := CamZip;
    Result  := True;
  end
  else
    Result := False;
end;

procedure TFmBackupDir.MoveResulBackup(Zipar, Web: Boolean; Caminho, NomeDir, Nome,
  Destino: String);
var
  CaminhoZip: String;
begin
  //Move o arquivo resultante
  if Zipar then
  begin
    if dmkPF.DelDir(Caminho + '\' + NomeDir + Nome) then
    begin
      Memo1.Text := 'Aviso: Diret�rio tempor�rio removido com sucesso!' +
        #13#10 + Memo1.Text;
    end else
    begin
      Memo1.Text := 'Erro: Falha ao remover o diret�rio tempor�rio!' + #13#10
        + Memo1.Text;
    end;
    CaminhoZip := Caminho + '\' + NomeDir + Nome + '.zip';
  end else
    CaminhoZip := Caminho + '\' + NomeDir + Nome;
  //
  if not Web then
  begin
    if dmkPF.MoveDir(CaminhoZip, Destino + '\') = True then
    begin
      Memo1.Text := 'Aviso: Diret�rio movido com sucesso!' + #13#10
        + Memo1.Text;
    end else
    begin
      Memo1.Text := 'Erro: Falha ao mover diret�rio!' + #13#10 + Memo1.Text;
    end;
  end;
end;

function TFmBackupDir.ObtemVariaveisFTP(const Diretorio: Integer; var FTPConfig: Integer;
  var FTPCaminho: String): Boolean;
begin
  Result := False;
  if DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrWebParams, 1) then
  begin
    Dmod.QrUpdN.Close;
    Dmod.QrUpdN.SQL.Clear;
    Dmod.QrUpdN.SQL.Add('SELECT FTPConfig, Caminho');
    Dmod.QrUpdN.SQL.Add('FROM ftpwebdir');
    Dmod.QrUpdN.SQL.Add('WHERE Codigo=:P0');
    Dmod.QrUpdN.Params[0].AsInteger := Diretorio;
    Dmod.QrUpdN.Open;
    if Dmod.QrUpdN.RecordCount > 0 then
    begin
      FTPConfig  := Dmod.QrUpdN.FieldByName('FTPConfig').Value;
      FTPCaminho := Dmod.QrUpdN.FieldByName('Caminho').Value;
      Dmod.QrUpdN.Close;

      Memo1.Text := 'Upload conclu�do com sucesso!' + #13#10 + Memo1.Text;

      Result := True;
    end else
    begin
      Geral.MensagemBox('N�o foi poss�vel definir os dados para conex�o FTP!',
        'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
  end;
end;

function TFmBackupDir.UpdDadosDB(Codigo: Integer; Campo: String): Boolean;
var
  Agora: String;
begin
  Result := False;
  Agora  := Geral.FDT(Now, 9);

  if UMyMod.SQLInsUpd_Old(Dmod.QrUpd, CO_ALTERACAO, 'backupdir', False,
  [
    Campo
  ], ['Codigo'],
  [
    Agora
  ], [Codigo]) then
  begin
    ReopenBackupDir(Codigo);
    //
    Result := True;
  end else
    Geral.MensagemBox('Falha ao atualizar dados no banco de dados!', 'Aviso',
      MB_OK+MB_ICONWARNING);
end;

function TFmBackupDir.ExecutaBackup: Boolean;
var
  Caminho, Destino, NomeDir, Nome, SenhaCrypt, FTPCaminho, ArqNome, FileName: String;
  SubDir, Zipar, Web, Encryptar: Boolean;
  FTPWebDir, FTPConfig: Integer;
begin
  Memo1.Text := '';

  if not UpdDadosDB(QrBackupDirCodigo.Value, 'UltimoIni') then
  begin
    Result := False;
    Exit;
  end;

  Caminho    := QrBackupDirCaminho.Value;
  Destino    := QrBackupDirDestino.Value;
  SubDir     := MLAGeral.IntToBool(QrBackupDirSubDir.Value);
  Web        := MLAGeral.IntToBool(QrBackupDirWeb.Value);
  NomeDir    := ExtractFileName(Caminho);
  Nome       := '_' + FormatDateTime('yymmdd_hhnnss', Now);
  Encryptar  := MLAGeral.IntToBool(QrBackupDirEncrypt.Value);
  SenhaCrypt := QrBackupDirSenhaCrypt.Value;
  Zipar      := MLAGeral.IntToBool(QrBackupDirZipar.Value);
  FTPWebDir  := QrBackupDirFTPWebDir.Value;

  if MyObjects.FIC(not DirectoryExists(Caminho), nil, 'Backup cancelado! Diret�rio origem n�o localizado!') then
  begin
    Result := False;
    Exit;
  end;

  if not Web then
  begin
    if not DirectoryExists(Destino) then
    begin
      if not ForceDirectories(Destino) then
      begin
        Geral.MensagemBox('Backup cancelado! Diret�rio destino n�o existe e n�o pode ser criado!',
          'Aviso', MB_OK+MB_ICONWARNING);
        Result := False;
        Exit;
      end;
    end;
  end else
  begin
    if DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrWebParams, 1) then
    begin
      Result := False;
      Exit;
    end;
  end;

  ExecBackup(SubDir, Caminho, NomeDir, Nome);

  if QrBackupDirZipar.Value = 1 then
  begin
    FileName := Caminho + '\' + NomeDir + Nome;
    if not ZipaArquivo(FileName, Encryptar, SenhaCrypt) then
    begin
      Geral.MensagemBox('Falha ao zipar arquivo!', 'Aviso', MB_OK+MB_ICONWARNING);
      Result := False;
      Exit;
    end;
  end;

  MoveResulBackup(Zipar, Web, Caminho, NomeDir, Nome, Destino);
  
  if Web then
  begin
    if DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrWebParams, 1) then
    begin
      if not ObtemVariaveisFTP(FTPWebDir, FTPConfig, FTPCaminho) then
      begin
        Result := False;
        Exit;
      end;
      //Faz upload do arquivo
      if Length(ExtractFileExt(FileName)) > 0 then
      begin
        Geral.MB_Aviso('Upload n�o implementado');
        Result := False;
        Exit;
        {Fazer FTP

        if not DmodFTP.FTPUpload(FTPConfig, FTPCaminho, FileName, False, ArqNome) then
        begin
          Geral.MensagemBox('Upload FTP falhou!', 'Aviso', MB_OK+MB_ICONWARNING);
          Result := False;
          Exit;
        end;

        }
      end else
      begin
        Geral.MensagemBox('Apenas arquivos s�o suportados para upload FTP!',
          'Aviso', MB_OK+MB_ICONWARNING);
        Result := False;
        Exit;
      end;
    end;
  end;

  if not UpdDadosDB(QrBackupDirCodigo.Value, 'UltimoFim') then
  begin
    Result := False;
    Exit;
  end;

  Result := True;
end;

procedure TFmBackupDir.FormActivate(Sender: TObject);
begin
  if TFmBackupDir(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end;
end;

procedure TFmBackupDir.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if not FTerminou then
  begin
    if Geral.MensagemBox('Existe um backup em execu��o no momento!' + #13#10 +
      'Deseja cancel�-lo?', 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES
    then
      Abort;
  end;
end;

procedure TFmBackupDir.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  ReopenBackupDir(0);
  PageControl1.ActivePageIndex := 0;
  FTerminou                    := True;
end;

procedure TFmBackupDir.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmBackupDir.FormShow(Sender: TObject);
begin
{$IfNDef cSkinRank} //Berlin
{$IfNDef cAlphaSkin} //Berlin
  FmMyGlyfs.DefineGlyfs(TForm(Sender));
{$EndIf}
{$EndIf}
{$IfDef cSkinRank} //Berlin
  if FmPrincipal.Sd1.Active then
    FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
{$EndIf}
{$IfDef cAlphaSkin} //Berlin
  if FmPrincipal.sSkinManager1.Active then
    FmMyGlyfs.DefineGlyfsTDI2(FmPrincipal.sSkinManager1, Sender);
{$EndIf}
end;

procedure TFmBackupDir.Inclui1Click(Sender: TObject);
begin
  MostraBackupDirIts(stIns, 0);
end;

procedure TFmBackupDir.Inclui2Click(Sender: TObject);
begin
  MostraBackupDirIts(stIns, 1);
end;

procedure TFmBackupDir.MostraBackupDirIts(SQLType: TSQLType; Web: Integer);
  procedure MostraBkpDirIts;
  begin
    if DBCheck.CriaFm(TFmBackupDirIts, FmBackupDirIts, afmoNegarComAviso) then
    begin
      FmBackupDirIts.ImgTipo.SQLType := SQLType;
      FmBackupDirIts.FWeb            := Web;
      FmBackupDirIts.FCodigo         := QrBackupDirCodigo.Value;
      FmBackupDirIts.FCaminho        := QrBackupDirCaminho.Value;
      FmBackupDirIts.FSubDir         := QrBackupDirSubDir.Value;
      FmBackupDirIts.FZipar          := QrBackupDirZipar.Value;
      FmBackupDirIts.FEncrypt        := QrBackupDirEncrypt.Value;
      FmBackupDirIts.FSenhaCrypt     := QrBackupDirSenhaCrypt.Value;
      FmBackupDirIts.FDirDestino     := QrBackupDirDestino.Value;
      FmBackupDirIts.FFTPWebDir      := QrBackupDirFTPWebDir.Value;
      FmBackupDirIts.ShowModal;
      FmBackupDirIts.Destroy;
      //
      if SQLType = stUpd then
        ReopenBackupDir(QrBackupDirCodigo.Value)
      else
        ReopenBackupDir(0);
    end;
  end;
begin
  if Web = 1 then
  begin
    if DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrWebParams, 1) then
      MostraBkpDirIts;
  end else
    MostraBkpDirIts;
end;

procedure TFmBackupDir.PMBackItsPopup(Sender: TObject);
var
  Enab, Web: Boolean;
begin
  Enab := (QrBackupDir.State <> dsInactive) and (QrBackupDir.RecordCount > 0); 
  //
  if Enab then
  begin
    Web := QrBackupDirWeb.Value = 1;
    if Web then
    begin
      //Local
      Altera1.Enabled := False;
      Exclui1.Enabled := False;
      //Web
      Altera2.Enabled := True;
      Exclui2.Enabled := True;
    end else
    begin
      //Local
      Altera1.Enabled := True;
      Exclui1.Enabled := True;
      //Web
      Altera2.Enabled := False;
      Exclui2.Enabled := False;
    end;
  end else
  begin
    //Local
    Altera1.Enabled := Enab;
    Exclui1.Enabled := Enab;
    //Web
    Altera2.Enabled := Enab;
    Exclui2.Enabled := Enab;
  end;
end;

procedure TFmBackupDir.QrBackupDirAfterScroll(DataSet: TDataSet);
begin
  if QrBackupDirOmitirExt.Value = 1 then
    TabSheet2.TabVisible := True
  else
    TabSheet2.TabVisible := False;
end;

procedure TFmBackupDir.ReopenBackupDir(Codigo: Integer);
begin
  QrBackupDir.Close;
  QrBackupDir.Open;
  if Codigo > 0 then  
    QrBackupDir.Locate('Codigo', Codigo, []);
end;

procedure TFmBackupDir.Selecionados1Click(Sender: TObject);
begin
  FTodos         := False;
  Timer1.Enabled := True;
end;

procedure TFmBackupDir.Timer1Timer(Sender: TObject);
  procedure ExecutaSel;
  var
    i: Integer;
    Finalizou: Boolean;
  begin
    Finalizou := False;
    if DBGrid1.SelectedRows.Count > 1 then
    begin
      with DBGrid1.DataSource.DataSet do
      for i:= 0 to DBGrid1.SelectedRows.Count-1 do
      begin
        GotoBookmark(DBGrid1.SelectedRows.Items[i]);
        try
          if QrBackupDirWeb.Value = 1 then
          begin
            if DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrWebParams, 1) then
              Finalizou := ExecutaBackup;
          end else
            Finalizou := ExecutaBackup;
        except
          Finalizou := False;
        end;
      end;
    end else
      Finalizou := ExecutaBackup();
    //
    if CkDesligar.Checked then
      MLAGeral.DesligarMeuWindows(EWX_SHUTDOWN or EWX_FORCE)
    else begin
      if Finalizou then
        Geral.MensagemBox('Backup finalizado com sucesso!', 'Aviso', MB_OK+MB_ICONWARNING);
    end;
  end;
  procedure ExecutaAll;
  var
    Finalizou: Boolean;
  begin
    Finalizou := False;
    QrBackupDir.First;
    while not QrBackupDir.Eof do
    begin
      try
        if QrBackupDirWeb.Value = 1 then
        begin
          if DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrWebParams, 1) then
            Finalizou := ExecutaBackup;
        end else
          Finalizou := ExecutaBackup;
      except
        Finalizou := False;
      end;
      //
      QrBackupDir.Next;
    end;
    if CkDesligar.Checked then
      MLAGeral.DesligarMeuWindows(EWX_SHUTDOWN or EWX_FORCE)
    else begin
      if Finalizou then
        Geral.MensagemBox('Backup finalizado com sucesso!', 'Aviso', MB_OK+MB_ICONWARNING);
    end;
  end;
begin
  try
    Screen.Cursor      := crHourGlass;
    BtExecutar.Enabled := False;
    Timer1.Enabled     := False;
    FTerminou          := False;
    //
    if FTodos then //Todos
      ExecutaAll
    else //Selecionados
      ExecutaSel;
  finally
    BtExecutar.Enabled := True;
    FTerminou          := True;
    Screen.Cursor      := crDefault;
  end;
end;

procedure TFmBackupDir.Todos1Click(Sender: TObject);
begin
  FTodos         := True;
  Timer1.Enabled := True;
end;

end.
