unit OpcoesDControl;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, dmkEdit, UnDmkEnums, DmkDAC_PF;

type
  TFmOpcoesDControl = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Label1: TLabel;
    SpeedButton5: TSpeedButton;
    GroupBox3: TGroupBox;
    Label96: TLabel;
    Label97: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    EdApliDiaAvi: TdmkEdit;
    EdApliDiaPro: TdmkEdit;
    EdCaminhoGlyfs: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure MostraEdicao;
  public
    { Public declarations }
  end;

  var
  FmOpcoesDControl: TFmOpcoesDControl;

implementation

uses UnMyObjects, Module, UMySQLModule, ModuleGeral;

{$R *.DFM}

procedure TFmOpcoesDControl.BtOKClick(Sender: TObject);
begin
  if UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    'UPDATE opcoesdcon SET ',
    'ApliDiaAvi=' + Geral.FF0(EdApliDiaAvi.ValueVariant) + ', ',
    'ApliDiaPro=' + Geral.FF0(EdApliDiaPro.ValueVariant) + ', ',
    'CaminhoGlyfs="' + EdCaminhoGlyfs.ValueVariant + '" ',
    '']) then
  begin
    Dmod.ReopenOpcoesDCon;
    DmodG.ReopenOpcoesGerl;
    //
    Close;
  end;
end;

procedure TFmOpcoesDControl.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOpcoesDControl.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOpcoesDControl.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  MostraEdicao;
end;

procedure TFmOpcoesDControl.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOpcoesDControl.MostraEdicao;
begin
  PageControl1.ActivePageIndex := 0;
  //
  EdApliDiaAvi.ValueVariant   := Dmod.QrOpcoesDCon.FieldByName('ApliDiaAvi').AsInteger;
  EdApliDiaPro.ValueVariant   := Dmod.QrOpcoesDCon.FieldByName('ApliDiaPro').AsInteger;
  EdCaminhoGlyfs.ValueVariant := Dmod.QrOpcoesDCon.FieldByName('CaminhoGlyfs').AsString;
end;

procedure TFmOpcoesDControl.SpeedButton5Click(Sender: TObject);
begin
  MyObjects.DefineDiretorio(FmOpcoesDControl, EdCaminhoGlyfs);
end;

end.
