unit Modulos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, dmkDBGrid, Mask, dmkDBEdit, UMySQLModule, dmkCheckBox, frxClass,
  frxDBSet, Menus, UnDmkEnums;

type
  TFmModulos = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtInclui: TBitBtn;
    LaTitulo1C: TLabel;
    PnEdita: TPanel;
    Label5: TLabel;
    EdModulo: TdmkEdit;
    Label1: TLabel;
    EdNome: TdmkEdit;
    Panel9: TPanel;
    BitBtn4: TBitBtn;
    Panel10: TPanel;
    BitBtn5: TBitBtn;
    QrModulos: TmySQLQuery;
    DsModulos: TDataSource;
    QrModulosModulo: TWideStringField;
    QrModulosNome: TWideStringField;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    QrModulosAtivo: TSmallintField;
    CkAtivo: TdmkCheckBox;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    QrModulosCodigo: TIntegerField;
    frxCAD_MODUL_001_1: TfrxReport;
    SbImprime: TBitBtn;
    QrModAplic: TmySQLQuery;
    StringField1: TWideStringField;
    StringField2: TWideStringField;
    SmallintField1: TSmallintField;
    IntegerField1: TIntegerField;
    QrModAplicAplicativo: TIntegerField;
    QrModAplicNomeAplic: TWideStringField;
    frxDsModAplic: TfrxDBDataset;
    PMImprime: TPopupMenu;
    Listadaaplicativospormdulo1: TMenuItem;
    Listadeclientespormdulo1: TMenuItem;
    frxCAD_MODUL_001_2: TfrxReport;
    QrModCli: TmySQLQuery;
    StringField3: TWideStringField;
    StringField4: TWideStringField;
    SmallintField2: TSmallintField;
    IntegerField2: TIntegerField;
    frxDsModCli: TfrxDBDataset;
    QrModCliCliente: TIntegerField;
    QrModCliNomeCliente: TWideStringField;
    QrModulosObserv: TWideMemoField;
    Label2: TLabel;
    MeObserv: TMemo;
    PnDados: TPanel;
    Panel6: TPanel;
    StaticText4: TStaticText;
    DBMemo1: TDBMemo;
    dmkDBGrid2: TdmkDBGrid;
    DBCGLicencaTip: TdmkDBCheckGroup;
    QrModulosLicencaTip: TIntegerField;
    CGLicencaTip: TdmkCheckGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure Listadaaplicativospormdulo1Click(Sender: TObject);
    procedure frxCAD_MODUL_001_1GetValue(const VarName: string;
      var Value: Variant);
    procedure Listadeclientespormdulo1Click(Sender: TObject);
    procedure frxCAD_MODUL_001_2GetValue(const VarName: string;
      var Value: Variant);
  private
    { Private declarations }
    procedure MostraEdicao(Mostra: Boolean; Tipo: TSQLType);
  public
    { Public declarations }
  end;

  var
  FmModulos: TFmModulos;

implementation

uses UnMyObjects, Module, UnitVersoes;

{$R *.DFM}

procedure TFmModulos.BitBtn4Click(Sender: TObject);

  function VerificaModulo(Modulo: String): Boolean;
  begin
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT Modulo FROM modulos WHERE Modulo=:P0');
    Dmod.QrAux.Params[0].AsString := Modulo;
    Dmod.QrAux.Open;
    if (Dmod.QrAux.RecordCount > 0) and
      (Dmod.QrAux.FieldByName('Modulo').AsString <> Modulo)
    then
      Result := True
    else
      Result := False;
  end;

var
  Modulo, Nome, Observ: String;
  Codigo, LicencaTip, Ativo: Integer;
begin
  Modulo     := EdModulo.ValueVariant;
  Nome       := EdNome.ValueVariant;
  Observ     := MeObserv.Text;
  LicencaTip := CGLicencaTip.Value;
  Ativo      := Geral.BoolToInt(CkAtivo.Checked);
  //
  if ImgTipo.SQLType = stIns then
    Codigo := UMyMod.BuscaNovoCodigo_Int(Dmod.QrUpd, 'modulos', 'Codigo', [],
      [], stIns, 0, siPositivo, nil)
  else
    Codigo := QrModulosCodigo.Value;
  //
  if MyObjects.FIC(Modulo = '', EdModulo, 'Defina o m�dulo!') then Exit;
  if MyObjects.FIC(Nome = '', EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(VerificaModulo(Modulo), EdModulo, 'Este m�dulo j� foi cadastrado!') then Exit;
  if MyObjects.FIC(LicencaTip = 0, CGLicencaTip, 'Voc� deve informar pelo menos um tipo de licen�a!') then Exit;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'modulos', False,
    ['Nome', 'Observ', 'Modulo', 'LicencaTip', 'Ativo'], ['Codigo'],
    [Nome, Observ, Modulo, LicencaTip, Ativo], [Codigo], True)
  then begin
    MostraEdicao(False, stLok);
    UMyMod.AbreQuery(QrModulos, Dmod.MyDB);
    //
    QrModulos.Locate('Codigo', Codigo, []);
  end;     
end;

procedure TFmModulos.BitBtn5Click(Sender: TObject);
begin
  MostraEdicao(False, stLok);
end;

procedure TFmModulos.BtAlteraClick(Sender: TObject);
begin
  if (QrModulos.State <> dsInactive) and (QrModulos.RecordCount > 0) then
    MostraEdicao(True, stUpd);
end;

procedure TFmModulos.BtExcluiClick(Sender: TObject);
begin
  if (QrModulos.State = dsInactive) or (QrModulos.RecordCount = 0) then Exit;
  //
  if UmyMod.ExcluiRegistroTxt1('Confirma a exclus�o do m�dulo ' +
    QrModulosModulo.Value + '?', 'modulos', 'Modulo', QrModulosModulo.Value,
    '', Dmod.MyDB) > 0
  then
    UMyMod.AbreQuery(QrModulos, Dmod.MyDB);
end;

procedure TFmModulos.BtIncluiClick(Sender: TObject);
begin
  MostraEdicao(True, stIns);
end;

procedure TFmModulos.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmModulos.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmModulos.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  DBCGLicencaTip.Items := UnVersoes.App_ObtemLicencasTipos;
  CGLicencaTip.Items   := UnVersoes.App_ObtemLicencasTipos;
  //
  UMyMod.AbreQuery(QrModulos, Dmod.MyDB);
  MostraEdicao(False, stLok);
end;

procedure TFmModulos.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmModulos.frxCAD_MODUL_001_1GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_TITULOREL' then
    Value := 'Lista da aplicativos por m�dulo'
  else if VarName = 'VARF_CODI_FRX' then
    Value := 'CAD-MODUL-001-1';
end;

procedure TFmModulos.frxCAD_MODUL_001_2GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_TITULOREL' then
    Value := 'Lista da aplicativos por m�dulo'
  else if VarName = 'VARF_CODI_FRX' then
    Value := 'CAD-MODUL-001-2';
end;

procedure TFmModulos.Listadaaplicativospormdulo1Click(Sender: TObject);
begin
  MyObjects.frxMostra(frxCAD_MODUL_001_1, 'Lista da aplicativos por m�dulo');
end;

procedure TFmModulos.Listadeclientespormdulo1Click(Sender: TObject);
begin
  MyObjects.frxMostra(frxCAD_MODUL_001_2, 'Lista de clientes por m�dulo');
end;

procedure TFmModulos.MostraEdicao(Mostra: Boolean; Tipo: TSQLType);
begin
  if Mostra then
  begin
    PnDados.Visible   := False;
    PnEdita.Visible   := True;
    GBAvisos1.Visible := False;
    GBRodaPe.Visible  := False;
    PnEdita.Align     := alClient;
    ImgTipo.SQLType   := Tipo;
    //
    if Tipo = stIns then
    begin
      EdCodigo.ValueVariant := 0;
      EdModulo.ValueVariant := '';
      EdNome.ValueVariant   := '';
      MeObserv.Text         := '';
      CGLicencaTip.Value    := 0;
      CkAtivo.Checked       := True;
    end else
    begin
      EdCodigo.ValueVariant := QrModulosCodigo.Value;
      EdModulo.ValueVariant := QrModulosModulo.Value;
      EdNome.ValueVariant   := QrModulosNome.Value;
      MeObserv.Text         := QrModulosObserv.Value;
      CGLicencaTip.Value    := QrModulosLicencaTip.Value;
      CkAtivo.Checked       := Geral.IntToBool(QrModulosAtivo.Value);
    end;
    EdModulo.SetFocus;
  end else
  begin
    PnDados.Visible   := True;
    PnEdita.Visible   := False;
    GBRodaPe.Visible  := True;
    GBAvisos1.Visible := True;
    PnEdita.Align     := alTop;
  end;
end;

procedure TFmModulos.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

end.
