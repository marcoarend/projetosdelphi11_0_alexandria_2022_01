unit Principal;

interface

uses
  Windows, Messages, System.SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  UnMLAGeral, UMySQLModule, ExtCtrls, Menus, Grids, DBGrids, Db, (*DBTables,*)
  TypInfo, StdCtrls, ZCF2, ToolWin, ComCtrls, UnInternalConsts, ExtDlgs, jpeg,
  mySQLDbTables, Buttons, WinSkinData, WinSkinStore, Mask, DBCtrls, ImgList,
  contnrs, frxClass, frxDBSet, dmkGeral, Variants, MyDBCheck, dmkDBGrid, dmkEdit,
  OleCtrls, SHDocVw, (*dmkPopOutFntCBox,*) ValEdit, AdvToolBar, AdvShapeButton,
  AdvGlowButton, AdvToolBarStylers, AdvOfficeHint, UnDmkEnums, AdvPreviewMenu,
  AdvMenus, DMKpnfsConversao, DmkDAC_PF, Vcl.Imaging.pngimage, dmkPageControl,
  UnDmkProcFunc, frxDock, Vcl.Tabs, dmkDockTabSet, UnitNotificacoes,
  UnitNotificacoesEdit, MIDASLIB;

const
  TMaxArtes = 127;

type
  TcpCalc = (cpJurosMes, cpMulta);
  //
  TLetraBmp = record
    Topo, Esqu, Base, Dire: Integer;
    Imagem: TImage;
  end;
  TFonteBMP = record
    Nome: ShortString;  // NomeFonte + NomeArte
    Letras: array[1..TMaxArtes] of TLetraBmp;
  end;
  TArteBMP = record
    Nome: ShortString;  // NomeFonte + NomeArte
    Fontes: array[1..TMaxArtes] of TFonteBMP;
  end;
  TArtesBMP = array[1..TMaxArtes] of TArteBMP;
  //
  TFmPrincipal = class(TForm)
    Timer1: TTimer;
    PageControl1: TdmkPageControl;
    StatusBar: TStatusBar;
    SkinStore1: TSkinStore;
    sd1: TSkinData;
    AdvPreviewMenu1: TAdvPreviewMenu;
    AdvOfficeHint1: TAdvOfficeHint;
    AdvToolBarOfficeStyler1: TAdvToolBarOfficeStyler;
    PMGeral: TPopupMenu;
    N1: TMenuItem;
    Reabrirtabelas1: TMenuItem;
    Memo3: TMemo;
    AdvPMImagem: TAdvPopupMenu;
    MenuItem1: TMenuItem;
    Limpar1: TMenuItem;
    AdvPMMenuCor: TAdvPopupMenu;
    Padro3: TMenuItem;
    Office20071: TMenuItem;
    Dermatek1: TMenuItem;
    Preto1: TMenuItem;
    Azul1: TMenuItem;
    Cinza1: TMenuItem;
    Verde1: TMenuItem;
    Prscia1: TMenuItem;
    WhidbeyStyle1: TMenuItem;
    WindowsXP1: TMenuItem;
    APMExtratos: TAdvPopupMenu;
    PagarReceber1: TMenuItem;
    Movimento1: TMenuItem;
    ResultadosMensais1: TMenuItem;
    ReceitaseDespesas1: TMenuItem;
    Prestaodecontas1: TMenuItem;
    APMPesquisas: TAdvPopupMenu;
    Emqualquerconta1: TMenuItem;
    Contascontroladas1: TMenuItem;
    Contassazonais1: TMenuItem;
    AdvPMCadastrosOld: TAdvPopupMenu;
    Perfis2: TMenuItem;
    AdvPMVerifiDB: TAdvPopupMenu;
    MenuItem20: TMenuItem;
    MenuItem21: TMenuItem;
    Suporte1: TMenuItem;
    N3: TMenuItem;
    VerificaTabelasPblicas1: TMenuItem;
    AdvToolBarPager1: TAdvToolBarPager;
    AdvToolBarPager21: TAdvPage;
    AdvToolBarPager22: TAdvPage;
    AdvToolBarPager23: TAdvPage;
    AdvQuickAccessToolBar2: TAdvQuickAccessToolBar;
    AdvToolBarButton7: TAdvToolBarButton;
    AdvToolBarButton8: TAdvToolBarButton;
    AdvToolBarButton9: TAdvToolBarButton;
    AdvToolBarButton10: TAdvToolBarButton;
    AdvToolBarButton11: TAdvToolBarButton;
    AdvToolBarButton12: TAdvToolBarButton;
    AdvToolBarButton13: TAdvToolBarButton;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    AdvPage8: TAdvPage;
    AdvPage9: TAdvPage;
    AdvPage11: TAdvPage;
    AdvPage12: TAdvPage;
    AdvPage13: TAdvPage;
    AdvPage14: TAdvPage;
    AdvToolBar16: TAdvToolBar;
    AdvGlowButton56: TAdvGlowButton;
    AdvToolBar29: TAdvToolBar;
    AdvGlowButton70: TAdvGlowButton;
    AdvGlowButton47: TAdvGlowButton;
    AdvToolBar26: TAdvToolBar;
    AdvGlowButton4: TAdvGlowButton;
    AdvGlowButton5: TAdvGlowButton;
    AdvGlowButton29: TAdvGlowButton;
    AdvGlowButton71: TAdvGlowButton;
    AdvToolBar31: TAdvToolBar;
    AdvGlowButton15: TAdvGlowButton;
    AdvGlowButton12: TAdvGlowButton;
    AGBLinkConcBco: TAdvGlowButton;
    AdvGlowButton11: TAdvGlowButton;
    AdvGlowButton10: TAdvGlowButton;
    AdvGlowButton13: TAdvGlowButton;
    AdvGlowButton14: TAdvGlowButton;
    AdvToolBar5: TAdvToolBar;
    AdvGlowButton31: TAdvGlowButton;
    AdvGlowButton55: TAdvGlowButton;
    AdvGlowButton22: TAdvGlowButton;
    AdvToolBar6: TAdvToolBar;
    AdvGlowButton20: TAdvGlowButton;
    AdvGlowButton21: TAdvGlowButton;
    AdvGlowButton36: TAdvGlowButton;
    AdvToolBar14: TAdvToolBar;
    AGBNFSe_RPSPesq: TAdvGlowButton;
    AGBNFSe_NFSePesq: TAdvGlowButton;
    AGBNFSeLRpsC: TAdvGlowButton;
    AGBNFSeFatCab: TAdvGlowButton;
    AGBNFSe_Edit: TAdvGlowButton;
    AdvToolBar15: TAdvToolBar;
    AGBNFSeMenCab: TAdvGlowButton;
    AGBNFSeSrvCad: TAdvGlowButton;
    AdvToolBar23: TAdvToolBar;
    AdvGlowButton42: TAdvGlowButton;
    AdvGlowButton43: TAdvGlowButton;
    AdvToolBar21: TAdvToolBar;
    AdvGlowMenuButton5: TAdvGlowMenuButton;
    AdvGlowButton37: TAdvGlowButton;
    AdvGlowButton38: TAdvGlowButton;
    AdvToolBar28: TAdvToolBar;
    AdvGlowButton69: TAdvGlowButton;
    AdvGlowButton32: TAdvGlowButton;
    AdvGlowButton33: TAdvGlowButton;
    AdvGlowButton34: TAdvGlowButton;
    AdvToolBar18: TAdvToolBar;
    AGBPesquisas: TAdvGlowMenuButton;
    AGBExtratos: TAdvGlowMenuButton;
    AdvGlowButton9: TAdvGlowButton;
    AdvGlowButton16: TAdvGlowButton;
    AdvToolBar7: TAdvToolBar;
    AdvGlowButton92: TAdvGlowButton;
    AdvGlowButton18: TAdvGlowButton;
    AdvToolBar12: TAdvToolBar;
    AdvGlowButton27: TAdvGlowButton;
    AdvGlowButton1: TAdvGlowButton;
    AdvToolBar30: TAdvToolBar;
    AdvGlowButton25: TAdvGlowButton;
    AdvGlowButton26: TAdvGlowButton;
    AdvGlowButton23: TAdvGlowButton;
    AdvGlowButton2: TAdvGlowButton;
    AdvGlowButton64: TAdvGlowButton;
    AdvGlowButton61: TAdvGlowButton;
    AdvGlowButton3: TAdvGlowButton;
    AdvToolBar2: TAdvToolBar;
    AdvGlowButton67: TAdvGlowButton;
    AdvToolBar3: TAdvToolBar;
    AdvGlowButton44: TAdvGlowButton;
    AdvGlowButton30: TAdvGlowButton;
    AdvGlowButton24: TAdvGlowButton;
    AdvToolBar8: TAdvToolBar;
    AGMBVerifiBD: TAdvGlowButton;
    AGMBBackup: TAdvGlowButton;
    AdvToolBar9: TAdvToolBar;
    AdvGlowButton58: TAdvGlowButton;
    AdvGlowButton52: TAdvGlowButton;
    AdvGlowButton17: TAdvGlowButton;
    AGBFiliais: TAdvGlowButton;
    AdvToolBar25: TAdvToolBar;
    AdvGlowMenuButton3: TAdvGlowMenuButton;
    AdvGlowMenuButton4: TAdvGlowMenuButton;
    AdvGlowButton7: TAdvGlowButton;
    AdvGlowButton8: TAdvGlowButton;
    AGBFinEncerMes: TAdvGlowButton;
    ProgressBar1: TProgressBar;
    AdvPage1: TAdvPage;
    AdvToolBar10: TAdvToolBar;
    AdvGlowButton19: TAdvGlowButton;
    AdvToolBar20: TAdvToolBar;
    AdvGlowButton6: TAdvGlowButton;
    APMWOSCadastros: TAdvPopupMenu;
    Resolues1: TMenuItem;
    Assuntos1: TMenuItem;
    Status1: TMenuItem;
    Modos1: TMenuItem;
    N2: TMenuItem;
    Atendentes1: TMenuItem;
    Gruposdeatendentes1: TMenuItem;
    AdvToolBar19: TAdvToolBar;
    AdvGlowButton60: TAdvGlowButton;
    AdvGlowButton54: TAdvGlowButton;
    AdvGlowButton57: TAdvGlowButton;
    N4: TMenuItem;
    Gruposdeassuntos1: TMenuItem;
    N5: TMenuItem;
    JanelasWEB1: TMenuItem;
    Calendrio1: TMenuItem;
    N6: TMenuItem;
    iposHistAlteraes1: TMenuItem;
    ProtocolosWEB1: TMenuItem;
    N7: TMenuItem;
    ProtocolosWEBOpes1: TMenuItem;
    Mostrardesktop1: TMenuItem;
    BalloonHint1: TBalloonHint;
    AdvToolBarButton1: TAdvToolBarButton;
    AdvGlowButton39: TAdvGlowButton;
    AdvGlowButton40: TAdvGlowButton;
    AdvGlowButton46: TAdvGlowButton;
    AdvGlowButton28: TAdvGlowButton;
    AdvGlowButton62: TAdvGlowButton;
    AdvGlowButton63: TAdvGlowButton;
    AdvGlowButton48: TAdvGlowButton;
    AdvToolBar1: TAdvToolBar;
    AdvGlowButton49: TAdvGlowButton;
    N8: TMenuItem;
    reinamentos1: TMenuItem;
    APMNfe: TAdvPopupMenu;
    ListadeWebSeriveces1: TMenuItem;
    CamposdaNFe1: TMenuItem;
    N9: TMenuItem;
    AtualizaLayout1: TMenuItem;
    AtualizaWebServices1: TMenuItem;
    AdvToolBar4: TAdvToolBar;
    AdvGlowButton65: TAdvGlowButton;
    AdvGlowButton45: TAdvGlowButton;
    AdvGlowButton50: TAdvGlowButton;
    AdvGlowButton41: TAdvGlowButton;
    AdvGlowButton51: TAdvGlowButton;
    BtEncode: TButton;
    BtDecode: TButton;
    Edit1: TMemo;
    TmVersao: TTimer;
    AdvShapeButton1: TAdvShapeButton;
    Button1: TButton;
    PB1: TProgressBar;
    AdvGlowButton53: TAdvGlowButton;
    AdvToolBar11: TAdvToolBar;
    AdvGlowButton66: TAdvGlowButton;
    AdvGlowButton68: TAdvGlowButton;
    AdvGlowButton72: TAdvGlowButton;
    AdvGlowButton73: TAdvGlowButton;
    AdvPage2: TAdvPage;
    AdvToolBar13: TAdvToolBar;
    AdvGlowMenuButton1: TAdvGlowMenuButton;
    AdvPMCadastros: TAdvPopupMenu;
    MenuItem10: TMenuItem;
    AdvGlowButton59: TAdvGlowButton;
    TimerIdle: TTimer;
    AdvGlowButton74: TAdvGlowButton;
    AdvGlowButton75: TAdvGlowButton;
    AdvGlowButton35: TAdvGlowButton;
    AdvGlowButton76: TAdvGlowButton;
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Timer1Timer(Sender: TObject);
    procedure frLFamiliasUserFunction(const Name: String; p1, p2,
      p3: Variant; var Val: Variant);
    procedure AGMBBackupClick(Sender: TObject);
    procedure AdvGlowButton29Click(Sender: TObject);
    procedure AdvGlowButton15Click(Sender: TObject);
    procedure AdvGlowButton14Click(Sender: TObject);
    procedure AdvGlowButton13Click(Sender: TObject);
    procedure AdvGlowButton12Click(Sender: TObject);
    procedure AdvGlowButton11Click(Sender: TObject);
    procedure AdvGlowButton10Click(Sender: TObject);
    procedure AdvGlowButton6Click(Sender: TObject);
    procedure AdvGlowButton17Click(Sender: TObject);
    procedure AdvGlowButton58Click(Sender: TObject);
    procedure AdvGlowButton7Click(Sender: TObject);
    procedure AdvToolBar3Close(Sender: TObject);
    procedure AdvGlowButton1Click(Sender: TObject);
    procedure AdvGlowButton3Click(Sender: TObject);
    procedure AdvGlowButton2Click(Sender: TObject);
    procedure AGBLinkConcBcoClick(Sender: TObject);
    procedure AdvGlowButton8Click(Sender: TObject);
    procedure AGBFinEncerMesClick(Sender: TObject);
    procedure AdvGlowButton5Click(Sender: TObject);
    procedure AdvGlowButton4Click(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure Limpar1Click(Sender: TObject);
    procedure Padro3Click(Sender: TObject);
    procedure AdvGlowButton9Click(Sender: TObject);
    procedure PagarReceber1Click(Sender: TObject);
    procedure Movimento1Click(Sender: TObject);
    procedure ResultadosMensais1Click(Sender: TObject);
    procedure ReceitaseDespesas1Click(Sender: TObject);
    procedure Prestaodecontas1Click(Sender: TObject);
    procedure Emqualquerconta1Click(Sender: TObject);
    procedure Contascontroladas1Click(Sender: TObject);
    procedure Contassazonais1Click(Sender: TObject);
    procedure AdvGlowButton16Click(Sender: TObject);
    procedure AdvGlowButton18Click(Sender: TObject);
    procedure AdvGlowButton92Click(Sender: TObject);
    procedure AdvGlowButton19Click(Sender: TObject);
    procedure AdvGlowButton23Click(Sender: TObject);
    procedure AdvGlowButton21Click(Sender: TObject);
    procedure AdvGlowButton20Click(Sender: TObject);
    procedure AdvGlowButton22Click(Sender: TObject);
    procedure AdvGlowButton36Click(Sender: TObject);
    procedure AdvGlowButton62Click(Sender: TObject);
    procedure AdvGlowButton63Click(Sender: TObject);
    procedure AdvGlowButton24Click(Sender: TObject);
    procedure AdvGlowButton61Click(Sender: TObject);
    procedure AdvGlowButton69Click(Sender: TObject);
    procedure AdvGlowButton25Click(Sender: TObject);
    procedure AdvGlowButton64Click(Sender: TObject);
    procedure AdvGlowButton26Click(Sender: TObject);
    procedure AdvGlowButton67Click(Sender: TObject);
    procedure AdvGlowButton27Click(Sender: TObject);
    procedure AdvGlowButton71Click(Sender: TObject);
    procedure AdvGlowButton47Click(Sender: TObject);
    procedure AdvGlowButton43Click(Sender: TObject);
    procedure AdvGlowButton42Click(Sender: TObject);
    procedure AdvGlowButton65Click(Sender: TObject);
    procedure AdvGlowButton70Click(Sender: TObject);
    procedure AdvGlowButton60Click(Sender: TObject);
    procedure AdvGlowButton38Click(Sender: TObject);
    procedure AdvGlowButton37Click(Sender: TObject);
    procedure AdvGlowButton41Click(Sender: TObject);
    procedure Perfis2Click(Sender: TObject);
    procedure AdvGlowButton52Click(Sender: TObject);
    procedure MenuItem20Click(Sender: TObject);
    procedure MenuItem21Click(Sender: TObject);
    procedure AdvGlowButton32Click(Sender: TObject);
    procedure AdvGlowButton33Click(Sender: TObject);
    procedure AdvGlowButton34Click(Sender: TObject);
    procedure Reabrirtabelas1Click(Sender: TObject);
    procedure Suporte1Click(Sender: TObject);
    procedure VerificaTabelasPblicas1Click(Sender: TObject);
    procedure AGBNFSe_EditClick(Sender: TObject);
    procedure AGBNFSe_NFSePesqClick(Sender: TObject);
    procedure AGBNFSeSrvCadClick(Sender: TObject);
    procedure AGBNFSeMenCabClick(Sender: TObject);
    procedure AGBFiliaisClick(Sender: TObject);
    procedure AGBNFSe_RPSPesqClick(Sender: TObject);
    procedure AGBNFSeLRpsCClick(Sender: TObject);
    procedure AdvGlowButton30Click(Sender: TObject);
    procedure AdvGlowButton31Click(Sender: TObject);
    procedure AdvGlowButton55Click(Sender: TObject);
    procedure AdvGlowButton44Click(Sender: TObject);
    procedure sd1FormSkin(Sender: TObject; aName: string; var DoSkin: Boolean);
    procedure AdvToolBarButton8Click(Sender: TObject);
    procedure AdvToolBarButton7Click(Sender: TObject);
    procedure AdvToolBarButton9Click(Sender: TObject);
    procedure AdvToolBarButton10Click(Sender: TObject);
    procedure AdvToolBarButton13Click(Sender: TObject);
    procedure AdvToolBarButton12Click(Sender: TObject);
    procedure AGBNFSeFatCabClick(Sender: TObject);
    procedure AdvGlowButton28Click(Sender: TObject);
    procedure Resolues1Click(Sender: TObject);
    procedure Assuntos1Click(Sender: TObject);
    procedure Status1Click(Sender: TObject);
    procedure Modos1Click(Sender: TObject);
    procedure Gruposdeatendentes1Click(Sender: TObject);
    procedure Atendentes1Click(Sender: TObject);
    procedure AdvGlowButton57Click(Sender: TObject);
    procedure Gruposdeassuntos1Click(Sender: TObject);
    procedure JanelasWEB1Click(Sender: TObject);
    procedure iposHistAlteraes1Click(Sender: TObject);
    procedure Calendrio1Click(Sender: TObject);
    procedure ProtocolosWEB1Click(Sender: TObject);
    procedure ProtocolosWEBOpes1Click(Sender: TObject);
    procedure AdvGlowButton45Click(Sender: TObject);
    procedure Mostrardesktop1Click(Sender: TObject);
    procedure AdvToolBarButton1Click(Sender: TObject);
    procedure AdvGlowButton40Click(Sender: TObject);
    procedure AdvGlowButton46Click(Sender: TObject);
    procedure AdvGlowButton48Click(Sender: TObject);
    procedure AdvGlowButton49Click(Sender: TObject);
    procedure AdvGlowButton56Click(Sender: TObject);
    procedure reinamentos1Click(Sender: TObject);
    procedure ListadeWebSeriveces1Click(Sender: TObject);
    procedure CamposdaNFe1Click(Sender: TObject);
    procedure AtualizaLayout1Click(Sender: TObject);
    procedure AtualizaWebServices1Click(Sender: TObject);
    procedure AdvGlowButton50Click(Sender: TObject);
    procedure AdvGlowButton51Click(Sender: TObject);
    procedure BtEncodeClick(Sender: TObject);
    procedure BtDecodeClick(Sender: TObject);
    procedure TmVersaoTimer(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure AdvGlowButton53Click(Sender: TObject);
    procedure AdvGlowButton66Click(Sender: TObject);
    procedure AdvGlowButton68Click(Sender: TObject);
    procedure AdvGlowButton72Click(Sender: TObject);
    procedure AdvGlowButton73Click(Sender: TObject);
    procedure MenuItem10Click(Sender: TObject);
    procedure AdvGlowButton59Click(Sender: TObject);
    procedure TimerIdleTimer(Sender: TObject);
    procedure AdvGlowButton75Click(Sender: TObject);
    procedure AdvGlowButton35Click(Sender: TObject);
    procedure AdvGlowButton76Click(Sender: TObject);
  private
    FALiberar, FAtualizouFavoritos: Boolean;
    { Private declarations }
    function  VerificaNovasVersoes(ApenasVerifica: Boolean = False): Boolean;
    procedure MostraLogoff();
    procedure MostraOpcoes();
    procedure MostraMatriz();
    procedure MostraAnotacoes();
    procedure CadastroBancos();
    procedure SkinMenu(Index: integer);
    procedure MostraFeriados();
    procedure MostraEnquete();
    procedure MostraBackupDir(AbrirEmAba: Boolean);
    procedure MostraOpcoesDControl;
    procedure MostraFiliais();
    procedure MostraWSincro();
    procedure MostraFormDescanso();
  public
    { Public declarations }
    Alfabeto: TObjectList;
    //PNG, JoinedPNG: TPngObject;
    FBarraTarefa: Integer;
    FTipoNovoEnti: Integer;
    FLoadedFonts: Boolean;
    FListaArtes: TStringList;
    FArtesBMP: Integer;
    FEntInt, FSeqJan, FSeqItm: Integer;
    //
    function  CalculaTextFontSize(FoNam: String; FoTam: Integer; ResTela: Double): Integer;
    function  CadastroDeContasSdoSimples(Entidade, Conta: Integer): Boolean;
    procedure ShowHint(Sender: TObject);
    procedure RetornoCNAB;
    procedure CriaCalcPercent(Valor, Porcentagem: String; Calc: TcpCalc );
    function  CartaoDeFatura: Integer;
    function  CompensacaoDeFatura: String;
    procedure SelecionaImagemdefundo;
    procedure PagarRolarEmissao(Query: TmySQLQuery);
    procedure CriaImpressaoDiversos(Indice: Integer);
    procedure CriaMinhasEtiquetas;
    procedure AcoesExtrasDeCadastroDeEntidades(Grade1: TStringGrid;
              Entidade: Integer; Aba: Boolean = False);
    procedure AcoesIniciaisDeCadastroDeEntidades(Reference: TComponent;
              Codigo: Integer; Grade: TStringGrid);
    function  RecriaTempTable(Tabela: String): Boolean;
    procedure SalvaArquivo(EdNomCli, EdCPFCli: TEdit; Grade:
                TStringGrid; Data: TDateTime; FileName: String; ChangeData: Boolean);
    function  AbreArquivoINI(Grade: TStringGrid; EdNomCli, EdCPFCli: TEdit;
                Data: TDateTime; Arquivo: String): Boolean;
    procedure DefineVarsCliInt(Empresa: Integer);
    procedure AcoesIniciaisDoAplicativo();
    function  ColunasDeTexto(FoNome: String; FoSizeT: Integer;
                MaxWidth: Integer; Qry: TMySQLQuery;
                Campo: String; Codigo: Integer): Integer;
    procedure ReCaptionComponentesDeForm(Form: TForm);
    function  PreparaPesquisaFinanceira(TabLctA: String): Boolean;
    procedure MostraConDef();
    procedure MostraModulos();
    procedure MostraAplicModul(Aplicativo: Integer);
    procedure CadastrodeAplicativos(Codigo: Integer);
    procedure CadastrodeClientes(AbrirEmAba: Boolean; Codigo, Page: Integer);
    procedure CadastroDeContratos(Codigo: Integer);
    function  ObtemNomeStatusAplicativo(): TStrings;
    procedure MostraFAQ();
    procedure EntidadeAtivaDesativa(Entidade: Integer);
    //Zip
    function  ZipaArquivo(var Caminho: String; const NovoNome: String;
                Encryptar: Boolean; Senha: String; Deletar: Boolean): Boolean;
    //Financeiros
    function  AcaoEspecificaDeApp(Servico: String): Boolean;
    procedure MostraContasMySQL(Codigo: Integer);
    //Notifica��es
    function  NotificacoesVerifica(QueryNotifi: TmySQLQuery; DataHora: TDateTime): Boolean;
    procedure NotificacoesJanelas(TipoNotifi: TNotifi; DataHora: TDateTime);
    function  FixBugAtualizacoesApp(FixBug: Integer; FixBugStr: String): Boolean;
  end;

var
  FmPrincipal: TFmPrincipal;

const
  CO_ARQLOG = 'Inicializacao';
  FAltLin = CO_POLEGADA / 2.16;

implementation

uses UnMyObjects, Module, Entidades, MyGlyfs, DControl_Dmk, Opcoes,
  UnLock_MLA, MyListas, MyVCLSkin, CalcPercent, UnMsgInt, UnGOTOy, UCreate,
  MultiEtiq, Maladireta, EntidadesImp, GetValor, ModuleGeral, Matriz, Cartas,
  Bancos, EmiteCheque_0, Entidade2, Backup3, UnMyPrinters, UnitMD5, ModuleFin,
  ModuleLct2, UnFinanceiro, Formularios, CentroCusto, LinkRankSkin, NovaVersao,
  UnDmkWeb, Anotacoes, PreEmail, UnBloquetos, ConDef, Aplicativos, MySQLConf,
  Zero800Cab, IGPMCad, Feriados, Contratos, Enquete, Clientes, UnGrl_Consts,
  TelCtaCab, BackupDir, GerGlyfs, WPrOpcoes, WProtocol, ModWOrdSer,
  OpcoesDControl, FavoritosG, CNAE21Cad, NFeLoadTabs, UnitWOrdSerJan,
  NFSe_PF_0000, NFSe_PF_0201, ParamsEmp, Modulos, MailGeren, UnFTPJan, AplicModul,
  UnWUsersJan, About, ZForge, UnFinanceiroJan, WOrdSer, UnitVersoes_Jan,
 {$IfNDef SemProtocolo} UnProtocoUnit, {$EndIf} Descanso, UnWTextos_Jan,
  NFeWebServicesGer, ServTarifas, Eventos, UnALL_Jan, UnDmkWeb_Jan,
  AgendaGer2, WTreiOpc, NFeNewVer, Restaura2, UnBloqGerl, UnBloqGerl_Jan,
  EntiAtivo, UnFixBugs, UnitAgendaJan, UnitHelpJan, UnGrl_Vars, UnAppPF,
  UnFTP, UnGrl_DmkDB, UnDmkWeb2_Jan, UMySQLDB;

{$R *.DFM}

procedure TFmPrincipal.FormActivate(Sender: TObject);
begin
  MLAGeral.CriaLog(CO_ARQLOG, 'Inicio ativa��o form principal.', False);
  APP_LIBERADO := True;
  if not MyObjects.CorIniComponente() then Hide else Show;
  VAR_ATUALIZANDO := False;
  VAR_APPNAME := Application.Title;
  if Trim(StatusBar.Panels[3].Text) <>
    Geral.FileVerInfo(Application.ExeName, 3 (*Versao*)) then
    ShowMessage('Vers�o difere do arquivo');
  if not FALiberar then
  Timer1.Enabled := True;
  MLAGeral.CriaLog(CO_ARQLOG, 'Fim ativa��o form principal.', False);
end;

procedure TFmPrincipal.FormCreate(Sender: TObject);
var
  MenuStyle: Integer;
  Imagem: String;
  Retorno: String;
begin
  FAtualizouFavoritos := False;
  //
  AdvToolBarPager1.ActivePageIndex := 0;
  //
  VAR_FIN_SELFG_000_CLI := 'Membro';
  VAR_FIN_SELFG_000_FRN := '-';
  VAR_LCT := '';
  Application.OnException := MyObjects.MostraErro;
  VAR_MULTIPLAS_TAB_LCT := True;
  //VAR_TYPE_LOG          := ttlCliIntUni;
  VAR_TYPE_LOG          := ttlFiliLog;
  VAR_ADMIN             := 'admin';
  FEntInt := -11;
  Alfabeto := TObjectList.Create;
  //
  //VAR_CALCULADORA_COMPONENTCLASS := TFmCalculadora;
  //VAR_CALCULADORA_REFERENCE      := FmCalculadora;
  VAR_USA_TAG_BITBTN := True;
  //
  FTipoNovoEnti := 0;
  MLAGeral.CriaLog(CO_ARQLOG, 'Inicio cria��o form principal.', True);
  VAR_STDATALICENCA := StatusBar.Panels[07];
  VAR_STTERMINAL    := StatusBar.Panels[05];
  VAR_SKINUSANDO    := StatusBar.Panels[09];
  VAR_STDATABASES   := StatusBar.Panels[11];
  VAR_STLOGIN       := StatusBar.Panels[01];
  StatusBar.Panels[3].Text := Geral.FileVerInfo(Application.ExeName, 3);
  VAR_TIPOSPRODM_TXT := '0,1,2,3,4,5,6,7,8,9,10,11,12,13';
  VAR_APP := ExtractFilePath(Application.ExeName);
  VAR_VENDEOQUE := 1;
  Application.OnMessage := MyObjects.FormMsg;
  //2017-07-19 => N�o ativar por causa da c�mera Application.OnIdle := AppIdle;
  TimerIdle.Interval := VAR_TIMERIDLEITERVAL;
  TimerIdle.Enabled  := True;
  //
  MLAGeral.CriaLog(CO_ARQLOG, 'Vari�veis iniciais configuradas.', False);
  //
  MenuStyle := Geral.ReadAppKey('MenuStyle', Application.Title,
    ktInteger, 0, HKEY_LOCAL_MACHINE);
  SkinMenu(MenuStyle);
  //
  VAR_CAD_POPUP := PMGeral;
  MyObjects.CopiaItensDeMenu(PMGeral, FmPrincipal);
  //
  MLAGeral.CriaLog(CO_ARQLOG, 'Popup geral definido.', False);
  PageControl1.ActivePageIndex := 0;
  MLAGeral.CriaLog(CO_ARQLOG, 'Pagina inicial do pagecontrol definida.', False);
  //////////////////////////////////////////////////////////////////////////////
  // Local
  VAR_IMPCHEQUE       := Geral.ReadAppKeyCU('ImpCheque', 'Dermatek', ktInteger, 0);
  VAR_IMPCHEQUE_PORTA := Geral.ReadAppKeyCU('ImpCheque_Porta', 'Dermatek', ktString, 'COM2');
  // Servidor
  VAR_IMPCH_IP        := Geral.ReadAppKeyCU('ImpreCh_IP', 'Dermatek', ktString, '127.0.0.1');
  VAR_IMPCHPORTA      := Geral.ReadAppKeyCU('ImpreChPorta', 'Dermatek', ktInteger, 9520);
  //
  MLAGeral.CriaLog(CO_ARQLOG, 'Verifica.ndo impressora de cheque', False);
  MyPrinters.VerificaImpressoraDeCheque(Retorno);
  if Retorno <> '' then
    Geral.MensagemBox(Retorno, 'Aviso', MB_OK+MB_ICONWARNING);
  //
  MyObjects.MaximizaAdvToolBarPager(AdvToolBarPager1, deftfTrue);
end;

procedure TFmPrincipal.SelecionaImagemdefundo;
begin
  MyObjects.SelecionaLimpaImagemdefundoFormDescanso(FmPrincipal, TFmDescanso, PageControl1);
end;

function TFmPrincipal.FixBugAtualizacoesApp(FixBug: Integer;
  FixBugStr: String): Boolean;
begin
  Result := True;
  (* Marcar quando for criar uma fun��o de atualiza��o
  try
    if FixBug = 0 then
      Atualiza��o1()
    else if FixBug = 1 then
      Atualiza��o2()
    else if FixBug = 2 then
      Atualiza��o3()
    else
      Result := False;
  except
    Result := False;
  end;
  *)
end;

procedure TFmPrincipal.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if DModG <> nil then
    DModG.MostraBackup3(False);
  //
  if FListaArtes <> nil then
    FListaArtes.Free;
  //
  Application.Terminate;
end;

function TFmPrincipal.PreparaPesquisaFinanceira(TabLctA: String): Boolean;
var
  MyCursor: TCursor;
begin
  Result := True;
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  //
  FinanceiroJan.LancamentosComProblemas(TabLctA);
  //
  Screen.Cursor := MyCursor;
  //
end;

procedure TFmPrincipal.Prestaodecontas1Click(Sender: TObject);
begin
  FinanceiroJan.MostraCashPreCta(0);
end;

procedure TFmPrincipal.ProtocolosWEB1Click(Sender: TObject);
begin
  if DmkWeb.ConexaoRemota(Dmod.MyDBn, DModG.QrWebParams, 1) then
  begin
    if DBCheck.CriaFm(TFmWProtocol, FmWProtocol, afmoNegarComAviso) then
    begin
      FmWProtocol.ShowModal;
      FmWProtocol.Destroy;
    end;
  end;
end;

procedure TFmPrincipal.ProtocolosWEBOpes1Click(Sender: TObject);
begin
  if DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrWebParams, 1) then
  begin
    if DBCheck.CriaFm(TFmWPrOpcoes, FmWPrOpcoes, afmoNegarComAviso) then
    begin
      FmWPrOpcoes.ShowModal;
      FmWPrOpcoes.Destroy;
    end;
  end;
end;

procedure TFmPrincipal.AdvGlowButton10Click(Sender: TObject);
begin
  FinanceiroJan.CadastroDeNiveisPlano;
end;

procedure TFmPrincipal.AdvGlowButton11Click(Sender: TObject);
begin
  FinanceiroJan.CadastroDeContas(0);
end;

procedure TFmPrincipal.AdvGlowButton12Click(Sender: TObject);
begin
  FinanceiroJan.CadastroDeSubGrupos(0);
end;

procedure TFmPrincipal.AdvGlowButton13Click(Sender: TObject);
begin
  FinanceiroJan.CadastroDeGrupos(0);
end;

procedure TFmPrincipal.AdvGlowButton14Click(Sender: TObject);
begin
  FinanceiroJan.CadastroDeConjutos(0);
end;

procedure TFmPrincipal.AdvGlowButton15Click(Sender: TObject);
begin
  FinanceiroJan.CadastroDePlano(0);
end;

procedure TFmPrincipal.AdvGlowButton16Click(Sender: TObject);
begin
  FinanceiroJan.MostraSaldos(0);
end;

procedure TFmPrincipal.AdvGlowButton17Click(Sender: TObject);
begin
  MostraOpcoes();
end;

procedure TFmPrincipal.AdvGlowButton18Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEntidadesImp, FmEntidadesImp, afmoNegarComAviso) then
  begin
    FmEntidadesImp.ShowModal;
    FmEntidadesImp.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton19Click(Sender: TObject);
begin
  {$IFDEF UsaWSuport}
  DmkWeb.MostraWSuporte(False, 2, BalloonHint1);
  {$ENDIF}
end;

procedure TFmPrincipal.AdvGlowButton1Click(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2, False, True,
    PageControl1, AdvToolBarPager1);
end;

procedure TFmPrincipal.AdvGlowButton20Click(Sender: TObject);
begin
  UBloqGerl_Jan.CadastroCNAB_Cfg;
end;

procedure TFmPrincipal.AdvGlowButton21Click(Sender: TObject);
begin
  UBloquetos.MostraBloArre(0, 0);
end;

procedure TFmPrincipal.AdvGlowButton22Click(Sender: TObject);
begin
  UBloquetos.MostraBloGeren(-1, 0, 0, 0, PageControl1, AdvToolBarPager1);
end;

procedure TFmPrincipal.AdvGlowButton23Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPreEmail, FmPreEmail, afmoNegarComAviso) then
  begin
    FmPreEmail.ShowModal;
    FmPreEmail.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton24Click(Sender: TObject);
begin
  CadastrodeAplicativos(0);
end;

procedure TFmPrincipal.AdvGlowButton25Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmIGPMCad, FmIGPMCad, afmoNegarComAviso) then
  begin
    FmIGPMCad.ShowModal;
    FmIGPMCad.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton26Click(Sender: TObject);
begin
  CadastroDeContratos(0);
end;

procedure TFmPrincipal.AdvGlowButton27Click(Sender: TObject);
begin
  CadastrodeClientes(True, 0, 0);
end;

procedure TFmPrincipal.AdvGlowButton28Click(Sender: TObject);
begin
  Application.CreateForm(TFmAbout, FmAbout);
  FmAbout.ShowModal;
  FmAbout.Destroy;
end;

procedure TFmPrincipal.AdvGlowButton29Click(Sender: TObject);
begin
  FinanceiroJan.MostraFinancas(PageControl1, AdvToolBarPager1);
end;

procedure TFmPrincipal.AdvGlowButton2Click(Sender: TObject);
begin
  CadastroBancos();
end;

procedure TFmPrincipal.AdvGlowButton30Click(Sender: TObject);
begin
  MostraModulos();
end;

procedure TFmPrincipal.AdvGlowButton31Click(Sender: TObject);
begin
  UBloquetos.MostraBloGerenInadImp(PageControl1, AdvToolBarPager1);
end;

procedure TFmPrincipal.AdvGlowButton32Click(Sender: TObject);
begin
  ProtocoUnit.MostraFormProtocolos(0, 0);
end;

procedure TFmPrincipal.AdvGlowButton33Click(Sender: TObject);
begin
  ProtocoUnit.MostraFormProtocoMot();
end;

procedure TFmPrincipal.AdvGlowButton34Click(Sender: TObject);
begin
  ProtocoUnit.MostraFormProtocoOco()
end;

procedure TFmPrincipal.AdvGlowButton35Click(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormNFSeParams();
end;

procedure TFmPrincipal.AdvGlowButton36Click(Sender: TObject);
begin
  UBloquetos.CadastroDeBloOpcoes;
end;

procedure TFmPrincipal.AdvGlowButton37Click(Sender: TObject);
begin
  UWUsersJan.MostraWUsers(0);
end;

procedure TFmPrincipal.AdvGlowButton38Click(Sender: TObject);
begin
  DmkWeb2_Jan.MostraWOpcoes(Dmod.MyDBn);
end;

procedure TFmPrincipal.AdvGlowButton3Click(Sender: TObject);
begin
  MostraAnotacoes();
end;

procedure TFmPrincipal.AdvGlowButton40Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmServTarifas, FmServTarifas, afmoNegarComAviso) then
  begin
    FmServTarifas.ShowModal;
    FmServTarifas.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton41Click(Sender: TObject);
begin
  UnVersoes_Jan.MostraFormWHisAlt(True, PageControl1, AdvToolBarPager1);
end;

procedure TFmPrincipal.AdvGlowButton42Click(Sender: TObject);
begin
  UFTPJan.MostraFTPWebArq(True, PageControl1, AdvToolBarPager1);
end;

procedure TFmPrincipal.AdvGlowButton43Click(Sender: TObject);
begin
  UFTPJan.MostraFTPConfig(0);
end;

procedure TFmPrincipal.AdvGlowButton44Click(Sender: TObject);
begin
  MostraConDef;
end;

procedure TFmPrincipal.AdvGlowButton45Click(Sender: TObject);
begin
  UnVersoes_Jan.MostraFormWVersao(True, 0, PageControl1, AdvToolBarPager1);
end;

procedure TFmPrincipal.AdvGlowButton46Click(Sender: TObject);
begin
  if DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrWebParams, 1) and
    (DmkWeb.ConectarUsuarioWEB(True)) then
  begin
    if DBCheck.CriaFm(TFmEventos, FmEventos, afmoNegarComAviso) then
    begin
      FmEventos.ShowModal;
      FmEventos.Destroy;
    end;
  end;
end;

procedure TFmPrincipal.AdvGlowButton47Click(Sender: TObject);
begin
  MostraBackupDir(True);
end;

procedure TFmPrincipal.AdvGlowButton48Click(Sender: TObject);
begin
  DModWOrdSer.MostraWOrdSerGru(True, FmPrincipal.PageControl1, FmPrincipal.AdvToolBarPager1);
end;

procedure TFmPrincipal.AdvGlowButton49Click(Sender: TObject);
begin
  MyObjects.FormTDICria(TFmMailGeren, PageControl1, AdvToolBarPager1, True, True);
end;

procedure TFmPrincipal.AdvGlowButton4Click(Sender: TObject);
begin
  FinanceiroJan.ImpressaoDoPlanoDeContas();
end;

procedure TFmPrincipal.AdvGlowButton50Click(Sender: TObject);
begin
  UnVersoes_Jan.MostraFormWUnLockStatis(0, 0, True, PageControl1, AdvToolBarPager1);
end;

procedure TFmPrincipal.AdvGlowButton51Click(Sender: TObject);
begin
  UnNotificacoes.MostraFmNotificacoes(PageControl1, AdvToolBarPager1);
end;

procedure TFmPrincipal.AdvGlowButton52Click(Sender: TObject);
begin
  MostraOpcoesDControl;
end;

procedure TFmPrincipal.AdvGlowButton53Click(Sender: TObject);
begin
  AppPF.AGEN_MostraFormAgenTarGer(FmPrincipal, True, PageControl1, AdvToolBarPager1, 0);
end;

procedure TFmPrincipal.AdvGlowButton55Click(Sender: TObject);
begin
  UBloqGerl_Jan.MostraBloCNAB_Ret;
end;

procedure TFmPrincipal.AdvGlowButton56Click(Sender: TObject);
begin
  MyObjects.FormTDICria(TFmAgendaGer2, PageControl1, AdvToolBarPager1, True, True);
end;

procedure TFmPrincipal.AdvGlowButton57Click(Sender: TObject);
begin
  DModWOrdSer.MostraWOrdSerOpc;
end;

procedure TFmPrincipal.AdvGlowButton58Click(Sender: TObject);
begin
  MostraMatriz();
end;

procedure TFmPrincipal.AdvGlowButton59Click(Sender: TObject);
begin
  dmkWeb.AbrirAppAcessoRemoto;
end;

procedure TFmPrincipal.AdvGlowButton5Click(Sender: TObject);
begin
  FinanceiroJan.SaldoDeContas(0);
end;

procedure TFmPrincipal.AdvGlowButton60Click(Sender: TObject);
begin
  UnWOrdSerJan.MostraWOrdSer(True, PageControl1, AdvToolBarPager1);
end;

procedure TFmPrincipal.AdvGlowButton61Click(Sender: TObject);
begin
  MostraContasMySQL(0);
end;

procedure TFmPrincipal.AdvGlowButton62Click(Sender: TObject);
begin
  ProtocoUnit.MostraProEnPr(0);
end;

procedure TFmPrincipal.AdvGlowButton63Click(Sender: TObject);
begin
  ProtocoUnit.MostraProtoGer(True, FmPrincipal.PageControl1,
    FmPrincipal.AdvToolBarPager1, 0, 0);
end;

procedure TFmPrincipal.AdvGlowButton64Click(Sender: TObject);
begin
  MostraFeriados;
end;

procedure TFmPrincipal.AdvGlowButton65Click(Sender: TObject);
begin
  UnVersoes_Jan.MostraFormWUnLockGeren(0, 0, True, PageControl1, AdvToolBarPager1);
end;

procedure TFmPrincipal.AdvGlowButton66Click(Sender: TObject);
begin
  UnHelpJan.MostraFormHelpOpc();
end;

procedure TFmPrincipal.AdvGlowButton67Click(Sender: TObject);
begin
  MostraEnquete;
end;

procedure TFmPrincipal.AdvGlowButton68Click(Sender: TObject);
begin
  UnHelpJan.MostraFormHelpFAQ(FmPrincipal, True, PageControl1, AdvToolBarPager1, 0);
end;

procedure TFmPrincipal.AdvGlowButton69Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmZero800Cab, FmZero800Cab, afmoNegarComAviso) then
  begin
    FmZero800Cab.ShowModal;
    FmZero800Cab.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton6Click(Sender: TObject);
begin
  VerificaNovasVersoes();
end;

procedure TFmPrincipal.AdvGlowButton70Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGerGlyfs, FmGerGlyfs, afmoNegarComAviso) then
  begin
    FmGerGlyfs.ShowModal;
    FmGerGlyfs.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton71Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmTelCtaCab, FmTelCtaCab, afmoNegarComAviso) then
  begin
    FmTelCtaCab.ShowModal;
    FmTelCtaCab.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton72Click(Sender: TObject);
begin
  UnHelpJan.MostraFormHelpPsq(FmPrincipal, True, PageControl1, AdvToolBarPager1);
end;

procedure TFmPrincipal.AdvGlowButton73Click(Sender: TObject);
begin
  UnHelpJan.MostraFormHelpCab(FmPrincipal, True, PageControl1, AdvToolBarPager1, 0);
end;

procedure TFmPrincipal.AdvGlowButton75Click(Sender: TObject);
begin
  MostraWSincro;
end;

procedure TFmPrincipal.AdvGlowButton76Click(Sender: TObject);
begin
  DmkWeb2_Jan.MostraUsuarios(CO_DMKID_APP);
end;

procedure TFmPrincipal.AdvGlowButton7Click(Sender: TObject);
begin
  FmLinkRankSkin.Show;
end;

procedure TFmPrincipal.AdvGlowButton8Click(Sender: TObject);
begin
  FinanceiroJan.CadastroDeCarteiras(0);
end;

procedure TFmPrincipal.AdvGlowButton92Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMalaDireta, FmMalaDireta, afmoNegarComAviso) then
  begin
    FmMalaDireta.ShowModal;
    FmMalaDireta.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton9Click(Sender: TObject);
begin
  FinanceiroJan.MostraCashBal(0);
end;

procedure TFmPrincipal.AdvToolBar3Close(Sender: TObject);
begin
  MostraAnotacoes();
end;

procedure TFmPrincipal.AdvToolBarButton10Click(Sender: TObject);
begin
  DModG.MostraBackup3();
end;

procedure TFmPrincipal.AdvToolBarButton12Click(Sender: TObject);
begin
  MyObjects.MaximizaAdvToolBarPager(AdvToolBarPager1, deftfInverse);
end;

procedure TFmPrincipal.AdvToolBarButton13Click(Sender: TObject);
begin
  {$IFDEF UsaWSuport}
  DmkWeb.MostraWSuporte(False, 2, BalloonHint1);
  {$ENDIF}
end;

procedure TFmPrincipal.AdvToolBarButton1Click(Sender: TObject);
begin
  MostraWSincro;
end;

procedure TFmPrincipal.AdvToolBarButton7Click(Sender: TObject);
begin
  MostraLogoff;
end;

procedure TFmPrincipal.AdvToolBarButton8Click(Sender: TObject);
begin
  VerificaNovasVersoes();
end;

procedure TFmPrincipal.AdvToolBarButton9Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmFavoritosG, FmFavoritosG, afmoLiberado) then
  begin
    FmFavoritosG.ShowModal;
    FmFavoritosG.Destroy;
    DModG.CriaFavoritos(AdvToolBarPager1, LaAviso1, LaAviso2, AdvGlowButton25, FmPrincipal);
  end;
end;

procedure TFmPrincipal.AGBFiliaisClick(Sender: TObject);
begin
  MostraFiliais();
end;

procedure TFmPrincipal.AGBFinEncerMesClick(Sender: TObject);
begin
  FinanceiroJan.MostraLctEncerraMes;
end;

procedure TFmPrincipal.AGBLinkConcBcoClick(Sender: TObject);
begin
  FinanceiroJan.CadastroContasLnk;
end;

procedure TFmPrincipal.AGBNFSeFatCabClick(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormNFSeFatCab(True, PageControl1, AdvToolBarPager1);
end;

procedure TFmPrincipal.AGBNFSeLRpsCClick(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormNFSeLRpsC_0201(0);
end;

procedure TFmPrincipal.AGBNFSeMenCabClick(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormNFSeMenCab(False, nil, nil);
end;

procedure TFmPrincipal.AGBNFSeSrvCadClick(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormNFSeSrvCad();
end;

procedure TFmPrincipal.AGBNFSe_EditClick(Sender: TObject);
const
  DPS           = 0;
  NFSeFatCab    = 0;
  Tomador       = 0;
  Intermediario = 0;
  MeuServico    = 0;
  ItemListSrv   = 0;
  Valor         = 0;

  Discriminacao = '';
  GeraNFSe      = True;
  SQLType       = stIns;
  Servico       = fgnLoteRPS;
var
  Prestador, NumNF: Integer;
  SerieNF: String;
begin
  Prestador := DmodG.QrFiliLogFilial.Value;
  UnNFSe_PF_0201.MostraFormNFSe(SQLType,
    Prestador, Tomador, Intermediario, MeuServico, ItemListSrv,
    Discriminacao, GeraNFSe, NFSeFatCab, DPS, Servico, nil, Valor, SerieNF,
    NumNF, nil);
end;

procedure TFmPrincipal.AGBNFSe_NFSePesqClick(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormNFSe_NFSePesq(True, PageControl1, AdvToolBarPager1);
end;

procedure TFmPrincipal.AGBNFSe_RPSPesqClick(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormNFSe_RPSPesq(True, PageControl1, AdvToolBarPager1);
end;

procedure TFmPrincipal.AGMBBackupClick(Sender: TObject);
begin
  DModG.MostraBackup3();
end;

procedure TFmPrincipal.Assuntos1Click(Sender: TObject);
begin
  DModWOrdSer.MostraWOrdSerAss(0);
end;

procedure TFmPrincipal.Atendentes1Click(Sender: TObject);
begin
  DModWOrdSer.MostraWOrdSerAte;
end;

procedure TFmPrincipal.AtualizaLayout1Click(Sender: TObject);
var
  Qry: TmySQLQuery;
  Res: Boolean;
  ArqNome: String;
  Versao, VerNFeLay: Integer;
begin
  //Fazer assim para n�o precisar incluir o m�dulo NF-e no DControl
  if not DBCheck.LiberaPelaSenhaAdmin then Exit;
  //
  Qry := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT VerNFeLay ',
      'FROM nfectrl ',
      '']);
    //
    VerNFeLay := Qry.FieldByName('VerNFeLay').AsInteger;
    //
    Res := DmkWeb.VerificaAtualizacaoVersao2(True, True, CO_WEBID_NFELAY,
             'Layout NFe', Geral.SoNumero_TT(DModG.QrMasterCNPJ.Value),
             VerNFeLay, 0, DModG.ObtemAgora(), nil, dtSQLs, Versao, ArqNome, False);
    if (Res) and (VAR_DOWNLOAD_OK) then
    begin
      //C:\Projetos\Delphi 2007\Aplicativos\Auxiliares\DermaBK\Restaura.pas
      Application.CreateForm(TFmRestaura2, FmRestaura2);
      FmRestaura2.Show;
      FmRestaura2.EdBackFile.Text := CO_DIR_RAIZ_DMK + '\' + ArqNome + '.SQL';
      //FmRestaura2.BeRestore.DatabaseName := DMod.MyDB.DatabaseName;
      FmRestaura2.FDB := DMod.MyDB;
      FmRestaura2.BtDiretoClick(Self);
      FmRestaura2.Close;
      //
      //Atualiza vers�o no banco de dados
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE nfectrl SET VerNFeLay=:P0');
      Dmod.QrUpd.Params[00].AsInteger := Versao;
      Dmod.QrUpd.ExecSQL;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TFmPrincipal.AtualizaWebServices1Click(Sender: TObject);
var
  Res: Boolean;
  ArqNome: String;
  Versao: Integer;
begin
  //Fazer assim para n�o precisar incluir o m�dulo NF-e no DControl
  if not DBCheck.LiberaPelaSenhaAdmin then Exit;
  //
  Res := DmkWeb.VerificaAtualizacaoVersao2(True, True, CO_WEBID_NFEWS,
           'Lista de Web Services NF-e', Geral.SoNumero_TT(DModG.QrMasterCNPJ.Value),
           0, 0, DModG.ObtemAgora(), nil, dtSQLs, Versao, ArqNome, False);
  if (Res) and (VAR_DOWNLOAD_OK) then
  begin
    //C:\Projetos\Delphi 2007\Aplicativos\Auxiliares\DermaBK\Restaura.pas
    Application.CreateForm(TFmRestaura2, FmRestaura2);
    FmRestaura2.Show;
    FmRestaura2.EdBackFile.Text := CO_DIR_RAIZ_DMK + '\' + ArqNome + '.SQL';
    //FmRestaura2.BeRestore.DatabaseName := DMod.MyDB.DatabaseName;
    FmRestaura2.FDB := DModG.AllID_DB;
    FmRestaura2.BtDiretoClick(Self);
    FmRestaura2.Close;
  end;
end;

procedure TFmPrincipal.BtDecodeClick(Sender: TObject);

function ValidaTextoRec_XML(Txt: String): String;
var
  I: Integer;
  Res: String;
begin
  Res := '';
  for I := 1 to Length(Txt) do
    //Ress := Res + MLAGeral.CharToUTF8XML(Ord(Txt[I]));
    Res := Res + MLAGeral.CharToMyXML_Receber(Ord(Txt[I]));
  Result := Res;
end;

var
  I: Integer;
  Txt, Txt2, Txt3, Txt4: String;
begin
  Txt  := Edit1.Text;
  Txt3 := '';

  for I := 1 to (Length(Txt) div 2) do
  begin
    Txt2 := Copy(Txt, I * 2 -1, 2);
    Txt3 := Txt3 + Char(StrToInt('$' + Txt2));
    //Txt4 := Txt4 + IntToHex(Ord(Txt3[I]), 2);
  end;

  Txt2 := dmkPF.Criptografia(Txt3, CO_RandStrWeb01);

  //Txt4 := ValidaTextoRec_XML(Txt2);
  //
  Geral.MB_Aviso('Texto Hex: ' + Txt + sLineBreak + 'Texto Cripto: ' + Txt3 +
    sLineBreak + 'Texto: ' + Txt2);
end;

procedure TFmPrincipal.BtEncodeClick(Sender: TObject);
var
  I: Integer;
  Txt, (*Txt2, *)Txt3, Txt4: String;
begin
  Txt  := Edit1.Text;
  //Txt2 := XXe_PF.ValidaTexto_XML(Txt, '', '');
  Txt3 := dmkPF.Criptografia(Txt, CO_RandStrWeb01);
  Txt4 := '';
  //
  (*
  for I := 32 to 128 do
  begin
    Geral.MB_Info(Geral.FF0(I) + ' ' + IntToHex(I, 2));
  end;
  *)

  for I := 1 to Length(Txt3) do
  begin
    Txt4 := Txt4 + IntToHex(Ord(Txt3[I]), 2);
  end;

  Geral.MB_Aviso('Texto: ' + Txt + sLineBreak + 'Texto Cripto: ' + Txt3 +
    sLineBreak + 'Cripto Ord: ' + Txt4);
end;

procedure TFmPrincipal.Button1Click(Sender: TObject);
begin
(*
  function ZipaArquivo(var Caminho: String; const Encryptar: Boolean;
    Senha: String): Boolean;
  var
    NomeArq, CamZip: String;
  begin
    CamZip  := '';
    NomeArq := Geral.SemAcento(ExtractFileName(Caminho));
    NomeArq := ChangeFileExt(NomeArq, '');
    //
    Application.CreateForm(TFmZForge, FmZForge);
    FmZForge.Show;
    //
    CamZip := FmZForge.ZipaArquivo(zftArquivo, ExtractFileDir(Caminho), Caminho,
                NomeArq, Senha, Encryptar, False);
    //
    FmZForge.Destroy;
    //
    if Length(CamZip) > 0 then
    begin
      Caminho := CamZip;
      Result  := True;
    end
    else
      Result := False;
  end;

const
  Pass = 'wkljweryhvbirt';
var
  I: Integer;
  Arquivo, Arq, NovoNome, NovoDir, Diretorio: String;
  ListaArq: TStringList;
  BuscaResul: TSearchRec;
begin
  PB1.Position := 0;
  ListaArq     := TStringList.Create;
  try
    Diretorio := MyObjects.DefineDiretorio(Self, nil);
    //
    if Diretorio = '' then
    begin
      Geral.MB_Erro('Falha ao obter diret�rio!');
      Exit;
    end;
    //
    if SetCurrentDir(Diretorio) then
    begin
      if findfirst('*.zip', faAnyFile, BuscaResul) = 0 then
      begin
        repeat
          Arq := Diretorio + '\' + BuscaResul.Name;
          //
          ListaArq.Add(Arq)
        until
          FindNext(BuscaResul) <> 0;
        FindClose(BuscaResul);
      end;
    end;
    if ListaArq.Count > 0 then
    begin
      PB1.Max := ListaArq.Count;
      //
      for I := 0 to ListaArq.Count - 1 do
      begin
        Arq     := ListaArq[I];
        Arquivo := Arq;
        //
        if not ZipaArquivo(Arquivo, True, Pass) then
        begin
          Geral.MB_Erro('Falha ao zipar arquivo "' + Arquivo + '"');
          Exit;
        end else
        begin
          NovoDir := Diretorio + '\Crypto';
          //
          if ForceDirectories(NovoDir) then
          begin
            dmkPF.MoverArquivo(Arq, NovoDir + '\' + ExtractFileName(Arq));
          end;
        end;
        PB1.Position := PB1.Position + 1;
        PB1.Update;
        Application.ProcessMessages;
      end;
    end;
  finally
    ListaArq.Free;
  end;
  *)
end;

procedure TFmPrincipal.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  FALiberar := True;
  FmDControl_Dmk.Show;
  Enabled := False;
  FmDControl_Dmk.Refresh;
  FmDControl_Dmk.EdSenha.Text := FmDControl_Dmk.EdSenha.Text+'*';
  FmDControl_Dmk.EdSenha.Refresh;
  FmDControl_Dmk.Refresh;
  try
    Application.CreateForm(TDmod, Dmod);
  except
    Application.MessageBox(PChar('Imposs�vel criar Modulo de dados'), 'Erro', MB_OK+MB_ICONERROR);
    Application.Terminate;
    Exit;
  end;
  FmDControl_Dmk.EdSenha.Text := FmDControl_Dmk.EdSenha.Text+'*';
  FmDControl_Dmk.EdSenha.Refresh;
  FmDControl_Dmk.ReloadSkin;
  FmDControl_Dmk.EdLogin.Text := '';
  FmDControl_Dmk.EdSenha.Text := '';
  FmDControl_Dmk.EdSenha.Refresh;
  FmDControl_Dmk.EdLogin.ReadOnly := False;
  FmDControl_Dmk.EdSenha.ReadOnly := False;
  FmDControl_Dmk.EdLogin.SetFocus;
  //FmDControl_Dmk.ReloadSkin;
  FmDControl_Dmk.Refresh;
end;

procedure TFmPrincipal.TimerIdleTimer(Sender: TObject);
begin
  if DModG <> nil then
    DmodG.ExecutaPing(FmDControl_Dmk, [Dmod.MyDB, Dmod.MyDBn, DModG.MyPID_DB,
      DModG.AllID_DB]);
end;

procedure TFmPrincipal.TmVersaoTimer(Sender: TObject);
begin
  TmVersao.Enabled := False;
  //
  if DmkWeb.RemoteConnection then
  begin
    if VerificaNovasVersoes(True) then
      DmkWeb.MostraBalloonHintMenuTopo(AdvToolBarButton8, BalloonHint1,
        'H� uma nova vers�o!', 'Clique aqui para atualizar.');
  end;
end;

function TFmPrincipal.VerificaNovasVersoes(ApenasVerifica: Boolean = False): Boolean;
var
  Versao: Integer;
  ArqNome: String;
begin
  Result := DmkWeb.VerificaAtualizacaoVersao2(True, True, 'DControl',
              'DControl', Geral.SoNumero_TT(DModG.QrMasterCNPJ.Value),
              CO_VERSAO, CO_DMKID_APP, DModG.ObtemAgora(), Memo3, dtExecAux,
              Versao, ArqNome, False, ApenasVerifica, BalloonHint1);
end;

procedure TFmPrincipal.VerificaTabelasPblicas1Click(Sender: TObject);
begin
  ALL_Jan.MostraFormVerifiDBTerceiros(False);
end;

procedure TFmPrincipal.CadastroBancos;
begin
  if DBCheck.CriaFm(TFmBancos, FmBancos, afmoNegarComAviso) then
  begin
    FmBancos.ShowModal;
    FmBancos.Destroy;
  end;
end;

procedure TFmPrincipal.MostraFormDescanso;
begin
  MyObjects.FormTDICria(TFmDescanso, PageControl1, AdvToolBarPager1, False, True, afmoLiberado);
end;

procedure TFmPrincipal.CadastrodeAplicativos(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmAplicativos, FmAplicativos, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmAplicativos.LocCod(Codigo, Codigo);
    FmAplicativos.ShowModal;
    FmAplicativos.Destroy;
  end;
end;

procedure TFmPrincipal.CadastrodeClientes(AbrirEmAba: Boolean; Codigo, Page: Integer);
var
  Form: TForm;
begin
  if AbrirEmAba then
  begin
    Form := MyObjects.FormTDICria(TFmClientes, PageControl1, AdvToolBarPager1, True, True);
    //
    if Codigo <> 0 then
      TFmClientes(Form).LocCod(Codigo, Codigo);
    TFmClientes(Form).PageControl1.ActivePageIndex := Page;
  end else
  begin
    if DBCheck.CriaFm(TFmClientes, FmClientes, afmoNegarComAviso) then
    begin
      if Codigo > 0 then
        FmClientes.LocCod(Codigo, Codigo);
      FmClientes.PageControl1.ActivePageIndex := Page;
      //
      FmClientes.ShowModal;
      FmClientes.Destroy;
      //
      FmClientes := nil;
    end;
  end;
end;

function TFmPrincipal.CartaoDeFatura: Integer;
begin
  ShowMessage('Faturando n�o implementado');
  Result := 0;
end;

function TFmPrincipal.ColunasDeTexto(FoNome: String; FoSizeT: Integer;
MaxWidth: Integer; Qry: TMySQLQuery; Campo: String; Codigo: Integer): Integer;
var
  MeLinha1, MeLinha2: TMemo;
  R: Integer;
begin
  Result := 1;
  MeLinha1 := TMemo.Create(FmPrincipal);
  MeLinha1.Visible := False;
  MeLinha1.Update;
  MeLinha1.Parent := FmPrincipal;
  // Evitar quebra de linha antecipada!
  MeLinha1.Width := MaxWidth;
  MeLinha1.Height := Height;
  MeLinha1.Font.Name := FoNome;
  MeLinha1.Font.Size := FoSizeT;
  //
  MeLinha2 := TMemo.Create(FmPrincipal);
  MeLinha2.Visible := False;
  MeLinha2.Update;
  MeLinha2.Parent := FmPrincipal;
  MeLinha2.Width := MaxWidth;
  MeLinha2.Height := Height;
  MeLinha2.WordWrap := False;
  MeLinha2.Font.Name := FoNome;
  MeLinha2.Font.Size := FoSizeT;
  //
  MeLinha1.Lines.Clear;
  MeLinha2.Lines.Clear;
  //
  Qry.Close;
  Qry.Params[0].AsInteger := Codigo;
  Qry.Open;
  while not Qry.Eof do
  begin
    MeLinha1.Lines.Add(Qry.FieldByName(Campo).AsString);
    MeLinha2.Lines.Add(Qry.FieldByName(Campo).AsString);
    //
    Qry.Next;
  end;
  //
  if MeLinha1.Lines.Count = MeLinha2.Lines.Count then
  begin
    R := 1;
    repeat
      R := R + 1;
      MeLinha1.Width := MaxWidth div R;
      MeLinha2.Width := MeLinha1.Width;
    until (MeLinha1.Lines.Count <> MeLinha2.Lines.Count);
    Result := R - 1;
  end;
  MeLinha1.Destroy;
  MeLinha2.Destroy;
end;

function TFmPrincipal.CompensacaoDeFatura: String;
begin
  ShowMessage('Faturando n�o implementado');
  Result := '';
end;

procedure TFmPrincipal.Contascontroladas1Click(Sender: TObject);
begin
  FinanceiroJan.MostraPesquisaContasControladas(0);
end;

procedure TFmPrincipal.Contassazonais1Click(Sender: TObject);
begin
  FinanceiroJan.MostraContasSazonais;
end;

procedure TFmPrincipal.CriaCalcPercent(Valor, Porcentagem: String; Calc: TcpCalc );
begin
  Application.CreateForm(TFmCalcPercent, FmCalcPercent);
  with FmCalcPercent do
  begin
    if Calc = cpMulta    then LaPorcent.Caption := '% Multa';
    if Calc = cpJurosMes then LaPorcent.Caption := '% Juros/m�s';
    EdValor.Text := Valor;
    EdPercent.Text := Porcentagem;
    ShowModal;
    Destroy;
  end;
end;

procedure TFmPrincipal.Padro3Click(Sender: TObject);
begin
  Geral.WriteAppKey('MenuStyle', Application.Title,
    TMenuItem(Sender).Tag, ktInteger, HKEY_LOCAL_MACHINE);
  SkinMenu(TMenuItem(Sender).Tag);
end;

procedure TFmPrincipal.PagarReceber1Click(Sender: TObject);
begin
  FinanceiroJan.ExtratosFinanceirosEmpresaUnica;
end;

procedure TFmPrincipal.PagarRolarEmissao(Query: TmySQLQuery);
begin
end;

procedure TFmPrincipal.DefineVarsCliInt(Empresa: Integer);
begin
  DmodG.QrCliIntUni.Close;
  DmodG.QrCliIntUni.Params[0].AsInteger := Empresa;
  DmodG.QrCliIntUni.Open;
  //
  FEntInt := DmodG.QrCliIntUniCodigo.Value;
  VAR_LIB_EMPRESAS := FormatFloat('0', DmodG.QrCliIntUniCodigo.Value);
  VAR_LIB_FILIAIS  := '';
  //
{:::
  DmodFin.QrCarts.Close;
  DmodFin.QrLctos.Close;
}
end;

procedure TFmPrincipal.MostraLogoff();
begin
  FmPrincipal.Enabled := False;
  //
  FmDControl_Dmk.Show;
  FmDControl_Dmk.EdLogin.Text := '';
  FmDControl_Dmk.EdSenha.Text := '';
  FmDControl_Dmk.EdLogin.SetFocus;
end;

procedure TFmPrincipal.MostraMatriz;
begin
  if DBCheck.CriaFm(TFmMatriz, FmMatriz, afmoSoBoss) then
  begin
    FmMatriz.ShowModal;
    FmMatriz.Destroy;
  end;
end;

procedure TFmPrincipal.MostraModulos;
begin
  if DBCheck.CriaFm(TFmModulos, FmModulos, afmoNegarComAviso) then
  begin
    FmModulos.ShowModal;
    FmModulos.Destroy;
  end;
end;

procedure TFmPrincipal.MostraOpcoes;
begin
  if DBCheck.CriaFm(TFmOpcoes, FmOpcoes, afmoNegarComAviso) then
  begin
    FmOpcoes.ShowModal;
    FmOpcoes.Destroy;
  end;
end;

procedure TFmPrincipal.MostraOpcoesDControl;
begin
  if DBCheck.CriaFm(TFmOpcoesDControl, FmOpcoesDControl, afmoNegarComAviso) then
  begin
    FmOpcoesDControl.ShowModal;
    FmOpcoesDControl.Destroy;
  end;
end;

procedure TFmPrincipal.Mostrardesktop1Click(Sender: TObject);
begin
  Application.Minimize;
end;

function TFmPrincipal.ZipaArquivo(
var Caminho: String; const NovoNome: String;
  Encryptar: Boolean; Senha: String; Deletar: Boolean): Boolean;
var
  Form: TForm;
  CamZip: String;
begin
  CamZip := '';
  //
  Form := MyObjects.FormTDICria(TFmZForge, PageControl1, AdvToolBarPager1, True, True);
  //
  CamZip := TFmZForge(Form).ZipaArquivo(zftArquivo, ExtractFileDir(Caminho), Caminho,
              Geral.SemAcento(NovoNome), Senha, Encryptar, Deletar);

  if Form <> nil then
    MyObjects.FormTDIFecha(Form, TTabSheet(TFmZForge(Form).CSTabSheetChamou.Component));
  //
  if Length(CamZip) > 0 then
  begin
    Caminho := CamZip;
    Result  := True;
  end
  else
    Result := False;
end;

procedure TFmPrincipal.MostraWSincro;
begin
  DmkWeb2_Jan.MostraWSincro(CO_DMKID_APP, CO_Versao,
    DModG.QrMasterHabilModulos.Value, DmodG.QrWebParams, Dmod.MyDB, Dmod.MyDBn,
    [], [], VAR_VERIFI_DB_CANCEL);
end;

procedure TFmPrincipal.Movimento1Click(Sender: TObject);
begin
  FinanceiroJan.MostraMovimento(0);
end;

procedure TFmPrincipal.NotificacoesJanelas(TipoNotifi: TNotifi;
  DataHora: TDateTime);
begin
  //Fazer
end;

function TFmPrincipal.NotificacoesVerifica(QueryNotifi: TmySQLQuery;
  DataHora: TDateTime): Boolean;
begin
  Result := True;
  //Fazer
end;

function TFmPrincipal.ObtemNomeStatusAplicativo: TStrings;
var
  Status: TStringList;
begin
  Status := TStringList.Create;
  //
  try
    Status.Add('Bloqueado');
    Status.Add('Monitorado');
    Status.Add('Vencido');
    Status.Add('Alugado');
    Status.Add('Liberado');
    Status.Add('N�o definido');
    //
    Result := Status;
  except
    Result := Status;
    Status.Free;
  end;
end;

procedure TFmPrincipal.iposHistAlteraes1Click(Sender: TObject);
begin
  UnVersoes_Jan.MostraFormWHisAltTip();
end;

procedure TFmPrincipal.JanelasWEB1Click(Sender: TObject);
begin
  UWUsersJan.MostraWPerfJan(False);
end;

procedure TFmPrincipal.Reabrirtabelas1Click(Sender: TObject);
begin
  UnDmkDAC_PF.ReabrirtabelasFormAtivo(Sender);
end;

procedure TFmPrincipal.ReCaptionComponentesDeForm(Form: TForm);
begin
  // N�o usa ainda!
end;

procedure TFmPrincipal.CriaImpressaoDiversos(Indice: Integer);
begin
  if DBCheck.CriaFm(TFmFormularios, FmFormularios, afmoNegarComAviso) then
  begin
    FmFormularios.RGRelatorio.ItemIndex := Indice;
    FmFormularios.ShowModal;
    FmFormularios.Destroy;
  end;
end;

procedure TFmPrincipal.CriaMinhasEtiquetas();
begin
  if DBCheck.CriaFm(TFmMultiEtiq, FmMultiEtiq, afmoNegarComAviso) then
  begin
    FmMultiEtiq.ShowModal;
    FmMultiEtiq.Destroy;
  end;
end;

procedure TFmPrincipal.Emqualquerconta1Click(Sender: TObject);
begin
  FinanceiroJan.MostraPesquisaPorNveldoPLanodeContas(0);
end;

procedure TFmPrincipal.EntidadeAtivaDesativa(Entidade: Integer);
begin
  if DBCheck.CriaFm(TFmEntiAtivo, FmEntiAtivo, afmoNegarComAviso) then
  begin
    FmEntiAtivo.FEntidade := Entidade;
    FmEntiAtivo.ShowModal;
    FmEntiAtivo.Destroy;
  end;
end;

function TFmPrincipal.CalculaTextFontSize(FoNam: String; FoTam: Integer;
  ResTela: Double): Integer;
var
  I, Ini, tOri, tCal: Integer;
begin
  Result := FoTam;
  if ResTela < 100 then
    Ini := 1
  else
    Ini := FoTam;
  //
  Canvas.Font.Name := FoNam;
  Canvas.Font.Size := FoTam;
  tOri := Canvas.TextWidth(TextStringRows);
  for I := Ini to 1024 do
  begin
    Canvas.Font.Size := I;
    tCal := Canvas.TextWidth(TextStringRows);
    if (tCal / tOri * 100) > ResTela then
    begin
      Result := I - 1;
      Break;
    end;
  end;
end;

procedure TFmPrincipal.Calendrio1Click(Sender: TObject);
begin
  DmkWeb.MostraWCalendar(Dmod.MyDBn, DModG.QrWebParams);
end;

procedure TFmPrincipal.CamposdaNFe1Click(Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaAdmin then Exit;
  //
  if DBCheck.CriaFm(TFmNFeNewVer, FmNFeNewVer, afmoLiberado) then
  begin
    FmNFeNewVer.ShowModal;
    FmNFeNewVer.Destroy;
  end;
end;

procedure TFmPrincipal.frLFamiliasUserFunction(const Name: String; p1, p2,
  p3: Variant; var Val: Variant);
begin
  Val := MLAGeral.MyUserFunction(Name, p1, p2, p3, Val);
end;

procedure TFmPrincipal.Gruposdeassuntos1Click(Sender: TObject);
begin
  DModWOrdSer.MostraWOrdSerAssGru(0);
end;

procedure TFmPrincipal.Gruposdeatendentes1Click(Sender: TObject);
begin
  DModWOrdSer.MostraWOrdSerAteParGr(0);
end;

procedure TFmPrincipal.Resolues1Click(Sender: TObject);
begin
  DModWOrdSer.MostraWOrdSerSol(0);
end;

procedure TFmPrincipal.ResultadosMensais1Click(Sender: TObject);
begin
  FinanceiroJan.MostraResultadosMensais;
end;

procedure TFmPrincipal.AcoesIniciaisDeCadastroDeEntidades(Reference: TComponent;
Codigo: Integer; Grade: TStringGrid);
begin
   // Compatibilidade
end;

procedure TFmPrincipal.AcoesIniciaisDoAplicativo();
begin
  try
    Screen.Cursor := crHourGlass;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Criando Module Geral');
    if DModG <> nil then
    begin
      if not FAtualizouFavoritos then
      begin
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Criando favoritos');
        DModG.CriaFavoritos(AdvToolBarPager1, LaAviso1, LaAviso2, AdvGlowButton25, FmPrincipal);
        //
        FAtualizouFavoritos := True;
      end;
      //
      DefineVarsCliInt(FEntInt);
      DModG.ReopenEmpresas(VAR_USUARIO, 0);
      //
      DmkWeb2_Jan.ConfiguraDBn(Dmod.MyDB, Dmod.MyDBn, DmodG.QrWebParams);
      //
      UMyMod.VerificaFeriadosFuturos(TFmFeriados, FmFeriados);
      //
      //  Descanso
      MostraFormDescanso();
      //
      DmkWeb.ConfiguraAlertaWOrdSerApp(nil, nil, BalloonHint1);
      //
      TmVersao.Enabled := True;
      //
      DmodG.VerificaHorVerao();
      //
      DmkWeb.HistoricoDeAlteracoes(CO_DMKID_APP, CO_VERSAO, dtMostra);
      //
      UFixBugs.MostraFixBugs(['']);
    end;
  finally
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '');
    Screen.Cursor := crDefault;
  end;  
end;

procedure TFmPrincipal.ReceitaseDespesas1Click(Sender: TObject);
begin
  FinanceiroJan.MostraReceDesp2(0);
end;

function TFmPrincipal.RecriaTempTable(Tabela: String): Boolean;
begin
  Result := Ucriar.RecriaTempTable(Tabela, DModG.QrUpdPID1, False) <> '';
end;

procedure TFmPrincipal.reinamentos1Click(Sender: TObject);
begin
  if DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrWebParams, 1) then
  begin
    if DBCheck.CriaFm(TFmWTreiOpc, FmWTreiOpc, afmoNegarComAviso) then
    begin
      FmWTreiOpc.ShowModal;
      FmWTreiOpc.Destroy;
    end;
  end;
end;

procedure TFmPrincipal.Perfis2Click(Sender: TObject);
begin
  UWUsersJan.MostraWPerfis(0);
end;

procedure TFmPrincipal.Limpar1Click(Sender: TObject);
begin
  MyObjects.SelecionaLimpaImagemdefundoFormDescanso(FmPrincipal, TFmDescanso,
    PageControl1, True);
end;

procedure TFmPrincipal.ListadeWebSeriveces1Click(Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaAdmin then Exit;
  //
  if DBCheck.CriaFm(TFmNFeWebServicesGer, FmNFeWebServicesGer, afmoLiberado) then
  begin
    FmNFeWebServicesGer.ShowModal;
    FmNFeWebServicesGer.Destroy;
  end;
end;

procedure InfoWindow(Wnd: HWnd; Codigo, Item: Integer; Texto: String; WndID: Int64);
var
  WInfo: TWindowInfo;
  //p: TPoint;
  //Atom: TAtom;
{

  DWORD cbSize, ;
  RECT  rcWindow;
  RECT  rcClient;
  DWORD dwStyle;
  DWORD dwExStyle;
  DWORD dwWindowStatus;
  UINT  cxWindowBorders;
  UINT  cyWindowBorders;
  ATOM  atomWindowType;
  WORD  wCreatorVersion;  ATOM: TAtom;
}
  cbSize,
  rcWindowT, rcWindowE, rcWindowB,
  rcWindowD, rcClientT, rcClientE,
  rcClientB, rcClientD, dwStyle,
  dwExStyle, dwWindowStatus, cxWindowBorders,
  cyWindowBorders, atomWindowType: Integer;
begin
  WInfo.cbSize := SizeOf(TWindowInfo);
  GetWindowInfo(Wnd, WInfo);
  // Obter as coordenadas de tela, as coordenadas
{  T := WInfo.rcWindow.Top;
  E := WInfo.rcWindow.Left;
  B := WInfo.rcWindow.Bottom;
  D := WInfo.rcWindow.Right;
  ATom := WInfo.atomWindowType;
{
  Showmessage('canto superior esquerdo do ret�ngulo da �rea cliente, coordenar, X: ' +
               InttoStr(P.X) + 'Y:  '+
               InttoStr(P.Y) + '�');
}
  cbSize    := WInfo.cbSize;
  rcWindowT := WInfo.rcWindow.Top;
  rcWindowE := WInfo.rcWindow.Left;
  rcWindowB := WInfo.rcWindow.Bottom;
  rcWindowD := WInfo.rcWindow.Right;
  rcClientT := WInfo.rcClient.Top;
  rcClientE := WInfo.rcClient.Left;
  rcClientB := WInfo.rcClient.Bottom;
  rcClientD := WInfo.rcClient.Right;
  //
  dwStyle         := WInfo.dwStyle;
  dwExStyle       := WInfo.dwExStyle;
  dwWindowStatus  := WInfo.dwOtherStuff;
  cxWindowBorders := WInfo.cxWindowBorders;
  cyWindowBorders := WInfo.cyWindowBorders;
  atomWindowType  := WInfo.atomWindowType;
  //
  UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, 'janelas', False, [
  'WndID', 'Texto', 'cbSize',
  'rcWindowT', 'rcWindowE', 'rcWindowB',
  'rcWindowD', 'rcClientT', 'rcClientE',
  'rcClientB', 'rcClientD', 'dwStyle',
  'dwExStyle', 'dwWindowStatus', 'cxWindowBorders',
  'cyWindowBorders', 'atomWindowType'], [
  'Codigo', 'Item'], [
  WndID, Trim(Texto), cbSize,
  rcWindowT, rcWindowE, rcWindowB,
  rcWindowD, rcClientT, rcClientE,
  rcClientB, rcClientD, dwStyle,
  dwExStyle, dwWindowStatus, cxWindowBorders,
  cyWindowBorders, atomWindowType], [
  Codigo, Item], False);
end;

(* N�o usa
function EnumChildWindowsProc(Wnd: HWnd; Form: TFmGetJanelas): Bool; export;
  {$ifdef Win32} stdcall; {$endif}
var
  Buffer: array[0..99] of Char;
  I: Int64;
//  T, E, B, D: Integer;
begin
  GetWindowText(Wnd, Buffer, 100);
  if StrLen(Buffer) <> 0 then
  begin
    FmPrincipal.FSeqItm := FmPrincipal.FSeqItm + 1;
    InfoWindow(Wnd, FmPrincipal.FSeqJan, FmPrincipal.FSeqItm, Buffer, 0);
    //FmPrincipal.ListBox7.Items.Add(StrPas(Buffer) + '(T=' + IntToStr(T) + ', E=' + IntToStr(E) + ', B=' + IntToStr(B) + ', D=' + IntToStr(D) + ')');
    I := GetWindow(Wnd, GW_CHILD);
    if I <> 0 then
    begin
      PNode := CNode;
      EnumChildWindows(Wnd, @EnumChildWindowsProc, 0);
    end;
  end;
  Result := True;
end;
*)

(* N�o usa
function EnumWindowsProc(Wnd: HWnd; Form: TForm): Boolean; Export; StdCall;
var
  Buffer: array[0..99] of char;
//  T, E, B, D: Integer;
begin
  GetWindowText(Wnd, Buffer, 100);
  if StrLen(Buffer) <> 0 then
  begin
    FmPrincipal.FSeqItm := 0;
    FmPrincipal.FSeqJan := FmPrincipal.FSeqJan + 1;
    InfoWindow(Wnd, FmPrincipal.FSeqJan, 0, Buffer, 0);
    //FmPrincipal.ListBox5.Items.Add(StrPas(Buffer) + '(T=' + IntToStr(T) + ', E=' + IntToStr(E) + ', B=' + IntToStr(B) + ', D=' + IntToStr(D) + ')');
    EnumChildWindows(Wnd, @EnumChildWindowsProc, 0);
  end;
  Result := True;
end;
*)

procedure TFmPrincipal.SalvaArquivo(EdNomCli, EdCPFCli: TEdit; Grade:
  TStringGrid; Data: TDateTime; FileName: String; ChangeData: Boolean);
begin
  //MyCBase.SalvaArquivo(EdNomCli, EdCPFCli, Grade, Data, VAR_DBPATH+FileName, ChangeData);
end;

procedure TFmPrincipal.sd1FormSkin(Sender: TObject; aName: string;
  var DoSkin: Boolean);
begin
  if aName = VAR_FORMTDI_NAME then
    DoSkin := False
  else if aname = 'TFmWOrdSerConversas' then
    doskin := False
  else if aname = 'TFmEventos' then
    doskin := False;
end;

function TFmPrincipal.AbreArquivoINI(Grade: TStringGrid; EdNomCli, EdCPFCli: TEdit;
  Data: TDateTime; Arquivo: String): Boolean;
begin
  Result := False;
  //MyCBase.AbreArquivoINI(Grade, EdNomCli, EdCPFCli, Data, Arquivo);
end;

procedure TFmPrincipal.RetornoCNAB;
begin
  // Compatibilidade
end;

procedure TFmPrincipal.ShowHint(Sender: TObject);
begin
end;

procedure TFmPrincipal.SkinMenu(Index: integer);
begin
  case Index of
    0: AdvToolBarOfficeStyler1.Style := bsOffice2003Blue;
    1: AdvToolBarOfficeStyler1.Style := bsOffice2003Classic;
    2: AdvToolBarOfficeStyler1.Style := bsOffice2003Olive;
    3: AdvToolBarOfficeStyler1.Style := bsOffice2003Silver;
    4: AdvToolBarOfficeStyler1.Style := bsOffice2007Luna;
    5: AdvToolBarOfficeStyler1.Style := bsOffice2007Obsidian;
    6: AdvToolBarOfficeStyler1.Style := bsOffice2007Silver;
    7: AdvToolBarOfficeStyler1.Style := bsOfficeXP;
    8: AdvToolBarOfficeStyler1.Style := bsWhidbeyStyle;
    9: AdvToolBarOfficeStyler1.Style := bsWindowsXP;
  end;
end;

procedure TFmPrincipal.Status1Click(Sender: TObject);
begin
  DModWOrdSer.MostraWOrdSerSta(0);
end;

procedure TFmPrincipal.Suporte1Click(Sender: TObject);
begin
  DmkWeb.MostraWSuporte(False, 2, BalloonHint1);
end;

function TFmPrincipal.CadastroDeContasSdoSimples(Entidade,
  Conta: Integer): Boolean;
begin
  Result := True;
end;

procedure TFmPrincipal.CadastroDeContratos(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmContratos, FmContratos, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then FmContratos.LocCod(Codigo, Codigo);
    FmContratos.ShowModal;
    FmContratos.Destroy;
  end;
end;

function TFmPrincipal.AcaoEspecificaDeApp(Servico: String): Boolean;
begin
  Result := True;
  //Compatibilidade
end;

procedure TFmPrincipal.AcoesExtrasDeCadastroDeEntidades(Grade1: TStringGrid;
  Entidade: Integer; Aba: Boolean = False);
begin
 //Compatibilidade
end;

procedure TFmPrincipal.MenuItem10Click(Sender: TObject);
begin
  WTextos_Jan.MostraFormWTermos(FmPrincipal, 0);
end;

procedure TFmPrincipal.MenuItem1Click(Sender: TObject);
begin
  SelecionaImagemdefundo;
end;

procedure TFmPrincipal.MenuItem20Click(Sender: TObject);
begin
  ALL_Jan.MostraFormVerifiDB(False);
end;

procedure TFmPrincipal.MenuItem21Click(Sender: TObject);
begin
  DmkWeb2_Jan.MostraVerifiDBi(Dmod.MyDBn, DmodG.QrWebParams, False);
end;

procedure TFmPrincipal.Modos1Click(Sender: TObject);
begin
  DModWOrdSer.MostraWOrdSerMod(0);
end;

procedure TFmPrincipal.MostraAnotacoes;
begin
  if DBCheck.CriaFm(TFmAnotacoes, FmAnotacoes, afmoNegarComAviso) then
  begin
    FmAnotacoes.ShowModal;
    FmAnotacoes.Destroy;
  end;
end;

procedure TFmPrincipal.MostraAplicModul(Aplicativo: Integer);
begin
  if DBCheck.CriaFm(TFmAplicModul, FmAplicModul, afmoNegarComAviso) then
  begin
    if Aplicativo <> 0 then
      FmAplicModul.FAplicativo := Aplicativo;
    FmAplicModul.ShowModal;
    FmAplicModul.Destroy;
  end;
end;

procedure TFmPrincipal.MostraBackupDir(AbrirEmAba: Boolean);
begin
  if AbrirEmAba then
  begin
    if FmBackupDir = nil then
      MyObjects.FormTDICria(TFmBackupDir, PageControl1, AdvToolBarPager1, False, True);
  end else
  begin
    if DBCheck.CriaFm(TFmBackupDir, FmBackupDir, afmoNegarComAviso) then
    begin
      FmBackupDir.ShowModal;
      FmBackupDir.Destroy;
      //
      FmBackupDir := nil;
    end;
  end;
end;

procedure TFmPrincipal.MostraConDef;
begin
  if DBCheck.CriaFm(TFmConDef, FmConDef, afmoNegarComAviso) then
  begin
    FmConDef.ShowModal;
    FmConDef.Destroy;
  end;
end;

procedure TFmPrincipal.MostraContasMySQL(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmMySQLConf, FmMySQLConf, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmMySQLConf.LocCod(Codigo, Codigo);
    FmMySQLConf.ShowModal;
    FmMySQLConf.Destroy;
  end;
end;

procedure TFmPrincipal.MostraEnquete;
begin
  if DBCheck.CriaFm(TFmEnquete, FmEnquete, afmoNegarComAviso) then
  begin
    FmEnquete.ShowModal;
    FmEnquete.Destroy;
  end;
end;

procedure TFmPrincipal.MostraFAQ;
var
  Link: String;
begin
  if (DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrWebParams, 1)) and
    (DmkWeb.ConectarUsuarioWEB(True)) then
  begin
    Link := 'http://www.dermatek.net.br/?page=wfaq&token=' + VAR_WEB_IDLOGIN;
    DmkWeb.MostraWebBrowser(Link, True, False, 0, 0);
  end;
end;

procedure TFmPrincipal.MostraFeriados;
begin
  if DBCheck.CriaFm(TFmFeriados, FmFeriados, afmoNegarComAviso) then
  begin
    FmFeriados.ShowModal;
    FmFeriados.Destroy;
  end;
end;

procedure TFmPrincipal.MostraFiliais;
begin
  if DBCheck.CriaFm(TFmParamsEmp, FmParamsEmp, afmoSoBoss) then
  begin
    FmParamsEmp.ShowModal;
    FmParamsEmp.Destroy;
  end;
end;

end.

