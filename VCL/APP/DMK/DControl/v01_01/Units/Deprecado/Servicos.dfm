object FmServicos: TFmServicos
  Left = 368
  Top = 194
  ActiveControl = SbImprime
  Caption = 'CAD-SERVI-001 :: Cadastro de servi'#231'os'
  ClientHeight = 226
  ClientWidth = 712
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 48
    Width = 712
    Height = 178
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Visible = False
    object Label10: TLabel
      Left = 17
      Top = 51
      Width = 31
      Height = 13
      Caption = 'Nome:'
    end
    object Label9: TLabel
      Left = 17
      Top = 10
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
    end
    object Label4: TLabel
      Left = 491
      Top = 51
      Width = 27
      Height = 13
      Caption = 'Valor:'
    end
    object PainelConfirma: TPanel
      Left = 1
      Top = 129
      Width = 710
      Height = 48
      Align = alBottom
      TabOrder = 5
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 8
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
        NumGlyphs = 2
      end
      object Panel1: TPanel
        Left = 601
        Top = 1
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
          NumGlyphs = 2
        end
      end
    end
    object EdNome: TdmkEdit
      Left = 17
      Top = 67
      Width = 469
      Height = 21
      MaxLength = 50
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Nome'
      UpdCampo = 'Nome'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      OnExit = EdNomeExit
    end
    object CkContinuar: TCheckBox
      Left = 73
      Top = 94
      Width = 112
      Height = 17
      Caption = 'Continuar inserindo.'
      TabOrder = 4
    end
    object EdCodigo: TdmkEdit
      Left = 17
      Top = 25
      Width = 71
      Height = 21
      Alignment = taRightJustify
      Color = clInactiveCaption
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBackground
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Codigo'
      UpdCampo = 'Codigo'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
    object dmkCkAtivo: TdmkCheckBox
      Left = 17
      Top = 94
      Width = 48
      Height = 17
      Caption = 'Ativo'
      TabOrder = 3
      QryCampo = 'Ativo'
      UpdCampo = 'Ativo'
      UpdType = utYes
      ValCheck = #0
      ValUncheck = #0
      OldValor = #0
    end
    object EdValor: TdmkEdit
      Left = 491
      Top = 67
      Width = 100
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'Valor'
      UpdCampo = 'Valor'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      OnExit = EdNomeExit
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 712
    Height = 178
    Align = alClient
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object PainelControle: TPanel
      Left = 1
      Top = 129
      Width = 710
      Height = 48
      Align = alBottom
      TabOrder = 0
      object LaRegistro: TdmkLabel
        Left = 173
        Top = 1
        Width = 67
        Height = 46
        Align = alClient
        Caption = '[N]: 0'
        UpdType = utYes
        SQLType = stNil
        ExplicitWidth = 26
        ExplicitHeight = 13
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 172
        Height = 46
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
          NumGlyphs = 2
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
          NumGlyphs = 2
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
          NumGlyphs = 2
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
          NumGlyphs = 2
        end
      end
      object Panel3: TPanel
        Left = 240
        Top = 1
        Width = 469
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object Panel2: TPanel
          Left = 360
          Top = 0
          Width = 109
          Height = 46
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
            NumGlyphs = 2
          end
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtIncluiClick
          NumGlyphs = 2
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtAlteraClick
          NumGlyphs = 2
        end
        object BtExclui: TBitBtn
          Tag = 12
          Left = 187
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          Enabled = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          NumGlyphs = 2
        end
      end
    end
    object PainelData: TPanel
      Left = 1
      Top = 1
      Width = 710
      Height = 112
      Align = alTop
      Enabled = False
      TabOrder = 1
      object Label2: TLabel
        Left = 6
        Top = 48
        Width = 31
        Height = 13
        Caption = 'Nome:'
      end
      object Label1: TLabel
        Left = 6
        Top = 7
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label3: TLabel
        Left = 479
        Top = 48
        Width = 27
        Height = 13
        Caption = 'Valor:'
      end
      object dmkDBEdit1: TdmkDBEdit
        Left = 6
        Top = 22
        Width = 72
        Height = 21
        DataField = 'Codigo'
        DataSource = DsServicos
        TabOrder = 0
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit2: TdmkDBEdit
        Left = 6
        Top = 63
        Width = 469
        Height = 21
        DataField = 'Nome'
        DataSource = DsServicos
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object CkAtivo: TDBCheckBox
        Left = 6
        Top = 89
        Width = 62
        Height = 17
        Caption = 'Ativo'
        DataField = 'Ativo'
        DataSource = DsServicos
        TabOrder = 2
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object dmkDBEdit3: TdmkDBEdit
        Left = 479
        Top = 63
        Width = 100
        Height = 21
        DataField = 'Valor'
        DataSource = DsServicos
        TabOrder = 3
        UpdType = utYes
        Alignment = taLeftJustify
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 712
    Height = 48
    Align = alTop
    Caption = '                              Cadastro de servi'#231'os'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 0
    object LaTipo: TdmkLabel
      Left = 629
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 708
      ExplicitTop = 2
      ExplicitHeight = 44
    end
    object Image1: TImage
      Left = 226
      Top = 1
      Width = 403
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 221
      ExplicitWidth = 401
      ExplicitHeight = 44
    end
    object PanelFill2: TPanel
      Left = 1
      Top = 1
      Width = 225
      Height = 46
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 0
        NumGlyphs = 2
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 1
        NumGlyphs = 2
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 2
        OnClick = SbNumeroClick
        NumGlyphs = 2
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 3
        OnClick = SbNomeClick
        NumGlyphs = 2
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 4
        OnClick = SbQueryClick
        NumGlyphs = 2
      end
    end
  end
  object DsServicos: TDataSource
    DataSet = QrServicos
    Left = 644
    Top = 13
  end
  object QrServicos: TmySQLQuery
    Database = DModG.DBDmk
    BeforeOpen = QrServicosBeforeOpen
    AfterOpen = QrServicosAfterOpen
    SQL.Strings = (
      'SELECT * '
      'FROM servicos'
      'WHERE Codigo > 0')
    Left = 616
    Top = 13
    object QrServicosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrServicosNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrServicosValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrServicosLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrServicosDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrServicosDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrServicosUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrServicosUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrServicosAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrServicosAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrLocNome: TmySQLQuery
    Database = DModG.DBDmk
    SQL.Strings = (
      'SELECT Nome '
      'FROM servicos'
      'WHERE Nome LIKE :P0'
      'AND Ativo = 1')
    Left = 588
    Top = 13
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocNomeNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtAltera
    CanDel01 = BtExclui
    Left = 308
    Top = 13
  end
end
