unit Servicos;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, Menus, Grids,
  DBGrids, dmkDBGridDAC, dmkDBGrid, dmkEdit, dmkLabel, dmkGeral, dmkDBEdit,
  ColorGrd, ActnColorMaps, ActnMan, dmkCheckBox, dmkValUsu, dmkPermissoes,
  UnDmkProcFunc;

type
  TFmServicos = class(TForm)
    DsServicos: TDataSource;
    QrServicos: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TdmkLabel;
    Image1: TImage;
    PanelFill2: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelDados: TPanel;
    PainelControle: TPanel;
    LaRegistro: TdmkLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    PainelData: TPanel;
    Label2: TLabel;
    Label1: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    dmkDBEdit2: TdmkDBEdit;
    PainelEdita: TPanel;
    Label10: TLabel;
    Label9: TLabel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    EdNome: TdmkEdit;
    CkContinuar: TCheckBox;
    EdCodigo: TdmkEdit;
    QrLocNome: TmySQLQuery;
    dmkCkAtivo: TdmkCheckBox;
    CkAtivo: TDBCheckBox;
    QrLocNomeNome: TWideStringField;
    dmkPermissoes1: TdmkPermissoes;
    dmkDBEdit3: TdmkDBEdit;
    Label3: TLabel;
    QrServicosCodigo: TIntegerField;
    QrServicosNome: TWideStringField;
    QrServicosValor: TFloatField;
    QrServicosLk: TIntegerField;
    QrServicosDataCad: TDateField;
    QrServicosDataAlt: TDateField;
    QrServicosUserCad: TIntegerField;
    QrServicosUserAlt: TIntegerField;
    QrServicosAlterWeb: TSmallintField;
    QrServicosAtivo: TSmallintField;
    EdValor: TdmkEdit;
    Label4: TLabel;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrServicosAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrServicosBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure EdNomeExit(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure MostraEdicao(Mostra : Boolean; Status : String; Codigo : Integer);
    //
  public
    { Public declarations }
  end;

var
  FmServicos: TFmServicos;
const
  FFormatFloat = '00000';

implementation

uses Module, UnMyObjects;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmServicos.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmServicos.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrServicosCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmServicos.DefParams;
begin
  VAR_GOTOTABELA := 'servicos';
  VAR_GOTOMYSQLTABLE := QrServicos;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM servicos');
  VAR_SQLx.Add('WHERE Codigo <> 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmServicos.EdNomeExit(Sender: TObject);
begin
  if LaTipo.Caption = CO_INCLUSAO then
  begin
    QrLocNome.SQL.Clear;
    QrLocNome.SQL.Add('SELECT Nome FROM servicos WHERE Nome LIKE :P0');
    QrLocNome.Params[00].AsString := EdNome.ValueVariant;
    QrLocNome.Open;
    if QrLocNome.RecordCount > 0 then
    begin
      Geral.MensagemBox('Este servi�o j� foi cadastrado.', 'Aviso', MB_OK+MB_ICONWARNING);
      EdNome.SetFocus;
      Exit;
    end;
  end;
end;

procedure TFmServicos.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmServicos.QueryPrincipalAfterOpen;
begin
end;

procedure TFmServicos.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmServicos.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmServicos.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmServicos.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmServicos.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmServicos.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrServicosCodigo.Value;
  Close;
end;

procedure TFmServicos.BtConfirmaClick(Sender: TObject);
var
  Codigo : Integer;
  Nome : String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Geral.MensagemBox('Defina uma descri��o.', 'Aviso', MB_OK+MB_ICONWARNING);
    EdNome.SetFocus;
    Exit;
  end;
  Codigo := UMyMod.BuscaEmLivreY_Def('servicos', 'Codigo', LaTipo.SQLType,
    QrServicosCodigo.Value);
  if UMyMod.ExecSQLInsUpdFm(FmServicos, LaTipo.SQLType, 'servicos', Codigo,
  Dmod.QrUpd) then
  begin
    if CkContinuar.Checked then
    begin
      Geral.MensagemBox('Servi�o cadastrado com sucesso!', 'Aviso', MB_OK+MB_ICONWARNING);
      LaTipo.SQLType := stIns;
      MostraEdicao(True, CO_INCLUSAO, 0);
    end else
      MostraEdicao(False, CO_TRAVADO, 0);
      LocCod(Codigo, Codigo);
  end;
end;

procedure TFmServicos.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if LaTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'servicos', Codigo);
  MostraEdicao(False, CO_TRAVADO, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'servicos', 'Codigo');
end;

procedure TFmServicos.FormCreate(Sender: TObject);
begin
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  CriaOForm;
  //
end;

procedure TFmServicos.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrServicosCodigo.Value,LaRegistro.Caption);
end;

procedure TFmServicos.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmServicos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmServicos.QrServicosAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmServicos.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmServicos.SbQueryClick(Sender: TObject);
begin
  LocCod(QrServicosCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'servicos', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmServicos.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmServicos.QrServicosBeforeOpen(DataSet: TDataSet);
begin
  QrServicosCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmServicos.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdita, QrServicos, [PainelDados],
  [PainelEdita], EdNome, LaTipo, 'servicos');
  //
  dmkCkAtivo.Checked  := True;
  CkContinuar.Visible := True;
end;

procedure TFmServicos.BtAlteraClick(Sender: TObject);
begin
  if QrServicosCodigo.Value > 0 then
  begin
    UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrServicos, [PainelDados],
    [PainelEdita], EdNome, LaTipo, 'servicos');
    //
    dmkCkAtivo.Checked := MLAGeral.IntToBool(QrServicosAtivo.Value);
    CkContinuar.Visible := False;
    CkContinuar.Checked := False;
  end;
end;

procedure TFmServicos.MostraEdicao(Mostra : Boolean; Status : String; Codigo : Integer);
begin
  if Mostra then
  begin
    PainelDados.Visible := False;
    PainelEdita.Visible := True;
    if Status = CO_INCLUSAO then
    begin
      EdCodigo.ValueVariant := FormatFloat('000', Codigo);
      EdNome.ValueVariant   := '';
      EdValor.ValueVariant  := 0;
      dmkCkAtivo.Checked    := true;
      //
      EdNome.SetFocus;
    end else begin
      EdCodigo.ValueVariant := QrServicosCodigo.Value;
      EdNome.ValueVariant   := QrServicosNome.Value;
      EdValor.ValueVariant  := QrServicosValor.Value;
      dmkCkAtivo.Checked    := MLAGeral.IntToBool(QrServicosAtivo.Value);
      //
      EdNome.SetFocus;
    end;
  end else begin
    PainelDados.Visible := True;
    PainelEdita.Visible := False;
  end;
  LaTipo.Caption := Status;
  GOTOy.BotoesSb(LaTipo.Caption);
end;

end.
