object FmDControl_Dmk: TFmDControl_Dmk
  Left = 399
  Top = 261
  Caption = 'DControl'
  ClientHeight = 232
  ClientWidth = 471
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnHide = FormHide
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 471
    Height = 100
    Align = alClient
    Caption = ' Defini'#231#245'es do perfil: '
    TabOrder = 0
    object Label3: TLabel
      Left = 49
      Top = 26
      Width = 43
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Terminal:'
    end
    object Label1: TLabel
      Left = 105
      Top = 26
      Width = 29
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Login:'
    end
    object Label2: TLabel
      Left = 238
      Top = 26
      Width = 34
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Senha:'
    end
    object Label4: TLabel
      Left = 369
      Top = 26
      Width = 44
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Empresa:'
    end
    object LaSenhas: TLabel
      Left = 109
      Top = 67
      Width = 80
      Height = 13
      Cursor = crHandPoint
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Gerenciar Senha'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      StyleElements = [seClient, seBorder]
      OnClick = LaSenhasClick
      OnMouseEnter = LaSenhasMouseEnter
      OnMouseLeave = LaSenhasMouseLeave
    end
    object LaConexao: TLabel
      Left = 243
      Top = 67
      Width = 96
      Height = 13
      Cursor = crHandPoint
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Gerenciar Conex'#245'es'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      StyleElements = [seClient, seBorder]
      OnClick = LaConexaoClick
      OnMouseEnter = LaConexaoMouseEnter
      OnMouseLeave = LaConexaoMouseLeave
    end
    object EdTerminal: TEdit
      Left = 49
      Top = 42
      Width = 52
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      Color = clBtnFace
      Constraints.MinHeight = 21
      ReadOnly = True
      TabOrder = 0
      OnKeyDown = EdLoginKeyDown
    end
    object EdLogin: TEdit
      Left = 105
      Top = 42
      Width = 129
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Constraints.MinHeight = 21
      Font.Charset = SYMBOL_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      PasswordChar = 'l'
      TabOrder = 1
      OnKeyDown = EdLoginKeyDown
    end
    object EdSenha: TEdit
      Left = 238
      Top = 42
      Width = 129
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Constraints.MinHeight = 21
      Font.Charset = SYMBOL_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      PasswordChar = 'l'
      TabOrder = 2
      OnExit = EdSenhaExit
      OnKeyDown = EdSenhaKeyDown
    end
    object EdEmpresa: TEdit
      Left = 369
      Top = 42
      Width = 48
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Constraints.MinHeight = 21
      Font.Charset = SYMBOL_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      PasswordChar = 'l'
      TabOrder = 3
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 100
    Width = 471
    Height = 63
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 467
      Height = 46
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 12
        Top = 2
        Width = 76
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 11
        Top = 1
        Width = 76
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object ProgressBar1: TProgressBar
        Left = 0
        Top = 26
        Width = 467
        Height = 20
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 163
    Width = 471
    Height = 69
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 317
      Top = 15
      Width = 152
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtDesiste: TBitBtn
        Tag = 15
        Left = 15
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtDesisteClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 315
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtEntra: TBitBtn
        Tag = 14
        Left = 15
        Top = 4
        Width = 120
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Confirma'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtEntraClick
      end
    end
  end
end
