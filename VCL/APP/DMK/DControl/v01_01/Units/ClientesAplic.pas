unit ClientesAplic;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, dmkPermissoes, mySQLDbTables,
  dmkCheckBox, dmkRadioGroup, dmkEditDateTimePicker, dmkDBLookupComboBox,
  dmkEditCB, dmkEdit, Mask, dmkDBEdit, Variants, UnDmkEnums, DmkDAC_PF;

type
  TFmClientesAplic = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    DsAplicativos: TDataSource;
    QrAplicativos: TmySQLQuery;
    QrAplicativosCodigo: TIntegerField;
    QrAplicativosNome: TWideStringField;
    EdControle: TdmkEdit;
    Label3: TLabel;
    Label9: TLabel;
    EdAplicativo: TdmkEditCB;
    CBAplicativo: TdmkDBLookupComboBox;
    SBAplicativo: TSpeedButton;
    RGStatAplic: TdmkRadioGroup;
    LaQtdLicen: TLabel;
    EdQtdLicen: TdmkEdit;
    TPFimData: TdmkEditDateTimePicker;
    Label1: TLabel;
    CkAtivo: TdmkCheckBox;
    EdQtdCliInt: TdmkEdit;
    LaQtdCliInt: TLabel;
    CkBeta: TdmkCheckBox;
    EdQtdUsuarios: TdmkEdit;
    LaQtdUsuarios: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SBAplicativoClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EdQtdCliIntEnter(Sender: TObject);
    procedure EdQtdCliIntExit(Sender: TObject);
  private
    { Private declarations }
    procedure MostraEdicao(SQLType: TSQLType; Query: TMySQLQuery);
  public
    { Public declarations }
    FSQLType: TSQLType;
    FQuery: TMySQLQuery;
    FCodigo, FControle: Integer;
    FModulo: Boolean;
  end;

  var
  FmClientesAplic: TFmClientesAplic;

implementation

uses UnMyObjects, Module, Principal, ModuleGeral, UMySQLModule, UnFTPJan,
  UnDmkWeb, MyListas, UnitVersoes;

{$R *.DFM}

procedure TFmClientesAplic.BtOKClick(Sender: TObject);
var
  Controle, Aplicativo, StatAplic, QtdLicen, QtdUsuarios, QtdCliInt, Beta,
  Ativo: Integer;
  FimData: TDate;
  AplicativoTxt: String;
begin
  Aplicativo    := EdAplicativo.ValueVariant;
  AplicativoTxt := QrAplicativosNome.Value;
  StatAplic     := RGStatAplic.ItemIndex;
  QtdLicen      := EdQtdLicen.ValueVariant;
  QtdUsuarios   := EdQtdUsuarios.ValueVariant;
  QtdCliInt     := EdQtdCliInt.ValueVariant;
  FimData       := TPFimData.Date;
  Beta          := Geral.BoolToInt(CkBeta.Checked);
  Ativo         := Geral.BoolToInt(CkAtivo.Checked);
  //
  Controle := UnVersoes.App_InsUpdCliAplic(FCodigo, FControle, Aplicativo,
              StatAplic, QtdLicen, QtdUsuarios, QtdCliInt, Beta, Ativo, FimData,
              AplicativoTxt, Dmod.MyDB, Dmod.QrUpd, EdAplicativo, RGStatAplic);
  //
  if Controle <> 0 then
  begin
    FControle := Controle;
    //
    if ImgTipo.SQLType = stIns then
      FModulo := True;
    Close;
  end;
end;

procedure TFmClientesAplic.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := FControle;
  Close;
end;

procedure TFmClientesAplic.EdQtdCliIntEnter(Sender: TObject);
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Para n�o controlar por cliente interno deixe o campo zerado!');
end;

procedure TFmClientesAplic.EdQtdCliIntExit(Sender: TObject);
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmClientesAplic.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmClientesAplic.FormCreate(Sender: TObject);
var
  Status: TStrings;
begin
  ImgTipo.SQLType := stLok;
  //
  CBAplicativo.ListSource := DsAplicativos;
  //
  Status              := FmPrincipal.ObtemNomeStatusAplicativo;
  RGStatAplic.Items   := Status;
  RGStatAplic.Columns := Status.Count;
  FModulo             := False;
end;

procedure TFmClientesAplic.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmClientesAplic.FormShow(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
    UnVersoes.App_ReopenAplicativos(CO_App_TipAmb_Int[0], Dmod.MyDB, QrAplicativos,
      'AND Codigo NOT IN (SELECT Aplicativo FROM cliaplic WHERE Codigo=' +
        Geral.FF0(FCodigo) +')')
  else
    UnVersoes.App_ReopenAplicativos(CO_App_TipAmb_Int[0], Dmod.MyDB, QrAplicativos, '');
  //
  MostraEdicao(FSQLType, FQuery);
end;

procedure TFmClientesAplic.MostraEdicao(SQLType: TSQLType; Query: TMySQLQuery);
begin
  if SQLType = stIns then
  begin
    EdControle.ValueVariant   := 0;
    EdAplicativo.ValueVariant := 0;
    CBAplicativo.KeyValue     := Null;
    RGStatAplic.ItemIndex     := -1;
    EdQtdLicen.ValueVariant   := 0;
    EdQtdCliInt.ValueVariant  := 0;
    TPFimData.Date            := 0;
    CkAtivo.Checked           := True;
    CkBeta.Checked            := False;
  end else
  begin
    EdControle.ValueVariant   := Query.FieldByName('Controle').AsInteger;
    EdAplicativo.ValueVariant := Query.FieldByName('Aplicativo').AsInteger;
    CBAplicativo.KeyValue     := Query.FieldByName('Aplicativo').AsInteger;
    RGStatAplic.ItemIndex     := Query.FieldByName('StatAplic').AsInteger;
    EdQtdLicen.ValueVariant   := Query.FieldByName('QtdLicen').AsInteger;
    EdQtdCliInt.ValueVariant  := Query.FieldByName('QtdCliInt').AsInteger;
    TPFimData.Date            := Query.FieldByName('FimData').AsDateTime;
    CkAtivo.Checked           := Geral.IntToBool(Query.FieldByName('Ativo').AsInteger);
    CkBeta.Checked            := Geral.IntToBool(Query.FieldByName('Beta').AsInteger);
  end;
  ImgTipo.SQLType := SQLType;
  //
  EdAplicativo.SetFocus;
end;

procedure TFmClientesAplic.SBAplicativoClick(Sender: TObject);
var
  Aplic: Integer;
begin
  VAR_CADASTRO := 0;
  Aplic        := EdAplicativo.ValueVariant;
  //
  FmPrincipal.CadastrodeAplicativos(Aplic);
  //
  if VAR_CADASTRO > 0 then
    UMyMod.SetaCodigoPesquisado(EdAplicativo, CBAplicativo, QrAplicativos, VAR_CADASTRO);
end;

end.
