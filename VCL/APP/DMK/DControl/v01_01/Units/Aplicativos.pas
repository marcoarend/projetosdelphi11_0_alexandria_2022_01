unit Aplicativos;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, dmkDBLookupComboBox, dmkEditCB, dmkMemo, dmkCheckBox,
  UnDmkEnums, Vcl.Menus, frxClass, frxDBSet, DmkDAC_PF, dmkCheckGroup;

type
  TFmAplicativos = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrAplicativos: TmySQLQuery;
    QrAplicativosCodigo: TIntegerField;
    QrAplicativosNome: TWideStringField;
    DsAplicativos: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    EdSigla: TdmkEdit;
    Label5: TLabel;
    MeDescri: TdmkMemo;
    Label6: TLabel;
    dmkCkAtivo: TdmkCheckBox;
    QrAplicativosSigla: TWideStringField;
    QrAplicativosCategoria: TSmallintField;
    DBEdNome: TdmkDBEdit;
    Label11: TLabel;
    dmkDBEdit2: TdmkDBEdit;
    Label12: TLabel;
    dmkDBEdit4: TdmkDBEdit;
    Label14: TLabel;
    Label15: TLabel;
    DBMemo1: TDBMemo;
    CkAtivo: TDBCheckBox;
    QrAplicativosAtivo: TSmallintField;
    QrAplicativosDescri: TWideStringField;
    RGCategoria: TdmkRadioGroup;
    Label2: TLabel;
    EdLinguagem: TdmkEdit;
    Label8: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    Label3: TLabel;
    EdCompilador: TdmkEdit;
    Label4: TLabel;
    EdBaseDados: TdmkEdit;
    QrAplicativosLinguagem: TWideStringField;
    QrAplicativosCompilador: TWideStringField;
    QrAplicativosBaseDados: TWideStringField;
    dmkDBEdit3: TdmkDBEdit;
    Label10: TLabel;
    dmkDBEdit5: TdmkDBEdit;
    Label13: TLabel;
    Panel6: TPanel;
    BtConditional: TBitBtn;
    BitBtn1: TBitBtn;
    PMImprime: TPopupMenu;
    Lista1: TMenuItem;
    Ativos1: TMenuItem;
    odos1: TMenuItem;
    QrListaAplic: TmySQLQuery;
    frxCAD_APLIC_001_001: TfrxReport;
    frxDsListaAplic: TfrxDBDataset;
    QrListaAplicCodigo: TIntegerField;
    QrListaAplicNome: TWideStringField;
    QrListaAplicSigla: TWideStringField;
    QrListaAplicCategoriaTXT: TWideStringField;
    QrListaAplicAtivo: TIntegerField;
    BitBtn2: TBitBtn;
    Label16: TLabel;
    EdLinkImg1: TdmkEdit;
    Label17: TLabel;
    EdLinkImg2: TdmkEdit;
    Label18: TLabel;
    EdLinkImg3: TdmkEdit;
    Label19: TLabel;
    EdLinkImg4: TdmkEdit;
    Label20: TLabel;
    EdLinkLogo: TdmkEdit;
    QrAplicativosLinkLogo: TWideStringField;
    QrAplicativosLinkImg1: TWideStringField;
    QrAplicativosLinkImg2: TWideStringField;
    QrAplicativosLinkImg3: TWideStringField;
    QrAplicativosLinkImg4: TWideStringField;
    QrContas: TmySQLQuery;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    QrContasNome2: TWideStringField;
    QrContasNome3: TWideStringField;
    QrContasID: TWideStringField;
    QrContasSubgrupo: TIntegerField;
    QrContasEmpresa: TIntegerField;
    QrContasCredito: TWideStringField;
    QrContasDebito: TWideStringField;
    QrContasMensal: TWideStringField;
    QrContasExclusivo: TWideStringField;
    QrContasMensdia: TSmallintField;
    QrContasMensdeb: TFloatField;
    QrContasMensmind: TFloatField;
    QrContasMenscred: TFloatField;
    QrContasMensminc: TFloatField;
    QrContasLk: TIntegerField;
    QrContasTerceiro: TIntegerField;
    QrContasExcel: TWideStringField;
    QrContasDataCad: TDateField;
    QrContasDataAlt: TDateField;
    QrContasUserCad: TSmallintField;
    QrContasUserAlt: TSmallintField;
    QrContasNOMESUBGRUPO: TWideStringField;
    QrContasNOMEGRUPO: TWideStringField;
    QrContasNOMECONJUNTO: TWideStringField;
    QrContasNOMEEMPRESA: TWideStringField;
    DsContas: TDataSource;
    Label23: TLabel;
    EdConta: TdmkEditCB;
    CBConta: TdmkDBLookupComboBox;
    SpeedButton5: TSpeedButton;
    Label24: TLabel;
    dmkDBEdit7: TdmkDBEdit;
    QrAplicativosConta_TXT: TWideStringField;
    QrAplicativosConta: TIntegerField;
    QrAplicativosCATEGORIA_TXT: TWideStringField;
    QrAplicativosLicencaTip: TIntegerField;
    DBCGLicencaTip: TdmkDBCheckGroup;
    CGLicencaTip: TdmkCheckGroup;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrAplicativosAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrAplicativosBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure BtConditionalClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure PMImprimePopup(Sender: TObject);
    procedure Ativos1Click(Sender: TObject);
    procedure odos1Click(Sender: TObject);
    procedure frxCAD_APLIC_001_001GetValue(const VarName: string;
      var Value: Variant);
    procedure BitBtn2Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
  private
    FAplicAtivos: Boolean;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure ImprimeListaClientes(Ativo: Boolean);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmAplicativos: TFmAplicativos;

const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, Principal, UnitVersoes_Jan, UnitMD5, UnFTPJan,
  UnFinanceiroJan, UnitVersoes;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmAplicativos.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmAplicativos.odos1Click(Sender: TObject);
begin
  FAplicAtivos := False;
  //
  ImprimeListaClientes(False);
end;

procedure TFmAplicativos.PMImprimePopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrAplicativos.State <> dsInactive) and (QrAplicativos.RecordCount > 0);
  //
  Lista1.Enabled := Enab;
end;

procedure TFmAplicativos.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrAplicativosCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmAplicativos.DefParams;
var
  Categ0, Categ1, Categ2, Categ3: String;
begin
  Categ0 := UnVersoes.App_ObtemCategoria(0);
  Categ1 := UnVersoes.App_ObtemCategoria(1);
  Categ2 := UnVersoes.App_ObtemCategoria(2);
  Categ3 := UnVersoes.App_ObtemCategoria(3);

  VAR_GOTOTABELA := 'aplicativos';
  VAR_GOTOMYSQLTABLE := QrAplicativos;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT apl.*, CASE apl.Categoria ');
  VAR_SQLx.Add('WHEN 0 THEN "' + Categ0 + '" ');
  VAR_SQLx.Add('WHEN 1 THEN "' + Categ1 + '" ');
  VAR_SQLx.Add('WHEN 2 THEN "' + Categ2 + '" ');
  VAR_SQLx.Add('WHEN 3 THEN "' + Categ3 + '" END CATEGORIA_TXT, ');
  VAR_SQLx.Add('con.Nome Conta_TXT');
  VAR_SQLx.Add('FROM aplicativos apl ');
  VAR_SQLx.Add('LEFT JOIN contas con ON con.Codigo = apl.Conta ');
  VAR_SQLx.Add('WHERE apl.Codigo > 0 ');
  //
  VAR_SQL1.Add('AND apl.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND apl.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND apl.Nome Like :P0');
  //
end;

procedure TFmAplicativos.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmAplicativos.QueryPrincipalAfterOpen;
begin
end;

procedure TFmAplicativos.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmAplicativos.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmAplicativos.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmAplicativos.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmAplicativos.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmAplicativos.SpeedButton5Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  FinanceiroJan.CadastroDeContas(EdConta.ValueVariant);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdConta, CBConta, QrContas, VAR_CADASTRO);
    //
    EdConta.SetFocus;
  end;
end;

procedure TFmAplicativos.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmAplicativos.Ativos1Click(Sender: TObject);
begin
  FAplicAtivos := True;
  //
  ImprimeListaClientes(True);
end;

procedure TFmAplicativos.BitBtn1Click(Sender: TObject);
var
  Enab: Boolean;
  Aplicativo: Integer;
begin
  Enab := (QrAplicativos.State <> dsInactive) and (QrAplicativos.RecordCount > 0);
  //
  if Enab then
    Aplicativo := QrAplicativosCodigo.Value
  else
    Aplicativo := 0;
  //
  FmPrincipal.MostraAplicModul(Aplicativo);
end;

procedure TFmAplicativos.BitBtn2Click(Sender: TObject);
var
  Aplic: Integer;
begin
  if (QrAplicativos.State <> dsInactive) and (QrAplicativos.RecordCount > 0) then
    Aplic := QrAplicativosCodigo.Value
  else
    Aplic := 0;
  //
  UnVersoes_Jan.MostraFormWVersao(True, Aplic, FmPrincipal.PageControl1,
    FmPrincipal.AdvToolBarPagerNovo);
  //
  Close;
end;

procedure TFmAplicativos.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrAplicativos, [PnDados],
    [PnEdita], EdNome, ImgTipo, 'aplicativos');
end;

procedure TFmAplicativos.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrAplicativosCodigo.Value;
  Close;
end;

procedure TFmAplicativos.BtConditionalClick(Sender: TObject);
begin
  FmPrincipal.MostraConDef();
end;

procedure TFmAplicativos.BtConfirmaClick(Sender: TObject);
var
  Codigo, Conta, Categoria, LicencaTip, Ativo: Integer;
  Nome, Sigla, Descri, Linguagem, Compilador, BaseDados, LinkLogo, LinkImg1,
  LinkImg2, LinkImg3, LinkImg4: String;
begin
  Categoria  := RGCategoria.ItemIndex;
  Ativo      := Geral.BoolToInt(CkAtivo.Checked);
  Nome       := EdNome.ValueVariant;
  Sigla      := EdSigla.ValueVariant;
  Descri     := MeDescri.Text;
  Linguagem  := EdLinguagem.ValueVariant;
  Compilador := EdCompilador.ValueVariant;
  BaseDados  := EdBaseDados.ValueVariant;
  LinkLogo   := EdLinkLogo.ValueVariant;
  LinkImg1   := EdLinkImg1.ValueVariant;
  LinkImg2   := EdLinkImg2.ValueVariant;
  LinkImg3   := EdLinkImg3.ValueVariant;
  LinkImg4   := EdLinkImg4.ValueVariant;
  Conta      := EdConta.ValueVariant;
  LicencaTip := CGLicencaTip.Value;
  //
  if MyObjects.FIC(Nome = '', EdNome, 'Defina o nome!') then Exit;
  if MyObjects.FIC(Sigla = '', EdSigla, 'Defina a sigla!') then Exit;
  if MyObjects.FIC(Categoria < 0, RGCategoria, 'Defina a categoria!') then Exit;
  if MyObjects.FIC(Descri = '', MeDescri, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(Linguagem = '', EdLinguagem, 'Defina a linguagem!') then Exit;
  if MyObjects.FIC(Compilador = '', EdCompilador, 'Defina o compilador!') then Exit;
  if MyObjects.FIC(Compilador = '', EdCompilador, 'Defina o compilador!') then Exit;
  if MyObjects.FIC(BaseDados = '', EdBaseDados, 'Defina o banco de dados!') then Exit;
  if MyObjects.FIC(Conta = 0, EdConta, 'Defina a conta do plano de contas!') then Exit;
  if MyObjects.FIC(LicencaTip = 0, CGLicencaTip, 'Voc� deve informar pelo menos um tipo de licen�a!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('aplicativos', 'Codigo', '', '', tsPos,
    ImgTipo.SQLType, QrAplicativosCodigo.Value);
  //
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita, 'aplicativos',
    Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmAplicativos.BtDesisteClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo          := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'aplicativos', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmAplicativos.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrAplicativos, [PnDados],
    [PnEdita], EdNome, ImgTipo, 'aplicativos');
end;

procedure TFmAplicativos.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align   := alClient;
  GBDados.Align   := alClient;
  FAplicAtivos    := False;
  //
  CriaOForm;
  //
  RGCategoria.Items    := UnVersoes.App_ObtemListaCategorias;
  DBCGLicencaTip.Items := UnVersoes.App_ObtemLicencasTipos;
  CGLicencaTip.Items   := UnVersoes.App_ObtemLicencasTipos;
  //
  UMyMod.AbreQuery(QrContas, Dmod.MyDB);
end;

procedure TFmAplicativos.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrAplicativosCodigo.Value, LaRegistro.Caption);
end;

procedure TFmAplicativos.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmAplicativos.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmAplicativos.SbNovoClick(Sender: TObject);
begin
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmAplicativos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmAplicativos.QrAplicativosAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmAplicativos.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmAplicativos.SbQueryClick(Sender: TObject);
begin
  LocCod(QrAplicativosCodigo.Value, CuringaLoc.CriaForm(CO_CODIGO, CO_NOME,
    'aplicativos', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmAplicativos.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmAplicativos.frxCAD_APLIC_001_001GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_TITULOREL' then
  begin
    if FAplicAtivos then
      Value := 'LISTA DE APLICATIVOS ATIVOS'
    else
      Value := 'LISTA DE APLICATIVOS';
  end;
end;

procedure TFmAplicativos.QrAplicativosBeforeOpen(DataSet: TDataSet);
begin
  QrAplicativosCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmAplicativos.ImprimeListaClientes(Ativo: Boolean);
var
  Categ0, Categ1, Categ2, Categ3, SQL: String;
begin
  if Ativo then
    SQL := 'WHERE Ativo = 1'
  else
    SQL := '';
  //
  Categ0 := UnVersoes.App_ObtemCategoria(0);
  Categ1 := UnVersoes.App_ObtemCategoria(1);
  Categ2 := UnVersoes.App_ObtemCategoria(2);
  Categ3 := UnVersoes.App_ObtemCategoria(3);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrListaAplic, Dmod.MyDB, [
    'SELECT Codigo, Nome, Sigla, Ativo, ',
    'CASE Categoria ',
    'WHEN 0 THEN "' + Categ0 + '" ',
    'WHEN 1 THEN "' + Categ1 + '" ',
    'WHEN 2 THEN "' + Categ2 + '" ',
    'WHEN 3 THEN "' + Categ3 + '" END CategoriaTXT ',
    'FROM aplicativos ',
    SQL,
    'ORDER BY CategoriaTXT, Nome ',
    '']);
  MyObjects.frxMostra(frxCAD_APLIC_001_001, 'Lista de Aplicativos');
end;

end.

