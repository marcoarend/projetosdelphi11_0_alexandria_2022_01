object FmBackupDirIts: TFmBackupDirIts
  Left = 339
  Top = 185
  Caption = 'FER-BADIR-002 :: Configura'#231#245'es do Backup'
  ClientHeight = 537
  ClientWidth = 574
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 574
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 526
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 478
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 312
        Height = 32
        Caption = 'Configura'#231#245'es do Backup'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 312
        Height = 32
        Caption = 'Configura'#231#245'es do Backup'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 312
        Height = 32
        Caption = 'Configura'#231#245'es do Backup'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 574
    Height = 375
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 574
      Height = 375
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 574
        Height = 375
        Align = alClient
        TabOrder = 0
        object PnExtOmit: TPanel
          Left = 372
          Top = 15
          Width = 200
          Height = 358
          Align = alClient
          TabOrder = 0
          object StaticText3: TStaticText
            Left = 1
            Top = 1
            Width = 198
            Height = 17
            Align = alTop
            BorderStyle = sbsSunken
            Caption = 'Extens'#245'es a omitir (Ex.:  .txt):'
            TabOrder = 0
          end
          object MeExt: TdmkMemo
            Left = 1
            Top = 18
            Width = 198
            Height = 339
            Align = alClient
            Lines.Strings = (
              '.~*'
              '.dcu'
              '.exe'
              '.map')
            ScrollBars = ssVertical
            TabOrder = 1
            UpdType = utYes
          end
        end
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 370
          Height = 358
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 1
          object PnDados1: TPanel
            Left = 0
            Top = 0
            Width = 370
            Height = 144
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object Label32: TLabel
              Left = 4
              Top = 51
              Width = 76
              Height = 13
              Caption = 'Diret'#243'rio origem:'
            end
            object Label9: TLabel
              Left = 4
              Top = 11
              Width = 14
              Height = 13
              Caption = 'ID:'
            end
            object SpeedButton2: TSpeedButton
              Left = 333
              Top = 66
              Width = 23
              Height = 21
              Caption = '...'
              OnClick = SpeedButton2Click
            end
            object RGWeb: TRadioGroup
              Left = 4
              Top = 94
              Width = 185
              Height = 45
              Caption = 'Tipo de diret'#243'rio destino'
              Columns = 2
              Items.Strings = (
                'Local'
                'WEB')
              TabOrder = 0
            end
            object EdOrigem: TdmkEdit
              Left = 4
              Top = 67
              Width = 327
              Height = 21
              MaxLength = 255
              TabOrder = 1
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdCodigo: TdmkEdit
              Left = 4
              Top = 26
              Width = 71
              Height = 21
              Alignment = taRightJustify
              Color = clInactiveCaption
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBackground
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              ReadOnly = True
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Codigo'
              UpdCampo = 'Codigo'
              UpdType = utInc
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object CkExtOmit: TdmkCheckBox
              Left = 195
              Top = 113
              Width = 161
              Height = 17
              Caption = 'Omitir extens'#245'es de arquivos'
              TabOrder = 3
              OnClick = CkExtOmitClick
              UpdType = utYes
              ValCheck = #0
              ValUncheck = #0
              OldValor = #0
            end
          end
          object PnDirWeb: TPanel
            Left = 0
            Top = 192
            Width = 370
            Height = 48
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object LaFTPDir: TLabel
              Left = 4
              Top = 4
              Width = 65
              Height = 13
              Caption = 'Diret'#243'rio FTP:'
            end
            object EdFTPWebDir: TdmkEditCB
              Left = 4
              Top = 20
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBFTPWebDir
              IgnoraDBLookupComboBox = False
            end
            object CBFTPWebDir: TdmkDBLookupComboBox
              Left = 60
              Top = 20
              Width = 296
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              TabOrder = 1
              dmkEditCB = EdFTPWebDir
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
          end
          object PnDados4: TPanel
            Left = 0
            Top = 240
            Width = 370
            Height = 118
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 2
            object GbOpcoes: TGroupBox
              Left = 4
              Top = 6
              Width = 327
              Height = 80
              Caption = 'Op'#231#245'es'
              TabOrder = 0
              object LaSenha: TLabel
                Left = 135
                Top = 34
                Width = 55
                Height = 13
                Caption = 'Senha [F4]:'
                Visible = False
              end
              object CkZipar: TdmkCheckBox
                Left = 9
                Top = 49
                Width = 120
                Height = 17
                Caption = 'Zipar o arquivo.'
                TabOrder = 1
                OnClick = CkZiparClick
                UpdType = utYes
                ValCheck = #0
                ValUncheck = #0
                OldValor = #0
              end
              object CkEncryptar: TdmkCheckBox
                Left = 135
                Top = 17
                Width = 95
                Height = 17
                Caption = 'Encryptar'
                TabOrder = 2
                OnClick = CkEncryptarClick
                UpdType = utYes
                ValCheck = #0
                ValUncheck = #0
                OldValor = #0
              end
              object EdSenha: TdmkEdit
                Left = 135
                Top = 49
                Width = 177
                Height = 21
                MaxLength = 50
                TabOrder = 3
                Visible = False
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnKeyDown = EdSenhaKeyDown
              end
              object CkSubDir: TdmkCheckBox
                Left = 9
                Top = 17
                Width = 120
                Height = 17
                Caption = 'Incluir subdiret'#243'rios.'
                TabOrder = 0
                UpdType = utYes
                ValCheck = #0
                ValUncheck = #0
                OldValor = #0
              end
            end
            object CkContinuar: TCheckBox
              Left = 4
              Top = 88
              Width = 112
              Height = 17
              Caption = 'Continuar inserindo.'
              TabOrder = 1
            end
          end
          object PnDirLoc: TPanel
            Left = 0
            Top = 144
            Width = 370
            Height = 48
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 3
            object Label1: TLabel
              Left = 4
              Top = 5
              Width = 79
              Height = 13
              Caption = 'Diret'#243'rio destino:'
            end
            object SpeedButton1: TSpeedButton
              Left = 333
              Top = 21
              Width = 23
              Height = 21
              Caption = '...'
              OnClick = SpeedButton1Click
            end
            object EdDestino: TdmkEdit
              Left = 4
              Top = 21
              Width = 327
              Height = 21
              MaxLength = 255
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnExit = EdDestinoExit
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 423
    Width = 574
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 570
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 467
    Width = 574
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 428
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 8
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 426
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 8
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
end
