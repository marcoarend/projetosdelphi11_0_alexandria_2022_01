unit Module;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, (*DBTables,*) UnInternalConsts, UnMLAGeral, UMySQLModule, dmkEdit,
  mySQLDbTables, UnGOTOy, Winsock, MySQLBatch, frxClass, frxDBSet, dmkGeral,
  Variants, StdCtrls, ComCtrls, IdBaseComponent, IdComponent,
  IdIPWatch, UnGrl_Vars;

type
  TDmod = class(TDataModule)
    QrMaster: TMySQLQuery;
    QrMasterCNPJ_TXT: TWideStringField;
    QrMasterTE1_TXT: TWideStringField;
    QrMasterCEP_TXT: TWideStringField;
    QrUpdU: TmySQLQuery;
    QrMasterEm: TWideStringField;
    QrMasterTipo: TSmallintField;
    QrMasterLogo: TBlobField;
    QrMasterDono: TIntegerField;
    QrMasterVersao: TIntegerField;
    QrMasterCNPJ: TWideStringField;
    QrMasterIE: TWideStringField;
    QrMasterECidade: TWideStringField;
    QrMasterNOMEUF: TWideStringField;
    QrMasterEFax: TWideStringField;
    QrMasterERua: TWideStringField;
    QrMasterEBairro: TWideStringField;
    QrMasterECompl: TWideStringField;
    QrMasterEContato: TWideStringField;
    QrMasterECel: TWideStringField;
    QrMasterETe1: TWideStringField;
    QrMasterETe2: TWideStringField;
    QrMasterETe3: TWideStringField;
    QrMasterEPais: TWideStringField;
    QrMasterRespons1: TWideStringField;
    QrMasterRespons2: TWideStringField;
    QrMasterECEP: TIntegerField;
    QrIdx: TmySQLQuery;
    QrMas: TmySQLQuery;
    QrUpd: TmySQLQuery;
    QrAux: TmySQLQuery;
    QrSQL: TmySQLQuery;
    MyDB: TmySQLDatabase;
    QrPriorNext: TmySQLQuery;
    QrControle: TmySQLQuery;
    QrMasterLogo2: TBlobField;
    QrTerminal: TmySQLQuery;
    QrTerminalIP: TWideStringField;
    QrTerminalTerminal: TIntegerField;
    QrMasterLimite: TSmallintField;
    QrNTV: TmySQLQuery;
    QrNTI: TmySQLQuery;
    ZZDB: TmySQLDatabase;
    QrMasterSolicitaSenha: TIntegerField;
    QrTerminais: TmySQLQuery;
    QrTerminaisIP: TWideStringField;
    QrTerminaisTerminal: TIntegerField;
    QrMasterENumero: TFloatField;
    QrTerminaisLicenca: TWideStringField;
    QrTransf: TmySQLQuery;
    QrTransfData: TDateField;
    QrTransfTipo: TSmallintField;
    QrTransfCarteira: TIntegerField;
    QrTransfControle: TIntegerField;
    QrTransfGenero: TIntegerField;
    QrTransfDebito: TFloatField;
    QrTransfCredito: TFloatField;
    QrTransfDocumento: TFloatField;
    frxDsMaster: TfrxDBDataset;
    QrMasterUsaAccMngr: TSmallintField;
    QrNew: TmySQLQuery;
    QrNewCodigo: TIntegerField;
    QlLocal: TMySQLBatchExecute;
    QrUpdM: TmySQLQuery;
    QrSomaM: TmySQLQuery;
    QrSomaMValor: TFloatField;
    QrDuplicStrX: TmySQLQuery;
    QrDuplicStrXNOME: TWideStringField;
    QrDuplicStrXCODIGO: TIntegerField;
    QrDuplicStrXANTERIOR: TIntegerField;
    QrDuplicIntX: TmySQLQuery;
    QrDuplicIntXINTEIRO1: TIntegerField;
    QrDuplicIntXINTEIRO2: TIntegerField;
    QrDuplicIntXCODIGO: TIntegerField;
    QrFields: TmySQLQuery;
    QrDelLogX: TmySQLQuery;
    QrInsLogX: TmySQLQuery;
    QrRecCountX: TmySQLQuery;
    QrRecCountXRecord: TIntegerField;
    QrLocY: TmySQLQuery;
    QrLocYRecord: TIntegerField;
    QrCountY: TmySQLQuery;
    QrCountYRecord: TIntegerField;
    QrPerfis: TmySQLQuery;
    QrPerfisLibera: TSmallintField;
    QrPerfisJanela: TWideStringField;
    QrSel: TmySQLQuery;
    QrSelMovix: TIntegerField;
    QrTerceiro: TmySQLQuery;
    QrTerceiroNOMEpUF: TWideStringField;
    QrTerceiroNOMEeUF: TWideStringField;
    QrTerceiroCodigo: TIntegerField;
    QrTerceiroRazaoSocial: TWideStringField;
    QrTerceiroFantasia: TWideStringField;
    QrTerceiroRespons1: TWideStringField;
    QrTerceiroRespons2: TWideStringField;
    QrTerceiroPai: TWideStringField;
    QrTerceiroMae: TWideStringField;
    QrTerceiroCNPJ: TWideStringField;
    QrTerceiroIE: TWideStringField;
    QrTerceiroIEST: TWideStringField;
    QrTerceiroNome: TWideStringField;
    QrTerceiroApelido: TWideStringField;
    QrTerceiroCPF: TWideStringField;
    QrTerceiroRG: TWideStringField;
    QrTerceiroELograd: TSmallintField;
    QrTerceiroERua: TWideStringField;
    QrTerceiroENumero: TIntegerField;
    QrTerceiroECompl: TWideStringField;
    QrTerceiroEBairro: TWideStringField;
    QrTerceiroECidade: TWideStringField;
    QrTerceiroEUF: TSmallintField;
    QrTerceiroECEP: TIntegerField;
    QrTerceiroEPais: TWideStringField;
    QrTerceiroETe1: TWideStringField;
    QrTerceiroEte2: TWideStringField;
    QrTerceiroEte3: TWideStringField;
    QrTerceiroECel: TWideStringField;
    QrTerceiroEFax: TWideStringField;
    QrTerceiroEEmail: TWideStringField;
    QrTerceiroEContato: TWideStringField;
    QrTerceiroENatal: TDateField;
    QrTerceiroPLograd: TSmallintField;
    QrTerceiroPRua: TWideStringField;
    QrTerceiroPNumero: TIntegerField;
    QrTerceiroPCompl: TWideStringField;
    QrTerceiroPBairro: TWideStringField;
    QrTerceiroPCidade: TWideStringField;
    QrTerceiroPUF: TSmallintField;
    QrTerceiroPCEP: TIntegerField;
    QrTerceiroPPais: TWideStringField;
    QrTerceiroPTe1: TWideStringField;
    QrTerceiroPte2: TWideStringField;
    QrTerceiroPte3: TWideStringField;
    QrTerceiroPCel: TWideStringField;
    QrTerceiroPFax: TWideStringField;
    QrTerceiroPEmail: TWideStringField;
    QrTerceiroPContato: TWideStringField;
    QrTerceiroPNatal: TDateField;
    QrTerceiroSexo: TWideStringField;
    QrTerceiroResponsavel: TWideStringField;
    QrTerceiroProfissao: TWideStringField;
    QrTerceiroCargo: TWideStringField;
    QrTerceiroRecibo: TSmallintField;
    QrTerceiroDiaRecibo: TSmallintField;
    QrTerceiroAjudaEmpV: TFloatField;
    QrTerceiroAjudaEmpP: TFloatField;
    QrTerceiroCliente1: TWideStringField;
    QrTerceiroCliente2: TWideStringField;
    QrTerceiroFornece1: TWideStringField;
    QrTerceiroFornece2: TWideStringField;
    QrTerceiroFornece3: TWideStringField;
    QrTerceiroFornece4: TWideStringField;
    QrTerceiroTerceiro: TWideStringField;
    QrTerceiroCadastro: TDateField;
    QrTerceiroInformacoes: TWideStringField;
    QrTerceiroLogo: TBlobField;
    QrTerceiroVeiculo: TIntegerField;
    QrTerceiroMensal: TWideStringField;
    QrTerceiroObservacoes: TWideMemoField;
    QrTerceiroTipo: TSmallintField;
    QrTerceiroCLograd: TSmallintField;
    QrTerceiroCRua: TWideStringField;
    QrTerceiroCNumero: TIntegerField;
    QrTerceiroCCompl: TWideStringField;
    QrTerceiroCBairro: TWideStringField;
    QrTerceiroCCidade: TWideStringField;
    QrTerceiroCUF: TSmallintField;
    QrTerceiroCCEP: TIntegerField;
    QrTerceiroCPais: TWideStringField;
    QrTerceiroCTel: TWideStringField;
    QrTerceiroCCel: TWideStringField;
    QrTerceiroCFax: TWideStringField;
    QrTerceiroCContato: TWideStringField;
    QrTerceiroLLograd: TSmallintField;
    QrTerceiroLRua: TWideStringField;
    QrTerceiroLNumero: TIntegerField;
    QrTerceiroLCompl: TWideStringField;
    QrTerceiroLBairro: TWideStringField;
    QrTerceiroLCidade: TWideStringField;
    QrTerceiroLUF: TSmallintField;
    QrTerceiroLCEP: TIntegerField;
    QrTerceiroLPais: TWideStringField;
    QrTerceiroLTel: TWideStringField;
    QrTerceiroLCel: TWideStringField;
    QrTerceiroLFax: TWideStringField;
    QrTerceiroLContato: TWideStringField;
    QrTerceiroComissao: TFloatField;
    QrTerceiroSituacao: TSmallintField;
    QrTerceiroNivel: TWideStringField;
    QrTerceiroGrupo: TIntegerField;
    QrTerceiroAccount: TIntegerField;
    QrTerceiroLogo2: TBlobField;
    QrTerceiroConjugeNome: TWideStringField;
    QrTerceiroConjugeNatal: TDateField;
    QrTerceiroNome1: TWideStringField;
    QrTerceiroNatal1: TDateField;
    QrTerceiroNome2: TWideStringField;
    QrTerceiroNatal2: TDateField;
    QrTerceiroNome3: TWideStringField;
    QrTerceiroNatal3: TDateField;
    QrTerceiroNome4: TWideStringField;
    QrTerceiroNatal4: TDateField;
    QrTerceiroCreditosI: TIntegerField;
    QrTerceiroCreditosL: TIntegerField;
    QrTerceiroCreditosF2: TFloatField;
    QrTerceiroCreditosD: TDateField;
    QrTerceiroCreditosU: TDateField;
    QrTerceiroCreditosV: TDateField;
    QrTerceiroMotivo: TIntegerField;
    QrTerceiroQuantI1: TIntegerField;
    QrTerceiroQuantI2: TIntegerField;
    QrTerceiroQuantI3: TIntegerField;
    QrTerceiroQuantI4: TIntegerField;
    QrTerceiroQuantN1: TFloatField;
    QrTerceiroQuantN2: TFloatField;
    QrTerceiroAgenda: TWideStringField;
    QrTerceiroSenhaQuer: TWideStringField;
    QrTerceiroSenha1: TWideStringField;
    QrTerceiroLimiCred: TFloatField;
    QrTerceiroDesco: TFloatField;
    QrTerceiroCasasApliDesco: TSmallintField;
    QrTerceiroTempD: TFloatField;
    QrTerceiroLk: TIntegerField;
    QrTerceiroDataCad: TDateField;
    QrTerceiroDataAlt: TDateField;
    QrTerceiroUserCad: TIntegerField;
    QrTerceiroUserAlt: TIntegerField;
    QrTerceiroCPF_Pai: TWideStringField;
    QrTerceiroSSP: TWideStringField;
    QrTerceiroCidadeNatal: TWideStringField;
    QrTerceiroUFNatal: TSmallintField;
    QrBinaLigouA: TmySQLQuery;
    DsBinaLigouA: TDataSource;
    QrBinaLigouACodigo: TAutoIncField;
    QrBinaLigouANome: TWideStringField;
    QrBinaLigouAxData: TWideStringField;
    QrBinaLigouAxHora: TWideStringField;
    QrBinaLigouAdData: TDateField;
    QrBinaLigouAdHora: TTimeField;
    QrBinaLigouAUsuario: TIntegerField;
    QrBinaLigouATerminal: TIntegerField;
    QrBinaLigouAIP: TWideStringField;
    QrBinaLigouAForcado: TSmallintField;
    QrBinaAB: TmySQLQuery;
    QrBinaLigouALk: TIntegerField;
    QrBinaLigouADataCad: TDateField;
    QrBinaLigouADataAlt: TDateField;
    QrBinaLigouAUserCad: TIntegerField;
    QrBinaLigouAUserAlt: TIntegerField;
    QrBinaLigouAAlterWeb: TSmallintField;
    QrBinaLigouAAtivo: TSmallintField;
    QrBinaLigouATELEFONE: TWideStringField;
    MyDBn: TmySQLDatabase;
    QrUpdN: TmySQLQuery;
    QrWControl: TmySQLQuery;
    QrWControlCodigo: TIntegerField;
    QrWControlLk: TIntegerField;
    QrWControlDataCad: TDateField;
    QrWControlDataAlt: TDateField;
    QrWControlUserCad: TIntegerField;
    QrWControlUserAlt: TIntegerField;
    QrWControlAlterWeb: TSmallintField;
    QrWControlAtivo: TSmallintField;
    QrWControlCrypt: TWideStringField;
    QrWControlMailCont: TIntegerField;
    QrWControlDiasExp: TIntegerField;
    QrWControlPerPasCli: TIntegerField;
    QrWControlPerPasUsu: TIntegerField;
    QrWControlPerPasAdm: TIntegerField;
    QrWControlPerPasBos: TIntegerField;
    QrWControlDono: TIntegerField;
    QrWControlProAltPas: TIntegerField;
    QrWControlProDowArq: TIntegerField;
    QrWControlProAtivApl: TIntegerField;
    QrWControlLogoWeb: TWideStringField;
    QrWeb: TmySQLQuery;
    QrUpd2: TmySQLQuery;
    QrOpcoesDCon: TmySQLQuery;
    QrAuxN: TmySQLQuery;
    QrNotifi: TmySQLQuery;
    MyLocDatabase: TmySQLDatabase;
    QrUpdL: TmySQLQuery;
    QrAuxL: TmySQLQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure QrMasterAfterOpen(DataSet: TDataSet);
    procedure QrMasterCalcFields(DataSet: TDataSet);
    procedure DataModuleDestroy(Sender: TObject);
    procedure QrControleAfterOpen(DataSet: TDataSet);
    procedure QrBinaLigouACalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    FCamposBinaLigou: String;
    FProdDelete: Integer;
    FStrFmtPrc, FStrFmtCus: String;
    function Privilegios(Usuario : Integer) : Boolean;
    procedure RecalcSaldoCarteira(Tipo, Carteira: Integer; Localiza: Byte);
    procedure VerificaSenha(Index: Integer; Login, Senha: String);

    function BuscaProximoMovix: Integer;
    procedure InsereRegistrosExtraBalanceteMensal_01(QrInd01: TmySQLQuery;
              DataIDate, DataFDate: TDateTime; DataITrue, DataFTrue: Boolean;
              CliInt: Integer; Campos: String; DescrSubstit: Boolean);
    function  TabelasQueNaoQueroCriar(): String;
    procedure ReopenControle();
    procedure ReopenOpcoesDCon();

    // Compatibilidade Estoque / NFe
    procedure AlteraSMIA(Qry: TmySQLQuery);
    procedure ExcluiItensEstqNFsCouroImportadas();
    procedure GeraEstoqueEm_2(Empresa_Txt: String; DataIni,
      DataFim: TDateTime; PGT, GG1, GG2, GG3, TipoPesq: Integer; GraCusPrc,
      TabePrcCab: Variant; GraCusPrcCodigo, TabePrcCabCodigo, PrdGrupTipCodigo,
      GraGru1Nivel1, GraGru2Nivel2, GraGru3Nivel3: Integer; QrPGT_SCC, QrAbertura2a,
      QrAbertura2b: TmySQLQuery; UsaTabePrcCab, SoPositivos: Boolean;
      LaAviso: TLabel; NomeTb_SMIC: String; QrSMIC_X: TmySQLQuery; var TxtSQL_Part1,
      TxtSQL_Part2: String);
    // Fim Compatibilidade
    procedure ReopenParamsEspecificos(Empresa: Integer);
    procedure AtivaDesativaCliente(Cliente, Entidade: Integer; Ativo: Boolean);
  end;

var
  Dmod: TDmod;
  FVerifi: Boolean;
  //
  QvEntra3: TmySQLQuery;
  QvVenda3: TmySQLQuery;
  QvDevol3: TmySQLQuery;
  QvRecom3: TmySQLQuery;

implementation

uses MyListas, UnMyObjects, Principal, SenhaBoss, UnLock_MLA, Servidor,
  UnALL_Jan, MyDBCheck, DControl_Dmk, ModuleGeral, ModuleFin, UnLic_Dmk,
  UnBloquetos, DmkDAC_PF, UnDmkProcFunc, UnDmkWeb, UMySQLDB;

{$R *.DFM}

procedure TDmod.DataModuleCreate(Sender: TObject);
var
  Versao, Resp: Integer;
  BD, OthSetCon: String;
  Verifica, VerificaDBTerc: Boolean;
begin
  VerificaDBTerc := False;
  //
  if MyDB.Connected then
    Geral.MensagemBox('MyDB est� connectado antes da configura��o!',
    'Aviso!', MB_OK+MB_ICONWARNING);
  (*
  if MyLocDataBase.Connected then
    Geral.MensagemBox('MyLocDataBase est� connectado antes da configura��o!',
    'Aviso!', MB_OK+MB_ICONWARNING);
  *)
  VAR_PORTA := Geral.ReadAppKeyCU('Porta', Application.Title, ktInteger, 3306);
  VAR_SERVIDOR := Geral.ReadAppKeyCU('Server', Application.Title, ktInteger, 0);
  VAR_IP       := Geral.ReadAppKeyCU('IPServer', Application.Title, ktString, CO_VAZIO);
  //
  if not (VAR_SERVIDOR in [1,2]) then
  begin
    Application.CreateForm(TFmServidor, FmServidor);
    FmServidor.ShowModal;
    FmServidor.Destroy;
  end;
  if VAR_IP = CO_VAZIO then
  begin
    VAR_IP := '127.0.0.1';
    if VAR_SERVIDOR = 1 then
    begin
      if InputQuery('IP do Servidor', 'Defina o IP do Servidor', VAR_IP) then
      begin
        Geral.WriteAppKeyCU('IPServer', Application.Title, VAR_IP, ktString);
      end else
      begin
        Application.Terminate;
        Exit;
      end;
    end;
  end;
  (*
  VAR_BDSENHA := 'wkljweryhvbirt';
  VAR_SQLHOST := Geral.ReadAppKeyLM('IPServer', Application.Title, ktString, '127.0.0.1');
  *)
  OthSetCon := Geral.ReadAppKeyCU('OthSetCon', Application.Title, ktString, '');
  if OthSetCon = EmptyStr then
    VAR_BDSENHA := CO_USERSPNOW
  else
    VAR_BDSENHA := dmkPF.Criptografia(OthSetCon, CO_LLM_SecureStr);
  //
  VAR_PORTA := Geral.ReadAppKeyCU('Porta', Application.Title, ktInteger, 3306);
  VAR_SERVIDOR := Geral.ReadAppKeyCU('Server', Application.Title, ktInteger, 0);
  //
  if not GOTOy.OMySQLEstaInstalado(FmDControl_Dmk.LaAviso1,
    FmDControl_Dmk.LaAviso2, FmDControl_Dmk.ProgressBar1, nil, nil) then
  begin
    //raise EAbort.Create('');
    //
    MyObjects.Informa2(FmDControl_Dmk.LaAviso1, FmDControl_Dmk.LaAviso2, False,
      'N�o foi poss�vel a conex�o ao IP: [' + VAR_IP + ']');
    FmDControl_Dmk.BtEntra.Visible := False;
    //
    Exit;
  end;
  /////////////////////////////////////////
  if GOTOy.SenhaDesconhecida then
  begin
    raise EAbort.Create('Senha desconhecida');
    Exit;
  end;
  ///////////////////////////////////////////////////////////
  ZZDB.Host         := VAR_SQLHOST;
  ZZDB.UserName     := VAR_SQLUSER;
  ZZDB.UserPassword := VAR_BDSENHA;
  ZZDB.Port         := VAR_PORTA;
  try
    ZZDB.Connected := True;
  except
    ZZDB.UserPassword := '852456';
    try
      ZZDB.Connected := True;
      QrUpd.Database := ZZDB;
      QrUpd.SQL.Clear;
      QrUpd.SQL.Add('UPDATE User SET Password=PASSWORD("'+VAR_BDSENHA+'")');
      QrUpd.SQL.Add('');
      QrUpd.SQL.Add('WHERE User="root"');
      QrUpd.ExecSQL;
      ///////////
      QrUpd.SQL.Clear;
      QrUpd.SQL.Add('FLUSH PRIVILEGES');
      QrUpd.ExecSQL;
      ///////////
      FmPrincipal.Close;
      Application.Terminate;
      Exit;
    except
      if VAR_SERVIDOR = 2 then ShowMessage('Banco de dados teste n�o se conecta!');
    end;
  end;
  /////////////////////////////////////////
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  //
  VAR_SQLx := TStringList.Create;
  VAR_SQL1 := TStringList.Create;
  VAR_SQL2 := TStringList.Create;
  VAR_SQLa := TStringList.Create;
  //
  MAR_SQLx := TStringList.Create;
  MAR_SQL1 := TStringList.Create;
  MAR_SQL2 := TStringList.Create;
  MAR_SQLa := TStringList.Create;
  try
    if VAR_SERVIDOR = 1 then
    begin
      if not GetSystemMetrics(SM_NETWORK) and $01 = $01 then
      begin
      Geral.MensagemBox('M�quina cliente sem rede.', 'Erro', MB_OK+MB_ICONERROR);
        //Application.Terminate;
      end;
    end;
  except
    Application.Terminate;
    Exit;
  end;
  if VAR_APPTERMINATE then
  begin
    Application.Terminate;
    Exit;
  end;
  Geral.DefineFormatacoes;
  //
  (*
  MyDB.UserName     := VAR_SQLUSER;
  MyDB.UserPassword := VAR_BDSENHA;
  MyDB.Host         := VAR_IP;
  MyDB.Port         := VAR_PORTA;
  MyDB.DatabaseName := 'mysql';// existe com certeza se estiver instalado
  *)
  UnDmkDAC_PF.ConectaMyDB_DAC(MyDB, 'mysql',  VAR_IP, VAR_PORTA,  VAR_SQLUSER,
    VAR_BDSENHA, (*Desconecta*)True, (*Configura*)True, (*Conecta*)False);
  //
  QrAux.Close;
  QrAux.SQL.Clear;
  QrAux.Database := MyDB;
  QrAux.SQL.Add('SHOW DATABASES');
  QrAux.Open;
  BD := CO_VAZIO;
  while not QrAux.Eof do
  begin
    if Uppercase(QrAux.FieldByName('Database').AsString)=Uppercase(TMeuDB) then
      BD := TMeuDB;
    QrAux.Next;
  end;
  MyDB.Close;
  MyDB.DataBaseName := BD;

  if MyDB.DataBaseName = CO_VAZIO then
  begin
    Resp := Geral.MensagemBox('O banco de dados '+TMeuDB+
      ' n�o existe e deve ser criado. Confirma a cria��o?', 'Banco de Dados',
      MB_YESNOCANCEL+MB_ICONQUESTION);
    if Resp = ID_YES then
    begin
      QrAux.Close;
      QrAux.SQL.Clear;
      QrAux.SQL.Add('CREATE DATABASE '+TMeuDB);
      QrAux.ExecSQL;
      if MyDB.Connected then
        MyDB.Disconnect;
      MyDB.DatabaseName := TMeuDB;
    end else
    begin
      Geral.MensagemBox('O aplicativo ser� encerrado!', 'Banco de Dados',
        MB_OK+MB_ICONWARNING);
      Application.Terminate;
      Exit;
    end;
  end;
  //
  GOTOy.DefinePathMySQL;
  //
  (*
  MyLocDatabase.UserName     := VAR_SQLUSER;
  MyLocDatabase.UserPassword := VAR_BDSENHA;
  MyLocDatabase.Port         := VAR_PORTA;
  MyLocDatabase.DatabaseName := 'mysql';// existe com certeza se estiver instalado
  *)
  {
  UnDmkDAC_PF.ConectaMyDB_DAC(MyLocDatabase, 'mysql',  VAR_IP, VAR_PORTA,
    VAR_SQLUSER, VAR_BDSENHA, (*Desconecta*)True, (*Configura*)True,
    (*Conecta*)False);
  //
  QrAuxL.Close;
  QrAuxL.SQL.Clear;
  QrAuxL.SQL.Add('SHOW DATABASES');
  QrAuxL.Open;
  BD := CO_VAZIO;
  while not QrAuxL.Eof do
  begin
    if Uppercase(QrAuxL.FieldByName('Database').AsString) = Uppercase(TLocDB) then
      BD := TLocDB;
    QrAuxL.Next;
  end;
  MyLocDatabase.Close;
  MyLocDatabase.DatabaseName := BD;
  if MyLocDatabase.DataBaseName = CO_VAZIO then
  begin
    Resp := Geral.MensagemBox('O banco de dados local '+TLocDB+
      ' n�o existe e deve ser criado. Confirma a cria��o?',
      'Banco de Dados Local', MB_YESNOCANCEL+MB_ICONQUESTION);
    if Resp = ID_YES then
    begin
      QrAuxL.Close;
      QrAuxL.SQL.Clear;
      QrAuxL.SQL.Add('CREATE DATABASE '+TLocDB);
      QrAuxL.ExecSQL;
      MyLocDatabase.DatabaseName := TLocDB;
    end else if Resp = ID_CANCEL then
    begin
      Geral.MensagemBox('O aplicativo ser� encerrado!',
      'Banco de Dados Local', MB_OK+MB_ICONWARNING);
      Application.Terminate;
      Exit;
    end;
  end;
  }
  //
  Versao   := USQLDB.ObtemVersaoAppDB(MyDB);
  Verifica := False;
  //
  if Versao < CO_VERSAO then
    Verifica := True;
  //
  if Versao = 0 then
  begin
    Resp := Geral.MensagemBox('N�o h� informa��o de vers�o no registro, ' +
              'se esta n�o for a primeira execu��o do aplicativo selecione cancelar e ' +
              'informe o respons�vel!. Caso confirme, ser� verificado a composi��o do ' +
              'banco de dados. Confirma a Verifica��o?',
              'Aus�ncia de Informa��o de Vers�o no Registro',
              MB_YESNOCANCEL+MB_ICONWARNING);
    if Resp = ID_YES then
      Verifica := True
    else if Resp = ID_CANCEL then
    begin
      Application.Terminate;
      Exit;
    end;
  end;
  if Verifica then
    ALL_Jan.MostraFormVerifiDB(True);
  //
  try
    QrMaster.Open;
    VAR_EMPRESANOME := QrMasterEm.Value;
  except
    try
      ALL_Jan.MostraFormVerifiDB(True);
      //
      VerificaDBTerc := True;
    except;
      MyDB.DatabaseName := CO_VAZIO;
      raise;
    end;
  end;
  //
  Mylist.ConfiguracoesIniciais(1, MyDB.DatabaseName);
  //
  try
    Application.CreateForm(TDmodG, DmodG);
    //
    DModG.MyPID_DB_Cria();
  except
    Geral.MensagemBox('Imposs�vel criar Modulo de dados Geral', 'Erro', MB_OK+MB_ICONERROR);
    Application.Terminate;
    Exit;
  end;
  //
  if VerificaDBTerc then
  begin
    All_Jan.MostraFormVerifiDBTerceiros(True);
    //
    VerificaDBTerc := False;
  end;
  (*
  // Conex�o na base de dados na url
  // S� pode ser ap�s abrir a tabela controle!
  // ver se est� ativado s� ent�o fazer !
  try
    if DmkWeb.ConexaoRemota(MyDBn, DModG.QrWebParams, 2) then
    begin
      if DmodG.QrWebParams.FieldByName('Web_MySQL').AsInteger = 2 then
      begin
        MyDBn.Connect;
        if MyDBn.Connected and Verifica then
        begin
          if not VAR_VERIFI_DB_CANCEL then
          begin
            Application.CreateForm(TFmVerifiDBi, FmVerifiDBi);
            with FmVerifiDbi do
            begin
              BtSair.Enabled := False;
              FVerifi := True;
              ShowModal;
              FVerifi := False;
              Destroy;
            end;
          end;
        end;
      end;
    end;
  except
    Geral.MensagemBox('N�o foi poss�vel se conectar � base de dados remota!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  *)
  try
    Application.CreateForm(TDModFin, DModFin);
  except
    Geral.MensagemBox('Imposs�vel criar Modulo Financeiro', 'Erro', MB_OK+MB_ICONERROR);
    Application.Terminate;
    Exit;
  end;
  if VAR_SERVIDOR = 2 then
    GOTOy.LiberaUso;
    //Geral.MB_Info('LiberaUso');
  //
  VAR_MyBDFINANCAS := MyDB;  // MyPagtos : CASHIER
  VAR_DB           := MyDB.DatabaseName;
end;

procedure TDmod.ReopenControle();
begin
  UMyMod.AbreQuery(QrControle, Dmod.MyDB);
end;

procedure TDmod.ReopenOpcoesDCon;
begin
  UMyMod.AbreQuery(QrOpcoesDCon, Dmod.MyDB);
end;

procedure TDmod.ReopenParamsEspecificos(Empresa: Integer);
begin
  //Compatibilidade
end;

procedure TDmod.RecalcSaldoCarteira(Tipo, Carteira: Integer; Localiza: Byte);
var
  Saldo: Double;
begin
  if Tipo < 2 then
  begin
    QrSomaM.Close;
    QrSomaM.SQL.Clear;
    QrSomaM.SQL.Add('SELECT Inicial Valor FROM Carteiras');
    QrSomaM.SQL.Add('WHERE Tipo=:P0 AND Codigo=:P1');
    QrSomaM.Params[0].AsInteger := Tipo;
    QrSomaM.Params[1].AsInteger := Carteira;
    QrSomaM.Open;
    Saldo := QrSomaMValor.Value;
  end else Saldo := 0;
  QrSomaM.Close;
  QrSomaM.SQL.Clear;
  if Tipo = 2 then begin
    QrSomaM.SQL.Add('SELECT SUM(IF(Sit=0, (Credito-Debito),');
    QrSomaM.SQL.Add('IF(Sit=1, (Credito-Debito+Pago), 0))) Valor FROM Lanctos');
  end else
    QrSomaM.SQL.Add('SELECT (SUM(Credito) - SUM(Debito)) Valor FROM Lanctos');
  QrSomaM.SQL.Add('WHERE Tipo=:P0 AND Carteira=:P1');
  QrSomaM.Params[0].AsInteger := Tipo;
  QrSomaM.Params[1].AsInteger := Carteira;
  QrSomaM.Open;
  Saldo := Saldo + QrSomaMValor.Value;
  QrSomaM.Close;

  QrUpdM.Close;
  QrUpdM.SQL.Clear;
  QrUpdM.SQL.Add('UPDATE Carteiras SET Saldo=:P0');
  QrUpdM.SQL.Add('WHERE Tipo=:P1 AND Codigo=:P2');
  QrUpdM.Params[0].AsFloat := Saldo;
  QrUpdM.Params[1].AsInteger := Tipo;
  QrUpdM.Params[2].AsInteger := Carteira;
  QrUpdM.ExecSQL;
(*  if Localiza = 3 then
  begin
    if Dmod.QrLanctos.State in [dsBrowse] then
    if (Dmod.QrLanctosTipo.Value = Tipo)
    and (Dmod.QrLanctosCarteira.Value = Carteira) then
    Localiza := 1;
    Dmod.DefParams;
  end;
  if Localiza = 1 then GOTOx.LocalizaCodigo(Carteira, Carteira);*)
end;

procedure TDmod.VerificaSenha(Index: Integer; Login, Senha: String);
begin
  if (Senha = VAR_BOSS) or (Senha = CO_MASTER) then VAR_SENHARESULT := 2
  else
  begin
    if Index = 7 then
    begin
(*      QrSenha.Close;
      QrSenha.Params[0].AsString := Login;
      QrSenha.Params[1].AsString := Senha;
      QrSenha.Open;
      if QrSenha.RecordCount > 0 then
      begin
        QrPerfis.Close;
        QrPerfis.Params[0].AsInteger := QrSenhaPerfilW.Value;
        QrPerfis.Open;
        if QrPerfisVendas.Value = 'V' then VAR_SENHARESULT := 2
        else Geral.MensagemBox(0,'Acesso negado. Senha Inv�lida',
        'Permiss�o por senha', MB_OK+MB_ICONEXCLAMATION);
        QrPerfis.Close;
      end else ShowMessage('Login inv�lido.');
      QrSenha.Close;                      *)
    end else ShowMessage('Em constru��o');
  end;
end;

procedure TDmod.QrMasterAfterOpen(DataSet: TDataSet);
begin
  if Trim(QrMasterCNPJ.Value) = CO_VAZIO then
    GOTOy.AvisoIndef(1);
  //
  FmPrincipal.Caption := Application.Title+'  ::  '+QrMasterEm.Value+' # '+
  QrMasterCNPJ.Value;
  //
  ReopenControle();
  ReopenOpcoesDCon();
  //
  if QrControle.FieldByName('SoMaiusculas').AsString = 'V' then
    VAR_SOMAIUSCULAS := True;
  //
  VAR_UFPADRAO     := QrControle.FieldByName('UFPadrao').AsInteger;
  VAR_CIDADEPADRAO := QrControle.FieldByName('Cidade').AsString;
  VAR_TRAVACIDADE  := QrControle.FieldByName('TravaCidade').AsInteger;
  VAR_MOEDA        := QrControle.FieldByName('Moeda').AsString;
  //
  VAR_CARTVEN      := QrControle.FieldByName('CartVen').AsInteger;
  VAR_CARTCOM      := QrControle.FieldByName('CartCom').AsInteger;
  VAR_CARTRES      := QrControle.FieldByName('CartReS').AsInteger;
  VAR_CARTDES      := QrControle.FieldByName('CartDeS').AsInteger;
  VAR_CARTREG      := QrControle.FieldByName('CartReG').AsInteger;
  VAR_CARTDEG      := QrControle.FieldByName('CartDeG').AsInteger;
  VAR_CARTCOE      := QrControle.FieldByName('CartCoE').AsInteger;
  VAR_CARTCOC      := QrControle.FieldByName('CartCoC').AsInteger;
  VAR_CARTEMD      := QrControle.FieldByName('CartEmD').AsInteger;
  VAR_CARTEMA      := QrControle.FieldByName('CartEmA').AsInteger;
  //
  VAR_PAPERSET     := True;
  VAR_PAPERTOP     := QrControle.FieldByName('PaperTop').AsInteger;
  VAR_PAPERLEF     := QrControle.FieldByName('PaperLef').AsInteger;
  VAR_PAPERWID     := QrControle.FieldByName('PaperWid').AsInteger;
  VAR_PAPERHEI     := QrControle.FieldByName('PaperHei').AsInteger;
  VAR_PAPERFCL     := QrControle.FieldByName('PaperFcl').AsInteger;
  //
end;

procedure TDmod.QrMasterCalcFields(DataSet: TDataSet);
begin
(*  case QrMasterTipo.Value of
    0: QrMasterEm.Value := QrMasterRazaoSocial.Value;
    1: QrMasterEm.Value := QrMasterNome.Value;
    else QrMasterEm.Value := QrMasterRazaoSocial.Value;
  end;*)
  QrMasterTE1_TXT.Value  := Geral.FormataTelefone_TT(QrMasterETe1.Value);
  QrMasterCEP_TXT.Value  := Geral.FormataCEP_NT(QrMasterECEP.Value);
  QrMasterCNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrMasterCNPJ.Value);
end;

function TDmod.TabelasQueNaoQueroCriar: String;
begin
end;

procedure TDmod.DataModuleDestroy(Sender: TObject);
begin
  WSACleanup;
end;

procedure TDmod.AtivaDesativaCliente(Cliente, Entidade: Integer; Ativo: Boolean);
const
  Cns_Msg_Erro = 'Falha ao atualiza cliente!';
var
  AtivoStr1, AtivoStr2: String;
  Controle, AtivoInt: Integer;
begin
  AtivoInt := Geral.BoolToInt(Ativo);
  //
  if AtivoInt = 0 then
  begin
    AtivoStr1 := 'desativar';
    AtivoStr2 := 'desativados';
  end else
  begin
    AtivoStr1 := 'ativar';
    AtivoStr2 := 'ativados';
  end;
  //
  if Geral.MB_Pergunta('Realmente deseja ' + AtivoStr1 + ' o cliente selecionado?' +
    sLineBreak + 'Os seguintes itens tamb�m ser�o ' + AtivoStr2 + '!' + sLineBreak +
    '* Entidade referente ao cliente' + sLineBreak + '* Contrato(s)' + sLineBreak +
    '* Arrecada��o(�es)' + sLineBreak + '* Usu�rio(s) Web') = ID_YES then
  begin
    if not DBCheck.LiberaPelaSenhaBoss then Exit;
    //
    if DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrWebParams, 1) then
    begin
      Screen.Cursor := crHourGlass;
      try
        //Desativa o cliente
        if not UnDmkDAC_PF.ExecutaMySQLQuery0(QrUpd, Dmod.MyDB, [
          'UPDATE clientes ',
          'SET Ativo=' + Geral.FF0(AtivoInt),
          'WHERE Codigo=' + Geral.FF0(Cliente),
          '']) then
        begin
          Geral.MB_Erro(Cns_Msg_Erro);
          Exit;
        end;
        //Desativa a entidade
        if not UnDmkDAC_PF.ExecutaMySQLQuery0(QrUpd, Dmod.MyDB, [
          'UPDATE entidades ',
          'SET Ativo=' + Geral.FF0(AtivoInt),
          'WHERE Codigo=' + Geral.FF0(Entidade),
          '']) then
        begin
          Geral.MB_Erro(Cns_Msg_Erro);
          Exit;
        end;
        //Desativa o(s) contrato(s)

        //Desativa a(s) arrecada��o(�es)
        UnDmkDAC_PF.AbreMySQLQuery0(QrAux, Dmod.MyDB, [
          'SELECT Controle ',
          'FROM arreits ',
          'WHERE Entidade=' + Geral.FF0(Entidade),
          '']);
        if QrAux.RecordCount > 0 then
        begin
          while not QrAux.Eof do
          begin
            Controle := QrAux.FieldByName('Controle').AsInteger;
            //
            if not UBloquetos.AtivaDesativaArreIts(Controle, AtivoInt, False) then
            begin
              Geral.MB_Erro(Cns_Msg_Erro);
              Exit;
            end;
            QrAux.Next;
          end;
        end;
        (*
        UnDmkDAC_PF.ExecutaMySQLQuery0(QrUpdN, Dmod.MyDBn, [
          'UPDATE wusers ',
          'SET Ativo=' + Geral.FF0(AtivoInt),
          'WHERE Entidade=' + Geral.FF0(Entidade),
          '']);
        *)
      finally
        Screen.Cursor := crDefault;
      end;
    end;
  end;
end;

function TDMod.Privilegios(Usuario : Integer) : Boolean;
begin
  Result := False;
  FM_MASTER := 'F';
  Dmod.QrPerfis.Params[0].AsInteger := Usuario;
  Dmod.QrPerfis.Open;
  if Dmod.QrPerfis.RecordCount > 0 then
  begin
    Result := True;
    //FM_EQUIPAMENTOS             := Dmod.QrPerfisEquipamentos.Value;
    //FM_MARCAS          := Dmod.QrPerfisMarcas.Value;
    //FM_MODELOS                := Dmod.QrPerfisModelos.Value;
    //FM_SERVICOS            := Dmod.QrPerfisServicos.Value;
    //FM_PRODUTOS            := Dmod.QrPerfisProdutos.Value;
  end;
  Dmod.QrPerfis.Close;
end;

procedure TDmod.ExcluiItensEstqNFsCouroImportadas();
begin
  // Compatibilidade
end;

procedure TDmod.GeraEstoqueEm_2(Empresa_Txt: String; DataIni,
  DataFim: TDateTime; PGT, GG1, GG2, GG3, TipoPesq: Integer; GraCusPrc,
  TabePrcCab: Variant; GraCusPrcCodigo, TabePrcCabCodigo, PrdGrupTipCodigo,
  GraGru1Nivel1, GraGru2Nivel2, GraGru3Nivel3: Integer; QrPGT_SCC, QrAbertura2a,
  QrAbertura2b: TmySQLQuery; UsaTabePrcCab, SoPositivos: Boolean;
  LaAviso: TLabel; NomeTb_SMIC: String; QrSMIC_X: TmySQLQuery; var TxtSQL_Part1,
  TxtSQL_Part2: String);
begin
  Geral.MensagemBox('Gera��o de estoque n�o implementada!' + #13#10 +
  'Solicite � DERMATEK!', 'Mensagem', MB_OK+MB_ICONWARNING);
end;

procedure TDmod.InsereRegistrosExtraBalanceteMensal_01(QrInd01: TMySQLQuery;
DataIDate, DataFDate: TDateTime; DataITrue, DataFTrue: Boolean;
CliInt: Integer; Campos: String; DescrSubstit: Boolean);
begin
  Geral.MensagemBox('Este balancete n�o possui dados industriais ' +
  'adicionais neste aplicativo!', 'Aviso', MB_OK+MB_ICONWARNING);
end;

procedure TDmod.AlteraSMIA(Qry: TmySQLQuery);
begin
  // Compatibilidade?
end;

function TDmod.BuscaProximoMovix: Integer;
begin
  QrSel.Close;
  QrSel.Open;
  Result := QrSelMovix.Value;
end;

procedure TDmod.QrBinaLigouACalcFields(DataSet: TDataSet);
begin
  QrBinaLigouATELEFONE.Value := Geral.FormataTelefone_TT(QrBinaLigouAnome.Value);
end;

procedure TDmod.QrControleAfterOpen(DataSet: TDataSet);
begin
  VAR_MEULOGOPATH := QrControle.FieldByName('MeuLogoPath').AsString;
  if FileExists(VAR_MEULOGOPATH) then
    VAR_MEULOGOEXISTE := True
  else
    VAR_MEULOGOEXISTE := False;
end;

end.
