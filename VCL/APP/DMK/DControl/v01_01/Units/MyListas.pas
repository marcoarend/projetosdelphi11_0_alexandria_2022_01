unit MyListas;

interface

uses
  System.Generics.Collections,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs, Forms,
  ExtCtrls, ComCtrls, StdCtrls, DB, (*DBTables,*) mysqlDBTables, UnMyLinguas,
  UnInternalConsts, UnMLAGeral, dmkGeral, UnDmkProcFunc, UnDmkEnums,
  UnProjGroup_Consts, UnGrl_Vars;

type
  TMyListas = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    function CriaListaImpDOS(FImpDOS: TStringList): Boolean;
    function CriaListaUserSets(FUserSets: TStringList): Boolean;
    //
    function CriaListaJanelas(FLJanelas: TList<TJanelas>): Boolean;
    function CriaListaTabelas(Database: TmySQLDatabase; Lista:
             TList<TTabelas>): Boolean;
    function CriaListaTabelasLocais(Lista: TList<TTabelas>): Boolean;
    function CriaListaIndices(TabelaBase, TabelaNome: String;
             FLIndices: TList<TIndices>): Boolean;
    function CriaListaCampos(Tabela: String; FLCampos: TList<TCampos>;
             var TemControle: TTemControle): Boolean;
    function CriaListaQeiLnk(TabelaCria: String; FLQeiLnk: TList<TQeiLnk>): Boolean;
    function CriaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
    function CompletaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
    function ExcluiTab: Boolean;
    function ExcluiReg: Boolean;
    function ExcluiIdx: Boolean;
    procedure VerificaOutrosAntes(DataBase: TmySQLDatabase; Memo: TMemo);
    procedure VerificaOutrosDepois(DataBase: TmySQLDatabase; Memo: TMemo);
    procedure ModificaDadosDeIndicesAlterados(Indice, Tabela: String;
              DataBase: TmySQLDatabase; Memo: TMemo);
    procedure ConfiguracoesIniciais(UsaCoresRel: integer; AppIDtxt: String);
  end;

(*
const
  CO_VERMCW = 2103281411;
  CO_VERMLA = 2103281411;
  CO_VERSAO = 2103281411;
  CO_SIGLA_APP = 'DCON';
  CO_DMKID_APP = 17;
  CO_GRADE_APP = False;
  CO_VLOCAL = True;
  CO_EXTRA_LCT_003 = False; //True somente para Credito2
  WLAN_CTOS = 'wlanctos';
*)
var
  MyList: TMyListas;
  FRCampos  : TCampos;
  FRIndices : TIndices;
  FRJanelas : TJanelas;
  //
  _ArrClieSets: array[01..32] of String;
  _MaxClieSets: Integer;
  _ArrFornSets: array[01..32] of String;
  _MaxFornSets: Integer;

implementation

uses Module, ModuleGeral, UMySQLModule, UnIBGE_DTB_Tabs, UnCNAB_Tabs,
  UnProtocol_Tabs, Arquivos_Tabs, WUsers_Tabs, Enquete_Tabs, CallCenter_Tabs,
  UnPerfJan_Tabs, UnContrat_Tabs, TelCta_Tabs, MyDBCheck, NFSe_Tabs, NFSe_TbTerc,
  Geral_TbTerc, Mail_Tabs, NFe_Tabs, UnAll_Tabs, UnAnotacoes_Tabs, UnEnti_Tabs,
  UnFinance_Tabs, UnMoedas_Tabs, UnEmpresas_Tabs, UnDocsCab_Tabs, UnServicos_Tabs,
  WEventos_Tabs, UnEntities, UnTextos_Tabs, UnAgenda_Tabs2, UnVersoes_Tabs,
  UnNotificacoes_Tabs, WGerl_Tabs, Web_Tabs, UnHelp_Tabs, UnGrl_Tabs,
  UnReCaptcha_Tabs, UnBloq_0202_Tabs, UnBloq_Tabs, UnDmkACBr_Tabs;

function TMyListas.CriaListaTabelas(Database: TmySQLDatabase; Lista:
 TList<TTabelas>): Boolean;
  procedure TabelasPorCliente((*Tab: TTabsCli*));
  var
    TbCI: String;
    CliInt: Integer;
  begin
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SHOW TABLES');
    Dmod.QrAux.SQL.Add('FROM ' + TMeuDB);
    Dmod.QrAux.SQL.Add('LIKE "entidades"');
    UMyMod.AbreQuery(Dmod.QrAux, Dmod.MyDB);
    if Dmod.QrAux.RecordCount > 0 then
    begin
      Dmod.QrAux.Close;
      Dmod.QrAux.SQL.Clear;
      Dmod.QrAux.SQL.Add('SELECT cliint');
      Dmod.QrAux.SQL.Add('FROM entidades');
      Dmod.QrAux.SQL.Add('WHERE cliint <> 0');
      UMyMod.AbreQuery(Dmod.QrAux, Dmod.MyDB);
      //
      while not Dmod.QrAux.Eof do
      begin
        CliInt := Dmod.QrAux.FieldByName('cliint').AsInteger;
        { N�o permitir n�meros negativos para filial!
        if CliInt < 0 then
          TbCI := '_' + FormatFloat('000', CliInt )
        else
          TbCI := FormatFloat('0000', CliInt);
        }
        if CliInt > 0 then
        begin
          TbCI := FormatFloat('0000', CliInt);
          //
          MyLinguas.AdTbLst(FTabelas, False, Lowercase('lct' + TbCI + 'A'), Lowercase(LAN_CTOS));
          MyLinguas.AdTbLst(FTabelas, False, Lowercase('lct' + TbCI + 'B'), Lowercase(LAN_CTOS));
          MyLinguas.AdTbLst(FTabelas, False, Lowercase('lct' + TbCI + 'D'), Lowercase(LAN_CTOS));
        end;
        //
        Dmod.QrAux.Next;
      end;
    end;
  end;
begin
  Result := True;
  try
    if Database = Dmod.MyDB then
    begin
      All_Tabs.CarregaListaTabelas(FTabelas);
      Anotacoes_Tabs.CarregaListaTabelas(FTabelas);
      Enti_Tabs.CarregaListaTabelas(FTabelas);
      Finance_Tabs.CarregaListaTabelas(FTabelas);
      Finance_Tabs.ComplementaListaComLcts(Lista);
      Moedas_Tabs.CarregaListaTabelas(FTabelas);
      Empresas_Tabs.CarregaListaTabelas(FTabelas);
      Bloq_Tabs.CarregaListaTabelas(Database, Lista, CO_DMKID_APP);
      Bloq_0202_Tabs.CarregaListaTabelas(Lista);
      CNAB_Tabs.CarregaListaTabelas(Database, Lista);
      Protocol_Tabs.CarregaListaTabelas(Database, Lista);
      PerfJan_Tabs.CarregaListaTabelas(Lista);
      IBGE_DTB_Tabs.CarregaListaTabelas(Lista);
      Enquete_Tb.CarregaListaTabelas(Lista);
      CallCenter_Tb.CarregaListaTabelas(Lista);
      Contrat_Tabs.CarregaListaTabelas(Lista);
      TelCta_Tb.CarregaListaTabelas(Lista);
      UnNFSe_Tabs.CarregaListaTabelas(Database.Databasename, Lista);
      Mail_Tb.CarregaListaTabelas(Lista);
      DocsCab_Tabs.CarregaListaTabelas(Lista);
      Servicos_Tabs.CarregaListaTabelas(Database, Lista);
      Agenda_Tabs2.CarregaListaTabelas(Database, Lista);
      NFe_Tb.CarregaListaTabelas(Database, Lista);
      Versoes_Tabs.CarregaListaTabelas(Database, Lista);
      Notificacoes_Tabs.CarregaListaTabelas(Lista);
      Textos_Tabs.CarregaListaTabelas(Database, Lista);
      //
      Help_Tabs.CarregaListaTabelas(Database, Lista);
      Grl_Tabs.CarregaListaTabelas(Lista);
      Web_Tb.CarregaListaTabelas(Database.Databasename, Lista, CO_DMKID_APP);
      DmkACBr_Tabs.CarregaListaTabelas(FTabelas);
      //
      MyLinguas.AdTbLst(Lista, False, Lowercase('backupdir'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('cliaplic'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('cliaplicmo'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('clientes'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('cliobser'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('condef'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('condefapl'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('controle'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('gerglyfs'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('gerglyfsit'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('mysqlconf'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('opcoesdcon'), '');
    end else
    if Database = Dmod.MyDBn then
    begin
      Arquivos_Tb.CarregaListaTabelas(Database, Lista);
      WUsers_Tb.CarregaListaTabelas(Database, Lista, CO_DMKID_APP);
      WGerl_Tb.CarregaListaTabelas(Database, Lista, CO_DMKID_APP);
      Protocol_Tabs.CarregaListaTabelas(Database, Lista);
      Servicos_Tabs.CarregaListaTabelas(Database, Lista);
      Versoes_Tabs.CarregaListaTabelas(Database, Lista);
      WEventos_Tb.CarregaListaTabelas(Lista);
      //
      UnNFSe_Tabs.CarregaListaTabelas(Database.Databasename, Lista);
      Help_Tabs.CarregaListaTabelas(Database, Lista);
      ReCaptcha_Tabs.CarregaListaTabelas(Lista, CO_DMKID_APP);
      Web_Tb.CarregaListaTabelas(Database.Databasename, Lista, CO_DMKID_APP);
      Agenda_Tabs2.CarregaListaTabelas(Database, Lista);
      Textos_Tabs.CarregaListaTabelas(Database, Lista);
      //
      MyLinguas.AdTbLst(Lista, False, Lowercase('clientes'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('cliaplic'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('cliaplicmo'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('clidb'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('cliwebapp'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('cliwebappmo'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('mysqlconf'), '');
      //
      //MyLinguas.AdTbLst(Lista, False, LowerCase('arreits'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('bloopcoes'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('carteiras'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('cnab_cfg'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('enticliint'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('entidades'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('enticontat'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('enticonent'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('entimail'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('entitel'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('bacen_pais'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('dtb_munici'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('listalograd'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('feriados'), '');
      MyLinguas.AdTbLst(Lista, TRUE, LowerCase('Lanctoz'), LAN_CTOS);
      MyLinguas.AdTbLst(Lista, False, LowerCase('prev'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('preemail'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('preemmsg'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('emailconta'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('ufs'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('entitipcto'), '');
      //
      MyLinguas.AdTbLst(Lista, False, Lowercase('wentidades'), 'entidades',  True);
      MyLinguas.AdTbLst(Lista, False, Lowercase('wenticonta'), 'enticontat', True);
      MyLinguas.AdTbLst(Lista, False, Lowercase('wentimail'),  'entimail',   True);
      MyLinguas.AdTbLst(Lista, False, Lowercase('wentitel'),   'entitel',    True);
      MyLinguas.AdTbLst(Lista, False, Lowercase('wentitipct'), 'entitipcto', True);
      MyLinguas.AdTbLst(Lista, False, Lowercase('wcarteiras'), 'carteiras',  True);
      MyLinguas.AdTbLst(Lista, False, Lowercase('wcontas'),    'contas',     True);
      MyLinguas.AdTbLst(Lista, False, Lowercase('wcontas'),    'contas',     True);
      MyLinguas.AdTbLst(Lista, False, LowerCase('wlctqrcode'), 'lctqrcode',  True);
      MyLinguas.AdTbLst(Lista, False, LowerCase(WLAN_CTOS),    LAN_CTOS,     True);
      //
      TabelasPorCliente();
      //Somente WEB
      //MyLinguas.AdTbLst(Lista, False, Lowercase('Licencas'), '');
      //
      //Verificar se usa
      MyLinguas.AdTbLst(Lista, False, Lowercase('wajuda'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('whelpfiles'), '');
    end
    else if Database = DModG.AllID_DB then
    begin
      IBGE_DTB_Tabs.CarregaListaTabelas(FTabelas);
      UnNFSe_TbTerc.CarregaListaTabelas(Lista);
      UnGeral_TbTerc.CarregaListaTabelas(Lista);
      NFe_Tb.CarregaListaTabelas(Database, Lista);
    end;
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaTabelasLocais(Lista: TList<TTabelas>): Boolean;
begin
  Result := True;
  try
    Finance_Tabs.CarregaListaTabelasLocais(Lista);
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
begin
  Result := True;
  try
    if Uppercase(Tabela) = Uppercase('opcoesdcon') then
    begin
      FListaSQL.Add('Codigo');
      FListaSQL.Add('1');
    end else
    if Uppercase(Tabela) = Uppercase('clientes') then
    begin
      FListaSQL.Add('Codigo|Cliente|    Cli_Id');
      FListaSQL.Add('    30|    -11|"dermatek"');
    end else
    if Uppercase(Tabela) = Uppercase('clidb') then
    begin
      FListaSQL.Add('Cliente');
      FListaSQL.Add('    -11');
    end else
    if Uppercase(Tabela) = Uppercase('cliwebapp') then
    begin
      FListaSQL.Add('Cliente|App');
      FListaSQL.Add('    -11|  1');
      FListaSQL.Add('    -11|  6');
    end else
    begin
      All_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      Anotacoes_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      Enti_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      Finance_Tabs.CarregaListaSQL(Tabela, FListaSQL, CO_DMKID_APP);
      Moedas_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      Empresas_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      Bloq_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      Bloq_0202_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      CNAB_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      Protocol_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      PerfJan_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      IBGE_DTB_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      Arquivos_Tb.CarregaListaSQL(Tabela, FListaSQL);
      WUsers_Tb.CarregaListaSQL(Tabela, FListaSQL, CO_DMKID_APP);
      WGerl_Tb.CarregaListaSQL(Tabela, FListaSQL, CO_DMKID_APP);
      Web_Tb.CarregaListaSQL(Dmod.MyDB, Tabela, FListaSQL, CO_DMKID_APP);
      Enquete_Tb.CarregaListaSQL(Tabela, FListaSQL);
      CallCenter_Tb.CarregaListaSQL(Tabela, FListaSQL);
      Contrat_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      TelCta_Tb.CarregaListaSQL(Tabela, FListaSQL);
      UnNFSe_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      Mail_Tb.CarregaListaSQL(Tabela, FListaSQL);
      NFe_Tb.CarregaListaSQL(Tabela, FListaSQL);
      DocsCab_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      Servicos_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      WEventos_Tb.CarregaListaSQL(Tabela, FListaSQL);
      Versoes_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      Agenda_Tabs2.CarregaListaSQL(Tabela, FListaSQL);
      Notificacoes_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      Textos_Tabs.CarregaListaSQL(Tabela, FListaSQL, CO_DMKID_APP);
      //
      Help_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      ReCaptcha_Tabs.CarregaListaSQL(Tabela, FListaSQL);
      //
      UnNFSe_TbTerc.CarregaListaSQL(Tabela, FListaSQL);
      UnGeral_TbTerc.CarregaListaSQL(Tabela, FListaSQL);
      DmkACBr_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    end;
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CompletaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
begin
  Result := True;
  try
    if Uppercase(Tabela) = Uppercase('contas') then
    begin
      (*FListaSQL.Add('-129,"Compra de mercadorias diversas"');
      FListaSQL.Add('-130,"Frete de mercadorias"');
      FListaSQL.Add('-131,"Compra de filmes"');
      FListaSQL.Add('-132,"Frete de filmes"');
      FListaSQL.Add('-133,"Inv�lido"');
      FListaSQL.Add('-134,"Venda e/ou loca��o"');*)
    end;
    //
    All_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Anotacoes_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Enti_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Finance_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Moedas_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Empresas_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    PerfJan_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Bloq_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Bloq_0202_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    CNAB_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Arquivos_Tb.ComplementaListaSQL(Tabela, FListaSQL);
    WUsers_Tb.ComplementaListaSQL(Tabela, FListaSQL);
    WGerl_Tb.ComplementaListaSQL(Tabela, FListaSQL);
    Web_Tb.ComplementaListaSQL(Tabela, FListaSQL);
    Protocol_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Enquete_Tb.ComplementaListaSQL(Tabela, FListaSQL);
    CallCenter_Tb.ComplementaListaSQL(Tabela, FListaSQL);
    Contrat_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    TelCta_Tb.ComplementaListaSQL(Tabela, FListaSQL);
    UnNFSe_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Mail_Tb.ComplementaListaSQL(Tabela, FListaSQL);
    DocsCab_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Servicos_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    WEventos_Tb.ComplementaListaSQL(Tabela, FListaSQL);
    Versoes_Tabs.ComplementaListaSQL(Tabela, FListaSQL, CO_DMKID_APP);
    Agenda_Tabs2.ComplementaListaSQL(Tabela, FListaSQL);
    Notificacoes_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Textos_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    //
    Help_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    ReCaptcha_Tabs.ComplementaListaSQL(Tabela, FListaSQL, CO_DMKID_APP);
    Web_Tb.ComplementaListaSQL(Tabela, FListaSQL);
    //
    UnNFSe_TbTerc.ComplementaListaSQL(Tabela, FListaSQL);
    UnGeral_TbTerc.ComplementaListaSQL(Tabela, FListaSQL);
    DmkACBr_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaIndices(TabelaBase, TabelaNome: String;
  FLIndices: TList<TIndices>): Boolean;
begin
  Result := True;
  try
    if Uppercase(TabelaBase) = Uppercase('backupdir') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('cliaplic') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'UNIQUE1';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'UNIQUE1';
      FRIndices.Seq_in_index  := 2;
      FRIndices.Column_name   := 'Aplicativo';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('cliaplicmo') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Conta';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('clientes') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'UNIQUE1';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Cliente';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'UNIQUE2';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Cli_Id';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('clidb') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Cliente';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('cliwebapp') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Cliente';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 2;
      FRIndices.Column_name   := 'App';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('cliwebappmo') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Cliente';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 2;
      FRIndices.Column_name   := 'App';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 3;
      FRIndices.Column_name   := 'Modulo';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('cliobser') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('condef') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('condefapl') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('gerglyfs') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('gerglyfsit') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'UNIQUE1';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Caminho';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('mysqlconf') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('opcoesdcon') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('wajuda') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('whelpfiles') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else
    begin
      All_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Anotacoes_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Enti_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Finance_Tabs.CarregaListaFRIndices(TabelaBase, TabelaNome, FRIndices, FLindices);
      Moedas_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      Empresas_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      Bloq_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Bloq_0202_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      CNAB_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Protocol_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      PerfJan_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      IBGE_DTB_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      Arquivos_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      WUsers_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      WGerl_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Enquete_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      CallCenter_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Contrat_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      TelCta_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      UnNFSe_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Mail_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      NFe_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      DocsCab_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      Servicos_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      WEventos_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      Versoes_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      Agenda_Tabs2.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      Notificacoes_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      Textos_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      //
      Help_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      ReCaptcha_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      //
      Web_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      Grl_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      //
      UnNFSe_TbTerc.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      UnGeral_TbTerc.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      DmkACBr_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
    end;
  except
    raise;
    Result := False;
  end;
end;

procedure TMyListas.ConfiguracoesIniciais(UsaCoresRel: integer;
  AppIDtxt: String);
begin
  dmkPF.ConfigIniApp(UsaCoresRel);
  if Uppercase(AppIDtxt) = 'DCONTROL' then
  begin
    Entities.ConfiguracoesIniciaisEntidades(CO_DMKID_APP);
  end else
    Geral.MensagemBox(
    'Database para configura��es de "CheckBox" n�o definidos!', 'Aviso!',
    MB_OK+MB_ICONWARNING)
end;

function TMyListas.CriaListaCampos(Tabela: String; FLCampos: TList<TCampos>;
var TemControle: TTemControle): Boolean;
begin
  TemControle := TemControle + cTemControleNao;
  try
    if Uppercase(Tabela) = Uppercase('backupdir') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Caminho';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Destino';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FTPWebDir';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SubDir';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Zipar';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Encrypt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SenhaCrypt';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Web';         //0 = Local
      FRCampos.Tipo       := 'tinyint(1)';  //1 = Web
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OmitirExt';   //0 = N�o
      FRCampos.Tipo       := 'tinyint(1)';  //1 = Sim
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ExtOmit';
      FRCampos.Tipo       := 'text';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UltimoIni';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UltimoFim';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('cliaplic') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'MUL';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Aplicativo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Beta';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FimData';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QtdLicen';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QtdCliInt';   //0 para n�o controlar
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QtdUsuarios';   //0 para n�o controlar
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      // D E P R E C A D O => In�cio
      New(FRCampos);
      FRCampos.Field      := 'StatAplic';   // 0 - Bloqueado
      FRCampos.Tipo       := 'tinyint(1)';  // 1 - Monitorado
      FRCampos.Null       := '';            // 2 - Vencido
      FRCampos.Key        := '';            // 3 - Alugado
      FRCampos.Default    := '-1';          // 4 - Liberado
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IGPM';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Valor';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FimChek';
      FRCampos.Tipo       := 'varchar(32)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NomeServer';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EntiMinD';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EntiAtzD';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EntiNCad';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LctsMinD';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LctsAtzD';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LctsNCad';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Telefone';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EMail';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NomeWeb';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      // D E P R E C A D O => Fim
    end else
    if Uppercase(Tabela) = Uppercase('cliaplicmo') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Modulo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ModuloStr';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Descri';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('clientes') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cliente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cli_Id';
      FRCampos.Tipo       := 'varchar(15)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '';

      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('clidb') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Cliente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DB';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('cliwebapp') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Cliente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'App';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FimData';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QtdLicen'; //0 para n�o controlar
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QtdCliInt';   //0 para n�o controlar
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QtdUsuarios';   //0 para n�o controlar
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QtdAvulso';   //0 para n�o controlar
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('cliwebappmo') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Cliente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'App';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Modulo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('cliobser') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Texto';
      FRCampos.Tipo       := 'text';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('condef') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Observ';
      FRCampos.Tipo       := 'varchar(150)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('condefapl') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Aplicativo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('controle') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'MySQLConf';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GerGlyfs';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GerGlyfsIt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BackupDir';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Clientes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CliAplic';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CliAplicMo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CliObser';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ConDef';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ConDefApl';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LiqStaAnt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LiqStaVct';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LiqStaVcd';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LiqOcoCDi';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LiqOcoCOc';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LiqOcoShw';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('gerglyfs') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Descri';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Descri2';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('gerglyfsit') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pasta';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Caminho';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('mysqlconf') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Web_Host';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Web_User';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Web_Pwd';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Web_DB';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Web_Porta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '3306';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
(*
    end else
    if Uppercase(Tabela) = Uppercase('Licencas') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'CNPJ';
      FRCampos.Tipo       := 'varchar(18)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Aplicativo';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NomePC';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Usuario';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Info_01';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Info_02';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Info_03';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataL';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
*)
    end else
    if Uppercase(Tabela) = Uppercase('opcoesdcon') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ApliDiaAvi';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ApliDiaPro';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CaminhoGlyfs';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('wajuda') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Endereco';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Geral';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('whelpfiles') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'varchar(25)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Descri';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tags';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
    end else
    begin
      All_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Anotacoes_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Enti_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Finance_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Moedas_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Empresas_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Bloq_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Bloq_0202_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      PerfJan_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      CNAB_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      IBGE_DTB_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Arquivos_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      WUsers_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle, CO_DMKID_APP);
      WGerl_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle, CO_DMKID_APP);
      Protocol_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Enquete_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      CallCenter_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Contrat_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      TelCta_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      UnNFSe_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Mail_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      NFe_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      DocsCab_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Servicos_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      WEventos_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Versoes_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Agenda_Tabs2.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Notificacoes_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Textos_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      //
      Help_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, CO_DMKID_APP, TemControle);
      ReCaptcha_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      //
      Web_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, CO_DMKID_APP, TemControle);
      Grl_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      //
      UnNFSe_TbTerc.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      UnGeral_TbTerc.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      DmkACBr_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
    end;
    All_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Anotacoes_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Enti_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Finance_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Moedas_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Empresas_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Bloq_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Bloq_0202_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    PerfJan_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    CNAB_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Arquivos_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    WUsers_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    WGerl_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Protocol_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Enquete_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    CallCenter_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Contrat_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    TelCta_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    UnNFSe_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Mail_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    NFe_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    DocsCab_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Servicos_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    WEventos_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Versoes_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Agenda_Tabs2.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Notificacoes_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Textos_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    //
    Help_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
    Web_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    //
    UnNFSe_TbTerc.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    UnGeral_TbTerc.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    DmkACBr_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    //
    Result := True;
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.ExcluiTab: Boolean;
begin
  Result := True;
end;

function TMyListas.ExcluiReg: Boolean;
begin
  Result := True;
end;

function TMyListas.ExcluiIdx: Boolean;
begin
  Result := True;
end;

function TMyListas.CriaListaUserSets(FUserSets: TStringList): Boolean;
begin
  Result := True;
  try
    FUserSets.Add('Edi��o de Itens de Mercadoria;C�digos Fiscais');
    FUserSets.Add('Edi��o de Itens de Mercadoria;Comiss�o de Representante');
    FUserSets.Add('Edi��o de Itens de Mercadoria;Comiss�o de Vendedor');
    FUserSets.Add('Edi��o de Itens de Mercadoria;IPI');
    FUserSets.Add('Edi��o de Itens de Mercadoria;ICMS');
    //
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaImpDOS(FImpDOS: TStringList): Boolean;
begin
  Result := True;
  try
    FImpDOS.Add('1001;01;NF (Topo)');
    FImpDOS.Add('1002;01;Sa�da [X]');
    FImpDOS.Add('1003;01;Entrada [X]');
    FImpDOS.Add('1004;01;Data emiss�o');
    FImpDOS.Add('1005;01;Data entra/sai');
    FImpDOS.Add('1006;01;C�digo CFOP');
    FImpDOS.Add('1007;01;Descri��o CFOP');
    FImpDOS.Add('1008;01;Base c�lculo ICMS');
    FImpDOS.Add('1009;01;Valor ICMS');
    FImpDOS.Add('1010;01;Base c�lc. ICMS subst.');
    FImpDOS.Add('1011;01;Valor ICMS subst.');
    FImpDOS.Add('1012;01;Valor frete');
    FImpDOS.Add('1013;01;Valor seguro');
    FImpDOS.Add('1014;01;Outras desp. aces.');
    FImpDOS.Add('1015;01;Valor total IPI');
    FImpDOS.Add('1016;01;Valor total produtos');
    FImpDOS.Add('1017;01;Valor total servicos');
    FImpDOS.Add('1018;01;Valor total nota');
    FImpDOS.Add('1019;01;Placa ve�culo');
    FImpDOS.Add('1020;01;UF placa ve�culo');
    FImpDOS.Add('1021;01;Vol. Quantidade');
    FImpDOS.Add('1022;01;Vol. Esp�cie');
    FImpDOS.Add('1023;01;Vol. Marca');
    FImpDOS.Add('1024;01;Vol. N�mero');
    FImpDOS.Add('1025;01;Vol. kg bruto');
    FImpDOS.Add('1026;01;Vol. kg l�quido');
    FImpDOS.Add('1027;01;Dados adicionais');
    FImpDOS.Add('1028;01;Frete por conta de ...');
    FImpDOS.Add('1029;01;Desconto especial');
    FImpDOS.Add('1030;01;NF (rodap�)');
    //
    FImpDOS.Add('2001;02;Nome ou Raz�o Social');
    FImpDOS.Add('2002;02;CNPJ ou CPF');
    FImpDOS.Add('2003;02;Endere�o');
    FImpDOS.Add('2004;02;Bairro');
    FImpDOS.Add('2005;02;CEP');
    FImpDOS.Add('2006;02;Cidade');
    FImpDOS.Add('2007;02;Telefone');
    FImpDOS.Add('2008;02;UF');
    FImpDOS.Add('2009;02;I.E. ou RG');
    FImpDOS.Add('2010;02;I.E.S.T.');
    //
    FImpDOS.Add('6001;06;Nome ou Raz�o Social');
    FImpDOS.Add('6002;06;CNPJ ou CPF');
    FImpDOS.Add('6003;06;Endere�o');
    FImpDOS.Add('6004;06;Bairro');
    FImpDOS.Add('6005;06;CEP');
    FImpDOS.Add('6006;06;Cidade');
    FImpDOS.Add('6007;06;Telefone');
    FImpDOS.Add('6008;06;UF');
    FImpDOS.Add('6009;06;I.E. ou RG');
    FImpDOS.Add('6010;06;I.E.S.T.');
    //
    FImpDOS.Add('3001;03;Descri��o');
    FImpDOS.Add('3002;03;Classifica��o Fiscal');
    FImpDOS.Add('3003;03;Situa��o Tribut�ria');
    FImpDOS.Add('3004;03;Unidade');
    FImpDOS.Add('3005;03;Quantidade');
    FImpDOS.Add('3006;03;Valor unit�rio');
    FImpDOS.Add('3007;03;Valor total');
    FImpDOS.Add('3008;03;Aliquota ICMS');
    FImpDOS.Add('3009;03;Aliquota IPI');
    FImpDOS.Add('3010;03;Valor IPI');
    FImpDOS.Add('3011;03;CFOP');
    FImpDOS.Add('3012;03;Refer�ncia');
    //
    FImpDOS.Add('4001;04;Descri��o');
    FImpDOS.Add('4002;04;Classifica��o Fiscal');
    FImpDOS.Add('4003;04;Situa��o Tribut�ria');
    FImpDOS.Add('4004;04;Unidade');
    FImpDOS.Add('4005;04;Quantidade');
    FImpDOS.Add('4006;04;Valor unit�rio');
    FImpDOS.Add('4007;04;Valor total');
    FImpDOS.Add('4008;04;Aliquota ICMS');
    FImpDOS.Add('4009;04;Aliquota IPI');
    FImpDOS.Add('4010;04;Valor IPI');
    FImpDOS.Add('4011;04;CFOP');
    FImpDOS.Add('4012;04;Refer�ncia');
    //
    FImpDOS.Add('5001;05;Parcela');
    FImpDOS.Add('5002;05;Valor');
    FImpDOS.Add('5003;05;Vencimento');
    //
  except
    raise;
    Result := False;
  end;
end;

procedure TMyListas.VerificaOutrosAntes(DataBase: TmySQLDatabase; Memo: TMemo);
begin
  //
end;

procedure TMyListas.VerificaOutrosDepois(DataBase: TmySQLDatabase; Memo: TMemo);
begin
  // Nada
end;

procedure TMyListas.ModificaDadosDeIndicesAlterados(Indice, Tabela: String;
  DataBase: TmySQLDatabase; Memo: TMemo);
begin
  // Nada
end;

function TMyListas.CriaListaJanelas(FLJanelas: TList<TJanelas>): Boolean;
begin
  All_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Anotacoes_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Enti_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Finance_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Moedas_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Empresas_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  PerfJan_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Bloq_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Bloq_0202_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  CNAB_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  IBGE_DTB_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Arquivos_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  WUsers_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  WGerl_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Web_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Protocol_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Enquete_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  CallCenter_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Contrat_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  TelCta_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Mail_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  NFe_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  DocsCab_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Servicos_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  WEventos_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Versoes_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Agenda_Tabs2.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Notificacoes_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Textos_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  //
  Help_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  ReCaptcha_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  //
  Result := True;
  //
  // GER-GLYFS-001 :: Gerenciamento de Glyfs
  New(FRJanelas);
  FRJanelas.ID        := 'GER-GLYFS-001';
  FRJanelas.Nome      := 'FmGerGlyfs';
  FRJanelas.Descricao := 'Gerenciamento de Glyfs';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-CLIEN-001 :: Cadastro de Clientes
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-CLIEN-001';
  FRJanelas.Nome      := 'FmClientes';
  FRJanelas.Descricao := 'Cadastro de Clientes';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-CLIEN-002 :: Aplicativos de Clientes
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-CLIEN-002';
  FRJanelas.Nome      := 'FmClientesAplic';
  FRJanelas.Descricao := 'Aplicativos de Clientes';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-CLIEN-004 :: Observa��es de Clientes
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-CLIEN-004';
  FRJanelas.Nome      := 'FmClientesObser';
  FRJanelas.Descricao := 'Observa��es de Clientes';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-CLIEN-005 :: Aplicativos WEB de Clientes
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-CLIEN-005';
  FRJanelas.Nome      := 'FmClientesAplicWeb';
  FRJanelas.Descricao := 'Aplicativos WEB de Clientes';
  FLJanelas.Add(FRJanelas);
  //
  // FER-BADIR-001 :: Backup de Diret�rios
  New(FRJanelas);
  FRJanelas.ID        := 'FER-BADIR-001';
  FRJanelas.Nome      := 'FmBackupDir';
  FRJanelas.Descricao := 'Backup de Diret�rios';
  FLJanelas.Add(FRJanelas);
  //
  // FER-BADIR-002 :: Configura��es do Backup
  New(FRJanelas);
  FRJanelas.ID        := 'FER-BADIR-002';
  FRJanelas.Nome      := 'FmBackupDirIts';
  FRJanelas.Descricao := 'Configura��es do Backup';
  FLJanelas.Add(FRJanelas);
  //
  // FER-OPCAO-002 :: Op��es espec�ficas
  New(FRJanelas);
  FRJanelas.ID        := 'FER-OPCAO-002';
  FRJanelas.Nome      := 'FmOpcoesDControl';
  FRJanelas.Descricao := 'Op��es espec�f�cas';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-CNDEF-001 :: Conditional Defines - Delphi
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-CNDEF-001';
  FRJanelas.Nome      := 'FmConDef';
  FRJanelas.Descricao := 'Conditional Defines - Delphi';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-CNDEF-002 :: Conditional Defines - Itens
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-CNDEF-002';
  FRJanelas.Nome      := 'FmConDefIts';
  FRJanelas.Descricao := 'Conditional Defines - Itens';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-CNDEF-003 :: Conditional Defines - Aplicativos
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-CNDEF-003';
  FRJanelas.Nome      := 'FmConDefApl';
  FRJanelas.Descricao := 'Conditional Defines - Aplicativos';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-MYSQL-001 :: Configura��es MySQL
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-MYSQL-001';
  FRJanelas.Nome      := 'FmMySQLConf';
  FRJanelas.Descricao := 'Configura��es MySQL';
  FLJanelas.Add(FRJanelas);
  //  
  ///////////////////////////////////////////////////WEB
  //
  UnNFSe_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  //
  UnNFSe_TbTerc.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  UnGeral_TbTerc.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  DmkACBr_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
end;

function TMyListas.CriaListaQeiLnk(TabelaCria: String;
  FLQeiLnk: TList<TQeiLnk>): Boolean;
begin
  Result := True;
  try
    begin
      All_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      Enti_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      PerfJan_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      Finance_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      IBGE_DTB_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      NFe_Tb.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      Mail_Tb.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      UnGeral_TbTerc.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      //
  	  Moedas_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      CNAB_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      Anotacoes_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      Empresas_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
    end;
  except
    raise;
    Result := False;
  end;

end;

end.
