object FmClientesAplic: TFmClientesAplic
  Left = 339
  Top = 185
  Caption = 'CAD-CLIEN-002 :: Aplicativos de Clientes'
  ClientHeight = 390
  ClientWidth = 633
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 120
  TextHeight = 16
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 633
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 574
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 515
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 322
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Aplicativos de Clientes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 322
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Aplicativos de Clientes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 322
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Aplicativos de Clientes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 59
    Width = 633
    Height = 190
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 633
      Height = 190
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 633
        Height = 190
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
        object Label3: TLabel
          Left = 7
          Top = 10
          Width = 16
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'ID:'
        end
        object Label9: TLabel
          Left = 103
          Top = 10
          Width = 62
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Aplicativo:'
        end
        object SBAplicativo: TSpeedButton
          Left = 587
          Top = 30
          Width = 26
          Height = 25
          Hint = 'Inclui item de carteira'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '...'
          OnClick = SBAplicativoClick
        end
        object LaQtdLicen: TLabel
          Left = 7
          Top = 110
          Width = 82
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Qtde. de PCs:'
        end
        object Label1: TLabel
          Left = 398
          Top = 110
          Width = 139
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Vencimento da licen'#231'a:'
        end
        object LaQtdCliInt: TLabel
          Left = 266
          Top = 110
          Width = 75
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Qtde. Cli. Int.:'
        end
        object LaQtdUsuarios: TLabel
          Left = 136
          Top = 110
          Width = 108
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Qtde. de usu'#225'rios:'
        end
        object EdControle: TdmkEdit
          Left = 7
          Top = 30
          Width = 87
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          Color = clInactiveCaption
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBackground
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utInc
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdAplicativo: TdmkEditCB
          Left = 103
          Top = 30
          Width = 69
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBAplicativo
          IgnoraDBLookupComboBox = False
        end
        object CBAplicativo: TdmkDBLookupComboBox
          Left = 172
          Top = 30
          Width = 409
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsAplicativos
          TabOrder = 2
          dmkEditCB = EdAplicativo
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object RGStatAplic: TdmkRadioGroup
          Left = 7
          Top = 60
          Width = 597
          Height = 46
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Status do aplicativo'
          Columns = 5
          ItemIndex = 0
          Items.Strings = (
            'Bloqueado'
            'Monitorado'
            'Vencido'
            'Alugado'
            'Liberado')
          TabOrder = 3
          UpdType = utYes
          OldValor = 0
        end
        object EdQtdLicen: TdmkEdit
          Left = 7
          Top = 129
          Width = 123
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object TPFimData: TdmkEditDateTimePicker
          Left = 398
          Top = 129
          Width = 138
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Date = 40409.800566064810000000
          Time = 40409.800566064810000000
          TabOrder = 7
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
        object CkAtivo: TdmkCheckBox
          Left = 7
          Top = 161
          Width = 62
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Ativo'
          Checked = True
          State = cbChecked
          TabOrder = 8
          UpdType = utYes
          ValCheck = '1'
          ValUncheck = '0'
          OldValor = #0
        end
        object EdQtdCliInt: TdmkEdit
          Left = 266
          Top = 129
          Width = 123
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 6
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnEnter = EdQtdCliIntEnter
          OnExit = EdQtdCliIntExit
        end
        object CkBeta: TdmkCheckBox
          Left = 79
          Top = 161
          Width = 427
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Utiliizar vers'#245'es Beta em todos os computadores'
          Checked = True
          State = cbChecked
          TabOrder = 9
          UpdType = utYes
          ValCheck = '1'
          ValUncheck = '0'
          OldValor = #0
        end
        object EdQtdUsuarios: TdmkEdit
          Left = 136
          Top = 129
          Width = 123
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 249
    Width = 633
    Height = 55
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 629
      Height = 35
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 586
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 
          'Para renova'#231#245'es autom'#225'ticas deixar o campo Vencimento da licen'#231'a' +
          ' em branco'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 586
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 
          'Para renova'#231#245'es autom'#225'ticas deixar o campo Vencimento da licen'#231'a' +
          ' em branco'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 304
    Width = 633
    Height = 86
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 454
      Top = 18
      Width = 177
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 5
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 18
      Width = 452
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 14
        Top = 5
        Width = 147
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 272
    Top = 320
  end
  object DsAplicativos: TDataSource
    DataSet = QrAplicativos
    Left = 328
    Top = 320
  end
  object QrAplicativos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Categoria'
      'FROM aplicativos'
      'WHERE Ativo=1'
      'AND Categoria IN (0, 1, 2, 3)'
      'AND Codigo  NOT IN '
      '('
      'SELECT Aplicativo'
      'FROM cliaplic'
      'WHERE Codigo=:P0'
      ')'
      'ORDER BY Nome'
      '')
    Left = 300
    Top = 320
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAplicativosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAplicativosNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
end
