object FmModulos: TFmModulos
  Left = 339
  Top = 185
  Caption = 'CAD-MODUL-001 :: Cadastro de Modulos'
  ClientHeight = 692
  ClientWidth = 965
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 965
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 906
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 5
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 847
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 304
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastro de Modulos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 304
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastro de Modulos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 304
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastro de Modulos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 59
    Width = 965
    Height = 492
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 965
      Height = 492
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 965
        Height = 492
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
        object PnEdita: TPanel
          Left = 2
          Top = 18
          Width = 961
          Height = 288
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          TabOrder = 0
          object Label5: TLabel
            Left = 86
            Top = 4
            Width = 48
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'M'#243'dulo:'
          end
          object Label1: TLabel
            Left = 185
            Top = 4
            Width = 65
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Descri'#231#227'o:'
          end
          object Label7: TLabel
            Left = 11
            Top = 4
            Width = 16
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'ID:'
          end
          object Label2: TLabel
            Left = 11
            Top = 59
            Width = 65
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Descri'#231#227'o:'
          end
          object EdModulo: TdmkEdit
            Left = 86
            Top = 25
            Width = 92
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            CharCase = ecUpperCase
            MaxLength = 4
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'Sigla'
            UpdCampo = 'Sigla'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdNome: TdmkEdit
            Left = 185
            Top = 25
            Width = 760
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            MaxLength = 50
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'Sigla'
            UpdCampo = 'Sigla'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object Panel9: TPanel
            Left = 1
            Top = 228
            Width = 959
            Height = 59
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alBottom
            ParentBackground = False
            TabOrder = 5
            object BitBtn4: TBitBtn
              Tag = 14
              Left = 14
              Top = 4
              Width = 147
              Height = 49
              Cursor = crHandPoint
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '&Confirma'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -15
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              NumGlyphs = 2
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BitBtn4Click
            end
            object Panel10: TPanel
              Left = 784
              Top = 1
              Width = 174
              Height = 57
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 1
              object BitBtn5: TBitBtn
                Tag = 15
                Left = 15
                Top = 2
                Width = 147
                Height = 50
                Cursor = crHandPoint
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = '&Desiste'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -15
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                NumGlyphs = 2
                ParentFont = False
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BitBtn5Click
              end
            end
          end
          object CkAtivo: TdmkCheckBox
            Left = 11
            Top = 197
            Width = 59
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Ativo'
            TabOrder = 4
            QryCampo = 'Ativo'
            UpdCampo = 'Ativo'
            UpdType = utYes
            ValCheck = '1'
            ValUncheck = '0'
            OldValor = #0
          end
          object EdCodigo: TdmkEdit
            Left = 11
            Top = 25
            Width = 69
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Codigo'
            UpdCampo = 'Codigo'
            UpdType = utInc
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object MeObserv: TMemo
            Left = 11
            Top = 80
            Width = 527
            Height = 110
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Lines.Strings = (
              'MeObserv')
            TabOrder = 3
          end
          object CGLicencaTip: TdmkCheckGroup
            Left = 545
            Top = 59
            Width = 400
            Height = 131
            Caption = 'Tipos de licen'#231'a'
            TabOrder = 6
            QryCampo = 'LicencaTip'
            UpdCampo = 'LicencaTip'
            UpdType = utYes
            Value = 0
            OldValor = 0
          end
        end
        object PnDados: TPanel
          Left = 2
          Top = 306
          Width = 961
          Height = 184
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object Panel6: TPanel
            Left = 690
            Top = 0
            Width = 271
            Height = 184
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alRight
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object StaticText4: TStaticText
              Left = 0
              Top = 120
              Width = 271
              Height = 20
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              Alignment = taCenter
              BorderStyle = sbsSunken
              Caption = 'Observa'#231#245'es'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -15
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 0
            end
            object DBMemo1: TDBMemo
              Left = 0
              Top = 140
              Width = 271
              Height = 44
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              DataField = 'Observ'
              DataSource = DsModulos
              TabOrder = 1
            end
            object DBCGLicencaTip: TdmkDBCheckGroup
              Left = 0
              Top = 0
              Width = 271
              Height = 120
              Align = alTop
              Caption = 'Tipos de licen'#231'a'
              DataField = 'LicencaTip'
              DataSource = DsModulos
              ParentBackground = False
              TabOrder = 2
            end
          end
          object dmkDBGrid2: TdmkDBGrid
            Left = 0
            Top = 0
            Width = 690
            Height = 184
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'ID'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Modulo'
                Title.Caption = 'M'#243'dulo'
                Width = 99
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Title.Caption = 'Descri'#231#227'o'
                Width = 250
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Ativo'
                Visible = True
              end>
            Color = clWindow
            DataSource = DsModulos
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
            TabOrder = 1
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -14
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'ID'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Modulo'
                Title.Caption = 'M'#243'dulo'
                Width = 99
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Title.Caption = 'Descri'#231#227'o'
                Width = 250
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Ativo'
                Visible = True
              end>
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 551
    Width = 965
    Height = 55
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 961
      Height = 35
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 606
    Width = 965
    Height = 86
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 786
      Top = 18
      Width = 177
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 18
      Width = 784
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtInclui: TBitBtn
        Tag = 10
        Left = 15
        Top = 5
        Width = 147
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Inclui'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtIncluiClick
      end
      object BtAltera: TBitBtn
        Tag = 11
        Left = 172
        Top = 5
        Width = 148
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Altera'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtAlteraClick
      end
      object BtExclui: TBitBtn
        Tag = 12
        Left = 330
        Top = 5
        Width = 148
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Exclui'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtExcluiClick
      end
    end
  end
  object QrModulos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM modulos')
    Left = 172
    Top = 260
    object QrModulosModulo: TWideStringField
      FieldName = 'Modulo'
      Size = 4
    end
    object QrModulosNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrModulosAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
    object QrModulosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrModulosObserv: TWideMemoField
      FieldName = 'Observ'
      BlobType = ftWideMemo
    end
    object QrModulosLicencaTip: TIntegerField
      FieldName = 'LicencaTip'
    end
  end
  object DsModulos: TDataSource
    DataSet = QrModulos
    Left = 200
    Top = 260
  end
  object frxCAD_MODUL_001_1: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39664.693294085700000000
    ReportOptions.LastChange = 41447.815443148150000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxCAD_MODUL_001_1GetValue
    Left = 60
    Top = 288
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsModAplic
        DataSetName = 'frxDsModAplic'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Columns = 2
      ColumnWidth = 92.500000000000000000
      ColumnPositions.Strings = (
        '0'
        '92,5')
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 66.141766460000000000
        Top = 18.897650000000000000
        Width = 699.213050000000000000
        object Shape2: TfrxShapeView
          Width = 699.213050000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo38: TfrxMemoView
          Width = 684.094930000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line9: TfrxLineView
          Top = 18.897650000000000000
          Width = 699.213050000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo39: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 593.386210000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 487.559370000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_TITULOREL]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Top = 52.913420000000000000
          Width = 37.795275590551180000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ID')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 94.488250000000000000
          Top = 52.913420000000000000
          Width = 211.653570160000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 37.795300000000000000
          Top = 52.913420000000000000
          Width = 56.692913385826770000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'M'#243'dulo')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 306.141930000000000000
          Top = 52.913420000000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Ativo')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 294.803340000000000000
        Width = 699.213050000000000000
        OnAfterPrint = 'PageFooter1OnAfterPrint'
        object Memo13: TfrxMemoView
          Width = 427.086890000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 427.086890000000000000
          Width = 272.126160000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 196.535560000000000000
        Width = 349.606525000000000000
        Columns = 2
        ColumnWidth = 170.078740157480300000
        DataSet = frxDsModAplic
        DataSetName = 'frxDsModAplic'
        RowCount = 0
        object Memo30: TfrxMemoView
          Width = 170.078703540000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsModAplic."Aplicativo"] [frxDsModAplic."NOMEAPLIC"]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Height = 26.346466460000000000
        Top = 147.401670000000000000
        Width = 349.606525000000000000
        Condition = 'frxDsModAplic."Codigo"'
        object CheckBox2: TfrxCheckBoxView
          Left = 306.141930000000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          CheckColor = clBlack
          CheckStyle = csCheck
          DataField = 'Ativo'
          DataSet = frxDsModAplic
          DataSetName = 'frxDsModAplic'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo26: TfrxMemoView
          Width = 37.795251180000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsModAplic."Codigo"]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 94.488250000000000000
          Width = 211.653631180000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsModAplic."Nome"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 37.795300000000000000
          Width = 56.692901180000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsModAplic."Modulo"]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Top = 13.118120000000000000
          Width = 170.078801180000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Aplicativos')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Top = 234.330860000000000000
        Width = 349.606525000000000000
      end
    end
  end
  object QrModAplic: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT mol.Codigo, mol.Modulo, mol.Nome, mol.Ativo,'
      'apl.Codigo Aplicativo, apl.Nome NomeAplic'
      'FROM modulos mol'
      'LEFT JOIN aplicmodul amo ON amo.Modulo = mol.Modulo'
      'LEFT JOIN aplicativos apl ON apl.Codigo = amo.Codigo'
      'ORDER BY mol.Modulo, NomeAplic')
    Left = 300
    Top = 276
    object StringField1: TWideStringField
      FieldName = 'Modulo'
      Size = 4
    end
    object StringField2: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object SmallintField1: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object QrModAplicAplicativo: TIntegerField
      FieldName = 'Aplicativo'
    end
    object QrModAplicNomeAplic: TWideStringField
      FieldName = 'NomeAplic'
      Size = 50
    end
  end
  object frxDsModAplic: TfrxDBDataset
    UserName = 'frxDsModAplic'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Modulo=Modulo'
      'Nome=Nome'
      'Ativo=Ativo'
      'Codigo=Codigo'
      'Aplicativo=Aplicativo'
      'NomeAplic=NOMEAPLIC')
    DataSet = QrModAplic
    BCDToCurrency = False
    Left = 328
    Top = 276
  end
  object PMImprime: TPopupMenu
    Left = 16
    Top = 16
    object Listadaaplicativospormdulo1: TMenuItem
      Caption = '&Lista da aplicativos por m'#243'dulo'
      OnClick = Listadaaplicativospormdulo1Click
    end
    object Listadeclientespormdulo1: TMenuItem
      Caption = 'Lista de &clientes por m'#243'dulo'
      OnClick = Listadeclientespormdulo1Click
    end
  end
  object frxCAD_MODUL_001_2: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39664.693294085700000000
    ReportOptions.LastChange = 41447.815443148150000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxCAD_MODUL_001_2GetValue
    Left = 60
    Top = 316
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsModCli
        DataSetName = 'frxDsModCli'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Columns = 2
      ColumnWidth = 92.500000000000000000
      ColumnPositions.Strings = (
        '0'
        '92,5')
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 66.141766460000000000
        Top = 18.897650000000000000
        Width = 699.213050000000000000
        object Shape2: TfrxShapeView
          Width = 699.213050000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo38: TfrxMemoView
          Width = 684.094930000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line9: TfrxLineView
          Top = 18.897650000000000000
          Width = 699.213050000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo39: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 593.386210000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 487.559370000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_TITULOREL]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Top = 52.913420000000000000
          Width = 37.795275590551180000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ID')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 94.488250000000000000
          Top = 52.913420000000000000
          Width = 211.653570160000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 37.795300000000000000
          Top = 52.913420000000000000
          Width = 56.692913385826770000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'M'#243'dulo')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 306.141930000000000000
          Top = 52.913420000000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Ativo')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 294.803340000000000000
        Width = 699.213050000000000000
        OnAfterPrint = 'PageFooter1OnAfterPrint'
        object Memo13: TfrxMemoView
          Width = 427.086890000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 427.086890000000000000
          Width = 272.126160000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 196.535560000000000000
        Width = 349.606525000000000000
        DataSet = frxDsModCli
        DataSetName = 'frxDsModCli'
        RowCount = 0
        object Memo30: TfrxMemoView
          Width = 347.716613540000000000
          Height = 13.228346460000000000
          DataField = 'NomeCliente'
          DataSet = frxDsModCli
          DataSetName = 'frxDsModCli'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsModCli."NomeCliente"]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Height = 26.346466460000000000
        Top = 147.401670000000000000
        Width = 349.606525000000000000
        Condition = 'frxDsModCli."Codigo"'
        object CheckBox2: TfrxCheckBoxView
          Left = 306.141930000000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          CheckColor = clBlack
          CheckStyle = csCheck
          DataField = 'Ativo'
          DataSet = frxDsModCli
          DataSetName = 'frxDsModCli'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo26: TfrxMemoView
          Width = 37.795251180000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsModCli."Codigo"]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 94.488250000000000000
          Width = 211.653631180000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsModCli."Nome"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 37.795300000000000000
          Width = 56.692901180000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsModCli."Modulo"]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Top = 13.118120000000000000
          Width = 347.716711180000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Clientes ativos')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Top = 234.330860000000000000
        Width = 349.606525000000000000
      end
    end
  end
  object QrModCli: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT mol.Codigo, mol.Modulo, mol.Ativo,'
      'mol.Nome, cli.Codigo Cliente, '
      'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) NomeCliente'
      'FROM modulos mol'
      'LEFT JOIN aplicmodul amu ON amu.Modulo=mol.Modulo'
      'LEFT JOIN cliaplicmo cam ON cam.Modulo = amu.Controle'
      'LEFT JOIN clientes cli ON cli.Codigo = cam.Codigo'
      'LEFT JOIN entidades ent ON ent.Codigo = cli.Cliente'
      'WHERE cli.Ativo = 1'#11
      'GROUP BY mol.Modulo, NomeCliente'
      'ORDER BY mol.Modulo, NomeCliente')
    Left = 300
    Top = 304
    object StringField3: TWideStringField
      FieldName = 'Modulo'
      Size = 4
    end
    object StringField4: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object SmallintField2: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
    end
    object QrModCliCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrModCliNomeCliente: TWideStringField
      FieldName = 'NomeCliente'
      Size = 100
    end
  end
  object frxDsModCli: TfrxDBDataset
    UserName = 'frxDsModCli'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Modulo=Modulo'
      'Nome=Nome'
      'Ativo=Ativo'
      'Codigo=Codigo'
      'Cliente=Cliente'
      'NomeCliente=NomeCliente')
    DataSet = QrModCli
    BCDToCurrency = False
    Left = 328
    Top = 304
  end
end
