program DermaArq;

uses
  Vcl.Forms,
  MyVCLSkin in '..\..\..\..\..\UTL\_UNT\v01_01\MyVCLSkin.pas',
  UnInternalConsts2 in '..\..\..\..\..\UTL\_UNT\v01_01\UnInternalConsts2.pas',
  dmkDAC_PF in '..\..\..\..\..\UTL\MySQL\v01_01\dmkDAC_PF.pas',
  UnMsgInt in '..\..\..\..\..\UTL\_UNT\v01_01\UnMsgInt.pas',
  UnDmkEnums in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnDmkEnums.pas',
  UnInternalConsts in '..\..\..\..\..\UTL\_UNT\v01_01\UnInternalConsts.pas',
  UnDmkProcFunc in '..\..\..\..\..\UTL\_UNT\v01_01\UnDmkProcFunc.pas',
  ZCF2 in '..\..\..\..\..\UTL\_UNT\v01_01\ZCF2.pas',
  ResIntStrings in '..\..\..\..\..\UTL\_UNT\v01_01\ResIntStrings.pas',
  UnMyObjects in '..\..\..\..\..\UTL\_UNT\v01_01\UnMyObjects.pas',
  Module in '..\Units\Module.pas' {DMod: TDataModule},
  UnMyLinguas in '..\..\..\..\..\..\MultiOS\AllOS\Listas\UnMyLinguas.pas',
  UnInternalConsts3 in '..\..\..\..\..\UTL\_UNT\v01_01\UnInternalConsts3.pas',
  UnLock_MLA in '..\..\..\UnLock\v01_01\UnLock_MLA.pas' {FmUnLock_MLA},
  MyListas in '..\Units\MyListas.pas',
  UMySQLDB in '..\..\..\..\..\UTL\MySQL\v01_01\UMySQLDB.pas',
  MyGlyfs in '..\..\..\..\..\UTL\_FRM\v01_01\MyGlyfs.pas' {FmMyGlyfs},
  UnDmkWeb in '..\..\..\..\..\MDL\WEB\v01_01\UnDmkWeb.pas',
  dmkSOAP in '..\..\..\..\..\MDL\WEB\v01_01\WebService\dmkSOAP.pas',
  NovaVersao in '..\..\..\..\..\UTL\_FRM\v01_01\NovaVersao.pas' {FmNovaVersao},
  UnGrl_Consts in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Consts.pas',
  UnitMD5 in '..\..\..\..\..\UTL\Encrypt\v01_01\UnitMD5.pas',
  ABOUT in '..\..\..\..\..\UTL\_FRM\v01_01\ABOUT.pas' {FmAbout},
  UnVCL_ZDB in '..\..\..\..\..\UTL\ZeosDB\UnVCL_ZDB.pas',
  UnGrl_DmkDB in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_DmkDB.pas',
  UnGrl_Vars in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Vars.pas',
  VerifiDB_SQLite in '..\..\..\..\..\UTL\ZeosDB\VerifiDB_SQLite.pas' {FmVerifiDB_SQLite},
  BackupDir in '..\Units\BackupDir.pas' {FmBackupDir},
  Principal in '..\Units\Principal.pas' {FmPrincipal},
  UnitAgendaAux in '..\..\..\..\..\MDL\AGEN\v02_01\UnitAgendaAux.pas',
  ZForge in '..\..\..\..\..\UTL\_FRM\v01_01\ZForge.pas' {FmZForge},
  UnGrl_Geral in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Geral.pas',
  UnDmkListas in '..\..\..\..\..\..\MultiOS\AllOS\Listas\UnDmkListas.pas',
  UnGrlUsuarios in '..\..\..\..\..\..\MultiOS\MDL\USR\v02_01\UnGrlUsuarios.pas',
  UnGrl_REST in '..\..\..\..\..\..\MultiOS\UTL\WEB\HTML\v01_01\UnGrl_REST.pas',
  UnDmkHTML2 in '..\..\..\..\..\UTL\WEB\HTML\v01_01\UnDmkHTML2.pas',
  WBFuncs in '..\..\..\..\..\UTL\WEB\HTML\v01_01\WBFuncs.pas',
  OAuth in '..\..\..\..\..\UTL\WEB\HTML\v01_01\OAuth.pas' {FmOAuth},
  UnGrlEnt in '..\..\..\..\..\..\MultiOS\MDL\ENT\v02_01\UnGrlEnt.pas',
  UnALL_Jan in '..\..\..\..\..\UTL\_UNT\v01_01\UnALL_Jan.pas',
  GetData in '..\..\..\..\..\UTL\_FRM\v01_01\GetData.pas' {FmGetData};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFmPrincipal, FmPrincipal);
  Application.CreateForm(TFmMyGlyfs, FmMyGlyfs);
  Application.Run;
end.
