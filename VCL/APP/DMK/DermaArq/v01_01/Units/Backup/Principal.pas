unit Principal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.Imaging.pngimage, AdvToolBar, AdvShapeButton, AdvGlowButton, Vcl.Grids,
  Vcl.DBGrids, WinSkinStore, WinSkinData, Vcl.ExtCtrls, Vcl.Menus, Registry,
  Data.DB, Bde.DBTables, Vcl.StdCtrls, Vcl.ComCtrls, UnDmkEnums,
  ZAbstractRODataset, ZAbstractDataset, ZDataset;

type
  TFmPrincipal = class(TForm)
    AdvToolBarPager1: TAdvToolBarPager;
    AdvPage2: TAdvPage;
    AdvToolBar2: TAdvToolBar;
    AGBAltera: TAdvGlowButton;
    AGBInclui: TAdvGlowButton;
    AGBExclui: TAdvGlowButton;
    AdvToolBarPager13: TAdvPage;
    AdvToolBar3: TAdvToolBar;
    AdvGlowButton6: TAdvGlowButton;
    AdvPage1: TAdvPage;
    AdvToolBar4: TAdvToolBar;
    AdvGlowButton28: TAdvGlowButton;
    AdvToolBar5: TAdvToolBar;
    AdvGlowButton19: TAdvGlowButton;
    AdvShapeButton1: TAdvShapeButton;
    AdvQuickAccessToolBar1: TAdvQuickAccessToolBar;
    ATBBAtualiza: TAdvToolBarButton;
    AdvToolBar1: TAdvToolBar;
    AGBOcultar: TAdvGlowButton;
    AGBExecutar: TAdvGlowButton;
    BalloonHint1: TBalloonHint;
    TmVersao: TTimer;
    TrayIcon1: TTrayIcon;
    PMMenu: TPopupMenu;
    Mostrar1: TMenuItem;
    Fechar1: TMenuItem;
    N1: TMenuItem;
    Inicializao1: TMenuItem;
    Executarnainicializao1: TMenuItem;
    NOexecutarnainicializao1: TMenuItem;
    TmHide: TTimer;
    AGMBVerifiBD: TAdvGlowButton;
    sd1: TSkinData;
    SkinStore1: TSkinStore;
    DsBackupDir: TDataSource;
    QrBackupDir: TZQuery;
    QrBackupDirCodigo: TLargeintField;
    QrBackupDirOrigem: TWideStringField;
    QrBackupDirDestino: TWideStringField;
    QrBackupDirSubDir: TSmallintField;
    QrBackupDirZipar: TSmallintField;
    QrBackupDirEncrypt: TSmallintField;
    TmConfiguraDB: TTimer;
    QrBackupDirDias: TLargeintField;
    DBGrid2: TDBGrid;
    procedure FormShow(Sender: TObject);
    procedure ATBBAtualizaClick(Sender: TObject);
    procedure AdvGlowButton6Click(Sender: TObject);
    procedure TmVersaoTimer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure AdvGlowButton28Click(Sender: TObject);
    procedure AdvGlowButton19Click(Sender: TObject);
    procedure Mostrar1Click(Sender: TObject);
    procedure Fechar1Click(Sender: TObject);
    procedure Executarnainicializao1Click(Sender: TObject);
    procedure NOexecutarnainicializao1Click(Sender: TObject);
    procedure TrayIcon1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure AGBOcultarClick(Sender: TObject);
    procedure PMMenuPopup(Sender: TObject);
    procedure TmHideTimer(Sender: TObject);
    procedure AGMBVerifiBDClick(Sender: TObject);
    procedure AGBIncluiClick(Sender: TObject);
    procedure AGBAlteraClick(Sender: TObject);
    procedure TmConfiguraDBTimer(Sender: TObject);
  private
    { Private declarations }
    FCriouDB, FShowed, FHideFirst: Boolean;
    FHides: Integer;
    function  VerificaNovasVersoes(ApenasVerifica: Boolean = False): Boolean;
    procedure ExecutaNaInicializacao(Executa: Boolean; Titulo, Programa: String);
    procedure MostraBackupDir(Codigo: Integer);
    procedure ConfiguraDB();
  public
    { Public declarations }
    procedure ReCaptionComponentesDeForm(Form: TForm);
  end;

var
  FmPrincipal: TFmPrincipal;
const
  CO_Titulo = 'Dermatek Backup - Arquivos';

implementation

uses MyVCLSkin, MyListas, UnDmkWeb, dmkGeral, About, UnMyObjects, Module,
  UnVCL_ZDB, UnInternalConsts, BackupDir, UnGrl_DmkDB;

{$R *.dfm}

procedure TFmPrincipal.AdvGlowButton19Click(Sender: TObject);
var
  Link: String;
begin
  Link := DmkWeb.ObtemLinkAjuda(CO_DMKID_APP, 0);
  //
  DmkWeb.MostraWebBrowser(Link);
end;

procedure TFmPrincipal.AdvGlowButton28Click(Sender: TObject);
begin
  Application.CreateForm(TFmAbout, FmAbout);
  FmAbout.ShowModal;
  FmAbout.Destroy;
end;

procedure TFmPrincipal.AdvGlowButton6Click(Sender: TObject);
begin
  VerificaNovasVersoes;
end;

procedure TFmPrincipal.AGBAlteraClick(Sender: TObject);
begin
  MostraBackupDir(0);
end;

procedure TFmPrincipal.AGBIncluiClick(Sender: TObject);
begin
  MostraBackupDir(0);
end;

procedure TFmPrincipal.AGBOcultarClick(Sender: TObject);
begin
  TrayIcon1.Visible             := True;
  Application.MainFormOnTaskBar := FShowed;
  //
  FmPrincipal.Hide;
  //
  Application.MainFormOnTaskBar := FShowed;
end;

procedure TFmPrincipal.AGMBVerifiBDClick(Sender: TObject);
begin
  VCL_ZDB.MostraVerifyDB(False, Dmod.MyDB);
end;

procedure TFmPrincipal.ATBBAtualizaClick(Sender: TObject);
begin
  VerificaNovasVersoes;
end;

procedure TFmPrincipal.ConfiguraDB;
begin
  FCriouDB := True;
  //
  Application.CreateForm(TDmod, Dmod);
  //
  Grl_DmkDB.AbreQuery(QrBackupDir, Dmod.MyDB);
  //
  TrayIcon1.Visible := True;
  //
  if FCriouDB then
  begin
    if (FHides = 0) and (Dmod.QrControle.Active = True) then
    begin
      FHides         := FHides + 1;
      TmHide.Enabled := True;
    end;
  end;
end;

procedure TFmPrincipal.ExecutaNaInicializacao(Executa: Boolean; Titulo,
  Programa: String);
var
  Registry: TRegistry;
begin
  Registry := TRegistry.Create;
  try
    Registry.RootKey := HKEY_CURRENT_USER;
    Registry.OpenKey('\Software\Microsoft\Windows\CurrentVersion\Run', False);
    if Executa then
      Registry.WriteString(Titulo, Programa)
    else
      Registry.WriteString(Titulo, '');
  finally
    Registry.Free;
  end;
end;

procedure TFmPrincipal.Executarnainicializao1Click(Sender: TObject);
begin
  ExecutaNaInicializacao(True, CO_Titulo, Application.ExeName);
end;

procedure TFmPrincipal.Fechar1Click(Sender: TObject);
begin
  Close;
end;

procedure TFmPrincipal.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  //
  if not FCriouDB then
    TmConfiguraDB.Enabled := True;
end;

procedure TFmPrincipal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if TrayIcon1 <> nil then
    TrayIcon1.Visible := False;
end;

procedure TFmPrincipal.FormCreate(Sender: TObject);
begin
  FCriouDB   := False;
  FHides     := 0;
  FShowed    := True;
  FHideFirst := False;
  //
  VAR_USA_TAG_BITBTN := True;
  //
  Geral.DefineFormatacoes;
  //
  AdvToolBarPager1.ActivePageIndex := 0;
  //
  TmVersao.Enabled := True;
end;

procedure TFmPrincipal.FormShow(Sender: TObject);
begin
  MeuVCLSkin.VCLSkinDefineUso(sd1, skinstore1);
end;

procedure TFmPrincipal.MostraBackupDir(Codigo: Integer);
begin
  Application.CreateForm(TFmBackupDir, FmBackupDir);
  FmBackupDir.FCodigo := Codigo;
  FmBackupDir.ShowModal;
  FmBackupDir.Destroy;
end;

procedure TFmPrincipal.Mostrar1Click(Sender: TObject);
begin
  FmPrincipal.Visible := True;
end;

procedure TFmPrincipal.NOexecutarnainicializao1Click(Sender: TObject);
begin
  ExecutaNaInicializacao(False, CO_Titulo, Application.ExeName);
end;

procedure TFmPrincipal.PMMenuPopup(Sender: TObject);
var
  Valor: String;
begin
  Valor := Geral.ReadKey(CO_Titulo, 'Software\Microsoft\Windows\CurrentVersion\Run', ktString, '', HKEY_CURRENT_USER);
  //
  if Valor <> '' then
  begin
    Executarnainicializao1.Checked   := True;
    NOexecutarnainicializao1.Checked := False;
  end else
  begin
    Executarnainicializao1.Checked   := False;
    NOexecutarnainicializao1.Checked := True;
  end;
end;

procedure TFmPrincipal.ReCaptionComponentesDeForm(Form: TForm);
begin
  //N�o Usa
end;

procedure TFmPrincipal.TmConfiguraDBTimer(Sender: TObject);
begin
  TmConfiguraDB.Enabled := False;
  //
  ConfiguraDB;
end;

procedure TFmPrincipal.TmHideTimer(Sender: TObject);
begin
  TmHide.Enabled := False;
  //
  if Visible then
  begin
    if not FHideFirst then
    begin
      FHideFirst := True;
      Application.MainFormOnTaskBar := FShowed;
      Hide;
      //FmPrincipal.WindowState :=  wsMinimized;
      Application.MainFormOnTaskBar := FShowed;
      TrayIcon1.Visible := True;
    end;
  end;
end;

procedure TFmPrincipal.TmVersaoTimer(Sender: TObject);
begin
  TmVersao.Enabled := False;
  //
  if DmkWeb.RemoteConnection then
  begin
    if VerificaNovasVersoes(True) then
      DmkWeb.MostraBalloonHintMenuTopo(ATBBAtualiza, BalloonHint1,
        'H� uma nova vers�o!', 'Clique aqui para atualizar.');
  end;
end;

procedure TFmPrincipal.TrayIcon1Click(Sender: TObject);
begin
  FmPrincipal.Visible := True;
end;

function TFmPrincipal.VerificaNovasVersoes(ApenasVerifica: Boolean): Boolean;
var
  Versao: Integer;
  ArqNome: String;
begin
  Result := DmkWeb.VerificaAtualizacaoVersao2(True, True, 'DermaArq', 'DermaArq',
              '', CO_VERSAO, CO_DMKID_APP, Now, nil, dtExecAux, Versao,
              ArqNome, False, ApenasVerifica, BalloonHint1);
end;

end.
