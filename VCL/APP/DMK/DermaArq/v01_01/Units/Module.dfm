object DMod: TDMod
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 300
  Width = 364
  object MyDB: TZConnection
    ControlsCodePage = cGET_ACP
    Properties.Strings = (
      'controls_cp=GET_ACP')
    Port = 0
    Protocol = 'sqlite'
    LibraryLocation = 'sqlite3.dll'
    Left = 80
    Top = 56
  end
  object QrLoc: TZQuery
    Connection = MyDB
    Params = <>
    Left = 128
    Top = 160
  end
  object QrControle: TZQuery
    Connection = MyDB
    Params = <>
    Left = 88
    Top = 160
  end
  object QrAux: TZQuery
    Connection = MyDB
    Params = <>
    Left = 168
    Top = 160
  end
  object QrUpd: TZQuery
    Connection = MyDB
    Params = <>
    Left = 208
    Top = 160
  end
end
