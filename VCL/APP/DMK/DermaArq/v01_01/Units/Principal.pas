unit Principal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  WinSkinData, WinSkinStore, Vcl.Imaging.pngimage, AdvToolBar, AdvShapeButton,
  AdvGlowButton, Data.DB, ZAbstractRODataset, ZAbstractDataset, ZDataset,
  Vcl.ExtCtrls, Vcl.Menus, Registry, Vcl.Grids, Vcl.DBGrids, Vcl.ComCtrls,
  Vcl.StdCtrls, System.DateUtils, PlanRecurr;

type
  TDtaTip = (dttpUltTry=0, dttpUltExe=1, dttpUltErr=2);
  TFmPrincipal = class(TForm)
    SkinStore1: TSkinStore;
    sd1: TSkinData;
    AdvToolBarPager1: TAdvToolBarPager;
    AdvPage2: TAdvPage;
    AdvToolBar2: TAdvToolBar;
    AGBAltera: TAdvGlowButton;
    AGBInclui: TAdvGlowButton;
    AGBExclui: TAdvGlowButton;
    AdvToolBar1: TAdvToolBar;
    AGBOcultar: TAdvGlowButton;
    AGBExecutar: TAdvGlowButton;
    AdvToolBarPager13: TAdvPage;
    AdvToolBar3: TAdvToolBar;
    AdvGlowButton6: TAdvGlowButton;
    AGMBVerifiBD: TAdvGlowButton;
    AdvPage1: TAdvPage;
    AdvToolBar4: TAdvToolBar;
    AdvGlowButton28: TAdvGlowButton;
    AdvToolBar5: TAdvToolBar;
    AdvGlowButton19: TAdvGlowButton;
    AdvShapeButton1: TAdvShapeButton;
    AdvQuickAccessToolBar1: TAdvQuickAccessToolBar;
    ATBBAtualiza: TAdvToolBarButton;
    QrBackupDir: TZQuery;
    QrBackupDirCodigo: TLargeintField;
    QrBackupDirOrigem: TWideStringField;
    QrBackupDirDestino: TWideStringField;
    DsBackupDir: TDataSource;
    BalloonHint1: TBalloonHint;
    TmConfiguraDB: TTimer;
    TmHide: TTimer;
    TmVersao: TTimer;
    TrayIcon1: TTrayIcon;
    PMMenu: TPopupMenu;
    Mostrar1: TMenuItem;
    Fechar1: TMenuItem;
    N1: TMenuItem;
    Inicializao1: TMenuItem;
    Executarnainicializao1: TMenuItem;
    NOexecutarnainicializao1: TMenuItem;
    QrBackupDirSubDir: TSmallintField;
    QrBackupDirZipar: TSmallintField;
    QrBackupDirEncrypt: TSmallintField;
    QrBackupDirSenhaCrypt: TWideStringField;
    QrBackupDirOmitirExt: TSmallintField;
    QrBackupDirExtOmit: TWideMemoField;
    QrBackupDirDias: TLargeintField;
    QrBackupDirRecorrencia: TWideStringField;
    QrExeBackup: TZQuery;
    PCBackupDir: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DBGBackupDir: TDBGrid;
    PB1: TProgressBar;
    GridPanel1: TGridPanel;
    LBLista1: TListBox;
    LBLista2: TListBox;
    AdvToolBar6: TAdvToolBar;
    AGBExtrair: TAdvGlowButton;
    QrBackupDirUltExe: TDateTimeField;
    QrBackupDirAviso: TWideStringField;
    procedure ATBBAtualizaClick(Sender: TObject);
    procedure AGBOcultarClick(Sender: TObject);
    procedure AGBIncluiClick(Sender: TObject);
    procedure AGBAlteraClick(Sender: TObject);
    procedure AdvGlowButton6Click(Sender: TObject);
    procedure AGMBVerifiBDClick(Sender: TObject);
    procedure AdvGlowButton28Click(Sender: TObject);
    procedure AdvGlowButton19Click(Sender: TObject);
    procedure TmConfiguraDBTimer(Sender: TObject);
    procedure TmHideTimer(Sender: TObject);
    procedure TmVersaoTimer(Sender: TObject);
    procedure TrayIcon1Click(Sender: TObject);
    procedure PMMenuPopup(Sender: TObject);
    procedure Mostrar1Click(Sender: TObject);
    procedure Fechar1Click(Sender: TObject);
    procedure Executarnainicializao1Click(Sender: TObject);
    procedure NOexecutarnainicializao1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure AGBExcluiClick(Sender: TObject);
    procedure AGBExecutarClick(Sender: TObject);
    procedure AGBExtrairClick(Sender: TObject);
  private
    { Private declarations }
    FCriouDB, FShowed, FHideFirst: Boolean;
    FHides: Integer;
    function  VerificaNovasVersoes(ApenasVerifica: Boolean = False): Boolean;
    function  AtualizaDataHoraBackup(TipData: TDtaTip; Codigo: Integer; Msg: String): Boolean;
    function  ExecBackup(SubDir, OmitirExt: Boolean; Caminho, NomeDir, Nome,
              ExtOmit: String): Boolean;
    function  ZipaArquivo(var Caminho: String; const Encryptar: Boolean;
                Senha: String): Boolean;
    function  MoveResulBackup(Zipar: Boolean; Caminho, NomeDir, Nome,
                Destino: String): Boolean;
    procedure ExecutaNaInicializacao(Executa: Boolean; Titulo, Programa: String);
    procedure MostraBackupDir(Codigo: Integer);
    procedure ConfiguraDB();
    procedure ReopenBackupDir();
    procedure BackupDir(Codigo: Integer);
    procedure ExtrairArquivo();
    procedure ExecutaBackupAutomatico();
  public
    { Public declarations }
    procedure ReCaptionComponentesDeForm(Form: TForm);
  end;

var
  FmPrincipal: TFmPrincipal;
const
  CO_Titulo = 'Dermatek Backup - Arquivos';

implementation

uses MyVCLSkin, MyListas, UnDmkWeb, dmkGeral, About, UnMyObjects, Module,
  UnVCL_ZDB, UnInternalConsts, BackupDir, UnGrl_DmkDB, UnDmkEnums, ZForge,
  UnDmkProcFunc, UMySQLDB;

{$R *.dfm}

{ TFmPrincipal }

procedure TFmPrincipal.AdvGlowButton19Click(Sender: TObject);
var
  Link: String;
begin
  Link := DmkWeb.ObtemLinkAjuda(CO_DMKID_APP, 0);
  //
  DmkWeb.MostraWebBrowser(Link);
end;

procedure TFmPrincipal.AdvGlowButton28Click(Sender: TObject);
begin
  Application.CreateForm(TFmAbout, FmAbout);
  FmAbout.ShowModal;
  FmAbout.Destroy;
end;

procedure TFmPrincipal.AdvGlowButton6Click(Sender: TObject);
begin
  VerificaNovasVersoes;
end;

procedure TFmPrincipal.AGBAlteraClick(Sender: TObject);
begin
  if (QrBackupDir.State <> dsInactive) and (QrBackupDir.RecordCount > 0) then
    MostraBackupDir(QrBackupDirCodigo.Value);
end;

procedure TFmPrincipal.AGBExcluiClick(Sender: TObject);
var
  Codigo: Integer;
begin
  if (QrBackupDir.State <> dsInactive) and (QrBackupDir.RecordCount > 0) then
  begin
    Codigo := QrBackupDirCodigo.Value;
    //
    if Grl_DmkDB.ExcluiRegistroInt1_SQLite('Deseja excluir este registro?',
      'backupdir', 'Codigo', Codigo, Dmod.MyDB) = mrYes then
    begin
      ReopenBackupDir();
    end;
  end;
end;

procedure TFmPrincipal.AGBExecutarClick(Sender: TObject);
begin
  BackupDir(QrBackupDirCodigo.Value);
end;

procedure TFmPrincipal.AGBExtrairClick(Sender: TObject);
begin
  //ExecutaBackupAutomatico;
  ExtrairArquivo;
end;

procedure TFmPrincipal.AGBIncluiClick(Sender: TObject);
begin
  MostraBackupDir(0);
end;

procedure TFmPrincipal.AGBOcultarClick(Sender: TObject);
begin
  TrayIcon1.Visible             := True;
  Application.MainFormOnTaskBar := FShowed;
  //
  FmPrincipal.Hide;
  //
  Application.MainFormOnTaskBar := FShowed;
end;

procedure TFmPrincipal.AGMBVerifiBDClick(Sender: TObject);
begin
  VCL_ZDB.MostraVerifyDB(False, Dmod.MyDB);
end;

procedure TFmPrincipal.ATBBAtualizaClick(Sender: TObject);
begin
  VerificaNovasVersoes;
end;

function TFmPrincipal.AtualizaDataHoraBackup(TipData: TDtaTip; Codigo: Integer; Msg: String): Boolean;
var
  Campo, Agora: String;
begin
  case TipData of
    dttpUltTry:
      Campo := 'UltTry';
    dttpUltExe:
      Campo := 'UltExe';
    dttpUltErr:
      Campo := 'UltErr';
  end;
  Agora := Geral.FDT(Now, 9);
  //
  if Grl_DmkDB.CarregaSQLInsUpd(Dmod.QrUpd, Dmod.MyDB, stUpd, 'backupdir', False,
    [Campo, 'Aviso'], ['Codigo'], [Agora, Msg], [Codigo],
     False, False, '', stDesktop) then
  begin
    Result := True;
  end else
    Result := False;
end;

procedure TFmPrincipal.BackupDir(Codigo: Integer);
var
  Continua, SubDir, OmitirExt, Zipar, Encrypt: Boolean;
  Msg, Origem, Destino, NomeDir, Nome, ExtOmit, FileName, SenhaCrypt: String;
begin
  PCBackupDir.ActivePageIndex := 1;
  //
  Continua := True;
  Msg      := '';
  //
  Grl_DmkDB.AbreSQLQuery0(QrExeBackup, DMod.MyDB, [
    'SELECT * ',
    'FROM backupdir ',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    '']);
  //
  if QrExeBackup.RecordCount > 0 then
  begin
    Origem     := dmkPF.SimplificaBarras(QrExeBackup.FieldByName('Origem').AsString);
    Destino    := dmkPF.SimplificaBarras(QrExeBackup.FieldByName('Destino').AsString);
    NomeDir    := ExtractFileName(Origem);
    Nome       := USQLDB.Backup_CriaNome('', Now);
    SubDir     := Geral.IntToBool(QrExeBackup.FieldByName('SubDir').AsInteger);
    OmitirExt  := Geral.IntToBool(QrExeBackup.FieldByName('OmitirExt').AsInteger);
    ExtOmit    := QrExeBackup.FieldByName('ExtOmit').AsString;
    Zipar      := Geral.IntToBool(QrExeBackup.FieldByName('Zipar').AsInteger);
    Encrypt    := Geral.IntToBool(QrExeBackup.FieldByName('Encrypt').AsInteger);
    SenhaCrypt := QrExeBackup.FieldByName('SenhaCrypt').AsString;
    //
    if MyObjects.FIC(not DirectoryExists(Origem), nil, 'Backup cancelado! Diret�rio origem n�o localizado!') then
      Exit;
    //
    if not DirectoryExists(Destino) then
    begin
      if not ForceDirectories(Destino) then
      begin
        Geral.MensagemBox('Backup cancelado! Diret�rio destino n�o existe e n�o pode ser criado!',
          'Aviso', MB_OK+MB_ICONWARNING);
        Exit;
      end;
    end;
    try
      AGBExecutar.Enabled := False;

      //Salva no banco de dados o in�cio do backup
      Continua := AtualizaDataHoraBackup(dttpUltTry, Codigo, 'Backup iniciado!');

      if not Continua then
        Exit;

      //Executa o backup
      Continua := ExecBackup(SubDir, OmitirExt, Origem, NomeDir, Nome, ExtOmit);

      if not Continua then
      begin
        Msg := 'Falha ao executar o backup dos arquivos!';
        Exit;
      end;

      //Zipa o backup
      if Zipar = True then
      begin
        FileName := Origem + '\' + NomeDir + Nome;
        //
        Continua := ZipaArquivo(FileName, Encrypt, SenhaCrypt);

        if not Continua then
        begin
          Msg := 'Falha ao zipar arquivo de backup!';
          Exit;
        end;
      end;

      //Move o backup para o diret�rio correspondente
      Continua := MoveResulBackup(Zipar, Origem, NomeDir, Nome, Destino);

      if not Continua then
      begin
        //Exclui se n�o conseguir mover para n�o ocupar espa�o em disco
        DeleteFile(FileName);
        //
        Msg := 'Falha mover backup para o diret�rio de destino!';
        Exit;
      end;
    finally
      if Continua then
        AtualizaDataHoraBackup(dttpUltExe, Codigo, 'Backup finalizado!')
      else
        AtualizaDataHoraBackup(dttpUltErr, Codigo, Msg);
      //
      ReopenBackupDir;
      //
      AGBExecutar.Enabled := True;
      //
      PCBackupDir.ActivePageIndex := 0;
    end;
  end;
end;

procedure TFmPrincipal.ReopenBackupDir;

  procedure ConfiguraComponentes(Ativa: Boolean);
  begin
    AGBExecutar.Enabled  := Ativa;
    AGBInclui.Enabled    := Ativa;
    AGBAltera.Enabled    := Ativa;
    AGBExclui.Enabled    := Ativa;
    DBGBackupDir.Enabled := Ativa;
    //
    if Ativa = False then
      QrBackupDir.DisableControls
    else
      QrBackupDir.EnableControls;
  end;
begin
  try
    ConfiguraComponentes(False);
    //
    Grl_DmkDB.AbreQuery(QrBackupDir, Dmod.MyDB);
    //
    if QrBackupDir.RecordCount > 0 then
    begin
      USQLDB.Backups_ExcluiAntigos(QrBackupDirUltExe.Value, QrBackupDirDias.Value,
        QrBackupDirDestino.Value, ExtractFileName(QrBackupDirOrigem.Value));
    end;
  finally
    ConfiguraComponentes(True);
  end;
end;

procedure TFmPrincipal.ConfiguraDB;
begin
  FCriouDB := True;
  //
  Application.CreateForm(TDmod, Dmod);
  //
  ReopenBackupDir();
  //
  TrayIcon1.Visible := True;
  //
  if FCriouDB then
  begin
    if (FHides = 0) and (Dmod.QrControle.Active = True) then
    begin
      FHides         := FHides + 1;
      TmHide.Enabled := True;
    end;
  end;
end;

function TFmPrincipal.ExecBackup(SubDir, OmitirExt: Boolean; Caminho, NomeDir,
  Nome, ExtOmit: String): Boolean;
var
  i, j, p: Integer;
  Eh: Boolean;
  Comp1, Comp2, Cam: String;
  ListaOmiExt: TStringList;
begin
  Result      := False;
  ListaOmiExt := TStringList.Create;
  try
    //Carrega
    LBLista1.Clear;
    dmkPF.GetAllFiles(SubDir, Caminho + '\*.*', lBLista1, True);
    //
    LBLista1.TopIndex := LBLista1.Items.Count - 1;
    //f
    //Executa o backup
    Screen.Cursor := crHourGlass;
    try
      PB1.Visible  := True;
      PB1.Position := 0;
      PB1.Max      := LBLista1.Items.Count;
      //
      for i := 0 to LBLista1.Items.Count -1 do
      begin
        PB1.Position := PB1.Position + 1;
        PB1.Update;
        Application.ProcessMessages;
        //
        Eh := False;
        //
        if OmitirExt then
        begin
          ListaOmiExt := Geral.Explode(ExtOmit, ';', 0);
          //
          for j := 0 to ListaOmiExt.Count - 1 do
          begin
            p := pos('*', ListaOmiExt.Strings[j]);
            //
            if p > 0 then
            begin
              Comp1 := Copy(ExtractFileExt(LBLista1.Items[i]), 1, p - 1);
              Comp2 := Copy(ListaOmiExt.Strings[j], 1, p - 1);
            end else begin
              Comp1 := ExtractFileExt(LBLista1.Items[i]);
              Comp2 := ListaOmiExt.Strings[j];
            end;
            if Uppercase(Comp1) = Uppercase(Comp2) then
            begin
              Eh := True;
              Break;
            end;
          end;
        end;

        if Eh then
          LBLista2.Items.Add(LBLista1.Items[i] + ' n�o foi copiado.')
        else begin
          Cam := LBLista1.Items[i];
          //
          Insert('\' + NomeDir + Nome, Cam, Length(Caminho) + 1);
          ForceDirectories(ExtractFilePath(Cam));
          //
          if CopyFile(PChar(LBLista1.Items[i]), PChar(Cam), False) then
            LBLista2.Items.Add(Cam + ' foi criado.')
          else
            LBLista2.Items.Add(Cam+' n�o pode ser criado.');
        end;
        LBLista2.Update;
        Application.ProcessMessages;
        //
        LBLista2.TopIndex := LBLista2.Items.Count - 1;
      end;
      Result := True;
    finally
      PB1.Position  := 0;
      Screen.Cursor := crDefault;
    end;
    ListaOmiExt.Free;
  except
    ListaOmiExt.Free;
  end;
end;

procedure TFmPrincipal.ExecutaBackupAutomatico;
var
  Dia, Mes, Ano: Word;
  Agora, Ini, Fim: TDateTime;
  Recur: TRecurrencyHandler;
  i: Integer;
begin
  Agora := Now;
  //
  DecodeDate(Agora, Ano, Mes, Dia);
  //
  Ini := EncodeDateTime(Ano, Mes, Dia, 00, 00, 00, 000);
  Fim := EncodeDateTime(Ano, Mes, Dia, 23, 59, 59, 999);
  //
  Recur            := TRecurrencyHandler.Create;
  Recur.Recurrency := QrBackupDirRecorrencia.Value;
  Recur.StartTime  := Ini;
  Recur.EndTime    := Fim;
  Recur.TimeSpan   := Fim;
  Recur.Parse;
  Recur.Generate;
  //
  for I := 0 to Recur.Dates.Count - 1 do
  begin
    Geral.MB_Aviso('Data incial: ' + Geral.FDT(Recur.Dates[I].StartDate, 0) +
      sLineBreak + 'Data fim: ' + Geral.FDT(Recur.Dates[I].EndDate, 0));
  end;
end;

procedure TFmPrincipal.ExecutaNaInicializacao(Executa: Boolean; Titulo,
  Programa: String);
var
  Registry: TRegistry;
begin
  Registry := TRegistry.Create;
  try
    Registry.RootKey := HKEY_CURRENT_USER;
    Registry.OpenKey('\Software\Microsoft\Windows\CurrentVersion\Run', False);
    if Executa then
      Registry.WriteString(Titulo, Programa)
    else
      Registry.WriteString(Titulo, '');
  finally
    Registry.Free;
  end;
end;

procedure TFmPrincipal.Executarnainicializao1Click(Sender: TObject);
begin
  ExecutaNaInicializacao(True, CO_Titulo, Application.ExeName);
end;

procedure TFmPrincipal.Fechar1Click(Sender: TObject);
begin
  Close;
end;

procedure TFmPrincipal.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  //
  if not FCriouDB then
    TmConfiguraDB.Enabled := True;
end;

procedure TFmPrincipal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if TrayIcon1 <> nil then
    TrayIcon1.Visible := False;
end;

procedure TFmPrincipal.FormCreate(Sender: TObject);
begin
  FCriouDB   := False;
  FHides     := 0;
  FShowed    := True;
  FHideFirst := False;
  //
  VAR_USA_TAG_BITBTN := True;
  //
  Geral.DefineFormatacoes;
  //
  AdvToolBarPager1.ActivePageIndex := 0;
  PCBackupDir.ActivePageIndex      := 0;
  //
  TmVersao.Enabled := True;
end;

procedure TFmPrincipal.FormShow(Sender: TObject);
begin
  MeuVCLSkin.VCLSkinDefineUso(sd1, skinstore1);
end;

procedure TFmPrincipal.MostraBackupDir(Codigo: Integer);
begin
  Application.CreateForm(TFmBackupDir, FmBackupDir);
  FmBackupDir.FCodigo := Codigo;
  FmBackupDir.FQuery  := QrBackupDir;
  FmBackupDir.ShowModal;
  FmBackupDir.Destroy;
  //
  ReopenBackupDir();
end;

procedure TFmPrincipal.Mostrar1Click(Sender: TObject);
begin
  FmPrincipal.Visible := True;
end;

function TFmPrincipal.MoveResulBackup(Zipar: Boolean; Caminho, NomeDir,
  Nome, Destino: String): Boolean;
var
  Continua: Boolean;
  CaminhoZip: String;
begin
  Continua := True;
  //
  //Move o arquivo resultante
  if Zipar then
  begin
    if dmkPF.DelDir(Caminho + '\' + NomeDir + Nome) then
    begin
      LBLista1.Items.Add('Aviso: Diret�rio tempor�rio removido com sucesso!');
    end else
    begin
      LBLista1.Items.Add('Erro: Falha ao remover o diret�rio tempor�rio!');
      Continua := False;
    end;
    CaminhoZip := Caminho + '\' + NomeDir + Nome + '.zip';
  end else
    CaminhoZip := Caminho + '\' + NomeDir + Nome;
  //
  if dmkPF.MoveDir(CaminhoZip, Destino + '\') = True then
  begin
    LBLista1.Items.Add('Aviso: Diret�rio movido com sucesso!');
  end else
  begin
    LBLista1.Items.Add('Erro: Falha ao mover diret�rio!');
    Continua := False;
  end;
  LBLista1.TopIndex := LBLista1.Items.Count - 1;
  //
  Result := Continua;
end;

procedure TFmPrincipal.NOexecutarnainicializao1Click(Sender: TObject);
begin
  ExecutaNaInicializacao(False, CO_Titulo, Application.ExeName);
end;

procedure TFmPrincipal.PMMenuPopup(Sender: TObject);
var
  Valor: String;
begin
  Valor := Geral.ReadKey(CO_Titulo, 'Software\Microsoft\Windows\CurrentVersion\Run', ktString, '', HKEY_CURRENT_USER);
  //
  if Valor <> '' then
  begin
    Executarnainicializao1.Checked   := True;
    NOexecutarnainicializao1.Checked := False;
  end else
  begin
    Executarnainicializao1.Checked   := False;
    NOexecutarnainicializao1.Checked := True;
  end;
end;

procedure TFmPrincipal.ReCaptionComponentesDeForm(Form: TForm);
begin
  //N�o Usa
end;

procedure TFmPrincipal.TmConfiguraDBTimer(Sender: TObject);
begin
  TmConfiguraDB.Enabled := False;
  //
  ConfiguraDB;
end;

procedure TFmPrincipal.TmHideTimer(Sender: TObject);
begin
  TmHide.Enabled := False;
  //
  if Visible then
  begin
    if not FHideFirst then
    begin
      FHideFirst := True;
      Application.MainFormOnTaskBar := FShowed;
      Hide;
      //FmPrincipal.WindowState :=  wsMinimized;
      Application.MainFormOnTaskBar := FShowed;
      TrayIcon1.Visible := True;
    end;
  end;
end;

procedure TFmPrincipal.TmVersaoTimer(Sender: TObject);
begin
  TmVersao.Enabled := False;
  //
  if DmkWeb.RemoteConnection then
  begin
    if VerificaNovasVersoes(True) then
      DmkWeb.MostraBalloonHintMenuTopo(ATBBAtualiza, BalloonHint1,
        'H� uma nova vers�o!', 'Clique aqui para atualizar.');
  end;
end;

procedure TFmPrincipal.TrayIcon1Click(Sender: TObject);
begin
  FmPrincipal.Visible := True;
end;

function TFmPrincipal.VerificaNovasVersoes(ApenasVerifica: Boolean): Boolean;
var
  Versao: Integer;
  ArqNome: String;
begin
  Result := DmkWeb.VerificaAtualizacaoVersao2(True, True, 'DermaArq', 'DermaArq',
              '', CO_VERSAO, CO_DMKID_APP, Now, nil, dtExecAux, Versao,
              ArqNome, False, ApenasVerifica, BalloonHint1);
end;

procedure TFmPrincipal.ExtrairArquivo;
var
  Arq: String;
begin
  Application.CreateForm(TFmZForge, FmZForge);
  FmZForge.Show;
  //
  Arq := FmZForge.ExtrairArquivo;
  //
  FmZForge.Destroy;
  //
  if (Arq <> '') and (FileExists(Arq)) then
  begin
    if Geral.MB_Pergunta('Arquivo extra�do com sucesso!' + sLineBreak +
      'Deseja abrir o diret�rio?') = ID_YES
    then
      Geral.AbreArquivo(Arq, True);
  end;
end;

function TFmPrincipal.ZipaArquivo(var Caminho: String; const Encryptar: Boolean;
  Senha: String): Boolean;
var
  CamZip: String;
begin
  CamZip := '';
  //
  Application.CreateForm(TFmZForge, FmZForge);
  FmZForge.Show;
  //
  CamZip := FmZForge.ZipaArquivo(zftDiretorio, ExtractFileDir(Caminho), Caminho,
              Geral.SemAcento(ExtractFileName(Caminho)), Senha, Encryptar, False);
  //
  FmZForge.Destroy;
  //
  if Length(CamZip) > 0 then
  begin
    Caminho := CamZip;
    Result  := True;
  end
  else
    Result := False;
end;

end.
