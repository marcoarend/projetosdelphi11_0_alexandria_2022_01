unit Module;

interface

uses
  System.SysUtils, System.Classes, Vcl.Forms, Data.DB, ZAbstractConnection,
  ZConnection, ZCompatibility, dmkGeral, ZAbstractRODataset, ZAbstractDataset,
  ZDataset;

type
  TDMod = class(TDataModule)
    MyDB: TZConnection;
    QrLoc: TZQuery;
    QrControle: TZQuery;
    QrAux: TZQuery;
    QrUpd: TZQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DMod: TDMod;

implementation

{%CLASSGROUP 'System.Classes.TPersistent'}

uses UnVCL_ZDB, UnGrl_DmkDB, UnInternalConsts, MyListas;

{$R *.dfm}

procedure TDMod.DataModuleCreate(Sender: TObject);
begin
  VAR_BDSENHA := 'wkljweryhvbirt';
  //
  if not VCL_ZDB.ConfiguraBD(MyDB, QrControle, QrLoc) then
    Application.Terminate;
end;

end.
