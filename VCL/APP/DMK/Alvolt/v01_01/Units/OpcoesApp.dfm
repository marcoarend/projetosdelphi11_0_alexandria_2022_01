object FmOpcoesApp: TFmOpcoesApp
  Left = 339
  Top = 185
  Caption = 'APP-OPCAO-001 :: Op'#231#245'es do Aplicativo'
  ClientHeight = 601
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 255
        Height = 32
        Caption = 'Op'#231#245'es do Aplicativo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 255
        Height = 32
        Caption = 'Op'#231#245'es do Aplicativo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 255
        Height = 32
        Caption = 'Op'#231#245'es do Aplicativo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 439
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 439
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 784
        Height = 439
        Align = alClient
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 780
          Height = 86
          Align = alTop
          TabOrder = 0
          object Label32: TLabel
            Left = 14
            Top = 43
            Width = 73
            Height = 13
            Caption = 'IP do Aparelho:'
          end
          object Label1: TLabel
            Left = 342
            Top = 43
            Width = 77
            Height = 13
            Caption = 'URL base (API):'
          end
          object SbURLBaseAPI: TSpeedButton
            Left = 501
            Top = 59
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SbURLBaseAPIClick
          end
          object SbDeviceHost: TSpeedButton
            Left = 317
            Top = 59
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SbDeviceHostClick
          end
          object SbdeviceMACAddress: TSpeedButton
            Left = 753
            Top = 59
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SbdeviceMACAddressClick
          end
          object Label2: TLabel
            Left = 526
            Top = 43
            Width = 217
            Height = 13
            Caption = 'Endere'#231'o MAC da placa de rede do aparelho:'
          end
          object EdDeviceHost: TdmkEdit
            Left = 14
            Top = 59
            Width = 299
            Height = 21
            MaxLength = 255
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '192.168.25.255'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = '192.168.25.255'
            ValWarn = False
          end
          object EdURLBaseAPI: TdmkEdit
            Left = 342
            Top = 59
            Width = 155
            Height = 21
            MaxLength = 255
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '/solar_api/v1/'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = '/solar_api/v1/'
            ValWarn = False
          end
          object RGDeviceAssembler: TRadioGroup
            Left = 1
            Top = 1
            Width = 778
            Height = 40
            Align = alTop
            Caption = ' Fabricante / montadora: '
            Columns = 2
            Items.Strings = (
              'Indefinido'
              'Fronius')
            TabOrder = 0
            OnClick = RGDeviceAssemblerClick
          end
          object EdDeviceMACAddress: TdmkEdit
            Left = 526
            Top = 59
            Width = 223
            Height = 21
            MaxLength = 255
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
        object GroupBox2: TGroupBox
          Left = 2
          Top = 245
          Width = 780
          Height = 192
          Align = alClient
          Caption = '  Dispositivos:  '
          TabOrder = 2
          object dmkDBGridZTO1: TdmkDBGridZTO
            Left = 2
            Top = 59
            Width = 776
            Height = 131
            Align = alClient
            DataSource = DsDevices
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'C'#243'digo'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome_DeviceAssembler'
                Title.Caption = 'Fabricante / montadora'
                Width = 119
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'UniqueId'
                Title.Caption = 'ID'
                Width = 65
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DeviceSeq'
                Title.Caption = 'Dispositivo'
                Width = 58
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DeviceType'
                Width = 65
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PVPower'
                Title.Caption = 'Wp energia CC'
                Width = 77
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CustomName'
                Title.Caption = 'Identifica'#231#227'o dada pelo usu'#225'rio'
                Width = 160
                Visible = True
              end>
          end
          object Panel6: TPanel
            Left = 2
            Top = 15
            Width = 776
            Height = 44
            Align = alTop
            TabOrder = 1
            object BtDevice: TBitBtn
              Tag = 243
              Left = 4
              Top = 1
              Width = 120
              Height = 40
              Cursor = crHandPoint
              Caption = '&Dispositivo'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtDeviceClick
            end
          end
        end
        object GroupBox3: TGroupBox
          Left = 2
          Top = 101
          Width = 780
          Height = 144
          Align = alTop
          Caption = 
            '  Data logger - dispositivo de grava'#231#227'o sistem'#225'tica de eventos, ' +
            'observa'#231#245'es ou medi'#231#245'es:  '
          TabOrder = 1
          object dmkDBGridZTO2: TdmkDBGridZTO
            Left = 2
            Top = 59
            Width = 776
            Height = 83
            Align = alClient
            DataSource = DsLoggers
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'C'#243'digo'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'UniqueId'
                Title.Caption = 'ID'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome_DeviceAssembler'
                Title.Caption = 'Fabricante'
                Width = 71
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CashCurrency'
                Title.Caption = 'Moeda'
                Width = 39
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CashFactor'
                Title.Caption = '$ fator'
                Width = 46
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DeliveryFactor'
                Title.Caption = 'Fator entrega'
                Width = 120
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CO2Factor'
                Title.Caption = 'Fator CO2'
                Width = 120
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CO2Unit'
                Title.Caption = 'Un. CO2'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'HWVersion'
                Title.Caption = 'Vers'#227'o'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SWVersion'
                Title.Caption = 'Sub-vers'#227'o'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PlatformID'
                Title.Caption = 'ID plataforma'
                Width = 70
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ProductID'
                Title.Caption = 'ID produto'
                Width = 140
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'TimezoneLocation'
                Title.Caption = 'Local Timezone'
                Width = 99
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'TimezoneName'
                Title.Caption = 'Nome Timezone'
                Width = 83
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DeviceSeq'
                Title.Caption = 'Dispositivo'
                Width = 58
                Visible = True
              end>
          end
          object Panel7: TPanel
            Left = 2
            Top = 15
            Width = 776
            Height = 44
            Align = alTop
            TabOrder = 1
            object BtLogger: TBitBtn
              Tag = 243
              Left = 4
              Top = 1
              Width = 120
              Height = 40
              Cursor = crHandPoint
              Caption = '&Logger'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtLoggerClick
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 487
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 531
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sair'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 416
    Top = 65531
  end
  object QrDevices: TZQuery
    Connection = DMod.MyDB
    OnCalcFields = QrDevicesCalcFields
    SQL.Strings = (
      'SELECT * '
      'FROM devices')
    Params = <>
    Left = 248
    Top = 372
    object QrDevicesCodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrDevicesDeviceAssembler: TLargeintField
      FieldName = 'DeviceAssembler'
      Required = True
    end
    object QrDevicesUniqueId: TWideStringField
      FieldName = 'UniqueId'
      Required = True
      Size = 60
    end
    object QrDevicesDeviceSeq: TLargeintField
      FieldName = 'DeviceSeq'
      Required = True
    end
    object QrDevicesDeviceType: TWideStringField
      FieldName = 'DeviceType'
      Required = True
      Size = 60
    end
    object QrDevicesPVPower: TLargeintField
      FieldName = 'PVPower'
      Required = True
    end
    object QrDevicesCustomName: TWideStringField
      FieldName = 'CustomName'
      Required = True
      Size = 255
    end
    object QrDevicesLk: TLargeintField
      FieldName = 'Lk'
    end
    object QrDevicesDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrDevicesDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrDevicesUserCad: TLargeintField
      FieldName = 'UserCad'
    end
    object QrDevicesUserAlt: TLargeintField
      FieldName = 'UserAlt'
    end
    object QrDevicesAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrDevicesAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrDevicesNome_DeviceAssembler: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Nome_DeviceAssembler'
      Size = 255
      Calculated = True
    end
  end
  object DsDevices: TDataSource
    DataSet = QrDevices
    Left = 248
    Top = 420
  end
  object PMDevice: TPopupMenu
    Left = 112
    Top = 388
    object MiNewDivice1: TMenuItem
      Caption = '&Pesquisa novos dispositivos'
      OnClick = MiNewDivice1Click
    end
    object MiUpdDevice1: TMenuItem
      Caption = '&Altera dispositivo atual'
      Enabled = False
    end
    object MiDelDevice1: TMenuItem
      Caption = '&Exclui dispositivo atual'
      OnClick = MiDelDevice1Click
    end
  end
  object PMLogger: TPopupMenu
    Left = 76
    Top = 232
    object MiNewLogger1: TMenuItem
      Caption = '&Pesquisa novos dispositivos'
      OnClick = MiNewLogger1Click
    end
    object MiUpdLogger1: TMenuItem
      Caption = '&Altera dispositivo atual'
      Enabled = False
    end
    object MiDelLogger1: TMenuItem
      Caption = '&Exclui dispositivo atual'
      OnClick = MiDelLogger1Click
    end
  end
  object QrLoggers: TZQuery
    Connection = DMod.MyDB
    OnCalcFields = QrLoggersCalcFields
    SQL.Strings = (
      'SELECT * '
      'FROM loggers')
    Params = <>
    MasterFields = 'QrLoggers'
    Left = 368
    Top = 372
    object QrLoggersNome_DeviceAssembler: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Nome_DeviceAssembler'
      Size = 255
      Calculated = True
    end
    object QrLoggersCodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrLoggersDeviceAssembler: TLargeintField
      FieldName = 'DeviceAssembler'
      Required = True
    end
    object QrLoggersUniqueId: TWideStringField
      FieldName = 'UniqueId'
      Required = True
      Size = 60
    end
    object QrLoggersDeviceSeq: TLargeintField
      FieldName = 'DeviceSeq'
      Required = True
    end
    object QrLoggersProductID: TWideStringField
      FieldName = 'ProductID'
      Required = True
      Size = 255
    end
    object QrLoggersPlatformID: TWideStringField
      FieldName = 'PlatformID'
      Required = True
      Size = 255
    end
    object QrLoggersHWVersion: TWideStringField
      FieldName = 'HWVersion'
      Required = True
      Size = 60
    end
    object QrLoggersSWVersion: TWideStringField
      FieldName = 'SWVersion'
      Required = True
      Size = 60
    end
    object QrLoggersTimezoneLocation: TWideStringField
      FieldName = 'TimezoneLocation'
      Required = True
      Size = 60
    end
    object QrLoggersTimezoneName: TWideStringField
      FieldName = 'TimezoneName'
      Required = True
    end
    object QrLoggersUTCOffset: TLargeintField
      FieldName = 'UTCOffset'
      Required = True
    end
    object QrLoggersCashFactor: TFloatField
      FieldName = 'CashFactor'
      Required = True
    end
    object QrLoggersDeliveryFactor: TFloatField
      FieldName = 'DeliveryFactor'
      Required = True
    end
    object QrLoggersCashCurrency: TWideStringField
      FieldName = 'CashCurrency'
      Required = True
    end
    object QrLoggersCO2Factor: TFloatField
      FieldName = 'CO2Factor'
      Required = True
    end
    object QrLoggersCO2Unit: TWideStringField
      FieldName = 'CO2Unit'
      Required = True
    end
    object QrLoggersLk: TLargeintField
      FieldName = 'Lk'
    end
    object QrLoggersDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLoggersDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLoggersUserCad: TLargeintField
      FieldName = 'UserCad'
    end
    object QrLoggersUserAlt: TLargeintField
      FieldName = 'UserAlt'
    end
    object QrLoggersAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrLoggersAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object DsLoggers: TDataSource
    DataSet = QrLoggers
    Left = 368
    Top = 420
  end
end
