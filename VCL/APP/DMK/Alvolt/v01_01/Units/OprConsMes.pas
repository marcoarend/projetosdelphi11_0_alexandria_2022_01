unit OprConsMes;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, Grids, DBGrids,
  Menus, Mask, ZAbstractRODataset, ZAbstractDataset, ZDataset, ZCF2, dmkGeral,
  ResIntStrings, UnInternalConsts, UnMsgInt, UnInternalConsts2, dmkPermissoes,
  dmkEdit, dmkLabel, dmkDBEdit, dmkImage, dmkRadioGroup, UnDmkProcFunc,
  Variants, UnDmkENums, dmkDBGridZTO, UnGrl_DmkDB, Vcl.ComCtrls, UnAppEnums,
  dmkEditDateTimePicker, Math;

type
  TFmOprConsMes = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DGDados: TdmkDBGridZTO;
    QrOprConsMes: TZQuery;
    DsOprConsMes: TDataSource;
    QrOprConsMesIts: TZQuery;
    DsOprConsMesIts: TDataSource;
    LaMes: TLabel;
    CBMesRef: TComboBox;
    CBAnoRef: TComboBox;
    LaAno: TLabel;
    TPData: TdmkEditDateTimePicker;
    Label53: TLabel;
    EdHora: TdmkEdit;
    QrSum: TZQuery;
    GroupBox1: TGroupBox;
    PnLeituras: TPanel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EdMedOpeLeiConsu: TdmkEdit;
    EdMedOpeLeiInjet: TdmkEdit;
    EdWacProdSumLei: TdmkEdit;
    Label9: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    EdMedOpeValConsu: TdmkEdit;
    EdMedOpeValInjet: TdmkEdit;
    EdWacProdSumMes: TdmkEdit;
    SbWacProdSumLei: TSpeedButton;
    CkDataDefinida: TCheckBox;
    QrOprConsMesCodigo: TLargeintField;
    QrOprConsMesAnoRef: TLargeintField;
    QrOprConsMesMesRef: TSmallintField;
    QrOprConsMesDtaLeitura: TDateTimeField;
    QrOprConsMesWacProdUnit: TWideStringField;
    QrOprConsMesWacProdSumLei: TFloatField;
    QrOprConsMesWacProdSumMes: TFloatField;
    QrOprConsMesMedOpeLeiInjet: TFloatField;
    QrOprConsMesMedOpeLeiConsu: TFloatField;
    QrOprConsMesMedOpeValInjet: TFloatField;
    QrOprConsMesMedOpeValConsu: TFloatField;
    QrOprConsMesMedValSumInjet: TFloatField;
    QrOprConsMesMedValSumConsu: TFloatField;
    QrOprConsMesMedValSumCityL: TFloatField;
    QrOprConsMesMedValRefCityL: TFloatField;
    QrOprConsMesMedValSumNoComp: TFloatField;
    QrOprConsMesLk: TLargeintField;
    QrOprConsMesDataCad: TDateField;
    QrOprConsMesDataAlt: TDateField;
    QrOprConsMesUserCad: TLargeintField;
    QrOprConsMesUserAlt: TLargeintField;
    QrOprConsMesAlterWeb: TSmallintField;
    QrOprConsMesAtivo: TSmallintField;
    QrOprConsMesItsCodigo: TLargeintField;
    QrOprConsMesItsControle: TLargeintField;
    QrOprConsMesItsKind: TSmallintField;
    QrOprConsMesItsKwh: TFloatField;
    QrOprConsMesItsPreco: TFloatField;
    QrOprConsMesItsLk: TLargeintField;
    QrOprConsMesItsDataCad: TDateField;
    QrOprConsMesItsDataAlt: TDateField;
    QrOprConsMesItsUserCad: TLargeintField;
    QrOprConsMesItsUserAlt: TLargeintField;
    QrOprConsMesItsAlterWeb: TSmallintField;
    QrOprConsMesItsAtivo: TSmallintField;
    QrOprConsMesItsNO_Kind: TWideStringField;
    Label10: TLabel;
    EdMedValRefCityL: TdmkEdit;
    Label11: TLabel;
    Label12: TLabel;
    EdMedOpeLeiRetrn: TdmkEdit;
    Label13: TLabel;
    EdMedOpeLeiSaldo: TdmkEdit;
    Label14: TLabel;
    EdMedOpeLeiAcuml: TdmkEdit;
    Label15: TLabel;
    EdMedOpeLeiTtUsd: TdmkEdit;
    QrOprConsMesMedOpeLeiRetrn: TFloatField;
    QrOprConsMesMedOpeLeiSaldo: TFloatField;
    QrOprConsMesMedOpeLeiAcuml: TFloatField;
    QrOprConsMesMedOpeLeiTtUsd: TFloatField;
    QrOprConsMesMedValSumFatura: TFloatField;
    SbMedOpeLeiInjet: TSpeedButton;
    SbMedOpeValInjet: TSpeedButton;
    SbMedOpeValConsu: TSpeedButton;
    Label16: TLabel;
    EdMedOpeLeiOblig: TdmkEdit;
    QrOprConsMesMedOpeLeiOblig: TFloatField;
    Label17: TLabel;
    EdMedValPrcXpectN: TdmkEdit;
    QrOprConsMesMedValSumConsN: TFloatField;
    QrOprConsMesMedValSumConsX: TFloatField;
    QrOprConsMesMedValPrcXpectN: TFloatField;
    QrOprConsMesMedValPrcConsN: TFloatField;
    QrOprConsMesTotKWhCosXpectd: TFloatField;
    QrOprConsMesTotKWhCosObtned: TFloatField;
    QrOprConsMesTotKWhCostSaved: TFloatField;
    QrOprConsMesTotCtyCostSaved: TFloatField;
    QrOprConsMesTotAllCostSaved: TFloatField;
    Label18: TLabel;
    EdIncrsTaxPercnt: TdmkEdit;
    QrOprConsMesItsAnoRef: TLargeintField;
    QrOprConsMesItsMesRef: TSmallintField;
    QrOprConsMesIncrsTaxPercnt: TFloatField;
    QrOprConsMesInvestValBased: TFloatField;
    QrOprConsMesInvestValTaxInc: TFloatField;
    QrOprConsMesInvestValUpdtd: TFloatField;
    QrOprConsMesRateInvtXSaved: TFloatField;
    BtFin: TBitBtn;
    PMFin: TPopupMenu;
    FinInclui1: TMenuItem;
    FinAltera1: TMenuItem;
    FinExclui1: TMenuItem;
    QrOprConsMesFin: TZQuery;
    DsOprConsMesFin: TDataSource;
    QrOprConsMesFinCodigo: TLargeintField;
    QrOprConsMesFinAnoRef: TLargeintField;
    QrOprConsMesFinMesRef: TSmallintField;
    QrOprConsMesFinControle: TLargeintField;
    QrOprConsMesFinProdDta: TDateTimeField;
    QrOprConsMesFinNome: TWideStringField;
    QrOprConsMesFinValrCred: TFloatField;
    QrOprConsMesFinValrDebi: TFloatField;
    QrOprConsMesFinLk: TLargeintField;
    QrOprConsMesFinDataCad: TDateField;
    QrOprConsMesFinDataAlt: TDateField;
    QrOprConsMesFinUserCad: TLargeintField;
    QrOprConsMesFinUserAlt: TLargeintField;
    QrOprConsMesFinAlterWeb: TSmallintField;
    QrOprConsMesFinAtivo: TSmallintField;
    Panel6: TPanel;
    dmkDBGridZTO1: TdmkDBGridZTO;
    QrOprConsMesFinValrInvt: TFloatField;
    QrOprConsMesFinKindLct: TSmallintField;
    QrOprConsMesItsValor: TFloatField;
    QrOprConsMesInvestValInMes: TFloatField;
    QrOprConsMesInvestValTotal: TFloatField;
    QrOprConsMesMantncValBased: TFloatField;
    QrOprConsMesMantncValTaxInc: TFloatField;
    QrOprConsMesMantncValUpdtd: TFloatField;
    QrOprConsMesMantncValInMes: TFloatField;
    QrOprConsMesMantncValTotal: TFloatField;
    QrOprConsMesSavedValBased: TFloatField;
    QrOprConsMesSavedValTaxInc: TFloatField;
    QrOprConsMesSavedValUpdtd: TFloatField;
    QrOprConsMesSavedValInMes: TFloatField;
    QrOprConsMesSavedValTotal: TFloatField;
    QrOprConsMesRateMntnXSaved: TFloatField;
    QrOprConsMesRateSpntXSaved: TFloatField;
    dmkDBGridZTO2: TdmkDBGridZTO;
    Splitter1: TSplitter;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrGraGruYAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrGraGruYBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrGraGruYAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrGraGruYBeforeClose(DataSet: TDataSet);
    procedure SbWacProdSumLeiClick(Sender: TObject);
    procedure CkDataDefinidaClick(Sender: TObject);
    procedure QrOprConsMesBeforeClose(DataSet: TDataSet);
    procedure QrOprConsMesAfterScroll(DataSet: TDataSet);
    procedure QrOprConsMesItsCalcFields(DataSet: TDataSet);
    procedure SbMedOpeValInjetClick(Sender: TObject);
    procedure SbMedOpeLeiInjetClick(Sender: TObject);
    procedure SbMedOpeValConsuClick(Sender: TObject);
    procedure EdWacProdSumMesChange(Sender: TObject);
    procedure EdMedOpeValInjetChange(Sender: TObject);
    procedure EdMedOpeLeiObligChange(Sender: TObject);
    procedure EdMedOpeValConsuChange(Sender: TObject);
    procedure EdMedOpeLeiRetrnChange(Sender: TObject);
    procedure EdIncrsTaxPercntKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FinExclui1Click(Sender: TObject);
    procedure FinAltera1Click(Sender: TObject);
    procedure FinInclui1Click(Sender: TObject);
    procedure BtFinClick(Sender: TObject);
    procedure PMFinPopup(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure MostraCadastroOprConsMesIts(SQLType: TSQLType);
    procedure MostraCadastroOprConsMesFin(SQLType: TSQLType);
    //
    function  AnoPeriodoAnt(const Y, M: Integer): String;
    procedure GetAntWacProdSumLei();
    procedure GetAntMedOpeLeiInjet();
    procedure GetMedOpeValConsu();
    function  MesPeriodoAnt(const M: Integer): String;
  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure AtualizaSomas(Codigo, AnoRef, MesRef: Integer);
    procedure CalculaNaoInjetados();
    procedure CalculaNaoSaldoInjet();
    procedure CalculaConsumoTotal();
    procedure ReabreLeituraAnterior();
    procedure ReopenOprConsMes(Codigo, AnoRef, MesRef: Integer);
    procedure ReopenOprConsMesIts(Controle: Integer);
    procedure ReopenOprConsMesFin(Controle: Integer);

  end;

var
  FmOprConsMes: TFmOprConsMes;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, DmkDAC_PF, OprConsMesIts, OprConsMesFin,
  OprConsMesLei;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmOprConsMes.LocCod(Atual, Codigo: Integer);
begin
  //DefParams;
  //GOTOy.LC(Atual, Codigo);
  ReopenOprConsMes(0, 0, 0);
end;

function TFmOprConsMes.MesPeriodoAnt(const M: Integer): String;
var
  nMes: Integer;
begin
  nMes := M - 1;
  if M = 0 then
    nMes := 12;
  Result := Geral.FF0(nMes);
end;

procedure TFmOprConsMes.MostraCadastroOprConsMesFin(SQLType: TSQLType);
begin
  if Grl_DmkDB.CriaFm(TFmOprConsMesFin, FmOprConsMesFin, afmoNegarComAviso) then
  begin
    FmOprConsMesFin.ImgTipo.SQLType := SQLType;
    FmOprConsMesFin.FQrCab := QrOprConsMes;
    FmOprConsMesFin.FDsCab := DsOprConsMes;
    FmOprConsMesFin.FQrIts := QrOprConsMesFin;
    if SQLType = stIns then

    else
    begin
      FmOprConsMesFin.EdControle.ValueVariant := QrOprConsMesFinControle.Value;
      //
      FmOprConsMesFin.EdNome.Text             := QrOprConsMesFinNome.Value;
      FmOprConsMesFin.TPProdDta.Date          := QrOprConsMesFinProdDta.Value;
      FmOprConsMesFin.EdProdDta.ValueVariant  := QrOprConsMesFinProdDta.Value;
      FmOprConsMesFin.RGKindLct.ItemIndex     := QrOprConsMesFinKindLct.Value;
      case QrOprConsMesFinKindLct.Value of
        1: FmOprConsMesFin.EdValor.ValueVariant := QrOprConsMesFinValrDebi.Value;
        2: FmOprConsMesFin.EdValor.ValueVariant := QrOprConsMesFinValrCred.Value;
        3: FmOprConsMesFin.EdValor.ValueVariant := QrOprConsMesFinValrInvt.Value;
        else FmOprConsMesFin.EdValor.ValueVariant := 0;
      end;
    end;
    FmOprConsMesFin.ShowModal;
    FmOprConsMesFin.Destroy;
  end;
end;

procedure TFmOprConsMes.MostraCadastroOprConsMesIts(SQLType: TSQLType);
begin
  if Grl_DmkDB.CriaFm(TFmOprConsMesIts, FmOprConsMesIts, afmoNegarComAviso) then
  begin
    FmOprConsMesIts.ImgTipo.SQLType := SQLType;
    FmOprConsMesIts.FQrCab := QrOprConsMes;
    FmOprConsMesIts.FDsCab := DsOprConsMes;
    FmOprConsMesIts.FQrIts := QrOprConsMesIts;
    if SQLType = stIns then

    else
    begin
      FmOprConsMesIts.EdControle.ValueVariant := QrOprConsMesItsControle.Value;
      //
      FmOprConsMesIts.RGKind.ItemIndex := QrOprConsMesItsKind.Value;
      FmOprConsMesIts.EdKwh.ValueVariant := QrOprConsMesItsKwh.Value;
      FmOprConsMesIts.EdPreco.ValueVariant := QrOprConsMesItsPreco.Value;
      FmOprConsMesIts.EdValor.ValueVariant := QrOprConsMesItsValor.Value;
    end;
    FmOprConsMesIts.ShowModal;
    FmOprConsMesIts.Destroy;
  end;
end;

procedure TFmOprConsMes.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrOprConsMes);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrOprConsMes, QrOprConsMesIts);
end;

procedure TFmOprConsMes.PMFinPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(FinInclui1, QrOprConsMes);
  MyObjects.HabilitaMenuItemItsUpd(FinAltera1, QrOprConsMesFin);
  MyObjects.HabilitaMenuItemItsDel(FinExclui1, QrOprConsMesFin);
end;

procedure TFmOprConsMes.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrOprConsMes);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrOprConsMesIts);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrOprConsMesIts);
end;

procedure TFmOprConsMes.Va(Para: TVaiPara);
begin
  DefParams;
  //LaRegistro.Caption := GOTOy.Go(Para, QrOprConsMesCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////


procedure TFmOprConsMes.DefParams;
begin
{
  VAR_GOTOTABELA := 'cadastro_com_itens_cab';
  VAR_GOTOMYSQLTABLE := QrOprConsMes;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM cadastro_com_itens_cab');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
}
end;


procedure TFmOprConsMes.EdIncrsTaxPercntKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Targt, Mensal, J, N, I, X: Double;
begin
  if Key = VK_F4 then
  begin
   dmkPF.ObtemValorDouble(Targt, 10);
    //Targt  := EdIncrsTaxPercnt.ValueVariant;
    J := 0;
    N := 1;
    //I := 0;
    Mensal := 0;
    while  N > 0.0000000001 do
    begin
      J := Mensal;
      I := 0;
      while J < Targt do
      begin
        I := I + 1;
        X := Mensal + (N * I);
        J := Power(1 + (X / 100), 12);
        J := (J-1) * 100;
        if J > Targt then
          Mensal := X - N;
      end;
      N := N / 10;
    end;
    //Geral.MB_Info(FloatToStr(Mensal));
    EdIncrsTaxPercnt.ValueVariant := Mensal;
  end;
end;

procedure TFmOprConsMes.EdMedOpeLeiObligChange(Sender: TObject);
begin
  CalculaNaoSaldoInjet();
end;

procedure TFmOprConsMes.EdMedOpeLeiRetrnChange(Sender: TObject);
begin
  CalculaConsumoTotal();
end;

procedure TFmOprConsMes.EdMedOpeValConsuChange(Sender: TObject);
begin
  CalculaNaoSaldoInjet();
  CalculaConsumoTotal();
end;

procedure TFmOprConsMes.EdMedOpeValInjetChange(Sender: TObject);
begin
  CalculaNaoInjetados();
  CalculaNaoSaldoInjet();
end;

procedure TFmOprConsMes.EdWacProdSumMesChange(Sender: TObject);
begin
  CalculaNaoInjetados();
end;

procedure TFmOprConsMes.ItsAltera1Click(Sender: TObject);
begin
  MostraCadastroOprConsMesIts(stUpd);
end;

procedure TFmOprConsMes.CabExclui1Click(Sender: TObject);
var
  Controle, Codigo, AnoRef, MesRef: Integer;
begin
  Codigo := QrOprConsMesCodigo.Value;
  AnoRef := QrOprConsMesAnoRef.Value;
  MesRef := QrOprConsMesMesRef.Value;
  //
  if Grl_DmkDB.ExcluiRegistros('', Dmod.QrUpd, Dmod.MyDB, 'oprconsmes', [
  'Codigo', 'AnoRef', 'MesRef'], ['=','=','='], [
  Codigo, AnoRef, MesRef], '') then
    ReopenOprConsMes(0, 0, 0);
{
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
}
end;

procedure TFmOprConsMes.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmOprConsMes.QueryPrincipalAfterOpen;
begin
end;

procedure TFmOprConsMes.ItsExclui1Click(Sender: TObject);
var
  Controle, Codigo, AnoRef, MesRef: Integer;
begin
  Codigo := QrOprConsMesCodigo.Value;
  AnoRef := QrOprConsMesAnoRef.Value;
  MesRef := QrOprConsMesMesRef.Value;
  //
  if Grl_DmkDB.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'OprConsMesIts', 'Controle', QrOprConsMesItsControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := Grl_DmkDB.LocalizaPriorNextLargeIntQr(QrOprConsMesIts,
      QrOprConsMesItsControle, QrOprConsMesItsControle.Value);
    FmOprConsMes.AtualizaSomas(Codigo, AnoRef, MesRef);
    ReopenOprConsMesIts(Controle);
  end;
end;

procedure TFmOprConsMes.ReabreLeituraAnterior();
var
  DtaLeitura: String;
begin
  DtaLeitura := Geral.FDT_TP_Ed(TPData.Date, EdHora.Text);
  //
  Grl_DmkDB.AbreSQLQuery0(QrSum, Dmod.MyDB, [
  'SELECT * FROM oprconsmes ',
  'WHERE DtaLeitura < "' + DtaLeitura + '" ',
  'ORDER BY DtaLeitura desc ',
  'LIMIT 0,1 ',
  '']);
end;

procedure TFmOprConsMes.ReopenOprConsMes(Codigo, AnoRef, MesRef: Integer);
begin
  Grl_DmkDB.AbreSQLQuery0(QrOprConsMes, Dmod.MyDB, [
  'SELECT * ',
  'FROM oprconsmes ',
  'ORDER BY AnoRef desc, MesRef desc ',
  '']);
  QrOprConsMes.Locate('Codigo;AnoRef;MesRef',
    VarArrayOf([Codigo, AnoRef, MesRef]), []);
end;

procedure TFmOprConsMes.ReopenOprConsMesFin(Controle: Integer);
begin
  if QrOprConsMes.RecordCount > 0 then
  begin
    Grl_DmkDB.AbreSQLQuery0(QrOprConsMesFin, Dmod.MyDB, [
(*
    'SELECT omf.*, ',
    'CAST(CASE KindLct ',
    '  WHEN 1 THEN ValrDebi ',
    '  WHEN 2 THEN ValrCred ',
    '  WHEN 3 THEN ValrInvt ',
    '  ELSE 0 END as real) Valor,',
    'CASE KindLct ',
    '  WHEN 1 THEN "D�bito"',
    '  WHEN 2 THEN "Cr�dito" ',
    '  WHEN 3 THEN "Investimento" ',
    '  ELSE "? ? ?" END NO_Tipo',
*)
    'SELECT omf.* ',
    //
    'FROM oprconsmesfin omf',
    'WHERE Codigo=' + Geral.FF0(QrOprConsMes.FieldByName('Codigo').AsInteger),
    'AND AnoRef=' + Geral.FF0(QrOprConsMes.FieldByName('AnoRef').AsInteger),
    'AND MesRef=' + Geral.FF0(QrOprConsMes.FieldByName('MesRef').AsInteger),
    'ORDER BY ProdDta',
    '']);
    //
    QrOprConsMesFin.Locate('Controle', Controle, []);
  end;
end;

procedure TFmOprConsMes.ReopenOprConsMesIts(Controle: Integer);
begin
//  UnDmkDAC_PF.AbreMySQLQuery0(QrOprConsMesIts, Dmod.MyDB, [
  if QrOprConsMes.RecordCount > 0 then
  begin
    Grl_DmkDB.AbreSQLQuery0(QrOprConsMesIts, Dmod.MyDB, [
    'SELECT * ',
    'FROM oprconsmesits ',
    'WHERE Codigo=' + Geral.FF0(QrOprConsMes.FieldByName('Codigo').AsInteger),
    'AND AnoRef=' + Geral.FF0(QrOprConsMes.FieldByName('AnoRef').AsInteger),
    'AND MesRef=' + Geral.FF0(QrOprConsMes.FieldByName('MesRef').AsInteger),
    //'ORDER BY AnoRef desc, MesRef desc ',
    '']);
    //
    QrOprConsMesIts.Locate('Controle', Controle, []);
  end;
end;


procedure TFmOprConsMes.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmOprConsMes.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmOprConsMes.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmOprConsMes.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmOprConsMes.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmOprConsMes.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOprConsMes.BtSaidaClick(Sender: TObject);
begin
  try
    VAR_CADASTRO := QrOprConsMes.FieldByName('Codigo').AsInteger;
  except
    VAR_CADASTRO := 0;
  end;
  Close;
end;

procedure TFmOprConsMes.ItsInclui1Click(Sender: TObject);
begin
  MostraCadastroOprConsMesIts(stIns);
end;

procedure TFmOprConsMes.CabAltera1Click(Sender: TObject);
//begin
{
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrOprConsMes, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'cadastro_com_itens_cab');
}
//var
  //Ano, Mes, Dia: Word;
begin
  CBAnoRef.Enabled := False;
  CbMesRef.Enabled := False;
  //
  //Codigo              :=
  EdCodigo.ValueVariant              := 0;
  //DecodeDate(Date(), Ano, Mes, Dia);
  CBAnoRef.text                      := Geral.FF0(QrOprConsMesAnoRef.Value);
  CbMesRef.Itemindex                 := QrOprConsMesMesRef.Value -1;
  TPData.Date                        := Int(QrOprConsMesDtaLeitura.Value);
  EdHora.ValueVariant                := QrOprConsMesDtaLeitura.Value;
  //EdWacProdUnit.ValueVariant         := 'kWh';
  EdWacProdSumLei.ValueVariant       := QrOprConsMesWacProdSumLei.Value;
  EdWacProdSumMes.ValueVariant       := QrOprConsMesWacProdSumMes.Value;
  EdMedOpeValInjet.ValueVariant      := QrOprConsMesMedOpeValInjet.Value;
  EdMedOpeLeiInjet.ValueVariant      := QrOprConsMesMedOpeLeiInjet.Value;
  EdMedOpeLeiConsu.ValueVariant      := QrOprConsMesMedOpeLeiConsu.Value;
  EdMedOpeLeiOblig.ValueVariant      := QrOprConsMesMedOpeLeiOblig.Value;
  EdMedOpeValConsu.ValueVariant      := QrOprConsMesMedOpeValConsu.Value;
  EdMedOpeLeiRetrn.ValueVariant      := QrOprConsMesMedOpeLeiRetrn.Value;
  EdMedOpeLeiSaldo.ValueVariant      := QrOprConsMesMedOpeLeiSaldo.Value;
  EdMedOpeLeiAcuml.ValueVariant      := QrOprConsMesMedOpeLeiAcuml.Value;
  EdMedOpeLeiTtUsd.ValueVariant      := QrOprConsMesMedOpeLeiTtUsd.Value;
  EdMedValRefCityL.ValueVariant      := QrOprConsMesMedValRefCityL.Value;
  EdMedValPrcXpectN.ValueVariant     := QrOprConsMesMedValPrcXpectN.Value;
  EdIncrsTaxPercnt.ValueVariant      := QrOprConsMesIncrsTaxPercnt.Value;
  (*
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrOprConsMes, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'cadastro_com_itens_cab');
*)
  CkDataDefinida.Checked := True;
  ImgTipo.SQLType := stUpd;
  PnEdita.Visible := True;
  PnDados.Visible := False;
end;

procedure TFmOprConsMes.BtConfirmaClick(Sender: TObject);
var
  DtaLeitura, WacProdUnit: String;
  Codigo, AnoRef, MesRef: Integer;
  WacProdSumLei, WacProdSumMes, MedOpeLeiInjet, MedOpeLeiConsu, MedOpeValInjet,
  //MedValSumInjet, MedValSumConsu, MedValSumCityL, MedValSumNoComp, MedValSumFatura
  MedOpeValConsu, MedValRefCityL, MedOpeLeiOblig, MedOpeLeiRetrn,
  MedOpeLeiSaldo, MedOpeLeiAcuml, MedOpeLeiTtUsd, MedValPrcXpectN,
  IncrsTaxPercnt: Double;


  SQLType: TSQLType;
begin
  //FDiaIni := EncodeDate(Geral.IMV(CBAno.Text), CBMes.ItemIndex + 1, 1);
  SQLType             := ImgTipo.SQLType;
  //
  Codigo              := EdCodigo.ValueVariant;
  AnoRef              := Geral.IMV(CBAnoRef.Text);
  MesRef              := CBMesRef.ItemIndex + 1;
  DtaLeitura          := Geral.FDT_TP_Ed(TPData.Date, EdHora.Text);
  WacProdUnit         := 'kWh';
  WacProdSumLei       := EdWacProdSumLei.ValueVariant;
  WacProdSumMes       := EdWacProdSumMes.ValueVariant;
  MedOpeLeiInjet      := EdMedOpeLeiInjet.ValueVariant;
  MedOpeLeiConsu      := EdMedOpeLeiConsu.ValueVariant;
  MedOpeValInjet      := EdMedOpeValInjet.ValueVariant;
  MedOpeValConsu      := EdMedOpeValConsu.ValueVariant;
  //MedValSumInjet      := ;
  //MedValSumConsu      := ;
  //MedValSumCityL      := ;
  MedValRefCityL      := EdMedValRefCityL.ValueVariant;
  //MedValSumNoComp     := ;
  MedOpeLeiOblig      := EdMedOpeLeiOblig.ValueVariant;
  MedOpeLeiRetrn      := EdMedOpeLeiRetrn.ValueVariant;
  MedOpeLeiSaldo      := EdMedOpeLeiSaldo.ValueVariant;
  MedOpeLeiAcuml      := EdMedOpeLeiAcuml.ValueVariant;
  MedOpeLeiTtUsd      := EdMedOpeLeiTtUsd.ValueVariant;
  MedValPrcXpectN     := EdMedValPrcXpectN.ValueVariant;
  IncrsTaxPercnt      := EdIncrsTaxPercnt.ValueVariant;

  //if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;

  //
  //Codigo := Grl_DmkDB.GetNxtCodigoInt(Dmod.QrAux, Dmod.MyDB, 'OprConsMes', 'Codigo', SQLType, CodAtual?);
  if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, 'oprconsmes', False, [
  'DtaLeitura', 'MedOpeLeiOblig',
  'WacProdUnit', 'WacProdSumLei', 'WacProdSumMes',
  'MedOpeLeiInjet', 'MedOpeLeiConsu', 'MedOpeLeiRetrn',
  'MedOpeLeiSaldo', 'MedOpeLeiAcuml', 'MedOpeLeiTtUsd',
  'MedOpeValInjet', 'MedOpeValConsu', 'MedValRefCityL',
  (*'MedValSumInjet', 'MedValSumConsu', 'MedValSumCityL',
  'MedValSumNoComp', 'MedValSumFatura'*)
  'MedValPrcXpectN', 'IncrsTaxPercnt'], [
  'Codigo', 'AnoRef', 'MesRef'], [
  DtaLeitura, MedOpeLeiOblig,
  WacProdUnit, WacProdSumLei, WacProdSumMes,
  MedOpeLeiInjet, MedOpeLeiConsu, MedOpeLeiRetrn,
  MedOpeLeiSaldo, MedOpeLeiAcuml, MedOpeLeiTtUsd,
  MedOpeValInjet, MedOpeValConsu, MedValRefCityL,
  (*MedValSumInjet, MedValSumConsu, MedValSumCityL,
  MedValSumNoComp, MedValSumFatura*)
  MedValPrcXpectN, IncrsTaxPercnt], [
  Codigo, AnoRef, MesRef], True, dmksqlinsInsOnly, '',  stDesktop, False) then
  begin
    AtualizaSomas(Codigo, AnoRef, MesRef);
    LocCod(Codigo, Codigo);
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    //GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmOprConsMes.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
(*
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'cadastro_com_itens_cab', Codigo);
*)
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
(*
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'cadastro_com_itens_cab', 'Codigo');
*)
end;

procedure TFmOprConsMes.BtFinClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMFin, BtFin);
end;

procedure TFmOprConsMes.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

function TFmOprConsMes.AnoPeriodoAnt(const Y, M: Integer): String;
var
  nAno: Integer;
begin
  nAno := Y;
  if M = 1 then
    nAno := nAno - 1;
  Result := Geral.FF0(nAno);
end;

procedure TFmOprConsMes.AtualizaSomas(Codigo, AnoRef, MesRef: Integer);
const
  sProcName = 'TFmOprConsMes.AtualizaSomas()';
var
  MedValSumInjet, MedValSumConsu, MedValSumCityL, MedValSumNoComp, Valor,
  MedValSumFatura, MedValSumConsN, MedValSumConsX, MedValPrcConsN,
  TotKWhCosXpectd, TotKWhCosObtned, TotKWhCostSaved, TotCtyCostSaved,
  TotAllCostSaved, MedOpeLeiTtUsd, MedValRefCityL, MedOpeValConsu,
  MedOpeLeiOblig, IncrsTaxPercnt,
  InvestValBased, InvestValTaxInc, InvestValUpdtd, InvestValInMes, InvestValTotal,
  MantncValBased, MantncValTaxInc, MantncValUpdtd, MantncValInMes, MantncValTotal,
  SavedValBased, SavedValTaxInc, SavedValUpdtd, SavedValInMes, SavedValTotal,
  RateInvtXSaved, RateMntnXSaved, RateSpntXSaved: Double;

  SQLType: TSQLType;
  KindLeituraKWH: TKindLeituraKWH;
begin
  //FDiaIni := EncodeDate(Geral.IMV(CBAno.Text), CBMes.ItemIndex + 1, 1);
  SQLType             := stUpd;
  //
(*
  Codigo              := QrOprConsMesCodigo.Value;
  AnoRef              := QrOprConsMesAnoRef.Value;
  MesRef              := QrOprConsMesMesRef.Value;
*)
  MedValSumInjet      := 0;
  MedValSumConsu      := 0;
  MedValSumCityL      := 0;
  MedValSumNoComp     := 0;
  MedValSumFatura     := 0;
  MedValSumConsN      := 0;
  MedValSumConsX      := 0;
  MedValPrcConsN      := 0;
  TotKWhCosXpectd     := 0;
  TotKWhCosObtned     := 0;
  TotKWhCostSaved     := 0;
  TotCtyCostSaved     := 0;
  TotAllCostSaved     := 0;
  //
  InvestValBased      := 0;
  InvestValTaxInc     := 0;
  InvestValUpdtd      := 0;
  InvestValInMes      := 0;
  InvestValTotal      := 0;
  //
  MantncValBased      := 0;
  MantncValTaxInc     := 0;
  MantncValUpdtd      := 0;
  MantncValInMes      := 0;
  MantncValTotal      := 0;
  //
  SavedValBased       := 0;
  SavedValTaxInc      := 0;
  SavedValUpdtd       := 0;
  SavedValInMes       := 0;
  SavedValTotal       := 0;

  RateInvtXSaved      := 0;
  RateMntnXSaved      := 0;
  RateSpntXSaved      := 0;
  //
  Grl_DmkDB.AbreSQLQuery0(QrSum, Dmod.MyDB, [
  'SELECT ',
  'SUM(ValrCred) ValrCred, ',
  'SUM(ValrInvt) ValrInvt, ',
  'SUM(ValrDebi) ValrDebi  ',
  'FROM oprconsmesfin',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  'AND AnoRef=' + Geral.FF0(AnoRef),
  'AND MesRef=' + Geral.FF0(MesRef),
  '']);
  InvestValInMes      := Grl_DmkDB.AsFloat(QrSum, 'ValrInvt');
  MantncValInMes      := Grl_DmkDB.AsFloat(QrSum, 'ValrDebi');
  SavedValInMes       := Grl_DmkDB.AsFloat(QrSum, 'ValrCred');
  //
  Grl_DmkDB.AbreSQLQuery0(QrSum, Dmod.MyDB, [
  'SELECT Kind, SUM(Valor) Valor ',
  'FROM oprconsmesits ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  'AND AnoRef=' + Geral.FF0(AnoRef),
  'AND MesRef=' + Geral.FF0(MesRef),
  'GROUP BY Kind ',
  '']);
  QrSum.First;
  while not QrSum.Eof do
  begin
    Valor := QrSum.FieldByName('Valor').AsFloat;
    KindLeituraKWH := TKindLeituraKWH(QrSum.FieldByName('Kind').AsInteger);
    //
    case KindLeituraKWH of
      //klkwhUnknown=0,
      klkwhEECCMinimo   ,
      klkwhEECCDifer    ,
      klkwhCXCContrato  ,
      klkwhCXCDifer     : MedValSumConsu := MedValSumConsu + Valor;
      klkwhIluminPublic : MedValSumCityL := MedValSumCityL + Valor;
      klkwhCGeraCredit  : MedValSumInjet := MedValSumInjet + Valor;
      else Geral.MB_Erro('"TKindLeituraKWH" (1) n�o implementado em ' + sProcName);
    end;
    case KindLeituraKWH of
      //klkwhUnknown=0,
      klkwhEECCMinimo   : MedValSumConsN := MedValSumConsN + Valor;
      klkwhEECCDifer    : MedValSumConsX := MedValSumConsX + Valor;
      klkwhCXCContrato  : MedValSumConsN := MedValSumConsN + Valor;
      klkwhCXCDifer     : MedValSumConsX := MedValSumConsX + Valor;
      klkwhIluminPublic : ;
      klkwhCGeraCredit  : ;
      else Geral.MB_Erro('"TKindLeituraKWH" (2) n�o implementado em ' + sProcName);
    end;
    QrSum.Next;
  end;
  MedValSumFatura := (MedValSumConsu + MedValSumCityL + MedValSumNoComp) -
    MedValSumInjet;
  //
  Grl_DmkDB.AbreSQLQuery0(QrSum, Dmod.MyDB, [
  'SELECT * ',
  'FROM oprconsmes ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  'AND AnoRef=' + Geral.FF0(AnoRef),
  'AND MesRef=' + Geral.FF0(MesRef),
  '']);
  IncrsTaxPercnt   := QrSum.FieldByName('IncrsTaxPercnt').AsFloat;
  MedOpeValConsu   := QrSum.FieldByName('MedOpeValConsu').AsFloat;
  MedOpeLeiOblig   := QrSum.FieldByName('MedOpeLeiOblig').AsFloat;
  MedValRefCityL   := QrSum.FieldByName('MedValRefCityL').AsFloat;
  MedOpeLeiTtUsd   := QrSum.FieldByName('MedOpeLeiTtUsd').AsFloat;
  if MedOpeLeiOblig > 0 then
    MedValPrcConsN := MedValSumConsN  / MedOpeLeiOblig
  else
  if MedValSumConsu > 0 then
    MedValPrcConsN := MedValSumConsN  / MedValSumConsu;
  //
  TotKWhCosXpectd  := MedOpeLeiTtUsd * MedValPrcConsN ;
  TotKWhCosObtned  := MedValSumConsN + MedValSumConsX - MedValSumInjet;
  TotKWhCostSaved  := TotKWhCosXpectd - TotKWhCosObtned;
  TotCtyCostSaved  := MedValRefCityL - MedValSumCityL;
  TotAllCostSaved  := TotKWhCostSaved + TotCtyCostSaved;
  //
  //InvestValTaxInc  := QrSum.FieldByName('InvestValTaxInc').AsFloat;
  Grl_DmkDB.AbreSQLQuery0(QrSum, Dmod.MyDB, [
  'SELECT * ',
  'FROM oprconsmes ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  'AND AnoRef=' + AnoPeriodoAnt(AnoRef, MesRef),
  'AND MesRef=' + MesPeriodoAnt(MesRef),
  '']);
  //Geral.MB_SQL(Self, QrSum);
  //
  InvestValBased      := QrSum.FieldByName('InvestValTotal').AsFloat;
  InvestValTaxInc     := InvestValBased * IncrsTaxPercnt / 100;
  InvestValUpdtd      := InvestValBased + InvestValTaxInc;
  //InvestValInMes      := 0;
  InvestValTotal      := InvestValUpdtd + InvestValInMes;
  //
  MantncValBased      := QrSum.FieldByName('MantncValTotal').AsFloat;;
  MantncValTaxInc     := MantncValBased * IncrsTaxPercnt / 100;
  MantncValUpdtd      := MantncValBased + MantncValTaxInc;
  //MantncValInMes      := 0;
  MantncValTotal      := MantncValUpdtd + MantncValInMes;
  //
  SavedValBased      := QrSum.FieldByName('SavedValTotal').AsFloat;;
  SavedValTaxInc     := SavedValBased * IncrsTaxPercnt / 100;
  SavedValUpdtd      := SavedValBased + SavedValTaxInc;
  //SavedValInMes      := 0;
  SavedValTotal      := SavedValUpdtd + SavedValInMes + TotAllCostSaved;
  //

  if  InvestValTotal <> 0 then
    RateInvtXSaved      := ((SavedValTotal / InvestValTotal) - 1) * 100;
  if  MantncValTotal <> 0 then
    RateMntnXSaved      := ((SavedValTotal / MantncValTotal) - 1) * 100;
  if (InvestValTotal + MantncValTotal) <> 0 then
    RateSpntXSaved      := ((SavedValTotal / (InvestValTotal + MantncValTotal)) - 1) * 100;
  //


  //
  if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, 'oprconsmes', False, [
  'MedValSumInjet', 'MedValSumConsN', 'MedValSumConsX',
  'MedValSumConsu', 'MedValSumCityL', 'MedValRefCityL',
  'MedValSumNoComp', 'MedValSumFatura',
  'MedValPrcConsN', 'TotKWhCosXpectd', 'TotKWhCosObtned',
  'TotKWhCostSaved', 'TotCtyCostSaved', 'TotAllCostSaved',
  'InvestValBased', 'InvestValTaxInc', 'InvestValUpdtd', 'InvestValInMes', 'InvestValTotal',
  'MantncValBased', 'MantncValTaxInc', 'MantncValUpdtd', 'MantncValInMes', 'MantncValTotal',
  'SavedValBased', 'SavedValTaxInc', 'SavedValUpdtd', 'SavedValInMes', 'SavedValTotal',
  'RateInvtXSaved', 'RateMntnXSaved', 'RateSpntXSaved'], [
  'Codigo', 'AnoRef', 'MesRef'], [
  MedValSumInjet, MedValSumConsN, MedValSumConsX,
  MedValSumConsu, MedValSumCityL, MedValRefCityL,
  MedValSumNoComp, MedValSumFatura,
  MedValPrcConsN, TotKWhCosXpectd, TotKWhCosObtned,
  TotKWhCostSaved, TotCtyCostSaved, TotAllCostSaved,
  InvestValBased, InvestValTaxInc, InvestValUpdtd, InvestValInMes, InvestValTotal,
  MantncValBased, MantncValTaxInc, MantncValUpdtd, MantncValInMes, MantncValTotal,
  SavedValBased, SavedValTaxInc, SavedValUpdtd, SavedValInMes, SavedValTotal,
  RateInvtXSaved, RateMntnXSaved, RateSpntXSaved], [
  Codigo, AnoRef, MesRef], True, dmksqlinsInsOnly, '',  stDesktop, False) then
  begin
    ReopenOprConsMes(Codigo, AnoRef, MesRef);
  end;
end;

procedure TFmOprConsMes.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmOprConsMes.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
  MyObjects.PreencheCBAnoECBMes(CBAnoRef, CBMesRef, -1);
  LocCod(FCabIni, FCabIni);
end;

procedure TFmOprConsMes.SbNumeroClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Codigo(QrOprConsMesCodigo.Value, LaRegistro.Caption);
end;

procedure TFmOprConsMes.SbMedOpeLeiInjetClick(Sender: TObject);
begin
  GetAntMedopeLeiInjet();
end;

procedure TFmOprConsMes.SbMedOpeValConsuClick(Sender: TObject);
begin
  GetMedOpeValConsu();
end;

procedure TFmOprConsMes.SbMedOpeValInjetClick(Sender: TObject);
begin
  ReabreLeituraAnterior();
  //
  EdMedOpeValInjet.ValueVariant := EdMedOpeLeiInjet.ValueVariant -
    QrSum.FieldByName('MedOpeLeiInjet').AsFloat;
end;

procedure TFmOprConsMes.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmOprConsMes.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrOprConsMesCodigo.Value, LaRegistro.Caption);
end;

procedure TFmOprConsMes.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  //Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmOprConsMes.QrGraGruYAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmOprConsMes.QrGraGruYAfterScroll(DataSet: TDataSet);
begin
  ReopenOprConsMesIts(0);
end;

procedure TFmOprConsMes.FinInclui1Click(Sender: TObject);
begin
  MostraCadastroOprConsMesFin(stIns);
end;

procedure TFmOprConsMes.FinAltera1Click(Sender: TObject);
begin
  MostraCadastroOprConsMesFin(stUpd);
end;

procedure TFmOprConsMes.FinExclui1Click(Sender: TObject);
var
  Controle, Codigo, AnoRef, MesRef: Integer;
begin
  Codigo := QrOprConsMesCodigo.Value;
  AnoRef := QrOprConsMesAnoRef.Value;
  MesRef := QrOprConsMesMesRef.Value;
  //
  if Grl_DmkDB.ExcluiRegistroInt1('Confirma a exclus�o do item financeiro selecionado?',
  'OprConsMesFin', 'Controle', QrOprConsMesFinControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := Grl_DmkDB.LocalizaPriorNextLargeIntQr(QrOprConsMesFin,
      QrOprConsMesFinControle, QrOprConsMesFinControle.Value);
    FmOprConsMes.AtualizaSomas(Codigo, AnoRef, MesRef);
    ReopenOprConsMesFin(Controle);
  end;
end;

procedure TFmOprConsMes.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrOprConsMes.FieldByName('Codigo').AsInteger <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmOprConsMes.SbQueryClick(Sender: TObject);
begin
(*
  LocCod(QrOprConsMesCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'cadastro_com_itens_cab', Dmod.MyDB, CO_VAZIO));
*)
end;

procedure TFmOprConsMes.SbWacProdSumLeiClick(Sender: TObject);
begin
  GetAntWacProdSumLei();
end;

procedure TFmOprConsMes.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOprConsMes.GetAntMedOpeLeiInjet();
begin
  ReabreLeituraAnterior();
  //
  EdMedOpeLeiInjet.ValueVariant := EdMedOpeValInjet.ValueVariant +
    QrSum.FieldByName('MedOpeLeiInjet').AsFloat;
end;

procedure TFmOprConsMes.GetAntWacProdSumLei();
var
  DtaLeitura: String;
begin
  DtaLeitura := Geral.FDT_TP_Ed(TPData.Date, EdHora.Text);
  Grl_DmkDB.AbreSQLQuery0(QrSum, Dmod.MyDB, [
  'SELECT SUM(Quantia) Quantia ',
  'FROM wacprodvalues ',
  'WHERE ProdDta <= "' + DtaLeitura + '" ',
  '']);
  EdWacProdSumLei.ValueVariant := QrSum.FieldByName('Quantia').AsFloat / 1000;
  Grl_DmkDB.AbreSQLQuery0(QrSum, Dmod.MyDB, [
  'SELECT * FROM oprconsmes ',
  'WHERE DtaLeitura < "' + DtaLeitura + '" ',
  'ORDER BY DtaLeitura desc ',
  'LIMIT 0,1 ',
  '']);
  if QrSum.FieldCount > 0 then
  begin
    EdWacProdSumMes.ValueVariant := EdWacProdSumLei.ValueVariant -
      QrSum.FieldByName('WacProdSumLei').AsFloat;
  end;
end;

procedure TFmOprConsMes.GetMedOpeValConsu();
begin
  ReabreLeituraAnterior();
  //
  EdMedOpeValConsu.ValueVariant := EdMedOpeLeiConsu.ValueVariant -
    QrSum.FieldByName('MedOpeLeiConsu').AsFloat;
end;

procedure TFmOprConsMes.CabInclui1Click(Sender: TObject);
var
  Ano, Mes, Dia: Word;
begin
  CBAnoRef.Enabled := True;
  CbMesRef.Enabled := True;
  //
  //Codigo              :=
  EdCodigo.ValueVariant              := 0;
  DecodeDate(Date(), Ano, Mes, Dia);
  CBAnoRef.text                      := Geral.FF0(Ano);
  CbMesRef.Itemindex                 := Mes -1;
  TPData.Date                        := Int(Date());
  EdHora.ValueVariant                := 0;
  //EdWacProdUnit.ValueVariant         := 'kWh';
  EdWacProdSumLei.ValueVariant       := 0;
  EdWacProdSumMes.ValueVariant       := 0;
  EdMedOpeValInjet.ValueVariant      := 0;
  EdMedOpeLeiInjet.ValueVariant      := 0;
  EdMedOpeLeiConsu.ValueVariant      := 0;
  EdMedOpeLeiOblig.ValueVariant      := 0;
  EdMedOpeValConsu.ValueVariant      := 0;
  EdMedOpeLeiRetrn.ValueVariant      := 0;
  EdMedOpeLeiSaldo.ValueVariant      := 0;
  EdMedOpeLeiAcuml.ValueVariant      := 0;
  EdMedOpeLeiTtUsd.ValueVariant      := 0;
  EdMedValRefCityL.ValueVariant      := 0;
  EdMedValPrcXpectN.ValueVariant     := 0;
  EdIncrsTaxPercnt.ValueVariant      := 0;
  //
  CkDataDefinida.Checked := False;
  ImgTipo.SQLType := stIns;
  PnEdita.Visible := True;
  PnDados.Visible := False;
end;

procedure TFmOprConsMes.CalculaConsumoTotal;
begin
  EdMedOpeLeiTtUsd.ValueVariant :=
    EdMedOpeValConsu.ValueVariant + EdMedOpeLeiRetrn.ValueVariant;
end;

procedure TFmOprConsMes.CalculaNaoInjetados();
begin
  EdMedOpeLeiRetrn.ValueVariant := EdWacProdSumMes.ValueVariant - EdMedOpeValInjet.ValueVariant;
end;

procedure TFmOprConsMes.CalculaNaoSaldoInjet;
var
  Saldo: Double;
begin
  Saldo := (EdMedOpeValInjet.ValueVariant +
    EdMedOpeLeiOblig.ValueVariant) - EdMedOpeValConsu.ValueVariant;
  EdMedOpeLeiSaldo.ValueVariant := Saldo;
  ReabreLeituraAnterior();
  Saldo := Saldo + QrSum.FieldByName('MedOpeLeiAcuml').AsFloat;
  if Saldo < 0 then
    Saldo := 0;
  EdMedOpeLeiAcuml.ValueVariant := Saldo;
end;

procedure TFmOprConsMes.CkDataDefinidaClick(Sender: TObject);
var
  Preencher: Boolean;
  Codigo, AnoRef, MesRef: Integer;
begin
  Preencher := (PnLeituras.Visible = False) and (CkDataDefinida.Checked) and
    (ImgTipo.SQLType = stIns);
  if Preencher then
  begin
    if QrOprConsMes.RecordCount > 2 then
    begin
      GetAntWacProdSumLei();
      //
      if Grl_DmkDB.CriaFm(TFmOprConsMesLei, FmOprConsMesLei, afmoNegarComAviso) then
      begin
        FmOprConsMesLei.ShowModal;
        if FmOprConsMesLei.FOK then
        begin
          EdMedOpeValInjet.ValueVariant := FmOprConsMesLei.EdMedOpeValInjet.ValueVariant;
          EdMedOpeLeiConsu.ValueVariant := FmOprConsMesLei.EdMedOpeLeiConsu.ValueVariant;
          //
          GetAntMedOpeLeiInjet();
        end;
        FmOprConsMesLei.Destroy;
      end;
      Codigo := QrOprConsMesCodigo.Value;
      AnoRef := QrOprConsMesAnoRef.Value;
      MesRef := QrOprConsMesMesRef.Value;
      Grl_DmkDB.AbreSQLQuery0(QrSum, Dmod.MyDB, [
      'SELECT * ',
      'FROM oprconsmes ',
      'WHERE Codigo=' + Geral.FF0(Codigo),
      'AND AnoRef=' + AnoPeriodoAnt(AnoRef, MesRef),
      'AND MesRef=' + MesPeriodoAnt(MesRef),
      '']);
      //
      EdMedOpeLeiOblig.ValueVariant := QrOprConsMesMedOpeLeiOblig.Value;
      GetMedOpeValConsu();
      EdMedValRefCityL.ValueVariant  := QrOprConsMesMedValRefCityL.Value;
      EdMedValPrcXpectN.ValueVariant := QrOprConsMesMedValPrcXpectN.Value;
      EdIncrsTaxPercnt.ValueVariant  := QrOprConsMesIncrsTaxPercnt.Value;
    end;
    //
  end;
  PnLeituras.Visible := CkDataDefinida.Checked;
end;

procedure TFmOprConsMes.QrGraGruYBeforeClose(
  DataSet: TDataSet);
begin
  QrOprConsMesIts.Close;
end;

procedure TFmOprConsMes.QrGraGruYBeforeOpen(DataSet: TDataSet);
begin
//  QrOprConsMesCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmOprConsMes.QrOprConsMesAfterScroll(DataSet: TDataSet);
begin
  ReopenOprConsMesIts(0);
  ReopenOprConsMesFin(0);
end;

procedure TFmOprConsMes.QrOprConsMesBeforeClose(DataSet: TDataSet);
begin
  QrOprConsMesFin.Close;
  QrOprConsMesIts.Close;
end;

procedure TFmOprConsMes.QrOprConsMesItsCalcFields(DataSet: TDataSet);
begin
  QrOprConsMesItsNO_Kind.Value := AppEnums.NomeKindLeituraKWH(
    TKindLeituraKWH(QrOprConsMesItsKind.Value));
end;

{

var
  DtaLeitura, WacProdUnit: String;
  Codigo, AnoRef, MesRef: Integer;
  WacProdSumLei, WacProdSumMes, MedOpeLeiInjet, MedOpeLeiConsu, MedOpeLeiRetrn, MedOpeLeiOblig, MedOpeLeiSaldo, MedOpeLeiAcuml, MedOpeLeiTtUsd, MedOpeValInjet, MedOpeValConsu, MedValSumInjet, MedValSumConsN, MedValSumConsX, MedValSumConsu, MedValSumCityL, MedValRefCityL, MedValSumNoComp, MedValSumFatura, MedValPrcXpectN, MedValPrcConsN, TotKWhCosXpectd, TotKWhCosObtned, TotKWhCostSaved, TotCtyCostSaved, TotAllCostSaved,
  TotAllCostOther, TotAllSaveOther, IncrsTaxPercnt, InvestValBased, InvestValTaxInc, InvestValUpdtd, TotSavValBased, TotSavValTaxInc, TotSavValUpdtd, RateInvtXSaved: Double;
  SQLType: TSQLType;
begin
  SQLType             := ImgTipo.SQLType?;
  //
  Codigo              := ;
  AnoRef              := ;
  MesRef              := ;
  DtaLeitura          := ;
  WacProdUnit         := ;
  WacProdSumLei       := ;
  WacProdSumMes       := ;
  MedOpeLeiInjet      := ;
  MedOpeLeiConsu      := ;
  MedOpeLeiRetrn      := ;
  MedOpeLeiOblig      := ;
  MedOpeLeiSaldo      := ;
  MedOpeLeiAcuml      := ;
  MedOpeLeiTtUsd      := ;
  MedOpeValInjet      := ;
  MedOpeValConsu      := ;
  MedValSumInjet      := ;
  MedValSumConsN      := ;
  MedValSumConsX      := ;
  MedValSumConsu      := ;
  MedValSumCityL      := ;
  MedValRefCityL      := ;
  MedValSumNoComp     := ;
  MedValSumFatura     := ;
  MedValPrcXpectN     := ;
  MedValPrcConsN      := ;
  TotKWhCosXpectd     := ;
  TotKWhCosObtned     := ;
  TotKWhCostSaved     := ;
  TotCtyCostSaved     := ;
  TotAllCostSaved     := ;
  TotAllCostOther     := ;
  TotAllSaveOther     := ;
  IncrsTaxPercnt      := ;
  InvestValBased      := ;
  InvestValTaxInc     := ;
  InvestValUpdtd      := ;
  TotSavValBased      := ;
  TotSavValTaxInc     := ;
  TotSavValUpdtd      := ;
  RateInvtXSaved      := ;

  //
? := Grl_DmkDB.GetNxtCodigoInt(Dmod.QrAux, Dmod.MyDB, 'oprconsmes', 'Codigo', SQLType, CodAtual?);
ou > ? := UMyMod.BPGS1I32('oprconsmes', 'Codigo', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, 'oprconsmes', auto_increment?[
'AnoRef', 'MesRef', 'DtaLeitura',
'WacProdUnit', 'WacProdSumLei', 'WacProdSumMes',
'MedOpeLeiInjet', 'MedOpeLeiConsu', 'MedOpeLeiRetrn',
'MedOpeLeiOblig', 'MedOpeLeiSaldo', 'MedOpeLeiAcuml',
'MedOpeLeiTtUsd', 'MedOpeValInjet', 'MedOpeValConsu',
'MedValSumInjet', 'MedValSumConsN', 'MedValSumConsX',
'MedValSumConsu', 'MedValSumCityL', 'MedValRefCityL',
'MedValSumNoComp', 'MedValSumFatura', 'MedValPrcXpectN',
'MedValPrcConsN', 'TotKWhCosXpectd', 'TotKWhCosObtned',
'TotKWhCostSaved', 'TotCtyCostSaved', 'TotAllCostSaved',
'TotAllCostOther', 'TotAllSaveOther', 'IncrsTaxPercnt',
'InvestValBased', 'InvestValTaxInc', 'InvestValUpdtd',
'TotSavValBased', 'TotSavValTaxInc', 'TotSavValUpdtd',
'RateInvtXSaved'], [
'Codigo'], [
AnoRef, MesRef, DtaLeitura,
WacProdUnit, WacProdSumLei, WacProdSumMes,
MedOpeLeiInjet, MedOpeLeiConsu, MedOpeLeiRetrn,
MedOpeLeiOblig, MedOpeLeiSaldo, MedOpeLeiAcuml,
MedOpeLeiTtUsd, MedOpeValInjet, MedOpeValConsu,
MedValSumInjet, MedValSumConsN, MedValSumConsX,
MedValSumConsu, MedValSumCityL, MedValRefCityL,
MedValSumNoComp, MedValSumFatura, MedValPrcXpectN,
MedValPrcConsN, TotKWhCosXpectd, TotKWhCosObtned,
TotKWhCostSaved, TotCtyCostSaved, TotAllCostSaved,
TotAllCostOther, TotAllSaveOther, IncrsTaxPercnt,
InvestValBased, InvestValTaxInc, InvestValUpdtd,
TotSavValBased, TotSavValTaxInc, TotSavValUpdtd,
RateInvtXSaved], [
Codigo], UserDataAlterweb?, IGNORE?, ComplUpd?: String; Device: TDeviceType.st??, Sincro?False?
}

end.

