object FmWacProdGet2: TFmWacProdGet2
  Left = 339
  Top = 185
  Caption = 'WAC-PRODU-001 :: Produ'#231#227'o Total Real WAC'
  ClientHeight = 713
  ClientWidth = 970
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 970
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 922
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 874
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 317
        Height = 32
        Caption = 'Produ'#231#227'o Total Real WAC'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 317
        Height = 32
        Caption = 'Produ'#231#227'o Total Real WAC'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 317
        Height = 32
        Caption = 'Produ'#231#227'o Total Real WAC'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 568
    Width = 970
    Height = 75
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    Visible = False
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 966
      Height = 58
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 24
        Width = 966
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
      object PB2: TProgressBar
        Left = 0
        Top = 41
        Width = 966
        Height = 17
        Align = alBottom
        TabOrder = 1
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 643
    Width = 970
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 824
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 822
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 656
        Top = 24
        Width = 32
        Height = 13
        Caption = 'Label1'
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object AGBSecs: TAdvGlowButton
        Left = 148
        Top = 4
        Width = 70
        Height = 41
        Caption = 'Dia'
        Default = True
        NotesFont.Charset = DEFAULT_CHARSET
        NotesFont.Color = clWindowText
        NotesFont.Height = -11
        NotesFont.Name = 'Tahoma'
        NotesFont.Style = []
        TabOrder = 1
        OnClick = AGBSecsClick
        Appearance.ColorChecked = 16111818
        Appearance.ColorCheckedTo = 16367008
        Appearance.ColorDisabled = 15921906
        Appearance.ColorDisabledTo = 15921906
        Appearance.ColorDown = 16111818
        Appearance.ColorDownTo = 16367008
        Appearance.ColorHot = 16117985
        Appearance.ColorHotTo = 16372402
        Appearance.ColorMirrorHot = 16107693
        Appearance.ColorMirrorHotTo = 16775412
        Appearance.ColorMirrorDown = 16102556
        Appearance.ColorMirrorDownTo = 16768988
        Appearance.ColorMirrorChecked = 16102556
        Appearance.ColorMirrorCheckedTo = 16768988
        Appearance.ColorMirrorDisabled = 11974326
        Appearance.ColorMirrorDisabledTo = 15921906
        GroupIndex = 1
      end
      object AGBDays: TAdvGlowButton
        Left = 220
        Top = 4
        Width = 70
        Height = 41
        Caption = 'M'#234's'
        Default = True
        NotesFont.Charset = DEFAULT_CHARSET
        NotesFont.Color = clWindowText
        NotesFont.Height = -11
        NotesFont.Name = 'Tahoma'
        NotesFont.Style = []
        TabOrder = 2
        OnClick = AGBDaysClick
        Appearance.ColorChecked = 16111818
        Appearance.ColorCheckedTo = 16367008
        Appearance.ColorDisabled = 15921906
        Appearance.ColorDisabledTo = 15921906
        Appearance.ColorDown = 16111818
        Appearance.ColorDownTo = 16367008
        Appearance.ColorHot = 16117985
        Appearance.ColorHotTo = 16372402
        Appearance.ColorMirrorHot = 16107693
        Appearance.ColorMirrorHotTo = 16775412
        Appearance.ColorMirrorDown = 16102556
        Appearance.ColorMirrorDownTo = 16768988
        Appearance.ColorMirrorChecked = 16102556
        Appearance.ColorMirrorCheckedTo = 16768988
        Appearance.ColorMirrorDisabled = 11974326
        Appearance.ColorMirrorDisabledTo = 15921906
        GroupIndex = 1
      end
      object AGBMnth: TAdvGlowButton
        Left = 292
        Top = 4
        Width = 70
        Height = 41
        Caption = 'Ano'
        Default = True
        NotesFont.Charset = DEFAULT_CHARSET
        NotesFont.Color = clWindowText
        NotesFont.Height = -11
        NotesFont.Name = 'Tahoma'
        NotesFont.Style = []
        TabOrder = 3
        OnClick = AGBMnthClick
        Appearance.ColorChecked = 16111818
        Appearance.ColorCheckedTo = 16367008
        Appearance.ColorDisabled = 15921906
        Appearance.ColorDisabledTo = 15921906
        Appearance.ColorDown = 16111818
        Appearance.ColorDownTo = 16367008
        Appearance.ColorHot = 16117985
        Appearance.ColorHotTo = 16372402
        Appearance.ColorMirrorHot = 16107693
        Appearance.ColorMirrorHotTo = 16775412
        Appearance.ColorMirrorDown = 16102556
        Appearance.ColorMirrorDownTo = 16768988
        Appearance.ColorMirrorChecked = 16102556
        Appearance.ColorMirrorCheckedTo = 16768988
        Appearance.ColorMirrorDisabled = 11974326
        Appearance.ColorMirrorDisabledTo = 15921906
        GroupIndex = 1
      end
      object AGBYers: TAdvGlowButton
        Left = 364
        Top = 4
        Width = 70
        Height = 41
        Caption = 'Total'
        Default = True
        NotesFont.Charset = DEFAULT_CHARSET
        NotesFont.Color = clWindowText
        NotesFont.Height = -11
        NotesFont.Name = 'Tahoma'
        NotesFont.Style = []
        TabOrder = 4
        Appearance.ColorChecked = 16111818
        Appearance.ColorCheckedTo = 16367008
        Appearance.ColorDisabled = 15921906
        Appearance.ColorDisabledTo = 15921906
        Appearance.ColorDown = 16111818
        Appearance.ColorDownTo = 16367008
        Appearance.ColorHot = 16117985
        Appearance.ColorHotTo = 16372402
        Appearance.ColorMirrorHot = 16107693
        Appearance.ColorMirrorHotTo = 16775412
        Appearance.ColorMirrorDown = 16102556
        Appearance.ColorMirrorDownTo = 16768988
        Appearance.ColorMirrorChecked = 16102556
        Appearance.ColorMirrorCheckedTo = 16768988
        Appearance.ColorMirrorDisabled = 11974326
        Appearance.ColorMirrorDisabledTo = 15921906
        GroupIndex = 1
      end
      object AGBPeriodo: TAdvGlowButton
        Left = 456
        Top = 4
        Width = 70
        Height = 41
        Caption = 'Per'#237'odo'
        Default = True
        NotesFont.Charset = DEFAULT_CHARSET
        NotesFont.Color = clWindowText
        NotesFont.Height = -11
        NotesFont.Name = 'Tahoma'
        NotesFont.Style = []
        TabOrder = 5
        Appearance.ColorChecked = 16111818
        Appearance.ColorCheckedTo = 16367008
        Appearance.ColorDisabled = 15921906
        Appearance.ColorDisabledTo = 15921906
        Appearance.ColorDown = 16111818
        Appearance.ColorDownTo = 16367008
        Appearance.ColorHot = 16117985
        Appearance.ColorHotTo = 16372402
        Appearance.ColorMirrorHot = 16107693
        Appearance.ColorMirrorHotTo = 16775412
        Appearance.ColorMirrorDown = 16102556
        Appearance.ColorMirrorDownTo = 16768988
        Appearance.ColorMirrorChecked = 16102556
        Appearance.ColorMirrorCheckedTo = 16768988
        Appearance.ColorMirrorDisabled = 11974326
        Appearance.ColorMirrorDisabledTo = 15921906
        GroupIndex = 2
      end
      object AdvGlowButton1: TAdvGlowButton
        Left = 528
        Top = 4
        Width = 70
        Height = 41
        Caption = 'Momento'
        Default = True
        NotesFont.Charset = DEFAULT_CHARSET
        NotesFont.Color = clWindowText
        NotesFont.Height = -11
        NotesFont.Name = 'Tahoma'
        NotesFont.Style = []
        TabOrder = 6
        Appearance.ColorChecked = 16111818
        Appearance.ColorCheckedTo = 16367008
        Appearance.ColorDisabled = 15921906
        Appearance.ColorDisabledTo = 15921906
        Appearance.ColorDown = 16111818
        Appearance.ColorDownTo = 16367008
        Appearance.ColorHot = 16117985
        Appearance.ColorHotTo = 16372402
        Appearance.ColorMirrorHot = 16107693
        Appearance.ColorMirrorHotTo = 16775412
        Appearance.ColorMirrorDown = 16102556
        Appearance.ColorMirrorDownTo = 16768988
        Appearance.ColorMirrorChecked = 16102556
        Appearance.ColorMirrorCheckedTo = 16768988
        Appearance.ColorMirrorDisabled = 11974326
        Appearance.ColorMirrorDisabledTo = 15921906
        GroupIndex = 2
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 970
    Height = 520
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 970
      Height = 520
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 970
        Height = 520
        Align = alClient
        Caption = 'vobject frxPreview1: TfrxPreview'
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 307
          Height = 503
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object Panel6: TPanel
            Left = 0
            Top = 0
            Width = 145
            Height = 503
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object Splitter1: TSplitter
              Left = 0
              Top = 81
              Width = 145
              Height = 5
              Cursor = crVSplit
              Align = alTop
              ExplicitWidth = 200
            end
            object Splitter2: TSplitter
              Left = 0
              Top = 331
              Width = 145
              Height = 5
              Cursor = crVSplit
              Align = alTop
              ExplicitWidth = 200
            end
            object dmkDBGridZTO1: TdmkDBGridZTO
              Left = 0
              Top = 0
              Width = 145
              Height = 81
              Align = alTop
              DataSource = DsAnos
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              TabOrder = 0
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'ANO'
                  Title.Caption = 'Ano'
                  Width = 36
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Quantia'
                  Title.Caption = 'kWh'
                  Visible = True
                end>
            end
            object dmkDBGridZTO2: TdmkDBGridZTO
              Left = 0
              Top = 86
              Width = 145
              Height = 245
              Align = alTop
              DataSource = DsMeses
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              TabOrder = 1
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'MES'
                  Title.Caption = 'M'#234's'
                  Width = 36
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Quantia'
                  Title.Caption = 'kWh'
                  Visible = True
                end>
            end
            object DBGDias: TdmkDBGridZTO
              Left = 0
              Top = 336
              Width = 145
              Height = 167
              Align = alClient
              DataSource = DsDias
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              TabOrder = 2
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'DIA'
                  Title.Caption = 'Dia'
                  Width = 36
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Quantia'
                  Title.Caption = 'kWh'
                  Visible = True
                end>
            end
          end
          object Panel7: TPanel
            Left = 145
            Top = 0
            Width = 162
            Height = 503
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 1
            object LaData: TLabel
              Left = 0
              Top = 0
              Width = 162
              Height = 19
              Align = alTop
              Alignment = taCenter
              Caption = '30/05/2018'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Calibri'
              Font.Style = []
              ParentFont = False
              ExplicitWidth = 76
            end
            object DBGInstante: TdmkDBGridZTO
              Left = 0
              Top = 19
              Width = 162
              Height = 484
              Align = alClient
              DataSource = DsInstantes
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              TabOrder = 0
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Hora'
                  Width = 53
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Quantia'
                  Title.Caption = 'Wh'
                  Visible = True
                end>
            end
          end
        end
        object PnFrx: TPanel
          Left = 309
          Top = 15
          Width = 659
          Height = 503
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object LBAvisos: TListBox
            Left = 0
            Top = 0
            Width = 659
            Height = 97
            Align = alTop
            ItemHeight = 13
            TabOrder = 0
            Visible = False
          end
          object Chart1: TChart
            Left = 16
            Top = 124
            Width = 659
            Height = 413
            BackWall.Color = clPurple
            BackWall.Pen.Color = clRed
            Border.Color = 13475719
            Border.Mode = pmMaskPenNot
            Title.Text.Strings = (
              'TChart')
            OnClickSeries = Chart1ClickSeries
            Frame.Color = clRed
            View3D = False
            TabOrder = 1
            OnMouseMove = Chart1MouseMove
            ColorPaletteIndex = 13
            object Series1: TBarSeries
              Marks.Arrow.Visible = True
              Marks.Callout.Brush.Color = clBlack
              Marks.Callout.Arrow.Visible = True
              Marks.Clip = True
              Marks.Style = smsPercentRelative
              Marks.Visible = False
              XValues.Name = 'X'
              XValues.Order = loAscending
              YValues.Name = 'Bar'
              YValues.Order = loNone
            end
          end
        end
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 171
  end
  object QrAnos: TZQuery
    Connection = DMod.MyDB
    BeforeClose = QrAnosBeforeClose
    AfterScroll = QrAnosAfterScroll
    SQL.Strings = (
      'SELECT strftime('#39'%Y'#39', ProdDta) ANO,'
      'SUM(Quantia)/1000 Quantia'
      'FROM wacprodvalues'
      'GROUP BY ANO'
      'ORDER BY ANO desc')
    Params = <>
    Left = 160
    Top = 204
    object QrAnosQuantia: TFloatField
      FieldName = 'Quantia'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
    end
    object QrAnosANO: TLargeintField
      FieldName = 'ANO'
    end
  end
  object QrMeses: TZQuery
    Connection = DMod.MyDB
    BeforeClose = QrMesesBeforeClose
    AfterScroll = QrMesesAfterScroll
    SQL.Strings = (
      'SELECT strftime('#39'%Y'#39', ProdDta) ANO,'
      'SUM(Quantia)/1000 Quantia'
      'FROM wacprodvalues'
      'GROUP BY ANO'
      'ORDER BY ANO desc')
    Params = <>
    Left = 228
    Top = 204
    object QrMesesQuantia: TFloatField
      FieldName = 'Quantia'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
    end
    object QrMesesMES: TLargeintField
      FieldName = 'MES'
    end
  end
  object DsAnos: TDataSource
    DataSet = QrAnos
    Left = 160
    Top = 252
  end
  object DsMeses: TDataSource
    DataSet = QrMeses
    Left = 228
    Top = 252
  end
  object QrInstantes: TZQuery
    Connection = DMod.MyDB
    SQL.Strings = (
      'SELECT strftime('#39'%Y'#39', ProdDta) ANO,'
      'SUM(Quantia)/1000 Quantia'
      'FROM wacprodvalues'
      'GROUP BY ANO'
      'ORDER BY ANO desc')
    Params = <>
    Left = 228
    Top = 304
    object QrInstantesQuantia: TFloatField
      FieldName = 'Quantia'
      ReadOnly = True
      DisplayFormat = '#,##0.000'
    end
    object QrInstantesHora: TWideMemoField
      FieldName = 'Hora'
      OnGetText = QrInstantesHoraGetText
      BlobType = ftWideMemo
      Size = 5
    end
  end
  object QrDias: TZQuery
    Connection = DMod.MyDB
    BeforeClose = QrDiasBeforeClose
    AfterScroll = QrDiasAfterScroll
    SQL.Strings = (
      'SELECT strftime('#39'%Y'#39', ProdDta) ANO,'
      'SUM(Quantia)/1000 Quantia'
      'FROM wacprodvalues'
      'GROUP BY ANO'
      'ORDER BY ANO desc')
    Params = <>
    Left = 160
    Top = 304
    object QrDiasQuantia: TFloatField
      FieldName = 'Quantia'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
    end
    object QrDiasDIA: TLargeintField
      FieldName = 'DIA'
    end
  end
  object DsDias: TDataSource
    DataSet = QrDias
    Left = 160
    Top = 352
  end
  object DsInstantes: TDataSource
    DataSet = QrInstantes
    Left = 228
    Top = 352
  end
  object QrTotal: TZQuery
    Connection = DMod.MyDB
    AfterOpen = QrTotalAfterOpen
    BeforeClose = QrTotalBeforeClose
    AfterScroll = QrTotalAfterScroll
    SQL.Strings = (
      'SELECT SUM(Quantia) Quantia'
      'FROM wacprodvalues ')
    Params = <>
    Left = 160
    Top = 400
  end
  object QrChart: TZQuery
    Params = <>
    Left = 184
    Top = 468
  end
  object TimerOP: TTimer
    Enabled = False
    Interval = 5000
    OnTimer = TimerOPTimer
    Left = 396
    Top = 88
  end
  object QrMax: TZQuery
    Params = <>
    Left = 468
    Top = 116
  end
end
