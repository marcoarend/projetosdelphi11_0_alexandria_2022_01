object FmOprConsMes: TFmOprConsMes
  Left = 368
  Top = 194
  Caption = 'WAC-PRODU-002 :: Leituras de Consumo'
  ClientHeight = 542
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 446
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 0
      Top = 207
      Width = 1008
      Height = 5
      Cursor = crVSplit
      Align = alBottom
      ExplicitTop = 209
    end
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 65
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 76
        Top = 32
        Width = 689
        Height = 21
        Color = clWhite
        DataField = 'Nome'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 382
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 485
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Per'#237'odo'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Conta '
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
        object BtFin: TBitBtn
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Finan'#231'as'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtFinClick
        end
      end
    end
    object DGDados: TdmkDBGridZTO
      Left = 0
      Top = 65
      Width = 1008
      Height = 76
      Align = alTop
      DataSource = DsOprConsMes
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      RowColors = <>
      Columns = <
        item
          Expanded = False
          FieldName = 'AnoRef'
          Title.Caption = 'Ano'
          Width = 32
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MesRef'
          Title.Caption = 'M'#234's'
          Width = 32
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TotAllCostSaved'
          Title.Caption = '$ econ. geral m'#234's'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'InvestValTotal'
          Title.Caption = '$ invest. atualiz.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SavedValTotal'
          Title.Caption = '$ toal econom. atualiz.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RateSpntXSaved'
          Title.Caption = 'Fator invest. x economiz.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DtaLeitura'
          Title.Caption = 'Data/hora Leitura'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MedOpeLeiConsu'
          Title.Caption = 'Leitura consumo'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MedOpeLeiInjet'
          Title.Caption = 'Leitura Injetado'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MedOpeValConsu'
          Title.Caption = 'Consumido'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MedOpeValInjet'
          Title.Caption = 'Injetado'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MedValRefCityL'
          Title.Caption = 'Ref. $ ilum.p'#250'blica'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MedValSumCityL'
          Title.Caption = '$ ilumum.p'#250'blica'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MedValSumConsu'
          Title.Caption = '$ consumido'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MedValSumInjet'
          Title.Caption = '$ Injetado'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MedValSumNoComp'
          Title.Caption = '$ n'#227'o comp'#245'e'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'WacProdSumLei'
          Title.Caption = 'WAC total'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'WacProdSumMes'
          Title.Caption = 'WAC m'#234's'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'WacProdUnit'
          Title.Caption = 'WAC unidade'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MedOpeLeiRetrn'
          Title.Caption = 'kWh N'#227'o injetado'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MedOpeLeiSaldo'
          Title.Caption = 'kWh Saldo m'#234's'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MedOpeLeiAcuml'
          Title.Caption = 'kWh acumulado'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MedOpeLeiTtUsd'
          Title.Caption = 'kWh total consumido'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MedValSumFatura'
          Title.Caption = 'Valor fatura'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MedValSumConsN'
          Title.Caption = '$ custo energ obrigat'#243'rio'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MedValSumConsX'
          Title.Caption = '$ custo energ extra'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MedValPrcXpectN'
          Title.Caption = '$ kWh obrigat'#243'rio'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MedValPrcConsN'
          Title.Caption = '$ kWh extra'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TotKWhCosXpectd'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TotKWhCosObtned'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TotKWhCostSaved'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TotCtyCostSaved'
          Visible = True
        end
        item
          Expanded = False
          Visible = True
        end
        item
          Expanded = False
          Visible = True
        end
        item
          Expanded = False
          Visible = True
        end
        item
          Expanded = False
          Visible = True
        end
        item
          Expanded = False
          Visible = True
        end
        item
          Expanded = False
          Visible = True
        end
        item
          Expanded = False
          Visible = True
        end
        item
          Expanded = False
          Visible = True
        end
        item
          Expanded = False
          Visible = True
        end
        item
          Expanded = False
          Visible = True
        end>
    end
    object Panel6: TPanel
      Left = 0
      Top = 212
      Width = 1008
      Height = 170
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 3
      object dmkDBGridZTO1: TdmkDBGridZTO
        Left = 0
        Top = 0
        Width = 481
        Height = 170
        Align = alLeft
        DataSource = DsOprConsMesIts
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        RowColors = <>
        Columns = <
          item
            Expanded = False
            FieldName = 'Controle'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Kwh'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Preco'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Valor'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_Kind'
            Title.Caption = 'Tipo'
            Visible = True
          end>
      end
      object dmkDBGridZTO2: TdmkDBGridZTO
        Left = 481
        Top = 0
        Width = 527
        Height = 170
        Align = alClient
        DataSource = DsOprConsMesFin
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        RowColors = <>
        Columns = <
          item
            Expanded = False
            FieldName = 'ProdDta'
            Title.Caption = 'Data'
            Width = 88
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValrDebi'
            Title.Caption = 'Despesa'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValrCred'
            Title.Caption = 'Receita'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValrInvt'
            Title.Caption = 'Investimento'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Title.Caption = 'Descri'#231#227'o'
            Visible = True
          end>
      end
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 446
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 269
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object LaMes: TLabel
        Left = 76
        Top = 16
        Width = 23
        Height = 13
        Caption = 'M'#234's:'
      end
      object LaAno: TLabel
        Left = 268
        Top = 16
        Width = 22
        Height = 13
        Caption = 'Ano:'
      end
      object Label53: TLabel
        Left = 364
        Top = 16
        Width = 150
        Height = 13
        Caption = 'Data / hora leitura do consumo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label11: TLabel
        Left = 8
        Top = 240
        Width = 521
        Height = 13
        Caption = 
          '$ ilumina'#231#227'o publica base*: Valor que seria cobrado se n'#227'o houve' +
          'ssa gera'#231#227'o fotovoltaica.'
        Color = clRed
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object CBMesRef: TComboBox
        Left = 76
        Top = 33
        Width = 182
        Height = 21
        Color = clWhite
        DropDownCount = 12
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 7622183
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        Text = 'CBMesRef'
      end
      object CBAnoRef: TComboBox
        Left = 268
        Top = 33
        Width = 90
        Height = 21
        Color = clWhite
        DropDownCount = 3
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 7622183
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
        Text = 'CBAnoRef'
      end
      object TPData: TdmkEditDateTimePicker
        Left = 364
        Top = 32
        Width = 108
        Height = 21
        Date = 0.639644131944805900
        Time = 0.639644131944805900
        TabOrder = 3
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtHrAberto'
        UpdCampo = 'DtHrAberto'
        UpdType = utYes
        DatePurpose = dmkdpGeneric
      end
      object EdHora: TdmkEdit
        Left = 476
        Top = 32
        Width = 40
        Height = 21
        TabOrder = 4
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryName = 'QrVSGerArt'
        QryCampo = 'DtHrAberto'
        UpdCampo = 'DtHrAberto'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object GroupBox1: TGroupBox
        Left = 8
        Top = 56
        Width = 993
        Height = 177
        Caption = 
          ' Informe os dados abaixo somente ap'#243's definir a data da leitura!' +
          '! : '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 6
        object PnLeituras: TPanel
          Left = 2
          Top = 15
          Width = 989
          Height = 160
          Align = alClient
          BevelOuter = bvNone
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentBackground = False
          ParentFont = False
          TabOrder = 0
          Visible = False
          object Label3: TLabel
            Left = 12
            Top = 12
            Width = 181
            Height = 13
            Caption = 'kWh total gerado pelo sistema pr'#243'prio:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label4: TLabel
            Left = 12
            Top = 84
            Width = 165
            Height = 13
            Caption = 'Leitura dos kWh injetados na rede:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label5: TLabel
            Left = 12
            Top = 108
            Width = 179
            Height = 13
            Caption = 'Leitura dos kWh consumidos da rede:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label9: TLabel
            Left = 12
            Top = 36
            Width = 195
            Height = 13
            Caption = 'kWh gerado pelo sistema pr'#243'prio no m'#234's:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label6: TLabel
            Left = 12
            Top = 60
            Width = 147
            Height = 13
            Caption = 'kWh injetados na rede no m'#234's:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label8: TLabel
            Left = 368
            Top = 12
            Width = 161
            Height = 13
            Caption = 'kWh consumidos da rede no m'#234's:'
            Color = clBtnFace
            ParentColor = False
          end
          object SbWacProdSumLei: TSpeedButton
            Left = 336
            Top = 8
            Width = 23
            Height = 22
            Caption = '?'
            OnClick = SbWacProdSumLeiClick
          end
          object Label10: TLabel
            Left = 728
            Top = 12
            Width = 129
            Height = 13
            Caption = '$ ilumina'#231#227'o publica base*:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label12: TLabel
            Left = 368
            Top = 36
            Width = 199
            Height = 13
            Caption = 'kWh sistema pr'#243'prio n'#227'o injetado na rede:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label13: TLabel
            Left = 368
            Top = 60
            Width = 179
            Height = 13
            Caption = 'kWh saldo do sistema pr'#243'prio no m'#234's:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label14: TLabel
            Left = 368
            Top = 84
            Width = 206
            Height = 13
            Caption = 'kWh acumulado do sistema pr'#243'prio no m'#234's:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label15: TLabel
            Left = 368
            Top = 108
            Width = 145
            Height = 13
            Caption = 'kWh total consumidos no m'#234's:'
            Color = clBtnFace
            ParentColor = False
          end
          object SbMedOpeLeiInjet: TSpeedButton
            Left = 336
            Top = 80
            Width = 23
            Height = 22
            Caption = '?'
            OnClick = SbMedOpeLeiInjetClick
          end
          object SbMedOpeValInjet: TSpeedButton
            Left = 336
            Top = 56
            Width = 23
            Height = 22
            Caption = '?'
            OnClick = SbMedOpeValInjetClick
          end
          object SbMedOpeValConsu: TSpeedButton
            Left = 692
            Top = 8
            Width = 23
            Height = 22
            Caption = '?'
            OnClick = SbMedOpeValConsuClick
          end
          object Label16: TLabel
            Left = 12
            Top = 132
            Width = 185
            Height = 13
            Caption = 'kWh obrigat'#243'rios da rede (100, 50, 30):'
            Color = clBtnFace
            ParentColor = False
          end
          object Label17: TLabel
            Left = 708
            Top = 36
            Width = 149
            Height = 13
            Caption = '$  base do kWh da operadora*:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label18: TLabel
            Left = 708
            Top = 60
            Width = 117
            Height = 13
            Caption = '% juro mensal [F4] anual:'
            Color = clBtnFace
            ParentColor = False
          end
          object EdMedOpeLeiConsu: TdmkEdit
            Left = 224
            Top = 104
            Width = 112
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdMedOpeLeiInjet: TdmkEdit
            Left = 224
            Top = 80
            Width = 112
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdWacProdSumLei: TdmkEdit
            Left = 224
            Top = 8
            Width = 112
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdMedOpeValConsu: TdmkEdit
            Left = 580
            Top = 8
            Width = 112
            Height = 21
            Alignment = taRightJustify
            TabOrder = 6
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnChange = EdMedOpeValConsuChange
          end
          object EdMedOpeValInjet: TdmkEdit
            Left = 224
            Top = 56
            Width = 112
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnChange = EdMedOpeValInjetChange
          end
          object EdWacProdSumMes: TdmkEdit
            Left = 224
            Top = 32
            Width = 112
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnChange = EdWacProdSumMesChange
          end
          object EdMedValRefCityL: TdmkEdit
            Left = 864
            Top = 8
            Width = 112
            Height = 21
            Alignment = taRightJustify
            TabOrder = 11
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdMedOpeLeiRetrn: TdmkEdit
            Left = 580
            Top = 32
            Width = 112
            Height = 21
            Alignment = taRightJustify
            TabOrder = 7
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnChange = EdMedOpeLeiRetrnChange
          end
          object EdMedOpeLeiSaldo: TdmkEdit
            Left = 580
            Top = 56
            Width = 112
            Height = 21
            Alignment = taRightJustify
            TabOrder = 8
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdMedOpeLeiAcuml: TdmkEdit
            Left = 580
            Top = 80
            Width = 112
            Height = 21
            Alignment = taRightJustify
            TabOrder = 9
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdMedOpeLeiTtUsd: TdmkEdit
            Left = 580
            Top = 104
            Width = 112
            Height = 21
            Alignment = taRightJustify
            TabOrder = 10
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdMedOpeLeiOblig: TdmkEdit
            Left = 224
            Top = 128
            Width = 112
            Height = 21
            Alignment = taRightJustify
            TabOrder = 5
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnChange = EdMedOpeLeiObligChange
          end
          object EdMedValPrcXpectN: TdmkEdit
            Left = 864
            Top = 32
            Width = 112
            Height = 21
            Alignment = taRightJustify
            TabOrder = 12
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 10
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000000000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdIncrsTaxPercnt: TdmkEdit
            Left = 864
            Top = 56
            Width = 112
            Height = 21
            Alignment = taRightJustify
            TabOrder = 13
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 10
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000000000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnKeyDown = EdIncrsTaxPercntKeyDown
          end
        end
      end
      object CkDataDefinida: TCheckBox
        Left = 520
        Top = 36
        Width = 97
        Height = 17
        Caption = 'Data definida?'
        TabOrder = 5
        OnClick = CkDataDefinidaClick
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 383
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 259
        Height = 32
        Caption = 'Leituras de Consumo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 259
        Height = 32
        Caption = 'Leituras de Consumo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 259
        Height = 32
        Caption = 'Leituras de Consumo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 664
    Top = 448
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 536
    Top = 448
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object QrOprConsMes: TZQuery
    Connection = DMod.MyDB
    BeforeClose = QrOprConsMesBeforeClose
    AfterScroll = QrOprConsMesAfterScroll
    SQL.Strings = (
      'SELECT * FROM oprconsmes')
    Params = <>
    Left = 112
    Top = 308
    object QrOprConsMesCodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrOprConsMesAnoRef: TLargeintField
      FieldName = 'AnoRef'
      Required = True
    end
    object QrOprConsMesMesRef: TSmallintField
      FieldName = 'MesRef'
      Required = True
    end
    object QrOprConsMesDtaLeitura: TDateTimeField
      FieldName = 'DtaLeitura'
      Required = True
    end
    object QrOprConsMesWacProdUnit: TWideStringField
      FieldName = 'WacProdUnit'
      Size = 15
    end
    object QrOprConsMesWacProdSumLei: TFloatField
      FieldName = 'WacProdSumLei'
    end
    object QrOprConsMesWacProdSumMes: TFloatField
      FieldName = 'WacProdSumMes'
    end
    object QrOprConsMesMedOpeLeiInjet: TFloatField
      FieldName = 'MedOpeLeiInjet'
    end
    object QrOprConsMesMedOpeLeiConsu: TFloatField
      FieldName = 'MedOpeLeiConsu'
    end
    object QrOprConsMesMedOpeValInjet: TFloatField
      FieldName = 'MedOpeValInjet'
    end
    object QrOprConsMesMedOpeValConsu: TFloatField
      FieldName = 'MedOpeValConsu'
    end
    object QrOprConsMesMedValSumInjet: TFloatField
      FieldName = 'MedValSumInjet'
    end
    object QrOprConsMesMedValSumConsu: TFloatField
      FieldName = 'MedValSumConsu'
    end
    object QrOprConsMesMedValSumCityL: TFloatField
      FieldName = 'MedValSumCityL'
    end
    object QrOprConsMesMedValRefCityL: TFloatField
      FieldName = 'MedValRefCityL'
    end
    object QrOprConsMesMedValSumNoComp: TFloatField
      FieldName = 'MedValSumNoComp'
    end
    object QrOprConsMesLk: TLargeintField
      FieldName = 'Lk'
    end
    object QrOprConsMesDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOprConsMesDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOprConsMesUserCad: TLargeintField
      FieldName = 'UserCad'
    end
    object QrOprConsMesUserAlt: TLargeintField
      FieldName = 'UserAlt'
    end
    object QrOprConsMesAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrOprConsMesAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrOprConsMesMedOpeLeiRetrn: TFloatField
      FieldName = 'MedOpeLeiRetrn'
    end
    object QrOprConsMesMedOpeLeiSaldo: TFloatField
      FieldName = 'MedOpeLeiSaldo'
    end
    object QrOprConsMesMedOpeLeiAcuml: TFloatField
      FieldName = 'MedOpeLeiAcuml'
    end
    object QrOprConsMesMedOpeLeiTtUsd: TFloatField
      FieldName = 'MedOpeLeiTtUsd'
    end
    object QrOprConsMesMedValSumFatura: TFloatField
      FieldName = 'MedValSumFatura'
    end
    object QrOprConsMesMedOpeLeiOblig: TFloatField
      FieldName = 'MedOpeLeiOblig'
    end
    object QrOprConsMesMedValSumConsN: TFloatField
      FieldName = 'MedValSumConsN'
      ReadOnly = True
    end
    object QrOprConsMesMedValSumConsX: TFloatField
      FieldName = 'MedValSumConsX'
      ReadOnly = True
    end
    object QrOprConsMesMedValPrcXpectN: TFloatField
      FieldName = 'MedValPrcXpectN'
      ReadOnly = True
    end
    object QrOprConsMesMedValPrcConsN: TFloatField
      FieldName = 'MedValPrcConsN'
      ReadOnly = True
    end
    object QrOprConsMesTotKWhCosXpectd: TFloatField
      FieldName = 'TotKWhCosXpectd'
      ReadOnly = True
    end
    object QrOprConsMesTotKWhCosObtned: TFloatField
      FieldName = 'TotKWhCosObtned'
      ReadOnly = True
    end
    object QrOprConsMesTotKWhCostSaved: TFloatField
      FieldName = 'TotKWhCostSaved'
      ReadOnly = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrOprConsMesTotCtyCostSaved: TFloatField
      FieldName = 'TotCtyCostSaved'
      ReadOnly = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrOprConsMesTotAllCostSaved: TFloatField
      FieldName = 'TotAllCostSaved'
      ReadOnly = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrOprConsMesIncrsTaxPercnt: TFloatField
      FieldName = 'IncrsTaxPercnt'
      ReadOnly = True
      DisplayFormat = '0.000000'
    end
    object QrOprConsMesInvestValBased: TFloatField
      FieldName = 'InvestValBased'
      ReadOnly = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrOprConsMesInvestValTaxInc: TFloatField
      FieldName = 'InvestValTaxInc'
      ReadOnly = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrOprConsMesInvestValUpdtd: TFloatField
      FieldName = 'InvestValUpdtd'
      ReadOnly = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrOprConsMesInvestValInMes: TFloatField
      FieldName = 'InvestValInMes'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrOprConsMesInvestValTotal: TFloatField
      FieldName = 'InvestValTotal'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrOprConsMesMantncValBased: TFloatField
      FieldName = 'MantncValBased'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrOprConsMesMantncValTaxInc: TFloatField
      FieldName = 'MantncValTaxInc'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrOprConsMesMantncValUpdtd: TFloatField
      FieldName = 'MantncValUpdtd'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrOprConsMesMantncValInMes: TFloatField
      FieldName = 'MantncValInMes'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrOprConsMesMantncValTotal: TFloatField
      FieldName = 'MantncValTotal'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrOprConsMesSavedValBased: TFloatField
      FieldName = 'SavedValBased'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrOprConsMesSavedValTaxInc: TFloatField
      FieldName = 'SavedValTaxInc'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrOprConsMesSavedValUpdtd: TFloatField
      FieldName = 'SavedValUpdtd'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrOprConsMesSavedValInMes: TFloatField
      FieldName = 'SavedValInMes'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrOprConsMesSavedValTotal: TFloatField
      FieldName = 'SavedValTotal'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrOprConsMesRateInvtXSaved: TFloatField
      FieldName = 'RateInvtXSaved'
      ReadOnly = True
      DisplayFormat = '#,##0.0000'
    end
    object QrOprConsMesRateMntnXSaved: TFloatField
      FieldName = 'RateMntnXSaved'
      DisplayFormat = '#,##0.0000'
    end
    object QrOprConsMesRateSpntXSaved: TFloatField
      FieldName = 'RateSpntXSaved'
      DisplayFormat = '#,##0.0000'
    end
  end
  object DsOprConsMes: TDataSource
    DataSet = QrOprConsMes
    Left = 112
    Top = 356
  end
  object QrOprConsMesIts: TZQuery
    Connection = DMod.MyDB
    OnCalcFields = QrOprConsMesItsCalcFields
    SQL.Strings = (
      'SELECT * FROM oprconsmesits')
    Params = <>
    Left = 204
    Top = 308
    object QrOprConsMesItsCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOprConsMesItsControle: TLargeintField
      FieldName = 'Controle'
    end
    object QrOprConsMesItsKind: TSmallintField
      FieldName = 'Kind'
      Required = True
    end
    object QrOprConsMesItsKwh: TFloatField
      FieldName = 'Kwh'
      Required = True
    end
    object QrOprConsMesItsPreco: TFloatField
      FieldName = 'Preco'
      Required = True
    end
    object QrOprConsMesItsValor: TFloatField
      FieldName = 'Valor'
    end
    object QrOprConsMesItsLk: TLargeintField
      FieldName = 'Lk'
    end
    object QrOprConsMesItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOprConsMesItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOprConsMesItsUserCad: TLargeintField
      FieldName = 'UserCad'
    end
    object QrOprConsMesItsUserAlt: TLargeintField
      FieldName = 'UserAlt'
    end
    object QrOprConsMesItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrOprConsMesItsAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrOprConsMesItsNO_Kind: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_Kind'
      Size = 255
      Calculated = True
    end
    object QrOprConsMesItsAnoRef: TLargeintField
      FieldName = 'AnoRef'
      ReadOnly = True
    end
    object QrOprConsMesItsMesRef: TSmallintField
      FieldName = 'MesRef'
      ReadOnly = True
    end
  end
  object DsOprConsMesIts: TDataSource
    DataSet = QrOprConsMesIts
    Left = 204
    Top = 356
  end
  object QrSum: TZQuery
    Params = <>
    Left = 396
    Top = 308
  end
  object PMFin: TPopupMenu
    OnPopup = PMFinPopup
    Left = 788
    Top = 440
    object FinInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = FinInclui1Click
    end
    object FinAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = FinAltera1Click
    end
    object FinExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = FinExclui1Click
    end
  end
  object QrOprConsMesFin: TZQuery
    Connection = DMod.MyDB
    OnCalcFields = QrOprConsMesItsCalcFields
    SQL.Strings = (
      'SELECT * FROM oprconsmesfin')
    Params = <>
    Left = 304
    Top = 308
    object QrOprConsMesFinCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOprConsMesFinAnoRef: TLargeintField
      FieldName = 'AnoRef'
      Required = True
    end
    object QrOprConsMesFinMesRef: TSmallintField
      FieldName = 'MesRef'
      Required = True
    end
    object QrOprConsMesFinControle: TLargeintField
      FieldName = 'Controle'
    end
    object QrOprConsMesFinProdDta: TDateTimeField
      FieldName = 'ProdDta'
      Required = True
      DisplayFormat = 'dd/mm/yy hh:nn'
    end
    object QrOprConsMesFinNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 255
    end
    object QrOprConsMesFinKindLct: TSmallintField
      FieldName = 'KindLct'
    end
    object QrOprConsMesFinValrCred: TFloatField
      FieldName = 'ValrCred'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrOprConsMesFinValrDebi: TFloatField
      FieldName = 'ValrDebi'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrOprConsMesFinValrInvt: TFloatField
      FieldName = 'ValrInvt'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrOprConsMesFinLk: TLargeintField
      FieldName = 'Lk'
    end
    object QrOprConsMesFinDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOprConsMesFinDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOprConsMesFinUserCad: TLargeintField
      FieldName = 'UserCad'
    end
    object QrOprConsMesFinUserAlt: TLargeintField
      FieldName = 'UserAlt'
    end
    object QrOprConsMesFinAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrOprConsMesFinAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object DsOprConsMesFin: TDataSource
    DataSet = QrOprConsMesFin
    Left = 304
    Top = 356
  end
end
