unit UnDevicesPF_Fronius;

interface

uses
  Windows, SysUtils, Classes, Menus, UnDmkEnums, mySQLDbTables, Forms,
  System.Generics.Collections, Variants, Vcl.StdCtrls, Vcl.ComCtrls,
  UnInternalConsts, UnProjGroup_Vars, UnMyJSON, uLkJSON, dmkGeral, UnDevicesPF,
  UnAppEnums, UnGrl_Geral, UnMyObjects, Data.DB;

type
  TUnDevicesPF_Fronius = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  AtualizaDevicesBD(): Boolean;
    function  AtualizaLoggersBD(): Boolean;
    function  GetJsonTextFromURL(Request: String; var JSON: String; const
              AvisaErro: Boolean = True; const LBAvisos: TListBox = nil; const
              UseURLBaseAPI: Boolean = True; const sHTTP: String = 'http://'):
              Boolean;
    function  ObtemDevicesAtivos(var Lista: TrInverters): Boolean;
    function  ObtemGeracaoInstantanea(const LBAvisos: TListBox; var P_PV:
              Double; var sDataHora: String; var UTCDiff: Double): Boolean;
    function  ObtemUrlBaseAPI(const AvisaVersao: Boolean; var Res:
              ObtemUrlBaseAPIResult; const AvisaErro: Boolean; LBAvisos:
              TListBox): Boolean;
    function  ObtemReal_WAC_Sum_Produced_Periodo(const DataI, DataF: TDateTime;
              LaAviso1, LaAviso2: TLabel; PB: TProgressBar; LBAvisos:
              TListBox): Boolean;
    function  ObtemProducao(QrTotal: TDataset; LaAviso1, LaAviso2: TLabel; PB:
              TProgressBar; LBAvisos: TListBox): Boolean;
    function  StatusConnectionOK(jHead: TlkJSONbase; LBAvisos: TListBox):
              Boolean;

  end;

var
  DevicesPF_Fronius: TUnDevicesPF_Fronius;
  cThisDeviceAssembler: TDevicesAssembler = TDevicesAssembler.assembFronius;

implementation

uses UnGrl_DmkDB, Module;

{ TUnDevicesPF_Fronius }

function TUnDevicesPF_Fronius.AtualizaDevicesBD(): Boolean;
const
  Avisa = True;
  Request = 'GetInverterInfo.cgi';
  LBAvisos = nil; // TListBox;
var
  MyArr: TMyJsonBasesArr;
  JSonStr, Nome, sTemp: String;
  jParsed, jBody, jData, Item: TlkJSONbase;
  nItem: TlkJSONobject;

  UniqueId, DeviceType, CustomName: String;
  Codigo, DeviceAssembler, DeviceSeq, PVPower: Integer;
  SQLType: TSQLType;
  I, N, QtdIns, QtdUpd: Integer;
begin
  Result := False;
  QtdIns := 0;
  QtdUpd := 0;
  // provisorio
  //JSonStr := x;
  //
  if not DevicesPF.GetJsonTextFromURL(cThisDeviceAssembler, Request, JSonStr) then Exit;
  //
  jParsed:= TlkJSON.ParseText(JsonStr);
  if not MyJSON.SliceExpectedBaseItems(jParsed, Avisa, ['Head', 'Body'], MyArr,
  LBAvisos) then Exit;
  //jHead := MyArr[0];
  jBody := MyArr[1];
  //
  //Geral.MB_Info(TlkJSON.GenerateText(jBody));
  if not MyJSON.SliceExpectedBaseItems(jBody, Avisa, ['Data'], MyArr, LBAvisos)
  then Exit;
  jData := MyArr[0];
  N := jData.Count;
  for I := 0 to N -1 do
  begin
    Item := TlkJSONObject(jData).child[I];
    if Item <> nil then
    begin
      Nome := TlkJSONobjectmethod(Item).Name;
      //
      if jData.Field[Nome] is TlkJSONobject then
      begin
        nItem := jData.Field[Nome] as TlkJSONobject;
        //Geral.MB_Info(TlkJSON.GenerateText(nItem));
        //{"DT":75,"PVPower":6600,"Show":1,"UniqueID":"537246","ErrorCode":0,"StatusCode":7,"CustomName":"&#80;&#114;&#105;&#109;&#111;&#32;&#54;&#46;&#48;&#45;&#49;&#32;&#40;&#49;&#41;"}
        //Assembler int(11) NOT NULL DEFAULT 0,
        DeviceAssembler := Integer(cThisDeviceAssembler);
        //UniqueId varchar(60) NOT NULL DEFAULT '0',
        UniqueId := nItem.getString('UniqueID');
        //DeviceSeq int(11) NOT NULL DEFAULT 0,
        DeviceSeq := StrToInt(Nome);
        //DeviceType varchar(60) NOT NULL DEFAULT '?',
        DeviceType := Geral.FF0(nItem.getInt('DT'));
        //PVPower int(11) NOT NULL DEFAULT 0,
        PVPower := nItem.getInt('PVPower');
        //CustomName varchar(255) NOT NULL DEFAULT '?',
        CustomName := nItem.getString('CustomName');
        if Geral.SoDecimalCharacter(CustomName) then
        begin
          if Geral.ConvertDecimalCharactersText(CustomName, sTemp) then
            CustomName := sTemp;
          //Geral.MB_Info(CustomName);
        end;
        Grl_DmkDB.AbreSQLQuery0(Dmod.QrPsq1, Dmod.MyDB, [
        'SELECT Codigo, DeviceAssembler, UniqueId, DeviceSeq ',
        'FROM devices ',
        'WHERE DeviceAssembler=' + Geral.FF0(DeviceAssembler),
        'AND UniqueId="'+ UniqueId + '"',
        'AND DeviceSeq=' + Geral.FF0(DeviceSeq),
        '']);
        //
        if Dmod.QrPsq1.RecordCount > 0 then
        begin
          Codigo  := Dmod.QrPsq1.FieldByName('Codigo').AsInteger;
          SQLType := stUpd;
        end else
        begin
          Codigo := Grl_DmkDB.GetNxtCodigoInt('devices', 'Codigo', SQLType, 0);
          SQLType := stIns;
        end;

        if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, 'devices', False, [
        'DeviceAssembler', 'UniqueId', 'DeviceSeq',
        'DeviceType', 'PVPower', 'CustomName'], [
        'Codigo'], [
        DeviceAssembler, UniqueId, DeviceSeq,
        DeviceType, PVPower, CustomName], [
        Codigo], True, dmksqlinsInsOnly, '', stDesktop) then
        begin
          if SQLType = stIns then
            QtdIns := QtdIns + 1
          else
            QtdUpd := QtdUpd + 1;
        end;
      end else
        Geral.MB_Erro(Nome + ' n�o � um objeto JSON!');
    end;
  end;
  Geral.MB_Info(
    'Itens inclu�dos: ' + Geral.FF0(QtdIns) + sLineBreak +
    'Itens atualizados: ' + Geral.FF0(QtdUpd) + sLineBreak
  );
  Result := QtdIns + QtdUpd > 0;
end;

function TUnDevicesPF_Fronius.AtualizaLoggersBD: Boolean;
const
  Avisa = True;
  Request = 'GetLoggerInfo.cgi';
var
  MyArr: TMyJsonBasesArr;
  JSonStr, Nome, sTemp: String;
  jParsed, jBody: TlkJSONbase;
  jLoggerInfo: TlkJSONobject;

  CO2Factor, CashFactor, DeliveryFactor: Double;
  I, N, Codigo, DeviceAssembler, DeviceSeq, UTCOffset: Integer;
  UniqueId, CO2Unit, CashCurrency, HWVersion, SWVersion, PlatformID, ProductID,
  TimezoneLocation, TimezoneName: String;

  SQLType: TSQLType;
  //
  QtdIns, QtdUpd: Integer;
begin
  Result := False;
  QtdIns := 0;
  QtdUpd := 0;
  //
  if not DevicesPF.GetJsonTextFromURL(cThisDeviceAssembler, Request, JSonStr) then
    Exit;
  //
  jParsed:= TlkJSON.ParseText(JsonStr);
  if not MyJSON.SliceExpectedBaseItems(jParsed, Avisa, ['Head', 'Body'], MyArr,
  ) then
    Exit;
  //jHead := MyArr[0];
  jBody := MyArr[1];
  //
  //Geral.MB_Info(TlkJSON.GenerateText(jBody));
  if not MyJSON.SliceExpectedBaseItems(jBody, Avisa, ['LoggerInfo'], MyArr) then
    Exit;
  jLoggerInfo := TlkJSONObject(MyArr[0]);
  //
  //Assembler int(11) NOT NULL DEFAULT 0,
  DeviceAssembler := Integer(cThisDeviceAssembler);
  //"CO2Factor" : 0.52999997138977051,
  CO2Factor := jLoggerInfo.getDouble('CO2Factor');
  //"CO2Unit" : "kg",
  CO2Unit := jLoggerInfo.getString('CO2Unit');
  //"CashCurrency" : "BRL",
  CashCurrency := jLoggerInfo.getString('CashCurrency');
  //"CashFactor" : 0.75,
  CashFactor := jLoggerInfo.getDouble('CashFactor');
  //"DefaultLanguage" : "en",  --->>> Deprecado!
  //"DeliveryFactor" : 0.05000000074505806,
  DeliveryFactor := jLoggerInfo.getDouble('DeliveryFactor');
  //"HWVersion" : "2.4D",
  HWVersion := jLoggerInfo.getString('HWVersion');
  //"PlatformID" : "wilma",
  //"SWVersion" : "3.10.2-1",
  SWVersion := jLoggerInfo.getString('SWVersion');
  PlatformID := jLoggerInfo.getString('PlatformID');
  //"ProductID" : "fronius-datamanager-card",
  ProductID := jLoggerInfo.getString('ProductID');
  //"TimezoneLocation" : "Sao_Paulo",
  TimezoneLocation := jLoggerInfo.getString('TimezoneLocation');
  //"TimezoneName" : "BRT",
  TimezoneName := jLoggerInfo.getString('TimezoneName');
  //"UTCOffset" : 4294956496,
  UTCOffset := jLoggerInfo.getInt('UTCOffset');
  //"UniqueID" : "240.400235"
  UniqueID := jLoggerInfo.getString('UniqueID');
  //
  Grl_DmkDB.AbreSQLQuery0(Dmod.QrPsq1, Dmod.MyDB, [
  'SELECT Codigo, DeviceAssembler, UniqueId, DeviceSeq ',
  'FROM loggers ',
  'WHERE DeviceAssembler=' + Geral.FF0(DeviceAssembler),
  'AND UniqueId="'+ UniqueId + '"',
  'AND DeviceSeq=' + Geral.FF0(DeviceSeq),
  '']);
  //
  Codigo  := Dmod.QrPsq1.FieldByName('Codigo').AsInteger;
  Codigo := Grl_DmkDB.GetNxtCodigoInt('loggers', 'Codigo', SQLType, Codigo);
(*
  if Dmod.QrPsq1.RecordCount > 0 then
  begin
    Codigo  := Dmod.QrPsq1.FieldByName('Codigo').AsInteger;
    SQLType := stUpd;
  end else
  begin
    Codigo  := Grl_DmkDB.ObtemCodigoInt('loggers', 'Codigo', Dmod.QrAux, Dmod.MyDB);
    SQLType := stIns;
  end;
*)
  if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, 'loggers', False, [
  'DeviceAssembler', 'UniqueId', 'DeviceSeq',
  'ProductID', 'PlatformID', 'HWVersion',
  'SWVersion', 'TimezoneLocation', 'TimezoneName',
  'UTCOffset', 'CashFactor', 'DeliveryFactor',
  'CashCurrency', 'CO2Factor', 'CO2Unit'], [
  'Codigo'], [
  DeviceAssembler, UniqueId, DeviceSeq,
  ProductID, PlatformID, HWVersion,
  SWVersion, TimezoneLocation, TimezoneName,
  UTCOffset, CashFactor, DeliveryFactor,
  CashCurrency, CO2Factor, CO2Unit], [
  Codigo], True, dmksqlinsInsOnly, '', stDesktop) then
  begin
    if SQLType = stIns then
      QtdIns := QtdIns + 1
    else
      QtdUpd := QtdUpd + 1;
  end;
  //
  Geral.MB_Info(
    'Itens inclu�dos: ' + Geral.FF0(QtdIns) + sLineBreak +
    'Itens atualizados: ' + Geral.FF0(QtdUpd) + sLineBreak
  );
  Result := QtdIns + QtdUpd > 0;
end;

function TUnDevicesPF_Fronius.GetJsonTextFromURL(Request: String; var JSON:
  String; const AvisaErro: Boolean = True; const LBAvisos: TListBox = nil;
  const UseURLBaseAPI: Boolean = True; const sHTTP: String = 'http://'):
  Boolean;
var
  URL: String;
begin
  if UseURLBaseAPI then
    URL := sHTTP + VAR_DeviceHost + VAR_URLBaseAPI + Request
  else
    URL := sHTTP + VAR_DeviceHost + VAR_OtherURLBaseAPI + Request;
  Result := MyJSON.GetJsonTextFromURL(VAR_DeviceHost, URL, JSON, AvisaErro, LBAvisos);
end;

function TUnDevicesPF_Fronius.ObtemDevicesAtivos(
  var Lista: TrInverters): Boolean;
const
  Avisa = True;
  Request = 'GetActiveDeviceInfo.cgi?DeviceClass=Inverter';
var
  JSonStr, Nome: String;
  jParsed, jBody, jData, Item: TlkJSONbase;
  nItem: TlkJSONobject;
  MyArr: TMyJsonBasesArr;
  I, N: Integer;
  y: String;
  Inverter: rInverter;
begin
  Result := False;
  Lista := nil;
  //
  if not DevicesPF.GetJsonTextFromURL(cThisDeviceAssembler, Request, JSonStr) then
    Exit;
  //
  jParsed:= TlkJSON.ParseText(JsonStr);
  if not MyJSON.SliceExpectedBaseItems(jParsed, Avisa, ['Head', 'Body'], MyArr) then
    Exit;
  //jHead := MyArr[0];
  jBody := MyArr[1];
  //
  //Geral.MB_Info(TlkJSON.GenerateText(jBody));
  if not MyJSON.SliceExpectedBaseItems(jBody, Avisa, ['Data'], MyArr) then
    Exit;
  //
  jData := MyArr[0];
  N := jData.Count;
  SetLength(Lista, N);
  for I := 0 to N -1 do
  begin
    Item := TlkJSONObject(jData).child[I];
    if Item <> nil then
    begin
      Nome := TlkJSONobjectmethod(Item).Name;
      //
      if jData.Field[Nome] is TlkJSONobject then
      begin
        nItem := jData.Field[Nome] as TlkJSONobject;
        Inverter.InverterNo := Nome;
        Inverter.DeviceType := Geral.FF0(nItem.getInt('DT'));
        Inverter.SerialNo := nItem.getString('Serial');
        Lista[I] := Inverter;
        //
        //Geral.MB_Info(Nome);
      end;
    end;
  end;
  Result := N > 0;
  jParsed.Free;
end;

function TUnDevicesPF_Fronius.ObtemGeracaoInstantanea(const LBAvisos: TListBox;
  var P_PV: Double; var sDataHora: String; var UTCDiff: Double): Boolean;
const
  Avisa = True;
  Request = 'GetPowerFlowRealtimeData.fcgi';
var
  MyArr: TMyJsonBasesArr;
  //
  JSonStr: String;
  jParsed, jHead, jBody, jTimeStamp, jData, jSite, jE_Day, jE_Total, jE_Year,
  jMeter_Location, jMode, jP_Akku, jP_Grid, jP_Load, jP_PV, jrel_Autonomy,
  jrel_SelfConsumption: TlkJSONbase;
  //
  sTimeStamp: String;
  sE_Day, sE_Total, sE_Year, sMeter_Location, sMode, sP_Akku, sP_Grid, sP_Load,
  sP_PV, srel_Autonomy, srel_SelfConsumption: String;
  //
begin
  Result := False;
  //
  //Temporario
  if not DevicesPF.GetJsonTextFromURL(cThisDeviceAssembler, Request, JSonStr,
  True, LBAvisos) then
    Exit;
  //JsonStr := X;
  //
  jParsed := TlkJSON.ParseText(JsonStr);
  if not MyJSON.SliceExpectedBaseItems(jParsed, Avisa, ['Head', 'Body'], MyArr) then
    Exit;
  jHead := MyArr[0];
  jBody := MyArr[1];
  //
  if not MyJSON.SliceExpectedBaseItems(jHead, Avisa, ['RequestArguments', 'Status',
  'Timestamp'], MyArr, LBAvisos) then
    Exit;
  jTimestamp         := MyArr[2];
  sTimeStamp         := jTimestamp.Value;
  Grl_Geral.AjustaDataHoraUTC(sTimestamp, UTCDiff);
  sDataHora          := sTimeStamp;
  //DataHora           := Geral.ValidaDataHoraSQL(sTimestamp);
  //
  //Geral.MB_Info(TlkJSON.GenerateText(jBody));
  if not MyJSON.SliceExpectedBaseItems(jBody, Avisa, ['Data'], MyArr) then
    Exit;
  //
  jData := MyArr[0];
  //Geral.MB_Info(TlkJSON.GenerateText(jBody));
  if not MyJSON.SliceExpectedBaseItems(jData, Avisa, ['Inverters', 'Site', 'Version'], MyArr) then
    Exit;
  //
  jSite := MyArr[1];
  if not MyJSON.SliceExpectedBaseItems(jSite, Avisa, ['E_Day', 'E_Total',
  'E_Year', 'Meter_Location', 'Mode', 'P_Akku', 'P_Grid', 'P_Load', 'P_PV',
  'rel_Autonomy', 'rel_SelfConsumption'], MyArr) then
    Exit;
  //
  jE_Day               := MyArr[00];
  jE_Total             := MyArr[01];
  jE_Year              := MyArr[02];
  jMeter_Location      := MyArr[03];
  jMode                := MyArr[04];
  jP_Akku              := MyArr[05];
  jP_Grid              := MyArr[06];
  jP_Load              := MyArr[07];
  jP_PV                := MyArr[08];
  jrel_Autonomy        := MyArr[09];
  jrel_SelfConsumption := MyArr[10];
  //
  //sE_Day               := jE_Day.Value;
  if jP_PV.Value <> null then
    sP_PV                := jP_PV.Value;
  //Geral.MB_Info(sE_Day);
  P_PV := Geral.DMV(sP_PV);
  //
  Result := True;
end;

function TUnDevicesPF_Fronius.ObtemProducao(QrTotal: TDataset; LaAviso1,
  LaAviso2: TLabel; PB: TProgressBar; LBAvisos: TListBox): Boolean;
const
  MaxDias = 16; // 16
  MinDias = 01; // 1
var
  DataI, DataF: TDateTime;
  F, I, N, Ini, Fim: Integer;
  //
  function Importa(Dias: Integer): Boolean;
  var
    Periodo: Integer;
  begin
    Result := False;
    Grl_DmkDB.AbreSQLQuery0(DMod.QrPsq1, DMod.MyDB, [
  {
    'SELECT MAX(ProdDta) ProdDta',
    'SELECT ProdDta ProdDta',
    'FROM wacprodvalues ;',
  }
    'SELECT ProdDta',
    'FROM wacprodvalues ',
    'ORDER BY ProdDta desc',
    'LIMIT 1',
    '']);
    I := Trunc(DMod.QrPsq1.FieldByName('ProdDta').AsDateTime);
    N := 0;
    //
    F := Trunc(Date());
    if (F - I = 0) and (Dias > 1) then
      Periodo := 1
    else
      Periodo := Dias;
    while F - I > -1 do
    begin
      DataI := I;
      DataF := I + Periodo - 1;
      //
      Result := DevicesPF.ObtemReal_WAC_Sum_Produced_Periodo(
        VAR_DeviceAssembler, DataI, DataF, LaAviso1, LaAviso2, PB, LBAvisos);
      if not Result then Exit;
      //
      N := N + 1;
      I := I + Periodo;
      if (QrTotal <> nil) and (QrTotal.State <> dsInactive) then
      begin
        QrTotal.Close;
        QrTotal.Open;
      end;
    end;
    //Result := True;
  end;
var
  Msg: String;
begin
  Result := Importa(MaxDias);
  if Result = False then
    Result := Importa(MinDias);
  if Result then
    Msg := '...'
  else
    Msg := 'N�o foi poss�vel buscar dados do sistema de gera��o!';
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, Msg);
end;

function TUnDevicesPF_Fronius.ObtemReal_WAC_Sum_Produced_Periodo(const DataI,
  DataF: TDateTime; LaAviso1, LaAviso2: TLabel; PB: TProgressBar; LBAvisos:
  TListBox): Boolean;
const
  sProcName = 'TUnDevicesPF_Fronius.ObtemReal_WAC_Sum_Produced_Periodo()';
  //
  procedure MeuAviso(s: String);
  begin
    Geral.MB_Aviso(s + sLineBreak + sProcName);
  end;
  procedure ExibeUser(Msg: String);
  begin
    MyObjects.Informa2EUpdPB(PB, LaAviso1, LaAviso2, True, Msg);
  end;
const
  Avisa = True;
var
  TxtPeriodo: String;
  MyArr: TMyJsonBasesArr;
  Dia, Mes, Ano: Word;
  sDataI, sDataF, Request: String;
  Inverters: TrInverters;
  MyInverter: rInverter;
  JSonStr: String;
  SQLType: TSQLType;

  jParsed,
  // Head
  jHead, jBody,
  jRequestArguments, jStatus, jTimestamp: TlkJSONbase;
  sTimeStamp: String;
  jScope, jStartDate, jEndDate, jSeriesType, jHumanReadable: TlkJSONbase;
  jChannel: TlkJSONobject;
  sStartDate, sEndDate: String;
  jCode, jReason, jUserMessage, jErrorDetail: TlkJSONbase;
  //
  Req_Scope: String;
  Req_StartDate, Req_EndDate: TDateTime;
  Req_StartUTCDiff, Req_EndUTCDiff: Double;
  Req_Channel, Req_SeriesType, Req_HumanReadable: String;
  Sta_Code, Codigo: Integer;
  Sta_Reason, Sta_UserMessage, Sta_ErrorDetail: String;
  Ask_Date: TDateTime;
  Ask_UTCDiff: Double;

  // Body
  jData, nItem: TlkJSONbase;
  //jInverter: TlkJSONbase;
  jInverter: TlkJSONObject;
  I, J, N: Integer;
  Nome, Inverter, Serial, _Start, _End: String;
  Date_Start, Date_End: TdateTime;
  Date_StartUTCDiff, Date_EndUTCDiff: Double;
  NodeType, DeviceType, Controle: Integer;

  // Data
  sPsqInverter: String;
  jInverterN, jLeiturasN, jLeiturasN_Data, jLeiturasN_Data_WAC, jLeiturasN_Unit,
  jProducao, jLeiturasN_Values, Item: TlkJSONbase;
  //: TlkJSONobject;
  jWh: TlkJSONnumber;

  ProdDta, RequDta, FatuDta: TDateTime;
  Unidade: String;
  DeviceSeq, RequSec: Integer;
  Quantia: Double;
// http://192.168.100.180//solar_api/v1/GetArchiveData.cgi?Scope=System&StartDate=7.8.2018&EndDate=7.8.2018&Channel=EnergyReal_WAC_Sum_Produced
begin
  Result := False;
  //
  PB.Position := 0;
  PB.Max := 1000;
  ExibeUser('Verificando inversores');
  if not DevicesPF.ObtemDevicesAtivos(VAR_DeviceAssembler, Inverters) then Exit;

  ExibeUser('Verificando produ��o');
  DecodeDate(DataI, Ano, Mes, Dia);
  sDataI := Geral.FF0(Dia) + '.' +Geral.FF0(Mes) + '.' +Geral.FF0(Ano);
  DecodeDate(DataF, Ano, Mes, Dia);
  sDataF := Geral.FF0(Dia) + '.' +Geral.FF0(Mes) + '.' +Geral.FF0(Ano);
  TxtPeriodo := 'Per�odo: ' + sDataI + ' a ' + sDataF + '. ';
  Request := 'GetArchiveData.cgi?Scope=System&StartDate=' + sDataI + '&EndDate='
             + sDataF + '&Channel=EnergyReal_WAC_Sum_Produced';
  if not DevicesPF.GetJsonTextFromURL(cThisDeviceAssembler, Request, JSonStr,
  True, LBAvisos)
  then Exit;

  ExibeUser(TxtPeriodo + 'Verificando produ��o > cabe�alho');
  jParsed:= TlkJSON.ParseText(JsonStr);
  //Geral.MB_Info(TlkJSON.GenerateText(jParsed));
  //
  if not MyJSON.SliceExpectedBaseItems(jParsed, Avisa, ['Head', 'Body'], MyArr,
  LBAvisos) then Exit;
  jHead := MyArr[0];
  jBody := MyArr[1];
  //
  if not StatusConnectionOK(jHead, LBAvisos) then
    Exit;
  //Geral.MB_Info(TlkJSON.GenerateText(jHead));
  if not MyJSON.SliceExpectedBaseItems(jHead, Avisa, ['RequestArguments', 'Status',
  'Timestamp'], MyArr, LBAvisos) then
    Exit;
  jRequestArguments := MyArr[0];
  jStatus           := MyArr[1];
  jTimestamp        := MyArr[2];
  sTimeStamp        := jTimestamp.Value;
  Grl_Geral.AjustaDataHoraUTC(sTimestamp, Ask_UTCDiff);
  Ask_Date          := Geral.ValidaDataHoraSQL(sTimestamp);
  //
  //Geral.MB_Info(TlkJSON.GenerateText(jRequestArguments));
  if not MyJSON.SliceExpectedBaseItems(jRequestArguments, Avisa, ['Scope',
  'StartDate', 'EndDate', 'Channel', 'SeriesType', 'HumanReadable'], MyArr,
  LBAvisos) then
    Exit;
  jScope            := MyArr[0];
  jStartDate        := MyArr[1];
  jEndDate          := MyArr[2];
  jChannel          := TlkJSONobject(MyArr[3]);
  jSeriesType       := MyArr[4];
  jHumanReadable    := MyArr[5];
  //
  Req_Scope         := jScope.Value;
  sStartDate        := jStartDate.Value;
  Grl_Geral.AjustaDataHoraUTC(sStartDate, Req_StartUTCDiff);
  Req_StartDate     := Geral.ValidaDataHoraSQL(sStartDate);
  sEndDate          := jEndDate.Value;
  Grl_Geral.AjustaDataHoraUTC(sEndDate, Req_EndUTCDiff);
  Req_EndDate       := Geral.ValidaDataHoraSQL(sEndDate);
  //
  //Req_Channel       := jChannel.Value;
  Req_Channel       := '';
  for I := 0 to jChannel.Count -1 do
  begin
    //NomeChannel := jChannel.NameOf[I];
    if TlkJSONlist(jChannel).Child[I].Value <> Null then
      //Geral.MB_Info(Nome + '->'+tlkJSONlist(jChannel).Child[k].Value);
      Req_Channel   := Req_Channel + ' ' + TlkJSONlist(jChannel).Child[I].Value;
  end;
  Req_Channel       := Trim(Req_Channel);
  //
  Req_SeriesType    := jSeriesType.Value;
  Req_HumanReadable := jHumanReadable.Value;
  //
  if not MyJSON.SliceExpectedBaseItems(jStatus, Avisa, ['Code',
  'Reason', 'UserMessage', 'ErrorDetail'], MyArr, LBAvisos) then
    Exit;
  jCode             := MyArr[0];
  jReason           := MyArr[1];
  jUserMessage      := MyArr[2];
  jErrorDetail      := MyArr[3];
  //
  Sta_Code          := jCode.Value;
  Sta_Reason        := jReason.Value;
  Sta_UserMessage   := jUserMessage.Value;
  Sta_ErrorDetail   := TlkJSON.GenerateText(jErrorDetail);
  if Sta_ErrorDetail = '{"Nodes":[]}' then
    Sta_ErrorDetail := '';
  //
  (*
  Codigo := Trunc(Date);//Grl_DmkDB.ObtemCodigoInt('wacprodhead', 'Codigo', Dmod.QrAux, Dmod.MyDB);
  SQLType := Grl_DmkDB.CodigoExisteInt1InsUpd(Codigo, 'wacprodhead', 'Codigo',
    Dmod.QrAux, Dmod.MyDB);
  *)
  Codigo := Grl_DmkDB.GetNxtCodigoInt('wacprodhead', 'Codigo', SQLType, 0);
  //
  //
  if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, 'wacprodhead', False, [
  'Codigo', 'Req_Scope', 'Req_StartDate',
  'Req_StartUTCDiff', 'Req_EndDate', 'Req_EndUTCDiff',
  'Req_Channel',
  'Req_SeriesType', 'Req_HumanReadable', 'Sta_Code',
  'Sta_Reason', 'Sta_UserMessage', 'Sta_ErrorDetail',
  'Ask_Date', 'Ask_UTCDiff'], [
  'Codigo'], [
  Codigo, Req_Scope, Req_StartDate,
  Req_StartUTCDiff, Req_EndDate, Req_EndUTCDiff,
  Req_Channel,
  Req_SeriesType, Req_HumanReadable, Sta_Code,
  Sta_Reason, Sta_UserMessage, Sta_ErrorDetail,
  Ask_Date, Ask_UTCDiff], [
  Codigo], True, dmksqlinsInsOnly, '', stDesktop) then
  begin
     ExibeUser(TxtPeriodo + 'Verificando produ��o > Inversor');
     if not MyJSON.SliceExpectedBaseItems(jBody, Avisa, ['Data'], MyArr, LBAvisos) then
      Exit;
    jData             := MyArr[0];

    //
    N := jData.Count;
    for I := 0 to N -1 do
    begin
      ExibeUser(TxtPeriodo + 'Verificando produ��o > Inversor ' + Geral.FF0(I+1));
      nItem := TlkJSONObject(jData).child[I];
      if nItem <> nil then
      begin
        Nome := TlkJSONobjectmethod(nItem).Name;
        //
        if jData.Field[Nome] is TlkJSONobject then
        begin
          //jInverter := jData.Field[Nome] as TlkJSONobject;
          jInverter := jData.Field[Nome] as TlkJSONobject;
          Inverter := Copy(Nome,10); //'Inverter/' >> 1, 2, 3....
          for J := 0 to Length(Inverters) - 1 do
          begin
            MyInverter := Inverters[J];
            if MyInverter.InverterNo = Inverter then
              Serial := MyInverter.SerialNo;
          end;
          //
          //Codigo
          //Controle
          //Inverter
          NodeType          := jInverter.getInt('NodeType');
          DeviceType        := jInverter.getInt('DeviceType');
          _Start            := jInverter.getString('Start');
          Grl_Geral.AjustaDataHoraUTC(_Start, Date_StartUTCDiff);
          Date_Start        := Geral.ValidaDataHoraSQL(_Start);
          //Date_StartUTCDiff
          _end              := jInverter.getString('End');
          Grl_Geral.AjustaDataHoraUTC(_end, Date_EndUTCDiff);
          Date_end          := Geral.ValidaDataHoraSQL(_end);
          //Date_endUTCDiff


          //SQLType             := ImgTipo.SQLType?;
          //
          //Codigo              := ;
            //
          Controle := 0;
          Grl_DmkDB.AbreSQLQuery0(Dmod.QrPsq1, Dmod.MyDB, [
          'SELECT Controle ',
          'FROM wacprodbody',
          'WHERE Codigo=' + Geral.FF0(Codigo),
          'AND Inverter="' + Inverter + '"',
          '']);
          //
          if Dmod.QrPsq1.RecordCount > 0 then
            Controle := Dmod.QrPsq1.FieldByName('Controle').AsInteger;
          //
          Controle := Grl_DmkDB.GetNxtCodigoInt('wacprodbody', 'Controle', SQLType, Controle);
          //
          if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, 'wacprodbody', False, [
          'Codigo', 'Inverter', 'NodeType',
          'DeviceType', 'Date_Start', 'Date_StartUTCDiff',
          'Date_End', 'Date_EndUTCDiff'], [
          'Controle'], [
          Codigo, Inverter, NodeType,
          DeviceType, Date_Start, Date_StartUTCDiff,
          Date_End, Date_EndUTCDiff], [
          Controle], True, dmksqlinsInsOnly, '',  stDesktop) then
          begin
            sPsqInverter := 'inverter/' + Trim(Inverter);
            //
            //if not MyJSON.SliceExpectedBaseItems(jData, Avisa, ['inverter/1'], MyArr) then
            if not MyJSON.SliceExpectedBaseItems(jData, Avisa, [sPsqInverter],
            MyArr, LBAvisos) then
              Exit;

            jLeiturasN        := MyArr[0];
            if not MyJSON.SliceExpectedBaseItems(jLeiturasN, Avisa, ['Data',
            'DeviceType', 'NodeType', 'Start', 'End'], MyArr, LBAvisos) then
              Exit;

            jLeiturasN_Data  := MyArr[0];
            if not MyJSON.SliceExpectedBaseItems(jLeiturasN_Data, Avisa, [
            'EnergyReal_WAC_Sum_Produced'], MyArr, LBAvisos) then
              Exit;

            jLeiturasN_Data_WAC  := MyArr[0];
            if not MyJSON.SliceExpectedBaseItems(jLeiturasN_Data_WAC, Avisa, [
            'Unit', 'Values', '_comment'], MyArr, LBAvisos) then
              Exit;

            jLeiturasN_Unit  := MyArr[0];

            jLeiturasN_Values  := MyArr[1];
            Item               := TlkJSONobject(MyArr[1]);
            //
            PB.Position := 0;
            PB.Max := jLeiturasN_Values.Count;
            for J := 0 to jLeiturasN_Values.Count - 1 do
            begin
              ExibeUser(TxtPeriodo + 'Verificando produ��o > Inversor ' + Geral.FF0(I+1) +
              '  Leitura ' + Geral.FF0(J));
              //NomeChannel := jChannel.NameOf[I];
              //Geral.MB_Info(jLeiturasN_Values.
              Item := TlkJSONObject(jLeiturasN_Values).child[J];
              Nome := TlkJSONobjectmethod(Item).Name;
              RequSec             := Geral.IMV(Nome);
              //jWh := Item.Field[Nome] as TlkJSONnumber;
              jWh := jLeiturasN_Values.Field[Nome] as TlkJSONnumber;

              //DeviceType          := ;
              DeviceSeq           := N;
              //ProdDta             := - Req_StartUTCDiff;
              ProdDta             := 0;
              ProdDta             := ProdDta + (RequSec / 86400);
              ProdDta             := ProdDta + Date_Start;
              //Inverter            := ;
              //Serial              := ;
              RequDta             := Req_StartDate;
              //RequSec             := Geral.IMV(Nome);
              FatuDta             := Geral.ValidaDataHoraSQL('0000-00-00 00:00:00');
              Unidade             := jLeiturasN_Unit.Value;
              Quantia             := jWh.Value;
              if Lowercase(Unidade) = 'wh' then
              begin
                //Unidade := 'Wh';
                //Quantia := Quantia * 1000;
              end else
              if Lowercase(Unidade) = 'kwh' then
              begin
                Unidade := 'Wh';
                Quantia := Quantia * 1000;
              end else
              if Lowercase(Unidade) = 'mwh' then
              begin
                Unidade := 'Wh';
                Quantia := Quantia * 1000000;
              end else
              if Lowercase(Unidade) = 'gwh' then
              begin
                Unidade := 'Wh';
                Quantia := Quantia * 1000000000;
              end else
              begin
                Geral.MB_Aviso('Unidade desconhecida: ' + Unidade);
              end;

              Grl_DmkDB.AbreSQLQuery0(Dmod.QrPsq1, Dmod.MyDB, [
              'SELECT ProdDta ',
              'FROM wacprodvalues',
              'WHERE DeviceType=' + Geral.FF0(DeviceType),
              'AND DeviceSeq=' + Geral.FF0(DeviceSeq),
              'AND ProdDta="' + Geral.FDT(ProdDta, 109) + '"',
              '']);
          //
          //Geral.MB_Info(Geral.FDT(Dmod.QrPsq1.FieldByName('ProdDta').AsDateTime, 109));
              if Dmod.QrPsq1.RecordCount > 0 then
                SQLType := stUpd
              else
                SQLType := stIns;
(*
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'DeviceType';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 2;
      FRIndices.Column_name   := 'DeviceSeq';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 3;
      FRIndices.Column_name   := 'ProdDta';
*)
              //
              //? := Grl_DmkDB.GetNxtCodigoInt(Dmod.QrAux, Dmod.MyDB, 'wacprodvalues', 'DeviceType', SQLType, CodAtual?);
              if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, 'wacprodvalues', False, [
              'Inverter',
              'Serial', 'RequDta', 'RequSec',
              'FatuDta', 'Unidade', 'Quantia'], [
              'DeviceType', 'DeviceSeq', 'ProdDta'], [
              Inverter,
              Serial, RequDta, RequSec,
              FatuDta, Unidade, Quantia], [
              DeviceType, DeviceSeq, ProdDta], True, dmksqlinsInsOnly, '', stDesktop) then
              begin
                //
              end;
            end // fim WacProdValues
          end;// fim WacProdBody
        end else
          MeuAviso(Nome + ' n�o � um objeto');
      end else
        MeuAviso('Item ' + Geral.FF0(I) + ' de jData n�o � um objeto');
    end; // fim for I ...
  end; // fim WacProdHead
  //
  jParsed.Free;
  MyObjects.Informa2EUpdPB(PB, LaAviso1, LaAviso2, False, '...');
  Result := True;
end;

function TUnDevicesPF_Fronius.ObtemUrlBaseAPI(const AvisaVersao: Boolean;
  var Res: ObtemUrlBaseAPIResult; const AvisaErro: Boolean; LBAvisos:
  TListBox): Boolean;
  //
  procedure ObtemVersaoAPI(Texto: String);
  var
    Objeto: TlkJSONobject;
  begin
    if MyJSON.GetJsonObjectFromJsonText(Texto, Objeto) then
    begin
      Res.Versao := Geral.FF0(Objeto.getInt('APIVersion'));
      Res.UrlBaseAPI := Objeto.getString('BaseURL');
      if AvisaVersao then
        Geral.MB_Info('Vers�o da API: ' + Res.Versao);
      Result := True;
      //
      Objeto.Free;
    end;
  end;
  //
const
  //API = '/solar_api/v1/';
  API = '/solar_api/'; //v1/';
  Request = 'GetAPIVersion.cgi';
  UseURLBaseAPI = False;
var
  InverterIP, URL, Texto: String;
begin
  Result := False;
  Res.Versao := '';
  Res.UrlBaseAPI := '';
  //
  VAR_OtherURLBaseAPI := API;
  //
  if DevicesPF.GetJsonTextFromURL(cThisDeviceAssembler, Request, Texto,
  AvisaErro, LBAvisos, UseURLBaseAPI) then
    ObtemVersaoAPI(Texto);
end;

function TUnDevicesPF_Fronius.StatusConnectionOK(jHead: TlkJSONbase; LBAvisos:
  TListBox): Boolean;
const
  sProcName = 'TUnDevicesPF_Fronius.StatusConnectionOK()';
  Avisa = True;
var
  jStatus: TlkJSONbase;
  I, qtdI: Integer;
  Item: TlkJSONbase;
  Nome: String;
  MyArr: TMyJsonBasesArr;
  jCode, jReason, jUserMessage, jErrorDetail: TlkJSONbase;
  StatusCode: Integer;
  StatusReason, StatusUserMessage, StatusErrorDetail, Aviso: String;
begin
  Result := True;
  //
  qtdI := jHead.Count;
  if qtdI = 0 then Exit;
  for I := 0 to qtdI -1 do
  begin
    Item := TlkJSONObject(jHead).child[I];
    if Item <> nil then
    begin
      Nome := TlkJSONobjectmethod(Item).Name;
      if Nome = 'Status' then
      begin
        jStatus := jHead.Field[Nome] as TlkJSONobject;
        //
        if not MyJSON.SliceExpectedBaseItems(jStatus, Avisa, ['Code',
        'Reason', 'UserMessage', 'ErrorDetail'], MyArr, LBAvisos) then
          Exit;
        jCode             := MyArr[0];
        jReason           := MyArr[1];
        jUserMessage      := MyArr[2];
        jErrorDetail      := MyArr[3];
        //
        StatusCode          := jCode.Value;
        StatusReason        := jReason.Value;
        StatusUserMessage   := jUserMessage.Value;
        StatusErrorDetail   := TlkJSON.GenerateText(jErrorDetail);
        if StatusErrorDetail = '{"Nodes":[]}' then
          StatusErrorDetail := '';
        //
        Result := StatusCode = 0;
        if not Result then
        begin
          Aviso :=      'Retorno de consulta ao dispositivo com erro: ' +
          //sLineBreak +   '============================================' +
          sLineBreak + sLineBreak +
          'Codigo do erro: ' + Geral.FF0(StatusCode) + ' ' + sLineBreak +
          'Motivo: ' + StatusReason + ' ' + sLineBreak;
          //
          if Trim(StatusUserMessage) <> '' then Aviso := Aviso +
          'Mensagem do usu�rio:' + StatusUserMessage + ' ' + sLineBreak;
          //
          if Trim(StatusErrorDetail) <> '' then Aviso := Aviso +
          'Detalhes do erro: ' + StatusErrorDetail + ' ' + sLineBreak;
          //
          if LBAvisos <> nil then
            LBAvisos.Items.Add(Aviso)
          else
            Geral.MB_Aviso(Aviso);
        end;
      end;
    end;
  end;
end;

(*
3.1 GetInverterRealtimeData - Produ��o real acumulada no dia
================================================================================

http://192.168.25.2/solar_api/v1/GetInverterRealtimeData.cgi?Scope=System
{
   "Body" : {
      "Data" : {
         "DAY_ENERGY" : {
            "Unit" : "Wh",
            "Values" : {
               "1" : 5084
            }
         },
         "PAC" : {
            "Unit" : "W",
            "Values" : {
               "1" : 183
            }
         },
         "TOTAL_ENERGY" : {
            "Unit" : "Wh",
            "Values" : {
               "1" : 887065
            }
         },
         "YEAR_ENERGY" : {
            "Unit" : "Wh",
            "Values" : {
               "1" : 887065
            }
         }
      }
   },
   "Head" : {
      "RequestArguments" : {
         "DeviceClass" : "Inverter",
         "Scope" : "System"
      },
      "Status" : {
         "Code" : 0,
         "Reason" : "",
         "UserMessage" : ""
      },
      "Timestamp" : "2018-08-05T16:24:32-03:00"
   }
}

////////////////////////////////////////////////////////////////////////////////

Todos dados em tempo real do dispositivo selecionado:
http://192.168.25.2/solar_api/v1/GetInverterRealtimeData.cgi?Scope=Device&DeviceId=1&DataCollection=CommonInverterData
{
   "Body" : {
      "Data" : {
         "DAY_ENERGY" : {
            "Unit" : "Wh",
            "Value" : 5106
         },
         "DeviceStatus" : {
            "ErrorCode" : 0,
            "LEDColor" : 2,
            "LEDState" : 0,
            "MgmtTimerRemainingTime" : -1,
            "StateToReset" : false,
            "StatusCode" : 7
         },
         "FAC" : {
            "Unit" : "Hz",
            "Value" : 59.960000000000001
         },
         "IAC" : {
            "Unit" : "A",
            "Value" : 0.63
         },
         "IDC" : {
            "Unit" : "A",
            "Value" : 0.22
         },
         "PAC" : {
            "Unit" : "W",
            "Value" : 135
         },
         "TOTAL_ENERGY" : {
            "Unit" : "Wh",
            "Value" : 887088.06000000006
         },
         "UAC" : {
            "Unit" : "V",
            "Value" : 218.5
         },
         "UDC" : {
            "Unit" : "V",
            "Value" : 692.39999999999998
         },
         "YEAR_ENERGY" : {
            "Unit" : "Wh",
            "Value" : 887088.12
         }
      }
   },
   "Head" : {
      "RequestArguments" : {
         "DataCollection" : "CommonInverterData",
         "DeviceClass" : "Inverter",
         "DeviceId" : "1",
         "Scope" : "Device"
      },
      "Status" : {
         "Code" : 0,
         "Reason" : "",
         "UserMessage" : ""
      },
      "Timestamp" : "2018-08-05T16:32:57-03:00"
   }
}

////////////////////////////////////////////////////////////////////////////////

3.2 GetSensorRealtimeData - Sensores (cart�o?) em tempo real  : sem retorno de informa��o
================================================================================
NowSensorData
  The presently measured values of every active channel.

http://192.168.25.2/solar_api/v1/GetSensorRealtimeData.cgi?Scope=System&DataCollection=NowSensorData
{
   "Body" : {
      "Data" : {}
   },
   "Head" : {
      "RequestArguments" : {
         "DataCollection" : "NowSensorData",
         "DeviceClass" : "SensorCard",
         "Scope" : "System"
      },
      "Status" : {
         "Code" : 0,
         "Reason" : "",
         "UserMessage" : ""
      },
      "Timestamp" : "2018-08-05T16:37:57-03:00"
   }
}

MinMaxSensorData
  The minimum and maximum values for every time period (day, month, year, total)
  of every channel. Some channels do not have a minimum value because it would
  always be zero. For these channels, the minimum   value is not included.

http://192.168.25.2/solar_api/v1/GetSensorRealtimeData.cgi?Scope=System&DataCollection=MinMaxSensorData
{
   "Body" : {
      "Data" : {}
   },
   "Head" : {
      "RequestArguments" : {
         "DataCollection" : "MinMaxSensorData",
         "DeviceClass" : "SensorCard",
         "Scope" : "System"
      },
      "Status" : {
         "Code" : 0,
         "Reason" : "",
         "UserMessage" : ""
      },
      "Timestamp" : "2018-08-05T16:39:06-03:00"
   }
}

////////////////////////////////////////////////////////////////////////////////

3.3 GetStringRealtimeData - Strings: sem retorno de informa��o
================================================================================

http://192.168.25.2/solar_api/v1/GetStringRealtimeData.cgi?Scope=System&DataCollection=NowStringControlData
http://192.168.25.2/solar_api/v1/GetStringRealtimeData.cgi?Scope=Device&DeviceId=1&DataCollection=NowStringControlData
http://192.168.25.2/solar_api/v1/GetStringRealtimeData.cgi?Scope=Device&DeviceId=1&DataCollection=CurrentSumStringControlData&TimePeriod=Day
http://192.168.25.2/solar_api/v1/GetStringRealtimeData.cgi?Scope=Device&DeviceId=1&DataCollection=LastErrorStringControlData


////////////////////////////////////////////////////////////////////////////////

3.4 GetLoggerInfo - Data logger - Cadastro - (dados para a rede e web ???)
================================================================================
This request provides information about the logging device which provides this API.

{
   "Body" : {
      "LoggerInfo" : {
         "CO2Factor" : 0.52999997138977051,
         "CO2Unit" : "kg",
         "CashCurrency" : "BRL",
         "CashFactor" : 0.75,
         "DefaultLanguage" : "en",
         "DeliveryFactor" : 0.05000000074505806,
         "HWVersion" : "2.4D",
         "PlatformID" : "wilma",
         "ProductID" : "fronius-datamanager-card",
         "SWVersion" : "3.10.2-1",
         "TimezoneLocation" : "Sao_Paulo",
         "TimezoneName" : "BRT",
         "UTCOffset" : 4294956496,
         "UniqueID" : "240.400235"
      }
   },
   "Head" : {
      "RequestArguments" : {},
      "Status" : {
         "Code" : 0,
         "Reason" : "",
         "UserMessage" : ""
      },
      "Timestamp" : "2018-08-05T08:41:11-03:00"
   }
}

////////////////////////////////////////////////////////////////////////////////

3.5 GetLoggerLEDInfo - LEDs: Vermelho / amarelo / verde???
================================================================================
http://192.168.25.2/solar_api/v1/GetLoggerLEDInfo.cgi

object {
object {
# State of one LED.
24
object {
# Color ("red", "green" or "none").
string Color;
# State ("on", "off", "blinking" or "alternating ").
string State;
} __LED_NAME__ ;
}* Data;
};

{
   "Body" : {
      "Data" : {
         "PowerLED" : {
            "Color" : "green",
            "State" : "on"
         },
         "SolarNetLED" : {
            "Color" : "green",
            "State" : "on"
         },
         "SolarWebLED" : {
            "Color" : "green",
            "State" : "on"
         },
         "WLANLED" : {
            "Color" : "green",
            "State" : "on"
         }
      }
   },
   "Head" : {
      "RequestArguments" : {},
      "Status" : {
         "Code" : 0,
         "Reason" : "",
         "UserMessage" : ""
      },
      "Timestamp" : "2018-08-05T16:54:50-03:00"
   }
}

////////////////////////////////////////////////////////////////////////////////

3.6 GetInverterInfo - Informa��es sobre cadastro do inverter
================================================================================
/solar_api/v1/GetInverterInfo.cgi


*)
  //Falta aqui

(*
3.7 GetActiveDeviceInfo - Informa��es sobre dispositivos que est�o on line
================================================================================
GetActiveDeviceInfo request
This request provides information about which devices are currently online.

http://192.168.25.2/solar_api/v1/GetActiveDeviceInfo.cgi?DeviceClass=System
{
   "Body" : {
      "Data" : {
         "Inverter" : {
            "1" : {
               "DT" : 75,
               "Serial" : "28450938"
            }
         },
         "Meter" : {},
         "Ohmpilot" : {},
         "SensorCard" : {},
         "Storage" : {},
         "StringControl" : {}
      }
   },
   "Head" : {
      "RequestArguments" : {
         "DeviceClass" : "System"
      },
      "Status" : {
         "Code" : 0,
         "Reason" : "",
         "UserMessage" : ""
      },
      "Timestamp" : "2018-08-05T17:03:25-03:00"
   }
}

Retorno diferente para os dispositivo em particular:
  -Inverter"
  -Storage"
  -Ohmpilot"
  -SensorCard"
  -StringControl"
  -Meter"
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

http://192.168.25.2/solar_api/v1/GetActiveDeviceInfo.cgi?DeviceClass=  --->>> Inverter, Sorage, ...
{
   "Body" : {
      "Data" : {
         "1" : {
            "DT" : 75,
            "Serial" : "28450938"
         }
      }
   },
   "Head" : {
      "RequestArguments" : {
         "DeviceClass" : "Inverter"
      },
      "Status" : {
         "Code" : 0,
         "Reason" : "",
         "UserMessage" : ""
      },
      "Timestamp" : "2018-08-05T17:00:29-03:00"
   }
}

////////////////////////////////////////////////////////////////////////////////

3.8 Meter: Sem retorno - Medidor de varias coisas
================================================================================
http://192.168.25.2/solar_api/v1/GetMeterRealtimeData.cgi?Scope=System
http://192.168.25.2/solar_api/v1/GetMeterRealtimeData.cgi?Scope=Device&DeviceId=1
{
   "Body" : {
      "Data" : {}
   },
   "Head" : {
      "RequestArguments" : {
         "DeviceClass" : "Meter",
         "Scope" : "System"
      },
      "Status" : {
         "Code" : 0,
         "Reason" : "",
         "UserMessage" : ""
      },
      "Timestamp" : "2018-08-05T17:12:35-03:00"
   }
}

////////////////////////////////////////////////////////////////////////////////

3.9 3.9 GetStorageRealtimeData - Baterias - sem retorno
================================================================================
GetStorageRealtimeData request
/solar_api/v1/GetStorageRealtimeData.cgi

////////////////////////////////////////////////////////////////////////////////

3.10 GetOhmPilotRealtimeData - Aquecedor !
================================================================================

/solar_api/v1/GetOhmPilotRealtimeData.cgi

O Fronius Ohmpilot � um regulador de consumo projetado para usar o excesso de
energia solar para aquecer a �gua. Gra�as � regulagem continuamente ajust�vel de
0 a 9 kW, a corrente excedente de energia fotovoltaica pode ser utilizada de
forma altamente eficiente e alimentada aos consumidores dom�sticos. O Fronius
Ohmpilot � usado principalmente para controlar de forma inteligente os elementos
de aquecimento para fornecer �gua quente em caldeiras e tanques de armazenamento
intermedi�rios, mas tamb�m pode ser usado para aquecedores infravermelhos de
aquecimento ou de toalhas. A energia solar pode, assim, fornecer uma casa familiar com n�veis m�dios de consumo de �gua com a maior parte de sua �gua quente de abril a outubro. O resultado � o consumo pr�prio m�ximo, uma redu��o nas emiss�es de CO2 do agregado familiar e menos desgaste no sistema de aquecimento principal do edif�cio durante os meses de ver�o.


////////////////////////////////////////////////////////////////////////////////

3.11 GetPowerFlowRealtimeData -

================================================================================
/solar_api/v1/GetPowerFlowRealtimeData.fcgi

Esta solicita��o fornece informa��es detalhadas sobre a grade de energia local.
Os valores respondidos representam o estado atual. Por causa dos dados terem
m�ltiplas origens assincr�nicas, � uma quest�o de fatos que a soma de todas as
for�as (grade, carga e gera��o) ser� diferente de zero. Esta solicita��o n�o se
preocupa com a visibilidade configurada de inversores �nicos. Todos os
inversores s�o reportados.

////////////////////////////////////////////////////////////////////////////////


$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


4 Archive Requests

Call is:
http://<hostname or IP>/solar_api/v1/GetArchiveData.cgi?<query parameters>

$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




================================================================================



////////////////////////////////////////////////////////////////////////////////



================================================================================



////////////////////////////////////////////////////////////////////////////////



================================================================================



////////////////////////////////////////////////////////////////////////////////



================================================================================



////////////////////////////////////////////////////////////////////////////////



================================================================================



////////////////////////////////////////////////////////////////////////////////



================================================================================

Pre�o kw residencial convencional : 0,76897

http://www.copel.com/hpcopel/root/nivel2.jsp?endereco=%2Fhpcopel%2Froot%2Fpagcopel2.nsf%2F5d546c6fdeabc9a1032571000064b22e%2Fe3a5cb971ca23bf503257488005939ba

*)


end.
