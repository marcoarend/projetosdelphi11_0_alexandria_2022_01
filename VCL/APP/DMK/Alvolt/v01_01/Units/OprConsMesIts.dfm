object FmOprConsMesIts: TFmOprConsMesIts
  Left = 339
  Top = 185
  Caption = 'WAC-PRODU-002 :: Item de Leitura de Consumo'
  ClientHeight = 513
  ClientWidth = 617
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 373
    Width = 617
    Height = 26
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitTop = 408
    ExplicitHeight = 89
    object CkContinuar: TCheckBox
      Left = 16
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 617
    Height = 64
    Align = alTop
    Caption = ' Dados do cabe'#231'alho:'
    Enabled = False
    TabOrder = 1
    object Label5: TLabel
      Left = 12
      Top = 20
      Width = 14
      Height = 13
      Caption = 'ID:'
      FocusControl = DBEdCodigo
    end
    object Label3: TLabel
      Left = 192
      Top = 20
      Width = 42
      Height = 13
      Caption = 'Controle:'
      Color = clBtnFace
      ParentColor = False
    end
    object Label6: TLabel
      Left = 72
      Top = 20
      Width = 22
      Height = 13
      Caption = 'Ano:'
      FocusControl = DBEdAnoRef
    end
    object Label7: TLabel
      Left = 132
      Top = 20
      Width = 23
      Height = 13
      Caption = 'M'#234's:'
      FocusControl = DBEdMesRef
    end
    object DBEdCodigo: TdmkDBEdit
      Left = 12
      Top = 36
      Width = 56
      Height = 21
      TabStop = False
      DataField = 'Codigo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 0
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
    object EdControle: TdmkEdit
      Left = 192
      Top = 36
      Width = 112
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object DBEdAnoRef: TdmkDBEdit
      Left = 72
      Top = 36
      Width = 56
      Height = 21
      TabStop = False
      DataField = 'AnoRef'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 2
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
    object DBEdMesRef: TdmkDBEdit
      Left = 132
      Top = 36
      Width = 56
      Height = 21
      TabStop = False
      DataField = 'MesRef'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 3
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 617
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 569
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 521
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 344
        Height = 32
        Caption = 'Item de Leitura de Consumo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 344
        Height = 32
        Caption = 'Item de Leitura de Consumo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 344
        Height = 32
        Caption = 'Item de Leitura de Consumo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 399
    Width = 617
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    ExplicitTop = 243
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 613
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 443
    Width = 617
    Height = 70
    Align = alBottom
    TabOrder = 5
    ExplicitTop = 287
    object PnSaiDesis: TPanel
      Left = 471
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 469
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 112
    Width = 617
    Height = 261
    Align = alTop
    Caption = 'GroupBox2'
    TabOrder = 0
    object GroupBox3: TGroupBox
      Left = 2
      Top = 15
      Width = 613
      Height = 244
      Align = alClient
      TabOrder = 0
      ExplicitHeight = 250
      object RGKind: TRadioGroup
        Left = 12
        Top = 12
        Width = 361
        Height = 165
        Caption = '  Tipo de d'#233'bito / cr'#233'dito: '
        ItemIndex = 0
        Items.Strings = (
          '0. Indefinido'
          '1. Energia el'#233'trica consumo (contrato m'#237'nimo)'
          '2. Energia el'#233'trico consumo (diferenciado)'
          '3. Cobran'#231'a extra consumo (contrato m'#237'nimo)'
          '4. Cobran'#231'a extra consumo (diferenciado)'
          '5. Ilumina'#231#227'o p'#250'blica'
          '6. Consumo gera'#231#227'o (cr'#233'dito)')
        TabOrder = 0
        OnClick = RGKindClick
      end
      object GroupBox4: TGroupBox
        Left = 12
        Top = 176
        Width = 361
        Height = 61
        Caption = ' Dados item: '
        TabOrder = 1
        object PnDados: TPanel
          Left = 2
          Top = 15
          Width = 357
          Height = 44
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label1: TLabel
            Left = 8
            Top = 0
            Width = 84
            Height = 13
            Caption = 'Quantidade kWh:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label2: TLabel
            Left = 124
            Top = 0
            Width = 75
            Height = 13
            Caption = 'Pre'#231'o por kWh:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label4: TLabel
            Left = 240
            Top = 0
            Width = 36
            Height = 13
            Caption = 'Total $:'
            Color = clBtnFace
            ParentColor = False
          end
          object EdKwh: TdmkEdit
            Left = 8
            Top = 16
            Width = 112
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnChange = EdKwhChange
          end
          object EdPreco: TdmkEdit
            Left = 124
            Top = 16
            Width = 112
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 10
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000000000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnChange = EdPrecoChange
          end
          object EdValor: TdmkEdit
            Left = 240
            Top = 16
            Width = 112
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
      end
    end
  end
end
