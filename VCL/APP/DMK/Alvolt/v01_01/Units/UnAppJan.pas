unit UnAppJan;

interface

uses
  Windows, SysUtils, Classes, Menus, UnDmkEnums, mySQLDbTables, Forms,
  UnInternalConsts, UnProjGroup_Vars, dmkGeral;

type
  IPsNaRedeResult = record
      IP: String;
      MAC: String;
  end;
  //
  TUnAppJan = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }

    //function MostraFormIPsNaRede(): String;
    function MostraFormIPsNaRede(): IPsNaRedeResult;
    //
    procedure MostraFormOpcoesApp();
    procedure MostraFormOprConsMes();
    procedure MostraFormWacProdGet();
    procedure MostraFormWacProdGet2();
    //
    function ApiDef(): Boolean;
  end;

var
  AppJan: TUnAppJan;


implementation

uses UnGrl_DmkDB, IPsNaRede, OpcoesApp, WacProdGet, WacProdGet2, OprConsMes;

{ TUnAppJan }

function TUnAppJan.ApiDef(): Boolean;
begin
  Result := VAR_OkDEVICEeAPI;
  if not Result then
  begin
    if Geral.MB_Pergunta(
    'As configura��es para acessar o inversor n�o est�o completas!' + sLineBreak +
    'Deseja configurar agora?') = ID_YES then
    begin
      MostraFormOpcoesApp();
      Result := VAR_OkDEVICEeAPI;
    end;
  end;
end;

function TUnAppJan.MostraFormIPsNaRede(): IPsNaRedeResult;
begin
  //Result := '';
  Result.IP := '';
  Result.MAC := '';
  Application.CreateForm(TFmIPsNaRede, FmIPsNaRede);
  FmIPsNaRede.ShowModal;
  //Result := FmIPsNaRede.FSelIP;
  Result.IP := FmIPsNaRede.FSelIP;
  Result.MAC := FmIPsNaRede.FSelMAC;
  FmIPsNaRede.Destroy;
end;

procedure TUnAppJan.MostraFormOpcoesApp();
begin
  Application.CreateForm(TFmOpcoesApp, FmOpcoesApp);
  FmOpcoesApp.ShowModal;
  FmOpcoesApp.Destroy;
end;

procedure TUnAppJan.MostraFormOprConsMes;
begin
  Application.CreateForm(TFmOprConsMes, FmOprConsMes);
  FmOprConsMes.ShowModal;
  FmOprConsMes.Destroy;
end;

procedure TUnAppJan.MostraFormWacProdGet();
begin
  if not ApiDef() then Exit;
  //
  Application.CreateForm(TFmWacProdGet, FmWacProdGet);
  FmWacProdGet.ShowModal;
  try
    FmWacProdGet.Destroy;
  except
    //
  end;
end;

procedure TUnAppJan.MostraFormWacProdGet2();
begin
  if not ApiDef() then Exit;
  //
  Application.CreateForm(TFmWacProdGet2, FmWacProdGet2);
  FmWacProdGet2.ShowModal;
  try
    FmWacProdGet2.Destroy;
  except
    //
  end;
end;

end.
