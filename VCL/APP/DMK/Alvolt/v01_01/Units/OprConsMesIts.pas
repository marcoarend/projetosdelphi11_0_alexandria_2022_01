unit OprConsMesIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, DBCtrls, dmkLabel, Mask, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants, dmkValUsu, dmkImage,
  UnDmkEnums, ZAbstractRODataset, ZAbstractDataset, ZDataset, UnGrl_DmkDB;

type
  TFmOprConsMesIts = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    RGKind: TRadioGroup;
    GroupBox4: TGroupBox;
    PnDados: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    EdKwh: TdmkEdit;
    EdPreco: TdmkEdit;
    EdValor: TdmkEdit;
    Label3: TLabel;
    EdControle: TdmkEdit;
    DBEdAnoRef: TdmkDBEdit;
    Label6: TLabel;
    DBEdMesRef: TdmkDBEdit;
    Label7: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdKwhChange(Sender: TObject);
    procedure EdPrecoChange(Sender: TObject);
    procedure RGKindClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenOprConsMesIts(Controle: Integer);
    procedure CalculaTotal();
  public
    { Public declarations }
    FQrCab, FQrIts: TZQuery;
    FDsCab: TDataSource;
  end;

  var
  FmOprConsMesIts: TFmOprConsMesIts;

implementation

uses UnMyObjects, Module, UnInternalConsts, DmkDAC_PF, OprConsMes;

{$R *.DFM}

procedure TFmOprConsMesIts.BtOKClick(Sender: TObject);
var
  Codigo, Controle, Kind, AnoRef, MesRef: Integer;
  Kwh, Preco, Valor: Double;
  SQLType: TSQLType;
begin
  SQLType             := ImgTipo.SQLType;
  //
  Codigo              := Geral.IMV(DBEdCodigo.Text);
  AnoRef              := Geral.IMV(DBEdAnoRef.Text);
  MesRef              := Geral.IMV(DBEdMesRef.Text);
  Controle            := EdControle.ValueVariant;
  Kind                := RGKind.ItemIndex;
  Kwh                 := EdKwh.ValueVariant;
  Preco               := EdPreco.ValueVariant;
  Valor               := EdValor.ValueVariant;

  //
  if MyObjects.FIC(Kind < 1, RGKind, 'Informe o tipo de d�bito / cr�dito') then
    Exit;
  //
  Controle := Grl_DmkDB.GetNxtCodigoInt('oprconsmesits', 'Controle', SQLType, Controle);
  //
  if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, 'oprconsmesits', False, [
  'Codigo', 'Kind', 'Kwh',
  'Preco', 'Valor'], [
  'Controle', 'AnoRef', 'MesRef'], [
  Codigo, Kind, Kwh,
  Preco, Valor], [
  Controle, AnoRef, MesRef], True, dmksqlinsInsOnly, '', TDeviceType.stDesktop, False) then
  begin
    FmOprConsMes.AtualizaSomas(Codigo, AnoRef, MesRef);
    ReopenOprConsMesIts(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdControle.ValueVariant  := 0;
      RGKind.ItemIndex         := 0;
      EdKwh.ValueVariant       := 0;
      EdPreco.ValueVariant     := 0;
      EdValor.ValueVariant     := 0;
      //
      //if RGKind.Focused then
        //RGKind.SetFocus;
    end else Close;
  end;
end;

procedure TFmOprConsMesIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOprConsMesIts.CalculaTotal();
var
  kWh, Preco: Double;
begin
  kWh   := EdKwh.ValueVariant;
  Preco := EdPreco.ValueVariant;
  if (kWh > 0) and (Preco > 0) then
    EdValor.ValueVariant := kWh * Preco;
end;

procedure TFmOprConsMesIts.EdKwhChange(Sender: TObject);
begin
  CalculaTotal();
end;

procedure TFmOprConsMesIts.EdPrecoChange(Sender: TObject);
begin
  CalculaTotal();
end;

procedure TFmOprConsMesIts.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdAnoRef.DataSource := FDsCab;
  DBEdMesRef.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmOprConsMesIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
end;

procedure TFmOprConsMesIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOprConsMesIts.ReopenOprConsMesIts(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    Grl_DmkDB.AbreQuery(FQrIts, Dmod.MyDB);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmOprConsMesIts.RGKindClick(Sender: TObject);
begin
  //EdKwh.SetFocus;
end;

end.
