unit UnProjGroup_Vars;

interface

uses Vcl.StdCtrls, UnAppEnums;

type
  ObtemUrlBaseAPIResult = record
      Versao: String;
      UrlBaseAPI: String;
  end;

var
  VAR_DeviceAssembler: TDevicesAssembler = TDevicesAssembler.assembUnknown;
  VAR_DeviceHost: String = '';
  VAR_URLBaseAPI: String = '';
  VAR_DeviceMACAddress: String = '';
  VAR_DeviceConnected: Boolean = False;
  VAR_OtherURLBaseAPI: String = '';
  VAR_OkDEVICEeAPI: Boolean = False;
  CO_HostPingTimeOutShort: Integer = 100;
  CO_HostPingTimeOutNormal: Integer = 500;

//const // que podem mudar  !! Ver B D E R M



implementation

end.
