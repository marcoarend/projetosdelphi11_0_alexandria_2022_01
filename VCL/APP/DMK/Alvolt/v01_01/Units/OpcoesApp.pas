unit OpcoesApp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  ZAbstractRODataset, ZAbstractDataset, ZDataset, uLkJSON, UnProjGroup_Vars,
  UnDevicesPF, UnAppEnums, dmkDBGridZTO, Vcl.Menus;

type
  TFmOpcoesApp = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrDevices: TZQuery;
    Panel5: TPanel;
    Label32: TLabel;
    EdDeviceHost: TdmkEdit;
    Label1: TLabel;
    EdURLBaseAPI: TdmkEdit;
    SbURLBaseAPI: TSpeedButton;
    SbDeviceHost: TSpeedButton;
    RGDeviceAssembler: TRadioGroup;
    DsDevices: TDataSource;
    QrDevicesCodigo: TLargeintField;
    QrDevicesDeviceAssembler: TLargeintField;
    QrDevicesUniqueId: TWideStringField;
    QrDevicesDeviceSeq: TLargeintField;
    QrDevicesDeviceType: TWideStringField;
    QrDevicesPVPower: TLargeintField;
    QrDevicesCustomName: TWideStringField;
    QrDevicesLk: TLargeintField;
    QrDevicesDataCad: TDateField;
    QrDevicesDataAlt: TDateField;
    QrDevicesUserCad: TLargeintField;
    QrDevicesUserAlt: TLargeintField;
    QrDevicesAlterWeb: TSmallintField;
    QrDevicesAtivo: TSmallintField;
    GroupBox2: TGroupBox;
    dmkDBGridZTO1: TdmkDBGridZTO;
    QrDevicesNome_DeviceAssembler: TWideStringField;
    Panel6: TPanel;
    BtDevice: TBitBtn;
    PMDevice: TPopupMenu;
    MiNewDivice1: TMenuItem;
    MiUpdDevice1: TMenuItem;
    MiDelDevice1: TMenuItem;
    GroupBox3: TGroupBox;
    dmkDBGridZTO2: TdmkDBGridZTO;
    Panel7: TPanel;
    BtLogger: TBitBtn;
    PMLogger: TPopupMenu;
    MiNewLogger1: TMenuItem;
    MiUpdLogger1: TMenuItem;
    MiDelLogger1: TMenuItem;
    QrLoggers: TZQuery;
    DsLoggers: TDataSource;
    QrLoggersCodigo: TLargeintField;
    QrLoggersDeviceAssembler: TLargeintField;
    QrLoggersUniqueId: TWideStringField;
    QrLoggersDeviceSeq: TLargeintField;
    QrLoggersProductID: TWideStringField;
    QrLoggersPlatformID: TWideStringField;
    QrLoggersHWVersion: TWideStringField;
    QrLoggersSWVersion: TWideStringField;
    QrLoggersTimezoneLocation: TWideStringField;
    QrLoggersTimezoneName: TWideStringField;
    QrLoggersUTCOffset: TLargeintField;
    QrLoggersCashFactor: TFloatField;
    QrLoggersDeliveryFactor: TFloatField;
    QrLoggersCashCurrency: TWideStringField;
    QrLoggersCO2Factor: TFloatField;
    QrLoggersCO2Unit: TWideStringField;
    QrLoggersLk: TLargeintField;
    QrLoggersDataCad: TDateField;
    QrLoggersDataAlt: TDateField;
    QrLoggersUserCad: TLargeintField;
    QrLoggersUserAlt: TLargeintField;
    QrLoggersAlterWeb: TSmallintField;
    QrLoggersAtivo: TSmallintField;
    QrLoggersNome_DeviceAssembler: TWideStringField;
    SbdeviceMACAddress: TSpeedButton;
    EdDeviceMACAddress: TdmkEdit;
    Label2: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure RGDeviceAssemblerClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SbDeviceHostClick(Sender: TObject);
    procedure SbURLBaseAPIClick(Sender: TObject);
    procedure QrDevicesCalcFields(DataSet: TDataSet);
    procedure BtDeviceClick(Sender: TObject);
    procedure MiDelDevice1Click(Sender: TObject);
    procedure MiNewDivice1Click(Sender: TObject);
    procedure BtLoggerClick(Sender: TObject);
    procedure MiNewLogger1Click(Sender: TObject);
    procedure MiDelLogger1Click(Sender: TObject);
    procedure QrLoggersCalcFields(DataSet: TDataSet);
    procedure SbdeviceMACAddressClick(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
  end;

  var
  FmOpcoesApp: TFmOpcoesApp;

implementation

uses UnMyObjects, UnAppPF, UnAppJan, Module, UnGrl_DmkDB, UnDmkWeb, UnMyJSON;

{$R *.DFM}

procedure TFmOpcoesApp.BtDeviceClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMDevice, BtDevice);
end;

procedure TFmOpcoesApp.BtLoggerClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMLogger, BtLogger);
end;

procedure TFmOpcoesApp.BtOKClick(Sender: TObject);
const
  Codigo = 1;
  SQLTipo = TSQLType.stUpd;
var
  DeviceAssembler: Integer;
  DeviceHost, URLBaseAPI, DeviceMACAddress: String;
begin
  DeviceAssembler := RGDeviceAssembler.ItemIndex;
  DeviceHost := EdDeviceHost.Text;
  URLBaseAPI := EdURLBaseAPI.Text;
  DeviceMACAddress := EdDeviceMACAddress.Text;
  //
  if MyObjects.FIC(Trim(DeviceHost) = '', EdDeviceHost, 'IP (host) do aparelho n�o definido!') then Exit;
  if MyObjects.FIC(Trim(URLBaseAPI) = '', EdURLBaseAPI, 'URL base (API) n�o definido!') then Exit;
  //
  if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLTipo, 'controle', False,
    ['DeviceAssembler', 'DeviceHost', 'URLBaseAPI',
    'DeviceMACAddress'], ['Codigo'],
    [DeviceAssembler, DeviceHost, URLBaseAPI,
    DeviceMACAddress], [Codigo],
     False, dmksqlinsInsOnly, '', stDesktop) then
  begin
    Dmod.RefreshApiBaseInfo();
    Close;
  end;
end;

procedure TFmOpcoesApp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOpcoesApp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOpcoesApp.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Dmod.RefreshApiBaseInfo();
end;

procedure TFmOpcoesApp.FormCreate(Sender: TObject);
var
  Qry: TZQuery;
begin
  ImgTipo.SQLType := stLok;
  //
  Qry := TZQuery.Create(Dmod);
  try
    Qry.SQL.Text := 'SELECT * FROM controle WHERE Codigo=1';
    Grl_DmkDB.AbreQuery(Qry, Dmod.MyDB);
    VAR_DeviceAssembler := TDevicesAssembler(Qry.FieldByName('DeviceAssembler').AsInteger);
    RGDeviceAssembler.ItemIndex := Integer(VAR_DeviceAssembler);
    EdDeviceHost.Text := Qry.FieldByName('DeviceHost').AsString;
    EdURLBaseAPI.Text := Qry.FieldByName('URLBaseAPI').AsString;
    EdURLBaseAPI.Text := Qry.FieldByName('URLBaseAPI').AsString;
    EdDeviceMACAddress.Text := Qry.FieldByName('DeviceMACAddress').AsString;
    //
  finally
    Qry.Free;
  end;
  Grl_DmkDB.ReopenQuery(QrDevices, Dmod.MyDB);
  Grl_DmkDB.ReopenQuery(QrLoggers, Dmod.MyDB);
end;

procedure TFmOpcoesApp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOpcoesApp.MiDelDevice1Click(Sender: TObject);
var
  Pergunta, Tabela, Campo: String;
  Inteiro1: Integer;
  DataBase: TComponent;
begin
  Pergunta := 'Confirma a exclus�o do cadastro do dispositivo selecionado: ' +
              QrDevicesCustomName.Value;
  Tabela   := 'devices';
  Campo    := 'Codigo';
  Inteiro1 := QrDevicesCodigo.Value;
  DataBase := Dmod.MyDB;
  if Grl_DmkDB.ExcluiRegistroInt1(Pergunta, Tabela, Campo, Inteiro1, DataBase) =
  ID_Yes then
  begin
    Grl_DmkDB.ReopenQuery(QrDevices, Dmod.MyDB);
  end;
end;

procedure TFmOpcoesApp.MiDelLogger1Click(Sender: TObject);
var
  Pergunta, Tabela, Campo: String;
  Inteiro1: Integer;
  DataBase: TComponent;
begin
  Pergunta := 'Confirma a exclus�o do cadastro do data logger selecionado: ' +
              QrLoggersUniqueId.Value;
  Tabela   := 'loggers';
  Campo    := 'Codigo';
  Inteiro1 := QrLoggersCodigo.Value;
  DataBase := Dmod.MyDB;
  if Grl_DmkDB.ExcluiRegistroInt1(Pergunta, Tabela, Campo, Inteiro1, DataBase) =
  ID_Yes then
  begin
    Grl_DmkDB.ReopenQuery(QrLoggers, Dmod.MyDB);
  end;
end;

procedure TFmOpcoesApp.MiNewDivice1Click(Sender: TObject);
const
  Avisa = True;
var
  MyArr: TMyJsonBasesArr;
  JSonStr, Nome: String;
  jParsed, jBody, jData, Item, nItem: TlkJSONbase;
  I, N: Integer;
begin
  if DevicesPF.AtualizaDevicesBD(VAR_DeviceAssembler) then
  begin
    Grl_DmkDB.ReopenQuery(QrDevices, Dmod.MyDB);
    Geral.MB_Info('Cadastro de equipamentos atualizado!');
  end;
end;

procedure TFmOpcoesApp.MiNewLogger1Click(Sender: TObject);
const
  Avisa = True;
var
  MyArr: TMyJsonBasesArr;
  JSonStr, Nome: String;
  jParsed, jBody, jData, Item, nItem: TlkJSONbase;
  I, N: Integer;
begin
  if DevicesPF.AtualizaLoggersBD(VAR_DeviceAssembler) then
  begin
    Grl_DmkDB.ReopenQuery(QrLoggers, Dmod.MyDB);
    Geral.MB_Info('Cadastro de data loggers atualizado!');
  end;
end;

procedure TFmOpcoesApp.QrDevicesCalcFields(DataSet: TDataSet);
begin
  QrDevicesNome_DeviceAssembler.Value := AppEnums.NomeDevicesAssembler(
    TDevicesAssembler(QrDevicesDeviceAssembler.Value));
end;

procedure TFmOpcoesApp.QrLoggersCalcFields(DataSet: TDataSet);
begin
  QrLoggersNome_DeviceAssembler.Value := AppEnums.NomeDevicesAssembler(
    TDevicesAssembler(QrLoggersDeviceAssembler.Value));
end;

procedure TFmOpcoesApp.RGDeviceAssemblerClick(Sender: TObject);
begin
  VAR_DeviceAssembler := TDevicesAssembler(RGDeviceAssembler.ItemIndex);
end;

procedure TFmOpcoesApp.SbDeviceHostClick(Sender: TObject);
var
  //SelIP: String;
  MyIPsNaTRedeResult: IPsNaRedeResult;
begin
  MyIPsNaTRedeResult := AppJan.MostraFormIPsNaRede();
(*
  if SelIP <> '' then
    EdDeviceHost.Text := SelIP;
*)
  if MyIPsNaTRedeResult.IP <> '' then
    EdDeviceHost.Text := MyIPsNaTRedeResult.IP;
end;

procedure TFmOpcoesApp.SbdeviceMACAddressClick(Sender: TObject);
var
  //DeviceMACAddress: String;
  MyIPsNaRedeResult: IPsNaRedeResult;
begin
  MyIPsNaRedeResult := AppJan.MostraFormIPsNaRede();
  //if DeviceMACAddress <> '' then
    EdDeviceMACAddress.Text := MyIPsNaRedeResult.MAC;
end;

procedure TFmOpcoesApp.SbURLBaseAPIClick(Sender: TObject);
var
  //URLBaseAPI: String;
  Retorno: ObtemUrlBaseAPIResult;
begin
  VAR_DeviceAssembler := TDevicesAssembler(RGDeviceAssembler.ItemIndex);
  VAR_DeviceHost := EdDeviceHost.Text;
  //
  if DevicesPF.ObtemUrlBaseAPI(VAR_DeviceAssembler, True, Retorno, True,
  nil) then
    if Retorno.URLBaseAPI <> '' then
      EdURLBaseAPI.Text := Retorno.URLBaseAPI;
end;

end.
